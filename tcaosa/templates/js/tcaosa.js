/**
 * Javascript for frontend
 * 
 * 
 * 
 * @author Thilo Muller(2013)
 * @package TCAOSA
 */

/**
 * Fade Background
 */
function fadeBackground() {
	$('#headerBackground').fadeIn(1500, 'swing');
}

/**
 * Slides the header text to the right
 */
function slideRight() {
	$("#headerTitle").animate({"marginLeft": "+=420px"}, 2000, 'swing');
}

/**
 * Initialize the page
 */
function init() {
	fadeBackground();
	slideRight();
	
	if ($( "#date_of_birth" )) {
		$(function() {
			$( "#date_of_birth" ).datepicker({
				dateFormat: "yy-mm-dd",
				changeYear: true,
				yearRange: "-100:-10",
				changeMonth: true
			});
		});
	}
	if ($( "#start_date" )) {
		$(function() {
			$( "#start_date" ).datepicker();
			$( "#start_date" ).datepicker("option",'dateFormat','yy-mm-dd');
		});
	}
	
	$( "#suburb" ).autocomplete({
		source: '/modules/member_management/ajax/suburb_select.ajax.php'
	 });
	$( "#city" ).autocomplete({
		source: '/modules/member_management/ajax/city_select.ajax.php'
	 });
	
}

/**
 * Toggle between the login form and recover password form
 */
function toggleRecover() {
	var formLogin = document.getElementById('loginForm');
	var formRecover = document.getElementById('recoverForm');

	if (formLogin.style.display == 'block') { //Show recover form
		formLogin.style.display = 'none';
		formRecover.style.display = 'block';
	} else { //Show login form
		formLogin.style.display = 'block';
		formRecover.style.display = 'none';
	}

}

/**
 * Login a user
 */
function login() {
	var saveRequest = $.ajax({
        url: '/modules/login/login.ajax.php',
        type: 'post',
        data: $('#loginForm').serialize()
    });
	
	saveRequest.done(function(xhr) {loginSuccess(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){loginFailed(xhr,ajaxOptions, thrownError)})
}

/**
 * Failed Login
 */
function loginFailed(xhr,ajaxOptions, thrownError) {
	switch (xhr.status) {
		case 404:
			document.getElementById('loginMessage').innerHTML = '<b>Could not log you in</b>.<br>The server returned an error: 404 '+thrownError+'.';
			document.getElementById('loginMessage').className = 'messageBlockError';
			break;
		case 500:
			document.getElementById('loginMessage').innerHTML = '<b>Could not log you in</b>.<br>The server returned an error: 500 '+thrownError+'.';
			document.getElementById('loginMessage').className = 'messageBlockError';
			break;
		default:
			document.getElementById('loginMessage').innerHTML = '<b>Could not log you in</b>.<br>Please check your internet connection and try again ('+thrownError+').';
			document.getElementById('loginMessage').className = 'messageBlockError';
			break;
	}	
}

/**
 * Login Success
 */
function loginSuccess(xhr) {
	if (xhr == 'OK') {
		document.location = document.location;
	} else {
		document.getElementById('loginMessage').innerHTML = '<b>Could not log you in</b>.<br>Username/password combination could not be found.';
		document.getElementById('loginMessage').className = 'messageBlockError';
	}
}

/**
 * Recover a password
 */
function recover() {
	var saveRequest = $.ajax({
        url: '/modules/login/recover.ajax.php',
        type: 'post',
        data: $('#recoverForm').serialize()
    });
	
	saveRequest.done(function(xhr) {recoverSuccess(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){recoverFailed(xhr,ajaxOptions, thrownError)})
}

/**
 * Recover Success
 * 
 * @todo Complete email send 
 */
function recoverSuccess(xhr) {
	if (xhr == 'OK') {
		toggleRecover();
	} else {
		
	}
	
	
}

/**
 * Recover Failed
 */
function recoverFailed(xhr,ajaxOptions, thrownError) {
	switch (xhr.status) {
		case 404:
			document.getElementById('loginMessage').innerHTML = '<b>Could not recover password</b>.<br>The server returned an error: 404 '+thrownError+'.';
			document.getElementById('loginMessage').className = 'messageBlockError';
			break;
		case 500:
			document.getElementById('loginMessage').innerHTML = '<b>Could not recover password</b>.<br>The server returned an error: 500 '+thrownError+'.';
			document.getElementById('loginMessage').className = 'messageBlockError';
			break;
		default:
			document.getElementById('loginMessage').innerHTML = '<b>Could not recover password</b>.<br>Please check your internet connection and try again ('+thrownError+').';
			document.getElementById('loginMessage').className = 'messageBlockError';
			break;
	}	
}

/**
 * Redirect to subscribe page
 */
function subscribe() {
	pageURL = 'register_popup.html';
	var left = (screen.width/2)-(250/2);
	var top = (screen.height/2)-(150/2);
	var targetWin = window.open (pageURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=250, height=150, top='+top+', left='+left);
//	document.location = '/index.php?page=subscribe';
}

/**
 * Submit the sebscription form
 */
function submitSubscription() {
	var form = document.getElementById('registrationForm');
	
	document.getElementById('registrationMessage').innerHTML = 'Registration in Progress...';
	document.getElementById('registrationMessage').className = 'messageBlockOk';
	
	if (checkForm(form) === true) {
//	if (true) {
		var saveRequest = $.ajax({
			url: '/modules/member_management/ajax/register_telkom.ajax.php',
			type: 'post',
			data: $('#registrationForm').serialize()
		});

		saveRequest.done(function(xhr) {registrationSuccess(xhr)});
		saveRequest.error(function(xhr,ajaxOptions, thrownError){registrationFailed(xhr,ajaxOptions, thrownError)})
	} else {
		//Do Nothing
	}
}

/**
 * registration Success
 */
function registrationSuccess(xhr) {
	document.getElementById('registrationMessage').innerHTML = 'Registration Success';
	document.getElementById('registrationMessage').className = 'messageBlockOk';
	
	setTimeout(function() {document.location='/index.php?page=subscription_complete'}, 3000);
			
}

/**
 * Registration Failed
 */
function registrationFailed(xhr,ajaxOptions, thrownError) {
	switch (xhr.status) {
		case 404:
			document.getElementById('registrationMessage').innerHTML = '<b>An Error occured during registration</b> :: The server returned an error: 404 '+thrownError+'.';
			document.getElementById('registrationMessage').className = 'messageBlockError';
			break;
		case 500:
			document.getElementById('registrationMessage').innerHTML = '<b>An Error occured during registration</b> :: The server returned an error: 500 '+thrownError+'.';
			document.getElementById('registrationMessage').className = 'messageBlockError';
			break;
		default:
			document.getElementById('registrationMessage').innerHTML = '<b>An Error occured during registration</b> :: Please check your internet connection and try again ('+thrownError+').';
			document.getElementById('registrationMessage').className = 'messageBlockError';
			break;
	}	
}

/**
 * Check the contact us form to make sure it completed properly
 */
function checkForm(form) {
    var emailID = form.email;
    if(form.title.value == "") {
		alert("Please fill in your Title!");
		form.title.focus();
		return false;
	} else if(form.firstname.value == "") {
		alert("Please fill in your Name!");
		form.firstname.focus();
		return false;
	} else if(form.lastname.value == "") {
		alert("Please fill in your Surname!");
		form.lastname.focus();
		return false;
	} else if(isNaN(form.tel_home.value)) {
		alert("Please use only numbers for your home telephone!");
		form.tel_home.focus();
		return false;
	} else if(isNaN(form.tel_work.value)) {
		alert("Please use only numbers for your work telephone!");
		form.tel_work.focus();
		return false;
	} else if(isNaN(form.cellphone.value)) {
		alert("Please use only numbers for your cellphone!");
		form.cellphone.focus();
		return false;
	} else if ((emailID.value==null)||(emailID.value=="")) {
		alert("Please Enter your Email")
		emailID.focus()
		return false
	} if (echeck(emailID.value)==false) {
		emailID.focus()
		return false
	} else if(form.id_number.value == "") {
		alert("Please fill in your ID Number!");
		form.id_number.focus();
		return false;
	} else if(isNaN(form.id_number.value)){
		alert("Please use only numbers!");
		form.id_number.focus();
		return false;
	} else if(form.address_1.value == "") {
		alert("Please fill in your Address!");
		form.physical_address.focus();
		return false;	
	} else if(isNaN(form.postal_code.value)) {
		alert("Please use only numbers!");
		form.postal_code.focus();
		return false;
	} else if(form.date_of_birth.value == "") {
		alert("Please fill in your Date of Birth!");
		form.date_of_birth.focus();
		return false;
	} else if(form.language.value == "") {
		alert("Please select your Language!");
		form.language.focus();
		return false;
	} else if(form.username.value == "") {
		alert("Please fill in your Username!");
		form.username.focus();
		return false;
	} else if(form.password.value == "") {
		alert("Please fill in your Password!");
		form.password.focus();
		return false;
	} else if(form.conf_password.value == "" || form.conf_password.value != form.password.value) {
		alert("Please confirm your Password!");
		form.conf_password.focus();
		return false;
	}
	return true;
}

function echeck(str) {
	var at="@"
	var dot="."
	var lat=str.indexOf(at)
	var lstr=str.length
	var ldot=str.indexOf(dot)
	if (str.indexOf(at)==-1){
	   alert("Invalid E-mail")
	   return false
	}

	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
	   alert("Invalid E-mail")
	   return false
	}

	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		alert("Invalid E-mail")
		return false
	}

	 if (str.indexOf(at,(lat+1))!=-1){
		alert("Invalid E-mail")
		return false
	 }

	 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		alert("Invalid E-mail")
		return false
	 }

	 if (str.indexOf(dot,(lat+2))==-1){
		alert("Invalid E-mail")
		return false
	 }

	 if (str.indexOf(" ")!=-1){
		alert("Invalid E-mail")
		return false
	 }
	 return true
}

/**
 * Send a message
 */
function sendMessage() {
	document.getElementById('contactMessage').innerHTML = 'Sending Message...';
	document.getElementById('contactMessage').className = 'messageBlockOk';
	
	var saveRequest = $.ajax({
		url: '/ajax/send_message.ajax.php',
		type: 'post',
		data: $('#contactForm').serialize()
	});

	saveRequest.done(function(xhr) {contactSuccess(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){contactFailed(xhr,ajaxOptions, thrownError)})
}

/**
 * registration Success
 */
function contactSuccess(xhr) {
	console.log(xhr);
	
	if (xhr == 'OK') {
		document.getElementById('contactMessage').innerHTML = 'Your message was sent successfully';
		document.getElementById('contactMessage').className = 'messageBlockOk';
//		setTimeout(function() {document.location='/index.php?page=contact_complete'}, 3000);
	} else {
		document.getElementById('contactMessage').innerHTML = xhr;
		document.getElementById('contactMessage').className = 'messageBlockError';
		document.getElementById('captcha').src = '/shared/securimage/securimage_show.php?' + Math.random();		
	}
}

/**
 * Registration Failed
 */
function contactFailed(xhr,ajaxOptions, thrownError) {
	switch (xhr.status) {
		case 404:
			document.getElementById('contactMessage').innerHTML = '<b>An Error occured during registration</b> :: The server returned an error: 404 '+thrownError+'.';
			document.getElementById('contactMessage').className = 'messageBlockError';
			break;
		case 500:
			document.getElementById('contactMessage').innerHTML = '<b>An Error occured during registration</b> :: The server returned an error: 500 '+thrownError+'.';
			document.getElementById('contactMessage').className = 'messageBlockError';
			break;
		default:
			document.getElementById('contactMessage').innerHTML = '<b>An Error occured during registration</b> :: Please check your internet connection and try again ('+thrownError+').';
			document.getElementById('contactMessage').className = 'messageBlockError';
			break;
	}	
}