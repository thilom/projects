/*
 * Custom script for Zita collections.
 */

/**
 * Copies irregular fields to fields required for for submission to work
 */
function checkout() {

	//Check Form
	form_ok = checkRequired();

	if (form_ok === false) {
		return false;
	}

	//Address Line 1 - street number & street name
	streetName = document.getElementById('streetName').value;
	streetNumber = document.getElementById('streetNumber').value;
	document.getElementById('shipping1').value = streetNumber + ' ' + streetName;
	document.getElementById('billing1').value = streetNumber + ' ' + streetName;

	//Suburb
	document.getElementById('shipping2').value = document.getElementById('suburb').value;
	document.getElementById('billing2').value = document.getElementById('suburb').value;

	//Town
	town = document.getElementById('city').value;
	if (town == 'Other') town = document.getElementById('other2').value;
	document.getElementById('shipping3').value = town;
	document.getElementById('billing3').value = town;

	//Postal Code
	document.getElementById('shippingCode').value = document.getElementById('postalcode').value;
	document.getElementById('billingCode').value = document.getElementById('postalcode').value;

	//Name
	firstname = document.getElementById('firstname').value;
	lastname = document.getElementById('lastname').value;
	document.getElementById('contact_name').value = firstname + ' ' + lastname;

	//Date of birth
	day = document.getElementById('day').value;
	month = document.getElementById('month').value;
	document.getElementById('custom_date_of_birth').value = day + ' ' + month;

	//Hear about
	hearabout = document.getElementById('hearabout').value;
	if (hearabout == 'other') hearabout = document.getElementById('other').value;
	document.getElementById('custom_hear_about').value = hearabout;


	return true;

}


