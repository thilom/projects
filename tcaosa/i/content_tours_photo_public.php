<?php
/**
 *  Display a photo list for tours - font end
 *
 *  @author Thilo Muller(2011)
 *  @version $Id: content_tours_photo_public.php 87 2011-09-14 06:45:49Z thilo $
 */

//Vars

//Get CSS file
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_$id.css";
if (is_file($file)) {
	echo "<link rel=stylesheet href='/styles/css_$id.css'>";
} else {
	echo "<link rel=stylesheet href='/modules/tours/defaults/default_tours_photo.css'>";
}

//Get Preferences
$pref_data = get_area_data($id, 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);

//Get list of tours
$statement = "SELECT tour_id, tour_name, tour_image, tour_category, UNIX_TIMESTAMP(tour_start) as tour_start, DATE_FORMAT(tour_start,'%e %M %Y') as start_date, DATE_FORMAT(tour_end,'%e %M %Y') as end_date FROM {$GLOBALS['db_prefix']}_tours WHERE ";

$statement .= "tour_enabled = 'Y' AND tour_type='tour'";
$sql_tours = $GLOBALS['dbCon']->prepare($statement);
$sql_tours->execute();
$sql_tour_data = $sql_tours->fetchAll(PDO::FETCH_ASSOC);
$sql_tours->closeCursor();

//Build table
$col_counter = 1;
$table = "<table class='tplTable'>";
foreach ($sql_tour_data as $tour_data) {
	//Check if tour is limited to category
	if (!empty($preferences['category'])) {
		if ($tour_data['tour_category'] != $preferences['category']) continue;
	}

	//Check if tour is time based
	if (!empty($preferences['time_limit'])) {
		$current_date = mktime();
		$cutoff_date = strtotime("now + {$preferences['time_limit']} months");
		if ($tour_data['tour_start'] < $current_date || $tour_data['tour_start']>$cutoff_date) continue;
	}

	if ($col_counter == 1) $table .= "<tr>";
	$table .= "<td>";
	if (!empty($preferences['link_id'])) $table .= "<a href='/index.php?page={$preferences['link_id']}&tour_id={$tour_data['tour_id']}'>";
	$table .= "<img border=0 src='/i/tours/{$tour_data['tour_image']}' class='tplImage'>";
	if (!empty($preferences['link_id'])) $table .= "</a>";
	if ($preferences['include_name'] == '1') {
		$table .= "<span class='tplName'>";
		if (!empty($preferences['link_id'])) $table .= "<a href='/index.php?page={$preferences['link_id']}&tour_id={$tour_data['tour_id']}' >";
		$table .= "<br>{$tour_data['tour_name']}";
			if (!empty($preferences['link_id'])) $table .= "</a></span>";
	}
	if ($preferences['include_dates'] == '1') {
		$table .= "<span class='tplDates'>";
		if (!empty($preferences['link_id'])) $table .= "<a href='/index.php?page={$preferences['link_id']}&tour_id={$tour_data['tour_id']}' >";
		$table .= "<br>{$tour_data['start_date']}";
		if (!empty($tour_data['end_date']) && ($tour_data['end_date'] != $tour_data['start_date'])) $table .= " - {$tour_data['end_date']}";
		if (!empty($preferences['link_id'])) $table .= "</a></span>";
	}
	if ($preferences['include_category'] == '1') {
		$table .= "<span class='tplCategory'>";
		if (!empty($preferences['link_id'])) $table .= "<a href='/index.php?page={$preferences['link_id']}&tour_id={$tour_data['tour_id']}' >";
		$table .= "<br>{$tour_data['tour_category']}";
			if (!empty($preferences['link_id'])) $table .= "</a></span>";
	}
	$table .= "</td>";
	if ($col_counter == $preferences['cols']) {
		$table .= "</tr>";
		$col_counter = 1;
	} else {
		$col_counter++;
	}
}

$table .= "</table>";

echo $table;
?>
