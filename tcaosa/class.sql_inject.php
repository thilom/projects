<?php
/**
 * this class try to detect KNOWN form of SQL inject
 * 
 */
class sql_inject {
    private $urlRedirect;		//@urlRedirect url to redirect if an sql inject attempt is detect. if unset, value is FALSE
    private $bdestroy_session;	//@bdesrtoy_session does the session must be destroy if an attempt is detect
    private $rq;				//@rq the SQL statement to test
    private $bLog;				//@bLog if not FALSE, the url to the log file
    
    /**
     * Constructor to setup sql injection. Initialises private variables.
     *
     * @param boolean mLog optinal
     * @param boolean bdestroy_session optional 
     * @param string urlRedirect optional     
     * @return boolean
     */
    public function sql_inject($mLog=FALSE,$bdestroy_session=FALSE,$urlRedirect=FALSE) {
        $this->bLog = (($mLog!=FALSE)?$mLog:'');
        $this->urlRedirect = (((trim($urlRedirect)!='') && file_exists($urlRedirect))?$urlRedirect:'');
        $this->bdestroy_session = $bdestroy_session;
        $this->rq = '';
        return true;
    }

    /**
     * test if there is a sql inject attempt detect
     *
     * @param string sRQ required. SQL Data to test     
     * @return bool
     */
    public function test_sql($sRQ) {
        $sRQ = strtolower($sRQ);
        $this->rq = $sRQ;
        $aValues = array();
        $aTemp = array(); // temp array
        $aWords = array(); 
        $aSep = array(' and ',' or '); // separators to detect in where clause
        $matches = array();
        
        // is there an attempt to unused part of the sql statement?
        if (is_int((strpos($sRQ,"#")))&&$this->_in_post('#')) {
        	return $this->detect();
        }
        
        // is there a attempt to do a 2nd SQL request ?
        if (is_int(strpos($sRQ,';'))) {
            $aTemp = explode(';',$sRQ);
            if ($this->_in_post($aTemp[1])) {
            	return $this->detect();
            }
        }
        
        $aTemp = explode(" where ",$sRQ);
        
        if (count($aTemp)==1) {
        	return FALSE;
        }
        
        $sConditions = $aTemp[1];
        $aWords = explode(" ",$sConditions);
        
        if(strcasecmp($aWords[0],'select')!=0) {
        	$aSep[] = ',';
        }
        
        $sSep = '('.implode('|',$aSep).')';
        $aValues = preg_split($sSep,$sConditions,-1, PREG_SPLIT_NO_EMPTY);

        // test the always true expressions
        foreach ($aValues as $v) {
            // SQL injection like 1=1 or a=a or 'za'='za'
            if (is_int(strpos($v,'='))) {
                 $aTemp = explode('=',$v);
                 
                 if (trim($aTemp[0])==trim($aTemp[1])) {
                 	return $this->detect();
                 }
            }
            
            //SQL injection like 1<>2
            if (is_int(strpos($v,'<>'))) {
                $aTemp = explode('<>',$v);
                
                if ((trim($aTemp[0])!=trim($aTemp[1]))&& ($this->_in_post('<>'))) {
                	return $this->detect();
                }
            }
        }
        
        if (strpos($sConditions,' null')) {
            if (preg_match("/null +is +null/",$sConditions)) {
            	return $this->detect();
            }
            
            if (preg_match("/is +not +null/",$sConditions,$matches)) {
                foreach ($matches as $v) {
                    if ($this->_in_post($v)){
                    	return $this->detect();
                    }
                }
            }
        }
        
        if (preg_match("/[a-z0-9]+ +between +[a-z0-9]+ +and +[a-z0-9]+/",$sConditions,$matches)) {
            $Temp = explode(' between ',$matches[0]);
            $Evaluate = $Temp[0];
            $Temp = explode(' and ',$Temp[1]);

            if ((strcasecmp($Evaluate,$Temp[0])>0) && (strcasecmp($Evaluate,$Temp[1])<0) && $this->_in_post($matches[0])) {
            	return $this->detect();
            }
        }
        
        return FALSE;
    }

    /**
     * compare with post values
     *
     * @param unknown_type $value
     * @return unknown
     */
    private function _in_post($value) {
        foreach($_POST as $v) {
             if (is_int(strpos(strtolower($v),$value))) {
             	return TRUE;
             }
        }
        return FALSE;
    }

    /**
     * do admin write log / redirect / remove session
     *
     * @return boolean
     */
    private function detect() {
        // log the attempt to sql inject?
        if ($this->bLog) {
            $fp = @fopen($this->bLog,'a+');
            if ($fp) {
                fputs($fp,"\r\n".date("d-m-Y H:i:s").' ['.$this->rq.'] from '.$this->sIp = getenv("REMOTE_ADDR"));
                fclose($fp);
            }
        }

        // destroy session?
        if ($this->bdestroy_session) {
        	session_destroy();
        }
        
        // redirect?
        if ($this->urlRedirect!='') {
             if (!headers_sent()) {
             	header("location: $this->urlRedirect");
             }
        }
        
        return TRUE;
    }
}

/**
 * example
 * $s = new sql_inject
 * $sql = "SELECT * FROM test WHERE a=b";
 * $s->test_sql($sql);
 */
?>
