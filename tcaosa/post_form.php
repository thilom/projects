<?php
/**
 * Send form - Parses form fields and sends them to a specified email address.
 *
 * If there are form fields that should NOT be included in the email, the
 * field name should contain the string '#DNS'. THese fields will not be added to the outgoing email.
 *
 * Headings - This script supports headings. To add a heading, create a form field called
 * 'heading[0]', 'heading[1]', etc. Headings will be displayed in the numerical order
 * assigned to them inside the square brackets. The value of each field is made up of the heading ID and
 * heading title. The heading ID then needs to be added to the beginning of each field which you want displayed
 * under this heading. eg. "<input name='DI-Heading Title' ...". The heading ID must be followed by a dash(no spaces).
 *
 * The send date and server name is automatically appended to the end of the email.
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/securimage/securimage.php';


//Vars
$securimage = new Securimage();
$recipients = array(1=>'admin@tcaosa.co.za',2=>'thilo@palladianbytes.co.za');
$hidden_fields = array('recipients','return','heading','subject','captcha_code');
$used_fields = array();

//Security
if ($securimage->check($_POST['captcha_code']) == false) {
	die("<script>document.location = '/index.php?page=contact_error'</script>");
}

//Assemble HTML
$email_content = "";
	foreach ($_POST['heading'] as $heading) {
		list($heading_id, $heading_title) = explode('-', $heading);
		$email_content .= "<p><table width=100% style='border-collapse: collapse; border: 1px solid black'>";
		$email_content .= "<tr><th colspan=2 style='background-color: gray'>$heading_title</th></tr>";
		foreach ($_POST as $k=>$v) {
			if (in_array($k, $hidden_fields)) continue;
			if (strpos($k, '#DNS') !== false) continue;
			if (substr($k, 0, 3) == "$heading_id-") {
				$email_content .= "<tr><td width=200px style='background-color: #E4E5E6; border: 1px solid black'>". str_replace('_', ' ', substr($k,3)) ."</td><td  style='background-color: #FFF; border: 1px solid black'>$v</td></tr>";
				$used_fields[] = $k;
			}
		}
		$email_content .= "</table>";
	}

$line_counter = 0;
foreach ($_POST as $k=>$v) {
	if (in_array($k, $used_fields)) continue;
	if (in_array($k, $hidden_fields)) continue;
	if (strpos($k, '#DNS') !== false) continue;
	if ($line_counter == 0) $email_content .= "<p><table width=100% style='border-collapse: collapse; border: 1px solid black'>";
	$email_content .= "<tr><td width=200px style='background-color: #E4E5E6; border: 1px solid black'>". str_replace('_', ' ', $k) ."</td><td  style='background-color: #FFF; border: 1px solid black'>$v</td></tr>";
	$line_counter++;
}
if ($line_counter > 0) $email_content .= "</table>";

$email_content .= "<hr><table width=100%><tr><td style='font-size: 8pt'>". date('r') ."</td><td align=right  style='font-size: 8pt'>Origin: {$_SERVER['SERVER_NAME']}</td></tr></table>";


//Send Email
$mail = new PHPMailer();
try {
	$mail->AddAddress($recipients[$_POST['recipients']]);
	$mail->Subject = $_POST['subject'];
	$mail->MsgHTML($email_content);
	$mail->Send();
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
	echo $e->getMessage(); //Boring error messages from anything else!
}

echo "<script>document.location = '". $_POST['return'] ."'</script>";
die();
?>
