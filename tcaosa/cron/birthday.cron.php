<?php

/*
 * Daily birthday message
 * 
 * 
 */

//includes
$cron = 1;
//$document_root = $_SERVER['DOCUMENT_ROOT'];
$document_root = '/home/tcaosa/public_html';
include_once $document_root . '/settings/init.php';
include_once $document_root . '/shared/database_functions.php';
require_once $document_root . '/modules/logs/log_functions.php';
require_once $document_root . '/shared/PHPMailer/class.phpmailer.php';

//Vars
$mail = new PHPMailer();
$email_date = date('d-m-Y');
$birthday = date('d-m');
$sent_list = array();
$english_message = file_get_contents("$document_root/html/birthday_email_template_english.html");
$afrikaans_message = file_get_contents("$document_root/html/birthday_email_template_afrikaans.html");
$birthday_count = 0;

//Get send list
$statement = "SELECT email_address 
				FROM {$GLOBALS['db_prefix']}_email_birthday_log
				WHERE DATE_FORMAT(email_date, '%d-%m-%Y') = :email_date";
$sql_sent = $GLOBALS['dbCon']->prepare($statement);
$sql_sent->bindParam(':email_date', $email_date);
$sql_sent->execute();
$sql_sent_data = $sql_sent->fetchAll();
$sql_sent->closeCursor();
foreach ($sql_sent_data as $sent_data) {
	$sent_list[] = $sent_data['email_address'];
}

//Get english data
$statement = "SELECT message_8
				FROM message_settings";
$sql_message = $GLOBALS['dbCon']->prepare($statement);
$sql_message->execute();
$sql_message_data = $sql_message->fetch();
$sql_message->closeCursor();
$english_message = str_replace('<!-- message8 -->', $sql_message_data['message_8'], $english_message);

//Get afrikaans data
$statement = "SELECT message_8
				FROM message_settings_afr";
$sql_message = $GLOBALS['dbCon']->prepare($statement);
$sql_message->execute();
$sql_message_data = $sql_message->fetch();
$sql_message->closeCursor();
$afrikaans_message = str_replace('<!-- message8 -->', $sql_message_data['message_8'], $afrikaans_message);

//Get birthday list
$statement = "SELECT firstname, lastname, email, language
				FROM {$GLOBALS['db_prefix']}_members
				WHERE DATE_FORMAT(date_of_birth, '%d-%m')=:birthday 
					AND get_birthday_sms='1'
					AND active='1'";
$sql_users = $GLOBALS['dbCon']->prepare($statement);
$sql_users->bindParam(':birthday', $birthday);
$sql_users->execute();
$sql_users_data = $sql_users->fetchAll();
$sql_users->closeCursor();

foreach ($sql_users_data as $user_data) {
	if ($user_data['language'] == 'Afrikaans') {
		$email_subject = 'Gelukkige Verjaardag';
		$email_message = $afrikaans_message;
	} else {
		$email_subject = 'Happy Birthday';
		$email_message = $english_message;
	}
	echo "{$user_data['email']}\n";
	//Replace tags
	$email_message = str_replace('<!-- firstname -->', $user_data['firstname'], $email_message);
	$email_message = str_replace('<!-- lastname -->', $user_data['lastname'], $email_message);
	
	try {
		$mail->ClearAddresses();
		$mail->AddAddress($user_data['email'], "{$user_data['firstname']} {$user_data['last_name']}");
		$mail->SetFrom('admin@tcaosa.co.za', 'The Christian Association of South Africa');
		$mail->AddReplyTo('admin@tcaosa.co.za', 'The Christian Association of South Africa');
		$mail->Subject = $email_subject;
		$mail->MsgHTML($email_message);
		$mail->Send();
		
		$email_result = 1;
		$birthday_count++;
		
	} catch (phpmailerException $e) {
		$error_message = $e->errorMessage();
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_errors
							(error_date, error_email, error_subject, error_message)
						VALUES
							(NOW(), :error_email, :error_subject, :error_message)";
		$sql_error = $GLOBALS['dbCon']->prepare($statement);
		$sql_error->bindParam(':error_email', $user_data['email']);
		$sql_error->bindParam(':error_message', $error_message);
		$sql_error->bindParam(':error_subject', $email_subject);
		$sql_error->execute();
		$sql_error_data = $sql_error_data->fetch();
		$sql_error->closeCursor();
		
		$email_result = 0;
		$error_count++;
	} catch (Exception $e) {
		$error_message = $e->getMessage();
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_errors
							(error_date, error_email, error_subject, error_message)
						VALUES
							(NOW(), :error_email, :error_subject, :error_message)";
		$sql_error = $GLOBALS['dbCon']->prepare($statement);
		$sql_error->bindParam(':error_email', $user_data['email']);
		$sql_error->bindParam(':error_message', $error_message);
		$sql_error->bindParam(':error_subject', $email_subject);
		$sql_error->execute();
		$sql_error_data = $sql_error_data->fetch();
		$sql_error->closeCursor();
		
		$email_result = 0;
		$error_count++;
	}
	
	//Add to sent list
	$sent_list[] = $user_data['email'];
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_birthday_log
						(email_date, email_address, email_result)
					VALUES
						(NOW(), :email_address, :email_result)";
	$sql_sent_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_sent_insert->bindParam(':email_address', $user_data['email']);
	$sql_sent_insert->bindParam(':email_result', $email_result);
	$sql_sent_insert->execute();
	
}

/* Insert Stats  START */

//Does an entry already exist for today
$statement = "SELECT total_birthday
				FROM {$GLOBALS['db_prefix']}_email_stats
				WHERE DATE_FORMAT(stat_date, '%d-%m-%Y')=:stat_date";
$sql_stat_check = $GLOBALS['dbCon']->prepare($statement);
$sql_stat_check->bindParam(':stat_date', $email_date);
$sql_stat_check->execute();
$sql_stat_check_data = $sql_stat_check->fetch();
$sql_stat_check->closeCursor();

if ($sql_stat_check_data === false) {
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_stats
						(stat_date, total_birthday)
					VALUES
						(NOW(), :total_birthday)";
	$sql_stats = $GLOBALS['dbCon']->prepare($statement);
	$sql_stats->bindParam(':total_birthday', $birthday_count);
	$sql_stats->execute();
} else {
	$birthday_count += $sql_stat_check_data['total_birthday'];
	
	$statement = "UPDATE {$GLOBALS['db_prefix']}_email_stats
					SET total_birthday = :total_birthday
					WHERE DATE_FORMAT(stat_date, '%d-%m-%Y')=:email_date
					LIMIT 1";
	$sql_stats_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_stats_update->bindParam(':total_birthday', $birthday_count);
	$sql_stats_update->bindParam(':email_date', $email_date);
	$sql_stats_update->execute();
}

/* Insert stats - END */

?>
