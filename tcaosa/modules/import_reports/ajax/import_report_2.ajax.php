<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$import_errors['aaData'] = array();
$data = array();
$user_id = $_SESSION['dbweb_user_id'];
$import_file = "{$_SERVER['DOCUMENT_ROOT']}/i/temp/{$user_id}_report_upload.csv";
$count = 0;
$firstname_col = $_GET['mapInitials'];
$surname_col = $_GET['mapName'];
$reference_col = $_GET['mapRef'];
$amount_col = $_GET['mapAmount'];


//Does the salary reference exist
$statement = "SELECT lastname, firstname, user_id, company
				FROM {$GLOBALS['db_prefix']}_members
				WHERE salary_ref = :salary_reference";
$sql_ref = $GLOBALS['dbCon']->prepare($statement);

//Find members by name - In case reference is not found
$statement = "SELECT lastname, firstname, user_id, company
				FROM {$GLOBALS['db_prefix']}_members
				WHERE lastname = :lastname";
$sql_name = $GLOBALS['dbCon']->prepare($statement);

//Import into Database
if (($handle = fopen($import_file, "r")) !== FALSE) {
    while (($csv_data = fgetcsv($handle, 1000)) !== FALSE) {
//		if ($count == 20) break ;
		$num = count($csv_data);
		$data = array();
//        for ($c=0; $c < $num; $c++) {
            if ($count == 0) {
				
			} else {
				//Check for errors				
				$sql_ref->bindParam(':salary_reference', $csv_data[$reference_col]);
				$sql_ref->execute();
				$sql_ref_data = $sql_ref->fetchAll();
					
				if (count($sql_ref_data) == 0) { //Reference not found
					$data['firstname'] = $csv_data[$firstname_col];
					$data['lastname'] = $csv_data[$surname_col];
					$data['reference'] = $csv_data[$reference_col];
					$data['amount'] = $csv_data[$amount_col];
					$data['button'] = "Reference Not Found";
					$data['button2'] = "<select name='fixes[]'  style='width: 100%'><option value='{$csv_data[$reference_col]}-Skip'>Skip</option>";
					
					$sql_name->bindParam(':lastname', $csv_data[$surname_col]);
					$sql_name->execute();
					$sql_name_data = $sql_name->fetchAll();
					foreach ($sql_name_data as $name_data) {
						$data['button2'] .= "<option value='{$csv_data[$reference_col]}-{$name_data['user_id']}'>{$name_data['firstname']} {$name_data['lastname']} ({$name_data['company']})</option>";
					}
					
					$data['button2'] .= "</select>";
					$import_errors['aaData'][] = $data;
				} else if (count($sql_ref_data) > 1) { //Multiple references found
					$data['firstname'] = $csv_data[$firstname_col];
					$data['lastname'] = $csv_data[$surname_col];
					$data['reference'] = $csv_data[$reference_col];
					$data['amount'] = $csv_data[$amount_col];
					$data['button'] = "Multiple References Found";
					$data['button2'] = "<select name='fixes[]' style='width: 100%'><option value='{$csv_data[$reference_col]}-Skip'>Skip</option>";
					foreach ($sql_ref_data as $ref_data) {
						$data['button2'] .= "<option value='{$csv_data[$reference_col]}-{$ref_data['user_id']}'>{$ref_data['firstname']} {$ref_data['lastname']} ({$ref_data['company']})</option>";
					}
					$data['button2'] .= "</select>";
					$import_errors['aaData'][] = $data;
				} else { //Perfect
//					$data['firstname'] = $csv_data[$firstname_col];
//					$data['lastname'] = $csv_data[$surname_col];
//					$data['reference'] = $csv_data[$reference_col];
//					$data['amount'] = $csv_data[$amount_col];
//					$data['button'] = 'OK';
//					$import_errors['aaData'][] = $data;
				}
			}
//		}
		$count++;
	}
	
}
fclose($handle);

echo json_encode($import_errors);
?>
