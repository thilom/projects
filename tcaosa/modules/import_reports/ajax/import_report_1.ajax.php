<?php

/*
 * First import step. Return a list of table headings with examples
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$data = array();
$user_id = $_SESSION['dbweb_user_id'];
$import_file = "{$_SERVER['DOCUMENT_ROOT']}/i/temp/{$user_id}_report_upload.csv";
$count = 0;

//Import into Database
if (($handle = fopen($import_file, "r")) !== FALSE) {
    while (($csv_data = fgetcsv($handle, 1000)) !== FALSE) {
		$num = count($csv_data);
        for ($c=0; $c < $num; $c++) {
            if ($count == 0) {
				$data["$c"] = "Column $c :: {$csv_data[$c]}";
			} else {
				break 2;
			}
        }
		$count++;
    }
    fclose($handle);
}

echo json_encode($data);
?>
