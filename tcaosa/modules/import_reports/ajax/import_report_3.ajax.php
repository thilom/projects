<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


//var_dump($_POST);
//echo "---------------------------<br>";
//var_dump($_GET);
//die();


//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$user_id = $_SESSION['dbweb_user_id'];
$corrections = array();
$import_file = "{$_SERVER['DOCUMENT_ROOT']}/i/temp/{$user_id}_report_upload.csv";
$firstname_col = $_GET['mapInitials'];
$surname_col = $_GET['mapName'];
$reference_col = $_GET['mapRef'];
$amount_col = $_GET['mapAmount'];
$import_result['aaData'] = array();

//Asemble corrections
foreach ($_POST['fixes'] AS $fix_data) {
	list($fix_ref, $fix_id) = explode('-', $fix_data);
	$corrections[$fix_ref] = $fix_id;
}

//Prepare Statement - Update user
$statement = "UPDATE {$GLOBALS['db_prefix']}_members
				SET amount = :amount,
					active = '1'
				WHERE salary_ref = :salary_ref
						OR user_id = :user_id
				LIMIT 1";
$sql_update = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Insert import log
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_import_log
					(import_date, user_id,import_surname,import_firstname,import_reference,import_amount,
					 import_success,import_notes)
				VALUES (NOW(), :user_id,:import_surname,:import_firstname,:import_reference,:import_amount,
					 :import_success,:import_notes);";
$sql_log = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Get user Data
$statement = "SELECT user_id
				FROM {$GLOBALS['db_prefix']}_members
				WHERE salary_ref = :salary_ref
				LIMIT 1";
$sql_user = $GLOBALS['dbCon']->prepare($statement);

//Import into Database
$count = 0;
if (($handle = fopen($import_file, "r")) !== FALSE) {
    while (($csv_data = fgetcsv($handle, 1000)) !== FALSE) {
//		if ($count == 20) break ;
		$num = count($csv_data);
		$data = array();
            if ($count == 0) {
				
			} else {
				
				$salary_ref = $csv_data[$reference_col];
				
				if (isset($corrections[$salary_ref])) { //Update the 'fixed' reference
					if ($corrections[$salary_ref] == 'Skip') {
						//Save Log						
						$success = '0';
						$notes = 'Skipped by user';
						$user = '';
						$sql_log->bindParam(':user_id', $user);
						$sql_log->bindParam(':import_surname', $csv_data['surname_col']);
						$sql_log->bindParam(':import_firstname', $csv_data['firstname_col']);
						$sql_log->bindParam(':import_reference', $csv_data['reference_col']);
						$sql_log->bindParam(':import_amount', $csv_data['amount_col']);
						$sql_log->bindParam(':import_success', $success);
						$sql_log->bindParam(':import_notes', $notes);
						$sql_log->execute();
						
						//Add to JSON array
						$data['firstname'] = $csv_data[$firstname_col];
						$data['lastname'] = $csv_data[$surname_col];
						$data['reference'] = $csv_data[$reference_col];
						$data['amount'] = $csv_data[$amount_col];
						$data['button'] = 'Skipped';
						$data['button2'] = '';
						$import_result['aaData'][] = $data;
						
						continue;
					} else {
						$no_ref = 'No Salary Reference';
						$sql_update->bindParam(':salary_ref', $no_ref);
						$sql_update->bindParam(':amount', $csv_data[$amount_col]);
						$sql_update->bindParam(':user_id', $corrections[$salary_ref]);
						$sql_update->execute();
						
						//Save Log						
						$success = '1';
						$notes = 'Member manually selected by user';
						$sql_log->bindParam(':user_id', $corrections[$salary_ref]);
						$sql_log->bindParam(':import_surname', $csv_data['surname_col']);
						$sql_log->bindParam(':import_firstname', $csv_data['firstname_col']);
						$sql_log->bindParam(':import_reference', $csv_data['reference_col']);
						$sql_log->bindParam(':import_amount', $csv_data['amount_col']);
						$sql_log->bindParam(':import_success', $success);
						$sql_log->bindParam(':import_notes', $notes);
						$sql_log->execute();
						
						//Add to JSON array
						$data['firstname'] = $csv_data[$firstname_col];
						$data['lastname'] = $csv_data[$surname_col];
						$data['reference'] = $csv_data[$reference_col];
						$data['amount'] = $csv_data[$amount_col];
						$data['button'] = 'OK';
						$data['button2'] = '';
						$import_result['aaData'][] = $data;
						
					}
				} else { //Update with csv salary reference
					
					//Get user ID
					$sql_user->bindParam(':salary_ref', $salary_ref);
					$sql_user->execute();
					$sql_user_data = $sql_user->fetch();
					$sql_user->closeCursor();
					
					$no_user = 'No User';
					$sql_update->bindParam(':salary_ref', $salary_ref);
					$sql_update->bindParam(':amount', $csv_data[$amount_col]);
					$sql_update->bindParam(':user_id', $no_user);
					$sql_update->execute();
					
					//Save Log	
					$success = '1';
					$notes = '';
					$sql_log->bindParam(':user_id', $sql_user_data['user_id']);
					$sql_log->bindParam(':import_surname', $csv_data['surname_col']);
					$sql_log->bindParam(':import_firstname', $csv_data['firstname_col']);
					$sql_log->bindParam(':import_reference', $csv_data['reference_col']);
					$sql_log->bindParam(':import_amount', $csv_data['amount_col']);
					$sql_log->bindParam(':import_success', $success);
					$sql_log->bindParam(':import_notes', $notes);
					$sql_log->execute();
					
					//Add to JSON array
					$data['firstname'] = $csv_data[$firstname_col];
					$data['lastname'] = $csv_data[$surname_col];
					$data['reference'] = $csv_data[$reference_col];
					$data['amount'] = $csv_data[$amount_col];
					$data['button'] = 'OK';
					$data['button2'] = '';
					$import_result['aaData'][] = $data;
					
				}
				
			}
		$count++;
	}
	
}
fclose($handle);

//Add a alog entry
$message = 'Monthly salary deductions imported';
write_log($message, 'mod_import', '', '', $user_id);

$import_result['iTotalRecords'] = $count;
$import_result['iTotalDisplayRecords'] = $count;
echo json_encode($import_result);

?>
