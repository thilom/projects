
var oTable;

/**
 * Run first import step
 */
function runImport_1() {
	$.getJSON(
		'/modules/import_reports/ajax/import_report_1.ajax.php',
		'',
		function(data) {
				var optionState = '';
				$.each(data, function(i){
					$("#surname1").append('<option value='+ i +' ' + optionState + '>'+ data[i] +'</option>');
					optionState = data[i].indexOf('SURNAME') == -1?'':$('#surname1').val(i);
					
					$("#firstname1").append('<option value='+ i +'>'+ data[i] +'</option>');
					optionState = data[i].indexOf('INITIALS') == -1?'':$('#firstname1').val(i);
					
					$("#reference1").append('<option value='+ i +'>'+ data[i] +'</option>');
					optionState = data[i].indexOf('REFNR') == -1?'':$('#reference1').val(i);
					
					$("#amount1").append('<option value='+ i +'>'+ data[i] +'</option>');
					optionState = data[i].indexOf('AMOUNT') == -1?'':$('#amount1').val(i);
				  }
				);
				$("#importStep1").show();
			}
		);
	
	
	
	
	
	
}

/**
 * Run second import step
 */
function runImport_2() {
	
	//Get map
	var csvMap = 'mapName=' + $('#surname1').val();
	csvMap += '&mapInitials=' + $('#firstname1').val();
	csvMap += '&mapRef=' + $('#reference1').val();
	csvMap += '&mapAmount=' + $('#amount1').val();
	
	oTable = $('#importList').dataTable( {
			"bProcessing": true,
			"bPaginate": false,
			"sAjaxSource": '/modules/import_reports/ajax/import_report_2.ajax.php?' + csvMap,
			"aoColumns": [
				{ "mData": "firstname" },
				{ "mData": "lastname" },
				{ "mData": "reference" },
				{ "mData": "amount" },
				{ "mData": "button", "bSortable": false },
				{ "mData": "button2", "bSortable": false }
			],
			"bJQueryUI": true
		} );
		
	$("#importStep1").hide();
	$("#importStep2").show();
}

/**
 * Run third and final step of import process
 */
function runImport_3() {

	//Overlay
	$(function() {
		$( "#dialog" ).dialog({ modal: true });
	  });

	//Get map
	var csvMap = 'mapName=' + $('#surname1').val();
	csvMap += '&mapInitials=' + $('#firstname1').val();
	csvMap += '&mapRef=' + $('#reference1').val();
	csvMap += '&mapAmount=' + $('#amount1').val();


	oTable = $.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": '/modules/import_reports/ajax/import_report_3.ajax.php?' + csvMap,
				"data": $("#importStep2_form").serialize(), 
				"success": function(jqXHR) {
					$( "#dialog" ).dialog( "close" );
					var oTable = $('#importList').dataTable( {
						"bProcessing": true,
						"aoColumns": [
							{ "mData": "firstname" },
							{ "mData": "lastname" },
							{ "mData": "reference" },
							{ "mData": "amount" },
							{ "mData": "button", "bSortable": true },
							{ "mData": "button2", "bSortable": false }
						],
						"aaData": jqXHR.aaData,
						"bJQueryUI": true,
						"bAutoWidth" : true,
						"bDestroy": true
					} );
					$("#importList").css('width', '100%');
					$('#finishButton').hide();
				},
				"error": function() {alert('Import Failed')}
			} );
				
	
}