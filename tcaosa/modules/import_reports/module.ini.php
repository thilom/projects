<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Import";
$module_id = "import_reports"; //needs to be the same as the module directory
$module_icon = 'off_atribute_64.png';
$module_link = 'import.php';
$window_width = '800';
$window_height = '';
$window_position = '';

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Import Reports";
$security_id = "mod_import";


?>