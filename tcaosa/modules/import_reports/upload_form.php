<?php

/**
 * Import Form
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/upload_form.html');
$user_id = $_SESSION['dbweb_user_id'];


if (count($_POST) > 0) {
	
	//Move uploaded file
	move_uploaded_file($_FILES['reportFile']['tmp_name'], "{$_SERVER['DOCUMENT_ROOT']}/i/temp/{$user_id}_report_upload.csv");
	
	echo "";
	echo "<script>parent.runImport_1()</script>";
	
} else {
	echo $template;
}
