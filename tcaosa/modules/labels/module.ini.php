<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Labels";
$module_id = "labels"; //needs to be the same as the module directory
$module_icon = 'off_mailing_list_64.png';
$module_link = 'labels.php';
$window_width = '800';
$window_height = '';
$window_position = '';

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Labels";
$security_id = "mod_labels";


?>