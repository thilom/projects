
/**
 * Create Labels (AJAX)
 */
function createLabels() {
	document.getElementById('labelDownload').innerHTML = 'Generating Labels...please wait';
	var orderBy = document.getElementById('orderBy').value;
	var deogram = document.getElementById('deogram').checked;
	var active = document.getElementById('active').value;
	var salaryRef = document.getElementById('salaryRef').value;
	
	var options = '?order='+orderBy;
	options += '&deogram='+ (deogram?'Y':'N');
	options += '&active='+active;
	options += '&salaryRef='+salaryRef;
	
	var saveRequest = $.get(
        '/modules/labels/ajax/create_labels.ajax.php' + options
    );
	
	saveRequest.done(function(xhr) {creationSuccess(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){creationError(xhr,ajaxOptions, thrownError)})
}

/**
 * Creation success
 */
function creationSuccess(xhr) {
	
//	console.log(xhr);
	
	//message
	data = JSON.parse(xhr);
	var message = data.count + " labels on " + data.pages + " Pages<br><a href='/modules/labels/generated_files/labels.pdf'>Download & Print</a>";
	
	document.getElementById('labelDownload').innerHTML = message;
}

/**
 * Creation Failod
 */
function creationError(xhr,ajaxOptions, thrownError) {
	switch (xhr.status) {
		case 404:
			document.getElementById('labelDownload').innerHTML = 'An error occured while attempting to create labels. <br>The server returned an error: 404 '+thrownError+'.';
//			document.getElementById('messageBlock').className = 'messageBlockError';
			break;
		case 500:
			document.getElementById('labelDownload').innerHTML = 'An error occured while attempting to create labels. <br>The server returned an error: 500 '+thrownError+'.';
//			document.getElementById('messageBlock').className = 'messageBlockError';
			break;
		default:
			document.getElementById('labelDownload').innerHTML = 'An error occured while attempting to create labels. <br>Please check your internet connection and try again ('+thrownError+').';
//			document.getElementById('messageBlock').className = 'messageBlockError';
			break;
	}
}