<?php

/*
 * Create Labels
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/tcpdf.php';

//Vars
$pdf_file = '/modules/labels/generated_files/labels.pdf';
$label_x_pos = 5;
$label_y_pos = 5;
$label_cols = 3;
$label_count = 0;
$page_count = 1;
$order_by = $_GET['order'];
$active = $_GET['active'];
$deogram = $_GET['deogram'];
$salary_reference = $_GET['salaryRef'];

//Get data
$statement = "SELECT a.salary_ref, a.firstname, a.lastname, a.title, b.address_1, b.address_2, b.suburb, b.city, b.province, b.postal_code 
				 FROM {$GLOBALS['db_prefix']}_members AS a
				 LEFT JOIN {$GLOBALS['db_prefix']}_members_address As b ON a.user_id=b.user_id ";
if ($active == 'active') {
	$statement .= " WHERE active=1 ";
} else if ($active == 'inactive') {
	$statement .= " WHERE a.active=0 OR a.active IS NULL ";
}
if ($deogram == 'Y') {
	$statement .= strpos($statement, 'WHERE')> 0?' AND ':' WHERE ';
	$statement .= " a.deogram = 1 ";
}
if (!empty($salary_reference)) {
	$references = explode(',', $salary_reference);
	$statement .= strpos($statement, 'WHERE')> 0?' AND (':' WHERE (';
	foreach ($references as $key=>$ref) {
		$statement .= $key==0?'':' OR ';
		$statement .= " a.salary_ref = $ref ";
	}
	
	$statement .= ")";
}
				
$statement .= " ORDER BY $order_by DESC";

//echo $statement;
$sql_labels = $GLOBALS['dbCon']->prepare($statement);
$sql_labels->execute();
$sql_labels_data = $sql_labels->fetchAll();
$sql_labels->closeCursor();

//Create PDF
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  
//PDF Settings
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('The Cristian Association of South Africa');
$pdf->SetTitle('TCAOSA: Labels');
$pdf->SetSubject('TCAOSA: Labels');
$pdf->setAutoPageBreak(false);

// add a page
$pdf->AddPage('P');

//Guide Lines
$page_height = $pdf->getPageHeight();
$page_width = $pdf->getPageWidth();

//Draw Labels
$col_no = 0;
$row_no = 1;
foreach ($sql_labels_data as $label_data) {
	//Vars
	$salary_ref = !empty($label_data['salary_ref'])?$label_data['salary_ref']:'---------';
	$postal_code = !empty($label_data['postal_code'])?str_pad(trim($label_data['postal_code']),4, 0, STR_PAD_LEFT):'---------';
	$name = "{$label_data['title']} {$label_data['firstname']} {$label_data['lastname']}";
	$address = '';
	
	//Assemble address
	if (empty($label_data['address_2']) && empty($label_data['city'])) {
		$address = str_replace(',', "\n", trim($label_data['address_1']));
	} else {
		$address = '';
		if (!empty($label_data['address_1'])) $address .= "{$label_data['address_1']}\n";
		if (!empty($label_data['address_2'])) $address .= "{$label_data['address_2']}\n";
		if (!empty($label_data['suburb'])) $address .= "{$label_data['suburb']}\n";
		if (!empty($label_data['city'])) $address .= "{$label_data['city']}\n";
		if (!empty($label_data['province']) && $label_data['province'] != 'NO') $address .= "{$province_list[$label_data['province']]}\n";
	}
	
	//Adresss cleanup
	if (empty($address)) {
		$address = '---------';
	}
	$address = str_replace(array('<br>', '<br />'), '', $address);
	if (substr_count($address, $postal_code) > 0) $postal_code = '';
	
	
//	$border_style = array('all' => array('width' => 0.1, 'cap' => 'square', 'join' => 'miter', 'dash' => 1, 'phase' => 0));
	$border_style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
	$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
	$pdf->Rect($label_x_pos, $label_y_pos, 59, 28, '', $border_style);
	$pdf->SetFont('helvetica', '', 8);
	$pdf->Text($label_x_pos+40, $label_y_pos, $salary_ref);
	$pdf->MultiCell(59, 21, "$name\n$address\n$postal_code", 0, 'C', false, 1, $label_x_pos, $label_y_pos+3, true);
	$label_count++;
	
	$label_x_pos += ($page_width/3);
	$col_no++;
	if ($col_no == $label_cols) {
		$col_no = 0;
		$label_y_pos += $page_height/8;
		if ($row_no == 4) {
			$label_y_pos += 3;
		}
		$label_x_pos = 5;
		$row_no++;
	}
	if ($row_no == 9) {
		$pdf->AddPage('P');
		$page_count++;
		$label_y_pos = 5;
        $label_x_pos = 5;
		$row_no = 1;
        $col_no = 0;
	}
}


//Save File
$pdf->Output($_SERVER['DOCUMENT_ROOT'] . $pdf_file, 'F');

//return data
$data_labels['count'] = $label_count;
$data_labels['pages'] = $page_count;

echo json_encode($data_labels);
?>
