<?php
/**
 * General scrum settings
 */

//Includes
include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php');

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/settings/html/general_settings.html');
$display_currency = '';
$currency_code = '';
$date_format = '';
$format_select = '';
$login_page_list = '';
$login_page = '';
$date_formats = array('%d %M %Y'=>'30 December 1971',
						'%W, %e %M %Y'=>'Monday, 30 December 1971',
						'%d-%m-%Y'=>'30-12-1971 (day-month-year)',
						'%d/%m/%Y'=>'30/12/1971 (day/month/year)',
						'%m-%d-%Y'=>'30-12-1971 (month-day-year)',
						'%m/%d/%Y'=>'30/12/1971 (month/day/year)');

//Get current FTP & Currency Details
$statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
$sql_ftp = $GLOBALS['dbCon']->prepare($statement);
$sql_ftp->execute();
$ftp_data = $sql_ftp->fetchAll();
$sql_ftp->closeCursor();
foreach ($ftp_data as $data) {
	switch ($data['tag']) {
		case 'display_currency':
			$display_currency = $data['value'];
			break;
		case 'currency_code':
			$currency_code = $data['value'];
			break;
		case 'date_format':
			$date_format = $data['value'];
			break;
	}
}

//Get current login page
$statement = "SELECT value
			FROM {$GLOBALS['db_prefix']}_settings
			WHERE tag='login_page'
			LIMIT 1";
$sql_login = $GLOBALS['dbCon']->prepare($statement);
$sql_login->execute();
$sql_login_data = $sql_login->fetch();
$sql_login->closeCursor();
$login_page = $sql_login_data['value'];

if (count($_POST) > 0) {
	$message = '';

	//Prepare statement - FTP Details Update
	$statement = "UPDATE {$GLOBALS['db_prefix']}_settings SET value=:value WHERE tag=:tag";
	$sql_ftp_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - FTP Check
	$statement = "SELECT COUNT(*) AS c FROM {$GLOBALS['db_prefix']}_settings WHERE tag=:tag";
	$sql_ftp_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - FTP Details Insert
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings (tag, value) VALUES (:tag,:value)";
	$sql_ftp_insert = $GLOBALS['dbCon']->prepare($statement);

	//Save Currency Data - Display
	if ($_POST['display_currency'] != $display_currency) {
		$tag = 'display_currency';
		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['display_currency']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['display_currency']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$message .= "&#187; Display currency updated to '{$_POST['display_currency']}'<br>";
		$log_message = "Display currency updated to '{$_POST['display_currency']}'";
		if (!empty($display_currency)) $log_message .= "(Previously: '$display_currency')";
		write_log($log_message, 'settings');

	}

	//Save Currency Data - Code
	if ($_POST['currency_code'] != $currency_code) {
		$tag = 'currency_code';
		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['currency_code']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['currency_code']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$message .= "&#187; Currency code updated to '{$_POST['currency_code']}'<br>";
		$log_message = "Currency code updated to '{$_POST['currency_code']}'";
		if (!empty($display_currency)) $log_message .= "(Previously: '$currency_code')";
		write_log($log_message, 'settings');

	}

	//Save Default Date Format
	if ($_POST['date_format'] != $date_format) {
		$tag = 'date_format';
		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['date_format']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['date_format']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$message .= "&#187; Date format updated to '{$_POST['date_format']}'<br>";
		$log_message = "Date format updated to '{$_POST['date_format']}'";
		if (!empty($date_format)) $log_message .= "(Previously: '$date_format')";
		write_log($log_message, 'settings');

	}

	//Save login page
	if ($login_page != $_POST['loginPage']) {
		$tag = 'login_page';
		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['loginPage']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['loginPage']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$message .= "&#187; Login page updeted to '{$_POST['loginPage']}'<br>";
		$log_message = "Login page updeted to '{$_POST['loginPage']}'";
		if (!empty($login_page)) $log_message .= "(Previously: '$login_page')";
		write_log($log_message, 'settings');
	}

	if (empty($message)) $message = "&#187; No Changes, Nothing to update";
	echo "<br><br><div class='dMsg'>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/settings/settings.php\"' ></div>";

	die();
}

//Currency Display
$currency_form_display = 'none';
if (is_module('cart') || is_module('products')) $currency_form_display = 'table';

//Assemble date format
foreach ($date_formats as $format=>$format_description) {
	$format_select .= "<option value='$format'";
	$format_select .= $format==$date_format?' selected ':'';
	$format_select .= ">$format_description</option>";
}

//Get All pages
$statement = "SELECT page_name, page_title
				FROM {$GLOBALS['db_prefix']}_pages
				ORDER BY page_name";
$sql_pages = $GLOBALS['dbCon']->prepare($statement);
$sql_pages->execute();
$sql_pages_data = $sql_pages->fetchAll();
$sql_pages->closeCursor();

//Assemble page list
foreach ($sql_pages_data as $page_data) {
	$login_page_list .= "<option value='{$page_data['page_name']}' ";
	$login_page_list .= $page_data['page_name']==$login_page?" selected >":">";
	$login_page_list .= "{$page_data['page_name']} - {$page_data['page_title']}</option>";

}

//Replace Tags
$template = str_replace('<!-- currency_form_display -->', $currency_form_display, $template);
$template = str_replace('<!-- display_currency -->', $display_currency, $template);
$template = str_replace('<!-- currency_code -->', $currency_code, $template);
$template = str_replace('<!-- date_format -->', $format_select, $template);
$template = str_replace('<!-- login_page_list -->', $login_page_list, $template);


echo $template;
?>

