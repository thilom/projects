<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$menu_id = (int) $_GET['itemID'];

//Remove
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_menu
				WHERE menu_id=:menu_id
				LIMIT 1";
$sql_delete = $GLOBALS['dbCon']->prepare($statement);
$sql_delete->bindParam(':menu_id', $menu_id);
$sql_delete->execute();
?>
