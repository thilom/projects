<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$template = file_get_contents('html/menu_edit.html');
$menu_id = (int) $_GET['itemID'];
$parent = isset($_GET['parent'])?$_GET['parent']:'';
$menu_list = "<option value='0'>Top Level Item</option>";
$menu_name = '';
$menu_link = '';
$menu_display_name = 'New Menu Item';
$attach_products = '';

//Get Item Data
if ($menu_id > 0) {
	$statement = "SELECT menu_id, menu_name, menu_link, parent_id
					FROM {$GLOBALS['db_prefix']}_menu
					WHERE menu_id=:menu_id
					LIMIT 1";
	$sql_menu = $GLOBALS['dbCon']->prepare($statement);
	$sql_menu->bindParam(':menu_id', $menu_id);
	$sql_menu->execute();
	$sql_menu_data = $sql_menu->fetch();
	$sql_menu->closeCursor();

	$parent = $sql_menu_data['parent_id'];
	$menu_name = $sql_menu_data['menu_name'];
	$menu_display_name = $sql_menu_data['menu_name'];
	$menu_link  = $sql_menu_data['menu_link'];

}

get_list($menu_list, 1, $parent);

if (is_module('products') && $menu_id == 0) {
	$attach_products = "<tr><td>Attach Product Categories</td><td><input type='checkbox' style='width: 15px;' value='Y' name='attachProduct' id='attachProduct'></td></tr>";
}

//Replace tags
$template = str_replace('<!-- menu_display_name -->', $menu_display_name, $template);
$template = str_replace('<!-- menu_name -->', $menu_name, $template);
$template = str_replace('<!-- menu_link -->', $menu_link, $template);
$template = str_replace('<!-- parent_list -->', $menu_list, $template);
$template = str_replace('<!-- menu_id -->', $menu_id, $template);
$template = str_replace('<!-- attach_product -->', $attach_products, $template);

echo $template;

function get_list(&$menu_list, $menu_group, $current_parent, $level=0, $parent_id=0) {
	global $counter;

	$statement = "SELECT menu_id, menu_name, parent_id
					FROM {$GLOBALS['db_prefix']}_menu
					WHERE menu_group=:menu_group AND parent_id=:parent_id
					ORDER BY menu_position";
	$sql_link = $GLOBALS['dbCon']->prepare($statement);
	$sql_link->bindParam(':menu_group', $menu_group);
	$sql_link->bindParam(':parent_id', $parent_id);
	$sql_link->execute();
	$sql_link_data = $sql_link->fetchAll();
	$sql_link->closeCursor();

	$item_count = 0;
	foreach ($sql_link_data as $link_data) {
		//Count sub items
		$statement = "SELECT COUNT(*) AS sub_count
						FROM {$GLOBALS['db_prefix']}_menu
						WHERE parent_id=:parent_id";
		$sql_sub = $GLOBALS['dbCon']->prepare($statement);
		$sql_sub->bindParam(':parent_id', $link_data['menu_id']);
		$sql_sub->execute();
		$sql_sub_data = $sql_sub->fetch();
		$sql_sub->closeCursor();

		$menu_list .= "<option value='{$link_data['menu_id']}'";
		$menu_list .= $current_parent == $link_data['menu_id']?' selected ':'';
		$menu_list .= ">";

		$item_count++;

		for ($x=0; $x<$level; $x++) {
			$menu_list .= "&nbsp;&nbsp;";
		}
		for ($x=0; $x<$level; $x++) {
			$menu_list .= "-";
		}


		$menu_list .= "{$link_data['menu_name']}</option>";
		$counter++;


		get_list($menu_list, $menu_group, $current_parent, $level+1, $link_data['menu_id']);

	}
}
?>
