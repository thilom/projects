<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Menu Editor";
$module_id = "menu_editor"; //needs to be the same as the module directory
$module_icon = '';
$module_link = '';
$window_width = '';
$window_height = '';
$window_position = '';

//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');


//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Menu Editor";
$security_id = "mod_menu";

//Content Types
$content_type[0]['name'] = 'Drop-down Menu';
$content_type[0]['id'] = 'dropdown_menu';
$content_type[0]['content_file'] = 'edit/dropdown_menu.php';
$content_type[0]['display_file'] = 'view/dropdown_menu.php';
$content_type[0]['public_display_file'] = 'view/dropdown_menu.php';
$content_type[0]['preferences_file'] = 'edit/preferences_dropdown_menu.php';
$content_type[0]['css_file'] = 'edit/css_dropdown_menu.php';
$content_type[0]['explanation'] = 'Display a drop-down menu';

//$content_type[1]['name'] = 'News: Article';
//$content_type[1]['id'] = 'news_article';
//$content_type[1]['content_file'] = 'edit/news_article.php';
//$content_type[1]['display_file'] = 'view/news_article.php';
//$content_type[1]['public_display_file'] = 'view/public_news_article.php';
//$content_type[1]['preferences_file'] = 'edit/preferences_news_article.php';
//$content_type[1]['css_file'] = 'edit/css_news_article.php';
//$content_type[1]['explanation'] = 'Dislay the news article';

?>