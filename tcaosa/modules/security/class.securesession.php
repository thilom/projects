<?php
/**
 * class to generate secure sessions for website
 * session hijacking or session fixation
 *
 */
class secureSession {
    private $check_browser;	    		// Include browser name in fingerprint?
    private $check_ip_blocks;		    // How many numbers from IP use in fingerprint?
    private $secure_word;				// Control word - any word you want.
    private $regenerate_id;				// Regenerate session ID to prevent fixation attacks?

    /**
     * constructor
     *
     * @param boolean $cbrowser
     * @param integer $ipblocks
     * @param boolean $regid
     * @return boolean
     */
    public function secureSession($cbrowser = true, $ipblocks = 2, $regid = true) {
    	$this->check_browser = $cbrowser;
    	$this->check_ip_blocks = $ipblocks;
    	$this->regenerate_id = $regid;
    	$this->set_secureword();
    	return true;
    }

    /**
     * set the secure word for the fingerprint
     *
     * @param string $word
     * @return boolean
     */
    public function set_secureword($word = 'secure') {
    	$this->secure_word = $word;
    	return true;
    }

	/**
     * Call this when init session
     *
     * @return boolean
     */
    public function Open() {
        $_SESSION['ss_fprint'] = $this->_Fingerprint();
        $this->_RegenerateId();
        return true;
    }

	/**
	 * Call this to check session
	 *
	 * @return boolean
	 */
    public function Check() {
        $this->_RegenerateId();
        return (isset($_SESSION['ss_fprint']) && $_SESSION['ss_fprint'] == $this->_Fingerprint());
    }

    /**
     * Create a secure fimgerprint
     *
     * @return md5
     */
    private function _Fingerprint() {
        $fingerprint = $this->secure_word;

        if ($this->check_browser) {
            $fingerprint .= $_SERVER['HTTP_USER_AGENT'];
        }

        if ($this->check_ip_blocks) {
            $num_blocks = abs(intval($this->check_ip_blocks));

            if ($num_blocks > 4) {
                $num_blocks = 4;
            }
            $blocks = explode('.', $_SERVER['REMOTE_ADDR']);

            for ($i = 0; $i < $num_blocks; $i++) {
                $fingerprint .= $blocks[$i] . '.';
            }
        }
        return md5($fingerprint);
    }

    /**
     * Regenerates session ID if possible.
     *
     * @return true;
     */
    private function _RegenerateId()
    {
        if ($this->regenerate_id && function_exists('session_regenerate_id')) {
            if (version_compare('5.1.0', phpversion(), '>=')) {
                session_regenerate_id(true);
            } else {
                session_regenerate_id();
            }
        }
        return true;
    }
}

/**
 * example
 *
 * $s = new secureSession();
 * $s->set_secureword('testsecure');
 *
 * //do login
 * if (check login vars eg user + pass) {
 * 	 $s->Open();
 * 	 $_SESSION['test'] = true;
 * 	 die();
 * }
 *
 * //check login
 * if (!$s->Check() || empty($_SESSION['test'] || !isset($_SESSION['test']) || !$_SESSION['test']) {
 * 	die(); //not logged in
 * }
 */
?>