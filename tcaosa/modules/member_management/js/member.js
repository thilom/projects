
/**
 * Initialize
 */
function init_update() {
	$(document).ready(
		function() {
			getUserData();
			if ($( "#date_of_birth" )) {
				$(function() {
					$( "#date_of_birth" ).datepicker();
					$( "#date_of_birth" ).datepicker("option",'dateFormat','yy-mm-dd');
				});
			}
		}
	);
}
/**
 * Initialize
 */
function init_fees() {
	$(document).ready(
		function() {
			getUserFees();
			if ($( "#start_date" )) {
				$(function() {
					$( "#start_date" ).datepicker();
					$( "#start_date" ).datepicker("option",'dateFormat','yy-mm-dd');
				});
			}
		}


	);
}

/**
 * initialize member edit
 */
function init_edit() {
	$( "#suburb" ).autocomplete({
		source: '/modules/member_management/ajax/suburb_select.ajax.php'
	 });
	$( "#city" ).autocomplete({
		source: '/modules/member_management/ajax/city_select.ajax.php'
	 });

	 $( "#date_of_birth" ).datepicker({
		 dateFormat: "yy-mm-dd",
		 changeYear: true
	  });
}

/**
 * Get user data
 */
function getUserData() {
	document.getElementById('messageBlock').innerHTML = 'Fetching Data...';
	document.getElementById('messageBlock').className = 'messageBlockFetch';

	var saveRequest = $.get(
        '/modules/member_management/ajax/member_data.ajax.php'
    );

	saveRequest.done(function(xhr) {dataFetched(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){fetchError(xhr,ajaxOptions, thrownError)})
}

/**
 * Get user fees
 */
function getUserFees() {
	document.getElementById('messageBlock').innerHTML = 'Fetching Data...';
	document.getElementById('messageBlock').className = 'messageBlockFetch';

	var saveRequest = $.get(
        '/modules/member_management/ajax/member_fees.ajax.php'
    );

	saveRequest.done(function(xhr) {feesFetched(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){fetchError(xhr,ajaxOptions, thrownError)})
}

/**
 * Fetch Successfull
 */
function dataFetched(xhr) {
	data = JSON.parse(xhr);

	document.getElementById('name').value = data.firstname;
	document.getElementById('surname').value = data.lastname;
	document.getElementById('email').value = data.email;
	document.getElementById('title').value = data.title;
	document.getElementById('tel_home').value = data.tel_home;
	document.getElementById('tel_work').value = data.tel_work;
	document.getElementById('cellphone').value = data.cellphone;
	document.getElementById('id_number').value = data.id_number;
	document.getElementById('address_1').value = data.address_1;
	document.getElementById('address_2').value = data.address_2;
	document.getElementById('suburb').value = data.suburb;
	document.getElementById('city').value = data.city;
	document.getElementById('province').value = data.province;
	document.getElementById('postal_code').value = data.postal_code;
	document.getElementById('date_of_birth').value = data.date_of_birth;
	document.getElementById('language').value = data.language;
	document.getElementById('skills').value = data.skills;
	document.getElementById('pensioner').value = data.pensioner;
	document.getElementById('company').value = data.company;
	document.getElementById('dailyMessage').checked = data.daily_message==1?true:false;

	document.getElementById('messageBlock').innerHTML = '';
}

/**
 * Fee Fetch Successfull
 */
function feesFetched(xhr) {
	var data = JSON.parse(xhr);
	var c1 = 'I authorize and request you herewith to deduct the stipulated amount from my salary as indicated above.';
	var c2 = 'Please contact the administrator on 012 311 6214 to arrange for payment.';

	if (data.salary_ref != '') {
		consentText = c1;
	} else {
		consentText = c2;
	}

	document.getElementById('currentAmount').innerHTML = 'R' + parseFloat(data.amount).toFixed(2);
	document.getElementById('consentText').innerHTML = consentText;

	document.getElementById('messageBlock').innerHTML = '';
}

/**
 * Fetch failed
 */
function fetchError(xhr,ajaxOptions, thrownError) {
	document.getElementById('messageBlock').innerHTML = 'WARNING! An error occured while attempting to fetch your data';
	document.getElementById('messageBlock').className = 'messageBlockError';
}

/**
 * Save user data
 */
function updateDetails() {
	document.getElementById('messageBlock2').innerHTML = 'Saving...';
	document.getElementById('messageBlock2').className = 'messageBlockSave';

	var saveRequest = $.ajax({
        url: '/modules/member_management/ajax/member_save.ajax.php',
        type: 'post',
        data: $('#updateForm').serialize()
    });

	saveRequest.done(function(xhr) {dataSaved(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){saveError(xhr,ajaxOptions, thrownError)})
}

/**
 * Save user fees
 */
function updateFees() {
	document.getElementById('messageBlock2').innerHTML = 'Saving...';
	document.getElementById('messageBlock2').className = 'messageBlockSave';

	var saveRequest = $.ajax({
        url: '/modules/member_management/ajax/fees_update_send.ajax.php',
        type: 'post',
        data: $('#updateForm').serialize()
    });

	saveRequest.done(function(xhr) {feesSaved(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){saveError(xhr,ajaxOptions, thrownError)})
}

/**
 * Data save :: success
 */
function dataSaved(xhr) {
	document.getElementById('messageBlock2').innerHTML = 'Details Updated Succesfully';
	document.getElementById('messageBlock2').className = 'messageBlockSaved';
}

/**
 * Data save :: success
 */
function feesSaved(xhr) {
	document.getElementById('messageBlock2').innerHTML = 'Update Sent';
	document.getElementById('messageBlock2').className = 'messageBlockSaved';
}

/**
 * Save failed
 */
function saveError(xhr,ajaxOptions, thrownError) {
	document.getElementById('messageBlock2').innerHTML = 'WARNING! An error occured while attempting to save your data';
	document.getElementById('messageBlock2').className = 'messageBlockError';
}

/**
 * Update Password
 */
function updatePassword() {
	if (document.getElementById('newPassword').value != document.getElementById('repeatPassword').value) {
		document.getElementById('messageBlock3').innerHTML = 'New password entered incorrectly';
	} else {
		document.getElementById('messageBlock3').innerHTML = 'Saving Password...';

		var saveRequest = $.ajax({
			url: '/modules/member_management/ajax/member_password_save.ajax.php',
			type: 'post',
			data: $('#passwordForm').serialize()
		});

		saveRequest.done(function(xhr) {passwordSaved(xhr)});
		saveRequest.error(function(xhr,ajaxOptions, thrownError){passwordError(xhr,ajaxOptions, thrownError)})
	}
}

/**
 * Password Saved
 */
function passwordSaved(xhr) {
	document.getElementById('messageBlock3').innerHTML = xhr;
	if (xhr.substr(0, 5) == 'Error') {
		document.getElementById('messageBlock3').className = 'messageBlockError';
	} else {
		document.getElementById('messageBlock3').className = 'messageBlockSaved';
	}

}

/**
 * password Error
 */
function passwordError(xhr,ajaxOptions, thrownError) {
	document.getElementById('messageBlock3').innerHTML = 'WARNING! An error occured. Password not updated.';
}

/**
 * logout
 */
function logout() {
	var saveRequest = $.ajax({
		url: '/modules/member_management/ajax/logout.ajax.php',
	});

	saveRequest.done(function(xhr) {logoutComplete(xhr)});
}

/**
 * Logout complete
 */
function logoutComplete(xhr) {
	document.location = document.location;
}

/**
 * Get member list
 */
function getMembers() {
	$(document).ready(function() {
		var active = getUrlValue('active');
		var oTable = $('#memberList').dataTable( {
			"bProcessing": true,
			"sAjaxSource": '/modules/member_management/ajax/members.ajax.php?active='+active,
			"aoColumns": [
				{ "mData": "salary_ref" },
				{ "mData": "firstname" },
				{ "mData": "lastname" },
				{ "mData": "email" },
				{ "mData": "button", "bSortable": false, "sClass": "nowrap" }
			],
			"bJQueryUI": true
		} );
	} );
}

/**
 * Open the display block
 */
function displayMember(userID) {
	url = '/modules/member_management/view_member.php?userID=' + userID;
	title = "View Member";
	window.parent.W(title,url,500,900);
}

/**
 * Save user data
 */
function updateMember(userID) {
	document.getElementById('messageBlock2').innerHTML = 'Saving...';
	document.getElementById('messageBlock2').className = 'messageBlockSave';

	var saveRequest = $.ajax({
        url: '/modules/member_management/ajax/member_update.ajax.php?userID=' + userID,
        type: 'post',
        data: $('#updateForm').serialize()
    });

	saveRequest.done(function(xhr) {dataSaved(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){saveError(xhr,ajaxOptions, thrownError)})
}

/**
 * Update the heading with changes to the name
 */
function updateHeading(content,pos) {
	if (pos == 1) {
		document.getElementById('hFirstname').innerHTML = content;
	} else {
		document.getElementById('hLastname').innerHTML = content;
	}
}

/**
 * Delete a member
 */
function deleteMember(memberID) {
	if (confirm('Are you sure you want to delete this member?')) {
		url = '/modules/member_management/ajax/member_delete.ajax.php';
		$.get(url, {'memberID': memberID}, function() {
				document.location = document.location;
		});
	}
}