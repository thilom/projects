<?php

/**
 * View member
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//include 'menu.php';

//Vars
$template = file_get_contents('html/view_member.html');
$user_id = $_GET['userID'];
$province_select = '';

//Get data
$statement = "SELECT a.firstname, a.lastname, a.email, a.title, a.tel_home, a.tel_work, a.cellphone, a.id_number, a.physical_address, a.postal_address, b.postal_code, 
						a.post_delivered_to, a.date_of_birth, a.language, a.skills, a.pensioner, a.company, a.amount, a.salary_ref, a.start_date, a.username, a.active,
						a.paid, a.deogram, a.daily_message, b.address_1, b.address_2, b.suburb, b.city, b.province
				FROM {$GLOBALS['db_prefix']}_members AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_members_address AS b ON a.user_id = b.user_id
				WHERE a.user_id=:user_id
				LIMIT 1";
$sql_member = $GLOBALS['dbCon']->prepare($statement);
$sql_member->bindParam(':user_id', $user_id);
$sql_member->execute();
$sql_member_data = $sql_member->fetch();
$sql_member->closeCursor();

//Assemble delivery preference
$delivery_options = array(''=>'Please Select', 'physical'=>'Physical Address', 'postal'=>'Postal Address');
$delivery_select = '';
foreach ($delivery_options as $key=>$option) {
	if ($key == $sql_member_data['post_delivered_to']) {
		$delivery_select .= "<option value='$key' selected >$option</option>";
	} else {
		$delivery_select .= "<option value='$key'>$option</option>";
	}
}

//Assemble type preference
$type_options = array(''=>'Please Select',"Telkom"=>'Telkom',"Post Office"=>'Post Office',"Pensioner"=>'Pensioner', 
						"Private"=>'Private',"Sponsored Member"=>'Sponsored Member',"Other"=>'Other');
$type_select = '';
foreach ($type_options as $key=>$option) {
	if ($key == $sql_member_data['pensioner']) {
		$type_select .= "<option value='$key' selected >$option</option>";
	} else {
		$type_select .= "<option value='$key'>$option</option>";
	}
}

//Active checkbox
$active_check = $sql_member_data['active']=='1'?'checked':'';

//Daily Message checkbox
$daily_check = $sql_member_data['daily_message']=='1'?'checked':'';

//Paid checkbox
$paid_check = $sql_member_data['paid']=='1'?'checked':'';

//Deogram checkbox
$deogram_check = $sql_member_data['deogram']=='1'?'checked':'';

//Assemble province list
foreach ($province_list as $province_key=>$province_name) {
	if ($province_key == $sql_member_data['province']) {
		$province_select .= "<option value='$province_key' selected >$province_name</option>";
	} else {
		$province_select .= "<option value='$province_key'>$province_name</option>";
	}
	
}

//Replace Tags
$template = str_replace('<!-- title -->', $sql_member_data['title'], $template);
$template = str_replace('<!-- firstname -->', $sql_member_data['firstname'], $template);
$template = str_replace('<!-- lastname -->', $sql_member_data['lastname'], $template);
$template = str_replace('<!-- email -->', $sql_member_data['email'], $template);
$template = str_replace('<!-- tel_home -->', $sql_member_data['tel_home'], $template);
$template = str_replace('<!-- tel_work -->', $sql_member_data['tel_work'], $template);
$template = str_replace('<!-- cellphone -->', $sql_member_data['cellphone'], $template);
$template = str_replace('<!-- id_number -->', $sql_member_data['id_number'], $template);
$template = str_replace('<!-- physical_address -->', $sql_member_data['physical_address'], $template);
$template = str_replace('<!-- postal_address -->', $sql_member_data['postal_address'], $template);
$template = str_replace('<!-- postal_code -->', $sql_member_data['postal_code'], $template);
$template = str_replace('<!-- post_delivered_to -->', $sql_member_data['post_delivered_to'], $template);
$template = str_replace('<!-- date_of_birth -->', $sql_member_data['date_of_birth'], $template);
$template = str_replace('<!-- language -->', $sql_member_data['language'], $template);
$template = str_replace('<!-- skills -->', $sql_member_data['skills'], $template);
$template = str_replace('<!-- company -->', $sql_member_data['company'], $template);
$template = str_replace('<!-- amount -->', $sql_member_data['amount'], $template);
$template = str_replace('<!-- salary_ref -->', $sql_member_data['salary_ref'], $template);
$template = str_replace('<!-- start_date -->', $sql_member_data['start_date'], $template);
$template = str_replace('<!-- username -->', $sql_member_data['username'], $template);
$template = str_replace('<!-- delivery_options -->', $delivery_select, $template);
$template = str_replace('<!-- type_options -->', $type_select, $template);
$template = str_replace('<!-- user_id -->', $user_id, $template);
$template = str_replace('<!-- active_check -->', $active_check, $template);
$template = str_replace('<!-- paid_check -->', $paid_check, $template);
$template = str_replace('<!-- deogram_check -->', $deogram_check, $template);
$template = str_replace('<!-- daily_check -->', $daily_check, $template);
$template = str_replace('<!-- address_1 -->', $sql_member_data['address_1'], $template);
$template = str_replace('<!-- address_2 -->', $sql_member_data['address_2'], $template);
$template = str_replace('<!-- suburb -->', $sql_member_data['suburb'], $template);
$template = str_replace('<!-- city -->', $sql_member_data['city'], $template);
$template = str_replace('<!-- province -->', $province_select, $template);

echo $template;
