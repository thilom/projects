<?php

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/login_public.class.php';

//Vars
$mail = new PHPMailer();
$login = new Public_login();
$email_message = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/member_management/html/registration_email.html');
$daily_message = isset($_POST['dailyMessage'])?'1':'';


//Insert into DB
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_members
					(title, firstname, lastname, tel_home, tel_work, cellphone, email, id_number, date_of_birth, language, skills, 
						pensioner, company, username, amount, salary_ref, start_date, active, daily_message)
				VALUES
					(:title, :firstname, :lastname, :tel_home, :tel_work, :cellphone, :email, :id_number, :date_of_birth, :language, :skills, 
						:pensioner, :company, :username, :amount, :salary_ref, :start_date, '1', :daily_message)";
$sql_insert = $GLOBALS['dbCon']->prepare($statement);
$sql_insert->bindParam(':title', $_POST['title']);
$sql_insert->bindParam(':firstname', $_POST['firstname']);
$sql_insert->bindParam(':lastname', $_POST['lastname']);
$sql_insert->bindParam(':tel_home', $_POST['tel_home']);
$sql_insert->bindParam(':tel_work', $_POST['tel_work']);
$sql_insert->bindParam(':cellphone', $_POST['cellphone']);
$sql_insert->bindParam(':email', $_POST['email']);
$sql_insert->bindParam(':id_number', $_POST['id_number']);
$sql_insert->bindParam(':date_of_birth', $_POST['date_of_birth']);
$sql_insert->bindParam(':language', $_POST['language']);
$sql_insert->bindParam(':skills', $_POST['skills']);
$sql_insert->bindParam(':pensioner', $_POST['pensioner']);
$sql_insert->bindParam(':company', $_POST['company']);
$sql_insert->bindParam(':username', $_POST['username']);
$sql_insert->bindParam(':amount', $_POST['amount']);
$sql_insert->bindParam(':salary_ref', $_POST['salary_ref']);
$sql_insert->bindParam(':start_date', $_POST['start_date']);
$sql_insert->bindParam(':daily_message', $daily_message);
$sql_insert->execute();
$user_id = $GLOBALS['dbCon']->lastInsertId();

//Add address information
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_members_address
					(user_id, address_1, address_2, suburb, city, province, postal_code)
				VALUES
					(:user_id, :address_1, :address_2, :suburb, :city, :province, :postal_code)";
$sql_address = $GLOBALS['dbCon']->prepare($statement);
$sql_address->bindParam(':user_id', $user_id);
$sql_address->bindParam(':address_1', $_POST['address_1']);
$sql_address->bindParam(':address_2', $_POST['address_2']);
$sql_address->bindParam(':suburb', $_POST['suburb']);
$sql_address->bindParam(':city', $_POST['city']);
$sql_address->bindParam(':province', $_POST['province']);
$sql_address->bindParam(':postal_code', $_POST['postal_code']);
$sql_address->execute();

//Add password
$login->resetPassword($_POST['username'], $_POST['password']);

//Assemble email message
$email_message = str_replace('<!-- title -->', $_POST['title'], $email_message);
$email_message = str_replace('<!-- firstname -->', $_POST['firstname'], $email_message);
$email_message = str_replace('<!-- lastname -->', $_POST['lastname'], $email_message);
$email_message = str_replace('<!-- tel_home -->', $_POST['tel_home'], $email_message);
$email_message = str_replace('<!-- tel_work -->', $_POST['tel_work'], $email_message);
$email_message = str_replace('<!-- cellphone -->', $_POST['cellphone'], $email_message);
$email_message = str_replace('<!-- email -->', $_POST['email'], $email_message);
$email_message = str_replace('<!-- id_number -->', $_POST['id_number'], $email_message);
$email_message = str_replace('<!-- date_of_birth -->', $_POST['date_of_birth'], $email_message);
$email_message = str_replace('<!-- language -->', $_POST['language'], $email_message);
$email_message = str_replace('<!-- skills -->', $_POST['skills'], $email_message);
$email_message = str_replace('<!-- pensioner -->', $_POST['pensioner'], $email_message);
$email_message = str_replace('<!-- company -->', $_POST['company'], $email_message);
$email_message = str_replace('<!-- username -->', $_POST['username'], $email_message);
$email_message = str_replace('<!-- amount -->', $_POST['amount'], $email_message);
$email_message = str_replace('<!-- salary_ref -->', $_POST['salary_ref'], $email_message);
$email_message = str_replace('<!-- start_date -->', $_POST['start_date'], $email_message);
$email_message = str_replace('<!-- address_1 -->', $_POST['address_1'], $email_message);
$email_message = str_replace('<!-- address_2 -->', $_POST['address_2'], $email_message);
$email_message = str_replace('<!-- suburb -->', $_POST['suburb'], $email_message);
$email_message = str_replace('<!-- city -->', $_POST['city'], $email_message);
$email_message = str_replace('<!-- province -->', $province_list[$_POST['province']], $email_message);
$email_message = str_replace('<!-- postal_address -->', $_POST['postal_address'], $email_message);

//Send Email to new member
if (!empty($_POST['email'])) {
	try {
		$mail->ClearAddresses();
		$subject = $_POST['subjectAdmin'];
		$mail->AddAddress($_POST['email'], "{$_POST['firstname']} {$_POST['lastname']}");
		$mail->SetFrom('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
		$mail->AddReplyTo('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
		$mail->Subject = $subject;
		$mail->MsgHTML($email_message);
		$mail->Send();
	} catch (phpmailerException $e) {
		echo $e->errorMessage(); //Pretty error messages from PHPMailer
	} catch (Exception $e) {
		echo $e->getMessage(); //Boring error messages from anything else!
	}
}

//Send Email to admin
try {
	$mail->ClearAddresses();
	$subject = $_POST['subjectAdmin'];
	$mail->AddAddress('admin@tcaosa.co.za');
//	$mail->AddAddress('thilo@palladianbytes.co.za');
	$mail->SetFrom('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
	$mail->AddReplyTo('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
	$mail->Subject = $subject;
	$mail->MsgHTML($email_message);
	$mail->Send();
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
	echo $e->getMessage(); //Boring error messages from anything else!
}

?>
