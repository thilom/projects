<?php

/**
 * Password update script for members
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/login_public.class.php';

//vars
$login = new Public_login();
$current_password = $_POST['currentPassword'];
$new_password = $_POST['newPassword'];
$repeat_password = $_POST['repeatPassword'];
$username = $_SESSION['user']['name'];

//Check new password match
if ($new_password != $repeat_password) {
	die('Error: New password mismatch');
}

//Does current password match
$result = $login->loginUser($username, $current_password, true);

if ($result === true) {
	$login->resetPassword($username, $new_password);
	die('Password Successfully Updated');
} else {
	die('Error: Incorrect current password');
}

?>
