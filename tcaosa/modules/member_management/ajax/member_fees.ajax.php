<?php

/**
 * Get member data and return in json format
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$member_id = $_SESSION['user']['id'];

//Get data
$statement = "SELECT amount, company, salary_ref
				FROM {$GLOBALS['db_prefix']}_members
				WHERE user_id=:user_id
				LIMIT 1";
$sql_member = $GLOBALS['dbCon']->prepare($statement);
$sql_member->bindParam(':user_id', $member_id);
$sql_member->execute();
$sql_member_data = $sql_member->fetch();
$sql_member->closeCursor();

echo json_encode($sql_member_data);

?>
