<?php

/*
 * Return a list of members
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$member_id = $_GET['memberID'];

//Copy member
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_members_deleted SELECT * FROM {$GLOBALS['db_prefix']}_members WHERE user_id=:user_id;";
$sql_copy = $GLOBALS['dbCon']->prepare($statement);
$sql_copy->bindParam(':user_id', $member_id);
$sql_copy->execute();

//Copy member address
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_members_address_deleted SELECT * FROM {$GLOBALS['db_prefix']}_members_address WHERE user_id=:user_id;";
$sql_copy = $GLOBALS['dbCon']->prepare($statement);
$sql_copy->bindParam(':user_id', $member_id);
$sql_copy->execute();

//Remove from live data
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_members
				WHERE user_id = :user_id
				LIMIT 1";
$sql_delet = $GLOBALS['dbCon']->prepare($statement);
$sql_delet->bindParam(':user_id', $member_id);
$sql_delet->execute();

$statement = "DELETE FROM {$GLOBALS['db_prefix']}_members_address
				WHERE user_id = :user_id
				LIMIT 1";
$sql_delet = $GLOBALS['dbCon']->prepare($statement);
$sql_delet->bindParam(':user_id', $member_id);
$sql_delet->execute();

?>
