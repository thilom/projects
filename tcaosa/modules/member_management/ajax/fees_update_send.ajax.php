<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/login_public.class.php';

//Vars
$mail = new PHPMailer();
$amount = $_POST['amount'];
$start_date = $_POST['start_date'];
$user_id = $_SESSION['user']['id'];
$email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/member_management/html/email_fees_update.html');
$subject = "TCAOSA :: Member fee update request";

//Update DB
//$statement = "";
//$sql_update = $GLOBALS['dbCon']->prepare($statement);
//$sql_update->bindParam(':user_id', $user_id);
//$sql_update->bindParam(':amount', $amount);
//$sql_update->execute();

//Get current info
$statement = "SELECT firstname, lastname, amount
				FROM {$GLOBALS['db_prefix']}_members
				WHERE user_id=:user_id";
$sql_user = $GLOBALS['dbCon']->prepare($statement);
$sql_user->bindParam(':user_id', $user_id);
$sql_user->execute();
$sql_user_data = $sql_user->fetch();
$sql_user->closeCursor();

//Replace tags
$email_template = str_replace('<!-- member_name -->', "{$sql_user_data['firstname']} {$sql_user_data['lastname']}", $email_template);
$email_template = str_replace('<!-- current_amount -->', "{$sql_user_data['amount']}", $email_template);
$email_template = str_replace('<!-- updated_fee -->', "$amount", $email_template);
$email_template = str_replace('<!-- start_date -->', "$start_date", $email_template);

try {
	$mail->ClearAddresses();
//	$mail->AddAddress('admin@tcaosa.co.za');
	$mail->AddAddress('thilo@palladianbytes.co.za');
	$mail->SetFrom('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
	$mail->AddReplyTo('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
	$mail->Subject = $subject;
	$mail->MsgHTML($email_template);
	$mail->Send();
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
	echo $e->getMessage(); //Boring error messages from anything else!
}

?>
