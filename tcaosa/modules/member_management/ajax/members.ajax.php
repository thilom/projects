<?php

/*
 * Return a list of members
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$json_list = '{ '. PHP_EOL .' "aaData": [' ;
$active = $_GET['active'];

//Get member list
$statement = "SELECT user_id, firstname, lastname, email, salary_ref
				FROM {$GLOBALS['db_prefix']}_members";
if ($active == '1') {
	$statement .= " WHERE active = 1 ";
} else {
	$statement .= " WHERE active = 0 ";
}
$statement .= " ORDER BY lastname";
$sql_members = $GLOBALS['dbCon']->prepare($statement);
$sql_members->execute();
$sql_members_data = $sql_members->fetchAll();
$sql_members->closeCursor();


//Assemble data
foreach ($sql_members_data as $member_data) {
	$json_list .=  PHP_EOL .'{' . PHP_EOL;
	$json_list .= '"salary_ref": "'.$member_data['salary_ref'].'",' . PHP_EOL;
	$json_list .= '"firstname": "' . trim(str_replace(array(',','"',"'"),'',$member_data['firstname'])) . '",' . PHP_EOL;
	$json_list .= '"lastname": "' . str_replace(array(',','"',"'"),'',$member_data['lastname']) . '",' . PHP_EOL;
	$json_list .= '"email": "' . str_replace(array(',','"',"'"),'',$member_data['email']) . '",' . PHP_EOL;
	$json_list .= '"button": "<button onclick=\'deleteMember(\"'.$member_data['user_id'].'\")\'>Delete</button><button onclick=\'displayMember(\"'.$member_data['user_id'].'\")\'>Edit</button>"' . PHP_EOL;
	$json_list .= "},";
}

$json_list = substr($json_list,0, -1);
$json_list .= ']}';

echo $json_list;

?>
