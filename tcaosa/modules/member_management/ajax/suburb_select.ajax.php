<?php

/**
 *  Get a list of suburbs for a ajax auto-complete field
 *  
 *  @author Thilo Muller(2013)
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Vars
$city_list = array();
$term = "%{$_GET['term']}%";

//Get list of cities
$statement = "SELECT DISTINCT suburb
				FROM {$GLOBALS['db_prefix']}_members_address
				WHERE suburb like :term";
$sql_cities = $GLOBALS['dbCon']->prepare($statement);
$sql_cities->bindParam(':term', $term);
$sql_cities->execute();
$sql_cities_data = $sql_cities->fetchAll();
$sql_cities->closeCursor();

//Assemble list
foreach ($sql_cities_data as $city_data) {
	$city_list[] = $city_data['suburb'];
}

echo json_encode($city_list);
?>
