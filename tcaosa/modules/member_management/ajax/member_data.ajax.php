<?php

/**
 * Get member data and return in json format
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$member_id = $_SESSION['user']['id'];

//Get data
$statement = "SELECT a.firstname, a.lastname, a.email, a.title, a.tel_home, a.tel_work, a.cellphone, a.id_number, a.date_of_birth, a.language, skills, 
					a.pensioner, a.company, a.daily_message, b.address_1, b.address_2, b.suburb, b.city, b.province, b.postal_code
				FROM {$GLOBALS['db_prefix']}_members AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_members_address AS b ON a.user_id = b.user_id
				WHERE a.user_id=:user_id
				LIMIT 1";
$sql_member = $GLOBALS['dbCon']->prepare($statement);
$sql_member->bindParam(':user_id', $member_id);
$sql_member->execute();
$sql_member_data = $sql_member->fetch();
$sql_member->closeCursor();

echo json_encode($sql_member_data);

?>
