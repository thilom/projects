<?php

/*
 * Save membership data
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$member_id = (int) $_GET['userID'];
$title = $_POST['title'];
$firstname = $_POST['name'];
$lastname = $_POST['surname'];
$email = $_POST['email'];
$tel_home = $_POST['tel_home'];
$tel_work = $_POST['tel_work'];
$cellphone = $_POST['cellphone'];
$id_number = $_POST['id_number'];
$postal_code = $_POST['postal_code']; 
$date_of_birth = $_POST['date_of_birth'];
$language = $_POST['language'];
$skills = $_POST['skills'];
$pensioner = $_POST['pensioner'];
$company = $_POST['company'];
$amount = $_POST['amount'];
$salary_ref = $_POST['salary_ref'];
$start_date = $_POST['start_date'];
$username = $_POST['username'];
$active = isset($_POST['active'])?1:0;
$paid = isset($_POST['paid'])?1:0;
$deogram = isset($_POST['deogram'])?1:0;
$daily_message = isset($_POST['dailyMessage'])?1:0;
$address_1 = $_POST['address_1'];
$address_2 = $_POST['address_2'];
$suburb = $_POST['suburb'];
$city = $_POST['city'];
$province = $_POST['province'];


//Save Data
$statement = "UPDATE {$GLOBALS['db_prefix']}_members
				SET title=:title,
					firstname=:name,
					lastname=:surname, 
					email=:email, 
					tel_home=:tel_home, 
					tel_work=:tel_work, 
					cellphone=:cellphone, 
					id_number=:id_number, 
					date_of_birth=:date_of_birth, 
					language=:language, 
					skills=:skills, 
					pensioner=:pensioner, 
					company=:company,
					amount=:amount,
					salary_ref=:salary_ref,
					start_date=:start_date,
					username=:username,
					active=:active,
					paid=:paid,
					deogram=:deogram,
					daily_message=:daily_message
				WHERE user_id=:user_id
				LIMIT 1";
$sql_save = $GLOBALS['dbCon']->prepare($statement);
$sql_save->bindParam(':user_id', $member_id);
$sql_save->bindParam(':title', $title);
$sql_save->bindParam(':name', $firstname);
$sql_save->bindParam(':surname', $lastname);
$sql_save->bindParam(':email', $email);
$sql_save->bindParam(':tel_home', $tel_home);
$sql_save->bindParam(':tel_work', $tel_work);
$sql_save->bindParam(':cellphone', $cellphone);
$sql_save->bindParam(':id_number', $id_number);
$sql_save->bindParam(':date_of_birth', $date_of_birth);
$sql_save->bindParam(':language', $language);
$sql_save->bindParam(':skills', $skills);
$sql_save->bindParam(':pensioner', $pensioner);
$sql_save->bindParam(':company', $company);
$sql_save->bindParam(':amount', $amount);
$sql_save->bindParam(':salary_ref', $salary_ref);
$sql_save->bindParam(':start_date', $start_date);
$sql_save->bindParam(':username', $username);
$sql_save->bindParam(':active', $active);
$sql_save->bindParam(':paid', $paid);
$sql_save->bindParam(':deogram', $deogram);
$sql_save->bindParam(':daily_message', $daily_message);
$sql_save->execute();

//Save address details
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_members_address
					(user_id, address_1, address_2, suburb, city, province, postal_code)
				VALUES
					(:user_id, :address_1, :address_2, :suburb, :city, :province, :postal_code)
				ON DUPLICATE KEY UPDATE
					address_1 = :address_1,
					address_2 = :address_2,
					suburb = :suburb,
					city = :city,
					province = :province,
					postal_code = :postal_code";
$sql_address = $GLOBALS['dbCon']->prepare($statement);
$sql_address->bindParam(':user_id', $member_id);
$sql_address->bindParam(':address_1', $address_1);
$sql_address->bindParam(':address_2', $address_2);
$sql_address->bindParam(':suburb', $suburb);
$sql_address->bindParam(':city', $city);
$sql_address->bindParam(':province', $province);
$sql_address->bindParam(':postal_code', $postal_code);
$sql_address->execute();

?>
