<?php

/*
 * Save membership data
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$member_id = $_SESSION['user']['id'];
$title = $_POST['title'];
$firstname = $_POST['name'];
$lastname = $_POST['surname'];
$email = $_POST['email'];
$tel_home = $_POST['tel_home'];
$tel_work = $_POST['tel_work'];
$cellphone = $_POST['cellphone'];
$id_number = $_POST['id_number'];
$date_of_birth = $_POST['date_of_birth'];
$language = $_POST['language'];
$skills = $_POST['skills'];
$pensioner = $_POST['pensioner'];
$company = $_POST['company'];


$daily_message = isset($_POST['dailyMessage'])?1:0;

//Save Data
$statement = "UPDATE {$GLOBALS['db_prefix']}_members
				SET title=:title,
					firstname=:name,
					lastname=:surname, 
					email=:email, 
					tel_home=:tel_home, 
					tel_work=:tel_work, 
					cellphone=:cellphone, 
					id_number=:id_number, 
					date_of_birth=:date_of_birth, 
					language=:language, 
					skills=:skills, 
					pensioner=:pensioner, 
					company=:company,
					daily_message = :daily_message
				WHERE user_id=:user_id
				LIMIT 1";
$sql_save = $GLOBALS['dbCon']->prepare($statement);
$sql_save->bindParam(':user_id', $member_id);
$sql_save->bindParam(':title', $title);
$sql_save->bindParam(':name', $firstname);
$sql_save->bindParam(':surname', $lastname);
$sql_save->bindParam(':email', $email);
$sql_save->bindParam(':tel_home', $tel_home);
$sql_save->bindParam(':tel_work', $tel_work);
$sql_save->bindParam(':cellphone', $cellphone);
$sql_save->bindParam(':id_number', $id_number);
$sql_save->bindParam(':date_of_birth', $date_of_birth);
$sql_save->bindParam(':language', $language);
$sql_save->bindParam(':skills', $skills);
$sql_save->bindParam(':pensioner', $pensioner);
$sql_save->bindParam(':company', $company);
$sql_save->bindParam(':daily_message', $daily_message);
$sql_save->execute();

//Add address information
$statement = "UPDATE {$GLOBALS['db_prefix']}_members_address
					SET address_1 = :address_1,
						address_2 = :address_2,
						suburb = :suburb,
						city = :city,
						province = :province,
						postal_code = :postal_code
				WHERE user_id = :user_id";
$sql_address = $GLOBALS['dbCon']->prepare($statement);
$sql_address->bindParam(':user_id', $member_id);
$sql_address->bindParam(':address_1', $_POST['address_1']);
$sql_address->bindParam(':address_2', $_POST['address_2']);
$sql_address->bindParam(':suburb', $_POST['suburb']);
$sql_address->bindParam(':city', $_POST['city']);
$sql_address->bindParam(':province', $_POST['province']);
$sql_address->bindParam(':postal_code', $_POST['postal_code']);
$sql_address->execute();

?>
