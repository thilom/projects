<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Member Management";
$module_id = "member_management"; //needs to be the same as the module directory
$module_icon = 'off_gpl_64.png';
$module_link = 'members.php?active=1';
$window_width = '800';
$window_height = '';
$window_position = '';

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Manage Member Messaging";
$security_id = "mod_messaging";


?>