<?php

/**
 * Manage members
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//include 'menu.php';
include 'toolbar.php';

//Vars
$template = file_get_contents('html/members.html');
$user_id = $_SESSION['dbweb_user_id'];

//Replace Tags

echo $template;
