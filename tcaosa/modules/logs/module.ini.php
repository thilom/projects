<?php
$module_name = "{$GLOBALS['prog_name']} History";
$module_icon = 'off_history_64.png';
$module_link = 'logs.php';
$window_width = '800';
$window_height = '600';
$window_position = '';

//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "View {$GLOBALS['prog_name']} Logs";
$security_id = "mod_logs";
?>