<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'menu.php';

//Vars
$template = file_get_contents('html/messaging.html');
$user_id = $_SESSION['dbweb_user_id'];

//Get currently logged in email address
$statement = "SELECT email
				FROM {$GLOBALS['db_prefix']}_user
				WHERE user_id=:user_id
				LIMIT 1";
$sql_email = $GLOBALS['dbCon']->prepare($statement);
$sql_email->bindParam(':user_id', $user_id);
$sql_email->execute();
$sql_email_data = $sql_email->fetch();
$sql_email->closeCursor();

//Replace Tags
$template = str_replace('<!-- email_address -->', $sql_email_data['email'], $template);

echo $template;

?>
