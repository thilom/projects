/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(
	function()
	{
		$( "#tabs" ).tabs();
		$('.redactor').redactor({
			buttons: ['html', '|', 'formatting', '|', 'underline','bold', 'italic', 'deleted', '|',
				'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
				'table', 'link', '|',
				'fontcolor', 'backcolor', '|', 'alignment', '|', 'horizontalrule'],
			minHeight: 240,
			autoresize: false,
			keyupCallback: function(obj, event) {setUnsaved()}
		});
	}
);
	
/**
 * Save Tab data
 */
function saveTab() {
	
	document.getElementById('messageBlock').innerHTML = 'Saving...';
	document.getElementById('messageBlock').className = 'messageBlockSave';
	
	var saveRequest = $.ajax({
        url: '/modules/messaging/saveMessage_afr.php',
        type: 'post',
        data: $('#messageForm').serialize()
    });
	
	saveRequest.done(function(xhr) {dataSaved(xhr)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){dataError(xhr,ajaxOptions, thrownError)})
}

/**
 * Show data saved
 */
function dataSaved(xhr) {
	var now = new Date();
	var saveHour = now.getHours();
	var saveMinute = now.getMinutes();
	if (xhr == 'OK') {
		document.getElementById('messageBlock').innerHTML = 'Document Saved - ' + saveHour + 'h' + saveMinute;
		document.getElementById('messageBlock').className = 'messageBlockSuccess';
	} else {
		document.getElementById('messageBlock').innerHTML = 'A script error occured while attempting to save your document. <br>Please contact the system administrator.';
		document.getElementById('messageBlock').className = 'messageBlockError';
	}
}

/**
 * Message if save was unsuccessfull
 */
function dataError(xhr,ajaxOptions, thrownError) {
	console.log(arguments)
	alert(xhr.status)
	switch (xhr.status) {
		case 404:
			document.getElementById('messageBlock').innerHTML = 'An error occured while attempting to save your document. <br>The server returned an error: 404 '+thrownError+'.';
			document.getElementById('messageBlock').className = 'messageBlockError';
			break;
		case 500:
			document.getElementById('messageBlock').innerHTML = 'An error occured while attempting to save your document. <br>The server returned an error: 500 '+thrownError+'.';
			document.getElementById('messageBlock').className = 'messageBlockError';
			break;
		default:
			document.getElementById('messageBlock').innerHTML = 'An error occured while attempting to save your document. <br>Please check your internet connection and try again ('+thrownError+').';
			document.getElementById('messageBlock').className = 'messageBlockError';
			break;
	}
}


/**
 * Sets the message to unsaved state
 */
function setUnsaved() {
	document.getElementById('messageBlock').innerHTML = 'Document updated - Save before closing';
	document.getElementById('messageBlock').className = 'messageBlockSave';
}