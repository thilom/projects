
/**
 * Send english test message
 */
function sendTest(mode) {
	document.getElementById(mode+'Message').innerHTML = 'Sending...';
	document.getElementById(mode+'Message').className = 'messageBlockSave';
	switch (mode) {
		case 'eng':
			file = '/modules/messaging/english_message_test.ajax.php';
			formBlock = 'engTest';
			break;
		case 'afr':
			file = '/modules/messaging/afrikaans_message_test.ajax.php';
			formBlock = 'afrTest';
			break;
		case 'birthday':
			file = '/modules/messaging/birthday_message_test.ajax.php';
			formBlock = 'birthTest';
			break;
		default:
			break;
	}

	
	var saveRequest = $.ajax({
        url: file,
        type: 'post',
        data: $('#'+formBlock).serialize()
    });
	
	saveRequest.done(function(xhr) {sendSuccess(xhr, mode)});
	saveRequest.error(function(xhr,ajaxOptions, thrownError){sendFailed(xhr,ajaxOptions, thrownError, mode)})
}


/**
 * Test sent OK
 */
function sendSuccess(xhr, mode) {
	var now = new Date();
	var saveHour = now.getHours();
	var saveMinute = now.getMinutes();
	if (xhr == 'OK') {
		document.getElementById(mode+'Message').innerHTML = 'Test Message Sent - ' + saveHour + 'h' + saveMinute;
		document.getElementById(mode+'Message').className = 'messageBlockSuccess';
	} else {
		document.getElementById(mode+'Message').innerHTML = 'A script error occured while attempting to send the test message. <br>Please contact the system administrator.';
		document.getElementById(mode+'Message').className = 'messageBlockError';
	}
}

/**
 * Sending failed
 */
function sendFailed(xhr,ajaxOptions, thrownError, mode) {
	switch (xhr.status) {
		case 404:
			document.getElementById(mode+'Message').innerHTML = 'An error occured while attempting to send the test message. <br>The server returned an error: 404 '+thrownError+'.';
			document.getElementById(mode+'Message').className = 'messageBlockError';
			break;
		case 500:
			document.getElementById(mode+'Message').innerHTML = 'An error occured while attempting to send the test message. <br>The server returned an error: 500 '+thrownError+'.';
			document.getElementById(mode+'Message').className = 'messageBlockError';
			break;
		default:
			document.getElementById(mode+'Message').innerHTML = 'An error occured while attempting to send the test message. <br>Please check your internet connection and try again ('+thrownError+').';
			document.getElementById(mode+'Message').className = 'messageBlockError';
			break;
	}
}

