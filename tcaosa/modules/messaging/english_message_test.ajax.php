<?php

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';

//Vars
$mail = new PHPMailer();
$recipient = $_POST['recipient'];
$subject = "TEST: Daily Report...";
$message = '';

//Get message Data
$statement = "SELECT message_1,message_3,message_4,message_5, message_6, message_7 
				FROM message_settings";
$sql_message = $GLOBALS['dbCon']->prepare($statement);
$sql_message->execute();
$sql_message_data = $sql_message->fetch();
$sql_message->closeCursor();
		
//Assemble Message
$message .= "<head><meta http-equiv='Content-Type' content='text/html; charset=windows-1251'></head>";
$message .= "<body>\r\n";
$message .= "<p>Beloved TEST,</p>\r\n";
		
//Daily Message
if (!empty($sql_message_data['message_1'])) {
	$message .= "<p><strong><u>Today's Message (English):</u></strong></p>\r\n";
	$message .= $sql_message_data['message_1']."\r\n";
	$message .= "<br><hr width='100%'>\r\n";
}
$message .= "<p><strong><u><font color='#F00'>Important Note:</font></u></strong></p>\r\n";
$message .= "<p><font color='#F00'>Please note it is contrary to send mail through the Company network that is not Company related. With the future development on the CA Web it is envisaged that you should be able to observe such large documents from the CA Web in your own time.</font></p>\r\n";
$message .= "<br><hr width='100%'>\r\n";
		
//Prayer Request
if (!empty($sql_message_data['message_3'])) {
	$message .= "<p><strong><u>Today's prayer requests:</u></strong> (Please note that prayer requests can also be logged online at : <a href='http://www.tcaosa.co.za/index.php?page=contact_us'>click here</a>)</p>\r\n";
	$message .= $sql_message_data['message_3']."\r\n";
	$message .= "<br><hr width='100%'>\r\n";
}
		
//Web Requests
if (!empty($sql_message_data['message_4'])) {
	$message .= "<p><strong><u>Today's Web Requests:</u></strong></p>\r\n";
	$message .= $sql_message_data['message_4']."\r\n";
	$message .= "<br><hr width='100%'>\r\n";
}

//Testemony
if (!empty($sql_message_data['message_5'])) {
	$message .= "<p><strong><u>Today's Testimony / Witness Received:</u></strong></p>\r\n";
	$message .= $sql_message_data['message_5']."\r\n";
	$message .= "<br><hr width='100%'>\r\n";
}

//Events
if (!empty($sql_message_data['message_6'])) {
	$message .= "<p><strong><u>This week's events:</u></strong></p>\r\n";
	$message .= $sql_message_data['message_6']."\r\n";
}
		
//Footer				
$message .= $sql_message_data['message_7']."\r\n";
				
//Send Email
try {
	$mail->AddAddress($recipient);
	$mail->CharSet = 'UTF-8';
	$mail->SetFrom('admin@tcaosa.co.za', 'The Christian Association of South Africa');
	$mail->AddReplyTo('admin@tcaosa.co.za', 'The Christian Association of South Africa');
	$mail->Subject = $subject;
	$mail->MsgHTML($message);
	$mail->Send();
	echo 'OK';
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
	echo 'Fail';
} catch (Exception $e) {
	echo 'Fail';
	echo $e->getMessage(); //Boring error messages from anything else!
}

?>