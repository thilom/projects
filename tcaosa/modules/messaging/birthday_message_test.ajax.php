<?php

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';

//Vars
$mail = new PHPMailer();
$recipient = $_POST['recipient'];
$subject = "TEST: Happy Birthday";
$message = '';

//Get message Data
$statement = "SELECT message_8 
				FROM message_settings";
$sql_message = $GLOBALS['dbCon']->prepare($statement);
$sql_message->execute();
$sql_message_data = $sql_message->fetch();
$sql_message->closeCursor();
		
	
$message .= "<body>\r\n";
$message .= "<p>Beloved TEST TEST,</p>\r\n";
$message .= $sql_message_data['message_8']."\r\n";
				
//Send Email
try {
	$mail->AddAddress($recipient);
	$mail->SetFrom('admin@tcaosa.co.za', 'The Christian Association of South Africa');
	$mail->AddReplyTo('admin@tcaosa.co.za', 'The Christian Association of South Africa');
	$mail->Subject = $subject;
	$mail->MsgHTML($message);
	$mail->Send();
	echo 'OK';
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
	echo 'Fail';
} catch (Exception $e) {
	echo 'Fail';
	echo $e->getMessage(); //Boring error messages from anything else!
}

?>