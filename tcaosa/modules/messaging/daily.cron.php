<?php


//includes
$cron = 1;
//$document_root = $_SERVER['DOCUMENT_ROOT'];
$document_root = '/home/tcaosa/public_html';
include_once $document_root. '/settings/init.php';
include_once $document_root . '/shared/database_functions.php';
require_once $document_root . '/modules/logs/log_functions.php';
require_once $document_root . '/shared/PHPMailer/class.phpmailer.php';

//Vars
$mail = new PHPMailer();
$users = array();
$counter = 0;
$english_template = file_get_contents("$document_root/html/english_email_template.html");
$afrikaans_template = file_get_contents("$document_root/html/afrikaans_email_template.html");
$english_count = 0;
$afrikaans_count = 0;
$error_count = 0;
$email_date = date('d-m-Y');
$sent_list = array();

//Is it sunday of saturday
if (date('w') == '6' || date('w') == '0') {
	die();
}

//Get send list
$statement = "SELECT email_address 
				FROM {$GLOBALS['db_prefix']}_email_log
				WHERE DATE_FORMAT(email_date, '%d-%m-%Y') = :email_date";
$sql_sent = $GLOBALS['dbCon']->prepare($statement);
$sql_sent->bindParam(':email_date', $email_date);
$sql_sent->execute();
$sql_sent_data = $sql_sent->fetchAll();
$sql_sent->closeCursor();
foreach ($sql_sent_data as $sent_data) {
	$sent_list[] = $sent_data['email_address'];
}

//Get user list
$statement = "SELECT firstname, lastname, email, language
				FROM {$GLOBALS['db_prefix']}_members";
$sql_users = $GLOBALS['dbCon']->prepare($statement);
$sql_users->execute();
$sql_users_data = $sql_users->fetchAll();
$sql_users->closeCursor();
foreach ($sql_users_data as $user_data) {
	$users[$counter]['language'] = $user_data['language'];
	$users[$counter]['firstname'] = $user_data['firstname'];
	$users[$counter]['lastname'] = $user_data['lastname'];
	$users[$counter]['email'] = $user_data['email'];
	
	$counter++;
}

/* Assemble English Email - START */

//Get english data
$statement = "SELECT message_1,message_3,message_4,message_5, message_6, message_7 
				FROM message_settings";
$sql_message = $GLOBALS['dbCon']->prepare($statement);
$sql_message->execute();
$sql_message_data = $sql_message->fetch();
$sql_message->closeCursor();

//Replace tags
$english_template = str_replace('<!-- message1 -->', $sql_message_data['message_1'], $english_template);
$english_template = str_replace('<!-- message3 -->', $sql_message_data['message_3'], $english_template);
$english_template = str_replace('<!-- message4 -->', $sql_message_data['message_4'], $english_template);
$english_template = str_replace('<!-- message5 -->', $sql_message_data['message_5'], $english_template);
$english_template = str_replace('<!-- message6 -->', $sql_message_data['message_6'], $english_template);
$english_template = str_replace('<!-- message7 -->', $sql_message_data['message_7'], $english_template);
/* Assemble English Email - END */

/* Assemble Afrikaans Email - START */
//Get afrikaans data
$statement = "SELECT message_1,message_3,message_4,message_5, message_6, message_7 
				FROM message_settings_afr";
$sql_message = $GLOBALS['dbCon']->prepare($statement);
$sql_message->execute();
$sql_message_data = $sql_message->fetch();
$sql_message->closeCursor();

//Replace tags
$afrikaans_template = str_replace('<!-- message1 -->', $sql_message_data['message_1'], $afrikaans_template);
$afrikaans_template = str_replace('<!-- message3 -->', $sql_message_data['message_3'], $afrikaans_template);
$afrikaans_template = str_replace('<!-- message4 -->', $sql_message_data['message_4'], $afrikaans_template);
$afrikaans_template = str_replace('<!-- message5 -->', $sql_message_data['message_5'], $afrikaans_template);
$afrikaans_template = str_replace('<!-- message6 -->', $sql_message_data['message_6'], $afrikaans_template);
$afrikaans_template = str_replace('<!-- message7 -->', $sql_message_data['message_7'], $afrikaans_template);

/* Assemble Afrikaans Email - END */

//Send emails
foreach ($users as $user_data) {
	if ($afrikaans_count+$english_count > 100) continue;
	if (in_array($user_data['email'], $sent_list)) continue;
	
	if ($user_data['language'] == 'Afrikaans') {
		$email_template = $afrikaans_template;
		$email_subject = 'Daaglikse Woord...';
	} else {
		$email_template = $english_template;
		$email_subject = 'Daily Report...';
	}
	
	//Replace name
	$email_template = str_replace('<!-- firstname -->', $user_data['firstname'], $email_template);
	$email_template = str_replace('<!-- lastname -->', $user_data['lastname'], $email_template);
	
	try {
		$mail->ClearAddresses();
		$mail->CharSet = 'UTF-8';
		$mail->AddAddress($user_data['email'], "{$user_data['firstname']} {$user_data['last_name']}");
		$mail->SetFrom('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
		$mail->AddReplyTo('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
		$mail->Subject = $email_subject;
		$mail->MsgHTML($email_template);
		$mail->Send();
		
		$email_result = 1;
		if ($user_data['language'] == 'Afrikaans') {
			$afrikaans_count++;
		} else {
			$english_count++;
		}
		
		echo "{$user_data['email']} :: {$user_data['language']}\n";
		
	} catch (phpmailerException $e) {
		$error_message = $e->errorMessage();
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_errors
							(error_date, error_email, error_subject, error_message)
						VALUES
							(NOW(), :error_email, :error_subject, :error_message)";
		$sql_error = $GLOBALS['dbCon']->prepare($statement);
		$sql_error->bindParam(':error_email', $user_data['email']);
		$sql_error->bindParam(':error_message', $error_message);
		$sql_error->bindParam(':error_subject', $email_subject);
		$sql_error->execute();
		$sql_error_data = $sql_error_data->fetch();
		$sql_error->closeCursor();
		
		$email_result = 0;
		$error_count++;
	} catch (Exception $e) {
		$error_message = $e->getMessage();
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_errors
							(error_date, error_email, error_subject, error_message)
						VALUES
							(NOW(), :error_email, :error_subject, :error_message)";
		$sql_error = $GLOBALS['dbCon']->prepare($statement);
		$sql_error->bindParam(':error_email', $user_data['email']);
		$sql_error->bindParam(':error_message', $error_message);
		$sql_error->bindParam(':error_subject', $email_subject);
		$sql_error->execute();
		$sql_error_data = $sql_error_data->fetch();
		$sql_error->closeCursor();
		
		$email_result = 0;
		$error_count++;
	}
	
	//Add to sent list
	$sent_list[] = $user_data['email'];
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_log
						(email_date, email_address, email_result)
					VALUES
						(NOW(), :email_address, :email_result)";
	$sql_sent_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_sent_insert->bindParam(':email_address', $user_data['email']);
	$sql_sent_insert->bindParam(':email_result', $email_result);
	$sql_sent_insert->execute();
}

/* Insert Stats  START */

//Does an entry already exist for today
$statement = "SELECT total_english, total_afrikaans, total_error
				FROM {$GLOBALS['db_prefix']}_email_stats
				WHERE DATE_FORMAT(stat_date, '%d-%m-%Y')=:stat_date";
$sql_stat_check = $GLOBALS['dbCon']->prepare($statement);
$sql_stat_check->bindParam(':stat_date', $email_date);
$sql_stat_check->execute();
$sql_stat_check_data = $sql_stat_check->fetch();
$sql_stat_check->closeCursor();

if ($sql_stat_check_data === false) {
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_email_stats
						(stat_date, total_english, total_afrikaans, total_error)
					VALUES
						(NOW(), :total_english, :total_afrikaans, :total_error)";
	$sql_stats = $GLOBALS['dbCon']->prepare($statement);
	$sql_stats->bindParam(':total_english', $english_count);
	$sql_stats->bindParam(':total_afrikaans', $afrikaans_count);
	$sql_stats->bindParam(':total_error', $error_count);
	$sql_stats->execute();
} else {
	$english_count += $sql_stat_check_data['total_english'];
	$afrikaans_count += $sql_stat_check_data['total_afrikaans'];
	$error_count += $sql_stat_check_data['total_error'];
	
	$statement = "UPDATE {$GLOBALS['db_prefix']}_email_stats
					SET total_english = :total_english,
						total_afrikaans = :total_afrikaans,
						total_error = :total_error
					WHERE DATE_FORMAT(stat_date, '%d-%m-%Y')=:email_date
					LIMIT 1";
	$sql_stats_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_stats_update->bindParam(':total_english', $english_count);
	$sql_stats_update->bindParam(':total_afrikaans', $afrikaans_count);
	$sql_stats_update->bindParam(':total_error', $error_count);
	$sql_stats_update->bindParam(':email_date', $email_date);
	$sql_stats_update->execute();
}

/* Insert stats - END */



?>
