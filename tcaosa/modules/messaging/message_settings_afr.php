<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'menu.php';

//Vars
$template = file_get_contents('html/message_settings_afrikaans.html');

//Get current data
$statement = "SELECT message_1,message_2,message_3,message_4,message_5, message_6, message_7, message_8 FROM message_settings_afr";
$sql_message = $GLOBALS['dbCon']->prepare($statement);
$sql_message->execute();
$sql_message_data = $sql_message->fetch();
$sql_message->closeCursor();

//Replace Tags
$template = str_replace('<!-- todays_message -->', $sql_message_data['message_1'], $template);
$template = str_replace('<!-- prayer_message -->', $sql_message_data['message_3'], $template);
$template = str_replace('<!-- web_message -->', $sql_message_data['message_4'], $template);
$template = str_replace('<!-- testimony_message -->', $sql_message_data['message_5'], $template);
$template = str_replace('<!-- events_message -->', $sql_message_data['message_6'], $template);
$template = str_replace('<!-- footer_message -->', $sql_message_data['message_7'], $template);
$template = str_replace('<!-- birthday_message -->', $sql_message_data['message_8'], $template);

echo $template;
?>