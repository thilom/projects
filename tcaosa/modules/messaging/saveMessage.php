<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$today_message = $_POST['todayMessage'];
$prayer_message = $_POST['prayer'];
$web_message = $_POST['web'];
$testimony_message = $_POST['testimony'];
$events_message = $_POST['events'];
$footer_message = $_POST['footer'];
$birthday_message = $_POST['birthday'];

//Save Data
try{
	$statement = "UPDATE message_settings 
					SET message_1=:today_message,
						message_3=:prayer_message,
						message_4=:web_message,
						message_5=:testimony_message,
						message_6=:events_message,
						message_7=:footer_message,
						message_8=:birthday_message";
	$sql_save = $GLOBALS['dbCon']->prepare($statement);
	$sql_save->bindParam(':today_message', $today_message);
	$sql_save->bindParam(':prayer_message', $prayer_message);
	$sql_save->bindParam(':web_message', $web_message);
	$sql_save->bindParam(':testimony_message', $testimony_message);
	$sql_save->bindParam(':events_message', $events_message);
	$sql_save->bindParam(':footer_message', $footer_message);
	$sql_save->bindParam(':birthday_message', $birthday_message);
	$sql_save->execute();
	
	echo 'OK';
} catch (Exception $e) {
    echo $e->getMessage();
}


?>
