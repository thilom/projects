<?php

/**
 *  Toolbar for tour editing
 *
 *  @author Thilo Muller(2011)
 *  @version $Id: toolbar.php 131 2012-01-02 06:53:41Z thilo $
 */



//Menu Bar
$menu_bar = "<table id=menu_bar><tr>";

//Vars


$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='messaging.php?W={$_GET['W']}'\">Home</td>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='message_settings_eng.php?W={$_GET['W']}'\">English Messages</td>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='message_settings_afr.php?W={$_GET['W']}'\">Afrikaanse Boodskappe</td>";


$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "</table>";
echo "<div id=top_menu >$menu_bar</div>";


?>
