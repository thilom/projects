<?php
/**
 * AJAX login 
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/login_public.class.php';

//Vars
$username = $_POST['username'];
$login = new Public_login();

//Attempt login
$login_result = $login->loginUser($username, $_POST['password']);

if ($login_result === true) {
	echo 'OK';
} else {
	echo 'Login Failed';
}

?>
