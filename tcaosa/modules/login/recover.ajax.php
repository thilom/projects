<?php

/**
 * Recover a lost password
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/login_public.class.php';

//Vars
$login = new Public_login();
$username = $_POST['username'];

//Reset password
$login->setEmailSubject('TCAOSA: password reset');
$login->setEmailTemplate($_SERVER['DOCUMENT_ROOT'] . '/modules/login/html/email_template.html');
$result = $login->resetPassword($username, false, true);
//$result = $login->resetPassword($username, 'hallothilo', false);

if ($result === true) {
	echo 'OK';
} else {
	
}
?>
