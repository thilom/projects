<?php
require_once SITE_ROOT .'/modules/login/module.ini.php';
require_once SITE_ROOT . '/class.sql_inject.php';

class login {
	private $username = "";
	private $pass = "";
	private $pass_encrypt;
	private $incorrect_login = 0;
	private $login_timeout;
	private $login_log = true;

	/**
	 * start session and set up timeout
	 */
	function __construct() {
		//secureSession::set_secureword(SECURE_PHRASE);

		$session_id = session_id();
		if (empty($session_id)) {
			session_cache_limiter('private');
			session_cache_expire($this->login_timeout);
			session_start();
		}
		$this->set_pass_encryption();
		$this->set_login_timeout();
	}

	/**
	 * set password encryption
	 *
	 * @param bool $encrypt
	 * @return bool
	 */
	public function set_pass_encryption($encrypt = true) {
		$this->pass_encrypt = $encrypt;
		return true;
	}

	/**
	 * set the amount of minutes for a session
	 *
	 * @param int $timeout
	 * @return bool
	 */
	public function set_login_timeout($timeout = LOGIN_TIMEOUT) {
		$this->login_timeout = $timeout;
		return true;
	}

	/**
	 * reset incorrect login count
	 *
	 * @return bool
	 */
	public function reset_login() {
		$this->incorrect_login = 0;
		return true;
	}

	/**
	 * do the login procedure
	 *
	 * @param string $table
	 * @param string $username
	 * @param string $password
	 * @return bool
	 */
	public function do_login($username, $password) {

		$statement = "SELECT user_id, user_type FROM {$GLOBALS['db_prefix']}_user WHERE username=:un AND np=:pass";
		$sql_login = $GLOBALS['dbCon']->prepare($statement);
		$sql_login->bindParam(':un', $username);
		$sql_login->bindParam(':pass', md5($password));
		$sql_login->execute();
		$res = $sql_login->fetchAll();
		$cnt = count($res);
		if ($cnt == 1) {
			$_SESSION['dbweb_user_id'] = $res[0]['user_id'];
			$_SESSION['user_type'] = $res[0]['user_type'];
		} else {
			echo "<div style='text-align: center; color: red; font-weight: bold; font-size: 0.8em'>Login Failed</div>";
		}

		return true;
	}

	/**
	 * log all login attempts
	 *
	 * @param string $username
	 * @return bool
	 */
	private function log_attempt($username) {
		$sql = "SELECT la_attempt, la_id FROM login_attempts WHERE LOWER(la_username) = LOWER('".trim($username)."') AND la_date >= ".(time()-(10*60))." AND la_date <= ".(time()+(10*60));
		$la_rs = mysql_query($sql);

		if (mysql_num_rows($la_rs) > 0) {
			$la_ds = mysql_fetch_object($la_rs);
			$sql = "UPDATE login_attempts SET la_attempt = (la_attempt + 1) WHER	E la_id = ".$la_ds->la_id;
			$this->incorrect_login = $la_ds->la_attempt + 1;
		} else {
			$sql = "INSERT INTO login_attempts (la_username, la_ip, la_attempt, la_date) VALUES ('".mysql_escape_string($username)."', '".getenv("REMOTE_ADDR")."', 1,  ".time().")";
			$this->incorrect_login = 1;
		}

		mysql_query($sql);
		return true;
	}

	/**
	 * log user out
	 *
	 * @return bool
	 */
	public function do_logout() {
		unset($_SESSION['dbweb_user_id']);
		session_destroy();
		return true;
	}

	/**
	 * check if user logged in
	 *
	 * @return bool
	 */
	public function check_login() {
		//if (!secureSession::Check() || empty($_SESSION['dbweb_user_id']) || !isset($_SESSION['dbweb_user_id']) || !$_SESSION['dbweb_user_id']) {
		if (empty($_SESSION['dbweb_user_id']) || !isset($_SESSION['dbweb_user_id']) || !$_SESSION['dbweb_user_id']) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * check the log
	 *
	 * @param string $field
	 * @return bool
	 */
	private function read_log($field) {
		$sql = "SELECT * FROM login_log WHERE ll_desc = '".$field."' ORDER BY ll_time DESC LIMIT 1";
		$rs = mysql_query($sql);

		if (mysql_num_rows($rs) == 1) {
			$ds = mysql_fetch_object($rs);
			if (time() > $ds->ll_time) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * log login attempts
	 *
	 * @return bool
	 */
	private function check_login_attempts($username="") {
		if ($this->incorrect_login == 3) {
			if ($this->login_log) {
	            $sql = "INSERT INTO login_log (ll_time, ll_desc) VALUES (".(time()+(10*60)).", '".getenv("REMOTE_ADDR")."')";
				mysql_query($sql);
	        }
		} elseif ($this->incorrect_login >= 5) {
			if ($this->login_log) {
	            $sql = "INSERT INTO login_log (ll_time, ll_desc) VALUES (".(time()+(10*60)).", '".$username."')";
				mysql_query($sql);
			}
		}
		return true;
	}

	/**
	 *
	 */
	function __destruct() {

	}

}

?>