<?php
$module_name = "{$GLOBALS['prog_name']} Users";
$module_id = "login"; //needs to be the same as the module directory
$module_icon = 'off_users_64.png';
$module_link = 'login_list.php';
$window_width = '800';
$window_height = '600';
$window_position = '';

//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

if (!defined('SECURE_PHRASE')) define('SECURE_PHRASE', 'dbweb4_secure');
if (!defined('LOGIN_TIMEOUT')) define('LOGIN_TIMEOUT', 30);

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Manage {$GLOBALS['prog_name']} Users";
$security_id = "mod_users";
?>