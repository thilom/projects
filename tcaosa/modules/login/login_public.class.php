<?php

/**
 * 
 */

class Public_login {
	private $salt = "36a2cc98337f49c9cf010458";
	private $email_template = '';
	private $email_subject = '';
	private $error_massage = '';

	/**
	 * start session and set up timeout
	 */
	function __construct() {
		
	}
	
	/**
	 * Login a user
	 */
	function loginUser($username, $password, $check_only=false) {
		$password = $this->hash($password);
				
		$statement = "SELECT password, user_id
						FROM {$GLOBALS['db_prefix']}_members
						WHERE username=:username
						LIMIT 1";
		$sql_user = $GLOBALS['dbCon']->prepare($statement);
		$sql_user->bindParam(':username', $username);
		$sql_user->execute();
		$sql_user_data = $sql_user->fetch();
		$sql_user->closeCursor();
		
		if (count($sql_user_data) == 0) {
			return false;
		} else {
			if ($sql_user_data['password'] == $password) {
				if ($check_only === false) {
					$_SESSION['user']['name'] = $username;
					$_SESSION['user']['id'] = $sql_user_data['user_id'];
				}
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 * Hash a password
	 */
	function hash($password) {
		$encrypted_password = crypt($password, "$2a$07$".$this->salt."$");
		$password = substr($encrypted_password, 28);
		return $password;
	}
	
	/**
	 * Resets the user password for a selected user.
	 * 
	 * @param str $username
	 * @param str $password False to auto create a password
	 */
	function resetPassword($username, $password=false, $email=false) {
		
		//Does the username exist
		$statement = "SELECT COUNT(*) AS user_count 
						FROM {$GLOBALS['db_prefix']}_members
						WHERE username = :username";
		$sql_check = $GLOBALS['dbCon']->prepare($statement);
		$sql_check->bindParam(':username', $username);
		$sql_check->execute();
		$sql_check_data = $sql_check->fetch();
		$sql_check->closeCursor();
		
		if ($sql_check_data['user_count'] === 0) {
			return false;
		}
		
		if ($password === false) {
			$created_password = $this->createPassword();
			$raw_password = $created_password['raw'];
			$password = $created_password['hash'];
		} else {
			$password = $this->hash($password);
		}
		
		//Update password
		$statement = "UPDATE {$GLOBALS['db_prefix']}_members
						SET password=:password
						WHERE username=:username
						LIMIT 1";
		$sql_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_update->bindParam(':username', $username);
		$sql_update->bindParam(':password', $password);
		$sql_update->execute();
		
		if ($email === true) {
			$this->send_email($username, $raw_password);
		} else {
			return true;
		}
		
		
	}
	
	/**
	 * Create a random password
	 * @return array 'raw'->Raw password, 'hash'->Password Hash
	 */
	private function createPassword() {
		$raw_password = '';
		$characters = '123456789BCDFGHJKMNPQRSTVWXYZ$#@!';
		for ($x=0; $x<8; $x++) {
			$char_pos = rand(0, strlen($characters)-1);
			$raw_password .= $characters[$char_pos];
		}
		$password['raw'] = $raw_password;
		$password['hash'] = $this->hash($raw_password);
		
		return $password;
	}
	
	/**
	 * Send an email to a user with the new password. Replaces the following tabs in the template.
	 * '<!-- username -->' - The users username
	 * '<!-- password -->' - The users new password
	 * '<!-- firstname -->' - The users firstname
	 * '<!-- lastname -->' - The users last name
	 * 
	 * @param type $username
	 * @param type $password
	 */
	private function send_email($username, $password) {
		$email_content = file_get_contents($this->email_template);
		
		//Get users email, firstname and lastname
		$statement = "SELECT firstname, lastname, email
						FROM {$GLOBALS['db_prefix']}_members
						WHERE username=:username
						LIMIT 1";
		$sql_user = $GLOBALS['dbCon']->prepare($statement);
		$sql_user->bindParam(':username', $username);
		$sql_user->execute();
		$sql_user_data = $sql_user->fetch();
		$sql_user->closeCursor();
		
		
		//Replace tags
		$email_content = str_replace('<!-- username -->', $username, $email_content);
		$email_content = str_replace('<!-- firstname -->', $sql_user_data['firstname'], $email_content);
		$email_content = str_replace('<!-- lastname -->', $sql_user_data['lastname'], $email_content);
		$email_content = str_replace('<!-- password -->', $password, $email_content);
		echo $password;
		//Send email
		require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
		$mail = new PHPMailer();
		try {
			$mail->AddAddress($sql_user_data['email']);
			$mail->SetFrom('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
			$mail->AddReplyTo('webmaster@cv-in-telkom-poskantoor.co.za', 'The Christian Association of South Africa');
			$mail->Subject = $this->email_subject;
			$mail->MsgHTML($email_content);
			$mail->Send();
			return true;
		} catch (phpmailerException $e) {
			$this->error_massage = $e->errorMessage(); //Pretty error messages from PHPMailer
			return false;
		} catch (Exception $e) {
			$this->error_massage = $e->getMessage(); //Boring error messages from anything else!
			return false;
		}
	}
	
	/**
	 * Set the template to use to send the password recovery email
	 * @param type $template
	 * @return boolean
	 */
	function setEmailTemplate($template) {
		$this->email_template = $template;
		return true;
	}
	
	/**
	 * Set the email subject to use in emails
	 * @param type $template
	 * @return boolean
	 */
	function setEmailSubject($subject) {
		$this->email_subject = $subject;
		return true;
	}
	
}
?>
