<?php
ob_start();

include_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";



if(isset($_POST['pages']))
{
	function strippost($v,$t=0)
	{
		$v=str_replace('"',"'",$v);
		$v=str_replace("  "," ",$v);
		if($t)
		{
			$v=str_replace("\n"," ",$v);
			$v=strtolower($v);
		}
		$v=trim($v);
		$v=trim($v,"\n");
//		$v=mysql_real_escape_string($v);
		return $v;
	}

	foreach($_POST as $k => $v)
	{
		if(strpos($k,"i_")!==FALSE)
		{
			$i=substr($k,2);

			if(isset($_POST['r_'.$i]))
			{
				$sql="DELETE FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=".$i;
				mysql_query($sql);
				$sql="DELETE FROM {$GLOBALS['db_prefix']}_areas WHERE page_id=".$i;
				mysql_query($sql);
			}
			else
			{
				if(isset($_POST['h_'.$i]))
				{
					$sql="UPDATE {$GLOBALS['db_prefix']}_pages SET page_home=0";
					mysql_query($sql);
				}
				$sql="UPDATE {$GLOBALS['db_prefix']}_pages ";
				$sql.="SET ";
				$sql.="page_home='".($_POST['h']=="h_".$i?1:0)."'";
				$sql.=",page_members_only='".(isset($_POST['m_'.$i])?1:0)."'";
				$sql.=",page_title='".strippost($_POST['n_'.$i])."'";
				$sql.=",page_name='".str_replace(" ","_",str_replace("%20","_",strippost($_POST['l_'.$i])))."'";
				$sql.=",template_id='".$_POST['t_'.$i]."'";
				$sql.=",page_description='".strippost($_POST['d_'.$i])."'";
				$sql.=",page_keywords='".strippost($_POST['k_'.$i],1)."'";
				$sql.=" WHERE ";
				$sql.="page_id=".$v;
				//echo $sql."<hr>";
				mysql_query($sql);
				echo mysql_error();
			}
		}
	}

	echo "<script>";
	echo "P=window.parent";
	echo ";P.WR(".$_GET['WR'].")";
	echo ";P.WMN(".$_GET['WR'].",1)";
	echo ";P.WD(".$_GET['W'].")";
	echo "</script>";
	die();
}




function strip($v,$t=0)
{
	$v=str_replace("  "," ",$v);
	$v=str_replace("\n",";;",$v);
	$v=str_replace('"',"'",$v);
	if($t)
		$v=strtolower($v);
	$v=trim($v);
	return $v;
}

$pages="";
$sql="SELECT * FROM {$GLOBALS['db_prefix']}_pages ORDER BY page_title";
$rs=mysql_query($sql);
if(mysql_num_rows($rs))
{
	while($g=mysql_fetch_array($rs))
	{
		$pages.=$g['page_id'];
		$pages.="|".($g['page_home']?1:"");
		$pages.="|".($g['page_members_only']?1:"");
		$pages.="|".stripslashes($g['page_title']);
		$pages.="|".stripslashes($g['page_name']);
		$pages.="|".stripslashes($g['template_id']);
		$pages.="|".($g['page_description']?stripslashes(strip($g['page_description'])):"");
		$pages.="|".($g['page_keywords']?stripslashes(strip($g['page_keywords'])):"");
		$pages.="}";
	}
}

echo "<html>";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/admin_style.php";
echo $css;
echo "<script src=/shared/G.js></script>";
echo "<script src=pages.js></script>";
echo "<script>dop(\"".$pages."\")</script>";

ob_end_flush();
?>