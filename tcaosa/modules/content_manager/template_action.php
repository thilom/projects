<?php
ob_start();
require_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";

// clean up
function cleanup()
{
	$sql="SELECT template_id,template_file,template_name FROM {$GLOBALS['db_prefix']}_templates";
	$r1=mysql_query($sql);
	if(mysql_num_rows($r1))
	{
		while($g=mysql_fetch_array($r1))
		{
			$used=0;
			if(strpos($g['template_name'],"Swap: ")!==FALSE)
			{
				$sql="SELECT page_id FROM {$GLOBALS['db_prefix']}_pages WHERE template_id=".$g['template_id'];
				$r2=mysql_query($sql);
				if(mysql_num_rows($r2))
					$used=1;
				if(!$used)
				{
					$sql="DELETE FROM {$GLOBALS['db_prefix']}_templates WHERE template_id=".$g['template_id'];
					mysql_query($sql);
				}
			}
		}
	}
}

// swap
if(isset($_GET['sp']))
{
	$swap=$_GET['sp'];
	$backup=(isset($_GET['bu'])?$_GET['bu']:"");

	if($backup)
	{
		if(strpos($swap,".htm")!==TRUE)
		{
			if(file_exists($_SERVER['DOCUMENT_ROOT']."/templates/backup/".$swap.".html"))
				$swap=$swap.".html";
			elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/templates/backup/".$swap.".htm"))
				$swap=$swap.".htm";
			elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/templates/backup/".$swap.".php"))
				$swap=$swap.".php";
		}
		$sql="SELECT template_id FROM {$GLOBALS['db_prefix']}_templates WHERE template_file='backup/".$swap."'";
		$rs=mysql_query($sql);
		if(mysql_num_rows($rs))
		{
			$g=mysql_fetch_array($rs);
			$swap=$g['template_id'];
		}
		else
		{
			$sql="INSERT INTO {$GLOBALS['db_prefix']}_templates (template_file,template_name) VALUES ('backup/".$swap."','Swap: ".$swap."')";
			mysql_query($sql);
			$sql="SELECT template_id FROM {$GLOBALS['db_prefix']}_templates ORDER BY template_id DESC LIMIT 1";
			$r=mysql_query($sql);
			$g=mysql_fetch_array($r);
			$swap=$g['template_id'];
		}
	}

	$sql="UPDATE {$GLOBALS['db_prefix']}_pages SET template_id=".$swap." WHERE page_id=".$_GET['Pid'];
	mysql_query($sql);

	cleanup();

	$sql="SELECT page_name FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=".$_GET['Pid'];
	$r=mysql_query($sql);
	$g=mysql_fetch_array($r);

	echo "<script>";
	echo "var P=window.parent";
	echo ";P.WR(".(isset($_GET['WR'])?$_GET['WR']:"0,'/modules/content_manager/start.php'").")";
	echo ";setTimeout(\"location='/modules/content_manager/manager.php?page=".$g['page_name']."&Pid=".$_GET['Pid']."&Tid=".$swap."&W=".$_GET['W']."&WR=".$_GET['WR']."'\",900)";
	echo "</script>";

}

// delete backups
elseif(isset($_GET['db']))
{
	include_once $_SERVER['DOCUMENT_ROOT']."/shared/directory_delete.php";
	deleteDirectory($_SERVER['DOCUMENT_ROOT']."/templates/backup",TRUE);

	cleanup();

	$sql="SELECT page_name FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=".$_GET['Pid'];
	$r=mysql_query($sql);
	$g=mysql_fetch_array($r);

	echo "<script>";
	echo "var P=window.parent";
	echo ";P.WR(".(isset($_GET['WR'])?$_GET['WR']:"0,'/modules/content_manager/start.php'").")";
	echo ";P.CONTENT['TEMPLATE_BACKUPS']=''";
	echo ";setTimeout(\"location='/modules/content_manager/manager.php?page=".$g['page_name']."&Pid=".$_GET['Pid']."&Tid=".$_GET['Tid']."&W=".$_GET['W']."&WR=".$_GET['WR']."'\",900)";
	echo "</script>";
}

ob_end_flush();
?>