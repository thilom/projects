<?php
/** 
 * Allows users to select an area type for the areas they are currently editing.
 * The code traverses the directory, 'modules' looking for module.ini.php. It then looks for the
 * $content_type variable to create a list of selections.
 *
 * @version $Id: area_type.php 105 2011-11-05 11:52:04Z thilo $
 * @author Thilo Muller(2011)
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once 'functions.php';

//Style Sheets
echo '<link href="/shared/style.css" type="text/css" rel="stylesheet">';
echo '<link href="css/area_type.css" type="text/css" rel="stylesheet">';
echo '<script type="text/javascript" src="js/area_settings.js"></script>';

if (count($_POST) > 0) {
	settings_save();
} else {
	settings_form();
}

function settings_form() {

	//Vars
	$template = file_get_contents('html/area_type.html');
	$area_name = '';
	$area_types = '';
	$area_id = $_GET['Eid'];
	$page_id = $_GET['Pid'];

	//Get access rights
	$content_types = array();
	$module_data = get_module_variables();
//	print_r($module_data);
	foreach ($module_data as $module_name=>$data) {
//	$dir = $_SERVER['DOCUMENT_ROOT'] . '/modules/';
//	if (is_dir($dir)) {
//		$dh = opendir($dir);
//		if ($dh) {
//			while (($file = readdir($dh)) !== false) {
//				if (is_dir($dir . $file)) {
//					if ($file == '.' || $file == '..') continue;
//					$content_type = '';
//					if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
					if (isset($data['content_type'])) {
						foreach ($data['content_type'] as $type_id=>$content_data) {
							$content_types[$content_data['id'].'|'.$data['module_id']]['module'] = $module_name;
							$content_types[$content_data['id'].'|'.$data['module_id']]['name'] = $content_data['name'];
							$content_types[$content_data['id'].'|'.$data['module_id']]['explanation'] = $content_data['explanation'];
						}
					}
//				}
//			}
//			closedir($dh);
//		}
//	}
	}

	//Get current area data
	$statement = "SELECT title, type, module FROM {$GLOBALS['db_prefix']}_areas_editing WHERE id=:id LIMIT 1";
	$sql_editing = $GLOBALS['dbCon']->prepare($statement);
	$sql_editing->bindParam(":id", $area_id);
	$sql_editing->execute();
	if ($sql_editing->rowCount() == 0) {
		$sql_editing->closeCursor();
		$statement = "SELECT title, type, module FROM {$GLOBALS['db_prefix']}_areas WHERE id=:id LIMIT 1";
		$sql_current = $GLOBALS['dbCon']->prepare($statement);
		$sql_current->bindParam(":id", $area_id);
		$sql_current->execute();
		$sql_data = $sql_current->fetchAll();
		$sql_current->closeCursor();
	} else {
		$sql_data = $sql_editing->fetchAll();
		$sql_editing->closeCursor();
	}
	$area_name = $sql_data[0]['title'];
	$area_type = "{$sql_data[0]['type']}|{$sql_data[0]['module']}";

	$current_module = '';
	$counter = 0;
	foreach ($content_types as $type_id=>$data) {
		if ($data['module'] != $current_module) {
			$module_name = str_replace('_', ' ', $data['module']);
			if ($counter != 0) $area_types .= "</td></tr></table>";
			$area_types .= "<table class='moduleTable' ><tr><td class='moduleName' onClick=toggleList('{$data['module']}')>$module_name<td></tr><tr id='{$data['module']}'  class='hidden'><td >";
			$current_module = $data['module'];
			$counter++;
		}
		$area_types .= "<table class='W100 collapsed' >";
		$area_types .= "<tr><td class='W10p'><input type='radio' name='area_types' value='$type_id' id='$type_id'";
		$area_types .= $type_id==$area_type?" checked":'';
		$area_types .= "></td>";
		$area_types .= "<td><label for='$type_id'>{$data['name']}</label></td></tr>";
		if (!empty($data['explanation'])) $area_types .= "<tr><td>&nbsp;</td><td class='pb'><label for='$type_id'><tt>{$data['explanation']}</tt></label></td></tr>";
		$area_types .= "</table>";
		$area_types .= $type_id==$area_type?" <script>toggleList('$current_module')</script>":'';
	}

	//Replace Tags
	$template = str_replace('!area_name!', $area_name, $template);
	$template = str_replace('<!-- area_type -->', $area_types, $template);
	$template = str_replace('!Pid!', $page_id, $template);
	$template = str_replace('!Eid!', $area_id, $template);
	$template = str_replace('!W!', $_GET['W'], $template);
	$template = str_replace('!Aid!', $_GET['Aid'], $template);


	echo $template;
}

function settings_save() {

	//Vars
	$area_name = $_POST['area_name'];
	$area_id = $_GET['Eid'];
	$page_id = $_GET['Pid'];
	list($area_type, $module) = explode('|', $_POST['area_types']);
	$message = '';

	//Get live data & create if it doesn't exist
	$statement = "SELECT id
					FROM {$GLOBALS['db_prefix']}_areas 
					WHERE id=:id
					LIMIT 1";
	$sql_find = $GLOBALS['dbCon']->prepare($statement);
	$sql_find->bindParam(':id', $area_id);
	$sql_find->execute();
	$find_data = $sql_find->fetch();
	$sql_find->closeCursor();
	
	
	if (empty($find_data)) {
		$id = next_id("{$GLOBALS['db_prefix']}_areas", 'id');
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas 
						(id, area_id, page_id, type, module)
						VALUES
						(:id, :area_id, :page_id, :type, :module)";
		$sql_create = $GLOBALS['dbCon']->prepare($statement);
		$sql_create->bindParam(':id', $id);
		$sql_create->bindParam(':area_id', $area_id);
		$sql_create->bindParam(':page_id', $page_id);
		$sql_create->bindParam(':type', $area_type);
		$sql_create->bindParam(':module', $module);
		$sql_create->execute();
		$sql_create->closeCursor();
	} else {
		$id = $find_data['id'];
	}
	
	//Get current area data
	$statement = "SELECT title, type, module FROM {$GLOBALS['db_prefix']}_areas_editing WHERE id=:id LIMIT 1";
	$sql_editing = $GLOBALS['dbCon']->prepare($statement);
	$sql_editing->bindParam(":id", $id);
	$sql_editing->execute();
	if ($sql_editing->rowCount() == 0) {
		copy_area($id, "{$GLOBALS['db_prefix']}_areas", "{$GLOBALS['db_prefix']}_areas_editing");
	}
	$sql_editing->execute();
	$sql_data = $sql_editing->fetchAll();
	$sql_editing->closeCursor();

	//Update area name (if changed)
	if ($sql_data[0]['title'] != $area_name) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_areas_editing SET title=:title WHERE id=:id";
		$sql_title_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_title_update->bindParam(':title', $area_name);
		$sql_title_update->bindParam(':id', $id);
		$sql_title_update->execute();
		$sql_title_update->closeCursor();
		$message .= "Area name updated to '$area_name'";

		write_log("Area name updated to '$area_name'", 'content_manager', $id);
	}

	//Update area Type (if changed)
	if ($sql_data[0]['type'] != $area_type) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_areas_editing SET module=:module, type=:type WHERE id=:id";
		$sql_type_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_type_update->bindParam(':module', $module);
		$sql_type_update->bindParam(':type', $area_type);
		$sql_type_update->bindParam(':id', $id);
		$sql_type_update->execute();
		$sql_type_update->closeCursor();

		$message .= "Area type updated to: $area_type ($module)<br>";

		write_log("Area type updated to: $area_type ($module)", 'content_manager', $id);
	}

	if (empty($message)) $message = 'No Changes, Nothing to update';
	echo "<div class='dMsg1' width=200px>$message</div>";
	echo "<div class='bMsg1'><input type=button value='Continue' class=ok_button onClick=\"close_settings('$page_id','$area_id','{$_GET['W']}','{$_GET['Aid']}','{$_GET['WR']}')\" ></div>";
}
?>
