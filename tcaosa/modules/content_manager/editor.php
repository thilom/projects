<?php

//error_reporting(E_ALL);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors',1);

include_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";
include_once "module.ini.php";
include_once SITE_ROOT . "/shared/database_functions.php";
include_once "functions.php";

//Vars
$area_id = isset($_GET['Eid'])?$_GET['Eid']:0;
$area_id_name = isset($_GET['Aid'])?$_GET['Aid']:'';
$page_id = isset($_GET['Pid'])?$_GET['Pid']:'';



//Get area data
if ($area_id != 0) {
	//Does the area exist
	$statement = "SELECT count(*) AS area_count
					FROM {$GLOBALS['db_prefix']}_areas
					WHERE id=:id";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bindParam(':id', $area_id);
	$sql_check->execute();
	$sql_check_data = $sql_check->fetch();
	$sql_check->closeCursor();

	if ($sql_check_data['area_count'] == 0) {
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas
						(id, area_id, page_id)
						VALUES
						(:id, :area_id, :page_id)";
		$sql_create = $GLOBALS['dbCon']->prepare($statement);
		$sql_create->bindParam(':id', $area_id);
		$sql_create->bindParam(':area_id', $area_id_name);
		$sql_create->bindParam(':page_id', $page_id);
		$sql_create->execute();
		$sql_create->closeCursor();
	}

	$area_data = get_area_data($area_id);
} else {
	$area_id = next_id("{$GLOBALS['db_prefix']}_areas", 'id');
	$title = "Area " . date('d/m/Y H:ia');

	//Insert new area
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas (id, page_id, area_id, title, type, module,header) VALUES (:id, :page_id, :area_id,:title,'text','content_manager','')";
	$sql_new_area = $GLOBALS['dbCon']->prepare($statement);
	$sql_new_area->bindParam(':id', $area_id);
	$sql_new_area->bindParam(':area_id', $area_id_name);
	$sql_new_area->bindParam(':page_id', $page_id);
	$sql_new_area->bindParam(':title', $title);
	$sql_new_area->execute();
	$sql_new_area->closeCursor();

	//Fetch Again
	$area_data = get_area_data($area_id);
}

//Update area to 'text' if no area is specified
if (empty($area_data['data']['type'])) {
	$statement = "UPDATE {$GLOBALS['db_prefix']}_areas SET type='text', module='content_manager' WHERE id=:id";
	$sql_update_area = $GLOBALS['dbCon']->prepare($statement);
	$sql_update_area->bindParam(':id', $area_id);
	$sql_update_area->execute();
	$sql_update_area->closeCursor();

	//Fetch Again
	$area_data = get_area_data($area_id);
}


echo "<html>";
echo "<script src=/shared/G.js></script>";
//echo "<script src=/shared/tabs.js></script>";
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/admin_style.php";
echo $css;

//Find include files
$mod_id = '';
$content_file = '';
$module_vars = get_module_variables();
foreach ($module_vars as $data) {
	if (isset($data['content_type'])) {
		foreach($data['content_type'] as $types) {
			if ($types['id'] == $area_data['data']['type']) {
				$mod_id = $data['module_id'];
				$content_file = isset($types['content_file'])?$types['content_file']:'No File Found';
				break;
			}
		}
	}
}

$submitted_by = isset($area_data['data']['submit_by'])?$area_data['data']['submit_by']:0;
$toolbar_mode = get_toolbar_mode($page_id, $area_id, $submitted_by);
$toolbar_switch = get_toolbar_bmode($area_id);

include 'toolbar.php';
include $_SERVER['DOCUMENT_ROOT'] . "/modules/$mod_id/$content_file";



?>