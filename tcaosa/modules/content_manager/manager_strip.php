<?php

function string_strip($v="")
{

	if($v)
	{
		$v=str_replace(" />",">",$v);
		$v=str_replace("/>",">",$v);
		$v=str_replace("   "," ",$v);
		$v=str_replace("  "," ",$v);
		$v=str_replace("\v","",$v);
		$v=str_replace("\n\n\n\n","\n\n",$v);
		$v=str_replace("\n\n\n","\n\n",$v);
		$v=str_replace("\r","",$v);
		$v=str_replace(";}","}",$v);
		$v=str_replace(';"','"',$v);
		$v=str_ireplace(' alt=""',"",$v);
		$v=str_ireplace(" alt=''","",$v);

		preg_match_all('@<([a-zA-Z]+)@i',$v,$match);
		foreach($match[1] as $t)
		{
			$v = preg_replace('@<'.$t.'@i',strtolower("<$t"),$v);
			$v = preg_replace('@</'.$t.'>@',strtolower("</$t>"),$v);
		}

		$v=str_ireplace("href","href",$v);
		$v=str_ireplace("href =","href=",$v);
		$v=str_ireplace("href= ","href=",$v);
		$v=str_ireplace("href=\"javascript:void(0)\"","href=javascript:void(0)",$v);
		$v=str_ireplace("href='javascript:void(0)'","href=javascript:void(0)",$v);

		$v=str_ireplace("name =","name=",$v);
		$v=str_ireplace("name =","name=",$v);
		$v=str_ireplace("name= ","name=",$v);

		$v=str_ireplace("target =","target=",$v);
		$v=str_ireplace("target =","target=",$v);
		$v=str_ireplace("target= ","target=",$v);

		$v=str_ireplace("lang=","lang=",$v);
		$v=str_ireplace("lang =","lang=",$v);
		$v=str_ireplace("lang= ","lang=",$v);

		$v=str_ireplace("id=","id=",$v);
		$v=str_ireplace("id =","id=",$v);
		$v=str_ireplace("id= ","id=",$v);

		$v=str_ireplace("onmouseover=","onmouseover=",$v);
		$v=str_ireplace("onmouseover =","onmouseover=",$v);
		$v=str_ireplace("onmouseover= ","onmouseover=",$v);

		$v=str_ireplace("onmouseout=","onmouseout=",$v);
		$v=str_ireplace("onmouseout =","onmouseout=",$v);
		$v=str_ireplace("onmouseout= ","onmouseout=",$v);

		$v=str_ireplace(" type=\"text/javascript\"","",$v);
		$v=str_ireplace(" type='text/javascript'","",$v);
		$v=str_ireplace(" type=text/javascript","",$v);

		$v=str_ireplace(" language=\"javascript\"","",$v);
		$v=str_ireplace(" language='javascript'","",$v);
		$v=str_ireplace(" language=javascript","",$v);

		$v=str_ireplace(" cellspacing='0'"," cellspacing=0",$v);
		$v=str_ireplace(" cellspacing=\"0\""," cellspacing=0",$v);

		$v=str_ireplace(" cellpadding='0'"," cellpadding=0",$v);
		$v=str_ireplace(" cellpadding=\"0\""," cellpadding=0",$v);

		$v=str_ireplace(" border='0'"," border=0",$v);
		$v=str_ireplace(" border=\"0\""," border=0",$v);

		$v=str_ireplace(" valign='top'"," valign=top",$v);
		$v=str_ireplace(" valign=\"top\""," valign=top",$v);

		$v=str_ireplace(" width='100%'"," width=100%",$v);
		$v=str_ireplace(" width=\"100%\""," width=100%",$v);

		$v=str_ireplace(" align='right'"," align=right",$v);
		$v=str_ireplace(" align=\"right\""," align=right",$v);
	}

	return $v;
}

?>