<?php
/**
 * CSS Editor for content manager.
 *
 * @version $Id: css_editor.php 69 2011-06-17 14:18:33Z thilo $
 * @author Thilo Muller(2011)
 */


include_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";
include_once "functions.php";

//Vars
$area_id = isset($_GET['Eid'])?$_GET['Eid']:0;
$area_id_name = isset($_GET['Aid'])?$_GET['Aid']:'';
$page_id = isset($_GET['Pid'])?$_GET['Pid']:'';

//Prepare statement - Edit data
$statement = "SELECT area_id, title, type, module FROM {$GLOBALS['db_prefix']}_areas_editing WHERE id=:id LIMIT 1";
$sql_editing = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Current Data
$statement = "SELECT area_id, title, type, module FROM {$GLOBALS['db_prefix']}_areas WHERE id=:id LIMIT 1";
$sql_current = $GLOBALS['dbCon']->prepare($statement);

//Get area data
if ($area_id != 0) {
	$sql_editing->bindParam(":id", $area_id);
	$sql_editing->execute();
	if ($sql_editing->rowCount() == 0) {
		$sql_current->bindParam(":id", $area_id);
		$sql_current->execute();
		$sql_data = $sql_current->fetchAll();
	} else {
		$sql_data = $sql_editing->fetchAll();
	}
}


if (!isset($sql_data[0]['type'])) {
	$_GET['Eid'] = next_id("{$GLOBALS['db_prefix']}_areas", 'id');
	$title = "Area " . date('d/m/Y H:ia');
	//Insert new area
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas (id, page_id, area_id, title, type, module,header) VALUES (:id, :page_id, :area_id,:title,'text','content_manager','')";
	$sql_new_area = $GLOBALS['dbCon']->prepare($statement);
	$sql_new_area->bindParam(':id', $_GET['Eid']);
	$sql_new_area->bindParam(':area_id', $area_id_name);
	$sql_new_area->bindParam(':page_id', $page_id);
	$sql_new_area->bindParam(':title', $title);
	$sql_new_area->execute();
	$sql_new_area->closeCursor();

	//Fetch Again
	$sql_current->bindParam(":id", $_GET['Eid']);
	$sql_current->execute();
	$sql_data = $sql_current->fetchAll();
}

//Update area to 'text' if no area is specified
if (empty($sql_data[0]['type'])) {
	$statement = "UPDATE {$GLOBALS['db_prefix']}_areas SET type='text', module='content_manager' WHERE id=:id";
	$sql_update_area = $GLOBALS['dbCon']->prepare($statement);
	$sql_update_area->bindParam(':id', $_GET['Eid']);
	$sql_update_area->execute();
	$sql_update_area->closeCursor();

	//Fetch Again
	$sql_current->bindParam(":id", $_GET['Eid']);
	$sql_current->execute();
	$sql_data = $sql_current->fetchAll();
	$sql_current->closeCursor();
}


echo "<html>";
echo "<script src=/shared/G.js></script>";
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/admin_style.php";
echo $css;

$area_data = get_area_data($area_id);
$submitted_by = isset($area_data['data']['submit_by'])?$area_data['data']['submit_by']:0;
$toolbar_mode = get_toolbar_mode($page_id, $area_id, $submitted_by);
$toolbar_switch = get_toolbar_bmode($area_id);
include 'toolbar.php';

//Find include files
$content_file = '';
$module_vars = get_module_variables();
foreach ($module_vars as $module_id=>$data) {
	if (isset($data['content_type'])) {
		foreach($data['content_type'] as $types) {
			if ($types['id'] == $sql_data[0]['type']) {
				$mod_id = $module_id;
				$content_file = isset($types['css_file'])?$types['css_file']:'No File Found';
				break;
			}
		}
	}
}

include $_SERVER['DOCUMENT_ROOT'] . "/modules/$mod_id/$content_file";




?>


