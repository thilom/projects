<?php

/**
 *  List rollbacks available for the elected area. 
 *  Re-instate a selected history item
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: rollback.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
require_once '../../settings/init.php' ;
require_once SITE_ROOT . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents(SITE_ROOT . '/modules/content_manager/html/rollback.html');
$history_list = '';

if (isset($_GET['rollback'])) {
	$rollback_id = $_GET['rollback'];
	$rollback_date = $_GET['date'];
	
	//Get rollback data
	$statement = "SELECT area_id, title, page_id, type, module, header, preferences, history_date
					FROM {$GLOBALS['db_prefix']}_areas_history
					WHERE id=:rollback_id AND history_date=:rollback_date
					LIMIT 1";
	$sql_history = $GLOBALS['dbCon']->prepare($statement);
	$sql_history->bindParam(':rollback_id', $rollback_id);
	$sql_history->bindParam(':rollback_date', $rollback_date);
	$sql_history->execute();
	$sql_history_data = $sql_history->fetch();
	$sql_history->closeCursor();
	
	//Assemble rollback Data
	$rollback_data['area_id'] = $sql_history_data['area_id']; 
	$rollback_data['title'] = $sql_history_data['title']; 
	$rollback_data['page_id'] = $sql_history_data['page_id']; 
	$rollback_data['type'] = $sql_history_data['type']; 
	$rollback_data['module'] = $sql_history_data['module']; 
	$rollback_data['header'] = $sql_history_data['header']; 
	$rollback_data['preferences'] = $sql_history_data['preferences']; 

	//Copy to current edit table
	update_area_data($rollback_id, 'draft', $rollback_data);
	
	$message = "&#187; Area rolledback to {$sql_history_data['history_date']}<br>";
	write_log("Area rolledback to {$sql_history_data['history_date']}",'content_manager', $rollback_id);

	echo "<script type='text/javascript' src='/modules/content_manager/js/rollback.js'></script>";
	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=close_edit({$_GET['W']},'{$sql_history_data['title']}','{$_GET['Pid']}','{$_GET['Aid']}','{$_GET['WR']}','$rollback_id') ></div>";

	
	die();
}

//Get rollback list
$statement = "SELECT history_date, module FROM {$GLOBALS['db_prefix']}_areas_history WHERE id=:id ORDER BY history_date DESC";
$sql_history = $GLOBALS['dbCon']->prepare($statement);
$sql_history->bindParam(':id', $_GET['Eid']);
$sql_history->execute();
$sql_history_data = $sql_history->fetchAll();
$sql_history->closeCursor();

foreach ($sql_history_data as $history_data) {
	$module = ucwords(str_replace('_', ' ', $history_data['module']));
	$history_list .= "<tr><td>{$history_data['history_date']}</td><td>$module</td><td style='text-align: right'><input type=button value='Rollback' class='undo_button' onclick=\"rollback('{$_GET['Eid']}','{$history_data['history_date']}','{$_GET['WR']}','{$_GET['Aid']}','{$_GET['Pid']}','{$_GET['W']}')\" ></td></tr>";
}

//Replace Tags
$template = str_replace('<!-- history_list -->', $history_list, $template);

echo $template;

?>
