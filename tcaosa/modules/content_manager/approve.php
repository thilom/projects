<?php
/** 
 *  Approve areas for publication on the website
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: approve.php 87 2011-09-14 06:45:49Z thilo $
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php';

if ((isset($_GET['submit']))) submit_area();
if ((isset($_GET['approve']))) approve_area();
if ((isset($_GET['reject']))) reject_area();
if ((isset($_GET['golive']))) golive_area();


function golive_area() {
	//Get current user name
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) AS username FROM {$GLOBALS['db_prefix']}_user WHERE user_id=:user_id LIMIT 1";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bindParam(':user_id', $_SESSION['dbweb_user_id']);
	$sql_user->execute();
	$sql_user_data = $sql_user->fetch();
	$sql_user->closeCursor();
	
	copy_area($_GET['golive'], "{$GLOBALS['db_prefix']}_areas_editing", "{$GLOBALS['db_prefix']}_areas_history", FALSE, TRUE);
	copy_area($_GET['golive'], "{$GLOBALS['db_prefix']}_areas_editing", "{$GLOBALS['db_prefix']}_areas", TRUE, TRUE);

	$message = "&#187; Text area saved to live";
	write_log("Content saved to live by {$sql_user_data['username']}",'content_manager', $_GET['golive']);
	
	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"document.location='editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&WR={$_GET['WR']}&Eid={$_GET['golive']}&W={$_GET['W']}'\" ></div>";

}

/**
 * Copy area data to the pending table
 */
function submit_area() {
	copy_area($_GET['submit'], "{$GLOBALS['db_prefix']}_areas_editing", "{$GLOBALS['db_prefix']}_areas_pending", TRUE, TRUE, $_SESSION['dbweb_user_id']);

	$message = "&#187; Content Sent for Approval";

	write_log("Content Send for Approval",'content_manager', $_GET['submit']);

	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"document.location='editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&WR={$_GET['WR']}&Eid={$_GET['submit']}&W={$_GET['W']}'\" ></div>";
}

/**
 * Approve area data and go live if all approvers have approved it
 */
function approve_area() {
	//Get current user name
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) AS username FROM {$GLOBALS['db_prefix']}_user WHERE user_id=:user_id LIMIT 1";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bindParam(':user_id', $_SESSION['dbweb_user_id']);
	$sql_user->execute();
	$sql_user_data = $sql_user->fetch();
	$sql_user->closeCursor();

	if ($GLOBALS['settings']['approval_system'] == 'multi') {
		//Find out who submitted it
		$statement = "SELECT submit_by FROM {$GLOBALS['db_prefix']}_areas_pending WHERE id=:id LIMIT 1";
		$sql_submit_by = $GLOBALS['dbCon']->prepare($statement);
		$sql_submit_by->bindParam(':id', $_GET['approve']);
		$sql_submit_by->execute();
		$submited = $sql_submit_by->fetch();
		$sql_submit_by->closeCursor();

		//Find out who must approve it
		$statement = "SELECT approved_by FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id";
		$sql_approval_list = $GLOBALS['dbCon']->prepare($statement);
		$sql_approval_list->bindParam(':user_id', $submited['submit_by']);
		$sql_approval_list->execute();
		$approval_list = $sql_approval_list->fetchAll();
		$sql_approval_list->closeCursor();
		foreach ($approval_list as $data) {
			$approvers[] = $data['approved_by'];
		}

		//Find out who has approved it so far
		$statement = "SELECT approver_id FROM {$GLOBALS['db_prefix']}_areas_approved WHERE area_id=:area_id";
		$sql_approved = $GLOBALS['dbCon']->prepare($statement);
		$sql_approved->bindParam(':area_id', $_GET['approve']);
		$sql_approved->execute();
		$approved = $sql_approved->fetchAll();
		$sql_approved->closeCursor();
		foreach ($approved as $data) {
			$key = array_search($data['approver_id'], $approvers);
			if ($key !== FALSE) unset($approvers[$key]);
		}

		//Remove current user from approvers list
		$key = array_search($_SESSION['dbweb_user_id'], $approvers);
		if ($key !== FALSE) unset($approvers[$key]);

		//Go live if all have approved it
		if (count($approvers) == 0) {
			copy_area($_GET['approve'], "{$GLOBALS['db_prefix']}_areas_pending", "{$GLOBALS['db_prefix']}_areas_history", FALSE, TRUE);
			copy_area($_GET['approve'], "{$GLOBALS['db_prefix']}_areas_pending", "{$GLOBALS['db_prefix']}_areas", TRUE,TRUE);

			$statement = "DELETE FROM {$GLOBALS['db_prefix']}_areas_pending WHERE id=:id";
			$sql_area_delete = $GLOBALS['dbCon']->prepare($statement);
			$sql_area_delete->bindParam(':id', $_GET['approve']);
			$sql_area_delete->execute();
			$sql_area_delete->closeCursor();

			$message = "&#187; Update approved for this area<br>";
			write_log("Content Update Aproved by {$sql_user_data['username']}",'content_manager', $_GET['approve']);

			$message .= "&#187; Updated content live";
			write_log("Updated content live (all approved)",'content_manager', $_GET['approve']);

			//Clear approved list
			$statement = "DELETE FROM {$GLOBALS['db_prefix']}_areas_approved WHERE area_id=:area_id";
			$sql_clear = $GLOBALS['dbCon']->prepare($statement);
			$sql_clear->bindParam(':area_id', $_GET['approve']);
			$sql_clear->execute();
			$sql_clear->closeCursor();

		} else {
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas_approved (area_id, approver_id, approved_date) VALUES (:area_id, :approver_id, NOW())";
			$sql_approver_insert = $GLOBALS['dbCon']->prepare($statement);
			$sql_approver_insert->bindParam(':area_id', $_GET['approve']);
			$sql_approver_insert->bindParam(':approver_id', $_SESSION['dbweb_user_id']);
			$sql_approver_insert->execute();

			$message = "&#187; Update approved for this area";
			write_log("Content Update Aproved by {$sql_user_data['username']}",'content_manager', $_GET['approve']);
		}
	} else {
		copy_area($_GET['approve'], "{$GLOBALS['db_prefix']}_areas_pending", "{$GLOBALS['db_prefix']}_areas_history", FALSE, TRUE);
		copy_area($_GET['approve'], "{$GLOBALS['db_prefix']}_areas_pending", "{$GLOBALS['db_prefix']}_areas", TRUE, TRUE);

		$message = "&#187; Update approved for this area";
		write_log("Content Update Aproved by {$sql_user_data['username']}",'content_manager', $_GET['approve']);
	}
	
	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"document.location='editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&WR={$_GET['WR']}&Eid={$_GET['submit']}&W={$_GET['W']}'\" ></div>";
}

/**
 * Remove a pending area
 */
function reject_area() {
	//Get current user name
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) AS username FROM {$GLOBALS['db_prefix']}_user WHERE user_id=:user_id LIMIT 1";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bindParam(':user_id', $_SESSION['dbweb_user_id']);
	$sql_user->execute();
	$sql_user_data = $sql_user->fetch();
	$sql_user->closeCursor();
	
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_areas_pending WHERE id=:id";
	$sql_area_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_area_delete->bindParam(':id', $_GET['reject']);
	$sql_area_delete->execute();
	$sql_area_delete->closeCursor();

	$message = "&#187; Update declined for this area";
	write_log("Update Declined by {$sql_user_data['username']}",'content_manager', $_GET['reject']);
	
	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"document.location='editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&WR={$_GET['WR']}&Eid={$_GET['reject']}&W={$_GET['W']}'\" ></div>";

}
?>
