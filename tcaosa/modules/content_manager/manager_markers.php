<?php
$mark="";

function mark_string($v="",$tag="TMP")
{
	global $mark;
	if($v)
	{
		$mark=$v;
		$mark=str_replace(">"," >",$mark);
		$mark=explode(">",$mark);

		$o="";
		$i=0;
		while(isset($mark[$i]))
		{

			if(
			!isset($_POST['EDIT_post'])
			&&
			stripos($mark[$i],"<area")===FALSE
			&&
			stripos($mark[$i],"<link")===FALSE
			&&
			stripos($mark[$i],"<a")!==FALSE
			&&
			stripos($mark[$i],"href=")>1
			)
			{
				$href=substr($mark[$i],stripos($mark[$i],"href=")+5);
				$href=substr($href,0,strpos($href," "));
				$href=str_replace(" ","",$href);
				$href=str_replace("'","",$href);
				$href=str_replace('"',"",$href);
				if($href && $href!="javascript:void(0)")
				{
					$href=trim($href);
					if(strpos($href,"modules/content_manager/manager.php")!==FALSE || strpos($href,"index.php")!==FALSE)
					{
						$href=trim($href,"/");
						$href=str_replace("modules/content_manager/manager.php","index.php",$href);
						$href="/".$href;
					}
					$mark[$i].=" rev=\"".$href."\"";
				}
			}

			if(
			$mark[$i]==""
			||
			strpos($mark[$i],"</")!==FALSE
			||
			strpos($mark[$i],"<!")!==FALSE
			||
			stripos($mark[$i]," id=")!==FALSE
			||
			stripos($mark[$i],"<abbr")!==FALSE
			||
			stripos($mark[$i],"<applet")!==FALSE
			||
			stripos($mark[$i],"<address")!==FALSE
			||
			stripos($mark[$i],"<area")!==FALSE
			||
			stripos($mark[$i],"<body")!==FALSE
			||
			stripos($mark[$i],"<bgsound")!==FALSE
			||
			stripos($mark[$i],"<br")!==FALSE
			||
			stripos($mark[$i],"<colgroup")!==FALSE
			||
			stripos($mark[$i],"<embed")!==FALSE
			||
			stripos($mark[$i],"<form")!==FALSE
			||
			stripos($mark[$i],"<head")!==FALSE
			||
			stripos($mark[$i],"<html")!==FALSE
			||
			stripos($mark[$i],"<hr")!==FALSE
			||
			stripos($mark[$i],"<iframe")!==FALSE
			||
			stripos($mark[$i],"<link")!==FALSE
			||
			stripos($mark[$i],"<map")!==FALSE
			||
			stripos($mark[$i],"<meta")!==FALSE
			||
			stripos($mark[$i],"<object")!==FALSE
			||
			stripos($mark[$i],"<option")!==FALSE
			||
			stripos($mark[$i],"<optgroup")!==FALSE
			||
			stripos($mark[$i],"<param")!==FALSE
			||
			stripos($mark[$i],"<script")!==FALSE
			||
			stripos($mark[$i],"<style")!==FALSE
			||
			stripos($mark[$i],"<tbody")!==FALSE
			||
			stripos($mark[$i],"<thead")!==FALSE
			||
			stripos($mark[$i],"<tfoot")!==FALSE
			||
			stripos($mark[$i],"<title")!==FALSE
			||
			stripos($mark[$i],"<tr")!==FALSE
			||
			stripos($mark[$i],"<xml")!==FALSE
			||
			stripos($mark[$i],"<xmp")!==FALSE
			)
			{}
			elseif(strpos($mark[$i],"<")!==FALSE)
				$mark[$i].=" id=".$tag."_".$i;
			$o.=$mark[$i];
			if(strpos($mark[$i],"<")!==FALSE)
				$o.=">";
			$i++;
		}

		$o=str_replace("  "," ",$o);
		$o=str_replace(" >",">",$o);
		$o=str_replace(">  <","><",$o);
		$o=str_replace("> <","><",$o);
		$o=str_replace(">\n>",">",$o);
		$o=str_replace(">>",">",$o);
		$o=str_replace(" <","<",$o);
		$v=$o;

		$i--;
		if(strpos($v,"> id=".$tag."_".$i))
			$v=substr($v,0,strpos($v,"> id=".$tag."_".$i)+1);
			
		if($tag=="TMP")
		{
			if(stripos($v,"</html")!==TRUE)
				$v=$v."</html>";
			if(stripos($v,"</body")!==TRUE)
				$v=str_replace("</html","</body></html",$v);
		}

	}

	return $v;
}
?>