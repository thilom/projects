MENUs=MENUs.split('|')
MENU_new=new Array
MENU_cap=new Array
MENU_cap_A=new Array
MENU_chi=new Array
MENU_chi_A=""
MENU_act=0

function MENU_capture(v)
{
	if(!MENU_cap_A[E_O])
	{
		MENU_cap_A[E_O]=1
		var n=MENU_cap.length
		MENU_cap[n]=new Array
		MENU_cap[n][0]=(v?v:"")
		MENU_cap[n][1]=E_O.tagName.toLowerCase()
		MENU_cap[n][2]=(E_O.id?E_O.id:"")
		MENU_cap[n][3]=(E_O.name?E_O.name:"")
		MENU_cap[n][4]=(E_O.src?E_O.src.substr(E_O.src.lastIndexOf("/")+1):"")
		MENU_cap[n][5]=(E_O.rev?E_O.rev:"")
		MENU_cap[n][6]=(E_O.value?E_O.value:"")
		MENU_cap[n][7]=(E_O.className?E_O.className:"")
		MENU_cap[n][8]=EDIT_getArea()
	}
}

function MENU_chiad(d)
{
	if(MENU_chi_A.indexOf(d+",")<0)
	{
		MENU_chi_A=MENU_chi_A+d+","
		MENU_chi_A[d]=1
		MENU_chi[MENU_chi.length]=d
	}
}

function MENU_save()
{
	if(MENU_act)
	{
		var c=""
		if(MENU_cap.length)
		{
			var i=0
			while(MENU_cap[i])
			{
				c+=MENU_cap[i][0]+"|"
				c+=MENU_cap[i][1]+"|"
				c+=MENU_cap[i][2]+"|"
				c+=MENU_cap[i][3]+"|"
				c+=MENU_cap[i][4]+"|"
				c+=MENU_cap[i][5]+"|"
				c+=MENU_cap[i][6]+"|"
				c+=MENU_cap[i][7]+"|"
				c+=MENU_cap[i][8]+"}"
				i++
			}
		}

		var h=""

		if(MENU_chi.length)
		{
			var q=""
			var i=0
			while(MENU_chi[i])
			{
				var m=(MENU_chi[i].indexOf("MENU_")<0?document.getElementById("MENU"+MENU_chi[i]):document.getElementById(MENU_chi[i]))
				h+=EDIT_getArea(m)+"."+m.lang+"!["
				var p=m.getElementsByTagName("P")
				var a=""
				var j=0
				while(p[j])
				{
					a=p[j].getElementsByTagName("A")
					if(a.length)
					{
						h+=p[j].id+"{"
						var k=0
						while(a[k])
						{
							h+=(a[k].id?a[k].id:"")+"|"
							h+=(a[k].innerHTML?a[k].innerHTML.replace(/{/g,"(").replace(/}/g,")"):"")+"|"
							h+=(a[k].lang?a[k].lang:"")+"|"
							h+=(!a[k].rev||a[k].rev=="javascript:void(0)"?"":a[k].rev)+"|"
							h+=(a[k].target?a[k].target.replace(/{/g,"(").replace(/}/g,")"):"")
							h+="^"
							k++
						}
						h+="}"
					}
					j++
				}
				h+="]!"
				i++
			}
		}

		EDIT_sv+=(c?"<textarea name=MENU_cap>"+c+"</textarea>":"")
		EDIT_sv+=(h?"<textarea name=MENU_link>"+h+"</textarea>":"")
		EDIT_sv+="<input type=text name=MENU_style value=\""+MENUs[0]+"|"+MENUs[1]+"|"+MENUs[2]+"|"+MENUs[3]+"|"+MENUs[4]+"\">"
		MENU_cap=new Array
		MENU_cap_A=new Array
		MENU_chi=new Array
		MENU_chi_A=""
		MENU_act=0
	}
}

function MENU_tag(v)
{
	var t=v.tagName.toUpperCase()
	if(v.id=="EDIT_AREA"||t=="BODY"||t=="INPUT"||t=="SELECT"||t=="BUTTON"||t=="TEXTAREA"||t=="HR"||t=="OBJECT"||t=="SCRIPT")
	{
		p=v.parentNode.tagName.toUpperCase()
		if(v.id!="EDIT_AREA"&&p!="BODY"&&p!="INPUT"&&p!="SELECT"&&p!="BUTTON"&&p!="TEXTAREA"&&p!="HR"&&p!="OBJECT"&&p!="SCRIPT")
		{
			if(confirm("WARNING!\nIt is not advisable to attach a Menu on <"+t+"> tags!\nWould you like to attach the Menu on this tags <"+p+"> parent instead?"))
				v=v.parentNode
			else
				v=0
		}
		else
		{
			v=0
			m0de_parent=0
			alert("WARNING!\nIncompatible <"+t+"> tag selected!\nTry again...")
		}
	}
	return v
}

function MENU_start()
{
	MENU_close()
	var ml=document.getElementById('MENU_link')
	var mc=document.getElementById('MENU_children')
	ml.style.top=y
	ml.style.left=x
	mc.style.top=y
	mc.style.left=x
	if(E_O.tagName.toUpperCase()=="A"&&!E_O.parentNode.id.indexOf("Mp_"))
	{
		document.getElementById("ml_ah").style.display="none"
		document.getElementById("ml_tg").style.display="none"
		document.getElementById("ml_ac").style.display="none"
		document.getElementById("ml_tt").style.display="none"
		document.getElementById("ml_mm").style.display="none"
		m=ml.getElementsByTagName("INPUT")
		m[0].value=E_O.innerHTML
		document.getElementById("ml_ah").style.display=""
		document.getElementById("ml_tg").style.display=""
		if(!E_O.id.indexOf("Mq_"))
		{
			document.getElementById("ml_mm").style.display=""
			document.getElementById("ml_mm").getElementsByTagName("SELECT")[0].value=E_O.id.substr(3,1)
		}
		else
		{
			document.getElementById("ml_tt").style.display=""
			document.getElementById("ml_ac").style.display=""
		}
		m[1].value=E_O.rev
		m[2].value=E_O.target
		m[3].value=E_O.lang
		ml.style.display=""
	}
	else
	{
		document.getElementById("mc_d").style.display="none"
		document.getElementById("mc_e").style.display="none"
		if(!E_O.id.indexOf("Mq_"))
		{
			document.getElementById("mc_e").value=E_O.id.substr(3,1)
			document.getElementById("mc_e").style.display=""
		}
		else
		{
			document.getElementById("mc_d").value=0
			document.getElementById("mc_d").style.display=""
		}
		EDIT_highLight(1)
		mc.style.display=""
	}
}

function MENU_child(v,r)
{
	if(r)
	{
		var o=document.getElementById(r)
		if(o)
		{
			E_O=o
			MENU_capture()
			o.id=(MENU_new[r]?MENU_new[r]:"")
			o.onmouseover=""
			o.onmouseout=""
			MENU_act++
			EDIT_TimeSave()
		}
		o=document.getElementById("Mp_"+(o?o.id:r).substr(4))
		if(o)
		{
			var a=o.getElementsByTagName("A")
			var i=0
			while(a[i])
			{
				if(!a[i].id.indexOf("Mq_"))
					MENU_child(0,a[i].id)
				i++
			}
			MENU_chiad(o.parentNode.id)
			o.parentNode.removeChild(o)
			MENU_act++
			EDIT_TimeSave()
		}
		E_O=0
	}
	else if(E_O.tagName)
	{
		if(!E_O.id.indexOf("Mq_1")||!E_O.id.indexOf("Mq_2"))
		{
			v="Mq_"+v+""+E_O.id.substr(4)
			MENU_capture(v)
			E_O.id=v
			_MENU(E_O,1)
		}
		else
		{
			var ec=1
			if(!E_O.parentNode.id.indexOf("Mp_"))
			{
				ec=0
				document.getElementById('ml_tt').style.display=""
				document.getElementById('ml_ah').style.display=""
				document.getElementById('ml_tg').style.display=""
				document.getElementById('ml_ac').style.display=""
				document.getElementById('MENU_link').style.left=parseInt(E_O.style.left)+170
			}
			v=(v?v:1)
			m="MENU"+(m0de_parent=="TMP"?"_":m0de_parent)
			MENU_chiad(m)
			var m=document.getElementById(m)
			var n=(m.lang?parseInt(m.lang)+1:99)
			m.lang=n
			var p=document.createElement("P")
			p.id="Mp_"+n+""+m0de_parent
			MENU_z++
			p.style.zIndex=MENU_z
			p.onmouseover=function(){_MENU(0,1,this)}
			p.onmouseout=function(){_MENU(0,0,this)}
			p.innerHTML="<a href=javascript:void(0) onfocus=blur()>New Link</a>"
			m.appendChild(p)
			item_XY(E_O)
			p.style.top=i_Y+parseInt(v==1?i_H:0)
			p.style.left=i_X+parseInt(v==2?i_W:0)
			v="Mq_"+v+""+n+""+m0de_parent
			MENU_new[v]=E_O.id
			if(ec)
				MENU_capture(v)
			E_O.id=v
			E_O.lang=""
			E_O.onmouseover=function(){_MENU(this,1)}
			E_O.onmouseout=function(){_MENU(this)}
			E_O=m.getElementsByTagName("A")[0]
		}
		MENU_act++
		EDIT_TimeSave()
	}
	MENU_close()
}

function MENU_move(v)
{
	EDIT_mw(document.getElementById("MENU_link"))
	var m=E_O
	var p=E_O.parentNode
	var a=p.getElementsByTagName('A')
	var i=0
	var n=0
	while(a[i])
	{
		if(a[i]==m)
			n=i
		i++
	}
	E_O.parentNode.removeChild(E_O)
	if(v&&n==0)
		p.appendChild(m)
	else if(!v&&n==(i-1))
		p.insertBefore(m,a[0])
	else
	{
		n=n+(v?-1:1)
		p.insertBefore(m,a[n])
	}
	E_O=m
	MENU_chiad(E_O.parentNode.parentNode.id)
	MENU_act++
	EDIT_TimeSave()
}

function MENU_add()
{
	var m=document.createElement("A")
	m.href="javascript:void(0)"
	m.onfocus=function(){blur()}
	m.innerHTML="New Link"
	E_O.parentNode.appendChild(m)
	_MENU(0,1,E_O.parentNode)
	MENU_chiad(E_O.parentNode.parentNode.id)
	MENU_act++
	EDIT_TimeSave()
}

function MENU_remove()
{
	if(confirm(warn+"Proceed with delete?"))
	{
		MENU_close()
		MENU_chiad(E_O.parentNode.parentNode.id)
		var d=E_O.id
		E_O.parentNode.removeChild(E_O)
		if(!d.indexOf("Mq_"))
			MENU_child(0,d)
		E_O=0
		MENU_act++
		EDIT_TimeSave()
		MENU_close()
	}
}

function MENU_value(t,d)
{
	MENU_chiad(E_O.parentNode.parentNode.id)
	EDIT_mw('MENU_link')
	if(d==1)
		E_O.innerHTML=t.value
	else if(d==2)
	{
		E_O.rev=t.value
		E_O.href=t.value.replace("index.php","modules/content_manager/manager.php")
	}
	else if(d==3)
		E_O.target=t.value
	else if(d==4)
	{
		if(t.value)
		{
			E_O.lang=t.value.replace(/\n/g,"<br>").replace(/"/g,"&quot;").replace(/'/g,"&#39;")
			E_O.onmouseover=function(e){T_S(e,this.lang)}
		}
		else
		{
			E_O.lang=""
			E_O.onmouseover=function(e){}
		}
	}
	MENU_act++
	EDIT_TimeSave()
}

function MENU_insert_address(v,t)
{
	if(t)
	{
		var o=document.getElementById("Mq_1Madtg")
		o.value=(v?v:"")
		MENU_value(o,3)
	}
	else
	{
		var o=document.getElementById("Mq_1Maddr")
		o.value=(v?"/index.php?page="+v:"")
		MENU_value(o,2)
	}
}

function MENU_col(t,v,e)
{
	MENUs[99]=v*1
	t=t.parentNode.offsetParent
	y=parseInt(t.style.top)-20
	x=parseInt(t.style.left)+170
	toggle(1,t.parentNode.offsetParent,e)
}

function MENU_colors(v)
{
	var o=document.getElementById("MENU_eg")
	v="#"+v
	MENUs[MENUs[99]]=v
	if(MENUs[99]==0)
		o.style.color=v
	else if(MENUs[99]==1)
		o.style.background=v
	else if(MENUs[99]==4)
		o.style.borderColor=v
	MENU_act++
	EDIT_TimeSave()
}

function MENU_purge()
{
	var k=document.getElementById("EDIT_AREA").getElementsByTagName("*")
	var i=0
	while(k[i])
	{
		if(!k[i].id.indexOf("Mq_1")||!k[i].id.indexOf("Mq_2"))
		{
			if(!document.getElementById("Mp_"+k[i].id.substr(4)))
				MENU_child(0,k[i].id)
		}
		i++
	}
	k=document.getElementById("EDIT_AREA").getElementsByTagName("SPAN")
	i=0
	while(k[i])
	{
		if(k[i].className=="MENU_")
		{
			s=0
			var p=k[i].getElementsByTagName("P")
			var d,e,c,j=0
			while(p[j])
			{
				c=1
				d="Mq_1"+p[j].id.substr(3)
				if(document.getElementById(d))
					c=0
				else
				{
					e="Mq_2"+p[j].id.substr(3)
					if(document.getElementById(e))
						c=0
				}
				if(c)
				{
					s++
					MENU_child(0,d)
					MENU_child(0,e)
				}
				j++
			}
			if(s)
			{
				MENU_act++
				EDIT_TimeSave()
			}
		}
		i++
	}
}

function MENU_close()
{
	toggle(0)
	MENU_TX('MENU_link')
	MENU_TX('MENU_children')
	MENU_TX('MENU_style')
	document.getElementById("EDIT_zhl").style.display="none"
}