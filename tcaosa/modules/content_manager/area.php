<?php

//Includes
include_once 'functions.php';
include_once SITE_ROOT . "/shared/database_functions.php";

$scripts = array();
$stripMarked = "";
$page_id = $_GET['Pid'];

//Get page areas
preg_match_all("@<!--AREA_([\w]+)-->@i", $template, $matches);
foreach ($matches[1] as $preg_match) {
	$template_areas[] = $preg_match;
}

//Got shared areas
preg_match_all("@<!--SHARED_([\w]+)-->@i", $template, $matches);
foreach ($matches[1] as $preg_match) {
	$shared_areas[] = $preg_match;
}

//Is the current user allowed to edit the shared areas
$statement = "SELECT shared_areas FROM {$GLOBALS['db_prefix']}_user_settings WHERE user_id=:user_id LIMIT 1";
$sql_user_setting = $GLOBALS['dbCon']->prepare($statement);
$sql_user_setting->bindParam(':user_id', $_SESSION['dbweb_user_id']);
$sql_user_setting->execute();
$sql_setting_data = $sql_user_setting->fetch();
$sql_user_setting->closeCursor();

//Is the current user allowed to edit this page
$statement = "SELECT count(*) AS c FROM {$GLOBALS['db_prefix']}_pages_owners WHERE (page_id=:page_id || page_id=0) AND user_id=:user_id";
$sql_access = $GLOBALS['dbCon']->prepare($statement);
$sql_access->bindParam(':user_id', $_SESSION['dbweb_user_id']);
$sql_access->bindParam(':page_id', $page_id);
$sql_access->execute();
$sql_data = $sql_access->fetch();
$sql_access->closeCursor();
$access_allowed = $sql_data['c'];

if ($access_allowed == 0) {
	echo "<script>window.parent.footer(\"<span style='color: red; font-weight: bold; font-size: 10pt'>NOTE! You do not have access to edit this page </span>\",{$_GET['W']});</script>";
}

echo "<div style='height: 20px'></div>";
echo "<script src=/modules/content_manager/area.js></script>";

//Menu Bar
$menu_bar = "<table id=menu_bar><tr>";
if ($access_allowed > 0) {
	$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"window.parent.W('Page_Details','/modules/content_manager/page_detail.php?Pid=$page_id&pW={$_GET['W']}',520)\">Page Attributes</td>";
	$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onclick='AREA_toggle_mode()')>Edit Mode</td>";
}
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onclick=\"location=location.href\" >Reload Page</td>";
$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td></tr>";
$menu_bar .= "</table>";
echo "<div id=top_menu >$menu_bar</div>";

//Prepare statment - Current version
$statement = "SELECT id,module FROM {$GLOBALS['db_prefix']}_areas WHERE area_id=:area_id AND page_id=:page_id LIMIT 1";
$sql_pages_current = $GLOBALS['dbCon']->prepare($statement);

//Prepare statment - Current version for shared area
$statement = "SELECT id,module FROM {$GLOBALS['db_prefix']}_areas WHERE area_id=:area_id AND shared='1' LIMIT 1";
$sql_shared_current = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - new area
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas (id, area_id, page_id, type, module) VALUES (:id, :area_id, :page_id, 'text', 'content_manager')";
$sql_page_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - new shared area
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas (id, area_id, shared, type, module) VALUES (:id, :area_id, '1', 'text', 'content_manager')";
$sql_shared_insert = $GLOBALS['dbCon']->prepare($statement);

//javascript areas array - Setup
$js_areas = "<script type='text/javascript'>" . PHP_EOL;
$js_areas .= "ars = new Array()" . PHP_EOL;
$js_areas .= "shr = new Array()" . PHP_EOL;

//get data for each area
$counter = 0;
foreach ($template_areas as $area_id) {
	$edit_count = 0;
	$header = '';
	$module = '';
	$type = '';
	$id = 0;
	$area_content = '';

	//Is there a current area
	$sql_pages_current->bindParam(':area_id', $area_id);
	$sql_pages_current->bindParam(':page_id', $page_id);
	$sql_pages_current->execute();
	$edit_count = $sql_pages_current->rowCount();
	$sql_pages_data = $sql_pages_current->fetch();

	//Insert an area if it doesn't exist
	if ($edit_count == 0) {
		$id = next_id("{$GLOBALS['db_prefix']}_areas", 'id');
		$sql_page_insert->bindParam(':id', $id);
		$sql_page_insert->bindParam(':page_id', $page_id);
		$sql_page_insert->bindParam(':area_id', $area_id);
		$sql_page_insert->execute();
	} else {
		$id = $sql_pages_data['id'];
	}

	//javascript areas array
	$js_areas .= "ars[$counter]='$area_id'" . PHP_EOL;

	//Check for current editing version
	$area_data = get_area_data($id);

	// module Data
	$content = '';
	$area_content = "<div id=EditArea_" . $area_id . " lang=" . $id . " name=_AREA>";
	$module_settings = get_module_variables($area_data['data']['module']);

	if (isset($module_settings[$area_data['data']['module']]['content_type'])) {
		foreach ($module_settings[$area_data['data']['module']]['content_type'] as $aid=>$area_ini) {
			 if ($area_ini['id'] == $area_data['data']['type']) {
				ob_start();
				include $_SERVER['DOCUMENT_ROOT'] . "/modules/{$module_settings[$area_data['data']['module']]['module_id']}/{$module_settings[$area_data['data']['module']]['content_type'][$aid]['display_file']}";
				$content = ob_get_contents();
				ob_end_clean();
			 }

		}
	}
	if (isset($content)) {
		$content_test = trim(strip_tags($content,'<img>'));
	} else {
		$content_test = '';
	}

	if (empty($content_test)) $content = "~Empty Area~";
	$area_content.="<div id=_HD" . $id . " >$content</div>";
	$area_content.="</div>";

	$template = str_replace("<!--AREA_" . $area_id . "-->", $area_content, $template);
	$counter++;
}

$counter = 0;
if (!empty($shared_areas)) {

	foreach ($shared_areas as $shared_id) {
		//Get current area data
		$sql_shared_data = array();
		$sql_shared_current->bindParam(':area_id', $shared_id);
		$sql_shared_current->execute();
		$sql_shared_data = $sql_shared_current->fetch();

		//Create DB entry if one doesn't already exist
		if (empty($sql_shared_data)) {
			$id = next_id("{$GLOBALS['db_prefix']}_areas", 'id');
			$sql_shared_insert->bindParam(':id', $id);
			$sql_shared_insert->bindParam(':area_id', $shared_id);
			$sql_shared_insert->execute();
		} else {
			$id = $sql_shared_data['id'];
		}

		//javascript areas array
		if ($sql_setting_data['shared_areas'] =='Y') {
			$js_areas .= "shr[$counter]='$shared_id'" . PHP_EOL;
		}


		//Check for current editing version
		$area_data = get_area_data($id);

		// module Data
		$content = '';
		$area_content = "<div id=EditShared_" . $shared_id . " lang=" . $id . " name=_AREA>";
		$module_settings = get_module_variables($area_data['data']['module']);
		if (isset($module_settings[$area_data['data']['module']]['content_type'])) {
			foreach ($module_settings[$area_data['data']['module']]['content_type'] as $aid=>$area_ini) {
				 if ($area_ini['id'] == $area_data['data']['type']) {
					ob_start();
					include $_SERVER['DOCUMENT_ROOT'] . "/modules/{$module_settings[$area_data['data']['module']]['module_id']}/{$module_settings[$area_data['data']['module']]['content_type'][$aid]['display_file']}";

					$content = ob_get_contents();
					ob_end_clean();
				 }
			}
		}
		if (isset($content)) {
			$content_test = trim(strip_tags($content));
		} else {
			$content_test = '';
		}

		if (empty($content_test)) $content = "~Empty Shared Area~";
		$area_content.="<div id=_HD$id >$content</div>";
		$area_content.="</div>";

		$template = str_replace("<!--SHARED_" . $shared_id . "-->", $area_content, $template);

		$counter++;
	}
}

$js_areas .= "</script>" . PHP_EOL;
echo $js_areas;
?>