<?php
if (!isset($document_root)) {
	$document_root = $_SERVER['DOCUMENT_ROOT'];
}

define('SITE_ROOT', $document_root);
$dir = $document_root . '/modules/';
$content_types = array();
$sName = $_SERVER['SERVER_NAME'];
$version = '$Id: init.php 132 2012-01-04 06:10:34Z thilo $';
$public_version = "1.0.1";

// default database connection values
include($document_root . '/settings/database.php');
include($document_root . '/settings/variables.php');
include_once $document_root . '/modules/content_manager/functions.php';

//Remove Slashes if magic_quotes is on
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}

//Error handling values
//$reporting_level = E_ALL; //One of the standard PHP error level constants

//date and Time settings
$timezone = 'Africa/Johannesburg'; //Only useful on PHP version 5.1.0 or greater

//Constants
//define('PROCESS', $_SERVER['DOCUMENT_ROOT'] . '/shared/page_process.php');

//Connect to Database - traditional
if (($db_con = mysql_connect($db_server, $db_username, $db_password)) === FALSE) die("<span style='font-weight: bold; color: red'>Failed to connect to Database</span>");
$db_con;
mysql_select_db($db_name);

//Connect to Database - PDO
try {
	$connect = "mysql:host=$db_server;dbname=$db_name";
	$dbCon = new PDO($connect, $db_username, $db_password);
	$dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$dbCon->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);

} catch(PDOException $e) {
	echo 'Failed to connect: PDO';
}


//Get settings
$SQL_statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
$SQL_set = mysql_query($SQL_statement);
while($set = mysql_fetch_array($SQL_set)) {
	$settings[$set['tag']] = $set['value'];
}

//Set time zone
if (version_compare('5.1.0', PHP_VERSION) < 0) {
	date_default_timezone_set($timezone);
}

//Start Session
$session_id = session_id();
if (empty($session_id)) {
	session_name('dbweb4x');
	ini_set('session.gc_maxlifetime', 30*60);
	if (!isset($cron)) session_start();
}

if (is_module('cart') && !isset($_SESSION['cart_position'])) {
	$statement = "SELECT id
				FROM {$GLOBALS['db_prefix']}_areas
				WHERE type='shopping_cart'
				LIMIT 1";
	$sql_cart = $GLOBALS['dbCon']->prepare($statement);
	$sql_cart->execute();
	$sql_cart_data = $sql_cart->fetch();
	$sql_cart->closeCursor();

	if (!isset($sql_cart_data['id'])) {
		$statement = "SELECT id
				FROM {$GLOBALS['db_prefix']}_areas_editing
				WHERE type='shopping_cart'
				LIMIT 1";
		$sql_cart = $GLOBALS['dbCon']->prepare($statement);
		$sql_cart->execute();
		$sql_cart_data = $sql_cart->fetch();
		$sql_cart->closeCursor();
	}

	if (isset($sql_cart_data['id'])) {
		$_SESSION['cart_position'] = $sql_cart_data['id'];
	}
}

//Register Modules
//if (!isset($_SESSION['module_register'])) {
	$module_data = get_module_variables();
	foreach ($module_data as $module_id=>$data) {
		$_SESSION['module_register'][] = $module_id;
	}
//}

if (!isset($ajax) && !isset($cron)) echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><link rel=stylesheet href=/shared/style.css>";
?>