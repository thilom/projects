<?php
/** 
 * Logout function
 *
 * @version $Id: logout.php 48 2011-05-06 03:47:20Z thilo $
 * @author Thilo Muller(2011)
 */

include $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
unset($_SESSION['dbweb_user_id']);

echo "<script type='text/javascript'>document.location='/admin'</script>";
die();
?>
