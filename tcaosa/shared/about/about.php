<?php

echo "<html>";
echo "<script src=/shared/G.js></script>";
//echo "<script src=/shared/tabs.js></script>";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/admin_style.php";
echo $css;
echo "<style type=text/css>td{vertical-align:top}span img{margin:0 18 3 0;float:left}span b{font-size:19pt;letter-spacing:-2;display:block;margin:0 0 8 0}</style>";
echo "<body>";

echo "<script>tabs(\"";
echo "<a href=# id=Mq_1In onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Index</a>";
echo "<a href=# id=Mq_1md onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Modules</a>";
echo "\")</script>";

echo "<span class=TabP>";
echo "<p id=Mp_In onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
echo "<a href=#in onfocus=blur()>Intro</a>";
echo "</p>";
echo "<p id=Mp_md onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
echo "<a href=#cm onfocus=blur()>Content Management</a>";
echo "<a href=#gl onfocus=blur()>Gallery</a>";
echo "</p>";
echo "</span>";

echo "<br>";
echo "<br>";

if(file_exists($_SERVER['DOCUMENT_ROOT']."/W/F/L.png"))
	echo "<img src=/W/F/L.png><br>";

echo "<a name=in><br></a>";
echo "<br>mOdularus is a content manger system that strives to put the power in your hands while removing the need for html or programming knowledge. Unlike other systems available, mOdularus goes the extra step to make adding editing and manipulating objects, as easy and intuitive as possible for the layman. In this way mOdularus ensures immediate editing capabilities by any one allowed secure access, without training or expensive editing programs. mOdularus offers an array of modules that handle Internet tasks with ease and a little more thought than found in other systems. All modules are SEO friendly while delivery of content is streamlined to accommodate slow connections.<hr>Note that mOdularus is continually building new modules to solve everyday business and Internet needs. Find out more about mOdularus modules:";
echo "<span>";

// please put modules alphabetically & update TABS index

echo "<a name=cm><hr></a>";
echo "<br><img src=content.png><b>Content Manager</b>We have gone the extra step to make managing sites as easy as pie. We have striven to avoid page reloading and excessive pop-up windows, while putting all the tools and gadgets at your disposal in an effective yet unobtrusive tool bar mechanism. mOdularus prefers the dynamic 'real-time' approach where what you do is immediately evident (as opposed to doing one thing in a window and then reloading the content area). W e have also included 'nice-to-haves' like Menu editor, Tool tip editor, link editor and Form editor which all have tool tip guides every step of the way.<br><br>";


echo "<a name=gl><hr></a>";
echo "<br><img src=gallery.png><b>Gallery</b>Our Gallery module is designed around ease of use for not only users but administrators as well. The mOdularus approach is to allow easy access to Gallery administration tools and settings whilst putting as many customization tools and extras at administrators' disposal. Administrators can configure how the gallery behaves and also how users interact, order or acquire further images. Gallery also integrates with our E-Commerce module. Galleries are often burdened by excessive html code that slows the delivery of images. This has been solved with a 'data-only' delivery solution that allows for rapid image deployment to users. Gallery also allows you to add your own database fields that are searchable, allowing for great flexibility. Gallery also has sophisticated displaying methods for sorting and viewing images, while allowing for full customisation of views by both admin and users. Further more, Gallery employs the latest EXIF extraction tools for easy automatic categorization of images.<br><br><hr>";



echo "</span>";
echo "</body>";
echo "</html>";

?>