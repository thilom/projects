<?php
ob_start();

include_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/directory_item.php";

echo "<html>";
echo "<script src=/shared/G.js></script>";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/admin_style.php";
echo $css;
echo "<body>";

echo "<b>Upload Files</b><tt>Click on the box you want to use, then Copy & Paste files onto the box to upload to mOdularus. When completed a list of files will appear (you can continue to upload from this point). Wait for files to complete uploading before closing window!</tt><br>";

$sql="SELECT * FROM dbweb_settings WHERE tag='ftp_host' OR tag='ftp_username' OR tag='ftp_password' OR tag='ftp_dir'";
$rs=mysql_query($sql);
if(mysql_num_rows($rs))
{
	$ftp_host="";
	$ftp_username="";
	$ftp_password="";
	$ftp_dir="";

	while($g=mysql_fetch_array($rs))
	{
		if($g['tag']=="ftp_host")
			$ftp_host=$g['value'];
		elseif($g['tag']=="ftp_username")
			$ftp_username=$g['value'];
		elseif($g['tag']=="ftp_password")
			$ftp_password=$g['value'];
		elseif($g['tag']=="ftp_dir")
			$ftp_dir=$g['value'];
	}


	include $_SERVER['DOCUMENT_ROOT']."/shared/css.php";
	getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","W.css");
	if($itemname)
		$css=$_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname;
	else
		$css=file_get_contents($_SERVER['DOCUMENT_ROOT']."/shared/W.css");

	$bg=css_extract($css,"body","background","#BBBBBB");
	if(strlen($bg)<5)
	{
		$b=substr($bg,3,1);
		$bg=$bg.$b.$b.$b;
	}
	$html="<body style=background:".$bg."><img src=http://".str_replace("ftp","www",$ftp_host)."/shared/upload/box.png></body>";

	echo "<table class=T>";
	
	$useApplet=0;
	$user_agent=$_SERVER['HTTP_USER_AGENT'];

	// templates
	echo "<tr>";
	echo "<td>";
	if(stristr($user_agent,"konqueror") || stristr($user_agent,"macintosh") || stristr($user_agent,"opera"))
	{
	  $useApplet=1;
	  echo "<applet name='Rad Upload Lite' archive=/shared/img_batch/dndlite.jar code=com.radinks.dnd.DNDAppletLite width=130 height=120>";
	}
	else
	{
	  if(strstr($user_agent,"MSIE"))
	  {
		 echo "<script src=/shared/upload/embed.js></script>";
		 echo "<script>IELoader()</script>";
	  }
	  else
	  {
		 echo "<object type='application/x-java-applet;version=1.4.1' width=130 height=120 id=rup name=rup>";
		 echo "<param name=archive value=/shared/upload/dndlite.jar>";
		 echo "<param name=code value='com.radinks.dnd.DNDAppletLite'>";
		 echo "<param name=name value='Rad Upload Lite'>";
	  }
	}
	
	echo "<param name=message value='".$html."'>";
	echo "<param name=max_upload value=2000000000>";
	echo "<param name=url value=ftp://".$ftp_username.":".$ftp_password."@".str_replace("www","ftp",$ftp_host)."/".trim($ftp_dir,"/")."/i/templates>";
	if(isset($_SERVER['PHP_AUTH_USER']))
	{
		printf("<param name=chap value=%s>",
		base64_encode($_SERVER['PHP_AUTH_USER'].":".$_SERVER['PHP_AUTH_PW']));
	}
	if($useApplet==1)
		echo "</applet>";
	else
		echo "</object>";
	
	echo "</td>";
	echo "<td><b>Templates</b><tt>Copy & Paste templates here. Note that any files in the Templates folder will be automatically entered into the database as templates (Re-open the Content Manager start window to do this).</tt>";
	echo "</td>";
	echo "</tr>";

	// template files
	echo "<tr>";
	echo "<td>";
	if(stristr($user_agent,"konqueror") || stristr($user_agent,"macintosh") || stristr($user_agent,"opera"))
	{
	  $useApplet=1;
	  echo "<applet name='Rad Upload Lite' archive=/shared/img_batch/dndlite.jar code=com.radinks.dnd.DNDAppletLite width=130 height=120>";
	}
	else
	{
	  if(strstr($user_agent,"MSIE"))
	  {
		 echo "<script src=/shared/upload/embed.js></script>";
		 echo "<script>IELoader()</script>";
	  }
	  else
	  {
		 echo "<object type='application/x-java-applet;version=1.4.1' width=130 height=120 id=rup name=rup>";
		 echo "<param name=archive value=/shared/upload/dndlite.jar>";
		 echo "<param name=code value='com.radinks.dnd.DNDAppletLite'>";
		 echo "<param name=name value='Rad Upload Lite'>";
	  }
	}
	
	echo "<param name=message value='".$html."'>";
	echo "<param name=max_upload value=2000000000>";
	echo "<param name=url value=ftp://".$ftp_username.":".$ftp_password."@".str_replace("www","ftp",$ftp_host)."/".trim($ftp_dir,"/")."/i>";
	if(isset($_SERVER['PHP_AUTH_USER']))
	{
		printf("<param name=chap value=%s>",
		base64_encode($_SERVER['PHP_AUTH_USER'].":".$_SERVER['PHP_AUTH_PW']));
	}
	if($useApplet==1)
		echo "</applet>";
	else
		echo "</object>";
	
	echo "</td>";
	echo "<td><b>Template Files</b><tt>Copy & Paste template related files like style-sheets, images, etc.</tt>";
	echo "</td>";
	echo "</tr>";

	// assets
	echo "<tr>";
	echo "<td>";
	if(stristr($user_agent,"konqueror") || stristr($user_agent,"macintosh") || stristr($user_agent,"opera"))
	{
	  $useApplet=1;
	  echo "<applet name='Rad Upload Lite' archive=/shared/img_batch/dndlite.jar code=com.radinks.dnd.DNDAppletLite width=130 height=120>";
	}
	else
	{
	  if(strstr($user_agent,"MSIE"))
	  {
		 echo "<script src=/shared/upload/embed.js></script>";
		 echo "<script>IELoader()</script>";
	  }
	  else
	  {
		 echo "<object type='application/x-java-applet;version=1.4.1' width=130 height=120 id=rup name=rup>";
		 echo "<param name=archive value=/shared/upload/dndlite.jar>";
		 echo "<param name=code value='com.radinks.dnd.DNDAppletLite'>";
		 echo "<param name=name value='Rad Upload Lite'>";
	  }
	}
	
	echo "<param name=message value='".$html."'>";
	echo "<param name=max_upload value=2000000000>";
	echo "<param name=url value=ftp://".$ftp_username.":".$ftp_password."@".str_replace("www","ftp",$ftp_host)."/".trim($ftp_dir,"/")."/i/i>";
	if(isset($_SERVER['PHP_AUTH_USER']))
	{
		printf("<param name=chap value=%s>",
		base64_encode($_SERVER['PHP_AUTH_USER'].":".$_SERVER['PHP_AUTH_PW']));
	}
	if($useApplet==1)
		echo "</applet>";
	else
		echo "</object>";
	
	echo "</td>";
	echo "<td><b>Assets</b><tt>Copy & Paste assets like images, flash, pdfs, movies, etc. Assets are available to the area editor.</tt>";
	echo "</td>";
	echo "</tr>";

	// gallery
	if(is_dir($_SERVER['DOCUMENT_ROOT']."/modules/gallery/ftp")==true)
	{
		echo "<tr>";
		echo "<td>";
		if(stristr($user_agent,"konqueror") || stristr($user_agent,"macintosh") || stristr($user_agent,"opera"))
		{
		  $useApplet=1;
		  echo "<applet name='Rad Upload Lite' archive=/shared/img_batch/dndlite.jar code=com.radinks.dnd.DNDAppletLite width=130 height=120>";
		}
		else
		{
		  if(strstr($user_agent,"MSIE"))
		  {
			 echo "<script src=/shared/upload/embed.js></script>";
			 echo "<script>IELoader()</script>";
		  }
		  else
		  {
			 echo "<object type='application/x-java-applet;version=1.4.1' width=130 height=120 id=rup name=rup>";
			 echo "<param name=archive value=/shared/upload/dndlite.jar>";
			 echo "<param name=code value='com.radinks.dnd.DNDAppletLite'>";
			 echo "<param name=name value='Rad Upload Lite'>";
		  }
		}
		
		echo "<param name=message value='".$html."'>";
		echo "<param name=max_upload value=2000000000>";
		echo "<param name=url value=ftp://".$ftp_username.":".$ftp_password."@".str_replace("www","ftp",$ftp_host)."/".trim($ftp_dir,"/")."/modules/gallery/ftp>";
		if(isset($_SERVER['PHP_AUTH_USER']))
		{
			printf("<param name=chap value=%s>",
			base64_encode($_SERVER['PHP_AUTH_USER'].":".$_SERVER['PHP_AUTH_PW']));
		}
		if($useApplet==1)
			echo "</applet>";
		else
			echo "</object>";
		
		echo "</td>";
		echo "<td><b>Gallery FTP</b><tt>Copy & Paste images to the Gallery FTP folder - note that images still have to be parsed to the database. Go to Gallery -> Upload for this.</tt>";
		echo "</td>";
		echo "</tr>";
	}

	echo "</table>";

}

echo "</body>";
echo "</html>";

ob_end_flush();
?>