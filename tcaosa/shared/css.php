<?php

// extracter
// css_extract($css,"body","background","#BBB");
// use $css=file_get_contents($_SERVER['DOCUMENT_ROOT']."/example.css");

function css_extract($string,$get1,$get2,$default="")
{
	$get1=trim($get1);
	$get2=trim($get2,";");
	$get1=trim($get1,"{");
	$get2=trim($get2,":");
	$get1=" ".$get1."{";
	$get2=";".$get2.":";
	$string="\n".$string;
	$string=str_replace("   "," ",$string);
	$string=str_replace("  "," ",$string);
	$string=str_replace("\n","\n ",$string);
	$string=str_replace(" {","{",$string);
	$string=str_replace("{","{;",$string);
	$string=str_replace("\n{","{",$string);
	$string=str_replace("}",";}",$string);
	$string=str_replace("\n}","}",$string);
	$string=str_replace("\n;",";",$string);
	$string=str_replace(";\n",";",$string);
	$string=str_replace(", ",",",$string);
	$string=str_replace(" ,",",",$string);
	if(stripos($string,$get1)>-1)
	{
		$part1=substr($string,0,stripos($string,$get1)+strlen($get1));
		$part2=substr($string,stripos($string,$get1)+strlen($get1));
		$part2=substr($part2,0,stripos($part2,"}"));
		if(stripos($part2,$get2)>-1)
		{
			$part2=substr($part2,stripos($part2,$get2)+strlen($get2));
			$part2=substr($part2,0,strpos($part2,";"));
			$string=$part2;
		}
		else
			$string=$default;
	}
	else
		$string=$default;
	return $string;
}
	
// inserter
// css_insert($css,"body","background","#BBB");

function css_insert($string,$get1,$get2,$value)
{
	$get1=trim($get1);
	$get2=trim($get2,";");
	$get1=trim($get1,"{");
	$get2=trim($get2,":");
	$get1=" ".$get1."{";
	$get2=";".$get2.":";
	$value.=";";
	$string="\n".$string;
	$string=str_replace("\n","\n ",$string);
	$string=str_replace(" {","{",$string);
	$string=str_replace("{","{;",$string);
	$string=str_replace("\n{","{",$string);
	$string=str_replace("}",";}",$string);
	$string=str_replace("\n}","}",$string);
	$string=str_replace("\n;",";",$string);
	$string=str_replace(";\n",";",$string);
	$string=str_replace(", ",",",$string);
	$string=str_replace(" ,",",",$string);
	if(stripos($string,$get1)>-1)
	{
		$part1=substr($string,0,stripos($string,$get1)+strlen($get1));
		$part2=substr($string,stripos($string,$get1)+strlen($get1));
		$part3=substr($part2,strpos($part2,"}"));
		$part2=substr($part2,0,stripos($part2,"}"));
		if(stripos($part2,$get2)>-1)
		{
			$part21=substr($part2,0,stripos($part2,$get2));
			$part22=substr($part2,stripos($part2,$get2)+strlen($get2));
			$part22=substr($part22,strpos($part22,";")+1);
			$string=$part1.$part21.$get2.$value.$part22.$part3;
		}
		else
			$string=$part1.$get2.$value.$part2.$part3;
	}
	else
		$string=$string."\n\n".$get1.$get2.$value."}";
	$string=str_replace("{;","{",$string);
	$string=str_replace(";}","}",$string);
	$string=str_replace("{{","{",$string);
	$string=str_replace("}}","}",$string);
	$string=str_replace("::",":",$string);
	$string=str_replace(";;",";",$string);
	$string=str_replace("  "," ",$string);
	$string=str_replace("\n ","\n",$string);
	$string=str_replace("\n\n\n\n","\n\n",$string);
	$string=str_replace("\n\n\n","\n\n",$string);
	return $string;
}

// WRITE
// use include "/shared/document_write.php";
// $date=mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
// write_doc($_SERVER['DOCUMENT_ROOT']."/fluid/".$date.".css",$css);
// If you have more than one css file use "$date.name.css" to differentiate
// BEWARE on some servers # is not allowed in strings?
?>