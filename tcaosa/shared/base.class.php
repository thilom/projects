<?php
/**
 * DBWeb4 Base Class
 * 
 * This is an abstract class which must be used in all classes eg. class xyz extends base {}.
 * 
 * @author Thilo Muller
 * @copyright DBWeb4 - 2008
 * @version 1.0.0
 * 
 */

abstract class base {
	
	
	function __construct() {
	}
	
	
}

?>