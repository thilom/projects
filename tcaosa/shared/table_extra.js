// add elemnet to any table
// arguments:
// t= table id
// v= instruction
// 1= add row
// 2 = add column
// 3 = remove row
// 4 = remove column

function table_extra(t,v)
{
	var g=document.getElementById(t).getElementsByTagName("TBODY")[0]
	if(v==1)
	{
		var tr=document.createElement("TR")
		var n=g.getElementsByTagName("TR")[0].getElementsByTagName("TD").length
		var i=0
		while(i<n)
		{
			var td=document.createElement("TD")
			td.innerHTML="&#9660;"
			tr.appendChild(td)
			i++
		}
		g.appendChild(tr)
	}
	else if(v==2)
	{
		var tr=g.getElementsByTagName("TR")
		var i=0
		while(tr[i])
		{
			var td=document.createElement("TD")
			td.innerHTML="&#9660;"
			tr[i].appendChild(td)
			i++
		}
	}
	else if(v==3)
	{
		var tr=g.getElementsByTagName("TR")
		if(tr.length>1)
			g.removeChild(tr[tr.length-1])
	}
	else if(v==4)
	{
		var tr=g.getElementsByTagName("TR")
		var i=0
		while(tr[i])
		{
			var td=tr[i].getElementsByTagName("TD")
			if(td.length>1)
				tr[i].removeChild(td[td.length-1])
			else
				break
			i++
		}
	}
}