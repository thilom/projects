<?php

$string_endtag=array();
$string_endtags["meta"]=1;
$string_endtags["img"]=1;
$string_endtags["link"]=1;
$string_endtags["!doctype"]=1;


function string_extract($tag,$idname="",$attr="")
{
	global $string;
	global $string_endtags;

	if(!is_array($string))
	{
		string_clean();
		$string=explode(">",$string);
	}

	$v="";

	$tag=strtolower($tag);
	$tag=trim($tag);
	$tag=trim($tag,"<");
	$tag=trim($tag,">");
	$tag_start="<".$tag." ";

	$tag_end="";
	if(!isset($string_endtags[$tag]))
		$tag_end="</".$tag." ";

	$i=0;
	$n=count($string);
	while($i<$n)
	{
	if($tag=="table")
			echo "<script>alert(\"".$string[$i]."\")</script>";

		if(
			$string[$i]==""
			||
			strpos($string[$i],"<!")!==FALSE
			||
			stripos($string[$i],"<?php")!==FALSE
			||
			stripos($string[$i],"?")!==FALSE
			)
		{}
		elseif(strpos($string[$i],$tag_start)!==FALSE)
		{
			if($idname)
			{
				if(strpos($string[$i],$idname)!==FALSE || strpos($string[$i],str_replace("=","=\"",$idname)."\"")!==FALSE || strpos($string[$i],str_replace("=","='",$idname)."'")!==FALSE)
				{
					if($attr)
					{
						$attr=strtolower($attr);
						$attr=trim($attr);
						$attr=trim($attr,"=");
						$attr=trim($attr,"'");
						$attr=trim($attr,"\"");
						$v=substr($string[$i],strpos($string[$i],$attr)+strlen($attr)+1)." ";
						if(substr($v,0,1)=="'")
						{
							$v=substr($v,1);
							$v=substr($v,0,strpos($v,"'"));
						}
						elseif(substr($v,0,1)=="\"")
						{
							$v=substr($v,1);
							$v=substr($v,0,strpos($v,"\""));
						}
						else
							$v=substr($v,0,strpos($v," "));
						$v=trim($v);
						break;
					}
					elseif($tag_end && strpos($string[$i+1],$tag_end)!==FALSE)
					{
						$v=substr($string[$i+1],0,strpos($string[$i+1],$tag_end));
						break;
					}
				}
			}
		}
		elseif($tag_end && strpos($string[$i],$tag_end)!==FALSE)
		{
			$v=substr($string[$i],0,strpos($string[$i],"<"));
			break;
		}
		$i++;
	}
	return $v;
}


function string_insert($insert,$tag="",$idname="",$attr="",$container="")
{
	global $string;
	global $string_endtags;

	if(!is_array($string))
	{
		string_clean();
		$string=explode(">",$string);
	}

	$done=0;

	$tag=strtolower($tag);
	$tag=trim($tag);
	$tag=trim($tag,"<");
	$tag=trim($tag,">");
	$tag_start="<".$tag." ";

	$tag_end="";
	if(!isset($string_endtags[$tag]))
		$tag_end="</".$tag." ";

	if($attr)
	{
		$attr=strtolower($attr);
		$attr=trim($attr);
		$attr=trim($attr,"=");
		$attr=trim($attr,"'");
	}

	$i=0;
	$n=count($string);
	while($i<$n)
	{
		if(
			$string[$i]==""
			||
			strpos($string[$i],"<!")!==FALSE
			||
			stripos($string[$i],"<php ")!==FALSE
			||
			stripos($string[$i],"\n?")!==FALSE
			)
		{}
		elseif(strpos($string[$i],$tag_start)!==FALSE)
		{
			if($idname)
			{
				if(strpos($string[$i],$idname)!==FALSE || strpos($string[$i],str_replace("=","=\"",$idname)."\"")!==FALSE || strpos($string[$i],str_replace("=","='",$idname)."'")!==FALSE)
				{
					if($attr)
					{
						if(stripos($string[$i],$attr)!==FALSE)
						{
							$insert=trim($insert);
							$insert=trim($insert,"\"");
							$insert=trim($insert,"'");
							$insert=str_replace("\n"," ",$insert);
							if(($insert && strpos($insert," ")) || ($idname && !$insert))
								$insert="\"".$insert."\"";

							$part1=substr($string[$i],0,stripos($string[$i],$attr)+strlen($attr)+1);
							$part2=substr($string[$i],stripos($string[$i],$attr)+strlen($attr)+1);
							$part2=str_replace("' ","'",$part2);
							$part2=str_replace(" '","'",$part2);
							$part2=str_replace("\" ","\"",$part2);
							$part2=str_replace(" \"","\"",$part2);
							if(substr($part2,0,1)=="'")
							{
								$part2=substr($part2,1);
								$part2=substr($part2,strpos($part2,"'")+1);
							}
							elseif(substr($part2,0,1)=="\"")
							{
								$part2=substr($part2,1);
								$part2=substr($part2,strpos($part2,"\"")+1);
							}
							else
								$part2=substr($part2,strpos($part2," "));
							$string[$i]=$part1.$insert.$part2;
						}
						else
							$string[$i].=" ".$attr."=".$insert;
						$done=1;
						break;
					}
				}
				elseif($tag_end && strpos($string[$i+1],$tag_end)!==FALSE)
				{
					$string[$i+1]=$insert.substr($string[$i+1],strpos($string[$i+1],"<"));
					$done=1;
					break;
				}
			}
		}
		elseif($tag_end && strpos($string[$i],$tag_end)!==FALSE)
		{
			$string[$i]=$insert.substr($string[$i],strpos($string[$i],"<"));
			$done=1;
			break;
		}
		$i++;
	}
	if(!$done && $insert && $container)
	{
		$container=strtolower($container);
		$container=trim($container);
		$container=trim($container,"<");
		$container=trim($container,">");
		$container="<".$container." ";
		string_recompile();
		if(strpos($string,$container)!==FALSE)
		{
			$insert=str_replace("\n"," ",$insert);
			if(strpos($insert," "))
				$insert="\"".$insert."\"";
			$tag_end=($tag_end?$tag_end." >":"");
			if($attr)
				$newtag=$tag_start.($idname?" ".$idname:"")." ".$attr."=".$insert." >".$tag_end;
			else
				$newtag=$tag_start.($idname?" ".$idname:"")." >".$insert.$tag_end;
			$part1=substr($string,0,strpos($string,$container));
			$part2=substr($string,strpos($string,$container));
			$part3=substr($part2,0,strpos($part2,">")+1);
			$part4=substr($part2,strpos($part2,">")+1);
			$string=$part1.$part3."\n".$newtag.$part4;
		}
		$string=explode(">",$string);
	}
}


function string_recompile()
{
	global $string;
	$n=count($string)-1;
	$comp="";
	$i=0;
	while($i<$n)
	{
		$comp.=$string[$i].">";
		$i++;
	}
	$string=$comp;
}

function string_clean()
{
	global $string;
	$string=str_replace("   "," ",$string);
	$string=str_replace("  "," ",$string);
	$string=str_replace(" < ","<",$string);
	$string=str_replace(" <","<",$string);
	$string=str_replace("< ","<",$string);
	$string=str_replace(" > ","> ",$string);
	$string=str_replace(" >",">",$string);
	$string=str_replace("> ",">",$string);
	$string=str_replace("> <","><",$string);
	$string=str_replace(" />",">",$string);
	$string=str_replace("/>",">",$string);
	$string=str_replace("\v","",$string);
	$string=str_replace("\n\n\n\n","\n\n",$string);
	$string=str_replace("\n\n\n","\n\n",$string);
	$string=str_replace("  "," ",$string);
	$string=str_replace("\r","",$string);
	$string=str_replace(";}","}",$string);
	$string=str_replace(';"','"',$string);
	$string=str_ireplace(' alt=""',"",$string);
	$string=str_ireplace(" alt=''","",$string);
	$string=str_replace(">"," >",$string); // IMPORTANT!

	preg_match_all('@<([a-zA-Z]+)@i',$string,$match);
	foreach($match[1] as $t)
	{
		$string=preg_replace('@<'.$t.'@i',strtolower("<$t"),$string);
		$string=preg_replace('@</'.$t.'>@',strtolower("</$t>"),$string);
	}

	$string=str_replace(" = ","=",$string);
	$string=str_replace(" =","=",$string);
	$string=str_replace("= ","=",$string);
	$string=str_ireplace("href","href",$string);
	$string=str_ireplace("href=\"javascript:void(0)\"","href=javascript:void(0)",$string);
	$string=str_ireplace("href='javascript:void(0)'","href=javascript:void(0)",$string);
	$string=str_ireplace("name=","name=",$string);
	$string=str_ireplace("target=","target=",$string);
	$string=str_ireplace("lang=","lang=",$string);
	$string=str_ireplace("id=","id=",$string);
	$string=str_ireplace("onmouseover=","onmouseover=",$string);
	$string=str_ireplace("onmouseout=","onmouseout=",$string);
	$string=str_ireplace(" type=\"text/javascript\"","",$string);
	$string=str_ireplace(" type='text/javascript'","",$string);
	$string=str_ireplace(" type=text/javascript","",$string);
	$string=str_ireplace(" language=\"javascript\"","",$string);
	$string=str_ireplace(" language='javascript'","",$string);
	$string=str_ireplace(" language=javascript","",$string);
}


function string_write($fn)
{
	global $string;
	if(is_array($string))
		string_recompile();

	$string=str_replace("  "," ",$string); // IMPORTANT!
	$string=str_replace(" >",">",$string);

	$fp=fopen($fn,"w");
	fwrite($fp,$string);
	fclose($fp);
	@chmod($fn,0777);
}

?>