<?php

// returns an array of filenames from a directory
$filelist="";
$filecount=0;

if(!function_exists("getFiles"))
{
	function getFiles($directory,$all=0)
	{
		global $filecount;
		global $filelist;
		$filelist=array();
		$directory="/".trim($directory,"/");
		if(is_dir($directory) && is_readable($directory))
		{
			$handle=opendir($directory);
			while(FALSE!==($item=readdir($handle)))
			{
				if($item!="." && $item!="..")
				{
					if($all)
						$filelist[]=$item;
					elseif(!is_dir($directory."/".$item) && strpos($item,".")>0)
					{
						$filelist[]=$item;
						$filecount++;
					}
				}
			}
			closedir($handle);
		}
	}
}
?>