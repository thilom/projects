var curId = '';


// hover effect on the dropdownlist arrow icon
function swapImage(img, src) {
    var img = document.getElementById(img);
    img.src = src;
}

// check the click events are not occurring on the controls
// if the mouse click event occurs outside the control, options should close
document.onclick=check;
function check(e){
    var target = (e && e.target) || (event && event.srcElement);
    var obj1 = document.getElementById('option_placeholder');
    checkParent(target)?obj1.style.display='none':null;
}
function checkParent(t){
    while(t.parentNode){
        var myPlaceholder = document.getElementById('option_placeholder');
        var myTextbox = document.getElementById('text1');
        var myImage = document.getElementById('imgText1');
        var myTextbox2 = document.getElementById('text2');
        var myImage2 = document.getElementById('imgText2');
        if(t==myPlaceholder ||  t==myTextbox || t==myImage ||  t==myTextbox2 || t==myImage2){
            return false
        }
        t=t.parentNode
    }
    return true
}

// show the options where the event took place
 function openCombo(textbox, div) {
        var div = document.getElementById(div);
        var box = document.getElementById(textbox);
        div.style.position = 'absolute';
        if(div.style.display=='none') {
        var X = (findPosX(box));
        var Y = findPosY(box) + 19;
        div.style.left =  (X - 1) + 'px';
        div.style.top = Y + 'px';
        div.style.display = '';
	} else {
		div.style.display = 'none';
	}
}

function findPosX(obj)
{
    var curleft = 0;
    if(obj.offsetParent)
        while(1)
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
}

function findPosY(obj)
{
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
}
