<?php
//Constants
define('ERROR_CLASS', $_SERVER['DOCUMENT_ROOT'] . '/shared/error.class.php');

//Includes
require_once ERROR_CLASS;

/**
 * Creates a public rror class that can be accessed from outside a class.
 *
 */
class public_error extends error {
	
}

?>