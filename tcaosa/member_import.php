<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

set_time_limit(0);

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/login_public.class.php';

//Vars
$login = new Public_login();

//Get data fom old DB
$statement = "SELECT title, name, surname, email, tel_home, tel_work, cellphone, id_number, physical_address, postal_address, postal_code,
						post_delivered_to, date_of_birth, language, skills, pensioner, company, amount, salary_ref, start_date, get_birthday_sms,
						active, date_active, paid, deogram, reminder_date, username, password
				FROM sapo";
$sql_old = $GLOBALS['dbCon']->prepare($statement);
$sql_old->execute();
$sql_old_data = $sql_old->fetchAll();
$sql_old->closeCursor();

//Insert into new
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_members
					(title, firstname, lastname, email, tel_home, tel_work, cellphone, id_number, physical_address, postal_address, postal_code,
						post_delivered_to, date_of_birth, language, skills, pensioner, company, amount, salary_ref, start_date, get_birthday_sms,
						active, date_active, paid, deogram, reminder_date, username, password)
				VALUES
					(:title, :firstname, :lastname, :email, :tel_home, :tel_work, :cellphone, :id_number, :physical_address, :postal_address, :postal_code,
						:post_delivered_to, :date_of_birth, :language, :skills, :pensioner, 'sapo', :amount, :salary_ref, :start_date, :get_birthday_sms,
						:active, :date_active, :paid, :deogram, :reminder_date, :username, :password)";
$sql_new = $GLOBALS['dbCon']->prepare($statement);
foreach ($sql_old_data as $old_data) {
	
	$password = $login->hash($old_data['password']);
	
	$sql_new->bindParam(':title', $old_data['title']);
	$sql_new->bindParam(':firstname', $old_data['name']);
	$sql_new->bindParam(':lastname', $old_data['surname']);
	$sql_new->bindParam(':email', $old_data['email']);
	$sql_new->bindParam(':tel_home', $old_data['tel_home']);
	$sql_new->bindParam(':tel_work', $old_data['tel_work']);
	$sql_new->bindParam(':cellphone', $old_data['cellphone']);
	$sql_new->bindParam(':id_number', $old_data['id_number']);
	$sql_new->bindParam(':physical_address', $old_data['physical_address']);
	$sql_new->bindParam(':postal_address', $old_data['postal_address']);
	$sql_new->bindParam(':postal_code', $old_data['postal_code']);
	$sql_new->bindParam(':post_delivered_to', $old_data['post_delivered_to']);
	$sql_new->bindParam(':date_of_birth', $old_data['date_of_birth']);
	$sql_new->bindParam(':language', $old_data['language']);
	$sql_new->bindParam(':skills', $old_data['skills']);
	$sql_new->bindParam(':pensioner', $old_data['pensioner']);
//	$sql_new->bindParam(':company', $old_data['company']);
	$sql_new->bindParam(':amount', $old_data['amount']);
	$sql_new->bindParam(':salary_ref', $old_data['salary_ref']);
	$sql_new->bindParam(':start_date', $old_data['start_date']);
	$sql_new->bindParam(':get_birthday_sms', $old_data['get_birthday_sms']);
	$sql_new->bindParam(':active', $old_data['active']);
	$sql_new->bindParam(':date_active', $old_data['date_active']);
	$sql_new->bindParam(':paid', $old_data['paid']);
	$sql_new->bindParam(':deogram', $old_data['deogram']);
	$sql_new->bindParam(':reminder_date', $old_data['reminder_date']);
	$sql_new->bindParam(':username', $old_data['username']);
	$sql_new->bindParam(':password', $password);
	$sql_new->execute();
}


?>
