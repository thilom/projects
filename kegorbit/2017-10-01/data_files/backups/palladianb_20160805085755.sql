DROP TABLE keg_palladianb_customers;

CREATE TABLE `keg_palladianb_customers` (
  `customer_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer_contact` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_contact_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_contact_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO keg_palladianb_customers VALUES("1","Test Test (PTY) LTD","Test","456 4565645","thilo@palladianbytes.co.za","","");




