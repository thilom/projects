DROP TABLE keg_mainstream_customers;

CREATE TABLE `keg_mainstream_customers` (
  `customer_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer_contact` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_contact_number` varbinary(100) DEFAULT NULL,
  `customer_contact_email` varbinary(100) DEFAULT NULL,
  `inactive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_physical_address1` varbinary(200) DEFAULT NULL,
  `customer_physical_address2` varbinary(200) DEFAULT NULL,
  `customer_physical_address3` varbinary(200) DEFAULT NULL,
  `customer_physical_address4` varbinary(200) DEFAULT NULL,
  `customer_postal_address1` varbinary(200) DEFAULT NULL,
  `customer_postal_address2` varbinary(200) DEFAULT NULL,
  `customer_postal_address3` varbinary(200) DEFAULT NULL,
  `customer_postal_address4` varbinary(200) DEFAULT NULL,
  `same_as_physical` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO keg_mainstream_customers VALUES("1","Mavungana","","n^f0�7j�����F��","n^f0�7j�����F��","","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","");

INSERT INTO keg_mainstream_customers VALUES("2","Sean","","n^f0�7j�����F��","n^f0�7j�����F��","Y","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","");

INSERT INTO keg_mainstream_customers VALUES("3","Sales-2 (Sean Kemp)","Sean Kemp"," wU�b��4��o��","�C$r7�u�1������iD:@<(����NM�","Y","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("4","BeerHouse Fourways","Chris Rall"," wU�b��4��o��","�\\.�өJ�˥�y-𮳠�v�l�A�c�\n���","","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("5","empty","","n^f0�7j�����F��","n^f0�7j�����F��","","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","");

INSERT INTO keg_mainstream_customers VALUES("6","Toni\'s Pizza","","n^f0�7j�����F��","n^f0�7j�����F��","","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","");

INSERT INTO keg_mainstream_customers VALUES("7","Broodhuijs","Monja Lingenfelder","΋��B~6Io9�","c|�1�\n�QT��,�UkI�Z�	<U�������","","","���X��87�s\0dg���偃�G��|���x","��$8j]���%z�5K�","�-[���^�v��%��","n^f0�7j�����F��","","","","","1");

INSERT INTO keg_mainstream_customers VALUES("8","GA Rouge Restaurant and Wine Cellar (Pty) Ltd","Janus Smith","�3��K���iԓ��","�\0��)VD-N�Qw���EU�WQ�� nَ��@","","","�3��p}[�}v&0T$�Б��#�@ߥ���[�9u","\\]��\'ˑ���R�޳��7-4�ݖ�U�(","�2�^мj$}�$+k�}�eɛQa�!�9D�vy","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("9","Machics","Dirk","VOK�����C#4��","���޺�7�,�W���-H�(�L�mu�S","","","�����\n������ο�\'�Nl=��ʾ�d�rmH�ݾ�k])�t��","D�P\"EU��	V٘T","w�+>o�	�5�0}qr","n^f0�7j�����F��","","","","","1");

INSERT INTO keg_mainstream_customers VALUES("10","Oze Cafe and Bistro cc","Marco Ferreira","� DPv\")V��/�͘ֆ","����5�n�D�r�\nY�]*��UKn�,��","","","�n������D�`�t��/\'X�[ad�","�	p�e��zO�n�9","w�+>o�	�5�0}qr","n^f0�7j�����F��","","","","","1");

INSERT INTO keg_mainstream_customers VALUES("11","Sales-3 (Adrian Barnes)","Adrian Barnes","SHG��ks!L,ìQq{","�LՇr�=���d�@E����7���	�","","","�Y�=�/3�>׈�Ռ","��r\\�o��$Y(��J���p�z��O9\"","�#(�Z�)���H�","w�+>o�	�5�0}qr","","","","","1");

INSERT INTO keg_mainstream_customers VALUES("12","Beerhouse Centurion","Suralda","�fZWV�nH)Z�^q>��","�����p����X��6:E	�0���di\'G�U$","","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("13","Sales (Carel Malherbe)","Carel Malherbe"," wU�b��4��o��","���bjd�	ֱ!�{\"�b~2�P��]>�.? U-","","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("14","Fishermans Deck","Casper Kruger","N�a\'E��s/�`����","�Cd�/K�<TY����r�y۴TmpdF���⍩","","","S�\"X�C	T�,��*","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("15","Kudo\'s Craft Cafe","Kim Maphefo","f��7�#�p�d����","*&���{.���L�\\����t�Τ\0�cnu��~","","","Z�1�w/��=�N�T�A�ڬWS��\"�d��XyY","ͩHU��\nD7_\n��FH��J���p�z��O9\"","�-[���^�v��%��","w�+>o�	�5�0}qr","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("16","The Lounge","Andre Niehuas","bB\"���Ɵ�a�4��","Hqh�AG�\'̊�������t��0�˽I2O�A","","","�3��-P�����[B���A����tk69���","_k�����J\'�ԁg��","�Z�mɧ�fޜ�$&","w�+>o�	�5�0}qr","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("17","Green Olive Restaurant","Danie","�K\'�������zL��i","�ʂ���p��\'���]K�����Ղ�T���","","","gBa���������<�Ri�|�\"�yR>-","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","","","","","1");

INSERT INTO keg_mainstream_customers VALUES("18","Stanley Beer Yard","Mark","Ǭ�3�I��^�>l","y��q�?`��|+��K�����Ղ�T���","","","n���}��\'��s�l�","�E-��X�F�u�����","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("19","Buffelspoort ATKV","Frikkie","��;�\nu�Ŕ]���Q","����}0��������&�6�n�q��Fm","","","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("20","Reinhard Spiwak","Reinard Spiwak","�?���@z�]mD�4�","O!\nh�.F��+���hh�7��+K�����","","","�����:cF_]t������\0�>t�Qe~�:�X�","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("21","The Higher Ground","Mark Burger","�;�;N�u+Ki���","��-��as����������ߚ�^q��gg�","","","����i`��,a�wa�H=���B`��$uP�?R\"��\nl�ԧ˙;?{\0","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("22","PHSOB Club","Louis Legat","HG�ū��/��#]`t-","���a�+��^���oB\"�Ţd�﫵����FC�","","","�9EG��r\0<8Ȝ�DX�Jy%�_��c�+�L�","���D�̦PJ=h�i�","w�+>o�	�5�0}qr","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");

INSERT INTO keg_mainstream_customers VALUES("23","Pirates Club","Aaron Mthembo","nm�/���<�3h޻","HѤ�(ܝ�^�a�ζ�hh�7��+K�����","","","�j[��/��9E:��-�n^f0�7j�����F��","���i�!q���A�])","��d�_�g0���2","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","n^f0�7j�����F��","0");




DROP TABLE keg_mainstream_container_history;

CREATE TABLE `keg_mainstream_container_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_serial_number` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `history_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `history_action` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1812 DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_container_history VALUES("1","1","43","2016-10-04 21:00:49","Created");

INSERT INTO keg_mainstream_container_history VALUES("2","1","43","2016-10-04 21:00:49","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("3","2","43","2016-10-04 21:00:49","Created");

INSERT INTO keg_mainstream_container_history VALUES("4","2","43","2016-10-04 21:00:49","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("5","3","43","2016-10-04 21:00:50","Created");

INSERT INTO keg_mainstream_container_history VALUES("6","3","43","2016-10-04 21:00:50","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("7","4","43","2016-10-04 21:00:50","Created");

INSERT INTO keg_mainstream_container_history VALUES("8","4","43","2016-10-04 21:00:50","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("9","5","43","2016-10-04 21:00:50","Created");

INSERT INTO keg_mainstream_container_history VALUES("10","5","43","2016-10-04 21:00:50","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("11","6","43","2016-10-04 21:00:50","Created");

INSERT INTO keg_mainstream_container_history VALUES("12","6","43","2016-10-04 21:00:50","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("13","7","43","2016-10-04 21:00:50","Created");

INSERT INTO keg_mainstream_container_history VALUES("14","7","43","2016-10-04 21:00:50","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("15","8","43","2016-10-04 21:00:51","Created");

INSERT INTO keg_mainstream_container_history VALUES("16","8","43","2016-10-04 21:00:51","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("17","9","43","2016-10-04 21:00:51","Created");

INSERT INTO keg_mainstream_container_history VALUES("18","9","43","2016-10-04 21:00:51","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("19","10","43","2016-10-04 21:00:52","Created");

INSERT INTO keg_mainstream_container_history VALUES("20","10","43","2016-10-04 21:00:52","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("21","11","43","2016-10-04 21:00:52","Created");

INSERT INTO keg_mainstream_container_history VALUES("22","11","43","2016-10-04 21:00:52","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("23","12","43","2016-10-04 21:00:52","Created");

INSERT INTO keg_mainstream_container_history VALUES("24","12","43","2016-10-04 21:00:52","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("25","13","43","2016-10-04 21:00:53","Created");

INSERT INTO keg_mainstream_container_history VALUES("26","13","43","2016-10-04 21:00:53","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("27","14","43","2016-10-04 21:00:53","Created");

INSERT INTO keg_mainstream_container_history VALUES("28","14","43","2016-10-04 21:00:53","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("29","15","43","2016-10-04 21:00:53","Created");

INSERT INTO keg_mainstream_container_history VALUES("30","15","43","2016-10-04 21:00:53","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("31","16","43","2016-10-04 21:00:54","Created");

INSERT INTO keg_mainstream_container_history VALUES("32","16","43","2016-10-04 21:00:54","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("33","17","43","2016-10-04 21:00:54","Created");

INSERT INTO keg_mainstream_container_history VALUES("34","17","43","2016-10-04 21:00:54","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("35","18","43","2016-10-04 21:00:55","Created");

INSERT INTO keg_mainstream_container_history VALUES("36","18","43","2016-10-04 21:00:55","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("37","19","43","2016-10-04 21:00:55","Created");

INSERT INTO keg_mainstream_container_history VALUES("38","19","43","2016-10-04 21:00:55","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("39","20","43","2016-10-04 21:00:56","Created");

INSERT INTO keg_mainstream_container_history VALUES("40","20","43","2016-10-04 21:00:56","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("41","21","43","2016-10-04 21:00:57","Created");

INSERT INTO keg_mainstream_container_history VALUES("42","21","43","2016-10-04 21:00:57","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("43","22","43","2016-10-04 21:00:57","Created");

INSERT INTO keg_mainstream_container_history VALUES("44","22","43","2016-10-04 21:00:57","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("45","23","43","2016-10-04 21:00:57","Created");

INSERT INTO keg_mainstream_container_history VALUES("46","23","43","2016-10-04 21:00:57","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("47","24","43","2016-10-04 21:00:58","Created");

INSERT INTO keg_mainstream_container_history VALUES("48","24","43","2016-10-04 21:00:58","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("49","25","43","2016-10-04 21:00:58","Created");

INSERT INTO keg_mainstream_container_history VALUES("50","25","43","2016-10-04 21:00:58","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("51","26","43","2016-10-04 21:00:58","Created");

INSERT INTO keg_mainstream_container_history VALUES("52","26","43","2016-10-04 21:00:58","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("53","27","43","2016-10-04 21:00:59","Created");

INSERT INTO keg_mainstream_container_history VALUES("54","27","43","2016-10-04 21:00:59","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("55","28","43","2016-10-04 21:00:59","Created");

INSERT INTO keg_mainstream_container_history VALUES("56","28","43","2016-10-04 21:00:59","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("57","29","43","2016-10-04 21:00:59","Created");

INSERT INTO keg_mainstream_container_history VALUES("58","29","43","2016-10-04 21:00:59","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("59","30","43","2016-10-04 21:00:59","Created");

INSERT INTO keg_mainstream_container_history VALUES("60","30","43","2016-10-04 21:00:59","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("61","31","43","2016-10-04 21:01:00","Created");

INSERT INTO keg_mainstream_container_history VALUES("62","31","43","2016-10-04 21:01:00","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("63","32","43","2016-10-04 21:01:01","Created");

INSERT INTO keg_mainstream_container_history VALUES("64","32","43","2016-10-04 21:01:01","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("65","33","43","2016-10-04 21:01:01","Created");

INSERT INTO keg_mainstream_container_history VALUES("66","33","43","2016-10-04 21:01:01","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("67","34","43","2016-10-04 21:01:01","Created");

INSERT INTO keg_mainstream_container_history VALUES("68","34","43","2016-10-04 21:01:01","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("69","35","43","2016-10-04 21:01:02","Created");

INSERT INTO keg_mainstream_container_history VALUES("70","35","43","2016-10-04 21:01:02","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("71","36","43","2016-10-04 21:01:02","Created");

INSERT INTO keg_mainstream_container_history VALUES("72","36","43","2016-10-04 21:01:02","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("73","37","43","2016-10-04 21:01:02","Created");

INSERT INTO keg_mainstream_container_history VALUES("74","37","43","2016-10-04 21:01:02","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("75","38","43","2016-10-04 21:01:03","Created");

INSERT INTO keg_mainstream_container_history VALUES("76","38","43","2016-10-04 21:01:03","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("77","39","43","2016-10-04 21:01:03","Created");

INSERT INTO keg_mainstream_container_history VALUES("78","39","43","2016-10-04 21:01:03","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("79","40","43","2016-10-04 21:01:03","Created");

INSERT INTO keg_mainstream_container_history VALUES("80","40","43","2016-10-04 21:01:03","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("81","41","43","2016-10-04 21:01:04","Created");

INSERT INTO keg_mainstream_container_history VALUES("82","41","43","2016-10-04 21:01:04","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("83","42","43","2016-10-04 21:01:04","Created");

INSERT INTO keg_mainstream_container_history VALUES("84","42","43","2016-10-04 21:01:04","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("85","43","43","2016-10-04 21:01:04","Created");

INSERT INTO keg_mainstream_container_history VALUES("86","43","43","2016-10-04 21:01:04","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("87","44","43","2016-10-04 21:01:05","Created");

INSERT INTO keg_mainstream_container_history VALUES("88","44","43","2016-10-04 21:01:05","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("89","45","43","2016-10-04 21:01:05","Created");

INSERT INTO keg_mainstream_container_history VALUES("90","45","43","2016-10-04 21:01:05","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("91","46","43","2016-10-04 21:01:05","Created");

INSERT INTO keg_mainstream_container_history VALUES("92","46","43","2016-10-04 21:01:05","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("93","47","43","2016-10-04 21:01:06","Created");

INSERT INTO keg_mainstream_container_history VALUES("94","47","43","2016-10-04 21:01:06","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("95","48","43","2016-10-04 21:01:06","Created");

INSERT INTO keg_mainstream_container_history VALUES("96","48","43","2016-10-04 21:01:06","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("97","49","43","2016-10-04 21:01:07","Created");

INSERT INTO keg_mainstream_container_history VALUES("98","49","43","2016-10-04 21:01:07","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("99","50","43","2016-10-04 21:01:07","Created");

INSERT INTO keg_mainstream_container_history VALUES("100","50","43","2016-10-04 21:01:07","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("101","51","43","2016-10-04 21:01:08","Created");

INSERT INTO keg_mainstream_container_history VALUES("102","51","43","2016-10-04 21:01:08","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("103","52","43","2016-10-04 21:01:08","Created");

INSERT INTO keg_mainstream_container_history VALUES("104","52","43","2016-10-04 21:01:08","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("105","53","43","2016-10-04 21:01:08","Created");

INSERT INTO keg_mainstream_container_history VALUES("106","53","43","2016-10-04 21:01:08","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("107","54","43","2016-10-04 21:01:08","Created");

INSERT INTO keg_mainstream_container_history VALUES("108","54","43","2016-10-04 21:01:08","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("109","55","43","2016-10-04 21:01:08","Created");

INSERT INTO keg_mainstream_container_history VALUES("110","55","43","2016-10-04 21:01:08","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("111","56","43","2016-10-04 21:01:08","Created");

INSERT INTO keg_mainstream_container_history VALUES("112","56","43","2016-10-04 21:01:08","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("113","57","43","2016-10-04 21:01:09","Created");

INSERT INTO keg_mainstream_container_history VALUES("114","57","43","2016-10-04 21:01:09","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("115","58","43","2016-10-04 21:01:12","Created");

INSERT INTO keg_mainstream_container_history VALUES("116","58","43","2016-10-04 21:01:12","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("117","59","43","2016-10-04 21:01:12","Created");

INSERT INTO keg_mainstream_container_history VALUES("118","59","43","2016-10-04 21:01:12","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("119","60","43","2016-10-04 21:01:12","Created");

INSERT INTO keg_mainstream_container_history VALUES("120","60","43","2016-10-04 21:01:12","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("121","61","43","2016-10-04 21:01:12","Created");

INSERT INTO keg_mainstream_container_history VALUES("122","61","43","2016-10-04 21:01:12","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("123","62","43","2016-10-04 21:01:13","Created");

INSERT INTO keg_mainstream_container_history VALUES("124","62","43","2016-10-04 21:01:13","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("125","63","43","2016-10-04 21:01:13","Created");

INSERT INTO keg_mainstream_container_history VALUES("126","63","43","2016-10-04 21:01:13","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("127","64","43","2016-10-04 21:01:13","Created");

INSERT INTO keg_mainstream_container_history VALUES("128","64","43","2016-10-04 21:01:13","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("129","65","43","2016-10-04 21:01:14","Created");

INSERT INTO keg_mainstream_container_history VALUES("130","65","43","2016-10-04 21:01:14","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("131","66","43","2016-10-04 21:01:14","Created");

INSERT INTO keg_mainstream_container_history VALUES("132","66","43","2016-10-04 21:01:14","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("133","67","43","2016-10-04 21:01:14","Created");

INSERT INTO keg_mainstream_container_history VALUES("134","67","43","2016-10-04 21:01:14","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("135","68","43","2016-10-04 21:01:15","Created");

INSERT INTO keg_mainstream_container_history VALUES("136","68","43","2016-10-04 21:01:15","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("137","69","43","2016-10-04 21:01:15","Created");

INSERT INTO keg_mainstream_container_history VALUES("138","69","43","2016-10-04 21:01:15","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("139","70","43","2016-10-04 21:01:16","Created");

INSERT INTO keg_mainstream_container_history VALUES("140","70","43","2016-10-04 21:01:16","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("141","71","43","2016-10-04 21:01:16","Created");

INSERT INTO keg_mainstream_container_history VALUES("142","71","43","2016-10-04 21:01:16","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("143","72","43","2016-10-04 21:01:16","Created");

INSERT INTO keg_mainstream_container_history VALUES("144","72","43","2016-10-04 21:01:16","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("145","73","43","2016-10-04 21:01:16","Created");

INSERT INTO keg_mainstream_container_history VALUES("146","73","43","2016-10-04 21:01:16","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("147","74","43","2016-10-04 21:01:17","Created");

INSERT INTO keg_mainstream_container_history VALUES("148","74","43","2016-10-04 21:01:17","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("149","75","43","2016-10-04 21:01:17","Created");

INSERT INTO keg_mainstream_container_history VALUES("150","75","43","2016-10-04 21:01:17","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("151","76","43","2016-10-04 21:01:18","Created");

INSERT INTO keg_mainstream_container_history VALUES("152","76","43","2016-10-04 21:01:18","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("153","77","43","2016-10-04 21:01:18","Created");

INSERT INTO keg_mainstream_container_history VALUES("154","77","43","2016-10-04 21:01:18","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("155","78","43","2016-10-04 21:01:19","Created");

INSERT INTO keg_mainstream_container_history VALUES("156","78","43","2016-10-04 21:01:19","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("157","79","43","2016-10-04 21:01:19","Created");

INSERT INTO keg_mainstream_container_history VALUES("158","79","43","2016-10-04 21:01:19","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("159","80","43","2016-10-04 21:01:19","Created");

INSERT INTO keg_mainstream_container_history VALUES("160","80","43","2016-10-04 21:01:19","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("161","81","43","2016-10-04 21:01:20","Created");

INSERT INTO keg_mainstream_container_history VALUES("162","81","43","2016-10-04 21:01:20","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("163","82","43","2016-10-04 21:01:20","Created");

INSERT INTO keg_mainstream_container_history VALUES("164","82","43","2016-10-04 21:01:20","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("165","83","43","2016-10-04 21:01:20","Created");

INSERT INTO keg_mainstream_container_history VALUES("166","83","43","2016-10-04 21:01:20","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("167","84","43","2016-10-04 21:01:20","Created");

INSERT INTO keg_mainstream_container_history VALUES("168","84","43","2016-10-04 21:01:20","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("169","85","43","2016-10-04 21:01:20","Created");

INSERT INTO keg_mainstream_container_history VALUES("170","85","43","2016-10-04 21:01:20","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("171","86","43","2016-10-04 21:01:20","Created");

INSERT INTO keg_mainstream_container_history VALUES("172","86","43","2016-10-04 21:01:20","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("173","87","43","2016-10-04 21:01:21","Created");

INSERT INTO keg_mainstream_container_history VALUES("174","87","43","2016-10-04 21:01:21","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("175","88","43","2016-10-04 21:01:23","Created");

INSERT INTO keg_mainstream_container_history VALUES("176","88","43","2016-10-04 21:01:24","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("177","89","43","2016-10-04 21:01:24","Created");

INSERT INTO keg_mainstream_container_history VALUES("178","89","43","2016-10-04 21:01:25","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("179","90","43","2016-10-04 21:01:25","Created");

INSERT INTO keg_mainstream_container_history VALUES("180","90","43","2016-10-04 21:01:25","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("181","91","43","2016-10-04 21:01:26","Created");

INSERT INTO keg_mainstream_container_history VALUES("182","91","43","2016-10-04 21:01:26","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("183","92","43","2016-10-04 21:01:26","Created");

INSERT INTO keg_mainstream_container_history VALUES("184","92","43","2016-10-04 21:01:26","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("185","93","43","2016-10-04 21:01:26","Created");

INSERT INTO keg_mainstream_container_history VALUES("186","93","43","2016-10-04 21:01:26","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("187","94","43","2016-10-04 21:01:27","Created");

INSERT INTO keg_mainstream_container_history VALUES("188","94","43","2016-10-04 21:01:27","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("189","95","43","2016-10-04 21:01:27","Created");

INSERT INTO keg_mainstream_container_history VALUES("190","95","43","2016-10-04 21:01:27","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("191","96","43","2016-10-04 21:01:28","Created");

INSERT INTO keg_mainstream_container_history VALUES("192","96","43","2016-10-04 21:01:28","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("193","97","43","2016-10-04 21:01:30","Created");

INSERT INTO keg_mainstream_container_history VALUES("194","97","43","2016-10-04 21:01:30","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("195","98","43","2016-10-04 21:01:30","Created");

INSERT INTO keg_mainstream_container_history VALUES("196","98","43","2016-10-04 21:01:30","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("197","99","43","2016-10-04 21:01:31","Created");

INSERT INTO keg_mainstream_container_history VALUES("198","99","43","2016-10-04 21:01:31","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("199","100","43","2016-10-04 21:01:31","Created");

INSERT INTO keg_mainstream_container_history VALUES("200","100","43","2016-10-04 21:01:31","Imported From CSV");

INSERT INTO keg_mainstream_container_history VALUES("201","16","43","2016-10-06 07:12:41","Updated");

INSERT INTO keg_mainstream_container_history VALUES("202","18","45","2016-10-06 17:14:53","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("203","18","45","2016-10-06 17:23:18","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("204","18","45","2016-10-06 17:24:47","Updated to \'Filled\' Stage (Product: )");

INSERT INTO keg_mainstream_container_history VALUES("205","18","45","2016-10-06 17:25:12","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("206","18","43","2016-10-06 18:35:20","Updated");

INSERT INTO keg_mainstream_container_history VALUES("207","1\'>1</a>","43","2016-10-09 18:40:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("208","6\'>6</a>","43","2016-10-09 18:40:51","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("209","11\'>11</a>","43","2016-10-09 18:40:51","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("210","25\'>25</a>","43","2016-10-09 18:40:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("211","47\'>47</a>","43","2016-10-09 18:40:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("212","34\'>34</a>","43","2016-10-09 18:40:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("213","62\'>62</a>","43","2016-10-09 18:40:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("214","71\'>71</a>","43","2016-10-09 18:40:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("215","84\'>84</a>","43","2016-10-09 18:40:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("216","85\'>85</a>","43","2016-10-09 18:40:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("217","1","43","2016-10-09 18:47:28","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("218","6","43","2016-10-09 18:47:28","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("219","62","43","2016-10-09 18:47:28","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("220","25","43","2016-10-09 18:47:28","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("221","84","43","2016-10-09 18:47:28","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("222","34","43","2016-10-09 18:47:28","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("223","11","43","2016-10-09 18:47:28","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("224","85","43","2016-10-09 18:47:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("225","71","43","2016-10-09 18:47:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("226","47","43","2016-10-09 18:47:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("227","53","43","2016-10-09 18:50:54","Modified Spear for removal");

INSERT INTO keg_mainstream_container_history VALUES("228","2","43","2016-10-09 18:53:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("229","60","43","2016-10-09 18:53:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("230","77","43","2016-10-09 18:53:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("231","43","43","2016-10-09 18:53:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("232","86","43","2016-10-09 18:53:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("233","79","43","2016-10-09 18:53:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("234","32","43","2016-10-09 18:53:09","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("235","49","43","2016-10-09 18:53:09","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("236","30","43","2016-10-09 18:53:09","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("237","78","43","2016-10-09 18:53:10","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("238","21","43","2016-10-09 19:00:57","Updated");

INSERT INTO keg_mainstream_container_history VALUES("239","36","43","2016-10-09 19:01:28","Updated");

INSERT INTO keg_mainstream_container_history VALUES("240","22","43","2016-10-09 19:04:19","Updated");

INSERT INTO keg_mainstream_container_history VALUES("241","23","43","2016-10-09 19:04:40","Updated");

INSERT INTO keg_mainstream_container_history VALUES("242","1","43","2016-10-10 12:11:08","Updated");

INSERT INTO keg_mainstream_container_history VALUES("243","2","43","2016-10-10 12:11:31","Updated");

INSERT INTO keg_mainstream_container_history VALUES("244","3","43","2016-10-10 12:12:11","Updated");

INSERT INTO keg_mainstream_container_history VALUES("245","5","43","2016-10-10 12:12:45","Updated");

INSERT INTO keg_mainstream_container_history VALUES("246","6","43","2016-10-10 12:13:07","Updated");

INSERT INTO keg_mainstream_container_history VALUES("247","7","43","2016-10-10 12:13:21","Updated");

INSERT INTO keg_mainstream_container_history VALUES("248","8","43","2016-10-10 12:13:30","Updated");

INSERT INTO keg_mainstream_container_history VALUES("249","10","43","2016-10-10 12:13:45","Updated");

INSERT INTO keg_mainstream_container_history VALUES("250","11","43","2016-10-10 12:14:28","Updated");

INSERT INTO keg_mainstream_container_history VALUES("251","13","43","2016-10-10 12:14:42","Updated");

INSERT INTO keg_mainstream_container_history VALUES("252","19","43","2016-10-10 12:14:56","Updated");

INSERT INTO keg_mainstream_container_history VALUES("253","24","43","2016-10-10 12:15:12","Updated");

INSERT INTO keg_mainstream_container_history VALUES("254","25","43","2016-10-10 12:15:29","Updated");

INSERT INTO keg_mainstream_container_history VALUES("255","53","43","2016-10-10 12:17:04","Updated");

INSERT INTO keg_mainstream_container_history VALUES("256","26","43","2016-10-10 12:18:01","Updated");

INSERT INTO keg_mainstream_container_history VALUES("257","29","43","2016-10-10 12:18:13","Updated");

INSERT INTO keg_mainstream_container_history VALUES("258","30","43","2016-10-10 12:18:28","Updated");

INSERT INTO keg_mainstream_container_history VALUES("259","15","43","2016-10-10 12:18:40","Updated");

INSERT INTO keg_mainstream_container_history VALUES("260","9","43","2016-10-10 12:18:49","Updated");

INSERT INTO keg_mainstream_container_history VALUES("261","12","43","2016-10-10 12:19:00","Updated");

INSERT INTO keg_mainstream_container_history VALUES("262","17","43","2016-10-10 12:19:11","Updated");

INSERT INTO keg_mainstream_container_history VALUES("263","20","43","2016-10-10 12:19:32","Updated");

INSERT INTO keg_mainstream_container_history VALUES("264","27","43","2016-10-10 12:19:45","Updated");

INSERT INTO keg_mainstream_container_history VALUES("265","28","43","2016-10-10 12:19:58","Updated");

INSERT INTO keg_mainstream_container_history VALUES("266","31","43","2016-10-10 12:20:12","Updated");

INSERT INTO keg_mainstream_container_history VALUES("267","32","43","2016-10-10 12:20:26","Updated");

INSERT INTO keg_mainstream_container_history VALUES("268","33","43","2016-10-10 12:20:37","Updated");

INSERT INTO keg_mainstream_container_history VALUES("269","34","43","2016-10-10 12:21:08","Updated");

INSERT INTO keg_mainstream_container_history VALUES("270","35","43","2016-10-10 12:21:32","Updated");

INSERT INTO keg_mainstream_container_history VALUES("271","37","43","2016-10-10 12:22:02","Updated");

INSERT INTO keg_mainstream_container_history VALUES("272","38","43","2016-10-10 12:22:16","Updated");

INSERT INTO keg_mainstream_container_history VALUES("273","39","43","2016-10-10 12:22:26","Updated");

INSERT INTO keg_mainstream_container_history VALUES("274","40","43","2016-10-10 12:22:42","Updated");

INSERT INTO keg_mainstream_container_history VALUES("275","41","43","2016-10-10 12:22:54","Updated");

INSERT INTO keg_mainstream_container_history VALUES("276","42","43","2016-10-10 12:23:04","Updated");

INSERT INTO keg_mainstream_container_history VALUES("277","43","43","2016-10-10 12:23:16","Updated");

INSERT INTO keg_mainstream_container_history VALUES("278","44","43","2016-10-10 12:23:29","Updated");

INSERT INTO keg_mainstream_container_history VALUES("279","45","43","2016-10-10 12:23:41","Updated");

INSERT INTO keg_mainstream_container_history VALUES("280","46","43","2016-10-10 12:23:52","Updated");

INSERT INTO keg_mainstream_container_history VALUES("281","47","43","2016-10-10 12:24:08","Updated");

INSERT INTO keg_mainstream_container_history VALUES("282","48","43","2016-10-10 12:24:16","Updated");

INSERT INTO keg_mainstream_container_history VALUES("283","49","43","2016-10-10 12:24:35","Updated");

INSERT INTO keg_mainstream_container_history VALUES("284","50","43","2016-10-10 12:25:16","Updated");

INSERT INTO keg_mainstream_container_history VALUES("285","51","43","2016-10-10 12:25:27","Updated");

INSERT INTO keg_mainstream_container_history VALUES("286","52","43","2016-10-10 12:25:37","Updated");

INSERT INTO keg_mainstream_container_history VALUES("287","54","43","2016-10-10 12:25:51","Updated");

INSERT INTO keg_mainstream_container_history VALUES("288","55","43","2016-10-10 12:26:03","Updated");

INSERT INTO keg_mainstream_container_history VALUES("289","56","43","2016-10-10 12:26:33","Updated");

INSERT INTO keg_mainstream_container_history VALUES("290","57","43","2016-10-10 12:26:45","Updated");

INSERT INTO keg_mainstream_container_history VALUES("291","58","43","2016-10-10 12:26:59","Updated");

INSERT INTO keg_mainstream_container_history VALUES("292","59","43","2016-10-10 12:27:13","Updated");

INSERT INTO keg_mainstream_container_history VALUES("293","92","43","2016-10-10 12:29:41","Updated");

INSERT INTO keg_mainstream_container_history VALUES("294","92","43","2016-10-10 12:30:31","Updated");

INSERT INTO keg_mainstream_container_history VALUES("295","93","43","2016-10-10 12:30:51","Updated");

INSERT INTO keg_mainstream_container_history VALUES("296","92","43","2016-10-10 12:31:55","Updated");

INSERT INTO keg_mainstream_container_history VALUES("297","93","43","2016-10-10 12:32:12","Updated");

INSERT INTO keg_mainstream_container_history VALUES("298","94","43","2016-10-10 12:32:29","Updated");

INSERT INTO keg_mainstream_container_history VALUES("299","95","43","2016-10-10 12:32:46","Updated");

INSERT INTO keg_mainstream_container_history VALUES("300","96","43","2016-10-10 12:33:02","Updated");

INSERT INTO keg_mainstream_container_history VALUES("301","97","43","2016-10-10 12:33:18","Updated");

INSERT INTO keg_mainstream_container_history VALUES("302","98","43","2016-10-10 12:33:32","Updated");

INSERT INTO keg_mainstream_container_history VALUES("303","99","43","2016-10-10 12:33:46","Updated");

INSERT INTO keg_mainstream_container_history VALUES("304","100","43","2016-10-10 12:33:58","Updated");

INSERT INTO keg_mainstream_container_history VALUES("305","60","43","2016-10-10 12:35:00","Updated");

INSERT INTO keg_mainstream_container_history VALUES("306","62","43","2016-10-10 12:35:52","Updated");

INSERT INTO keg_mainstream_container_history VALUES("307","63","43","2016-10-10 12:36:36","Updated");

INSERT INTO keg_mainstream_container_history VALUES("308","64","43","2016-10-10 12:39:23","Updated");

INSERT INTO keg_mainstream_container_history VALUES("309","65","43","2016-10-10 12:39:36","Updated");

INSERT INTO keg_mainstream_container_history VALUES("310","66","43","2016-10-10 12:39:47","Updated");

INSERT INTO keg_mainstream_container_history VALUES("311","67","43","2016-10-10 12:40:03","Updated");

INSERT INTO keg_mainstream_container_history VALUES("312","68","43","2016-10-10 12:40:17","Updated");

INSERT INTO keg_mainstream_container_history VALUES("313","69","43","2016-10-10 12:40:30","Updated");

INSERT INTO keg_mainstream_container_history VALUES("314","70","43","2016-10-10 12:40:41","Updated");

INSERT INTO keg_mainstream_container_history VALUES("315","71","43","2016-10-10 12:40:54","Updated");

INSERT INTO keg_mainstream_container_history VALUES("316","72","43","2016-10-10 12:41:06","Updated");

INSERT INTO keg_mainstream_container_history VALUES("317","73","43","2016-10-10 12:41:14","Updated");

INSERT INTO keg_mainstream_container_history VALUES("318","74","43","2016-10-10 12:41:24","Updated");

INSERT INTO keg_mainstream_container_history VALUES("319","75","43","2016-10-10 12:41:36","Updated");

INSERT INTO keg_mainstream_container_history VALUES("320","76","43","2016-10-10 12:41:46","Updated");

INSERT INTO keg_mainstream_container_history VALUES("321","77","43","2016-10-10 12:42:07","Updated");

INSERT INTO keg_mainstream_container_history VALUES("322","78","43","2016-10-10 12:42:18","Updated");

INSERT INTO keg_mainstream_container_history VALUES("323","79","43","2016-10-10 12:42:49","Updated");

INSERT INTO keg_mainstream_container_history VALUES("324","80","43","2016-10-10 12:55:18","Updated");

INSERT INTO keg_mainstream_container_history VALUES("325","81","43","2016-10-10 12:56:14","Updated");

INSERT INTO keg_mainstream_container_history VALUES("326","82","43","2016-10-10 12:56:31","Updated");

INSERT INTO keg_mainstream_container_history VALUES("327","83","43","2016-10-10 12:56:43","Updated");

INSERT INTO keg_mainstream_container_history VALUES("328","84","43","2016-10-10 12:56:52","Updated");

INSERT INTO keg_mainstream_container_history VALUES("329","85","43","2016-10-10 12:57:02","Updated");

INSERT INTO keg_mainstream_container_history VALUES("330","86","43","2016-10-10 12:57:14","Updated");

INSERT INTO keg_mainstream_container_history VALUES("331","87","43","2016-10-10 12:57:29","Updated");

INSERT INTO keg_mainstream_container_history VALUES("332","88","43","2016-10-10 12:57:39","Updated");

INSERT INTO keg_mainstream_container_history VALUES("333","89","43","2016-10-10 12:57:48","Updated");

INSERT INTO keg_mainstream_container_history VALUES("334","90","43","2016-10-10 12:57:59","Updated");

INSERT INTO keg_mainstream_container_history VALUES("335","91","43","2016-10-10 12:58:09","Updated");

INSERT INTO keg_mainstream_container_history VALUES("336","70","43","2016-10-10 12:58:21","Updated");

INSERT INTO keg_mainstream_container_history VALUES("337","71","43","2016-10-10 12:58:35","Updated");

INSERT INTO keg_mainstream_container_history VALUES("338","72","43","2016-10-10 12:58:53","Updated");

INSERT INTO keg_mainstream_container_history VALUES("339","73","43","2016-10-10 12:59:05","Updated");

INSERT INTO keg_mainstream_container_history VALUES("340","74","43","2016-10-10 12:59:18","Updated");

INSERT INTO keg_mainstream_container_history VALUES("341","75","43","2016-10-10 12:59:35","Updated");

INSERT INTO keg_mainstream_container_history VALUES("342","18","45","2016-10-11 07:21:55","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("343","18","45","2016-10-11 07:22:35","Container State Changed to \'Damaged (Repairable)\'");

INSERT INTO keg_mainstream_container_history VALUES("344","18","45","2016-10-11 07:22:57","Container State Changed to \'Damaged (Repairable)\'");

INSERT INTO keg_mainstream_container_history VALUES("345","55","45","2016-10-11 07:23:31","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("346","55","45","2016-10-11 07:24:00","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("347","18","43","2016-10-11 08:45:34","Keg gassy - Returned and swapped out with other keg");

INSERT INTO keg_mainstream_container_history VALUES("348","55","43","2016-10-11 08:46:23","Updated");

INSERT INTO keg_mainstream_container_history VALUES("349","4","43","2016-10-11 11:32:52","Updated");

INSERT INTO keg_mainstream_container_history VALUES("350","13","43","2016-10-11 11:50:32","Updated");

INSERT INTO keg_mainstream_container_history VALUES("351","13","43","2016-10-11 11:50:55","Updated");

INSERT INTO keg_mainstream_container_history VALUES("352","13","43","2016-10-11 11:51:29","Updated");

INSERT INTO keg_mainstream_container_history VALUES("353","12","43","2016-10-11 11:57:01","Updated");

INSERT INTO keg_mainstream_container_history VALUES("354","27","43","2016-10-11 11:57:44","Updated");

INSERT INTO keg_mainstream_container_history VALUES("355","13","43","2016-10-11 15:31:19","Updated");

INSERT INTO keg_mainstream_container_history VALUES("356","4","43","2016-10-11 15:32:05","Updated");

INSERT INTO keg_mainstream_container_history VALUES("357","2","45","2016-10-13 10:40:49","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("358","2","45","2016-10-13 10:41:13","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("359","2","45","2016-10-13 10:41:44","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("360","11","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("361","6","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("362","27","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("363","34","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("364","84","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("365","52","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("366","67","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("367","88","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("368","50","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("369","61","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("370","47","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("371","58","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("372","71","43","2016-10-13 21:29:49","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("373","20","43","2016-10-13 21:30:59","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("374","64","43","2016-10-13 21:30:59","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("375","20","43","2016-10-13 21:31:55","Updated to \'Delivered\' Stage (Customer: BeerHouse)");

INSERT INTO keg_mainstream_container_history VALUES("376","64","43","2016-10-13 21:31:55","Updated to \'Delivered\' Stage (Customer: BeerHouse)");

INSERT INTO keg_mainstream_container_history VALUES("377","83","43","2016-10-13 21:33:30","Updated");

INSERT INTO keg_mainstream_container_history VALUES("378","83","43","2016-10-13 21:34:04","Updated to \'Delivered\' Stage (Customer: BeerHouse)");

INSERT INTO keg_mainstream_container_history VALUES("379","83","43","2016-10-13 21:34:25","Updated to \'Delivered\' Stage (Customer: BeerHouse)");

INSERT INTO keg_mainstream_container_history VALUES("380","38","43","2016-10-13 21:34:25","Updated to \'Delivered\' Stage (Customer: BeerHouse)");

INSERT INTO keg_mainstream_container_history VALUES("381","38\'>38</a>","43","2016-10-13 21:34:55","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("382","83\'>83</a>","43","2016-10-13 21:34:56","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("383","38","43","2016-10-13 21:36:01","Updated");

INSERT INTO keg_mainstream_container_history VALUES("384","83","43","2016-10-13 21:36:20","Updated");

INSERT INTO keg_mainstream_container_history VALUES("385","57","43","2016-10-13 21:38:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("386","62","43","2016-10-13 21:38:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("387","59","43","2016-10-13 21:38:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("388","72","43","2016-10-13 21:38:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("389","80","43","2016-10-13 21:38:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("390","3","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("391","85","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("392","19","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("393","39","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("394","1","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("395","48","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("396","5","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("397","66","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("398","40","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("399","26","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("400","25","43","2016-10-13 21:38:12","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("401","61","43","2016-10-13 21:40:07","Updated");

INSERT INTO keg_mainstream_container_history VALUES("402","14","43","2016-10-13 21:42:19","Updated");

INSERT INTO keg_mainstream_container_history VALUES("403","12","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("404","45","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("405","10","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("406","81","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("407","17","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("408","54","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("409","91","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("410","8","43","2016-10-16 16:55:17","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("411","31","43","2016-10-16 16:57:01","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("412","73","43","2016-10-16 16:57:01","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("413","89","43","2016-10-16 16:57:01","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("414","28","43","2016-10-16 16:58:47","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("415","87","43","2016-10-16 16:58:47","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("416","65","43","2016-10-16 16:58:47","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("417","70","43","2016-10-16 16:58:47","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("418","28","43","2016-10-16 16:59:47","Updated");

INSERT INTO keg_mainstream_container_history VALUES("419","9","43","2016-10-16 17:00:35","Updated");

INSERT INTO keg_mainstream_container_history VALUES("420","9","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("421","46","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("422","15","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("423","35","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("424","44","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("425","16","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("426","63","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("427","42","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("428","90","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("429","29","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("430","7","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("431","37","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("432","56","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("433","33","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("434","14","43","2016-10-16 17:02:49","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("435","15","43","2016-10-16 17:03:28","Updated");

INSERT INTO keg_mainstream_container_history VALUES("436","33","43","2016-10-16 17:03:57","Updated");

INSERT INTO keg_mainstream_container_history VALUES("437","44","43","2016-10-16 17:04:31","Updated");

INSERT INTO keg_mainstream_container_history VALUES("438","12","43","2016-10-16 17:06:00","Updated");

INSERT INTO keg_mainstream_container_history VALUES("439","16","43","2016-10-16 17:06:55","Updated");

INSERT INTO keg_mainstream_container_history VALUES("440","17","43","2016-10-16 17:07:18","Updated");

INSERT INTO keg_mainstream_container_history VALUES("441","63","43","2016-10-16 17:07:42","Updated");

INSERT INTO keg_mainstream_container_history VALUES("442","70","43","2016-10-16 17:08:19","Updated");

INSERT INTO keg_mainstream_container_history VALUES("443","73","43","2016-10-16 17:09:30","Updated");

INSERT INTO keg_mainstream_container_history VALUES("444","41","43","2016-10-16 17:12:23","Updated");

INSERT INTO keg_mainstream_container_history VALUES("445","74","43","2016-10-16 17:13:24","Updated");

INSERT INTO keg_mainstream_container_history VALUES("446","82","43","2016-10-16 17:14:10","Updated");

INSERT INTO keg_mainstream_container_history VALUES("447","51","43","2016-10-18 10:16:06","Updated");

INSERT INTO keg_mainstream_container_history VALUES("448","41","43","2016-10-18 10:17:06","Returned by Sean on 18 October 2016 - Check");

INSERT INTO keg_mainstream_container_history VALUES("449","82","43","2016-10-18 10:17:44","Returned by Sean on 18 October 2016 - Check");

INSERT INTO keg_mainstream_container_history VALUES("450","72","46","2016-10-20 20:19:50","Container State Changed to \'Unused\'");

INSERT INTO keg_mainstream_container_history VALUES("451","17","43","2016-10-25 10:30:03","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("452","51","43","2016-10-25 10:30:03","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("453","4","43","2016-10-25 10:30:03","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("454","38","43","2016-10-25 10:30:03","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("455","86","43","2016-10-25 10:30:03","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("456","29","43","2016-10-25 10:30:04","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("457","83","43","2016-10-25 10:30:04","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("458","72","45","2016-10-26 07:24:10","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("459","72","45","2016-10-26 07:24:30","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("460","28","45","2016-10-26 07:25:23","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("461","28","45","2016-10-26 07:26:21","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("462","13","43","2016-10-26 10:25:03","Updated");

INSERT INTO keg_mainstream_container_history VALUES("463","2","43","2016-11-06 09:15:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("464","55","43","2016-11-06 09:15:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("465","68","43","2016-11-06 09:15:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("466","13","43","2016-11-06 09:15:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("467","55","43","2016-11-06 09:15:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("468","68","43","2016-11-06 09:15:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("469","2","43","2016-11-06 09:15:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("470","54","43","2016-11-06 09:18:39","Container State Changed to \'Contents Used Before -Sample\'");

INSERT INTO keg_mainstream_container_history VALUES("471","59","43","2016-11-06 09:18:39","Container State Changed to \'Contents Used Before -Sample\'");

INSERT INTO keg_mainstream_container_history VALUES("472","31","43","2016-11-06 09:20:09","Container State Changed to \'Contents Used Before -Sample\'");

INSERT INTO keg_mainstream_container_history VALUES("473","49","43","2016-11-06 09:20:48","Updated");

INSERT INTO keg_mainstream_container_history VALUES("474","30","43","2016-11-06 09:22:43","Updated");

INSERT INTO keg_mainstream_container_history VALUES("475","30","43","2016-11-06 09:23:40","Updated");

INSERT INTO keg_mainstream_container_history VALUES("476","78","43","2016-11-06 09:24:16","Updated");

INSERT INTO keg_mainstream_container_history VALUES("477","7","44","2016-11-06 20:28:13","Updated");

INSERT INTO keg_mainstream_container_history VALUES("478","9","44","2016-11-06 20:29:25","Updated");

INSERT INTO keg_mainstream_container_history VALUES("479","15","44","2016-11-06 20:31:21","Updated");

INSERT INTO keg_mainstream_container_history VALUES("480","24","44","2016-11-06 20:36:18","Updated");

INSERT INTO keg_mainstream_container_history VALUES("481","40","44","2016-11-06 20:40:10","Container State Changed to \'Contents Used Before -Sample\'");

INSERT INTO keg_mainstream_container_history VALUES("482","40","44","2016-11-06 20:40:52","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("483","41","44","2016-11-06 20:41:58","Updated");

INSERT INTO keg_mainstream_container_history VALUES("484","44","44","2016-11-06 20:43:27","Updated");

INSERT INTO keg_mainstream_container_history VALUES("485","46","44","2016-11-06 20:44:11","Updated");

INSERT INTO keg_mainstream_container_history VALUES("486","82","44","2016-11-06 20:50:36","Container State Changed to \'Contents Used Before -Sample\'");

INSERT INTO keg_mainstream_container_history VALUES("487","43","44","2016-11-06 20:55:02","Updated");

INSERT INTO keg_mainstream_container_history VALUES("488","32","45","2016-11-14 10:33:13","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("489","32","45","2016-11-14 10:33:40","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("490","12","45","2016-11-14 10:34:58","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("491","30","45","2016-11-14 10:38:19","Updated to \'Delivered\' Stage (Customer: Oźe Cafe & Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("492","30","45","2016-11-14 10:38:44","Updated to \'Delivered\' Stage (Customer: Oźe Cafe & Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("493","89","45","2016-11-14 10:38:44","Updated to \'Delivered\' Stage (Customer: Oźe Cafe & Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("494","30","45","2016-11-14 10:39:04","Updated to \'Delivered\' Stage (Customer: )");

INSERT INTO keg_mainstream_container_history VALUES("495","89","45","2016-11-14 10:39:04","Updated to \'Delivered\' Stage (Customer: )");

INSERT INTO keg_mainstream_container_history VALUES("496","45","45","2016-11-14 10:39:04","Updated to \'Delivered\' Stage (Customer: )");

INSERT INTO keg_mainstream_container_history VALUES("497","72","45","2016-11-14 10:40:56","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("498","45","45","2016-11-14 10:42:44","Updated to \'Delivered\' Stage (Customer: Oze Cafe & Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("499","89","45","2016-11-14 10:43:13","Updated to \'Delivered\' Stage (Customer: Oze Cafe & Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("500","89","45","2016-11-14 10:43:44","Updated to \'Delivered\' Stage (Customer: Oze Cafe & Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("501","30","45","2016-11-14 10:43:44","Updated to \'Delivered\' Stage (Customer: Oze Cafe & Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("502","77","45","2016-11-18 15:28:52","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("503","77","45","2016-11-18 15:29:17","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("504","91","45","2016-11-18 15:29:18","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("505","32","45","2016-11-18 15:29:34","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("506","32","45","2016-11-18 15:29:43","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("507","8","45","2016-11-18 15:31:37","Updated to \'Delivered\' Stage (Customer: Adrian Barnes)");

INSERT INTO keg_mainstream_container_history VALUES("508","63","45","2016-11-18 15:32:01","Updated to \'Delivered\' Stage (Customer: Adrian Barnes)");

INSERT INTO keg_mainstream_container_history VALUES("509","8","45","2016-11-18 15:32:01","Updated to \'Delivered\' Stage (Customer: Adrian Barnes)");

INSERT INTO keg_mainstream_container_history VALUES("510","28","45","2016-11-18 15:33:25","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("511","72","43","2016-11-27 19:29:20","Updated");

INSERT INTO keg_mainstream_container_history VALUES("512","72","43","2016-11-27 19:29:47","Updated");

INSERT INTO keg_mainstream_container_history VALUES("513","74\'>74</a>","43","2016-11-27 19:37:33","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("514","74","43","2016-11-27 19:37:53","Updated");

INSERT INTO keg_mainstream_container_history VALUES("515","75","43","2016-11-27 19:38:12","Updated");

INSERT INTO keg_mainstream_container_history VALUES("516","66","43","2016-11-27 19:38:36","Updated");

INSERT INTO keg_mainstream_container_history VALUES("517","42","43","2016-11-28 09:24:04","Updated");

INSERT INTO keg_mainstream_container_history VALUES("518","72","43","2016-11-28 09:25:24","Updated");

INSERT INTO keg_mainstream_container_history VALUES("519","80","45","2016-11-28 18:19:49","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("520","80","45","2016-11-28 18:20:07","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("521","80","45","2016-11-28 18:21:04","Updated to \'Empty\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("522","80","45","2016-11-28 18:21:30","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("523","80","45","2016-11-28 18:22:09","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("524","80","45","2016-11-28 18:22:41","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("525","80","45","2016-11-28 18:23:04","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("526","80","45","2016-11-28 18:23:42","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("527","80","45","2016-11-28 18:24:03","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("528","12","45","2016-11-28 18:26:25","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("529","63","45","2016-11-28 18:27:41","Updated to \'In Transit\' Stage (Customer: Sales-2 (Sean Kemp))");

INSERT INTO keg_mainstream_container_history VALUES("530","63","45","2016-11-28 18:28:00","Updated to \'In Transit\' Stage (Customer: Sales-2 (Sean Kemp))");

INSERT INTO keg_mainstream_container_history VALUES("531","8","45","2016-11-28 18:28:00","Updated to \'In Transit\' Stage (Customer: Sales-2 (Sean Kemp))");

INSERT INTO keg_mainstream_container_history VALUES("532","63","45","2016-11-28 18:28:25","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("533","8","45","2016-11-28 18:28:25","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("534","42","45","2016-11-28 18:28:25","Updated to \'In Transit\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("535","72\'>72</a>","44","2016-11-29 06:32:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("536","76\'>76</a>","44","2016-11-29 06:32:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("537","72\'>72</a>","44","2016-11-29 06:32:48","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("538","72\'>72</a>","44","2016-11-29 06:33:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("539","72","44","2016-11-29 06:34:06","Updated");

INSERT INTO keg_mainstream_container_history VALUES("540","63\'>63</a>","44","2016-11-29 06:34:43","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("541","63","44","2016-11-29 06:35:05","Updated");

INSERT INTO keg_mainstream_container_history VALUES("542","43","44","2016-11-29 06:35:57","Updated");

INSERT INTO keg_mainstream_container_history VALUES("543","8","44","2016-11-29 06:36:28","Updated");

INSERT INTO keg_mainstream_container_history VALUES("544","10","44","2016-11-29 06:36:57","Updated");

INSERT INTO keg_mainstream_container_history VALUES("545","72","44","2016-12-04 19:10:48","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("546","72","44","2016-12-04 19:11:18","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("547","63","44","2016-12-04 19:11:18","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("548","72","44","2016-12-04 19:11:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("549","43","44","2016-12-04 19:11:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("550","8","44","2016-12-04 19:11:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("551","63","44","2016-12-04 19:11:52","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("552","72","44","2016-12-04 19:12:13","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("553","43","44","2016-12-04 19:12:13","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("554","10","44","2016-12-04 19:12:13","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("555","8","44","2016-12-04 19:12:13","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("556","63","44","2016-12-04 19:12:13","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("557","54","43","2016-12-08 11:58:04","Updated");

INSERT INTO keg_mainstream_container_history VALUES("558","5","43","2016-12-08 11:58:21","Updated");

INSERT INTO keg_mainstream_container_history VALUES("559","5","43","2016-12-08 11:59:04","Updated");

INSERT INTO keg_mainstream_container_history VALUES("560","5","43","2016-12-08 11:59:14","Updated");

INSERT INTO keg_mainstream_container_history VALUES("561","54","43","2016-12-08 11:59:31","Updated");

INSERT INTO keg_mainstream_container_history VALUES("562","62","43","2016-12-12 12:23:52","Updated");

INSERT INTO keg_mainstream_container_history VALUES("563","70","43","2016-12-12 12:24:33","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("564","73","43","2016-12-12 12:24:33","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("565","74","43","2016-12-12 12:24:33","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("566","","43","2016-12-12 12:24:33","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("567","27","43","2016-12-14 13:47:37","Updated");

INSERT INTO keg_mainstream_container_history VALUES("568","27","43","2016-12-14 13:48:11","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("569","","43","2016-12-14 13:48:11","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("570","4","43","2016-12-14 13:49:44","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("571","47","43","2016-12-14 13:49:44","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("572","51","43","2016-12-14 13:49:44","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("573","76","43","2016-12-14 13:49:44","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("574","86","43","2016-12-14 13:49:44","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("575","","43","2016-12-14 13:49:44","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("576","12","43","2016-12-14 14:17:20","Updated to \'In Transit\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("577","25","43","2016-12-14 14:17:20","Updated to \'In Transit\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("578","","43","2016-12-14 14:17:20","Updated to \'In Transit\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("579","17","43","2016-12-14 14:18:11","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("580","","43","2016-12-14 14:18:11","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("581","17","43","2016-12-14 14:18:50","Updated to \'In Transit\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("582","19","43","2016-12-14 14:18:50","Updated to \'In Transit\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("583","47","43","2016-12-14 14:18:50","Updated to \'In Transit\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("584","","43","2016-12-14 14:18:50","Updated to \'In Transit\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("585","19","47","2016-12-20 08:56:52","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("586","33","47","2016-12-20 08:56:52","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("587","76","47","2016-12-20 08:56:52","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("588","","47","2016-12-20 08:56:52","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("589","17","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("590","89","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("591","47","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("592","54","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("593","77","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("594","30","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("595","5","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("596","","47","2016-12-20 09:05:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("597","7","47","2016-12-21 10:12:42","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("598","62","47","2016-12-21 10:12:42","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("599","","47","2016-12-21 10:12:42","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("600","4","47","2017-01-05 08:07:32","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("601","44","47","2017-01-05 08:07:32","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("602","","47","2017-01-05 08:07:32","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("603","7","47","2017-01-05 08:08:22","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("604","76","47","2017-01-05 08:08:23","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("605","","47","2017-01-05 08:08:23","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("606","6","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("607","8","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("608","10","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("609","18","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("610","45","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("611","55","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("612","79","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("613","81","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("614","82","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("615","","47","2017-01-05 10:16:10","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("616","44","47","2017-01-05 10:18:56","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("617","","47","2017-01-05 10:18:56","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("618","44","47","2017-01-05 10:19:45","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("619","","47","2017-01-05 10:19:45","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("620","44","47","2017-01-05 10:19:45","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("621","","47","2017-01-05 10:19:45","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("622","44","47","2017-01-05 10:19:45","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("623","","47","2017-01-05 10:19:45","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("624","33","47","2017-01-05 10:23:33","Container State Changed to \'Keg Contents Over Carbonated\'");

INSERT INTO keg_mainstream_container_history VALUES("625","33","47","2017-01-05 10:23:55","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("626","","47","2017-01-05 10:23:55","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("627","19","47","2017-01-05 10:26:55","Container State Changed to \'Keg Contents Over Carbonated\'");

INSERT INTO keg_mainstream_container_history VALUES("628","19","47","2017-01-05 10:28:32","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("629","","47","2017-01-05 10:28:32","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("630","62","47","2017-01-05 10:30:49","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("631","","47","2017-01-05 10:30:49","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("632","62","47","2017-01-05 10:31:52","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("633","","47","2017-01-05 10:31:52","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("634","13","47","2017-01-05 14:02:52","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("635","60","47","2017-01-05 14:02:52","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("636","","47","2017-01-05 14:02:52","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("637","51","47","2017-01-05 14:04:17","Updated to \'Delivered\' Stage (Customer: Toni\'s Pizza)");

INSERT INTO keg_mainstream_container_history VALUES("638","","47","2017-01-05 14:04:17","Updated to \'Delivered\' Stage (Customer: Toni\'s Pizza)");

INSERT INTO keg_mainstream_container_history VALUES("639","67","47","2017-01-13 09:05:22","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("640","","47","2017-01-13 09:05:22","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("641","67","47","2017-01-13 09:08:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("642","79","47","2017-01-13 09:08:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("643","3","47","2017-01-13 09:08:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("644","","47","2017-01-13 09:08:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("645","86","47","2017-01-13 09:10:43","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("646","","47","2017-01-13 09:10:43","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("647","31","47","2017-01-24 10:00:56","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("648","","47","2017-01-24 10:00:56","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("649","31","47","2017-01-24 10:03:35","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("650","31","47","2017-01-24 10:05:04","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("651","","47","2017-01-24 10:05:04","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("652","69","47","2017-01-24 10:09:16","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("653","","47","2017-01-24 10:09:16","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("654","77","47","2017-01-24 10:14:29","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("655","","47","2017-01-24 10:14:29","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("656","47","47","2017-01-24 10:22:25","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("657","","47","2017-01-24 10:22:25","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("658","43","47","2017-01-24 10:31:13","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("659","","47","2017-01-24 10:31:13","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("660","13","47","2017-01-24 10:39:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("661","","47","2017-01-24 10:39:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("662","58","47","2017-01-24 10:46:21","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("663","","47","2017-01-24 10:46:21","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("664","78","47","2017-01-24 10:53:50","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("665","","47","2017-01-24 10:53:50","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("666","61","47","2017-01-24 10:59:31","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("667","","47","2017-01-24 10:59:31","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("668","49","47","2017-01-24 11:03:34","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("669","49","47","2017-01-24 11:04:35","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("670","","47","2017-01-24 11:04:35","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("671","34","47","2017-01-24 11:08:53","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("672","","47","2017-01-24 11:08:53","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("673","30","47","2017-01-24 11:15:23","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("674","","47","2017-01-24 11:15:23","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("675","84","47","2017-01-24 11:22:24","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("676","","47","2017-01-24 11:22:24","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("677","32","47","2017-01-24 11:28:33","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("678","","47","2017-01-24 11:28:37","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("679","60","47","2017-01-24 11:34:28","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("680","","47","2017-01-24 11:34:28","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("681","89","47","2017-01-24 11:43:12","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("682","","47","2017-01-24 11:43:12","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("683","82","47","2017-01-26 18:17:55","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("684","","47","2017-01-26 18:17:55","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("685","2","47","2017-01-26 18:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("686","","47","2017-01-26 18:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("687","6","44","2017-01-29 12:41:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("688","13","44","2017-01-29 12:41:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("689","","44","2017-01-29 12:41:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("690","47","44","2017-01-29 12:44:31","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("691","37","44","2017-01-29 12:44:31","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("692","48","44","2017-01-29 12:44:32","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("693","32","44","2017-01-29 12:44:32","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("694","18","44","2017-01-29 12:44:32","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("695","8","44","2017-01-29 12:44:32","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("696","","44","2017-01-29 12:44:32","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("697","89","44","2017-01-29 12:45:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("698","81","44","2017-01-29 12:45:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("699","57","44","2017-01-29 12:45:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("700","","44","2017-01-29 12:45:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("701","84","44","2017-01-29 12:45:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("702","","44","2017-01-29 12:45:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("703","56","47","2017-02-01 09:33:42","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("704","","47","2017-02-01 09:33:42","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("705","31","47","2017-02-01 09:34:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("706","","47","2017-02-01 09:34:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("707","56","47","2017-02-01 09:34:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("708","","47","2017-02-01 09:34:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("709","56","47","2017-02-01 09:34:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("710","","47","2017-02-01 09:34:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("711","56","47","2017-02-01 09:34:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("712","","47","2017-02-01 09:34:50","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("713","64","47","2017-02-01 14:49:37","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("714","","47","2017-02-01 14:49:37","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("715","64","47","2017-02-01 14:50:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("716","","47","2017-02-01 14:50:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("717","64","47","2017-02-01 14:50:50","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("718","","47","2017-02-01 14:50:50","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("719","9","47","2017-02-01 14:53:20","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("720","","47","2017-02-01 14:53:20","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("721","68","47","2017-02-01 14:59:46","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("722","","47","2017-02-01 14:59:46","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("723","29","47","2017-02-01 15:07:23","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("724","","47","2017-02-01 15:07:23","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("725","15","47","2017-02-01 15:13:46","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("726","","47","2017-02-01 15:13:46","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("727","38","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("728","63","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("729","88","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("730","83","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("731","52","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("732","46","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("733","11","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("734","","47","2017-02-01 15:15:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("735","37","47","2017-02-01 16:04:57","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("736","","47","2017-02-01 16:04:57","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("737","12","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("738","77","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("739","59","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("740","27","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("741","45","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("742","42","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("743","1","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("744","67","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("745","69","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("746","91","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("747","10","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("748","","47","2017-02-05 14:07:11","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("749","19","44","2017-02-06 13:38:59","Container State Changed to \'Partly used at a previous event\'");

INSERT INTO keg_mainstream_container_history VALUES("750","34","44","2017-02-06 13:40:05","Container State Changed to \'Partly used at a previous event\'");

INSERT INTO keg_mainstream_container_history VALUES("751","77","47","2017-02-12 09:38:10","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("752","","47","2017-02-12 09:38:10","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("753","76","47","2017-02-12 09:56:27","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("754","","47","2017-02-12 09:56:27","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("755","13","47","2017-02-12 10:08:00","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("756","","47","2017-02-12 10:08:00","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("757","2","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("758","84","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("759","89","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("760","69","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("761","67","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("762","47","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("763","32","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("764","","47","2017-02-12 10:13:26","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("765","48","47","2017-02-12 11:09:40","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("766","1","47","2017-02-12 11:09:40","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("767","","47","2017-02-12 11:09:40","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("768","50","47","2017-02-12 11:16:04","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("769","","47","2017-02-12 11:16:04","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("770","10","47","2017-02-12 11:23:13","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("771","","47","2017-02-12 11:23:13","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("772","42","47","2017-02-12 11:39:58","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("773","","47","2017-02-12 11:39:58","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("774","27","47","2017-02-12 12:05:39","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("775","59","47","2017-02-12 12:05:39","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("776","27","47","2017-02-12 12:06:54","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("777","59","47","2017-02-12 12:06:54","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("778","","47","2017-02-12 12:06:54","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("779","28","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("780","43","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("781","25","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("782","57","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("783","80","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("784","7","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("785","5","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("786","","47","2017-02-15 08:30:11","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("787","6","47","2017-02-22 14:18:57","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("788","","47","2017-02-22 14:18:57","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("789","6","47","2017-02-22 14:19:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("790","38","47","2017-02-22 14:19:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("791","","47","2017-02-22 14:19:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("792","78","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("793","30","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("794","61","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("795","34","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("796","21","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("797","91","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("798","53","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("799","69","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("800","84","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("801","42","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("802","67","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("803","85","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("804","40","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("805","4","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("806","31","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("807","36","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("808","55","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("809","62","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("810","37","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("811","32","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("812","50","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("813","10","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("814","76","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("815","12","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("816","17","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("817","81","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("818","88","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("819","83","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("820","","47","2017-02-27 10:32:02","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("821","85","47","2017-03-03 09:46:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("822","40","47","2017-03-03 09:46:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("823","4","47","2017-03-03 09:46:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("824","12","47","2017-03-03 09:46:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("825","","47","2017-03-03 09:46:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("826","48","47","2017-03-03 10:08:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("827","8","47","2017-03-03 10:08:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("828","55","47","2017-03-03 10:08:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("829","69","47","2017-03-03 10:08:51","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("830","","47","2017-03-03 10:08:51","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("831","12","47","2017-03-03 10:10:21","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("832","","47","2017-03-03 10:10:21","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("833","51","47","2017-03-03 10:20:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("834","84","47","2017-03-03 10:20:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("835","50","47","2017-03-03 10:20:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("836","31","47","2017-03-03 10:20:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("837","","47","2017-03-03 10:20:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("838","72","47","2017-03-03 10:21:19","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("839","","47","2017-03-03 10:21:19","Updated to \'Delivered\' Stage (Customer: Broodhuijs)");

INSERT INTO keg_mainstream_container_history VALUES("840","62","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("841","42","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("842","78","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("843","59","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("844","30","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("845","57","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("846","91","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("847","53","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("848","10","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("849","17","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("850","37","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("851","34","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("852","83","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("853","88","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("854","81","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("855","76","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("856","61","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("857","","47","2017-03-03 11:21:50","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("858","76","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("859","88","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("860","91","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("861","17","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("862","83","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("863","48","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("864","34","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("865","61","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("866","31","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("867","53","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("868","8","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("869","78","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("870","81","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("871","","47","2017-03-03 14:41:01","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("872","2","47","2017-03-08 17:23:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("873","13","47","2017-03-08 17:23:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("874","79","47","2017-03-08 17:23:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("875","90","47","2017-03-08 17:23:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("876","82","47","2017-03-08 17:23:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("877","86","47","2017-03-08 17:23:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("878","","47","2017-03-08 17:23:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("879","82","47","2017-03-08 17:23:32","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("880","18","47","2017-03-08 17:24:57","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("881","18","47","2017-03-08 17:28:46","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("882","","47","2017-03-08 17:28:46","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("883","18","47","2017-03-08 17:29:12","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("884","","47","2017-03-08 17:29:12","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("885","47","47","2017-03-08 17:35:52","Updated to \'Delivered\' Stage (Customer: Kudo\'s Craft Cafe)");

INSERT INTO keg_mainstream_container_history VALUES("886","78","47","2017-03-08 17:35:52","Updated to \'Delivered\' Stage (Customer: Kudo\'s Craft Cafe)");

INSERT INTO keg_mainstream_container_history VALUES("887","26","47","2017-03-08 17:35:52","Updated to \'Delivered\' Stage (Customer: Kudo\'s Craft Cafe)");

INSERT INTO keg_mainstream_container_history VALUES("888","","47","2017-03-08 17:35:52","Updated to \'Delivered\' Stage (Customer: Kudo\'s Craft Cafe)");

INSERT INTO keg_mainstream_container_history VALUES("889","75","47","2017-03-08 17:36:34","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("890","","47","2017-03-08 17:36:34","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("891","88","47","2017-03-09 15:32:17","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("892","","47","2017-03-09 15:32:17","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("893","56","47","2017-03-14 11:26:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("894","60","47","2017-03-14 11:26:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("895","45","47","2017-03-14 11:26:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("896","72","47","2017-03-14 11:26:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("897","","47","2017-03-14 11:26:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("898","77","47","2017-03-14 11:27:30","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("899","76","47","2017-03-14 11:27:30","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("900","11","47","2017-03-14 11:27:30","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("901","","47","2017-03-14 11:27:30","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("902","63","47","2017-03-14 11:27:59","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("903","","47","2017-03-14 11:27:59","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("904","2","43","2017-03-20 11:49:46","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("905","13","43","2017-03-20 11:49:46","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("906","69","43","2017-03-20 11:49:46","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("907","86","43","2017-03-20 11:49:46","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("908","","43","2017-03-20 11:49:46","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("909","2","43","2017-03-20 11:50:21","Updated");

INSERT INTO keg_mainstream_container_history VALUES("910","13","43","2017-03-20 11:50:42","Updated");

INSERT INTO keg_mainstream_container_history VALUES("911","69","43","2017-03-20 11:51:04","Updated");

INSERT INTO keg_mainstream_container_history VALUES("912","86","43","2017-03-20 11:51:32","Updated");

INSERT INTO keg_mainstream_container_history VALUES("913","69","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("914","86","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("915","13","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("916","48","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("917","31","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("918","65","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("919","46","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("920","","43","2017-03-21 20:52:09","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("921","37","47","2017-03-23 13:25:03","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("922","85","47","2017-03-23 13:25:03","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("923","55","47","2017-03-23 13:25:03","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("924","40","47","2017-03-23 13:25:03","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("925","59","47","2017-03-23 13:25:03","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("926","62","47","2017-03-23 13:25:03","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("927","","47","2017-03-23 13:25:03","Updated to \'Filled\' Stage (Product: R.I.P Ale)");

INSERT INTO keg_mainstream_container_history VALUES("928","84","47","2017-03-23 13:31:17","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("929","","47","2017-03-23 13:31:17","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("930","84","47","2017-03-23 13:32:23","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("931","39","47","2017-03-23 13:32:23","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("932","34","47","2017-03-23 13:32:23","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("933","","47","2017-03-23 13:32:23","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("934","91","47","2017-03-23 13:32:54","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("935","","47","2017-03-23 13:32:54","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("936","32","47","2017-03-23 13:53:23","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("937","","47","2017-03-23 13:53:23","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("938","32","47","2017-03-23 13:53:43","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("939","","47","2017-03-23 13:53:43","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("940","32","47","2017-03-23 13:54:00","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("941","","47","2017-03-23 13:54:00","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("942","67","47","2017-03-29 12:25:33","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("943","69","47","2017-03-29 12:25:33","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("944","86","47","2017-03-29 12:25:33","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("945","10","47","2017-03-29 12:25:33","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("946","13","47","2017-03-29 12:25:33","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("947","30","47","2017-03-29 12:25:33","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("948","","47","2017-03-29 12:25:33","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("949","42","47","2017-03-29 12:26:55","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("950","50","47","2017-03-29 12:26:55","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("951","60","47","2017-03-29 12:26:55","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("952","1","47","2017-03-29 12:26:55","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("953","","47","2017-03-29 12:26:55","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("954","53","47","2017-03-29 12:28:24","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("955","8","47","2017-03-29 12:28:24","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("956","","47","2017-03-29 12:28:24","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("957","12","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("958","31","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("959","48","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("960","15","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("961","56","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("962","88","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("963","46","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("964","65","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("965","16","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("966","82","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("967","79","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("968","90","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("969","45","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("970","72","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("971","","47","2017-03-29 12:31:00","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("972","6","47","2017-03-30 13:55:03","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("973","38","47","2017-03-30 13:55:03","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("974","89","47","2017-03-30 13:55:03","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("975","","47","2017-03-30 13:55:03","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("976","58","47","2017-03-30 13:55:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("977","","47","2017-03-30 13:55:59","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("978","17","47","2017-03-30 13:58:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("979","29","47","2017-03-30 13:58:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("980","4","47","2017-03-30 13:58:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("981","51","47","2017-03-30 13:58:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("982","83","47","2017-03-30 13:58:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("983","68","47","2017-03-30 13:58:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("984","","47","2017-03-30 13:58:29","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("985","4","47","2017-03-30 13:59:00","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("986","","47","2017-03-30 13:59:00","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("987","51","47","2017-03-30 13:59:54","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("988","","47","2017-03-30 13:59:54","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("989","4","47","2017-03-30 13:59:58","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("990","51","47","2017-03-30 13:59:58","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("991","","47","2017-03-30 13:59:58","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("992","44","47","2017-03-30 14:05:11","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("993","","47","2017-03-30 14:05:11","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("994","54","47","2017-04-05 17:35:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("995","49","47","2017-04-05 17:35:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("996","","47","2017-04-05 17:35:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("997","54","47","2017-04-05 17:36:07","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("998","81","47","2017-04-05 17:38:35","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("999","42","47","2017-04-05 17:38:35","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1000","","47","2017-04-05 17:38:35","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1001","27","47","2017-04-06 11:48:28","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1002","","47","2017-04-06 11:48:28","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1003","12","47","2017-04-06 11:50:51","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1004","59","47","2017-04-06 11:50:51","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1005","60","47","2017-04-06 11:50:51","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1006","","47","2017-04-06 11:50:51","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1007","48","47","2017-04-06 15:26:36","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1008","82","47","2017-04-06 15:26:36","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1009","79","47","2017-04-06 15:26:36","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1010","45","47","2017-04-06 15:26:36","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1011","88","47","2017-04-06 15:26:36","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1012","90","47","2017-04-06 15:26:36","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1013","","47","2017-04-06 15:26:36","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1014","48","47","2017-04-06 15:27:27","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1015","82","47","2017-04-06 15:27:27","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1016","","47","2017-04-06 15:27:27","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1017","45","47","2017-04-12 10:11:34","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1018","79","47","2017-04-12 10:11:35","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1019","","47","2017-04-12 10:11:35","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1020","9","47","2017-04-12 10:11:52","Updated to \'Delivered\' Stage (Customer: )");

INSERT INTO keg_mainstream_container_history VALUES("1021","","47","2017-04-12 10:11:52","Updated to \'Delivered\' Stage (Customer: )");

INSERT INTO keg_mainstream_container_history VALUES("1022","9","47","2017-04-12 10:12:17","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1023","","47","2017-04-12 10:12:17","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1024","41","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1025","54","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1026","89","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1027","44","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1028","27","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1029","6","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1030","88","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1031","19","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1032","16","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1033","30","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1034","","43","2017-04-16 14:31:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1035","15","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1036","49","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1037","14","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1038","1","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1039","38","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1040","52","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1041","87","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1042","90","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1043","33","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1044","7","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1045","","43","2017-04-16 14:33:29","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1046","8","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1047","56","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1048","86","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1049","10","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1050","61","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1051","21","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1052","2","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1053","67","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1054","58","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1055","35","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1056","","43","2017-04-16 14:35:40","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1057","53","43","2017-04-16 14:49:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1058","","43","2017-04-16 14:49:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1059","64","45","2017-04-19 13:36:01","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1060","","45","2017-04-19 13:36:01","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1061","13","45","2017-04-19 13:36:45","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1062","","45","2017-04-19 13:36:45","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1063","11","45","2017-04-19 13:37:42","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1064","","45","2017-04-19 13:37:42","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1065","77","45","2017-04-19 13:38:27","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1066","","45","2017-04-19 13:38:27","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1067","76","45","2017-04-19 13:39:12","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1068","","45","2017-04-19 13:39:12","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1069","8","43","2017-04-22 08:03:19","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1070","","43","2017-04-22 08:03:19","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1071","52","43","2017-04-22 08:05:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1072","","43","2017-04-22 08:05:10","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1073","8","43","2017-04-22 08:06:31","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1074","52","43","2017-04-22 08:06:34","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1075","","43","2017-04-22 08:06:34","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1076","8","43","2017-04-22 08:06:34","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1077","52","43","2017-04-22 08:06:37","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1078","","43","2017-04-22 08:06:42","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1079","14","43","2017-04-23 10:46:37","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1080","38","43","2017-04-23 10:46:37","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1081","","43","2017-04-23 10:46:37","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1082","14","43","2017-04-23 10:46:57","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1083","","43","2017-04-23 10:46:57","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1084","11","43","2017-04-23 10:47:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1085","17","43","2017-04-23 10:47:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1086","29","43","2017-04-23 10:47:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1087","76","43","2017-04-23 10:47:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1088","77","43","2017-04-23 10:47:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1089","","43","2017-04-23 10:47:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1090","64","43","2017-04-23 12:03:11","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1091","13","43","2017-04-23 12:04:00","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1092","72","47","2017-05-04 10:37:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1093","","47","2017-05-04 10:37:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1094","38","47","2017-05-04 10:38:16","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1095","","47","2017-05-04 10:38:16","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1096","72","47","2017-05-04 10:38:48","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1097","","47","2017-05-04 10:38:48","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1098","76","47","2017-05-05 10:25:26","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1099","90","47","2017-05-05 10:25:26","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1100","","47","2017-05-05 10:25:26","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1101","76","47","2017-05-05 10:26:18","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1102","90","47","2017-05-05 10:26:18","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1103","","47","2017-05-05 10:26:18","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1104","11","47","2017-05-05 11:12:31","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1105","","47","2017-05-05 11:12:31","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1106","11","47","2017-05-05 11:13:02","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1107","","47","2017-05-05 11:13:02","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1108","17","47","2017-05-05 11:13:27","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1109","","47","2017-05-05 11:13:27","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1110","17","47","2017-05-05 11:13:44","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1111","","47","2017-05-05 11:13:44","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1112","1","47","2017-05-05 17:45:54","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1113","15","47","2017-05-05 17:45:54","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1114","16","47","2017-05-05 17:45:54","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1115","29","47","2017-05-05 17:45:54","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1116","","47","2017-05-05 17:45:54","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1117","17","47","2017-05-05 18:42:59","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1118","54","47","2017-05-05 18:42:59","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1119","","47","2017-05-05 18:42:59","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1120","2","47","2017-05-05 18:49:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1121","77","47","2017-05-05 18:49:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1122","","47","2017-05-05 18:49:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1123","89","47","2017-05-05 18:55:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1124","77","47","2017-05-05 18:55:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1125","2","47","2017-05-05 18:55:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1126","","47","2017-05-05 18:55:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1127","10","47","2017-05-09 13:05:07","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1128","27","47","2017-05-09 13:05:07","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1129","86","47","2017-05-09 13:05:07","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1130","","47","2017-05-09 13:05:07","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1131","63","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1132","31","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1133","45","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1134","48","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1135","65","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1136","46","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1137","56","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1138","67","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1139","75","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1140","","47","2017-05-13 14:33:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1141","63","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1142","31","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1143","45","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1144","48","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1145","65","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1146","67","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1147","56","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1148","46","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1149","75","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1150","","47","2017-05-13 14:35:22","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1151","63","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1152","56","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1153","46","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1154","33","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1155","58","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1156","41","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1157","19","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1158","49","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1159","","47","2017-05-15 12:03:22","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1160","19","47","2017-05-15 12:03:56","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1161","68","47","2017-05-15 15:29:48","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1162","83","47","2017-05-15 15:29:48","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1163","77","47","2017-05-15 15:29:48","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1164","54","47","2017-05-15 15:29:48","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1165","8","47","2017-05-15 15:29:48","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1166","","47","2017-05-15 15:29:48","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1167","88","47","2017-05-15 15:59:49","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1168","","47","2017-05-15 15:59:49","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1169","57","47","2017-05-15 16:00:10","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1170","","47","2017-05-15 16:00:10","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1171","25","47","2017-05-15 16:19:56","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1172","","47","2017-05-15 16:19:56","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1173","25","47","2017-05-15 16:20:16","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1174","","47","2017-05-15 16:20:16","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1175","17","47","2017-05-15 16:23:37","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1176","3","47","2017-05-15 17:00:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1177","","47","2017-05-15 17:00:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1178","3","47","2017-05-15 17:01:07","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1179","","47","2017-05-15 17:01:07","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1180","80","47","2017-05-15 17:01:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1181","","47","2017-05-15 17:01:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1182","80","47","2017-05-15 17:01:57","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1183","","47","2017-05-15 17:01:57","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1184","41","47","2017-05-15 17:02:37","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1185","64","47","2017-05-15 17:03:08","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1186","","47","2017-05-15 17:03:08","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1187","64","47","2017-05-15 17:03:42","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1188","","47","2017-05-15 17:03:42","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1189","33","47","2017-05-15 17:04:08","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1190","62","47","2017-05-16 09:48:33","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1191","","47","2017-05-16 09:48:33","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1192","75","45","2017-05-16 14:44:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1193","","45","2017-05-16 14:44:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1194","75","45","2017-05-16 14:45:13","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1195","","45","2017-05-16 14:45:13","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1196","61","47","2017-05-16 16:27:53","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1197","","47","2017-05-16 16:27:53","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1198","27","47","2017-05-16 16:29:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1199","","47","2017-05-16 16:29:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1200","27","47","2017-05-16 16:29:50","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1201","61","47","2017-05-16 16:29:50","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1202","90","47","2017-05-16 16:29:50","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1203","","47","2017-05-16 16:29:50","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1204","2","47","2017-05-16 16:45:32","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1205","50","47","2017-05-16 16:45:32","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1206","89","47","2017-05-16 16:45:32","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1207","79","47","2017-05-16 16:45:32","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1208","","47","2017-05-16 16:45:32","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1209","88","47","2017-05-16 16:47:35","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1210","86","47","2017-05-16 16:47:35","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1211","58","47","2017-05-16 16:47:35","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1212","","47","2017-05-16 16:47:35","Updated to \'Delivered\' Stage (Customer: Fishermans Deck)");

INSERT INTO keg_mainstream_container_history VALUES("1213","51","47","2017-05-16 16:47:50","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1214","","47","2017-05-16 16:47:50","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1215","32","47","2017-05-16 17:13:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1216","34","47","2017-05-16 17:13:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1217","39","47","2017-05-16 17:13:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1218","84","47","2017-05-16 17:13:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1219","","47","2017-05-16 17:13:58","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1220","17","47","2017-05-17 16:02:39","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1221","69","47","2017-05-17 16:02:39","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1222","","47","2017-05-17 16:02:39","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1223","5","47","2017-05-18 09:32:26","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("1224","10","47","2017-05-18 09:32:26","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("1225","","47","2017-05-18 09:32:26","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("1226","39","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1227","32","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1228","8","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1229","83","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1230","77","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1231","91","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1232","68","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1233","51","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1234","84","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1235","54","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1236","","43","2017-05-23 11:59:38","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1237","18","45","2017-05-25 10:07:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1238","34","45","2017-05-25 10:07:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1239","72","45","2017-05-25 10:07:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1240","60","45","2017-05-25 10:07:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1241","32","45","2017-05-25 10:07:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1242","","45","2017-05-25 10:07:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1243","27","45","2017-05-25 10:08:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1244","","45","2017-05-25 10:08:37","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1245","27","45","2017-05-25 10:10:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1246","84","45","2017-05-25 10:10:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1247","83","45","2017-05-25 10:10:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1248","91","45","2017-05-25 10:10:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1249","53","45","2017-05-25 10:10:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1250","","45","2017-05-25 10:10:03","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1251","27","45","2017-05-25 10:11:10","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1252","","45","2017-05-25 10:11:10","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1253","8","47","2017-05-25 10:31:03","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1254","6","47","2017-05-25 10:31:03","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1255","31","47","2017-05-25 10:31:03","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1256","","47","2017-05-25 10:31:03","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1257","8","47","2017-05-25 10:31:44","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1258","41","47","2017-05-25 10:31:44","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1259","","47","2017-05-25 10:31:44","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1260","6","47","2017-05-25 10:32:15","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1261","31","47","2017-05-25 10:32:15","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1262","","47","2017-05-25 10:32:15","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1263","14","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1264","76","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1265","13","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1266","69","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1267","17","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1268","4","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1269","82","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1270","11","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1271","90","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1272","34","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1273","60","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1274","32","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1275","53","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1276","72","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1277","18","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1278","","47","2017-06-02 11:48:41","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1279","14","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1280","76","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1281","13","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1282","69","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1283","17","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1284","4","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1285","82","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1286","11","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1287","90","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1288","34","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1289","60","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1290","32","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1291","53","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1292","72","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1293","18","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1294","","47","2017-06-02 11:50:56","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1295","70","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1296","44","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1297","65","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1298","39","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1299","7","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1300","68","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1301","87","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1302","30","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1303","35","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1304","45","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1305","48","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1306","51","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1307","77","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1308","67","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1309","54","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1310","","47","2017-06-02 11:55:53","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1311","67","47","2017-06-02 12:00:15","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1312","","47","2017-06-02 12:00:15","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1313","25","47","2017-06-02 12:08:37","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1314","28","47","2017-06-02 12:08:37","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1315","","47","2017-06-02 12:08:37","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1316","91","47","2017-06-02 12:48:06","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1317","","47","2017-06-02 12:48:06","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1318","40","47","2017-06-02 12:53:51","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1319","","47","2017-06-02 12:53:51","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1320","73","47","2017-06-02 12:55:43","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1321","","47","2017-06-02 12:55:43","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1322","73","47","2017-06-02 12:56:07","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1323","","47","2017-06-02 12:56:07","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1324","54","47","2017-06-02 13:43:22","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("1325","","47","2017-06-02 13:43:22","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("1326","46","47","2017-06-02 14:30:44","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1327","","47","2017-06-02 14:30:44","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1328","66","47","2017-06-02 14:42:37","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1329","","47","2017-06-02 14:42:37","Updated to \'Filled\' Stage (Product: Altbier)");

INSERT INTO keg_mainstream_container_history VALUES("1330","63","47","2017-06-06 19:30:07","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1331","84","47","2017-06-06 19:30:07","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1332","","47","2017-06-06 19:30:07","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1333","72","47","2017-06-09 09:52:00","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1334","74","47","2017-06-09 09:52:00","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1335","","47","2017-06-09 09:52:00","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1336","72","47","2017-06-09 09:52:33","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1337","74","47","2017-06-09 09:52:33","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1338","","47","2017-06-09 09:52:33","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1339","72","47","2017-06-09 09:54:17","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1340","74","47","2017-06-09 09:54:17","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1341","","47","2017-06-09 09:54:17","Updated to \'Delivered\' Stage (Customer: Sales-3 (Adrian Barnes))");

INSERT INTO keg_mainstream_container_history VALUES("1342","72","47","2017-06-09 09:57:17","Updated to \'Delivered\' Stage (Customer: Reinhard Spiwak)");

INSERT INTO keg_mainstream_container_history VALUES("1343","74","47","2017-06-09 09:57:17","Updated to \'Delivered\' Stage (Customer: Reinhard Spiwak)");

INSERT INTO keg_mainstream_container_history VALUES("1344","","47","2017-06-09 09:57:17","Updated to \'Delivered\' Stage (Customer: Reinhard Spiwak)");

INSERT INTO keg_mainstream_container_history VALUES("1345","4","47","2017-06-14 13:39:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1346","13","47","2017-06-14 13:39:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1347","60","47","2017-06-14 13:39:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1348","69","47","2017-06-14 13:39:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1349","32","47","2017-06-14 13:39:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1350","","47","2017-06-14 13:39:48","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1351","32","47","2017-06-14 13:40:33","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1352","35","47","2017-06-14 13:40:33","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1353","","47","2017-06-14 13:40:33","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1354","18","47","2017-06-21 10:43:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1355","","47","2017-06-21 10:43:08","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1356","69","47","2017-06-21 10:44:23","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1357","60","47","2017-06-21 10:44:23","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1358","57","47","2017-06-21 10:44:23","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1359","","47","2017-06-21 10:44:23","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1360","10","47","2017-06-21 11:44:54","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1361","46","47","2017-06-21 11:44:54","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1362","62","47","2017-06-21 11:44:54","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1363","81","47","2017-06-21 11:44:54","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1364","","47","2017-06-21 11:44:54","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1365","2","47","2017-06-21 12:29:02","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1366","12","47","2017-06-21 12:29:02","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1367","47","47","2017-06-21 12:29:02","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1368","49","47","2017-06-21 12:29:02","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1369","85","47","2017-06-21 12:29:02","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1370","","47","2017-06-21 12:29:02","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1371","30","47","2017-06-21 12:32:12","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1372","","47","2017-06-21 12:32:12","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1373","50","47","2017-06-21 12:47:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1374","63","47","2017-06-21 12:47:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1375","","47","2017-06-21 12:47:06","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1376","78","47","2017-06-21 12:47:44","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1377","84","47","2017-06-21 12:47:44","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1378","","47","2017-06-21 12:47:44","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1379","3","47","2017-06-21 12:48:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1380","88","47","2017-06-21 12:48:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1381","","47","2017-06-21 12:48:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1382","32","47","2017-06-21 12:49:42","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1383","64","47","2017-06-21 12:49:42","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1384","","47","2017-06-21 12:49:42","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1385","35","47","2017-06-21 12:50:41","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1386","52","47","2017-06-21 12:50:41","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1387","","47","2017-06-21 12:50:41","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1388","9","47","2017-06-21 12:52:07","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1389","25","47","2017-06-21 12:52:07","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1390","27","47","2017-06-21 12:52:07","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1391","38","47","2017-06-21 12:52:07","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1392","75","47","2017-06-21 12:52:07","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1393","","47","2017-06-21 12:52:07","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1394","84","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1395","85","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1396","50","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1397","27","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1398","10","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1399","2","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1400","32","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1401","47","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1402","","47","2017-06-22 11:08:49","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1403","62","47","2017-06-22 12:10:13","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1404","","47","2017-06-22 12:10:13","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1405","35","47","2017-06-22 12:21:38","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1406","","47","2017-06-22 12:21:38","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1407","3","47","2017-06-22 12:24:56","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1408","","47","2017-06-22 12:24:56","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1409","25","47","2017-06-22 12:30:04","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1410","","47","2017-06-22 12:30:04","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1411","4","47","2017-06-27 12:37:19","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1412","26","47","2017-06-27 12:37:19","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1413","48","47","2017-06-27 12:37:19","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1414","55","47","2017-06-27 12:37:19","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1415","","47","2017-06-27 12:37:19","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1416","34","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1417","88","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1418","82","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1419","78","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1420","48","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1421","76","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1422","26","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1423","17","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1424","55","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1425","81","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1426","90","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1427","4","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1428","9","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1429","14","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1430","11","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1431","61","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1432","52","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1433","63","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1434","12","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1435","","47","2017-06-29 10:38:05","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1436","6","47","2017-06-29 10:42:20","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1437","","47","2017-06-29 10:42:20","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1438","81","47","2017-06-29 11:19:42","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1439","90","47","2017-06-29 11:19:42","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1440","18","47","2017-06-29 11:19:42","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1441","","47","2017-06-29 11:19:42","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1442","41","47","2017-07-07 14:28:43","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1443","59","47","2017-07-07 14:28:43","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1444","28","47","2017-07-07 14:28:43","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1445","6","47","2017-07-07 14:28:43","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1446","91","47","2017-07-07 14:28:43","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1447","","47","2017-07-07 14:28:43","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1448","87","47","2017-07-11 14:17:52","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1449","","47","2017-07-11 14:17:52","Updated to \'Delivered\' Stage (Customer: Sales (Carel Malherbe))");

INSERT INTO keg_mainstream_container_history VALUES("1450","82","47","2017-07-13 10:06:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1451","16","47","2017-07-13 10:06:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1452","25","47","2017-07-13 10:06:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1453","47","47","2017-07-13 10:06:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1454","48","47","2017-07-13 10:06:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1455","","47","2017-07-13 10:06:27","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1456","75","47","2017-07-13 10:06:59","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1457","","47","2017-07-13 10:06:59","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1458","75","47","2017-07-13 10:07:39","Updated to \'Delivered\' Stage (Customer: Reinhard Spiwak)");

INSERT INTO keg_mainstream_container_history VALUES("1459","","47","2017-07-13 10:07:39","Updated to \'Delivered\' Stage (Customer: Reinhard Spiwak)");

INSERT INTO keg_mainstream_container_history VALUES("1460","2","47","2017-07-14 10:38:48","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1461","9","47","2017-07-14 10:38:48","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1462","35","47","2017-07-14 10:38:48","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1463","","47","2017-07-14 10:38:48","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1464","88","47","2017-07-20 15:10:37","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("1465","","47","2017-07-20 15:10:37","Updated to \'Delivered\' Stage (Customer: The Lounge)");

INSERT INTO keg_mainstream_container_history VALUES("1466","7","47","2017-07-25 16:01:57","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1467","61","47","2017-07-25 16:01:57","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1468","62","47","2017-07-25 16:01:57","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1469","","47","2017-07-25 16:01:57","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1470","67","47","2017-07-25 16:03:16","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1471","","47","2017-07-25 16:03:16","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1472","56","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1473","64","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1474","46","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1475","15","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1476","38","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1477","29","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1478","1","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1479","33","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1480","19","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1481","41","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1482","43","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1483","13","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1484","83","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1485","80","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1486","37","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1487","5","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1488","69","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1489","79","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1490","57","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1491","67","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1492","89","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1493","66","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1494","","47","2017-07-27 14:18:12","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1495","56","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1496","64","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1497","46","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1498","15","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1499","38","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1500","29","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1501","1","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1502","33","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1503","19","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1504","41","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1505","","47","2017-07-27 14:20:34","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1506","17","47","2017-07-27 14:21:09","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1507","55","47","2017-07-27 14:21:09","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1508","","47","2017-07-27 14:21:09","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1509","49","47","2017-07-31 16:51:39","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1510","89","47","2017-07-31 16:51:39","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1511","13","47","2017-07-31 16:51:39","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1512","69","47","2017-07-31 16:51:39","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1513","59","47","2017-07-31 16:51:39","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1514","5","47","2017-07-31 16:51:39","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1515","","47","2017-07-31 16:51:39","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1516","49","47","2017-07-31 16:51:46","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1517","89","47","2017-07-31 16:51:46","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1518","13","47","2017-07-31 16:51:46","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1519","69","47","2017-07-31 16:51:46","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1520","59","47","2017-07-31 16:51:46","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1521","5","47","2017-07-31 16:51:46","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1522","","47","2017-07-31 16:51:46","Updated to \'Filled\' Stage (Product: Funky Berry)");

INSERT INTO keg_mainstream_container_history VALUES("1523","83","47","2017-07-31 16:53:14","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1524","57","47","2017-07-31 16:53:14","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1525","43","47","2017-07-31 16:53:14","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1526","80","47","2017-07-31 16:53:14","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1527","91","47","2017-07-31 16:53:14","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1528","28","47","2017-07-31 16:53:14","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1529","","47","2017-07-31 16:53:14","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1530","79","47","2017-07-31 16:54:28","Updated to \'Filled\' Stage (Product: Berliner Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1531","67","47","2017-07-31 16:54:28","Updated to \'Filled\' Stage (Product: Berliner Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1532","6","47","2017-07-31 16:54:28","Updated to \'Filled\' Stage (Product: Berliner Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1533","66","47","2017-07-31 16:54:28","Updated to \'Filled\' Stage (Product: Berliner Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1534","37","47","2017-07-31 16:54:28","Updated to \'Filled\' Stage (Product: Berliner Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1535","","47","2017-07-31 16:54:28","Updated to \'Filled\' Stage (Product: Berliner Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1536","4","47","2017-08-01 10:56:19","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1537","27","47","2017-08-01 10:56:19","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1538","76","47","2017-08-01 10:56:19","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1539","85","47","2017-08-01 10:56:19","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1540","","47","2017-08-01 10:56:19","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1541","14","47","2017-08-01 10:56:49","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1542","","47","2017-08-01 10:56:49","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1543","2","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1544","9","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1545","18","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1546","35","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1547","55","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1548","60","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1549","81","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1550","","47","2017-08-02 09:37:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1551","31","47","2017-08-02 09:58:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1552","40","47","2017-08-02 09:58:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1553","82","47","2017-08-02 09:58:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1554","73","47","2017-08-02 09:58:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1555","72","47","2017-08-02 09:58:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1556","","47","2017-08-02 09:58:54","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1557","78","47","2017-08-02 09:59:28","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1558","83","47","2017-08-02 09:59:28","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1559","","47","2017-08-02 09:59:28","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1560","71","47","2017-08-02 09:59:47","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1561","","47","2017-08-02 09:59:47","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1562","71","47","2017-08-02 09:59:44","Updated to \'Delivered\' Stage (Customer: Reinhard Spiwak)");

INSERT INTO keg_mainstream_container_history VALUES("1563","","47","2017-08-02 09:59:44","Updated to \'Delivered\' Stage (Customer: Reinhard Spiwak)");

INSERT INTO keg_mainstream_container_history VALUES("1564","68","47","2017-08-02 10:00:07","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1565","","47","2017-08-02 10:00:07","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1566","34","47","2017-08-04 10:23:09","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1567","44","47","2017-08-04 10:23:09","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1568","50","47","2017-08-04 10:23:09","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1569","","47","2017-08-04 10:23:09","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1570","34","47","2017-08-04 10:23:23","Container State Changed to \'In Use\'");

INSERT INTO keg_mainstream_container_history VALUES("1571","19","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1572","45","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1573","28","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1574","51","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1575","5","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1576","43","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1577","80","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1578","82","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1579","61","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1580","2","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1581","9","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1582","18","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1583","55","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1584","40","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1585","62","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1586","91","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1587","3","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1588","31","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1589","81","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1590","60","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1591","35","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1592","","47","2017-08-08 12:12:15","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1593","3","47","2017-08-10 10:42:08","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1594","45","47","2017-08-10 10:42:08","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1595","31","47","2017-08-10 10:42:08","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1596","55","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1597","43","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1598","9","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1599","2","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1600","80","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1601","82","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1602","91","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1603","81","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1604","35","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1605","61","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1606","40","47","2017-08-10 10:42:09","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1607","19","47","2017-08-10 10:42:13","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1608","18","47","2017-08-10 10:42:13","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1609","51","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1610","62","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1611","60","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1612","28","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1613","5","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1614","72","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1615","73","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1616","","47","2017-08-10 10:42:14","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1617","28","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1618","40","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1619","60","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1620","18","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1621","5","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1622","62","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1623","35","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1624","91","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1625","2","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1626","3","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1627","43","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1628","80","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1629","51","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1630","45","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1631","19","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1632","61","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1633","81","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1634","31","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1635","82","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1636","9","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1637","55","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1638","72","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1639","73","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1640","","47","2017-08-10 13:36:36","Updated to \'Filled\' Stage (Product: African Vibe)");

INSERT INTO keg_mainstream_container_history VALUES("1641","50","43","2017-08-13 17:17:20","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1642","65","43","2017-08-13 17:18:55","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1643","11","43","2017-08-13 17:19:29","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1644","19","43","2017-08-13 17:19:29","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1645","65","43","2017-08-13 17:19:29","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1646","","43","2017-08-13 17:19:29","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1647","46","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1648","55","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1649","84","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1650","89","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1651","69","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1652","57","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1653","64","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1654","37","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1655","6","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1656","39","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1657","50","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1658","59","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1659","70","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1660","82","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1661","","47","2017-08-14 10:23:20","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1662","37","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1663","39","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1664","57","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1665","50","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1666","55","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1667","64","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1668","82","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1669","6","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1670","","47","2017-08-15 09:19:09","Updated to \'Filled\' Stage (Product: APA)");

INSERT INTO keg_mainstream_container_history VALUES("1671","12","47","2017-08-15 11:21:21","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1672","52","47","2017-08-15 11:21:21","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1673","10","47","2017-08-15 11:21:21","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1674","61","47","2017-08-15 11:21:21","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1675","35","47","2017-08-15 11:21:21","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1676","","47","2017-08-15 11:21:21","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1677","63","47","2017-08-15 13:05:53","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1678","","47","2017-08-15 13:05:53","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1679","87","47","2017-08-16 13:51:18","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1680","17","47","2017-08-16 13:51:18","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1681","","47","2017-08-16 13:51:18","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1682","17","47","2017-08-18 10:10:39","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1683","59","47","2017-08-18 10:10:39","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1684","","47","2017-08-18 10:10:39","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1685","17","47","2017-08-18 10:46:02","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1686","31","47","2017-08-18 10:46:02","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1687","","47","2017-08-18 10:46:02","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1688","87","47","2017-08-18 10:46:34","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1689","","47","2017-08-18 10:46:34","Updated to \'Filled\' Stage (Product: Mainstream R&D)");

INSERT INTO keg_mainstream_container_history VALUES("1690","87","47","2017-08-18 10:47:06","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1691","","47","2017-08-18 10:47:06","Updated to \'Delivered\' Stage (Customer: Machics)");

INSERT INTO keg_mainstream_container_history VALUES("1692","25","47","2017-08-18 10:48:00","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1693","48","47","2017-08-18 10:48:00","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1694","83","47","2017-08-18 10:48:00","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1695","74","47","2017-08-18 10:48:00","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1696","16","47","2017-08-18 10:48:00","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1697","","47","2017-08-18 10:48:00","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1698","74","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1699","85","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1700","88","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1701","90","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1702","77","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1703","32","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1704","4","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1705","86","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1706","83","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1707","16","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1708","25","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1709","54","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1710","27","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1711","48","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1712","58","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1713","","47","2017-08-22 13:55:39","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1714","26","47","2017-08-22 15:52:02","Updated to \'Delivered\' Stage (Customer: PHSOB Club)");

INSERT INTO keg_mainstream_container_history VALUES("1715","6","47","2017-08-22 15:52:02","Updated to \'Delivered\' Stage (Customer: PHSOB Club)");

INSERT INTO keg_mainstream_container_history VALUES("1716","","47","2017-08-22 15:52:02","Updated to \'Delivered\' Stage (Customer: PHSOB Club)");

INSERT INTO keg_mainstream_container_history VALUES("1717","59","47","2017-08-25 10:29:38","Updated to \'Delivered\' Stage (Customer: Pirates Club)");

INSERT INTO keg_mainstream_container_history VALUES("1718","","47","2017-08-25 10:29:38","Updated to \'Delivered\' Stage (Customer: Pirates Club)");

INSERT INTO keg_mainstream_container_history VALUES("1719","4","47","2017-09-01 10:31:29","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1720","88","47","2017-09-01 10:31:29","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1721","","47","2017-09-01 10:31:29","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1722","4","47","2017-09-01 10:34:01","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1723","9","47","2017-09-01 10:34:01","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1724","82","47","2017-09-01 10:34:01","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1725","","47","2017-09-01 10:34:01","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1726","88","47","2017-09-01 10:34:33","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1727","","47","2017-09-01 10:34:33","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1728","8","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1729","47","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1730","63","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1731","11","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1732","7","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1733","34","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1734","19","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1735","14","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1736","","47","2017-09-04 11:25:30","Updated to \'Washed\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1737","78","45","2017-09-05 11:51:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1738","","45","2017-09-05 11:51:37","Updated to \'Returned\' Stage");

INSERT INTO keg_mainstream_container_history VALUES("1739","51","45","2017-09-05 11:52:19","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1740","","45","2017-09-05 11:52:19","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1741","63","45","2017-09-05 11:53:12","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1742","","45","2017-09-05 11:53:12","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1743","63","45","2017-09-05 11:53:30","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1744","","45","2017-09-05 11:53:30","Updated to \'Delivered\' Stage (Customer: Oze Cafe and Bistro cc)");

INSERT INTO keg_mainstream_container_history VALUES("1745","11","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1746","14","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1747","34","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1748","48","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1749","47","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1750","8","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1751","77","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1752","89","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1753","84","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1754","69","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1755","19","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1756","","47","2017-09-05 14:03:30","Updated to \'Filled\' Stage (Product: Summer Weiss)");

INSERT INTO keg_mainstream_container_history VALUES("1757","69","47","2017-09-06 09:19:30","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1758","19","47","2017-09-06 09:19:30","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1759","45","47","2017-09-06 09:19:30","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1760","43","47","2017-09-06 09:19:30","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1761","57","47","2017-09-06 09:19:30","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1762","33","47","2017-09-06 09:19:30","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1763","","47","2017-09-06 09:19:30","Updated to \'Delivered\' Stage (Customer: Buffelspoort ATKV)");

INSERT INTO keg_mainstream_container_history VALUES("1764","7","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1765","25","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1766","90","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1767","46","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1768","32","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1769","58","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1770","85","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1771","16","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1772","83","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1773","54","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1774","","47","2017-09-08 10:13:22","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1775","27","47","2017-09-08 10:15:30","Updated to \'Filled\' Stage (Product: Ginger Gin & Tonic)");

INSERT INTO keg_mainstream_container_history VALUES("1776","86","47","2017-09-08 10:15:36","Updated to \'Filled\' Stage (Product: Ginger Gin & Tonic)");

INSERT INTO keg_mainstream_container_history VALUES("1777","","47","2017-09-08 10:15:36","Updated to \'Filled\' Stage (Product: Ginger Gin & Tonic)");

INSERT INTO keg_mainstream_container_history VALUES("1778","27","47","2017-09-08 10:15:36","Updated to \'Filled\' Stage (Product: Ginger Gin & Tonic)");

INSERT INTO keg_mainstream_container_history VALUES("1779","86","47","2017-09-08 10:15:36","Updated to \'Filled\' Stage (Product: Ginger Gin & Tonic)");

INSERT INTO keg_mainstream_container_history VALUES("1780","","47","2017-09-08 10:15:36","Updated to \'Filled\' Stage (Product: Ginger Gin & Tonic)");

INSERT INTO keg_mainstream_container_history VALUES("1781","92","47","2017-09-08 10:18:24","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1782","92","47","2017-09-08 10:18:56","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1783","","47","2017-09-08 10:18:56","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1784","93","47","2017-09-08 10:19:49","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1785","93","47","2017-09-08 10:20:15","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1786","","47","2017-09-08 10:20:15","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1787","92","47","2017-09-08 10:20:37","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1788","94","47","2017-09-08 10:24:14","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1789","94","47","2017-09-08 10:24:33","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1790","","47","2017-09-08 10:24:33","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1791","95","47","2017-09-08 10:30:12","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1792","95","47","2017-09-08 10:30:36","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1793","","47","2017-09-08 10:30:36","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1794","96","47","2017-09-08 10:43:59","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1795","96","47","2017-09-08 10:44:18","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1796","","47","2017-09-08 10:44:18","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1797","97","47","2017-09-08 10:47:33","Updated");

INSERT INTO keg_mainstream_container_history VALUES("1798","97","47","2017-09-08 10:47:59","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1799","","47","2017-09-08 10:47:59","Updated to \'Filled\' Stage (Product: Affirmative Stout)");

INSERT INTO keg_mainstream_container_history VALUES("1800","84","47","2017-09-08 11:34:56","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1801","77","47","2017-09-08 11:34:56","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1802","","47","2017-09-08 11:34:56","Updated to \'Delivered\' Stage (Customer: Stanley Beer Yard)");

INSERT INTO keg_mainstream_container_history VALUES("1803","2","47","2017-09-08 11:35:36","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1804","29","47","2017-09-08 11:35:36","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1805","83","47","2017-09-08 11:35:36","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1806","89","47","2017-09-08 11:35:36","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1807","","47","2017-09-08 11:35:36","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1808","3","44","2017-09-09 11:31:36","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1809","","44","2017-09-09 11:31:36","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1810","8","44","2017-09-09 11:32:03","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");

INSERT INTO keg_mainstream_container_history VALUES("1811","","44","2017-09-09 11:32:03","Updated to \'Delivered\' Stage (Customer: The Higher Ground)");




DROP TABLE keg_mainstream_container_notes;

CREATE TABLE `keg_mainstream_container_notes` (
  `container_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_serial_number` varchar(50) NOT NULL,
  `container_note` varchar(200) DEFAULT NULL,
  `container_note_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`container_note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_container_notes VALUES("1","53","Modified Spear for removal","2016-10-09 18:50:54","43");

INSERT INTO keg_mainstream_container_notes VALUES("2","18","Keg gassy - Returned and swapped out with other keg","2016-10-11 08:45:34","43");

INSERT INTO keg_mainstream_container_notes VALUES("3","41","Returned by Sean on 18 October 2016 - Check","2016-10-18 10:17:06","43");

INSERT INTO keg_mainstream_container_notes VALUES("4","82","Returned by Sean on 18 October 2016 - Check","2016-10-18 10:17:44","43");




DROP TABLE keg_mainstream_containers;

CREATE TABLE `keg_mainstream_containers` (
  `container_serial_number` varchar(50) NOT NULL,
  `container_bar_code` varchar(50) NOT NULL,
  `container_type` varchar(50) NOT NULL COMMENT 'comes from the types table',
  `container_capacity` varchar(50) NOT NULL,
  `container_capacity_type` varchar(50) DEFAULT NULL,
  `container_stage` int(11) NOT NULL,
  `container_state` int(11) DEFAULT NULL,
  `container_date` datetime NOT NULL,
  `container_contents` int(11) NOT NULL,
  `container_client` varchar(50) DEFAULT NULL,
  `type_name` varbinary(50) DEFAULT NULL,
  `sell_by_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `batch_number` varchar(250) DEFAULT NULL,
  `filled_date` date DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  PRIMARY KEY (`container_serial_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_containers VALUES("1","00342201","2","27","L","3","1","2016-10-10 12:11:08","3","","","2017-11-24","2017-12-24","","2017-07-27","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("10","00342210","2","27","L","4","1","2016-11-29 06:36:57","7","19","","2017-10-20","2017-11-19","","2017-06-22","2017-08-15");

INSERT INTO keg_mainstream_containers VALUES("100","00342300","5","0","L","6","8","2016-10-10 12:33:58","0","","","1970-01-01","1970-01-01","","1970-01-01","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("11","00342211","2","27","L","3","1","2016-10-10 12:14:28","4","","","2018-01-03","2018-02-02","","2017-09-05","2017-08-12");

INSERT INTO keg_mainstream_containers VALUES("12","00342212","2","27","L","4","1","2016-10-16 05:06:00","4","19","","2017-10-27","2017-11-26","","2017-06-29","2017-08-15");

INSERT INTO keg_mainstream_containers VALUES("13","00342213","2","30","L","3","1","2017-04-23 12:04:00","5","","","2017-11-28","2017-12-28","","2017-07-31","2017-04-19");

INSERT INTO keg_mainstream_containers VALUES("14","00342214","2","27","L","3","1","2016-10-13 09:42:19","4","","","2018-01-03","2018-02-02","","2017-09-05","2017-08-01");

INSERT INTO keg_mainstream_containers VALUES("15","00342215","2","27","L","3","1","2016-11-06 08:31:21","3","","","2017-11-24","2017-12-24","","2017-07-27","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("16","00342216","2","27","L","3","1","2016-10-16 05:06:55","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-07-13");

INSERT INTO keg_mainstream_containers VALUES("17","00342217","2","27","L","4","1","2017-05-15 04:23:37","4","10","","2017-12-16","2018-01-15","","2017-08-18","2017-08-18");

INSERT INTO keg_mainstream_containers VALUES("18","00342218","2","27","L","3","1","2016-10-11 08:45:34","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-29");

INSERT INTO keg_mainstream_containers VALUES("19","00342219","2","27","L","4","1","2017-05-15 12:03:56","4","19","","2018-01-03","2018-02-02","","2017-09-05","2017-09-06");

INSERT INTO keg_mainstream_containers VALUES("2","00342202","2","30","L","4","1","2017-03-20 11:50:20","7","21","","2017-12-08","2018-01-07","","2017-08-10","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("20","00342220","2","27","L","4","1","2016-10-10 12:19:31","3","4","","2017-01-01","2017-01-31","","2016-09-03","2016-10-13");

INSERT INTO keg_mainstream_containers VALUES("21","00342221","4","27","L","2","6","2016-10-09 07:00:57","0","","","0000-00-00","0000-00-00","","1970-01-01","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("22","00342222","2","27","L","1","7","2016-10-09 07:04:19","0","","","1970-01-01","1970-01-01","","1970-01-01","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("23","00342223","2","27","L","1","7","2016-10-09 07:04:40","0","","","1970-01-01","1970-01-01","","1970-01-01","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("24","00342224","2","27","L","4","1","2016-11-06 08:36:18","3","4","","2017-03-06","2017-08-03","","1970-01-01","2016-09-24");

INSERT INTO keg_mainstream_containers VALUES("25","00342225","2","27","L","3","1","2016-10-10 12:15:28","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-07-13");

INSERT INTO keg_mainstream_containers VALUES("26","00342226","2","27","L","4","1","2016-10-10 12:18:01","4","22","","2017-10-27","2017-11-26","","2017-06-29","2017-08-22");

INSERT INTO keg_mainstream_containers VALUES("27","00342227","2","27","L","3","1","2016-12-14 01:47:37","13","","","2018-03-27","2018-05-06","","2017-09-08","2017-08-01");

INSERT INTO keg_mainstream_containers VALUES("28","00342228","2","27","L","3","1","2016-10-16 04:59:47","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-02");

INSERT INTO keg_mainstream_containers VALUES("29","00342229","2","27","L","4","1","2016-10-10 12:18:13","3","21","","2017-11-24","2017-12-24","","2017-07-27","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("3","00342203","2","27","L","4","1","2016-10-10 12:12:11","7","21","","2017-12-08","2018-01-07","","2017-08-10","2017-09-09");

INSERT INTO keg_mainstream_containers VALUES("30","00342230","2","27","L","4","1","2016-11-06 09:23:40","8","9","","2017-09-30","2017-10-30","","2017-06-02","2017-06-21");

INSERT INTO keg_mainstream_containers VALUES("31","00342231","2","27","L","4","1","2016-10-10 12:20:12","7","10","","2017-12-08","2018-01-07","","2017-08-10","2017-08-18");

INSERT INTO keg_mainstream_containers VALUES("32","00342232","2","27","L","3","1","2016-10-10 12:20:26","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-06-14");

INSERT INTO keg_mainstream_containers VALUES("33","00342233","2","27","L","4","1","2017-05-15 05:04:08","3","19","","2017-11-24","2017-12-24","","2017-07-27","2017-09-06");

INSERT INTO keg_mainstream_containers VALUES("34","00342234","2","27","L","3","1","2016-10-10 12:21:08","4","","","2018-01-03","2018-02-02","","2017-09-05","2017-08-04");

INSERT INTO keg_mainstream_containers VALUES("35","00342235","2","27","L","4","1","2016-10-10 12:21:32","7","19","","2017-12-08","2018-01-07","","2017-08-10","2017-08-15");

INSERT INTO keg_mainstream_containers VALUES("36","00342236","4","27","L","5","6","2016-10-09 07:01:28","0","","","1970-01-01","1970-01-01","","1970-01-01","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("37","00342237","2","27","L","3","1","2016-10-10 12:22:02","3","","","2017-12-13","2018-01-12","","2017-08-15","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("38","00342238","2","27","L","3","1","2016-10-13 09:36:01","3","","","2017-11-24","2017-12-24","","2017-07-27","2017-05-04");

INSERT INTO keg_mainstream_containers VALUES("39","00342239","2","27","L","3","1","2016-10-10 12:22:26","3","","","2017-12-13","2018-01-12","","2017-08-15","2017-03-23");

INSERT INTO keg_mainstream_containers VALUES("4","00342204","2","27","L","4","1","2016-10-11 03:32:05","4","21","","2017-12-30","2018-01-29","","2017-09-01","2017-09-01");

INSERT INTO keg_mainstream_containers VALUES("40","00342240","2","27","L","3","1","2016-10-10 12:22:42","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-02");

INSERT INTO keg_mainstream_containers VALUES("41","00342241","2","27","L","3","1","2017-05-15 05:02:37","3","","","2017-11-24","2017-12-24","","2017-07-27","2017-05-25");

INSERT INTO keg_mainstream_containers VALUES("42","00342242","2","27","L","4","1","2016-11-28 09:24:04","7","11","","2017-07-27","2017-08-26","","2017-03-29","2017-04-05");

INSERT INTO keg_mainstream_containers VALUES("43","00342243","2","27","L","4","1","2016-11-29 06:35:57","7","19","","2017-12-08","2018-01-07","","2017-08-10","2017-09-06");

INSERT INTO keg_mainstream_containers VALUES("44","00342244","2","27","L","4","1","2016-11-06 08:43:27","8","21","","2017-09-30","2017-10-30","","2017-06-02","2017-08-04");

INSERT INTO keg_mainstream_containers VALUES("45","00342245","2","27","L","4","1","2016-10-10 12:23:41","7","19","","2017-12-08","2018-01-07","","2017-08-10","2017-09-06");

INSERT INTO keg_mainstream_containers VALUES("46","00342246","2","27","L","3","1","2016-11-06 08:44:11","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-06-02");

INSERT INTO keg_mainstream_containers VALUES("47","00342247","2","27","L","3","1","2016-10-10 12:24:08","4","","","2018-01-03","2018-02-02","","2017-09-05","2017-07-13");

INSERT INTO keg_mainstream_containers VALUES("48","00342248","2","27","L","3","1","2016-10-10 12:24:16","4","","","2018-01-03","2018-02-02","","2017-09-05","2017-07-13");

INSERT INTO keg_mainstream_containers VALUES("49","00342249","2","27","L","3","1","2016-11-06 09:20:48","5","","","2017-11-28","2017-12-28","","2017-07-31","2016-10-10");

INSERT INTO keg_mainstream_containers VALUES("5","00342205","2","27","L","3","1","2016-12-08 11:59:14","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-05-18");

INSERT INTO keg_mainstream_containers VALUES("50","00342250","2","27","L","3","1","2017-08-13 05:17:20","3","","","2017-12-13","2018-01-12","","2017-08-15","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("51","00342251","2","27","L","4","1","2016-10-18 10:16:06","7","10","","2017-12-08","2018-01-07","","2017-08-10","2017-09-05");

INSERT INTO keg_mainstream_containers VALUES("52","00342252","2","27","L","4","1","2016-10-10 12:25:37","4","19","","2017-10-27","2017-11-26","","2017-06-29","2017-08-15");

INSERT INTO keg_mainstream_containers VALUES("53","00342253","2","27","L","2","6","2016-10-10 12:17:04","0","","","0000-00-00","0000-00-00","","2017-05-25","2017-03-29");

INSERT INTO keg_mainstream_containers VALUES("54","00342254","2","27","L","3","1","2016-12-08 11:59:31","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-06-02");

INSERT INTO keg_mainstream_containers VALUES("55","00342255","2","27","L","3","1","2016-10-11 08:46:23","3","","","2017-12-13","2018-01-12","","2017-08-15","2017-07-27");

INSERT INTO keg_mainstream_containers VALUES("56","00342256","2","27","L","3","1","2016-10-10 12:26:33","3","","","2017-11-24","2017-12-24","","2017-07-27","2017-02-01");

INSERT INTO keg_mainstream_containers VALUES("57","00342257","2","27","L","4","1","2016-10-10 12:26:45","3","19","","2017-12-13","2018-01-12","","2017-08-15","2017-09-06");

INSERT INTO keg_mainstream_containers VALUES("58","00342258","2","27","L","3","1","2016-10-10 12:26:59","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-05-16");

INSERT INTO keg_mainstream_containers VALUES("59","00342259","2","27","L","4","1","2016-10-10 12:27:13","4","23","","2017-12-16","2018-01-15","","2017-08-18","2017-08-25");

INSERT INTO keg_mainstream_containers VALUES("6","00342206","2","27","L","4","1","2016-10-10 12:13:07","3","22","","2017-12-13","2018-01-12","","2017-08-15","2017-08-22");

INSERT INTO keg_mainstream_containers VALUES("60","00342260","2","27","L","3","1","2016-10-10 12:34:59","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-21");

INSERT INTO keg_mainstream_containers VALUES("61","00342261","2","27","L","4","1","2016-10-13 09:40:07","7","19","","2017-12-08","2018-01-07","","2017-08-10","2017-08-15");

INSERT INTO keg_mainstream_containers VALUES("62","00342262","2","27","L","3","1","2016-12-12 12:23:52","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-07-25");

INSERT INTO keg_mainstream_containers VALUES("63","00342263","2","27","L","4","1","2016-11-29 06:35:05","4","10","","2018-01-03","2018-02-02","","2017-09-05","2017-09-05");

INSERT INTO keg_mainstream_containers VALUES("64","00342264","2","27","L","3","1","2017-04-23 12:03:11","3","","","2017-12-13","2018-01-12","","2017-08-15","2017-04-20");

INSERT INTO keg_mainstream_containers VALUES("65","00342265","2","27","L","4","1","2017-08-13 05:18:55","11","21","","2017-09-30","2018-02-27","","2017-06-02","2017-08-12");

INSERT INTO keg_mainstream_containers VALUES("66","00342266","2","27","L","3","1","2016-11-27 07:38:36","10","","","2017-11-28","2017-12-28","","2017-07-31","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("67","00342267","2","27","L","3","1","2016-10-10 12:40:03","10","","","2017-11-28","2017-12-28","","2017-07-31","2017-06-02");

INSERT INTO keg_mainstream_containers VALUES("68","00342268","2","27","L","4","1","2016-10-10 12:40:17","8","9","","2017-09-30","2017-10-30","","2017-06-02","2017-08-02");

INSERT INTO keg_mainstream_containers VALUES("69","00342269","2","30","L","4","1","2017-03-20 11:51:04","4","19","","2018-01-03","2018-02-02","","2017-09-05","2017-09-06");

INSERT INTO keg_mainstream_containers VALUES("7","00342207","2","27","L","3","1","2016-11-06 08:28:13","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-07-25");

INSERT INTO keg_mainstream_containers VALUES("70","00342270","1","10","L","2","1","2016-10-16 05:08:19","0","","","0000-00-00","0000-00-00","","2017-06-02","2016-10-10");

INSERT INTO keg_mainstream_containers VALUES("71","00342271","1","10","L","4","1","2016-10-10 12:58:35","7","20","","2017-11-30","2017-12-30","","2017-08-02","2017-08-02");

INSERT INTO keg_mainstream_containers VALUES("72","00342272","1","10","L","3","1","2016-11-29 06:34:06","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-09");

INSERT INTO keg_mainstream_containers VALUES("73","00342273","1","10","L","3","1","2016-10-16 05:09:30","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-02");

INSERT INTO keg_mainstream_containers VALUES("74","00342274","1","10","L","2","1","2016-11-27 07:37:53","0","","","0000-00-00","0000-00-00","","2017-06-09","2017-06-09");

INSERT INTO keg_mainstream_containers VALUES("75","00342275","1","10","L","4","1","2016-11-27 07:38:12","7","20","","2017-11-10","2017-12-10","","2017-07-13","2017-07-13");

INSERT INTO keg_mainstream_containers VALUES("76","00342276","2","27","L","4","1","2016-10-10 12:41:46","4","19","","2017-10-27","2017-11-26","","2017-06-29","2017-08-01");

INSERT INTO keg_mainstream_containers VALUES("77","00342277","2","27","L","4","1","2016-10-10 12:42:07","4","18","","2018-01-03","2018-02-02","","2017-09-05","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("78","00342278","2","27","L","5","1","2016-11-06 09:24:16","4","","","2017-10-27","2017-11-26","","2017-06-29","2017-08-02");

INSERT INTO keg_mainstream_containers VALUES("79","00342279","2","20","L","3","1","2016-10-10 12:42:49","10","","","2017-11-28","2017-12-28","","2017-07-31","2017-05-16");

INSERT INTO keg_mainstream_containers VALUES("8","00342208","2","27","L","4","1","2016-11-29 06:36:28","4","21","","2018-01-03","2018-02-02","","2017-09-05","2017-09-09");

INSERT INTO keg_mainstream_containers VALUES("80","00342280","2","27","L","3","1","2016-10-10 12:55:18","7","","","2017-12-08","2018-01-07","","2017-08-10","2016-11-28");

INSERT INTO keg_mainstream_containers VALUES("81","00342281","2","27","L","3","1","2016-10-10 12:56:14","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-29");

INSERT INTO keg_mainstream_containers VALUES("82","00342282","2","27","L","4","1","2016-10-18 10:17:44","3","21","","2017-12-13","2018-01-12","","2017-08-15","2017-09-01");

INSERT INTO keg_mainstream_containers VALUES("83","00342283","2","27","L","4","1","2016-10-13 09:36:20","11","21","","2018-01-06","2018-02-05","","2017-09-08","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("84","00342284","2","27","L","4","1","2016-10-10 12:56:52","4","18","","2018-01-03","2018-02-02","","2017-09-05","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("85","00342285","2","27","L","3","1","2016-10-10 12:57:02","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-08-01");

INSERT INTO keg_mainstream_containers VALUES("86","00342286","2","30","L","3","1","2017-03-20 11:51:32","13","","","2018-03-27","2018-05-06","","2017-09-08","2017-05-16");

INSERT INTO keg_mainstream_containers VALUES("87","00342287","2","27","L","4","1","2016-10-10 12:57:29","8","9","","2017-12-16","2018-01-15","","2017-08-18","2017-08-18");

INSERT INTO keg_mainstream_containers VALUES("88","00342288","2","27","L","4","1","2016-10-10 12:57:38","4","18","","2017-12-30","2018-01-29","","2017-09-01","2017-09-01");

INSERT INTO keg_mainstream_containers VALUES("89","00342289","2","27","L","4","1","2016-10-10 12:57:48","4","21","","2018-01-03","2018-02-02","","2017-09-05","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("9","00342209","2","27","L","4","1","2016-11-06 08:29:25","7","21","","2017-12-08","2018-01-07","","2017-08-10","2017-09-01");

INSERT INTO keg_mainstream_containers VALUES("90","00342290","2","27","L","3","1","2016-10-10 12:57:59","11","","","2018-01-06","2018-02-05","","2017-09-08","2017-06-29");

INSERT INTO keg_mainstream_containers VALUES("91","00342291","2","27","L","3","1","2016-10-10 12:58:09","7","","","2017-12-08","2018-01-07","","2017-08-10","2017-06-02");

INSERT INTO keg_mainstream_containers VALUES("92","00342292","2","30","L","3","1","2017-09-08 10:20:37","11","0","","2018-01-06","2018-02-05","","2017-09-08","2016-10-10");

INSERT INTO keg_mainstream_containers VALUES("93","00342293","2","30","L","3","1","2017-09-08 10:19:49","11","0","","2018-01-06","2018-02-05","","2017-09-08","2016-10-10");

INSERT INTO keg_mainstream_containers VALUES("94","00342294","2","30","L","3","1","2017-09-08 10:24:13","11","0","","2018-01-06","2018-02-05","","2017-09-08","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("95","00342295","2","30","L","3","1","2017-09-08 10:30:12","11","0","","2018-01-06","2018-02-05","","2017-09-08","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("96","00342296","2","30","L","3","1","2017-09-08 10:43:59","11","0","","2018-01-06","2018-02-05","","2017-09-08","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("97","00342297","2","30","L","3","1","2017-09-08 10:47:33","11","0","","2018-01-06","2018-02-05","","2017-09-08","2017-09-08");

INSERT INTO keg_mainstream_containers VALUES("98","00342298","5","0","L","6","8","2016-10-10 12:33:32","0","","","1970-01-01","1970-01-01","","1970-01-01","1970-01-01");

INSERT INTO keg_mainstream_containers VALUES("99","00342299","5","0","L","6","8","2016-10-10 12:33:46","0","","","1970-01-01","1970-01-01","","1970-01-01","1970-01-01");




DROP TABLE keg_mainstream_module_permissions;

CREATE TABLE `keg_mainstream_module_permissions` (
  `permission_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) unsigned NOT NULL,
  `permission_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `permission_desc` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_module` char(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1=Yes, 0=No',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO keg_mainstream_module_permissions VALUES("1","1","general_access","Allow access to the dashboard module","0");

INSERT INTO keg_mainstream_module_permissions VALUES("2","2","general_access","Allow access to the Keg Management module","0");

INSERT INTO keg_mainstream_module_permissions VALUES("3","3","general_access","Allow access to the Products Module","0");

INSERT INTO keg_mainstream_module_permissions VALUES("4","4","general_access","Allow access to the Customer Manager module","0");

INSERT INTO keg_mainstream_module_permissions VALUES("5","5","general_access","Allow this user to the Reports Module","0");

INSERT INTO keg_mainstream_module_permissions VALUES("6","6","general_access","Allow this user to the Account Management Module","0");

INSERT INTO keg_mainstream_module_permissions VALUES("8","8","general_access","Allow access to the Settings module","0");

INSERT INTO keg_mainstream_module_permissions VALUES("9","5","container_reports","Allow access to View Container Reports","0");

INSERT INTO keg_mainstream_module_permissions VALUES("10","1","account_expiry_block","Allow access to View Company Expiry date block","0");

INSERT INTO keg_mainstream_module_permissions VALUES("12","1","container_stages_block","Allow access to View Container Stages Block on the Dashboard","0");

INSERT INTO keg_mainstream_module_permissions VALUES("13","1","container_states_block","Allow access to View Container States Block on the Dashboard","0");

INSERT INTO keg_mainstream_module_permissions VALUES("14","8","application_settings_button","Allow access to View Application Settings button in Settings management","0");

INSERT INTO keg_mainstream_module_permissions VALUES("15","8","backup_settings_button","Allow access to View Button to Backup information in settings Management ","0");

INSERT INTO keg_mainstream_module_permissions VALUES("16","8","company_settings_button","Allow access to view Company Details Button in Settings Management","0");

INSERT INTO keg_mainstream_module_permissions VALUES("17","1","product_status_block","Allow access to View Products Status Block on the Dashboard","0");

INSERT INTO keg_mainstream_module_permissions VALUES("18","8","about_application_button","Allow access to View the About Application Button","0");

INSERT INTO keg_mainstream_module_permissions VALUES("19","8","subscription_button","Allow access to view the Subscription Details Button","0");

INSERT INTO keg_mainstream_module_permissions VALUES("20","2","enable_advanced","Enable advanced container editing","0");

INSERT INTO keg_mainstream_module_permissions VALUES("21","2","enable_editing","Allow user to edit container","0");




DROP TABLE keg_mainstream_registered_devices;

CREATE TABLE `keg_mainstream_registered_devices` (
  `device_id` varbinary(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_name` varchar(100) DEFAULT NULL,
  `device_active` varchar(2) DEFAULT 'N',
  `access_granted` varchar(2) DEFAULT 'N',
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE keg_mainstream_stages;

CREATE TABLE `keg_mainstream_stages` (
  `stage_id` int(11) NOT NULL AUTO_INCREMENT,
  `stage_name` varchar(100) DEFAULT NULL,
  `active` varchar(2) DEFAULT 'Y',
  `stage_order` int(1) DEFAULT NULL,
  `stage_description` varchar(20) DEFAULT NULL,
  `onselect` int(1) unsigned DEFAULT NULL,
  `is_default` char(1) DEFAULT 'N',
  PRIMARY KEY (`stage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_stages VALUES("1","Empty","Y","","Empty","5","Y");

INSERT INTO keg_mainstream_stages VALUES("2","Washed","Y","","Washed","5","N");

INSERT INTO keg_mainstream_stages VALUES("3","Filled","Y","","Filled","1","N");

INSERT INTO keg_mainstream_stages VALUES("4","Delivered","Y","","Delivered","2","N");

INSERT INTO keg_mainstream_stages VALUES("5","Returned","Y","","Returned","4","N");

INSERT INTO keg_mainstream_stages VALUES("6","Unused","Y","","","0","N");

INSERT INTO keg_mainstream_stages VALUES("7","In Transit","Y","","In transit with Sale","2","N");




DROP TABLE keg_mainstream_states;

CREATE TABLE `keg_mainstream_states` (
  `state_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `state_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `out_of_circulation` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO keg_mainstream_states VALUES("1","In Use","","","Y");

INSERT INTO keg_mainstream_states VALUES("2","Damaged (Repairable)","","","N");

INSERT INTO keg_mainstream_states VALUES("3","Damaged (Unrepairable)","","","N");

INSERT INTO keg_mainstream_states VALUES("4","Lost","","","N");

INSERT INTO keg_mainstream_states VALUES("5","Stolen","","","N");

INSERT INTO keg_mainstream_states VALUES("6","Modified Spear","","","N");

INSERT INTO keg_mainstream_states VALUES("7","Serial Number Scrapped","Y","","N");

INSERT INTO keg_mainstream_states VALUES("8","Unused","","","N");

INSERT INTO keg_mainstream_states VALUES("9","Keg Contents Bad","","","N");

INSERT INTO keg_mainstream_states VALUES("10","Keg Contents Over Carbonated","","","N");

INSERT INTO keg_mainstream_states VALUES("11","Contents Used Before -Sample","","","N");

INSERT INTO keg_mainstream_states VALUES("12","Partly used at a previous event","","","N");




DROP TABLE keg_mainstream_terminology;

CREATE TABLE `keg_mainstream_terminology` (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_singular` varchar(50) DEFAULT NULL,
  `container_plural` varchar(50) DEFAULT NULL,
  `stage_singular` varchar(50) DEFAULT NULL,
  `stage_plural` varchar(50) DEFAULT NULL,
  `state_singular` varchar(50) DEFAULT NULL,
  `state_plural` varchar(50) DEFAULT NULL,
  `client_singular` varchar(50) DEFAULT NULL,
  `client_plural` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_terminology VALUES("1","Keg","Kegs","Stage","Stages","Condition","Conditions","Customer","Customers");




DROP TABLE keg_mainstream_types;

CREATE TABLE `keg_mainstream_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) DEFAULT NULL,
  `type_description` varchar(100) DEFAULT NULL,
  `active` varchar(2) DEFAULT 'Y',
  `type_expiration` int(11) DEFAULT NULL,
  `type_best_before` int(11) DEFAULT NULL,
  PRIMARY KEY (`type_id`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_types VALUES("1","Altbier","Altbier","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("2","Africn Vibe","African Vibe","N","1","1");

INSERT INTO keg_mainstream_types VALUES("3","APA","American Pale Ale","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("4","Summer Weiss","Summer Weiss","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("5","Funky Berry","Funky Berry","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("6","Weiss","","N","0","0");

INSERT INTO keg_mainstream_types VALUES("7","African Vibe","","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("8","Mainstream R&D","Mainstream R&D","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("9","R.I.P Ale","Rest In Peace","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("10","Berliner Weiss","","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("11","Affirmative Stout","","Y","150","120");

INSERT INTO keg_mainstream_types VALUES("12","Plain Gin & Tonic","","Y","240","200");

INSERT INTO keg_mainstream_types VALUES("13","Ginger Gin & Tonic","","Y","240","200");




DROP TABLE keg_mainstream_user_access;

CREATE TABLE `keg_mainstream_user_access` (
  `user_id` int(10) unsigned NOT NULL,
  `permission_id` int(11) unsigned DEFAULT NULL,
  `access` char(1) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0=No, 1=Yes',
  UNIQUE KEY `user_id_security_id` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO keg_mainstream_user_access VALUES("43","1","1");

INSERT INTO keg_mainstream_user_access VALUES("43","2","1");

INSERT INTO keg_mainstream_user_access VALUES("43","3","1");

INSERT INTO keg_mainstream_user_access VALUES("43","4","1");

INSERT INTO keg_mainstream_user_access VALUES("43","5","1");

INSERT INTO keg_mainstream_user_access VALUES("43","6","1");

INSERT INTO keg_mainstream_user_access VALUES("43","8","1");

INSERT INTO keg_mainstream_user_access VALUES("43","9","1");

INSERT INTO keg_mainstream_user_access VALUES("43","10","1");

INSERT INTO keg_mainstream_user_access VALUES("43","12","1");

INSERT INTO keg_mainstream_user_access VALUES("43","13","1");

INSERT INTO keg_mainstream_user_access VALUES("43","14","0");

INSERT INTO keg_mainstream_user_access VALUES("43","15","1");

INSERT INTO keg_mainstream_user_access VALUES("43","16","1");

INSERT INTO keg_mainstream_user_access VALUES("43","17","1");

INSERT INTO keg_mainstream_user_access VALUES("43","18","0");

INSERT INTO keg_mainstream_user_access VALUES("43","19","1");

INSERT INTO keg_mainstream_user_access VALUES("43","20","1");

INSERT INTO keg_mainstream_user_access VALUES("43","21","1");

INSERT INTO keg_mainstream_user_access VALUES("44","1","1");

INSERT INTO keg_mainstream_user_access VALUES("44","2","1");

INSERT INTO keg_mainstream_user_access VALUES("44","3","1");

INSERT INTO keg_mainstream_user_access VALUES("44","4","1");

INSERT INTO keg_mainstream_user_access VALUES("44","5","1");

INSERT INTO keg_mainstream_user_access VALUES("44","6","1");

INSERT INTO keg_mainstream_user_access VALUES("44","8","1");

INSERT INTO keg_mainstream_user_access VALUES("44","9","1");

INSERT INTO keg_mainstream_user_access VALUES("44","10","1");

INSERT INTO keg_mainstream_user_access VALUES("44","12","1");

INSERT INTO keg_mainstream_user_access VALUES("44","13","1");

INSERT INTO keg_mainstream_user_access VALUES("44","14","0");

INSERT INTO keg_mainstream_user_access VALUES("44","15","1");

INSERT INTO keg_mainstream_user_access VALUES("44","16","1");

INSERT INTO keg_mainstream_user_access VALUES("44","17","1");

INSERT INTO keg_mainstream_user_access VALUES("44","18","0");

INSERT INTO keg_mainstream_user_access VALUES("44","19","1");

INSERT INTO keg_mainstream_user_access VALUES("44","20","1");

INSERT INTO keg_mainstream_user_access VALUES("44","21","1");

INSERT INTO keg_mainstream_user_access VALUES("45","1","1");

INSERT INTO keg_mainstream_user_access VALUES("45","2","1");

INSERT INTO keg_mainstream_user_access VALUES("45","3","0");

INSERT INTO keg_mainstream_user_access VALUES("45","4","0");

INSERT INTO keg_mainstream_user_access VALUES("45","5","1");

INSERT INTO keg_mainstream_user_access VALUES("45","6","0");

INSERT INTO keg_mainstream_user_access VALUES("45","8","0");

INSERT INTO keg_mainstream_user_access VALUES("45","9","1");

INSERT INTO keg_mainstream_user_access VALUES("45","10","0");

INSERT INTO keg_mainstream_user_access VALUES("45","12","1");

INSERT INTO keg_mainstream_user_access VALUES("45","13","1");

INSERT INTO keg_mainstream_user_access VALUES("45","14","0");

INSERT INTO keg_mainstream_user_access VALUES("45","15","1");

INSERT INTO keg_mainstream_user_access VALUES("45","16","1");

INSERT INTO keg_mainstream_user_access VALUES("45","17","1");

INSERT INTO keg_mainstream_user_access VALUES("45","18","0");

INSERT INTO keg_mainstream_user_access VALUES("45","19","0");

INSERT INTO keg_mainstream_user_access VALUES("45","20","0");

INSERT INTO keg_mainstream_user_access VALUES("45","21","0");

INSERT INTO keg_mainstream_user_access VALUES("46","1","1");

INSERT INTO keg_mainstream_user_access VALUES("46","2","1");

INSERT INTO keg_mainstream_user_access VALUES("46","3","0");

INSERT INTO keg_mainstream_user_access VALUES("46","4","1");

INSERT INTO keg_mainstream_user_access VALUES("46","5","1");

INSERT INTO keg_mainstream_user_access VALUES("46","6","0");

INSERT INTO keg_mainstream_user_access VALUES("46","8","0");

INSERT INTO keg_mainstream_user_access VALUES("46","9","1");

INSERT INTO keg_mainstream_user_access VALUES("46","10","0");

INSERT INTO keg_mainstream_user_access VALUES("46","12","1");

INSERT INTO keg_mainstream_user_access VALUES("46","13","1");

INSERT INTO keg_mainstream_user_access VALUES("46","14","0");

INSERT INTO keg_mainstream_user_access VALUES("46","15","1");

INSERT INTO keg_mainstream_user_access VALUES("46","16","1");

INSERT INTO keg_mainstream_user_access VALUES("46","17","1");

INSERT INTO keg_mainstream_user_access VALUES("46","18","0");

INSERT INTO keg_mainstream_user_access VALUES("46","19","0");

INSERT INTO keg_mainstream_user_access VALUES("46","20","0");

INSERT INTO keg_mainstream_user_access VALUES("46","21","0");

INSERT INTO keg_mainstream_user_access VALUES("47","1","1");

INSERT INTO keg_mainstream_user_access VALUES("47","2","1");

INSERT INTO keg_mainstream_user_access VALUES("47","3","0");

INSERT INTO keg_mainstream_user_access VALUES("47","4","1");

INSERT INTO keg_mainstream_user_access VALUES("47","5","1");

INSERT INTO keg_mainstream_user_access VALUES("47","6","0");

INSERT INTO keg_mainstream_user_access VALUES("47","8","0");

INSERT INTO keg_mainstream_user_access VALUES("47","9","1");

INSERT INTO keg_mainstream_user_access VALUES("47","10","1");

INSERT INTO keg_mainstream_user_access VALUES("47","12","1");

INSERT INTO keg_mainstream_user_access VALUES("47","13","1");

INSERT INTO keg_mainstream_user_access VALUES("47","14","0");

INSERT INTO keg_mainstream_user_access VALUES("47","15","0");

INSERT INTO keg_mainstream_user_access VALUES("47","16","0");

INSERT INTO keg_mainstream_user_access VALUES("47","17","1");

INSERT INTO keg_mainstream_user_access VALUES("47","18","0");

INSERT INTO keg_mainstream_user_access VALUES("47","19","0");

INSERT INTO keg_mainstream_user_access VALUES("47","20","0");

INSERT INTO keg_mainstream_user_access VALUES("47","21","1");

INSERT INTO keg_mainstream_user_access VALUES("48","1","1");

INSERT INTO keg_mainstream_user_access VALUES("48","2","1");

INSERT INTO keg_mainstream_user_access VALUES("48","3","0");

INSERT INTO keg_mainstream_user_access VALUES("48","4","1");

INSERT INTO keg_mainstream_user_access VALUES("48","5","1");

INSERT INTO keg_mainstream_user_access VALUES("48","6","0");

INSERT INTO keg_mainstream_user_access VALUES("48","8","0");

INSERT INTO keg_mainstream_user_access VALUES("48","9","1");

INSERT INTO keg_mainstream_user_access VALUES("48","10","1");

INSERT INTO keg_mainstream_user_access VALUES("48","12","1");

INSERT INTO keg_mainstream_user_access VALUES("48","13","1");

INSERT INTO keg_mainstream_user_access VALUES("48","14","0");

INSERT INTO keg_mainstream_user_access VALUES("48","15","0");

INSERT INTO keg_mainstream_user_access VALUES("48","16","0");

INSERT INTO keg_mainstream_user_access VALUES("48","17","1");

INSERT INTO keg_mainstream_user_access VALUES("48","18","0");

INSERT INTO keg_mainstream_user_access VALUES("48","19","0");

INSERT INTO keg_mainstream_user_access VALUES("48","20","1");

INSERT INTO keg_mainstream_user_access VALUES("48","21","1");




DROP TABLE keg_mainstream_user_role_permissions;

CREATE TABLE `keg_mainstream_user_role_permissions` (
  `role_permission_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  KEY `role_permission_id` (`role_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_user_role_permissions VALUES("20","3","1");

INSERT INTO keg_mainstream_user_role_permissions VALUES("21","3","2");

INSERT INTO keg_mainstream_user_role_permissions VALUES("38","3","3");

INSERT INTO keg_mainstream_user_role_permissions VALUES("22","3","4");

INSERT INTO keg_mainstream_user_role_permissions VALUES("23","3","5");

INSERT INTO keg_mainstream_user_role_permissions VALUES("24","3","6");

INSERT INTO keg_mainstream_user_role_permissions VALUES("25","3","8");

INSERT INTO keg_mainstream_user_role_permissions VALUES("19","3","12");

INSERT INTO keg_mainstream_user_role_permissions VALUES("39","3","13");

INSERT INTO keg_mainstream_user_role_permissions VALUES("48","3","10");

INSERT INTO keg_mainstream_user_role_permissions VALUES("49","3","9");

INSERT INTO keg_mainstream_user_role_permissions VALUES("50","3","15");

INSERT INTO keg_mainstream_user_role_permissions VALUES("52","3","15");

INSERT INTO keg_mainstream_user_role_permissions VALUES("54","3","15");

INSERT INTO keg_mainstream_user_role_permissions VALUES("56","3","16");

INSERT INTO keg_mainstream_user_role_permissions VALUES("83","3","17");

INSERT INTO keg_mainstream_user_role_permissions VALUES("87","3","19");

INSERT INTO keg_mainstream_user_role_permissions VALUES("90","3","20");

INSERT INTO keg_mainstream_user_role_permissions VALUES("91","3","21");

INSERT INTO keg_mainstream_user_role_permissions VALUES("40","4","1");

INSERT INTO keg_mainstream_user_role_permissions VALUES("41","4","2");

INSERT INTO keg_mainstream_user_role_permissions VALUES("42","4","3");

INSERT INTO keg_mainstream_user_role_permissions VALUES("43","4","4");

INSERT INTO keg_mainstream_user_role_permissions VALUES("44","4","5");

INSERT INTO keg_mainstream_user_role_permissions VALUES("45","4","8");

INSERT INTO keg_mainstream_user_role_permissions VALUES("46","4","12");

INSERT INTO keg_mainstream_user_role_permissions VALUES("47","4","13");

INSERT INTO keg_mainstream_user_role_permissions VALUES("53","4","9");

INSERT INTO keg_mainstream_user_role_permissions VALUES("55","4","15");

INSERT INTO keg_mainstream_user_role_permissions VALUES("57","4","16");

INSERT INTO keg_mainstream_user_role_permissions VALUES("84","4","17");

INSERT INTO keg_mainstream_user_role_permissions VALUES("92","2","1");




DROP TABLE keg_mainstream_user_roles;

CREATE TABLE `keg_mainstream_user_roles` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `role_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `role_display_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `role_desc` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `employee` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_mainstream_user_roles VALUES("3","company_administrator","Company Administrator","","1","1","");

INSERT INTO keg_mainstream_user_roles VALUES("4","employee","Employee","","1","1","1");

INSERT INTO keg_mainstream_user_roles VALUES("2","sales_rep","Sales Rep","","","1","");




DROP TABLE keg_mainstream_container_types;

CREATE TABLE `keg_mainstream_container_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_type` varbinary(100) DEFAULT NULL,
  `container_capacity` varbinary(100) DEFAULT NULL,
  `active` varbinary(2) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO keg_mainstream_container_types VALUES("1","Stainless 10L (DIN)","10L","");

INSERT INTO keg_mainstream_container_types VALUES("2","Stainless 30L (DIN)","30L","");

INSERT INTO keg_mainstream_container_types VALUES("3","PET 20L","20L","");

INSERT INTO keg_mainstream_container_types VALUES("4","Stainless 30L (EURO - SAB)","30L","");

INSERT INTO keg_mainstream_container_types VALUES("5","Blank","Blank","");




DROP TABLE keg_mainstream_user_role_access;

CREATE TABLE `keg_mainstream_user_role_access` (
  `role_access_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  KEY `role_access_id` (`role_access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

INSERT INTO keg_mainstream_user_role_access VALUES("24","3","1");

INSERT INTO keg_mainstream_user_role_access VALUES("25","3","2");

INSERT INTO keg_mainstream_user_role_access VALUES("23","3","3");

INSERT INTO keg_mainstream_user_role_access VALUES("27","3","4");

INSERT INTO keg_mainstream_user_role_access VALUES("28","3","5");

INSERT INTO keg_mainstream_user_role_access VALUES("29","3","6");

INSERT INTO keg_mainstream_user_role_access VALUES("32","3","8");

INSERT INTO keg_mainstream_user_role_access VALUES("41","4","1");

INSERT INTO keg_mainstream_user_role_access VALUES("42","4","2");

INSERT INTO keg_mainstream_user_role_access VALUES("43","4","3");

INSERT INTO keg_mainstream_user_role_access VALUES("44","4","4");

INSERT INTO keg_mainstream_user_role_access VALUES("45","4","5");

INSERT INTO keg_mainstream_user_role_access VALUES("46","4","8");

INSERT INTO keg_mainstream_user_role_access VALUES("47","2","1");

INSERT INTO keg_mainstream_user_role_access VALUES("48","2","2");

INSERT INTO keg_mainstream_user_role_access VALUES("49","2","3");

INSERT INTO keg_mainstream_user_role_access VALUES("50","2","4");

INSERT INTO keg_mainstream_user_role_access VALUES("51","2","5");




