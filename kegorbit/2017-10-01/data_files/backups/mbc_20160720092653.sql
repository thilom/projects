DROP TABLE keg_mbc_customers;

CREATE TABLE `keg_mbc_customers` (
  `customer_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer_contact` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_contact_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_contact_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO keg_mbc_customers VALUES("1","Palladian Bytes","Thilo Muller","0832761411","thilo@palladianbytes.co.za","");

INSERT INTO keg_mbc_customers VALUES("2","idhuidhuh","","","","");

INSERT INTO keg_mbc_customers VALUES("3","idhuidhuh","uihidhuidh","idhuidhui","dhiudhiudhiuhdu","");

INSERT INTO keg_mbc_customers VALUES("4","oeuioeui","","","","Y");

INSERT INTO keg_mbc_customers VALUES("5","oeuioeui","","","","Y");

INSERT INTO keg_mbc_customers VALUES("6",".fpyf.pf","","","","Y");

INSERT INTO keg_mbc_customers VALUES("7",".pyf.pf","","","","Y");

INSERT INTO keg_mbc_customers VALUES("8",".pyf.pf","","","","Y");

INSERT INTO keg_mbc_customers VALUES("9","udeudeud","","","","Y");

INSERT INTO keg_mbc_customers VALUES("10","yyfgc","","","","Y");

INSERT INTO keg_mbc_customers VALUES("11","ypgpg","","","","Y");

INSERT INTO keg_mbc_customers VALUES("12","fypyfgp","","","","Y");

INSERT INTO keg_mbc_customers VALUES("13","fypyfgp","","","","Y");

INSERT INTO keg_mbc_customers VALUES("14","euide","","","","Y");

INSERT INTO keg_mbc_customers VALUES("15","odeud","","","","Y");

INSERT INTO keg_mbc_customers VALUES("16","odeud","","","","");

INSERT INTO keg_mbc_customers VALUES("17","duhidh","","","","");

INSERT INTO keg_mbc_customers VALUES("18","euioei","","","","");

INSERT INTO keg_mbc_customers VALUES("19","euideud","","","","Y");

INSERT INTO keg_mbc_customers VALUES("20","euideud","","","","");

INSERT INTO keg_mbc_customers VALUES("21","uidhidhtidth","","","","");

INSERT INTO keg_mbc_customers VALUES("22","ueiduid","","","","");

INSERT INTO keg_mbc_customers VALUES("23","ueiduid","","","","");

INSERT INTO keg_mbc_customers VALUES("24","euieioeio","","","","");

INSERT INTO keg_mbc_customers VALUES("25","pyfg","","","","");

INSERT INTO keg_mbc_customers VALUES("26",".pf.pf.pf","","","","Y");

INSERT INTO keg_mbc_customers VALUES("27","euideuideuid","","","","");

INSERT INTO keg_mbc_customers VALUES("28","euideuideudeud","uideuide","euid","eud","");

INSERT INTO keg_mbc_customers VALUES("29","eeidudeuid","","","","");

INSERT INTO keg_mbc_customers VALUES("30","Palladian Bytes34","Thilo Muller","0832761411","thilo@palladianbytes.co.za","Y");




