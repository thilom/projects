DROP TABLE keg_palladianb_customers;

CREATE TABLE `keg_palladianb_customers` (
  `customer_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customer_contact` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_contact_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_contact_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




DROP TABLE keg_palladianb_container_history;

CREATE TABLE `keg_palladianb_container_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_serial_number` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `history_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `history_action` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO keg_palladianb_container_history VALUES("1","78978897897","22","2016-08-01 14:39:04","Created");




DROP TABLE keg_palladianb_container_notes;

CREATE TABLE `keg_palladianb_container_notes` (
  `container_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_serial_number` varchar(50) NOT NULL,
  `container_note` varchar(200) DEFAULT NULL,
  `container_note_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`container_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE keg_palladianb_containers;

CREATE TABLE `keg_palladianb_containers` (
  `container_serial_number` varchar(50) NOT NULL,
  `container_bar_code` varchar(50) NOT NULL,
  `container_type` varchar(50) NOT NULL COMMENT 'comes from the types table',
  `container_capacity` varchar(50) NOT NULL,
  `container_capacity_type` varchar(50) DEFAULT NULL,
  `container_stage` int(11) NOT NULL,
  `container_state` int(11) DEFAULT NULL,
  `container_date` datetime NOT NULL,
  `container_contents` int(11) NOT NULL,
  `container_client` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`container_serial_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO keg_palladianb_containers VALUES("78978897897","789789789789","STEEL","7","L","1","5","2016-08-01 00:00:00","9","");




DROP TABLE keg_palladianb_module_permissions;

CREATE TABLE `keg_palladianb_module_permissions` (
  `permission_id` int(11) unsigned NOT NULL DEFAULT '0',
  `module_id` int(11) unsigned NOT NULL,
  `permission_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permission_desc` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `dev_module` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '1=Yes, 0=No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_module_permissions VALUES("1","1","general_access","Allow access to the dashboard module","0");

INSERT INTO keg_palladianb_module_permissions VALUES("2","2","general_access","Allow access to the Keg Management module","0");

INSERT INTO keg_palladianb_module_permissions VALUES("3","3","general_access","Allow access to the Products Module","0");

INSERT INTO keg_palladianb_module_permissions VALUES("4","4","general_access","Allow access to the Customer Manager module","0");

INSERT INTO keg_palladianb_module_permissions VALUES("5","5","general_access","Allow this user to the Reports Module","0");

INSERT INTO keg_palladianb_module_permissions VALUES("6","6","general_access","Allow this user to the Account Management Module","0");

INSERT INTO keg_palladianb_module_permissions VALUES("7","7","general_access","Allow this user to the Company Management Module","0");

INSERT INTO keg_palladianb_module_permissions VALUES("8","8","general_access","Allow access to the Settings module","0");




DROP TABLE keg_palladianb_registered_devices;

CREATE TABLE `keg_palladianb_registered_devices` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE keg_palladianb_stages;

CREATE TABLE `keg_palladianb_stages` (
  `stage_id` int(11) NOT NULL DEFAULT '0',
  `stage_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8 DEFAULT 'Y',
  `stage_description` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `stage_order` int(1) DEFAULT NULL,
  `onselect` int(1) unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_stages VALUES("1","Empty","Y","","1","");

INSERT INTO keg_palladianb_stages VALUES("2","Washed","Y","","2","");

INSERT INTO keg_palladianb_stages VALUES("3","Filled","Y","","3","");

INSERT INTO keg_palladianb_stages VALUES("4","Delivered","Y","","4","");

INSERT INTO keg_palladianb_stages VALUES("5","Returned","Y","","5","");




DROP TABLE keg_palladianb_states;

CREATE TABLE `keg_palladianb_states` (
  `state_id` int(4) unsigned NOT NULL DEFAULT '0',
  `state_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `out_of_circulation` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_states VALUES("1","Stolen","1","");

INSERT INTO keg_palladianb_states VALUES("2","Damaged","1","");

INSERT INTO keg_palladianb_states VALUES("3","Broken","1","");

INSERT INTO keg_palladianb_states VALUES("4","Inactive","","");

INSERT INTO keg_palladianb_states VALUES("5","Active","","");

INSERT INTO keg_palladianb_states VALUES("6","yyyyyfh","","Y");




DROP TABLE keg_palladianb_terminology;

CREATE TABLE `keg_palladianb_terminology` (
  `term_id` int(11) NOT NULL DEFAULT '0',
  `container_singular` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `container_plural` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `stage_singular` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `stage_plural` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `state_singular` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `state_plural` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `client_singular` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `client_plural` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_terminology VALUES("1","Container","Containers","Stage","Stages","State","States","Client","Clients");




DROP TABLE keg_palladianb_types;

CREATE TABLE `keg_palladianb_types` (
  `type_id` int(11) NOT NULL DEFAULT '0',
  `type_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `type_description` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(2) CHARACTER SET utf8 DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_types VALUES("1","Aegir Project","Aegir Project","N");

INSERT INTO keg_palladianb_types VALUES("2","Apollo Craft Beer","Apollo Craft Beer","Y");

INSERT INTO keg_palladianb_types VALUES("3","Atlantic Storm Craft Beer","Atlantic Storm Craft Beer","Y");

INSERT INTO keg_palladianb_types VALUES("7","Weis Beer","eoudinthoed otu idteohdi tho","Y");

INSERT INTO keg_palladianb_types VALUES("8","Birkenhead Brewery","Birkenhead Brewery","Y");

INSERT INTO keg_palladianb_types VALUES("9","Boston Breweries Craft Beer","Boston Breweries Craft Beer","Y");

INSERT INTO keg_palladianb_types VALUES("10","Black LAbel","Black Label Champion Beer","N");

INSERT INTO keg_palladianb_types VALUES("11","Blue Label","Black Label new brand","Y");

INSERT INTO keg_palladianb_types VALUES("12","Heinekein","Beer","N");




DROP TABLE keg_palladianb_user_access;

CREATE TABLE `keg_palladianb_user_access` (
  `user_id` int(10) unsigned NOT NULL,
  `permission_id` int(11) unsigned DEFAULT NULL,
  `access` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '0=No, 1=Yes'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_user_access VALUES("22","1","1");

INSERT INTO keg_palladianb_user_access VALUES("22","2","1");

INSERT INTO keg_palladianb_user_access VALUES("22","4","1");

INSERT INTO keg_palladianb_user_access VALUES("22","5","1");

INSERT INTO keg_palladianb_user_access VALUES("22","6","1");

INSERT INTO keg_palladianb_user_access VALUES("22","7","1");

INSERT INTO keg_palladianb_user_access VALUES("22","8","1");

INSERT INTO keg_palladianb_user_access VALUES("23","1","1");

INSERT INTO keg_palladianb_user_access VALUES("23","2","1");

INSERT INTO keg_palladianb_user_access VALUES("23","3","1");

INSERT INTO keg_palladianb_user_access VALUES("23","4","1");

INSERT INTO keg_palladianb_user_access VALUES("23","5","1");

INSERT INTO keg_palladianb_user_access VALUES("23","6","1");

INSERT INTO keg_palladianb_user_access VALUES("23","7","0");

INSERT INTO keg_palladianb_user_access VALUES("23","8","1");

INSERT INTO keg_palladianb_user_access VALUES("24","1","1");

INSERT INTO keg_palladianb_user_access VALUES("24","2","0");

INSERT INTO keg_palladianb_user_access VALUES("24","4","0");

INSERT INTO keg_palladianb_user_access VALUES("24","5","0");

INSERT INTO keg_palladianb_user_access VALUES("24","6","0");

INSERT INTO keg_palladianb_user_access VALUES("24","7","0");

INSERT INTO keg_palladianb_user_access VALUES("24","8","0");

INSERT INTO keg_palladianb_user_access VALUES("22","3","1");

INSERT INTO keg_palladianb_user_access VALUES("25","1","1");

INSERT INTO keg_palladianb_user_access VALUES("25","2","1");

INSERT INTO keg_palladianb_user_access VALUES("25","3","0");

INSERT INTO keg_palladianb_user_access VALUES("25","4","1");

INSERT INTO keg_palladianb_user_access VALUES("25","5","1");

INSERT INTO keg_palladianb_user_access VALUES("25","6","1");

INSERT INTO keg_palladianb_user_access VALUES("25","7","0");

INSERT INTO keg_palladianb_user_access VALUES("25","8","1");

INSERT INTO keg_palladianb_user_access VALUES("26","1","1");

INSERT INTO keg_palladianb_user_access VALUES("26","2","1");

INSERT INTO keg_palladianb_user_access VALUES("26","3","0");

INSERT INTO keg_palladianb_user_access VALUES("26","4","1");

INSERT INTO keg_palladianb_user_access VALUES("26","5","1");

INSERT INTO keg_palladianb_user_access VALUES("26","6","1");

INSERT INTO keg_palladianb_user_access VALUES("26","7","0");

INSERT INTO keg_palladianb_user_access VALUES("26","8","1");

INSERT INTO keg_palladianb_user_access VALUES("27","1","1");

INSERT INTO keg_palladianb_user_access VALUES("27","2","1");

INSERT INTO keg_palladianb_user_access VALUES("27","3","0");

INSERT INTO keg_palladianb_user_access VALUES("27","4","1");

INSERT INTO keg_palladianb_user_access VALUES("27","5","1");

INSERT INTO keg_palladianb_user_access VALUES("27","6","1");

INSERT INTO keg_palladianb_user_access VALUES("27","7","0");

INSERT INTO keg_palladianb_user_access VALUES("27","8","1");

INSERT INTO keg_palladianb_user_access VALUES("28","1","1");

INSERT INTO keg_palladianb_user_access VALUES("28","2","1");

INSERT INTO keg_palladianb_user_access VALUES("28","3","0");

INSERT INTO keg_palladianb_user_access VALUES("28","4","1");

INSERT INTO keg_palladianb_user_access VALUES("28","5","1");

INSERT INTO keg_palladianb_user_access VALUES("28","6","1");

INSERT INTO keg_palladianb_user_access VALUES("28","7","0");

INSERT INTO keg_palladianb_user_access VALUES("28","8","1");

INSERT INTO keg_palladianb_user_access VALUES("29","1","1");

INSERT INTO keg_palladianb_user_access VALUES("29","2","1");

INSERT INTO keg_palladianb_user_access VALUES("29","3","0");

INSERT INTO keg_palladianb_user_access VALUES("29","4","1");

INSERT INTO keg_palladianb_user_access VALUES("29","5","1");

INSERT INTO keg_palladianb_user_access VALUES("29","6","1");

INSERT INTO keg_palladianb_user_access VALUES("29","7","0");

INSERT INTO keg_palladianb_user_access VALUES("29","8","1");

INSERT INTO keg_palladianb_user_access VALUES("30","1","1");

INSERT INTO keg_palladianb_user_access VALUES("30","2","1");

INSERT INTO keg_palladianb_user_access VALUES("30","3","0");

INSERT INTO keg_palladianb_user_access VALUES("30","4","1");

INSERT INTO keg_palladianb_user_access VALUES("30","5","1");

INSERT INTO keg_palladianb_user_access VALUES("30","6","1");

INSERT INTO keg_palladianb_user_access VALUES("30","7","0");

INSERT INTO keg_palladianb_user_access VALUES("30","8","1");

INSERT INTO keg_palladianb_user_access VALUES("31","1","1");

INSERT INTO keg_palladianb_user_access VALUES("31","2","1");

INSERT INTO keg_palladianb_user_access VALUES("31","3","0");

INSERT INTO keg_palladianb_user_access VALUES("31","4","1");

INSERT INTO keg_palladianb_user_access VALUES("31","5","1");

INSERT INTO keg_palladianb_user_access VALUES("31","6","1");

INSERT INTO keg_palladianb_user_access VALUES("31","7","0");

INSERT INTO keg_palladianb_user_access VALUES("31","8","1");

INSERT INTO keg_palladianb_user_access VALUES("32","1","1");

INSERT INTO keg_palladianb_user_access VALUES("32","2","1");

INSERT INTO keg_palladianb_user_access VALUES("32","3","0");

INSERT INTO keg_palladianb_user_access VALUES("32","4","1");

INSERT INTO keg_palladianb_user_access VALUES("32","5","1");

INSERT INTO keg_palladianb_user_access VALUES("32","6","1");

INSERT INTO keg_palladianb_user_access VALUES("32","7","0");

INSERT INTO keg_palladianb_user_access VALUES("32","8","1");

INSERT INTO keg_palladianb_user_access VALUES("33","1","1");

INSERT INTO keg_palladianb_user_access VALUES("33","2","1");

INSERT INTO keg_palladianb_user_access VALUES("33","3","0");

INSERT INTO keg_palladianb_user_access VALUES("33","4","1");

INSERT INTO keg_palladianb_user_access VALUES("33","5","1");

INSERT INTO keg_palladianb_user_access VALUES("33","6","1");

INSERT INTO keg_palladianb_user_access VALUES("33","7","0");

INSERT INTO keg_palladianb_user_access VALUES("33","8","1");

INSERT INTO keg_palladianb_user_access VALUES("34","1","1");

INSERT INTO keg_palladianb_user_access VALUES("34","2","1");

INSERT INTO keg_palladianb_user_access VALUES("34","3","0");

INSERT INTO keg_palladianb_user_access VALUES("34","4","1");

INSERT INTO keg_palladianb_user_access VALUES("34","5","1");

INSERT INTO keg_palladianb_user_access VALUES("34","6","1");

INSERT INTO keg_palladianb_user_access VALUES("34","7","0");

INSERT INTO keg_palladianb_user_access VALUES("34","8","1");




DROP TABLE keg_palladianb_user_role_permissions;

CREATE TABLE `keg_palladianb_user_role_permissions` (
  `role_permission_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_user_role_permissions VALUES("1","1","1");

INSERT INTO keg_palladianb_user_role_permissions VALUES("2","1","2");

INSERT INTO keg_palladianb_user_role_permissions VALUES("4","1","4");

INSERT INTO keg_palladianb_user_role_permissions VALUES("5","1","5");

INSERT INTO keg_palladianb_user_role_permissions VALUES("6","1","6");

INSERT INTO keg_palladianb_user_role_permissions VALUES("7","1","7");

INSERT INTO keg_palladianb_user_role_permissions VALUES("18","1","8");

INSERT INTO keg_palladianb_user_role_permissions VALUES("20","3","1");

INSERT INTO keg_palladianb_user_role_permissions VALUES("21","3","2");

INSERT INTO keg_palladianb_user_role_permissions VALUES("22","3","4");

INSERT INTO keg_palladianb_user_role_permissions VALUES("23","3","5");

INSERT INTO keg_palladianb_user_role_permissions VALUES("24","3","6");

INSERT INTO keg_palladianb_user_role_permissions VALUES("25","3","8");

INSERT INTO keg_palladianb_user_role_permissions VALUES("19","7","1");




DROP TABLE keg_palladianb_user_roles;

CREATE TABLE `keg_palladianb_user_roles` (
  `role_id` int(11) NOT NULL DEFAULT '0',
  `role_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `role_display_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `role_desc` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO keg_palladianb_user_roles VALUES("1","application_administrator","Application Administrator","");

INSERT INTO keg_palladianb_user_roles VALUES("2","developer","Developer","");

INSERT INTO keg_palladianb_user_roles VALUES("3","company_administrator","Company Administrator","");

INSERT INTO keg_palladianb_user_roles VALUES("4","employee","Employee","");




