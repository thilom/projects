<?php

/**
 * Set of functions used throughout the system
 *
 * @author thilo
 */

class General {
	function __construct() {

	}

	function getScrumSettings() {
		$settings = array();

		$statement = "SELECT tag, value
						FROM {$GLOBALS['db_prefix']}_settings ";
		$sql_settings = $GLOBALS['dbCon']->prepare($statement);
		$sql_settings->execute();
		$sql_settings_data = $sql_settings->fetchAll();
		$sql_settings->closeCursor();

		foreach ($sql_settings_data as $settings_data) {
			$settings[$settings_data['tag']] = $settings_data['value'];
		}

		return $settings;
	}
        
}