<?php
/**
 * A general class for use across the site
 * @author Sigal
 * @Date 03/10/2013
 */

class System {
    /**
     * Merge page data into the template.
     *
     * @param string $template The template to merge data into
     * @param mixed[] $merge_data An array of values to merge into the template
     * @param string $placeholder_start The start of the placeholder
     * @param string $placeholder_end The end of the placeholder
     * @return string The merged template
     */
    function merge_data($template, $merge_data, $placeholder_start = '<!-- ', $placeholder_end = ' -->') {

        foreach ($merge_data as $merge_key=>$merge_value) {
            $template = str_replace("{$placeholder_start}$merge_key{$placeholder_end}", $merge_value, $template);
        }

        return $template;
    }

    function get_scrum_settings() {
        $settings = array();

        $statement = "SELECT setting_tag, setting_value
						FROM {$GLOBALS['db_prefix']}_settings ";
        $sql_settings = $GLOBALS['dbCon']->prepare($statement);
        $sql_settings->execute();
        $sql_settings_data = $sql_settings->fetchAll();
        $sql_settings->closeCursor();

        foreach ($sql_settings_data as $settings_data) {
            $settings[$settings_data['setting_tag']] = $settings_data['setting_value'];
        }

        return $settings;
    }

    /**
     * Get the system constants
     * @return array constants
     */
    function get_constants()
    {
        $statement = "SELECT constant_title, constant_name, constant_value, do_mapping
                        FROM {$GLOBALS['db_prefix']}_constants";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $constants = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();

        return $constants;
    }

    function file_upload_max_size() {
        static $max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $max_size = $this->parse_size(ini_get('post_max_size'));

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = $this->parse_size(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    private function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }

    /**
     * Converts a sql date format to jquery datepicker format
     *
     * @param $sql_format
     * @return mixed
     */
    function convert_date_format($sql_format) {

        $search = array('%e','%d','%j','%a','%W','%c','%m','%b','%M','%y','%Y');
        $replace = array('d','dd','oo','D','DD','m','mm','M','MM','y','yy');
        $jquery_date = str_replace($search, $replace, $sql_format);

        return $jquery_date;
    }
}
/*
 * End of file system.class.php
 */