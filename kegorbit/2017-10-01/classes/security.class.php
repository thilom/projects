<?php

/**
 * Handles all security aspects for Scrum 2.0
 *
 * 2013-10-08: Thilo Muller 	- Added hash function (hash())
 * 								- Added API login function (login_api())
 *
 * @author thilo Muller
 */
class Security {

	/**
	 * @var string THe salt for password hashing
	 */
	private $salt = 'NTH5DXTMH(&IB';

	/**
	 * Company code
	 *
	 * @var string
	 */
	private $company_code = 'default';


	function is_logged_in() {
		$status = false;

		if (isset($_SESSION['user']['id'])) $status = true;

		return $status;
	}


	/**
	 * State constructor.
	 */
	function __construct() {
        if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
            $this->company_code = $_SESSION['user']['run_as'];
        } else {
            if(isset($_SESSION['user']['prefix'])) {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }
	}

	function login($username, $password) {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/password_hash_helper.php';
		//Vars
		$result = array('status'=>false, 'message'=>'Login Failed: Username and password required');

		if($username != '' && $password != '') {
			$statement = "SELECT np, inactive, user_type, user_id
						FROM {$GLOBALS['db_prefix']}_users
						WHERE username = :username
						LIMIT 1";
			$sql_user = $GLOBALS['dbCon']->prepare($statement);
			$sql_user->bindParam(':username', $username);
			$sql_user->execute();
			$sql_user_data = $sql_user->fetch();
			$sql_user->closeCursor();

			$valid_password = validate_password($password, $sql_user_data['np']);

			if($valid_password) {
				if ($sql_user_data['inactive'] == 'Y') {
					$result['status'] = false;
					$result['message'] = 'Login Failed: Account deactivated';
				} else {
					//Set session
					$result['status'] = true;
					$result['message'] = '';
					$_SESSION['user']['id'] = $sql_user_data['user_id'];
					$_SESSION['login_time'] = time();
					require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/user_management/classes/user.class.php';
					$userClass = new User();
					$userClass->update_last_login($sql_user_data['user_id']);
				}
			} else {
				$result['status'] = false;
				$result['message'] = 'Login Failed: Username or password not recognized';
			}
		}

		//Return result
		return $result;
	}

	function login_new_user($user_id, $code,$username, $password) {
		require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/password_hash_helper.php';
		$salt = create_salt($password);
		$password = create_hash($password, $salt);
		//Vars
		$result = array('status'=>false, 'message'=>'Login Failed: Username and password required');

		if($username != '' && $password != '') {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_users
                        SET np = :password
                        WHERE user_id = :user_id
                        AND activation_code = :activation_code";
			$sql = $GLOBALS['dbCon']->prepare($statement);
			$sql->bindParam(':user_id', $user_id);
			$sql->bindParam(':activation_code', $code);
			$sql->bindParam(':password', $password);
			$sql->execute();
			$sql->closeCursor();


			$statement = "SELECT  inactive, user_type, user_id
						FROM {$GLOBALS['db_prefix']}_users
						WHERE username = :username
						LIMIT 1";
			$sql_user = $GLOBALS['dbCon']->prepare($statement);
			$sql_user->bindParam(':username', $username);
			$sql_user->execute();
			$sql_user_data = $sql_user->fetch();
			$sql_user->closeCursor();



				if ($sql_user_data['inactive'] == 'Y') {
					$result['status'] = false;
					$result['message'] = 'Login Failed: Account deactivated';
				} else {
					//Set session
					$result['status'] = true;
					$result['message'] = '';
					$_SESSION['user']['id'] = $sql_user_data['user_id'];
					$_SESSION['login_time'] = time();
					require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/user_management/classes/user.class.php';
					$userClass = new User();
					$userClass->update_last_login($sql_user_data['user_id']);
				}
		}

		//Return result
		return $result;
	}

	function user_access($module_id, $function_id, $user_id) {
		$access_allowed = false;

		//Get access from DB
		$statement = "SELECT access
						FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_access
						WHERE user_id=:user_id
							AND module_id=:module_id
							AND function_id=:function_id
						LIMIT 1";
		$sql_access = $GLOBALS['dbCon']->prepare($statement);
		$sql_access->bindParam(':user_id', $user_id);
		$sql_access->bindParam(':module_id', $module_id);
		$sql_access->bindParam(':function_id', $function_id);
		$sql_access->execute();
		$sql_access_data = $sql_access->fetch();
		$sql_access->closeCursor();

		//Check Access
		if (isset($sql_access_data['access']) && $sql_access_data['access'] == 'Y') {
			$access_allowed = true;
		}

		return $access_allowed;
	}

	function get_logs($criteria) {

		//Assemble statement
		$statement = "SELECT DATE_FORMAT(a.log_timestamp, '{$GLOBALS['scrum_settings']['date_format']} (%H:%i:%s)') AS log_timestamp,
							a.user_id, a.log_message, a.module_id, a.target, b.firstname, b.lastname
						FROM {$GLOBALS['db_prefix']}_logs AS a
						LEFT JOIN {$GLOBALS['db_prefix']}_users AS b ON a.user_id=b.user_id ";
		if (isset($criteria['module_id'])) {
			$statement .= strpos($statement, 'WHERE')>0?' AND ':' WHERE ';
			$statement .= "a.module_id = :module_id";
		}
		if (isset($criteria['sub_module_id'])) {
			$statement .= strpos($statement, 'WHERE')>0?' AND ':' WHERE ';
			$statement .= "a.sub_module_id = :sub_module_id";
		}
		if (isset($criteria['target'])) {
			$statement .= strpos($statement, 'WHERE')>0?' AND ':' WHERE ';
			$statement .= "a.target = :target";
		}


		$sql_logs = $GLOBALS['dbCon']->prepare($statement);
		if (isset($criteria['sub_module_id']))  $sql_logs->bindParam(':sub_module_id', $criteria['sub_module_id']);
		if (isset($criteria['module_id']))  $sql_logs->bindParam(':module_id', $criteria['module_id']);
		if (isset($criteria['target']))  $sql_logs->bindParam(':target', $criteria['target']);

		$sql_logs->execute();
		$sql_logs_data = $sql_logs->fetchAll();
		$sql_logs->closeCursor();

		return $sql_logs_data;
	}


	/**
	 * Login for API calls.
	 *
	 * Adds security to API calls by checking if the correct credentials are sent with the API call.
	 *
	 * @param string $api_name 	The name of the API referrer
	 * @param string $api_password	The API passcode
	 *
	 * @return boolean
	 */
	public function login_api($api_name, $api_password) {
		$exchange_password = $this->hash($api_password, false);

		$statement = "SELECT exchange_password
						FROM {$GLOBALS['db_prefix']}_exchanges
						WHERE exchange_name = :exchange_name";
		$sql_api = $GLOBALS['dbCon']->prepare($statement);
		$sql_api->bindParam(':exchange_name', $api_name);
		$sql_api->execute();
		$sql_api_data = $sql_api->fetch();
		$sql_api->closeCursor();

		if (isset($sql_api_data['exchange_password']) && base64_decode($sql_api_data['exchange_password']) == $exchange_password) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Hash a password
	 *
	 * Uses base64 encoding for easier DB storage.
	 *
	 * @param string  	$password The string to hash
	 * @param bool 		$base64 disable base64 encoding
	 *
	 * @return string
	 */
	private function hash($password, $base64 = true) {
		$encrypted_password = crypt($password, "$2a$07$".$this->salt."$");
		if ($base64 === true) $password = base64_encode($password);
		return $password;
	}

	/**
	 * Check if a user has access to a given module permission
	 * @param int $user_id
	 * @param string $module_folder
	 * @param string $permission_name
	 * @return char access
	 */
	function check_user_permission($user_id, $module_folder, $permission_name)
	{
        if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
            $this->company_code = $_SESSION['user']['run_as'];
        } else {
            if(isset($_SESSION['user']['prefix'])) {
                $this->company_code = $_SESSION['user']['prefix'];
                require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
                $accountClass = new Accounts();
                $account_data = $accountClass->get_account($user_id);
                if(empty($account_data['company_db_prefix'])){
                    $this->company_code = 'default';
                }
                else{
                    $this->company_code = $account_data['company_db_prefix'];
                }
            }
        }

		$access = 0;
		$statement = "SELECT access
                        FROM {$GLOBALS['db_prefix']}_modules m
                        JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions mp
                        ON m.module_id = mp.module_id AND permission_name = :permission_name
                        JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_user_access ua
                        ON ua.permission_id = mp.permission_id AND user_id = :user_id
                        WHERE module_folder = :module_folder
                        LIMIT 1";
		$sql = $GLOBALS['dbCon']->prepare($statement);
		$sql->bindParam(':permission_name', $permission_name);
		$sql->bindParam(':user_id', $user_id);
		$sql->bindParam(':module_folder', $module_folder);
		$sql->execute();
		$access = $sql->fetchColumn();
		$sql->closeCursor();

		if($access == 0)
			return false;
		else
			return true;
	}

	/**
	 * Checks if a user id and activation code exist
	 * @param int $user_id
	 * @param string $activation_code
	 * @return int result
	 */
	function check_user_activation_code($user_id, $activation_code)
	{
		$statement = "SELECT count(user_id)
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE user_id = :user_id
                        AND activation_code = :code
                        LIMIT 1";
		$sql = $GLOBALS['dbCon']->prepare($statement);
		$sql->bindParam(':user_id', $user_id);
		$sql->bindParam(':code', $activation_code);
		$sql->execute();
		$result = $sql->fetchColumn();
		$sql->closeCursor();
		return $result == 1;
	}

	/**
	 * Updates user password
	 * @param int $user_id
	 * @param string $password
	 */
	function reset_password($user_id, $password)
	{
		require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/password_hash_helper.php';
		$salt = create_salt($password);
		$password = create_hash($password, $salt);

		$statement = "UPDATE {$GLOBALS['db_prefix']}_users
                        SET np = :password, activation_code = NULL
                        WHERE user_id = :user_id";
		$sql = $GLOBALS['dbCon']->prepare($statement);
		$sql->bindParam(':user_id', $user_id);
		$sql->bindParam(':password', $password);
		$sql->execute();
		$sql->closeCursor();
	}

	/**
	 * Gets a user email or username and checks if it exists
	 * @param string $email $username
	 * @return array user
	 */
	function check_user_email($email,$username)
	{
		if(empty($email)){
			$statement = "SELECT user_id, firstname, lastname, email, username
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE username = :username
                        LIMIT 1";
			$sql = $GLOBALS['dbCon']->prepare($statement);
			$sql->bindParam(':username', $username);
			$sql->execute();
			$user = $sql->fetch(PDO::FETCH_ASSOC);
			$sql->closeCursor();

			return $user;
		}
		else{
			$statement = "SELECT user_id, firstname, lastname, email, username
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE email = :email
                        LIMIT 1";
			$sql = $GLOBALS['dbCon']->prepare($statement);
			$sql->bindParam(':email', $email);
			$sql->execute();
			$user = $sql->fetch(PDO::FETCH_ASSOC);
			$sql->closeCursor();

			return $user;
		}

	}



	/**
	 * Save forgot password activation code
	 * @param int $user_id
	 * @param string $code
	 */
//	function save_forgot_password_code($user_id, $code)
//	{
//		$statement = "UPDATE {$GLOBALS['db_prefix']}_users
//                        SET activation_code = :code
//                        WHERE user_id = :user_id";
//		$sql = $GLOBALS['dbCon']->prepare($statement);
//		$sql->bindParam(':code', $code);
//		$sql->bindParam(':user_id', $user_id);
//		$sql->execute();
//		$sql->closeCursor();
//	}

	/**
	 * Get the modules the user has permissions to (that will include modules with no permissions as well)
	 * @param int $user_id
	 * @return array modules
	 */
	function get_user_modules($user_id) {
        if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
            $this->company_code = $_SESSION['user']['run_as'];
        } else {
            if(isset($_SESSION['user']['prefix'])) {
                $this->company_code = $_SESSION['user']['prefix'];
                require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
                $accountClass = new Accounts();
                $account_data = $accountClass->get_account($user_id);
                if(empty($account_data['company_db_prefix'])){
                    $this->company_code = 'default';
                }
                else{
                    $this->company_code = $account_data['company_db_prefix'];
                }
            }
        }
		$access = 1;
		$statement = "SELECT DISTINCT um.module_id, module_folder, module_name, module_font_awesome, module_version, module_page_name, ua.user_id
                        FROM {$GLOBALS['db_prefix']}_user_modules um
                        LEFT JOIN {$GLOBALS['db_prefix']}_modules m ON um.module_id = m.module_id
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions mp ON mp.module_id = m.module_id
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_user_access ua ON ua.permission_id = mp.permission_id
                        WHERE ua.user_id = :user_id
                        AND ua.access = :access
                        ORDER BY m.module_id";
		$sql = $GLOBALS['dbCon']->prepare($statement);
		$sql->bindParam(':user_id', $user_id);
		$sql->bindParam(':access', $access);
		$sql->execute();
		$modules = $sql->fetchAll(PDO::FETCH_ASSOC);
		$sql->closeCursor();
		return $modules;
	}

	/*
     * Get installed modules
     * @param bool @active
     * @return array $modules
     */
	public function get_installed_modules($active = FALSE, $role_id)
	{
		$statement = "SELECT modules.module_id, module_folder, module_name, module_version, has_permissions, active
                        FROM {$GLOBALS['db_prefix']}_modules AS modules
                       JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_access AS roles  ON modules.module_id = roles.module_id
                        WHERE module_folder <> 'about'
                        AND role_id =:role_id";
		if($active)
			$statement .= " WHERE active = '1'";

		$statement .= " ORDER BY module_name ASC";
		$sql = $GLOBALS['dbCon']->prepare($statement);
		$sql->bindParam(':role_id', $role_id);
		$sql->execute();

		$modules = $sql->fetchAll();
		return $modules;
	}
}
