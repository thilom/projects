<?php
/**
 * Users class
 *
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";

class User extends System
{
    /**
     * Company code
     *
     * @var string
     */
    private $company_code = 'default';

    /**
     * State constructor.
     */
    function __construct() {
        if(!empty($_SESSION['user']['prefix'])) {
            $this->company_code = $_SESSION['user']['prefix'];
        }
    }

    private $log = '';

    /*
     * Save a user
     * @param user_id (int)
     * @param user_data (array)
     *
     * @return int
     */
    function create_user($user_data){

        require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/password_hash_helper.php';
        $salt = create_salt($user_data['password']);
        $password = create_hash($user_data['password'], $salt);


        $passPhrase = new Passphrase();
        $phrase = $passPhrase->passphrase();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_users
                      (user_email_address, user_full_name, np)
					  VALUES
					  (AES_ENCRYPT(:user_email_address, :phrase),AES_ENCRYPT(:user_full_name, :phrase), :np)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_email_address', $user_data['email']);
        $sql->bindParam(':user_full_name', $user_data['name']);
        $sql->bindParam(':np', $password);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $user_id = $GLOBALS['dbCon']->LastInsertId();
        //create session
        $_SESSION['user']['id'] = $user_id;
        $_SESSION['user']['login_time'] = time();
        return $user_id;
    }

    /*
    * login in user
    * @param $email, $password
    * @return
    */
    function login($email, $password){
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();
        require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/password_hash_helper.php';
        //Vars
        if ($password == '') {
            $result['status'] = false;
            $result['message'] = ' The Password or Username that you have entered is incorrect';
        }

        if ($email != '' && $password != '') {

            $statement = "SELECT user_id, AES_DECRYPT(user_email_address, :phrase) AS user_email_address, np,  AES_DECRYPT(user_full_name, :phrase) AS user_full_name, user_type, users.active, users.company_id, AES_DECRYPT(:user_username, :phrase) AS user_username, company_db_prefix
						FROM {$GLOBALS['db_prefix']}_users AS users
						LEFT JOIN {$GLOBALS['db_prefix']}_companies AS company ON company.company_id = users.company_id
						WHERE user_email_address = AES_ENCRYPT(:user_email_address, :phrase)
						OR user_username = AES_ENCRYPT(:user_username, :phrase) 
						LIMIT 1";
            $sql_user = $GLOBALS['dbCon']->prepare($statement);
            $sql_user->bindParam(':user_email_address', $email);
            $sql_user->bindParam(':user_username', $email);
            $sql_user->bindParam(':phrase', $phrase);
            $sql_user->execute();
            $sql_user_data = $sql_user->fetch();
            $sql_user->closeCursor();

            $valid_password = validate_password($password, $sql_user_data['np']);

            require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
            $appClass = new Application();
            $settingsClass = new Settings();
            //Get current settings
            $settings = $appClass->get_settings();
            $maintenance_data = $settingsClass->get_maintenance();
            $now = date('Y-m-d h:i:s');
            //activate maintenance
            if (isset($settings['activate_maintenance']) && $settings['activate_maintenance'] == '1' && $sql_user_data['user_type'] != '1' && $maintenance_data['maintenance_commence'] <= $now) {
                $result['status'] = false;
                $result['message'] = 'Login Failed: Maintenance in progress!!!';
            } else {
                if ($valid_password) {
                    if ($sql_user_data['active'] == 'N') {
                        $result['status'] = false;
                        $result['message'] = 'Login Failed: Account deactivated Please Contact Your system Administrator';
                    } else {
                        //Set session
                        $result['status'] = true;
                        $result['message'] = 'login';
                        $_SESSION['user']['id'] = $sql_user_data['user_id'];
                        $_SESSION['user']['user_type'] = $sql_user_data['user_type'];
                        $_SESSION['user']['company_id'] = $sql_user_data['company_id'];

                        if($sql_user_data['company_db_prefix'] == NULL){
                            $_SESSION['user']['prefix'] = 'default';
                        }
                        else{
                            $_SESSION['user']['prefix'] = $sql_user_data['company_db_prefix'];
                        }
                        $_SESSION['user']['login_time'] = time();
                        setcookie("loggedin", "true", time() + 30);
                        require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
                        $accountClass = new Accounts();
                        $accountClass->update_last_login($sql_user_data['user_id']);
                    }
                } else {
                    $result['status'] = false;
                    $result['message'] = 'The Password or Username that you have entered is incorrect!';

                }
            }

            //Return result
            return $result;
        }
    }


    /*
    * Get user
    * @param user_id (int)
    */
    function get_user($user_id) {

        $passPhrase = new Passphrase();
        $phrase = $passPhrase->passphrase();

        $statement = "SELECT AES_DECRYPT(user_email_address, :phrase) AS email,AES_DECRYPT(user_full_name, :phrase) AS firstname,  user_type, users.company_id, company_db_prefix
                        FROM {$GLOBALS['db_prefix']}_users AS users
                        LEFT JOIN {$GLOBALS['db_prefix']}_companies AS company ON company.company_id = users.company_id
                        WHERE user_id = :user_id";
        $sql_user = $GLOBALS['dbCon']->prepare($statement);
        $sql_user->bindParam(':user_id', $user_id);
        $sql_user->bindParam(':phrase', $phrase);
        $sql_user->execute();
        $sql_user_data = $sql_user->fetch();
        $sql_user->closeCursor();
        return $sql_user_data;
    }

    /*
   * Get company logo
   * @param user_id (int)
   */
    function get_company_logo($company_id) {

        $statement = "SELECT company_image
                        FROM {$GLOBALS['db_prefix']}_companies
                        WHERE company_id = :company_id";
        $sql_user = $GLOBALS['dbCon']->prepare($statement);
        $sql_user->bindParam(':company_id', $company_id);
        $sql_user->execute();
        $sql_company_data = $sql_user->fetch();
        $sql_user->closeCursor();
        return $sql_company_data['company_image'];
    }





    /**
     * Get a users emal and check if it already exists in the users table
     * @param string $email
     * @return int
     */
    function check_duplicate_username($email){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT count(user_email_address)
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE user_email_address = AES_ENCRYPT(:user_email_address, :phrase)
                        LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_email_address', $email);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();
        $email = $sql->fetchColumn();
        $sql->closeCursor();

        return $email;
    }

    /**
     * Gets a users email and checks if it exists
     * @param string $email
     * @return array
     */
    function check_user_email($email){
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT user_id, AES_DECRYPT(user_email_address, :phrase) AS email,AES_DECRYPT(user_full_name, :phrase) AS firstname
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE user_email_address = AES_ENCRYPT(:email, :phrase)
                        LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':email', $email);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();
        $user = $sql->fetch(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $user;
    }
    
    /*
    * Updating the activation code to be used to activate new users/ change forgot password
    * @param int $user_id,$code
    */
    function update_user_activation_code($code, $user_id){

            $statement = "UPDATE {$GLOBALS['db_prefix']}_users
                            SET activation_code = :activation_code
                            WHERE user_id = :user_id";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':user_id', $user_id);
            $sql->bindParam(':activation_code', $code);
            $sql->execute();
    }

    /*
    * update user password & log new user in
    * @param $password
    * @return int
    */
    function save_new_password($user_data){


        require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/password_hash_helper.php';
        $salt = create_salt($user_data['password']);
        $password = create_hash($user_data['password'], $salt);

        $statement = "UPDATE  {$GLOBALS['db_prefix']}_users
                        SET np = :np,
                            activation_code = ''
                        WHERE user_id = :user_id
                        AND activation_code = :activation_code";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':np', $password);
        $sql->bindParam(':user_id', $user_data['user_id']);
        $sql->bindParam(':activation_code', $user_data['code']);
        $sql->execute();
        $sql->closeCursor();
        $sql_user_data = $this->get_user($user_data['user_id']);
        //Set session
        $result['status'] = true;
        $result['message'] = 'login';
        $result['username'] =  $sql_user_data['firstname'];
        $_SESSION['user']['id'] = $user_data['user_id'];
        $_SESSION['user']['user_type'] = $sql_user_data['user_type'];
        $_SESSION['user']['company_id'] = $sql_user_data['company_id'];
        if($sql_user_data['company_db_prefix'] == NULL){
            $_SESSION['user']['prefix'] = 'default';
        }
        else{
            $_SESSION['user']['prefix'] = $sql_user_data['company_db_prefix'];
        }
        $_SESSION['user']['login_time'] = time();
        setcookie("loggedin", "true", time() + 30);
        require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
        $accountClass = new Accounts();
        $accountClass->update_last_login($user_data['user_id']);

        return $result;
    }

    /**
     * check if hash is older than 60 minutes
     * @param $hash , $account
     * @return array
     */
    function check_password_request_time($hash, $account)
    {
        $current_date = new DateTime(date('Y-m-d H:i:s'));
        $statement = "SELECT user_id, lost_password_code, password_request_time
                      FROM  {$GLOBALS['db_prefix']}_users
                      WHERE Account = :Account
                      AND lost_password_code = :lost_password_code";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':Account', $account);
        $sql_select->bindParam(':lost_password_code', $hash);
        $sql_select->execute();
        $sql_select_data = $sql_select->fetchAll();
        $sql_select->closeCursor();
        $db_date = new DateTime(date($sql_select_data[0]['password_request_time']));
        $dteDiff = $current_date->diff($db_date);
        return $dteDiff->format("%H:%I:%S");
    }

    /**
     * get name of user type
     * @param $user_type
     * @return array
     */
    function get_usertype_name($user_type){
        
        $statement = "SELECT role_display_name
                      FROM  {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                      WHERE role_id = :role_id";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':role_id', $user_type);
        $sql_select->execute();
        $sql_select_data = $sql_select->fetch();
        $sql_select->closeCursor();
        return $sql_select_data['role_display_name'];
    }

    /*
    * Get user
    * @param $email (string)
    */
    function get_user_by_email($email) {

        $passPhrase = new Passphrase();
        $phrase = $passPhrase->passphrase();

        $statement = "SELECT user_id,  AES_DECRYPT(user_email_address, :phrase) AS email,AES_DECRYPT(user_full_name, :phrase) AS firstname,  user_type, users.company_id, company_db_prefix
                        FROM {$GLOBALS['db_prefix']}_users AS users
                        LEFT JOIN {$GLOBALS['db_prefix']}_companies AS company ON company.company_id = users.company_id
                        WHERE user_email_address = AES_ENCRYPT(:user_email_address, :phrase)
						OR user_username = AES_ENCRYPT(:user_username, :phrase) 
						AND users.active = 'Y' OR users.active = 'NULL';
						LIMIT 1";
        $sql_user = $GLOBALS['dbCon']->prepare($statement);
        $sql_user->bindParam(':user_email_address', $email);
        $sql_user->bindParam(':user_username', $email);
        $sql_user->bindParam(':phrase', $phrase);
        $sql_user->execute();
        $sql_user_data = $sql_user->fetch();
        $sql_user->closeCursor();
        return $sql_user_data;
    }

}