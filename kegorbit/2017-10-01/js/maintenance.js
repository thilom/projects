$(document).ready(function() {


    $.ajax({
        type: 'GET',
        url: '/ajax/check_maintenance_mode.ajax.php',
        async: false,
        success: function (data) {
            if (data.responseText == 'none') {

            }
            else {

                var myObject = eval('(' + data + ')');
                console.log(myObject);

                if (myObject["maintenance_commence"]  <= myObject["today"]){
                    $('#countdown').slideDown();
                    $("#countdown")
                        .countdown(myObject["maintenance_end"], function(event) {
                            $(this).text(
                                event.strftime('Returns in: %D days %H:%M:%S')
                            );
                        });
                    if (myObject["maintenance_timer"] == '1') {

                        var closeInSeconds = myObject["seconds"] * 86400,
                            displayText = myObject["countdown_message"] + "We Will be Back in #1 seconds.", timer;
                        swal({
                            title: "Scheduled Maintenance in Progress",
                            text: displayText.replace(/#1/, closeInSeconds),
                            type: "warning",
                            timer: closeInSeconds * 1000,
                            showConfirmButton: true,
                            closeOnConfirm: true,
                            html: true
                        });
                        timer = setInterval(function () {
                            closeInSeconds--;
                            if (closeInSeconds < 0) {
                                clearInterval(timer);
                            }
                            $('.sweet-alert > p').text(displayText.replace(/#1/, closeInSeconds));
                        }, 1000);
                    }
                    else {
                        var closeInSeconds = myObject["seconds"] * 86400,
                            displayText = myObject["countdown_message"] + "We Will be Back in #1 seconds.", timer;
                        swal({
                            title: "Scheduled Maintenance in Progress",
                            text: displayText.replace(/#1/, closeInSeconds),
                            type: "warning",
                            timer: closeInSeconds * 1000,
                            showConfirmButton: true,
                            closeOnConfirm: true,
                            html: true
                        });
                        timer = setInterval(function () {
                            closeInSeconds--;
                            if (closeInSeconds < 0) {
                                clearInterval(timer);
                            }
                            $('.sweet-alert > p').text(displayText.replace(/#1/, closeInSeconds));
                        }, 1000);

                    }

                }
                else{

                }
            }

        }

    });
});





