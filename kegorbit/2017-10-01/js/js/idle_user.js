/**
 * Created by khulumusa on 9/7/16.
 */
$(document).ready(function() {
    $.ajax({
        type: 'GET',
        url: '/ajax/check_maintenance_mode.ajax.php' ,
        async: false,
        success: function(data) {
            if(data.responseText  == 'none'){

            }
            else {
                var myObject = eval('(' + data + ')');
                console.log(myObject);
                    if(myObject["popup_frequency"] == '1'){
                        if(localStorage.getItem('popState') != 'shown'){
                            //alert('once upon logging in');
                            var message =   myObject["popup_message"] +"<br> <b>   Commencing: </b>" + myObject["maintenance_commence"]+ "<br>Ending:" + myObject["maintenance_end"];
                            swal({
                                    title: "Scheduled Maintenance",
                                    text: '<p><b><span style="color:'+ myObject["popup_color"] +'">'+message+'<span></b></p>',
                                    type: "warning",
                                    confirmButtonColor: "#4caf50",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true,
                                    html: true
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                    }
                                });
                            localStorage.setItem('popState','shown');
                        }
                    }
                    else if(myObject["popup_frequency"] == '2'){
                        message =   myObject["popup_message"] +"<br> <b>   Commencing: </b>" + myObject["maintenance_commence"]+ "<br>Ending:" + myObject["maintenance_end"];
                        setInterval(function(){
                            swal({
                                    title: "Scheduled Maintenance",
                                    text: '<p><b><span style="color:'+ myObject["popup_color"] +'">'+message+'<span></b></p>',
                                    type: "warning",
                                    confirmButtonColor: "#4caf50",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true,
                                    html: true
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                    }
                                });
                        }, 300000);
                    }
                    else if(myObject["popup_frequency"] == '3'){
                        message =   myObject["popup_message"] +"<br> <b>   Commencing: </b>" + myObject["maintenance_commence"]+ "<br>Ending:" + myObject["maintenance_end"];
                        setInterval(function(){
                            swal({
                                    title: "Scheduled Maintenance",
                                    text: '<p><b><span style="color:'+ myObject["popup_color"] +'">'+message+'<span></b></p>',
                                    type: "warning",
                                    confirmButtonColor: "#4caf50",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true,
                                    html: true
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                    }
                                });
                        }, 600000);
                    }
                    else if(myObject["popup_frequency"] == '4'){
                        message =   myObject["popup_message"] +"<br> <b>   Commencing: </b>" + myObject["maintenance_commence"]+ "<br>Ending:" + myObject["maintenance_end"];
                        setInterval(function(){
                            swal({
                                    title: "Scheduled Maintenance",
                                    text: '<p><b><span style="color:'+ myObject["popup_color"] +'">'+message+'<span></b></p>',
                                    type: "warning",
                                    confirmButtonColor: "#4caf50",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true,
                                    html: true
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                    }
                                });
                        }, 900000);
                    }


            }

        }

    });
    //
    // window.setTimeout(
    //     function(){
    //         alert('musa');
    //         //setInterval(function(){   alert('musa');}, 3000);
    //     }
    //     ,3000);



    $( document ).idleTimer( 1200000 ); //20 minutes 1200000

    $( document ).on( "idle.idleTimer", function(event, elem, obj) {
        $.getScript("/lib/css/plugins/sweetalert/sweetalert.css");
        $.getScript("/lib/js/plugins/sweetalert/sweetalert.min.js")
            .done(function() {
                swal({
                    title: "Idle Screen Lock",
                    text: "Enter your password to Continue:",
                    type: "input",
                    inputType: "password",
                    showCancelButton: false,
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: "",
                    allowEscapeKey: false
                }, function (inputValue) {
                    if (inputValue === false) return false;
                    if (inputValue === "") {
                        swal.showInputError("You need to enter your password");
                        return false
                    }
                    $.ajax({
                        url: '/ajax/login.ajax.php' ,
                        method: 'post',
                        data: {'password':inputValue,'email':$('#userEmail').val(), 'source':'idleTimer'},
                        complete: function(data) {
                            if (data.responseText == 'Incorrect Password') {
                                swal.showInputError("The password you entered is incorrect");
                            } else {
                                swal.close();
                            }
                        }

                    })
                });
            });



    });

    $( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent) {
        // function you want to fire when the user becomes active again
        console.log('Active')
    });


});







