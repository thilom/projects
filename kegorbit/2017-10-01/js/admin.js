
var maintenanceTimer;

$(document).ready(function() {

    //Start maintenance timer
    maintenanceTimer = setInterval(function() {
        $.ajax({
            url: '/ajax/monitor_maintenance.ajax.php',
            complete: function(data) {

                if (data.responseText == '1') {

                    console.log(readCookie('maintenance'));

                    if (readCookie('maintenance') === null) {
                        createCookie('maintenance', '101010', 1);
                        swal({
                            title: "Scheduled Maintenance",
                            text: '<p><b><span style="">System Maintenance Started. You will be logged out in 60 seconds.<span></b></p>',
                            type: "warning",
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true,
                            html: true
                        });
                    } else {
                        eraseCookie('maintenance');
                        document.location = '/index.php?logout'
                    }
                }
            }
        })
    }, 60000);

    //setInterval(function() {
    //    $.ajax({
    //        url: '/ajax/server_time.ajax.php',
    //        complete: function (data) {
    //            console.warn(data.responseText);
    //        }
    //    });
    //}, 3000);

                //add active class to thew selected menu
    var active_menu = getParameterByName('m');
    var activeList = document.getElementById(active_menu);
    if(activeList != null){
        var element = document.getElementById("dashboard_management");
        if (element != null) {
            document.getElementById("dashboard_management").className = "";

        }
        activeList.className += " active";
    }


    //Form validations
    $.fn.bootstrapValidator.validators.duplicateUsername = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/ajax/check_duplicate_email.ajax.php?email=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });

            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };


    $.fn.bootstrapValidator.validators.check_password_length = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            //get value
            var passsword_length = $field.val();
            //
            if (passsword_length.length < 8) {
                var correct = false;

            }
            else {
                correct = true;
            }
            return correct;

        }
    };

    //check valid password
    $.fn.bootstrapValidator.validators.check_password = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var ok = 0;
            // Perform validating
            $.ajax({
                url: '/ajax/check_password.ajax.php?password=' +$field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    ok = data;
                }
            });

            // return true if the field value is valid
            // otherwise return false
            return ok != 0;
        }
    };

    $('#resetPasswordForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {message: "Please Provide Your Email."}
                }
            },
            password: {
                validators: {
                    notEmpty: {message: "Please Provide Your Password."},
                    check_password: {message: 'Password should contain one letter and one character'},
                    check_password_length: {message: "Password Minimum Length – 8 characters"}
                }
            },
            password_confirm: {
                validators: {
                    notEmpty: {message: "Please Comfirm Your Password."},
                    identical: {
                        field: "password",
                        message: "Password does not match the confirm password."
                    }
                }
            }

        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/ajax/forgot_password.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if (data === 'login') {
                    document.location = '/index.php';
                } else {

                    $('#password').val("");
                    $('#errorMessage').html(data);
                    $('#errorMessage').slideDown();
                }

            }
        });
    });

    $('#loginForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {message: "Please Provide Your Email or Username."}
                }
            },
            password: {
                validators: {
                    notEmpty: {message: "Please Provide Your Password."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');
        localStorage.setItem('popState','empty');
        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/ajax/login.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if (data === 'login') {
                    document.location = 'index.php';
                } else {
                    $('#password').val("");
                    $('#errorMessage').html(data);
                    $('#errorMessage').slideDown();
                    grecaptcha.reset();
                }

            }
        });
    });

    $.fn.bootstrapValidator.validators.check_password_length = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            //get value
            var passsword_length = $field.val();
            //
            if (passsword_length.length < 8) {
                var correct = false;

            }
            else {
                correct = true;
            }
            return correct;

        }
    };
    //check valid password
    $.fn.bootstrapValidator.validators.check_password = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var ok = 0;
            // Perform validating
            $.ajax({
                url: '/ajax/check_password.ajax.php?password=' +$field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    ok = data;
                }
            });

            // return true if the field value is valid
            // otherwise return false
            return ok != 0;
        }
    };

    $('#newPasswordForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {message: "Please Provide Your Email."}
                }
            },
            password: {
                validators: {
                    notEmpty: {message: "Please Provide a Password."},
                    check_password: {message: 'Password should contain one letter and one character'},
                    check_password_length: {message: "Password Minimum Length – 8 characters"}
                }
            },
            password_confirm: {
                validators: {
                    notEmpty: {message: "Please Confirm Password."},
                    identical: {
                        field: "password",
                        message: "Password does not match the confirm password."
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/ajax/save_new_password.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if (data === 'login') {
                    document.location = '/index.php';
                } else {
                    $('#password').val("");
                    $('#errorMessage').html(data);
                    $('#errorMessage').slideDown();
                }
            }
        });
    });
    //check if hard limit is checked
    $('input').on('ifChecked', function (event){
        $("#hard_limit").val(1);
    });
    $('input').on('ifUnchecked', function (event) {
        $("#hard_limit").val(0);
    });

    //Idle Timer

    $('#requestNewPasswordForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {message: "Please Provide Your Email."}
                }
            }

        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/ajax/request_password_reset_user.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if(data =='Success: An email has been sent to the email provided please check your email for further instructions.'){
                    $('#errorMessage').html(data);
                    $('#errorMessage').slideDown();
                    self.close();
                }
                else{
                    $('#errorMessage').html(data);
                    $('#errorMessage').slideDown();
                }
            }
        });
    });

    if (detectIE() !== false) {
        $('#ieNotice').slideDown();
    }

});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function runAdmin() {
    $.getScript("/lib/css/plugins/sweetalert/sweetalert.css");
    $.getScript("/lib/js/plugins/sweetalert/sweetalert.min.js")
        .done(function() {
            swal({
                title: "Switch to Account",
                text: "Are you sure you want to switch to back to admin account",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    type: 'GET',
                    url: '/modules/company_management/ajax/switch_account.ajax.php?code=return',
                    success: function (data) {
                        document.location = '/index.php';
                    }
                });
            });

        });
}

function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}


function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-100);
    console.log('Cookie Erased');
}
