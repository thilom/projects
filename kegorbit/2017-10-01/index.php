<?php

/**
 * Keg Orbit intial page
 * 2016-05-25: Musa Khulu - Created
 *
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/company_management/classes/company.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";


//Vars
$template = file_get_contents('html/login_screen.html');
$companyClass = new Company();
$securityClass = new Security();
$userClass = new User();
$systemClass = new System();
$settingsClass = new Settings();
$applicationClass = new Application();
$access_available = FALSE;
$about_module = '';
$firstname ='';
$lastname = '';
$usertype = '';
$menu = '';
$tContent = '';
$location = '';
$user_email = '';
$logo = '/assets/images/KegOrbit-black.png';
$switch_string = '';

if (isset($_GET['dev'])) {
    if ($_GET['dev'] == 1) {
        $_SESSION['user']['dev'] = 1;
    } else {
        unset($_SESSION['user']['dev']);
    }
}

//Logout if requested
if (isset($_GET['logout'])) {
    unset($_SESSION['user']);
    setcookie("loggedin", "false", time()+30);
    die("<script>document.location='index.php'</script>");
}
//Logout ends

//New User first time logging onto the system to create password
if(isset($_GET['new']) && isset($_GET['code'])) {
    $user_id = $_GET['new'];
    $code = $_GET['code'];

    $template = file_get_contents('html/new_user_password_screen.html');
    $user = $securityClass->check_user_activation_code($user_id, $code);
    $user_data = $userClass->get_user($user_id);
    if($user == false) {
        die("<script>document.location='/index.php'</script>");
    }
    else {
        $template_data['user_id'] = $user_id;
        $template_data['code'] = $code;
        $template = str_replace('<!-- user_id -->', $user_id, $template);
        $template = str_replace('<!-- code -->', $code, $template);
        $template = str_replace('<!-- email_address -->', $user_data['email'], $template);
    }
}

//Reset password
if(isset($_GET['code']) && isset($_GET['u'])) {
    $user_id = $_GET['u'];
    $code = $_GET['code'];
    $template = file_get_contents('html/reset_password_screen.html');

    $user = $securityClass->check_user_activation_code($user_id, $code);
    $user_data = $userClass->get_user($user_id);
    if($user == false) {
        die("<script>document.location='/index.php'</script>");
    }
    else
    {
        $template_data['user_id'] = $user_id;
        $template_data['code'] = $code;
        $template = str_replace('<!-- user_id -->', $user_id, $template);
        $template = str_replace('<!-- code -->', $code, $template);
        $template = str_replace('<!-- email_address -->', $user_data['email'], $template);

    }
}

//Forgot Password screen
if(isset($_GET['forgot_password'])) {
    $template = file_get_contents('html/forgot_password_screen.html');
}


//Check if user is logged in
if ($securityClass->is_logged_in() === true) {
    $template = file_get_contents('templates/backend/index.html');
} else {
    //Get settings
    $settings = $applicationClass->get_settings();
    if (isset($settings['login_captcha']) && $settings['login_captcha'] =='1') {
        $captcha_js = "<script src='https://www.google.com/recaptcha/api.js'></script>";
        $captcha_html = "<div class='form-group'>
                    <div class='g-recaptcha' data-sitekey='{$settings['captcha_site']}'></div>
                </div>";

        $ie_notice = '<div id="ieNotice" class="alert alert-info" style="display: none">
                reCAPTCHA cannot display correctly if Internet Explorer Compatibility View is enabled. If you DO NOT
                see reCAPTCHA below, click <a href="https://support.google.com/recaptcha/?hl=en#6223828" target="_blank">here</a> to rectify.
            </div>';

        $template = str_replace("<!-- captcha_js -->", $captcha_js, $template);
        $template = str_replace("<!-- captcha_html -->", $captcha_html, $template);
        $template = str_replace("<!-- ie_notice -->", $ie_notice, $template);
    }
}


//Get modules and it's icons
if (isset($_SESSION['user'])) {

    //get user details
    if(isset($_SESSION['user']['run_as'])){
        $user = $userClass->get_user($_SESSION['user']['id']);
        //get user modules

        $modules = $securityClass->get_user_modules($_SESSION['user']['run_as_id']);

        //get company logo
        $company = $userClass->get_company_logo($_SESSION['user']['run_as']);
    }
    else{
        $user = $userClass->get_user($_SESSION['user']['id']);
        //get user modules
        $modules = $securityClass->get_user_modules($_SESSION['user']['id']);
        //get company logo
        $company = $userClass->get_company_logo($_SESSION['user']['company_id']);
    }
    if(empty($company)){
        $logo = '/assets/images/KegOrbit-Transparent.png';
    }
    else{
        $logo = '/uploads/users/images/'.$company;
    }

    $menu = '';

    foreach($modules as $module) {

        $module_id = $module['module_id'];
        $module_folder = $module['module_folder'];
        $module_name = $module['module_name'];

        //Apply company specified terminology to the keg management module
        if($module_name == 'Container Management'){

            $name = $settingsClass->get_container_terminology();
            $module_name = $name.' Management';
        }
        //Apply company specified terminology to the customers/clients module
        if($module_name == 'Customers'){
            $customers = $settingsClass->get_customer_terminology($_SESSION['user']['company_id']);
            $module_name = $customers;
        }
        $module_font_awesome = $module['module_font_awesome'];
        $module_version = $module['module_version'];
        $module_page_name = $module['module_page_name'];

        if($module_folder == 'dashboard_management'){
            $menu .= '<li class="active" id="'.$module_folder.'">';
            $menu .= "<a href='/index.php'><i class='fa {$module_font_awesome}' title='{$module_name}'></i> <span class='nav-label'>$module_name</span></a>";
        }
        else{
            $menu .= '<li id="'.$module_folder.'">';
            $menu .= "<a href='/index.php?m={$module_folder}&a={$module_page_name}' title='{$module_name}'><i class='fa {$module_font_awesome}'></i> <span class='nav-label'>$module_name</span></a>";
            $menu .= '</li>';
        }
    }
    $menu .= '<li id="help_and_support"><a href="http://kegorbit.com/support" target="_blank"><i class="fa fa-info"  title=" help and support external"></i><span class="nav-label"> Help/Support</span> </a></li>';

    //get user type
    $usertype = $user['user_type'];
    //get user type name
    $usertype = $userClass->get_usertype_name($usertype);


    // get users name and surname
    if(isset($user['firstname'])) {
        $firstname = $user['firstname'];

    }

    if(isset($user['email'])) {
        $user_email = $user['email'];
    } else {
        $user_email = '';
    }

    //Get content area
    if (!isset($_GET['m'])) {
        $location = "{$_SERVER['DOCUMENT_ROOT']}/modules/dashboard_management/controllers/dashboard.php";
    } else {
        $location = "{$_SERVER['DOCUMENT_ROOT']}/modules/{$_GET['m']}/controllers/{$_GET['a']}.php";
    }

    ob_start();
    include($location);
    $tContent = ob_get_contents();
    ob_end_clean();

    if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
        $company_data = $companyClass->get_company_by_code($_SESSION['user']['run_as']);
        $switch_string = "<a class='label label-warning' onclick='runAdmin()'>
                            <i class='fa fa-exclamation-triangle'></i> Running As: {$company_data['company_name']}
                            </a>";
    }

}
$title = isset($_GET['m'])?$_GET['m']:'';

//Dev stuff
if (isset($_SESSION['user']['dev']) && $_SESSION['user']['dev']==1) {
    $menu .= "<div class='alert alert-info' style='margin: 4px'><b>DEV STUFF</b>";
    $menu .= "<li>Company ID: " . (isset($_SESSION['user']['company_id']) ? $_SESSION['user']['company_id'] : ' -') . "</li>";
    $menu .= "<li>User ID: " . (isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : ' - ') . "</li>";
    $menu .= "<li>Prefix: " . (isset($_SESSION['user']['prefix']) ? $_SESSION['user']['prefix'] : ' -') . "</li>";
    $menu .= "<li>Run As: " . (isset($_SESSION['user']['run_as']) ? $_SESSION['user']['run_as'] : ' - ') . "</li>";

    $menu .= "</div>";
}

//Replace Tags
$template = str_replace('<!-- content -->', $tContent, $template);
$template = str_replace('<!-- navigation -->', $menu, $template);
$template = str_replace('<!-- admin_title -->', $GLOBALS['site_name'] . ' - Keg Management and Tracking', $template);
$template = str_replace('<!-- admin_logo -->', $logo, $template);
$template = str_replace('<!-- name -->', $firstname, $template);
$template = str_replace('<!-- user_email -->', $user_email, $template);
$template = str_replace('<!-- user_type -->', $usertype, $template);
$template = str_replace('<!-- switch_string -->', $switch_string, $template);

echo $template;