$(document).ready(function() {
    //backup click
    $('#backup').click(function () {
        document.location = '/index.php?m=settings_management&a=backup_data';
    });

    $('#terminology').click(function () {
        document.location = '/index.php?m=settings_management&a=edit_terminology';
    });

    $('#company').click(function () {
        document.location = '/index.php?m=settings_management&a=edit_company';
    });

    $('#application').click(function () {
        document.location = '/index.php?m=settings_management&a=application';
    });

    $('#about').click(function () {
        document.location = '/index.php?m=settings_management&a=about';
    });
    $('#stages').click(function () {
        document.location = '/index.php?m=settings_management&a=list_stages';
    });
    $('#states').click(function () {
        document.location = '/index.php?m=settings_management&a=list_states';
    });
    $('#products').click(function () {
        document.location = '/index.php?m=settings_management&a=list_types';
    });
    $('#subscription').click(function () {
        document.location = '/index.php?m=settings_management&a=subscription';
    });
    $('#container_types').click(function () {
        document.location = '/index.php?m=settings_management&a=container_types';
    });

    $('#maintenance').click(function () {
        document.location = '/index.php?m=settings_management&a=schedule_maintenance';
    });




});