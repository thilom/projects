$(document).ready(function() {


    $('#edittypeForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            type_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Type Name."}
                }
            },
            type_capacity: {
                validators: {
                    notEmpty: {message: "Please Provide  the capacity"}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/settings_management/ajax/save_type.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if (data == 'update') {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'Type Successfully Updated.';
                    toastr.success(toastMessage);

                    swal({
                            title: "Type Successfully Updated",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To Type List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {

                            } else {
                                document.location = 'index.php?m=settings_management&a=container_types';
                            }
                        })
                }
                else{
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'New Type Added!';
                    toastr.success(toastMessage);

                    swal({
                            title: "New Type Added!",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Create Another Type",
                            cancelButtonText: "Back To Type List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $("#edittypeForm")[0].reset();

                            } else {
                                document.location = 'index.php?m=settings_management&a=container_types';
                            }
                        })
                }
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        document.location = 'index.php?m=settings_management&a=container_types';
    });
});