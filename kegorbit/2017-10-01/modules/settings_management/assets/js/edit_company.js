$(document).ready(function() {


    $('#editcompanyForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            company_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Company Name."}
                }
            },
            company_contact_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Company Contact Name."}
                }
            },
            company_contact_number: {
                validators: {
                    notEmpty: {message: "Please Provide  the Company Contact Number."}
                }
            },
            company_email: {
                validators: {
                    notEmpty: {message: "Please Provide  the Company Email."}
                }
            },
            keg_limit: {
                validators: {
                    notEmpty: {message: "Please Provide  the Keg Limit."}
                }
            },
            user_limit: {
                validators: {
                    notEmpty: {message: "Please Provide  the User Limit."}
                }
            },
            device_limit: {
                validators: {
                    notEmpty: {message: "Please Provide  the Device Limit."}
                }
            },
            delivery_street_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Street Name."}
                }
            },
            delivery_suburb: {
                validators: {
                    notEmpty: {message: "Please Provide  the suburb."}
                }
            },
            delivery_town: {
                validators: {
                    notEmpty: {message: "Please Provide  the Town/City."}
                }
            },
            delivery_physical_code: {
                validators: {
                    notEmpty: {message: "Please Provide  the Physical Code."}
                }
            },
            invoice_street_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Street Name."}
                }
            },
            invoice_suburb: {
                validators: {
                    notEmpty: {message: "Please Provide  the suburb."}
                }
            },
            invoice_town: {
                validators: {
                    notEmpty: {message: "Please Provide  the Town/City."}
                }
            },
            invoice_physical_code: {
                validators: {
                    notEmpty: {message: "Please Provide  the Physical Code."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/settings_management/ajax/save_company.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {

                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'Company Successfully Updated.';
                    toastr.success(toastMessage);

                    swal({
                            title: "Company Successfully Updated",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To Settings List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {

                            } else {
                                document.location = 'index.php?m=settings_management&a=list_settings';
                            }
                        })
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        document.location = 'index.php?m=settings_management&a=list_settings';
    });

    $('#file').change(function () {
        $("#editcompanyForm").ajaxSubmit({
            url: "/modules/settings_management/ajax/save_company_logo.ajax.php",
            success: function(data){
                $("#image").attr("src","/uploads/users/images/"+data);
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = data + ' has been set as the new company logo.';
                toastr.success(toastMessage);
            }
        });
    })


    $('#imports').each(function(){
        var self = $(this),
            label = self.next(),
            label_text = label.text();
        label.remove();
        self.iCheck({
            checkboxClass: 'icheckbox_line-aero',
            radioClass: 'iradio_line-aero',
            insert: '<span class="icheck_line-icon"></span>' + label_text
        });
    });

    $('#same').each(function(){
        var self = $(this),
            label = self.next(),
            label_text = label.text();
        label.remove();
        self.iCheck({
            checkboxClass: 'icheckbox_line-aero',
            radioClass: 'iradio_line-aero',
            insert: '<span class="icheck_line-icon"></span>' + label_text
        });
    });

    $('#same').on('ifChecked', function() {
        $('.addr').prop('disabled', true);
        $('.addr1').data('value', $('.addr1').val());
        $('.addr2').data('value', $('.addr2').val());
        $('.addr3').data('value', $('.addr3').val());
        $('.addr4').data('value', $('.addr4').val());
        $('.addr').val('');
    })

    $('#same').on('ifUnchecked', function() {
        $('.addr').prop('disabled', false);
        $('.addr1').val($('.addr1').data('value'));
        $('.addr2').val($('.addr2').data('value'));
        $('.addr3').val($('.addr3').data('value'));
        $('.addr4').val($('.addr4').data('value'));
    })

    if ($('#same').iCheck('update')[0].checked ) {
        $('.addr').prop('disabled', true);
        $('.addr1').data('value', $('.addr1').val());
        $('.addr2').data('value', $('.addr2').val());
        $('.addr3').data('value', $('.addr3').val());
        $('.addr4').data('value', $('.addr4').val());
        $('.addr').val('');
    }

});