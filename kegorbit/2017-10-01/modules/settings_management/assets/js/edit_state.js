
$(document).ready(function() {
    $('#oos, #is_default').iCheck({
        checkboxClass: 'icheckbox_square-green',
        increaseArea: '20%' // optional
    });
});

function saveState() {
    $.ajax({
        url: '/modules/settings_management/ajax/save_state.ajax.php',
        method: 'post',
        data: $('#stateForm').serialize(),
        complete: function(data) {
            if ($('#state_id').val() == 0) {
                message = "Container State '"+ $('#state_name').val() +"' Created";
            } else {
                message = "Container State '"+ $('#state_name').val() +"' Updated";
            }

            swal({
                    title: "Edit Container State",
                    text: message,
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#458B00",
                    confirmButtonText: "Continue Editing",
                    cancelButtonText: "Done",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#state_id').val(data.responseText)
                        //$('#state_title').text($('#state_name').val());
                        $('.state_task').text('Edit');
                    } else {
                        document.location = '/index.php?m=settings_management&a=list_states';
                    }
                });
        }
    });
}

/**
 * Cancel Container state editing
 */
function cancelEdit() {
    document.location = '/index.php?m=settings_management&a=list_states';
}