$(document).ready(function() {

    $('#is_def').iCheck({
        checkboxClass: 'icheckbox_square-green',
        increaseArea: '20%' // optional
    });



    $('#editstageForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            stage_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the stage Name."}
                }
            },
            onselect: {
                validators: {
                    notEmpty: {message: "Please Provide the next stage to."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/settings_management/ajax/save_stage.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if (data == 'update') {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'Stage Successfully Updated.';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#stage_name').val() +" Successfully Updated",
                            text: $('#stage_name').val() +" Successfully Updated",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To Stage List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {

                            } else {
                                document.location = 'index.php?m=settings_management&a=list_stages';
                            }
                        })
                }
                else{
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'New Stage Added!';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#stage_name').val() +" Successfully Added",
                            text: $('#stage_name').val() +" Successfully Added",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Create Another Stage",
                            cancelButtonText: "Back To Stage List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $("#editstageForm")[0].reset();

                            } else {
                                document.location = 'index.php?m=settings_management&a=list_stages';
                            }
                        })
                }
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        document.location = 'index.php?m=settings_management&a=list_stages';
    });
});