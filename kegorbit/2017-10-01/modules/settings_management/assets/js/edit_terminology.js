$(document).ready(function() {

    $('#editTerminologyForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            container_singular: {
                validators: {
                    notEmpty: {message: "Please Provide  the Singular form for container."}
                }
            },
            container_plural: {
                validators: {
                    notEmpty: {message: "Please Provide  the Plural form for containers."}
                }
            },
            stage_singular: {
                validators: {
                    notEmpty: {message: "Please Provide  the Singular form for stage."}
                }
            },
            stage_plural: {
                validators: {
                    notEmpty: {message: "Please Provide  the Plural form for stages."}
                }
            },
            state_singular: {
                validators: {
                    notEmpty: {message: "Please Provide  the Singular form for state."}
                }
            },
            state_plural: {
                validators: {
                    notEmpty: {message: "Please Provide  the Plural form for states."}
                }
            },
            client_singular: {
                validators: {
                    notEmpty: {message: "Please Provide  the Singular form for Client."}
                }
            },
            client_plural: {
                validators: {
                    notEmpty: {message: "Please Provide  the Plural form for Clients."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/settings_management/ajax/save_terminology.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = 'Company Terminology Successfully Updated';
                toastr.success(toastMessage);
                    swal({
                            title: "Company Terminology Successfully Updated",
                            text: "ALL instances of Container, State and Stage have been modified according to your specification",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To Settings",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {

                            } else {
                                document.location = '/index.php?m=settings_management&a=list_settings';
                            }
                        })
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        document.location = '/index.php?m=settings_management&a=list_settings';
    });

});