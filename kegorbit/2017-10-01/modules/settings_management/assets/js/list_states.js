
var activeTable, inactiveTable;

$(document).ready(function() {

    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        if ($(e.target).attr('id') == 'inactiveTab') drawInactiveStateTable();
    } );

    drawStateTable();
});

function drawStateTable() {
    activeTable = $('#stateTable').DataTable({
        ajax: '/modules/settings_management/ajax/list_states.ajax.php',
        destroy: true,
        stateSave: true,
        stateDuration: 0
    });
}


function drawInactiveStateTable() {
    inactiveTable = $('#inactiveStateTable').DataTable({
        ajax: '/modules/settings_management/ajax/list_states.ajax.php?type=inactive',
        destroy: true,
        stateSave: true,
        stateDuration: 0
    });
}

function editState(id) {
    document.location = "/index.php?m=settings_management&a=edit_states&id=" + id;
}


function toggle_active_state(id,p,n) {
    swal({
            title: p==0?"Deactivate Container State?":"Activate Container State?",
            text: n,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F8AC59",
            confirmButtonText: p==0?"Yes - Deactivate":"Yes - Activate",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '/modules/settings_management/ajax/toggle_active_state.ajax.php?id=' + id,
                    complete: function() {
                        setTimeout(function () {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                positionClass: 'toast-bottom-right',
                                timeOut: 4000
                            };
                            toastMessage = p==0?'Container State \'' + n + '\' deactivated':'Container State \'' + n + '\' activated'
                            toastr.success(toastMessage);
                            p===0?drawStateTable():drawInactiveStateTable();

                        }, 1300);
                    }
                })
            }
        });
}