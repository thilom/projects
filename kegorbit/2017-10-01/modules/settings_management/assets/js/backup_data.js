/**
 * Backup management
 *
 * 2016-07-18: Thilo Muller - Created
 */


function run_backup() {

    var fileName = '';
    var l = '';

    $.ajax({
        url: '/modules/settings_management/ajax/run_backup.ajax.php',
        beforeSend: function() {
            l = $('#run_backup').ladda();
            l.ladda( 'start' );
        },
        complete: function(data) {
            fileName = data.responseText;
            l.ladda('stop');
            swal({
                    title: "Backup",
                    text: "Backup created. ",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#458B00",
                    confirmButtonText: "Download Backup File",
                    cancelButtonText: "Done",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        document.location = "/modules/settings_management/ajax/download_backup.ajax.php?file=" + fileName;
                    }
                });
        }
    });
}

function restore_backup() {

    var ld = '';
    var fd = new FormData($("#restoreForm"));
    fd.append('restoreFile', $('#restoreFile')[0].files[0]);

    $.ajax({
        url: '/modules/settings_management/ajax/restore_backup.ajax.php',
        beforeSend: function() {
            ld = $('#restore_backup_button').ladda();
            ld.ladda( 'start' );
        },
        method: 'post',
        data: fd,
        processData: false,
        contentType: false,
        complete: function(data) {
            ld.ladda('stop');
            swal({
                    title: "Backup",
                    text: "Backup restored. ",
                    type: "success",
                    showCancelButton: true,
                    showConfirmButton: false,
                    confirmButtonColor: "#458B00",
                    confirmButtonText: "Download Backup File",
                    cancelButtonText: "OK",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        document.location = "/data_files/backups/" + fileName;
                    }
                });
        }
    });
}
