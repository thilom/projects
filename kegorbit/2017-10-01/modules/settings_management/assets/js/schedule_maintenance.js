$(document).ready(function() {
    $('#activate_maintenance').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square'
    });
    $('#activate_countdown').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square'
    });

    $('#cancel_popup').click(function () {
        document.location = '/index.php?m=settings_management&a=list_settings';
    });

    $('#activate_maintenance').on('ifChecked', function() {
        $('.pop').prop('disabled', false);
        $('#preview').prop('disabled', false);

    });

    $('#activate_maintenance').on('ifUnchecked', function() {
        $('.pop').prop('disabled', true);
        $('#preview').prop('disabled', true);
    });
    var chckValue = $('#activate_maintenance').iCheck('update')[0].checked;

    if (chckValue == false) {
        $('.pop').prop('disabled', true);
        $('#preview').prop('disabled', true);
    }



    $('#activate_countdown').on('ifChecked', function() {
        $('.count').prop('disabled', false);

    });

    $('#activate_countdown').on('ifUnchecked', function() {
        $('.count').prop('disabled', true);
    });
    var checkValue = $('#activate_countdown').iCheck('update')[0].checked;

    if (checkValue == false) {
        $('.count').prop('disabled', true);
    }

        $('#preview').click(function () {
       var message =   $('#display_message').val() +"<br> <b>   Commencing: </b>" + $('#start').val()+ "<br>Ending:" + $('#end').val();
        swal({
                title: "Scheduled Maintenance",
                text: '<p><b><span style="color:'+ $('#color').val() +'">'+message+'<span></b></p>',
                type: "warning",
                confirmButtonColor: "#4caf50",
                confirmButtonText: "Ok",
                closeOnConfirm: true,
                html: true
            },
            function (isConfirm) {
                if (isConfirm) {

                }
            })
    });

    $('#maintenaceForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            popup_frequency: {
                validators: {
                    notEmpty: {message: "Please Choose the popup Frequency."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/settings_management/ajax/save_maintenance_mode.ajax.php',
            success: function (data) {
                swal({
                        title: " Maintenance Scheduled",
                        text: "  Maintenance Scheduled Successfully",
                        type: "success",
                        showCancelButton: true,
                        confirmButtonColor: "#4caf50",
                        confirmButtonText: "Continue Editing",
                        cancelButtonText: "Settings",
                        closeOnConfirm: true,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {

                        } else {
                            document.location = 'index.php?m=settings_management&a=list_settings';
                        }
                    })
            }
        })
    });

    $('#countdownForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            countdown_message: {
                validators: {
                    notEmpty: {message: "Please provide the message to display below count down."}
                }
            },
            count_down_color: {
                validators: {
                    notEmpty: {message: "Please provide the color of the ."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/settings_management/ajax/save_countdown_message.ajax.php',
            success: function (data) {
                swal({
                        title: "Count down  Maintenance Scheduled",
                        text: " Count down  Maintenance Scheduled Successfully",
                        type: "success",
                        showCancelButton: true,
                        confirmButtonColor: "#4caf50",
                        confirmButtonText: "Continue Editing",
                        cancelButtonText: "Settings",
                        closeOnConfirm: true,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {

                        } else {
                            document.location = 'index.php?m=settings_management&a=list_settings';
                        }
                    })
            }
        })
    });

});
