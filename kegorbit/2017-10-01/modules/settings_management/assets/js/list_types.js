$(document).ready(function () {
    create_table();
    create_table2();

});

$('#activeTypes').click(function(){
    create_table();
});
$('#inactiveTypes').click(function(){
    create_table2();
});

function create_table() {
    var active = "Y";

    $('#activeTypesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/settings_management/ajax/list_types.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": false, className: 'text-center'}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#activeTypesTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

function create_table2() {
    var active = "N";

    $('#inactiveTypesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/settings_management/ajax/list_types.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": false, className: 'text-center'}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#inactiveTypesTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}


$(document).on('click', '.deactivateBtn', function(event) {
    var deactivate_url = $(this).attr('href');
    var type_name = $(this).data('type');
    swal({
        title: "Are you sure?",
        text: "You will be able to undo this action by activating the type later under the inactive type Tab",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Deactivate!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: 'GET',
            url: deactivate_url,
            success: function (data) {
                swal("Type Disabled!", "Type has been suspended.", "success");
                create_table();
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = type_name + ' has been suspended.';
                toastr.success(toastMessage);
            }
        });
    });
    event.preventDefault();
});

$(document).on('click', '.activateBtn', function(event) {
    var activate_url = $(this).attr('href');
    var type_name = $(this).data('type');
    swal({
        title: "Are you sure?",
        text: "You will be able to undo this action by deactivating the type later under the active type Tab",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#458B00",
        confirmButtonText: "Yes, Activate!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: 'GET',
            url: activate_url,
            success: function (data) {
                swal("Type Activated!", "Type has been activated.", "success");
                create_table2();
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = type_name + ' has been activated.';
                toastr.success(toastMessage);
            }
        });
    });
    event.preventDefault();
});

//create new company button
$('#create_type').click(function(){
    document.location = 'index.php?m=settings_management&a=edit_type&type_id=0';
});
