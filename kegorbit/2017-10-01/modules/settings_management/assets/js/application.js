/**
 * Created by thilom on 2016/07/19.
 */

$(document).ready(function() {
    $('#login_captcha').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square'
    });
    $('#login_captcha').on('ifChecked', (function() {
        $('#login_captcha_settings').slideDown();
    }));
    $('#login_captcha').on('ifUnchecked', (function() {
        $('#login_captcha_settings').slideUp();
    }));

    toggleCaptchaSettings();

});

function cancelEdit() {
    document.location = '/index.php?m=settings_management&a=list_settings';
}

function saveSettings() {
    $.ajax({
        url: '/modules/settings_management/ajax/save_application_settings.ajax.php',
        method: 'post',
        data: $('#settingForm').serialize(),
        complete: function(data) {
            swal({
                    title: "Application Settings",
                    text: "Settings updated successfully. ",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#458B00",
                    confirmButtonText: "OK",
                    cancelButtonText: "Done",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                    }
                });
        }
    });
}

function toggleCaptchaSettings() {
    var login_captcha = $('#login_captcha').iCheck('update')[0].checked;
    if (login_captcha === true) {
        $('#login_captcha_settings').show();
    } else {
        $('#login_captcha_settings').hide();
    }

}