var table;
var table2;
$(document).ready(function () {
    create_table();
    table.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';

        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();

            result += rowData[1]+' updated to be in position '+
                diff[i].newData+' (was '+diff[i].oldData+')<br>';

            var stage_name = rowData[1];
            var stage_order = i + 1;
            $.ajax({
                type: 'POST',
                url: '/modules/settings_management/ajax/save_stage_order.ajax.php?stage_name='+stage_name+'&stage_order='+diff[i].newData,
                success: function (data) {
                }
            });
        }
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            positionClass: 'toast-bottom-right',
            timeOut: 120000
        };
        toastMessage =  'Event result:<br>'+result;
        toastr.success(toastMessage);
    } );
    create_table2();

    table2.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table2.row( diff[i].node ).data();
            var stage_name = rowData[1];
            var stage_order = i + 1;
            $.ajax({
                type: 'POST',
                url: '/modules/settings_management/ajax/save_stage_order.ajax.php?stage_name='+stage_name+'&stage_order='+stage_order,
                success: function (data) {
                }
            });
        }
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            positionClass: 'toast-bottom-right',
            timeOut: 6000
        };
        toastMessage = stage_name + ' has been moved to new position.';
        toastr.success(toastMessage);
    } );
});


$('#activeStages').click(function(){
    create_table();
});
$('#inactiveStages').click(function(){
    create_table2();
});

function create_table() {
    var active = "Y";

     table = $('#activeStagesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "rowReorder": true,
        "sAjaxSource": "/modules/settings_management/ajax/list_stages.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false, className: 'text-center'}
        ],
         "columnDefs": [
        { "visible": false, "targets": [0] }
    ],
         stateSave: true,
         stateDuration: 0
    });
    $('#activeStagesTbl').each(function() {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

    }





function create_table2() {
    var active = "N";

    table2 = $('#inactiveStagesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "rowReorder": true,
        "sAjaxSource": "/modules/settings_management/ajax/list_stages.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true, "visible": false },
            { "bSortable": true },
            { "bSortable": false, className: 'text-center'}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#inactiveStagesTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

$(document).on('click', '.deactivateBtn', function(event) {
    var deactivate_url = $(this).attr('href');
    var stage_name = $(this).data('stage');
    swal({
        title: "Deactivate: " +stage_name,
        text: "You will be able to activate '"+ stage_name +"' later under the inactive Tab",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Deactivate!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'GET',
            url: deactivate_url,
            success: function (data) {
                create_table();
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = stage_name + ' has been suspended.';
                toastr.success(toastMessage);
            }
        });
    });
    event.preventDefault();
});

$(document).on('click', '.activateBtn', function(event) {
    var activate_url = $(this).attr('href');
    var stage_name = $(this).data('stage');
    swal({
        title: "Activate: " +stage_name,
        text: "You will be able to deactivate '"+ stage_name +"' later under the active Tab",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Activate!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'GET',
            url: activate_url,
            success: function (data) {
                create_table2();
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = stage_name + ' has been activated.';
                toastr.success(toastMessage);
            }
        });
    });
    event.preventDefault();
});

//create new company button
$('#create_stage').click(function(){
    document.location = 'index.php?m=settings_management&a=edit_stage&stage_id=0';
});
