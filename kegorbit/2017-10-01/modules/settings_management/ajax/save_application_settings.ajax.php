<?php
/**
 * Save application settings
 *
 * 2016-07-19: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";

//Vars
$appClass = new Application();

//CAPCHA Setting
if (!isset($_POST['login_captcha'])) $_POST['login_captcha'] = 0;

//Update
$appClass->update_settings($_POST);



