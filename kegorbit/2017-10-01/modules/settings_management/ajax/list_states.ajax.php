<?php
/**
 * List container states for datatables
 *
 * 2016-07-06: Thilo Muller - Created
 */

//Includes
require_once '../../../settings/init.php';
require_once '../classes/state.class.php';

//Vars
$stateClass = new State();
$dData = array('aaData' => array());
$type = isset($_GET['type'])&&$_GET['type']=='inactive'?true:false;

//Get states
$states = $stateClass->list_states($type);

//Assemble for datatables
foreach ($states as $data) {
    $line = array();
    $line[] = $data['is_default']=='Y'?"<sup><i class='fa fa-asterisk' style='color: green; ' title='Default Selection'></i></sup> {$data['state_name']}":$data['state_name'];
    $line[] = $data['out_of_circulation']=='Y'?"<i class='fa fa-check-square' aria-hidden='true' style='color: green'></i>":"";

    if ($type === true) {
        $line[] = "<div class='btn-group'>
                    <button onclick='editState({$data['state_id']})' class='btn btn-primary btn-sm '><span class='btn-label'><i class='fa fa-pencil-square-o'></i></span><span class='hidden-xs'> Edit</span>  </button>
							<button class='btn btn-primary btn-sm   dropdown-toggle' data-toggle='dropdown'>
								<span class='caret'></span>
							</button>
							<ul class='dropdown-menu' >
								<li>
									<a onclick='toggle_active_state({$data['state_id']},1,\"{$data['state_name']}\")' ><i class='fa fa-ban'></i></span> Activate State</a>
								</li>
							</ul>
						</div>";
    } else {
        $line[] = "<div class='btn-group'>
                    <button onclick='editState({$data['state_id']})' class='btn btn-primary btn-sm '><span class='btn-label'><i class='fa fa-pencil-square-o'></i></span> <span class='hidden-xs'> Edit</span> </button>
							<button class='btn btn-primary btn-sm   dropdown-toggle' data-toggle='dropdown'>
								<span class='caret'></span>
							</button>
							<ul class='dropdown-menu' >
								<li>
									<a onclick='toggle_active_state({$data['state_id']},0,\"{$data['state_name']}\")' ><i class='fa fa-ban'></i></span> De-activate State</a>
								</li>
							</ul>
						</div>";
    }

    $dData['aaData'][] = $line;

}


echo json_encode($dData);

