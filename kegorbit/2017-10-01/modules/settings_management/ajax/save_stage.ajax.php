<?php

/**
 *  save/update stage
 *  2016-07-12 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();
$accountClass = new Accounts();
$companyClass = new Company();
$stage_id = $_POST['stage_id'];

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//Get user company data
$company_data = $companyClass->get_company($_SESSION['user']['company_id']);

//save Stage
if($_POST['stage_id'] == 0) {
    $stage_id = $settingsClass->save_stage($_POST);
    echo 'new';
    $accountClass->insert_user_activity("Stage: '{$_POST['stage_name']}' Added for {$company_data['company_name']}");
}
else{
    $settingsClass->update_stage($_POST);
    echo 'update';
    $accountClass->insert_user_activity("Stage: '{$_POST['stage_name']}' Updated for {$company_data['company_name']}");
}

if (isset($_POST['is_default'])) {
    $settingsClass->update_stage_default($stage_id);
}