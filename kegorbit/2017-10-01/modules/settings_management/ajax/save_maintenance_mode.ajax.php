<?php

/**
 *  save/update maintenance mode
 *  2016-09-08 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//save maintenance messages
$settingsClass->save_maintenance($_POST);

//update maintenance
if (!isset($_POST['activate_maintenance'])) $_POST['activate_maintenance'] = 0;
$settingsClass->update_settings($_POST);

