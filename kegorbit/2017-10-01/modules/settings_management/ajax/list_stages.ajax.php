<?php
/**
 * setting management - fetch a list of all companies
 * 2016-07-01- Musa Khulu - Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingClass = new Settings();

$output = array();
$output['aaData'] = '';
$active = $_GET['active'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all companies
$stages = $settingClass->get_stages($active);


if(empty($stages)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
//    $aColumns = array('stage_order', 'stage_name');

    foreach($stages as $stage) {
        $row = array();

        $row[] = $stage['stage_order'];
        $row[] = $stage['is_default'] == 'Y'?"<sup><i class='fa fa-asterisk' style='color: green; ' title='Default Selection'></i></sup> {$stage['stage_name']}":$stage['stage_name'];

        if($active == 'Y'){
            $row[] =
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=settings_management&a=edit_stage&stage_id=' . $stage['stage_id'] . '" class="btn btn-primary btn-sm "><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span><span class="hidden-xs"> Edit</span> </a>
							<button class="btn btn-primary btn-sm  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a class="deactivateBtn" href="/modules/settings_management/ajax/edit_stage_status.ajax.php?stage_id=' . $stage['stage_id'] . '&active=N" data-stage="' . $stage['stage_name'] . '"><i class="fa fa-ban"></i></span> Deactivate</a>
								</li>
							</ul>
						</div>';
        }

        else {
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=settings_management&a=edit_stage&stage_id=' . $stage['stage_id'] . '" class="btn btn-primary  btn-sm"><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span> <span class="hidden-xs"> Edit</span> </a>
							<button class="btn btn-primary  btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a class="activateBtn" href="/modules/settings_management/ajax/edit_stage_status.ajax.php?stage_id=' . $stage['stage_id'] . '&active=Y"  data-stage="' . $stage['stage_name'] . '"><i class="fa fa-check"></i></span> Activate</a>
								</li>
							</ul>
						</div>';
        }

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
