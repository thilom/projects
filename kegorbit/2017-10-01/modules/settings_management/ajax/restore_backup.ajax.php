<?php
/**
 * Restore a backup
 *
 * 2016-07-19: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/backup.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$accountClass = new Accounts();
$companyClass = new Company();
$backupClass = new Backup();
$file = "{$_SERVER['DOCUMENT_ROOT']}/data_files/restore/{$_FILES['restoreFile']['name']}";

//Move uploaded file
move_uploaded_file($_FILES['restoreFile']['tmp_name'], $file);

//Restore
$backupClass->restore_backup($file);

//Get user company data
$company_data = $companyClass->get_company($_SESSION['user']['company_id']);
$accountClass->insert_user_activity("{$company_data['company_name']}: Backup Restored");

