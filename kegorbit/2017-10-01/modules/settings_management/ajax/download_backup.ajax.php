<?php
/**
 * Force a backup file download
 *
 * 2016-08-05: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$accountClass = new Accounts();
$companyClass = new Company();
$file_name = $_GET['file'];
$file_url = "{$_SERVER['DOCUMENT_ROOT']}/data_files/backups/" . $file_name;

//Get user company data
$company_data = $companyClass->get_company($_SESSION['user']['company_id']);

$accountClass->insert_user_activity("{$company_data['company_name']}: Backup Downloaded");

header('Content-Type: application/octet-stream');
header("Content-Transfer-Encoding: Binary");
header("Content-disposition: attachment; filename=\"".$file_name."\"");
readfile($file_url);