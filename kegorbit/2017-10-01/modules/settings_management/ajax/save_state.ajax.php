<?php
/**
 * Save update a container state
 *
 * 2016-07-06: Thilo Muller - Created
 */

//Includes
require_once '../../../settings/init.php';
require_once '../classes/state.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$stateClass = new State();
$accountClass = new Accounts();
$companyClass = new Company();
$state_id = (int) $_POST['state_id'];
$created = $state_id==0?true:false;

//State_data
$state_data['state_name'] = $_POST['state_name'];
$state_data['out_of_circulation'] = isset($_POST['out_of_circulation'])?'Y':'';

//Save Data
$state_id  = $stateClass->save_state($state_id, $state_data);

//Get user company data
$company_data = $companyClass->get_company($_SESSION['user']['company_id']);

if ($created === true) {
    $accountClass->insert_user_activity("State: '{$_POST['state_name']}' Added for {$company_data['company_name']}");
} else {
    $accountClass->insert_user_activity("State: '{$_POST['state_name']}' Updated for {$company_data['company_name']}");
}

if (isset($_POST['is_default'])) {
    $stateClass->set_default_state($state_id);
}

echo $state_id;