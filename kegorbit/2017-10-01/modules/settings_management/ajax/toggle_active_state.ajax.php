<?php
/**
 * Toggle container state
 *
 * 2016-07-06: Thilo Muller - Created
 */

//Includes
require_once '../../../settings/init.php';
require_once '../classes/state.class.php';

//Vars
$stateClass = new State();
$state_id = (int) $_GET['id'];

//Toggle container state
$stateClass->toggle_state($state_id);

