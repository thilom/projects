<?php

/**
 *  save/update type
 *  2016-07-12 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//save terminology
if($_POST['type_id'] == 0) {

    $settingsClass->save_type($_POST);
    echo 'new';
}
else{
    $settingsClass->update_type($_POST);
    echo 'update';
}
