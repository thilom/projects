<?php
/**
 * Create a company DB backup
 *
 * 2016-07-19: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/backup.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$accountClass = new Accounts();
$companyClass = new Company();
$backupClass = new Backup();
$backup_tables = array('keg_{code}_customers', 'keg_{code}_container_history', 'keg_{code}_container_notes',
                        'keg_{code}_containers', 'keg_{code}_module_permissions', 'keg_{code}_registered_devices',
                        'keg_{code}_stages', 'keg_{code}_states', 'keg_{code}_terminology', 'keg_{code}_types',
                        'keg_{code}_user_access', 'keg_{code}_user_role_permissions', 'keg_{code}_user_roles',
                        'keg_{code}_container_types', 'keg_{code}_user_role_access');

//Get user company data
$company_data = $companyClass->get_company($_SESSION['user']['company_id']);
$accountClass->insert_user_activity("{$company_data['company_name']}: Backup Created");

//Run backup
$backup_file = $backupClass->create_backup($backup_tables);

echo "$backup_file";