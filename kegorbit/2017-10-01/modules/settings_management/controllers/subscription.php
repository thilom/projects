<?php
/**
 * About Keg Orbit
 *
 * 2016-07-18: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/customer_management/classes/customer.class.php';

//Vars
$accountClass = new Accounts();
$companyClass = new Company();
$customerClass = new Customer();
$about_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/subscription.html");
$history_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/version_history.html");
$versions = array();
$version_data = '';


//get predefined terminology  stage the user specified
$stage_plural =$settingsClass->get_stage_terminology('plural');
$stage_singular =$settingsClass->get_stage_terminology();
//get predefined terminology  container the user specified
$container_singular =$settingsClass->get_container_terminology();
$container_plural =$settingsClass->get_container_terminology('plural ');
//get predefined terminology  state the user specified
$state_plural =$settingsClass->get_state_terminology('plural');
//get predefined terminology  customer the user specified
$customer_plural=$settingsClass->get_customer_terminology('plural');

//get company data
if(isset($_SESSION['user']['run_as'])){
    $company_data = $companyClass->get_company_by_code($_SESSION['user']['run_as']);
}
else{
    $company_data = $companyClass->get_company($_SESSION['user']['company_id']);
}

//Get version history for sorting
$dir = new DirectoryIterator("{$_SERVER['DOCUMENT_ROOT']}/assets/versions/");
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
        $versions[] = $fileinfo->getFilename();
    }
}
usort($versions, 'version_compare');
$versions = array_reverse($versions);

//Get version data
foreach ($versions as $data) {
    $file_data = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/assets/versions/$data");
    $id = str_replace(array(' ','.'), '', trim($data, '.txt'));

    $entry = str_replace('<!-- version -->', trim($data, '.txt'), $history_template);
    $entry = str_replace('<!-- id -->', $id, $entry);

    $file_data = str_replace('-', "</li><li>", $file_data);
    $entry = str_replace('<!-- version_content -->', $file_data, $entry);

    $version_data .= $entry;
}

$customer_data = $companyClass->get_companies_stats($company_data['company_id']);


$about_template = str_replace('<!-- version_history -->', $version_data, $about_template);
$about_template = str_replace('<!-- current_version -->', trim($versions[0], '.txt'), $about_template);

$about_template = str_replace('<!-- active -->', $accountClass->active_user_count_company('Y', $company_data['company_id']), $about_template);
$about_template = str_replace('<!-- inactive -->', $accountClass->active_user_count_company('N',$company_data['company_id']), $about_template);

$about_template = str_replace('<!-- container_limit -->', $customer_data['company_keg_limit'], $about_template);
$about_template = str_replace('<!-- container_count -->', $customer_data['container_total'], $about_template);
$usage_container = ($customer_data['container_total']/$customer_data['company_keg_limit'])*100;
$about_template = str_replace('<!-- container_percentage -->',(int)$usage_container , $about_template);
$about_template = str_replace('<!-- more_containers -->', $customer_data['company_keg_limit'] - $customer_data['container_total'] , $about_template);

$about_template = str_replace('<!-- device_limit -->', $customer_data['company_device_limit'], $about_template);
$about_template = str_replace('<!-- device_count -->', $customer_data['device_total'], $about_template);
$usage_device = ($customer_data['device_total']/$customer_data['company_device_limit'])*100;
$about_template = str_replace('<!-- device_percentage -->',(int)$usage_device , $about_template);
$about_template = str_replace('<!-- more_device -->', $customer_data['company_device_limit'] - $customer_data['device_total'] , $about_template);

$about_template = str_replace('<!-- user_limit -->', $customer_data['company_user_limit'], $about_template);
$about_template = str_replace('<!-- user_count -->', $customer_data['user_total'], $about_template);
$usage_user = ($customer_data['user_total']/$customer_data['company_user_limit'])*100;
$about_template = str_replace('<!-- user_percentage -->',(int)$usage_user , $about_template);
$about_template = str_replace('<!-- more_users -->', $customer_data['company_user_limit'] - $customer_data['user_total'] , $about_template);


$about_template = str_replace('<!-- day_count -->', $customer_data['company_renewal_date'], $about_template);
$usage_days  = ($customer_data['user_total']/$customer_data['company_user_limit'])*100;
$date1 = new DateTime($customer_data['creation_date']);
$date2 = new DateTime($customer_data['renewal_date']);
$diff = $date2->diff($date1)->format("%a");

$about_template = str_replace('<!-- day_percentage -->',$customer_data['company_renewal_date'], $about_template);
$about_template = str_replace('<!-- difference -->', $diff , $about_template);
$about_template = str_replace('<!-- day_limit -->', $diff - $customer_data['company_renewal_date'], $about_template);


$about_template = str_replace('<!-- termination -->', $company_data['renewal_date'], $about_template);
$about_template = str_replace('<!-- initiation -->', $company_data['creation_date'], $about_template);
$about_template = str_replace('<!-- company_name -->', $company_data['company_name'], $about_template);
$about_template = str_replace('<!-- container_singular -->', $container_singular, $about_template);
$about_template = str_replace('<!-- customer_plural -->', $customer_plural, $about_template);
$about_template = str_replace('<!-- customer_count-->', $customerClass->get_customer_count(true), $about_template);
echo $about_template;