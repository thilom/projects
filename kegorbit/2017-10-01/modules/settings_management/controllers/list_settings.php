<?php
/**
 * settings management to list all settings
 * 2016-06-14 -  Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;
//get container terminology
$container_name = $settingsClass->get_container_terminology();
//get stage terminology
$stages_name = $settingsClass->get_stage_terminology('plural');
//get state terminology
$states_name = $settingsClass->get_state_terminology('plural');

//check if user has permission to view company button
if(isset($_SESSION['user']['run_as'])){
    $access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'settings_management', 'company_settings_button');
}
else{
    $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'settings_management', 'company_settings_button');
}



if(!$access){
    $company_button = '';
    $buttons_class = 'col-xs-offset-0';
}
else{
    $company_button = '<button class="btn btn-warning  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="company"><i class="fa fa-building  fa-2x" ></i><br><h2>Company</h2></button>';
    $buttons_class = 'col-xs-offset-1';
}
//check if user has permission to view application button
if(isset($_SESSION['user']['run_as'])){
    $access_application = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'settings_management', 'application_settings_button');
}
else{
    $access_application = $securityClass->check_user_permission($_SESSION['user']['id'], 'settings_management', 'application_settings_button');
}
if(!$access_application){
    $application_button = '';
}
else{
    $application_button = '<button class="btn btn-danger  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="application"><i class="fa fa-wrench fa-2x" ></i><br><h2>Application</h2></button>';
}

//check if user has permission to view Backup button
if(isset($_SESSION['user']['run_as'])){
    $access_backup = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'settings_management', 'backup_settings_button');
}
else{
    $access_backup = $securityClass->check_user_permission($_SESSION['user']['id'], 'settings_management', 'backup_settings_button');
}
if(!$access_backup){
    $backup_button = '';
}
else{
    $backup_button = '<button class="btn btn-info  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="backup"><i class="fa fa-database fa-2x" ></i><br><h2>Backup</h2></button>';
}

//check if user has permission to view about application button
if(isset($_SESSION['user']['run_as'])){
    $about_application = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'settings_management', 'about_application_button');
}
else{
    $about_application = $securityClass->check_user_permission($_SESSION['user']['id'], 'settings_management', 'about_application_button');
}
if(!$about_application){
    $about_button = '';
}
else{
    $about_button = '<button class="btn btn-success  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="about"><i class="fa fa-file fa-2x" ></i><br><h2>About</h2></button>';
}


//check if user has permission to view subscription details button
if(isset($_SESSION['user']['run_as'])){
    $subscription_application = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'settings_management', 'subscription_button');
}
else{
    $subscription_application = $securityClass->check_user_permission($_SESSION['user']['id'], 'settings_management', 'subscription_button');
}
if(!$subscription_application){
    $subscription_button = '';
}
else{
    $subscription_button = '<button class="btn btn-success  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="subscription"><i class="fa fa-file fa-2x" ></i><br><h2>Subscription</h2></button>';
}
if(isset($_SESSION['user']['run_as_id'])){
    $maintenance_button = '';
}
else{
    if($_SESSION['user']['prefix'] == 'default'){
        $maintenance_button = '
                        <button class="btn btn-info dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="maintenance"><i class="fa fa-cogs fa-2x" ></i><br><h2>Schedule<br>Maintenance</h2></button>
                    ';
    }
    else{
        $maintenance_button = '';
    }
}



$template_data = array(
    'container'=> $container_name,
    'stages'=> $stages_name,
    'states'=> $states_name,
    'buttons_class'=> $buttons_class,
    'company'=> $company_button,
    'application'=> $application_button,
    'backup_button' => $backup_button,
    'about' =>$about_button,
    'subscription_details' => $subscription_button,
    'maintenance_button' =>$maintenance_button
);

$module_name = $container_name;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/list_settings.html");
$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
