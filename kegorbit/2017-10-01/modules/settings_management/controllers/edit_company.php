<?php

/**
 * Call the edit stage page
 * 2016-06-20 - Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/classes/account.class.php";

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();
$companyClass = new Company();
$accountClass = new Accounts();

$type_id = isset($_GET['type_id'])? $_GET['type_id'] :0;

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/edit_company.html");


    //get selected stage
    if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
        $account_data = $accountClass->get_account($_SESSION['user']['run_as_id']);
        $company_details = $companyClass->get_company($account_data['company_id']);
    } else {
        $company_details = $companyClass->get_company($_SESSION['user']['company_id']);
    }

//get the date format specified by the company
$date_formats = array(array("option" =>"Y-m-d","value" => date("Y-m-d") . ' (year-month-day)'),
    array("option" =>"d-m-Y", "value" => date("d-m-Y") . ' (day-month-year)'),
    array("option" =>"m-d-Y", "value" => date("m-d-Y") . ' (month-day-year)'),
    array("option" =>"Y/m/d", "value" =>  date("Y/m/d") . ' (year/month/day)'),
    array("option" =>"d/m/Y", "value" =>  date("d/m/Y") . ' (day/month/year)'),
    array("option" =>"m/d/Y", "value" =>  date("m/d/Y") . ' (month/day/year)'));
$date_lists = '<select name="app_date" class="form-control" id="app_date">';

foreach($date_formats as $date_format) {
    if ($date_format["option"] == $company_details['application_date']) {
        $date_lists .= '<option value="' . $date_format["option"] . '" selected>' . $date_format["value"]  . '</option>';
    } else {
        $date_lists .= '<option value="' . $date_format["option"]. '" >' . $date_format["value"] . '</option>';
    }
}
$date_lists .= '</select>';

//check if image exists
$filename = '';
if( $company_details['company_image'] == ''){
    $filename .= '/uploads/users/images/company_name.jpg';
} else if ($company_details['company_image'] != ''){
    $filename .= "/uploads/users/images/{$company_details['company_image']}";
} else {
    $filename .= '/uploads/users/images/company_name.jpg';
}

    $template_data = array(
        'company_id'       =>  $company_details['company_id'],
        'company_name'       =>  $company_details['company_name'],
        'date'       =>  $date = date('Y-m-d'),
        'creation_date' => '<label> Last Update: </label>' .' '. $company_details['company_last_update'],
        'renewal'     =>  $company_details['company_renewal_date'].' Day(s)' ,
        'company_names'      =>  $company_details['company_name'],
        'company_contact_name'      =>  $company_details['company_contact_name'],
        'company_contact_number'      =>  $company_details['company_contact_number'],
        'company_email'         =>  $company_details['company_email'],
        'delivery_street_name'   => $company_details['delivery_street_name'],
        'delivery_suburb'   => $company_details['delivery_suburb'],
        'delivery_town'   => $company_details['delivery_town'],
        'delivery_physical_code'   => $company_details['delivery_physical_code'],
        'invoice_street_name'   => $company_details['invoice_street_name'],
        'invoice_suburb'   => $company_details['invoice_suburb'],
        'invoice_town'   => $company_details['invoice_town'],
        'invoice_physical_code'   => $company_details['invoice_physical_code'],
        'near_date_range'   => $company_details['near_date_range'],
        'company_logo' =>  $filename,
        'same_as_physical' => $company_details['same_as_physical']==1?'checked':'',
        'import_checked' => $company_details['allow_imports']==1?'checked':'',
        'import_val' => $company_details['allow_imports'],
        'application_date' => $date_lists,
        'sender_email' => $company_details['sender_email'],
        'reply_email' => $company_details['reply_email'],
    );

$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
