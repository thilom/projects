<?php

/**
 * Call the edit stage page
 * 2016-06-20 - Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();

$type_id = isset($_GET['type_id'])? $_GET['type_id'] :0;

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/edit_type.html");

//get predefined terminology either container or keg whatever the user specified
$singular = $settingsClass->get_container_terminology();
$plural = $settingsClass->get_container_terminology('plural');
//get predefined terminology for stage
$singular_stage = $settingsClass->get_stage_terminology();
$plural_stage = $settingsClass->get_stage_terminology('plural');



if($type_id != 0){
    //get selected stage
    $type_data = $settingsClass->get_container_type($type_id);

    $template_data = array(
        'type_id'       =>  $type_id,
        'type_names'       =>  $type_data[0]['container_type'],
        'type_name'       =>  $type_data[0]['container_type'],
        'type_capacity'       =>  $type_data[0]['container_capacity'],
        'type'       =>  'Edit',
        'container_singular' => $singular,
        'container_plural' => $plural,
    );
}
else{

    $template_data = array(
        'type_id'       =>  $type_id,
        'type_names'       =>  'New '.$singular.' Type',
        'type_name'       =>  '',
        'type_capacity'       =>  '',
        'type'       =>  'Create', 'container_singular' => $singular,
        'container_plural' => $plural,
    );
}

$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
