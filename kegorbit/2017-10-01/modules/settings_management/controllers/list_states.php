<?php
/**
 * List container states for managing
 *
 * 2016-07-06: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/state.class.php";

//Vars
$stateClass = new State();
$state_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/list_states.html");

//get predefined terminology either container or keg whatever the user specified
$singular = $settingsClass->get_container_terminology();
$plural = $settingsClass->get_container_terminology('plural');

//get predefined terminology for state
$singular_state = $settingsClass->get_state_terminology();
$plural_state = $settingsClass->get_state_terminology('plural');

$template_data = array(
    'container_singular' => $singular,
    'container_plural' => $plural,
    'singular_state' => $singular_state,
    'plural_state' => $plural_state,
);
$template1 = $systemClass->merge_data($state_template, $template_data);

echo $template1;


