<?php

/**
 * Call the edit stage page
 * 2016-06-20 - Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();
$onselect_entities = array(0 => "Do Nothing",
                            1 => "Request Product",
                            2 => "Request Client",
                            3 => "Clear Attached Product",
                            4 => "Clear Attached Client",
                            5 => "Clear Both Product and Client",
);
$stage_id = isset($_GET['stage_id'])? $_GET['stage_id'] :0;

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/edit_stage.html");

if($stage_id != 0){
    //get selected stage
    $stage_data = $settingsClass->get_stage($stage_id);

    //Setup On Select dropdown
    $onselect = "";
    foreach ($onselect_entities as $key=>$data) {
        if ($key == $stage_data[0]['onselect']) {
            $onselect .= "<option value='$key' selected>{$data}</option>";
        } else {
            $onselect .= "<option value='$key'>{$data}</option>";
        }
    }

    //Is Default
    $is_default = $stage_data[0]['is_default'] == 'Y'?'checked':'';

    $template_data = array(
        'stage_id'       =>  $stage_id,
        'stage_name'       =>  $stage_data[0]['stage_name'],
        'stage_name_header' => $stage_data[0]['stage_name'],
        'stage_description'       =>  $stage_data[0]['stage_description'],
        'stage_type'       =>  'Edit',
        'onselect'       =>  $onselect,
        'is_default' => $is_default
    );
} else {

    //Setup On Select dropdown
    $onselect = "";
    foreach ($onselect_entities as $key=>$data) {
        $onselect .= "<option value='$key'>{$data}</option>";
    }

    $template_data = array(
        'stage_id'       =>  $stage_id,
        'stage_name'       =>  '',
        'stage_name_header' => 'New Stage',
        'stage_description'       =>  '',
        'stage_type'       =>  'Create',
        'onselect'       =>  $onselect,
        'is_default' => ''
    );
}

$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
