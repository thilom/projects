<?php
/**
 * Application Settings
 *
 * 2016-07-19: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";

//Vars
$appClass = new Application();
$systemClass = new System();
$app_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/application.html");

//Get current settings
$settings = $appClass->get_settings();

//Set a default grace period
if (!isset($settings['grace_period'])) $settings['grace_period'] = 0;

//Login CAPTCHA
if (isset($settings['login_captcha']) && $settings['login_captcha'] == '1') {
    $settings['login_captcha'] = 'checked';
} else {
    $settings['login_captcha'] = '';
}


$app_template = $systemClass->merge_data($app_template, $settings);

echo $app_template;