<?php

/**
 * Call the edit stage page
 * 2016-06-20 - Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();
$companyClass = new Company();
$accountClass = new Accounts();
$appClass = new Application();


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/schedule_maintenance.html");

//Get current settings
$settings = $appClass->get_settings();

//get maintenance details
$maintenance = $settingsClass->get_maintenance();


$frequencys = array(array("frequency_id" => 1, "frequency_name" => "Once Upon Logging in "),
        array("frequency_id" => 2, "frequency_name" => "Regularly After 5 min"),
        array("frequency_id" => 3, "frequency_name" => "Regularly After 10 min "),
            array("frequency_id" => 4, "frequency_name" => "Regularly After 15 min"));

$frequency_lists = '<select name="popup_frequency" class="form-control pop" id="popup_frequency"><option value="">Choose Popup Frequency</option>';

foreach($frequencys as $frequency) {
    if ($frequency['frequency_id'] == $maintenance['popup_frequency']) {
        $frequency_lists .= '<option value="' . $frequency['frequency_id'] . '" selected>' . $frequency['frequency_name']  . '</option>';
    } else {
        $frequency_lists .= '<option value="' . $frequency['frequency_id'] . '" >' . $frequency['frequency_name']  . '</option>';
    }
}
$frequency_lists .= '</select>';

$settings['message'] = isset($maintenance['popup_message'])? $maintenance['popup_message'] :'kegorbit.com will be undergoing a schedule maintence';
$settings['start'] = isset($maintenance['maintenance_commence'])? $maintenance['maintenance_commence'] :date('Y-m-d');
$settings['end'] = isset($maintenance['maintenance_end'])? $maintenance['maintenance_end'] :date('Y-m-d');
$settings['color'] = isset($maintenance['popup_color'])? $maintenance['popup_color'] :'#C0C0C0';
$settings['popup_frequency'] = $frequency_lists;
$settings['count_down_message'] = isset($maintenance['countdown_message'])? $maintenance['countdown_message'] :'Coming Back Soon kegorbit.com is currently down for maintence';
$settings['count_down_color'] = isset($maintenance['countdown_message_color'])? $maintenance['countdown_message_color'] :'#5367ce';
//activate maintenance
if (isset($settings['activate_maintenance']) && $settings['activate_maintenance'] == '1') {
    $settings['maintenance'] = 'checked';
} else {
    $settings['maintenance'] = '';
}
//show count down timer
if (isset($maintenance['maintenance_timer']) && $maintenance['maintenance_timer'] == '1') {
    $settings['countdown'] = 'checked';
} else {
    $settings['countdown'] = '';
}


$template1 = $systemClass->merge_data($template2, $settings);
echo $template1;
