<?php
/**
 * About Keg Orbit
 *
 * 2016-07-18: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$accountClass = new Accounts();
$companyClass = new Company();
$about_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/about.html");
$history_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/version_history.html");
$versions = array();
$version_data = '';

//Get version history for sorting
$dir = new DirectoryIterator("{$_SERVER['DOCUMENT_ROOT']}/assets/versions/");
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
        $versions[] = $fileinfo->getFilename();
    }
}
usort($versions, 'version_compare');
$versions = array_reverse($versions);

//Get version data
foreach ($versions as $data) {
    $file_data = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/assets/versions/$data");
    $id = str_replace(array(' ','.'), '', trim($data, '.txt'));

    $entry = str_replace('<!-- version -->', trim($data, '.txt'), $history_template);
    $entry = str_replace('<!-- id -->', $id, $entry);

    $file_data = str_replace('-', "</li><li>", $file_data);
    $entry = str_replace('<!-- version_content -->', $file_data, $entry);

    $version_data .= $entry;
}

$customer_data = $companyClass->get_companies_report();

$over_user = 0;
$over_container = 0;
$over_device= 0;
foreach ($customer_data as $data) {

    //Container Limit
    $keg_diff = $data['company_keg_limit'] - $data['container_count'];
    if ($keg_diff < 0) {
        $over_container++;
    }

    //User Limit
    $user_diff = $data['company_user_limit'] - $data['user_count'];
    if ($user_diff < 0) {
        $over_user++;
    }

    //Device Limit
    $device_diff = $data['company_device_limit'] - $data['device_count'];
    if ($device_diff < 0) {
        $over_device++;
    }

}

$about_template = str_replace('<!-- version_history -->', $version_data, $about_template);
$about_template = str_replace('<!-- current_version -->', trim($versions[0], '.txt'), $about_template);
$about_template = str_replace('<!-- active -->', $accountClass->active_user_count('Y'), $about_template);
$about_template = str_replace('<!-- inactive -->', $accountClass->active_user_count('N'), $about_template);
$about_template = str_replace('<!-- total_company -->', $companyClass->active_company_count('0'), $about_template);
$about_template = str_replace('<!-- active_company -->', $companyClass->active_company_count('Y'), $about_template);
$about_template = str_replace('<!-- inactive_company -->', $companyClass->active_company_count('N'), $about_template);
$about_template = str_replace('<!-- over_user -->', $over_user, $about_template);
$about_template = str_replace('<!-- over_container -->', $over_container, $about_template);
$about_template = str_replace('<!-- over_device -->', $over_device, $about_template);
$about_template = str_replace('<!-- admin -->', $accountClass->account_user_type(1), $about_template);
$about_template = str_replace('<!-- company_admin -->', $accountClass->account_user_type(3), $about_template);
$about_template = str_replace('<!-- employees -->', $accountClass->account_user_type(4), $about_template);
echo $about_template;