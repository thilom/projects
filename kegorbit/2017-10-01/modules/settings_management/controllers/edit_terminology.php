<?php
/**
 * settings management to list all settings
 * 2016-06-30-  Musa Khulu- Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();


$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/edit_terminology.html");


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$company_terminology = $settingsClass->get_company_terminology();

$template_data = array(
    'company_id'=> $_SESSION['user']['company_id'],
    'container_singular'=>  $company_terminology['container_singular'],
    'container_plural'       =>  $company_terminology['container_plural'],
    'stage_singular' => $company_terminology['stage_singular'],
    'stage_plural'       =>  $company_terminology['stage_plural'],
    'state_singular' => $company_terminology['state_singular'],
    'state_plural'   => $company_terminology['state_plural'],
    'client_singular'   => $company_terminology['client_singular'],
    'client_plural'   => $company_terminology['client_plural'],
);

$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;