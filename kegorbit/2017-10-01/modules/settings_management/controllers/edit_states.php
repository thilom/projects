<?php
/**
 * Edit / Add a container state
 *
 * 2016-07-06: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/state.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";

//Vars
$stateClass = new State();
$systemClass = new System();
$state_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/html/edit_state.html");
$state_id = (int) $_GET['id'];
$state_data = array();

//Get container state info
if ($state_id != 0) {
    $state_data = $stateClass->get_state($state_id);
    $state_data['state_id'] = $state_id;
    $state_data['ooc_checked'] = $state_data['out_of_circulation']=='Y'?'checked':'';
    $state_data['state_task'] = 'Edit';
    $state_data['is_default'] = $state_data['is_default']=='Y'?'checked':'';
} else {
    $state_data['state_id'] = 0;
    $state_data['state_name'] = '';
    $state_data['ooc_checked'] = '';
    $state_data['state_task'] = 'Create';
    $state_data['is_default'] = '';
}

//get predefined terminology either container or keg whatever the user specified
$singular = $settingsClass->get_container_terminology();
$plural = $settingsClass->get_container_terminology('plural');
$state_data['container_singular'] = $singular;
$state_data['container_plural'] = $plural;


$state_template = $systemClass->merge_data($state_template, $state_data);

echo $state_template;