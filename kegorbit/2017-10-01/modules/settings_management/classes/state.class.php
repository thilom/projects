<?php

/**
 * Manage container state information
 *
 * 2016-07-06: Thilo Muller - Created
 */

class State {

    /**
     * Company code
     *
     * @var string
     */
    private $company_code = 'default';

    /**
     * State constructor.
     */
    function __construct() {
        if(isset($_SESSION['user']['prefix'])){
            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }
    }

    /**
     * Return a list of all active cantainer states
     *
     * @param bool $inactive
     * @return mixed
     */
    function list_states($inactive = false) {
        $statement = "SELECT state_id, state_name, out_of_circulation, is_default
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_states";
        if ($inactive === false) {
            $statement .= " WHERE inactive IS NULL OR inactive = ''";
        } else {
            $statement .= " WHERE inactive = 'Y' ";
        }
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }


    /**
     * Get containor state info
     *
     * @param $state_id
     * @return mixed
     */
    function get_state($state_id) {
        $statement = "SELECT state_name, out_of_circulation, is_default
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_states
                        WHERE state_id = :state_id
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':state_id', $state_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Get default container state
     *
     * @param $state_id
     * @return mixed
     */
    function get_default_state() {
        $statement = "SELECT state_id, state_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_states
                        WHERE is_default = 'Y'
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Save/update state info
     *
     * @param $state_id
     * @param $state_data
     * @return mixed
     */
    function save_state($state_id, $state_data) {

        if ($state_id == 0) {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_states
                              (state_name, out_of_circulation)
                            VALUES
                              (:state_name, :out_of_circulation)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bindParam(':state_name', $state_data['state_name']);
            $sql_insert->bindParam(':out_of_circulation', $state_data['out_of_circulation']);
            $sql_insert->execute();
            $sql_insert->closeCursor();

            $state_id = $GLOBALS['dbCon']->lastinsertid();

        } else {
            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_states
                            SET state_name = :state_name,
                                out_of_circulation = :out_of_circulation
                            WHERE state_id = :state_id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':state_id', $state_id);
            $sql_update->bindParam(':state_name', $state_data['state_name']);
            $sql_update->bindParam(':out_of_circulation', $state_data['out_of_circulation']);
            $sql_update->execute();
            $sql_update->closeCursor();
        }

        return $state_id;
    }


    function toggle_state($state_id) {
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_states
                        SET inactive = IF(inactive='Y','','Y')
                        WHERE state_id = :state_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':state_id', $state_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    function set_default_state($state_id) {

        //Set all states to 'N'
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_states
                            SET is_default = 'N'";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->execute();
        $sql_update->closeCursor();

        //Set selected state to 'Y'
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_states
                        SET is_default = 'Y'
                        WHERE state_id = :state_id
                        LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':state_id', $state_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }


    /**
     * Check the DB for an existing state. Return false if it doesn't elso return it's ID.
     *
     * @param $state_name
     * @return bool
     */
    function state_exists($state_name) {
        $statement = "SELECT state_id
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_states
                        WHERE state_name = :state_name
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':state_name', $state_name);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        if ($sql_result === false || count($sql_result) == 0) {
            return false;
        } else {
            return $sql_result['state_id'];
        }
    }

}