<?php

/**
 * KegOrbit backup functions
 *
 * 2016-07-19: Thilo Muller - Created
 */


class Backup {

    /**
     * Company code for logged in user
     *
     * @var string
     */
    private $company_code = 'default';

    /**
     * Backup constructor.
     */
    function __construct() {
        if(isset($_SESSION['user'])){
            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }
    }


    function create_backup($tables = array()) {

        //Vars
        $file_name = "{$this->company_code}_" . date('Ymdhis') . ".sql";

        //Open backup file for writing
        $handle = fopen("{$_SERVER['DOCUMENT_ROOT']}/data_files/backups/$file_name",'w+');

        //cycle through tables
        foreach($tables as $table) {

            //Vars
            $table = str_replace('{code}', $this->company_code, $table);
            $sql_statement = '';

            //Get table Data
            $statement = "SELECT * FROM $table";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();

            //Write SQL statement
            $sql_statement= 'DROP TABLE '.$table.';';
            fwrite($handle,$sql_statement);

            //Get create table SQL
            $statement = "SHOW CREATE TABLE $table";
            $sql_show = $GLOBALS['dbCon']->prepare($statement);
            $sql_show->execute();
            $sql_create_result = $sql_show->fetch(PDO::FETCH_ASSOC);
            $sql_show->closeCursor();
            $sql_statement= "\n\n".$sql_create_result['Create Table'].";\n\n";
            fwrite($handle,$sql_statement);

            //Add insert statements
            foreach ($sql_result as $data) {
                $sql_statement= 'INSERT INTO '.$table.' VALUES(';
                $field_count = count($data);
                $c = 1;
                foreach ($data as $field) {
                    $field = addslashes($field);
                    $field = str_replace("\n","\\n",$field);
                    if (isset($field)) { $sql_statement.= '"'.$field.'"' ; } else { $sql_statement.= '""'; }
                    if ($c < $field_count) { $sql_statement.= ','; }
                    $c++;
                }
                $sql_statement.= ");\n\n";
                fwrite($handle,$sql_statement);
            }
            $sql_statement ="\n\n\n";
            fwrite($handle,$sql_statement);

        }

        fclose($handle);
        return $file_name;
    }

    function restore_backup($file) {

        //Vars
        $statement = '';

        //Open file for reading
        $handle = fopen($file, 'r');

        //Iterate through file
        while (($line = fgets($handle, 4096)) !== false) {
            $statement .= $line;

            if ($line == PHP_EOL) {

                //Run statement
                try {
                    $statement = trim($statement);
                    $sql_run = $GLOBALS['dbCon']->prepare($statement);
                    $sql_run->execute();
                    $sql_run->closeCursor();
                } catch (PDOException $e) {
                    //Do nothing. This is just to catch cases where the table has been deleted and can't be dropped.
                }

                $statement = '';
            }
        }


    }

}