<?php
/**
 * Settings Class
 *
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";

class Settings extends System
{
    /**
     * Company code
     *
     * @var string
     */
    private $company_code = 'default';

    /**
     * State constructor.
     */
    function __construct() {
        if(isset($_SESSION['user']['prefix'])){
            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }
    }


    /*
   * Get company terminology
   * @param $company_id (int)
   */
    function get_company_terminology() {

        $statement = "SELECT container_singular,container_plural,stage_singular,stage_plural,state_singular,state_plural,client_singular,client_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $sql_company_data = $sql->fetch();
        $sql->closeCursor();
        return $sql_company_data;
    }

    /*
    * Save/Update terminology
    * @param $terminology_data (array)
    */
    function save_terminology($terminology_data){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_terminology
						SET	container_singular =:container_singular,
						    container_plural = :container_plural,
						    stage_singular = :stage_singular,
						    stage_plural = :stage_plural,
						    state_singular = :state_singular,
						    state_plural = :state_plural,
						    client_singular = :client_singular,
						    client_plural = :client_plural";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_singular', $terminology_data['container_singular']);
        $sql->bindParam(':container_plural', $terminology_data['container_plural']);
        $sql->bindParam(':stage_singular', $terminology_data['stage_singular']);
        $sql->bindParam(':stage_plural', $terminology_data['stage_plural']);
        $sql->bindParam(':state_singular', $terminology_data['state_singular']);
        $sql->bindParam(':state_plural', $terminology_data['state_plural']);
        $sql->bindParam(':client_singular', $terminology_data['client_singular']);
        $sql->bindParam(':client_plural', $terminology_data['client_plural']);
        $sql->execute();
        $sql->closeCursor();
    }

    /*
  * Get container terminology
  * @param $company_id (int)
  */
    function get_container_terminology( $value='singular') {

        if($value== 'singular'){
            $statement = "SELECT container_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        else{
            $statement = "SELECT container_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }
    /*
 * Get state terminology
 * @param $company_id (int)
 */
    function get_state_terminology( $value='singular') {

        if($value== 'singular'){
            $statement = "SELECT state_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        else{
            $statement = "SELECT state_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }
    /*
    * Get stage terminology
    * @param $company_id (int)
    */
    function get_stage_terminology( $value='singular') {

        if($value== 'singular'){
            $statement = "SELECT stage_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        else{
            $statement = "SELECT stage_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }

    /*
   * Get customer terminology
   * @param $company_id (int)
   */
    function get_customer_terminology($value='singular') {

        if($value== 'singular'){
            $statement = "SELECT client_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        else{
            $statement = "SELECT client_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }


    /*
   * Get stages
   * @param active (string)
   */
    function get_stages($active = '') {

        $statement = "SELECT stage_id, stage_name, active, stage_order, onselect, is_default
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_stages";
        if ($active == 'N')
            $statement .= " WHERE active = 'N' ORDER BY stage_order";
        else
            $statement .= " WHERE active = 'Y' OR active IS NULL ORDER BY stage_order";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();

        $stages= $sql->fetchAll();
        $sql->closeCursor();
        return $stages;
    }

    /**
      * Get default stage
      */
    function get_default_stage() {

        $statement = "SELECT stage_id, stage_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                        WHERE is_default = 'Y'";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $sql_result= $sql->fetch();
        $sql->closeCursor();

        return $sql_result;
    }

    /**
     * Activate/ Deactivate stage
     * @param $stage_id, $active
     * @return array
     */
    function change_stage_status($stage_id, $active){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                        SET active = :active
                        WHERE stage_id = :stage_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':active', $active);
        $sql_update->bindParam(':stage_id', $stage_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /**
     * Save/update stage info
     *
     * @param $stage_data
     * @return mixed
     */
    function save_stage($stage_data) {
        
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                              (stage_name, stage_description, onselect)
                            VALUES
                              (:stage_name, :stage_description, :onselect)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bindParam(':stage_name', $stage_data['stage_name']);
            $sql_insert->bindParam(':stage_description', $stage_data['stage_description']);
            $sql_insert->bindParam(':onselect', $stage_data['onselect']);
            $sql_insert->execute();
            $sql_insert->closeCursor();

            $stage_id = $GLOBALS['dbCon']->lastinsertid();
            return $stage_id;
    }

    /**
     * Save/update stage info
     *
     * @param $stage_data
     * @return mixed
     */
    function update_stage($stage_data) {

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                            SET stage_name = :stage_name,
                                stage_description = :stage_description,
                                onselect = :onselect
                            WHERE stage_id = :stage_id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':stage_id', $stage_data['stage_id']);
            $sql_update->bindParam(':stage_name', $stage_data['stage_name']);
            $sql_update->bindParam(':stage_description', $stage_data['stage_description']);
            $sql_update->bindParam(':onselect', $stage_data['onselect']);
            $sql_update->execute();
            $sql_update->closeCursor();
    }

    function update_stage_default($default_id) {

        //Set all to 'N'
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                        SET is_default='N'";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->execute();
        $sql_update->closeCursor();

        //Set selected stage to 'Y'
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                        SET is_default = 'Y'
                        WHERE stage_id = :stage_id
                        LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':stage_id', $default_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /**
     * Save/update stage order
     *
     * @param $stage_data
     * @return mixed
     */
    function update_stage_order($stage_name, $stage_order) {

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                            SET stage_order = :stage_order
                            WHERE stage_name = :stage_name
                            LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':stage_name', $stage_name);
        $sql_update->bindParam(':stage_order', $stage_order);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /*
  * Get stage
  * @param stage_id (int)
  */
    function get_stage($stage_id){

        $statement = "SELECT stage_id, stage_name, active, stage_description, onselect, is_default
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                        WHERE stage_id = :stage_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':stage_id', $stage_id);
        $sql->execute();
        $stages= $sql->fetchAll();
        $sql->closeCursor();

        return $stages;
    }
    /*
   * Get types
   * @param active (string)
   */
    function get_types($active = ''){

        $statement = "SELECT type_id, type_name,type_description,  active
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_types";
        if ($active == 'N')
            $statement .= " WHERE active = 'N'";
        else
            $statement .= " WHERE active = 'Y' OR active IS NULL";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();

        $types= $sql->fetchAll();
        $sql->closeCursor();
        return $types;
    }

    /**
     * Activate/ Deactivate type
     * @param $type_id, $active
     * @return array
     */
    function change_type_status($type_id, $active){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_container_types
                        SET active = :active
                        WHERE type_id = :type_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':active', $active);
        $sql_update->bindParam(':type_id', $type_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /**
     * Save/update type info
     *
     * @param $type_data
     * @return mixed
     */
    function save_type($type_data) {

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_container_types
                              (container_type, container_capacity)
                            VALUES
                              (:container_type, :container_capacity)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bindParam(':container_type', $type_data['type_name']);
        $sql_insert->bindParam(':container_capacity', $type_data['type_capacity']);
        $sql_insert->execute();
        $sql_insert->closeCursor();

        $type_id = $GLOBALS['dbCon']->lastinsertid();
        return $type_id;
    }

    /**
     * Save/update stage info
     *
     * @param $type_data
     * @return mixed
     */
    function update_type($type_data) {

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_container_types
                            SET container_type = :container_type,
                                container_capacity = :container_capacity
                            WHERE type_id = :type_id
                            LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':type_id', $type_data['type_id']);
        $sql_update->bindParam(':container_type', $type_data['type_name']);
        $sql_update->bindParam(':container_capacity', $type_data['type_capacity']);
        $sql_update->execute();
        $sql_update->closeCursor();
    }
    /*
 * Get type
 * @param type_id (int)
 */
    function get_type($type_id){

        $statement = "SELECT type_id, type_name, active, type_description,type_expiration,type_best_before
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_types
                        WHERE type_id = :type_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':type_id', $type_id);
        $sql->execute();
        $types = $sql->fetchAll();
        $sql->closeCursor();
        return $types;
    }

    /*
  * Get container Types
  * @param active (string)
  */
    function get_container_types($active = ''){

        $statement = "SELECT type_id, container_type, container_capacity, active
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_container_types";
        if ($active == 'N')
            $statement .= " WHERE active = 'N'";
        else
            $statement .= " WHERE active = 'Y' OR (active IS NULL)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();

        $types= $sql->fetchAll();
        $sql->closeCursor();
        return $types;
    }


    /**
     * Activate/ Deactivate container type
     * @param $type_id, $active
     * @return array
     */
    function change_container_type_status($type_id, $active) {

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_container_types
                        SET active = :active
                        WHERE type_id = :type_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':active', $active);
        $sql_update->bindParam(':type_id', $type_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /*
* Get type
* @param type_id (int)
*/
    function get_container_type($type_id){

        $statement = "SELECT type_id, container_type, container_capacity, active
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_container_types
                        WHERE type_id = :type_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':type_id', $type_id);
        $sql->execute();
        $types = $sql->fetchAll();
        $sql->closeCursor();
        return $types;
    }

    function update_settings($update_data) {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        foreach ($update_data as $key=>$data) {

            //Check if setting exists
            $statement = "SELECT COUNT(*) as a
                            FROM {$GLOBALS['db_prefix']}_settings
                            WHERE setting_tag = :setting_tag";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->bindParam(':setting_tag', $key);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();
                //Update
                if ($key == 'activate_maintenance') {
                        $statement = "UPDATE {$GLOBALS['db_prefix']}_settings
                                SET setting_value = :setting_value
                                WHERE setting_tag = :setting_tag
                                LIMIT 1";
                        $sql_update = $GLOBALS['dbCon']->prepare($statement);
                        $sql_update->bindParam(':setting_tag', $key);
                        $sql_update->bindParam(':setting_value', $data);
                        $sql_update->execute();
                        $sql_update->closeCursor();
            }
        }
    }
    /*
  * Get get_maintenance
  * @param
  */
    function get_maintenance() {

        $statement = "SELECT datediff(maintenance_end, CURDATE()) AS seconds, popup_message,maintenance_commence,maintenance_end,popup_color,popup_frequency, maintenance_timer, countdown_message, countdown_message_color, DATE_FORMAT(NOW(),'%Y-%m-%d %h:%i:%s') as today
                        FROM {$GLOBALS['db_prefix']}_maintenance";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $sql_company_data = $sql->fetch();
        $sql->closeCursor();
        return $sql_company_data;
    }

    /*
    * Save/Update maintenance
    * @param $maintenance_data (array)
    */
    function save_maintenance($maintenance_data){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_maintenance
						SET	popup_message =:popup_message,
						    maintenance_commence = :maintenance_commence,
						    maintenance_end = :maintenance_end,
						    popup_color = :popup_color,
						    popup_frequency = :popup_frequency";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':popup_message', $maintenance_data['display_message']);
        $sql->bindParam(':maintenance_commence', $maintenance_data['start']);
        $sql->bindParam(':maintenance_end', $maintenance_data['end']);
        $sql->bindParam(':popup_color', $maintenance_data['color']);
        $sql->bindParam(':popup_frequency', $maintenance_data['popup_frequency']);
        $sql->execute();
        $sql->closeCursor();
    }

    /*
   * Save/Update maintenance
   * @param $maintenance_data (array)
   */
    function save_maintenance_count_down($maintenance_data){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_maintenance
						SET	maintenance_timer =:maintenance_timer,
						    countdown_message = :countdown_message,
						    countdown_message_color = :countdown_message_color";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':maintenance_timer', $maintenance_data['activate_countdown']);
        $sql->bindParam(':countdown_message', $maintenance_data['countdown_message']);
        $sql->bindParam(':countdown_message_color', $maintenance_data['count_down_color']);
        $sql->execute();
        $sql->closeCursor();
    }


    /**
     * Checks the database to see if a type exists. If not it will return false else it will return the tpe ID.
     *
     * @param $type_name
     * @return bool
     */
    function type_exists($type_name) {

        $statement = "SELECT type_id
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_container_types
                        WHERE container_type = :type_name
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':type_name', $type_name);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        if ($sql_result === false || count($sql_result) == 0) {
            return false;
        } else {
            return $sql_result['type_id'];
        }
    }

    /**
     * Check if a stage exits.
     * Return FALSE if not else return the stage ID.
     *
     * @param $stage_name
     * @return bool
     */
    function stage_exists($stage_name) {

        $statement = "SELECT stage_id
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                        WHERE stage_name = :stage_name
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':stage_name', $stage_name);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        if ($sql_result === false || count($sql_result) == 0) {
            return false;
        } else {
            return $sql_result['stage_id'];
        }
    }


    /**
     * Disable the import function
     *
     */
    function disable_import() {
        $statement = "UPDATE {$GLOBALS['db_prefix']}_companies
                        SET allow_imports = 0
                        WHERE company_db_prefix = :prefix
                        LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':prefix', $this->company_code);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

}