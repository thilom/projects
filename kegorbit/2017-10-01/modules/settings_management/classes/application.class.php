<?php

/**
 * Application management
 * .
 * 2016-07-19: Thilo Muller - Created
 */

require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";

class Application {

    function get_settings() {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        //Vars
        $settings = array();

        //Get settings
        $statement = "SELECT setting_tag, setting_value, AES_DECRYPT(encValue, :phrase) AS encValue
                        FROM {$GLOBALS['db_prefix']}_settings";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':phrase', $phrase);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        //Assemble
        foreach ($sql_result as $data) {
            $settings[$data['setting_tag']] = !empty($data['encValue'])?$data['encValue']:$data['setting_value'];
        }

        return $settings;
    }

    function update_settings($update_data) {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        foreach ($update_data as $key=>$data) {

            //Check if setting exists
            $statement = "SELECT COUNT(*) as c
                            FROM {$GLOBALS['db_prefix']}_settings
                            WHERE setting_tag = :setting_tag";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->bindParam(':setting_tag', $key);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();

            if ($sql_result['c'] == 0) {

                if ($key == 'captcha_secret' || $key == 'smtp_password') {
                        //Insert setting
                        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings
                                      (setting_name, setting_tag, encValue)
                                    VALUES
                                      (:setting_name, :setting_tag, AES_ENCRYPT(:setting_value, :phrase))";
                        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
                        $sql_insert->bindParam(':setting_name', $key);
                        $sql_insert->bindParam(':setting_tag', $key);
                        $sql_insert->bindParam(':setting_value', $data);
                        $sql_insert->bindParam(':phrase', $phrase);
                        $sql_insert->execute();
                        $sql_insert->closeCursor();
                } else {
                    //Insert setting
                    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings
                                  (setting_name, setting_tag, setting_value)
                                VALUES
                                  (:setting_name, :setting_tag, :setting_value)";
                    $sql_insert = $GLOBALS['dbCon']->prepare($statement);
                    $sql_insert->bindParam(':setting_name', $key);
                    $sql_insert->bindParam(':setting_tag', $key);
                    $sql_insert->bindParam(':setting_value', $data);
                    $sql_insert->execute();
                    $sql_insert->closeCursor();
                }
            } else {
                //Update
                if ($key == 'captcha_secret') {
                    if (!empty($data)) {
                        $statement = "UPDATE {$GLOBALS['db_prefix']}_settings
                                SET encValue = AES_ENCRYPT(:setting_value, :phrase)
                                WHERE setting_tag = :setting_tag
                                LIMIT 1";
                        $sql_update = $GLOBALS['dbCon']->prepare($statement);
                        $sql_update->bindParam(':setting_tag', $key);
                        $sql_update->bindParam(':setting_value', $data);
                        $sql_update->bindParam(':phrase', $phrase);
                        $sql_update->execute();
                        $sql_update->closeCursor();
                    }
                } else if ($key == 'smtp_password') {
                    if (!empty($data)) {
                        $statement = "UPDATE {$GLOBALS['db_prefix']}_settings
                                SET encValue = AES_ENCRYPT(:setting_value, :phrase)
                                WHERE setting_tag = :setting_tag
                                LIMIT 1";
                        $sql_update = $GLOBALS['dbCon']->prepare($statement);
                        $sql_update->bindParam(':setting_tag', $key);
                        $sql_update->bindParam(':setting_value', $data);
                        $sql_update->bindParam(':phrase', $phrase);
                        $sql_update->execute();
                        $sql_update->closeCursor();
                    }
                } else {
                    $statement = "UPDATE {$GLOBALS['db_prefix']}_settings
                                SET setting_value = :setting_value
                                WHERE setting_tag = :setting_tag
                                LIMIT 1";
                    $sql_update = $GLOBALS['dbCon']->prepare($statement);
                    $sql_update->bindParam(':setting_tag', $key);
                    $sql_update->bindParam(':setting_value', $data);
                    $sql_update->execute();
                    $sql_update->closeCursor();
                }

            }
        }
    }

}