<?php

/**
 * Customer management class
 *
 * 2016-07-05: Thilo Muller - Created
 */


require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";

class Customer {

    /**
     * Company code for logged in user
     *
     * @var string
     */
    private $company_code = '';

    /**
     * Customer constructor.
     */
    function __construct() {

        if(isset($_SESSION['user'])){
            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }
    }

    /**
     * Return a list of all customers
     *
     * @return mixed
     */
    function get_customers($active = true, $start, $length, $order_col='', $order_dir='', $search='') {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT customer_id, customer_name, customer_contact,
                              AES_DECRYPT(customer_contact_number, :phrase) AS customer_contact_number,
                               AES_DECRYPT(customer_contact_email, :phrase) AS customer_contact_email
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_customers";
        if ($active == true) {
            $statement .= " WHERE (inactive IS NULL OR inactive <> 'Y') ";
        } else {
            $statement .= " WHERE inactive = 'Y' ";
        }
        if (!empty($search)) {
            $statement .= " AND (customer_name LIKE :search OR customer_contact LIKE :search)";
            $search = "%$search%";
        }
        if (!empty($order_col)) {
            $statement .= " ORDER BY $order_col $order_dir ";
        }
        $statement .= " LIMIT $start, $length ";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':phrase', $phrase);
        if (!empty($search)) $sql_select->bindParam(':search', $search);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Number of customers
     *
     * @param bool $active
     * @return mixed
     */
    function get_customer_count($active=true, $search='') {
        $statement = "SELECT COUNT(*) AS cCount
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_customers ";
        if ($active == true) {
            $statement .= " WHERE inactive IS NULL OR inactive = '' ";
        } else {
            $statement .= " WHERE inactive = 'Y' ";
        }
        if (!empty($search)) {
            $statement .= " AND (customer_name = :search OR customer_contact = :search)";
            $search = "%$search%";
        }

        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        if (!empty($search)) $sql_select->bindParam(':search', $search);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result['cCount'];

    }


    /**
     * Return specifiod customer data
     *
     * @param $customer_id
     * @return mixed
     */
    function get_customer($customer_id) {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $customer_id = (int) $customer_id;

        $statement = "SELECT customer_id, customer_name, customer_contact,
                              AES_DECRYPT(customer_contact_number, :phrase) AS customer_contact_number,
                              AES_DECRYPT(customer_contact_email, :phrase) AS customer_contact_email,
                              AES_DECRYPT(customer_postal_address1, :phrase) AS customer_postal_address1,
                              AES_DECRYPT(customer_postal_address2, :phrase) AS customer_postal_address2,
                              AES_DECRYPT(customer_postal_address3, :phrase) AS customer_postal_address3,
                              AES_DECRYPT(customer_postal_address4, :phrase) AS customer_postal_address4,
                              AES_DECRYPT(customer_physical_address1, :phrase) AS customer_physical_address1,
                              AES_DECRYPT(customer_physical_address2, :phrase) AS customer_physical_address2,
                              AES_DECRYPT(customer_physical_address3, :phrase) AS customer_physical_address3,
                              AES_DECRYPT(customer_physical_address4, :phrase) AS customer_physical_address4,
                              same_as_physical
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                        WHERE customer_id = :customer_id
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':customer_id', $customer_id);
        $sql_select->bindParam(':phrase', $phrase);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }


    /**
     * Update/Save customer information
     *
     * @param $customer_id
     * @param $customer_data
     * @return int
     */
    function save_customer($customer_id, $customer_data) {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $customer_id = (int) $customer_id;

        if (!isset($customer_data['same_as_physical'])) $customer_data['same_as_physical'] = 0;

        if ($customer_id == 0) {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                              (customer_name, customer_contact, customer_contact_number, customer_contact_email,
                                customer_physical_address1, customer_physical_address2, customer_physical_address3, customer_physical_address4,
                                customer_postal_address1, customer_postal_address2, customer_postal_address3, customer_postal_address4,
                                same_as_physical)
                            VALUES
                              (:customer_name, :customer_contact, AES_ENCRYPT(:customer_contact_number, '{$phrase}'),
                              AES_ENCRYPT(:customer_contact_email, '{$phrase}'),
                                AES_ENCRYPT(:customer_physical_address1, '{$phrase}'),
                                AES_ENCRYPT(:customer_physical_address2, '{$phrase}'),
                                AES_ENCRYPT(:customer_physical_address3, '{$phrase}'),
                                AES_ENCRYPT(:customer_physical_address4, '{$phrase}'),
                                AES_ENCRYPT(:customer_postal_address1, '{$phrase}'),
                                AES_ENCRYPT(:customer_postal_address2, '{$phrase}'),
                                AES_ENCRYPT(:customer_postal_address3, '{$phrase}'),
                                AES_ENCRYPT(:customer_postal_address4, '{$phrase}'),
                                :same_as_physical
                              )";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bindParam(':customer_name', $customer_data['customer_name']);
            $sql_insert->bindParam(':customer_contact', $customer_data['customer_contact']);
            $sql_insert->bindParam(':customer_contact_number', $customer_data['customer_contact_number']);
            $sql_insert->bindParam(':customer_contact_email', $customer_data['customer_contact_email']);
            $sql_insert->bindParam(':customer_physical_address1', $customer_data['customer_physical_address1']);
            $sql_insert->bindParam(':customer_physical_address2', $customer_data['customer_physical_address2']);
            $sql_insert->bindParam(':customer_physical_address3', $customer_data['customer_physical_address3']);
            $sql_insert->bindParam(':customer_physical_address4', $customer_data['customer_physical_address4']);
            $sql_insert->bindParam(':customer_postal_address1', $customer_data['customer_postal_address1']);
            $sql_insert->bindParam(':customer_postal_address2', $customer_data['customer_postal_address2']);
            $sql_insert->bindParam(':customer_postal_address3', $customer_data['customer_postal_address3']);
            $sql_insert->bindParam(':customer_postal_address4', $customer_data['customer_postal_address4']);
            $sql_insert->bindParam(':same_as_physical', $customer_data['same_as_physical']);
            $sql_insert->execute();
            $sql_insert->closeCursor();

            $customer_id = $GLOBALS['dbCon']->lastinsertid();

        } else {

            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                            SET customer_name = :customer_name,
                                customer_contact = :customer_contact,
                                customer_contact_number = AES_ENCRYPT(:customer_contact_number, '{$phrase}'),
                                customer_contact_email = AES_ENCRYPT(:customer_contact_email, '{$phrase}'),
                                customer_physical_address1 = AES_ENCRYPT(:customer_physical_address1, '{$phrase}'),
                                customer_physical_address2 = AES_ENCRYPT(:customer_physical_address2, '{$phrase}'),
                                customer_physical_address3 = AES_ENCRYPT(:customer_physical_address3, '{$phrase}'),
                                customer_physical_address4 = AES_ENCRYPT(:customer_physical_address4, '{$phrase}'),
                                customer_postal_address1 = AES_ENCRYPT(:customer_postal_address1, '{$phrase}'),
                                customer_postal_address2 = AES_ENCRYPT(:customer_postal_address2, '{$phrase}'),
                                customer_postal_address3 = AES_ENCRYPT(:customer_postal_address3, '{$phrase}'),
                                customer_postal_address4 = AES_ENCRYPT(:customer_postal_address4, '{$phrase}'),
                                same_as_physical = :same_as_physical
                            WHERE customer_id = :customer_id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':customer_id', $customer_id);
            $sql_update->bindParam(':customer_contact', $customer_data['customer_contact']);
            $sql_update->bindParam(':customer_contact_number', $customer_data['customer_contact_number']);
            $sql_update->bindParam(':customer_contact_email', $customer_data['customer_contact_email']);
            $sql_update->bindParam(':customer_name', $customer_data['customer_name']);
            $sql_update->bindParam(':customer_physical_address1', $customer_data['customer_physical_address1']);
            $sql_update->bindParam(':customer_physical_address2', $customer_data['customer_physical_address2']);
            $sql_update->bindParam(':customer_physical_address3', $customer_data['customer_physical_address3']);
            $sql_update->bindParam(':customer_physical_address4', $customer_data['customer_physical_address4']);
            $sql_update->bindParam(':customer_postal_address1', $customer_data['customer_postal_address1']);
            $sql_update->bindParam(':customer_postal_address2', $customer_data['customer_postal_address2']);
            $sql_update->bindParam(':customer_postal_address3', $customer_data['customer_postal_address3']);
            $sql_update->bindParam(':customer_postal_address4', $customer_data['customer_postal_address4']);
            $sql_update->bindParam(':same_as_physical', $customer_data['same_as_physical']);
            $sql_update->execute();
            $sql_update->closeCursor();

        }

        return $customer_id;
    }


    function toggle_active($customer_id) {
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                        SET inactive = IF(inactive='Y','','Y')
                        WHERE customer_id = :customer_id
                        LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':customer_id', $customer_id);
        $sql_update->execute();
        $sql_update->closeCursor();

        //Get current state
        $statement = "SELECT inactive
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                        WHERE customer_id = :customer_id
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':customer_id', $customer_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result['inactive']=='Y'?'Deactivated':'Activated';
    }
    /*
  * Get number of containers at client
  * @param active (string)
  */
    function get_customer_container_count($customer_id) {

        $statement = "SELECT  COUNT(*) AS container_count
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                         WHERE  container_client = :container_client";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_client', $customer_id);
        $sql->execute();
        $sql_count_data = $sql->fetch();
        $sql->closeCursor();
        return ($sql_count_data['container_count']);
    }


    /**
     * Check if a customer exists.
     * Returns FALSE if it doesn't or Customer ID if it does.
     *
     * @param $customer_name
     * @return bool
     */
    function customer_exists($customer_name) {
        $statement = "SELECT customer_id
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                        WHERE customer_name = :customer_name
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':customer_name', $customer_name);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        if ($sql_result === false || count($sql_result) == 0) {
            return false;
        } else {
            return $sql_result['customer_id'];
        }
    }

}