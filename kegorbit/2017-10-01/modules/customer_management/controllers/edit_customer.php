<?php
/**
 * Edit a customer
 *
 * 2016-07-05: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/customer_management/classes/customer.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$customerClass = new Customer();
$systemClass = new System();
$settingsClass = new Settings();
$edit_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/customer_management/html/edit_customer.html");
$customer_id = (int) $_GET['id'];

//Get custamer data
if ($customer_id > 0) {
    $customer_data = $customerClass->get_customer($customer_id);
    $customer_data['customer_task'] = 'Edit';
    $customer_data['same_as_physical'] = $customer_data['same_as_physical']=='1'?'checked':'';
} else {
    $customer_data['customer_id'] = $customer_id;
    $customer_data['customer_name'] = '';
    $customer_data['customer_contact'] = '';
    $customer_data['customer_contact_number'] = '';
    $customer_data['customer_contact_email'] = '';
    $customer_data['customer_task'] = 'Create';
    $customer_data['same_as_physical'] = '';
    $customer_data['customer_physical_address1'] = '';
    $customer_data['customer_physical_address2'] = '';
    $customer_data['customer_physical_address3'] = '';
    $customer_data['customer_physical_address4'] = '';
    $customer_data['customer_postal_address1'] = '';
    $customer_data['customer_postal_address2'] = '';
    $customer_data['customer_postal_address3'] = '';
    $customer_data['customer_postal_address4'] = '';
}

//get terminology
$singular = $settingsClass->get_customer_terminology('singular');
$plural  = $settingsClass->get_customer_terminology('plural');

//
$customer_data['customer_singular'] = $singular;
$customer_data['customer_plural'] = $plural;

$edit_template = $systemClass->merge_data($edit_template, $customer_data);


echo $edit_template;


