<?php
/**
 * customer management
 * 2016-06-14 -  Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//get terminology
$singular = $settingsClass->get_customer_terminology($_SESSION['user']['company_id'], 'singular');
$plural  = $settingsClass->get_customer_terminology($_SESSION['user']['company_id'], 'plural');

$template_data = array(
    'customer_singular'=> $singular,
    'customer_plural'=> $plural,
);

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/customer_management/html/list_customers.html");
$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;