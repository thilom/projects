<?php
/**
 * Activate/Deactivate customers
 *
 * 2016-07-05: Thilo Muller - Created
 */

//Includes
require_once '../../../settings/init.php';
require_once '../classes/customer.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$customerClass = new Customer();
$accountClass = new Accounts();
$customer_id = (int) $_GET['id'];

//Toggle activity
$change = $customerClass->toggle_active($customer_id);

//Get company data
$customer_data  = $customerClass->get_customer($customer_id);


$accountClass->insert_user_activity("Customer '{$customer_data['customer_name']}' {$change}");

