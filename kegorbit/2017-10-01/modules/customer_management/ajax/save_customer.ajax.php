<?php
/**
 * Save updated customer information
 *
 * 2016-07-05: Thilo Muller - Created
 */

//Includes
require_once '../../../settings/init.php';
require_once '../classes/customer.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$customerClass = new Customer();
$accountClass = new Accounts();
$customer_id = (int) $_POST['customer_id'];
$created = $customer_id==0?true:false;


//Save data
$customer_id = $customerClass->save_customer($customer_id, $_POST);

//Add activity
if ($created === true) {
    $message = "Created Customer '{$_POST['customer_name']}'";
} else {
    $message = "Updated Customer '{$_POST['customer_name']}'";
}
$accountClass->insert_user_activity($message);


echo $customer_id;