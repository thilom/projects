<?php
/**
 * List company customers
 *
 * 2016-07-05: Thilo Muller - Created
 */

//Includes
require_once '../../../settings/init.php';
require_once '../classes/customer.class.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);
//Vars
$customerClass = new Customer();
$datatableData = array('aaData' => array());
$customer_type = $_GET['type']=='active'?true:false;
$start = (int) $_GET['start'];
$length = (int) $_GET['length'];
$columns = array('customer_name','customer_contact');

//Attach draw value
$datatableData['draw'] = (int) $_GET['draw'];

//Total Customer Count
$datatableData['recordsTotal'] = $customerClass->get_customer_count($customer_type);

//Set filters
$search = empty($_GET['search']['value'])?'':$_GET['search']['value'];

//Filtered Customer Count
$datatableData['recordsFiltered'] = $customerClass->get_customer_count($customer_type, $search);

//Order By
$order_dir = $_GET['order'][0]['dir'];
$order_col = $columns[$_GET['order'][0]['column']];

//Get list
$customers = $customerClass->get_customers($customer_type, $start, $length, $order_col, $order_dir, $search);

//Assemble for Datatables
foreach ($customers as $data) {
    $line = array();
    $line[] = utf8_encode($data['customer_name']);
    $line[] = utf8_encode($data['customer_contact']);
    $line[] = utf8_encode($data['customer_contact_number']);
    $line[] = utf8_encode($data['customer_contact_email']);
    $line[] = $customerClass->get_customer_container_count($data['customer_id']);

    if ($customer_type === true) {
        $line[] = "<div class='btn-group'>
                    <button onclick='editCustomer({$data['customer_id']})' class='btn btn-primary btn-sm'><span class='btn-label'><i class='fa fa-pencil-square-o'></i></span> <span class='hidden-xs'> Edit</span></button>
							<button class='btn btn-primary btn-sm  dropdown-toggle' data-toggle='dropdown'>
								<span class='caret'></span>
							</button>
							<ul class='dropdown-menu' >
								<li>
									<a onclick='toggle_active_customer({$data['customer_id']},0,\"{$data['customer_name']}\")' ><i class='fa fa-ban'></i></span> De-activate Customer</a>
								</li>
							</ul>
						</div>";
    } else {
        $line[] = "<div class='btn-group'>
                    <button onclick='editCustomer({$data['customer_id']})' class='btn btn-primary btn-sm'><span class='btn-label'><i class='fa fa-pencil-square-o'></i></span> <span class='hidden-xs'> Edit</span></button>
							<button class='btn btn-primary btn-sm  dropdown-toggle' data-toggle='dropdown'>
								<span class='caret'></span>
							</button>
							<ul class='dropdown-menu' >
								<li>
									<a onclick='toggle_active_customer({$data['customer_id']},1,\"{$data['customer_name']}\")' ><i class='fa fa-ban'></i></span> Activate Customer</a>
								</li>
							</ul>
						</div>";
    }


    $datatableData['aaData'][] = $line;
}

//Send to browser
echo json_encode($datatableData);