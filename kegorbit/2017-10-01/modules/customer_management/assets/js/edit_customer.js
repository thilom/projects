
$(document).ready(function() {
    $('#same').each(function(){
        var self = $(this),
            label = self.next(),
            label_text = label.text();
        label.remove();
        self.iCheck({
            checkboxClass: 'icheckbox_line-aero',
            radioClass: 'iradio_line-aero',
            insert: '<span class="icheck_line-icon"></span>' + label_text
        });
    });

    $('#same').on('ifChecked', function() {
        $('.addr').prop('disabled', true);
        $('.addr1').data('value', $('.addr1').val());
        $('.addr2').data('value', $('.addr2').val());
        $('.addr3').data('value', $('.addr3').val());
        $('.addr4').data('value', $('.addr4').val());
        $('.addr').val('');
    })

    $('#same').on('ifUnchecked', function() {
        $('.addr').prop('disabled', false);
        $('.addr1').val($('.addr1').data('value'));
        $('.addr2').val($('.addr2').data('value'));
        $('.addr3').val($('.addr3').data('value'));
        $('.addr4').val($('.addr4').data('value'));
    })

    if ($('#same').iCheck('update')[0].checked ) {
        $('.addr').prop('disabled', true);
        $('.addr1').data('value', $('.addr1').val());
        $('.addr2').data('value', $('.addr2').val());
        $('.addr3').data('value', $('.addr3').val());
        $('.addr4').data('value', $('.addr4').val());
        $('.addr').val('');
    }

    //$("#customer_contact_number").mask("(999) 999-9999",{placeholder:"(999) 999-9999"});

//Form validations
    $.fn.bootstrapValidator.validators.duplicateUsername = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/ajax/check_duplicate_email.ajax.php?email=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });

            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };

    $('#customerForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            customer_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Customer's Name."}
                }
            },
            customer_contact: {
                validators: {
                    notEmpty: {message: "Please Provide  the Customer's Contact Name."}
                }
            },
            customer_contact_number: {
                validators: {
                    notEmpty: {message: "Please Provide  the Customer's Contact Numbers."}
                }
            },
            customer_contact_email: {
                validators: {
                    notEmpty: {message: "Please Provide  the Customer's Email Name."},
                    duplicateUsername:{message: "Duplicate Customer's Email Address Detected."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            data: $('#customerForm').serialize(),
            method: 'post',
            url: '/modules/customer_management/ajax/save_customer.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if ($('#customer_id').val() == 0) {
                    message = "Customer '" + $('#customer_name').val() + "' Created";
                } else {
                    message = "Customer '" + $('#customer_name').val() + "' Updated";
                }

                swal({
                        title: "Edit Customer",
                        text: message,
                        type: "success",
                        showCancelButton: true,
                        confirmButtonColor: "#458B00",
                        confirmButtonText: "Continue Editing",
                        cancelButtonText: "Done",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $('#customer_id').val(data.responseText);
                            $('#customer_title').text($('#customer_name').val());
                            $('.customer_task').text('Edit');
                        } else {
                            document.location = '/index.php?m=customer_management&a=list_customers';
                        }
                    });
            }
        })
    });

});



/**
 * Cancel the edit process
 */
function cancelEdit() {
    document.location = '/index.php?m=customer_management&a=list_customers';
}

