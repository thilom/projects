
var activeTable, inactiveTable;

$(document).ready(function() {
    drawActiveTable();

    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
        if ($(e.target).attr('id') == 'inactiveTab') drawInactiveTable();
    } );
});

/**
 * Draw the active customer table
 */
function drawActiveTable() {
    activeTable = $('#activeCustomerTable').DataTable({
        "processing": true,
        "serverSide": true,
        destroy: true,
        ajax: '/modules/customer_management/ajax/list_customers.ajax.php?type=active',
        "aoColumns": [
            null,
            null,
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false }
        ],
        stateSave: true,
        stateDuration: 0
    });
}

/**
 * Draw the active customer table
 */
function drawInactiveTable() {
    inactiveTable = $('#inactiveCustomerTable').DataTable({
        "processing": true,
        "serverSide": true,
        destroy: true,
        ajax: '/modules/customer_management/ajax/list_customers.ajax.php?type=inactive',
        "aoColumns": [
            null,
            null,
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false }
        ],
        stateSave: true,
        stateDuration: 0
    });
}

/**
 * Redirect to edit customer screen
 *
 * @param id
 */
function editCustomer(id) {
        document.location = '/index.php?m=customer_management&a=edit_customer&id=' + id;
}


/**
 * Activate/Deactivate a customer
 *
 * @param id
 */
function toggle_active_customer(id, p, n) {
    swal({
            title: p==0?"Deactivate Customer?":"Activate Customer?",
            text: n,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#F8AC59",
            confirmButtonText: p==0?"Yes - Deactivate":"Yes - Activate",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '/modules/customer_management/ajax/toggle_active_customer.ajax.php?id=' + id,
                    complete: function() {
                        setTimeout(function () {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                positionClass: 'toast-bottom-right',
                                timeOut: 4000
                            };
                            toastMessage = p==0?'Customer \'' + n + '\' deactivated':'Customer \'' + n + '\' activated'
                            toastr.success(toastMessage);
                            p===0?activeTable.draw():inactiveTable.draw();

                        }, 1300);
                    }
                })
            }
        });
}