<?php
/**
 * KegOrbit dashboard
 *
 * 2016-07-03: Thilo Muller - Created
 *
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$dashboard_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/dashboard_management/html/dashboard.html");
$usersClass = new User();
$companyClass = new Company();
$hooks = array();
$hook_type = 'dashboard';
$hook_content = '';
$hook_content_left = '';
$hook_content_right = '';
$hook_content_bottom = '';
$dir = "{$_SERVER['DOCUMENT_ROOT']}/modules";

//Run through directories looking for dashboard hooks
$d = dir($dir);
$counter = 0;
while (false !== ($entry = $d->read())) {
    if ($entry == '.' || $entry == '..') continue;
    if (is_dir("{$dir}/{$entry}") && is_dir("{$dir}/{$entry}/hooks")) {
        include "{$dir}/{$entry}/hooks/hooks.ini.php";
        foreach ($ini_hooks as $data) {
            $data['module'] = $entry;
            if ($data['type'] == $hook_type) {
                $hooks["hook$counter"] = $data;
                $hook_order["hook$counter"] = $data['order'];
                $counter++;
            }

        }
    }
}
$d->close();

//Reorder hooks
asort($hook_order);

//Run Hooks
foreach ($hook_order as $hKey=>$order) {
    ob_start();
    include "{$_SERVER['DOCUMENT_ROOT']}/modules/{$hooks[$hKey]['module']}/hooks/{$hooks[$hKey]['dir']}/hook.php";
    if (!isset($hooks[$hKey]['position'])) $hooks[$hKey]['position'] = 'left'; //Set default to left

    switch ($hooks[$hKey]['position']) {
        case 'bottom':
            $hook_content_bottom .= ob_get_contents();
            break;
        case 'right':
            $hook_content_right .= ob_get_contents();
            break;
        default:
            $hook_content_left .= ob_get_contents();
            break;
    }

    ob_end_clean();
}
// get user details
$user = $usersClass->get_user($_SESSION['user']['id']);
$name = $user['firstname'];
$company = $companyClass->get_company($user['company_id']);
if($company['company_name'] == 'Application Administration'){
    $company_name = 'Keg Orbit Software SYSTEM';
}else{
    $company_name = $company['company_name'];
}


//Replace 'hook content' in template
$dashboard_template = str_replace('<!-- hook_content_left -->', $hook_content_left, $dashboard_template);
$dashboard_template = str_replace('<!-- hook_content_right -->', $hook_content_right, $dashboard_template);
$dashboard_template = str_replace('<!-- hook_content_bottom -->', $hook_content_bottom, $dashboard_template);
$dashboard_template = str_replace('<!-- company_name -->', $company_name, $dashboard_template);
$dashboard_template = str_replace('<!-- username -->', $name, $dashboard_template);

echo $dashboard_template;