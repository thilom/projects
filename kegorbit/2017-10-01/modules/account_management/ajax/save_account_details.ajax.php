<?php

/**
 *  save/update account
 *  2016-06-22 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/swiftmailer/lib/swift_required.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();
$companyClass = new Company();
$smtp_url = $GLOBALS['smtp']['url'];
$smtp_port = $GLOBALS['smtp']['port'];
$smtp_username = $GLOBALS['smtp']['username'];
$smtp_password = $GLOBALS['smtp']['password'];

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//Get user company data
$company_data = $companyClass->get_company($_SESSION['user']['company_id']);

if($_POST['user_id'] == 0) {

    //get company_id
    $user = $accountClass->get_account($_SESSION['user']['id']);

    //create new account
        $user_id = $accountClass->save_new_account($_POST, $user['company_id']);

        if(!empty($user_id)){
                //send email to user to complete registration
                $code = sha1(mt_rand(10000,99999).time().$_POST['user_email_address']);

                //save activation code
                $userClass->update_user_activation_code($code,$user_id);
                // Create the mail transport configuration
                $transport = Swift_SmtpTransport::newInstance($smtp_url, $smtp_port)->setUsername($smtp_username)->setPassword($smtp_password);
                //Supposed to allow local domain sending to work from what I read
                //$transport->setLocalDomain('[127.0.0.1]');

                // Create the message
                $message = Swift_Message::newInstance($transport);

                $message->setTo($_POST['user_email_address']);
                if(!empty($user['company_name'])){
                    $message->setSubject($user['company_name']." Registration");
                }
                else{
                    $message->setSubject("Keg Orbit Registration");
                }


                $body = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/html/email_company_template/new_user_email.html');

                $table_details = array('email' => $_POST['user_email_address'], 'username' => $_POST['user_username'], 'firstname' => $_POST['user_full_name']);
                $body = $systemClass->merge_data($body, $table_details);


                $email_name = array('contact_name' => $_POST['user_full_name']);
                $body = $systemClass->merge_data($body, $email_name);

                $email_data['reset_link'] = "<a href='http://{$_SERVER['HTTP_HOST']}/index.php?code=$code&new={$user_id}'>{$_SERVER['HTTP_HOST']}/index.php?code=$code&new={$user_id}</a>";
                if(!empty($user['company_name'])){
                    $email_data['company_name'] =  $user['company_name'];
                }
                else{
                    $email_data['company_name'] = 'Keg Orbit';
                }
                $email_data['company_name'] =
                $body = $systemClass->merge_data($body, $email_data);

//
                $server_details = array('server' => $_SERVER['HTTP_HOST'], 'timestamp' => date('d-m-Y H:i:s'));
                $body = $systemClass->merge_data($body, $server_details);

                $message->setBody($body, 'text/html');
                $message->setFrom(array($GLOBALS['email']['from']['email'] => $GLOBALS['email']['from']['name']));
                // Send the email
                $mailer = Swift_Mailer::newInstance($transport);
                echo 'new';
                $mailer->send($message);

                $accountClass->insert_user_activity("Account: '{$_POST['user_full_name']}' Created for {$company_data['company_name']}");
            }
}
else{
    //update user account details
   $accountClass->update_user_details($_POST);
    echo 'update';

    $accountClass->insert_user_activity("Account: '{$_POST['user_full_name']}' Updated for {$company_data['company_name']}");
}


