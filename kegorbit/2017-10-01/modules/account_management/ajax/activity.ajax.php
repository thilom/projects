<?php
/**
 * Retrieve latest activity
 *
 * 2016-07-30: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/classes/account.class.php";

//Vars
$accountClass = new Accounts();
$from_time = $_GET['t'];
$rData = array('timestamp' => '', 'mData' => array());

//Get latest activity
$t = date('Y-m-d H:i:s', $from_time);
$messages = $accountClass->get_activity($t);
$messages = array_reverse($messages);
foreach ($messages as $key=>$data) {
    $rData['mData'][] .= "<div  class='msg'><small>{$data['user_name']} - {$data['activity_timestamp']}</small> <div class='alert alert-info'>{$data['activity_message']}</div></div>";
    if ($key == 0)  $rData['timestamp'] = strtotime($data['activity_timestamp']);
}

if (empty($rData['timestamp'])) $rData['timestamp'] = $from_time;

echo json_encode($rData);

