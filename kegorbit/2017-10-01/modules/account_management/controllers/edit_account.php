<?php

/**
 * Call the edit company page
 * 2016-06-20 - Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();
$companyClass = new Company();

$output = array();
$output['aaData'] = '';
$user_id = isset($_GET['user_id'])? $_GET['user_id'] :0;

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//get the company date format
if(isset($_SESSION['user']['run_as'])) {
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['run_as']);
} else{
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
}
$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];


if($user_id != 0){

    if($user_id == $_SESSION['user']['id']){
        $template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/html/edit_own_account.html");
    }else{
        $template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/html/edit_account.html");
    }
    //get a list of all companies
    $account = $accountClass->get_account($user_id);

    //get all user roles
    $user_roles = $accountClass->get_all_user_roles();

    $job_description = '<select name="user_job_description" class="form-control" id="user_job_description">
                                                <option value="">Select User Type</option>';
    foreach($user_roles as $roles) {
        if ($account['user_type'] == $roles['role_id']) {
            $job_description .= '<option value="' . $roles['role_id'] . '" selected>' . $roles['role_display_name'] . '</option>';
        } else {
            $job_description .= '<option value="' . $roles['role_id'] . '" >' . $roles['role_display_name'] . '</option>';
        }
    }
    $job_description .= '</select>';


    $modules = $accountClass->get_secure_modules($user_id);
    $table = '<div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th>Allow Access</th>
                    </tr>
                </thead>
                <tbody>';

    foreach($modules as $module)
    {
        $module_folder = $module['module_folder'];
        $module_name = $module['module_name'];

        require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
        $settingsClass = new Settings();
        $container_singular = $settingsClass->get_container_terminology();
        $client_singular = $settingsClass->get_customer_terminology();
        $module_name = str_replace("Container",$container_singular,$module_name);
        $module_name = str_replace("Customer",$client_singular,$module_name);

        $table .= "<tr style='background-color: #d3d3d3'><th colspan='2'>$module_name</th></tr>";

        $permissions = $accountClass->get_module_permissions($module_folder, $user_id);

        foreach($permissions as $permission)
        {
            $permission_id = $permission['permission_id'];
            $permission_name = $permission['permission_name'];
            $permission_description = $permission['permission_desc'];

            require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
            $settingsClass = new Settings();

            $container_singular = $settingsClass->get_container_terminology();
            $container_plural = $settingsClass->get_container_terminology('plural');
            $stage_singular = $settingsClass->get_stage_terminology();
            $stage_plural = $settingsClass->get_stage_terminology('plural');
            $state_singular = $settingsClass->get_state_terminology();
            $state_plural = $settingsClass->get_state_terminology('plural');
            $customer_singular = $settingsClass->get_customer_terminology();
            $customer__plural = $settingsClass->get_customer_terminology('plural');


            $permission_description = str_replace("Container",$container_singular,$permission_description);
            $permission_description = str_replace("Containers",$container_singular,$permission_description);
            $permission_description = str_replace("Stages",$stage_plural,$permission_description);
            $permission_description = str_replace("States",$state_plural,$permission_description);
            $permission_description = str_replace("Customer",$customer_singular,$permission_description);


            $access = $securityClass->check_user_permission($user_id, $module_folder, $permission_name);

            if($permission_name == 'general_access') {
                $general = 'y';
                $general_allowed = $access;
            }
            else
            {
                $general = 'n';
            }

            if($access)
            {
                if($general != 'y')
                {
                    $access_radio = '<label class="radio-inline i-checks">
                        <input type="radio" class="' . $module['module_id'] . '" name="' . $permission_id . '" value="1" data-general="' . $general . '" checked>
                        Yes
                    </label>
                    <label class="radio-inline i-checks">
                        <input type="radio" class="' . $module['module_id'] . '" name="' . $permission_id . '" value="0" data-general="' . $general . '">
                        No
                    </label>';
                }
                else
                {
                    if($general_allowed)
                    {
                        $access_radio = '<label class="radio-inline i-checks">
                        <input type="radio" name="' . $permission_id . '" value="1" data-general="' . $general . '"  data-module="' . $module['module_id'] . '" checked>
                        Yes
                    </label>
                    <label class="radio-inline i-checks">
                        <input type="radio" name="' . $permission_id . '" value="0" data-general="' . $general . '"  data-module="' . $module['module_id'] . '">
                        No
                    </label>';
                    }
                    else
                    {
                        $access_radio = '<label class="radio-inline i-checks">
                        <input type="radio" name="' . $permission_id . '" value="1" data-general="' . $general . '"  data-module="' . $module['module_id'] . '" checked disabled>
                        Yes
                    </label>
                    <label class="radio-inline i-checks">
                        <input type="radio" name="' . $permission_id . '" value="0" data-general="' . $general . '"  data-module="' . $module['module_id'] . '" disabled>
                        No
                    </label>';
                    }
                }
            }
            else
            {
                if($general == 'y')
                {
                    $access_radio = '<label class="radio-inline i-checks">
                        <input type="radio" name="' . $permission_id . '" value="1" data-general="' . $general . '" data-module="' . $module['module_id'] . '">
                        Yes
                    </label>
                    <label class="radio-inline i-checks">
                        <input type="radio" name="' . $permission_id . '" value="0" data-general="' . $general . '" checked>
                        No
                    </label>';
                }
                else
                {
                    if($general_allowed)
                    {
                        $access_radio = '<label class="radio-inline i-checks">
                        <input type="radio" class="' . $module['module_id'] . '" name="' . $permission_id . '" value="1" data-general="' . $general . '">
                        Yes
                    </label>
                    <label class="radio-inline i-checks">
                        <input type="radio" class="' . $module['module_id'] . '" name="' . $permission_id . '" value="0" data-general="' . $general . '" checked>
                        No
                    </label>';
                    }
                    else
                    {
                        $access_radio = '<label class="radio-inline i-checks">
                        <input type="radio" class="' . $module['module_id'] . '" name="' . $permission_id . '" value="1" data-general="' . $general . '" disabled>
                        Yes
                    </label>
                    <label class="radio-inline i-checks">
                        <input type="radio" class="' . $module['module_id'] . '" name="' . $permission_id . '" value="0" data-general="' . $general . '" checked disabled>
                        No
                    </label>';
                    }
                }
            }

            $table .= "<tr>
                    <td>$permission_description</td>
                    <td>$access_radio</td>
                    </tr>";
        }
    }

    $table .= '</tbody></table></div>';

    $date = ($format);
    if(!empty($account['last_login'])){
        $date = date($format. ' H:i:s',strtotime($account['last_login']));
    }
    else{
        $date = '';
    }



    $template_data = array(
        'user_id'       =>  $user_id,
        'account_name'       =>  $account['company_name'],
        'new_account' => 'Edit Account',
        'date'       =>  $date,
        'creation_date' => '<label> Last Logged in: </label>' .' '. $date,
        'user_full_name'   => $account['user_full_name'],
        'user_email_address'   => $account['user_email_address'],
        'user_contact_numbers'   => $account['users_contact_numbers'],
        'user_job_description'   => $job_description,
        'user_username'   => $account['user_username'],
        'user_notes'   => $account['user_notes'],
        'workplace_description' => $account['user_job_description'],
        'modules'=>$table,
    );
}


else{
    $template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/html/create_account.html");

    //get all user roles
    $user_roles = $accountClass->get_all_user_roles();

    $job_description = '<select name="user_job_description" class="form-control" id="user_job_description">
                                                <option value="">Select User Type</option>';
    foreach($user_roles as $roles) {
        $job_description .= '<option value="' . $roles['role_id'] . '" >' . $roles['role_display_name'] . '</option>';
    }
    $job_description .= '</select>';

    $current_user_company = $companyClass->get_company($_SESSION['user']['company_id']);
    $company_list = ' <label id="company_label" style="display: none">Select Association</label> <select name="company" class="form-control" id="company" style="display:none">
                                                <option value="">Belongs to?</option>';
    if($current_user_company['company_name'] != "Application Administration"){
            $company_list .= '<option value="' . $current_user_company['company_id'] . '" >' . $current_user_company['company_name'] . '</option>';
        $company_list .= '</select>';
    }
    else{
        //get a list of all companies
        $companies = $companyClass->get_companies('Y');
        $company_list = ' <label id="company_label" style="display: none">Select Association</label> <select name="company" class="form-control" id="company" style="display:none">
                                                <option value="">Belongs to?</option>';

        foreach($companies as $company){
            $company_list .= '<option value="' . $company['company_id'] . '" >' . $company['company_name'] . '</option>';
        }
        $company_list .= '</select>';
    }


    $template_data = array(
        'user_id'       =>  $user_id,
        'account_name'       =>  'New Account',
        'new_account' => 'Create Account',
        'date'       =>  $date = date($format.'h:i:s'),
        'creation_date' => '<label> Creation Date: </label> ' . $date = date($format.' h:i:s'),
        'user_full_name'   => '',
        'user_email_address'   => '',
        'user_contact_numbers'   => '',
        'user_job_description'   => $job_description,
        'user_username'   => '',
        'user_notes'   => '',
        'workplace_description' => '',
        'select_company' => $company_list,

    );
}
$template1 = $systemClass->merge_data($template2, $template_data);

echo $template1;




