<?php
/**
 * account management - fetch a list of all modules per role
 * 2016-07-07 - Musa Khulu - Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();

$output = array();
$output['aaData'] = '';
$role_id = $_GET['role_id'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all accounts related to logged in user
$permissions = $accountClass->get_permissions($role_id);

if(empty($permissions)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('permission_id', 'permission_name');

    foreach($permissions as $permission)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'permission_id')
                $permission_id = $permission[$aColumns[$i]];

            $row[] = $permission[$aColumns[$i]];

            if($permission['permission_desc'] === 'Allow access to the Keg Management module'){
                require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
                $settingsClass = new Settings();
                $name = $settingsClass->get_container_terminology();
                $module_name = ' Allow access to the '.$name.' Management module';
            }
            elseif($permission['permission_desc'] === 'Allow access to the Customer Manager module'){
                require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
                $settingsClass = new Settings();
                $customers = $settingsClass->get_customer_terminology($_SESSION['user']['company_id']);
                $module_name  = ' Allow access to the '.$customers.' Management module';
            }
            else{
                $module_name  = $permission['permission_desc'] ;
            }


        }
        $row[]  = $module_name;
        $row[] = ' <p><span class="badge badge-info">Granted</span></p>';
        $row[] = '<button type="button" class="btn btn btn-danger  btn-sm remove_permission" id="remove_permission" value="'. $permission['permission_id'] . '" data-value="'. $module_name. '"> <i class="fa fa-ban"></i> Revoke Permission</button>';

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
