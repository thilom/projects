<?php
/**
 * Delete a role
 *
 * 2016-08-18: Thilo Muller - Created
 */


//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$accountClass = new Accounts();
$role_id = (int) $_POST['roleID'];
$role_name = $_POST['roleName'];

//Rename Role
$accountClass->remove_role($role_id);