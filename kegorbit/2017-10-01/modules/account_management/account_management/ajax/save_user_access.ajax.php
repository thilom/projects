<?php
/**
 * Save updated user access to modules
 *
 * 2016-07-05 - Musa Khulu - Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

/*** Security checks ***/
$securityClass = new Security();
$acountClass = new Accounts();
//Check if the user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in)
    die;

//Check if user has permission to change user access
if(isset($_SESSION['user']['run_as'])){
    $allowed = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'account_management', 'general_access');

}
else{
    $allowed = $securityClass->check_user_permission($_SESSION['user']['id'], 'account_management', 'general_access');
}
if(!$allowed)
    die;

//Vars
$userClass = new User();
$user_id = $_POST['user_id'];
if($user_id == $_SESSION['user']['id']){
    echo '2';
}
else{
    unset($_POST['user_id']);
    $acountClass->update_user_permissions($user_id, $_POST);
    echo '1';
}



