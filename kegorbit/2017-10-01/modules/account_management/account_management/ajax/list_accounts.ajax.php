<?php
/**
 * account management - fetch a list of all accounts
 * 2016-06-24- Musa Khulu - Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();

$output = array();
$output['aaData'] = '';
$active = $_GET['active'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all accounts related to logged in user
$accounts = $accountClass->get_accounts($active);



if(empty($accounts)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('user_full_name', 'company_name', 'user_username', 'user_email_address', 'users_contact_numbers');

    foreach($accounts as $account)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'user_id')
                $user_id = $account[$aColumns[$i]];

            if($aColumns[$i] == 'company_name' && empty($account[$aColumns[$i]])) {
                $account[$aColumns[$i]] = 'Admin';
            }

            $row[] = $account[$aColumns[$i]];

        }
        $row[] = $accountClass->device_count($account['user_id']);
        if($active == 'Y'){
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=account_management&a=edit_account&user_id=' . $account['user_id'] . '" class="btn btn-primary btn-sm"><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span> Edit</a>
							<button class="btn btn-primary btn-sm  dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" >
								<li>
									<a  data-value="' . $account['user_id'] . '" class="reset_user_password" data-username="' . $account['user_full_name'] . '"><i class="fa fa-envelope-o"></i></span> Reset Password</a>
								</li>
								<li>
									<a class="deactivateBtn" href="/modules/account_management/ajax/edit_account_status.ajax.php?user_id=' . $account['user_id'] . '&active=N" data-username="' . $account['user_full_name'] . '"><i class="fa fa-ban"></i></span> Deactivate</a>
								</li>
							</ul>
						</div>';
        }

        else {
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=account_management&a=edit_account&user_id=' . $account['user_id'] . '" class="btn btn-primary  btn-sm"><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span> Edit</a>
							<button class="btn btn-primary  btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" >
								<li>
									<a class="activateBtn" href="/modules/account_management/ajax/edit_account_status.ajax.php?user_id=' . $account['user_id'] . '&active=Y" data-value='.$account['user_id'] .' data-username="' . $account['user_full_name'] . '"><i class="fa fa-check"></i></span> Activate</a>
								</li>
							</ul>
						</div>';
        }

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
