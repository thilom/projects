<?php

/**
 *  send email to reset user account login details if requested
 *  2016-06-27 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/swiftmailer/lib/swift_required.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();
$companyClass = new Company();
$smtp_url = $GLOBALS['smtp']['url'];
$smtp_port = $GLOBALS['smtp']['port'];
$smtp_username = $GLOBALS['smtp']['username'];
$smtp_password = $GLOBALS['smtp']['password'];

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;




$user_id = $_GET['user_id'];
echo $user_id;


//get user details
$company_user = $accountClass->get_account($user_id);
$company_data = $companyClass->get_company($company_user['company_id']);
$code = sha1(mt_rand(10000,99999).time().$company_user['user_email_address']);
//update activation code
$accountClass->update_user_activation_code($code,$user_id);


// Create the mail transport configuration
$transport = Swift_SmtpTransport::newInstance($smtp_url, $smtp_port)->setUsername($smtp_username)->setPassword($smtp_password);
//Supposed to allow local domain sending to work from what I read
//$transport->setLocalDomain('[127.0.0.1]');

// Create the message
$message = Swift_Message::newInstance($transport);

$message->setTo($company_user['user_email_address']);
if(!empty($company_data['company_name'])){
    $message->setSubject('Change to your '.$company_data['company_name']." Password");
}
else{
    $message->setSubject("Change to your Keg Orbit  Password ");
}

$body = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/html/email_company_template/lost_password.html');


$email_name = array('contact_name' => $company_user['user_full_name']);
$body = $systemClass->merge_data($body, $email_name);

$email_data['reset_link'] = "<a href='http://{$_SERVER['HTTP_HOST']}/index.php?code=$code&u={$user_id}'>{$_SERVER['HTTP_HOST']}/index.php?code=$code&u={$user_id}</a>";
$body = $systemClass->merge_data($body, $email_data);

if(!empty($company_data['company_name'])){
    $email_data['company_name'] =  $company_data['company_name'];
}
else{
    $email_data['company_name'] = 'Keg Orbit';
}
$body = $systemClass->merge_data($body, $email_data);

//
$server_details = array('server' => $_SERVER['HTTP_HOST'], 'timestamp' => date('d-m-Y H:i:s'));
$body = $systemClass->merge_data($body, $server_details);

$message->setBody($body, 'text/html');
if(!empty($company_data['company_email'])){
    $message->setFrom(array($company_data['company_email'] => $company_data['company_name']));
}
else{
    $message->setFrom(array($GLOBALS['email']['from']['email'] => $GLOBALS['email']['from']['name']));
}
// Send the email
$mailer = Swift_Mailer::newInstance($transport);
$mailer->send($message);





