<?php
/**
 * account management - fetch a list of all accounts
 * 2016-06-24- Musa Khulu - Created
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();

$output = array();
$output['aaData'] = '';

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all roles related to logged in user
$roles = $accountClass->get_all_user_roles();



if(empty($roles)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('role_id', 'role_display_name');

    foreach($roles as $role)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'role_id')
                $role_id = $role[$aColumns[$i]];

            $row[] = $role[$aColumns[$i]];

        }
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=account_management&a=edit_roles&role_id=' . $role['role_id'] . '" class="btn btn-primary btn-sm"><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span> Edit Access</a>
							<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" >
							<li>
									<a class="rename_role"  onclick="renameRole(' . $role['role_id'] .', \'' . $role['role_display_name'] .'\')"><i class="fa fa-pencil"></i></span> Rename Role</a>
								</li>
								<li>
									<a  data-value="' . $role['role_id'] . '" class="remove_role" onclick="removeRole(' . $role['role_id'] .', \'' . $role['role_display_name'] .'\')"><i class="fa fa-ban"></i></span> Remove Role</a>
								</li>
							</ul>
						</div>';

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
