<?php
/**
 * account management - fetch a list of all modules per role
 * 2016-07-07 - Musa Khulu - Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();

$output = array();
$output['aaData'] = '';
$role_id = $_GET['role_id'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all accounts related to logged in user
$modules = $accountClass->get_modules($role_id);

if(empty($modules)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('module_id');

    foreach($modules as $module)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'module_id')
                $user_id = $module[$aColumns[$i]];

            $row[] = $module[$aColumns[$i]];

            if($module['module_name'] === 'Keg Management'){
                require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
                $settingsClass = new Settings();
                $name = $settingsClass->get_container_terminology();
               $module_name = $name.' Management';
            }
            elseif($module['module_name'] === 'Customers'){
                require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
                $settingsClass = new Settings();
                $customers = $settingsClass->get_customer_terminology($_SESSION['user']['company_id']);
                $module_name  = $customers;
            }
            else{
                $module_name  = $module['module_name'];
            }
            $row[]  = $module_name;

        }
            $row[] = ' <p><span class="badge badge-info">Active</span></p>';
            $row[] = '<button type="button" class="btn btn btn-danger btn-sm remove_module" id="remove_module" value="'. $module['module_id'] . '" data-value="'. $module_name . '"> <i class="fa fa-ban"></i> Revoke Module</button>';

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
