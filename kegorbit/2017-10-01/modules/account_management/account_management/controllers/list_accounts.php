<?php
/**
 * account management list of all accounts
 * 2016-06-14 -  Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();
$companyClass = new Company();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$company_data = $companyClass->get_company($_SESSION['user']['company_id']);
if(isset($company_data['company_user_limit'])){
    $company_data['company_user_limit'];
}
$count = $accountClass->company_user_count($_SESSION['user']['company_id']);

$company_user_limit = isset($company_data['company_user_limit'])?$company_data['company_user_limit']:'';
$company_hard_limit = isset($company_data['company_hard_limit'])?$company_data['company_hard_limit']:'';

if( $company_user_limit >$count && $company_hard_limit != '1'){
    $create_button = '<button type="button" class="btn btn btn-primary   btn-sm dim" id="create_account"> <i class="fa fa-plus-square-o"></i> Create Account</button>';
}
elseif($company_user_limit <= $count && $company_hard_limit == '1'){
    $create_button = '<button type="button" class="btn btn btn-primary disabled  btn-sm dim" id="create_account" title="User Limit Exceeded"> <i class="fa fa-plus-square-o"></i> Create Account</button>';
}
else{
    $create_button = '<button type="button" class="btn btn btn-primary   btn-sm dim" id="create_account"> <i class="fa fa-plus-square-o"></i> Create Account</button>';
}


$template_data = array(
    'create_account_button' =>  $create_button,
);

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/html/list_accounts.html");
$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
