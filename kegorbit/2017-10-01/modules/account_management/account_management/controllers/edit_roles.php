<?php
/**
 * account management list of all modules, permissions for each role
 * 2016-06-14 -  Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;
$role_id = $_GET['role_id'];

//get all modules that user can add
$modules = $accountClass->get_secure_modules($_SESSION['user']['id']);
$module_options = '<option value="">Add a new Module</option>';
foreach ($modules as $module){
    $module_name =  $module['module_name'];

    if($module_name === 'Keg Management'){
        require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
        $settingsClass = new Settings();
        $name = $settingsClass->get_container_terminology();
        $module_name = $name.' Management';
    }
    if($module_name === 'Customers'){
        require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
        $settingsClass = new Settings();
        $customers = $settingsClass->get_customer_terminology($_SESSION['user']['company_id']);
        $module_name = $customers;
    }
    $module_options .= '<option value="' . $module['module_id'] . '" >' . $module_name. '</option>';


}

$permissions = $accountClass->get_all_permissions();

$permission_options = '<option value="">Grant Permisssion to user role</option>';
foreach ($permissions as $permission){

    $permission_name =  $permission['permission_desc'];


    if($permission_name === 'Allow access to the Keg Management module'){
        require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
        $settingsClass = new Settings();
        $name = $settingsClass->get_container_terminology();
        $permission_name = ' Allow access to the '.$name.' Management module';
    }
    if($permission_name === 'Allow access to the Customer Manager module'){
        require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
        $settingsClass = new Settings();
        $customers = $settingsClass->get_customer_terminology($_SESSION['user']['company_id']);
        $permission_name = ' Allow access to the '.$customers.' Management module';
    }

    $permission_options .= '<option value="' . $permission['permission_id'] . '" >' . $permission_name . '</option>';
}


$template_data = array(
    'role_id'       =>  $role_id,
    'modules' => $module_options,
    'permissions' => $permission_options,
);
$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/html/edit_roles.html");
$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;