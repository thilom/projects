<?php
/**
 * Accounts class
 *
 * 2016-08-01: Thilo Muller - Added $company_code to create_user_permissions().
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";

class Accounts extends System
{

    /**
     * Company code
     *
     * @var string
     */
    private $company_code = 'default';

    /**
     * State constructor.
     */
    function __construct() {

            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                if(isset($_SESSION['user']['prefix'])) {
                    $this->company_code = $_SESSION['user']['prefix'];
                }
            }
    }

    /*
    * Get Accounts
    * @param active (string)
    */
    function get_accounts($active = ''){
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();


        if($this->company_code == 'default') {
            $statement = "SELECT DISTINCT user_id, AES_DECRYPT(user_full_name, :phrase) AS user_full_name,
                                    AES_DECRYPT(user_username, :phrase) AS user_username,
                                    AES_DECRYPT(user_email_address, :phrase) AS user_email_address,
                                    AES_DECRYPT(users_contact_numbers, :phrase) AS users_contact_numbers,
                                    users.company_id, company.company_name
                        FROM {$GLOBALS['db_prefix']}_users AS users
                         LEFT JOIN {$GLOBALS['db_prefix']}_companies AS company ON company.company_id = users.company_id";
            if ($active == 'N')
                $statement .= " WHERE users.active = 'N' AND users.super_user_admin = 0";
            else
                $statement .= " WHERE users.active = 'Y' OR users.active IS NULL AND users.super_user_admin = 0";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':user_type', $_SESSION['user']['user_type']);
            $sql->bindParam(':phrase', $phrase);
            $sql->execute();
        }
        else {
            $statement = "SELECT DISTINCT user_id, AES_DECRYPT(user_full_name, :phrase) AS user_full_name,
                                    AES_DECRYPT(user_username, :phrase) AS user_username,
                                    AES_DECRYPT(user_email_address, :phrase) AS user_email_address,
                                    AES_DECRYPT(users_contact_numbers, :phrase) AS users_contact_numbers,
                                    users.company_id, company.company_name
                        FROM {$GLOBALS['db_prefix']}_users AS users
                         LEFT JOIN {$GLOBALS['db_prefix']}_companies AS company ON company.company_id = users.company_id";
            if ($active == 'N')
                $statement .= " WHERE users.active = 'N' AND company_db_prefix = :prefix AND users.super_user_admin = 0";
            else
                $statement .= " WHERE  (users.active = 'Y' OR users.active IS NULL) AND company_db_prefix = :prefix AND users.super_user_admin = 0";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':user_type', $_SESSION['user']['user_type']);
            $sql->bindParam(':created_by', $_SESSION['user']['id']);
            $sql->bindParam(':user_id', $_SESSION['user']['id']);
            $sql->bindParam(':prefix', $this->company_code);
            $sql->bindParam(':phrase', $phrase);
            $sql->execute();
        }
        $accounts = $sql->fetchAll();
        $sql->closeCursor();
        return $accounts;
    }

    /**
     * get the number of devices registered per user
     * @param $user_id
     * @return count
     */
    function device_count($user_id){

        $statement = "SELECT COUNT(*) AS registered_devices
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_registered_devices 
                        WHERE user_id = :user_id";
        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->bindParam(':user_id', $user_id);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();

        return ($sql_count_data['registered_devices']);
    }

    /**
     * Activate/ Deactivate Account
     * @param $account_id, $active
     * @return array
     */
    function change_account_status($account_id, $active){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_users 
                        SET active = :active
                        WHERE user_id = :user_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':active', $active);
        $sql_update->bindParam(':user_id', $account_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }
    /*
     * Update last login date
     * @param user_id (int)
     */
    function update_last_login($user_id){
        $statement = "UPDATE {$GLOBALS['db_prefix']}_users
                        SET last_login = NOW()
                        WHERE user_id = :user_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $user_id);
        $sql->execute();
        $sql->closeCursor();
    }




    /*
    * Get account
    * @param $user_id (int)
    */
    function get_account($user_id){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT users.user_id, AES_DECRYPT(user_full_name, :phrase) AS user_full_name, AES_DECRYPT(user_username, :phrase) AS user_username, AES_DECRYPT(user_email_address, :phrase) AS user_email_address, AES_DECRYPT(users_contact_numbers, :phrase) AS users_contact_numbers, users.company_id, company.company_id, company.company_db_prefix, company_name, AES_DECRYPT(company_email, :phrase) AS company_email, user_type, user_notes, user_job_description, last_login
                        FROM {$GLOBALS['db_prefix']}_users AS users
                         LEFT JOIN {$GLOBALS['db_prefix']}_companies AS company ON company.company_id = users.company_id
                         WHERE users.user_id = :user_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $user_id);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $account = $sql->fetch();
        $sql->closeCursor();
        return $account;
    }

    /*
    * Save new user account user login details
    * @param $account_data (array)
     * return $user_id
    */
    function save_new_account($user_data, $company_id){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_users
                      (user_email_address,user_type,user_username,date_created,created_by,company_id,users_contact_numbers,user_notes,user_full_name,user_job_description)
					  VALUES
					  (AES_ENCRYPT(:user_email_address, :phrase),:user_type,AES_ENCRYPT(:user_username, :phrase), NOW() ,:created_by,:company_id,AES_ENCRYPT(:users_contact_numbers, :phrase),:user_notes,AES_ENCRYPT(:user_full_name, :phrase),:user_job_description)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_email_address', $user_data['user_email_address']);
        $sql->bindParam(':user_type', $user_data['user_job_description']);
        $sql->bindParam(':user_username', $user_data['user_username']);
        $sql->bindParam(':created_by', $_SESSION['user']['id']);

        if(isset($user_data['company']) && $user_data['company'] != ""){
            $sql->bindParam(':company_id', $user_data['company']);
        } else {
            $sql->bindParam(':company_id', $company_id);
        }
        $sql->bindParam(':users_contact_numbers', $user_data['user_contact_numbers']);
        $sql->bindParam(':user_notes', $user_data['user_notes']);
        $sql->bindParam(':user_full_name', $user_data['user_full_name']);
        $sql->bindParam(':user_job_description', $user_data['workplace_description']);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();
        $user_id = $GLOBALS['dbCon']->lastInsertId();
        $sql->closeCursor();

        //Get company code
        if (empty($company_id)) {
            if(isset($user_data['company']) && $user_data['company'] != ""){
               $company_id = $user_data['company'];
            }
        }
        $statement = "SELECT company_db_prefix
                        FROM {$GLOBALS['db_prefix']}_companies
                        Where company_id = :company_id
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':company_id', $company_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        $this->create_new_user_modules($user_id, $user_data['user_job_description']);
        $this->create_user_permissions($user_id, $user_data['user_job_description'], $sql_result['company_db_prefix']);

        return $user_id;
    }
    /*
   * update user login details
   * @param $user_data (array),
    * return $user_data (array)
   */
    function update_user_details($user_data){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "UPDATE {$GLOBALS['db_prefix']}_users
						SET	user_email_address = AES_ENCRYPT(:user_email_address, '" . $phrase . "'),
						    user_username = AES_ENCRYPT(:user_username, '" . $phrase . "'),
						    created_by = :created_by,
						    users_contact_numbers = AES_ENCRYPT(:users_contact_numbers, '" . $phrase . "'),
						    user_notes = :user_notes,
						    user_full_name = AES_ENCRYPT(:user_full_name, '" . $phrase . "'),
						    user_job_description = :user_job_description
							WHERE user_id = :user_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $user_data['user_id']);
        $sql->bindParam(':user_email_address', $user_data['user_email_address']);
        $sql->bindParam(':user_username', $user_data['user_username']);
        $sql->bindParam(':created_by', $_SESSION['user']['id']);
        $sql->bindParam(':users_contact_numbers', $user_data['user_contact_numbers']);
        $sql->bindParam(':user_notes', $user_data['user_notes']);
        $sql->bindParam(':user_full_name', $user_data['user_full_name']);
        $sql->bindParam(':user_job_description', $user_data['workplace_description']);
        $sql->execute();
        $sql->closeCursor();

//        $this->create_new_user_modules($user_data['user_id'], $user_data['user_job_description']);
//        $this->create_user_permissions($user_data['user_id'], $user_data['user_job_description']);
    }

    /*
    * Get all roles on the system
    * @param
     * return $user_roles (array)
    */
    function get_all_user_roles(){

        if($_SESSION['user']['user_type'] == '1'){
            $statement = "SELECT role_id, role_name, role_display_name
                         FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                         WHERE admin = 1";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->execute();
        }
        elseif ($_SESSION['user']['user_type'] == '2'){
            $statement = "SELECT role_id, role_name, role_display_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->execute();
        }
        elseif ($_SESSION['user']['user_type'] == '3'){
            $statement = "SELECT role_id, role_name, role_display_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                        WHERE company = 1";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->execute();
        }
        else{

            $statement = "SELECT role_id, role_name, role_display_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                         WHERE employee = 1";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->execute();
        }
        $roles = $sql->fetchAll();
        $sql->closeCursor();
        return $roles;
    }

    /**
     * Get all the modules permissions
     *
     * @param int $user_id
     * @return array modules
     */
    function get_secure_modules($user_id)
    {
        $account_data = $this->get_account($user_id);
        if(empty($account_data['company_db_prefix'])){
            $this->company_code = 'default';
        }
        else{
            $this->company_code = $account_data['company_db_prefix'];
        }
        $access = 1;
        $statement = "SELECT DISTINCT m.module_id, module_name, module_folder, ua.user_id
                        FROM {$GLOBALS['db_prefix']}_user_modules um
                        LEFT JOIN {$GLOBALS['db_prefix']}_modules AS m ON um.module_id = m.module_id
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions mp ON mp.module_id = m.module_id
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_user_access ua ON ua.permission_id = mp.permission_id
                        WHERE ua.user_id = :user_id
                        AND access = :access ORDER BY m.module_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $user_id);
        $sql->bindParam(':access', $access);
        $sql->execute();
        $modules = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $modules;
    }

    /**
     * Get permissions for a given module
     * @param string $module_folder
     * @param int $module_id
     * @return array permissions
     */
    function get_module_permissions($module_folder, $user_id = 0, $module_id = 0)
    {
        if($module_id == 0)
        {
            $statement = "SELECT module_id
                        FROM {$GLOBALS['db_prefix']}_modules
                        WHERE module_folder = :module_folder
                        LIMIT 1";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':module_folder', $module_folder);
            $sql->execute();
            $module_id = $sql->fetchColumn();
        }
        $account_data = $this->get_account($user_id);
        if(empty($account_data['company_db_prefix'])){
            $this->company_code = 'default';
        }
        else{
            $this->company_code = $account_data['company_db_prefix'];
        }
        $access = 1;
        $statement = "SELECT DISTINCT mp.permission_id, mp.permission_name, mp.permission_desc
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions AS mp
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_user_access ua ON ua.permission_id = mp.permission_id
                        WHERE module_id = :module_id
                        AND access = :access
                        AND user_id = :user_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':module_id', $module_id);
        $sql->bindParam(':access', $access);
        $sql->bindParam(':user_id', $user_id);
        $sql->execute();
        $permissions = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $permissions;

    }
    /**
     * Get permission id return its name and module id
     * @param int $permission_id
     * @return array permission details
     */
    function get_permission($permission_id)
    {
        $statement = "SELECT module_id, permission_name
                                FROM {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions
                                WHERE permission_id = :permission_id
                                LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':permission_id', $permission_id);
        $sql->execute();
        $permission = $sql->fetch(PDO::FETCH_ASSOC);

        return $permission;
    }
    /**
     * Check if a user has a module in the user_modules table
     * @param int $user_id
     * @param int $module_id
     * @return int $count
     */
    function check_module_for_user($user_id, $module_id)
    {
        $statement = "SELECT count(user_id)
                                    FROM {$GLOBALS['db_prefix']}_user_modules
                                    WHERE user_id = :user_id
                                    AND module_id = :module_id
                                    LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $user_id);
        $sql->bindParam(':module_id', $module_id);
        $sql->execute();
        $count = $sql->fetchColumn();
        return $count;
    }
    /**
     * Get user modules order
     * @param int $user_id
     * @return array modules
     */
    function get_user_modules_order($user_id)
    {
        $statement = "SELECT um.module_id, um.module_order, module_name
                        FROM {$GLOBALS['db_prefix']}_user_modules um
                        JOIN {$GLOBALS['db_prefix']}_modules m
                        ON um.module_id = m.module_id
                        WHERE user_id = :user_id
                        ORDER BY um.module_order";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $user_id);
        $sql->execute();

        $modules = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();

        return $modules;
    }



    /**
    * Add user access
     *
    * @param int $user_id
    * @param array permissions
    */
    function update_user_permissions($user_id, $permissions){
        //If one of the modules exists in the user_modules and should be removed, this var will be updated to true and we will run
        // on the user_modules table to reorganize the modules ordering
        $access_denied = false;
        $account_data = $this->get_account($user_id);
        if(empty($account_data['company_db_prefix'])){
            $this->company_code = 'default';
        }
        else{
            $this->company_code = $account_data['company_db_prefix'];
        }

        foreach ($permissions as $permission => $access) {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_user_access
                        (user_id, permission_id, access)
                        VALUES(:user_id, :permission_id, :access)
                        ON DUPLICATE KEY UPDATE
                        access = :access";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':user_id', $user_id);
            $sql->bindParam(':permission_id', $permission);
            $sql->bindParam(':access', $access);
            $sql->execute();

            //If permission granted, check if the module for this permission exists in the user_modules table
            if ($access == 1) {
                $module = $this->get_permission($permission);

                //If the permission is 'general_access' check if the module exists in the user_modules table
                if ($module['permission_name'] == 'general_access') {
                    $count = $this->check_module_for_user($user_id, $module['module_id']);

                    //The module does not exist in the table
                    if ($count == 0) {
                        //Get the last order from the table
                        $statement = "SELECT module_order
                                        FROM {$GLOBALS['db_prefix']}_user_modules
                                        WHERE user_id = :user_id
                                        ORDER BY module_order DESC";
                        $sql = $GLOBALS['dbCon']->prepare($statement);
                        $sql->bindParam(':user_id', $user_id);
                        $sql->execute();
                        $last_order = $sql->fetchColumn();
                        $last_order++;

                        //Insert the module to the user_modules table
                        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_user_modules
                                        (user_id, module_id, module_order)
                                        VALUES (:user_id, :module_id, :module_order)";
                        $sql = $GLOBALS['dbCon']->prepare($statement);
                        $sql->bindParam(':user_id', $user_id);
                        $sql->bindParam(':module_id', $module['module_id']);
                        $sql->bindParam(':module_order', $last_order);
                        $sql->execute();
                    }
                }
            } //If access denied, check if it is general access, if it is, check if the module exists in the user_modules table and remove it
            else {
                $module = $this->get_permission($permission);

                //If the permission is 'general_access' check if the module exists in the user_modules table
                if ($module['permission_name'] == 'general_access') {
                    $count = $this->check_module_for_user($user_id, $module['module_id']);

                    //If exists remove the module
                    if ($count > 0) {
                        $statement = "DELETE FROM {$GLOBALS['db_prefix']}_user_modules
                                        WHERE user_id = :user_id
                                        AND module_id = :module_id";
                        $sql = $GLOBALS['dbCon']->prepare($statement);
                        $sql->bindParam(':user_id', $user_id);
                        $sql->bindParam(':module_id', $module['module_id']);
                        $sql->execute();
                        $access_denied = true;
                    }

                    //Disable all the other permissions for the module
                    $module_permissions = $this->get_module_permissions('', $module['module_id']);
                    foreach ($module_permissions as $perm) {
                        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_user_access
                                    SET access = 0
                                    WHERE user_id = :user_id
                                    AND permission_id = :permission_id";
                        $sql = $GLOBALS['dbCon']->prepare($statement);
                        $sql->bindParam(':user_id', $user_id);
                        $sql->bindParam(':permission_id', $perm['permission_id']);
                        $sql->execute();
                    }
                }
            }
        }

        //At least one of the modules has been removed from the user_modules table, need to regorganize the modules order
        if ($access_denied) {
            $modules = $this->get_user_modules_order($user_id);

            $i = 1;
            foreach ($modules as $module) {
                $statement = "UPDATE {$GLOBALS['db_prefix']}_user_modules
                                SET module_order = :module_order
                                WHERE user_id = :user_id
                                AND module_id = :module_id";
                $sql = $GLOBALS['dbCon']->prepare($statement);
                $sql->bindParam(':user_id', $user_id);
                $sql->bindParam(':module_id', $module['module_id']);
                $sql->bindParam(':module_order', $i);
                $sql->execute();
                $i++;
            }
        }
        $sql->closeCursor();
    }
    /*
   * Get list of all modules a certain role has access to
   * @param $role_id (int)
   */
    function get_modules($role_id){

        $statement = "SELECT role_id, modules.module_id, module_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_access AS roles
                         LEFT JOIN {$GLOBALS['db_prefix']}_modules AS modules ON roles.module_id = modules.module_id
                         WHERE role_id = :role_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':role_id', $role_id);
        $sql->execute();

        $modules = $sql->fetchAll();
        $sql->closeCursor();
        return $modules;
    }

    /*
 * Get list of all permissions a certain role has access to
 * @param $role_id (int)
 */
    function get_permissions($role_id){

        $statement = "SELECT role_id, permission.permission_id, permission_name, permission_desc
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_permissions AS role_permisssions
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions AS permission ON role_permisssions.permission_id = permission.permission_id
                         WHERE role_id = :role_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':role_id', $role_id);
        $sql->execute();

        $permisssions = $sql->fetchAll();
        $sql->closeCursor();
        return $permisssions;
    }

    /**
     * Revoke Module access to a role
     * @param $role_id, $module_id
     * @return array
     */
    function revoke_module_access($role_id, $module_id){

        $statement = "DELETE FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_access
                                        WHERE role_id = :role_id
                                        AND module_id = :module_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':role_id', $role_id);
        $sql_update->bindParam(':module_id', $module_id);
        $sql_update->execute();
        $sql_update->closeCursor();
        //revoke access to all permissions inside the module
        $module_permissions  = $this->get_module_permissions('', $module_id);
        foreach ($module_permissions as $module_permission){
            $this->revoke_permission_access($role_id, $module_permission['permission_id']);
        }
    }

    /**
     * grant Module access to a role
     * @param $role_id, $module_id
     * @return array
     */
    function grant_module_access($role_id, $module_id){

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_access
                        (role_id, module_id)
                        VALUES(:role_id, :module_id)
                        ON DUPLICATE KEY UPDATE
                        role_id = :role_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':role_id', $role_id);
        $sql->bindParam(':module_id', $module_id);
        $sql->execute();
    }
    /**
     * Revoke Permission access to a role
     * @param $role_id, $permission_id
     * @return array
     */
    function revoke_permission_access($role_id, $permission_id){

        $statement = "DELETE FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_permissions
                                        WHERE role_id = :role_id
                                        AND permission_id = :permission_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':role_id', $role_id);
        $sql_update->bindParam(':permission_id', $permission_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }
    /**
     * grant Permission access to a role
     * @param $role_id, $permission_id
     * @return array
     */
    function grant_permission_access($role_id, $permission_id){

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_permissions
                        (role_id, permission_id)
                        VALUES(:role_id, :permission_id)
                        ON DUPLICATE KEY UPDATE
                        role_id = :role_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':role_id', $role_id);
        $sql->bindParam(':permission_id', $permission_id);
        $sql->execute();
    }


    /**
     * get all Permission
     * @param
     * @return array
     */
    function get_all_permissions(){
        $access = 1;
        $statement = "SELECT a.user_id, access, m.permission_id, permission_name, permission_desc
                         FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_access AS  a
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions AS m ON a.permission_id = m.permission_id
                         WHERE user_id = :user_id
                         AND access = :access";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $_SESSION['user']['id']);
        $sql->bindParam(':access', $access);
        $sql->execute();
        $permissions = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $permissions;
    }

    /**
     * Add installed modules to the user_modules table
     * @param int $user_id
     */
    function create_new_user_modules($user_id, $job_description){

        $securityClass = new Security();
        $modules = $securityClass->get_installed_modules(FALSE,$job_description);

        $order = 1;
        foreach ($modules as $module) {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_user_modules
                                        (user_id, module_id, module_order)
                                        VALUES (:user_id, :module_id, :module_order)";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':user_id', $user_id);
            $sql->bindParam(':module_id', $module['module_id']);
            $sql->bindParam(':module_order', $order);
            $sql->execute();
            $order++;
        }
    }

    /**
     * Create new User permissions based on new roles
     * @param int $user_id
     */
    function create_user_permissions($user_id, $role_id, $company_code = '') {

        //Set company code
        $company_code = empty($company_code)?$this->company_code:$company_code;

        //Get all the modules permissions
        $statement = "SELECT permission_id, permission_name, module_id
                       FROM {$GLOBALS['db_prefix']}_{$company_code}_module_permissions";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $permissions = $sql->fetchAll(PDO::FETCH_ASSOC);

        //Get all the modules permissions
        $statement = "SELECT role_id, permission_id
                      FROM {$GLOBALS['db_prefix']}_{$company_code}_user_role_permissions
                      WHERE role_id =:role_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':role_id', $role_id);
        $sql->execute();
        $role_permissions = $sql->fetchAll(PDO::FETCH_ASSOC);

        foreach ($permissions as $permission) {
            $access = 0;
            foreach ($role_permissions as $role_permission){
                if($permission['permission_id'] == $role_permission['permission_id']){
                    $access = 1;
                }
            }
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$company_code}_user_access
                        (user_id, permission_id, access)
                        VALUES (:user_id, :permission_id, :access)
                        ON DUPLICATE KEY UPDATE
                        user_id = :user_id, permission_id = :permission_id";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':user_id', $user_id);
            $sql->bindParam(':permission_id', $permission['permission_id']);
            $sql->bindParam(':access', $access);
            $sql->execute();
        }
    }

    /**
     * save new role
     * @param int $user_id
     */
    function create_new_role($role_data){

        $statement = "SELECT COUNT(*) AS row_counter
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles";
        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();
        $sql_count_data['row_counter'];

            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                                        (role_id, role_name, role_display_name, admin, company, employee)
                                        VALUES (:role_id, :role_name, :role_display_name, :admin, :company, :employee)";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':role_id', $sql_count_data['row_counter']);
            $role_name = strtolower(str_replace(' ', '_',$role_data['role_name']));
                $sql->bindParam(':role_name', $role_name);
                $sql->bindParam(':role_display_name', $role_data['role_name']);
            $value= NULL;
            $role_assigned = 1;
            if($_SESSION['user']['user_type'] == '1'){
                $sql->bindParam(':admin', $role_assigned);
                $sql->bindParam(':company', $value);
                $sql->bindParam(':employee', $value);
            }
            if($_SESSION['user']['user_type'] == '3'){
                $sql->bindParam(':admin', $value);
                $sql->bindParam(':company', $role_assigned);
                $sql->bindParam(':employee', $value);
            }
            if($_SESSION['user']['user_type'] == '4'){
                $sql->bindParam(':admin', $value);
                $sql->bindParam(':company', $value);
                $sql->bindParam(':employee', $role_assigned);
            }

            $sql->execute();
            return  $sql_count_data['row_counter'];
    }

    /**
     * Get a list of user activities
     *
     * @param string $time
     * @return mixed
     */
    function get_activity($time = '') {
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();



        $statement = "SELECT activity_timestamp, activity_message, AES_DECRYPT(b.user_full_name, :phrase) AS user_name
                         FROM {$GLOBALS['db_prefix']}_user_activity AS a
                         LEFT JOIN {$GLOBALS['db_prefix']}_users AS b ON a.user_id = b.user_id
                         LEFT JOIN {$GLOBALS['db_prefix']}_companies AS c ON b.company_id = c.company_id";

        if (!empty($time)) $statement .= " WHERE TIMESTAMPDIFF(SECOND,activity_timestamp,'$time') < 0";

        if ($this->company_code !== 'default') {
            if (strpos($statement, 'WHERE') > 0) {
                $statement .= ' AND';
            } else {
                $statement .= " WHERE ";
            }
            $statement .= " c.company_db_prefix = :company_code ";
        }

        $statement .= " ORDER BY activity_timestamp DESC
                        LIMIT 8";

        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':phrase', $phrase);
        if ($this->company_code !== 'default') $sql_select->bindParam(':company_code', $this->company_code);
//        if (!empty($time)) $sql_select->bindParam(':time', $time);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }


    function insert_user_activity($user_message) {

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_user_activity
                          (user_id, activity_message)
                        VALUES
                          (:user_id, :activity_message)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bindParam(':user_id', $_SESSION['user']['id']);
        $sql_insert->bindParam(':activity_message', $user_message);
        $sql_insert->execute();
        $sql_insert->closeCursor();
    }

    /*
   * Updating the activation code to be used to activate new users/ change forgot password
   * @param int $user_id,$code
   */
    function update_user_activation_code($code, $user_id){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_users
                            SET activation_code = :activation_code,
                               password_request_time = NOW()
                            WHERE user_id = :user_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_id', $user_id);
        $sql->bindParam(':activation_code', $code);
        $sql->execute();
    }
    /**
     * get the number of accounts for a company
     * @param $company_id
     * @return count
     */
    function company_user_count($company_id){

        $statement = "SELECT COUNT(*) AS user_count
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE company_id = :company_id
                        AND super_user_admin <> 1";
        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->bindParam(':company_id', $company_id);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();

        return ($sql_count_data['user_count']);
    }


    /**
     * Rename a role
     *
     * @param $role_id
     * @param $role_name
     */
    function rename_role($role_id, $role_name) {
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                        SET role_display_name = :role_name,
                            role_name = :role_name
                        WHERE role_id = :role_id
                        LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':role_name', $role_name);
        $sql_update->bindParam(':role_id', $role_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    function remove_role($role_id) {
        $statement = "DELETE FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                        WHERE role_id = :role_id
                        LIMIT 1";
        $sql_delete = $GLOBALS['dbCon']->prepare($statement);
        $sql_delete->bindParam(':role_id', $role_id);
        $sql_delete->execute();
        $sql_delete->closeCursor();
    }

    /**
     * get the number of active/inactive accounts
     * @param $active
     * @return count
     */
    function active_user_count($active){

        $statement = "SELECT COUNT(*) AS user_count
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE super_user_admin <> 1";
        if ($active == 'N')
            $statement .= " AND  active = 'N'";
        else
            $statement .= " AND  active IS NULL ";

        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();

        return ($sql_count_data['user_count']);
    }

    /**
     * get the number of active/inactive accounts
     * @param $active
     * @return count
     */
    function active_user_count_company($active,$company_id){


        $statement = "SELECT COUNT(*) AS user_count
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE company_id = :company_id
                        AND super_user_admin <> 1";
        if ($active == 'N')
            $statement .= " AND  active = 'N'";
        else
            $statement .= " AND  active IS NULL ";

        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->bindParam(':company_id', $company_id);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();

        return ($sql_count_data['user_count']);

    }


    /**
     * get the number of accounts by types
     * @param $account_type
     * @return count
     */
    function account_user_type($account_type){

        $statement = "SELECT COUNT(*) AS user_count
                        FROM {$GLOBALS['db_prefix']}_users
                        WHERE super_user_admin <> 1
                        AND user_type = :user_type";

        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->bindParam(':user_type', $account_type);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();

        return ($sql_count_data['user_count']);
    }


}