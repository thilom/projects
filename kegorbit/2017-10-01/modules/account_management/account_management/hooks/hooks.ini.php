<?php
/**
 * List of dashboard hooks
 *
 * 2016-07-03: Thilo Muller - Created
 */

$ini_hooks = array();

$ini_hooks[0] = array('dir' => 'dashboard_activity',
                        'order' => 10,
                        'type' => 'dashboard',
                        'position' => 'right',
                        'access' => array('administrator'));

$ini_hooks[1] = array('dir' => 'account_status',
    'order' => 1,
    'type' => 'dashboard',
    'position' => 'left',
    'access' => array('administrator'));

$ini_hooks[2] = array('dir' => 'dashboard_expired',
    'order' => 10,
    'type' => 'dashboard',
    'position' => 'left',
    'access' => array('administrator'));

$ini_hooks[3] = array('dir' => 'maintenance_hook',
    'order' => 3,
    'type' => 'dashboard',
    'position' => 'left',
    'access' => array('administrator'));

