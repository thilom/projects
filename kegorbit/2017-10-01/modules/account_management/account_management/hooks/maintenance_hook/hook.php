<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/modules/company_management/classes/company.class.php';
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$companyClass = new Company();
$appClass = new Application();
$securityClass = new Security();
$settingsClass = new Settings();
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = '';
$message = '';


        $block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/hooks/account_status/html/block.html");
        $color = 'info';


    //Get maintenance mode
        $app_data = $appClass->get_settings();
        $maintenance_mode = $app_data['activate_maintenance'];

        if ($maintenance_mode == 1) {

            //get maintenance details
            $maintenance = $settingsClass->get_maintenance();

            $message = "Maintenance has been scheduled to commence on " .$maintenance['maintenance_commence']. " up until the " .$maintenance['maintenance_end'];
            $color = 'danger';
        } else{
            $block = '';
            $message = '';
        }

        $block = str_replace('<!-- message -->', $message, $block);
        $block = str_replace('<!-- color -->', $color, $block);


echo $block;