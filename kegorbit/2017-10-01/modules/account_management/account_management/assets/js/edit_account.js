$(document).ready(function() {



    $( "#save" ).click(function() {

        if ($('#user_id').val() == 0) {
            var bootstrapValidator = $('#editAccountForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('user_email_address', true);
        }
        else{
            var bootstrapValidator = $('#editAccountForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('user_email_address', false);
        }
    });

    //Form validations
    $.fn.bootstrapValidator.validators.duplicateUsername = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/ajax/check_duplicate_email.ajax.php?email=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });

            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };

    $('#editAccountForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            user_full_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the User's Full Name."}
                }
            },
            user_email_address: {
                validators: {
                    notEmpty: {message: "Please Provide  the User's Email Name."},
                    duplicateUsername:{message: "Duplicate Email Address Detected."}
                }
            },
            user_username: {
                validators: {
                    notEmpty: {message: "Please Provide  the  User's Username."}
                }
            },
            company: {
                validators: {
                    notEmpty: {message: "Please Provide  the  Association."}
                }
            }
        }
    }).on('success.form.bv', function (e) {

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/account_management/ajax/save_account_details.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if (data == 'update') {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'Account Successfully Updated';
                    toastr.success(toastMessage);

                     message = "Account for: '"+ $('#user_full_name').val() +"' has been modified";
                    swal({
                            title: "Account Successfully Updated",
                            text: message,
                            type: "success",
                            cancelButtonColor: "#4caf50",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Done",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {

                            } else {
                                document.location = 'index.php?m=account_management&a=list_accounts';
                            }
                        })
                }
                else{
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'New Account Added!';
                    toastr.success(toastMessage);

                        message = $('#user_job_description').find(":selected").text() +" Account Created for: "+ $('#user_full_name').val();
                    swal({
                            title: "New Account Added!",
                            text: message,
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Create Another Account",
                            cancelButtonColor: "#4caf50",
                            cancelButtonText: "Done",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $("#editAccountForm")[0].reset();

                            } else {
                                document.location = 'index.php?m=account_management&a=list_accounts';
                            }
                        })
                }
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        document.location = 'index.php?m=account_management&a=list_accounts';
    });

// mask input
//    $("#user_contact_numbers").inputmask({mask:"(999)999-9999",placeholder:"(999)999-9999"});


    $(function() {
        $('#editUserAccessBtn').on('click', function() {
            $('#editUserAccessForm').ajaxSubmit({
                url: '/modules/account_management/ajax/save_user_access.ajax.php',
                beforeSubmit: function() {
                },
                dataType: 'json',
                success: function(data) {
                    if(data == '2'){
                        sweetAlert("Oops...", " This feature requires Admin user level or higher!", "error");
                    }
                    else{
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            positionClass: 'toast-bottom-right',
                            timeOut: 6000
                        };
                        toastMessage = 'User Access Successfully Updated';
                        toastr.success(toastMessage);
                        swal({
                                title: "User Access Successfully Updated",
                                type: "success",
                                showCancelButton: true,
                                confirmButtonColor: "#4caf50",
                                confirmButtonText: "Continue Editing",
                                cancelButtonText: "Back To Accounts List",
                                closeOnConfirm: true,
                                closeOnCancel: false
                            },
                            function (isConfirm) {
                                if (isConfirm) {

                                } else {
                                    document.location = 'index.php?m=account_management&a=list_accounts';
                                }
                            })
                    }

                }
            });
            return false;
        });

        $('#exitEditUserAccessBtn').on('click', function() {
            document.location = 'index.php?m=account_management&a=list_accounts';
            return false;
        });

        $('#allowAllBtn').on('click', function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                positionClass: 'toast-bottom-right',
                timeOut: 6000
            };
            toastMessage = 'User has been granted access to all modules';
            toastr.success(toastMessage);

            $('input[type="radio"]').each(function() {
                var group = $(this).attr('name');
                if($('input[name='+group+'][value=0]').prop('checked'))
                    $('input[name='+group+'][value=1]').iCheck('check');
                    //$('input[name='+group+'][value=1]').prop('checked', true);


                if($('input[name='+group+']').is(':disabled'))
                    $('input[name='+group+']').attr('disabled', false);
            });

            return false;
        });

        $('#disallowAllBtn').on('click', function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                positionClass: 'toast-bottom-right',
                timeOut: 6000
            };
            toastMessage = 'User access has been revoked to all modules';
            toastr.success(toastMessage);
            $('input[type="radio"]:checked').each(function() {
                var group = $(this).attr('name');
                if($('input[name='+group+'][value=1]').prop('checked'))
                    $('input[name='+group+'][value=0]').iCheck('check');
                    //$('input[name='+group+'][value=0]').prop('checked', true);
            });

            return false;
        });
    });

    $(document).on('click', 'input[type="radio"]', function() {
        //Check if the permission is a general access one
        var is_general_access = $(this).data('general');

        //If it is check it's value
        if(is_general_access == 'y')
        {
            var group = $(this).attr('name');
            var module = $(this).data('module');

            //If it is now checked, enable all the module permissions radios
            if($('input[name='+group+'][value=1]').prop('checked'))
            {
                $('.'+module).each(function() {
                    $(this).attr('disabled', false);
                });
            }
            //Else disable all the module permissions radios
            else
            {
                $('.'+module).each(function() {
                    var group = $(this).attr('name');
                    if($('input[name='+group+'][value=1]').prop('checked'))
                        $('input[name='+group+'][value=0]').prop('checked', true);

                    $(this).attr('disabled', true);
                });
            }
        }
    });

    $('#user_job_description').change(function () {
       if ($('#user_job_description').find(":selected").val() != 1){
           $('#company_label').slideDown();
           $('#company').slideDown();
       }
       else{
           $('#company_label').slideUp();
           $('#company').slideUp();
       }
    });

});