<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/company_management/classes/company.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";


//Vars
$companyClass = new Company();
$systemClass = new System();
$appClass = new Application();
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/hooks/dashboard_expired/html/block.html");
$colors = array('expired'=>'ff0000','grace'=>'FFA500','soon'=>'ffff00','ok'=>'00ff00');
$company_count = array('expired'=>0, 'grace'=>0, 'soon' => 0, 'ok' => 0);
$template_data['table_line'] = '';
$template_data['labels'] = '';
$template_data['data'] = '';
$template_data['colors'] = '';
$template_data['total'] = 0;



//check if user has permission to view company Accounts Block
if(isset($_SESSION['user']['run_as'])){
    $access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'dashboard_management', 'company_accounts_block');
}
else{
    $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'dashboard_management', 'company_accounts_block');
}

if(!$access){
    $template_data['display_or_not'] = 'Display:none';
}
else{
    $template_data['display_or_not'] = '';
}


//Get Grace period
$app_data = $appClass->get_settings();
$grace_period = $app_data['grace_period'];

//Get company accounts
$companies = $companyClass->get_companies('active');
$expire;
//Count company data
foreach ($companies as $company) {

    $expire = $company['company_renewal_date'];

    if ($expire <= $grace_period * -1) {
        $company_count['expired']++;
    } else if ($expire < 0) {
        $company_count['grace']++;
    } else if ($expire < $grace_period) {
        $company_count['soon']++;
    } else {
        $company_count['ok']++;
    }
}


foreach ($company_count as $cKey=>$cData) {

    switch ($cKey) {
        case 'expired':
            $status = 'Expired and Locked';
            break;
        case 'grace':
            $status = 'Expired but in Grace Period';
            break;
        case 'soon':
            $status = 'Expire Soon';
            break;
        default:
            $status = 'OK';
    }
    if($cData == 0){
        $disabled = "<button class='btn btn-xs btn-info disabled view_account' data-value='{$status}' data-count='{$cData}'>View</button>";
    }
    else{
        $disabled = "<button class='btn btn-xs btn-info view_account' data-value='{$status}' data-count='{$cData}'>View</button>";
    }
    $template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[$cKey]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>{$status}</td>
                        <td>{$cData}</td>
                        <td>{$disabled}</td>
                    </tr>";
    $template_data['labels'] .= "'{$cKey}',";
    $template_data['colors'] .= "'#{$colors[$cKey]}',";
    $template_data['data'] .= "{$cData},";
    $template_data['total'] += $cData;
}


$block = $systemClass->merge_data($block, $template_data);

echo $block;