<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/state.class.php";

//Vars
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/hooks/dashboard_activity/html/block.html");


//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/classes/account.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/company_management/classes/company.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";

//Vars
$accountClass = new Accounts();
$systemClass = new System();
$companyClass = new Company();
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/hooks/dashboard_activity/html/block.html");
$template_data = array();
$template_data['dashboard_content'] = '';
$template_data['timestamp'] = '';

//get the company date format
if(isset($_SESSION['user']['run_as'])) {

    $date_format = $companyClass->get_company_date_format($_SESSION['user']['run_as']);
} else{
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
}

$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];


//Get Messages
$messages = $accountClass->get_activity();
//$messages = array_reverse($messages);
foreach ($messages as $key=>$data) {
    $date = date($format. ' H:i:s',strtotime($data['activity_timestamp']));
    $template_data['dashboard_content'] .= "<div class='msg'><small>{$data['user_name']} - {$date}</small> <div class='alert alert-info'>{$data['activity_message']}</div></div>";
    if ($key == 0) $template_data['timestamp'] = strtotime($data['activity_timestamp']);
}

$block = $systemClass->merge_data($block, $template_data);
echo $block;