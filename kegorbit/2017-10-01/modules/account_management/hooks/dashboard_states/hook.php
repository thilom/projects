<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/classes/container.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";

//Vars
$containerClass = new Container();
$systemClass = new System();
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = file_get_contents("$dir/html/block.html");
$colors = array('000022','001166','09146D','0012A8','112288','2B2B2B','223399','3D3E4C','3F467F','8888AA','7986F2','99A4FF');

//Get state count
$state_data = $containerClass->get_state_count();
$template_data['table_line'] = '';
$template_data['labels'] = '';
$template_data['data'] = '';
$template_data['colors'] = '';
$template_data['total'] = 0;

//Assemble for graph
foreach ($state_data as $key=>$data) {
    $template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[$key]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>{$data['state_name']}</td>
                        <td>{$data['state_count']}</td>
                        <td><button class='btn btn-xs btn-info view_state'  data-value='{$data['state_id']}'>View</button></td>
                    </tr>";
    $template_data['labels'] .= "'{$data['state_name']}',";
    $template_data['colors'] .= "'#{$colors[$key]}',";
    $template_data['data'] .= "{$data['state_count']},";
    $template_data['total'] += $data['state_count'];
}
$template_data['labels'] = trim($template_data['labels'],',');
$template_data['colors'] = trim($template_data['colors'],',');
$template_data['data'] = trim($template_data['data'],',');

$block = $systemClass->merge_data($block, $template_data);

echo $block;