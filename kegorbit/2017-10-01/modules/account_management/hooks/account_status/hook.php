<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/modules/company_management/classes/company.class.php';
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";

//Vars
$companyClass = new Company();
$appClass = new Application();
$securityClass = new Security();
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = '';
$message = '';

//check if user has permission to view account expiry block

    if(isset($_SESSION['user']['run_as'])){
        $access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'dashboard_management', 'account_expiry_block');
    }
    else{
        $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'dashboard_management', 'account_expiry_block');
    }
    if(!$access) {
        $block = '';
    } else {
        $block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/account_management/hooks/account_status/html/block.html");
        $color = 'info';

    //Get account status
        $company_data = $companyClass->get_active_company();
        $expire = $company_data['company_renewal_date'];

    //Get Grace period
        $app_data = $appClass->get_settings();
        $grace_period = $app_data['grace_period'];

        if ($expire < $grace_period*-1) {
            $message = "Account Expired";
            $color = 'danger';
        } else if ($expire < 0) {
            $account_lock = $grace_period + $expire;
            $message = "Account Expired. Account will be locked in $account_lock days";
            $color = 'danger';
        } else if ($expire < $grace_period) {
            $message = "Account will expire in $expire days";
            $color = 'warning';
        } else if ($expire < $grace_period*2) {
            $message = "Account will expire in $expire days";
            $color = 'warning';
        } else {
            $block = '';
            $message = '';
        }

        $block = str_replace('<!-- message -->', $message, $block);
        $block = str_replace('<!-- color -->', $color, $block);
}


echo $block;