$(document).ready(function () {
    create_table();
    create_table2();
});


$('#modulesTab').click(function(){
    create_table();
});
$('#permissionstTab').click(function(){
    create_table2();
});

function create_table() {

    $('#modulesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "responsive": true,
        "sAjaxSource": "/modules/account_management/ajax/list_modules.ajax.php?role_id="+$('#role_id').val(),
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false , className: 'text-center'},
            { "bSortable": false, className: 'text-center'}
        ]
    });
    $('#modulesTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

function create_table2() {

    $('#permissionsTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "responsive": true,
        "sAjaxSource": "/modules/account_management/ajax/list_permissions.ajax.php?role_id="+$('#role_id').val(),
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true},
            { "bSortable": false, className: 'text-center'}
        ]
    });
    $('#permissionsTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });
}
$(document).on('click', '.remove_module', function(event) {
    var module_id = $(this).val();
    var module_name = $(this).data('value');
    swal({
        title: "Deny Access to the: " +module_name+ "?",
        text: "All Future Users assigned to this role will no longer be able to access the " + module_name+ " module",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Revoke!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'POST',
            url: '/modules/account_management/ajax/revoke_module_access.ajax.php?role_id='+$('#role_id').val()+'&module_id='+ module_id,
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = 'Access to the ' +module_name+ ' been Denied to the role.';
                toastr.success(toastMessage);
                create_table();
            }
        });
    });
    event.preventDefault();
});


$(document).on('click', '.remove_permission', function(event) {

    var permission_id = $(this).val();
    var permission_name = $(this).data('value');
    swal({
        title: "Are you sure?",
        text: "Revoke Permission:" + permission_name,
        type: "error",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Revoke!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'POST',
            url: '/modules/account_management/ajax/revoke_permission_access.ajax.php?role_id='+$('#role_id').val()+'&permission_id='+ permission_id,
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 4000
                };
                toastMessage = 'Access Denied: ' + permission_name;
                toastr.success(toastMessage);
                create_table2();
            }
        });
    });
    event.preventDefault();
});

$("#modules").change(function() {
    var module_id = $(this).val();
    if(module_id == ''){
        return false;
    }
    else{

        swal({
            title: "Are you sure?",
            text: "All new users assigned to this role will be granted access to the " +$("#modules").find("option:selected").text()+ " Module",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Grant Access!",
            closeOnConfirm: true
        }, function () {
            $.ajax({
                type: 'POST',
                url: '/modules/account_management/ajax/save_new_module_access.ajax.php?role_id='+$('#role_id').val()+'&module_id='+ module_id,
                success: function (data) {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'Permission has been granted to the ' +$("#modules").find("option:selected").text()+' module.';
                    toastr.success(toastMessage);
                    create_table();
                }
            });
        });
    }

});


$("#permissions").change(function() {
    var permission_id = $(this).val();
    if(permission_id == ''){
        return false;
    }
    else{
        swal({
            title: "Are you sure?",
            text: $("#permissions").find("option:selected").text()+'?',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Grant Permission!",
            closeOnConfirm: true
        }, function () {
            $.ajax({
                type: 'POST',
                url: '/modules/account_management/ajax/save_new_permission_access.ajax.php?role_id='+$('#role_id').val()+'&permission_id='+ permission_id,
                success: function (data) {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 4000
                    };
                    toastMessage = 'Access Granted: '+$("#permissions").find("option:selected").text();
                    toastr.success(toastMessage);
                    create_table2();
                }
            });
        });
    }

});

//create new company button
$('#create_account').click(function(){
    document.location = 'index.php?m=account_management&a=edit_account&user_id=0';
});
