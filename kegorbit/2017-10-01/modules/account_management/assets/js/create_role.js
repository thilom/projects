$(document).ready(function() {

    $('#NewRoleForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            role_name: {
                validators: {
                    notEmpty: {message: "Please Provide  New Role Name."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/account_management/ajax/save_new_role.ajax.php',
            beforeSubmit: function () {
                var l = $( '#save' ).ladda();
                // Start loading
                l.ladda( 'start' );
            },
            success: function (data) {
                    var l = $( '#save' ).ladda();
                    // Start loading
                    l.ladda( 'stop' );
                    document.location = 'index.php?m=account_management&a=edit_roles&role_id='+data;
            }
        });
    });//cancel creating role
    $('#cancel').click(function () {
        document.location = 'index.php?m=account_management&a=list_roles';
    });

});