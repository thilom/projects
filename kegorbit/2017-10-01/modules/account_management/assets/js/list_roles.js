$(document).ready(function () {
    create_table();

});


function create_table() {

    $('#rolesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/account_management/ajax/list_roles.ajax.php",
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false, className: 'text-center'}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#rolesTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

$(document).on('click', '.deactivateBtn', function(event) {
    var deactivate_url = $(this).attr('href');
    swal({
        title: "Are you sure?",
        text: "You will be able to undo this action by activating the account later under the disabled Accounts Tab",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Deactivate!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: 'GET',
            url: deactivate_url,
            success: function (data) {
                swal("Account Disabled!", "Account has been suspended.", "success");
                create_table();
            }
        });
    });
    event.preventDefault();
});


$(document).on('click', '.activateBtn', function(event) {
    event.preventDefault();
    var activate_url = $(this).attr('href');
    var user_id = $(this).data('value');
    swal({
        title: "Are you sure?",
        text: "You will be able to undo this action by deactivating the account later under the enabled Accounts Tab",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#458B00",
        confirmButtonText: "Yes, Activate!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: 'POST',
            url: '/modules/account_management/ajax/verify_company_status.ajax.php?user_id='+user_id,
            success: function (data) {
                if(data === 'Y' || data == ''){
                    $.ajax({
                        type: 'GET',
                        url: activate_url,
                        success: function (data) {
                            swal("Account Disabled!", "Account has been suspended.", "success");
                            create_table();
                        }
                    });
                }
                else{
                    swal("Error!", "Account cannot be activated because it belongs to a suspended company.", "warning");
                    create_table();
                }

            }
        });
        event.preventDefault();
    });
    event.preventDefault();
});

$(document).on('click', '.reset_user_password', function(event) {
    var user_id = $(this).data('value');
    $.ajax({
        type: 'GET',
        url: '/modules/account_management/ajax/reset_account_password.ajax.php?user_id='+user_id,
        success: function (data) {
            swal("EMAIL SENT!", "Email to reset password has been sent to the user.", "success");
        }
    });
    event.preventDefault();
});


//create new company button
$('#new_role').click(function(){
    document.location = 'index.php?m=account_management&a=create_role';
});



function renameRole(roleID, roleName) {

    swal({
        title: "Rename Role",
        text: "Enter a new name for '"+ roleName +"':",
        type: "input",
        inputType: "text",
        showCancelButton: true,
        closeOnConfirm: true,
        animation: "slide-from-top",
        inputPlaceholder: roleName,
        allowEscapeKey: true
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("Please enter a new name for the role.");
            return false
        }
        $.ajax({
            url: '/modules/account_management/ajax/rename_role.ajax.php' ,
            method: 'post',
            data: {'roleName':inputValue,'roleID':roleID},
            complete: function(data) {
                    swal.close();
                create_table();
            }

        })
    });
}

function removeRole(roleID, roleName) {
    swal({
        title: "Remove Role",
        text: "Are you sure you want to remove the role '"+ roleName +"'?",
        showCancelButton: true,
        closeOnConfirm: true,
        animation: "slide-from-top",
        allowEscapeKey: true
    }, function () {
        $.ajax({
            url: '/modules/account_management/ajax/delete_role.ajax.php' ,
            method: 'post',
            data: {'roleName':roleName,'roleID':roleID},
            complete: function(data) {
                swal.close();
                create_table();
            }

        })
    });
}