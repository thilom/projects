$(document).ready(function () {
    create_table();
    create_table2();
});


$('#activeAccountTab').click(function(){
    create_table();
});
$('#inactiveAccountTab').click(function(){
    create_table2();
});


function create_table() {
    var active = "Y";

    $('#activeAccountsTbl').DataTable({

        "bProcessing": true,
        "bDestroy": true,
        "responsive": true,
        "sAjaxSource": "/modules/account_management/ajax/list_accounts.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": false, className: 'text-center', "sWidth": "20%"}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#activeAccountsTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

function create_table2() {
    var active = "N";

    $('#inactiveAccountsTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "responsive": true,
        "sAjaxSource": "/modules/account_management/ajax/list_accounts.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": false, className: 'text-center', "sWidth": "20%"}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#inactiveAccountsTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });
}
$(document).on('click', '.deactivateBtn', function(event) {
    var deactivate_url = $(this).attr('href');
    var username = $(this).data('username');
    swal({
        title: "Deactivate: " +username,
        text: "You will be able to undo this action by activating '"+ username +"' account later under the Inactive Accounts Tab",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Deactivate!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'GET',
            url: deactivate_url,
            success: function (data) {
                create_table();
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = username + 's account has been suspended.';
                toastr.success(toastMessage);

            }
        });
    });
    event.preventDefault();
});


$(document).on('click', '.activateBtn', function(event) {
    event.preventDefault();
    var activate_url = $(this).attr('href');
    var user_id = $(this).data('value');
    var username = $(this).data('username');
    swal({
        title: "Activate: " +username,
        text: "You will be able to undo this action performed to '"+ username +"' by activating the account later under the enabled Accounts Tab",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Activate!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'POST',
            url: '/modules/account_management/ajax/verify_company_status.ajax.php?user_id='+user_id,
            success: function (data) {
                if(data === 'Y' || data == ''){
                    $.ajax({
                        type: 'GET',
                        url: activate_url,
                        success: function (data) {
                            create_table2();
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                positionClass: 'toast-bottom-right',
                                timeOut: 6000
                            };
                            toastMessage = username + 's account has been activated.';
                            toastr.success(toastMessage);
                        }
                    });
                }
                else{
                    swal("Error!", "Account cannot be activated because it belongs to a suspended company.", "warning");
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = username + 's account cannot be activated because it belongs to a suspended company.';
                    toastr.success(toastMessage);
                    create_table2();
                }

            }
        });
        event.preventDefault();
    });
    event.preventDefault();
});

$(document).on('click', '.reset_user_password', function(event) {
    var user_id = $(this).data('value');
    var url = $(this).attr('href');
    var username = $(this).data('username');
    swal({
            title: "Reset Password for:" +username+" ?",
            text: "You are about to send a Reset Password Link to: "+username+" The user will Recieve an email that will allow him/her to reset the password",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Send Email",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'GET',
                    url: '/modules/account_management/ajax/reset_account_password.ajax.php?user_id='+user_id,
                    success: function (data) {
                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            positionClass: 'toast-bottom-right',
                            timeOut: 6000
                        };

                        toastMessage = 'EMAIL to reset password has been sent to: ' +username;
                        toastr.success(toastMessage);
                    }
                });
                event.preventDefault();

            } else {

            }
        });


});


//create new company button
$('#create_account').click(function(event){
    if($("#create_account").hasClass('disabled')){
        event.preventDefault();
    }else{
        document.location = 'index.php?m=account_management&a=edit_account&user_id=0';
    }
});
//manage user roles button
$('#manage_templates').click(function(){
    document.location = 'index.php?m=account_management&a=list_roles';
});
