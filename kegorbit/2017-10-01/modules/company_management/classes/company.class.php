<?php
/**
 * Company class
 *
 * 2016-07-25: Thilo Muller - Added db_prefix and raw_renewal_date to get_companies()
 *                            Added get_company_report() function
 * 2016-07-31: Thilo Muller - Updated save_new_company() to generate a unique company code.
 *                          - Removed duplicate table creation in company_database_tables().
 *                          - Updated save_new_company_user() to use NOW() in DB query.
 * 2016-08-01: Thilo Muller - Added get_active_company();
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/security.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/js/plugins/Zebra-2.2.3/Zebra_Image.php';
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";

class Company extends System
{
    /**
     * Company code
     *
     * @var string
     */
    private $company_code = 'default';
    private $zebraClass = '';
    function __construct() {

        $this->zebraClass = new Zebra_Image();

        if(isset($_SESSION['user']['prefix'])){
            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }
    }


    /*
    * Get Companies
    * @param active (string)
    */
    function get_companies($active = ''){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();
        $statement = "SELECT DISTINCT company.company_id, users.user_id, company_name,  AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,company_contact_number,
                        TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) AS company_renewal_date, company_keg_limit,
                        company_user_limit, company_device_limit, company_db_prefix, DATE_FORMAT(company_renewal_date, '%Y-%m-%d') AS raw_renewal_date
                        FROM {$GLOBALS['db_prefix']}_companies AS company
                        LEFT JOIN {$GLOBALS['db_prefix']}_users AS users ON company.company_id = users.company_id";
        if ($active == 'N')
            $statement .= " WHERE company.active = 'N' GROUP BY company.company_id";
        else
            $statement .= " WHERE (company.active = 'Y') OR (company.active IS NULL) GROUP BY company.company_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $companies = $sql->fetchAll();
        $sql->closeCursor();
        return $companies;
    }

    /*
    * Get Companies
    * @param active (string)
    */
    function get_companies_filter($active = '', $expiry){
        //var_dumpvar_dump($active, $expiry);
        //Get Grace period
        $appClass = new Application();
        $app_data = $appClass->get_settings();
        $grace_period = $app_data['grace_period'];

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();
        $statement = "SELECT company_id, company_name,  AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,company_contact_number,
                        TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) AS company_renewal_date, company_keg_limit,
                        company_user_limit, company_device_limit, company_db_prefix, DATE_FORMAT(company_renewal_date, '%Y-%m-%d') AS raw_renewal_date
                        FROM {$GLOBALS['db_prefix']}_companies";
        if ($expiry == '0'){
            $statement .= " WHERE active = 'Y' OR active IS NULL";
        }
        elseif($expiry == 'Expired and Locked'){

            $statement .= " WHERE active = 'N' AND  TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date)   <= {$grace_period} * -1";
        }
        elseif($expiry == 'Expired but in Grace Period'){

            $statement .= " WHERE active = 'Y' AND  TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) < 0";
        }
        elseif($expiry == 'Expire Soon'){

            $statement .= " WHERE active = 'Y' AND  TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date)  < {$grace_period}";
        }
        elseif($expiry == 'OK'){

            $statement .= " WHERE active = 'Y' AND  TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date)  > 10";
        }
        else{
            $statement .= " WHERE active = 'Y' OR active IS NULL";
        }

        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $companies = $sql->fetchAll();
        $sql->closeCursor();
        return $companies;
    }



    /*
    * Get company
    * @param $company_id (int)
    */
    function get_company($company_id){

        if (empty($company_id) || $company_id == 0) {
            $company = array('company_name' => 'Application Administration');
        } else {

            $Passphrase = new Passphrase();
            $phrase = $Passphrase->passphrase();

            $statement = "SELECT company.company_id, company_name, AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,
                          TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) AS company_renewal_date,
                           DATE(company_renewal_date) AS renewal_date, DATE(DATE_SUB(company_renewal_date,INTERVAL 365 DAY)) AS creation_date,
                           company_keg_limit,company_user_limit, company_device_limit, company_contact_number,
                          AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,
                          AES_DECRYPT(company_email, :phrase) AS company_email,
                          AES_DECRYPT(delivery_street_name, :phrase) AS delivery_street_name,
                          AES_DECRYPT(delivery_suburb, :phrase) AS delivery_suburb,
                          AES_DECRYPT(delivery_town, :phrase) AS delivery_town,
                          AES_DECRYPT(delivery_physical_code, :phrase) AS delivery_physical_code,
                          AES_DECRYPT(invoice_street_name, :phrase) AS invoice_street_name,
                          AES_DECRYPT(invoice_suburb, :phrase) AS  invoice_suburb,
                          AES_DECRYPT(invoice_town, :phrase) AS invoice_town,
                          AES_DECRYPT(invoice_physical_code, :phrase) AS invoice_physical_code,company_hard_limit,
                          AES_DECRYPT(user_full_name, :phrase) AS user_full_name,
                          AES_DECRYPT(user_email_address, :phrase) AS user_email_address,
                          AES_DECRYPT(users_contact_numbers, :phrase) AS users_contact_numbers,
                          users.user_type, AES_DECRYPT(user_username, :phrase) AS user_username,users.user_notes,
                          company_last_update, user_job_description, company_image, company_db_prefix, same_as_physical,
                          near_date_range, allow_imports,application_date,AES_DECRYPT(sender_email, :phrase) AS sender_email, AES_DECRYPT(reply_email, :phrase) AS reply_email
                            FROM {$GLOBALS['db_prefix']}_companies AS company
                             LEFT JOIN {$GLOBALS['db_prefix']}_users AS users ON company.company_id = users.company_id
                            WHERE company.company_id = :company_id";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':company_id', $company_id);
            $sql->bindParam(':phrase', $phrase);
            $sql->execute();

            $company = $sql->fetch();
            $sql->closeCursor();
        }
        return $company;
    }

    /*
    * Get company
    * @param $company_id (int)
    */
    function get_company_by_code($company_code){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT company.company_id, company_name, AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,
                          TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) AS company_renewal_date,
                            DATE(company_renewal_date) AS renewal_date,  DATE(DATE_SUB(company_renewal_date,INTERVAL 365 DAY)) AS creation_date,
                            company_keg_limit,company_user_limit, company_device_limit, company_contact_number,
                          AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,
                          AES_DECRYPT(company_email, :phrase) AS company_email,
                          AES_DECRYPT(delivery_street_name, :phrase) AS delivery_street_name,
                          AES_DECRYPT(delivery_suburb, :phrase) AS delivery_suburb,
                          AES_DECRYPT(delivery_town, :phrase) AS delivery_town,
                          AES_DECRYPT(delivery_physical_code, :phrase) AS delivery_physical_code,
                          AES_DECRYPT(invoice_street_name, :phrase) AS invoice_street_name,
                          AES_DECRYPT(invoice_suburb, :phrase) AS  invoice_suburb,
                          AES_DECRYPT(invoice_town, :phrase) AS invoice_town,
                          AES_DECRYPT(invoice_physical_code, :phrase) AS invoice_physical_code,company_hard_limit,
                          AES_DECRYPT(user_full_name, :phrase) AS user_full_name,
                          AES_DECRYPT(user_email_address, :phrase) AS user_email_address,
                          AES_DECRYPT(users_contact_numbers, :phrase) AS users_contact_numbers,
                          users.user_type, AES_DECRYPT(user_username, :phrase) AS user_username,users.user_notes,
                          company_last_update, user_job_description, company_image, company_db_prefix, same_as_physical,
                          near_date_range, allow_imports,application_date,AES_DECRYPT(sender_email, :phrase) AS sender_email, AES_DECRYPT(reply_email, :phrase) AS reply_email
                        FROM {$GLOBALS['db_prefix']}_companies AS company
                         LEFT JOIN {$GLOBALS['db_prefix']}_users AS users ON company.company_id = users.company_id
                        WHERE company.company_db_prefix = :company_db_prefix";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_db_prefix', $company_code);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $company = $sql->fetch();
        $sql->closeCursor();
        return $company;
    }


    /*
    * Get company details for the currently active company
     *
    * @param $company_id (int)
    */
    function get_active_company(){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT company.company_id, company_name, AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,
                      TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) AS company_renewal_date, company_keg_limit,
                      company_user_limit, company_device_limit, company_contact_number,
                      AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,
                      AES_DECRYPT(company_email, :phrase) AS company_email,
                      AES_DECRYPT(delivery_street_name, :phrase) AS delivery_street_name,
                      AES_DECRYPT(delivery_suburb, :phrase) AS delivery_suburb,
                      AES_DECRYPT(delivery_town, :phrase) AS delivery_town,
                      AES_DECRYPT(delivery_physical_code, :phrase) AS delivery_physical_code,
                      AES_DECRYPT(invoice_street_name, :phrase) AS invoice_street_name,
                      AES_DECRYPT(invoice_suburb, :phrase) AS  invoice_suburb,
                      AES_DECRYPT(invoice_town, :phrase) AS invoice_town,
                      AES_DECRYPT(invoice_physical_code, :phrase) AS invoice_physical_code,company_hard_limit,
                      AES_DECRYPT(user_full_name, :phrase) AS user_full_name,
                      AES_DECRYPT(user_email_address, :phrase) AS user_email_address,
                      AES_DECRYPT(users_contact_numbers, :phrase) AS users_contact_numbers,
                      users.user_type, AES_DECRYPT(user_username, :phrase) AS user_username,users.user_notes,
                      company_last_update, user_job_description, company_image, company_db_prefix,
                      near_date_range
                        FROM {$GLOBALS['db_prefix']}_companies AS company
                         LEFT JOIN {$GLOBALS['db_prefix']}_users AS users ON company.company_id = users.company_id
                        WHERE company.company_db_prefix = :company_db_prefix";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_db_prefix', $this->company_code);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $company = $sql->fetch();
        $sql->closeCursor();
        return $company;
    }

    /*
    * Save new company
    * @param $company_data (array)
     * return company_id
    */
    function save_new_company($company_data){

        //renewal date 12 months from creation
        $date = $company_data['creation_date'];
        $date = strtotime(date("Y-m-d", strtotime($date)) . " +12 month");
        $date = date("Y-m-d",$date);


        //Create and check company prefix
        $prefix = str_replace(' ', '', $company_data['company_name']);
        $prefix = preg_replace("/[^a-zA-Z]+/", "", $prefix);
        $prefix = substr($prefix, 0, 10);
        $prefix = strtolower($prefix);
        $check_pass = false;
        $fail_safe = 0;
        do {
            $statement = "SELECT COUNT(*) as company_count
                            FROM {$GLOBALS['db_prefix']}_companies
                            WHERE company_db_prefix = :prefix";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->bindParam(':prefix', $prefix);
            $sql_select->execute();
            $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();

            if ($sql_result['company_count'] > 0) {
                $prefix1 = substr($prefix, 0, strlen($prefix)-2);
                $currNum = substr($prefix, -2);
                $nextNum = (int) $currNum;
                $prefix2 = str_pad($nextNum + 1, 2, '0', STR_PAD_LEFT);
                $prefix = $prefix1 . $prefix2;
            } else {
                $check_pass = true;
            }

            $fail_safe++;
            if ($fail_safe == 100) {
                echo "FAIL SAFE Triggered!";
                break;
            }

        } while ($check_pass === false);

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        if (!isset($company_data['same_as_physical'])) $company_data['same_as_physical'] = 0;

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_companies 
                      (company_name,company_contact_number,company_renewal_date,company_keg_limit,company_user_limit,
                      company_device_limit,company_contact_name,company_email,delivery_street_name,delivery_suburb,
                      delivery_town,delivery_physical_code,invoice_street_name,invoice_suburb,invoice_town,
                      invoice_physical_code,company_hard_limit, company_db_prefix, same_as_physical)
					  VALUES
					  (:company_name,:company_contact_number,:company_renewal_date,:company_keg_limit,:company_user_limit,
					  :company_device_limit,
					  AES_ENCRYPT(:company_contact_name, :phrase),
					  AES_ENCRYPT(:company_email, :phrase),
					  AES_ENCRYPT(:delivery_street_name, :phrase),
					  AES_ENCRYPT(:delivery_suburb, :phrase),
					  AES_ENCRYPT(:delivery_town, :phrase),
					  AES_ENCRYPT(:delivery_physical_code, :phrase),
					  AES_ENCRYPT(:invoice_street_name, :phrase),
					  AES_ENCRYPT(:invoice_suburb, :phrase),
					  AES_ENCRYPT(:invoice_town, :phrase),
					  AES_ENCRYPT(:invoice_physical_code, :phrase),
					  :company_hard_limit, :company_db_prefix, :same_as_physical)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_name', $company_data['company_name']);
        $sql->bindParam(':company_contact_name', $company_data['company_contact_name']);
        $sql->bindParam(':company_contact_number', $company_data['company_contact_number']);
        $sql->bindParam(':company_renewal_date', $date);
        $sql->bindParam(':company_keg_limit', $company_data['keg_limit']);
        $sql->bindParam(':company_user_limit', $company_data['user_limit']);
        $sql->bindParam(':company_device_limit', $company_data['device_limit']);
        $sql->bindParam(':company_email', $company_data['company_email']);
        $sql->bindParam(':delivery_street_name', $company_data['delivery_street_name']);
        $sql->bindParam(':delivery_suburb', $company_data['delivery_suburb']);
        $sql->bindParam(':delivery_town', $company_data['delivery_town']);
        $sql->bindParam(':delivery_physical_code', $company_data['delivery_physical_code']);
        $sql->bindParam(':invoice_street_name', $company_data['invoice_street_name']);
        $sql->bindParam(':invoice_suburb', $company_data['invoice_suburb']);
        $sql->bindParam(':invoice_town', $company_data['invoice_town']);
        $sql->bindParam(':invoice_physical_code', $company_data['invoice_physical_code']);
        $sql->bindParam(':company_hard_limit', $company_data['hard_limit']);
        $sql->bindParam(':same_as_physical', $company_data['same_as_physical']);
        $sql->bindParam(':company_db_prefix', $prefix);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();
        $company_id = $GLOBALS['dbCon']->lastInsertId();
        $sql->closeCursor();
        $this->company_database_tables($prefix);
        return $company_id;
    }
    /*
    * Save new company user login details
    * @param $user_data (array), $company_id (int)
     * return $user_data (array)
    */
    function save_new_company_user($user_data, $company_id){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();


        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_users 
                      (user_email_address,user_type,user_username,date_created,created_by,company_id,users_contact_numbers,user_notes,user_full_name, user_job_description)
					  VALUES
					  (AES_ENCRYPT(:user_email_address, :phrase),:user_type,AES_ENCRYPT(:user_username, :phrase),NOW(),:created_by,:company_id,AES_ENCRYPT(:users_contact_numbers, :phrase),:user_notes,AES_ENCRYPT(:user_full_name, :phrase), :user_job_description)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_email_address', $user_data['user_email_address']);
        $sql->bindParam(':user_type', $user_data['user_job_description']);
        $sql->bindParam(':user_username', $user_data['user_username']);
        $sql->bindParam(':created_by', $_SESSION['user']['id']);
        $sql->bindParam(':company_id', $company_id);
        $sql->bindParam(':users_contact_numbers', $user_data['user_contact_numbers']);
        $sql->bindParam(':user_notes', $user_data['user_notes']);
        $sql->bindParam(':user_full_name', $user_data['user_full_name']);
        $sql->bindParam(':user_job_description', $user_data['workplace_description']);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();
        $user_id = $GLOBALS['dbCon']->lastInsertId();
        $sql->closeCursor();
        $this->create_new_user_modules($user_id, $user_data['user_job_description']);
        $this->create_user_permissions($user_id, $user_data['user_job_description']);
        return $user_id;
    }

    /*
   * Save super user for each company
   * @param $user_data (array), $company_id (int)
    * return $user_data (array)
   */
    function save_super_company_user($user_data, $company_id){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();
        $super_admin = 1;
        $username = $user_data['user_username'];
        $email = $user_data['user_email_address'];

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_users 
                      (user_email_address,user_type,user_username,date_created,created_by,company_id,users_contact_numbers,user_notes,user_full_name, user_job_description, super_user_admin, active)
					  VALUES
					  (AES_ENCRYPT(:user_email_address, :phrase),:user_type,AES_ENCRYPT(:user_username, :phrase),NOW(),:created_by,:company_id,AES_ENCRYPT(:users_contact_numbers, :phrase),:user_notes,AES_ENCRYPT(:user_full_name, :phrase), :user_job_description, :super_user_admin, 'Y')";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':user_email_address', $email);
        $sql->bindParam(':user_type', $user_data['user_job_description']);
        $sql->bindParam(':user_username', $username);
        $sql->bindParam(':created_by', $_SESSION['user']['id']);
        $sql->bindParam(':company_id', $company_id);
        $sql->bindParam(':users_contact_numbers', $user_data['user_contact_numbers']);
        $sql->bindParam(':user_notes', $user_data['user_notes']);
        $sql->bindParam(':user_full_name', $user_data['user_full_name']);
        $sql->bindParam(':user_job_description', $user_data['workplace_description']);
        $sql->bindParam(':super_user_admin', $super_admin);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();
        $user_id = $GLOBALS['dbCon']->lastInsertId();
        $sql->closeCursor();
        $this->create_new_user_modules($user_id, $user_data['user_job_description']);
        $this->create_user_permissions($user_id, $user_data['user_job_description']);
        return $user_id;
    }
    /**
     * Add installed modules to the user_modules table
     * @param int $user_id
     */
    function create_new_user_modules($user_id, $job_description){

        $securityClass = new Security();
        $modules = $securityClass->get_installed_modules(FALSE,$job_description);

        $order = 1;
        foreach ($modules as $module) {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_user_modules
                                        (user_id, module_id, module_order)
                                        VALUES (:user_id, :module_id, :module_order)";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':user_id', $user_id);
            $sql->bindParam(':module_id', $module['module_id']);
            $sql->bindParam(':module_order', $order);
            $sql->execute();
            $order++;
        }
    }

    /**
     * Create new User permissions based on new roles
     * @param int $user_id
     */
    function create_user_permissions($user_id, $role_id)
    {
        //Get all the modules permissions
        $statement = "SELECT permission_id, permission_name, module_id, admin_module
                       FROM {$GLOBALS['db_prefix']}_{$this->company_code}_module_permissions";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $permissions = $sql->fetchAll(PDO::FETCH_ASSOC);

        //Get all the modules permissions
        $statement = "SELECT role_id, permission_id
                      FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_role_permissions
                      WHERE role_id =:role_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':role_id', $role_id);
        $sql->execute();
        $role_permissions = $sql->fetchAll(PDO::FETCH_ASSOC);

                foreach ($permissions as $permission) {
                    $access = 0;
                    foreach ($role_permissions as $role_permission){
                        if($permission['permission_id'] == $role_permission['permission_id']){
                            $access = 1;
                        }
                    }
                    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_user_access
                        (user_id, permission_id, access)
                        VALUES (:user_id, :permission_id, :access)
                        ON DUPLICATE KEY UPDATE
                        user_id = :user_id, permission_id = :permission_id";
                    $sql = $GLOBALS['dbCon']->prepare($statement);
                    $sql->bindParam(':user_id', $user_id);
                    $sql->bindParam(':permission_id', $permission['permission_id']);
                    $sql->bindParam(':access', $access);
                    $sql->execute();
                }
    }

    /**
     * Update company renewal date to another year
     * @param $company_id
     * @return array
     */
    function renew_company_account($company_id){
        $date = date('Y-m-d h:i:sa');
        $date = strtotime(date("Y-m-d h:i:sa", strtotime($date)) . " +12 month");
        $date = date("Y-m-d h:i:s",$date);

        $statement = "UPDATE {$GLOBALS['db_prefix']}_companies 
                        SET company_renewal_date = :company_renewal_date
                        WHERE company_id = :company_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':company_renewal_date', $date);
        $sql_update->bindParam(':company_id', $company_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /**
     * Activate/ Deactivate company
     * @param $company_id, $active
     * @return array
     */
    function change_company_status($company_id, $active){

        $statement = "UPDATE {$GLOBALS['db_prefix']}_companies 
                        SET active = :active
                        WHERE company_id = :company_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':active', $active);
        $sql_update->bindParam(':company_id', $company_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /**
     * Activate/ Deactivate company
     * @param $company_id, $active
     * @return array
     */
    function verfy_company_status($company_id){

        $statement = "SELECT active
                        FROM {$GLOBALS['db_prefix']}_companies
                        WHERE company_id = :company_id
                        LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $company_id);
        $sql->execute();
        $company = $sql->fetch();
        $sql->closeCursor();
        return $company;
    }

    /*
   * Get company user details
   * @param $company_id, $username (int)
   */
    function get_company_user_details($company_id){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT company.company_id, company_name, AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name, TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) AS company_renewal_date, company_keg_limit, company_user_limit, company_device_limit,
                             company_contact_number,AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name, AES_DECRYPT(company_email, :phrase) AS company_email, AES_DECRYPT(delivery_street_name, :phrase) AS delivery_street_name, AES_DECRYPT(delivery_suburb, :phrase) AS delivery_suburb, AES_DECRYPT(delivery_town, :phrase) AS delivery_town, AES_DECRYPT(delivery_physical_code, :phrase) AS delivery_physical_code, AES_DECRYPT(invoice_street_name, :phrase) AS invoice_street_name, AES_DECRYPT(invoice_suburb, :phrase) AS  invoice_suburb, AES_DECRYPT(invoice_town, :phrase) AS invoice_town, AES_DECRYPT(invoice_physical_code, :phrase) AS invoice_physical_code,company_hard_limit,
                              AES_DECRYPT(user_full_name, :phrase) AS user_full_name,  AES_DECRYPT(user_email_address, :phrase) AS user_email_address, AES_DECRYPT(users_contact_numbers, :phrase) AS users_contact_numbers, users.user_type, AES_DECRYPT(user_username, :phrase) AS user_username,users.user_notes, company_last_update
                        FROM {$GLOBALS['db_prefix']}_companies as company
                         LEFT JOIN {$GLOBALS['db_prefix']}_users AS users ON company.company_id = users.company_id
                        WHERE company.company_id = :company_id
                        LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $company_id);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $company = $sql->fetch();
        $sql->closeCursor();
        return $company;
    }

    /*
    * Updating the activation code to be used to  change forgot password
    * @param int $company_id,$code
    */
    function update_company_user_activation_code($code, $company_id){

        $current_date = date('Y-m-d H:i:s');
        $statement = "UPDATE {$GLOBALS['db_prefix']}_users
                            SET activation_code = :activation_code,
                            password_request_time= :password_request_time
                            WHERE company_id = :company_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $company_id);
        $sql->bindParam(':activation_code', $code);
        $sql->bindParam(':password_request_time', $current_date);
        $sql->execute();
    }

    /*
    * Update compay details
    * @param donor_data (array)
    */
    function update_company_details($company_data){
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        if (!isset($company_data['same_as_physical']))$company_data['same_as_physical'] = 0;

        $statement = "UPDATE {$GLOBALS['db_prefix']}_companies
						SET	company_name = :company_name,
						    company_contact_number = :company_contact_number,
						    company_keg_limit = :company_keg_limit,
						    company_user_limit = :company_user_limit,
						    company_device_limit = :company_device_limit,
						    company_contact_name = AES_ENCRYPT(:company_contact_name, '" . $phrase . "'),
						    company_email = AES_ENCRYPT(:company_email, '" . $phrase . "'),
						    delivery_street_name = AES_ENCRYPT(:delivery_street_name, '" . $phrase . "'),
						    delivery_suburb = AES_ENCRYPT(:delivery_suburb, '" . $phrase . "'),
						    delivery_town = AES_ENCRYPT(:delivery_town, '" . $phrase . "'),
						    delivery_physical_code =  AES_ENCRYPT(:delivery_physical_code, '" . $phrase . "'),
						    invoice_street_name = AES_ENCRYPT(:invoice_street_name, '" . $phrase . "'),
						    invoice_suburb = AES_ENCRYPT(:invoice_suburb, '" . $phrase . "'),
						    invoice_town = AES_ENCRYPT(:invoice_town, '" . $phrase . "'),
						    invoice_physical_code = AES_ENCRYPT(:invoice_physical_code, '" . $phrase . "'),
						    company_hard_limit = :company_hard_limit,
						    same_as_physical = :same_as_physical
							WHERE company_id = :company_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $company_data['company_id']);
        $sql->bindParam(':company_name', $company_data['company_name']);
        $sql->bindParam(':company_contact_name', $company_data['company_contact_name']);
        $sql->bindParam(':company_contact_number', $company_data['company_contact_number']);
        $sql->bindParam(':company_keg_limit', $company_data['keg_limit']);
        $sql->bindParam(':company_user_limit', $company_data['user_limit']);
        $sql->bindParam(':company_device_limit', $company_data['device_limit']);
        $sql->bindParam(':company_email', $company_data['company_email']);
        $sql->bindParam(':delivery_street_name', $company_data['delivery_street_name']);
        $sql->bindParam(':delivery_suburb', $company_data['delivery_suburb']);
        $sql->bindParam(':delivery_town', $company_data['delivery_town']);
        $sql->bindParam(':delivery_physical_code', $company_data['delivery_physical_code']);
        $sql->bindParam(':invoice_street_name', $company_data['invoice_street_name']);
        $sql->bindParam(':invoice_suburb', $company_data['invoice_suburb']);
        $sql->bindParam(':invoice_town', $company_data['invoice_town']);
        $sql->bindParam(':invoice_physical_code', $company_data['invoice_physical_code']);
        $sql->bindParam(':company_hard_limit', $company_data['hard_limit']);
        $sql->bindParam(':same_as_physical', $company_data['same_as_physical']);
        $sql->execute();
        $sql->closeCursor();
    }
    /*
   * Save new company user login details
   * @param $user_data (array), $company_id (int)
    * return $user_data (array)
   */
    function update_company_user_details($user_data){
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "UPDATE {$GLOBALS['db_prefix']}_users
						SET	user_email_address = AES_ENCRYPT(:user_email_address, '" . $phrase . "'),
						    user_type = :user_type,
						    user_username = AES_ENCRYPT(:user_username, '" . $phrase . "'),
						    created_by = :created_by,
						    users_contact_numbers = AES_ENCRYPT(:users_contact_numbers, '" . $phrase . "'),
						    user_notes = :user_notes,
						    user_full_name = AES_ENCRYPT(:user_full_name, '" . $phrase . "'),
						    user_job_description = :user_job_description
							WHERE company_id = :company_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $user_data['company_id']);
        $sql->bindParam(':user_email_address', $user_data['user_email_address']);
        $sql->bindParam(':user_type', $user_data['user_job_description']);
        $sql->bindParam(':user_username', $user_data['user_username']);
        $sql->bindParam(':created_by', $_SESSION['user']['id']);
        $sql->bindParam(':users_contact_numbers', $user_data['user_contact_numbers']);
        $sql->bindParam(':user_notes', $user_data['user_notes']);
        $sql->bindParam(':user_full_name', $user_data['user_full_name']);
        $sql->bindParam(':user_job_description', $user_data['workplace_description']);
        $sql->execute();
        $sql->closeCursor();
    }

    /*
    * Get all users for a specific company
    * @param $company_id
    */
    function get_all_company_users($company_id){

        $statement = "SELECT user_id
                        FROM {$GLOBALS['db_prefix']}_users 
                         WHERE company_id = :company_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $company_id);
        $sql->execute();

        $users = $sql->fetchAll();
        $sql->closeCursor();
        return $users;
    }
    /*
  * Get all roles on the system
  * @param
   * return $user_roles (array)
  */
    function get_all_user_roles(){
        if($_SESSION['user']['user_type'] == 'developer'){
            $statement = "SELECT role_id,role_name, role_display_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->execute();
        }
        elseif ($_SESSION['user']['user_type'] == 'application_administrator'){
            $role = 'developer';
            $statement = "SELECT role_id, role_name, role_display_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                        WHERE role_name != :role";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':role', $role);
            $sql->execute();
        }
        else{
            $role = 'developer';
            $statement = "SELECT role_id, role_name, role_display_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_user_roles
                        WHERE role_name != :role";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':role', $role);
            $sql->execute();
        }
        $roles = $sql->fetchAll();
        $sql->closeCursor();
        return $roles;
    }

    /*
  * Save new company tables
  * @param
   * return $user_data (array)
  */
    function company_database_tables($db_prefix){




        //create keg_default_user_roles table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_user_roles";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_user_roles
                        SELECT *
                        FROM {$GLOBALS['db_prefix']}_default_user_roles
                        WHERE company = 1 OR employee = 1";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();


        //create keg_default_module_permissions table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_module_permissions";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_module_permissions
                        SELECT *
                        FROM {$GLOBALS['db_prefix']}_default_module_permissions
                        WHERE admin_module <> 1";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();


        //create keg_default_stages table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_stages";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_stages
                        SELECT *
                        FROM {$GLOBALS['db_prefix']}_default_stages
                        WHERE active = 'Y'";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();


        //create keg_default_states table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_states";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_states
                        SELECT *
                        FROM {$GLOBALS['db_prefix']}_default_states
                        WHERE inactive <> 'Y' OR inactive IS NULL";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();

        //create keg_default_types table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_types";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();


        //create keg_default_terminology table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_terminology";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_terminology
                        SELECT *
                        FROM {$GLOBALS['db_prefix']}_default_terminology";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();


        //create keg_default_user_role_permissions table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_user_role_permissions";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_user_role_permissions
                        SELECT a.*
                        FROM {$GLOBALS['db_prefix']}_default_user_role_permissions AS a
                        RIGHT JOIN {$GLOBALS['db_prefix']}_{$db_prefix}_user_roles AS b ON a.role_id = b.role_id";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();


        //create keg_default_user_role_access table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_user_access";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();


        //create keg_default_user_role_access table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_user_role_access";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_user_role_access
                        SELECT a.*
                        FROM {$GLOBALS['db_prefix']}_default_user_role_access AS a
                        RIGHT JOIN {$GLOBALS['db_prefix']}_{$db_prefix}_user_roles AS b ON a.role_id = b.role_id";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();


        //create keg_default_container_notes table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_container_notes";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();


        //create keg_default_container_history table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_container_history";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();


        //create keg_default_containers table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_containers";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();


        //create keg_default_customers table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_customers";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();


        //create keg_default_registered_devices table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_registered_devices";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        //create keg_default_container_types table
        $statement = "SHOW CREATE TABLE {$GLOBALS['db_prefix']}_default_container_types";
        $sql_describe = $GLOBALS['dbCon']->prepare($statement);
        $sql_describe->execute();
        $sql_result = $sql_describe->fetch(PDO::FETCH_ASSOC);
        $sql_describe->closeCursor();

        $statement = str_replace('_default_', "_{$db_prefix}_", $sql_result['Create Table']);
        $sql_create = $GLOBALS['dbCon']->prepare($statement);
        $sql_create->execute();
        $sql_create->closeCursor();

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$db_prefix}_container_types
                        SELECT *
                        FROM {$GLOBALS['db_prefix']}_default_container_types";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->execute();
        $sql_insert->closeCursor();



    }

    /**
     * Upload cmpany logo
     *
     * @param image_id
     * @param $array_name
     * @return array
     */
    function upload_new_image( $array_name) {

        $upload_result = array('status'=>true);
        $filename = rand(0,200) . "_{$_FILES[$array_name]['name']}";
        $filename_thm = "thm_$filename";

        $this->zebraClass->source_path = $_FILES[$array_name]['tmp_name'];
        $this->zebraClass->target_path = "{$_SERVER['DOCUMENT_ROOT']}/uploads/users/images/$filename";
        $this->zebraClass->preserve_aspect_ratio = true;
        $this->zebraClass->enlarge_smaller_images = false;
        if (!$this->zebraClass->resize(800, 600, ZEBRA_IMAGE_CROP_CENTER)) {
            switch (!$this->zebraClass->error) {
                case 1:
                    $upload_result = array('status'=>true, 'message'=>'Source file could not be found!');
                    break;
                case 2:
                    $upload_result = array('status'=>true, 'message'=>'Source file is not readable!');
                    break;
                case 3:
                    $upload_result = array('status'=>true, 'message'=>'Could not write target file!');
                    break;
                case 4:
                    $upload_result = array('status'=>true, 'message'=>'Unsupported source file format!');
                    break;
                case 5:
                    $upload_result = array('status'=>true, 'message'=>'Unsupported target file format!');
                    break;
                case 6:
                    $upload_result = array('status'=>true, 'message'=>'GD library version does not support target file format!');
                    break;
                case 7:
                    $upload_result = array('status'=>true, 'message'=>'GD library is not installed!');
                    break;
            }
        }

        $this->zebraClass->target_path = "{$_SERVER['DOCUMENT_ROOT']}/uploads/users/images/$filename_thm";
        if (!$this->zebraClass->resize(200, 150, ZEBRA_IMAGE_CROP_CENTER)) {
            switch ($this->zebraClass->error) {
                case 1:
                    $upload_result = array('status'=>true, 'message'=>'Thumbnail: Source file could not be found!');
                    break;
                case 2:
                    $upload_result = array('status'=>true, 'message'=>'Thumbnail: Source file is not readable!');
                    break;
                case 3:
                    $upload_result = array('status'=>true, 'message'=>'Thumbnail: Could not write target file!');
                    break;
                case 4:
                    $upload_result = array('status'=>true, 'message'=>'Thumbnail: Unsupported source file format!');
                    break;
                case 5:
                    $upload_result = array('status'=>true, 'message'=>'Thumbnail: Unsupported target file format!');
                    break;
                case 6:
                    $upload_result = array('status'=>true, 'message'=>'Thumbnail: GD library version does not support target file format!');
                    break;
                case 7:
                    $upload_result = array('status'=>true, 'message'=>'Thumbnail: GD library is not installed!');
                    break;
            }
        }


        $statement = "UPDATE {$GLOBALS['db_prefix']}_companies
						SET	company_image = :company_image
							WHERE company_id = :company_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $_SESSION['user']['company_id']);
        $sql->bindParam(':company_image', $filename);
        $sql->execute();
        $sql->closeCursor();

        return $filename;
    }

    /*
   * Update company details from settings modules
   * @param donor_data (array)
   */
    function update_company_details_settings($company_data){
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "UPDATE {$GLOBALS['db_prefix']}_companies
						SET	company_name = :company_name,
						    company_contact_number = :company_contact_number,
						    company_contact_name = AES_ENCRYPT(:company_contact_name, '" . $phrase . "'),
						    company_email = AES_ENCRYPT(:company_email, '" . $phrase . "'),
						    delivery_street_name = AES_ENCRYPT(:delivery_street_name, '" . $phrase . "'),
						    delivery_suburb = AES_ENCRYPT(:delivery_suburb, '" . $phrase . "'),
						    delivery_town = AES_ENCRYPT(:delivery_town, '" . $phrase . "'),
						    delivery_physical_code =  AES_ENCRYPT(:delivery_physical_code, '" . $phrase . "'),
						    invoice_street_name = AES_ENCRYPT(:invoice_street_name, '" . $phrase . "'),
						    invoice_suburb = AES_ENCRYPT(:invoice_suburb, '" . $phrase . "'),
						    invoice_town = AES_ENCRYPT(:invoice_town, '" . $phrase . "'),
						    invoice_physical_code = AES_ENCRYPT(:invoice_physical_code, '" . $phrase . "'),
						    near_date_range = :near_date_range,
						    allow_imports = :allow_imports,
						    application_date = :application_date,
						    sender_email = AES_ENCRYPT(:sender_email, '" . $phrase . "'),
						    reply_email = AES_ENCRYPT(:reply_email, '" . $phrase . "')
							WHERE company_id = :company_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $company_data['company_id']);
        $sql->bindParam(':company_name', $company_data['company_name']);
        $sql->bindParam(':company_contact_name', $company_data['company_contact_name']);
        $sql->bindParam(':company_contact_number', $company_data['company_contact_number']);
        $sql->bindParam(':company_email', $company_data['company_email']);
        $sql->bindParam(':delivery_street_name', $company_data['delivery_street_name']);
        $sql->bindParam(':delivery_suburb', $company_data['delivery_suburb']);
        $sql->bindParam(':delivery_town', $company_data['delivery_town']);
        $sql->bindParam(':delivery_physical_code', $company_data['delivery_physical_code']);
        $sql->bindParam(':invoice_street_name', $company_data['invoice_street_name']);
        $sql->bindParam(':invoice_suburb', $company_data['invoice_suburb']);
        $sql->bindParam(':invoice_town', $company_data['invoice_town']);
        $sql->bindParam(':invoice_physical_code', $company_data['invoice_physical_code']);
        $sql->bindParam(':near_date_range', $company_data['near_date_range']);
        $sql->bindParam(':allow_imports', $company_data['imports']);
        $sql->bindParam(':sender_email', $company_data['sender_email']);
        $sql->bindParam(':application_date', $company_data['app_date']);
        $sql->bindParam(':sender_email', $company_data['sender_email']);
        $sql->bindParam(':reply_email', $company_data['reply_to_email']);
        $sql->execute();
        $sql->closeCursor();
    }


    function get_companies_report() {

        $companies = $this->get_companies('Y');

        foreach ($companies as $key=>$company) {

            //Get container count
            $statement = "SELECT COUNT(*) AS container_count
                            FROM keg_{$company['company_db_prefix']}_containers";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();
            $companies[$key]['container_count'] = $sql_result['container_count'];

            //Get user count
            $statement = "SELECT COUNT(*) AS user_count
                            FROM keg_users
                            WHERE super_user_admin = 0 AND company_id = :company_id";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->bindParam(':company_id', $company['company_id']);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();
            $companies[$key]['user_count'] = $sql_result['user_count'];

            //Get device Limit
            $statement = "SELECT COUNT(*) AS device_count
                            FROM keg_{$company['company_db_prefix']}_registered_devices AS a
                            LEFT JOIN keg_users AS b ON a.user_id = b.user_id
                            WHERE b.company_id = :company_id";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->bindParam(':company_id', $company['company_id']);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();
            $companies[$key]['device_count'] = $sql_result['device_count'];
        }

        return $companies;
    }
    /*
    * Get Companies expiring in the next 30 days
    * @param active (string)
    */
    function get_companies_expiry($active = ''){

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();
        $statement = "SELECT company_id, company_name,  AES_DECRYPT(company_contact_name, :phrase) AS company_contact_name,company_contact_number, AES_DECRYPT(company_email, :phrase) AS company_email,
                        TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date) AS company_renewal_date, company_keg_limit,
                        company_user_limit, company_device_limit, company_db_prefix, DATE_FORMAT(company_renewal_date, '%Y-%m-%d') AS raw_renewal_date
                        FROM {$GLOBALS['db_prefix']}_companies
                        WHERE active = 'Y' AND  TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date)  <= 30 AND TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP , company_renewal_date)  >= 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();

        $companies = $sql->fetchAll();
        $sql->closeCursor();
        return $companies;
    }

    /*
       * Get all application administrators
       * @param
       */
    function get_admin()
    {
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

            $statement = "SELECT AES_DECRYPT(user_email_address, :phrase) AS user_email_address
                          FROM {$GLOBALS['db_prefix']}_users
                          WHERE user_type = '1' AND active = 'Y' OR active IS NULL AND super_user_admin = 0";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':phrase', $phrase);
            $sql->execute();
            $sql_result = $sql->fetchAll(PDO::FETCH_ASSOC);
             $sql->closeCursor();
        return $sql_result;
    }

    /*
   * Get date format for each company
   * @param $company_id (string)
   */
    function get_company_date_format($company_id){

        $statement = "SELECT application_date
                        FROM {$GLOBALS['db_prefix']}_companies
                        WHERE company_id = :company_id
                        OR company_db_prefix = :company_db_prefix";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':company_id', $company_id);
        $sql->bindParam(':company_db_prefix', $company_id);
        $sql->execute();

        $date_format = $sql->fetch();
        $sql->closeCursor();
        return $date_format;
    }
    /**
     * get the number of active/inactive companies
     * @param $active
     * @return count
     */
    function active_company_count($active ){

        $statement = "SELECT COUNT(*) AS company_count
                        FROM {$GLOBALS['db_prefix']}_companies";
        if ($active == 'N')
            $statement .= " WHERE active = 'N' ";
        else
            $statement .= " WHERE (active = 'Y') OR (active IS NULL) OR (active = '')";

        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->bindParam(':active', $active);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();

        return ($sql_count_data['company_count']);
    }
    /**
     * get the number of companies over keg limit & device limit & user limit
     * @param $company_id
     * @return count
     */
    function get_companies_stats($company_id) {

        $company_data = $this->get_company($company_id);



            //Get container count
            $statement = "SELECT COUNT(*) AS container_count
                            FROM keg_{$company_data['company_db_prefix']}_containers";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();
            $company_data['container_total'] = $sql_result['container_count'];

            //Get user count
            $statement = "SELECT COUNT(*) AS user_count
                            FROM keg_users
                            WHERE super_user_admin = 0 AND company_id = :company_id";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->bindParam(':company_id', $company_data['company_id']);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();
            $company_data['user_total'] = $sql_result['user_count'];

            //Get device Limit
            $statement = "SELECT COUNT(*) AS device_count
                            FROM keg_{$company_data['company_db_prefix']}_registered_devices AS a
                            LEFT JOIN keg_users AS b ON a.user_id = b.user_id
                            WHERE b.company_id = :company_id";
            $sql_count = $GLOBALS['dbCon']->prepare($statement);
            $sql_count->bindParam(':company_id', $company_data['company_id']);
            $sql_count->execute();
            $sql_result = $sql_count->fetch(PDO::FETCH_ASSOC);
            $sql_count->closeCursor();
            $company_data['device_total'] = $sql_result['device_count'];




        return $company_data;
    }
}