<?php

/**
 * Call the edit company page
 * 2016-06-20 - Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();
$settingsClass = new Settings();

$output = array();
$output['aaData'] = '';
$company_id = isset($_GET['company_id'])? $_GET['company_id'] :0;

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/company_management/html/edit_company.html");

//get container terminology
$container_name = $settingsClass->get_container_terminology();


//get the company date format
if(isset($_SESSION['user']['run_as'])) {
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['run_as']);
} else{
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
}
$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];



if($company_id != 0){
    //get a list of all companies
    $company = $companyClass->get_company($company_id);

    //get all user roles
    $user_roles = $companyClass->get_all_user_roles();

    $job_description = '<select name="user_job_description" class="form-control" id="user_job_description" style="display: none">
                                                <option value="">Select User Type</option>';
    foreach($user_roles as $roles) {
        if ($company['user_type'] == $roles['role_id']) {
            $job_description .= '<option value="' . $roles['role_id'] . '" selected>' . $roles['role_display_name'] . '</option>';
        } else {
            $job_description .= '<option value="' . $roles['role_id'] . '" >' . $roles['role_display_name'] . '</option>';
        }
    }
    $job_description .= '</select>';
    //hard limit
    if($company['company_hard_limit'] == 1){
        $hard_limit = ' <label> <input type="checkbox" class="i-checks" name="hard_limits" id="hard_limits" checked> Hard Limit </label>';
    }
    else{
        $hard_limit = ' <label> <input type="checkbox" class="i-checks" name="hard_limits" id="hard_limits" > Hard Limit </label>';
    }



    $date = date($format. ' H:i:s',strtotime($company['company_last_update']));
    $template_data = array(
        'company_id'       =>  $company_id,
        'company_name'       =>  $company['company_name'],
        'date'       =>   date($format),
        'creation_date' => '<label> Last Update: </label>' .' '. $date,
        'renewal'     =>  $company['company_renewal_date'].' Day(s) Until Renewal.' ,
        'company_names'      =>  $company['company_name'],
        'company_contact_name'      =>  $company['company_contact_name'],
        'company_contact_number'      =>  $company['company_contact_number'],
        'company_email'         =>  $company['company_email'],
        'keg_limit'     =>  $company['company_keg_limit'],
        'user_limit'   => $company['company_user_limit'],
        'device_limit'   => $company['company_device_limit'],
        'delivery_street_name'   => $company['delivery_street_name'],
        'delivery_suburb'   => $company['delivery_suburb'],
        'delivery_town'   => $company['delivery_town'],
        'delivery_physical_code'   => $company['delivery_physical_code'],
        'invoice_street_name'   => $company['invoice_street_name'],
        'invoice_suburb'   => $company['invoice_suburb'],
        'invoice_town'   => $company['invoice_town'],
        'invoice_physical_code'   => $company['invoice_physical_code'],
        'user_full_name'   => '',
        'user_email_address'   => '',
        'user_contact_numbers'   => '',
        'user_job_description'   => '',
        'user_username'   => '',
        'user_notes'   => '',
        'hard_limit' => $hard_limit,
        'renew_button' => ' <button class="btn btn-primary  pull-right btn-sm" type="button" id="renew_account"><i class="fa fa-credit-card"></i> Renew Account</button>',
        'workplace_description' => $company['user_job_description'],
        'new_company' => 'Edit Company',
        'container_singular'=> $container_name,
        'same_as_physical' => $company['same_as_physical']==1?'checked':'',
        'hide_initial_user' => 'none'
    );
}
else{
    //get all user roles
    $user_roles = $companyClass->get_all_user_roles();

    $job_description = '<select name="user_job_description" class="form-control" id="user_job_description" style="display: none">
                                                <option value="">Select User Type</option>';
    foreach($user_roles as $roles) {
        if ('3' == $roles['role_id']) {
            $job_description .= '<option value="' . $roles['role_id'] . '" selected>' . $roles['role_display_name'] . '</option>';
        } else {
            $job_description .= '<option value="' . $roles['role_id'] . '" >' . $roles['role_display_name'] . '</option>';
        }

    }
    $job_description .= '</select>';

    //get the company date format
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
    $format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];
    $date = ($format);




    $template_data = array(
        'company_id'       =>  $company_id,
        'company_name'       =>  'New Company',
        'date'       =>  date($date),
        'creation_date' => '<label> Creation Date: </label> '. date($date.' h:i:s'),
        'renewal'     =>  '' ,
        'company_names'      =>  '',
        'company_contact_name'      =>  '',
        'company_contact_number'      =>  '',
        'company_email'         =>  '',
        'keg_limit'     =>  '',
        'user_limit'   => '',
        'device_limit'   => '',
        'delivery_street_name'   => '',
        'delivery_suburb'   => '',
        'delivery_town'   => '',
        'delivery_physical_code'   => '',
        'invoice_street_name'   => '',
        'invoice_suburb'   => '',
        'invoice_town'   => '',
        'invoice_physical_code'   => '',
        'user_full_name'   => '',
        'user_email_address'   => '',
        'user_contact_numbers'   => '',
        'user_job_description'   => $job_description,
        'user_username'   => '',
        'user_notes'   => '',
        'hard_limit' => ' <label> <input type="checkbox" class="i-checks" name="hard_limits" id="hard_limits" > Hard Limit </label>',
        'workplace_description' => '',
        'new_company' => 'Create Company',
        'container_singular'=> $container_name,
        'same_as_physical' => '',
        'hide_initial_user' =>'block'
    );


}


$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
