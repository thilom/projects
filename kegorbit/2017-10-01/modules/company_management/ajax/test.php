<?php

/**
 *  save/update company
 *  2016-06-22 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/swiftmailer/lib/swift_required.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$companyClass->company_database_tables();


