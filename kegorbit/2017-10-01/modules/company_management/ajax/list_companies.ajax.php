<?php
/**
 * company management - fetch a list of all companies
 * 2016-06-20 - Musa Khulu - Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();

$output = array();
$output['aaData'] = '';
$active = $_GET['active'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all companies
$companies = $companyClass->get_companies($active);


if(empty($companies)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('company_name', 'company_contact_name', 'company_renewal_date', 'company_keg_limit', 'company_user_limit', 'company_device_limit');

    foreach($companies as $company)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'company_id')
                $company_id = $company[$aColumns[$i]];

            $row[] = $company[$aColumns[$i]];
        }

        if($active == 'Y'){
            $row[] =
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=company_management&a=edit_company&company_id=' . $company['company_id'] . '" class="btn btn-primary btn-sm "><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span><span class="hidden-xs"> Edit</span> </a>
							<button class="btn btn-primary btn-sm  dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" >
							    <li>
									<a class="switchBtn"  onclick="runAs(\'' . $company['company_db_prefix'] . '\', \''.$company['company_name'].'\', \''.$company['user_id'].'\')" data-value="'.$company['company_name'].'"><i class="fa fa-random"></i></span> Run As</a>
								</li>
								<li>
									<a  data-value="' . $company['company_id'] . '" class="reset_user_password"><i class="fa fa-envelope-o"></i></span>  Reset Password </a>
								</li>
								<li>
									<a class="deactivateBtn" href="/modules/company_management/ajax/edit_company_status.ajax.php?company_id=' . $company['company_id'] . '&active=N" data-value="'.$company['company_name'].'"><i class="fa fa-ban"></i></span> Deactivate</a>
								</li>
							</ul>
						</div>';
        }

        else {
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=company_management&a=edit_company&company_id=' . $company['company_id'] . '" class="btn btn-primary  btn-sm"><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span><span class="hidden-xs"> Edit</span></a>
							<button class="btn btn-primary  btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" >
								<li>
									<a class="activateBtn" href="/modules/company_management/ajax/edit_company_status.ajax.php?company_id=' . $company['company_id'] . '&active=Y" data-value="'.$company['company_name'].'"><i class="fa fa-check"></i></span> Activate</a>
								</li>
							</ul>
						</div>';
        }

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
