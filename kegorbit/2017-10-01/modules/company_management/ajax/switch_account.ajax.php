<?php
/**
 * Switch to a company account
 *
 * 2016-08-01: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";

//Vars
$company_code = $_GET['code'];
$user_id = $_GET['user_id'];

if ($company_code == 'return') {
    unset($_SESSION['user']['run_as']);
    unset($_SESSION['user']['run_as_id']);
} else {
    $_SESSION['user']['run_as_id'] = $user_id;
    $_SESSION['user']['run_as'] = $company_code;

}




