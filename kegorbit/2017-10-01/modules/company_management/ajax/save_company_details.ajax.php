<?php

/**
 *  save/update company
 *  2016-06-22 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/swiftmailer/lib/swift_required.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();
$accountClass = new Accounts();
$appClass = new Application();
$smtp_url = $GLOBALS['smtp']['url'];
$smtp_port = $GLOBALS['smtp']['port'];
$smtp_username = $GLOBALS['smtp']['username'];
$smtp_password = $GLOBALS['smtp']['password'];

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

if($_POST['company_id'] == 0) {

    //create new company
    $company_id = $companyClass->save_new_company($_POST);
    $company_data = $companyClass->get_company($company_id);
    $company_code = $company_data['company_db_prefix'];

    if(!empty($company_id)) {
        //save super user for company
        $super_user =$companyClass->save_super_company_user($_POST, $company_id);
        //Attachuser permissions
        $accountClass->create_user_permissions($super_user, 3, $company_code);
        //create user for new company
//        $user_id = $companyClass->save_new_company_user($_POST, $company_id);
        $user_id = $super_user;

        //Attachuser permissions
        $accountClass->create_user_permissions($user_id, 3, $company_code);

        if(!empty($user_id)){
            //send email to user to complete registration

            $code = sha1(mt_rand(10000,99999).time().$_POST['user_email_address']);

            //save activation code
            $userClass->update_user_activation_code($code,$user_id);


            //Get SMTP Details
            $settings = $appClass->get_settings();
            $smtp_url = $settings['smtp_url'];
            $smtp_port = $settings['smtp_port'];
            $smtp_username = $settings['smtp_username'];
            $smtp_password = $settings['smtp_password'];

            // Create the mail transport configuration
            $transport = Swift_SmtpTransport::newInstance($smtp_url, $smtp_port)->setUsername($smtp_username)->setPassword($smtp_password);
            //Supposed to allow local domain sending to work from what I read
            //$transport->setLocalDomain('[127.0.0.1]');

            // Create the message
            $message = Swift_Message::newInstance($transport);

            $message->setTo($_POST['user_email_address']);
            $message->setSubject("KegOrbit  Registration");

            $body = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/html/email_company_template/new_user_email.html');

            $table_details = array('email' => $_POST['user_email_address'], 'username' => $_POST['user_username'], 'firstname' => $_POST['user_full_name']);
            $body = $systemClass->merge_data($body, $table_details);

            $email_name = array('contact_name' => $_POST['user_full_name']);
            $body = $systemClass->merge_data($body, $email_name);

            $email_data['reset_link'] = "<a href='http://{$_SERVER['HTTP_HOST']}/index.php?code=$code&new={$user_id}'>{$_SERVER['HTTP_HOST']}/index.php?code=$code&new={$user_id}</a>";
            $body = $systemClass->merge_data($body, $email_data);

            $server_details = array('server' => $_SERVER['HTTP_HOST'], 'timestamp' => date('d-m-Y H:i:s'));
            $body = $systemClass->merge_data($body, $server_details);

            $message->setBody($body, 'text/html');
            $message->setFrom(array($GLOBALS['email']['from']['email'] => $GLOBALS['email']['from']['name']));
            // Send the email
            $mailer = Swift_Mailer::newInstance($transport);
            echo 'new';
            $mailer->send($message);


            //Add to user activity
            $accountClass->insert_user_activity("Company: '{$_POST['company_name']}' Created");

        }
    }
} else {
    //update company
    $company_id = $companyClass->update_company_details($_POST);
    //update company user
//    $user_id = $companyClass->update_company_user_details($_POST);
    //Add to user activity
    $accountClass->insert_user_activity("Company: '{$_POST['company_name']}' Updated");

    echo 'update';
}


