<?php

/**
 *  deactivate/ activate company
 *  2016-06-23 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();
$accountClass = new Accounts();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$company_id = $_GET['company_id'];
$active = $_GET['active'];
//enable/disable company
$companyClass->change_company_status($company_id, $active);

//get all users for company and disabled all their accounts
$users =$companyClass->get_all_company_users($company_id);
foreach($users as $user){
    //deactivate all company related accounts
    $accountClass->change_account_status($user['user_id'], $active);
}






