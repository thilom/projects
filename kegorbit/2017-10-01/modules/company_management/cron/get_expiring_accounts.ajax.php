<?php
/**
 * Return data for all accounts expiring in the next 30 day's & send email to system administrator
 *
 * 2016-08-22: Musa Khulu - Created
 */

//Includes
//$doc_root = '/home/palladia/public_html/kegorbit';
$doc_root = $_SERVER['DOCUMENT_ROOT'];
require_once "{$doc_root}/settings/init.php";
require_once "{$doc_root}/modules/company_management/classes/company.class.php";
require_once "{$doc_root}/modules/account_management/classes/account.class.php";
require_once "{$doc_root}/lib/helpers/swiftmailer/lib/swift_required.php";
require_once "{$doc_root}/modules/settings_management/classes/application.class.php";

//Vars
$companyClass = new Company();
$accountClass = new Accounts();
$appClass = new Application();

//Get company data
$company_data = $companyClass->get_companies_expiry('Y');

if(!empty($company_data)){
    $admin_accounts = $companyClass->get_admin();



    $accounts_table = '';
    $email_to = '';

    foreach($company_data as $companies_data){

        $accounts_table .= '<tr>
            <td style="width: 150px; padding:10px 5px; border: 1px solid #ccc; color:#333; background-color:#fff;">'.$companies_data['company_name'].'</td>
             <td style="width: 150px; padding:10px 5px; border: 1px solid #ccc; color:#333; background-color:#fff;">'.$companies_data['company_contact_name'].'</td>
              <td style="width: 150px; padding:10px 5px; border: 1px solid #ccc; color:#333; background-color:#fff;">'.$companies_data['company_contact_number'].'</td>
              <td style="width: 150px; padding:10px 5px; border: 1px solid #ccc; color:#333; background-color:#fff;">'.$companies_data['company_email'].'</td>
               <td style="width: 150px; padding:10px 5px; border: 1px solid #ccc; color:#333; background-color:#fff;">'.$companies_data['raw_renewal_date'].' ('.$companies_data['company_renewal_date'].'days)</td>' .
            '
        </tr>';
    }

    foreach($admin_accounts as $admin_account) {

        //Get SMTP Details
        $settings = $appClass->get_settings();
        $smtp_url = $settings['smtp_url'];
        $smtp_port = $settings['smtp_port'];
        $smtp_username = $settings['smtp_username'];
        $smtp_password = $settings['smtp_password'];

// Create the mail transport configuration
        $transport = Swift_SmtpTransport::newInstance($smtp_url, $smtp_port)->setUsername($smtp_username)->setPassword($smtp_password);
//Supposed to allow local domain sending to work from what I read
//        $transport->setLocalDomain('[127.0.0.1]');

// Create the message
        $message = Swift_Message::newInstance($transport);
        $email_to = $admin_account['user_email_address'];
        $message->setTo($email_to);

        $message->setSubject("KegOrbit  Accounts Expiring 30 Day's from Now");
        $body = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/html/email_company_template/expiry_accounts_email.html');

        $table_details = array('all_accounts_expiry' => $accounts_table);
        $body = $systemClass->merge_data($body, $table_details);


        $server_details = array('server' => $_SERVER['HTTP_HOST'], 'timestamp' => date('d-m-Y H:i:s'));
        $body = $systemClass->merge_data($body, $server_details);

        $message->setBody($body, 'text/html');
        $message->setFrom(array($GLOBALS['email']['from']['email'] => $GLOBALS['email']['from']['name']));
// Send the email
        $mailer = Swift_Mailer::newInstance($transport);
        $mailer->send($message);
    }

//Add to user activity
    $accountClass->insert_user_activity("Email Containing All Accounts expiring in 30 day's Sent to all admin accounts");

}

