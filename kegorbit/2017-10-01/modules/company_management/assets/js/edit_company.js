$(document).ready(function() {

    $("#company_contact_name").keyup(function(){
        $("#user_full_name").val($("#company_contact_name").val());
    });

    $("#company_email").keyup(function(){
        $("#user_email_address").val($("#company_email").val());
    });

    $("#company_contact_number").keyup(function(){
        $("#user_contact_numbers").val($("#company_contact_number").val());
    });


    $( "#save" ).click(function() {
        if ($('#company_id').val() == 0) {
            var bootstrapValidator = $('#editCompanyForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('company_email', true);
            bootstrapValidator.enableFieldValidators('user_email_address', true);
        }
        else{
            var bootstrapValidator = $('#editCompanyForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('company_email', false);
            bootstrapValidator.enableFieldValidators('user_email_address', false);
        }
    });

    //Form validations
    $.fn.bootstrapValidator.validators.duplicateUsername = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/ajax/check_duplicate_email.ajax.php?email=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });

            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };


    $('#editCompanyForm').bootstrapValidator({ 

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            company_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Company Name."}
                }
            },
            company_contact_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Company Contact Name."}
                }
            },
            company_email: {
                validators: {
                    notEmpty: {message: "Please Provide  the Company Email."},
                    duplicateUsername:{message: "Duplicate Email Address Detected."}
                }
            },
            keg_limit: {
                validators: {
                    notEmpty: {message: "Please Provide  the Keg Limit."}
                }
            },
            user_limit: {
                validators: {
                    notEmpty: {message: "Please Provide  the User Limit."}
                }
            },
            device_limit: {
                validators: {
                    notEmpty: {message: "Please Provide  the Device Limit."}
                }
            },
            delivery_street_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Street Name."}
                }
            },
            delivery_town: {
                validators: {
                    notEmpty: {message: "Please Provide  the Town/City."}
                }
            },
            invoice_street_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Street Name."}
                }
            },
            invoice_town: {
                validators: {
                    notEmpty: {message: "Please Provide  the Town/City."}
                }
            },
            user_full_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the User's Full Name."}
                }
            },
            user_email_address: {
                validators: {
                    notEmpty: {message: "Please Provide  the User's Email Name."},
                    duplicateUsername:{message: "Duplicate Email Address Detected."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/company_management/ajax/save_company_details.ajax.php',
            beforeSubmit: function () {
                var l = $( '#save' ).ladda();
                // Start loading
                l.ladda( 'start' );
            },
            success: function (data) {
                var l = $( '#save' ).ladda();
                l.ladda( 'stop' );

                if (data == 'update') {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = $('#company_name').val()+' Company Successfully Updated.';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#company_name').val()+" Company Successfully Updated",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To Companies List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                var l = $( '#save' ).ladda();
                                l.ladda( 'stop' );


                            } else {
                                document.location = 'index.php?m=company_management&a=list_companies';
                            }
                        })
                }
                else{
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = $('#company_name').val()+' Company Added!';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#company_name').val()+" Company Added!",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Create Another Company",
                            cancelButtonText: "Back To Companies List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $("#editCompanyForm")[0].reset();
                                var l = $( '#save' ).ladda();
                                l.ladda( 'stop' );

                            } else {
                                document.location = 'index.php?m=company_management&a=list_companies';
                            }
                        })
                }
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        var l = $( '#cancel' ).ladda();
        l.ladda( 'start' );
        document.location = 'index.php?m=company_management&a=list_companies';
    });

// mask input
//        $("#company_contact_number").mask("(999) 999-9999",{placeholder:"(999) 999-9999"});
//     $("#user_contact_numbers").mask("(999) 999-9999",{placeholder:"(999) 999-9999"});

    //renew contract
    $('#renew_account').click(function (event) {
        swal({
            title: "Are you sure?",
            text: "The Account will be renewed for another 365 days",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#458B00",
            confirmButtonText: "Renew Account",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '/modules/company_management/ajax/renew_company_account.ajax.php?company_id=' + $("#company_id").val(),
                        type: 'POST',
                        success: function () {
                            setTimeout(function () {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                    showMethod: 'slideDown',
                                    positionClass: 'toast-bottom-right',
                                    timeOut: 4000
                                };
                                toastr.success('Account has been  renewed for another year', 'COMPANY');

                            }, 1300);

                        }
                    });

                } else {
                }
    });
    });

    $('#same').each(function(){
        var self = $(this),
            label = self.next(),
            label_text = label.text();
        label.remove();
        self.iCheck({
            checkboxClass: 'icheckbox_line-aero',
            radioClass: 'iradio_line-aero',
            insert: '<span class="icheck_line-icon"></span>' + label_text
        });
    });

    $('#same').on('ifChecked', function() {
        $('.addr').prop('disabled', true);
        $('.addr1').data('value', $('.addr1').val());
        $('.addr2').data('value', $('.addr2').val());
        $('.addr3').data('value', $('.addr3').val());
        $('.addr4').data('value', $('.addr4').val());
        $('.addr').val('');
    })

    $('#same').on('ifUnchecked', function() {
        $('.addr').prop('disabled', false);
        $('.addr1').val($('.addr1').data('value'));
        $('.addr2').val($('.addr2').data('value'));
        $('.addr3').val($('.addr3').data('value'));
        $('.addr4').val($('.addr4').data('value'));
    })

    if ($('#same').iCheck('update')[0].checked ) {
        $('.addr').prop('disabled', true);
        $('.addr1').data('value', $('.addr1').val());
        $('.addr2').data('value', $('.addr2').val());
        $('.addr3').data('value', $('.addr3').val());
        $('.addr4').data('value', $('.addr4').val());
        $('.addr').val('');
    }

});