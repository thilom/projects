$(document).ready(function () {
    create_table();
    create_table2();
});


$('#activeCompany').click(function(){
    create_table();
});
$('#inactiveCompany').click(function(){
    create_table2();
});

function create_table() {
    var active = "Y";

    $('#activeCompaniesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/company_management/ajax/list_companies.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": false, className: 'text-center', "sWidth": "20%"}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#activeCompaniesTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

function create_table2() {
    var active = "N";

    $('#inactiveCompaniesTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/company_management/ajax/list_companies.ajax.php?active="+active,
        "aoColumns": [
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": false, className: 'text-center'}
        ],
        stateSave: true,
        stateDuration: 0
    });
    $('#inactiveCompaniesTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

$(document).on('click', '.deactivateBtn', function(event) {
    var deactivate_url = $(this).attr('href');
    var company_name = $(this).data('value');
        swal({
            title: "Deactivate: " + company_name+ "?",
            text: "All users associated with '"+ company_name +"' will be barred from using the system. You will be able to undo this action by activating '"+ company_name +"' account later under the Inative companies Tab",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Deactivate!",
            closeOnConfirm: true
        }, function () {
            $.ajax({
                type: 'GET',
                url: deactivate_url,
                success: function (data) {

                    $("#activeCompaniesTbl").empty();
                        create_table();
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 4000
                    };
                    toastMessage = company_name +  ' Account has been suspended';
                    toastr.success(toastMessage);
                }
            });
    });
    event.preventDefault();
});

$(document).on('click', '.activateBtn', function(event) {
    var activate_url = $(this).attr('href');
    var company_name = $(this).data('value');
    swal({
        title: "Activate: " + company_name+ "?",
        text: "All users associated with the '"+ company_name +"' were barred from using this system. You can activate their profile's in Account Management. You will be able to undo this action by deactivating the account later under the enabled companies Tab",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, Activate!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'GET',
            url: activate_url,
            success: function (data) {
                $("#inactiveCompaniesTbl").empty();
                create_table2();
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 4000
                };
                toastMessage = company_name +  ' Account has been activated';
                toastr.success(toastMessage);
            }
        });
    });
    event.preventDefault();
});

$(document).on('click', '.reset_user_password', function(event) {
    var company_id = $(this).data('value');
    $.ajax({
        type: 'GET',
        url: '/modules/company_management/ajax/reset_company_user_password.ajax.php?company_id='+company_id,
        success: function (data) {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                positionClass: 'toast-bottom-right',
                timeOut: 6000
            };
            toastMessage = 'EMAIL SENT! Email to reset password has been sent to the user.';
            toastr.success(toastMessage);
        }
    });
    event.preventDefault();
});

//create new company button
$('#create_company').click(function(){
document.location = 'index.php?m=company_management&a=edit_company&company_id=0';

});


function runAs(id, cName, user_id) {
    swal({
        title: "Switch to Account",
        text: "Are you sure you want to switch to '"+ cName +"'",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: 'GET',
            url: '/modules/company_management/ajax/switch_account.ajax.php?code=' + id + '&user_id=' + user_id,
            success: function (data) {
                document.location = '/index.php';
            }
        });
    });
};
