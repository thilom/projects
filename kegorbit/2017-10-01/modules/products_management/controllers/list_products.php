<?php
/**
 * products management
 * 2016-07-29 - Musa Khulu  Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/products_management/classes/products.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();
$productClass = new Products();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/products_management/html/list_products.html");

$singular = $settingsClass->get_container_terminology();
$plural = $settingsClass->get_container_terminology('plural');
$template_data = array(
    'container_singular' => $singular,
    'container_plural' => $plural,
);

$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;