<?php

/**
 * Call the edit stage page
 * 2016-06-20 - Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';


//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();


$type_id = isset($_GET['type_id'])? $_GET['type_id'] :0;

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/products_management/html/edit_type.html");

if($type_id != 0){
    //get selected stage
    $type_data = $settingsClass->get_type($type_id);

    $template_data = array(
        'type_id'       =>  $type_id,
        'type_names'       =>  $type_data[0]['type_name'],
        'type_name'       =>  $type_data[0]['type_name'],
        'type_description'       =>  $type_data[0]['type_description'],
        'type'       =>  'Edit',
        'expiry_date' =>$type_data[0]['type_expiration'],
        'best_before' => $type_data[0]['type_best_before'],
    );
}
else{

    $template_data = array(
        'type_id'       =>  $type_id,
        'type_names'       =>  'New Product',
        'type_name'       =>  '',
        'type_description'       =>  '',
        'type'       =>  'Create',
        'expiry_date' => '',
        'best_before' => '',
    );
}

$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
