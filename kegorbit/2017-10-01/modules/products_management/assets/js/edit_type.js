$(document).ready(function() {



    $( "#save" ).click(function() {
        if ($('#type_id').val() == 0) {
            var bootstrapValidator = $('#edittypeForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('type_name', true);
        }
        else{
            var bootstrapValidator = $('#edittypeForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('type_name', false);
        }
    });

    //Form validations
    $.fn.bootstrapValidator.validators.Product = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/modules/products_management/ajax/check_duplicate_new_product.ajax.php?product_name=' + $field.val() + '&id=' + $('#type_id').val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });

            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };


    $('#edittypeForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            type_name: {
                validators: {
                    notEmpty: {message: "Please Provide  the Product Name."},
                    Product: {message: "Duplicate Product Exists."}
                }
            },
            best_before: {
                validators: {
                    notEmpty: {message: "Please Provide  the Best Before Date in Days."}
                }
            },

            expiry_date: {
                validators: {
                    notEmpty: {message: "Please Provide  a Expiry Date in Days."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/products_management/ajax/save_type.ajax.php',
            beforeSubmit: function () {
            },
            success: function (data) {
                if (data == 'update') {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'Product Successfully Updated.';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#type_name').val() +" Successfully Updated",
                            text: $('#type_name').val() +" Successfully Updated",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To Product List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {

                            } else {
                                document.location = 'index.php?m=products_management&a=list_products';
                            }
                        })
                }
                else{
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'New Product Added!';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#type_name').val() +" Successfully Added",
                            text: $('#type_name').val() +" Successfully Added",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Create Another Product",
                            cancelButtonText: "Back To Product List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $("#edittypeForm")[0].reset();

                            } else {
                                document.location = 'index.php?m=products_management&a=list_products';
                            }
                        })
                }
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        document.location = 'index.php?m=products_management&a=list_products';
    });
});