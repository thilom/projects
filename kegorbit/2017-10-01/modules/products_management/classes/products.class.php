<?php
/**
 * Created by PhpStorm.
 *
 * 2016-08-08 - Musa Khulu - added check_duplicate_product to check if product already exists in the table
 * 2016-10-11: Thilo Muller - Added $product_id to check_duplicate_product()
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";
class Products  extends System
{
    /**
     * Company code
     *
     * @var string
     */
    private $company_code = '';

    /**
     * State constructor.
     */
    function __construct()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }
    }

    /*
  * Get container terminology
  * @param $company_id (int)
  */
    function get_container_terminology($value = 'singular')
    {

        if ($value == 'singular') {
            $statement = "SELECT container_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        } else {
            $statement = "SELECT container_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }

    /*
 * Get state terminology
 * @param $company_id (int)
 */
    function get_state_terminology($value = 'singular')
    {

        if ($value == 'singular') {
            $statement = "SELECT state_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        } else {
            $statement = "SELECT state_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }

    /*
    * Get stage terminology
    * @param $company_id (int)
    */
    function get_stage_terminology($value = 'singular')
    {

        if ($value == 'singular') {
            $statement = "SELECT stage_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        } else {
            $statement = "SELECT stage_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }

    /*
   * Get customer terminology
   * @param $company_id (int)
   */
    function get_customer_terminology($value = 'singular')
    {

        if ($value == 'singular') {
            $statement = "SELECT client_singular
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        } else {
            $statement = "SELECT client_plural
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_terminology";
            $sql = $GLOBALS['dbCon']->prepare($statement);
        }
        $sql->execute();
        $sql_company_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_company_data;
    }
    /*
   * Get types
   * @param active (string)
   */
    function get_types($active = '') {


        $statement = "SELECT type_id, type_name,type_description,  active, type_expiration, type_best_before
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_types";
        if ($active == 'N')
            $statement .= " WHERE active = 'N'";
        else
            $statement .= " WHERE active = 'Y' OR active IS NULL";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();

        $types = $sql->fetchAll();
        $sql->closeCursor();
        return $types;
    }

    /**
     * Activate/ Deactivate type
     * @param $type_id , $active
     * @return array
     */
    function change_type_status($type_id, $active)
    {

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_types
                        SET active = :active
                        WHERE type_id = :type_id";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':active', $active);
        $sql_update->bindParam(':type_id', $type_id);
        $sql_update->execute();
        $sql_update->closeCursor();
    }


    /**
     * Save/update type info
     *
     * @param $type_data
     * @return mixed
     */
    function save_type($type_data) {

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_types
                              (type_name, type_description, type_expiration, type_best_before )
                            VALUES
                              (:type_name, :type_description, :type_expiration, :type_best_before)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bindParam(':type_name', $type_data['type_name']);
        $sql_insert->bindParam(':type_description', $type_data['type_description']);
        $sql_insert->bindParam(':type_expiration', $type_data['expiry_date']);
        $sql_insert->bindParam(':type_best_before', $type_data['best_before']);
        $sql_insert->execute();
        $sql_insert->closeCursor();

        $type_id = $GLOBALS['dbCon']->lastinsertid();
        return $type_id;
    }

    /**
     * Save/update stage info
     *
     * @param $type_data
     * @return mixed
     */
    function update_type($type_data)
    {
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_types
                            SET type_name = :type_name,
                                type_description = :type_description,
                                type_expiration = :type_expiration,
                                type_best_before = :type_best_before
                            WHERE type_id = :type_id
                            LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':type_id', $type_data['type_id']);
        $sql_update->bindParam(':type_name', $type_data['type_name']);
        $sql_update->bindParam(':type_description', $type_data['type_description']);
        $sql_update->bindParam(':type_expiration', $type_data['expiry_date']);
        $sql_update->bindParam(':type_best_before', $type_data['best_before']);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

    /*
 * Get type
 * @param type_id (int)
 */
    function get_type($type_id)
    {

        $statement = "SELECT type_id, type_name, active, type_description,type_expiration,type_best_before
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_types
                        WHERE type_id = :type_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':type_id', $type_id);
        $sql->execute();
        $types = $sql->fetchAll();
        $sql->closeCursor();
        return $types;
    }

    /**
     * Get a product name and check if it already exists
     *
     * @param string $product_name
     * @return int
     */
    function check_duplicate_product($product_name, $product_id=''){

        $statement = "SELECT count(type_name)
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_types
                        WHERE type_name = :type_name";
        if (!empty($product_id)) $statement .= " AND type_id <> :product_id ";

        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':type_name', $product_name);
        if (!empty($product_id)) $sql->bindParam(':product_id', $product_id);
        $sql->execute();
        $product = $sql->fetchColumn();
        $sql->closeCursor();
        return $product;
    }


    /**
     * Check if a product exists
     * Returns FALSE if it doesn't and the product ID if it does.
     *
     * @param $product_name
     * @return bool
     */
    function product_exists($product_name) {
        $statement = "SELECT type_id
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_types
                        WHERE type_name = :type_name
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':type_name', $product_name);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        if ($sql_result === false || count($sql_result) == 0) {
            return false;
        } else {
            return $sql_result['type_id'];
        }
    }
}




