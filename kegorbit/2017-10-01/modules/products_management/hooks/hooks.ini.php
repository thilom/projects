<?php
/**
 * List of dashboard hooks
 *
 * 2016-07-03: Thilo Muller - Created
 */

$ini_hooks = array();

$ini_hooks[0] = array('dir' => 'dashboard_products',
                        'order' => 30,
                        'type' => 'dashboard',
                        'position' => 'left');


