<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/classes/container.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/company_management/classes/company.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";

//Vars
$companyClass = new Company();
$containerClass = new Container();
$systemClass = new System();


$dir = substr(__FILE__, 0, strrpos(__FILE__, '/'));
$block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/products_management/hooks/dashboard_products/html/block.html");

$colors = array('000022', '001166', '09146D', '0012A8', '112288', '2B2B2B', '223399', '3D3E4C', '3F467F', '8888AA', '7986F2', '99A4FF');

//Setup state count
$template_data['table_line'] = '';
$template_data['labels'] = '';
$template_data['data'] = '';
$template_data['colors'] = '';

//Get company details
$company_data = $companyClass->get_active_company();

//check if user has permission to view Products Status  Block
if (isset($_SESSION['user']['run_as'])) {
    $access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'dashboard_management', 'product_status_block');
} else {
    $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'dashboard_management', 'product_status_block');
}

if (!$access) {
    $template_data['display_or_not'] = 'Display:none';
} else {
    $template_data['display_or_not'] = '';
}


//Get number of expired containers
$expire_data = $containerClass->get_expired_containers_count();
if ($expire_data['expired'] == 0) {
    $disabled_expire = 'disabled';
} else {
    $disabled_expire = '';
}
if ($expire_data['near_expired'] == 0) {
    $disabled_near_expire = 'disabled';
} else {
    $disabled_near_expire = '';
}
if ($expire_data['past_sell_by'] == 0) {
    $disabled_past_sell = 'disabled';
} else {
    $disabled_past_sell = '';
}
if ($expire_data['near_sell_by'] == 0) {
    $disabled_near_sell_by = 'disabled';
} else {
    $disabled_near_sell_by = '';
}
if ($expire_data['ok'] == 0) {
    $disabled_ok = 'disabled';
} else {
    $disabled_ok = '';
}

$template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[11]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td style='width: 70%'>Expired</td>
                        <td>{$expire_data['expired']}</td>
                        <td><button class='btn btn-xs btn-info {$disabled_expire} view_product'  data-value='{$expire_data['expired']}' data-action='expired' >View</button></td>
                    </tr>";
$template_data['labels'] .= "'Expired',";
$template_data['colors'] .= "'#{$colors[11]}',";
$template_data['data'] .= "{$expire_data['expired']},";

$template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[10]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>Within {$company_data['near_date_range']} days of expiry</td>
                        <td>{$expire_data['near_expired']}</td>
                        <td><button class='btn btn-xs btn-info {$disabled_near_expire} view_product'  data-value='{$expire_data['near_expired']}' data-action='near_expired'>View</button></td>
                    </tr>";
$template_data['labels'] .= "'Near Expired',";
$template_data['colors'] .= "'#{$colors[10]}',";
$template_data['data'] .= "{$expire_data['near_expired']},";

$template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[9]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>Past Sell By Date</td>
                        <td>{$expire_data['past_sell_by']}</td>
                        <td><button class='btn btn-xs btn-info {$disabled_past_sell} view_product'  data-value='{$expire_data['past_sell_by']}' data-action='past_sell_by'>View</button></td>
                    </tr>";
$template_data['labels'] .= "'Past Sell By Date',";
$template_data['colors'] .= "'#{$colors[9]}',";
$template_data['data'] .= "{$expire_data['past_sell_by']},";

$template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[8]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>Within {$company_data['near_date_range']} days of Sell By Date</td>
                        <td>{$expire_data['near_sell_by']}</td>
                        <td><button class='btn btn-xs btn-info {$disabled_near_sell_by} view_product' data-value='{$expire_data['near_sell_by']}' data-action='near_sell_by'>View</button></td>
                    </tr>";
$template_data['labels'] .= "'Near Sell By Date',";
$template_data['colors'] .= "'#{$colors[8]}',";
$template_data['data'] .= "{$expire_data['near_sell_by']},";

$template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[8]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>OK</td>
                        <td>{$expire_data['ok']}</td>
                        <td><button class='btn btn-xs btn-info {$disabled_ok} view_product' data-value='{$expire_data['ok']}' data-action='ok'>View</button></td>
                    </tr>";
$template_data['labels'] .= "'OK',";
$template_data['colors'] .= "'#{$colors[7]}',";
$template_data['data'] .= "{$expire_data['ok']},";


$template_data['labels'] = trim($template_data['labels'], ',');
$template_data['colors'] = trim($template_data['colors'], ',');
$template_data['data'] = trim($template_data['data'], ',');

$block = $systemClass->merge_data($block, $template_data);

echo $block;