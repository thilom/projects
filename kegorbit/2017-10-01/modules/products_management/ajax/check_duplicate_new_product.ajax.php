<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-08-08  Musa Khulu - Created
 * Time: 01:59 PM
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/products_management/classes/products.class.php';


//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$productsClass = new Products();
$product_id = isset($_GET['id'])?(int) $_GET['id']:'';

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$product_name = $_GET['product_name'];

$duplicate = $productsClass->check_duplicate_product($product_name, $product_id);
echo $duplicate;
