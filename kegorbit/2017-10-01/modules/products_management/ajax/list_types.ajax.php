<?php
/**
 * setting management - fetch a list of all companies
 * 2016-07-01- Musa Khulu - Created
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/products_management/classes/products.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingClass = new Settings();
$productsClass = new Products();

$output = array();
$output['aaData'] = '';
$active = $_GET['active'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all types
$types = $productsClass->get_types($active);


if(empty($types)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('type_name', 'type_best_before', 'type_expiration');

    foreach($types as $type)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'type_id')
                $stage_id = $type[$aColumns[$i]];

            $row[] = $type[$aColumns[$i]];
        }

        if($active == 'Y'){
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=products_management&a=edit_product&type_id=' . $type['type_id'] . '" class="btn btn-primary btn-sm "><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span> <span class="hidden-xs"> Edit</span></a>
							<button class="btn btn-primary btn-sm  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a class="deactivateBtn" href="/modules/products_management/ajax/edit_type_status.ajax.php?type_id=' . $type['type_id'] . '&active=N" data-type="' . $type['type_name'] . '"><i class="fa fa-ban"></i></span> Discontinue</a>
								</li>
							</ul>
						</div>';
        }

        else {
            $row[] = '<div class="btn-group">
                    <a href="/index.php?m=products_management&a=edit_product&type_id=' . $type['type_id'] . '" class="btn btn-primary  btn-sm"><span class="btn-label"><i class="fa fa-pencil-square-o"></i></span> <span class="hidden-xs"> Edit</span></a>
							<button class="btn btn-primary  btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a class="activateBtn" href="/modules/products_management/ajax/edit_type_status.ajax.php?type_id=' . $type['type_id'] . '&active=Y"  data-type="' . $type['type_name'] . '"><i class="fa fa-check"></i></span> Activate</a>
								</li>
							</ul>
						</div>';
        }

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
