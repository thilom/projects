<?php

/**
 *  deactivate/ activate type
 *  2016-06-23 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/products_management/classes/products.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$productsClass = new Products();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$type_id = $_GET['type_id'];
$active = $_GET['active'];
//enable/disable type
$productsClass->change_type_status($type_id, $active);





