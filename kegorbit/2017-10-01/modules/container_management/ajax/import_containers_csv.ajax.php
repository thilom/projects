<?php
/**
 * Upload a CSV file for processing (step 1 of subscriber import)
 *
 * 2015-11-14: Thilo Muller - Created
 */

//Includes
$ajax = 1;
require_once '../../../settings/init.php';

//Vars
$filename = "{$_SESSION['user']['id']}.csv";
$csv_data = array('filename' => '', 'rows' => 0, 'error' => false, 'message' => '');

if (!isset($_SESSION['user']['id'])) {
    $csv_data['error'] = true;
    $csv_data['message'] = "Unauthorized entry prohibited. Please log in.";
    echo json_encode($csv_data);
    die();
}

if (!isset($_FILES['subscribers_csv'])) {
    $csv_data['error'] = true;
    $csv_data['message'] = "Please select a file to upload.";
    echo json_encode($csv_data);
    die();
}

//Check for file corruption attack
if (!isset($_FILES['subscribers_csv']['error']) || is_array($_FILES['subscribers_csv']['error'])) {
    $csv_data['error'] = true;
    $csv_data['message'] = "Sorry, upload error. Please try again with a different file.";
    echo json_encode($csv_data);
    die();
}

//Check for upload error
switch ($_FILES['subscribers_csv']['error']) {
    case UPLOAD_ERR_OK:
        break;
    case UPLOAD_ERR_NO_FILE:
        $csv_data['error'] = true;
        $csv_data['message'] = "Please select a file to upload.";
        echo json_encode($csv_data);
        die();
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
        $csv_data['error'] = true;
        $csv_data['message'] = "Maximum file size exceeded.";
        echo json_encode($csv_data);
        die();
    default:
        $csv_data['error'] = true;
        $csv_data['message'] = "We have encountered an unknown error. Please try reuploading the file.";
        echo json_encode($csv_data);
        die();
}

//Check file size
if ($_FILES['subscribers_csv']['size'] > 10*1048576) {
    $csv_data['error'] = true;
    $csv_data['message'] = "Maximum file size exceeded.";
    echo json_encode($csv_data);
    die();
}

//Check if it's a csv file
$finfo = new finfo(FILEINFO_MIME_TYPE);
$ext = $finfo->file($_FILES['subscribers_csv']['tmp_name']);

if (false === $ext = array_search(
        $finfo->file($_FILES['subscribers_csv']['tmp_name']),
        array(
            'csv' => 'text/csv',
            'txt' => 'text/plain'
        ),
        true
    )) {
    $csv_data['error'] = true;
    $csv_data['message'] = "Only CSV files allowed.";
    echo json_encode($csv_data);
    die();
}


//Move uploaded file
move_uploaded_file($_FILES['subscribers_csv']['tmp_name'], "{$_SERVER['DOCUMENT_ROOT']}/uploads/csv_documents/$filename");
$csv_data['filename'] = $filename;

//Count number of rows
if (($handle = fopen("{$_SERVER['DOCUMENT_ROOT']}/uploads/csv_documents/$filename", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if ($data[0] == 'Serial Number') continue;
        $csv_data['rows']++;
    }
    fclose($handle);
}

//Some fun stuff
$csv_data['message'] = "Ready to import {$csv_data['rows']} entries. Hit continue.";

echo json_encode($csv_data);
