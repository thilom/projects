<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-08-02
 * Time: 02:23 PM
 * Musa Khulu - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';


//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;



//Vars
$containerClass = new Container();
$bar_code = $_GET['bar_code'];
$duplicate = $containerClass->check_duplicate_bar_code($bar_code);
echo $duplicate;
