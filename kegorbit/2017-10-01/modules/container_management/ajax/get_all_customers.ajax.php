<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-08-04
 * Time: 01:34 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/customer_management/classes/customer.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$customerClass = new Customer();


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;
 //get all clients
$customers = $customerClass->get_customers(true,1,5000);
echo json_encode($customers);


