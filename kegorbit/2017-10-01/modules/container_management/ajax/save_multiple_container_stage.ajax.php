<?php

/**
 * save new container state of multiple selected rows
 *  2016-07-26 - Musa Khulu - Created
 *
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/products_management/classes/products.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/customer_management/classes/customer.class.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);
//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();
$settingsClass = new Settings();
$accountClass = new Accounts();
$productClass = new Products();
$customerClass = new Customer();
$container_id = explode(',', $_GET['container_id']);
$new_stage = $_GET['new_stage'];
$action = isset($_GET['action']) ? $_GET['action'] : 0;
$new_product = isset($_GET['new_product']) ? $_GET['new_product'] : 0;
$new_client = isset($_GET['new_client']) ? $_GET['new_client'] : 0;
$iteration = isset($_GET['iteration']) ? $_GET['iteration'] : '';
$batch_no = isset($_GET['batch_no']) ? $_GET['batch_no'] : '';

//Get stage Name
$stage_data = $settingsClass->get_stage($new_stage);
$stage_name = $stage_data[0]['stage_name'];


//Get product Name
if ($new_product > 0) {
    $product_data = $productClass->get_type($new_product);
    $product_name = $product_data[0]['type_name'];
} else {
    $product_name = '';
}

//Get client Name
if ($new_client > 0) {
    $customer_data = $customerClass->get_customer($new_client);
    $client_name = $customer_data['customer_name'];
} else {
    $client_name = '';
}

//Best before date
if (isset($_GET['best_before'])) {
    $best_before_date = date('Y-m-d', strtotime($_GET['best_before']));
} else {
    $best_before_date = '';
}

//Best before date
if (isset($_GET['expire'])) {
    $expire_date = date('Y-m-d', strtotime($_GET['expire']));
} else {
    $expire_date = '';
}

//Delivery date
if (isset($_GET['deliverydate'])) {
    $delivery_date = date('Y-m-d', strtotime($_GET['deliverydate']));
} else {
    $delivery_date = '';
}

if (isset($_GET['filldate'])) {
    $fill_date = date('Y-m-d', strtotime($_GET['filldate']));
} else {
    $fill_date = '';
}

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if (!$user_logged_in) die;

foreach ($container_id as $data) {
//save new
    if ($new_product != 0) {
        $containerClass->save_new_container_stage($data, $new_stage, $action, $_GET['new_product'], $best_before_date, $expire_date, $batch_no, $delivery_date, $fill_date);
        if ($iteration == 0) {
            $accountClass->insert_user_activity("Multiple Containers Updated to '$stage_name' (Product: '$product_name')");
        }
    } elseif ($new_client != 0) {
        $containerClass->save_new_container_stage($data, $new_stage, $action, $_GET['new_client'], $best_before_date, $expire_date, $batch_no, $delivery_date, $fill_date);
        if ($iteration == 0) {
            $accountClass->insert_user_activity("Multiple Containers Updated to '$stage_name' (Customer: '$client_name')");
        }
    } else {
        $containerClass->save_new_container_stage($data, $new_stage, $action, $new_product, $best_before_date, $expire_date, $batch_no, $delivery_date, $fill_date);
        if ($iteration == 0) {
            $accountClass->insert_user_activity("Multiple Containers Updated to '$stage_name'");
        }
    }
}





