<?php
/**
 * Process an uploaded CSV file to add containers
 *
 * 2016-09-13: Thilo Muller - Created
 */

//Includes
require_once '../../../settings/init.php';
require_once '../classes/container.class.php';
require_once '../../settings_management/classes/settings.class.php';
require_once '../../settings_management/classes/state.class.php';
require_once '../../customer_management/classes/customer.class.php';
require_once '../../products_management/classes/products.class.php';
require_once '../../account_management/classes/account.class.php';



//Vars
$containerClass = new Container();
$settingClass = new Settings();
$stateClass = new State();
$customerClass = new Customer();
$productClass = new Products();
$accountClass = new Accounts();
$import_list = array();
$filename = $_GET['file'];
$result['success'] = '1';
$count_csv_lines = 0;
$result['errors'] = array();
$read_limit = 1;
$lines_read = 0;
$badCount = 0;
$duplicateCount = 0;
$complete = false;
$lines_left = 0;

//Get the default stage - Used later for empty stages
$default_stage = $settingClass->get_default_stage();
if (empty($default_stage)) {
    $default_stage = '';
} else {
    $default_stage = $default_stage['stage_name'];
}

//Get default State
$default_state = $stateClass->get_default_state();
if (empty($default_state)) {
    $default_state = '';
} else {
    $default_state = $default_state['state_name'];
}


//Open result files for writing
$success_file = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/csv_documents/success_$filename", 'a');
$bad_file = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/csv_documents/bad_$filename", 'a');
$duplicate_file = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/csv_documents/duplicate_$filename", 'a');

//Read file and import
$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/csv_documents/$filename", 'r');
$copy_fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/uploads/csv_documents/tmp_$filename", 'w+');
while (($data = fgetcsv($fp, 1000, ",")) !== false) {

    if ($lines_read < $read_limit) {
        $lines_read++;

        //Create readable variables
        $container_data = array();
        $container_data['serial_number'] = $data['0'];
        $container_data['bar_code'] = $data['1'];
        $container_data['type'] = $data['4'];
        $container_data['capacity'] = $data['2'];
        $container_data['capacity_type'] = $data['3'];
        $container_data['current_stage'] = empty($data['5'])?$default_stage:$data['5'];
        $container_data['current_state'] = empty($data['11'])?$default_state:$data['11'];
        $container_data['contents'] = $data['7'];
        $container_data['sell_by_date'] = $data['8'];
        $container_data['expiry_date'] = $data['9'];
        $container_data['current_client'] = $data['6'];
        $container_data['batch_no'] = $data['10'];
        $container_data['contact_name'] = $data['12'];
        $container_data['contact_number'] = $data['13'];
        $container_data['contact_email'] = $data['14'];
        $container_data['physical1'] = $data['15'];
        $container_data['physical2'] = $data['16'];
        $container_data['physical3'] = $data['17'];
        $container_data['physical4'] = $data['18'];
        $container_data['postal1'] = $data['19'];
        $container_data['postal2'] = $data['20'];
        $container_data['postal3'] = $data['21'];
        $container_data['postal4'] = $data['22'];
        $container_data['same'] = $data['23'];

        //Skip headers
        if ($container_data['serial_number'] == 'Serial Number') continue;

        //Skip containers without serial numbers
        if (empty($container_data['serial_number'])) {
            $badCount++;
            $container_data['message'] = 'No Serial Number';
            fputcsv($bad_file, $container_data);
            continue;
        }

        //Check if container already exists
        $serial_count = $containerClass->check_duplicate_serial_number($container_data['serial_number']);
        if ($serial_count > 0) {
            $duplicateCount++;
            fputcsv($duplicate_file, $container_data);
            continue;
        }

        //Check if type exists. Insert if it doesn't
        if (!empty($container_data['type'])) {
            $type_id = $settingClass->type_exists($container_data['type']);
            if ($type_id === false) {
                $type_data = array();
                $type_data['type_name'] = $container_data['type'];
                $type_data['type_capacity'] = "{$container_data['capacity']} {$container_data['capacity_type']}";
                $type_id = $settingClass->save_type($type_data);
            }
            $container_data['type'] = $type_id;
        } else {
            $container_data['type'] = '';
        }

        //Check if State exists, add if it doesn't
        if (!empty($container_data['current_state'])) {
            $state_id = $stateClass->state_exists($container_data['current_state']);
            if ($state_id === false) {
                $state_data['state_name'] = $container_data['current_state'];
                $state_data['out_of_circulation'] = 0;
                $state_id = $stateClass->save_state(0, $state_data);
            }
            $container_data['current_state'] = $state_id;
        } else {
            $container_data['current_state'] = '';
        }

        //Check if Stages exist, add if they don't
        if (!empty($container_data['current_stage'])) {
            $stage_id = $settingClass->stage_exists($container_data['current_stage']);
            if ($stage_id === false) {
                $stage_data = array();
                $stage_data['stage_name'] = $container_data['current_stage'];
                $stage_data['stage_description'] = '';
                $stage_data['onselect'] = '';
                $stage_id = $settingClass->save_stage($stage_data);
            }
            $container_data['current_stage'] = $stage_id;
        } else {
            $container_data['current_stage'] = '';
        }


        //Check if client exists, add if not
        if (!empty($container_data['current_client'])) {
            $client_id = $customerClass->customer_exists($container_data['current_client']);
            if ($client_id === false) {
                $customer_data = array();
                $customer_data['customer_name'] = $container_data['current_client'];
                $customer_data['customer_contact'] = $container_data['contact_name'];
                $customer_data['customer_contact_number'] = $container_data['contact_number'];
                $customer_data['customer_contact_email'] = $container_data['contact_email'];
                $customer_data['customer_physical_address1'] = $container_data['physical1'];
                $customer_data['customer_physical_address2'] = $container_data['physical2'];
                $customer_data['customer_physical_address3'] = $container_data['physical3'];
                $customer_data['customer_physical_address4'] = $container_data['physical4'];
                if ($container_data['same'] == 'Y') {
                    $customer_data['customer_postal_address1'] = '';
                    $customer_data['customer_postal_address2'] = '';
                    $customer_data['customer_postal_address3'] = '';
                    $customer_data['customer_postal_address4'] = '';
                } else {
                    $customer_data['customer_postal_address1'] = $container_data['postal1'];
                    $customer_data['customer_postal_address2'] = $container_data['postal2'];
                    $customer_data['customer_postal_address3'] = $container_data['postal3'];
                    $customer_data['customer_postal_address4'] = $container_data['postal4'];
                }
                $customer_data['same_as_physical'] = $container_data['same'];
                $customer_id = $customerClass->save_customer(0, $customer_data);
            }
            $container_data['current_client'] = $client_id;

        } else {
            $container_data['current_client'] = '';
        }

        //Check if Product exits. Add if it doesn't
        if (!empty($container_data['contents'])) {
            $product_id = $productClass->product_exists($container_data['contents']);
            if ($product_id === false) {
                $product_data = array();
                $product_data['type_name'] = $container_data['contents'];
                $product_data['type_description'] = '';
                $product_data['expiry_date'] = '';
                $product_data['best_before'] = '';
                $product_id = $productClass->save_type($product_data);
            }
            $container_data['contents'] = $product_id;
        } else {
            $container_data['contents'] = '';
        }


        //No more failures, add to import list
        $import_list[] = $container_data;
        fputcsv($success_file, $container_data);

    } else {
        //Copy to new file
        fputcsv($copy_fp, $data);
        $lines_left++;
    }
}
fclose($fp);
fclose($copy_fp);
fclose($success_file);
fclose($bad_file);
fclose($duplicate_file);

//Copy leftover data to read file
if ($lines_left == 0) {
    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/uploads/bulkmail/csv_imports/tmp_$filename")) unlink($_SERVER['DOCUMENT_ROOT'] . "/uploads/bulkmail/csv_imports/tmp_$filename");
    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/uploads/bulkmail/csv_imports/$filename")) unlink($_SERVER['DOCUMENT_ROOT'] . "/uploads/bulkmail/csv_imports/$filename");
    $complete = true;
} else {
    rename($_SERVER['DOCUMENT_ROOT'] . "/uploads/csv_documents/tmp_$filename", $_SERVER['DOCUMENT_ROOT'] . "/uploads/csv_documents/$filename");
}

if(count($import_list) > 0) {
    foreach ($import_list as $container_data) {
        $container_id = $containerClass->save_new_container($container_data);
        $containerClass->save_container_history($container_data['serial_number'], 'Imported From CSV');

        //Dates
        if (!empty($container_data['sell_by_date']))  $containerClass->update_container_dates($container_data['serial_number'], $container_data['sell_by_date'], $container_data['expiry_date']);
    }
}

if ($complete === true) {
    $accountClass->insert_user_activity("Containers imported via CSV");
    $settingClass->disable_import();
}

$result = array('complete' => $complete,
    'importCount' => count($import_list),
    'badCount' => $badCount,
    'duplicateCount' => $duplicateCount,
    'successFile' => "/uploads/csv_documents/success_{$filename}",
    'badFile' => "/uploads/csv_documents/bad_{$filename}",
    'duplicateFile' => "/uploads/csv_documents/duplicate_{$filename}" );

echo json_encode($result);

