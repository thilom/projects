<?php
/**
 * container management - fetch a list of all containers
 *
 * 2016-06-24- Musa Khulu - Created
 * 2016-09-29: Thilo Muller - Removed Edit button
 *                          - Added scrollable menu
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();
$companyClass = new Company();
$output = array();
$output['aaData'] = '';
$active = $_GET['active'];

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//get a list of all containers related to logged in user
$containers = $containerClass->get_containers($active);

//get the company date format
if(isset($_SESSION['user']['run_as'])) {
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['run_as']);
} else {
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
}
$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];

if(empty($containers)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
} else {
    $aColumns = array('container_serial_number', 'container_bar_code', 'container_type','stage_name', 'customer_name');

    foreach($containers as $container) {
        $row = array();
        $row[] = [];

        //check if user has permission to edit containers
        if(isset($_SESSION['user']['run_as'])){
            $edit_container = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'container_management', 'enable_editing');
        }
        else{
            $edit_container = $securityClass->check_user_permission($_SESSION['user']['id'], 'container_management', 'enable_editing');
        }
        if($edit_container == false){
            $row[] = $container['container_serial_number'];
            $row[] = $container['container_bar_code'];
        }
        else{
            $row[] = "<a title='Edit This Record' href='/index.php?m=container_management&a=edit_container&container_id={$container['container_serial_number']}'>{$container['container_serial_number']}</a>";
            $row[] = "<a title='Edit This Record' href='/index.php?m=container_management&a=edit_container&container_id={$container['container_serial_number']}'>{$container['container_bar_code']}</a>";
        }

        $row[] = $container['container_type'];
        $row[] = $container['stage_name'];
        $row[] = empty($container['customer_name'])?'Brewery':$container['customer_name'];

        $date = date($format,strtotime($container['container_date']));
        $row[] = $date;
        $row[] = $container['type_name'] . (empty($container['batch_number'])?'':" (Batch: {$container['batch_number']})");
        $row[] = $container['state_name'];
        $row[] = $container['container_capacity'].''.$container['container_capacity_type'];

        $output['aaData'][] = $row;
    }
}

echo json_encode($output);
