var table, table2, container_stage;
var clickBound1 = false;
var clickBound2 = false;
var selectedTR = new Array();
var setCounter = false;
var done, counter;


$(document).ready(function () {
    create_table();
    create_table2();

    var dFormat = $('#dateFormat').val();
    $('#sellBy').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });

    $('#expireDate').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });

    $('#deliveryDate').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });
    $('#deliveryDate').datepicker('update', new Date());

    $('#fillDate').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });
    $('#fillDate').datepicker('update', new Date());
    setCounter = true;

    $('#copy').click(function () {
        $(".copyButton").trigger('click');
    });
    $('#excel').click(function () {
        $(".excelButton").trigger('click');
    });
    $('#csv').click(function () {
        $(".csvButton").trigger('click');
    });
    $('#pdf').click(function () {
        $(".pdfButton").trigger('click');
    });
    $('#print').click(function () {
        $(".printButton").trigger('click');
    });

    $('#copy2').click(function () {
        $(".copyButton").trigger('click');
    });
    $('#excel2').click(function () {
        $(".excelButton").trigger('click');
    });
    $('#csv2').click(function () {
        $(".csvButton").trigger('click');
    });
    $('#pdf2').click(function () {
        $(".pdfButton").trigger('click');
    });
    $('#print2').click(function () {
        $(".printButton").trigger('click');
    });

    $(document).on('click', '.container_state', function (event) {
        var container_id = $(this).data('serial');
        var container_state = $(this).data('state');
        var new_state = $(this).text();
        swal({
            title: "New State: '" + new_state + "'",
            text: "Modify Container state to" + new_state,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Modify!"
        }).then(function (result) {
            $.ajax({
                type: 'GET',
                url: "/modules/container_management/ajax/save_new_container_state.ajax.php?container_id=" + container_id + "&new_state=" + container_state,
                success: function (data) {
                    $("#activeContainerTbl").empty();
                    create_table();
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'Container State Updated to:' + new_state;
                    toastr.success(toastMessage);
                }
            });
        })
        event.preventDefault();
    });

    $(document).on('click', '.container_stage', function (event) {
        var container_id = $(this).data('serial');
        var container_stage = $(this).data('stage');
        $.ajax({
            type: 'POST',
            url: '/modules/container_management/ajax/get_next_action.ajax.php?stage_id=' + container_stage,
            success: function (data) {
                // clear the client and product
                if (data == 5) {
                    $.ajax({
                        type: 'POST',
                        url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_id + "&new_stage=" + container_stage + "&action=clear_product_client",
                        success: function (data) {
                            $("#activeContainerTbl").empty();
                            create_table();
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                positionClass: 'toast-bottom-right',
                                timeOut: 6000
                            };
                            toastMessage = 'Container ' + $('#stage').val() + ' Updated';
                            toastr.success(toastMessage);
                        }
                    });
                }
                //clear the client
                else if (data == 4) {
                    for (i = 0; i < table.rows('.selected').data().length; i++) {
                        $.ajax({
                            type: 'POST',
                            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_id + "&new_stage=" + container_stage + "&action=clear_client",
                            success: function (data) {
                                $("#activeContainerTbl").empty();
                                create_table();
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                    showMethod: 'slideDown',
                                    positionClass: 'toast-bottom-right',
                                    timeOut: 6000
                                };
                                toastMessage = 'Container ' + $('#stage').val() + ' Updated';
                                toastr.success(toastMessage);
                            }
                        });
                    }
                }
                //clear the product
                else if (data == 3) {
                    for (i = 0; i < table.rows('.selected').data().length; i++) {
                        $.ajax({
                            type: 'POST',
                            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_id + "&new_stage=" + container_stage + "&action=clear_product",
                            success: function (data) {
                                $("#activeContainerTbl").empty();
                                create_table();
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                    showMethod: 'slideDown',
                                    positionClass: 'toast-bottom-right',
                                    timeOut: 6000
                                };
                                toastMessage = 'Container ' + $('#stage').val() + ' Updated';
                                toastr.success(toastMessage);
                            }
                        });
                    }
                }
                //Request user to select the client
                else if (data == 2) {
                    $('#container_id').val(container_id);
                    $('#clientSelectModal').modal();
                }
                //Request user to select the product
                else if (data == 1) {
                    $('#serial').val(container_id);
                    $('#productSelectModal').modal();

                } else {
                    swal({
                        title: 'Container Stage Change',
                        text: "The selected containers will updated",
                        showCancelButton: true
                    }).then(function (result) {
                        $.ajax({
                            type: 'POST',
                            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_id + "&new_stage=" + container_stage,
                            success: function (data) {
                                $("#activeContainerTbl").empty();
                                create_table();
                            }
                        });


                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            positionClass: 'toast-bottom-right',
                            timeOut: 6000
                        };
                        toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                        toastr.success(toastMessage);
                    })
                }
            }
        });
    });


    $(document).on('click', '.activateBtn', function (event) {
        var activate_url = $(this).attr('href');
        var company_name = $(this).data('value');
        swal({
            title: "Are you sure?",
            text: "All users associated with '" + company_name + "' were barred from using this system. You can activate their profile's in Account Management. You will be able to undo this action by deactivating the account later under the enabled companies Tab",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Activate!",
            closeOnConfirm: true
        }, function () {
            $.ajax({
                type: 'GET',
                url: activate_url,
                success: function (data) {
                    $("#inactiveCompaniesTbl").empty();
                    create_table2();
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 4000
                    };
                    toastMessage = company_name + ' Account has been activated';
                    toastr.success(toastMessage);
                }
            });
        });
        event.preventDefault();
    });

    $(document).on('click', '.reset_user_password', function (event) {
        var company_id = $(this).data('value');
        $.ajax({
            type: 'GET',
            url: '/modules/company_management/ajax/reset_company_user_password.ajax.php?company_id=' + company_id,
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = 'EMAIL SENT! Email to reset password has been sent to the user.';
                toastr.success(toastMessage);
            }
        });
        event.preventDefault();
    });

//create new company button
    $('#create_container').click(function () {
        if ($("#create_container").hasClass('disabled')) {
            event.preventDefault();
        } else {
            document.location = 'index.php?m=container_management&a=edit_container&container_id=0';
        }

    });

    var offset = 180;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('#menuBar2').slideDown();
        } else {
            $('#menuBar2').slideUp();
        }
    });



});

function importContainers() {
    document.location = '/index.php?m=container_management&a=import_containers';
}

function create_table() {
    var date = moment().format('YYYY-MM-DD HH:mm:ss');
    var active = "Y";
    table = $('#activeContainerTbl').DataTable({
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                title: $('#company_name').val() + ' - Container Report ' + date,
                className: 'copyButton hidden',
                text: '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            {
                extend: 'excelHtml5',
                title: $('#company_name').val() + ' - Container Report ' + date,
                className: 'excelButton hidden',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            {
                extend: 'csvHtml5',
                title: $('#company_name').val() + ' - Container Report ' + date,
                className: 'csvButton hidden',
                text: '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            {
                extend: 'pdfHtml5',
                title: $('#company_name').val() + ' - Container Report ' + date,
                className: 'pdfButton hidden',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                }
            },
            {
                extend: 'print',
                title: $('#company_name').val() + ' - Container Report ' + date,
                className: 'printButton hidden',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                },
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/container_management/ajax/list_containers.ajax.php?active=" + active,
        "aoColumns": [
            {"bSortable": false, "className": 'details-control text-center'},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true}
        ],
        select: {
            // style:    'os',
            // selector: 'td:first-child'
        },
        order: [[1, 'asc']],
        stateSave: true,
        stateDuration: 0
    });

    if (clickBound1 === false) {
        clickBound1 = true;
        $('#activeContainerTbl tbody').on('click', 'tr', function () {

            $(this).toggleClass('selected');
            $(this).toggleClass('shown');

            //Append selected (or deselected) ID to global array
            var selectedID = $(this).find('td:nth-child(2)').text();
            var appendID = true;
            var newList = new Array();
            $.each(selectedTR, function (i, v) {
                if (selectedID == v) {
                    appendID = false;
                } else {
                    newList.push(v);
                }
            });
            if (appendID === true) {
                selectedTR.push($(this).find('td:nth-child(2)').text());
            } else {
                selectedTR = newList;
            }

            if (selectedTR.length > 0) {
                $('#stage_list').slideDown();
                $('#stage_list2').slideDown();
                $('#state_list').slideDown();
                $('#state_list2').slideDown();
                $('#clearButton1').slideDown();
                $('#clearButton2').slideDown();
            } else {
                $('#stage_list').slideUp();
                $('#stage_list2').slideUp();
                $('#state_list').slideUp();
                $('#state_list2').slideUp();
                $('#clearButton1').slideUp();
                $('#clearButton2').slideUp();
            }

        });
    }

    table.each(function () {
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });

}

function create_table2() {
    var active = "N";

    table2 = $('#inactiveContainerTbl').DataTable({
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/container_management/ajax/list_containers.ajax.php?active=" + active,
        "aoColumns": [
            {"bSortable": false, "className": 'details-control text-center'},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true}
        ],
        select: {
            // /style:    'os',
            // selector: 'td:first-child'
        },
        order: [[1, 'asc']],
        stateSave: true,
        stateDuration: 0
    });


    if (clickBound2 === false) {
        clickBound2 = true;
        $('#inactiveContainerTbl tbody').on('click', 'tr', function () {

            $(this).toggleClass('selected');
            $(this).toggleClass('shown');

            //Update selected ID's accordingly
            var selectedID = $(this).find('td:nth-child(2)').text();
            var appendID = true;
            var newList = new Array();
            $.each(selectedTR, function (i, v) {
                if (selectedID == v) {
                    appendID = false;
                } else {
                    newList.push(v);
                }
            });
            if (appendID === true) {
                selectedTR.push(selectedID);
            } else {
                selectedTR = newList;
            }

            if (selectedTR.length > 0) {
                $('#stage_list').slideDown();
                $('#stage_list2').slideDown();
                $('#state_list').slideDown();
                $('#state_list2').slideDown();
                $('#clearButton1').slideDown();
                $('#clearButton2').slideDown();
            } else {
                $('#stage_list').slideUp();
                $('#stage_list2').slideUp();
                $('#state_list').slideUp();
                $('#state_list2').slideUp();
                $('#clearButton1').slideUp();
                $('#clearButton2').slideUp();
            }

        });
    }



    table2.each(function () {
        var datatable2 = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable2.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable2.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable2.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });
}




function setDates(productID) {

    if (setCounter === false) {
        return;
    }

    if (!productID) productID = $('#product').val();
    var fromDate = $('#fillDate').datepicker('getDate');

    var dates = $.parseJSON($('#expiry_days').val());
    var today = moment(fromDate);

    if (dates[productID].best_before) {
        var bestBefore = today.add(dates[productID].best_before, 'days').format('YYYY-MM-DD');
    } else {
        var bestBefore = today.format('YYYY-MM-DD');
    }

    $('#sellBy').datepicker('update', bestBefore);

    today = moment(fromDate);
    var expiryDate = today.add(dates[productID].expiry, 'days').format('YYYY-MM-DD');
    $('#expireDate').datepicker('update', expiryDate);

}


function updateContainerProducts(container_id) {
    if (container_id == 0) {
        var container_list = '';
        $.each(selectedTR, function(i,v) {
            container_list += v + ',';
        });

        $.ajax({
            type: 'POST',
            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_list + "&new_stage=" + container_stage + "&action=select_product&new_product=" + $('#product').val() + '&best_before=' + $('#sellBy').val() + '&expire=' + $('#expireDate').val() + '&iteration=1&batch_no=' + $('#batchNo').val() + '&filldate=' + $('#fillDate').val(),
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                toastr.success(toastMessage);

                $('#productSelectModal').modal('hide');
                $('#sellBy').val('');
                $('#expireDate').val('');
                $('#product').val('0');

                document.location.reload();

                //create_table();
                //create_table2();
            }
        });

    } else {
        $.ajax({
            type: 'POST',
            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_id + "&new_stage=" + container_stage + "&action=select_product&new_product=" + $('#product').val() + '&best_before=' + $('#sellBy').val() + '&expire=' + $('#expireDate').val() + '&batch_no=' + $('#batchNo').val() + '&filldate=' + $('#fillDate').val(),
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                toastr.success(toastMessage);

                $('#productSelectModal').modal('hide');
                $('#sellBy').val('');
                $('#expireDate').val('');
                $('#product').val('0');

                create_table();
                create_table2();
            }
        });
    }




}


function updateContainerClients(container_id) {

    if (container_id == 0) {
        var container_list = '';
        $.each(selectedTR, function(i,v) {
            container_list += v + ',';
        });
        $.ajax({
            type: 'POST',
            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_list + "&new_stage=" + container_stage + "&action=select_customer&new_client=" + $('#client').val() + '&iteration=1&deliverydate=' + $('#deliveryDate').val(),
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                toastr.success(toastMessage);

                $('#clientSelectModal').modal('hide');
                $('#client').val('0');

                document.location.reload();
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_id + "&new_stage=" + container_stage + "&action=select_customer&new_client=" + $('#client').val() + '&iteration=1&deliverydate=' + $('#deliveryDate').val(),
            success: function (data) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    positionClass: 'toast-bottom-right',
                    timeOut: 6000
                };
                toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                toastr.success(toastMessage);

                $('#clientSelectModal').modal('hide');
                $('#client').val('0');

                create_table();
                create_table2();
            }
        });
    }
}


function clearSelectedID(tabID) {
    if (!$('#' + tabID).hasClass('active')) {
        selectedTR.length= 0;
        if (tabID == 'activeContainer') {
            for (i = 0; i < table.rows('.selected').data().length; i++) {
                selectedTR.push(table.rows('.selected').data()[i][1]);
            }
        } else {
            for (i = 0; i < table2.rows('.selected').data().length; i++) {
                selectedTR.push(table2.rows('.selected').data()[i][1]);
            }
        }
        if (selectedTR.length > 0) {
            $('#stage_list').slideDown();
            $('#stage_list2').slideDown();
            $('#state_list').slideDown();
            $('#state_list2').slideDown();
            $('#clearButton1').slideDown();
            $('#clearButton2').slideDown();
        } else {
            $('#stage_list').slideUp();
            $('#stage_list2').slideUp();
            $('#state_list').slideUp();
            $('#state_list2').slideUp();
            $('#clearButton1').slideUp();
            $('#clearButton2').slideUp();
        }
    }
}

$('.new_state').on('click', function () {

    var container_state = $(this).data('value');

    $.each(selectedTR, function(i,v) {
        $.ajax({
            type: 'POST',
            url: "/modules/container_management/ajax/save_multiple_container_state.ajax.php?container_id=" + v + "&new_state=" + container_state + '&iteration=' + i,
            success: function (data) {

            }
        });
    });

    $("#activeContainerTbl").empty();

    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        positionClass: 'toast-bottom-right',
        timeOut: 6000
    };
    toastMessage = 'Container ' + $('#state').val() + ' Updated';
    toastr.success(toastMessage);

    create_table();
    create_table2();
});

$('.new_stage').click(function () {
    container_stage = $(this).data('value');
    $.ajax({
        type: 'POST',
        url: '/modules/container_management/ajax/get_next_action.ajax.php?stage_id=' + container_stage,
        success: function (data) {

            // clear the client and product
            if (data == 5) {
                var container_list = '';
                $.each(selectedTR, function(i,v) {
                    container_list += v + ',';
                });

                $.ajax({
                    type: 'POST',
                    url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_list + "&new_stage=" + container_stage + "&action=clear_product_client",
                    success: function (data) {

                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            positionClass: 'toast-bottom-right',
                            timeOut: 6000
                        };
                        toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                        toastr.success(toastMessage)

                        document.location.reload();
                    }
                });
            }
            //clear the client
            else if (data == 4) {
                var container_list = '';
                $.each(selectedTR, function(i,v) {
                    container_list += v + ',';
                });
                $.ajax({
                    type: 'POST',
                    url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_list + "&new_stage=" + container_stage + "&action=clear_client",
                    success: function (data) {

                        toastr.options = {
                            closeButton: true,
                            progressBar: true,
                            showMethod: 'slideDown',
                            positionClass: 'toast-bottom-right',
                            timeOut: 6000
                        };
                        toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                        toastr.success(toastMessage)

                        document.location.reload();
                    }
                });
            }
            //clear the product
            else if (data == 3) {
                var container_list = '';
                $.each(selectedTR, function(i,v) {
                    container_list += v + ',';
                });
                    $.ajax({
                        type: 'POST',
                        url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_list + "&new_stage=" + container_stage + "&action=clear_product",
                        success: function (data) {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                positionClass: 'toast-bottom-right',
                                timeOut: 6000
                            };
                            toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                            toastr.success(toastMessage)

                            document.location.reload();
                        }
                    });


            }
            //Request user to select the client
            else if (data == 2) {
                $('#clientSelectModal').modal();
            }
            //Request user to select the product
            else if (data == 1) {
                $('#productSelectModal').modal();
            } else {
                swal({
                    title: 'Container Stage Change',
                    text: "The selected containers will updated",
                    showCancelButton: true
                }).then(function (result) {
                    var container_list = '';
                    $.each(selectedTR, function(i,v) {
                        container_list += v + ',';
                    });
                        $.ajax({
                            type: 'POST',
                            url: "/modules/container_management/ajax/save_multiple_container_stage.ajax.php?container_id=" + container_list + "&new_stage=" + container_stage,
                            success: function (data) {
                                toastr.options = {
                                    closeButton: true,
                                    progressBar: true,
                                    showMethod: 'slideDown',
                                    positionClass: 'toast-bottom-right',
                                    timeOut: 6000
                                };
                                toastMessage = $('#container').val() + ' ' + $('#stage').val() + 's Updated';
                                toastr.success(toastMessage);

                                document.location.reload();
                            }
                        });
                })
            }
        }
    });
});


function resetSelection() {

    $.each($('#activeContainerTbl tbody tr'), function (i,v) {
        $(this).removeClass('selected');
        $(this).removeClass('shown');
    });

    $.each($('#inactiveContainerTbl tbody tr'), function (i,v) {
        $(this).removeClass('selected');
        $(this).removeClass('shown');
    });

    selectedTR.length = 0;
    $('#stage_list').slideUp();
    $('#stage_list2').slideUp();
    $('#state_list').slideUp();
    $('#state_list2').slideUp();
    $('#clearButton1').slideUp();
    $('#clearButton2').slideUp();


}