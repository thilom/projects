

var CSVTotalRows;
var CSVFile;
var importCount = 0;
var importSuccess = 0;
var importDuplicate = 0;
var importBad = 0;
var importUnsubscribed = 0;
var resetTable = false;
var previousImportTotal = 0;
var previousImportCurrent = 0;

$(document).ready(function () {

});


function uploadFile2() {
    $('#importCsvForm').ajaxSubmit({
        url: '/modules/container_management/ajax/import_containers_csv.ajax.php',
        dataType: 'json',
        beforeSubmit: function() {
            $('#uploadCSVBar').slideDown();
            $('#importCsvForm').slideUp();
            $('#CSVResult').slideUp();
        },
        success: function(data) {
            CSVTotalRows = data.rows;
            CSVFile = data.filename;

            if (data.error === true) {
                $('#CSVResult').removeClass('text-success');
                $('#CSVResult').addClass('text-danger');
            } else {
                $('#CSVResult').removeClass('text-danger');
                $('#CSVResult').addClass('text-success');
                $('#CSVContinueButton').fadeIn();
            }

            $('#CSVResult').text(data.message);
            $('#CSVResult').slideDown();
        },
        uploadProgress: function(event, position, total, percentComplete) {
            $('#CSVProgressBar .progress-bar').width(percentComplete+'%');
            $('#CSVSize').html(parseInt(total/(1024*1024)));
            $('#CSVProgress').text(percentComplete);

        }
    });
}


function continueImport() {

    $('#CSVTotalCount').text(CSVTotalRows);

    $('#uploadCSVBar').slideUp();
    $('#uploadCSVInProgress').slideDown();

    importSubscribers();
}

function resetCSVImport() {
    //$('#uploadCSVInProgress').slideUp();
    //$('#uploadCSVBar').slideUp();
    //$('#importCsvForm').slideDown();
    //$('#successRate').text('0');
    //$('#unsubscribeRate').text('0');
    //$('#duplicateRate').text('0');
    //$('#badRate').text('0');
    //
    //$('#CSVImportProgress').text('0');
    //$('#uploadCSVInProgress .progress-bar').width('0%');
    //
    //importSuccess = 0;
    //importDuplicate = 0;
    //importBad = 0;
    //importUnsubscribed = 0;
    document.location = '/index.php?m=container_management&a=list_containers';
}

function importSubscribers() {

    $.ajax({
        url: '/modules/container_management/ajax/import_containers_write.ajax.php?file=' + CSVFile,
        complete: function(data) {

            var data = $.parseJSON(data.responseText);

            importSuccess += parseInt(data.importCount);
            importDuplicate += parseInt(data.duplicateCount);
            importBad += parseInt(data.badCount);
            importCount = importSuccess + importDuplicate + importBad;

            $('#successRate').text(importSuccess);
            $('#unsubscribeRate').text(importUnsubscribed);
            $('#duplicateRate').text(importDuplicate);
            $('#badRate').text(importBad);

            var percentage = parseInt(importCount/CSVTotalRows * 100);
            $('#CSVImportProgress').text(percentage);
            $('#uploadCSVInProgress .progress-bar').width(percentage+'%');

            if (data.complete === false) {
                importSubscribers();
            } else {

                var percentage = 100;
                $('#CSVImportProgress').text(percentage);
                $('#uploadCSVInProgress .progress-bar').width(percentage+'%');

                $('#CSVImportResult').html('Import Complete <button class="btn btn-success pull-right" onclick="resetCSVImport()">Continue</button>');
                $('#CSVImportResult').removeClass('text-danger');
                $('#CSVImportResult').addClass('text-success');
                resetTable = true;

                $('#successFile').html("<a target='_blank' href='"+ data.successFile +"' ><span class='glyphicon glyphicon-save' aria-hidden='true'></span> Download</a>");
                $('#unsubscribeFile').html("<a target='_blank' href='"+ data.unsubscribeFile +"' ><span class='glyphicon glyphicon-save' aria-hidden='true'></span>Download</a>");
                $('#badFile').html("<a target='_blank' href='"+ data.badFile +"' ><span class='glyphicon glyphicon-save' aria-hidden='true'></span>Download</a>");
                $('#duplicateFile').html("<a target='_blank' href='"+ data.duplicateFile +"' ><span class='glyphicon glyphicon-save' aria-hidden='true'></span>Download</a>");

                $('#successFile').slideDown();
                $('#unsubscribeFile').slideDown();
                $('#badFile').slideDown();
                $('#duplicateFile').slideDown();
                $('#downloadMessage').slideDown();

            }

        }
    });
}
