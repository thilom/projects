$(document).ready(function() {

    //show client dropdown if it is not empty
    if ($('#current_client').val() != ''){
        $('#clients_div').slideDown();
    }

    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        dateFormat: 'yy-mm-dd'
    });
    $('#brew_date').datepicker({
        dateFormat: 'yy-mm-dd'
    });


    $( "#save" ).click(function() {
        if ($('#container_id').val() == 0) {
            var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('serial_number', true);
            bootstrapValidator.enableFieldValidators('bar_code', true);
        }
        else{
            var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('serial_number', false);
            bootstrapValidator.enableFieldValidators('bar_code', false);
        }
    });



    $( "#current_stage" ).change(function() {


        $.ajax({
            type: 'POST',
            url: '/modules/container_management/ajax/get_next_action.ajax.php?stage_id='+$('#current_stage').val(),
            success: function (data) {

                // clear the client and product
                if (data == 5) {
                        //creating of the contaner
                        $('#contents').prop('selectedIndex',0);
                        $('#current_client').prop('selectedIndex',0);
                        $('#clients_div').slideUp();
                        //remove validation on the container client and product
                        var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                         bootstrapValidator.enableFieldValidators('contents', false);
                        bootstrapValidator.enableFieldValidators('current_client', false);
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#contents').slideUp();
                }
                //clear the client
                else if (data == 4) {
                    $('#clients_div').slideUp();
                        $('#current_client').prop('selectedIndex', 0);
                        //remove validation on the container client
                        var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                        bootstrapValidator.enableFieldValidators('current_client', false);
                        //revalidate the product
                        bootstrapValidator.enableFieldValidators('contents', true);
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#contents').slideUp();
                }
                //clear the product
                else if (data == 3) {
                    $('#clients_div').slideUp();
                        $('#contents').prop('selectedIndex',0);
                        //remove validation on the container product
                         var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                         bootstrapValidator.enableFieldValidators('contents', false);
                        //revalidate the client
                     bootstrapValidator.enableFieldValidators('current_client', true);
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#contents').slideUp();
                }
                //Request user to select the client
                else if (data == 2) {
                    $('#clients_div').slideDown();
                    open($('#current_client'));
                    //revalidate the client
                    var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                    bootstrapValidator.enableFieldValidators('current_client', true);
                        $('#contents_label').slideDown();
                     $('#data_1').slideDown();
                        $('#contents').slideDown();
                }
                //Request user to select the product
                else if (data == 1) {
                    open($('#contents'));
                    //revalidate the product
                    var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                    bootstrapValidator.enableFieldValidators('contents', true);
                    $('#contents_label').slideDown();
                    $('#data_1').slideDown();
                    $('#contents').slideDown();
                }
                //do nothing
                else {
                    $('#contents').slideUp();
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#clients_div').slideUp()
                }
            }
        });





    });

    //Form validations
    $.fn.bootstrapValidator.validators.duplicateSerialNumber = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/modules/container_management/ajax/check_duplicate_container.ajax.php?serial_number=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });
            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };

    //Form validations
    $.fn.bootstrapValidator.validators.duplicateBarCode = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/modules/container_management/ajax/check_duplicate_bar_code.ajax.php?bar_code=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });
            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };


        $('#editContainerForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            serial_number: {
                validators: {
                    notEmpty: {message: "Please Provide  Serial Number."},
                    duplicateSerialNumber: {message:"Serial Number Possible Duplicate."}
                }
            },
            type: {
                validators: {
                    notEmpty: {message: "Please Provide  the Type."}
                }
            },
            contents: {
                validators: {
                    notEmpty: {message: "Please Provide  Whats is Inside."}
                }
            },
            current_stage: {
                validators: {
                    notEmpty: {message: "Please Provide  the Current Stage."}
                }
            },
            current_state: {
                validators: {
                    notEmpty: {message: "Please Provide  the Current State."}
                }
            },
            capacity: {
                validators: {
                    notEmpty: {message: "Please Provide  the Capacity."}
                }
            },
            capacity_type: {
                validators: {
                    notEmpty: {message: "Please Provide  the Capacity Type."}
                }
            },
            current_client:{
                validators: {
                    notEmpty: {message: "Please Provide  the Client where it was Delivered."}
                }
            },
            brew_date:{
                validators: {
                    notEmpty: {message: "Please Provide  the Brew/batch date."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/container_management/ajax/save_container.ajax.php',
            beforeSubmit: function () {
                var l = $( '#save' ).ladda();
                // Start loading
                l.ladda( 'start' );
            },
            success: function (data) {
                var l = $( '#save' ).ladda();
                // Start loading
                l.ladda( 'stop' );
                if (data == 'update') {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = $('#container_singular').val()+' Successfully Updated.';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#container_singular').val()+" Successfully Updated",
                            text:  $('#container_singular').val()+" Successfully Updated",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To "+$('#container_singular').val()+" List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                var l = $( '#save' ).ladda();
                                // Start loading
                                l.ladda( 'stop' );

                            } else {
                                document.location = '/index.php?m=container_management&a=list_containers';
                            }
                        })
                }
                else{
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'New '+$('#container_singular').val()+' Added!';
                    toastr.success(toastMessage);

                    swal({
                            title:  'New '+$('#container_singular').val()+' Successfully Added!',
                            text:  'New '+ $('#container_singular').val()+" Successfully Added",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Create Another "+$('#container_singular').val(),
                            cancelButtonText: 'Back to '+$('#container_singular').val()+' List!',
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $("#editContainerForm")[0].reset();
                                var l = $( '#save' ).ladda();
                                // Start loading
                                l.ladda( 'stop' );

                            } else {
                                document.location = 'index.php?m=container_management&a=list_containers';
                            }
                        })
                }
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        var l = $( '#cancel' ).ladda();
        // Start loading
        l.ladda( 'start' );

        document.location = 'index.php?m=container_management&a=list_containers';
    });

    $('#show_more_notes').click(function () {
        $('#ContainerNotes a[href="#ContainerNotesTab"]').trigger('click');
    });
});

function open(elem) {
    if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
    } else if (element.fireEvent) {
        elem[0].fireEvent("onmousedown");
    }
}