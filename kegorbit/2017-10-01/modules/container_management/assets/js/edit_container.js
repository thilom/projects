
var advancedEditing = false;


$(document).ready(function() {

    var dFormat = $('#dateFormat').val();

    $('#sell_by_date').datepicker({todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });


    $('#expiry_date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });

    $('#fillDate').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });

    $('#deliveryDate').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: dFormat
    });


    $( "#save" ).click(function() {
        if ($('#container_id').val() == 0) {
            var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('serial_number', true);
            bootstrapValidator.enableFieldValidators('bar_code', true);
        }
        else{
            var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
            bootstrapValidator.enableFieldValidators('serial_number', false);
            bootstrapValidator.enableFieldValidators('bar_code', false);
        }
    });



    $("#current_stage").change(function() {
        checkStage();
    });


    //Form validations
    $.fn.bootstrapValidator.validators.duplicateSerialNumber = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/modules/container_management/ajax/check_duplicate_container.ajax.php?serial_number=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });
            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };

    //Form validations
    $.fn.bootstrapValidator.validators.duplicateBarCode = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function (validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            var duplicate = 1;
            // Perform validating
            $.ajax({
                url: '/modules/container_management/ajax/check_duplicate_bar_code.ajax.php?bar_code=' + $field.val(),
                type: 'GET',
                async: false,
                success: function (data) {
                    duplicate = data;
                }
            });
            // return true if the field value is valid
            // otherwise return false
            return duplicate != 1;
        }
    };


        $('#editContainerForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {},
        fields: {
            serial_number: {
                validators: {
                    notEmpty: {message: "Please Provide  Serial Number."},
                    duplicateSerialNumber: {message:"Serial Number Possible Duplicate."}
                }
            },
            type: {
                validators: {
                    notEmpty: {message: "Please Provide  the Type."}
                }
            },
            contents: {
                validators: {
                    notEmpty: {message: "Please Provide  Whats is Inside."}
                }
            },
            current_stage: {
                validators: {
                    notEmpty: {message: "Please Provide  the Current Stage."}
                }
            },
            current_state: {
                validators: {
                    notEmpty: {message: "Please Provide  the Current State."}
                }
            },
            capacity: {
                validators: {
                    notEmpty: {message: "Please Provide  the Capacity."}
                }
            },
            capacity_type: {
                validators: {
                    notEmpty: {message: "Please Provide  the Capacity Type."}
                }
            },
            current_client:{
                validators: {
                    notEmpty: {message: "Please Provide  the Client where it was Delivered."}
                }
            },
            brew_date:{
                validators: {
                    notEmpty: {message: "Please Provide  the Brew/batch date."}
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data;
        $form.ajaxSubmit({
            type: 'POST',
            url: '/modules/container_management/ajax/save_container.ajax.php',
            beforeSubmit: function () {
                var l = $( '#save' ).ladda();
                // Start loading
                l.ladda( 'start' );
            },
            success: function (data) {
                var l = $( '#save' ).ladda();
                // Start loading
                l.ladda( 'stop' );
                if (data == 'update') {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = $('#container_singular').val()+' Successfully Updated.';
                    toastr.success(toastMessage);

                    swal({
                            title: $('#container_singular').val()+" Successfully Updated",
                            text:  $('#container_singular').val()+" Successfully Updated",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Continue Editing",
                            cancelButtonText: "Back To "+$('#container_singular').val()+" List",
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                var l = $( '#save' ).ladda();
                                // Start loading
                                l.ladda( 'stop' );
                                document.location = document.location;

                            } else {
                                document.location = '/index.php?m=container_management&a=list_containers';
                            }
                        })
                }
                else{
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        positionClass: 'toast-bottom-right',
                        timeOut: 6000
                    };
                    toastMessage = 'New '+$('#container_singular').val()+' Added!';
                    toastr.success(toastMessage);

                    swal({
                            title:  'New '+$('#container_singular').val()+' Successfully Added!',
                            text:  'New '+ $('#container_singular').val()+" Successfully Added",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#4caf50",
                            confirmButtonText: "Create Another "+$('#container_singular').val(),
                            cancelButtonText: 'Back to '+$('#container_singular').val()+' List!',
                            closeOnConfirm: true,
                            closeOnCancel: false
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                $("#editContainerForm")[0].reset();
                                var l = $( '#save' ).ladda();
                                // Start loading
                                l.ladda( 'stop' );

                            } else {
                                document.location = 'index.php?m=container_management&a=list_containers';
                            }
                        })
                }
            }
        });
    });

    //cancel creating/editing a company
    $('#cancel').click(function () {
        var l = $( '#cancel' ).ladda();
        // Start loading
        l.ladda( 'start' );

        document.location = 'index.php?m=container_management&a=list_containers';
    });

    $('#show_more_notes').click(function () {
        $('#ContainerNotes a[href="#ContainerNotesTab"]').trigger('click');
    });

    checkStage();
});

function open(elem) {
    if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
    } else if (element.fireEvent) {
        elem[0].fireEvent("onmousedown");
    }
}

function checkStage() {
    $.ajax({
        type: 'POST',
        url: '/modules/container_management/ajax/get_next_action.ajax.php?stage_id=' + $('#current_stage').val(),
        success: function (data) {

            if (advancedEditing === false) {
                $('.xtraInfo').slideUp();
            }

            // clear the client and product
            if (data == 5) {
                //creating of the contaner
                $('#contents').prop('selectedIndex', 0);
                $('#current_client').prop('selectedIndex', 0);
                //remove validation on the container client and product
                var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                bootstrapValidator.enableFieldValidators('contents', false);
                bootstrapValidator.enableFieldValidators('current_client', false);

                if (advancedEditing === false) {
                    $('#clients_div').slideUp();
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#data_fill_date').slideUp();
                    $('#data_delivery_date').slideUp();
                    $('#contents').slideUp();
                }

                $('#batch_no').val('');
                $('#contents').val('');
                $('#current_client').val('');
                $('#sell_by_date').val('');
                $('#expiry_date').val('');
                $('#deliveryDate').val('');
            }
            //clear the client
            else if (data == 4) {
                $('#clients_div').slideUp();
                $('#current_client').prop('selectedIndex', 0);
                //remove validation on the container client
                var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                bootstrapValidator.enableFieldValidators('current_client', false);
                //revalidate the product
                bootstrapValidator.enableFieldValidators('contents', true);

                if (advancedEditing === false) {
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#data_fill_date').slideUp();
                    $('#data_delivery_date').slideUp();
                    $('#contents').slideUp();
                    $('#current_client').val('');
                    $('#deliveryDate').val('');
                }
            }
            //clear the product
            else if (data == 3) {
                $('#clients_div').slideUp();
                $('#contents').prop('selectedIndex', 0);
                //remove validation on the container product
                var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                bootstrapValidator.enableFieldValidators('contents', false);
                //revalidate the client
                bootstrapValidator.enableFieldValidators('current_client', true);

                if (advancedEditing === false) {
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#data_fill_date').slideUp();
                    $('#data_delivery_date').slideUp();
                    $('#contents').slideUp();
                    $('#contents').val('');
                    $('#batch_no').val('');
                    $('#sell_by_date').val('');
                    $('#expiry_date').val('');
                }
            }
            //Request user to select the client
            else if (data == 2) {
                $('#clients_div').slideDown();
                //open($('#current_client'));
                //revalidate the client
                var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                bootstrapValidator.enableFieldValidators('current_client', true);

                if (advancedEditing === false) {
                    $('#contents_label').slideDown();
                    $('#contents').slideDown();
                    $('#data_delivery_date').slideDown();
                }
            }
            //Request user to select the product
            else if (data == 1) {
                //revalidate the product
                var bootstrapValidator = $('#editContainerForm').data('bootstrapValidator');
                bootstrapValidator.enableFieldValidators('contents', true);

                if (advancedEditing === false) {
                    $('#contents_label').slideDown();
                    $('#data_0').slideDown();
                    $('#data_1').slideDown();
                    $('#data_2').slideDown();
                    $('#data_3').slideDown();
                    $('#data_fill_date').slideDown();
                    $('#contents').slideDown();
                }
            }
            //do nothing
            else {
                if (advancedEditing === false) {
                    $('#contents').slideUp();
                    $('#contents_label').slideUp();
                    $('#data_1').slideUp();
                    $('#data_fill_date').slideUp();
                    $('#data_delivery_date').slideUp();
                    $('#clients_div').slideUp()
                }
            }
        }
    });
}



function setDates(productID) {

    if (!productID) productID = $('#container_id').val();

    var dates = $.parseJSON($('#expiry_days').val());
    var fromDate = $('#fillDate').datepicker('getDate');
    var today = moment(fromDate);

    var bestBefore = today.add(dates[productID].best_before, 'days').format('YYYY-MM-DD');
    $('#sell_by_date').datepicker('update', bestBefore);

    var expiryDate = today.add(dates[productID].expiry, 'days').format('YYYY-MM-DD');
    $('#expiry_date').datepicker('update', expiryDate);

}

function toggleAdvanced() {

    if ($('#enableAdvancedButton').text() == 'Disable Advanced Editing') {
        $('#enableAdvancedButton').text('Enable Advanced Editing');
        advancedEditing = false;
        checkStage();
    } else {
        $('#contents').slideDown();
        $('#contents_label').slideDown();
        $('#data_1').slideDown();
        $('#clients_div').slideDown();
        $('#data_0').slideDown();
        $('#data_2').slideDown();
        $('#data_3').slideDown();
        $('#data_fill_date').slideDown();
        $('#data_delivery_date').slideDown();
        $('#enableAdvancedButton').text('Disable Advanced Editing');
        advancedEditing = true;
    }


}