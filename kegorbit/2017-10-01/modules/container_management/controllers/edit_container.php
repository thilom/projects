<?php

/**
 * Call the edit contaner page
 * 2016-07-22- Musa Khulu -
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/customer_management/classes/customer.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/products_management/classes/products.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();
$settingsClass = new Settings();
$customerClass = new Customer();
$companyClass = new Company();
$productClass = new Products();

$output = array();
$output['aaData'] = '';
$container_id = isset($_GET['container_id'])? $_GET['container_id'] :0;

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//Get user access
if (isset($_SESSION['user']['run_as'])) {
    $access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'container_management', 'enable_advanced');
} else {
    $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'container_management', 'enable_advanced');
}
if (!$access) {
    $enable_advanced = '';
} else {
    $enable_advanced = '<button class="pull-right btn btn-warning" onclick="toggleAdvanced()" id="enableAdvancedButton">Enable Advanced Editing</button>';
}

//get container terminology
$container_name = $settingsClass->get_container_terminology();
$stage_singular = $settingsClass->get_stage_terminology();
$state_singular = $settingsClass->get_state_terminology();
$customer_singular = $settingsClass->get_customer_terminology();

//get the company date format
if(isset($_SESSION['user']['run_as'])) {
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['run_as']);
} else{
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
}
$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];

//JS Date format for date picker
$js_date_format = str_replace('Y', 'yyyy', $date_format['application_date']);
$js_date_format = str_replace('d', 'dd', $js_date_format);
$js_date_format = str_replace('m', 'mm', $js_date_format);


if($container_id != 0) {

    $template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/html/edit_container.html");
    //get container
    $container_data = $containerClass->get_selected_containers($container_id);
    //get number of notes
    $count = $containerClass->get_container_notes_count($container_id);
    if($count<1){
        $show_more = '';
    }
    else{
        $show_more = ' <button  type="button" class="btn btn-primary dim btn-block m-t" id="show_more_notes"><i class="fa fa-arrow-up"></i> Show More</button>';
    }

    //get 3 container_notes for the preview section
    $container_notes = $containerClass->get_container_notes($container_id, 3);
    $notes_section = '';
    foreach ($container_notes as $container_note){
        $date = date($format. ' H:i:s',strtotime($container_note['container_note_date']));
        $notes_section .= '<div class="feed-element">
                      <div class="media-body "><small class="pull-right"> '.$container_note['hours_elapsed'].' h ago</small>
                      <strong> '.$container_note['user_full_name'].'</strong> '.$container_note['container_note'].'. <br>
                      <small class="text-muted">'.date('M j Y g:i A', strtotime($container_note['container_note_date'])).'</small></div></div>';
    }
    //get all container_notes for the preview section
    $all_notes = $containerClass->get_container_notes($container_id, 100);
    $all_notes_section = '';
    foreach ($all_notes as $all_note){
        $all_notes_section .= '<div class="feed-element">
                      <div class="media-body "><small class="pull-right"> '.$all_note['hours_elapsed'].' h ago</small>
                      <strong> '.$all_note['user_full_name'].'</strong> '.$all_note['container_note'].'. <br>
                      <small class="text-muted">'.date('M j Y g:i A', strtotime($all_note['container_note_date'])).'</small></div></div>';
    }

    //get all  types
    $types = $containerClass->get_types();
    $contents = '<select name="contents" class="form-control" id="contents" tabindex="6" onclick="setDates(this.value)"> <option value="">What is inside?</option>';
    foreach($types as $type) {
        if ($type['type_id'] == $container_data[0]['container_contents']) {
            $contents .= '<option value="' . $type['type_id'] . '" selected>' . $type['type_name'] . '</option>';
            $current_content = $type['type_name'] ;
        } else {
            $contents .= '<option value="' . $type['type_id'] . '" >' . $type['type_name']  . '</option>';
        }
    }
    $contents .= '</select>';

    //get all container stages
    $stages = $containerClass->get_stages();
    $current_stage = '<select name="current_stage" class="form-control" id="current_stage" tabindex="7"><option value="" >Choose</option>';
    foreach($stages as $stage) {
        if ($stage['stage_name'] == $container_data[0]['stage_name']) {
            $current_stage .= '<option value="' . $stage['stage_id'] . '" selected>' . $stage['stage_name'] . '</option>';
        } else {
            $current_stage .= '<option value="' . $stage['stage_id'] . '" >' . $stage['stage_name']  . '</option>';
        }
    }
    $current_stage .= '</select>';

    //get all container states
    $states = $containerClass->list_states();
    $current_state = '<select name="current_state" class="form-control" id="current_state" tabindex="8">';
    foreach($states as $state) {
        if ($state['state_name'] == $container_data[0]['state_name']) {
            $current_state .= '<option value="' . $state['state_id'] . '" selected>' . $state['state_name'] . '</option>';
            $state_display =  $state['state_name'];
        } else {
            $current_state .= '<option value="' . $state['state_id'] . '" >' . $state['state_name']  . '</option>';
        }
    }
    $current_state .= '</select>';

    //get all container capacity
    $capacity_types =  array(array('option' => 'L', 'selected' => 'Litre'), array('option' => 'KG', 'selected' => 'KG'));

    $current_capacity_type = '<select name="capacity_type" class="form-control" id="capacity_type" style="width: 120px" tabindex="5">';
    foreach($capacity_types as $capacity_type) {

        if ($capacity_type['option'] == $container_data[0]['container_capacity_type']) {
            $current_capacity_type .= '<option value="' . $capacity_type['option'] . '" selected>' . $capacity_type['selected']  . '</option>';
        } else {
            $current_capacity_type .= '<option value="' . $capacity_type['option']  . '" >' . $capacity_type['selected']   . '</option>';
        }
    }
    $current_capacity_type .= '</select>';

    //get all container history
    $historys= $containerClass->get_history($container_id);
    $history_list = '';
    foreach($historys as $history) {

        $history_list .= '<div class="timeline-item">
                                                <div class="row">
                                                    <div class="col-xs-3 date">
                                                        <i class="fa fa-history"></i>
                                                        '.$history['TIMEONLY'].'
                                                        <br/>
                                                        <small class="text-navy"> '.$history['hours_elapsed'].' hour ago</small>
                                                    </div>
                                                    <div class="col-xs-7 content no-top-border">
                                                        <p class="m-b-xs"><strong>'.$history['user_full_name'].'</strong></p>

                                                        <p>'.$history['history_action'].'.</p>
                                                    </div>
                                                </div>
                                            </div>';
        }

    //get all clients
    $customers = $containerClass->get_customer_container_location_report();

    $clients_lists = '<select name="current_client" class="form-control" id="current_client"><option value="" >Where Was it Delivered?</option>';
    foreach($customers as $customer) {
        if ($customer['customer_id'] == $container_data[0]['container_client']) {
            $clients_lists .= '<option value="' . $customer['customer_id'] . '" selected>' . $customer['customer_name']  . '</option>';
            $current_customer = $customer['customer_name'];
        } else {
            $clients_lists .= '<option value="' . $customer['customer_id'] . '" >' . $customer['customer_name']  . '</option>';
        }
    }
    $clients_lists .= '</select>';

    //get container types
    $container_types = $containerClass->get_container_types();
    $types_lists = '<select name="type" class="form-control" id="type" tabindex="3"><option value="">Choose Type</option>';
    foreach($container_types as $container_type) {
        if ($container_type['type_id'] == $container_data[0]['container_type']) {
            $types_lists .= '<option value="' . $container_type['type_id'] . '" selected>' . $container_type['container_type'].' '.$container_name  . '</option>';
        } else {
            $types_lists .= '<option value="' . $container_type['type_id'] . '" >' . $container_type['container_type'].' '.$container_name  . '</option>';
        }
    }
    $types_lists .= '</select>';


    $date = date($format.' H:i:s',strtotime($container_data[0]['container_date']));
    $template_data = array(
        'new_container' => 'Edit '.$container_name,
        'enable_advanced' => $enable_advanced,
        'container_name' => $container_id,
        'container_singular' => $container_name,
        'stage_singular' => $stage_singular,
        'state_singular' => $state_singular,
        'customer_singular' => $customer_singular,
        'container_id'       =>  $container_id,
        'serial_number'       =>  $container_data[0]['container_serial_number'],
        'container_types'       =>  $container_data[0]['container_capacity'],
        'bar_code'       =>  $container_data[0]['container_bar_code'],
        'capacity'       =>  $container_data[0]['container_capacity'],
        'sell_by_date'       =>  date($format, strtotime($container_data[0]['sell_by_date'])),
        'expiry_date'       =>  date($format, strtotime($container_data[0]['expiry_date'])),
        'date'       => date($format. ' H:i:s'),
        'creation_date' => '<label> Last Updated: </label> '. $date,
        'contents'     => $contents ,
        'stages'     => $current_stage ,
        'states'     => $current_state ,
        'container_notes' => '',
        'count' => $count,
        'notes_messages' => $notes_section,
        'full_notes' => $all_notes_section,
        'container_type' => $types_lists,
        'capacity_type' => $current_capacity_type,
        'history_list' => $history_list,
        'client_list' => $clients_lists,
        'show_more' =>$show_more,
        'batch_number'=> $container_data[0]['batch_number'],
        'date_format'=> $js_date_format,
        'current_stage'=> empty($container_data[0]['stage_name'])?"<span style='color: red'>Not Set</span>":$container_data[0]['stage_name'],
        'current_content'=> isset($current_content)?$current_content:"-",
        'current_batch'=> empty($container_data[0]['batch_number'])?"":"Batch:  {$container_data[0]['batch_number']}",
        'current_state'=> isset($state_display)?$state_display:"<span style='color: red'>Not Set</span>",
        'current_customer'=> isset($current_customer)?$current_customer:'-',
        'current_sell_by'=> empty($container_data[0]['sell_by_date'])||$container_data[0]['sell_by_date']=='1970-01-01'?"-":date($format, strtotime($container_data[0]['sell_by_date'])),
        'current_expiry'=> empty($container_data[0]['expiry_date'])||$container_data[0]['expiry_date']=='1970-01-01'?'-':date($format, strtotime($container_data[0]['expiry_date'])),
        'fill_date'=> empty($container_data[0]['filled_date'])||$container_data[0]['filled_date']=='1970-01-01'?'-':date($format, strtotime($container_data[0]['filled_date'])),
        'delivery_date'=> empty($container_data[0]['delivery_date'])||$container_data[0]['delivery_date']=='1970-01-01'?date($format):date($format, strtotime($container_data[0]['delivery_date'])),
    );


} else{

    $template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/html/create_container.html");
    //get all container types
    $types = $containerClass->get_types();
    $contents = '<select name="contents" class="form-control" id="contents" tabindex="6" style="display: none"><option value="0">What is inside?</option>';
    foreach($types as $type) {
        if ($type['type_name'] == 'Empty') {
            $contents .= '<option value="' . $type['type_id'] . '" selected>' . $type['type_name'] . '</option>';
        } else {
            $contents .= '<option value="' . $type['type_id'] . '" >' . $type['type_name']  . '</option>';
        }
    }
    $contents .= '</select>';

    //get all container stages
    $stages = $containerClass->get_stages();
    $current_stage = '<select name="current_stage" class="form-control" id="current_stage" tabindex="7" > <option value="" >Choose</option>';
    foreach($stages as $stage) {
        if ($stage['is_default'] == 'Y') {
            $current_stage .= '<option value="' . $stage['stage_id'] . '" selected >' . $stage['stage_name'] . '</option>';
        } else {
            $current_stage .= '<option value="' . $stage['stage_id'] . '" >' . $stage['stage_name'] . '</option>';
        }
    }
    $current_stage .= '</select>';



    //get all container states
    $states = $containerClass->list_states();
    $current_state = '<select name="current_state" class="form-control" id="current_state" tabindex="8">';
    foreach($states as $state) {
        if ($state['is_default'] == 'Y') {
            $current_state .= '<option value="' . $state['state_id'] . '" selected>' . $state['state_name'] . '</option>';
        } else {
            $current_state .= '<option value="' . $state['state_id'] . '" >' . $state['state_name']  . '</option>';
        }
    }
    $current_state .= '</select>';

    //get all clients
    $customers = $containerClass->get_customer_container_location_report();

    $clients_lists = '<select name="current_client" class="form-control" id="current_client"><option value="" >Where Was it Delivered?</option>';
    foreach($customers as $customer) {
            $clients_lists .= '<option value="' . $customer['customer_id'] . '" >' . $customer['customer_name']  . '</option>';
        }
    $clients_lists .= '</select>';


    $date = date($format.' H:i:s');


    //get container types
    $container_types = $containerClass->get_container_types();
    $types_lists = '<select name="type" class="form-control" id="type" tabindex="3"><option value="">Choose Type</option>';
    foreach($container_types as $container_type) {
            $types_lists .= '<option value="' . $container_type['type_id'] . '" >' . $container_type['container_type'].' '.$container_name. '</option>';
    }
    $types_lists .= '</select>';



    $template_data = array(
        'new_container' => 'Create '.$container_name,
        'container_name' => 'New '.$container_name,
        'container_singular' => $container_name,
        'container_id'       =>  $container_id,
        'serial_number'       =>  '',
        'container_types'       =>  '',
        'bar_code'       =>  '',
        'capacity'       =>  '',
        'date'       =>  $date,
        'creation_date' => '<label> Creation Date: </label> '. $date,
        'contents'     => $contents ,
        'stages'     => $current_stage ,
        'states'     => $current_state ,
        'container_notes' => '',
        'client_list' => $clients_lists,
        'type' => '',
        'container_type'=> $types_lists,
        'batch_number'=> '',
        'date_format'=> $js_date_format,

    );
}

//Get Products
$product_data = $productClass->get_types('Y');
$template_data['product_options'] = '';
$expiry = array();
$template_data['sell_by_days'] = '';
foreach ($product_data as $data) {
    $template_data['product_options'] .= "<option value='{$data['type_id']}'>{$data['type_name']}</option>";
    $expiry[$data['type_id']]['expiry'] = $data['type_expiration'];
    $expiry[$data['type_id']]['best_before'] = $data['type_best_before'];
}
$template_data['expiry_days'] = json_encode($expiry);

$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
