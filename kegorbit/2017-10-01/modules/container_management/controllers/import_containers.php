<?php
/**
 * Created by PhpStorm.
 * User: khulumusa
 * Date: 9/1/16
 * Time: 2:33 PM
 * 2016-08-31: Musa Khulu - Created
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/state.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/products_management/classes/products.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/customer_management/classes/customer.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();
$settingsClass  = new Settings();
$stateClass = new State();
$productClass = new Products();
$customerClass = new Customer();

//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//get predefined terminology either stage the user specified
$singular =$settingsClass->get_stage_terminology();

//get predefined terminology either container the user specified
$container_singular =$settingsClass->get_container_terminology();

$container_plural =$settingsClass->get_container_terminology('plural');

//get predefined terminology either state the user specified
$state_singular =$settingsClass->get_state_terminology();


$stages = $settingsClass->get_stages('Y');

$stage_list = '';
foreach ($stages AS $stage){
    $stage_list .= ' <li id="'.$stage['stage_id'].'" data-value="'.$stage['stage_id'].'" class="new_stage"><a href="#">'.$stage['stage_name'].'</a></li>';
}

$states = $stateClass->list_states();
$state_list = '';
foreach ($states AS $state){
    $state_list .= ' <li id="'.$state['state_id'].'" data-value="'.$state['state_id'].'" class="new_state"><a href="#">'.$state['state_name'].'</a></li>';
}

if(isset($_SESSION['user']['run_as'])){
    $company_data = $companyClass->get_company_by_code($_SESSION['user']['run_as']);
    if(isset($company_data['company_keg_limit'])){
        $company_data['company_keg_limit'];
    }
}
else{
    $company_data = $companyClass->get_company($_SESSION['user']['company_id']);
    if(isset($company_data['company_keg_limit'])){
        $company_data['company_keg_limit'];
    }
}
$count = $containerClass->company_container_count();

$company_container_limit = isset($company_data['company_keg_limit'])?$company_data['company_keg_limit']:'';
$company_hard_limit = isset($company_data['company_hard_limit'])?$company_data['company_hard_limit']:'';


if( $company_container_limit >$count && $company_hard_limit != '1'){
    $create_button = '<button type="button" class="btn btn btn-primary  btn-sm dim" id="create_container"> <i class="fa fa-plus-square-o"></i> Create '.$container_singular.'</button>';
}
elseif($company_container_limit <= $count && $company_hard_limit == '1'){
    $create_button = '<button type="button" class="btn btn btn-primary disabled  btn-sm dim" id="create_container" title="Limit Exceeded!"> <i class="fa fa-plus-square-o"></i> Create '.$container_singular.'</button>';
}
else{
    $create_button = '<button type="button" class="btn btn btn-primary  btn-sm dim" id="create_container"> <i class="fa fa-plus-square-o"></i> Create '.$container_singular.'</button>';
}

//allow or donot import button to display
if($company_data['allow_imports'] === '1'){
    $import_button = '<button type="button" class="btn btn btn-primary  btn-sm dim" id="import"> <i class="fa fa-upload"></i> Import '.$container_plural.'</button>';
}
else{
    $import_button ='';
}


$template_data = array(
    'stage_singular' => $singular,
    'state_singular' => $state_singular,
    'stage_list' => $stage_list,
    'state_list' => $state_list,
    'container_singular' => $container_singular,
    'create_container_button' =>  $create_button,
    'company_name' => $company_data['company_name'],
    'import_button' => $import_button,
);

//Get Products
$product_data = $productClass->get_types('Y');
$template_data['product_options'] = '';
$expiry = array();
$template_data['sell_by_days'] = '';
foreach ($product_data as $data) {
    $template_data['product_options'] .= "<option value='{$data['type_id']}'>{$data['type_name']}</option>";
    $expiry[$data['type_id']]['expiry'] = $data['type_expiration'];
    $expiry[$data['type_id']]['best_before'] = $data['type_best_before'];
}
$template_data['expiry_days'] = json_encode($expiry);

//Get client list
$client_data = $customerClass->get_customers(true,0,1000);
$template_data['client_options'] = '';
foreach ($client_data as $data) {
    $template_data['client_options'] .= "<option value='{$data['customer_id']}'>{$data['customer_name']}</option>";
}

$template2 = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/html/import_containers.html");
$template1 = $systemClass->merge_data($template2, $template_data);
echo $template1;
