<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 * 2016-08-18: Musa Khulu -Added Terminology switching to the container stages bloc on the dashboard
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';


//Vars
$systemClass = new System();
$settingsClass  = new Settings();


//Vars
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = file_get_contents("$dir/html/block.html");

//get predefined terminology for container the user specified
$container_singular =$settingsClass->get_container_terminology();

//get predefined terminology for  stage the user specified
$state_plural =$settingsClass->get_state_terminology('plural');

$template_data = array(
    'state_plural' => $state_plural,
    'container_singular' => $container_singular,
);


$template1 = $systemClass->merge_data($template2, $block);
echo $template1;
