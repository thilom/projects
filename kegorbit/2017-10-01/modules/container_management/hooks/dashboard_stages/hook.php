<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/state.class.php";

//Vars
$dir = substr(__FILE__, 0, strrpos(__FILE__, '/'));
$block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/hooks/dashboard_stages/html/block.html");


//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/classes/container.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$containerClass = new Container();
$systemClass = new System();
$settingsClass  = new Settings();
$dir = substr(__FILE__, 0, strrpos(__FILE__, '/'));
$block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/hooks/dashboard_stages/html/block.html");
$colors = array('002FFF', '5100FF', 'D000FF', 'FF00AE', 'FF002F', 'FF5100', '00FFF0', '008FFF', '7000FF', 'F000FF', 'FF008F');

//Get state count
$state_data = $containerClass->get_stage_count_default();

//get predefined terminology for container the user specified
$container_singular =$settingsClass->get_container_terminology();

//get predefined terminology for  stage the user specified
$stage_plural =$settingsClass->get_stage_terminology('plural');

$template_data['table_line'] = '';
$template_data['labels'] = '';
$template_data['data'] = '';
$template_data['colors'] = '';
$template_data['total'] = 0;

//check if user has permission to view Container Stages  Block
if (isset($_SESSION['user']['run_as'])) {
    $access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'dashboard_management', 'container_stages_block');
} else {
    $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'dashboard_management', 'container_stages_block');
}
if (!$access) {
    $template_data['display_or_not'] = 'Display:none';
} else {
    $template_data['display_or_not'] = '';
}

//Assemble for graph
foreach ($state_data as $key => $data) {
    $stage_count = $containerClass->get_stage_count($data['stage_id']);
    if (empty($stage_count)) {
        $stage_count = 0;
        $disabled = 'disabled';
    } else {
        $disabled = '';
    }
    $template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[$key]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>{$data['stage_name']}</td>
                        <td>{$stage_count}</td>
                        <td><button class='btn btn-xs btn-info {$disabled} view_stage' data-value='{$data['stage_id']}'>View</button></td>
                    </tr>";
    $template_data['labels'] .= "'{$data['stage_name']}',";
    $template_data['colors'] .= "'#{$colors[$key]}',";
    $template_data['data'] .= "{$stage_count},";
    $template_data['total'] += $stage_count;
}
$template_data['labels'] = trim($template_data['labels'], ',');
$template_data['colors'] = trim($template_data['colors'], ',');
$template_data['data'] = trim($template_data['data'], ',');
$template_data['stage_plural'] = $stage_plural;
$template_data['container_singular'] = $container_singular;

$block = $systemClass->merge_data($block, $template_data);


echo $block;