<?php
/**
 * Dashboard Container hook
 *
 * 2016-07-03: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/classes/container.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/classes/system.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';

//Vars
$containerClass = new Container();
$systemClass = new System();
$settingsClass  = new Settings();
$dir = substr(__FILE__, 0, strrpos(__FILE__,'/'));
$block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/container_management/hooks/dashboard_states/html/block.html");
$colors = array('000022','001166','09146D','0012A8','112288','2B2B2B','223399','3D3E4C','3F467F','8888AA','7986F2','99A4FF');

//Get state count
$state_data = $containerClass->get_state_count_default();

//get predefined terminology for container the user specified
$container_singular =$settingsClass->get_container_terminology();

//get predefined terminology for  state the user specified
$state_plural =$settingsClass->get_state_terminology('plural');

$template_data['table_line'] = '';
$template_data['labels'] = '';
$template_data['data'] = '';
$template_data['colors'] = '';
$template_data['total'] = 0;


//check if user has permission to view Container states  Block
if(isset($_SESSION['user']['run_as'])){
$access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'dashboard_management', 'container_states_block');
}
else{
    $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'dashboard_management', 'container_states_block');
}

if(!$access){
    $template_data['display_or_not'] = 'Display:none';
}
else{
    $template_data['display_or_not'] = '';
}

//Assemble for graph
foreach ($state_data as $key=>$data) {
    $state_count = $containerClass->get_state_count($data['state_id']);
    if(empty($state_count)){
        $state_count = 0;
        $disabled = 'disabled';
    }
    else{
        $disabled = '';
    }

    $template_data['table_line'] .= "<tr>
                        <td><div style='background-color: #{$colors[$key]}; width: 20px; height: 20px; margin: 2px;'></div></td>
                        <td>{$data['state_name']}</td>
                        <td>{$state_count}</td>
                        <td><button class='btn btn-xs btn-info {$disabled}  view_state'  data-value='{$data['state_id']}'>View</button></td>
                    </tr>";
    $template_data['labels'] .= "'{$data['state_name']}',";
    $template_data['colors'] .= "'#{$colors[$key]}',";
    $template_data['data'] .= "{$state_count},";
    $template_data['total'] += $state_count;
}
$template_data['labels'] = trim($template_data['labels'],',');
$template_data['colors'] = trim($template_data['colors'],',');
$template_data['data'] = trim($template_data['data'],',');
$template_data['state_plural'] = $state_plural;
$template_data['container_singular'] = $container_singular;

$block = $systemClass->merge_data($block, $template_data);

echo $block;