<?php
/**
 * List of dashboard hooks
 *
 * 2016-07-03: Thilo Muller - Created
 */

$ini_hooks = array();

$ini_hooks[0] = array('dir' => 'dashboard_stages',
                        'order' => 30,
                        'type' => 'dashboard',
                        'position' => 'left',
                        'access' => array('administrator'));

$ini_hooks[1] = array('dir' => 'dashboard_states',
                        'order' => 40,
                        'type' => 'dashboard',
                        'position' => 'left',
                        'access' => array('administrator'));
