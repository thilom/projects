<?php
/**
 * Container Class
 *
 * 2016-10-11: Thilo Muller - Added 'Brewery' (Items with no location) to get_customer_container_location_report().
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . "/kegorbit_passphrase/passphrase.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/settings_management/classes/settings.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/settings_management/classes/state.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/products_management/classes/products.class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/customer_management/classes/customer.class.php";

class Container extends System {


    private $settingClass;

    private $productClass;
    private $customerClass;
    private $stateClass;

    /**
     * State constructor.
     */
    function __construct() {
        if (isset($_SESSION['user']['prefix'])) {
            if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
                $this->company_code = $_SESSION['user']['run_as'];
            } else {
                $this->company_code = $_SESSION['user']['prefix'];
            }
        }

        $this->settingClass = new Settings();
        $this->productClass = new Products();
        $this->customerClass = new Customer();
        $this->stateClass = new State();
    }

    /*
    * Get container
    * @param active (string)
    */
    function get_containers($active = '') {
        $statement = "SELECT container.container_serial_number, container_types.container_type, container.container_capacity, stage.stage_name,
                            container_date, types.type_name, stage.active, states.out_of_circulation, states.state_name,
                            container_capacity_type, customer.customer_name, batch_number, container.container_bar_code
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers AS container
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_stages AS stage ON stage.stage_id = container.container_stage
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_types AS types ON types.type_id = container.container_contents
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_states AS states ON states.state_id = container.container_state
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_customers AS customer ON container.container_client = customer.customer_id
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_container_types AS container_types ON container.container_type = container_types.type_id";
        if ($active == 'N')
            $statement .= " WHERE states.out_of_circulation = 'Y'";
        else
            $statement .= " WHERE  states.out_of_circulation IS NULL OR states.out_of_circulation = '' ";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $container = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $container;
    }

    /*
   * Get containers for stage report
   * @param active (string)
   */
    function get_container_stage_report($active = '', $stage_id) {

        $statement = "SELECT container_serial_number, container_types.container_type, container.container_capacity,
                              stage.stage_name, container_date, expiry_date, types.type_name, stage.active, states.out_of_circulation,
                              states.state_name, container_capacity_type, container_client, customer_name, container_bar_code,
                              filled_date, delivery_date
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers AS container
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_stages AS stage ON stage.stage_id = container.container_stage
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_types AS types ON types.type_id = container.container_contents
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_states AS states ON states.state_id = container.container_state
                          LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_customers AS customers ON customers.customer_id = container.container_client
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_container_types AS container_types ON container.container_type = container_types.type_id";
        if ($stage_id == 0) {
            $statement .= " WHERE  states.out_of_circulation IS NULL OR states.out_of_circulation = '' ORDER BY stage.stage_name";
        } else {
            $statement .= " WHERE container_stage = :container_stage AND (states.out_of_circulation IS NULL OR states.out_of_circulation = '') ORDER BY stage.stage_name";
        }
        $sql = $GLOBALS['dbCon']->prepare($statement);
        if ($stage_id != 0) {
            $sql->bindParam(':container_stage', $stage_id);
        }
        $sql->execute();
        $container = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $container;
    }

    /*
 * Get containers for state report
 * @param active (string)
 */
    function get_container_state_report($active = '', $state_id) {

        $statement = "SELECT container_serial_number, container_types.container_type, container.container_capacity,
                                stage.stage_name, container_date, types.type_name, stage.active, states.out_of_circulation,
                                states.state_name, container_capacity_type, container_client,container.container_state, customer_name,
                                container_bar_code
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers AS container
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_stages AS stage ON stage.stage_id = container.container_stage
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_types AS types ON types.type_id = container.container_contents
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_states AS states ON states.state_id = container.container_state
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_customers AS customers ON customers.customer_id = container.container_client
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_container_types AS container_types ON container.container_type = container_types.type_id";
        if ($state_id == 0) {
            $statement .= " ORDER BY container.container_state";
        } else {
            $statement .= " WHERE container.container_state = :container_state  ORDER BY container.container_state";
        }
        $sql = $GLOBALS['dbCon']->prepare($statement);
        if ($state_id != 0) {
            $sql->bindParam(':container_state', $state_id);
        }
        $sql->execute();
        $container = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $container;
    }

    /*
   * Get container Location report
   * @param active (string)
   */
    function get_customer_container_location_report() {
        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();

        $statement = "SELECT customer_id, customer_name, customer_contact, AES_DECRYPT(customer_contact_number, :phrase) AS customer_contact_number,
                                    AES_DECRYPT(customer_contact_email, :phrase) AS customer_contact_email
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                        WHERE (inactive IS NULL OR inactive <> 'Y')";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':phrase', $phrase);
        $sql->execute();
        $customers = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();

        $customers[] = array('customer_id' => 0,
                                'customer_name' => 'Brewery',
                                'customer_contact' => '-',
                                'customer_contact_number' => '-',
                                'customer_contact_email' => '-'
        );

        return $customers;
    }

    /*
  * Get clients containers
  * @param active (string)
  */
    function get_customer_id($customer_name) {

        $statement = "SELECT customer_id
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_customers
                        WHERE  customer_name = :customer_name";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':customer_name', $customer_name);
        $sql->execute();
        $customer_id = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $customer_id[0]['customer_id'];
    }

    /*
  * Get clients containers
  * @param active (string)
  */
    function get_customer_container__report($customer_id) {

        if (!is_numeric($customer_id)) {
            $customer_id = $this->get_customer_id($customer_id);
        }

        if ($customer_id == 0) $customer_id = '';



        $statement = "SELECT container_serial_number, container_types.container_type, container.container_capacity,
                                    stage.stage_name, delivery_date, types.type_name, stage.active, states.out_of_circulation,
                                    states.state_name, container_capacity_type, container_client,container.container_state,
                                    container_bar_code
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers AS container
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_stages AS stage ON stage.stage_id = container.container_stage
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_types AS types ON types.type_id = container.container_contents
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_states AS states ON states.state_id = container.container_state
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_container_types AS container_types ON container.container_type = container_types.type_id
                        WHERE  container_client = :container_client";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_client', $customer_id);
        $sql->execute();
        $customer_containers = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $customer_containers;
    }


    /*
   * Get number of containers at client
   * @param active (string)
   */
    function get_customer_container_count($customer_id) {

        if ($customer_id == 0) {
            $statement = "SELECT  COUNT(*) AS container_count
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                         WHERE  container_client = '' ";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':container_client', $customer_id);
            $sql->execute();
            $sql_count_data = $sql->fetch();
            $sql->closeCursor();
        } else {
            $statement = "SELECT  COUNT(*) AS container_count
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                         WHERE  container_client = :container_client";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':container_client', $customer_id);
            $sql->execute();
            $sql_count_data = $sql->fetch();
            $sql->closeCursor();
        }
        return ($sql_count_data['container_count']);
    }


    /*
   * Get states
   * @param active (string)
   */
    function get_states() {

        $statement = "SELECT state_id, state_name, out_of_circulation
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_states
                        WHERE inactive = ''  OR inactive IS NULL";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        //$sql->bindParam(':state_name', $state_name);
        $sql->execute();
        $states = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $states;
    }

    /*
   * Get types
   * @param active (string)
   */
    function get_types() {

        $statement = "SELECT type_id, type_name,type_description,  active, type_expiration, type_best_before
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_types
                         WHERE active = 'Y' OR active IS NULL";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();

        $types = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $types;
    }

    /*
  * Get stages
  * @param active (string)
  */
    function get_stages() {

        $statement = "SELECT stage_id, stage_name, active, stage_order, is_default
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_stages
                        WHERE active = 'Y' OR active IS NULL ORDER BY stage_order";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();
        $stages = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $stages;
    }

    /**
     * Return a list of all active container states
     *
     * @param
     * @return mixed
     */
    function list_states() {
        $statement = "SELECT state_id, state_name, out_of_circulation, is_default
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_states
                        WHERE inactive IS NULL OR inactive = ''";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }


    /**
     * Modify container state
     * @param $container_id , $new_state
     * @return
     */
    function save_new_container_state($container_id, $new_state) {

        //Get state
        $state_data = $this->stateClass->get_state($new_state);
        $state_name = $state_data['state_name'];

        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_state = :container_state
                        WHERE container_serial_number = :container_serial_number
                        LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':container_state', $new_state);
        $sql_update->bindParam(':container_serial_number', $container_id);
        $sql_update->execute();
        $sql_update->closeCursor();
        $this->save_container_history($container_id, "Container State Changed to '$state_name'");
    }

    /**
     * Modify container stage
     * @param $container_id , $new_stage
     * @return
     */
    function save_new_container_stage($container_id, $new_stage, $action, $new_product = '', $best_before = '', $expiry_date = '', $batch_no = '', $delivery_date='', $filled_date = '') {

        //Get stage name
        $stage_data = $this->settingClass->get_stage($new_stage);
        $stage_name = $stage_data[0]['stage_name'];

        if ($action == 'clear_product_client') {
            $action = NULL;
            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_stage = :container_stage,
                            container_contents = :container_contents,
                            container_client = :container_client,
                            batch_number = '',
                            sell_by_date = '',
                            expiry_date = ''
                        WHERE container_serial_number = :container_serial_number";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':container_stage', $new_stage);
            $sql_update->bindParam(':container_serial_number', $container_id);
            $sql_update->bindParam(':container_contents', $action);
            $sql_update->bindParam(':container_client', $action);
            $sql_update->execute();
            $sql_update->closeCursor();
            $this->save_container_history($container_id, "Updated to '$stage_name' Stage");
        } elseif ($action == 'clear_client') {
            $action = NULL;
            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_stage = :container_stage,
                            container_client = :container_client
                        WHERE container_serial_number = :container_serial_number";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':container_stage', $new_stage);
            $sql_update->bindParam(':container_serial_number', $container_id);
            $sql_update->bindParam(':container_client', $action);
            $sql_update->execute();
            $sql_update->closeCursor();
            $this->save_container_history($container_id, "Updated to '$stage_name' Stage");
        } elseif ($action == 'clear_product') {
            $action = NULL;
            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_stage = :container_stage,
                           container_contents = :container_contents,
                            batch_number = '',
                            sell_by_date = '',
                            expiry_date = '',
                            filled_date = ''
                        WHERE container_serial_number = :container_serial_number";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':container_stage', $new_stage);
            $sql_update->bindParam(':container_serial_number', $container_id);
            $sql_update->bindParam(':container_contents', $action);
            $sql_update->execute();
            $sql_update->closeCursor();
            $this->save_container_history($container_id, "Updated to '$stage_name' Stage");
        } elseif ($action == 'select_product') {

            //Get product Name
            $product_data = $this->productClass->get_type($new_product);
            $product_name = $product_data[0]['type_name'];

            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_stage = :container_stage,
                           container_contents = :container_contents,
                           sell_by_date = :sell_by_date,
                           expiry_date = :expiry_date,
                           batch_number = :batch_number,
                           filled_date = :filled_date
                        WHERE container_serial_number = :container_serial_number
                        LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':container_stage', $new_stage);
            $sql_update->bindParam(':container_serial_number', $container_id);
            $sql_update->bindParam(':container_contents', $new_product);
            $sql_update->bindParam(':sell_by_date', $best_before);
            $sql_update->bindParam(':expiry_date', $expiry_date);
            $sql_update->bindParam(':batch_number', $batch_no);
            $sql_update->bindParam(':filled_date', $filled_date);
            $sql_update->execute();
            $sql_update->closeCursor();
            $this->save_container_history($container_id, "Updated to '$stage_name' Stage (Product: $product_name)");
        } elseif ($action == 'select_customer') {

            //Get Customer name
            $customer_data = $this->customerClass->get_customer($new_product);
            $customer_name = $customer_data['customer_name'];

            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_stage = :container_stage,
                           container_client = :container_client,
                           delivery_date = :delivery_date
                        WHERE container_serial_number = :container_serial_number";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':container_stage', $new_stage);
            $sql_update->bindParam(':container_serial_number', $container_id);
            $sql_update->bindParam(':container_client', $new_product);
            $sql_update->bindParam(':delivery_date', $delivery_date);
            $sql_update->execute();
            $sql_update->closeCursor();
            $this->save_container_history($container_id, "Updated to '$stage_name' Stage (Customer: $customer_name)");
        } else {
            $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_stage = :container_stage
                        WHERE container_serial_number = :container_serial_number";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bindParam(':container_stage', $new_stage);
            $sql_update->bindParam(':container_serial_number', $container_id);
            $sql_update->execute();
            $sql_update->closeCursor();
            $this->save_container_history($container_id, "Updated to '$stage_name' Stage");
        }
    }

    /*
    * Save new container
    * @param $container_data (array)
     * return $container_id
    */
    function save_new_container($container_data) {

        $date = date("Y-m-d h:i:s");

        $container_client = isset($container_data['current_client']) ? $container_data['current_client'] : '';
        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                      (container_serial_number,container_bar_code,container_type,container_capacity,container_capacity_type,container_stage,container_state,container_date,container_contents, container_client,batch_number)
					  VALUES
					  (:container_serial_number,:container_bar_code,:container_type,:container_capacity,:container_capacity_type,:container_stage,:container_state,:container_date,:container_contents, :container_client, :batch_no)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_serial_number', $container_data['serial_number']);
        $sql->bindParam(':container_bar_code', $container_data['bar_code']);
        $sql->bindParam(':container_type', $container_data['type']);
        $sql->bindParam(':container_capacity', $container_data['capacity']);
        $sql->bindParam(':container_capacity_type', $container_data['capacity_type']);
        $sql->bindParam(':container_stage', $container_data['current_stage']);
        $sql->bindParam(':container_state', $container_data['current_state']);
        $sql->bindParam(':container_date', $date);
        $sql->bindParam(':container_contents', $container_data['contents']);
        $sql->bindParam(':container_client', $container_client);
        $sql->bindParam(':batch_no', $container_data['batch_no']);
        $sql->execute();
        $container_id = $GLOBALS['dbCon']->lastInsertId();
        $sql->closeCursor();
        if (!empty($container_data['container_notes'])) {
            $this->save_container_note($container_data['serial_number'], $container_data['container_notes']);
            $this->save_container_history($container_data['serial_number'], $container_data['container_notes']);
        } else {
            $this->save_container_history($container_data['serial_number'], 'Created');
        }
        return $container_id;
    }

    /*
   * Save new note about container
   * @param $container_data (array)
    * return $container_id
   */
    function save_container_note($container_id, $note) {

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_container_notes
                      (container_serial_number,container_note, created_by)
					  VALUES
					  (:container_serial_number,:container_note, :created_by)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_serial_number', $container_id);
        $sql->bindParam(':container_note', $note);
        $sql->bindParam(':created_by', $_SESSION['user']['id']);
        $sql->execute();
        $sql->closeCursor();
    }

    /*
    * Get container
    * @param $container_id (string)
    */
    function get_selected_containers($container_id) {

        $statement = "SELECT container_serial_number, container_bar_code, container_type, container_capacity,container_capacity_type, stage.
                                        stage_name, container_date, types.type_name, stage.active, states.out_of_circulation,
                                        states.state_name, container_contents,container_client, batch_number, sell_by_date, expiry_date,
                                        filled_date, delivery_date
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers AS container
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_stages AS stage ON stage.stage_id = container.container_stage
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_types AS types ON types.type_id = container.container_contents
                         LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_states AS states ON states.state_id = container.container_state
                         WHERE  container.container_serial_number = :container_serial_number";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_serial_number', $container_id);
        $sql->execute();
        $container = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $container;
    }

    /*
    * Get container notes
    * @param $container_id (string)
    */
    function get_container_notes($container_id, $limit) {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();
        $date = date('Y-m-d H:i:s');

        $statement = "SELECT TIMESTAMPDIFF(HOUR,container_note_date,:date_now) as hours_elapsed  , container_note_id, container_serial_number, container_note, container_note_date, AES_DECRYPT(user_full_name, :phrase) AS user_full_name
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_container_notes as notes
                        LEFT JOIN {$GLOBALS['db_prefix']}_users AS users ON notes.created_by = users.user_id
                         WHERE  container_serial_number = :container_serial_number
                         LIMIT $limit";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_serial_number', $container_id);
        $sql->bindParam(':phrase', $phrase);
        $sql->bindParam(':date_now', $date);
        $sql->execute();
        $container_notes = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $container_notes;
    }

    /*
   * Get container notes count
   * @param $container_id (string)
   */
    function get_container_notes_count($container_id) {

        $statement = "SELECT  COUNT(*) AS note_count
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_container_notes
                         WHERE  container_serial_number = :container_serial_number";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_serial_number', $container_id);
        $sql->execute();
        $sql_count_data = $sql->fetch();
        $sql->closeCursor();
        return ($sql_count_data['note_count']);

    }


    /**
     * Return a count of containers in each state
     *
     * @return mixed
     */
    function get_state_count($state_id) {

        $statement = "SELECT COUNT(container_state) AS state_count
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                         WHERE  container_state = :container_state";
        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->bindParam(':container_state', $state_id);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetchColumn();
        $sql_count->closeCursor();
        return ($sql_count_data);

    }

    /**
     * Return a count of containers in each state
     *
     * @return mixed
     */
    function get_state_count_default() {

        $statement = "SELECT  state_name, state_id
                        FROM keg_{$this->company_code}_states
                          WHERE inactive = '' OR inactive IS NULL";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Return a count of containers in each stage
     *
     * @return mixed
     */
    function get_stage_count($stage_id) {

        $statement = "SELECT COUNT(a.container_stage) AS container_count
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers AS a
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_states AS b ON a.container_state = b.state_id
                         WHERE  a.container_stage = :container_stage
                            AND (b.out_of_circulation IS NULL OR b.out_of_circulation <> 1)";
        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->bindParam(':container_stage', $stage_id);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetchColumn();
        $sql_count->closeCursor();
        return ($sql_count_data);
    }

    /**
     * Return a count of containers in each stage
     *
     * @return mixed
     */
    function get_stage_count_default() {
        $statement = "SELECT  stage_name, stage_id
                        FROM keg_{$this->company_code}_stages
                         WHERE active = 'Y' OR active IS NULL";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Update Container
     * @param $container_data
     * @return
     */
    function update_container_data($container_data) {

        $date = date("Y-m-d h:i:s");
        $sell_by_date = date('Y-m-d', strtotime($container_data['sell_by_date']));
        $expiry_date = date('Y-m-d', strtotime($container_data['expiry_date']));
        $fill_date = date('Y-m-d', strtotime($container_data['fill_date']));
        $delivery_date = date('Y-m-d', strtotime($container_data['delivery_date']));

        $container_client = isset($container_data['current_client']) ? $container_data['current_client'] : '';
        if ($container_data['contents'] == '') {
            $container_contents = NULL;
        } else {
            $container_contents = $container_data['contents'];
        }
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET container_serial_number = :container_serial_number,
                            container_bar_code = :container_bar_code,
                            container_type = :container_type,
                            container_capacity = :container_capacity,
                            container_capacity_type = :container_capacity_type,
                            container_stage = :container_stage,
                            container_state = :container_state,
                            container_date = :container_date,
                            container_contents = :container_contents,
                            container_client = :container_client,
                            batch_number = :batch_number,
                            sell_by_date = :sell_by_date,
                            expiry_date = :expiry_date,
                            filled_date = :fill_date,
                            delivery_date = :delivery_date
                        WHERE container_serial_number = :container_serial_number";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':container_serial_number', $container_data['serial_number']);
        $sql_update->bindParam(':container_bar_code', $container_data['bar_code']);
        $sql_update->bindParam(':container_type', $container_data['type']);
        $sql_update->bindParam(':container_capacity', $container_data['capacity']);
        $sql_update->bindParam(':container_capacity_type', $container_data['capacity_type']);
        $sql_update->bindParam(':container_stage', $container_data['current_stage']);
        $sql_update->bindParam(':container_state', $container_data['current_state']);
        $sql_update->bindParam(':container_date', $date);
        $sql_update->bindParam(':container_contents', $container_contents);
        $sql_update->bindParam(':container_client', $container_client);
        $sql_update->bindParam(':batch_number', $container_data['batch_no']);
        $sql_update->bindParam(':sell_by_date', $sell_by_date);
        $sql_update->bindParam(':expiry_date', $expiry_date);
        $sql_update->bindParam(':fill_date', $fill_date);
        $sql_update->bindParam(':delivery_date', $delivery_date);
        $sql_update->execute();
        $sql_update->closeCursor();
        if (!empty($container_data['container_notes'])) {
            $this->save_container_note($container_data['serial_number'], $container_data['container_notes']);
            $this->save_container_history($container_data['serial_number'], $container_data['container_notes']);
        } else {
            $this->save_container_history($container_data['serial_number'], 'Updated');
        }

    }

    /**
     * Save container history
     *
     * @param $container_data (array)
     */
    function save_container_history($container_id, $action) {

        $statement = "INSERT INTO {$GLOBALS['db_prefix']}_{$this->company_code}_container_history
                      (container_serial_number,created_by, history_action)
					  VALUES
					  (:container_serial_number,:created_by, :history_action)";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_serial_number', $container_id);
        $sql->bindParam(':created_by', $_SESSION['user']['id']);
        $sql->bindParam(':history_action', $action);
        $sql->execute();
        $sql->closeCursor();
    }

    /**
     * Return a list of all active container history
     * @param
     * @return mixed
     */
    function get_history($container_id) {

        $Passphrase = new Passphrase();
        $phrase = $Passphrase->passphrase();
        $date = date('Y-m-d H:i:s');
        $statement = "SELECT container_serial_number, history_date, history_action,  AES_DECRYPT(user_full_name, :phrase) AS user_full_name,  DATE_FORMAT(history_date,'%H:%i:%s') TIMEONLY, TIMESTAMPDIFF(HOUR,history_date,:date_now) as hours_elapsed
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_container_history as history
                        LEFT JOIN {$GLOBALS['db_prefix']}_users AS users ON history.created_by = users.user_id
                        WHERE container_serial_number = :container_serial_number ORDER BY time(history_date) DESC";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':container_serial_number', $container_id);
        $sql_select->bindParam(':date_now', $date);
        $sql_select->bindParam(':phrase', $phrase);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /*
    * Get next step to take
    * @param $stage_id (string)
    */
    function get_next_step($stage_id) {

        $statement = "SELECT onselect
                      FROM {$GLOBALS['db_prefix']}_{$this->company_code}_stages 
                         WHERE  stage_id = :stage_id";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':stage_id', $stage_id);
        $sql->execute();
        $next_step = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $next_step[0]['onselect'];
    }

    /**
     * Get a container serial number and check if it already exists in the containers table
     * @param string $serial_number
     * @return int
     */
    function check_duplicate_serial_number($serial_number) {

        $statement = "SELECT COUNT(container_serial_number)
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        WHERE container_serial_number = :container_serial_number
                        LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_serial_number', $serial_number);
        $sql->execute();
        $sql_select_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_select_data;
    }

    /**
     * Get a container bar code and check if it already exists in the containers table
     * @param string $bar_code
     * @return int
     */
    function check_duplicate_bar_code($bar_code) {

        $statement = "SELECT COUNT(container_bar_code)
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        WHERE container_bar_code = :container_bar_code
                        LIMIT 1";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->bindParam(':container_bar_code', $bar_code);
        $sql->execute();
        $sql_select_data = $sql->fetchColumn();
        $sql->closeCursor();
        return $sql_select_data;
    }

    /**
     * get the containersfor a company
     * @param
     * @return count
     */
    function company_container_count() {

        $statement = "SELECT COUNT(*) AS container_count
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers";
        $sql_count = $GLOBALS['dbCon']->prepare($statement);
        $sql_count->execute();
        $sql_count_data = $sql_count->fetch();
        $sql_count->closeCursor();

        return ($sql_count_data['container_count']);
    }


    function get_expired_containers_count() {

        $result['ok'] = 0;
        $result['near_expired'] = 0;
        $result['expired'] = 0;
        $result['past_sell_by'] = 0;
        $result['near_sell_by'] = 0;

        //Get company_data
        $statement = "SELECT near_date_range
                        FROM {$GLOBALS['db_prefix']}_companies
                        WHERE company_db_prefix = :prefix
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':prefix', $this->company_code);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();
        $days = $sql_result['near_date_range'] == 0 ? 10 : $sql_result['near_date_range'];

        //Calculate dates
        $today = strtotime("today");
        $near_expiry = strtotime("today + $days days");

        //Expired Containers
        $statement = "SELECT container_serial_number, expiry_date, sell_by_date
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        WHERE container_contents != 0 AND expiry_date <> '0000-00-00' AND expiry_date IS NOT NULL";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result2 = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        foreach ($sql_result2 as $data) {

            $data_sell_by = strtotime($data['sell_by_date']);
            $data_expired = strtotime($data['expiry_date']);
            $data_near_expired = strtotime($data['expiry_date'] . "- $days days");
            $data_near_sell_by = strtotime($data['sell_by_date'] . "- $days days");
//            $dne = date('Y-m-d', $data_near_expired);
//            $dnsb = date('Y-m-d', $data_near_sell_by);

//            echo "SID: {$data['container_serial_number']}<br>";
//            echo "NSB: $data_near_sell_by ({$dnsb})<br>";
//            echo "SB: $data_sell_by ({$data['sell_by_date']})<br>";
//            echo "NEX: $data_near_expired ({$dne})<br>";
//            echo "EX: $data_expired ({$data['expiry_date']})<br>";

            if ($data_expired < 0) {
//                echo "SKIPPED <hr>";
                continue;
            } else if ($today > $data_expired) {
                $result['expired']++;
//                echo "EXPIRED";
            } else if ($today > $data_near_expired) {
                $result['near_expired']++;
//                echo "NEAR EXPIRY";
            } else if ($today > $data_sell_by) {
                $result['past_sell_by']++;
//                echo "PAST SELL BY";
            } else if ($today > $data_near_sell_by) {
                $result['near_sell_by']++;
//                echo "NEAR SELL BY";
            } else {
                $result['ok']++;
//                echo "OK";
            }
//            echo "<hr>";
        }

        //Expired Containers
//        $statement = "SELECT COUNT(*) AS near_expired
//                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
//                        WHERE container_stage = 3
//                            AND expiry_date > :expiry_date
//                            AND expiry_date < :near_expiry_date";
//        $sql_select = $GLOBALS['dbCon']->prepare($statement);
//        $sql_select->bindParam(':expiry_date', $today);
//        $sql_select->bindParam(':near_expiry_date', $near_expiry);
//        $sql_select->execute();
//        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
//        $sql_select->closeCursor();
//        $result['near_expired'] = $sql_result['near_expired'];

        //Past Sell By
//        $statement = "SELECT COUNT(*) AS past_sell_by
//                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
//                        WHERE container_stage = 3
//                            AND sell_by_date < :expiry_date";
//        $sql_select = $GLOBALS['dbCon']->prepare($statement);
//        $sql_select->bindParam(':expiry_date', $today);
//        $sql_select->execute();
//        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
//        $sql_select->closeCursor();
//        $result['past_sell_by'] = $sql_result['past_sell_by'];

        //Expired Containers
//        $statement = "SELECT COUNT(*) AS near_sell_by
//                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
//                        WHERE container_stage = 3
//                            AND sell_by_date > :sell_by_date
//                            AND sell_by_date < :near_sell_by_date";
//        $sql_select = $GLOBALS['dbCon']->prepare($statement);
//        $sql_select->bindParam(':sell_by_date', $today);
//        $sql_select->bindParam(':near_sell_by_date', $near_expiry);
//        $sql_select->execute();
//        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
//        $sql_select->closeCursor();
//        $result['near_sell_by'] = $sql_result['near_sell_by'];

        //Expired Containers
//        $statement = "SELECT COUNT(*) AS ok
//                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers
//                        WHERE container_stage = 3
//                            AND sell_by_date > :near_sell_by_date";
//        $sql_select = $GLOBALS['dbCon']->prepare($statement);
//        $sql_select->bindParam(':near_sell_by_date', $near_expiry);
//        $sql_select->execute();
//        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
//        $sql_select->closeCursor();
//        $result['ok'] = $sql_result['ok'];

        return $result;
    }

    /**
     * get the container contents for a company
     * @param
     * @return count
     */
    function company_container_contents_report($action) {

        $result = array();
        $today = strtotime("today");

        //Get company_data
        $statement = "SELECT near_date_range
                        FROM {$GLOBALS['db_prefix']}_companies
                        WHERE company_db_prefix = :prefix
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':prefix', $this->company_code);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();
        $days = $sql_result['near_date_range'] == 0 ? 10 : $sql_result['near_date_range'];

        //GET Containers
        $statement = "SELECT container_contents, container_serial_number, container_types.container_type,
                                    container.container_capacity, container_capacity_type, container_client, types.type_name,
                                    customer_name, container_bar_code, filled_date, sell_by_date, expiry_date
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_containers as container
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_types AS types ON types.type_id = container.container_contents
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_customers AS customers ON customers.customer_id = container.container_client
                        LEFT JOIN {$GLOBALS['db_prefix']}_{$this->company_code}_container_types AS container_types ON container.container_type = container_types.type_id
                        WHERE container_contents != 0  ";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result2 = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        foreach ($sql_result2 as $data) {

            $data_sell_by = strtotime($data['sell_by_date']);
            $data_expired = strtotime($data['expiry_date']);
            $data_near_expired = strtotime($data['expiry_date'] . "- $days days");
            $data_near_sell_by = strtotime($data['sell_by_date'] . "- $days days");
//            $dne = date('Y-m-d', $data_near_expired);
//            $dnsb = date('Y-m-d', $data_near_sell_by);

//            echo "SID: {$data['container_serial_number']}<br>";
//            echo "NSB: $data_near_sell_by ({$dnsb})<br>";
//            echo "SB: $data_sell_by ({$data['sell_by_date']})<br>";
//            echo "NEX: $data_near_expired ({$dne})<br>";
//            echo "EX: $data_expired ({$data['expiry_date']})<br>";

            if ($action == '0') {
                $result[] = $data;
            } else {

                if ($data_expired < 0) {
//                echo "SKIPPED <hr>";
                    continue;
                } else if ($today > $data_expired) {
                    if ($action == 'expired') $result[] = $data;
//                $result['expired']++;
//                echo "EXPIRED";


                } else if ($today > $data_near_expired) {
                    if ($action == 'near_expired') $result[] = $data;
//                $result['near_expired']++;
//                echo "NEAR EXPIRY";
                } else if ($today > $data_sell_by) {
                    if ($action == 'past_sell_by') $result[] = $data;
//                $result['past_sell_by']++;
//                echo "PAST SELL BY";
                } else if ($today > $data_near_sell_by) {
                    if ($action == 'near_sell_by') $result[] = $data;
//                $result['near_sell_by']++;
//                echo "NEAR SELL BY";
                } else {
                    if ($action == 'ok') $result[] = $data;
//                $result['ok']++;
//                echo "OK";
                }
            }
//            echo "<hr>";
        }

        return $result;

    }

    /*
   * Get All conrtainer types
   * @param active (string)
   */
    function get_container_types() {

        $statement = "SELECT type_id, container_type,container_capacity,  active
                        FROM {$GLOBALS['db_prefix']}_{$this->company_code}_container_types
                         WHERE active = 'Y' OR active IS NULL";
        $sql = $GLOBALS['dbCon']->prepare($statement);
        $sql->execute();

        $contaner_types = $sql->fetchAll(PDO::FETCH_ASSOC);
        $sql->closeCursor();
        return $contaner_types;
    }


    /**
     * Update a containers sell by and expiry date.
     * For CSV import purposes only.
     *
     * @param $container_serial
     * @param $sell_by_date
     * @param $expiry_date
     */
    function update_container_dates($container_serial, $sell_by_date, $expiry_date) {
        $statement = "UPDATE {$GLOBALS['db_prefix']}_{$this->company_code}_containers
                        SET sell_by_date = :sell_by_date,
                              expiry_date = :expiry_date
                        WHERE container_serial_number = :container_serial
                        LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':container_serial', $container_serial);
        $sql_update->bindParam(':sell_by_date', $sell_by_date);
        $sql_update->bindParam(':expiry_date', $expiry_date);
        $sql_update->execute();
        $sql_update->closeCursor();
    }

}