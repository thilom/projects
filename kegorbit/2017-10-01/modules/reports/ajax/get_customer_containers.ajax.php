<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-07-28
 * Time: 12:12 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();
$companyClass = new Company();
$customer_id = $_GET['customer_id'];

$output = array();
$output['aaData'] = '';


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

//get the company date format
$date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];


//get a list of all containers and their locations
$location_containers = $containerClass->get_customer_container__report($customer_id);

if(empty($location_containers)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array( 'container_serial_number', 'container_bar_code', 'delivery_date', 'stage_name');
    foreach($location_containers as $location_container) {
        $row = array();
        $row[] = $location_container['container_serial_number'];
        $row[] = $location_container['container_bar_code'];
        $row[] = $location_container['delivery_date'];
//        $row[] = $location_container['stage_name'];

//        for($i=0; $i<count($aColumns); $i++)
//        {
//            if($aColumns[$i] == 'customer_id')
//                $container_id = $location_container[$aColumns[$i]];
//            $row[] = $location_container[$aColumns[$i]];
//        }
        $date = date($format. ' H:i:s',strtotime($location_container['delivery_date']));

//        $row[] = $date;
        $row[] = $location_container['stage_name'];
        $row[] = $location_container['type_name'];

        $row[] = $location_container['container_capacity'].''.$location_container['container_capacity_type'];



        $output['aaData'][] = $row;
    }
}
echo json_encode($output);

