<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-07-27
 * Time: 11:56 AM
 * report  management - fetch a list of all containers by stage
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();
$companyClass = new Company();
$output = array();
$output['aaData'] = '';
$active = $_GET['active'];
$stage_id =$_GET['stage_id'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all containers related to logged in user
$containers = $containerClass->get_container_stage_report($active, $stage_id);

//get the company date format
if(isset($_SESSION['user']['run_as'])) {
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['run_as']);
} else{
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
}

$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];

if(empty($containers)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('stage_name','container_serial_number', 'container_bar_code', 'container_type');

    foreach($containers as $container) {
        $row = array();

        $row[] = $container['stage_name'];
        $row[] = $container['container_serial_number'];
        $row[] = $container['container_bar_code'];

        $row[] = empty($container['filled_date'])||$container['filled_date']=='0000-00-00'?'-':date($format,strtotime($container['filled_date']));
        $row[] = empty($container['delivery_date'])||$container['delivery_date']=='0000-00-00'?'-':date($format,strtotime($container['delivery_date']));
        $row[] = $container['type_name'];
        $row[] = $container['container_capacity'].''.$container['container_capacity_type'];
        $date1 = new DateTime($container['expiry_date']);
        $date2 = new DateTime('today');
        if(empty($container['type_name']))
            $row[] ='-';
        else
            if (empty($container['expiry_date']) || $container['expiry_date'] == '0000-00-00') {
                $row[] = '-';
            } else {
                $diff = $date1->diff($date2)->format("%a days");
                $row[] = $diff;
            }

        $row[] = empty($container['customer_name'])?'Brewery':$container['customer_name'];

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
