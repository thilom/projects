<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-07-27
 * Time: 01:57 PM
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();
$companyClass = new Company();

$output = array();
$output['aaData'] = '';
$active = $_GET['active'];
$state_id =$_GET['state_id'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all containers related to logged in user
$containers = $containerClass->get_container_state_report($active, $state_id);

//get the company date format
if(isset($_SESSION['user']['run_as'])) {
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['run_as']);
} else{
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
}

$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];

if(empty($containers)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('state_name','container_serial_number', 'container_bar_code', 'container_type');

    foreach($containers as $container)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'container_serial_number')
                $container_id = $container[$aColumns[$i]];
            $row[] = $container[$aColumns[$i]];
        }

        $date = date($format. ' H:i:s',strtotime($container['container_date']));
        $row[] = $date;
        $row[] = $container['type_name'];
        if($container['out_of_circulation'] == 1){
            $row[] = '<span class="label label-danger">Yes</span>';
        }
        else{
            $row[] = '<span class="label label-primary">No</span>';
        }
        $row[] = $container['container_capacity'].''.$container['container_capacity_type'];
        $row[] = empty($container['customer_name'])?'Brewery':$container['customer_name'];

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);

