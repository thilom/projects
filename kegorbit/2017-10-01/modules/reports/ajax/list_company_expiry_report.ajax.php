<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-08-01
 * Time: 11:02 AM
 * 2016-08-01: Musa Khulu - Created
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();

$output = array();
$output['aaData'] = '';



//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;

$expiry =$_GET['expiry'];
//get a list of all companies with their expected expiry date
$company_expirations = $companyClass->get_companies_filter('Y', $expiry);

//get company date format
$date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
$format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];



if(empty($company_expirations)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}

else {
    $aColumns = array('company_name','company_contact_name', 'company_contact_number');
    foreach($company_expirations as $company_expiration)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'company_id')
                $company_id = $company_expiration[$aColumns[$i]];
            $row[] = $company_expiration[$aColumns[$i]];
        }
        $date = date($format,strtotime($company_expiration['raw_renewal_date']));
        if($company_expiration['company_renewal_date'] >= 90){
            $row[] = '<h1><span class="label label-primary"><i class="fa fa-calendar "></i> '.$date.' ('.$company_expiration['company_renewal_date'].' Days) </span></h1>';
        }
        elseif($company_expiration['company_renewal_date'] >= 60 && $company_expiration['company_renewal_date'] <90){
            $row[] = '<h1><span class="label label-warning"><i class="fa fa-calendar "></i> '.$date.' ('.$company_expiration['company_renewal_date'].' Days) </span></h1>';
        }
        else{
            $row[] = '<h1><span class="label label-danger"><i class="fa fa-calendar "></i> '.$date.' ('.$company_expiration['company_renewal_date'].' Days) </span></h1>';
        }

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
