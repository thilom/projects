<?php
/**
 * Return data for customer usage
 *
 * 2016-07-25: Thilo Muller - Created
 */

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/settings_management/classes/application.class.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/company_management/classes/company.class.php";

//Vars
$companyClass = new Company();
$appClass = new Application();

//Get customer data
$customer_data = $companyClass->get_companies_report();
$dData =  array('aaData' => array());


//Get current settings
$settings = $appClass->get_settings();

//Assemble for datatables
foreach ($customer_data as $data) {

   //Vars
    $notes = '';

    //Container Limit
    $keg_diff = $data['company_keg_limit'] - $data['container_count'];
    if ($keg_diff < 0) {
        $notes .= "<span class='text-danger'>Over Keg Limit by: {$keg_diff} more.</span><br>";
    } else if ($keg_diff == 0) {
        $notes .= "<span class='text-warning'>Keg Limit Reached</span><br>";
    } else if ($data['container_count'] > ($data['company_keg_limit']-floor($data['company_keg_limit']/10)) && $data['container_count'] < $data['company_keg_limit']) {
        $notes .= "<span class='text-warning'>Nearing Keg Limit</span><br>";
    }

    //User Limit
    $user_diff = $data['company_user_limit'] - $data['user_count'];
    if ($user_diff < 0) {
        $notes .= "<span class='text-danger'>Over User Limit by: {$user_diff} more.</span><br>";
    } else if ($user_diff == 0) {
        $notes .= "<span class='text-warning'>User Limit Reached</span><br>";
    } else if ($data['user_count'] > ($data['company_user_limit']-floor($data['company_user_limit']/10)) && $data['user_count'] < $data['company_user_limit']) {
        $notes .= "<span class='text-warning'>Nearing User Limit</span><br>";
    }

    //Device Limit
    $device_diff = $data['company_device_limit'] - $data['device_count'];
    if ($device_diff < 0) {
        $notes .= "<span class='text-danger'>Over Device Limit by: {$device_diff} more.</span><br>";
    } else if ($device_diff == 0) {
        $notes .= "<span class='text-warning'>Device Limit Reached</span><br>";
    } else if ($data['device_count'] > ($data['company_device_limit']-floor($data['company_device_limit']/10)) && $data['device_count'] < $data['company_device_limit']) {
        $notes .= "<span class='text-warning'>Nearing Device Limit</span><br>";
    }

    //Expiry
    if ($data['company_renewal_date'] < -$settings['grace_period']) {
        $notes .= "<span class='text-danger'>Account Expired</span><br>";
    } else if ($data['company_renewal_date'] < 0) {
        $notes .= "<span class='text-danger'>Account in Grace Period</span><br>";
    } else if ($data['company_renewal_date'] < $settings['grace_period']) {
        $notes .= "<span class='text-warning'>Account Nearing Expiry</span><br>";
    }
    //get company date format
    $date_format = $companyClass->get_company_date_format($_SESSION['user']['company_id']);
    $format = empty($date_format['application_date']) ? 'Y-m-d' : $date_format['application_date'];

    $date = date($format,strtotime($data['raw_renewal_date']));

    $line = array();
    $line[] = $data['company_name'];
    $line[] = $data['user_count'] . " (" . $data['company_user_limit'] . ')';;
    $line[] = $data['container_count'] . " (" . $data['company_keg_limit'] . ')';
    $line[] = $data['device_count'] . " (" . $data['company_device_limit'] . ')';
    $line[] = $date . " (" . $data['company_renewal_date'] . " days)";
    $line[] = $notes;

    $dData['aaData'][] = $line;

}

echo json_encode($dData);

