<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-07-28
 * Time: 08:55 AM
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();

$output = array();
$output['aaData'] = '';


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all containers and their locations
$location_containers = $containerClass->get_customer_container_location_report();

if(empty($location_containers)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('customer_name','customer_contact', 'customer_contact_number', 'customer_contact_email');
    foreach($location_containers as $location_container)
    {
        $row = array();
        $row[] = '';
        $row[] = $containerClass->get_customer_container_count($location_container['customer_id']);


        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'customer_id')
                $container_id = $location_container[$aColumns[$i]];
            $row[] = $location_container[$aColumns[$i]];
        }

        $row[] = '<a href="/index.php?m=reports&a=customer_location_report_filtered&customer_id='.$location_container['customer_id'].'"  class="btn btn-primary  btn-sm dim" > <i class="fa fa-plus-square"></i> Show More</a>';


        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
