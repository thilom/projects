<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-07-27
 * Time: 11:56 AM
 * report  management - fetch a list of all containers products by expiry
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/container_management/classes/container.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$containerClass = new Container();

$output = array();
$output['aaData'] = '';
$action =$_GET['action'];


//Check if user is logged in
$user_logged_in = $securityClass->is_logged_in();
if(!$user_logged_in) die;


//get a list of all containers related to logged in user
$containers = $containerClass->company_container_contents_report( $action);



if(empty($containers)) {
    $row = array();
    $row[] = array();
    $output['sEcho'] = 1;
    $output['iTotalRecords'] = 0;
    $output['iTotalDisplayRecords'] = 0;
    $output['aaData'] = array();
}
else {
    $aColumns = array('type_name','container_serial_number', 'container_bar_code', 'filled_date', 'container_type');

    foreach($containers as $container)
    {
        $row = array();
        for($i=0; $i<count($aColumns); $i++)
        {
            if($aColumns[$i] == 'container_serial_number')
                $container_id = $container[$aColumns[$i]];
                $row[] = $container[$aColumns[$i]];
        }
        $row[] = $container['container_capacity'].''.$container['container_capacity_type'];
        $row[] = empty($container['customer_name'])?'Brewery':$container['customer_name'];

        $output['aaData'][] = $row;
    }
}
echo json_encode($output);
