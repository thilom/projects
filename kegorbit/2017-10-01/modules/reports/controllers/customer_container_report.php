<?php
/**
 * Container report for a specified client
 *
 * 2016-07-24: Thilo Muller - Created
 */


//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';


//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();


//Vars
$systemClass = new System();
$expiry= isset($_GET['expiry'])? $_GET['expiry'] :0;
$report_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/reports/html/customer_container_report.html");

if(isset($_SESSION['user']['run_as'])){
    $company_data = $companyClass->get_company($_SESSION['user']['run_as']);
}
else{
    $company_data = $companyClass->get_company($_SESSION['user']['company_id']);
}



$template_data = array(
    'filter' => $expiry,
    'company_name' => $company_data['company_name'],
);

$template1 = $systemClass->merge_data($report_template, $template_data);
echo $template1;



