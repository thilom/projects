<?php
/**
 * Client Usage report
 *
 * 2016-07-25: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/classes/company.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';


//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$companyClass = new Company();
$settingsClass = new Settings();


$report_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/reports/html/client_usage_report.html");


//get predefined terminology  for stage the user specified
$stage_plural =$settingsClass->get_stage_terminology('plural');
//get predefined terminology for container the user specified
$container_plural =$settingsClass->get_container_terminology('plural');
//get predefined terminology for  state the user specified
$state_plural =$settingsClass->get_state_terminology('plural');
//get predefined terminology  customer the user specified
$customer_singular =$settingsClass->get_customer_terminology();


if(isset($_SESSION['user']['run_as'])){
    $company_data = $companyClass->get_company($_SESSION['user']['run_as']);
}
else{
    $company_data = $companyClass->get_company($_SESSION['user']['company_id']);
}

$template_data = array(
    'company_name' => $company_data['company_name'],
    'container_plural' => $container_plural,
    'customer_singular' => $customer_singular,
);


$template1 = $systemClass->merge_data($report_template, $template_data);
echo $template1;
