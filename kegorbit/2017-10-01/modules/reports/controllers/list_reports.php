<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/07/25
 * Time: 12:25 PM
 * 2016-08-19: Musa Khulu - added terminlogy checker
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';


//Vars
$report_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/reports/html/list_reports.html");
$securityClass = new Security();


$company_code = 'default';
if (isset($_SESSION['user']['run_as']) && !empty($_SESSION['user']['run_as'])) {
    $company_code = $_SESSION['user']['run_as'];
} else {
    if(isset($_SESSION['user']['prefix']) && !empty($_SESSION['user']['prefix'])) {
        $company_code = $_SESSION['user']['prefix'];
    }
}

//get predefined terminology  for stage the user specified
$stage_plural =$settingsClass->get_stage_terminology('plural');
//get predefined terminology for container the user specified
$container_singular =$settingsClass->get_container_terminology();
//get predefined terminology for  state the user specified
$state_plural =$settingsClass->get_state_terminology('plural');
//get predefined terminology  customer the user specified
$customer_singular =$settingsClass->get_customer_terminology();


//check if user has permission to view container reports
if(isset($_SESSION['user']['run_as'])){
    $access = $securityClass->check_user_permission($_SESSION['user']['run_as_id'], 'reports', 'container_reports');
}
else{
    $access = $securityClass->check_user_permission($_SESSION['user']['id'], 'reports', 'container_reports');
}


if($company_code == 'default') {
    $container_reports = '<div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title text-center">
                    <h4><span class="text-navy">Company  Reports</span></h4>
                </div>
                <div class="ibox-content p-md ">
                    <div class="text-center">
                        <button class="btn btn-info  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="client_usage_report"><i class="fa fa-users fa-2x" ></i><br><h2>Account<br>Usage</h2></button>
                        <button class="btn btn-success dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="account_expiry_report"><i class="fa fa-calendar  fa-2x" ></i><br><h2>Account<br>Expiry</h2></button>
                    </div>
                </div>
            </div>
        </div>
    </div>';
}
else {
    $container_reports = '<div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title text-center">
                    <h4><span class="text-navy">'.$container_singular. ' Reports</span></h4>
                </div>
                <div class="ibox-content p-md" >
                    <div class="text-center">
                        <button class="btn btn-info  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="customer_container_location_report"><i class="fa fa-globe fa-2x" ></i><br><h2>'.$container_singular. '<br>Location</h2></button>
                        <button class="btn btn-warning  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="customer_container_stages_report"><i class="fa fa-file-word-o  fa-2x" ></i><br><h2>'.$container_singular. '<br>'.$stage_plural. '</h2></button>
                        <button class="btn btn-danger  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="customer_container_states_report"><i class="fa fa-file-o  fa-2x" ></i><br><h2>'.$container_singular. '<br>'.$state_plural. '</h2></button>
                        <button class="btn btn-success  dim btn-large-dim btn-outline" type="button" style="height: 5cm; width: 5cm" id="customer_container_product_report"><i class="fa fa-cart-plus  fa-2x" ></i><br><h2>'.$container_singular. '<br>Contents</h2></button>
                    </div>
                </div>
            </div>
        </div>
    </div>';
}


$template_data = array(
    'stage_plural' => $stage_plural,
    'state_plural' => $state_plural,
    'container_singular' => $container_singular,
    'customer_singular' => $customer_singular,
    'container_reports' => $container_reports,
);

$template1 = $systemClass->merge_data($report_template, $template_data);
echo $template1;
