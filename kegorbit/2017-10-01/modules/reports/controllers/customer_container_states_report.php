<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-07-27
 * Time: 01:49 PM
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';


//Vars
$report_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/reports/html/customer_container_states_report.html");
$state_id = isset($_GET['state_id'])? $_GET['state_id'] :0;


//get predefined terminology  container the user specified
$container_singular =$settingsClass->get_container_terminology();
$container_plural =$settingsClass->get_container_terminology('plural');
//get predefined terminology  state the user specified
$state_plural =$settingsClass->get_state_terminology('plural');
$state_singular =$settingsClass->get_state_terminology();
//get predefined terminology  customer the user specified
$customer_singular =$settingsClass->get_customer_terminology();

if(isset($_SESSION['user']['run_as'])){
    $company_data = $companyClass->get_company($_SESSION['user']['run_as']);
}
else{
    $company_data = $companyClass->get_company($_SESSION['user']['company_id']);
}


$template_data = array(
    'state_plural' => $state_plural,
    'state_singular' => $state_singular,
    'container_singular' => $container_singular,
    'container_plural' => $container_plural,
    'customer_singular' => $customer_singular,
    'state_id' => $state_id,
    'company_name' => $company_data['company_name'],
);

$template1 = $systemClass->merge_data($report_template, $template_data);
echo $template1;
