<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-07-28
 * Time: 08:37 AM
 */
//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/customer_management/classes/customer.class.php';


//Vars
$customerClass = new Customer();
$report_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/reports/html/customer_location_report_filtered.html");
$customer_id = isset($_GET['customer_id'])? $_GET['customer_id']:0;


//get predefined terminology  stage the user specified
$stage_plural =$settingsClass->get_stage_terminology('plural');
$stage_singular =$settingsClass->get_stage_terminology();
//get predefined terminology  container the user specified
$container_singular =$settingsClass->get_container_terminology();
$container_plural =$settingsClass->get_container_terminology('plural');
//get predefined terminology  state the user specified
$state_plural =$settingsClass->get_state_terminology('plural');
//get predefined terminology  customer the user specified
$customer_singular =$settingsClass->get_customer_terminology();
$customer_plural =$settingsClass->get_customer_terminology('plural');

//get customer details
$customer_data = $customerClass->get_customer($customer_id);

if(isset($_SESSION['user']['run_as'])){
    $company_data = $companyClass->get_company($_SESSION['user']['run_as']);
}
else{
    $company_data = $companyClass->get_company($_SESSION['user']['company_id']);
}





$template_data = array(
    'stage_plural' => $stage_plural,
    'stage_singular' => $stage_singular,
    'state_plural' => $state_plural,
    'container_singular' => $container_singular,
    'container_plural' =>$container_plural,
    'customer_singular' => $customer_singular,
    'customer_id' => $customer_id,
    'customer_plural' => $customer_plural,
    'customer_name' => $customer_id==0?'Brewery':$customer_data['customer_name'],
    'company_name' => $company_data['company_name'],
);

$template1 = $systemClass->merge_data($report_template, $template_data);
echo $template1;

