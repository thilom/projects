
$(document).ready(function() {

    $('#client_usage_report').click(function () {
        document.location = '/index.php?m=reports&a=client_usage_report';
    });

    $('#account_expiry_report').click(function () {
        document.location = '/index.php?m=reports&a=customer_container_report';
    });

    $('#customer_container_stages_report').click(function () {
        document.location = '/index.php?m=reports&a=customer_container_stages_report';
    });

    $('#customer_container_states_report').click(function () {
        document.location = '/index.php?m=reports&a=customer_container_states_report';
    });

    $('#customer_container_location_report').click(function () {
        document.location = '/index.php?m=reports&a=customer_container_location_report';
    });

    $('#customer_container_product_report').click(function () {
        document.location = '/index.php?m=reports&a=customer_container_product_report';
    });

});