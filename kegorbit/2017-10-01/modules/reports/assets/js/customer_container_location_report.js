
$(document).ready(function () {
    create_table();
    $('#copy').click(function(){
        $(".copyButton").trigger('click');
    });

    $('#excel').click(function(){
        $(".excelButton").trigger('click');
    });
    $('#csv').click(function(){
        $(".csvButton").trigger('click');
    });
    $('#pdf').click(function(){
        $(".pdfButton").trigger('click');
    });
    $('#print').click(function(){
        $(".printButton").trigger('click');
    });

});

function create_table() {
    var date = moment().format('YYYY-MM-DD h-mm-ss');
  var table =  $('#locationReportTbl').DataTable({
        dom: 'Blfrtip',
        buttons: [
            {
                extend:    'copyHtml5',
                className: 'copyButton hidden',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4]
                }
            },
            {
                extend:    'excelHtml5',
                title: $('#company_name').val()+' '+ $('#container_singular').val()+' Location Report '+ date,
                className: 'excelButton hidden',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4]
                }
            },
            {
                extend:    'csvHtml5',
                title: $('#company_name').val()+' '+ $('#container_singular').val()+' Location Report '+ date,
                className: 'csvButton hidden',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4 ]
                }
            },
            {
                extend:    'pdfHtml5',
                title: $('#company_name').val()+' '+ $('#container_singular').val()+' Location Report '+ date,
                className: 'pdfButton hidden',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4 ]
                }
            },
            {
                extend: 'print',
                title: $('#company_name').val()+' '+ $('#container_singular').val()+' Location Report '+ date,
                className: 'printButton hidden',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4 ]
                },
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/reports/ajax/list_customer_container_location_report.ajax.php",
        "aoColumns": [
            { "bSortable": true, "className":'details-control',  "data": null, "defaultContent": ''},
            { "bSortable": true, className: 'text-center'},
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": false},
            { "bSortable": false}
        ],
        stateSave: true,
      stateDuration: 0
    });

    $('#locationReportTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });


    $('#locationReportTbl tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
         console.log( table.row(tr).data()[1] );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(table.row(tr).data()[2], row.data()) ).show();
            tr.addClass('shown');
        }
    } );
}


/* Formatting function for row details - modify as you need */
function format (customer_id, data ) {
    var table_name = '';
     $.ajax({
        type: 'GET',
        url: "/modules/reports/ajax/get_customers_containers_collapse.ajax.php?customer_id="+ encodeURIComponent(customer_id),
        async: false,
        success: function (info) {
            console.log(info);
            var myObject = eval('(' + info + ')');
            table_name +='<table class="table table-striped table-bordered table-hover" width="100%">'+
                '<thead>'+
                '<tr>'+
                '<th> Serial Number</th>'+
                '<th>Bar Code</th>'+
                '<th>Delivered</th>'+
                '<th>Type </th>'+
                '<th>Stage</th>'+
                '<th>Contents</th>'+
                '<th>Capacity</th>'+
                '</tr>'+
                '</thead>'+
                '<tr>'+
                '<tbody>';
            for (i in myObject)
            {
                if(myObject[i]["type_name"] == null){
                    var typename = ''
                }
                else{
                typename = myObject[i]["type_name"];
                }

                if (myObject[i]["delivery_date"] == null) {
                    var delivery_date = '-';
                } else {
                    var delivery_date = myObject[i]["delivery_date"];
                }

                table_name += '<tr>'+
                    '<td>'+myObject[i]["container_serial_number"]+'</td>'+
                    '<td>'+myObject[i]["container_bar_code"]+'</td>'+
                    '<td>'+delivery_date+'</td>'+
                    '<td>'+myObject[i]["container_type"]+'</td>'+
                    '<td>'+myObject[i]["stage_name"]+'</td>'+
                    '<td>'+typename+'</td>'+
                    '<td>'+myObject[i]["container_capacity"]+myObject[i]["container_capacity_type"]+'</td>'+
                    '</tr>';
            }
            table_name +=  '</tbody>'+
                            '</table>';
            // `data` is the original data object for the row
        }
    });
return table_name;

}