
$(document).ready(function() {
    drawTable();
});


function drawTable() {
    inactiveTable = $('#ContainerTbl').DataTable({
        "processing": true,
        "serverSide": true,
        destroy: true,
        ajax: '/modules/customer_management/ajax/customer_container_report.ajax.php?customerID=' + $('#customerID').val(),
        "aoColumns": [
            null,
            null,
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false }
        ],
        stateSave: true,
        stateDuration: 0
    });
}