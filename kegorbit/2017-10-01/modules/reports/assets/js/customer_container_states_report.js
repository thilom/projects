$(document).ready(function () {
    create_table();

    $('#copy').click(function(){
        $(".copyButton").trigger('click');
    });

    $('#excel').click(function(){
        $(".excelButton").trigger('click');
    });
    $('#csv').click(function(){
        $(".csvButton").trigger('click');
    });
    $('#pdf').click(function(){
        $(".pdfButton").trigger('click');
    });
    $('#print').click(function(){
        $(".printButton").trigger('click');
    });

});

function create_table() {
    var date = moment().format('YYYY-MM-DD h-mm-ss');
    var active = "Y";
    var table = $('#statesReportTbl').DataTable({
        dom: 'Blfrtip',
        buttons: [
            {
                extend:    'copyHtml5',
                className: 'copyButton hidden',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5,6,7 ]
                }
            },
            {
                extend:    'excelHtml5',
                title: $('#company_name').val()+' '+ $('#state_singular').val()+'  Report '+ date,
                className: 'excelButton hidden',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5,6,7 ]
                }
            },
            {
                extend:    'csvHtml5',
                title: $('#company_name').val()+' '+ $('#state_singular').val()+'  Report '+ date,
                className: 'csvButton hidden',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5,6,7 ]
                }
            },
            {
                extend:    'pdfHtml5',
                title: $('#company_name').val()+' '+ $('#state_singular').val()+'  Report '+ date,
                className: 'pdfButton hidden',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5,6,7 ]
                }
            },
            {
                extend: 'print',
                title: $('#company_name').val()+' '+ $('#state_singular').val()+'  Report '+ date,
                className: 'printButton hidden',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5,6,7 ]
                },
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        "bProcessing": true,
        "bDestroy": true,
        "sAjaxSource": "/modules/reports/ajax/list_customer_container_states_report.ajax.php?active="+active+"&state_id="+$('#state_id').val(),
        "aoColumns": [
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": true},
            { "bSortable": true, className: 'text-center'},
            { "bSortable": true},
            { "bSortable": false, className: 'text-center'}
        ],
        stateSave: true,
        stateDuration: 0
    });

    $('#statesReportTbl').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');

        var group_add_on = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input-group-addon');
        //search_input.attr('placeholder', 'Search');
        group_add_on.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
    });
}
