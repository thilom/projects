
$(document).ready(function() {
    drawTable();
    $('#copy').click(function(){
        $(".copyButton").trigger('click');
    });

    $('#excel').click(function(){
        $(".excelButton").trigger('click');
    });
    $('#csv').click(function(){
        $(".csvButton").trigger('click');
    });
    $('#pdf').click(function(){
        $(".pdfButton").trigger('click');
    });
    $('#print').click(function(){
        $(".printButton").trigger('click');
    });


});

function drawTable() {
    var date = moment().format('YYYY-MM-DD h-mm-ss');

    $('#usageTbl').DataTable({
        dom: 'Blfrtip',
        buttons: [
            {
                extend:    'copyHtml5',
                className: 'copyButton hidden',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5]
                }
            },
            {
                extend:    'excelHtml5',
                title: $('#company_name').val()+' Company Usage Report '+ date,
                className: 'excelButton hidden',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5]
                }
            },
            {
                extend:    'csvHtml5',
                title: $('#company_name').val()+' Company Usage Report '+ date,
                className: 'csvButton hidden',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4,5 ]
                }
            },
            {
                extend:    'pdfHtml5',
                title: $('#company_name').val()+' Company Usage Report '+ date,
                className: 'pdfButton hidden',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4, 5]
                }
            },
            {
                extend: 'print',
                title: $('#company_name').val()+' Company Usage Report '+ date,
                className: 'printButton hidden',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
        ,
        destroy: true,
        ajax: '/modules/reports/ajax/customer_usage_report.ajax.php',
        "aoColumns": [
            null,
            null,
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false }
        ],
        stateSave: true,
        stateDuration: 0
    });
}
