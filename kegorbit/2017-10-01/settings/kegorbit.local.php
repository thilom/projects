<?php
/**
 * Variables file for Thilo's local server
 *
 * 2016-07-20: Thilo Muller - Created
 */

//Database strings
$db_server = 'localhost';
$db_name = 'kegorbit';
$db_username = 'root';
$db_password = 'root';
//SMTP Details
$smtp['url'] = "mail.palladian-sandbox.co.za";
$smtp['port'] = "25";
$smtp['username'] = "test@palladian-sandbox.co.za";
$smtp['password'] = "test23X";

//Email Settings
$email['from']['email'] = "noreply@kegorbit.com";
$email['from']['name'] = "Kegorbit Support";
