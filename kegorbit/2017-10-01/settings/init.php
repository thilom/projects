<?php
/**
 *
 * 2015-09-07: Thilo Muller       - Created
 */
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/variables.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';

if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/settings/' . $_SERVER['HTTP_HOST'] . '.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/' . $_SERVER['HTTP_HOST'] . '.php';
} else {
//    echo "Settings file not found";
    error_log("Settings file not found ({$_SERVER['HTTP_HOST']} | {$_SERVER['REMOTE_ADDR']})", 0);
}



//Start Session
session_start();

$systemClass = new System();

//Vars
$general = new General();

//Connect to DB - PDO
try {
    $connect = "mysql:host=$db_server;dbname=$db_name";
    $dbCon = new PDO($connect, $db_username, $db_password);
    $dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbCon->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
} catch(PDOException $e) {
    echo 'Failed to connect database';
}

//Setup security functions
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
$security = new Security();

//Added for future use
$GLOBALS['scrum_settings']['date_format'] = '';

//Get Scrum Settings
$scrum_settings = $systemClass->get_scrum_settings();