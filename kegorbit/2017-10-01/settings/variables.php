<?php
/**
 * SOLEX 3 - General settings
 *
 * 2015-09-07: Thilo Muller       - Created
 */

$app_name = 'Keg Orbit';
$server_name = 'Keg Orbit';
$site_name = $server_name; //Deprecated 07-2014
$db_prefix = 'keg';
$server_address = 'kegorbit.com';

//SMTP Details
$smtp['url'] = "";
$smtp['port'] = "";
$smtp['username'] = "";
$smtp['password'] = "";

//Email Settings
$email['from']['email'] = "noreply@kegorbit.com";
$email['from']['name'] = "Keg Orbit Support";

$admin_title = 'Keg Orbit - Keg Management and Tracking';
$admin_logo = '/admin/images/03_solex_medium.png';

$reply_to_name = 'noreply';
$site_email = 'noreply@solexinnovation.com';
$reply_to_email = 'noreply@solexinnovation.com';

/**
 * Where challenge responses (BCC) should be sent to.
 */
//$challenges_email = 'challenges@riissa.co.za';
$challenges_email = 'info@palladianbytes.co.za';
$technology_email = 'info@palladianbytes.co.za';


/**
 * Constants for event types
 */
define('EVENT_TYPE_USER', 1);
define('EVENT_TYPE_CONTENT', 2);
define('EVENT_TYPE_BULKMAIL', 3);
define('EVENT_TYPE_KNOWLEDGE', 4);
define('EVENT_TYPE_SELECTS', 5);
define('EVENT_TYPE_LOCATIONS', 6);
define('EVENT_TYPE_CHALLENGES', 7);
define('EVENT_TYPE_TECHNOLOGY_OFFERS', 8);
define('EVENT_TYPE_MENU', 9);

/**
 * User readable mapping for event types
 */
$event_map = array(	1	=>	'User Management',
    2	=>	'Content Management',
    3 => 'Campaigns Management',
    4 => 'Knowledge Management',
    5 => 'Selects',
    6 => 'Locations',
    7 => 'Challenges',
    8 => 'Technology Offers',
    9 => 'Menus');


/**
 * Constants for member status
 */
define('MEMBER_STATUS_OK', 1);
define('MEMBER_STATUS_SUSPENDED', 2);
define('MEMBER_STATUS_NEW', 3);

/**
 * Hidden areas
 * The page areas that should NOT be highlighted in the editor
 * but should appear in the hidden areas list instead
 */
$hidden_areas = array(	'knowledge_white_papers',
    'knowledge_thought_pieces',
    'knowledge_how_to',
    'knowledge_case_studies',
    'knowledge_articles',
    'knowledge_faq',
    'find_an_expert_side_menu',
    'challenges_side_menu');


