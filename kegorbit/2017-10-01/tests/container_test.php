<?php
/**
 * Insert containers into db
 *
 * 2016-08-14: Thilo Muller - Created
 */


//Set Company
require_once '../settings/init.php';
$company_code = 'kegfactory';
$_SESSION['user']['prefix'] = $company_code;
$_SESSION['user']['id'] = '1';

//Includes

require_once '../modules/container_management/classes/container.class.php';
require_once '../modules/products_management/classes/products.class.php';
require_once '../modules/customer_management/classes/customer.class.php';

//Vars
$containerClass = new Container();
$customerClass = new Customer();
$productClass = new Products();
$no_of_kegs = 1000;
$types = array('Wood - Oak','Stainless Steel','Clear Plastic','Black Plastic');
$capacity = array(20, 30,50, 100);


//Get products
$product_data = $productClass->get_types('');
$products = array();
foreach ($product_data as $pData) {
    $products[] = $pData['type_id'];
}


//Get clients
$client_data = $customerClass->get_customers(true,0, 1000);
$clients = array();
foreach ($client_data AS $cData) {
    $clients[] = $cData['customer_name'];
}


//Add containers
for ($i=0; $i<$no_of_kegs; $i++) {

    //Create serial No
    $data['serial_number'] = str_pad(rand(0, 10000000),8, '0', STR_PAD_LEFT);

    //Create Barcode
    $data['bar_code'] = str_pad(rand(0, 10000000),8, '0', STR_PAD_LEFT);

    //Container Type
    $v = floor(rand(0,3.9));
    $data['type'] = $types[$v];

    //Capacity
    $v = floor(rand(0,3.9));
    $data['capacity'] = $capacity[$v];

    //Capacity Type
    $data['capacity_type'] = 'L';

    //Current State
    $state = floor(rand(1,5.9));
    $data['current_state'] = $state;

    //Current Stage
    switch ($state) {
        case 5:
            $stage = floor(rand(1,5.9));
            $data['current_stage'] = $stage;

            switch ($stage) {
                case 3: //Filled
                    $v = rand(0,count($products)-0.1);
                    $data['contents'] = $products[$v];
                    $data['current_client'] = '';
                    break;
                case 4: //Delivered
                    $v = rand(0,count($clients)-0.1);
                    $data['current_client'] = $clients[$v];
                    $v = rand(0,count($products)-0.1);
                    $data['contents'] = $products[$v];
                    break;
                default:
                    $data['contents'] = 0;
                    $data['current_client'] = '';
            }
            break;
        default:
            $stage = 1;
            $data['current_stage'] = $stage;
    }

    //Date
    $data['creation_date'] = date('Y-m-d');

    //Created By


    //Insert Container
    $containerClass->save_new_container($data);
}

