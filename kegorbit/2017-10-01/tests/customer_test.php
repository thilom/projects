<?php
/**
 * Create customers
 *
 * 2016-08-14: Thilo Muller - Created
 */


//Set Company
require_once '../settings/init.php';
$company_code = 'kegfactory';
$_SESSION['user']['prefix'] = $company_code;
$_SESSION['user']['id'] = '1';

//Includes
require_once '../modules/customer_management/classes/customer.class.php';

//Vars
$customerClass = new Customer();
$no_of_customers = 40;


//Get test data
$statement = "SELECT customer_name, customer_contact, customer_contact_number, customer_contact_email, inactive
                FROM customer_test_data
                ORDER BY RAND()
                LIMIT $no_of_customers";
$sql_select = $GLOBALS['dbCon']->prepare($statement);
$sql_select->bindParam(':area_id', $area_id);
$sql_select->execute();
$sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
$sql_select->closeCursor();

//Insert data
foreach ($sql_result as $cData) {

    $statement = "INSERt INTO keg_{$company_code}_customers
                      (customer_name, customer_contact, customer_contact_number, customer_contact_email, inactive, created_by)
                    VALUES
                      ('{$cData['customer_name']}','{$cData['customer_contact']}','{$cData['customer_contact_number']}','{$cData['customer_contact_email']}','{$cData['inactive']}',1)";
    $sql_insert = $GLOBALS['dbCon']->prepare($statement);
    $sql_insert->bindParam(':area_id', $area_id);
    $sql_insert->execute();
    $sql_insert->closeCursor();


}