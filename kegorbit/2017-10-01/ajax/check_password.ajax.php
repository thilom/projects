<?php
/**
 * Check if a password has at least one number and one letter in it
 * 2014/05/07 Sigal Zahavi - Created
 *
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Vars
$password = $_GET['password'];

if (preg_match('/[A-Za-z]+[0-9]+/', $password))
    echo '1';
else
    echo '0';
