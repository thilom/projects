<?php

/**
 * save new password
 * 2016-08-10: Musa Khulu -  Created
 *
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';


//Vars
$securityClass = new Security();
$userClass = new User();

//Reset
$results = $userClass->save_new_password($_POST);

require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
$accountClass = new Accounts();
$accountClass->insert_user_activity("Account: '{$results['username']}' Logged in for the first time.");
echo $results['message'];
