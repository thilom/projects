<?php
/**
 * Returns the server time
 *
 * 2016-09-30: Thilo Muller - Created
 */

echo date('H:i:s');