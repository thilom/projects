<?php

/**
 * save new password
 * 2016-02-11: Musa Khulu -  Created
 *
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';


//Vars
$securityClass = new Security();
$userClass = new User();

//Reset
$results = $userClass->save_new_password($_POST);
echo $results['message'];
