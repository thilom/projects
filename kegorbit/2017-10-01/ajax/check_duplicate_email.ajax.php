<?php
/**
 * Gets a username and checkes if it already exists AJAX
 * 2014/04/01 Sigal Zahavi - Created
 *
 * @copyright Palladian Bytes (Pty) Ltd
 */
//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';

//Vars
$userClass = new User();
$username = $_GET['email'];
$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : 0;

$duplicate = $userClass->check_duplicate_username($username, $user_id);

echo $duplicate;
