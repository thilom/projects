<?php
/**
 * Created by PhpStorm.
 * User: khulu
 * Date: 2016-08-18
 * Time: 11:15 AM
 * 2016-08-18: Musa Khulu - Created
 */


//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/helpers/swiftmailer/lib/swift_required.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$accountClass = new Accounts();
$appClass = new Application();

//Reset
$results = $userClass->get_user_by_email($_POST['email']);
if(!($results)){
    echo 'Your Details do not match any of our records. Please Contact your System Administrator';
}
else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
    $appClass = new Application();
    $settingsClass = new Settings();
    //Get current settings
    $settings = $appClass->get_settings();
    $maintenance_data = $settingsClass->get_maintenance();
    $now = date('Y-m-d h:i:s');
    //activate maintenance
    if (isset($settings['activate_maintenance']) && $settings['activate_maintenance'] == '1' && $results['user_type'] != '1' && $maintenance_data['maintenance_commence'] <= $now) {
        echo 'Password cannot be retrieved. Maintenance in progress!!!';
        die;
    }

$user_id = $results['user_id'];
//get user details
$company_user = $accountClass->get_account($user_id);
$code = sha1(mt_rand(10000,99999).time().$company_user['user_email_address']);
//update activation code
$accountClass->update_user_activation_code($code,$user_id);


//Get SMTP Details
$settings = $appClass->get_settings();
$smtp_url = $settings['smtp_url'];
$smtp_port = $settings['smtp_port'];
$smtp_username = $settings['smtp_username'];
$smtp_password = $settings['smtp_password'];

// Create the mail transport configuration
$transport = Swift_SmtpTransport::newInstance($smtp_url, $smtp_port)->setUsername($smtp_username)->setPassword($smtp_password);
//Supposed to allow local domain sending to work from what I read
//$transport->setLocalDomain('[127.0.0.1]');

// Create the message
$message = Swift_Message::newInstance($transport);

$message->setTo($company_user['user_email_address']);
$message->setSubject("Change to your Keg Orbit Password ");

$body = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/company_management/html/email_company_template/lost_password.html');


$email_name = array('contact_name' => $company_user['user_full_name']);
$body = $systemClass->merge_data($body, $email_name);

$email_data['reset_link'] = "<a href='http://{$_SERVER['HTTP_HOST']}/index.php?code=$code&u={$user_id}'>{$_SERVER['HTTP_HOST']}/index.php?code=$code&u={$user_id}</a>";
$body = $systemClass->merge_data($body, $email_data);

//
$server_details = array('server' => $_SERVER['HTTP_HOST'], 'timestamp' => date('d-m-Y H:i:s'));
$body = $systemClass->merge_data($body, $server_details);

$message->setBody($body, 'text/html');
    if(!empty($company_user['company_email'])){
        $message->setFrom(array($company_user['company_email'] => $company_user['company_name']));
    }
    else{
        $message->setFrom(array($GLOBALS['email']['from']['email'] => $GLOBALS['email']['from']['name']));
    }
// Send the email
$mailer = Swift_Mailer::newInstance($transport);
$mailer->send($message);

    echo 'Success: An email has been sent to the email provided please check your email for further instructions.';
}
