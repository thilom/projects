<?php

/**
 *
 *  2016-01-21: Musa Khulu - Created
 *
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/account_management/classes/account.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';


//Vars
$securityClass = new User();
$accountClass = new Accounts();
$applicationClass = new Application();

//CAPTCHA
$settings = $applicationClass->get_settings();
if (isset($settings['login_captcha']) && $settings['login_captcha'] == 1) {
    $captcha_vars = "secret={$settings['captcha_secret']}";
    $captcha_vars .= "&response={$_POST['g-recaptcha-response']}";
    $captcha_vars .= "&remoteip={$_SERVER["REMOTE_ADDR"]}";

    $captcha = curl_init();
    curl_setopt($captcha,CURLOPT_URL,'https://www.google.com/recaptcha/api/siteverify');
    curl_setopt($captcha,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($captcha,CURLOPT_POST,true);
    curl_setopt($captcha,CURLOPT_POSTFIELDS,$captcha_vars);
    $captcha_result = curl_exec($captcha);
    curl_close($captcha);
    $captcha_result = json_decode($captcha_result, true);
    if ($captcha_result['success'] == false) {
        echo "Login Error: Please check that all fields are completed correctly and try again";
        die();
    }
}

//Login
    $login_result = $securityClass->login($_POST['email'], $_POST['password']);
    if ($login_result['status'] === false) {

        if (isset($_POST['source']) && $_POST['source'] == 'idleTimer') {
            $error_message = 'Incorrect Password';
        } else {
            $error_message = $login_result['message'];
        }
        echo $error_message;
    } else {
        echo $login_result['message'];

        //Add to user activity
        $accountClass->insert_user_activity("Successfully Logged in");
}
