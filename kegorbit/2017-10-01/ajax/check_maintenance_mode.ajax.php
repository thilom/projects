<?php
/**
 * check if mantenance mode is on
 * 2016-09-09  Musa Khulu - Created
 *
 * @copyright Palladian Bytes (Pty) Ltd
 */
//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();
$appClass = new Application();


//Get current settings
$settings = $appClass->get_settings();


//activate maintenance
if (isset($settings['activate_maintenance']) && $settings['activate_maintenance'] == '1') {
    //get maintenance details
    $maintenance = $settingsClass->get_maintenance();
    echo json_encode($maintenance);
}
else {
    echo 'none';
}

