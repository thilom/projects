<?php
/**
 * Monitor maintenance mode for logged in users
 *
 * 2016-09-30: Thilo Muller - Created
 *
 * @copyright Palladian Bytes (Pty) Ltd
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/general.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/settings.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/settings_management/classes/application.class.php';

//Vars
$systemClass = new System();
$securityClass = new Security();
$userClass = new User();
$settingsClass = new Settings();
$appClass = new Application();

//Get current settings
$settings = $appClass->get_settings();



//Get user details
$user_data = $userClass->get_user($_SESSION['user']['id']);

//activate maintenance
if (isset($settings['activate_maintenance']) && $settings['activate_maintenance'] == '1' && $user_data['user_type'] != '1' && isset($_SESSION['user']['id'])) {
    $maintenance_data = $settingsClass->get_maintenance();
    $now = date('Y-m-d H:i:s');

    //activate maintenance
    if (isset($settings['activate_maintenance']) && $settings['activate_maintenance'] == '1' && $maintenance_data['maintenance_commence'] <= $now) {
        echo "1";
    }


}


