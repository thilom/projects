<?php
/**
 * Add a new user
 * 2016-05-30 : Musa Khulu - Created
 */
//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/security.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/system.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/vendor/swiftmailer/lib/swift_required.php';
//vars
$userClass = new User();
$securityClass = new Security();
$systemClass = new System();

//Save
$user_id = $userClass->create_user($_POST);

//send welcome email
if($user_id != 0 ){
    // Create the mail transport configuration
    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')->setUsername('sigaldev@gmail.com')->setPassword('BarakZahav');
    //Supposed to allow local domain sending to work from what I read
    $transport->setLocalDomain('[127.0.0.1]');

    // Create the message
    $message = Swift_Message::newInstance($transport);

    $message->setTo($_POST['email']);
    $message->setSubject("Contry Travel Welcome Email");

    $body = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/html/new_user_email.html');

    $table_details = array('email' => $_POST['email'], 'firstname' => $_POST['name'], 'lastname' => $_POST['surname']);
    $body = $systemClass->merge_data($body, $table_details);


    $email_name = array('contact_name' => $_POST['name'], 'contact_surname' => $_POST['surname']);
    $body = $systemClass->merge_data($body, $email_name);


//
    $server_details = array('server' => $_SERVER['HTTP_HOST'], 'timestamp' => date('d-m-Y H:i:s'));
    $body = $systemClass->merge_data($body, $server_details);

    $message->setBody($body, 'text/html');
    $message->setFrom(array($GLOBALS['email']['from']['email'] => $GLOBALS['email']['from']['name']));
    // Send the email
    $mailer = Swift_Mailer::newInstance($transport);
    $mailer->send($message);
}
echo $user_id;
