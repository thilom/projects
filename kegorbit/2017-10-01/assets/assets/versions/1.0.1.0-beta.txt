- Fixed tab order in container editor.

- Updated container type to be a text field instead of a dropdown.

- Added company request when selecting company roles in account manager.

- Removed development popup when selecting a container stage.

- Fixed various places where terminology is not implemented.

- Removed user access to edit their own access details.

- Fixed company management module available to non application administrators.

- Updated stage and state dashboard blocks to show information even if none has been set up.

- Fixed container, user and device limit check.

- Added auto populating of default user fields when creating a company.

- Fixed various spelling errors.

