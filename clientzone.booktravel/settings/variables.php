<?php
//Constants
if (!defined('SITE_ROOT')) define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT']);
if (!defined('PROCESS')) define('PROCESS', SITE_ROOT . '/shared/page_process.php');

//General Settings
$dir = SITE_ROOT . '/modules/';
$content_types = array();
$sName = $_SERVER['SERVER_NAME'];
$version = '201004200';
$public_version = "1.0.3";

//Error handling values
$reporting_level = E_ALL; //One of the standard PHP error level constants

//date and Time settings
$timezone = 'Africa/Johannesburg'; //Only useful on PHP version 5.1.0 or greater
						
// use for items seldom used
$site_name="ETInfo";
$site_address="http://" . $_SERVER['SERVER_NAME'];

// Admin
$admin_title="AA Travel Admin";

//FTP Connections
$ftp = array();
$ftp['www.aatravel.co.za']['username'] = 'aatrae';
$ftp['www.aatravel.co.za']['password'] = '88trav3l';
$ftp['www.aatravel.co.za']['root'] = '/public_html/';

//$ftp['www.essentialtravelinfo.com']['username'] = '';
//$ftp['www.essentialtravelinfo.com']['password'] = '';
//$ftp['www.essentialtravelinfo.com']['root'] = '';

$months = array(1=>"January","February","March","April","May","June","July","August","September","October","November","December");

?>