<?php
/**
 * DBWeb4 setup file
 *
 * Sets variables and constants that are required across the whole application.
 * @package DBWeb4
 * @TODO This file should be broken up into two files. One for setting variables and the other for non-user definable setup. - Thilo
 */

//General Settings
define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT']);
$dir = SITE_ROOT . '/modules/';
$content_types = array();
$sName = $_SERVER['SERVER_NAME'];
$version = '200809200';
$public_version = "1.0.1";

// default database connection values
$db_server = "dedi2042.nur4.host-h.net";
$db_name = "aatravel_dev";
$db_username = "aatrae_5";
$db_password = "zK5yVN28";

//Error handling values
$reporting_level = E_ALL; //One of the standard PHP error level constants

//date and Time settings
$timezone = 'Africa/Johannesburg'; //Only useful on PHP version 5.1.0 or greater

//Constants
define('PROCESS', $_SERVER['DOCUMENT_ROOT'] . '/shared/page_process.php');

//Permission settings (Directory=>permission)
$permissions = array(
						'/i'=>0777,
						'/i/i'=>0777,
						'/i/fluid'=>0777,
						'/modules/chart/temp'=>0777,
						'/modules/gallery/ftp'=>0777,
						'/modules/gallery/img'=>0777,
						'/modules/gallery/img/1'=>0777,
						'/modules/gallery/img/2'=>0777,
						'/modules/gallery/img/3'=>0777,
						'/modules/gallery/img/4'=>0777,
						'/W/index.php'=>0777
						);

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/exception.class.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/sql.class.php';
						
//append permissions
include_once $_SERVER['DOCUMENT_ROOT']."/shared/directory_files.php";
$append=array();
$append[$_SERVER['DOCUMENT_ROOT']."/i"]="/i/";
$append[$_SERVER['DOCUMENT_ROOT']."/i/i"]="/i/i/";
$append[$_SERVER['DOCUMENT_ROOT']."/i/templates"]="/i/templates/";
$append[$_SERVER['DOCUMENT_ROOT']."/i/templates/backup"]="/i/templates/backup/";
$append[$_SERVER['DOCUMENT_ROOT']."/i/fluid"]="/i/fluid/";
if(is_dir($_SERVER['DOCUMENT_ROOT']."/modules/gallery/ftp"))
	$append[$_SERVER['DOCUMENT_ROOT']."/modules/gallery/ftp"]="/modules/gallery/ftp/";
foreach($append as $a1 => $a2)
{
	getFiles($a1);
	if(count($filelist))
	{
		foreach($filelist as $b1 => $b2)
			$permissions[$a2.$b2]=0777;
	}
}
//


//Connect to Database - Traditional
if (($db_con = @mysql_connect($db_server, $db_username, $db_password)) === FALSE) die("<span style='font-weight: bold; color: red'>Failed to connect to Database (Basic)</span>");
$db_con;
mysql_select_db($db_name);

//Connect to Database - Mysqli
try {
	$dbCon = @new aa_sql($db_server, $db_username, $db_password, $db_name);
} catch (SQLConnectException   $e) {
	die("<span style='font-weight: bold; color: red'>Failed to connect to Database (Advanced)</span>");
}

//Initialize Log Tool
include_once SITE_ROOT . '/shared/logging.class.php';
$log_tool = new logging();

//Check if modularus is installed
$SQL_statement = "SHOW TABLES FROM  $db_name LIKE 'dbweb_pages'";
$tables = mysql_query($SQL_statement);
echo mysql_error();
if (mysql_num_rows($tables) == 0) {
	include '../admin/install.php';
}


//Get settings
$SQL_statement = "SELECT tag, value FROM dbweb_settings";
$SQL_set = mysql_query($SQL_statement);
while($set = mysql_fetch_array($SQL_set)) {
	$settings[$set['tag']] = $set['value'];
}


//Setup Error Handling
if ($settings['error_reporting'] == 'off') {
	error_reporting(0);
} else {
	error_reporting($reporting_level);
}

//Get settings
$SQL_statement = "SELECT tag, value FROM dbweb_settings";
$SQL_setting = mysql_query($SQL_statement);
while ($set = mysql_fetch_array($SQL_setting)) {
	$settings[$set['tag']] = $set['value'];
}
define('__SNAME__', $settings['rCode']);

//Itterate thru directories looking for module.ini.php
$setting_tags = array('module_name', 'module_icon', 'module_link', 'window_width', 'window_height', 'window_xposition', 'window_yposition', 'window_attribute');
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if (is_dir($dir . $file)) {
            	if ($file == '.' || $file == '..') continue;
            		if (!is_file($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
                        include_once($dir.$file.'/module.ini.php');
                        foreach ($setting_tags as $k=>$v) {
                        	if (isset($$v)) {
                        		$module_settings[$file][$v] = $$v;
                        	}
                        }
            		if (!empty($type)) {
            			foreach ($type as $tab_name=>$module) {
            				$content_types[$tab_name] = $module;
            			}
            		}
            }
        }
        closedir($dh);
    }
}

//Set time zone
if (version_compare('5.1.0', PHP_VERSION) < 0) {
	date_default_timezone_set($timezone);
}

//Start Session
$session_id = session_id();
if (empty($session_id)) {
	session_name('dbweb4x');
	ini_set('session.gc_maxlifetime', 30*60);
	session_start();
}

if (!defined('__SNAME__')) define('__SNAME__', $settings['rCode']);

//Permissions
include_once $_SERVER['DOCUMENT_ROOT']."/shared/file_man.class.php";
$fm = new file_man();
//if (!isset($_SESSION['perms_done'])) {
//	foreach ($permissions as $loc=>$perm) {
//		if (is_file($_SERVER['DOCUMENT_ROOT'] . $loc) || is_dir($_SERVER['DOCUMENT_ROOT'] . $loc)) {
//			if ($fm->dbweb_chmod($loc, $perm) === FALSE) trigger_error($fm->get_error(), E_USER_WARNING);
//		}
//	}
//	$_SESSION['perms_done'] = 'ok';
//}

?>