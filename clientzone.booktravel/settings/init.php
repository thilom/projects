<?php
/**
 * DBWeb4 setup file
 *
 * Sets variables and constants that are required across the whole application.
 * @package DBWeb4
 * @TODO This file should be broken up into two files. One for setting variables and the other for non-user definable setup. - Thilo
 */

define('SITE_ROOT', '/var/www/clients/client1/web4/web');

//Includes
include_once 'variables.php';
include_once 'database.php';
include_once 'google.php';
include_once SITE_ROOT . '/shared/exception.class.php';
include_once SITE_ROOT . '/shared/sql.class.php';
require_once SITE_ROOT . '/shared/security.class.php';
						

//Instantiate Security
$security = new security();

//Connect to Database - Traditional
if (($db_con = @mysql_connect($db['aa_travel']['db_server'], $db['aa_travel']['db_username'], $db['aa_travel']['db_password'])) === FALSE) die("<span style='font-weight: bold; color: red'>Failed to connect to Database (Basic)</span>");
$db_con;
mysql_select_db($db['aa_travel']['db_name']);

//Connect to AA Travel Database - Mysqli
try {
	$dbCon = @new aa_sql($db['aa_travel']['db_server'], $db['aa_travel']['db_username'], $db['aa_travel']['db_password'], $db['aa_travel']['db_name']);
} catch (SQLConnectException   $e) {
	die("<span style='font-weight: bold; color: red'>Failed to connect to AA Travel Database (Advanced) </span>");
}

//Connect to AA Travel Database - Mysqli
//try {
//	$dbCon_shell = @new aa_sql($db['shell']['db_server'], $db['shell']['db_username'], $db['shell']['db_password'], $db['shell']['db_name']);
//} catch (SQLConnectException   $e) {
//	die("<span style='font-weight: bold; color: red'>Failed to connect to Shell Database (Advanced)</span>");
//}

//Initialize Log Tool
include_once SITE_ROOT . '/shared/logging.class.php';
$log_tool = new logging();

//Get settings
$SQL_statement = "SELECT tag, value FROM dbweb_settings";
$SQL_set = mysql_query($SQL_statement);
while($set = mysql_fetch_array($SQL_set)) {
	$settings[$set['tag']] = $set['value'];
}

//Get settings
$SQL_statement = "SELECT tag, value FROM dbweb_settings";
$SQL_setting = mysql_query($SQL_statement);
while ($set = mysql_fetch_array($SQL_setting)) {
	$settings[$set['tag']] = $set['value'];
}
define('__SNAME__', $settings['rCode']);

if (isset($_SERVER['HTTP_HOST'])) {
//Itterate thru directories looking for module.ini.php
$setting_tags = array('module_name', 'module_icon', 'module_link', 'window_width', 'window_height', 'window_xposition', 'window_yposition', 'window_attribute');
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if (is_dir($dir . $file)) {
            	if ($file == '.' || $file == '..') continue;
            		if (!is_file($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
                        include_once($dir.$file.'/module.ini.php');
                        foreach ($setting_tags as $k=>$v) {
                        	if (isset($$v)) {
                        		$module_settings[$file][$v] = $$v;
                        	}
                        }
//            		if (!empty($type) ) {
//            			foreach ($type as $tab_name=>$module) {
//            				$content_types[$tab_name] = $module;
//            			}
//            		}
            }
        }
        closedir($dh);
    }
}
}
//Set time zone
if (version_compare('5.1.0', PHP_VERSION) < 0) {
	date_default_timezone_set($timezone);
}

//Start Session
$session_id = session_id();
if (empty($session_id)) {
	session_name('dbweb4x');
	ini_set('session.gc_maxlifetime', 30*60);
	session_start();
}

if (!defined('__SNAME__')) define('__SNAME__', $settings['rCode']);

//Permissions
include_once SITE_ROOT."/shared/file_man.class.php";
$fm = new file_man();

?>
