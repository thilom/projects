<?php
//VArs
$c = 0;

//init
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Prepare statement
$statement = "UPDATE nse_bulk_emails SET email_content=? WHERE mail_id=?";
$sql_update = $GLOBALS['dbCon']->prepare($statement);


$statement = "SELECT mail_id, email_content FROM nse_bulk_emails WHERE send_date IS NULL";
$sql_get = $GLOBALS['dbCon']->prepare($statement);
$sql_get->execute();
$sql_get->store_result();
$sql_get->bind_result($mail_id, $email_content);
while ($sql_get->fetch()) {
	$email_content = str_replace('Friday night', 'the 2010 Worldcup draw', $email_content);
	
	$sql_update->bind_param('ss', $email_content, $mail_id);
	$sql_update->execute();
}
$sql_get->free_result();
$sql_get->close();

?>