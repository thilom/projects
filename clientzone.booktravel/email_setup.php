<?php
/**
 *
 */

//Vars
$error_list = '';
$a = 1;

//init
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Clases
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/tcrypt.class.php';
$tc = new tcrypt();

//Get template
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/email_templates/new_admin_access.html');

//Prepare Statement - estab check
$statement = "SELECT a.establishment_code, b.establishment_name
				FROM nse_user_establishments AS a
				JOIN nse_establishment AS b ON a.establishment_code = b.establishment_code
				WHERE a.user_id=? && b.aa_estab='Y'";
$sql_estab_check = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - Add to email list
$statement = "INSERT INTO nse_bulk_emails (establishment_code, email_address, email_subject, email_content, create_date, email_id) VALUES (?,?,?,?,NOW(),'admin_access')";
$sql_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - email check
$statement = "SELECT COUNT(*) FROM nse_bulk_emails WHERE email_address=?";
$sql_email = $GLOBALS['dbCon']->prepare($statement);


//Get list of users
$statement = "SELECT user_id, email, firstname, lastname, new_password FROM nse_user";
$sql_users = $GLOBALS['dbCon']->prepare($statement);
$sql_users->execute();
$sql_users->store_result();
$sql_users->bind_result($user_id, $email, $firstname, $lastname, $encrypted_password);


//$fh = fopen($_SERVER['DOCUMENT_ROOT'] . '/temp/mailings2.csv', 'r');

//while (!feof($fh)) {
while ($sql_users->fetch()) {
	//Vars
	$counter = 0;
	$counter2 = 0;
	$establishment = '';
	$name = '';
	
	//Check if they are attached to an AA establishment
	$sql_estab_check->bind_param('i', $user_id);
	$sql_estab_check->execute();
	$sql_estab_check->store_result();
	$sql_estab_check->bind_result($establishment, $name);
	$sql_estab_check->fetch();
	
	if ($sql_estab_check->num_rows == 0) {
		//echo "$name :: $email - No Establishment <br>";
	} else {
		//Check if email already exists
		$sql_email->bind_param('s', $email);
		$sql_email->execute();
		$sql_email->store_result();
		$sql_email->bind_result($counter2);
		$sql_email->fetch();
		
		if ($counter2 > 0) {
			echo "$name :: $email - Already Sent <br>";
			continue;
		}
		
		//decode password
		$decoded_password = $tc->decrypt($encrypted_password);
		$subject = 'You can now update your own information on www.aatravel.co.za';
		
		//Replace Tags
		$body = str_replace('<!-- salutation -->', "$firstname $lastname", $template);
		$body = str_replace('<!-- username -->', $email, $body);
		$body = str_replace('<!-- password -->', $decoded_password, $body);
		
		//Add to DB
		$sql_insert->bind_param('ssss', $establishment_code, $email, $subject, $body);
		$sql_insert->execute();
		
		echo "$a - $name :: $email - OK <br>";
		$a++;
		//echo $body;
	}

}
$sql_users->free_result();
$sql_users->close();





?>