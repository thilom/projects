<?php

//Includes
require_once '../../settings/init.php';
$category_id = $_GET['c'];

//Vars
$category_link = '';
$category_name = '';
$category_full_id = '';
$country_id = 0;
$country_name = '';
$province_id = 0;
$province_name = '';
$town_id = 0;
$suburb_id = 0;
$suburb_name = '';
$borough_id = 0;
$count = 0;
$line = 0;
$town_name = '';

//Prepare Statement - Insert Export Line
$statement = "INSERT INTO nse_export (level, parent_id, base_dir, current_id, trail, region_id, country_id, province_id, town_id, suburb_id, borough_id, export_category) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
//$sql_insert = $GLOBALS['dbCon_shell']->prepare($statement);

//Prepare Statement - Check Export Line
$statement = "SELECT COUNT(*) FROM nse_export WHERE level=? && current_id=? && export_category=?";
//$sql_check = $GLOBALS['dbCon_shell']->prepare($statement);

//Prepare Statement - Town in Province
$statement = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=?";
$sql_town_province = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - Town in Country
$statement = "SELECT town_id, town_name FROM nse_location_town WHERE country_id=?";
$sql_town_country = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Suburbs
$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=?";
$sql_suburb = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - Countries
$statement = "SELECT a.country_id, b.country_name
				FROM nse_location_region_towns AS a
				JOIN nse_location_country_lang AS b ON a.country_id=b.country_id
				WHERE region_id='61'";
$sql_countries = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - Provinces
$statement = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=?";
$sql_province = $GLOBALS['dbCon']->prepare($statement);
//Page Categories
$statement = "SELECT dir_link, title2, restype FROM nse_export_data WHERE identifier=?";
$sql_categories = $GLOBALS['dbCon']->prepare($statement);
$sql_categories->bind_param('s', $category_id);
$sql_categories->execute();
$sql_categories->store_result();
$sql_categories->bind_result($category_link, $category_name, $category_full_id);
while ($sql_categories->fetch()) {
	echo "<div style='font-weight: bold; font-size: 20pt'>$line: Category: $category_name </div>";
	$line++;
	$base_dir = "/accommodation/$category_link/southern_africa";
	$breadcrumb_country = "<a href='/index.php' style='color: black'>AA Travel Guides </a> ";
	$breadcrumb_country .= "&#187; <a href='/accommodation/$category_link/index.html' style='color: black'>$category_name	</a> ";
	$breadcrumb_country .= "&#187; <a href='/accommodation/$category_link/southern_africa/index.html' style='color: black'>Southern Africa</a>";
	$sql_countries->execute();
	$sql_countries->store_result();
	$sql_countries->bind_result($country_id, $country_name);
	echo $sql_countries->num_rows;
	while ($sql_countries->fetch()) {
		$level = "country";
		$region_id = '61';
		$sql_check->bind_param('sss', $level, $country_id, $category_full_id);
		$sql_check->execute();
		$sql_check->store_result();
		$sql_check->bind_result($count);
		$sql_check->fetch();
		$sql_check->free_result();
//		$establishment_count = category_count($category_id, 'country', $country_id);
		//echo "<div style='margin-left: 0px'>$line: Country: $country_name ($establishment_count)</div>";
		$line++;
//		if ($establishment_count != 0) {
			if ($count == 0) {
				$sql_insert->bind_param('ssssssssssss',$level, $region_id, $base_dir, $country_id, $breadcrumb_country, $region_id, $country_id, $province_id, $town_id, $suburb_id, $borough_id, $category_full_id);
				$sql_insert->execute();
			}
			$sql_province->bind_param('s', $country_id);
			$sql_province->execute();
			$sql_province->store_result();
			$sql_province->bind_result($province_id, $province_name);
			if ($sql_province->num_rows != 0) {
				while ($sql_province->fetch()) {
					$breadcrumb_province = $breadcrumb_country;
					$breadcrumb_province .= "&#187; <a href='/accommodation/$category_link/southern_africa/";
					$breadcrumb_province .= str_replace(' ', '_', strtolower($country_name));
					$breadcrumb_province .= "/index.html' style='color: black'>$country_name</a>";
					$province_base_dir = $base_dir . '/' . str_replace(' ', '_', strtolower($country_name));
//					$establishment_count = category_count($category_id, 'province', $province_id);
					//echo "<div style='margin-left: 20px'>$line: Province: $province_name ($establishment_count)</div>";
					$line++;
//					if ($establishment_count != 0) {
						$level = 'province';
						$sql_check->bind_param('sss', $level, $province_id, $category_full_id);
						$sql_check->execute();
						$sql_check->store_result();
						$sql_check->bind_result($count);
						$sql_check->fetch();
						$sql_check->free_result();
						if ($count == 0) {
							$sql_insert->bind_param('ssssssssssss',$level, $country_id, $province_base_dir, $province_id, $breadcrumb_province, $region_id, $country_id, $province_id, $town_id, $suburb_id, $borough_id, $category_full_id);
							$sql_insert->execute();
						}
						$sql_town_province->bind_param('s', $province_id);
						$sql_town_province->execute();
						$sql_town_province->store_result();
						$sql_town_province->bind_result($town_id, $town_name);
						while ($sql_town_province->fetch()) {
							$breadcrumb_town = $breadcrumb_province;
							$breadcrumb_town .= "&#187; <a href='/accommodation/$category_link/southern_africa/";
							$breadcrumb_town .= str_replace(' ', '_', strtolower($country_name));
							$breadcrumb_town .= "/";
							$breadcrumb_town .= str_replace(' ', '_', strtolower($province_name));
							$breadcrumb_town .= "/index.html' style='color: black'>$province_name</a>";
							$town_base_dir = $province_base_dir . '/' . str_replace(' ', '_', strtolower($province_name));
//							$establishment_count = category_count($category_id, 'town', $town_id);
							//echo "<div style='margin-left: 40px'>$line: Town ($town_id): $town_name ($establishment_count)</div>";
							$line++;
//							if ($establishment_count != 0) {
								$level = 'town';
								$sql_check->bind_param('sss', $level, $town_id, $category_full_id);
								$sql_check->execute();
								$sql_check->store_result();
								$sql_check->bind_result($count);
								$sql_check->fetch();
								$sql_check->free_result();
								if ($count == 0) {
									$sql_insert->bind_param('ssssssssssss',$level, $province_id, $town_base_dir, $town_id, $breadcrumb_town, $region_id, $country_id, $province_id, $town_id, $suburb_id, $borough_id, $category_full_id);
									$sql_insert->execute();
								}
								$sql_suburb->bind_param('s', $town_id);
								$sql_suburb->execute();
								$sql_suburb->store_result();
								$sql_suburb->bind_result($suburb_id, $suburb_name);
								while ($sql_suburb->fetch()) {
									$breadcrumb_suburb = $breadcrumb_town;
									$breadcrumb_suburb .= "&#187; <a href='/accommodation/$category_link/southern_africa/";
									$breadcrumb_suburb .= str_replace(' ', '_', strtolower($country_name));
									$breadcrumb_suburb .= "/";
									$breadcrumb_suburb .= str_replace(' ', '_', strtolower($province_name));
									$breadcrumb_suburb .= "/";
									$tname = str_replace(array("'",'(',')','"','=','+','[',']',',','/','\\','&'), '', $town_name);
									$breadcrumb_suburb .= str_replace(array(' ',','),array('_',''), strtolower($tname));
									$breadcrumb_suburb .= "/index.html' style='color: black'>$town_name</a>";
									
//									$establishment_count = category_count($category_id, 'suburb', $suburb_id);
									//echo "<div style='margin-left: 60px'>$line: Suburb ($suburb_id): $suburb_name ($establishment_count)</div>";
									$line++;
//									if ($establishment_count != 0) {
										$level = 'suburb';
										$sql_check->bind_param('sss', $level, $suburb_id, $category_full_id);
										$sql_check->execute();
										$sql_check->store_result();
										$sql_check->bind_result($count);
										$sql_check->fetch();
										$sql_check->free_result();
										if ($count == 0) {
											$sql_insert->bind_param('ssssssssssss',$level, $town_id, $town_base_dir, $suburb_id, $breadcrumb_suburb, $region_id, $country_id, $province_id, $town_id, $suburb_id, $borough_id, $category_full_id);
											$sql_insert->execute();
										}
//									}
								}
								$sql_suburb->free_result();
//							}
						}
						$sql_town_province->free_result();
//					}
				}
			} else {
				//No Provinces
				$sql_town_country->bind_param('s', $country_id);
				$sql_town_country->execute();
				$sql_town_country->store_result();
				$sql_town_country->bind_result($town_id, $town_name);
				while ($sql_town_country->fetch()) {
					$breadcrumb_town = $breadcrumb_country;
					$breadcrumb_town .= "&#187; <a href='/accommodation/$category_link/southern_africa/";
					$breadcrumb_town .= str_replace(' ', '_', strtolower($country_name));
					$breadcrumb_town .= "/index.html' style='color: black'>$country_name</a>";
					$town_base_dir = $base_dir . '/' . str_replace(' ', '_', strtolower($country_name));
//					$establishment_count = category_count($category_id, 'town', $town_id);
					//echo "<div style='margin-left: 40px'>$line: Town ($town_id): $town_name ($establishment_count)</div>";
					$line++;
//					if ($establishment_count != 0) {
						$level = 'town';
						$sql_check->bind_param('sss', $level, $town_id, $category_full_id);
						$sql_check->execute();
						$sql_check->store_result();
						$sql_check->bind_result($count);
						$sql_check->fetch();
						$sql_check->free_result();
						if ($count == 0) {
							$sql_insert->bind_param('ssssssssssss',$level, $province_id, $town_base_dir, $town_id, $breadcrumb_province, $region_id, $country_id, $province_id, $town_id, $suburb_id, $borough_id, $category_full_id);
							$sql_insert->execute();
						}
						$sql_suburb->bind_param('s', $town_id);
						$sql_suburb->execute();
						$sql_suburb->store_result();
						$sql_suburb->bind_result($suburb_id, $suburb_name);
						while ($sql_suburb->fetch()) {
//							$establishment_count = category_count($category_id, 'suburb', $suburb_id);
							//echo "<div style='margin-left: 60px'>$line: Suburb ($suburb_id): $suburb_name ($establishment_count)</div>";
							$line++;
//							if ($establishment_count != 0) {
								$level = 'suburb';
								$sql_check->bind_param('sss', $level, $suburb_id, $category_full_id);
								$sql_check->execute();
								$sql_check->store_result();
								$sql_check->bind_result($count);
								$sql_check->fetch();
								$sql_check->free_result();
								if ($count == 0) {
									$sql_insert->bind_param('ssssssssssss',$level, $town_id, $town_base_dir, $suburb_id, $breadcrumb_province, $region_id, $country_id, $province_id, $town_id, $suburb_id, $borough_id, $category_full_id);
									$sql_insert->execute();
								}
//							}
						}
						$sql_suburb->free_result();
//					}
				}
				$sql_town_country->free_result();
							
			}
//		}
	}
	$sql_countries->free_result();
}
$sql_categories->free_result();

//Close Prepared Statements
$sql_categories->close();
$sql_countries->close();
		
	

echo "<script>parent.cache_update_done_shell('{$_GET['c']}')</script>";

function category_count($category_id, $level, $level_id) {
	//Vars
	$count = '';
	
	//Setup WHERE Clause depending on level and category
	switch ($level) {
		case 'country':
			$data_where = "b.country_id=$level_id";
			break;
		case 'province':
			$data_where = "b.province_id=$level_id";
			break;
		case 'town':
			$data_where = "b.town_id=$level_id";
			break;
		case 'suburb':
			$data_where = "b.suburb_id=$level_id";
			break;
	}
	if ($category_id != 'PTS' && $category_id != 'HA') {
		$data_where .= "&& " . $category_id . "='Y'";
	} else {
		$data_where .= '';
	}
	$data_where .= " && a.aa_estab='Y' && a.active='1' ";
		
	//Get list of establishments
	$statement = "SELECT COUNT(*)
    			FROM nse_export_establishments AS a
    			JOIN nse_export_establishments_cache AS b ON a.establishment_code=b.establishment_code
    			WHERE $data_where";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->execute();
	$sql_count->store_result();
	$sql_count->bind_result($count);
	$sql_count->fetch();
	$sql_count->free_result();
	$sql_count->close();
	
	return $count;
}

?>
