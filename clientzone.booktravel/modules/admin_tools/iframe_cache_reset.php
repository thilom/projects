<?php
//Vars
$establishment_cade = '';

//Includes
require_once '../../settings/init.php';

//Delete All
$statement = "DELETE FROM nse_export_establishments";
$sql_clear = $GLOBALS['dbCon']->prepare($statement);
$sql_clear->execute();
$sql_clear->close();

//Prepare statement - insert
$statement = "INSERT INTO nse_export_establishments (establishment_code) VALUES (?)";
$sql_import = $GLOBALS['dbCon']->prepare($statement);

//Get list of estabs
$statement = "SELECT establishment_code FROM nse_establishment";
$sql_list = $GLOBALS['dbCon']->prepare($statement);
$sql_list->execute();
$sql_list->store_result();
$sql_list->bind_result($establishment_code);
while ($sql_list->fetch()) {
	if (empty($establishment_code)) continue;
	$sql_import->bind_param('s', $establishment_code);
	$sql_import->execute();
}
$sql_list->free_result();
$sql_list->close();
$sql_import->close();

echo "<script>parent.cache_reset_return();</script>";
?>