<?php
/**
 * Updates the location and returns a javascript function call
 */

//Vars
$export_code = $_GET['code'];
$complete = 0;
$level = '';
$current_id = '';
$estab_count = 0;

//Includes
require_once '../../settings/init.php';

//Check if already complete
$statement = "SELECT complete FROM nse_export WHERE export_id=?";
$sql_check = $GLOBALS['dbCon']->prepare($statement);
$sql_check->bind_param('i', $export_code);
$sql_check->execute();
$sql_check->store_result();
$sql_check->bind_result($complete);
$sql_check->fetch();
$sql_check->free_result();
$sql_check->close();

//Check for establishments
$statement = "SELECT level, current_id FROM nse_export WHERE export_id=?";
$sql_list = $GLOBALS['dbCon']->prepare($statement);
$sql_list->bind_param('s', $export_code);
$sql_list->execute();
$sql_list->store_result();
$sql_list->bind_result($level, $current_id);
$sql_list->fetch();
$sql_list->free_result();
$sql_list->close();

$statement = "SELECT COUNT(*) FROM nse_establishment_location AS a JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code WHERE b.aa_estab='Y' && ";
switch ($level) {
	case 'country':
		$statement .= "a.country_id=?";
		break;
	case 'province':
		$statement .= "a.province_id=?";
		break;
	case 'town':
		$statement .= "a.town_id=?";
		break;
	case 'suburb':
		$statement .= "a.suburb_id=?";
		break;
}
//$sql_count = $GLOBALS['dbCon']->prepare($statement);
//$sql_count->bind_param('i', $current_id);
//$sql_count->execute();
//$sql_count->store_result();
//$sql_count->bind_result($estab_count);
//$sql_count->fetch();
//$sql_count->free_result();
//$sql_count->close();
//echo "Establishments :: $estab_count";
//if ($estab_count > 0) {
	//Update Remote Establishment Details Page
	if ($complete != 'Y' && $complete != 'B') {
		//Claim the rights to this entry
		$statement = "UPDATE nse_export SET complete='B' WHERE export_id=?";
		$sql_claim = $GLOBALS['dbCon']->prepare($statement);
		$sql_claim->bind_param('i', $export_code);
		$sql_claim->execute();
		$sql_claim->close();
		
		$ch = curl_init("http://www.aatravel.co.za/export/update_location.php?code=$export_code");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$res = curl_exec($ch);
		curl_close($ch);
		$res = addslashes($res);
		echo $res;
		$statement = "UPDATE nse_export SET complete = 'Y' WHERE export_id=?";
		$sql_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_update->execute();
		$sql_update->close();
		echo "<script>parent.location_return('$res',$export_code);</script>";
		
	} else {
		echo "<script>parent.location_return('Already Done',$export_code);</script>";
	}
//} else {
//	$statement = "UPDATE nse_export SET complete = 'Y' WHERE export_id=?";
//	$sql_update = $GLOBALS['dbCon']->prepare($statement);
//	$sql_update->execute();
//	$sql_update->close();
//	echo "<script>parent.location_return('No Estabs',$export_code);</script>";
//}



?>