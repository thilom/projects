<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'admin_tools';
$module_name = "Administration Tools";
$module_icon = 'configure-64.png';
$module_link = 'tools.php';
$window_width = '800';
$window_height = '700';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 's'; //a = all
$sub_levels = array();
$sub_levels['admin_tools']['name'] = 'Administration Tools';
$sub_levels['admin_tools']['file'] = 'tools.php';
$sub_levels['admin_tools']['subs']['add']['name'] = 'Create Generation List';



?>