<?php
/**
 * Tools for administrators/super users only.
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category tools
 */

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/estab_s/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';

//Vars
$function = isset($_GET['f'])?$_GET['f']:'';
$css = '';

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/admin_tools/html/tools.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/admin_tools/html/tools.js'> </script>";

//Map
switch ($function) {
	case 'estab_regen':
		regenerate_establishments();
		break;
        case 'theme_cache':
                regenerate_theme_cache();
                break;
	case 'location_regen':
		regenerate_locations();
		break;
	case 'location_regen_new':
		regenerate_locations_new();
		break;
	case 'page_cache':
		page_generation_cache();
		break;
	case 'page_cache_shell':
		page_generation_cache_shell();
		break;
	case 'php_info':
		php_info();
		break;
	case 'estab_cache':
		establishment_cache_update();
		break;
	default:
		welcome();
}

function welcome() {
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/admin_tools/html/welcome.html');
		
	echo $template;
}

function regenerate_establishments() {
	//Vars
	$estab_code = '';
	$estab_array = '';
	$c = 0;
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/admin_tools/html/establishment_regeneration.html');
	
	//Get list of estabs
	$statement = "SELECT establishment_code FROM nse_establishment WHERE aa_estab='Y' && active='1'";
	$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
	$sql_estabs->execute();
	$sql_estabs->store_result();
	$sql_estabs->bind_result($estab_code);
	while ($sql_estabs->fetch()) {
		$estab_array .= "estabs[$c] = '$estab_code';\n";
		$c++;
	}
	$sql_estabs->free_result();
	$sql_estabs->close();
	
	//Replace Tags
	$template = str_replace('<!-- estabs_array -->', $estab_array, $template);
	$template = str_replace('<!-- estabs_count -->', $c, $template);
	
	echo $template;
}

function page_generation_cache() {
	//Vars
	$category_id = '';
	$category_name = '';
	$category_list = '';
	$cache_list = '';
	$col_count = 0;
	
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/admin_tools/html/page_generate.html');
		
	//Get Category List
	$statement = "SELECT identifier, breadcrumb FROM nse_export_data";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	$sql_data->execute();
	$sql_data->store_result();
	$sql_data->bind_result($category_id, $category_name);
	while ($sql_data->fetch()) {
		$cache_list .= "$category_id,";
		if ($col_count == 0) {
			$category_list .= "<tr>";
		}
		$category_list .= "<TD><img src='/i/blank.png' width=16 height=16 id=img_$category_id />$category_name</TD>";
		$col_count++;
		if ($col_count == 3) {
			$category_list .= "</tr>";
			$col_count = 0;
		}
	}
	$cache_list = substr($cache_list, 0, -1);
	$sql_data->free_result();
	$sql_data->close();
		
	//Replace Tags
	$template = str_replace('<!-- cache_list -->', $cache_list, $template);
	$template = str_replace('<!-- category_list -->', $category_list, $template);
	
	echo $template;
}

function page_generation_cache_shell() {
	//Vars
	$category_id = '';
	$category_name = '';
	$category_list = '';
	$cache_list = '';
	$col_count = 0;
	
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/admin_tools/html/page_generate_shell.html');
		
	//Get Category List
	$statement = "SELECT identifier, breadcrumb FROM nse_export_data";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	$sql_data->execute();
	$sql_data->store_result();
	$sql_data->bind_result($category_id, $category_name);
	while ($sql_data->fetch()) {
		$cache_list .= "$category_id,";
		if ($col_count == 0) {
			$category_list .= "<tr>";
		}
		$category_list .= "<TD><img src='/i/blank.png' width=16 height=16 id=img_$category_id />$category_name</TD>";
		$col_count++;
		if ($col_count == 3) {
			$category_list .= "</tr>";
			$col_count = 0;
		}
	}
	$cache_list = substr($cache_list, 0, -1);
	$sql_data->free_result();
	$sql_data->close();
		
	//Replace Tags
	$template = str_replace('<!-- cache_list -->', $cache_list, $template);
	$template = str_replace('<!-- category_list -->', $category_list, $template);
	
	echo $template;
}


/**
 *	Regenerate the location pages for AA Travel
 */
function regenerate_locations() {
	//Vars
	$export_code = '';
	$location_array = '';
	$c = 0;
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/admin_tools/html/location_regeneration.html');
	
	//Get list of locations
	$statement = "SELECT export_id FROM nse_export WHERE complete IS NULL ORDER BY export_id DESC";
	$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
	$sql_estabs->execute();
	$sql_estabs->store_result();
	$sql_estabs->bind_result($export_code);
	while ($sql_estabs->fetch()) {
		$location_array .= "locations[$c] = '$export_code';\n";
		$c++;
	}
	$sql_estabs->free_result();
	$sql_estabs->close();
	
	//Replace Tags
	$template = str_replace('<!-- location_array -->', $location_array, $template);
	$template = str_replace('<!-- location_count -->', $c, $template);
	
	echo $template;
}

/**
 *	Regenerate the location pages for AA Travel on stage (new server)
 */
function regenerate_locations_new() {
	//Vars
	$export_code = '';
	$location_array = '';
	$c = 0;
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/admin_tools/html/location_regeneration_new.html');
	
	//Get list of locations
	$statement = "SELECT export_id FROM nse_export WHERE complete3 IS NULL ORDER BY export_id DESC";
	$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
	$sql_estabs->execute();
	$sql_estabs->store_result();
	$sql_estabs->bind_result($export_code);
	while ($sql_estabs->fetch()) {
		$location_array .= "locations[$c] = '$export_code';\n";
		$c++;
	}
	$sql_estabs->free_result();
	$sql_estabs->close();
	
	//Replace Tags
	$template = str_replace('<!-- location_array -->', $location_array, $template);
	$template = str_replace('<!-- location_count -->', $c, $template);
	
	echo $template;
}

function php_info() {
	echo phpinfo();
}

function establishment_cache_update() {
	//Vars
	$estab_code = '';
	$estab_array = '';
	$c = 0;
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/admin_tools/html/establishment_cache.html');
	
	//Get list of estabs
	$statement = "SELECT a.establishment_code
					FROM nse_export_establishments As a
					JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE complete IS NULL
					ORDER BY b.aa_estab DESC";
	$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
	$sql_estabs->execute();
	$sql_estabs->store_result();
	$sql_estabs->bind_result($estab_code);
	while ($sql_estabs->fetch()) {
		$estab_array .= "estabs[$c] = '$estab_code';\n";
		$c++;
	}
	$sql_estabs->free_result();
	$sql_estabs->close();
	
	//Replace Tags
	$template = str_replace('<!-- estabs_array -->', $estab_array, $template);
	$template = str_replace('<!-- estabs_count -->', $c, $template);
	
	echo $template;
	
}

function regenerate_theme_cache() {
    //Vars
    $template = file_get_contents(SITE_ROOT . '/modules/admin_tools/html/theme_cache.html');
    $theme_list = '';
    $line_color = 'line1';
    $js_list = '';

    //Get list of themes
    $statement = "SELECT theme_id, theme_name FROM nse_themes ORDER BY theme_name";
    $sql_theme = $GLOBALS['dbCon']->prepare($statement);
    $sql_theme->execute();
    $sql_theme->store_result();
    $sql_theme->bind_result($theme_id, $theme_name);
    while ($sql_theme->fetch()) {
        $line_color = $line_color=='line1'?'line2':'line1';
        $theme_list .= "<tr class=$line_color><td>$theme_name</td><td align=right id=process_$theme_id ></td></tr>";
        $js_list .= "$theme_id|";
    }
    $sql_theme->free_result();
    $sql_theme->close();

    //Roplace Tags
    $template = str_replace('<!-- theme_list -->', $theme_list, $template);
    $template = str_replace('!js_list!', $js_list, $template);

    echo $template;
}
?>