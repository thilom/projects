var cD = new Array();
var cID = '';

function run_cache_update() {

	var list = cache_list.split(',');
	
	for(var x=0; x<list.length; x++) {
		e = list[x];	
		var f = false;
		for (var y=0; y<cD.length; y++) {
			if (cD[y] == e) {
				f = true;
				break;
			}
		}
		if (!f) break;
	}

	if (!f) {
		cID = e;
		document.getElementById('img_' + e).src = '/i/silk/bullet_go.png';
		document.getElementById('ajax_iframe').src = '/modules/admin_tools/iframe_page_generate.php?c=' + e;
	}
}

function run_cache_update_shell() {

	var list = cache_list.split(',');
	
	for(var x=0; x<list.length; x++) {
		e = list[x];	
		var f = false;
		for (var y=0; y<cD.length; y++) {
			if (cD[y] == e) {
				f = true;
				break;
			}
		}
		if (!f) break;
	}

	if (!f) {
		cID = e;
		document.getElementById('img_' + e).src = '/i/silk/bullet_go.png';
		document.getElementById('ajax_iframe').src = '/modules/admin_tools/iframe_page_generate_shell.php?c=' + e;
	}
}

function cache_update_done(id,va) {
	cD[cD.length+1] = id;
	if (va == '1') {
		document.getElementById('img_' + id).src = '/i/silk/cross.png';
	} else {
		document.getElementById('img_' + id).src = '/i/silk/tick.png';
	}
	
	run_cache_update();
	
}

function cache_update_done_shell(id,va) {
	cD[cD.length+1] = id;
	if (va == '1') {
		document.getElementById('img_' + id).src = '/i/silk/cross.png';
	} else {
		document.getElementById('img_' + id).src = '/i/silk/tick.png';
	}
	
	run_cache_update_shell();
	
}

function toggle_iFrame(id) {
	if (document.getElementById(id).style.display == 'none' || document.getElementById(id).style.display == '') {
		document.getElementById(id).style.display = 'block';
	} else {
		document.getElementById(id).style.display = 'none';
	}
}

function toggle_estab_generation() {
	if (playing) {
		document.getElementById('play_button').value = button_value;
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		document.getElementById('play_button').value = 'Pause';
		document.getElementById('play_button').className = 'pause_button';
		playing = true;
		next_estab();
	}
}

function estab_return(eName,eCode,eStatus) {
	estab_done++;
	estab_todo--;
	perc = Math.floor((estab_done/eCount)*100);
	
	setProgress ('element1',perc);
	document.getElementById('currEstab').innerHTML = eName;
	document.getElementById('estabDone').innerHTML = estab_done;
	document.getElementById('estabTodo').innerHTML = estab_todo;
	
	//Remove
	for(i=0; i<estabs.length; i++) {
		if (estabs[i] == eCode) {
			array_id = i;
			break;
		}
	}
	estabs.splice(array_id,1);
	
	next_estab();
	
}

function next_estab() {	
	if (estabs.length == 0) {
		document.getElementById('play_button').value = 'Done';
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('play_button').disabled = true;
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		code = estabs[0];
		document.getElementById('ajax_iframe').src = 'iframe_update_establishment.php?source=aa_update&code=' + code;
	}
}


function toggle_location_generation() {
	if (playing) {
		document.getElementById('play_button').value = button_value;
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		document.getElementById('play_button').value = 'Pause';
		document.getElementById('play_button').className = 'pause_button';
		playing = true;
		next_location();
	}
}

function next_location() {	
	if (locations.length == 0) {
		document.getElementById('play_button').value = 'Done';
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('play_button').disabled = true;
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		code = locations[0];
		document.getElementById('ajax_iframe').src = 'iframe_update_locations.php?source=aa_update&code=' + code;
	}
}

function location_return(eName,eCode) {
	location_done++;
	location_todo--;
	perc = Math.floor((location_done/eCount)*100);
	
	setProgress ('element1',perc);
	document.getElementById('currEstab').innerHTML = eName;
	document.getElementById('estabDone').innerHTML = location_done;
	document.getElementById('estabTodo').innerHTML = location_todo;
	
	//Remove
	for(i=0; i<locations.length; i++) {
		if (locations[i] == eCode) {
			array_id = i;
			break;
		}
	}
	locations.splice(array_id,1);
	next_location();
	
}

function toggle_location_generation_new() {
	if (playing) {
		document.getElementById('play_button').value = button_value;
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		document.getElementById('play_button').value = 'Pause';
		document.getElementById('play_button').className = 'pause_button';
		playing = true;
		next_location_new();
	}
}

function next_location_new() {	
	if (locations.length == 0) {
		document.getElementById('play_button').value = 'Done';
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('play_button').disabled = true;
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		code = locations[0];
		document.getElementById('ajax_iframe').src = 'iframe_update_locations_new.php?source=aa_update&code=' + code;
	}
}

function location_return_new(eName,eCode) {
	location_done++;
	location_todo--;
	perc = Math.floor((location_done/eCount)*100);
	
	setProgress ('element1',perc);
	document.getElementById('currEstab').innerHTML = eName;
	document.getElementById('estabDone').innerHTML = location_done;
	document.getElementById('estabTodo').innerHTML = location_todo;
	
	//Remove
	for(i=0; i<locations.length; i++) {
		if (locations[i] == eCode) {
			array_id = i;
			break;
		}
	}
	locations.splice(array_id,1);
	next_location_new();
	
}

function toggle_estab_cache() {
	if (playing) {
		document.getElementById('play_button').value = button_value;
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		document.getElementById('play_button').value = 'Pause';
		document.getElementById('play_button').className = 'pause_button';
		playing = true;
		next_cache_estab();
	}
}

function next_cache_estab() {	
	if (estabs.length == 0) {
		document.getElementById('play_button').value = 'Done';
		document.getElementById('play_button').className = 'go_button';
		document.getElementById('play_button').disabled = true;
		document.getElementById('ajax_iframe').src = '';
		playing = false;
	} else {
		code = estabs[0];
		document.getElementById('ajax_iframe').src = 'iframe_cache_establishment.php?id=' + code;
	}
}

function estab_cache_return(eCode, eName) {
	estab_done++;
	estab_todo--;
	perc = Math.floor((estab_done/eCount)*100);
	
	setProgress ('element1',perc);
	document.getElementById('currEstab').innerHTML = eCode + ' - ' +eName;
	document.getElementById('estabDone').innerHTML = estab_done;
	document.getElementById('estabTodo').innerHTML = estab_todo;
	
	//Remove
	for(i=0; i<estabs.length; i++) {
		if (estabs[i] == eCode) {
			array_id = i;
			break;
		}
	}
	estabs.splice(array_id,1);
	
	next_cache_estab();
	
}

function cache_reset() {
	document.getElementById('ajax_iframe').src = 'iframe_cache_reset.php';
}

function cache_reset_return() {
	document.getElementById('currEstab').innerHTML = "Cache Reset";
	document.location=document.location;
}

function toggle_cache() {
    toggle_status = document.getElementById('start_button').value;
    if (toggle_status == 'Start') {
        document.getElementById('start_button').value = 'Pause';
        next_theme();
    } else {
        document.getElementById('start_button').value = 'Start';
    }
}

function next_theme() {
    themes = theme_list.split('|');

    for (i=0; i<themes.length; i++) {
        if (themes[i] == '') continue;
        if (!inArray(themes[i], theme_done)) {
            document.getElementById('process_' + themes[i]).innerHTML = 'BUSY';
            update_theme(themes[i]);
            break;
        }
    }
}

function update_theme(id) {
    if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			el = xmlhttp.responseText;
                        if (el == 1) {
                            theme_done[theme_done.length +1] = id;
                            document.getElementById('process_' + id).innerHTML = 'DONE';
                            if (document.getElementById('start_button').value == 'Pause') next_theme();
                        }
		} else {
                    m = 'BUSY';
                    document.getElementById('process_' + id).innerHTML = m;
                }
	}

	xmlhttp.open("GET","/modules/admin_tools/ajax_theme.php?id=" + id,true);
	xmlhttp.send();

}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
