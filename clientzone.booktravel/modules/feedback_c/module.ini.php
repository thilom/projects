<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'feedback_c';
$module_name = "Guest Reviews";
$module_icon = 'groupevent-64.png';
$noaccess_icon = 'feedback_noaccess.png';
$module_link = 'feedback.php';
$window_width = '750';
$window_height = '0';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 'c'; //a = all;

//General Settings
$list_length = 15;

?>