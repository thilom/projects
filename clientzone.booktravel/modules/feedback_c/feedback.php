<?php
/**
 * Client feedback manager. Allow clients to view and reply te feedback.
 *
 *  @author Thilo Muller (2009)
 *  @package RVBUS
 *  @category feedback
 */

//Vars
$function_white_list = array('list','view');
$function_id = '';
$css = '';

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include SITE_ROOT . '/modules/feedback_c/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/feedback_c/html/feedback.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/feedback_c/html/feedback.js'> </script>";

switch ($function_id) {
	case 'list':
		list_feedback();
		break;
	case 'view':
		if (isset($_POST['save_comment'])) {
			save_comment();
		} else if (isset($_POST['send_comment'])) {
			send_comment();
		} else {
			view_feedback();
		}
		break;
	default:
		welcome();
}

function save_comment() {
	//Vars
	$comment = $_POST['eComment'];
	$feedback_id = $_GET['id'];
	
	$statement = "UPDATE nse_establishment_feedback SET establishment_comment=?";
	if (!empty($comment)) $statement .= " ,feedback_status='2'";
	$statement .= " WHERE feedback_id=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bind_param('si', $comment, $feedback_id);
	$sql_update->execute();
	$sql_update->close();
	
	echo "<script>document.location='/modules/feedback_c/feedback.php?list&id=$feedback_id';</script>";
}

function send_comment() {
	//Vars
	$comment = $_POST['eComment'];
	$feedback_id = $_GET['id'];
	
	//Instantiate Email class
	require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';
	$mail = new PHPMailer(true);
	
	$statement = "UPDATE nse_establishment_feedback SET establishment_comment=? WHERE feedback_id=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bind_param('si', $comment, $feedback_id);
	$sql_update->execute();
	$sql_update->close();
	
	//Get feedback data
	$statement = "SELECT DATE_FORMAT(a.feedback_date, '%d %M, %Y'), a.visitor_comment, a.feedback_status, a.establishment_code, d.establishment_name, c.firstname, c.surname
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				JOIN nse_establishment AS d ON a.establishment_code=d.establishment_code
    				WHERE feedback_id=? ";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('s', $feedback_id);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_date, $feedback_message, $feedback_status, $establishment_code, $establishment_name, $firstname, $surname);
    $sql_feedback->fetch();
    $sql_feedback->free_result();
    $sql_feedback->close();
	
    $guest_name = "$firstname $surname";
    if (empty($guest_name)) $guest_name = 'Anonymous';
    
    //Get Client Details
    $statement = "SELECT email, firstname, lastname FROM nse_user WHERE user_id=?";
    $sql_client = $GLOBALS['dbCon']->prepare($statement);
    $sql_client->bind_param('i', $_SESSION['dbweb_user_id']);
    $sql_client->execute();
    $sql_client->store_result();
    $sql_client->bind_result($client_email, $firstname, $lastname);
    $sql_client->fetch();
    $sql_client->free_result();
    $sql_client->close();
    
	//Send email
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/feedback_c/html/staff_email.html');
	$template = str_replace('<!-- comment -->', $comment, $template);
	$template = str_replace('<!-- establishment -->', "$establishment_name ($establishment_code)", $template);
	$template = str_replace('<!-- guest_name -->', $guest_name, $template);
	$template = str_replace('<!-- feedback_date -->', $feedback_date, $template);
	$template = str_replace('<!-- client_name -->', "$firstname $lastname", $template);
	$template = str_replace('<!-- client_email -->', $client_email, $template);
	$template = str_replace('<!-- guest_comment -->', $feedback_message, $template);
	
	
	try {
      $mail->AddReplyTo('admin@aatravel.co.za', 'AA Travel Guides');
      $mail->AddAddress('feedback@aatravel.co.za', 'AA Travel Guides');
      $mail->SetFrom('admin@aatravel.co.za', 'AA Travel Guides');
      $mail->Subject = 'Client comment to feedback - AA Travel Guides Administration System';
      $mail->MsgHTML($template);
      $mail->Send();
      
      //Log
      $GLOBALS['log_tool']->write_entry("Client comment to guest feedback sent", $_SESSION['dbweb_user_id']);
    } catch (phpmailerException $e) {
      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Error! Client comment to guest feedback - email failed. $error", $_SESSION['dbweb_user_id']);
    } catch (Exception $e) {
      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Error! Client comment to guest feedback - email failed. $error", $_SESSION['dbweb_user_id']);
    }
	
	echo "<script>document.location='/modules/feedback_c/feedback.php?list&id=$feedback_id';</script>";
}

function welcome() {
	//Vars
	$establishment_code = '';
	$establishment_name = '';
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/feedback_c/html/welcome.html');
	
	//Get Establishment List
	$statement = "SELECT a.establishment_code, b.establishment_name
					FROM nse_user_establishments AS a
					JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE a.user_id=?";
	$sql_list = $GLOBALS['dbCon']->prepare($statement);
	$sql_list->bind_param('s', $_SESSION['dbweb_user_id']);
	$sql_list->execute();
	$sql_list->store_result();
	$sql_list->bind_result($establishment_code, $establishment_name);
	if ($sql_list->num_rows == 0) {
		$content = "<div id='no_estabs' >You have no access to feedback data. </div>";
	} else if ($sql_list->num_rows == 1) {
		$sql_list->fetch();
		$content = "<table width=100% id=icons><tr><td align=right><input type=button class=cancel_button onclick=cI() value='Close' /><input type=button value='Continue' class=ok_button onClick=document.location='/modules/feedback_c/feedback.php?f=list&id=$establishment_code' /></td></tr></table>";
		
	} else {
		$content = "<table width=100% cellpadding=0 cellspacing=0 id=TRW><tr><th align=left>Select the establishment to edit</td><th></th></tr>";
		while ($sql_list->fetch()) {
			$content .= "<tr><td>$establishment_name</td><td align=right><input type=button value='View' class=ok_button onClick=document.location='/modules/feedback_c/feedback.php?f=list&id=$establishment_code' /></td></tr>";
		}
		$content .= "<tr height=2><th colspan=10></th></tr></table><script>whack();</script> ";
	}
	$sql_list->free_result();
	$sql_list->close();
	
	$template = str_replace('<!-- establishment_list -->', $content, $template);
	
	echo $template;
}

function view_feedback() {
	//Vars
    $feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
    $feedback_date = '';
    $establishment_name = '';
    $firstname = '';
    $surname = '';
    $feedback_message = '';
    $feedback_status = '';
    $matches = array();
    $nLine = '';
    $feedback_value = '';
    $feedback_description = '';
    $buttons = '';
    $town_name = '';
    $suburb_name = '';
    $establishment_code = '';
    $location = '';
    $reply = '';
    $guest_email = '';
    $visit_date = '';
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/feedback_c/html/view_feedback.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@', $content, $matches);
    $lContent = $matches[0][0];
    
    //Get Data
    $statement = "SELECT DATE_FORMAT(a.feedback_date, '%d %M, %Y'), a.visitor_comment, a.feedback_status, a.establishment_code, c.firstname, c.surname, a.establishment_comment, a.visit_date, c.email
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE feedback_id=? ";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('s', $feedback_id);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_date, $feedback_message, $feedback_status, $establishment_code, $firstname, $surname, $reply, $visit_date, $guest_email);
    $sql_feedback->fetch();
    $sql_feedback->free_result();
    $sql_feedback->close();
    if (empty($feedback_message))$feedback_message = 'No Comment';
    
    //Get feedback results
    $statement = "SELECT a.feedback_value, b.category_description
    				FROM nse_feedback_results AS a
    				JOIN nse_feedback_category AS b ON a.category_code=b.category_code
    				WHERE a.feedback_id=?";
    $sql_results = $GLOBALS['dbCon']->prepare($statement);
    $sql_results->bind_param('s', $feedback_id);
    $sql_results->execute();
    $sql_results->bind_result($feedback_value, $feedback_description);
    $sql_results->store_result();
    while ($sql_results->fetch()) {
        $line = str_replace('<!-- feedback_item -->', $feedback_description, $lContent);
        
        for ($x=5; $x>=-1; $x--) {
            if ($x == $feedback_value) {
                $line = str_replace("<!-- feedback_value$x -->", "<img src='/i/silk/asterisk_orange.png'", $line);
            } else {
                $line = str_replace("<!-- feedback_value$x -->", '', $line);
            }
        }
        
        $nLine .= $line;
    }
    
    //Get Establishment Location
    $statement = "SELECT a.establishment_name, c.town_name, d.suburb_name
    				FROM nse_establishment AS a
    				JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_location_town AS c ON b.town_id=c.town_id
    				LEFT JOIN nse_location_suburb AS d ON b.suburb_id=d.suburb_id
    				WHERE a.establishment_code=?";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->bind_result($establishment_name, $town_name, $suburb_name);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
    
    //Assemble Location
    if (!empty($suburb_name)) {
        $location = "$suburb_name, $town_name";
    } else {
        $location = $town_name;
    }
    
    //Calculate status
    switch ($feedback_status) {
        case 0:
            $status = 'Hidden';
            break;
        case 1:
            $status = 'Hidden';
            break;
        case 2:
            $status = 'Visible';
            break;
        case 3:
            $status = 'Hidden';
            break;
        default:
            $status = '';
    }
    
    //Take care of empty values
    if (empty($feedback_date)) $feedback_date = '&nbsp;';
    if (empty($guest_email)) $guest_email = 'No Email';
    if (empty($visit_date)) $visit_date = '&nbsp;';
       
    //Replace Tags
    $content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
    $content = str_replace('<!-- feedback_name -->', "$firstname $surname", $content);
    $content = str_replace('<!-- feedback_date -->', $feedback_date, $content);
    $content = str_replace('<!-- feedback_message -->', $feedback_message, $content);
    $content = str_replace('<!-- feedback_status -->', $status, $content);
    $content = str_replace('<!-- establishment_code -->', $establishment_code, $content);
    $content = str_replace('<!-- location -->', $location, $content);
    $content = str_replace('<!-- reply -->', $reply, $content);
    $content = str_replace('<!-- guest_email -->', $guest_email, $content);
    $content = str_replace('<!-- visit_date -->', $visit_date, $content);
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    
    echo $content;
}

function list_feedback() {
    //Vars
    $start = isset($_GET['s'])?$_GET['s']:'0';
    $feedback_id = '';
    $feedback_date = '';
    $establishment_name = '';
    $establishment_code = $_GET['id'];
    $firstname = '';
    $surname = '';
    $matches = array();
    $nLine = '';
    $feedback_status = '';
    $where = '';
    $link = '/modules/feedback_c/feedback.php?f=' . $_GET['f'] . "&id=$establishment_code";
    $total_results = 0;
    $comment = '';
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/feedback_c/html/feedback_list.html');
    $content = str_replace(array("\r\n", "\r", "\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@', $content, $matches);
    $lContent = $matches[0][0];
    
    //Get list of Feedback
    $statement = "SELECT a.feedback_id, DATE_FORMAT(a.feedback_date, '%d %b, %Y'), b.establishment_name, c.firstname, c.surname, a.feedback_status, a.establishment_comment
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE a.establishment_code=?
    				ORDER BY a.feedback_id DESC
    				LIMIT ?,?";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('sii', $establishment_code, $start, $GLOBALS['list_length']);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_id, $feedback_date, $establishment_name, $firstname, $surname, $feedback_status, $comment);
    $sql_feedback->store_result();
    while ($sql_feedback->fetch()) {
        $buttons = '';
        $status = '';
        switch ($feedback_status) {
            case 0:
                $status = "<img src='/i/blank.png'>";
                $status_text = 'Hidden';
                break;
            case 2:
                $status = "<img src='/i/led-icons/eye.png'>";
                $status_text = 'Visible';
                if (!empty($comment)) $status_text .= ' with comment';
                break;
            case 1:
                $status = "<img src='/i/blank.png'>";
                $status_text = 'Hidden';
                break;
            case 3:
                $status = "<img src='/i/blank.png'>";
                $status_text = 'Hidden';
                break;
        }
        
        //Full name
        $full_name = "$firstname $surname";
        if ($full_name == ' ') $full_name = 'Anonymous';
        
        $line = str_replace('<!-- date -->', $feedback_date, $lContent);
        $line = str_replace('<!-- name -->', $full_name, $line);
        $line = str_replace('<!-- status -->', $status, $line);
        $line = str_replace('<!-- status_text -->', $status_text, $line);
        $line = str_replace('<!-- establishment -->', $establishment_name, $line);
         $buttons .= "<input type=button value='View' class=view_button onClick='document.location=\"/modules/feedback_c/feedback.php?f=view&id=$feedback_id\"'>";
        $line = str_replace('<!-- buttons -->', $buttons, $line);
        
        $nLine .= $line;
    }
    $sql_feedback->free_result();
    $sql_feedback->close();
    
	//Paging
    $statement = "SELECT COUNT(*)
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE a.establishment_code=?";
    $sql_count = $GLOBALS['dbCon']->prepare($statement);
    $sql_count->bind_param('s', $establishment_code);
    $sql_count->execute();
    $sql_count->bind_result($total_results);
    $sql_count->store_result();
    $sql_count->fetch();
    $sql_count->close();
    if ($total_results < $GLOBALS['list_length']) {
        $paging = "<td id=td1>&#9668;First</td><td id=td2>&lt;Previous</td><td id=td3>  </td><td id=td4>Next&gt;</td><td id=td5>Last&#9658;</td>";
    } else {
        if ($start != 0) {
            $paging = "<td id=td1><a href='$link&s=0'>&#9668;First</a></td>";
            
            $s = $start - $GLOBALS['list_length'];
            $paging .= "<td id=td2><a href='$link&s=$s'>&lt;Previous</a></td>";
        } else {
            $paging = "<td id=td1>&#9668;First</td><td id=td2>&lt;Previous</td>";
        }
        
        if ($total_results > $GLOBALS['list_length']) {
            $paging .= "<td id=td3>";
            for($x=1; $x<ceil($total_results/$GLOBALS['list_length'])+1; $x++) {
                $ns = ($x-1) * $GLOBALS['list_length'];
                if ($ns == $start) {
                    $paging .= "<span class='active_page'>$x</span> |";
                } else {
                    $paging .= "<a href='$link&s=$ns'>$x</a> |";
                }
            }
            $paging = substr($paging, 0, -1);
            $paging .= "</td>";
        } else {
            $paging .= "<td id=td3>  </td>";
        }
        
        
        if ($start + $GLOBALS['list_length'] > $total_results) {
            $paging .= "<td id=td4>Next&gt;</td><td id=td5>Last&#9658;</td>";
        } else {
            $s = $start + $GLOBALS['list_length'];
            $paging .= "<td id=td4><a href='$link&s=$s'>Next&gt;</a></td>";
            
            $s = (floor($total_results/$GLOBALS['list_length'])) * $GLOBALS['list_length'];
            $paging .= "<td id=td5><a href='$link&s=$s'>Last&#9658;</a></td>";
        }
    }
    
    //Replace Tags
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    $content = str_replace('<!-- paging -->', $paging, $content);
    
    echo $content;
}

?>