var geocoder;
var map;
var cnt = 0;

function show_block(bId) {
	m=1;
	c=1;
	var cu = document.getElementById('content' + bId).style.display;
	if (cu == 'block') {
		document.getElementById('content'+bId).style.display = 'none';
		document.getElementById('header'+bId).className = 'block_header';
	} else {
		while(m) {
			if (document.getElementById('content'+c)) {
				document.getElementById('content'+c).style.display = 'none';
				document.getElementById('header'+c).className = 'block_header';
			} else {
				m=0;
			}
			c++;
		}
		document.getElementById('content'+bId).style.display = 'block';
		document.getElementById('header'+bId).className = 'block_header_active';
	}
	//if(!FF && typeof TabScroll=="function")
	//	TabScroll()
}

function filter(v) {
	if (v == 'all') {
		eraseCookie('uEstabFilter');
	} else {
		createCookie('uEstabFilter', v);
	}
	document.location = document.location;
}

function sFilter(c) {
	if (c) {
		document.location = '/modules/estab_s/estab.php?f=estab_list';
	} else {
		v2 = document.getElementById('searchString').value;
		v1 = document.getElementById('searchFilter').value;
		document.location = '/modules/estab_s/estab.php?f=estab_list&sFilter=' + v1 + '&sString=' + v2;
	}
}

function showFilter() {
	v = readCookie('uEstabFilter');
	document.getElementById('uFilter').value = v;
}

function populate_contact_list() {
	var table = "<table width=100% cellpadding=2 cellspacing=0 id=contact_list>";
	table += "<tr><th>&nbsp;</th><th>Name</th><th>Designation</th><th>Tel</th><th>Cell</th><th>Email</th><th>&nbsp;</th></tr>";

	if (cL) {
		var item = cL.split('!|!');
		cl = 'line1';
		for (i=0; i<item.length; i++) {
			if (item[i] == '') continue;
			iT = item[i].split('|');
			if (iT[9] == 1) continue;
			table += "<tr ";
			table += i%2==0?" class='line1' ":" class='line2' ";
			table += "onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"";
			table += i%2==0?"line1\"' ":"line2\"' ";
			table += "><td>"
			table += iT[6]==1?"<img src='/i/silk/shield.png' title='This contact has access to the admin system' />":'';
			table += "</td><td>" + iT[1] + "</td><td>" + iT[2] + "</td><td>" + iT[3] + "</td><td>" + iT[4] + "</td><td>" + iT[5] + "</td>";
			table += "<td align=right><input type=button value='Edit' class=edit_button onClick=\"add_contact_form('"+ iT[0] +"')\" /><input type=button value='Remove' class=delete_button onClick=\"remove_contact('"+ iT[0] +"')\" /></td></tr>";
		}
	}

	table += "<tr><td colspan=10 align=right><input type=button value='Add Contact' class=add_button onClick=add_contact_form('new') /></td>";
	table += "</tr></table>";
	table += "<input type=hidden name=contacts value='"+ cL +"' >";

	document.getElementById('contacts').innerHTML = table;

	document.getElementById('contacts_list').value = cL;

	if (!fRun) {
		set_change('private_contact', 'private_contacts', 'contacts');
	}
}

function populate_invoice_list() {
	var table = "<table width=100% cellpadding=2 cellspacing=0 id=contact_list>";
	table += "<tr><th>date</th><th>Item</th><th>Value</th><th>Paid</th><th>&nbsp;</th></tr>";
	
	if (iN) {
		var item = iN.split('!|!');
		for (i=0; i<item.length; i++) {
			if (item[i] == '') continue;
			iT = item[i].split('|');
			table += "<tr ";
			table += i%2==0?" class='line1' ":" class='line2' ";
			table += "onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"";
			table += i%2==0?"line1\"' ":"line2\"' ";
			table += "><td>" + iT[1] + "</td><td>" + iT[2] + "</td><td>" + iT[3] + "</td><td>" + iT[4] + "</td>";
			table += "<td align=right><input type=button value='Edit' class=edit_button onClick=\"add_invoice_form('"+ iT[0] +"')\" /><input type=button value='Remove' class=delete_button onClick=\"remove_invoice('"+ iT[0] +"')\" /></td></tr>";
		}
	}
	
	table += "<tr><td colspan=10 align=right><input type=button value='Add Invoice' class=add_button onClick=add_invoice_form('new') /></td>";
	table += "</tr></table>";
	table += "<input type=hidden name=invoices value='"+ iN +"' >";
	
	document.getElementById('invoice_list').innerHTML = table;

	if (!iRun) {
		set_change('invoice', 'invoice');
	}
}



function remove_contact(id) {
	newList = '';
	var item = cL.split('!|!');
	for (i=0; i<item.length; i++) {
		iT = item[i].split('|');
		if (iT[0] != id) {
			newList += item[i] + "!|!";
		} else {
			if (iT[8] == 'new') continue;
			newList += iT[0] + "|" + iT[1] + "|" + iT[2] + "|";
			newList += iT[3] + "|" + iT[4] + "|" + iT[5];
			newList += '|' + iT[6];
			newList += '|' + iT[7];
			newList += "|" + iT[8] + '|1';
			newList += "!|!";
		}
	}
	cL = newList;
	populate_contact_list();
}

function remove_invoice(id) {
	newList = '';
	var item = iN.split('!|!');
	for (i=0; i<item.length; i++) {
		iT = item[i].split('|');
		if (iT[0] != id) {
			newList += item[i] + "!|!";
		}
	}
	iN = newList;
	iRun = false;
	populate_invoice_list();
}

function add_contact_form(id) {
	contact_name = '';
	designation = '';
	tel = '';
	cell = '';
	email = '';
	admin_user = 0;
	email_password = 0;
	if (id !== 'new') {
		var item = cL.split('!|!');
		for (i=0; i<item.length; i++) {
			iT = item[i].split('|');
			if (iT[0] == id) {
				contact_name = iT[1];
				designation = iT[2];
				tel = iT[3];
				cell = iT[4];
				email = iT[5];
				admin_user = iT[6];
				email_password = iT[7];
			}
		}
	}

	cB = "<table>";
	cB += "<tr><td style='width: 50px' class=field_label>Name</td><td style='width: 200px'><input style='width: 100%' type=text id=add_name value='" + contact_name + "'></td></tr>";
	cB += "<tr valign=top><td class=field_label>Designation</td><td><input style='width: 100%' type=text id=add_designation value='" + designation + "'><br />";
	cB += "<select id=sD style='width: 100%' onchange='change_designation(this.value)' ><option value=''> </option><option value='Owner'>Owner</option><option value='Owner/Manager'>Owner/Manager</option>";
	cB += "<option value='Manager'>Manager</option><option value='Marketing Manager'>Marketing Manager</option></select></td></tr>";
	cB += "<tr><td class=field_label>Tel</td><td><input style='width: 100%' type=text id=add_tel value='" + tel + "'></td></tr>";
	cB += "<tr><td class=field_label>Cell</td><td><input style='width: 100%' type=text id=add_cell value='" + cell + "'></td></tr>";
	cB += "<tr><td class=field_label>Email</td><td><input style='width: 100%' type=text id=add_email value='" + email + "'></td></tr>";
	cB += "<tr><td>&nbsp;</td><td class=field_label title='Allow this user access to the admin system'><input type=checkbox name=admin_user id=admin_user ";
	cB += admin_user==1?"checked":'';
	cB += " >Admin System Access</td></tr>";
	//cB += "<tr><td>&nbsp;</td><td ";
	//cB += admin_user==1?"class=field_label_enabled":"class=field_label_disabled";
	//cB += " id=rPC title='Resend the user thier password to access the admin system'><input type=checkbox name=resend_password id=resend_password "
	//cB += admin_user==1?' ':' disabled ';
	//cB += email_password==1?' checked ':' ';
	//cB += " >Email Password</td></tr>";
	cB += "<tr><td colspan=2>&nbsp;</td></tr>";
	cB += "<tr><td colspan=2 align=right><input type=button value='Cancel' class=cancel_button onClick='document.getElementById(\"contact_form\").style.display=\"none\"' /><input type=button value='OK' class=ok_button onClick=add_contact_update('" + id + "') /></td></tr>";


	cB += "</table>";
	document.getElementById('contact_form').innerHTML = cB;
	document.getElementById('contact_form').style.display = 'block';
}

function change_designation(v) {
	document.getElementById('add_designation').value=v;
	document.getElementById('sD').selectedIndex = 0;
}

function check_admin_user(v) {
	if (v) {
		document.getElementById('rPC').className = 'field_label_enabled';
		document.getElementById('resend_password').disabled = false;
	} else {
		document.getElementById('resend_password').checked = false;
		document.getElementById('resend_password').disabled = true;
		document.getElementById('rPC').className = 'field_label_disabled';
	}
}

function add_contact_update(id) {
	if (id == 'new') {
		cL += "!|!";
		cL += "new" + cnt + "|" + document.getElementById('add_name').value + "|" + document.getElementById('add_designation').value + "|";
		cL += document.getElementById('add_tel').value + "|" + document.getElementById('add_cell').value + "|" + document.getElementById('add_email').value;
		cL += document.getElementById('admin_user').checked?"|1":'|0';
		//cL += document.getElementById('resend_password').checked?"|1":'|0';
		cL += "|0|new|0";

		cnt++;
	} else {
		newList = '';
		var item = cL.split('!|!');
		for (i=0; i<item.length; i++) {
			iT = item[i].split('|');
			if (iT[0] == id) {
				newList += id + "|" + document.getElementById('add_name').value + "|" + document.getElementById('add_designation').value + "|";
				newList += document.getElementById('add_tel').value + "|" + document.getElementById('add_cell').value + "|" + document.getElementById('add_email').value;
				newList += document.getElementById('admin_user').checked?"|1":'|0';
				//newList += document.getElementById('resend_password').checked?"|1":'|0';
				newList += '|0|' + iT[8] + '|' + iT[9];
				newList += "!|!";
			} else {
				newList += item[i] + "!|!";
			}
		}
		cL = newList;
	}

	populate_contact_list();
	document.getElementById('contact_form').style.display = 'none';
}


function add_invoice_form(id) {
	invoice_date = '';
	invoice_item = '';
	invoice_cost = '';
	invoice_paid = '';
	if (id !== 'new') {
		var item = iN.split('!|!');
		for (i=0; i<item.length; i++) {
			iT = item[i].split('|');
			if (iT[0] == id) {
				invoice_date = iT[1];
				invoice_item = iT[2];
				invoice_cost = iT[3];
				invoice_paid = iT[4];
			}
		}
	}
	
	cB = "<table>";
	cB += "<tr><td style='width: 50px'>Date</td><td style='width: 200px'><input style='width: 100%' type=text id=invoice_date value='" + invoice_date + "'></td></tr>";
	cB += "<tr><td>Item</td><td><input style='width: 100%' type=text id=invoice_item value='" + invoice_item + "'></td></tr>";
	cB += "<tr><td>Value</td><td><input style='width: 100%' type=text id=invoice_cost value='" + invoice_cost + "'></td></tr>";
	cB += "<tr><td>Paid</td><td><input style='width: 100%' type=text id=invoice_paid value='" + invoice_paid + "'></td></tr>";
	cB += "<tr><td colspan=2 align=right><input type=button value='Cancel' class=cancel_button onClick='document.getElementById(\"invoice_form\").style.display=\"none\"' /><input type=button value='OK' class=ok_button onClick=add_invoice_update('" + id + "') /></td></tr>";
	
	
	cB += "</table>";
	document.getElementById('invoice_form').innerHTML = cB;
	document.getElementById('invoice_form').style.display = 'block';
}


function add_invoice_update(id) {
	if (id == 'new') {
		iN += "!|!new" + cnt + "|" + document.getElementById('invoice_date').value + "|" + document.getElementById('invoice_item').value + "|";
		iN += document.getElementById('invoice_cost').value + "|" + document.getElementById('invoice_paid').value + "|2";
		cnt++;
	} else {
		newList = '';
		var item = iN.split('!|!');
		for (i=0; i<item.length; i++) {
			iT = item[i].split('|');
			if (iT[0] == id) {
				newList += id + "|" + document.getElementById('invoice_date').value + "|" + document.getElementById('invoice_item').value + "|";
				newList += document.getElementById('invoice_cost').value + "|" + document.getElementById('invoice_paid').value;
				if (iT[5] != 2) {
					newList += "|1";
				} else {
					newList += "|2";
				}
				newList += "!|!";
			} else {
				newList += item[i] + "!|!";
			}
		}
		iN = newList;
	}
	
	iRun = false;
	populate_invoice_list();
	document.getElementById('invoice_form').style.display = 'none';
}

function populate_image_list() {
	table = "<table width=100% cellpadding=2 cellspacing=0 id=image_list_table>";
	
	if (iM) {
		var item = iM.split('!|!');
		for (i=0; i<item.length; i++) {
			if (item[i] == '') continue;
			iT = item[i].split('|');
			if (iT[4] == 3) continue;
			table += "<tr valign=top ";
			table += i%2==0?" class='line1' ":" class='line2' ";
			table += "onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"";
			table += i%2==0?"line1\"' ":"line2\"' ";
			table += ">";
			table += "<td width=100><img src='" + iT[1] + "' width=100px /></td><td><b>"+ iT[2]+"</b><br />"+ iT[3]+"</td>";
			table += "<td align=right nowrap valign=top><input type=button value='Edit' class=edit_button onClick=add_image_form('"+iT[0]+"','"+ eID +"') /><br />";
			table += "<input type=button value='Delete' class=delete_button onClick=delete_image('"+iT[0]+"') /><br />";
			table += "<input type=radio name=primary_image value='"+ iT[0] +"' id=im"+ iT[0] + " onClick='set_default_image(this.value)' ";
			if (iT[0]==default_image) table += " checked";
			table += " /><label for=im"+ iT[0] +">Primary Image</label></td>";
			table += "</tr>";
		}
	}
	
	table += "<tr><td colspan=10 align=right><input type=button value='Add Images' class=add_button onClick=add_image_form('new') /></td>";
	table += "</tr></table>";
	table += "<input type=hidden name=images value='"+ iM +"' >";
	
	document.getElementById('images').innerHTML = table;
	
	if (!fRun) {
		set_change('image', 'image', 'images');
	}
	fRun = false;
	
}

function add_image_form(id) {
	var image = '';
	var title = ''; 
	var description = '';
	var item = iM.split('!|!');
	for (i=0; i<item.length; i++) {
		if (item[i] == '') continue;
		iT = item[i].split('|');
		if (iT[0] == id) {
			image = urlencode(iT[1]);
			title = urlencode(iT[2]);
			description = urlencode(iT[3]);
		}
	}	
	document.getElementById('image_iframe').src = "upload_image.php?id=" +id + "&title=" + title + "&description=" + description + "&image=" + image + "&estab=" + eID;
	document.getElementById('image_iframe').style.display='block';
	document.getElementById('image_iframe').style.top = YPos - 200;

}

function add_image_return(id, image_name, image_title, image_description) {
	iL = '';
	ada = false;
	document.getElementById('image_iframe').style.display = 'none';
	var item = iM.split('!|!');
	for (i=0; i<item.length; i++) {
		if (item[i] == '') continue;
		iT = item[i].split('|');
		if (iT[0] == id) {
			iL += id + "|" + image_name + "|" + image_title + "|" + image_description + "|1!|!";
			ada = true;
		} else {
			iL += item[i] + '!|!';
		}
	}
	
	if (!ada) {
		iL += "new" + new_count + "|" + image_name + "|" + image_title + "|" + image_description + "|2!|!";
		new_count++;
	}
	iM = iL;
	populate_image_list();
}

function delete_image(id) {
	iL = '';
	var item = iM.split('!|!');
	for (i=0; i<item.length; i++) {
		if (item[i] == '') continue;
		iT = item[i].split('|');
		if (iT[0] != id) {
			iL += item[i] + '!|!';
		} else {
			iL += iT[0] + "|" + iT[1] + "|" + iT[2] + "|" + iT[3] + "|3!|!";
		}
	}
		
	iM = iL;
	populate_image_list();
}

function set_default_image(id) {
	default_image = id;
	set_change('image', 'image', 'images');
}

function close_iframe() {
	document.getElementById('image_iframe').style.display = 'none';
}

function toggleFacility(s,id,on1,off1) {
	if(s) {
		document.getElementById('facility'+id).style.backgroundImage = "url(" + on1 + ")";
	} else {
		document.getElementById('facility'+id).style.backgroundImage = "url(" + off1 + ")";
	}
}

function getLocation(t,v) {
	if (t == 'c') {
		document.getElementById('iframeCountry').src = "/modules/estab_s/iframe_location.php?cID=" + v;
	}
	if (t == 'p') {
		document.getElementById('iframeCountry').src = "/modules/estab_s/iframe_location.php?pID=" + v;
	}
	if (t == 't') {
		document.getElementById('iframeCountry').src = "/modules/estab_s/iframe_location.php?tID=" + v;
	}
}

function updateLocation(p,t,s) {
	//alert ('p=' + p + ', t=' + t + ', s=' + s);
	if (p == -1) {
		document.getElementById('province_id').options.length = 0;
		document.getElementById('province_id').options[0] = new Option('-- No Provinces Available --', '0');
		document.getElementById('province_id').disabled = true;
	} else if (p == '') {
		document.getElementById('province_id').disabled = false;
		document.getElementById('province_id').options.length = 0;
		document.getElementById('province_id').options[0] = new Option(' ', '0');
	} else if (p != 0) {
		document.getElementById('province_id').disabled = false;
		document.getElementById('province_id').options.length = 0;
		opst = p.split('#');
		for(x=0; x<opst.length; x++) {
			if (opst[x]) {
				opst_v = opst[x].split('|');
				document.getElementById('province_id').options[x] = new Option(opst_v[1], opst_v[0]);
			}
		}
	} else {

	}  
	
	if  (t == '') {
		document.getElementById('town_id').disabled = false;
		document.getElementById('town_id').options.length = 0;
		document.getElementById('town_id').options[0] = new Option(' ', '0');
	} else if (t != 0) {
		document.getElementById('town_id').disabled = false;
		document.getElementById('town_id').options.length = 0;
		document.getElementById('town_id').options[0] = new Option('-- Select Town --', '0');
		opst = t.split('#');
		for(x=0; x<opst.length; x++) {
			if (opst[x]) {
				opst_v = opst[x].split('|');
				document.getElementById('town_id').options[x+1] = new Option(opst_v[1], opst_v[0]);
			}
		}
		
	} else {
		
	} 
	
	if (s == -1) {
		document.getElementById('suburb_id').options.length = 0;
		document.getElementById('suburb_id').options[0] = new Option('-- No Suburbs Available --', '0');
		document.getElementById('suburb_id').disabled = true;
	} else if  (s == '') {
		document.getElementById('suburb_id').disabled = false;
		document.getElementById('suburb_id').options.length = 0;
		document.getElementById('suburb_id').options[0] = new Option(' ', '0');
	} else if (s != 0) {
		document.getElementById('suburb_id').disabled = false;
		document.getElementById('suburb_id').options.length = 0;
		document.getElementById('suburb_id').options[0] = new Option('-- Select Suburb / No Suburb --', '0');
		opst = s.split('#');
		for(x=0; x<opst.length; x++) {
			if (opst[x]) {
				opst_v = opst[x].split('|');
				document.getElementById('suburb_id').options[x+1] = new Option(opst_v[1], opst_v[0]);
			}
		}
	} else {
		
	} 
}

function urlencode (str) {
    // http://kevin.vanzonneveld.net
    // +   original by: Philip Peterson
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: AJ
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: travc
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Lars Fischer
    // +      input by: Ratheous
    // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
    // %          note 1: This reflects PHP 5.3/6.0+ behavior
    // *     example 1: urlencode('Kevin van Zonneveld!');
    // *     returns 1: 'Kevin+van+Zonneveld%21'
    // *     example 2: urlencode('http://kevin.vanzonneveld.net/');
    // *     returns 2: 'http%3A%2F%2Fkevin.vanzonneveld.net%2F'
    // *     example 3: urlencode('http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a');
    // *     returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a'
 
    var hexStr = function (dec) {
        return '%' + dec.toString(16).toUpperCase();
    };
 
    var ret = '',
            unreserved = /[\w.-]/; // A-Za-z0-9_.- // Tilde is not here for historical reasons; to preserve it, use rawurlencode instead
    str = (str+'').toString();
 
    for (var i = 0, dl = str.length; i < dl; i++) {
        var ch = str.charAt(i);
        if (unreserved.test(ch)) {
            ret += ch;
        }
        else {
            var code = str.charCodeAt(i);
            // Reserved assumed to be in UTF-8, as in PHP
            if (code === 32) {
                ret += '+'; // %20 in rawurlencode
            }
            else if (code < 128) { // 1 byte
                ret += hexStr(code);
            }
            else if (code >= 128 && code < 2048) { // 2 bytes
                ret += hexStr((code >> 6) | 0xC0);
                ret += hexStr((code & 0x3F) | 0x80);
            }
            else if (code >= 2048 && code < 65536) { // 3 bytes
                ret += hexStr((code >> 12) | 0xE0);
                ret += hexStr(((code >> 6) & 0x3F) | 0x80);
                ret += hexStr((code & 0x3F) | 0x80);
            }
            else if (code >= 65536) { // 4 bytes
                ret += hexStr((code >> 18) | 0xF0);
                ret += hexStr(((code >> 12) & 0x3F) | 0x80);
                ret += hexStr(((code >> 6) & 0x3F) | 0x80);
                ret += hexStr((code & 0x3F) | 0x80);
            }
        }
    }
    return ret;
}

function set_change(id,id2) {
	var append = true;
	var cV = document.getElementById('change_list').value;
	var cV2 = document.getElementById('change_list2').value;
	
	if (cV) {
		c = cV.split(',');
		for (i=0; i<c.length; i++) {
			if (c[i] == id) append=false;
		}
	}
	if (append) {
		if (cV) {
			cV = cV + ',' + id;
		} else {
			cV = id;
		}
		document.getElementById('change_list').value = cV;
	}
	
	append = true;	
	if (cV2) {
		c = cV2.split(',');
		for (i=0; i<c.length; i++) {
			if (c[i] == id2) append=false;
		}
	}
	if (append) {
		if (cV2) {
			cV2 = cV2 + ',' + id2;
		} else {
			cV2 = id2;
		}
		document.getElementById('change_list2').value = cV2;
		document.getElementById(id2 + '_indicator').src = '/i/led-icons/application_edit.png';
	}
	page_status();
}

function update_page(ok) {
	if (ok) {
		 document.getElementById('busyBar').style.display = 'none';
		 document.getElementById('doneBar').style.display = 'block';
	} else {
		document.getElementById('busyBar').style.display = 'none';
		document.getElementById('failBar').style.display = 'block';
	}
}

function cover_page() {
	document.body.style.overflow = 'hidden';
	document.body.scrollTop = 0;
	document.getElementById('send_cover').style.display = 'block';
	
}

function count_words(v) {
	var fullStr = v + " ";
	var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
	var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
	var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
	var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
	var splitString = cleanedStr.split(" ");
	var word_count = splitString.length -1;
	
	if (word_count < 36) color = 'green';
	if (word_count > 35 && word_count < 41) color = 'orange';
	if (word_count > 40) color = 'red';
	
	document.getElementById('word_count').style.color = color;
	document.getElementById('word_count').innerHTML = word_count;
	
}

function createCookie(name,value) {
	var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function poi_check(val,type,id) {
	var town_id = document.getElementById('town_id').value;
	document.getElementById(type+'_iframe_'+id).src = '/modules/estab_s/did_you_mean.php?search=' + val + '&town_id=' + town_id + '&fieldno=' + id + '&type=' + type;
}

function return_dym(id,type,landmark_id,landmark_name) {
	document.getElementById(type+'_name_'+id).value = landmark_name;
	document.getElementById(type+'_id_'+id).value = landmark_id;
	document.getElementById(type+'_iframe_'+id).src = '/modules/estab_s/did_you_mean.php';
}

function check_changes() {

	for (var x=0; x<chgFields.length; x++) {
		var a = chgFields[x];
		if (a == 'map_type') {
			a = a + '_' + chgList[a];
			if (document.getElementById(a)) {
				document.getElementById(a).checked = true;
				document.getElementById(a).onchange();
			}
		} else if (a.substr(0,10) == 'facilities') {
			if (document.getElementById(a)) {
				document.getElementById(a).checked = chgList[a];
				document.getElementById(a).onchange();
			}
		} else if (a == 'images') {
			iM = chgList[a];
			populate_image_list();
		} else if (a == 'contacts') {
			cL = chgList[a];
			populate_contact_list();
		} else {
			if (document.getElementById(a)) {
				document.getElementById(a).value = chgList[a];
				document.getElementById(a).onchange();
				document.getElementById(chgFields[x]).className = 'edit_changed';
			}
		}
	}
}


function delete_estab(id) {
	if (confirm('Warning!\n\nThis will permanently delete the establishment.\n\nContinue?')) {
		document.location="estab.php?f=estab_delete&id=" + id;
	}
}

function google_init() {
	geocoder = new GClientGeocoder();	
}

function calc_gps(n,v) {

	switch(n) {
		case 'dd_lat':
			if (v == '') break;
			nv = dd_to_dms('lat',v);
			document.getElementById('dms_lat').value = nv;
			nv = dd_to_gps('lat',v);
			document.getElementById('gps_lat').value = nv;
			break;
		case 'dd_lon':
			if (v == '') break;
			nv = dd_to_dms('lon',v);
			document.getElementById('dms_lon').value = nv;
			nv = dd_to_gps('lon',v);
			document.getElementById('gps_lon').value = nv;
			break;
		case 'dms_lat':
			nv = dms_to_dd('lat',v);
			document.getElementById('dd_lat').value = nv;
			nv = dms_to_gps('lat',v);
			document.getElementById('gps_lat').value = nv;
			break;
		case 'dms_lon':
			nv = dms_to_dd('lon',v);
			document.getElementById('dd_lon').value = nv;
			nv = dms_to_gps('lon',v);
			document.getElementById('gps_lon').value = nv;
			break;	
		case 'gps_lat':
			nv = gps_to_dd('lat',v);
			document.getElementById('dd_lat').value = nv;
			nv = gps_to_dms('lat',v);
			document.getElementById('dms_lat').value = nv;
			break;
		case 'gps_lon':
			nv = gps_to_dd('lon',v);
			document.getElementById('dd_lon').value = nv;
			nv = gps_to_dms('lon',v);
			document.getElementById('dms_lon').value = nv;
			break;
	}
}

// Convert decimal degrees TO degrees,minutes, seconds
function dd_to_dms(t,v) {
	if (t == 'lat') {
		if (v.substr(0,1) == '-') {
			nv = 'S'; 
			v = v.substr(1);
		} else {
			nv = 'N';
		}
	} else {
		if (v.substr(0,1) == '-') {
			nv = 'W'; 
			v = v.substr(1);
		} else {
			nv = 'E';
		}
	}

	nv += parseInt(v); //Degrees
	//nv += chr(167) + " ";
	nv += String.fromCharCode(176) + " "; 
	
	v = (v - parseInt(v)) * 60;
	nv += parseInt(v); //Minutes
	nv += "' ";
	
	v = (v - parseInt(v)) * 60;
	nv += parseInt(v); //Seconds
	nv += '" ';
	
	return nv;
}

// Convert decimal degrees TO GPS
function dd_to_gps(t,v) {
	if (t == 'lat') {
		if (v.substr(0,1) == '-') {
			nv = 'S'; 
			v = v.substr(1);
		} else {
			nv = 'N';
		}
	} else {
		if (v.substr(0,1) == '-') {
			nv = 'W'; 
			v = v.substr(1);
		} else {
			nv = 'E';
		}
	}
	nv += ' ';
	
	nv += parseInt(v); //Degrees
	nv += " ";
	
	v = (v - parseInt(v)) * 60;
	nv += v.toFixed(3); //Decimal
	
	return nv;
}

//Convert degrees, minutes, seconds TO decimal degrees
function dms_to_dd(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = '-';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = '';
		} else {
			nv = '';
		}
		
	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = '-'; 
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = '';
		} else {
			nv = '';
		}
	}
	
	if (v.indexOf('�') != -1) {
		d = v.substr(0, v.indexOf('�'));
		v = v.substr(v.indexOf('�')+1);
	} else {
		d = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}
	v = trim(v);
	
	if (v.indexOf("'") != -1) {
		m = v.substr(0, v.indexOf("'"));
		v = v.substr(v.indexOf("'")+1);
	} else {
		m = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}
	
	if (v.indexOf('"') != -1) {
		s = v.substr(0, v.indexOf('"'));
	} else {
		s = v.substr(v.indexOf(' '));
	}
	
	r = (((parseInt(m)*60)+parseInt(s))/3600) + parseInt(d);
	nv += r.toFixed(6);

	set_change('gps','locations','dd_lat');
	set_change('gps','locations','dd_lon');
	
	return nv;
}

//Convert degrees, minutes, seconds TO GPS
function dms_to_gps(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = 'S';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = 'N';
		} else {
			nv = 'S';
		}
	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = 'W'; 
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = 'E';
		} else {
			nv = 'E';
		}
	}
	
	if (v.indexOf('�') != -1) {
		d = v.substr(0, v.indexOf('�'));
		v = v.substr(v.indexOf('�')+1);
	} else {
		d = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}
	v = trim(v);
	
	if (v.indexOf("'") != -1) {
		m = v.substr(0, v.indexOf("'"));
		v = v.substr(v.indexOf("'")+1);
	} else {
		m = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}
	
	if (v.indexOf('"') != -1) {
		s = v.substr(0, v.indexOf('"'));
	} else {
		s = v.substr(v.indexOf(' '));
	}
	
	nv += ' ' + d;
	r = (parseInt(s)/60) + parseInt(m);
	nv += ' ' + r.toFixed(3);
	
	return nv;
}


//Convert GPS to decimal degrees
function gps_to_dd(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = '-';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = '';
		} else {
			nv = '';
		}
	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = '-'; 
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = '';
		} else {
			nv = '';
		}
	}
	
	v = trim(v);


	d = v.substr(0, v.indexOf(' '));
	v = v.substr(v.indexOf(' '));

	r = (parseFloat(v)/60) + parseInt(d);
	
	nv += '' + r.toFixed(6);
	
	set_change('gps','locations','dd_lat');
	set_change('gps','locations','dd_lon');
	
	return nv;
}

// Convert GPS to Degrees, Minutes, Seconds
function gps_to_dms(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = 'S';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		}else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = 'N';
		} else {
			nv = 'N';
		}
	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = 'W'; 
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = 'E';
		} else {
			nv = 'E';
		}
	}

	v = trim(v);

	d = v.substr(0, v.indexOf(' '));
	v = v.substr(v.indexOf(' '));
	m = parseInt(v);
	s = (parseFloat(v) - parseInt(v)) * 60;
	s = parseInt(s);
	
	nv +=  d + '� ' + m + "' " + s + '"';
	
	
	return nv;
}

function code_address() {
    address = '';
    if (document.getElementById('street_number').value != '') address += document.getElementById('street_number').value;
    if (document.getElementById('street_name').value != '') address += ' ' + document.getElementById('street_name').value;
    if (document.getElementById('suburb').value != '') address += ',' + document.getElementById('suburb').value;
    if (document.getElementById('town').value != '') address += ',' + document.getElementById('town').value;
    
    if (geocoder) {
      geocoder.getLocations(address, pop_address);
    }
  }

function pop_address(results) {
	
	
	if (results.Status.code != 200) {	
		alert('Geo Coordinates could not be retrieved');
	} else {
		if (results.Placemark[0]) {
			place = results.Placemark[0];
			lon = place.Point.coordinates[0];
			lat = place.Point.coordinates[1];
			document.getElementById('dd_lat').value = lat;
			document.getElementById('dd_lon').value = lon;
			
			calc_gps('dd_lat',document.getElementById('dd_lat').value);
			calc_gps('dd_lon',document.getElementById('dd_lon').value);
			set_change('gps','locations','dd_lat');
			set_change('gps','locations','dd_lon');
		}
	}
}

function check_map() {
	lat = document.getElementById('dd_lat').value;
	lon = document.getElementById('dd_lon').value;	
	
	if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById('map_canvas'));
        map.setCenter(new GLatLng(lat,lon), 13);
        map.setUIToDefault();
    	
        var hereIcon = new GIcon();
        hereIcon.image = "http://maps.google.com/mapfiles/ms/micons/blue-dot.png";
        hereIcon.shadow = "http://maps.google.com/mapfiles/ms/micons/msmarker.shadow.png";
        hereIcon.iconSize = new GSize(32, 32);
        hereIcon.shadowSize = new GSize(59, 32);
        hereIcon.iconAnchor = new GPoint(20, 32);
        // Set up our GMarkerOptions object literal
        markerOptions = {icon:hereIcon};
        map.addOverlay(new GMarker(new GLatLng(lat,lon), markerOptions));   	
     }
	
	document.getElementById('map_check').style.display = 'block';
	document.getElementById('map_check').top = YPos - 200;
	document.getElementById('map_check').left = document.clientWidth/2 - 150;
	map.setCenter(new GLatLng(lat,lon), 13);
	map.checkResize();
	map.panTo(new GLatLng(lat,lon));
}

function close_map() {
	document.getElementById('map_check').style.display = 'none';
}

function clear_coord() {
	document.getElementById('dd_lat').value = '';
	document.getElementById('dd_lon').value = '';
	document.getElementById('gps_lat').value = '';
	document.getElementById('gps_lon').value = '';
	document.getElementById('dms_lat').value = '';
	document.getElementById('dms_lon').value = '';
}

function publication_form() {
	document.getElementById('publication_iframe').top = YPos - 200;
	document.getElementById('publication_iframe').left = document.clientWidth/2 - 150;
	document.getElementById('publication_iframe').style.display = 'block';
	document.getElementById('publication_iframe').src = '/modules/estab_s/publications.php';
}

function awards_form() {
	document.getElementById('awards_iframe').top = YPos - 100;
	document.getElementById('awards_iframe').left = document.clientWidth/2 - 150;
	document.getElementById('awards_iframe').style.display = 'block';
	document.getElementById('awards_iframe').src = '/modules/estab_s/iframe_awards.php';
}

function close_awards_iframe() {
	document.getElementById('awards_iframe').style.display = 'none';
}

function draw_awards_list() {
	data = '<table id=awards_table ><tr><th>Year</th><th>Status</th><th>Category</th><th>&nbsp;</th></tr>';
	awards = '';
	row = 'row1';
	if (awards_list == '') {
		data += "<tr><td colspan=4 align=center> -- This establishment is not won any awards -- </td></tr>";
		document.getElementById('awards').value = awards_list;
	} else {
		lines = awards_list.split('#');
		for (i=0; i<lines.length; i++) {
			if (lines[i] == '') continue;
			items = lines[i].split('|');
			awards += items[0] + ',';
			data += '<tr class=' + row + ' ><td>' + items[1] + '</td><td>' + items[5] + '</td><td>' + items[3] + '</td>';
			data += '<td align=right ><input type=button value="Remove" class=book_delete_button onClick="delete_award(\'' + items[0] + '\')" /></td></tr>';
			row = (row=='row1')?'row2':'row1';
		}
		document.getElementById('awards').value = awards_list;
	}
	data += "<tr><th colspan=10 align=right ><input type=button value='Add Award' class=book_add_button onClick='awards_form()' /></th></tr></table>";
	document.getElementById('awards_list').innerHTML = data;
}

function delete_award(id) {
	new_awards_list = '';
	lines = awards_list.split('#');
	for (i=0; i<lines.length; i++) {
		if (lines[i] == '') continue;
		items = lines[i].split('|');
		if (items[0] == id) continue;
		new_awards_list += items[0] + '|' + items[1] + '|' + items[2] + '|' + items[3] + '|' + items[4] + '|' + items[5] + '#';
	}
	awards_list = new_awards_list;
	draw_awards_list();
	set_change('awards','details');
}

function add_award(year,category,result) {
	cat = category.split(':');
	res = result.split(':');
	awards_list += "new" + cnt + "|" + year + "|" + cat[0] + "|" + cat[1] + "|" + res[0] + "|" + res[1] + "#";
	cnt++;
	close_awards_iframe();
	draw_awards_list();
	set_change('awards','details');
}

function select_publication(id,n,x) {
	lines = publication_list.split('#');
	a = true;
	for (i=0; i<lines.length; i++) {
		if (lines[i] == '') continue;
		items = lines[i].split('|');
		if (items[0] == id) {
			a = false;
		}
	}
	
	if (a) {
		publication_list += '#' + id + '|' + n + '|Today' + '|' + x;
		draw_publication_list();
		set_change('publication','details');
	}
	document.getElementById('publication_iframe').style.display = 'none';
	
}

function close_publication_iframe() {
	document.getElementById('publication_iframe').style.display = 'none';
}

function draw_publication_list() {
	data2 = '<table id=publications_table ><tr><th>Publication name</th><th>Added</th><th>Expires</th><th>&nbsp;</th></tr>';
	publications = '';
	row = 'row1';
	if (publication_list == '') {
		data2 += "<tr><td colspan=4 align=center> -- This establishment is not listed in any publications -- </td></tr>";
		document.getElementById('publications').value = publications;
	} else {
		lines = publication_list.split('#');
		for (i=0; i<lines.length; i++) {
			if (lines[i] == '') continue;
			items = lines[i].split('|');
			publications += items[0] + ',';
			data2 += '<tr class=' + row + ' ><td>' + items[1] + '</td><td>' + items[2] + '</td><td>' + items[3] + '</td>';
			data2 += '<td align=right ><input type=button value="Remove" class=book_delete_button onClick="delete_publication(\'' + items[0] + '\')" /></td></tr>';
			row = (row=='row1')?'row2':'row1';
		}
		document.getElementById('publications').value = publications;
	}
	data2 += "<tr><th colspan=10 align=right ><input type=button value='Add Publication' class=book_add_button onClick='publication_form()' /></th>					</tr>				</table>";
	//data2 += "<tr><th colspan=10 align=right ><input type=button value='Add Publication' class=book_add_button onClick='publication_form()' /></th></tr>";
	document.getElementById('publication_list').innerHTML = data2;
}

function delete_publication(id) {
	new_publication_list = '';
	lines = publication_list.split('#');
	for (i=0; i<lines.length; i++) {
		if (lines[i] == '') continue;
		items = lines[i].split('|');
		if (items[0] == id) continue;
		new_publication_list += '#' + items[0] + '|' + items[1] + '|' + items[2] + '|' + items[3];
	}
	publication_list = new_publication_list;
	draw_publication_list();
	set_change('publication','details');
}

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function activate_estab(chk) {
	if (chk) {
		document.getElementById('inactive_text').innerHTML = 'Inactive<br>Suspended';
		document.getElementById('inactive_text').className = 'inactive_estab';
		document.getElementById('inactive_block').className = 'inactive_estabb';
	} else {
		document.getElementById('inactive_text').innerHTML = 'Active';
		document.getElementById('inactive_text').className = 'active_estab';
		document.getElementById('inactive_block').className = 'active_estabb';
	}
	page_status();
}

function revert_changes(id) {
	if (confirm('WARNING! This could overwrite changes that you may already have made in the editor\n\nContinue?')) {
		document.getElementById('iframeRevert').src = '/modules/estab_s/iframe_revert.php?code=' + id;
	}
}

function revert_changes_return(dt) {
	if (dt == '0') {
		alert('No changes to update');
	} else {
		chgFields = new Array();
		c = 0;
		line = dt.split('#|#');
		for (x=0; x<line.length; x++) {
			if (line[x] == '') continue;
			lp = line[x].split('*/*');
			chgFields[c] = lp[0];
			chgList[lp[0]] = lp[1];
			c++;
		}
		
		check_changes();
	}
}

function page_status() {
	var status_message = '';
	var page_status = '';

	if (document.getElementById('inactive_estab').checked) {
		status_message = '<span class=unlisted >This listing will not be displayed because it is marked as inactive</span>';
	} else {
		ap_aa_grade = document.getElementById('aa_grade').value;
		ap_advertiser = document.getElementById('advertiser').value;
		ap_publication = true;

		today=new Date();
		lines = publication_list.split('#');
		for (i=0; i<lines.length; i++) {
			if (lines[i] == '') continue;
			items = lines[i].split('|');
			dp = items[3].split('-');
			test_date = new Date(dp[0], dp[1], dp[2]);
			diff = today.getTime()-test_date.getTime();

			if (diff < 0) ap_publication = false;
		}

		if (ap_aa_grade == '' && ap_advertiser == '' && ap_publication) {
			status_message = "<span class='unlisted' >This listing will NOT be displayed because it is not quality assured, isn't an advertiser or isn't listed in a current publication </span>";
		}
	}

	if(document.getElementById('change_list').value != '') status_message += '<br><span class=unsaved >Changes to this listing have not been saved</span>';

	if (status_message != '') {
		page_status = "<table width=100%><tr><td width=50px><img src=/i/messagebox_warning-64.png style='height: 50px' /></td><td>" + status_message + "</td></tr></table>";
	} else {
		page_status = '';
	}

	document.getElementById('info_block').innerHTML = page_status;
}





