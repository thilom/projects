<?php
/**
 * Shared functions used to manage establishments.
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 *
 */
class establishment {
	
	function __construct() {
		
	}
	
	/**
	 * Returns the next available establishment code
	 *
	 * The establishment code consists of a letter followed by 5 digits (A00000) to create a 6 character establishment code.
	 * The last used letter and number is stored in a table (nse_settings). The next available number is checked against
	 * the current establishment_table (nse_establishment) for availability. This is done to ensure that new codes are not already in use
	 * due to a legacy (deprecated) code system.
	 *
	 * @return string
	 */
	function next_establishment_code() {
		//Vars
		$alphabet = str_split('ABCDEFGHIJKLMNPQRSTUVWXYZ123456789');
		$code_letter = '';
		$code_number = '';
		$code_ok = FALSE;
		$check_result = '';
		
		//Get current letter & number
		$statement = "SELECT variable_value FROM nse_variables WHERE variable_name=?";
		$sql_settings = $GLOBALS['dbCon']->prepare($statement);
		
		$tag_name = 'code_letter';
		$sql_settings->bind_param('s', $tag_name);
		$sql_settings->execute();
		$sql_settings->store_result();
		$sql_settings->bind_result($code_letter);
		$sql_settings->fetch();
		$sql_settings->free_result();
		
		$tag_name = 'code_number';
		$sql_settings->bind_param('s', $tag_name);
		$sql_settings->execute();
		$sql_settings->store_result();
		$sql_settings->bind_result($code_number);
		$sql_settings->fetch();
		$sql_settings->free_result();
		$sql_settings->close();
		
		
		//Prepare statement for code check
		$statement = "SELECT COUNT(*) FROM nse_establishment WHERE establishment_code=?";
		$sql_code_check = $GLOBALS['dbCon']->prepare($statement);
		
		//Prepare statement for code update
		$statement = "UPDATE nse_variables SET variable_value=? WHERE variable_name=?";
		$sql_code_update = $GLOBALS['dbCon']->prepare($statement);
		
		while (!$code_ok) {
			$code_number++;
			if ($code_number > 99999) {
				$code_number = 0;
				foreach ($alphabet as $k=>$v) {
					if ($v == $code_letter) {
						$code_letter = $alphabet[$k+1];
						
						//Update Settings
						$code_tag = 'code_letter';
						$sql_code_update->bind_param('ss', $code_letter, $code_tag);
						$sql_code_update->execute();
						
						break;
					}
				}
			}
			
			//Check if code exists
			$new_code = $code_letter . str_pad($code_number, 5, 0, STR_PAD_LEFT);
			$sql_code_check->bind_param('s', $new_code);
			$sql_code_check->execute();
			$sql_code_check->store_result();
			$sql_code_check->bind_result($check_result);
			$sql_code_check->fetch();
			$sql_code_check->free_result();
			
			if ($check_result == 0) {
				$code_ok = TRUE;
			}
		}
		
		//Update code number
		$code_tag = 'code_number';
		$sql_code_update->bind_param('ss', $code_number, $code_tag);
		$sql_code_update->execute();
		
		return $new_code;
	}
	
	/**
	 * Index an establishment to make it easily searchable.
	 *
	 * @param $establishment_code
	 * @return boolean
	 */
	function index_establishment($establishment_code) {
		//Vars
		$description = '';
		$title = '';
		$wordlist2 = ' ';
		$province_name = '';
		$town_name = '';
		$suburb_name = '';
		$icon_name = '';
		$aa_estab = '';
		$word_string = '';
		$star_grading = '';
		$aa_status = ',';
		$icon_list = '';
		$restypes = ',';
		$restype_id = '';
		$restype_name = '';
		$count = '';
		$icon_id = '';
		$caravan = '';
		$camp = '';
		$caravan_camp = '';
		$advertiser = '';
		$country_id = '';
		$province_id = '';
		$town_id = '';
		$suburb_id = '';
		$aa_code = '';
		$borough_id = '';
		$region_id = '';
		$star_grades = array();
		$restype_ids = array();
		$icons = array();
		
		//Prepare SQL Statements
		$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
		$sql_establishment_name = $GLOBALS['dbCon']->prepare($statement);

		$statement2 = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=?";
		$sql_description = $GLOBALS['dbCon']->prepare($statement2);

		$statement = "SELECT b.province_name, c.town_name, d.suburb_name, a.country_id, a.province_id, a.town_id, a.suburb_id
						FROM nse_establishment_location AS a
						LEFT JOIN nse_location_province AS b ON a.province_id=b.province_id
						LEFT JOIN nse_location_town AS c ON a.town_id=c.town_id
						LEFT JOIN nse_location_suburb AS d ON a.suburb_id=d.suburb_id
						WHERE establishment_code=?";
		$sql_locations = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT b.icon_name, a.icon_id
						FROM nse_establishment_icon AS a
						JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
						WHERE a.establishment_code=?";
		$sql_icons = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT aa_category_code, advertiser FROM nse_establishment WHERE establishment_code=?";
		$sql_aa_category = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT c.category_code, a.subcategory_id, a.star_grading
						FROM nse_establishment_restype AS a
						JOIN nse_restype_subcategory_parent AS b ON a.subcategory_id=b.subcategory_id
						JOIN nse_restype_category AS c ON b.category_id=c.category_id
						WHERE a.establishment_code=?";
		$sql_restype = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT COUNT(*) FROM nse_search_brush WHERE establishment_code=?";
		$sql_check = $GLOBALS['dbCon']->prepare($statement);

		$statement = "INSERT INTO nse_search_brush (establishment_code, star_grading, aa_status, icons, restype, aa_estab) VALUES (?,?,?,?,?,?)";
		$sql_brush_insert = $GLOBALS['dbCon']->prepare($statement);

		$statement = "UPDATE nse_search_brush SET star_grading=?, aa_status=?, icons=?, restype=?, aa_estab=? WHERE establishment_code=?";
		$sql_brush_update = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT MAX(star_grading) FROM nse_establishment_restype WHERE establishment_code=?";
		$sql_star_grading = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "DELETE FROM nse_search_drum WHERE establishment_code=?";
		$sql_drum_clear = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "INSERT INTO nse_search_drum (word, establishment_code, position, density, title) VALUES (?,?,?,?,?)";
		$sql_drum_insert = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT caravan_sites, camp_sites, caravan_camping_sites
						FROM nse_establishment_data
						WHERE establishment_code=?";
		$sql_cc = $GLOBALS['dbCon']->prepare($statement);
		
		//Propare Statement - Insert establishment into cache
		$statement = "INSERT INTO nse_export_establishments_cache
						(establishment_name, region_id,country_id,province_id,town_id,borough_id,suburb_id,4X4,ARTGAL,BACK,BB,BIRD,BOU,SSHO,COMH,CC,CF,CONF,
							DA,FARM,FISH,STAR5,STAR4,GLOD,GR,GF,GOLF,GH,HALAAL,HSPA,HIKE,HC,SCSA,HM,HO,HB,KOSHER,LO,LH,MUSEUM,PET,RVA,
							REST,ASHO,STAR3,TRAD,TRAIN,WHALE,WINEFARM,aa_estab,establishment_code)
						VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$sql_cache = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "INSERT INTO nse_export_establishments (establishment_code) VALUES (?)";
		$sql_data_insert = $GLOBALS['dbCon']->prepare($statement);
		
		
		//Prepare Statement - Delete establishment from cache
		$statement = "DELETE FROM nse_export_establishments WHERE establishment_code=?";
		$sql_data_delete = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "DELETE FROM nse_export_establishments_cache WHERE establishment_code=?";
		$sql_cache_delete = $GLOBALS['dbCon']->prepare($statement);
		
		
		$sql_establishment_name->bind_param('s', $establishment_code);
		$sql_establishment_name->execute();
		$sql_establishment_name->store_result();
		$sql_establishment_name->bind_result($title);
		$sql_establishment_name->fetch();
	
		$sql_description->bind_param('s', $establishment_code);
		$sql_description->execute();
		$sql_description->store_result();
		$sql_description->bind_result($description);
		while ($sql_description->fetch()) {
			$wordlist2 .= $description;
		}
	
		$sql_locations->bind_param('s', $establishment_code);
		$sql_locations->execute();
		$sql_locations->store_result();
		$sql_locations->bind_result($province_name, $town_name, $suburb_name, $country_id, $province_id, $town_id, $suburb_id);
		$sql_locations->fetch();
		
		$word_string = "$title $province_name  $town_name  $suburb_name $wordlist2";
	
		$w_ret = explode_and_condense($word_string);
		$t_ret = explode_and_condense($title);
		$word_list = $w_ret['word_list'];
		$wordCount = $w_ret['wordCount'];
		$wordPosition = $w_ret['wordPosition'];
		
		//Remove establishment from index
		$sql_drum_clear->bind_param('s', $establishment_code);
		$sql_drum_clear->execute();
		
		//Index Page (Title First)
		foreach ($t_ret['wordPosition'] as $position=>$word) {
			$position = 0;
			if (isset($wordCount[$word])) {
				if (count($word_list) < 100) {
					$density = ($wordCount[$word] / 100) * 100 ;
				} else {
					$density = ($wordCount[$word] / count($word_list)) * 100 ;
				}
			} else {
				$density = 0.00;
			}
			$isTitle = '1';
			$sql_drum_insert->bind_param('ssids', $word, $establishment_code, $position, $density, $isTitle);
			$sql_drum_insert->execute();
		}
		
		foreach ($wordPosition as $position=>$word) {
			if (isset($wordCount[$word])) {
				if (count($word_list) < 100) {
						$density = ($wordCount[$word] / 100) * 100 ;
					} else {
						$density = ($wordCount[$word] / count($word_list)) * 100 + 1 ;
					}
			} else {
				$density = 0.00;
			}
			$isTitle = '0';
			$sql_drum_insert->bind_param('ssids', $word, $establishment_code, $position, $density, $isTitle);
			$sql_drum_insert->execute();
		}
	
		//Get Icons
		$sql_icons->bind_param('s', $establishment_code);
		$sql_icons->execute();
		$sql_icons->bind_result($icon_name, $icon_id);
		$sql_icons->store_result();
		while ($sql_icons->fetch()) {
			$icon_list .= "$icon_name,";
			$icons[] = $icon_id;
		}
	
		//Get  AA status
		$sql_aa_category->bind_param('s', $establishment_code);
		$sql_aa_category->execute();
		$sql_aa_category->bind_result($icon_name, $advertiser);
		$sql_aa_category->store_result();
		while ($sql_aa_category->fetch()) {
			$aa_status .= "$icon_name,";
		}
	
		//Get  restype
		$sql_restype->bind_param('s', $establishment_code);
		$sql_restype->execute();
		$sql_restype->bind_result($restype_name, $restype_id, $star_grading);
		$sql_restype->store_result();
		while ($sql_restype->fetch()) {
			$restypes .= "$restype_name,";
			$restype_ids[] = $restype_id;
			$star_grades[] = $star_grading;
		}
	
		//Get Star Grading
		$sql_star_grading->bind_param('s', $establishment_code);
		$sql_star_grading->execute();
		$sql_star_grading->bind_result($star_grading);
		$sql_star_grading->store_result();
		$sql_star_grading->fetch();
		
		//AA Estab
		$ad_ids = array('P', 'L', 'A', 'B', 'S', 'F');
		if ($aa_status != ',' || in_array($advertiser, $ad_ids) ) {
			$aa_estab = 'Y';
		} else {
			$aa_estab = '';
		}
		
		//Check and update brush
		$sql_check->bind_param('s', $establishment_code);
		$sql_check->execute();
		$sql_check->bind_result($count);
		$sql_check->store_result();
		$sql_check->fetch();
		if($count == 0) {
			$sql_brush_insert->bind_param('ssssss', $establishment_code, $star_grading, $aa_status, $icon_list, $restypes, $aa_estab);
			$sql_brush_insert->execute();
		} else {
			$sql_brush_update->bind_param('ssssss', $star_grading, $aa_status, $icon_list, $restypes, $aa_estab, $establishment_code);
			$sql_brush_update->execute();
		}
	
		$sql_establishment_name->close();
		$sql_aa_category->close();
		$sql_check->close();
		$sql_brush_insert->close();
		$sql_brush_update->close();
		$sql_description->close();
		$sql_icons->close();
		
		
		//4X4
		$fourbyfour = in_array('20', $icons)?'Y':'';

		//Art Gallery
		$artgal = in_array('111', $restype_ids)?'y':'';
		
		//Backpacker Lodges
		$back_ids = array(47,29,48);
		$back = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $back_ids)) $back='Y';
		}
		
		//Bed & Breakfasts
		$bb_ids = array(5,43,15,17,21,6,18,22,39,26,23,15,4);
		$bb = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $bb_ids)) $bb='Y';
		}
		
		//Bird Watching
		$bird = in_array('21', $icons)?'Y':'';
		
		//Boutique Hotels
		$bou_ids = array(42,26);
		$bou = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $bou_ids)) $bou='Y';
		}
		
		//Budget Hotels
		$ssho_ids = array(36,37);
		$ssho = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $ssho_ids)) $ssho='Y';
		}
		
		//Business Hotels
		$comh_ids = array(8,9);
		if (in_array('49', $icons) || in_array('50', $icons)) {
			$comh = 'Y';
		} else {
			$comh = '';
		}
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $comh_ids)) $comh='Y';
		}
		
		//Caravan & Camping
		$sql_cc->bind_param('s', $establishment_code);
		$sql_cc->execute();
		$sql_cc->bind_result($caravan, $camp, $caravan_camp);
		$sql_cc->store_result();
		$sql_cc->fetch();
		if ($caravan > 0 || $camp > 0 || $caravan_camp > 0 || in_array('7',$restype_ids)) {
			$cc = 'Y';
		} else {
			$cc = '';
		}
		
		//Child Friendly
		if (in_array('75', $icons) || in_array('34', $icons)) {
			$cf = 'Y';
		} else {
			$cf = '';
		}
		
		//Conference
		if ($restype_id == '10' || in_array('48', $icons)) {
			$conf = 'Y';
		} else {
			$conf = '';
		}
		
		//Disabled Friendly
		$da = in_array('83', $icons)?'Y':'';
		
		//Farm Holidays
		$farm = in_array('15',$restype_ids)?'Y':'';

		//Fishing
		$fish = in_array('31', $icons)?'Y':'';
		
		//5 star
		$star5 = in_array('5', $star_grades)?'Y':'';
		
		//4 Stars
		$star4 = in_array('4', $star_grades)?'Y':'';
		
		//3 Stars
		$star3 = in_array('3', $star_grades)?'Y':'';
		
		//Game Lodges
		$glod_ids = array(19,20,38);
		if (in_array('22', $icons)) {
			$glod = 'y';
		} else {
			$glod = '';
		}
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $glod_ids)) $glod='Y';
		}
		
		//Game Reserves
		$gr_ids = array(20,38,19);
		$gr = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $gr_ids)) $gr='Y';
		}
		
		//Gay Friendly
		$gf = in_array('91', $icons)?'Y':'';
		
		//Golf Resorts
		$golf = in_array('27', $icons)?'Y':'';

		//Guest Houses
		$gh_ids = array(15,43,17,23,26,22,18,5,4,13);
		$gh = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $gh_ids)) $gh='Y';
		}
		
		//Halaal
		$halaal = in_array('90', $icons)?'Y':'';
		
		//Health Spas
		if (in_array('24', $restype_ids) || in_array('41', $icons)) {
			$spa = 'Y';
		} else {
			$spa = '';
		}
		
		//Hiking
		$hike = in_array('24', $icons)?'Y':'';
		
		//Holiday Cotages
		$hc_ids = array(32,46);
		$hc = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $hc_ids)) $hc='Y';
		}
		
		//Holiday flats
		$hf = in_array('34',$restype_ids)?'Y':'';
		
		//Honeymoon
		$hm_ids = array(42,11,12,13,27,28,24);
		if (in_array('41', $icons)) {
			$hm = 'Y';
		} else {
			$hm = '';
		}
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $hm_ids)) $hm='Y';
		}
		
		//Hotels
		$ho_ids = array(16,28,35,22,23,25,26,36,37);
		$ho = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $ho_ids)) $ho='Y';
		}
		
		//Houseboats
		$hb = in_array('21',$restype_ids)?'Y':'';

		//Kosher
		$kosher = in_array('89',$icons)?'Y':'';
		
		//Lodges
		$lo_ids = array(47,38,26,19,48);
		$lo = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $lo_ids)) $lo='Y';
		}
		
		//Luxury Hotels
		$lh_ids = array(26,27,28);
		$lh = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $lh_ids)) $lh='Y';
		}
		
		//Museums
		$museum = in_array('116', $restype_ids)?'Y':'';
		
		//Pet Friendly
		if (in_array('84', $icons) || in_array('85', $icons)) {
			$pet = 'Y';
		} else {
			$pet = '';
		}
		
		//Resorts
		$rva_ids = array(31,33,27,14);
		$rva = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $rva_ids)) $rva='Y';
		}
		
		//Restaurants
		$rest = in_array('124',$restype_ids)?'Y':'';
		
		//Self Catering
		$sc_ids = array(3,5,7,32,20,31,33,34,46);
		$sc = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $sc_ids)) $sc='Y';
		}
		
		//Township Accomodation
		$trad = in_array('39',$restype_ids)?'Y':'';
		
		//Train Accomodahion
		$train = '';
		
		//Whale Watching
		$whale = in_array('23', $icons)?'Y':'';
		
		//Wine Farms
		$wine = in_array('167',$restype_ids)?'Y':'';
		
		//Clear Cache
		$sql_cache_delete->bind_param('s', $establishment_code);
		$sql_cache_delete->execute();
		$sql_data_delete->bind_param('s', $establishment_code);
		$sql_data_delete->execute();
		
		//Save Cache
		$sql_cache->bind_param('sssssssssssssssssssssssssssssssssssssssssssssssssss', $title, $region_id, $country_id, $province_id, $town_id, $borough_id, $suburb_id, $fourbyfour, $artgal, $back, $bb, $bird, $bou, $ssho, $comh, $cc, $cf, $conf, $da, $farm, $fish, $star5, $star4, $glod, $gr, $gf, $golf, $gh, $halaal, $spa, $hike, $hc, $hf, $hm, $ho, $hb, $kosher, $lo, $lh, $museum, $pet, $rva, $rest, $sc, $star3, $trad, $train, $whale, $wine, $aa_estab, $establishment_code);
		$sql_cache->execute();
		$sql_data_insert->bind_param('s', $establishment_code);
		$sql_data_insert->execute();
		
		$sql_cache->close();
		$sql_cache_delete->close();
		$sql_data_delete->close();
		$sql_data_insert->close();
		
		return TRUE;
	}
	
	
	function cache_establishment($establishment_code) {
		//Vars
		$description = '';
		$title = '';
		$wordlist2 = ' ';
		$province_name = '';
		$town_name = '';
		$suburb_name = '';
		$icon_name = '';
		$aa_estab = '';
		$word_string = '';
		$star_grading = '';
		$aa_status = ',';
		$icon_list = '';
		$restypes = ',';
		$restype_id = '';
		$restype_name = '';
		$count = '';
		$icon_id = '';
		$caravan = '';
		$camp = '';
		$caravan_camp = '';
		$advertiser = '';
		$country_id = '';
		$province_id = '';
		$town_id = '';
		$suburb_id = '';
		$aa_code = '';
		$borough_id = '';
		$region_id = '';
		$star_grades = array();
		$restype_ids = array();
		$icons = array();
		
		//Prepare SQL Statements
		$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
		$sql_establishment_name = $GLOBALS['dbCon']->prepare($statement);

		$statement2 = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=?";
		$sql_description = $GLOBALS['dbCon']->prepare($statement2);

		$statement = "SELECT b.province_name, c.town_name, d.suburb_name, a.country_id, a.province_id, a.town_id, a.suburb_id
						FROM nse_establishment_location AS a
						LEFT JOIN nse_location_province AS b ON a.province_id=b.province_id
						LEFT JOIN nse_location_town AS c ON a.town_id=c.town_id
						LEFT JOIN nse_location_suburb AS d ON a.suburb_id=d.suburb_id
						WHERE establishment_code=?";
		$sql_locations = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT b.icon_name, a.icon_id
						FROM nse_establishment_icon AS a
						JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
						WHERE a.establishment_code=?";
		$sql_icons = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT aa_category_code, advertiser FROM nse_establishment WHERE establishment_code=?";
		$sql_aa_category = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT c.category_code, a.subcategory_id, a.star_grading
						FROM nse_establishment_restype AS a
						JOIN nse_restype_subcategory_parent AS b ON a.subcategory_id=b.subcategory_id
						JOIN nse_restype_category AS c ON b.category_id=c.category_id
						WHERE a.establishment_code=?";
		$sql_restype = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT MAX(star_grading) FROM nse_establishment_restype WHERE establishment_code=?";
		$sql_star_grading = $GLOBALS['dbCon']->prepare($statement);

		$statement = "SELECT caravan_sites, camp_sites, caravan_camping_sites
						FROM nse_establishment_data
						WHERE establishment_code=?";
		$sql_cc = $GLOBALS['dbCon']->prepare($statement);
		
		//Propare Statement - Insert establishment into cache
		$statement = "INSERT INTO nse_export_establishments_cache
						(establishment_name, region_id,country_id,province_id,town_id,borough_id,suburb_id,4X4,ARTGAL,BACK,BB,BIRD,BOU,SSHO,COMH,CC,CF,CONF,
							DA,FARM,FISH,STAR5,STAR4,GLOD,GR,GF,GOLF,GH,HALAAL,HSPA,HIKE,HC,SCSA,HM,HO,HB,KOSHER,LO,LH,MUSEUM,PET,RVA,
							REST,ASHO,STAR3,TRAD,TRAIN,WHALE,WINEFARM,aa_estab,establishment_code)
						VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$sql_cache = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "INSERT INTO nse_export_establishments (establishment_code) VALUES (?)";
		$sql_data_insert = $GLOBALS['dbCon']->prepare($statement);
		
		//Prepare Statement - Delete establishment from cache
		$statement = "DELETE FROM nse_export_establishments WHERE establishment_code=?";
		$sql_data_delete = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "DELETE FROM nse_export_establishments_cache WHERE establishment_code=?";
		$sql_cache_delete = $GLOBALS['dbCon']->prepare($statement);
		
		$sql_establishment_name->bind_param('s', $establishment_code);
		$sql_establishment_name->execute();
		$sql_establishment_name->store_result();
		$sql_establishment_name->bind_result($title);
		$sql_establishment_name->fetch();
	
		$sql_description->bind_param('s', $establishment_code);
		$sql_description->execute();
		$sql_description->store_result();
		$sql_description->bind_result($description);
		while ($sql_description->fetch()) {
			$wordlist2 .= $description;
		}
	
		$sql_locations->bind_param('s', $establishment_code);
		$sql_locations->execute();
		$sql_locations->store_result();
		$sql_locations->bind_result($province_name, $town_name, $suburb_name, $country_id, $province_id, $town_id, $suburb_id);
		$sql_locations->fetch();
		
		$word_string = "$title $province_name  $town_name  $suburb_name $wordlist2";
	
		$w_ret = explode_and_condense($word_string);
		$t_ret = explode_and_condense($title);
		$word_list = $w_ret['word_list'];
		$wordCount = $w_ret['wordCount'];
		$wordPosition = $w_ret['wordPosition'];
		
		//Remove establishment from index
		$sql_drum_clear->bind_param('s', $establishment_code);
		$sql_drum_clear->execute();
		
		//Index Page (Title First)
		foreach ($t_ret['wordPosition'] as $position=>$word) {
			$position = 0;
			if (isset($wordCount[$word])) {
				if (count($word_list) < 100) {
					$density = ($wordCount[$word] / 100) * 100 ;
				} else {
					$density = ($wordCount[$word] / count($word_list)) * 100 ;
				}
			} else {
				$density = 0.00;
			}
			$isTitle = '1';
			$sql_drum_insert->bind_param('ssids', $word, $establishment_code, $position, $density, $isTitle);
			$sql_drum_insert->execute();
		}
		
		foreach ($wordPosition as $position=>$word) {
			if (isset($wordCount[$word])) {
				if (count($word_list) < 100) {
						$density = ($wordCount[$word] / 100) * 100 ;
					} else {
						$density = ($wordCount[$word] / count($word_list)) * 100 + 1 ;
					}
			} else {
				$density = 0.00;
			}
			$isTitle = '0';
			$sql_drum_insert->bind_param('ssids', $word, $establishment_code, $position, $density, $isTitle);
			$sql_drum_insert->execute();
		}
	
		//Get Icons
		$sql_icons->bind_param('s', $establishment_code);
		$sql_icons->execute();
		$sql_icons->bind_result($icon_name, $icon_id);
		$sql_icons->store_result();
		while ($sql_icons->fetch()) {
			$icon_list .= "$icon_name,";
			$icons[] = $icon_id;
		}
	
		//Get  AA status
		$sql_aa_category->bind_param('s', $establishment_code);
		$sql_aa_category->execute();
		$sql_aa_category->bind_result($icon_name, $advertiser);
		$sql_aa_category->store_result();
		while ($sql_aa_category->fetch()) {
			$aa_status .= "$icon_name,";
		}
	
		//Get  restype
		$sql_restype->bind_param('s', $establishment_code);
		$sql_restype->execute();
		$sql_restype->bind_result($restype_name, $restype_id, $star_grading);
		$sql_restype->store_result();
		while ($sql_restype->fetch()) {
			$restypes .= "$restype_name,";
			$restype_ids[] = $restype_id;
			$star_grades[] = $star_grading;
		}
	
		//Get Star Grading
		$sql_star_grading->bind_param('s', $establishment_code);
		$sql_star_grading->execute();
		$sql_star_grading->bind_result($star_grading);
		$sql_star_grading->store_result();
		$sql_star_grading->fetch();
		
		//AA Estab
		$ad_ids = array('P', 'L', 'A', 'B', 'S', 'F');
		if ($aa_status != ',' || in_array($advertiser, $ad_ids) ) {
			$aa_estab = 'Y';
		} else {
			$aa_estab = '';
		}
		
		//Check and update brush
		$sql_check->bind_param('s', $establishment_code);
		$sql_check->execute();
		$sql_check->bind_result($count);
		$sql_check->store_result();
		$sql_check->fetch();
		if($count == 0) {
			$sql_brush_insert->bind_param('ssssss', $establishment_code, $star_grading, $aa_status, $icon_list, $restypes, $aa_estab);
			$sql_brush_insert->execute();
		} else {
			$sql_brush_update->bind_param('ssssss', $star_grading, $aa_status, $icon_list, $restypes, $aa_estab, $establishment_code);
			$sql_brush_update->execute();
		}
	
		$sql_establishment_name->close();
		$sql_aa_category->close();
		$sql_check->close();
		$sql_brush_insert->close();
		$sql_brush_update->close();
		$sql_description->close();
		$sql_icons->close();
		
		
		//4X4
		$fourbyfour = in_array('20', $icons)?'Y':'';

		//Art Gallery
		$artgal = in_array('111', $restype_ids)?'y':'';
		
		//Backpacker Lodges
		$back_ids = array(47,29,48);
		$back = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $back_ids)) $back='Y';
		}
		
		//Bed & Breakfasts
		$bb_ids = array(5,43,15,17,21,6,18,22,39,26,23,15,4);
		$bb = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $bb_ids)) $bb='Y';
		}
		
		//Bird Watching
		$bird = in_array('21', $icons)?'Y':'';
		
		//Boutique Hotels
		$bou_ids = array(42,26);
		$bou = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $bou_ids)) $bou='Y';
		}
		
		//Budget Hotels
		$ssho_ids = array(36,37);
		$ssho = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $ssho_ids)) $ssho='Y';
		}
		
		//Business Hotels
		$comh_ids = array(8,9);
		if (in_array('49', $icons) || in_array('50', $icons)) {
			$comh = 'Y';
		} else {
			$comh = '';
		}
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $comh_ids)) $comh='Y';
		}
		
		//Caravan & Camping
		$sql_cc->bind_param('s', $establishment_code);
		$sql_cc->execute();
		$sql_cc->bind_result($caravan, $camp, $caravan_camp);
		$sql_cc->store_result();
		$sql_cc->fetch();
		if ($caravan > 0 || $camp > 0 || $caravan_camp > 0 || in_array('7',$restype_ids)) {
			$cc = 'Y';
		} else {
			$cc = '';
		}
		
		//Child Friendly
		if (in_array('75', $icons) || in_array('34', $icons)) {
			$cf = 'Y';
		} else {
			$cf = '';
		}
		
		//Conference
		if ($restype_id == '10' || in_array('48', $icons)) {
			$conf = 'Y';
		} else {
			$conf = '';
		}
		
		//Disabled Friendly
		$da = in_array('83', $icons)?'Y':'';
		
		//Farm Holidays
		$farm = in_array('15',$restype_ids)?'Y':'';

		//Fishing
		$fish = in_array('31', $icons)?'Y':'';
		
		//5 star
		$star5 = in_array('5', $star_grades)?'Y':'';
		
		//4 Stars
		$star4 = in_array('4', $star_grades)?'Y':'';
		
		//3 Stars
		$star3 = in_array('3', $star_grades)?'Y':'';
		
		//Game Lodges
		$glod_ids = array(19,20,38);
		if (in_array('22', $icons)) {
			$glod = 'y';
		} else {
			$glod = '';
		}
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $glod_ids)) $glod='Y';
		}
		
		//Game Reserves
		$gr_ids = array(20,38,19);
		$gr = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $gr_ids)) $gr='Y';
		}
		
		//Gay Friendly
		$gf = in_array('91', $icons)?'Y':'';
		
		//Golf Resorts
		$golf = in_array('27', $icons)?'Y':'';

		//Guest Houses
		$gh_ids = array(15,43,17,23,26,22,18,5,4,13);
		$gh = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $gh_ids)) $gh='Y';
		}
		
		//Halaal
		$halaal = in_array('90', $icons)?'Y':'';
		
		//Health Spas
		if (in_array('24', $restype_ids) || in_array('41', $icons)) {
			$spa = 'Y';
		} else {
			$spa = '';
		}
		
		//Hiking
		$hike = in_array('24', $icons)?'Y':'';
		
		//Holiday Cotages
		$hc_ids = array(32,46);
		$hc = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $hc_ids)) $hc='Y';
		}
		
		//Holiday flats
		$hf = in_array('34',$restype_ids)?'Y':'';
		
		//Honeymoon
		$hm_ids = array(42,11,12,13,27,28,24);
		if (in_array('41', $icons)) {
			$hm = 'Y';
		} else {
			$hm = '';
		}
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $hm_ids)) $hm='Y';
		}
		
		//Hotels
		$ho_ids = array(16,28,35,22,23,25,26,36,37);
		$ho = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $ho_ids)) $ho='Y';
		}
		
		//Houseboats
		$hb = in_array('21',$restype_ids)?'Y':'';

		//Kosher
		$kosher = in_array('89',$icons)?'Y':'';
		
		//Lodges
		$lo_ids = array(47,38,26,19,48);
		$lo = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $lo_ids)) $lo='Y';
		}
		
		//Luxury Hotels
		$lh_ids = array(26,27,28);
		$lh = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $lh_ids)) $lh='Y';
		}
		
		//Museums
		$museum = in_array('116', $restype_ids)?'Y':'';
		
		//Pet Friendly
		if (in_array('84', $icons) || in_array('85', $icons)) {
			$pet = 'Y';
		} else {
			$pet = '';
		}
		
		//Resorts
		$rva_ids = array(31,33,27,14);
		$rva = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $rva_ids)) $rva='Y';
		}
		
		//Restaurants
		$rest = in_array('124',$restype_ids)?'Y':'';
		
		//Self Catering
		$sc_ids = array(3,5,7,32,20,31,33,34,46);
		$sc = '';
		foreach ($restype_ids as $ids) {
			if (in_array($ids, $sc_ids)) $sc='Y';
		}
		
		//Township Accomodation
		$trad = in_array('39',$restype_ids)?'Y':'';
		
		//Train Accomodahion
		$train = '';
		
		//Whale Watching
		$whale = in_array('23', $icons)?'Y':'';
		
		//Wine Farms
		$wine = in_array('167',$restype_ids)?'Y':'';
		
		//Clear Cache
		$sql_cache_delete->bind_param('s', $establishment_code);
		$sql_cache_delete->execute();
		$sql_data_delete->bind_param('s', $establishment_code);
		$sql_data_delete->execute();
		
		//Save Cache
		$sql_cache->bind_param('sssssssssssssssssssssssssssssssssssssssssssssssssss', $title, $region_id, $country_id, $province_id, $town_id, $borough_id, $suburb_id, $fourbyfour, $artgal, $back, $bb, $bird, $bou, $ssho, $comh, $cc, $cf, $conf, $da, $farm, $fish, $star5, $star4, $glod, $gr, $gf, $golf, $gh, $halaal, $spa, $hike, $hc, $hf, $hm, $ho, $hb, $kosher, $lo, $lh, $museum, $pet, $rva, $rest, $sc, $star3, $trad, $train, $whale, $wine, $aa_estab, $establishment_code);
		$sql_cache->execute();
		$sql_data_insert->bind_param('s', $establishment_code);
		$sql_data_insert->execute();
		
		$sql_cache->close();
		$sql_cache_delete->close();
		$sql_data_delete->close();
		$sql_data_insert->close();
		
		return TRUE;
	}
}

/**
 * Breaks a string into individual words and returns a word position list, a word Count list and a word list.
 *
 * @param $word_string
 * @return mixed
 */
function explode_and_condense($word_string) {
		$words = strip_tags($word_string);
		
		//Remove punctuation
		$punc = array('.', ';', '"', "'", ',', ':', '/', '\\', '=', '+', '-', '_', '(', ')', '*', '&', '^', '%', '$', '#', '@', '!', '~', '`', '?', '<', '>');
		$words = str_replace($punc, '', $words);
		$words = strtolower($words);
		$word_list = explode(' ', $words);
		
		//Create stop word list
		$stopList = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/shared/stop_words.txt');
		$stopList = str_replace(array("\r\n", "\n","\r"), '|', $stopList);
		$stopList = str_replace(' ', '', $stopList);
		$stop_list = explode('|', $stopList);
		
		//Remove stop words
		foreach ($word_list as $k=>$v) {
			if (in_array(strtolower($v), $stop_list)) unset($word_list[$k]);
			if (strlen($v) < 3) unset($word_list[$k]);
		}
		
		//Get word positions
		$w['wordPosition'] = str_word_count(join(' ', $word_list), 1);
		
		//Count words
		$w['wordCount'] = array_count_values($word_list);
		
		//Remove duplicates
		$w['word_list'] = $word_list;
		
		return $w ;
	}
?>