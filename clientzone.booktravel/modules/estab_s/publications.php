<?php
/**
 * Add publications to the establishment publications list.
 */

include $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

if (isset($_POST['save_publication'])) {
	
} else {
	publication_form();
}

function publication_form() {
	//Vars
	$publication_list = '';
	$publication_id = "";
	$publication_name = '';
	$publication_year = '';
	$expiry_date = '';
	
	//Get Template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/estab_s/html/publications_form.html');
	
	//Get List of Publications
	$statement = "SELECT publication_id, publication_name, publication_year, expiry_date FROM nse_publications WHERE expiry_date > NOW()";
	$sql_publications = $GLOBALS['dbCon']->prepare($statement);
	$sql_publications->execute();
	$sql_publications->store_result();
	$sql_publications->bind_result($publication_id, $publication_name, $publication_year, $expiry_date);
	while ($sql_publications->fetch()) {
		$publication_list .= "<tr><td>$publication_year - $publication_name</td><td>$expiry_date</td><td><input type=button value='Select' onCLick='parent.select_publication(\"$publication_id\",\"$publication_year - $publication_name\",\"$expiry_date\")' class=select_button /></td></tr>";
	}
	$sql_publications->free_result();
	$sql_publications->close();
	
	//Replace Tags
	$template = str_replace('<!-- publication_list -->', $publication_list, $template);
	
	echo $template;
}
?>