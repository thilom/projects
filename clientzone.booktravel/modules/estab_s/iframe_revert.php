<?php

//Vars
$establishment_code = $_GET['code'];
$update_date = '';
$field_name = '';
$field_value = '';
$return_list = '';

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Get last update date
$statement = "SELECT MAX(DATE_FORMAT(update_date, '%Y-%m-%d')) FROM nse_establishment_updates_log WHERE establishment_code=?";
$sql_date = $GLOBALS['dbCon']->prepare($statement);
$sql_date->bind_param('s', $establishment_code);
$sql_date->execute();
$sql_date->store_result();
$sql_date->bind_result($update_date);
$sql_date->fetch();
$sql_date->free_result();
$sql_date->close();

if (empty($update_date)) {
	echo "<script>parent.revert_changes_return('0');</script>";
	die();
}


//Get list of updates
$statement = "SELECT field_name, field_value FROM nse_establishment_updates_log WHERE DATE_FORMAT(update_date, '%Y-%m-%d')=? && establishment_code=?";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
$sql_data->bind_param('ss', $update_date, $establishment_code);
$sql_data->execute();
$sql_data->store_result();
$sql_data->bind_result($field_name, $field_value);
while ($sql_data->fetch()) {
	$field_value = addslashes($field_value);
	$field_value = str_replace(array("\r\n","\r","\n"), '\n', $field_value);
	$return_list .= "$field_name*/*$field_value#|#";
}
$sql_data->free_result();
$sql_data->close();

echo "<script>parent.revert_changes_return('$return_list');</script>";

?>