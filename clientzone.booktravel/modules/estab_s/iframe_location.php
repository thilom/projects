<?php
/**
 * Returns a list of provinces, towns and suburbs.
 *
 * @Author Thilo Muller(2009)
 * @package RVBUS
 * @category establishments
 *
 */

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/estab_s/module.ini.php';

if (isset($_GET['cID'])) {
	$provinces = get_provinces($_GET['cID']);
	if (empty($provinces)) {
		$provinces = '-1';
		$towns = get_towns($_GET['cID'], 'country');
	} else {
		$towns = '';
	}
	$suburbs = '';
}

if (isset($_GET['pID'])) {
	$provinces = '0';
	$towns = get_towns($_GET['pID']);
	$suburbs = '';
	
}

if (isset($_GET['tID'])) {
	$provinces = '0';
	$towns = '0';
	$suburbs = get_suburbs($_GET['tID']);
}

//Serve
echo "<script>parent.updateLocation('$provinces', '$towns', '$suburbs')</script>";


function get_provinces($parent_id) {
	//Vars
	$province_id = '';
	$province_name = '';
	$province_list = '';
	
	//Get Province List
	$statement = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=? ORDER BY province_name";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	$sql_province->bind_param('s', $parent_id);
	$sql_province->execute();
	$sql_province->bind_result($province_id, $province_name);
	$sql_province->store_result();
	if ($sql_province->num_rows == 0) return '';
	while ($sql_province->fetch()) {
		$province_list .= "$province_id|$province_name#";
	}
	$province_list = substr($province_list, 0, -1);
	return $province_list;
}

function get_towns($parent_id, $look_in='') {
	//Vars
	$town_id = '';
	$town_name = '';
	$town_list = '';
	
	//Get Town Data
	if ($look_in == 'country') {
		$statement = "SELECT town_id, town_name FROM nse_location_town WHERE country_id=? ORDER BY town_name";
	} else {
		$statement = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=? ORDER BY town_name";
	}
	$sql_towns = $GLOBALS['dbCon']->prepare($statement);
	$sql_towns->bind_param('s',$parent_id);
	$sql_towns->execute();
	$sql_towns->bind_result($town_id, $town_name);
	$sql_towns->store_result();
	if ($sql_towns->num_rows == 0) return '';
	while ($sql_towns->fetch()) {
	$res = mysql_query($statement);
		if (strtolower(substr($town_name, 0, 3)) == 'aaa') continue;
		if (strtolower(substr($town_name, 0, 3)) == 'zzz') continue;
		if (strpos($town_name, '***') > 0) continue;
		$town_name = str_replace("'", '', $town_name);
		$town_list .= "$town_id|$town_name#";
	}
	$town_list = substr($town_list, 0, -1);
	
	return $town_list;
}

function get_suburbs($parent_id) {
	//Vars
	$suburb_name = '';
	$suburb_id = '';
	$suburb_list = '';
	
	$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=? ORDER BY suburb_name";
	$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
	$sql_suburb->bind_param('s',$parent_id);
	$sql_suburb->execute();
	$sql_suburb->bind_result($suburb_id, $suburb_name);
	$sql_suburb->store_result();
	if ($sql_suburb->num_rows == 0) return '-1';
	while ($sql_suburb->fetch()) {
		$suburb_name = str_replace("'", '', $suburb_name);
		$suburb_list .= "$suburb_id|$suburb_name#";
	}
	$suburb_list = substr($suburb_list, 0, -1);
	
	return $suburb_list;
	
}

?>