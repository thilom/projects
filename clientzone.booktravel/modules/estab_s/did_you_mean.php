<?php
/**
 * Checks the levenstein distance between a supplied string and a list of points of interest(POIs) in the DB (nse_landmarks).
 * If the distance falls between 0 and maximum distance the result with the lowest distance is displayed.
 * If an exact match is found or the distance is greater than the maximum distance then nothing is displayed.
 * Searches are limited to a specific town.
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 */

//Checks and Balanceif (!isset($_GET['search'])) die('<body style="margin: 0px; background-color: #D0D0D0"> </body>');
if (!isset($_GET['town_id'])) die('<body style="margin: 0px; background-color: #D0D0D0"> </body>');
if (!isset($_GET['fieldno'])) die('<body style="margin: 0px; background-color: #D0D0D0"> </body>');
if (!isset($_GET['type'])) die('<body style="margin: 0px; background-color: #D0D0D0"> </body>');

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Vars
$max_distance = 10;
$search_string = $_GET['search'];
$town_id = $_GET['town_id'];
$field_no = $_GET['fieldno'];
$type = $_GET['type'];
$landmark_id = '';
$landmark_name = '';
$landmarks = array();
$selected_landmark = array('distance'=>999, 'id'=>'', 'name'=>'');
$exact_count = 0;

//Check for exact match
$statement = "SELECT COUNT(*) FROM nse_landmark WHERE town_id=? && landmark_type_id=? && landmark_name=?";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->bind_param('sss', $town_id, $type, $search_string);
$sql_count->execute();
$sql_count->store_result();
$sql_count->bind_result($exact_count);
$sql_count->fetch();
$sql_count->free_result();
$sql_count->close();
if ($exact_count > 0) die('<body style="margin: 0px; background-color: #D0D0D0"> </body>');


//Get List of POIs
$statement = "SELECT landmark_id, landmark_name FROM nse_landmark WHERE town_id=? && landmark_type_id=?";
$sql_landmarks = $GLOBALS['dbCon']->prepare($statement);
$sql_landmarks->bind_param('ss', $town_id, $type);
$sql_landmarks->execute();
$sql_landmarks->store_result();
$sql_landmarks->bind_result($landmark_id, $landmark_name);
while($sql_landmarks->fetch()) {
	if (!empty($landmark_name)) {
		$landmarks[$landmark_id] = $landmark_name;
	}
}
$sql_landmarks->free_result();
$sql_landmarks->close();

//Check Levenstein distance
foreach ($landmarks as $id=>$name) {
	$distance = levenshtein($search_string, $name);
	if ($distance < $max_distance && $selected_landmark['distance'] > $distance) {
		$selected_landmark['distance'] = $distance;
		$selected_landmark['id'] = $id;
		$selected_landmark['name'] = $name;
	}
}

//Display result
if (!empty($selected_landmark['name'])) {
	echo "<body style='margin: 0px; background-color: #D0D0D0'>";
	echo "<link rel=stylesheet href=/modules/estab_s/html/estab.css>";
	echo "<span class='did_you_mean'>Did You Mean: <a href=javascript:void(0) onCLick=\"parent.return_dym('$field_no','$type','{$selected_landmark['id']}','{$selected_landmark['name']}')\" >{$selected_landmark['name']}</a></span>";
}


?>