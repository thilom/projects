<?php
$year_options = '';
$category_options = '';
$result_options = '';

$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/estab_s/html/awards_form.html');

include $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//year options
$start_year = date('Y');
for ($x=0; $x<10; $x++) {
	$year_options .= "<option value='$start_year'>$start_year</option>";
	$start_year--;
}

//Category list
$statement = "SELECT category_code, category_description FROM nse_award_category ORDER BY category_description";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->execute();
$sql_category->store_result();
$sql_category->bind_result($category_code, $category_name);
while ($sql_category->fetch()) {
	$category_options .= "<option value='$category_code:$category_name'>$category_name</option>";
}
$sql_category->free_result();
$sql_category->close();

//Satus list
$statement = "SELECT result_code, result_name FROM nse_award_result ORDER BY result_name";
$sql_result = $GLOBALS['dbCon']->prepare($statement);
$sql_result->execute();
$sql_result->store_result();
$sql_result->bind_result($result_code, $result_name);
while ($sql_result->fetch()) {
	$result_options .= "<option value='$result_code:$result_name'>$result_name</option>";
}
$sql_result->free_result();
$sql_result->close();

//Replace tags
$template = str_replace('<!-- year_options -->', $year_options, $template);
$template = str_replace('<!-- category_options -->', $category_options, $template);
$template = str_replace('<!-- result_options -->', $result_options, $template);


echo $template;

?>