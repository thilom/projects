<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'estab_s';
$module_name = "Establishment Manager";
$module_icon = 'gohome-64.png';
$module_link = 'estab.php';
$window_width = '1000';
$window_height = '800';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 's'; //a = all
$sub_levels = array();
$sub_levels['manage_estab']['name'] = 'Manage Establishments';
$sub_levels['manage_estab']['file'] = 'estab_list.php';
$sub_levels['manage_estab']['subs']['add']['name'] = 'Create Establishments';
$sub_levels['manage_estab']['subs']['view']['name'] = 'View Establishments';
$sub_levels['manage_estab']['subs']['edit']['name'] = 'Edit Establishments';
$sub_levels['manage_estab']['subs']['delete']['name'] = 'Delete Establishments';
$sub_levels['manage_estab']['subs']['changes']['name'] = 'Manage Establishment Changes';



?>