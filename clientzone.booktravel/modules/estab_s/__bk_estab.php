<?php
/**
 * Manage establishment Data
 *
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 */

//Vars
$function_white_list = array('estab_list', 'estab_view', 'estab_edit', 'estab_add', 'estab_delete', 'dbase_view');
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';
$tables_list = array('nse_export');
$access = array('add_establishment'=>FALSE,
				'edit_establishment'=>FALSE,
				'delete_establishment'=>FALSE,
				'view_establishment'=>FALSE,
				'manage_changes'=>FALSE);

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/estab_s/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';
require_once SITE_ROOT . '/modules/login/tcrypt.class.php';
include_once SITE_ROOT . '/modules/shared/php/clean_up.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

//Prepare Statements
$statement = "SELECT function_id, parent_id, access FROM nse_user_access WHERE user_id=? && module_id=?";
$sql_access = $GLOBALS['dbCon']->prepare($statement);

//Get Access Rights
$sql_access->bind_param('ss', $_SESSION['dbweb_user_id'], $module_id);
$sql_access->execute();
$sql_access->bind_result($function, $parent_id, $user_access);
$sql_access->store_result();
while ($sql_access->fetch()) {
    if ($function == 'add' && 'manage_estab' == $parent_id && '1' == $user_access) $access['add_establishment'] = TRUE;
    if ($function == 'edit' && $parent_id == 'manage_estab' && $user_access == '1') $access['edit_establishment'] = TRUE;
    if ($function == 'delete' && $parent_id == 'manage_estab' && $user_access == '1') $access['delete_establishment'] = TRUE;
    if ($function == 'view' && $parent_id == 'manage_estab' && $user_access == '1') $access['view_establishment'] = TRUE;
    if ($function == 'changes' && $parent_id == 'manage_estab' && $user_access == '1') $access['manage_changes'] = TRUE;
}
$sql_access->close();

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/estab_s/html/estab.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/estab_s/html/estab.js'> </script>";
echo "<script src='/modules/shared/js/notes.js'> </script>";

//Assemble Menu Structure
echo "<script src=/shared/G2.js></script>";
echo "<script src=/shared/tabs.js></script>";
echo "<script>tabs(\"";
echo "<a href=# id=Mq_1In onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Manage Establishments</a>";
echo "\")</script>";
echo "<span class=TabP>";
echo "<p id=Mp_In onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
if ($access['add_establishment']) {
    echo "<a href='/modules/estab_s/estab.php?f=estab_add' onfocus=blur()>Add Establishment</a>";
} else {
    echo "<a href='/modules/estab_s/estab.php?f=no_access' onfocus=blur() style='color: silver'>Add Establishment</a>";
}
if ($access['edit_establishment'] || $access['view_establishment'] || $access['delete_establishment']) {
    echo "<a href='/modules/estab_s/estab.php?f=estab_list' onfocus=blur()>Edit/Delete Establishments</a>";
} else {
    echo "<a href='/modules/estab_s/estab.php?f=no_access' onfocus=blur() style='color: silver'>Edit/Delete Establishments</a>";
}
if ($access['manage_changes']) {
    echo "<a href='/modules/estab_s/estab.php?f=changes' onfocus=blur()>Manage Changes</a>";
} else {
    echo "<a href='/modules/estab_s/estab.php?f=no_access' onfocus=blur() style='color: silver'>Manage Changes</a>";
}
echo "</p>";
echo "</span>";
echo "<br />";

//Map
switch ($function_id) {
	case 'estab_list';
		establishment_list();
		break;
	case 'estab_view';
		establishment_edit_form(TRUE);
		break;
	case 'estab_add':
		if (isset($_POST['insert_estab'])) {
			establishment_add_insert();
		} else if (isset($_POST['updateData'])) {
			establishment_edit_update();
		} else {
			establishment_add_form();
		}
		break;
	case 'estab_edit':
		if (isset($_POST['updateData'])) {
			establishment_edit_update();
		} elseif(isset($_POST['insertData'])) {
			establishment_edit_insert();
		} elseif ($function_id=='estab_add') {
			establishment_edit_form(FALSE, TRUE);
		} else {
			establishment_edit_form();
		}
		break;
	case 'estab_delete':
		establishment_delete();
		break;
	case 'dbase_view':
		dbase_view();
		break;
	default:
		welcome();
}

/**
 * List establishment according to filters and search results.
 */
function establishment_list() {
	//Vars
	$assessment_list = array();
	$establishment_code = '';
	$establishment_name = '';
	$matches = '';
	$nLine = '';
	$province_id = '';
	$country_id = '';
	$town_id = '';
	$where = '';
	$waitlist = 0;
	$waiting_list = array();
	
	//Get Template
	$template = file_get_contents(SITE_ROOT . '/modules/estab_s/html/establishment_list.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
	$lTemplate = $matches[0][0];

	//Get list of assessments waiting for approval
	$statement = "SELECT establishment_code FROM nse_establishment_assessment WHERE assessment_status='waiting'";
	$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessments->execute();
	$sql_assessments->store_result();
	$sql_assessments->bind_result($code);
	while ($sql_assessments->fetch()) {
		$assessment_list[] = $code;
	}
	$sql_assessments->free_result();
	$sql_assessments->close();

	//Get list of stablishment updates waiting for approval
	$statement = "SELECT establishment_code FROM nse_establishment_updates";
	$sql_waiting = $GLOBALS['dbCon']->prepare($statement);
	$sql_waiting->execute();
	$sql_waiting->store_result();
	$sql_waiting->bind_result($code);
	while ($sql_waiting->fetch()) {
		$waiting_list[] = $code;
	}
	$sql_waiting->free_result();
	$sql_waiting->close();
	$waiting_list = array_unique($waiting_list);
	
	//Searches
	if (isset($_GET['sFilter'])) {
		switch ($_GET['sFilter']) {
			case 'establishmentName':
				$where = " WHERE a.establishment_name LIKE '%{$_GET['sString']}%'";
				break;
			case 'establishmentCode':
				$where = " WHERE a.establishment_code LIKE '%{$_GET['sString']}%'";
				break;
			case 'email_address': //Get list of establishment codes with email address
                $email_list = array();
				$email_st = "%{$_GET['sString']}%";
                $statement = "SELECT b.establishment_code
                                FROM nse_user AS a
                                JOIN nse_user_establishments AS b ON a.user_id=b.user_id
                                WHERE a.email LIKE ?";
				$sql_email = $GLOBALS['dbCon']->prepare($statement);
				$sql_email->bind_param('s', $email_st);
				$sql_email->execute();
				$sql_email->store_result();
				$sql_email->bind_result($search_result);
				while ($sql_email->fetch()) {
					$email_list[] = $search_result;
				}
				$sql_email->close();
				$email_list = array_unique($email_list);
				break;
		}
		$link .= "&sFilter={$_GET['sFilter']}&sString={$_GET['sString']}";
	}

	//Filters
	if (isset($_COOKIE['uEstabFilter'])) {
		switch ($_COOKIE['uEstabFilter']) {
			case 'aaqa':
				$where .= empty($where)?"WHERE a.aa_estab = 'Y'":" && a.aa_estab = 'Y'";
				break;
			default:
				//$where = '';
		}
	}
	
	//Prepare Statements - Province Name
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=? LIMIT 1";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Town Name
	$statement = "SELECT town_name FROM nse_location_town WHERE town_id=? LIMIT 1";
	$sql_town = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Country Name
	$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=? LIMIT 1";
	$sql_country = $GLOBALS['dbCon']->prepare($statement);


	//Prepare Statement - Establishment_list
	$statement = "SELECT a.establishment_code, a.establishment_name, b.province_id, b.town_id, b.country_id
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
					$where
					ORDER BY establishment_name";
	$statement .= isset($_COOKIE['uEstabFilter']) && ($_COOKIE['uEstabFilter'] == 'waiting' || $_COOKIE['uEstabFilter'] == 'assessment') || (isset($_GET['sFilter']) && $_GET['sFilter'] == 'email_address') || $_GET['sFilter'] == 'tel'?'':' LIMIT 20';
	$sql_establishment = $GLOBALS['dbCon']->prepare($statement);
	
	//get establishments
	$sql_establishment->execute();
	$sql_establishment->bind_result($establishment_code, $establishment_name, $province_id, $town_id, $country_id);
	$sql_establishment->store_result();
	while ($sql_establishment->fetch()) {
		//Reset Vars
		$province_name = '';
		$town_name = '';
		$country_name = '';
		$buttons = '';
		$wait_counter = 0;
		
		if (isset($_COOKIE['uEstabFilter']) && $_COOKIE['uEstabFilter'] == 'waiting') {
			if (!in_array($establishment_code, $waiting_list)) continue;
		}

		if (isset($_COOKIE['uEstabFilter']) && $_COOKIE['uEstabFilter'] == 'assessment') {
			if (!in_array($establishment_code, $assessment_list)) continue;
		}

		if (isset($_GET['sFilter']) && $_GET['sFilter'] == 'email_address') {
			if (!in_array($establishment_code, $email_list)) continue;
		}

		//Get province Name
		$sql_province->bind_param('s', $province_id);
		$sql_province->execute();
		$sql_province->bind_result($province_name);
		$sql_province->store_result();
		$sql_province->fetch();
		$sql_province->free_result();
		
		//Get Town Name
		$sql_town->bind_param('s', $town_id);
		$sql_town->execute();
		$sql_town->bind_result($town_name);
		$sql_town->store_result();
		$sql_town->fetch();
		$sql_town->free_result();
		
		//Get Country Name
		$sql_country->bind_param('s', $country_id);
		$sql_country->execute();
		$sql_country->bind_result($country_name);
		$sql_country->store_result();
		$sql_country->fetch();
		$sql_country->free_result();
		
		//Buttons
		$buttons .= "<input type=button value='DBase' class=db_button onClick='document.location=\"estab.php?f=dbase_view&id=$establishment_code\"' />";
		if ($GLOBALS['access']['view_establishment']) $buttons .= "<input type=button value='View' class=view_button onClick='document.location=\"estab.php?f=estab_view&id=$establishment_code\"' />";
		if ($GLOBALS['access']['edit_establishment']) $buttons .= "<input type=button value='Edit' class=edit_button onClick='document.location=\"estab.php?f=estab_edit&id=$establishment_code\"' />";
		if ($GLOBALS['access']['delete_establishment']) $buttons .= "<input type=button value='Delete' class=delete_button onClick='delete_estab(\"$establishment_code\")' />";
		
		$line = str_replace('<!-- establishment_name -->', $establishment_name, $lTemplate);
		$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
		$line = str_replace('<!-- location -->', "$town_name, $province_name, $country_name", $line);
		$line = str_replace('<!-- buttons -->', $buttons, $line);

		$nLine .= $line;
	}
	$sql_establishment->free_result();
	$sql_establishment->close();
	$sql_province->close();
	$sql_town->close();
	$sql_country->close();
	
	//Replace Tags
	$template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}

/**
 * Display a form to edit establishment details. Setting the view_only variable to true will display the form in view only format.
 * @param $view_only
 */
function establishment_edit_form($view_only=FALSE, $new=FALSE) {
	//Vars
	$advertiser_code = '';
	$advertiser_name = '';
	$advertiser_options = '';
	$advertiser = '';
	$aa_grade = '';
	$assessor_id = '';
	$assessor_list = '';
	$assessor_name = '';
	$assessor_code = '';
	$awards_list = '';
	
	$billing_line1 = '';
	$billing_line2 = '';
	$billing_line3 = '';
	$billing_code = '';
	$base_tel = '';
	$billing_hash = '';
	
	$company_name = '';
	$company_number = '';
	$contact_line = '';
	$contact_name = '';
	$contact_designation = '';
	$contact_tel = '';
	$contact_cell = '';
	$contact_email = '';
	$contacts_list = '';
	$contact_type = '';
	$contact_value = '';
	$contact_tel1 = '';
	$contact_tel2 = '';
	$contact_fax1 = '';
	$contact_fax2 = '';
	$contact_cell1 = '';
	$contact_cell2 = '';
	$contact_pemail = '';
	$contact_pemail2 = '';
	$contact_pemail3 = '';
	$contact_tel1_description = '';
	$contact_tel2_description = '';
	$contact_fax1_description = '';
	$contact_fax2_description = '';
	$contact_cell1_description = '';
	$contact_cell2_description = '';
	$country_options = '';
	$country_name = '';
	$country_id = '';
	$contact_description = '';
	$cancellation_policy = '';
	$child_policy = '';
	$contact_tel = '';
	$contact_fax = '';
	$category_prefix = '';
	$category = '';
	$category_suffix = '';
	$curr_icon = '';
	$current_restype_id = '';
	$current_star_grading = '';
	$change_name = '';
	$change_value = '';
	$change_list  = '';
	
	$directions = '';
	$default_image = '';
	
	$establishment_name = '';
	
	$facilities = '';
	$form_heading = "New Establishment";
	
	$grade_name = '';
	$grade_code = '';
	$grade_options = '';
	$gps_latitude = '';
	$gps_longitude = '';
	$gps_hash = '';
	
	$image_list = '';
	$image_hash = '';
	$image_id = '';
	$image_name = '';
	$image_title = '';
	$image_description = '';
	$inactive_estab = '';
	$icon_id = '';
	$icon_description = '';
	$icon_src = '';
	$icon_category_id = '';
	$icon = '';
	$icon_category_name = '';
	$invoice_list = '';
	$invoice_id = '';
	$invoice_item = '';
	$invoice_value = '';
	$invoice_paid = '';
	$invoice_date = '';
	
	$landmark_distance = '';
	$landmark_measurement = '';
	$landmark_type_id = '';
	$landmark_type_name = '';
	$landmark_id = '';
	$landmark_name = '';
	$landmark_list = '';
	$last_updated = '';
	$long_description = '';
	$location_country_id = '';
	$location_province_id = '';
	$location_town_id = '';
	$location_suburb_id = '';
	
	$matches = array();
	$map_type_image = '';
	$map_type_google = '';
	$map_type_none = '';
	$map_type = '';
	$measurement_id = '';
	$measurement_name = '';
	
	
	$noprov = FALSE;
	$nightsbridge_id = '';
	
	$pastel_code = '';
	$public_contact_harh = '';
	$postal_line1 = '';
	$postal_line2 = '';
	$postal_line3 = '';
	$postal_code = '';
	$primary_image = '';
	$province_id = '';
	$province_name = '';
	$province_options = '';
	$province_options_status = '';
	$pricing = '';
	$price_options = '<option value=""> </option>';
	$poi_entertainment = '';
	$poi_historical = '';
	$poi_other = '';
	$prebook_id = '';
	$poi_list = '';
	$price_prefixes = array('Unit','Double','Suite','Room','Cave','Site');
	$prefix_options = '<option value=""> </option>';
	$publication_list = '';
	$publication_id = '';
	$publication_name = '';
	$publication_expire = '';
	$publication_added = '';
	$publication_year = '';

    $qaout_date = '';
	
	$representative_list = '';
	$reservation_fax = '';
	$reservation_tel = '';
	$reservation_email = '';
	$reservation_cell = '';
	$reservation_postal1 = '';
	$reservation_postal2 = '';
	$reservation_postal3 = '';
	$reservation_postal_code = '';
	$restype_options_0 = '<option value=0>-- None --</option>';
	$restype_options_1 = '<option value=0>-- None --</option>';
	$restype_options_2 = '<option value=0>-- None --</option>';
	$room_count = '';
	
	$street_line1 = '';
	$street_line2 = '';
	$street_line3 = '';
	$short_description = '';
	$suburb_id = '';
	$suburb_name = '';
	$suburb_options = '';
	$suburb_options_status = '';
	$star_grading_options_0 = '';
	$star_grading_options_1 = '';
	$star_grading_options_2 = '';
	$subcategory_id = '';
	$subcategory_name = '';
	$sgl_prefix_options = '<option value=""> </option>';
	$sgl_category = '';
	$sgl_prefix = '';
	$sgl_suffix = '';
	$priceoutlow = '';
	$sgloutlo = '';
	$sgl_low_options = '<option value=""> </option>';
	$sgl_options ='<option value=""> </option>';
	$price_low_options = '<option value=""> </option>';
		
	$town_id = '';
	$town_name = '';
	$town_options = '';
	$town_options_status = '';
	$toplevel_id = '';
	$toplevel_name = '';
    $tv_licence = '';
    $tv_sets = '';
	
	$vat_number = '';
	
	$website_url = '';	
	
	//Arrays
	$price_codes = array('G' => 'Under R100',
						'F' => 'From R100 - R149',
						'E' => 'From R150 - R199',
						'D' => 'From R200 - R299',
						'C' => 'From R300 - R399',
						'B' => 'From R400 - R499',
						'A' => 'From R500 - R999',
						'A1' => 'Over R1000',
						'A2' => 'Over R2000',
						'A3' => 'Over R3000',
						'A4' => 'Over R4000',
						'A5' => 'Over R5000',
						'A6' => 'Over R6000',
						'A7' => 'Over R7000',);
	
	
	//Establishment Code
	if ($new !== FALSE) {
		$establishment_code = $new;
	} else {
		$establishment_code = isset($_GET['id'])&&ctype_alnum($_GET['id'])&&strlen($_GET['id'])<8?$_GET['id']:'';
	}
	
	//Get Template
	$template = file_get_contents(SITE_ROOT . '/modules/estab_s/html/establishment_edit_form.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	
	//Get Basic Data
	
		//Get Changed Data
		$change_counter = 0;
		$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
		$sql_changes = $GLOBALS['dbCon']->prepare($statement);
		$sql_changes->bind_param('s', $establishment_code);
		$sql_changes->execute();
		$sql_changes->store_result();
		$sql_changes->bind_result($change_name, $change_value);
		while ($sql_changes->fetch()) {
			$change_list .= "chgList['$change_name']='" . str_replace("\r\n",'\n', $change_value) . "'\r\n";
			$change_list .= "chgFields[$change_counter] = '$change_name'\r\n";
			$change_counter++;
		}
		$sql_changes->free_result();
		$sql_changes->close();
		
		$statement = "SELECT establishment_name, last_updated, postal_address_line1, postal_address_line2, postal_address_line3, postal_address_code, street_address_line1, street_address_line2, street_address_line3, aa_category_code, nightsbridge_bbid, prebook_bid, advertiser, website_url, pastel_code, active, assessor_id, enter_date, rep_id FROM nse_establishment WHERE establishment_code=?";
		$sql_basic = $GLOBALS['dbCon']->prepare($statement);
		$sql_basic->bind_param('s', $establishment_code);
		$sql_basic->execute();
		$sql_basic->bind_result($establishment_name, $last_updated, $postal_line1, $postal_line2, $postal_line3, $postal_code, $street_line1, $street_line2, $street_line3, $aa_grade, $nightsbridge_id, $prebook_id, $advertiser, $website_url, $pastel_code, $inactive_estab, $assessor_id, $entered_date, $rep_id);
		$sql_basic->store_result();
		$sql_basic->fetch();
		$sql_basic->free_result();
		$sql_basic->close();
	
		//Inactive Establishment
		$inactive_estab = $inactive_estab == '1'?'':'checked';
		
		//Get assessor list
		$statement = "SELECT CONCAT(firstname, ' ', lastname), user_id FROM nse_user WHERE assessor='1' ORDER BY firstname";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->execute();
		$sql_assessor->store_result();
		$sql_assessor->bind_result($assessor_name, $assessor_code);
		while ($sql_assessor->fetch()) {
			if ($assessor_id == $assessor_code) {
				$assessor_list .= "<option value='$assessor_code' selected >$assessor_name</option>";
			} else {
				$assessor_list .= "<option value='$assessor_code'>$assessor_name</option>";
			}
		}
		$sql_assessor->free_result();
		$sql_assessor->close();

        //Get sales rep list        
		$statement = "SELECT CONCAT(firstname, ' ', lastname), user_id FROM nse_user WHERE sales_rep='1' ORDER BY firstname";
		$sql_rep = $GLOBALS['dbCon']->prepare($statement);
		$sql_rep->execute();
		$sql_rep->store_result();
		$sql_rep->bind_result($rep_name, $rep_code);
		while ($sql_rep->fetch()) {
			if ($rep_id == $rep_code) {
				$representative_list .= "<option value='$rep_code' selected >$rep_name</option>";
			} else {
				$representative_list .= "<option value='$rep_code'>$rep_name</option>";
			}
		}
		$sql_rep->free_result();
		$sql_rep->close();

		//Get Billing Data
		$statement = "SELECT billing_line1, billing_line2, billing_line3, billing_code, vat_number, company_name, company_number FROM nse_establishment_billing WHERE establishment_code=?";
		$sql_billing = $GLOBALS['dbCon']->prepare($statement);
		$sql_billing->bind_param('s', $establishment_code);
		$sql_billing->execute();
		$sql_billing->bind_result($billing_line1, $billing_line2, $billing_line3, $billing_code, $vat_number, $company_name, $company_number);
		$sql_billing->store_result();
		$sql_billing->fetch();
		$sql_billing->free_result();
		$sql_billing->close();

		//Prepare statement - Check if user has admin access
		$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
		$sql_admin = $GLOBALS['dbCon']->prepare($statement);

		//Get & Assemble Contact details
		$counter = 0;
		$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.phone, b.cell, b.email, b.new_password, a.user_id
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE a.establishment_code=?";
		$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
		$sql_contacts->bind_param('s', $establishment_code);
		$sql_contacts->execute();
		$sql_contacts->bind_result($contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $new_password, $user_id);
		$sql_contacts->store_result();
		while ($sql_contacts->fetch()) {
			$sql_admin->bind_param('s', $contact_email);
			$sql_admin->execute();
			$sql_admin->store_result();
			$sql_admin->bind_result($user_counter);
			$sql_admin->fetch();
            $user_counter = !empty($new_password)?'1':'0';
			$contacts_list .= "!|!$user_id|$contact_name|$contact_designation|$contact_tel|$contact_cell|$contact_email|$user_counter|0|$contact_email|0";
			$counter++;
		}
		$sql_contacts->free_result();
		$sql_contacts->close();
	
		//Get Public Contact Details
		$statement = "SELECT contact_type, contact_value, contact_description FROM nse_establishment_public_contact WHERE establishment_code=?";
		$sql_public_contact = $GLOBALS['dbCon']->prepare($statement);
		$sql_public_contact->bind_param('s', $establishment_code);
		$sql_public_contact->execute();
		$sql_public_contact->bind_result($contact_type, $contact_value, $contact_description);
		$sql_public_contact->store_result();
		if ($sql_public_contact->num_rows == 0) {
			//Get Reservation Data
			$statement = "SELECT reservation_tel, reservation_fax, reservation_cell, reservation_email, reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code FROM nse_establishment_reservation WHERE establishment_code=?";
			$sql_reservation = $GLOBALS['dbCon']->prepare($statement);
			$sql_reservation->bind_param('s', $establishment_code);
			$sql_reservation->execute();
			$sql_reservation->store_result();
			$sql_reservation->bind_result($reservation_tel, $reservation_fax, $reservation_cell, $reservation_email, $reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code);
			$sql_reservation->fetch();
			$sql_reservation->free_result();
			
			//Get contact Data
			$statement = "SELECT contact_tel, contact_cell, contact_fax FROM nse_establishment_contact WHERE establishment_code = ?";
			$sql_contact = $GLOBALS['dbCon']->prepare($statement);
			$sql_contact->bind_param('s', $establishment_code);
			$sql_contact->execute();
			$sql_contact->bind_result($contact_tel, $contact_cell, $contact_fax);
			$sql_contact->store_result();
			$sql_contact->fetch();
			$sql_contact->free_result();
			
			//Assemble tel /fax numbers
			$establishment_tel = !empty($contact_tel)?$contact_tel:$establishment_tel;
			$establishment_tel = !empty($base_tel)?$base_tel:$establishment_tel;
			$establishment_tel = !empty($reservation_tel)?$reservation_tel:$establishment_tel;
			$establishment_fax = !empty($contact_fax)?$contact_fax:$establishment_fax;
			if (substr($establishment_tel, 0, 5) == '& Fax') {
				$establishment_fax = substr($establishment_tel, 6);
				$establishment_tel = substr($establishment_tel, 6);
			}
			$establishment_fax = !empty($reservation_fax)?$reservation_fax:$establishment_fax;
			$establishment_cell = !empty($contact_cell)?$contact_cell:$establishment_cell;
			$establishment_cell = !empty($reservation_cell)?$reservation_cell:$establishment_cell;
			if (substr($establishment_tel, -1) == ',' || substr($establishment_tel, -1) == ';') $establishment_tel = substr($establishment_tel, 0, -1);
			if (substr($establishment_fax, -1) == ',' || substr($establishment_fax, -1) == ';') $establishment_fax = substr($establishment_fax, 0, -1);
			if (substr($establishment_cell, -1) == ',' || substr($establishment_cell, -1) == ';') $establishment_cell = substr($establishment_cell, 0, -1);
			
			$contact_tel1 = $establishment_tel;
			$contact_fax1 = $establishment_fax;
			$contact_cell1 = $establishment_cell;
			$contact_pemail = $reservation_email;
		} else {
			while ($sql_public_contact->fetch()) {
				switch ($contact_type) {
					case 'tel1':
						$contact_tel1 = $contact_value;
						$contact_tel1_description = $contact_description;
						break;
					case 'tel2':
						$contact_tel2 = $contact_value;
						$contact_tel2_description = $contact_description;
						break;
					case 'fax1':
						$contact_fax1 = $contact_value;
						$contact_fax1_description = $contact_description;
						break;
					case 'fax2':
						$contact_fax2 = $contact_value;
						$contact_fax2_description = $contact_description;
						break;
					case 'cell1':
						$contact_cell1 = $contact_value;
						$contact_cell1_description = $contact_description;
						break;
					case 'cell2':
						$contact_cell2 = $contact_value;
						$contact_cell2_description = $contact_description;
						break;
					case 'email':
						$contact_pemail = $contact_value;
						break;
					case 'email2':
						$contact_pemail2 = $contact_value;
						break;
					case 'email3':
						$contact_pemail3 = $contact_value;
						break;
				}
			}
		}
		$sql_public_contact->free_result();
		$sql_public_contact->close();

        //Get QA Out date
        $statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=? ORDER BY cancelled_date DESC LIMIT 1";
        $sql_qaout = $GLOBALS['dbCon']->prepare($statement);
        $sql_qaout->bind_param('s', $establishment_code);
        $sql_qaout->execute();
        $sql_qaout->bind_result($qaout_date);
        $sql_qaout->fetch();
        $sql_qaout->free_result();
        $sql_qaout->close();

		//Get publications
		$statement = "SELECT b.publication_id, b.publication_name, a.add_date, b.publication_year, b.expiry_date
						FROM nse_establishment_publications AS a
						JOIN nse_publications AS b ON a.publication_id=b.publication_id
						WHERE a.establishment_code=?";
		$sql_publications = $GLOBALS['dbCon']->prepare($statement);
        echo mysqli_error($GLOBALS['dbCon']);
		$sql_publications->bind_param('s', $establishment_code);
		$sql_publications->execute();
		$sql_publications->store_result();
		$sql_publications->bind_result($publication_id, $publication_name, $publication_added, $publication_year, $publication_expire);
		while ($sql_publications->fetch()) {
			$publication_list .= "$publication_id|$publication_year - $publication_name|$publication_added|$publication_expire#";
		}
		$sql_publications->free_result();
		$sql_publications->close();
		
		//Get awards
		$statement = "SELECT a.award_id, a.year, b.category_code, b.category_description, c.result_code, c.result_name
						FROM award AS a
						JOIN nse_award_category AS b ON a.category=b.category_code
						JOIN nse_award_result AS c ON a.status=c.result_code
						WHERE code=?
						ORDER BY a.year ";
		$sql_awards = $GLOBALS['dbCon']->prepare($statement);
		$sql_awards->bind_param('s', $establishment_code);
		$sql_awards->execute();
		$sql_awards->store_result();
		$sql_awards->bind_result($award_id, $award_year, $category_code, $award_category,$result_code,  $award_status);
		while ($sql_awards->fetch()) {
			$award_category = str_replace(array("/r/n", "\r","\n"), '', $award_category);
			$awards_list .= "$award_id|$award_year|$category_code|$award_category|$result_code|$award_status#";
		}
		$sql_awards->free_result();
		$sql_awards->close();
		
		//Get Images
		$statement = "SELECT image_id, thumb_name, image_title, image_description, primary_image FROM nse_establishment_images WHERE establishment_code=? ORDER BY primary_image DESC, image_name ";
		$sql_images = $GLOBALS['dbCon']->prepare($statement);
		$sql_images->bind_param('s', $establishment_code);
		$sql_images->execute();
		$sql_images->bind_result($image_id, $image_name, $image_title, $image_description, $primary_image);
		$sql_images->store_result();
		$counter = 0;
		while ($sql_images->fetch()) {
			if ($counter==0) $default_image = $image_id;
			$image_description = addslashes($image_description);
			$image_list .= "$image_id|http://www.aatravel.co.za/res_images/$image_name|$image_title|$image_description|";
			//$image_list .= $counter==0?'1':'0';
			$image_list .= "0!|!";
			$counter++;
		}
		$sql_images->free_result();
		$sql_images->close();
	
		//Get Descriptions, policies and pricing
		$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=? && language_code='EN' && description_type=?";
		$sql_description = $GLOBALS['dbCon']->prepare($statement);
		$description_type = 'short_description';
		$sql_description->bind_param('ss', $establishment_code, $description_type);
		$sql_description->execute();
		$sql_description->bind_result($short_description);
		$sql_description->store_result();
		$sql_description->fetch();
		$sql_description->free_result();
		
		$description_type = 'long_description';
		$sql_description->bind_param('ss', $establishment_code, $description_type);
		$sql_description->execute();
		$sql_description->bind_result($long_description);
		$sql_description->store_result();
		$sql_description->fetch();
		$sql_description->free_result();

		//Alternate - Data is returned corrupt when the above mysqli method is used.
		$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code='$establishment_code' && language_code='EN' && description_type='long_description'";
		$sql_description2 = mysql_query($statement);
		list($long_description) = mysql_fetch_array($sql_description2);
		
		$description_type = 'cancellation policy';
		$sql_description->bind_param('ss', $establishment_code, $description_type);
		$sql_description->execute();
		$sql_description->bind_result($cancellation_policy);
		$sql_description->store_result();
		$sql_description->fetch();
		$sql_description->free_result();
		
		$description_type = 'child policy';
		$sql_description->bind_param('ss', $establishment_code, $description_type);
		$sql_description->execute();
		$sql_description->bind_result($child_policy);
		$sql_description->store_result();
		$sql_description->fetch();
		$sql_description->free_result();
		
		$sql_description->close();
		
		if (empty($long_description)) $long_description = $short_description;
	
		
		/* POINTS OF INTEREST */
		
		//Get distance measurements
		$statement = "SELECT landmark_measurement_id, landmark_measurement_name FROM nse_landmark_measurement";
		$sql_measurements = $GLOBALS['dbCon']->prepare($statement);
		$sql_measurements->execute();
		$sql_measurements->store_result();
		$sql_measurements->bind_result($measurement_id, $measurement_name);
		while ($sql_measurements->fetch()) {
			$measurements[$measurement_id] = $measurement_name;
		}
		$sql_measurements->free_result();
		$sql_measurements->close();
		
		//prepare statement - establishment poi
		$statement = "SELECT b.landmark_id, b.landmark_name, a.landmark_distance, a.landmark_measurement
						FROM nse_establishment_landmarks as a
						JOIN nse_landmark as b ON a.landmark_id=b.landmark_id
						WHERE b.landmark_type_id=? && a.establishment_code=?";
		$sql_landmarks = $GLOBALS['dbCon']->prepare($statement);
		
		//Get landmarks
		$statement = "SELECT landmark_type_id, landmark_type_name FROM nse_landmark_types ORDER BY display_order";
		$sql_landmark_types = $GLOBALS['dbCon']->prepare($statement);
		$sql_landmark_types->execute();
		$sql_landmark_types->store_result();
		$sql_landmark_types->bind_result($landmark_type_id, $landmark_type_name);
		while($sql_landmark_types->fetch()) {
			$list_count = 1;
			$landmark_list .= "<TR><TD colspan=10 class=field_label>$landmark_type_name</TD></TR>";
			
			$sql_landmarks->bind_param('ss', $landmark_type_id, $establishment_code);
			$sql_landmarks->execute();
			$sql_landmarks->store_result();
			$sql_landmarks->bind_result($landmark_id, $landmark_name, $landmark_distance, $landmark_measurement);
		while($sql_landmarks->fetch()) {
				$landmark_list .= "<tr>";
				$landmark_list .= "<td width=10px>&nbsp;</td>";
				$landmark_list .= "<td width=10px >$list_count. </td>";
				$landmark_list .= "<td>";
				$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='$landmark_name' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
				$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='$landmark_id' />";
				$landmark_list .= "</td>";
				$landmark_list .= "<td>&nbsp;</td>";
				$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='$landmark_distance' onChange=set_change('poi','poi',this.name) /></td>";
				$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
				foreach ($measurements as $k=>$v) {
					if ($k == $landmark_measurement) {
						$landmark_list .= "<option value='$k' selected>$v</option>";
					} else {
						$landmark_list .= "<option value='$k'>$v</option>";
					}
				}
				$landmark_list .= "</select></td>";
				$landmark_list .= "<td><iframe class=poi_dym id={$landmark_type_id}_iframe_{$list_count}></iframe></td>";
				$landmark_list .= "</tr>";
				$list_count++;
			}
			while ($list_count < 4) {
				$landmark_list .= "<tr>";
				$landmark_list .= "<td width=10px>&nbsp;</td>";
				$landmark_list .= "<td width=10px >$list_count. </td>";
				$landmark_list .= "<td>";
				$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
				$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='' />";
				$landmark_list .= "</td>";
				$landmark_list .= "<td>&nbsp;</td>";
				$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='' onChange=set_change('poi','poi',this.name) /></td>";
				$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
				foreach ($measurements as $k=>$v) {
					$landmark_list .= "<option value='$k'>$v</option>";
				}
				$landmark_list .= "</select></td>";
				$landmark_list .= "<td><iframe class=poi_dym id={$landmark_type_id}_iframe_{$list_count}> </iframe></td>";
				$landmark_list .= "</tr>";
				$list_count++;
			}
			$landmark_list .= "<TR><TD colspan=10>&nbsp;</TD></TR>";
		}
		$sql_landmark_types->free_result();
		$sql_landmark_types->close();
		$sql_landmarks->free_result();
		$sql_landmarks->close();
		
		//Get Pricing Data
		$statement = "SELECT price_description, category_prefix, category, category_suffix, single_category, single_prefix, single_description, category_low, single_category_low FROM nse_establishment_pricing WHERE establishment_code=?";
		$sql_pricing = $GLOBALS['dbCon']->prepare($statement);
		$sql_pricing->bind_param('s', $establishment_code);
		$sql_pricing->execute();
		$sql_pricing->store_result();
		$sql_pricing->bind_result($pricing, $category_prefix, $category, $category_suffix, $sgl_category, $sgl_prefix, $sgl_suffix, $priceoutlow, $sgloutlo);
		$sql_pricing->fetch();
		$sql_pricing->free_result();
		$sql_pricing->close();
		
		//Assemble price category
		foreach ($price_codes as $code=>$icon_description) {
			if ($code == $category) {
				$price_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
			} else {
				$price_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
			}
		}
		foreach ($price_codes as $code=>$icon_description) {
			if ($code == $priceoutlow) {
				$price_low_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
			} else {
				$price_low_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
			}
		}
		foreach ($price_codes as $code=>$icon_description) {
			if ($code == $sgl_category) {
				$sgl_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
			} else {
				$sgl_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
			}
		}
		foreach ($price_codes as $code=>$icon_description) {
			if ($code == $sgloutlo) {
				$sgl_low_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
			} else {
				$sgl_low_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
			}
		}
		
		//Assemble price prefix
		foreach ($price_prefixes as $prefix) {
			if ($category_prefix == $prefix) {
				$prefix_options .= "<OPTION value='$prefix' selected>$prefix</option>";
			} else {
				$prefix_options .= "<OPTION value='$prefix' >$prefix</option>";
			}
		}
		foreach ($price_prefixes as $prefix) {
			if ($sgl_prefix == $prefix) {
				$sgl_prefix_options .= "<OPTION value='$prefix' selected>$prefix</option>";
			} else {
				$sgl_prefix_options .= "<OPTION value='$prefix' >$prefix</option>";
			}
		}
		
		//Get Current Facilities
		$curr_icons = array();
		$statement = "SELECT icon_id FROM nse_establishment_icon WHERE establishment_code=?";
		$sql_icons = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_icons->bind_param('s', $establishment_code);
		$sql_icons->execute();
		$sql_icons->bind_result($icon);
		$sql_icons->store_result();
		while ($sql_icons->fetch()) {
			$curr_icons[$icon] = $icon;
		}
		$sql_icons->free_result();
		$sql_icons->close();
		
		//Get establishment location data
		$statement = "SELECT country_id, province_id, town_id, suburb_id, gps_latitude, gps_longitude FROM nse_establishment_location WHERE establishment_code=?";
		$sql_location = $GLOBALS['dbCon']->prepare($statement);
		$sql_location->bind_param('s', $establishment_code);
		$sql_location->execute();
		$sql_location->bind_result($location_country_id, $location_province_id, $location_town_id, $location_suburb_id, $gps_latitude, $gps_longitude);
		$sql_location->store_result();
		$sql_location->fetch();
		$sql_location->free_result();
		$sql_location->close();
		
		//Get Location Data = Country
		$statement = "SELECT country_id, country_name FROM nse_location_country_lang WHERE language_code='EN' ORDER BY country_name";
		$sql_country = $GLOBALS['dbCon']->prepare($statement);
		$sql_country->execute();
		$sql_country->bind_result($country_id, $country_name);
		$sql_country->store_result();
		$country_options .= "<option value='0'>-- Select Country --</option>";
		while ($sql_country->fetch()) {
			if ($location_country_id == $country_id) {
				$country_options .= "<option value='$country_id' selected >$country_name</option>";
			} else {
				$country_options .= "<option value='$country_id'>$country_name</option>";
			}
		}
		
		//Get Location Data - Province
		$statement = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=? ORDER BY province_name";
		$sql_province = $GLOBALS['dbCon']->prepare($statement);
		$sql_province->bind_param('s', $location_country_id);
		$sql_province->execute();
		$sql_province->bind_result($province_id, $province_name);
		$sql_province->store_result();
		if ($sql_province->num_rows == 0) {
			$province_options = "<option>-- No Provinces For Selected Country --</option>";
			$province_options_status = 'disabled';
			$noprov = TRUE;
		} else {
			$province_options .= "<option value='0'>-- Select Province --</option>";
			while ($sql_province->fetch()) {
				if ($location_province_id == $province_id) {
					$province_options .= "<option value='$province_id' selected>$province_name</option>";
				} else {
					$province_options .= "<option value='$province_id'>$province_name</option>";
				}
			}
		}
		
		//Get Location Data - Towns
		if ($noprov) {
			$statement = "SELECT town_id, town_name FROM nse_location_town WHERE country_id=? ORDER BY town_name";
			$sql_towns = $GLOBALS['dbCon']->prepare($statement);
			$sql_towns->bind_param('s', $location_country_id);
		} else {
			$statement = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=? ORDER BY town_name";
			$sql_towns = $GLOBALS['dbCon']->prepare($statement);
			$sql_towns->bind_param('s', $location_province_id);
		}
		$sql_towns->execute();
		$sql_towns->bind_result($town_id, $town_name);
		$sql_towns->store_result();
		if ($sql_towns->num_rows == 0) {
			$town_options = "-- No Towns For Selected Province --";
			$town_options_status = 'disabled';
		} else {
			$town_options .= "<option value='0'>-- Select Town --</option>";
			while ($sql_towns->fetch()) {
				if (strpos($town_name, 'Aaa') !== FALSE) continue;
				if (strpos($town_name, 'Zzz') !== FALSE) continue;
				if (strpos($town_name, '***') !== FALSE) continue;
				if ($location_town_id == $town_id) {
					$town_options .= "<option value='$town_id' selected >$town_name</option>";
				} else {
					$town_options .= "<option value='$town_id' >$town_name</option>";
				}
			}
		}
		
		//Get Location Data - Suburbs
		$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=? ORDER BY suburb_name";
		$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb->bind_param('s', $location_town_id);
		$sql_suburb->execute();
		$sql_suburb->bind_result($suburb_id, $suburb_name);
		$sql_suburb->store_result();
		if ($sql_suburb->num_rows == 0) {
			$suburb_options = "-- No Suburbs For Selected Town --";
			$suburb_options_status = 'disabled';
		} else {
			$suburb_options .= "<option value='0'>-- Select Suburb --</option>";
			while ($sql_suburb->fetch()) {
				if ($location_suburb_id == $suburb_id) {
					$suburb_options .= "<option value='$suburb_id' selected >$suburb_name</option>";
				} else {
					$suburb_options .= "<option value='$suburb_id' >$suburb_name</option>";
				}
			}
		}
		
		//Get Directions
		$statement = "SELECT directions, map_type FROM nse_establishment_directions WHERE establishment_code=?";
		$sql_directions = $GLOBALS['dbCon']->prepare($statement);
		$sql_directions->bind_param('s', $establishment_code);
		$sql_directions->execute();
		$sql_directions->bind_result($directions, $map_type);
		$sql_directions->store_result();
		$sql_directions->fetch();
		$sql_directions->free_result();
		$sql_directions->close();
		$directions = stripslashes($directions);
		switch ($map_type) {
			case 'none':
				$map_type_none = 'checked';
				break;
			case 'google':
				$map_type_google = 'checked';
				break;
			case 'upload':
				$map_type_image = 'checked';
				break;
			default:
				$map_type_none = 'checked';
		}
		
		//Get Current Restype
		$counter = 0;
		$statement = "SELECT subcategory_id, star_grading FROM nse_establishment_restype WHERE establishment_code=?";
		$sql_restype_current = $GLOBALS['dbCon']->prepare($statement);
		$sql_restype_current->bind_param('s', $establishment_code);
		$sql_restype_current->execute();
		$sql_restype_current->store_result();
		$sql_restype_current->bind_result($current_restype_id, $current_star_grading);
		while ($sql_restype_current->fetch()) {
			$current_restype[$counter] = $current_restype_id;
			$current_grading[$counter] = $current_star_grading;
			$counter++;
		}
		$sql_restype_current->free_result();
		$sql_restype_current->close();
		
		//Get AA Grades List
		$grade_options .= "<option value=''>N/A</option>";
		$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category";
		$sql_aa_categories = $GLOBALS['dbCon']->prepare($statement);
		$sql_aa_categories->execute();
		$sql_aa_categories->store_result();
		$sql_aa_categories->bind_result($grade_code, $grade_name);
		while ($sql_aa_categories->fetch()) {
			if ($grade_code == $aa_grade) {
				$grade_options .= "<option value='$grade_code' selected>$grade_name</option>";
			} else {
				$grade_options .= "<option value='$grade_code'>$grade_name</option>";
			}
		}
		$sql_aa_categories->free_result();
		$sql_aa_categories->close();
		
		//Get Advertisers
		$advertiser_options .= "<option value=''>N/A</option>";
		$statement = "SELECT advertiser_code, advertiser_name FROM nse_advertiser ORDER BY advertiser_name";
		$sql_advertiser = $GLOBALS['dbCon']->prepare($statement);
		$sql_advertiser->execute();
		$sql_advertiser->store_result();
		$sql_advertiser->bind_result($advertiser_code, $advertiser_name);
		while ($sql_advertiser->fetch()) {
			if ($advertiser == $advertiser_code) {
				$advertiser_options .= "<option value='$advertiser_code' selected>$advertiser_name</option>";
			} else {
				$advertiser_options .= "<option value='$advertiser_code'>$advertiser_name</option>";
			}
		}
		$sql_advertiser->free_result();
		$sql_advertiser->close();
		
		
		$form_heading = "$establishment_name ($establishment_code)";
	
	
	//Get restypes list
	$statement = "SELECT toplevel_id, toplevel_name FROM nse_restype_toplevel ORDER BY toplevel_id";
	$sql_restype_toplevel = $GLOBALS['dbCon']->prepare($statement);
		
	$statement = "SELECT DISTINCT(a.subcategory_name), a.subcategory_id
					FROM nse_restype_subcategory_lang AS a
					JOIN nse_restype_subcategory_parent AS b ON a.subcategory_id=b.subcategory_id
					JOIN nse_restype_category AS c ON c.category_id=b.category_id
					WHERE c.toplevel_id=?
					ORDER BY a.subcategory_name";
	$sql_restypes = $GLOBALS['dbCon']->prepare($statement);
	
	$sql_restype_toplevel->execute();
	$sql_restype_toplevel->store_result();
	$sql_restype_toplevel->bind_result($toplevel_id, $toplevel_name);
	while ($sql_restype_toplevel->fetch()) {
		$restype_options_0 .= "<optgroup label='$toplevel_name' >";
		$restype_options_1 .= "<optgroup label='$toplevel_name' >";
		$restype_options_2 .= "<optgroup label='$toplevel_name' >";
			
		$sql_restypes->bind_param('s', $toplevel_id);
		$sql_restypes->execute();
		$sql_restypes->store_result();
		$sql_restypes->bind_result($subcategory_name, $subcategory_id);
		while ($sql_restypes->fetch()) {
			if (isset($current_restype[0]) && $subcategory_id == $current_restype[0]) {
				$restype_options_0 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_0 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}

			if (isset($current_restype[1]) && $subcategory_id == $current_restype[1]) {
				$restype_options_1 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_1 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
			if (isset($current_restype[2]) && $subcategory_id == $current_restype[2]) {
				$restype_options_2 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_2 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
		}
		$restype_options_0 .= "</optgroup>";
		$restype_options_1 .= "</optgroup>";
		$restype_options_2 .= "</optgroup>";
	}
	$sql_restype_toplevel->free_result();
	$sql_restype_toplevel->close();
	$sql_restypes->free_result();
	$sql_restypes->close();
	
	//Get stargradings list
	for ($x=0; $x<6; $x++) {
		if (isset($current_grading[0]) && $x == $current_grading[0]) {
			$star_grading_options_0 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_0 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[1]) && $x == $current_grading[1]) {
			$star_grading_options_1 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_1 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[2]) && $x == $current_grading[2]) {
			$star_grading_options_2 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_2 .= "<option value=$x>$x</option>";
		}
	}

	//Get Room count & tv licencu
	$statement = 'SELECT room_count, room_type, tv_licence, tv_count FROM nse_establishment_data WHERE establishment_code=?';
	$sql_rooms = $GLOBALS['dbCon']->prepare($statement);
	$sql_rooms->bind_param('s', $establishment_code);
	$sql_rooms->execute();
	$sql_rooms->store_result();
	$sql_rooms->bind_result($room_count, $room_type, $tv_licence, $tv_sets);
	$sql_rooms->fetch();
	$sql_rooms->free_result();
	$sql_rooms->close();
	
	//Assemble room type
	$room_options = '';
	$room_types = array('Rooms', 'Units', 'Apartments','Chalets', 'Cottages');
	foreach ($room_types AS $type) {
		if ($type == $room_type) {
			$room_options .= "<option value='$type' selected >$type</options>";
		} else {
			$room_options .= "<option value='$type'>$type</options>";
		}
	}
	
	
	//Get Facilities
	$facilities = "";
	$lRowsCount = 0;
	$rRowsCount = 0;
	$colLength = 3;
	$lRow = '<table width=100% cellpadding=0 cellspacing=0>';
	$rRow = '<table width=100%  cellpadding=0 cellspacing=0>';

	$statement = "SELECT a.icon_id, a.icon_url, b.icon_description
					FROM nse_icon AS a
					JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
					WHERE a.icon_category_id=?";
	$sql_facility_data = $GLOBALS['dbCon']->prepare($statement);

	$statement = "SELECT b.icon_name, a.icon_category_id
					FROM nse_icon_category AS a
					JOIN nse_icon_category_lang AS b ON a.icon_category_id=b.icon_category_id
					ORDER BY a.icon_priority ";
	$sql_facilities = $GLOBALS['dbCon']->prepare($statement);
	$sql_facilities->execute();
	$sql_facilities->bind_result($icon_category_name, $icon_category_id);
	$sql_facilities->store_result();
	while ($sql_facilities->fetch()) {
//		if ($icon_category_name == 'Affiliations') continue;
		$col = 0;
		$sql_facility_data->bind_param('s', $icon_category_id);
		$sql_facility_data->execute();
		$sql_facility_data->bind_result($icon_id, $icon_src, $icon_description);
		$sql_facility_data->store_result();
		//if ($lRowsCount < $rRowsCount) {
			$lRow .= "<tr><td colspan=10 style='padding: 20px 0px 5px 0px'><b>$icon_category_name</b></td></tr>";
			while ($sql_facility_data->fetch()) {
				//$src_off = str_replace('.gif', '_off.gif', $icon_src);
				if ($col == 0) {
					$lRow .= "<tr valign=top>";
					$lRowsCount++;
				}
				$lRow .= "<td><input type=checkbox name='facilities[]' id='facilities_$icon_id' value='$icon_id' onChange=set_change('facility','facility',this.id) ";
				if (isset($curr_icons[$icon_id])) {
					$lRow .= 'checked';
				}
				$lRow .= " ></td><td><label for='facilities_$icon_id'><img src='$icon_src' width=33 height=35 style='margin: 1px'></label></td><td style='font-size: 12px'><label for='facilities_$icon_id'>$icon_description</label></td>";
				$col++;
				if ($col == $colLength) {
					$lRow .= "</tr>";
					$col = 0;
				} else {
					$lRow .= "<td>&nbsp;</td>";
				}
			}
			if ($col < $colLength) $lRow .= "";
	}
	$sql_facilities->free_result();
	$sql_facilities->close();
	
	$lRow .= '</table>';
	$rRow .= '</table>';
	$facilities .= "<table width=100% ><tr valign=top>";
	$facilities .= "<td>$lRow</td><td>&nbsp;&nbsp;&nbsp;</td><td>$rRow</td>";
	$facilities.= "</tr></table>";
	
//	//Get invoices
//	$statement = "SELECT invoice_id, line_date, line_item, line_cost, line_paid FROM nse_establishment_invoice WHERE establishment_code=? ORDER BY line_date DESC";
//	$sql_invoice = $GLOBALS['dbCon']->prepare($statement);
//	$sql_invoice->bind_param('s', $establishment_code);
//	$sql_invoice->execute();
//	$sql_invoice->store_result();
//	$sql_invoice->bind_result($invoice_id, $invoice_date, $invoice_item, $invoice_value, $invoice_paid);
//	while ($sql_invoice->fetch()) {
//		$invoice_list .= "$invoice_id|$invoice_date|$invoice_item|$invoice_value|$invoice_paid|0!|!";
//	}
//	$sql_invoice->free_result();
//	$sql_invoice->close();
	$invoice_list = '';

	//Get notes
	$notes = "<table id=notes_list >";
	$statement = "SELECT user_name, note_timestamp, note_content FROM nse_establishment_notes WHERE establishment_code=? ORDER BY note_timestamp DESC";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);
	$sql_notes->bind_param('s', $establishment_code);
	$sql_notes->execute();
	$sql_notes->store_result();
	$sql_notes->bind_result($user_name, $note_timestamp, $note_content);
	while ($sql_notes->fetch()) {
		$date = date('d-m-Y h:i',$note_timestamp);
		$notes .= "<tr>";
		$notes .= "<td class=note_name>$user_name</td>";
		$notes .= "<td class=note_date>$date</td>";
		$notes .= "<td class=note_content colspan=2>$note_content</td>";
		$notes .= "</tr>";
	}
	$sql_notes->free_result();
	$sql_notes->close();
	$notes .= "</table>";

	//Replace Tags
	$template = str_replace('<!-- form_heading -->', $form_heading, $template);
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_Replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- billing1 -->', $billing_line1, $template);
	$template = str_replace('<!-- billing2 -->', $billing_line2, $template);
	$template = str_replace('<!-- billing3 -->', $billing_line3, $template);
	$template = str_replace('<!-- billing_code -->', $billing_code, $template);
	$template = str_replace('<!-- vat_number -->', $vat_number, $template);
	$template = str_replace('<!-- company_number -->', $company_number, $template);
	$template = str_replace('<!-- company_name -->', $company_name, $template);
	$template = str_replace('<!-- contacts_list -->', $contacts_list, $template);
    $template = str_replace('!qaout_date!', $qaout_date, $template);
	$template = str_replace('<!-- tel1 -->', $contact_tel1, $template);
	$template = str_replace('<!-- tel2 -->', $contact_tel2, $template);
	$template = str_replace('<!-- fax1 -->', $contact_fax1, $template);
	$template = str_replace('<!-- fax2 -->', $contact_fax2, $template);
	$template = str_replace('<!-- cell1 -->', $contact_cell1, $template);
	$template = str_replace('<!-- cell2 -->', $contact_cell2, $template);
	$template = str_replace('<!-- tel1_description -->', $contact_tel1_description, $template);
	$template = str_replace('<!-- tel2_description -->', $contact_tel2_description, $template);
	$template = str_replace('<!-- fax1_description -->', $contact_fax1_description, $template);
	$template = str_replace('<!-- fax2_description -->', $contact_fax2_description, $template);
	$template = str_replace('<!-- cell1_description -->', $contact_cell1_description, $template);
	$template = str_replace('<!-- cell2_description -->', $contact_cell2_description, $template);
	$template = str_replace('<!-- email -->', $contact_pemail, $template);
	$template = str_replace('<!-- email2 -->', $contact_pemail2, $template);
	$template = str_replace('<!-- email3 -->', $contact_pemail3, $template);
	$template = str_replace('<!-- postal1 -->', $postal_line1, $template);
	$template = str_replace('<!-- postal2 -->', $postal_line2, $template);
	$template = str_replace('<!-- postal3 -->', $postal_line3, $template);
	$template = str_replace('<!-- postal_code -->', $postal_code, $template);
	$template = str_replace('<!-- street1 -->', $street_line1, $template);
	$template = str_replace('<!-- street2 -->', $street_line2, $template);
	$template = str_replace('<!-- street3 -->', $street_line3, $template);
	$template = str_replace('<!-- image_list -->', $image_list, $template);
	$template = str_replace('<!-- short_description -->', $short_description, $template);
	$template = str_replace('<!-- long_description -->', $long_description, $template);
	$template = str_replace('<!-- facilities -->', $facilities, $template);
	$template = str_replace('<!-- province_options -->', $province_options, $template);
	$template = str_replace('<!-- country_options -->', $country_options, $template);
	$template = str_replace('<!-- province_options_status -->', $province_options_status, $template);
	$template = str_replace('<!-- town_options -->', $town_options, $template);
	$template = str_replace('<!-- town_options_status -->', $town_options_status, $template);
	$template = str_replace('<!-- suburb_options -->', $suburb_options, $template);
	$template = str_replace('<!-- suburb_options_status -->', $suburb_options_status, $template);
	$template = str_replace('<!-- dd_lat -->', $gps_latitude, $template);
	$template = str_replace('<!-- dd_lon -->', $gps_longitude, $template);
	$template = str_replace('<!-- pricing -->', $pricing, $template);
	$template = str_replace('<!-- child -->', $child_policy, $template);
	$template = str_replace('<!-- cancellation -->', $cancellation_policy, $template);
	$template = str_replace('<!-- map_type_none -->', $map_type_none, $template);
	$template = str_replace('<!-- map_type_google -->', $map_type_google, $template);
	$template = str_replace('<!-- map_type_image -->', $map_type_image, $template);
	$template = str_replace('<!-- directions -->', $directions, $template);
	$template = str_replace('<!-- price_options -->', $price_options, $template);
	$template = str_replace('<!-- publication_list -->', $publication_list, $template);
	$template = str_replace('<!-- awards_list -->', $awards_list, $template);
	$template = str_replace('<!-- publications -->', '', $template);
	$template = str_replace('<!-- invoice_list -->', $invoice_list, $template);
	$template = str_replace('!pastel_code!', $pastel_code, $template);
	$template = str_replace('!room_count!', $room_count, $template);
	$template = str_replace('!inactive_estab!', $inactive_estab, $template);
	$template = str_replace('<!-- assessor_list -->', $assessor_list, $template);
	$template = str_replace('!entered_date!', $entered_date, $template);
	$template = str_replace('<!-- notes -->', $notes, $template);
    $template = str_replace('<!-- representative_list -->', $representative_list, $template);
    $template = str_replace('!tv_licence!', $tv_licence, $template);
    $template = str_replace('!tv_sets!', $tv_sets, $template);
	
	$template = str_replace('<!-- sgl_options -->', $sgl_options, $template);
	$template = str_replace('<!-- price_low_options -->', $price_low_options, $template);
	$template = str_replace('<!-- sgl_low_options -->', $sgl_low_options, $template);
	
	$template = str_replace('<!-- price_prefix -->', $prefix_options, $template);
	$template = str_replace('<!-- category_suffix -->', $category_suffix, $template);
	$template = str_replace('<!-- sgl_suffix -->', $sgl_suffix, $template);
	$template = str_replace('<!-- sgl_prefix -->', $sgl_prefix_options, $template);
	$template = str_replace('<!-- restype_0 -->', $restype_options_0, $template);
	$template = str_replace('<!-- restype_1 -->', $restype_options_1, $template);
	$template = str_replace('<!-- restype_2 -->', $restype_options_2, $template);
	$template = str_replace('<!-- star_grading_0 -->', $star_grading_options_0, $template);
	$template = str_replace('<!-- star_grading_1 -->', $star_grading_options_1, $template);
	$template = str_replace('<!-- star_grading_2 -->', $star_grading_options_2, $template);
	$template = str_replace('<!-- poi_list -->', $landmark_list, $template);
	$template = str_Replace('<!-- grade_options -->', $grade_options, $template);
	$template = str_Replace('<!-- nightsbridge -->', $nightsbridge_id, $template);
	$template = str_Replace('<!-- prebook -->', $prebook_id, $template);
	$template = str_replace('<!-- advertiser_options -->', $advertiser_options, $template);
	$template = str_replace('<!-- website -->', $website_url, $template);
	$template = str_replace('<!-- change_list -->', $change_list, $template);
	$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- room_options -->', $room_options, $template);
	
	$template = str_replace('<!-- api_key -->', $GLOBALS['api_key'], $template);
	$template = str_replace('<!-- default_image -->', $default_image, $template);
	$template = str_replace('<!-- button_name -->', 'updateData', $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}

/**
 * Update establishment data
 */
function establishment_edit_insert() {
	//Vars
	$vat_number = ctype_alnum($_POST['vat_number'])?$_POST['vat_number']:'';
	$billing_line1 = count($_POST['billing_line1'])<100?$_POST['billing_line1']:'';
	$billing_line2 = count($_POST['billing_line2'])<100?$_POST['billing_line2']:'';
	$billing_line3 = count($_POST['billing_line3'])<100?$_POST['billing_line3']:'';
	$billing_code = count($_POST['billing_code'])<100?$_POST['billing_code']:'';
	$establishment_name = $_POST['establishment_name'];
	$contacts_list = $_POST['contacts'];
	$contact_tel1 = $_POST['tel1'];
	$contact_tel2 = $_POST['tel2'];
	$contact_fax1 = $_POST['fax1'];
	$contact_fax2 = $_POST['fax2'];
	$contact_cell1 = $_POST['cell1'];
	$contact_cell2 = $_POST['cell2'];
	$contact_email = $_POST['email'];
	$contact_tel1_description = $_POST['tel1_description'];
	$contact_tel2_description = $_POST['tel2_description'];
	$contact_fax1_description = $_POST['fax1_description'];
	$contact_fax2_description = $_POST['fax2_description'];
	$contact_cell1_description = $_POST['cell1_description'];
	$contact_cell2_description = $_POST['cell2_description'];
	$postal_line1 = $_POST['postal1'];
	$postal_line2 = $_POST['postal2'];
	$postal_line3 = $_POST['postal3'];
	$postal_code = $_POST['postal_code'];
	$street_line1 = $_POST['street1'];
	$street_line2 = $_POST['street2'];
	$street_line3 = $_POST['street3'];
	$image_list = $_POST['images'];
	$new_country = isset($_POST['country_id']) && ctype_digit($_POST['country_id'])?$_POST['country_id']:'';
	$new_province = isset($_POST['province_id']) && ctype_digit($_POST['province_id'])?$_POST['province_id']:'';
	$new_town = isset($_POST['town_id']) && ctype_digit($_POST['town_id'])?$_POST['town_id']:'';
	$new_suburb = isset($_POST['suburb_id']) && ctype_digit($_POST['suburb_id'])?$_POST['suburb_id']:'';
	$current_country = '';
	$current_province = '';
	$current_town = '';
	$current_suburb = '';
	$location_change = FALSE;
	$gps_latitude = $_POST['dd_lat'];
	$gps_longitude = $_POST['dd_lon'];
	$cancellation_policy = $_POST['cancellation_policy'];
	$child_policy = $_POST['child_policy'];
	$directions = $_POST['directions'];
	$map_type = $_POST['map_type'];
	$delete_images = array();
	$update_images = array();
	$curr_images = array();
	$curr_image_id = '';
	$curr_image_name = '';
	$curr_thumb_name = '';
	$pricing = $_POST['pricing'];
	$description['short_description'] = $_POST['short_description'];
	$description['long_description'] = $_POST['long_description'];
	$description['poi_entertainment'] = $_POST['poi_entertainment'];
	$description['poi_historical'] = $_POST['poi_historical'];
	$description['poi_other'] = $_POST['poi_other'];
	$pricing_prefix = $_POST['pricing_prefix'];
	$pricing_category_hi = $_POST['pricing_category_hi'];
	$pricing_category_low = $_POST['pricing_category_low'];
	$category_suffix = $_POST['category_suffix'];
	$sgl_prefix = $_POST['sgl_prefix'];
	$sgl_category_hi = $_POST['sgl_category_hi'];
	$sgl_category_low = $_POST['sgl_category_low'];
	$sgl_suffix = $_POST['sgl_suffix'];
	
	//Calculate Establishment Code
	include_once 'establishment.php';
	$estab = new establishment();
	$establishment_code = $estab->next_establishment_code();
	$establishment_code = trim($establishment_code);
	
	//Insert Contacts
	$statement = "INSERT INTO nse_establishment (postal_address_line1, postal_address_line2, postal_address_line3, postal_address_code, street_address_line1, street_address_line2, street_address_line3, establishment_name, establishment_code) VALUES (?,?,?,?,?,?,?,?,?)";
	$sql_postal = $GLOBALS['dbCon']->prepare($statement);
	$sql_postal->bind_param('sssssssss', $postal_line1, $postal_line2, $postal_line3, $postal_code, $street_line1, $street_line2, $street_line3, $establishment_name, $establishment_code);
	$sql_postal->execute();
	$sql_postal->close();
	
	//Insert Billing Info
	$statement = "INSERT INTO nse_establishment_billing (billing_line1, billing_line2, billing_line3, billing_code, vat_number, establishment_code) VALUES (?,?,?,?,?,?)";
	$sql_billing = $GLOBALS['dbCon']->prepare($statement);
	$sql_billing->bind_param('ssssss', $billing_line1, $billing_line2, $billing_line3, $billing_code, $vat_number, $establishment_code);
	$sql_billing->execute();
	$sql_billing->close();
		
	$statement = "INSERT INTO nse_establishment_contacts (establishment_code, contact_name, contact_designation, contact_tel, contact_cell, contact_email) VALUES (?,?,?,?,?,?)";
	$sql_contacts_insert = $GLOBALS['dbCon']->prepare($statement);
	$contact_line = explode("!|!", $contacts_list);
	foreach ($contact_line as $line) {
		if (empty($line)) continue;
		list($line_id, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email) = explode('|', $line);
		$sql_contacts_insert->bind_param('ssssss', $establishment_code, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
		$sql_contacts_insert->execute();
	}
	$sql_contacts_insert->close();
	
	//Insert Location Data
	$statement = "INSERT INTO nse_establishment_location (establishment_code, country_id, province_id, town_id, suburb_id, gps_latitude, gps_longitude) VALUES (?,?,?,?,?,?,?)";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('sssssss', $establishment_code, $new_country, $new_province, $new_town, $new_suburb, $gps_latitude, $gps_longitude);
	$sql_location->execute();
	$sql_location->close();
	
	//Insert Public Contacts
	$statement = "INSERT INTO nse_establishment_public_contact (establishment_code, contact_type, contact_value, contact_description) VALUES (?,?,?,?)";
	$sql_contact_insert = $GLOBALS['dbCon']->prepare($statement);
	
	if (!empty($contact_tel1)) {
		$contact_type = 'tel1';
		$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_tel1, $contact_tel1_description);
		$sql_contact_insert->execute();
	}
	
	if (!empty($contact_tel2)) {
		$contact_type = 'tel2';
		$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_tel2, $contact_tel2_description);
		$sql_contact_insert->execute();
	}
	
	if (!empty($contact_fax1)) {
		$contact_type = 'fax1';
		$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_fax1, $contact_fax1_description);
		$sql_contact_insert->execute();
	}
	
	if (!empty($contact_fax2)) {
		$contact_type = 'fax2';
		$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_fax2, $contact_fax2_description);
		$sql_contact_insert->execute();
	}
	
	if (!empty($contact_cell1)) {
		$contact_type = 'cell1';
		$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_cell1, $contact_cell1_description);
		$sql_contact_insert->execute();
	}
	
	if (!empty($contact_cell2)) {
		$contact_type = 'cell2';
		$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_cell2, $contact_cell2_description);
		$sql_contact_insert->execute();
	}
	
	if (!empty($contact_email)) {
		$contact_type = 'email';
		$contact_email_description = '';
		$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_email, $contact_email_description);
		$sql_contact_insert->execute();
	}
	
	$sql_contact_insert->close();
	
	
	//Insert Descriptions
	$statement = "INSERT INTO nse_establishment_descriptions (language_code, description_type, establishment_description, establishment_code) VALUES ('EN',?,?,?)";
	$sql_policy_insert = $GLOBALS['dbCon']->prepare($statement);
	
	$description_type = 'child policy';
	$sql_policy_insert->bind_param('sss', $description_type, $child_policy, $establishment_code);
	$sql_policy_insert->execute();
	
	$description_type = 'cancellation policy';
	$sql_policy_insert->bind_param('sss', $description_type, $cancellation_policy, $establishment_code);
	$sql_policy_insert->execute();
	
	$sql_policy_insert->close();
	
	print_r($_POST);
}

/**
 * Updates Establishment Tables
 *
 * - Changes are only updates if they have been marked as changed in the javascript ($_POST['change_list'].
 *
 */
function establishment_edit_update() {
	
	//Vars
	$establishment_code = isset($_GET['id'])?$_GET['id']:$_POST['establishment_code'];
	$aa_estab_count = '';
	$awards = $_POST['awards'];
	$error_message = '';
	$vat_number = $_POST['vat_number'];
	$billing_line1 = count($_POST['billing_line1'])<100?$_POST['billing_line1']:'';
	$billing_line2 = count($_POST['billing_line2'])<100?$_POST['billing_line2']:'';
	$billing_line3 = count($_POST['billing_line3'])<100?$_POST['billing_line3']:'';
	$billing_code = count($_POST['billing_code'])<100?$_POST['billing_code']:'';
	$establishment_name = $_POST['establishment_name'];
	$contacts_list = $_POST['contacts'];
	$contact_tel1 = $_POST['tel1'];
	$contact_tel2 = $_POST['tel2'];
	$contact_fax1 = $_POST['fax1'];
	$contact_fax2 = $_POST['fax2'];
	$contact_cell1 = $_POST['cell1'];
	$contact_cell2 = $_POST['cell2'];
	$contact_email = $_POST['email'];
	$contact_email2 = $_POST['email2'];
	$contact_email3 = $_POST['email3'];
	$contact_tel1_description = $_POST['tel1_description'];
	$contact_tel2_description = $_POST['tel2_description'];
	$contact_fax1_description = $_POST['fax1_description'];
	$contact_fax2_description = $_POST['fax2_description'];
	$contact_cell1_description = $_POST['cell1_description'];
	$contact_cell2_description = $_POST['cell2_description'];
	$postal_line1 = $_POST['postal1'];
	$postal_line2 = $_POST['postal2'];
	$postal_line3 = $_POST['postal3'];
	$postal_code = $_POST['postal_code'];
	$street_line1 = $_POST['street1'];
	$street_line2 = $_POST['street2'];
	$street_line3 = $_POST['street3'];
	$image_list = $_POST['images'];
	$new_country = isset($_POST['country_id']) && ctype_digit($_POST['country_id'])?$_POST['country_id']:'';
	$new_province = isset($_POST['province_id']) && ctype_digit($_POST['province_id'])?$_POST['province_id']:'';
	$new_town = isset($_POST['town_id']) && ctype_digit($_POST['town_id'])?$_POST['town_id']:'';
	$new_suburb = isset($_POST['suburb_id']) && ctype_digit($_POST['suburb_id'])?$_POST['suburb_id']:'';
	$current_country = '';
	$current_province = '';
	$current_town = '';
	$current_suburb = '';
	$location_change = FALSE;
	$gps_latitude = $_POST['dd_lat'];
	$gps_longitude = $_POST['dd_lon'];
	$cancellation_policy = $_POST['cancellation_policy'];
	$child_policy = $_POST['child_policy'];
	$directions = $_POST['directions'];
	$map_type = $_POST['map_type'];
	$delete_images = array();
	$update_images = array();
	$curr_images = array();
	$curr_image_id = '';
	$curr_image_name = '';
	$curr_thumb_name = '';
	$pricing = $_POST['pricing'];
	$description['short_description'] = $_POST['short_description'];
	$description['long_description'] = $_POST['long_description'];
	$entered_date = $_POST['entered_date'];
	$company_name = $_POST['company_name'];
	$company_number = $_POST['company_number'];
	$nightsbridge_code = $_POST['nightsbridge'];
	$prebook_code = $_POST['prebook'];
	$advertiser = $_POST['advertiser'];
	$aa_grade = $_POST['aa_grade'];
	$website_url = $_POST['website'];
	$landmark_type_id = '';
	$aa_estab = '';
	$pricing_prefix = $_POST['pricing_prefix'];
	$pricing_category_hi = $_POST['pricing_category_hi'];
	$pricing_category_low = $_POST['pricing_category_low'];
	$category_suffix = $_POST['category_suffix'];
	$sgl_prefix = $_POST['sgl_prefix'];
	$sgl_category_hi = $_POST['sgl_category_hi'];
	$sgl_category_low = $_POST['sgl_category_low'];
	$sgl_suffix = $_POST['sgl_suffix'];
	$estab_user_email = '';
	$publications_list = $_POST['publications'];
	$publication_counter = '';
	$publication_id = '';
	$old_establishment_name = '';
	$name_change = false;
	$active_estab = isset($_POST['inactive_estab'])?'':'1';
	$assessor_id = $_POST['assessor'];
	$pastel_code = $_POST['pastel_code'];
	$room_count = $_POST['room_count'];
	$room_type = $_POST['room_type'];
	$tc = new tcrypt();
	$notes = $_POST['notes'];
	$representative = $_POST['representative'];
    $tv_licence = $_POST['tv_licence'];
    $tv_sets = $_POST['tv_sets'];
    $qaout = $_POST['qaout_date'];
	
	$search_hash = '';
	$search_hashes = array();
	
	//Remove funny characters
	$search_string = strtolower($establishment_name);
	$search_string = plainize_name($search_string);
	
	//split into single words
	$words = explode(' ', $search_string);
	
	//calculate hash
	foreach ($words as $word) {
		$words2 = explode('-', $word);
		foreach ($words2 as $word2) {
			$search_hashes[] = sha1(trim($word2));
		}
		$search_hashes[] = sha1(trim($word));
		$word = str_replace("'", '', $word);
		$search_hashes[] = sha1(trim($word));
		$word = str_replace("-", '', $word);
		$search_hashes[] = sha1(trim($word));
		$word = str_replace("-", ' ', $word);
	}
	
	array_unique($search_hashes);
	
	//Glue back together
	$search_hash = implode(',', $search_hashes);
	
	//Get Template
	$template = file_get_contents(SITE_ROOT . "/modules/estab_s/html/update_complete.html");

	//Check changes
	$changes = explode(',', $_POST['change_list']);
	
	//Update Billing Info
	if (in_array('billing', $changes)) {
		$statement = "UPDATE nse_establishment_billing SET billing_line1=?, billing_line2=?, billing_line3=?, billing_code=?, vat_number=?, company_number=?, company_name=? WHERE establishment_code=?";
		$sql_billing = $GLOBALS['dbCon']->prepare($statement);
		$sql_billing->bind_param('ssssssss', $billing_line1, $billing_line2, $billing_line3, $billing_code, $vat_number,  $company_name, $company_number, $establishment_code);
		$sql_billing->execute();
		$sql_billing->close();
	}
	
	//Update Contact Details
	if (in_array('private_contact', $changes)) {
        //Prepare statement - Remove establishment link
        $statement = "DELETE FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
        $sql_estab_remove = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - New user
        $statement = "INSERT INTO nse_user (firstname, lastname, new_password, phone, cell, email, contact, top_level_type) VALUES (?,?,?,?,?,?,'1','c')";
        $sql_user_insert = $GLOBALS['dbCon']->prepare($statement);

        //repare statement - check if user already exists
        $statement = "SELECT user_id FROM nse_user WHERE firstname=? && lastname=? && email=?";
        $sql_user_check = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Attach estab
        $statement = "INSERT INTO nse_user_establishments (user_id, establishment_code, designation) VALUES (?,?,?)";
        $sql_estab_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update user
        $statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=? WHERE user_id=?";
        $sql_user_update = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Update estab
        $statement = "UPDATE nse_user_establishments SET designation=? WHERE user_id=? && establishment_code=?";
        $sql_estab_update = $GLOBALS['dbCon']->prepare($statement);

        $contact_line = explode("!|!", $contacts_list);

        foreach ($contact_line as $line) {
            if (empty($line)) continue;

            list($line_id, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $admin_user, $send_password, $old_email, $delete_tag) = explode('|', $line);
            if ($delete_tag == 1) {
                $sql_estab_remove->bind_param('is', $line_id, $establishment_code);
                $sql_estab_remove->execute();
            }

            //Extract firstname & lastname
            $names = explode(' ', $contact_name);
            if (count($names) == 2) {
                $firstname = $names[0];
                $lastname = $names[1];
            } else {
                $firstname = $contact_name;
                $lastname = '';
            }

            if (substr($line_id, 0,3) == 'new') {
                $uID = 0;
                $sql_user_check->bind_param('sss', $firstname, $lastname, $contact_email);
                $sql_user_check->execute();
                $sql_user_check->bind_result($uID);
                $sql_user_check->fetch();
                $sql_user_check->free_result();

                if ($uID == 0) {
                    $password = '';
                    if ($admin_user == '1') {
                        $password = $GLOBALS['security']->generate_password(6);
                        $password = $tc->encrypt($password);
                    }
                    $sql_user_insert->bind_param('ssssss', $firstname, $lastname, $password, $contact_tel, $contact_cell, $contact_email);
                    $sql_user_insert->execute();
                    $uID = $sql_user_insert->insert_id;

                    $sql_estab_insert->bind_param('iss', $uID, $establishment_code, $contact_designation);
                    $sql_estab_insert->execute();
                } else {
                    $sql_estab_insert->bind_param('iss', $uID, $establishment_code, $contact_designation);
                    $sql_estab_insert->execute();
                }
            } else {
                $sql_user_update->bind_param('sssssi', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $line_id);
                $sql_user_update->execute();

                $sql_estab_update->bind_param('sis', $contact_designation, $line_id, $establishment_code);
                $sql_estab_update->execute();
            }

        }

    }
		//Delete current Contacts
//		$statement = "DELETE FROM nse_establishment_contacts WHERE establishment_code=?";
//		$sql_contacts_delete = $GLOBALS['dbCon']->prepare($statement);
//		$sql_contacts_delete->bind_param('s', $establishment_code);
//		$sql_contacts_delete->execute();
//		$sql_contacts_delete->close();

		//Prepare statement - check if user has admin access
//		$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
//		$sql_admin_check = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare Statement - Add new user
//		$statement = "INSERT INTO nse_user (firstname, lastname, email, phone, cell, new_password, top_level_type, user_type_id) VALUES (?,?,?,?,?,?,'c','2')";
//		$sql_admin_insert = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare statement - Update user
//		$statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=? WHERE user_id=?";
//		$sql_admin_update = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare statement - Check establishment access
//		$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=?";
//		$sql_user_access = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare statement - Get user id
//		$statement = "SELECT user_id FROM nse_user WHERE email=?";
//		$sql_admin_id = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare statement - Delete admin user
//		$statement = "DELETE FROM nse_user WHERE user_id=?";
//		$sql_admin_delete = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare statement - Delete user/establishment link
//		$statement = "DELETE FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
//		$sql_estab_delete = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare statement - Add user/establishment link
//		$statement = "INSERT INTO nse_user_establishments (user_id, establishment_code) VALUES (?,?)";
//		$sql_estab_add = $GLOBALS['dbCon']->prepare($statement);
//
//		//Prepare statement - check user/establishment link
//		$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
//		$sql_estab_check = $GLOBALS['dbCon']->prepare($statement);
//
//		//Update Contacts
//		$statement = "INSERT INTO nse_establishment_contacts (establishment_code, contact_name, contact_designation, contact_tel, contact_cell, contact_email) VALUES (?,?,?,?,?,?)";
//		$sql_contacts_insert = $GLOBALS['dbCon']->prepare($statement);
//		$contact_line = explode("!|!", $contacts_list);
//
//		foreach ($contact_line as $line) {
//			if (empty($line)) continue;
//			list($line_id, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $admin_user, $send_password, $old_email, $delete_tag) = explode('|', $line);
//			if ($delete_tag == 0) {
//				$sql_contacts_insert->bind_param('ssssss', $establishment_code, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
//				$sql_contacts_insert->execute();
//			}
//
//			//Extract firstname & lastname
//			$names = explode(' ', $contact_name);
//			if (count($names) == 2) {
//				$firstname = $names[0];
//				$lastname = $names[1];
//			} else {
//				$firstname = $contact_name;
//				$lastname = '';
//			}
//			//echo "<p style='color: navy'>$line<br>Email: $old_email<br>Admin User: $admin_user</p>";
//			if ($delete_tag == 1 || $admin_user == 0) {
//				//echo "DELETE<br>=======<br />Email: $old_email<br>";
//				$admin_user_id = 0;
//				$sql_admin_id->bind_param('s', $old_email);
//				$sql_admin_id->execute();
//				$sql_admin_id->store_result();
//				$sql_admin_id->bind_result($admin_user_id);
//				$sql_admin_id->fetch();
//				$sql_admin_id->free_result();
//
//				if ($admin_user_id != 0) {
//					$sql_user_access->bind_param('i', $admin_user_id);
//					$sql_user_access->execute();
//					$sql_user_access->store_result();
//					$sql_user_access->bind_result($estab_count);
//					$sql_user_access->fetch();
//					$sql_user_access->free_result();
//
//					if ($estab_count < 2) {
//						//echo "Deleting Admin User<br>";
//						$sql_admin_delete->bind_param('i', $admin_user_id);
//						$sql_admin_delete->execute();
//					}
//
//					//echo "Deleting estab link<p />";
//					$sql_estab_delete->bind_param('is', $admin_user_id, $establishment_code);
//					$sql_estab_delete->execute();
//				}
//			} else if ($admin_user == 1) {
//				if ($old_email == 'new') {
//					//echo "ADDING USER<br>=======<br />Email: $old_email<br />";
//					//Check if user exists
//					$admin_user_id = 0;
//					$sql_admin_id->bind_param('s', $contact_email);
//					$sql_admin_id->execute();
//					$sql_admin_id->store_result();
//					$sql_admin_id->bind_result($admin_user_id);
//					$sql_admin_id->fetch();
//					$sql_admin_id->free_result();
//					//echo "User ID: $admin_user_id<br>";
//
//					if ($admin_user_id == 0) {
//						//Get Password
//						$password = $GLOBALS['security']->generate_password(6);
//						$password = $tc->encrypt($password);
//
//						//Save user
//						//echo "Inserting user<br>";
//						$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
//						$sql_admin_insert->execute();
//						$admin_user_id = $sql_admin_insert->insert_id;
//					}
//
//					//Check if user/establishment link exists
//					$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
//					$sql_estab_check->execute();
//					$sql_estab_check->store_result();
//					$sql_estab_check->bind_result($estab_count);
//					$sql_estab_check->fetch();
//					$sql_estab_check->free_result();
//
//					//Attach establishment
//					if ($estab_count == 0) {
//						//echo "Inserting estab link<p />";
//						$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
//						$sql_estab_add->execute();
//					}
//
//
//				} else {
//					//echo "UPDATING USER<br>=======<br />Email: $old_email<br />";
//					$admin_user_id = 0;
//					$sql_admin_id->bind_param('s', $old_email);
//					$sql_admin_id->execute();
//					$sql_admin_id->store_result();
//					$sql_admin_id->bind_result($admin_user_id);
//					$sql_admin_id->fetch();
//					$sql_admin_id->free_result();
//					//echo "User ID: $admin_user_id<br>";
//
//					if ($admin_user_id == 0) {
//						//echo "Adding user<br>";
//						$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
//						$sql_admin_insert->execute();
//						$admin_user_id = $sql_admin_insert->insert_id;
//					} else {
//						//echo "Updating user <br>";
//						$sql_admin_update->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $admin_user_id);
//						$sql_admin_update->execute();
//					}
//
//					//Check if estab exists
//					$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
//					$sql_estab_check->execute();
//					$sql_estab_check->store_result();
//					$sql_estab_check->bind_result($estab_count);
//					$sql_estab_check->fetch();
//					$sql_estab_check->free_result();
//
//					//Attach establishment
//					if ($estab_count == 0) {
//						//echo "Adding estab link<p />";
//						$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
//						$sql_estab_add->execute();
//					}
//				}
//			}
//		}
//		$sql_contacts_insert->close();
//		$sql_admin_check->close();
//		$sql_admin_update->close();
//		$sql_admin_insert->close();
//	}
	
	//Update Public Contact Details
	if (in_array('contact', $changes)) {
		$statement = "UPDATE nse_establishment SET postal_address_line1=?, postal_address_line2=?, postal_address_line3=?, postal_address_code=?, street_address_line1=?, street_address_line2=?, street_address_line3=? WHERE establishment_code=?";
		$sql_postal = $GLOBALS['dbCon']->prepare($statement);
		$sql_postal->bind_param('ssssssss', $postal_line1, $postal_line2, $postal_line3, $postal_code, $street_line1, $street_line2, $street_line3, $establishment_code);
		$sql_postal->execute();
		$sql_postal->close();
		
		//Delete current public contact data
		$statement = "DELETE FROM nse_establishment_public_contact WHERE establishment_code=?";
		$sql_contact_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_contact_delete->bind_param('s', $establishment_code);
		$sql_contact_delete->execute();
		
		//Insert new contacts
		$statement = "INSERT INTO nse_establishment_public_contact (establishment_code, contact_type, contact_value, contact_description) VALUES (?,?,?,?)";
		$sql_contact_insert = $GLOBALS['dbCon']->prepare($statement);
		
		if (!empty($contact_tel1)) {
			$contact_type = 'tel1';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_tel1, $contact_tel1_description);
			$sql_contact_insert->execute();
		}
		
		if (!empty($contact_tel2)) {
			$contact_type = 'tel2';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_tel2, $contact_tel2_description);
			$sql_contact_insert->execute();
		}
		
		if (!empty($contact_fax1)) {
			$contact_type = 'fax1';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_fax1, $contact_fax1_description);
			$sql_contact_insert->execute();
		}
		
		if (!empty($contact_fax2)) {
			$contact_type = 'fax2';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_fax2, $contact_fax2_description);
			$sql_contact_insert->execute();
		}
		
		if (!empty($contact_cell1)) {
			$contact_type = 'cell1';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_cell1, $contact_cell1_description);
			$sql_contact_insert->execute();
		}
		
		if (!empty($contact_cell2)) {
			$contact_type = 'cell2';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_cell2, $contact_cell2_description);
			$sql_contact_insert->execute();
		}
		
		if (!empty($contact_email)) {
			$contact_type = 'email';
			$contact_email_description = '';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_email, $contact_email_description);
			$sql_contact_insert->execute();
		}
	if (!empty($contact_email2)) {
			$contact_type = 'email2';
			$contact_email_description = '';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_email2, $contact_email_description);
			$sql_contact_insert->execute();
		}
	if (!empty($contact_email3)) {
			$contact_type = 'email3';
			$contact_email_description = '';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_email3, $contact_email_description);
			$sql_contact_insert->execute();
		}
		
		$sql_contact_insert->close();
	}
	
	// Images
	if (in_array('image', $changes)) {
		$statement = "DELETE FROM nse_establishment_images WHERE image_id=?";
		$sql_image_delete = $GLOBALS['dbCon']->prepare($statement);

		$statement = "INSERT INTO nse_establishment_images (establishment_code, image_name, thumb_name, image_title, image_description) VALUES (?,?,?,?,?)";
		$sql_image_insert = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "UPDATE nse_establishment_images SET image_name=?, thumb_name=?, image_title=?, image_description=? WHERE image_id=?";
		$sql_image_update = $GLOBALS['dbCon']->prepare($statement);
		
		//Get current image list
		$statement = "SELECT image_id, image_name, thumb_name FROM nse_establishment_images WHERE establishment_code=?";
		$sql_image_list = $GLOBALS['dbCon']->prepare($statement);
		$sql_image_list->bind_param('s', $establishment_code);
		$sql_image_list->execute();
		$sql_image_list->store_result();
		$sql_image_list->bind_result($curr_image_id, $curr_image_name, $curr_thumb_name);
		while($sql_image_list->fetch()) {
			$curr_images[$curr_image_id]['image'] = $curr_image_name;
			$curr_images[$curr_image_id]['thumb'] = $curr_thumb_name;
		}
		$sql_image_list->free_result();
		$sql_image_list->close();
		
		$image_line = explode('!|!', $image_list);
		foreach($image_line as $line){
			if (empty($line)) continue;
			$primary_image = isset($_POST['primary_image'])?$_POST['primary_image']:'';
			list($image_id, $image_location, $image_title, $image_description, $process) = explode('|', $line);
			$thumb_name = substr($image_location, strrpos($image_location, '/')+1);
			$image_name = substr($thumb_name, 3);
			switch ($process) {
				case 1:
					if (substr($image_id, 0, 3) == 'new') {
						$update_images[$image_name] = $image_name;
						$update_images[$thumb_name] = $thumb_name;
						$sql_image_insert->bind_param('sssss',$establishment_code, $image_name, $thumb_name, $image_title, $image_description);
						$sql_image_insert->execute();
					} else {
						$update_images[$image_name] = $image_name;
						$update_images[$thumb_name] = $thumb_name;
						$sql_image_update->bind_param('sssss',$image_name, $thumb_name, $image_title, $image_description, $image_id);
						$sql_image_update->execute();
					}
					if ($_POST['primary_image'] == $image_id) $primary_image = $image_id;
					break;
				case 2:
					$update_images[$image_name] = $image_name;
					$update_images[$thumb_name] = $thumb_name;
					$sql_image_insert->bind_param('sssss',$establishment_code, $image_name, $thumb_name, $image_title, $image_description);
					$sql_image_insert->execute();
					if ($_POST['primary_image'] == $image_id) $primary_image = $sql_image_insert->insert_id;
					break;
				case 3:
					$delete_images[] = $curr_images[$image_id]['image'];
					$delete_images[] = $curr_images[$image_id]['thumb'];
					$sql_image_delete->bind_param('s', $image_id);
					$sql_image_delete->execute();
					break;
			}
			
			
			//Update Primary Image
			$statement = "UPDATE nse_establishment_images SET primary_image='' WHERE establishment_code=?";
			$sql_primary_reset = $GLOBALS['dbCon']->prepare($statement);
			$sql_primary_reset->bind_param('s', $establishment_code);
			$sql_primary_reset->execute();
			$sql_primary_reset->close();
			
			
			$statement = "UPDATE nse_establishment_images SET primary_image='1' where image_id=?";
			$sql_primary_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_primary_update->bind_param('s', $primary_image);
			$sql_primary_update->execute();
			
			
		}
		//Upload images
//		foreach ($GLOBALS['ftp'] as $server=>$data) {
//			$ftp_id = ftp_connect($server);
//			ftp_login($ftp_id, $data['username'], $data['password']);
//			ftp_pasv($ftp_id, true);
//			ftp_chdir($ftp_id, $data['root'] . "res_images/");
			foreach ($delete_images as $image) {
//				@ftp_delete($ftp_id, $image);
                                unlink("/var/www/clients/client1/web25/web/res_images/$image");
			}
			foreach ($update_images as $local_image=>$image) {
//				ftp_put($ftp_id, $image, SITE_ROOT . "/temp_images/$local_image", FTP_BINARY);
                                copy("/var/www/clients/client1/web4/web/temp_images/$local_image", "/var/www/clients/client1/web25/web/res_images/$image");
			}

//			ftp_close($ftp_id);
//		}

//                   foreach ($update_images as $local_image=>$image) {
//                        copy("/temp_images/$local_image", $image);
//                   }
                //copy("/temp_images/$local_image", $image);
		
	}
	
	//Get Current Location Data
	$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=?";
	$sql_current_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_current_location->bind_param('s', $establishment_code);
	$sql_current_location->execute();
	$sql_current_location->bind_result($current_country, $current_province, $current_town, $current_suburb);
	$sql_current_location->store_result();
	$sql_current_location->fetch();
	$sql_current_location->free_result();
	$sql_current_location->close();
	
	//Update Location Data - Country
	if ($current_country != $new_country) {
		$location_change = TRUE;
		foreach ($GLOBALS['tables_list'] as $table) {
			$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='country' && (current_id=? || current_id=?)";
			$sql_country_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_country_update->bind_param('ss', $current_country, $new_country);
			$sql_country_update->execute();
			$sql_country_update->close();

			$sql_country_update = $GLOBALS['dbCon_shell']->prepare($statement);
			$sql_country_update->bind_param('ss', $current_country, $new_country);
			$sql_country_update->execute();
			$sql_country_update->close();
		}
	}
	
	//Update Location Data - Province
	if ($current_province != $new_province) {
		$location_change = TRUE;
		foreach ($GLOBALS['tables_list']  as $table) {
			$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='province' && (current_id=? || current_id=?)";
			$sql_province_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_province_update->bind_param('ss', $current_province, $new_province);
			$sql_province_update->execute();
			$sql_province_update->close();

			$sql_province_update = $GLOBALS['dbCon_shell']->prepare($statement);
			$sql_province_update->bind_param('ss', $current_province, $new_province);
			$sql_province_update->execute();
			$sql_province_update->close();
		}
	}
	
	//Update Location Data - Town
	if ($current_town != $new_town) {
		$location_change = TRUE;
		foreach ($GLOBALS['tables_list']  as $table) {
			$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='town' && (current_id=? || current_id=?)";
			$sql_town_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_town_update->bind_param('ss', $current_town, $new_town);
			$sql_town_update->execute();
			$sql_town_update->close();

			$sql_town_update = $GLOBALS['dbCon_shell']->prepare($statement);
			$sql_town_update->bind_param('ss', $current_town, $new_town);
			$sql_town_update->execute();
			$sql_town_update->close();
		}
	}
	
	//Update Location Data - Suburb
	if ($current_suburb != $new_suburb) {
		$location_change = TRUE;
		foreach ($GLOBALS['tables_list']  as $table) {
			$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='suburb' && (current_id=? || current_id=?)";
			$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_suburb_update->bind_param('ss', $current_suburb, $new_suburb);
			$sql_suburb_update->execute();
			$sql_suburb_update->close();

			$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
			$sql_suburb_update->bind_param('ss', $current_suburb, $new_suburb);
			$sql_suburb_update->execute();
			$sql_suburb_update->close();
		}
	}
	
	if ($location_change || in_array('gps',$changes)) {
		$statement = "UPDATE nse_establishment_location SET gps_latitude=?, gps_longitude=?, country_id=?, province_id=?, town_id=?, suburb_id=? WHERE establishment_code=?";
		$sql_location_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_location_update->bind_param('sssssss', $gps_latitude, $gps_longitude, $new_country, $new_province, $new_town, $new_suburb, $establishment_code);
		$sql_location_update->execute();
	}
	
	//Update Publications
	$statement = "SELECT publication_id FROM nse_establishment_publications WHERE establishment_code=?";
	$sql_publication_list = $GLOBALS['dbCon']->prepare($statement);
	$sql_publication_list->bind_param('s', $establishment_code);
	$sql_publication_list->execute();
	$sql_publication_list->store_result();
	$sql_publication_list->bind_result($publication_id);
	while ($sql_publication_list->fetch()) {
		$publication_current[$publication_id] = '';
	}
	$sql_publication_list->free_result();
	$sql_publication_list->close();
	
	$statement = "SELECT COUNT(*) FROM nse_establishment_publications WHERE establishment_code=? && publication_id=?";
	$sql_publication_check = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "INSERT INTO nse_establishment_publications (establishment_code, publication_id, add_date) VALUES (?,?, NOW())";
	$sql_publication_insert = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "DELETE FROM nse_establishment_publications WHERE establishment_code=? && publication_id=?";
	$sql_publication_delete = $GLOBALS['dbCon']->prepare($statement);
	
	$publication_ids = explode(',', $publications_list);
//	echo $publications_list;
	foreach ($publication_ids as $publication_id) {
		if (empty($publication_id)) continue;
		$sql_publication_check->bind_param('ss', $establishment_code, $publication_id);
		$sql_publication_check->execute();
		$sql_publication_check->store_result();
		$sql_publication_check->bind_result($publication_counter);
		$sql_publication_check->fetch();
		if ($publication_counter == 0) {
			$sql_publication_insert->bind_param('ss',$establishment_code, $publication_id);
			$sql_publication_insert->execute();
		}
		
		unset($publication_current[$publication_id]);
	}
	$sql_publication_check->close();
	$sql_publication_insert->close();
	if (!empty($publication_current)) {
		foreach ($publication_current as $publication_id=>$v) {
			$sql_publication_delete->bind_param('ss', $establishment_code, $publication_id);
			$sql_publication_delete->execute();
		}
	}
	$sql_publication_delete->close();
	
	//Update Awards
	if (in_array('awards', $changes)) {
		//echo "AWARD";
		$statement = "DELETE FROM award WHERE code=?";
		$sql_award_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_award_delete->bind_param('s', $establishment_code);
		$sql_award_delete->execute();
		$sql_award_delete->close();
		$statement = "INSERT INTO award (code, status, year, category) VALUE (?,?,?,?)";
		$sql_award_insert = $GLOBALS['dbCon']->prepare($statement);
		$awards_list = explode('#',$awards);
		foreach ($awards_list as $award_line) {
			if (empty($award_line)) continue;
			$items = explode('|', $award_line);
			$sql_award_insert->bind_param('ssss', $establishment_code , $items[4], $items[1], $items[2]);
			$sql_award_insert->execute();
		}
		$sql_award_insert->close();
	}
	
	
	//Update primary details
	if (in_array('primary', $changes)) {
		//Get current name to check for name change
		$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
		$sql_name = $GLOBALS['dbCon']->prepare($statement);
		$sql_name->bind_param('s', $establishment_code);
		$sql_name->execute();
		$sql_name->store_result();
		$sql_name->bind_result($old_establishment_name);
		$sql_name->fetch();
		$sql_name->free_result();
		$sql_name->close();
		
		if ($old_establishment_name != $establishment_name) $name_change = true;

        //Update Date
        $qaout_id = '';
        $statement = "SELECT qaout_id FROM nse_establishment_qa_cancelled WHERE establishment_code=? ORDER BY cancelled_date DESC LIMIT 1";
        $sql_qaout = $GLOBALS['dbCon']->prepare($statement);
        $sql_qaout->bind_param('s', $establishment_code);
        $sql_qaout->execute();
        $sql_qaout->bind_result($qaout_id);
        $sql_qaout->fetch();
        $sql_qaout->free_result();
        $sql_qaout->close();
        if (empty($qaout_id)) {
            $statement = "INSERT INTO nse_establishment_qa_cancelled (establishment_code, cancelled_date) VALUES (?,?)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bind_param('ss', $establishment_code, $qaout);
            $sql_insert->execute();
            $sql_insert->close();
        } else {
            $statement = "UPDATE nse_establishment_qa_cancelled SET cancelled_date=? WHERE qaout_id=?";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            $sql_update->bind_param('si', $qaout, $qaout_id);
            $sql_update->execute();
            $sql_update->close();
        }

		//AA Estab
		if (!empty($aa_grade)) $aa_estab = 'Y';
		if (in_array($advertiser, array('P','L','A','B','S'))) {
			$aa_estab = 'Y';
		} else {
			$statement = "SELECT COUNT(*)
							FROM nse_establishment_publications AS a
							JOIN nse_publications AS b ON a.publication_id=b.publication_id
							WHERE a.establishment_code=? && b.expiry_date>NOW()";
			$sql_aa_estab = $GLOBALS['dbCon']->prepare($statement);
			$sql_aa_estab->bind_param('s', $establishment_code);
			$sql_aa_estab->execute();
			$sql_aa_estab->store_result();
			$sql_aa_estab->bind_result($aa_estab_count);
			$sql_aa_estab->fetch();
			$sql_aa_estab->free_result();
			$sql_aa_estab->close();
			
			if ($aa_estab_count > 0) {
				$aa_estab = 'Y';
			}
		}

		$statement = "UPDATE nse_establishment SET establishment_name=?, nightsbridge_bbid=?, prebook_bid=?, aa_category_code=?, advertiser=?, website_url=?, last_updated=NOW(),aa_estab=?, search_hash=?, assessor_id=?, pastel_code=?, enter_date=?, rep_id=? WHERE establishment_code=?";
		$sql_primary = $GLOBALS['dbCon']->prepare($statement);
		$sql_primary->bind_param('sssssssssssss', $establishment_name, $nightsbridge_code, $prebook_code, $aa_grade, $advertiser, $website_url, $aa_estab, $search_hash, $assessor_id, $pastel_code, $entered_date, $representative, $establishment_code);
		$sql_primary->execute();
		$sql_primary->close();
		
		foreach ($GLOBALS['tables_list']  as $table) {
			$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='country' && current_id=?";
			$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_suburb_update->bind_param('s', $current_country);
			$sql_suburb_update->execute();
			$sql_suburb_update->close();

			$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
			$sql_suburb_update->bind_param('s', $current_country);
			$sql_suburb_update->execute();
			$sql_suburb_update->close();
		}

		if ($current_province != '0') {
			foreach ($GLOBALS['tables_list']  as $table) {
				$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='province' && current_id=?";
				$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
				$sql_suburb_update->bind_param('s', $current_province);
				$sql_suburb_update->execute();
				$sql_suburb_update->close();

				$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
				$sql_suburb_update->bind_param('s', $current_province);
				$sql_suburb_update->execute();
				$sql_suburb_update->close();
			}
		}

		if ($current_town != '0') {
			foreach ($GLOBALS['tables_list']  as $table) {
				$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='town' && current_id=?";
				$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
				$sql_suburb_update->bind_param('s', $current_town);
				$sql_suburb_update->execute();
				$sql_suburb_update->close();

				$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
				$sql_suburb_update->bind_param('s', $current_town);
				$sql_suburb_update->execute();
				$sql_suburb_update->close();
			}
		}

		if ($current_suburb != '0') {
			foreach ($GLOBALS['tables_list']  as $table) {
				$statement = "UPDATE $table SET complete = NULL, complete4 = NULL WHERE level='suburb' && current_id=?";
				$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
				$sql_suburb_update->bind_param('s', $current_suburb);
				$sql_suburb_update->execute();
				$sql_suburb_update->close();

				$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
				$sql_suburb_update->bind_param('s', $current_suburb);
				$sql_suburb_update->execute();
				$sql_suburb_update->close();
			}
		}
	}

	
	//Update active status
	$statement = "UPDATE nse_establishment SET active=? WHERE establishment_code=?";
	$sql_active = $GLOBALS['dbCon']->prepare($statement);
	$sql_active->bind_param('ss', $active_estab, $establishment_code);
	$sql_active->execute();
	$sql_active->close();

	//Update Notes
	if (in_array('notes', $changes)) {
		//Prepare statement - add note
		$statement = "INSERT INTO nse_establishment_notes (establishment_code, user_name, note_timestamp, note_content, module_name ) VALUES (?,?,?,?,'general')";
		$sql_notes = $GLOBALS['dbCon']->prepare($statement);

		//Get user name
		$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
		$sql_user = $GLOBALS['dbCon']->prepare($statement);
		$sql_user->bind_param('i', $_SESSION['dbweb_user_id']);
		$sql_user->execute();
		$sql_user->store_result();
		$sql_user->bind_result($user_name);
		$sql_user->fetch();
		$sql_user->free_result();
		$sql_user->close();

		$line = explode('##',$notes);
		foreach ($line as $note_line) {
			if (empty($note_line)) continue;
			$note_items = explode('|', $note_line);
			//Calculate timestamp
			list($date, $time) = explode(' ', $note_items[1]);
			list($day, $month, $year) = explode('-', $date);
			list($hours, $minutes) = explode(':',$time);
			$timestamp = mktime($hours, $minutes, 0, $month, $day, $year);

			$sql_notes->bind_param('ssss', $establishment_code, $user_name, $timestamp, $note_items[2]);
			$sql_notes->execute();
		}
		$sql_notes->close();
	}
	
	//Update Invoices
	if (in_array('invoice', $changes)) {
		$invoice_list = explode('!|!', $_POST['invoices']);
		foreach ($invoice_list as $invoice_line) {
			if (empty($invoice_line)) continue;
			list($invoice_id, $invoice_date, $invoice_item, $invoice_cost, $invoice_paid, $update_code) = explode('|', $invoice_line);
			if ($update_code == 0) continue;
			if ($update_code == 1) { //Update current
				$statement = "UPDATE nse_establishment_invoice SET line_date=?, line_item=?, line_cost=?, line_paid=? WHERE invoice_id=?";
				$sql_invoice_update = $GLOBALS['dbCon']->prepare($statement);
				$sql_invoice_update->bind_param('ssssi', $invoice_date, $invoice_item, $invoice_cost, $invoice_paid, $invoice_id);
				$sql_invoice_update->execute();
				$sql_invoice_update->close();
			}
			if ($update_code == 2) { //Insert new line
				$statement = "INSERT INTO nse_establishment_invoice (establishment_code, line_date, line_item, line_cost, line_paid) VALUES (?,?,?,?,?)";
				$sql_invoice_insert = $GLOBALS['dbCon']->prepare($statement);
				$sql_invoice_insert->bind_param('sssss', $establishment_code, $invoice_date, $invoice_item, $invoice_cost, $invoice_paid);
				$sql_invoice_insert->execute();
				$sql_invoice_insert->close();
			}
		}
	}
	
	//Update Policies
	if (in_array('policy',$changes)) {
		$statement = "SELECT COUNT(*) FROM nse_establishment_descriptions WHERE language_code='EN' && description_type=? && establishment_code=?";
		$sql_policy_check = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "INSERT INTO nse_establishment_descriptions (language_code, description_type, establishment_description, establishment_code) VALUES ('EN',?,?,?)";
		$sql_policy_insert = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "UPDATE nse_establishment_descriptions SET establishment_description=? WHERE language_code='EN' && description_type=? && establishment_code=?";
		$sql_policy_update = $GLOBALS['dbCon']->prepare($statement);
		
		$description_type = 'child policy';
		$count = 0;
		$sql_policy_check->bind_param('ss', $description_type, $establishment_code);
		$sql_policy_check->execute();
		$sql_policy_check->bind_result($count);
		$sql_policy_check->store_result();
		$sql_policy_check->fetch();
		$sql_policy_check->free_result();
		if ($count == 0) {
			$sql_policy_insert->bind_param('sss', $description_type, $child_policy, $establishment_code);
			$sql_policy_insert->execute();
		} else {
			$sql_policy_update->bind_param('sss', $child_policy, $description_type, $establishment_code);
			$sql_policy_update->execute();
		}
		
		$description_type = 'cancellation policy';
		$count = 0;
		$sql_policy_check->bind_param('ss', $description_type, $establishment_code);
		$sql_policy_check->execute();
		$sql_policy_check->bind_result($count);
		$sql_policy_check->store_result();
		$sql_policy_check->fetch();
		$sql_policy_check->free_result();
		if ($count == 0) {
			$sql_policy_insert->bind_param('sss', $description_type, $cancellation_policy, $establishment_code);
			$sql_policy_insert->execute();
		} else {
			$sql_policy_update->bind_param('sss', $cancellation_policy, $description_type, $establishment_code);
			$sql_policy_update->execute();
		}
		
		$sql_policy_check->close();
		$sql_policy_insert->close();
		$sql_policy_update->close();
	}
	
	if (!empty($_FILES['directions_map']['name'])) {
		//Resize image & Create thumbnail
		move_uploaded_file($_FILES['directions_map']['tmp_name'], SITE_ROOT . "/res_maps/$file_name1");
                $file_name1 = strtolower($establishment_code) . image_type_to_extension($image_type);
		$file_name2 = strtolower($establishment_code);
                list($src_width, $src_height, $image_type, $html_markup) = getimagesize(SITE_ROOT . "/res_maps/$file_name1");
                
		
		switch ($image_type) {
			case IMG_GIF:
				$src_image = imagecreatefromgif(SITE_ROOT . "/res_maps/$file_name1");
				break;
			case IMG_JPG:
				$src_image = imagecreatefromjpeg(SITE_ROOT . "/res_maps/$file_name1");
				break;
			case IMG_PNG:
				$src_image = imagecreatefrompng(SITE_ROOT . "/res_maps/$file_name1");
				break;
			case IMG_WBMP:
				$src_image = imagecreatefromwbmp(SITE_ROOT . "/res_maps/$file_name1");
				break;
		}
		
		if ($src_width > $src_height) {
			$new_height = 200*($src_height/$src_width);
			$new_image = imagecreatetruecolor(200, $new_height);
			imagecopyresampled($new_image, $src_image, 0, 0, 0, 0, 200, $new_height, $src_width, $src_height);
		} else {
			$new_width = 200*($src_width/$src_height);
			$new_image = imagecreatetruecolor($new_height, 200);
			imagecopyresampled($new_image, $src_image, 0, 0, 0, 0, $new_width, 200, $src_width, $src_height);
		}
		
		imagejpeg($new_image, SITE_ROOT . "/res_maps/TN_$file_name2.jpg");
		 copy("/var/www/clients/client1/web4/web/res_maps/TN_$file_name2.jpg", "/var/www/clients/client1/web25/web/res_maps/TN_$file_name2.jpg");

//		foreach ($GLOBALS['ftp'] as $server=>$data) {
//			$ftp_id = ftp_connect($server);
//			ftp_login($ftp_id, $data['username'], $data['password']);
//			ftp_chdir($ftp_id, $data['root'] . "res_maps/");
//			ftp_put($ftp_id, "TN_$file_name2.jpg", SITE_ROOT . "/res_maps/TN_$file_name2.jpg", FTP_BINARY);
//			ftp_put($ftp_id, "$file_name1", SITE_ROOT . "/res_maps/$file_name1", FTP_BINARY);
//			ftp_close($ftp_id);
//		}


	}
	
	//Update Directions
	if (in_array('directions', $changes)) {
		$statement = "SELECT COUNT(*) FROM nse_establishment_directions WHERE establishment_code=? ";
		$sql_directions_count = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_directions_count->bind_param('s', $establishment_code);
		$sql_directions_count->execute();
		$sql_directions_count->store_result();
		$sql_directions_count->bind_result($count);
		$sql_directions_count->fetch();
		$sql_directions_count->free_result();
		$sql_directions_count->close();
		
		if ($count == 0) {
			$statement = "INSERT INTO nse_establishment_directions (directions, map_type, image_name, establishment_code) VALUES (?,?,?,?)";
		} else {
			$statement = "UPDATE nse_establishment_directions SET directions=?, map_type=?, image_name=? WHERE establishment_code=?";
		}
		$sql_directions = $GLOBALS['dbCon']->prepare($statement);
		$sql_directions->bind_param('ssss',$directions, $map_type, $file_name1, $establishment_code);
		$sql_directions->execute();
		$sql_directions->close();
	}
	
	
	
	//Update Pricing
	if (in_array('price', $changes)) {
		$statement = "UPDATE nse_establishment_pricing SET
						price_description=?,
						category_prefix=?,
						category=?,
						category_suffix=?,
						single_category=?,
						single_prefix=?,
						single_description=?,
						category_low=?,
						single_category_low=?
						WHERE establishment_code=?";
		$sql_pricing = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_pricing->bind_param('ssssssssss', $pricing, $pricing_prefix, $pricing_category_hi, $category_suffix, $sgl_category_hi, $sgl_prefix, $sgl_suffix, $pricing_category_low, $sgl_category_low, $establishment_code);
		$sql_pricing->execute();
		$sql_pricing->close();
	}
		$pricing_prefix = $_POST['pricing_prefix'];
	$pricing_category_hi = $_POST['pricing_category_hi'];
	$pricing_category_low = $_POST['pricing_category_low'];
	$category_suffix = $_POST['category_suffix'];
	$sgl_prefix = $_POST['sgl_prefix'];
	$sgl_category_hi = $_POST['sgl_category_hi'];
	$sgl_category_low = $_POST['sgl_category_low'];
	$sgl_suffix = $_POST['sgl_suffix'];
	
	//Update Facilities (icons)
	if (in_array('facility', $changes)) {
		$statement = "DELETE FROM nse_establishment_icon WHERE establishment_code=?";
		$sql_icon_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_icon_delete->bind_param('s', $establishment_code);
		$sql_icon_delete->execute();
		$sql_icon_delete->close();
		
		$statement = "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES (?,?)";
		$sql_icon_insert = $GLOBALS['dbCon']->prepare($statement);
		foreach ($_POST['facilities'] as $icon_id) {
			$sql_icon_insert->bind_param('ss', $establishment_code, $icon_id);
			$sql_icon_insert->execute();
		}
	}
	
	//Update Restype
	if (in_array('restype', $changes)) {
		$statement = "DELETE FROM nse_establishment_restype WHERE establishment_code=?";
		$sql_restype_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_restype_delete->bind_param('s', $establishment_code);
		$sql_restype_delete->execute();
		
		$statement = "INSERT INTO nse_establishment_restype (establishment_code, subcategory_id, star_grading) VALUES (?,?,?)";
		$sql_restype_insert = $GLOBALS['dbCon']->prepare($statement);
		for ($i=0; $i<3; $i++) {
			$restype = $_POST['restype'][$i];
			$star_grading = $_POST['star_grading'][$i];
			if ($restype == 0) continue;
			$sql_restype_insert->bind_param('sss', $establishment_code, $restype, $star_grading);
			$sql_restype_insert->execute();
		}
		$sql_restype_delete->close();
		$sql_restype_insert->close();
		
		//Update Room count & tv licence
		$statement = "UPDATE nse_establishment_data SET room_count=?, room_type=?, tv_licence=?, tv_count=? WHERE establishment_code=?";
		$sql_rooms = $GLOBALS['dbCon']->prepare($statement);
		$sql_rooms->bind_param('sssss', $room_count, $room_type, $tv_licence, $tv_sets, $establishment_code);
		$sql_rooms->execute();
		$sql_rooms->close();
	}
	
	//Update Descriptions
	$description_types = array('short_description','long_description');
	$statement = "DELETE FROM nse_establishment_descriptions WHERE establishment_code=? && description_type=?";
	$sql_description_delete = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "INSERT INTO nse_establishment_descriptions (establishment_code, description_type, establishment_description, language_code) VALUES (?,?,?,'EN')";
	$sql_description_insert = $GLOBALS['dbCon']->prepare($statement);
	
	foreach ($description_types as $type) {
		if (in_array($type, $changes)) {
			$sql_description_delete->bind_param('ss', $establishment_code, $type);
			$sql_description_delete->execute();
			$sql_description_insert->bind_param('sss', $establishment_code, $type, $description[$type]);
			$sql_description_insert->execute();
		}
	}
	$sql_description_delete->close();
	$sql_description_insert->close();
	
	//Clear Landmarks
	$statement = "DELETE FROM nse_establishment_landmarks WHERE establishment_code=?";
	$sql_landmarks_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_landmarks_delete->bind_param('s', $establishment_code);
	$sql_landmarks_delete->execute();
	$sql_landmarks_delete->close();
	
	//Prepare landmark queries
	$statement = "INSERT INTO nse_establishment_landmarks (establishment_code, landmark_id, landmark_distance, landmark_measurement) VALUES (?,?,?,?)";
	$sql_landmark_establishment = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "INSERT INTO nse_landmark (landmark_name, town_id, landmark_type_id) VALUES (?,?,?)";
	$sql_landmark_insert = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "SELECT landmark_id FROM nse_landmark WHERE lower(landmark_name)=? && town_id=? && landmark_type_id=?";
	$sql_landmark_check = $GLOBALS['dbCon']->prepare($statement);
		
	//Update Landmarks
	$statement = "SELECT landmark_type_id FROM nse_landmark_types";
	$sql_landmark_types = $GLOBALS['dbCon']->prepare($statement);
	$sql_landmark_types->execute();
	$sql_landmark_types->store_result();
	$sql_landmark_types->bind_result($landmark_type_id);
	while ($sql_landmark_types->fetch()) {
		for ($x=1; $x<4; $x++) {
			$landmark_id = $_POST["{$landmark_type_id}_id_$x"];
			$landmark_name = $_POST["{$landmark_type_id}_name_$x"];
			$landmark_measurement = $_POST["{$landmark_type_id}_measurement_$x"];
			$landmark_distance = $_POST["{$landmark_type_id}_distance_$x"];
			if (empty($landmark_name)) continue;
			$town_id = $_POST['town_id'];
			$sql_landmark_check->bind_param('sss', strtolower($landmark_name), $town_id, $landmark_type_id);
			$sql_landmark_check->execute();
			$sql_landmark_check->store_result();
			if ($sql_landmark_check->num_rows > 0) {
				$sql_landmark_check->bind_result($landmark_id);
				$sql_landmark_check->fetch();
			} else {
				$sql_landmark_insert->bind_param('sss',$landmark_name, $town_id, $landmark_type_id);
				$sql_landmark_insert->execute();
				$landmark_id = $sql_landmark_insert->insert_id;
			}
			$sql_landmark_establishment->bind_param('ssss',$establishment_code, $landmark_id, $landmark_distance, $landmark_measurement);
			$sql_landmark_establishment->execute();
		}
	}
	$sql_landmark_types->free_result();
	$sql_landmark_types->close();
	$sql_landmark_establishment->close();
	$sql_landmark_insert->close();
	$sql_landmark_check->close();
	
	//Prepare Statement - Check User
	$statement = "SELECT notification_value FROM nse_user_notices WHERE user_id=? && notification_code='change_approval'";
	$sql_notice = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Count changes
	$statement = "SELECT count(*) FROM nse_establishment_updates WHERE establishment_code=?";
	$sql_change_count = $GLOBALS['dbCon']->prepare($statement);
	
	
	//Check if client has elected to recieve change update
	$estab_user_id = '';
	$statement = "SELECT a.user_id, b.email
					FROM nse_user_establishments AS a
					JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=?";
	$sql_users = $GLOBALS['dbCon']->prepare($statement);
	
	$sql_users->bind_param('s', $establishment_code);
	$sql_users->execute();
	$sql_users->store_result();
	$sql_users->bind_result($estab_user_id, $estab_user_email);
	while($sql_users->fetch()) {
		$notice_value = 0;
		$sql_notice->bind_param('i', $estab_user_id);
		$sql_notice->execute();
		$sql_notice->store_result();
		$sql_notice->bind_result($notice_value);
		$sql_notice->fetch();
		if ($notice_value == '1') {
		//Send email to Clients who have requested changes
			$change_count = 0;
			
			$sql_change_count->bind_param('s', $establishment_code);
			$sql_change_count->execute();
			$sql_change_count->store_result();
			$sql_change_count->bind_result($change_count);
			$sql_change_count->fetch();
			
			if ($change_count > 0) {
				$mail_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/estab_s/html/change_email.html');
				
				$mail = new PHPMailer(true);
				try {
					$mail->AddReplyTo('admin@aatravel.co.za', 'AA Travel Guides');
				    $mail->AddAddress($estab_user_email);
				    //$mail->AddAddress('meloth14@gmail.com');
				    $mail->SetFrom('admin@aatravel.co.za', 'AA Travel Guides');
				    $mail->Subject = "Requested changes for $establishment_name have been approved";
				    $mail->MsgHTML($mail_template);
				    $mail->Send();
				    $mail->ClearAddresses();
				    
					//Log
			        $GLOBALS['log_tool']->write_entry("Requested changes approved - email sent", $_SESSION['dbweb_user_id']);
			    } catch (phpmailerException $e) {
			      //Log
			      $error = $e->errorMessage();
			      $GLOBALS['log_tool']->write_entry("Error! Requested changes approved email failed $error", $_SESSION['dbweb_user_id']);
			    } catch (Exception $e) {
			      //Log
			      $error = $e->errorMessage();
			      $GLOBALS['log_tool']->write_entry("Error! Requested changes approved email failed $error", $_SESSION['dbweb_user_id']);
			    }
			}
		}
	}
	$sql_users->free_result();
	$sql_users->close();
	$sql_notice->close();
	
	//Check if client updates have been changed
//	$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
//	$sql_check_updates = $GLOBALS['dbCon']->prepare($statement);
//	$sql_check_updates->bind_param('s', $establishment_code);
//	$sql_check_updates->execute();
//	$sql_check_updates->store_result();
//	$sql_check_updates->bind_result($field_name, $field_value);
//	while ($sql_check_updates->fetch()) {
//		switch($field_name) {
//			case 'images':
//				$images = explode('!|!', $field_value);
//				foreach ($images as $image_data) {
//					if (empty($image_data)) continue;
//					list($image_id, $image_file, $image_title, $image_description, $process) = explode('|', $image_data);
//					if ($process == '1' || $process == '2') {
//						$thumb_name = substr($image_file, strrpos($image_file, '/'));
//						$statement = "SELECT COUNT(*) FROM nse_establishment_images WHERE establishment_code=? && thumb_name=? && image_title=? && image_description=?";
//						$sql_image_check = $GLOBALS['dbCon']->prepare($statement);
//						$sql_image_check->bind_param('ssss', $establishment_code, $thumb_name, $image_title, $image_description);
//						$sql_image_check->execute();
//						$sql_image_check->bind_result($image_count);
//						$sql_image_check->fetch();
//						$sql_image_check->close();
//
//						if ($image_count == 0) {
//							$error_message .= "($establishment_code) Images did not save correctly" . PHP_EOL;
//						}
//					}
//				}
//				break;
//		}
//	}
//	$sql_check_updates->free_result();
//	$sql_check_updates->close();
//
//	if (!empty($error_message)) {
//		mail('meloth14@gmail.com', 'Update Error', $error_message);
//	}

	//Log updates
	$statement = "INSERT INTO nse_establishment_updates_log SELECT establishment_code, field_name, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? ";
	$sql_log = $GLOBALS['dbCon']->prepare($statement);
	$sql_log->bind_param('s', $establishment_code);
	$sql_log->execute();
	$sql_log->close();

	//Clear Updates
	$statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=?";
	$sql_clear = $GLOBALS['dbCon']->prepare($statement);
	$sql_clear->bind_param('s', $establishment_code);
	$sql_clear->execute();
	$sql_clear->close();
	
	//Update Remote Establishment Details Page
	$ch = curl_init("http://aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	//$ch = curl_init("http://aatravel.server1/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	if ($name_change) {
		//Log change
		$statement = "INSERT INTO nse_establishment_name_history (establishment_code, old_establishment_name, change_date) VALUES (?,?,NOW())";
		$sql_change = $GLOBALS['dbCon']->prepare($statement);
		$sql_change->bind_param('ss', $establishment_code, $old_establishment_name);
		$sql_change->execute();
		$sql_change->close();
		
		//Update old page to redirect
		$ch = curl_init("http://aatravel.co.za/export/update_old_establishment.php?code=$establishment_code");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$res = curl_exec($ch);
		curl_close($ch);

                //Update search DB
                $search_name = clean_up($establishment_name);
                $statement = "UPDATE nse_search_names SET search_name=? WHERE establishment_code=?";
                $sql_search = $GLOBALS['dbCon']->prepare($statement);
                $sql_search->bind_param('ss', $search_name, $establishment_code);
                $sql_search->execute();
                $sql_search->close();
	}
	
	include_once 'establishment.php';
	$estab = new establishment();
	$estab->index_establishment($establishment_code);
	
	$GLOBALS['log_tool']->write_entry("Establishment Update: $establishment_name ($establishment_code)", $_SESSION['dbweb_user_id']);
	
	//Get file name
	$name =  stripslashes($establishment_name);
	$name = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $name);
	$name = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $name );
	$name = htmlentities($name);
	$name = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $name);
	$name = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $name);
	$name = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $name);
	$name = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $name);
	$name = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $name);
	$name = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $name);
		
	$file_name = "/accommodation/". strtolower(str_replace(' ', '_', $establishment_code.'_'.$name . ".html"));
	
	//Replace Tags
	$template = str_replace('<!-- file_name -->', $file_name, $template);
	
	echo $template;
	echo "<script>update_page('ok')</script>";

}

/**
 * Permanently delete an establishment. Should be used sparingly, usually an establishment should be suspended.
 */
function establishment_delete() {
	//Vars
	$establishment_code = ctype_alnum($_GET['id'])&&strlen($_GET['id'])<8?$_GET['id']:'';
	$table_list = array('nse_establishment_updates','nse_establishment_units','nse_establishment_seasons','nse_establishment_restype',
						'nse_establishment_reservation','nse_establishment_public_contact','nse_establishment_pricing','nse_establishment_location',
						'nse_establishment_landmarks','nse_establishment_images','nse_establishment_icon','nse_establishment_feedback',
						'nse_establishment_enquiry','nse_establishment_directions','nse_establishment_descriptions','nse_establishment_deprecated',
						'nse_establishment_date','nse_establishment_data','nse_establishment_contacts','nse_establishment_contact',
						'nse_establishment_billing','nse_establishment');
	
	//Delete from updates
	foreach ($table_list as $table) {
		$statement = "DELETE FROM $table WHERE establishment_code=?";
		$sql_delete = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_delete->bind_param('s', $establishment_code);
		$sql_delete->execute();
		$sql_delete->close();
	}
	
	$GLOBALS['log_tool']->write_entry("Establishment Deleted: $establishment_code)", $_SESSION['dbweb_user_id']);
	
	echo "<script>document.location='/modules/estab_s/estab.php?f=estab_list'</script>";
}

/**
 * Display a form to add a new establishment
 */
function establishment_add_form() {
	
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/estab_s/html/establishment_add_form.html');
	
	echo $template;
}

function establishment_add_insert() {
	//Vars
	$establishment_code = $_POST['establishment_code'];
	$establishment_name = $_POST['establishment_name'];
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$designation = $_POST['designation'];
	$email = $_POST['email'];
	$tel = $_POST['tel'];
	$cell = $_POST['cell'];
	$contact_name = "$firstname $lastname";
	$user_id = '';
	
	//Clase - to encypt password
	require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/tcrypt.class.php';
	$tc = new tcrypt();
	
	//Class - to generate establishment code
	include 'establishment.php';
	$est = new establishment();
	
	//Get establishment code
	if (empty($establishment_code)) {
		$establishment_code = $est->next_establishment_code();
	}
	
	//Add to DB
	$statement = "INSERT INTO nse_establishment (establishment_code, establishment_name) VALUES (?,?)";
	$sql_establishment = $GLOBALS['dbCon']->prepare($statement);
	$sql_establishment->bind_param('ss', $establishment_code, $establishment_name);
	$sql_establishment->execute();
	$sql_establishment->close();
	
	//Add Initial user
    //Prepare statement - Remove establishment link
    $statement = "DELETE FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
    $sql_estab_remove = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - New user
    $statement = "INSERT INTO nse_user (firstname, lastname, new_password, phone, cell, email, contact, top_level_type) VALUES (?,?,?,?,?,?,'1','c')";
    $sql_user_insert = $GLOBALS['dbCon']->prepare($statement);

    //repare statement - check if user already exists
    $statement = "SELECT user_id FROM nse_user WHERE firstname=? && lastname=? && email=?";
    $sql_user_check = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Attach estab
    $statement = "INSERT INTO nse_user_establishments (user_id, establishment_code, designation) VALUES (?,?,?)";
    $sql_estab_insert = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - update user
    $statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=? WHERE user_id=?";
    $sql_user_update = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Update estab
    $statement = "UPDATE nse_user_establishments SET designation=? WHERE user_id=? && establishment_code=?";
    $sql_estab_update = $GLOBALS['dbCon']->prepare($statement);

    $uID = 0;
    $sql_user_check->bind_param('sss', $firstname, $lastname, $contact_email);
    $sql_user_check->execute();
    $sql_user_check->bind_result($uID);
    $sql_user_check->fetch();
    $sql_user_check->free_result();

    if ($uID == 0) {
        $password = $GLOBALS['security']->generate_password(6);
        $password = $tc->encrypt($password);

        $sql_user_insert->bind_param('ssssss', $firstname, $lastname, $password, $tel, $cell, $email);
        $sql_user_insert->execute();
        $uID = $sql_user_insert->insert_id;

        $sql_estab_insert->bind_param('iss', $uID, $establishment_code, $contact_designation);
        $sql_estab_insert->execute();
    } else {
        $sql_estab_insert->bind_param('iss', $uID, $establishment_code, $contact_designation);
        $sql_estab_insert->execute();
    }
	
	//Set user defaults
	$statement = "INSERT INTO nse_user_notices (user_id, notification_code, notification_value) VALUE (?,?,'1')";
	$sql_user_notice = $GLOBALS['dbCon']->prepare($statement);
	
	$notice_code = 'feedback';
	$sql_user_notice->bind_param('ss', $user_id, $notice_code);
	$sql_user_notice->execute();
	
	$notice_code = 'change_approval';
	$sql_user_notice->bind_param('ss', $user_id, $notice_code);
	$sql_user_notice->execute();
	
	//Add other
	$tables = array('nse_establishment_billing',
					'nse_establishment_data',
					'nse_establishment_deprecated',
					'nse_establishment_directions',
					'nse_establishment_location',
					'nse_establishment_pricing',
					'nse_establishment_public_contact',
					'nse_establishment_reservation',
					'nse_establishment_restype');
	foreach ($tables as $table) {
		$statement = "INSERT INTO $table (establishment_code) VALUES (?)";
		$sql_billing = $GLOBALS['dbCon']->prepare($statement);
		$sql_billing->bind_param('s', $establishment_code);
		$sql_billing->execute();
		$sql_billing->close();
	}
	
	
	
	establishment_edit_form(FALSE, $establishment_code);
}

/**
 * Displays the welcome page on first entering the module.
 */
function welcome() {
	//Vars
	$wait_counter = 0;
	$overdue_count = 0;
	$establishment_code = '';
	$update_distance = '';
	
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/estab_s/html/welcome.html');
	
	//Prepare stotement - update age
	$statement = "SELECT NOW()-update_date FROM nse_establishment_updates WHERE establishment_code=? LIMIT 1";
	$sql_age = $GLOBALS['dbCon']->prepare($statement);
	
	//Count number of establishment waiting for approval
	$statement = "SELECT DISTINCT(establishment_code) FROM nse_establishment_updates";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->execute();
	$sql_count->store_result();
	$sql_count->bind_result($establishment_code);
	while ($sql_count->fetch()) {
		$wait_counter++;
		
		$sql_age->bind_param('s', $establishment_code);
		$sql_age->execute();
		$sql_age->store_result();
		$sql_age->bind_result($update_distance);
		$sql_age->fetch();
		$sql_age->free_result();
		if ($update_distance > 172800) $overdue_count++;
	}
	$sql_count->free_result();
	$sql_count->close();
	$sql_age->close();

	//Count number of assessments waiting approval
	$statement = "SELECT COUNT(*) FROM nse_establishment_assessment WHERE assessment_status='waiting'";
	$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessments->execute();
	$sql_assessments->store_result();
	$sql_assessments->bind_result($assessment_count);
	$sql_assessments->fetch();
	$sql_assessments->free_result();
	$sql_assessments->close();


	//Buttons
	if (!$GLOBALS['access']['add_establishment']) $template = preg_replace('@<!-- add_start -->(.)*<!-- add_end -->@', '', $template);
	if (!$GLOBALS['access']['edit_establishment']) $template = preg_replace('@<!-- edit_start -->(.)*<!-- edit_end -->@', '', $template);
	
	//Replace Tags
	$template = str_replace('<!-- update_counter -->', $wait_counter, $template);
	$template = str_replace('<!-- overdue_counter -->', $overdue_count, $template);
	$template = str_replace('<!-- assessment_counter -->', $assessment_count, $template);
	
	echo $template;
}

/**
 * Replaces special characters with the english alphabet (a-Z) equivalent.
 * @param $name
 */
function plainize_name($name) {
	$name = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $name);
	$name = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $name );
	$name = htmlentities($name);
	$name = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $name);
	$name = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $name);
	$name = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $name);
	$name = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $name);
	$name = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $name);
	$name = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $name);
	
	return $name;
}

function dbase_view() {
	//Vars
	$establishment_code = $_GET['id'];
	$line_class = 'line1';
	$template = file_get_contents(SITE_ROOT . '/modules/estab_s/html/dbase_data.html');

	//Get establishment name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_name->bind_param('s', $establishment_code);
	$sql_name->execute();
	$sql_name->store_result();
	$sql_name->bind_result($establishment_name);
	$sql_name->fetch();
	$sql_name->close();

	//Get data
	$counter = 0;
	$statement = "SELECT * FROM nse_establishment_dbase WHERE code=?";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	$sql_data->bind_param('s', $establishment_code);
	$sql_data->execute();
	$meta = $sql_data->result_metadata();
	while ($field = $meta->fetch_field()) {
		$params[] = &$row[$field->name];
	} 
	call_user_func_array(array($sql_data, 'bind_result'), $params);

	while ($sql_data->fetch()) {
		foreach($row as $key => $val) {
		    $c[$counter][$key] = $val;
		}
		$counter++;
	}
	$sql_data->free_result();
	$sql_data->close();
	
	$table_data = "<table width=100% id=dbase_table >";
	if (count($c) == 0) {
		$table_data .= "<tr><td colspan=100 align=center>No DBase entries for this establishment</td></tr>";
	} else {
		foreach ($c as $k1 => $v1) {
			foreach ($v1 as $key=>$value) {
				if ($key == 'line_id' || $key == 'code') continue;
				$line_class = $line_class=='line1'?'line2':'line1';
				$table_data .= "<tr valign=top class=$line_class ><td class=field_label width=200px>$key</td><td class=field_label>$value</td></tr>";
			}
			$table_data .= "<tr><td colspan=100><hr></td></tr>";

		}
	}

	$table_data .= "</table>";

	//Replace Tags
	$template = str_replace('<!-- dbase_table -->', $table_data, $template);
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);

	echo $template;
}

?>