function delete_feedback(id) {
	if(confirm("WARNING!\nThis will permanently delete the selected feedback from the database.\nThis action cannot be undone.\n\nContinue")) {
		document.location="/modules/feedback_s/feedback.php?f=delete&id=" + id;
	}
}

function filter(v) {
	if (v == 'all') {
		eraseCookie('uFilter');
	} else {
		createCookie('uFilter', v);
	}
	document.location = document.location;
}

function sFilter() {
	v2 = document.getElementById('searchString').value;
	v1 = document.getElementById('searchFilter').value;
	document.location = '/modules/feedback_s/feedback.php?f=list&sFilter=' + v1 + '&sString=' + v2;
}

function showFilter() {
	v = readCookie('uFilter');
	document.getElementById('uFilter').value = v;
}

function createCookie(name,value) {
	var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function return_update(ok) {
	if (ok) {
		 document.getElementById('busyBar').style.display = 'none';
		 document.getElementById('doneBar').style.display = 'block';
	} else {
		document.getElementById('busyBar').style.display = 'none';
		document.getElementById('failBar').style.display = 'block';
	}
}
