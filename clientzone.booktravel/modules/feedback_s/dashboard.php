<?php
/**
 * Dashboard - Feedback
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 */

//Vars
$access = 0;

//Check access
$statement = "SELECT access FROM nse_user_access WHERE module_id='feedback_s' && function_id='approve' && parent_id='manage_feedback' && user_id=?";
$sql_access = $GLOBALS['dbCon']->prepare($statement);
$sql_access->bind_param('s', $_SESSION['dbweb_user_id']);
$sql_access->execute();
$sql_access->bind_result($access);
$sql_access->fetch();
$sql_access->close();

if ('1' == $access) display_dashboard();

/**
 * Display the dashboard contents
 *
 */
function display_dashboard() {
    //Vars
    $total = 0;
    $graph_location = "/modules/feedback_s/i/feedback_graph.png";
    $image_width = 200;
    $image_height = 80;
    $font_mono = SITE_ROOT . '/shared/fonts/VeraMono.ttf';
    $this_month = 0;
    $last_month =  0;

    //Get un-approved feedback
    $statement = "SELECT COUNT(*) FROM nse_establishment_feedback WHERE feedback_status='1'";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->execute();
    $sql_feedback->bind_result($total);
    $sql_feedback->store_result();
    $sql_feedback->fetch();
    $sql_feedback->close();
    
    //Generate graph
    $statement = "SELECT COUNT(*) FROM nse_establishment_feedback WHERE DATE_FORMAT(feedback_date,'%b')=?";
    $sql_count = $GLOBALS['dbCon']->prepare($statement);

    for ($x=0; $x<6; $x++) {
        $counter = 0;
        $month = date('M', strtotime("today - $x months"));
        $sql_count->bind_param('s', $month);
        $sql_count->execute();
        $sql_count->bind_result($counter);
        $sql_count->store_result();
        $sql_count->fetch();
        $month = date('M', strtotime("today - $x months"));
        $values[$month] = $counter;
        if (0 == $x) $this_month = $counter;
        if (1 == $x) $last_month = $counter;
    }
    $sql_count->close();

    $values = array_reverse($values, TRUE);
    
    $img_width=200;
	$img_height=80;
	$margins=20;

 
	# ---- Find the size of graph by substracting the size of borders
	$graph_width=$img_width - $margins * 2;
	$graph_height=$img_height - $margins * 2;
	$img=imagecreate($img_width,$img_height);

 
	$bar_width=20;
	$total_bars=count($values);
	$gap= ($graph_width- $total_bars * $bar_width ) / ($total_bars +1);

 
	# -------  Define Colors ----------------
	$bar_color=imagecolorallocate($img,0,64,128);
	$background_color=imagecolorallocate($img,240,240,255);
	$border_color=imagecolorallocate($img,200,200,200);
	$line_color=imagecolorallocate($img,220,220,220);
 
	# ------ Create the border around the graph ------

	imagefilledrectangle($img,1,1,$img_width-2,$img_height-2,$border_color);
	imagefilledrectangle($img,$margins,$margins-10,$img_width-1-$margins,$img_height-1-$margins,$background_color);

 
	# ------- Max value is required to adjust the scale	-------
	$max_value=max($values);
	$ratio= $graph_height/$max_value;

 
	# -------- Create scale and draw horizontal lines  --------
	$horizontal_lines=5;
	$horizontal_gap=$graph_height/$horizontal_lines;

	for($i=1;$i<=$horizontal_lines;$i++){
		$y=$img_height - $margins - $horizontal_gap * $i ;
		imageline($img,$margins,$y,$img_width-$margins,$y,$line_color);
		$v=intval($horizontal_gap * $i /$ratio);
		//imagestring($img,0,5,$y-5,$v,$bar_color);

	}
 
 
	# ----------- Draw the bars here ------
	for($i=0;$i< $total_bars; $i++){
		# ------ Extract key and value pair from the current pointer position
		list($key,$value)=each($values);
		$x1= $margins + $gap + $i * ($gap+$bar_width) ;
		$x2= $x1 + $bar_width;
		$y1=$margins +$graph_height- intval($value * $ratio) ;
		$y2=$img_height-$margins;
		imagestring($img,0,$x1+3,$y1-10,$value,$bar_color);
		imagestring($img,0,$x1+3,$img_height-15,$key,$bar_color);
		imagefilledrectangle($img,$x1,$y1,$x2,$y2,$bar_color);
	}


    
    
    imagepng($img, SITE_ROOT . $graph_location);
    
    //Serve
    echo "
    	<table cellpadding=0 cellspacing=0>
    		<tr>
    			<td rowspan=3><img src='$graph_location' width=$image_width height=$image_height></td>
    			<td>&nbsp;Feedback requiring your attention:</td>
    			<td>&nbsp;$total</td>
    		</tr>
    		<tr>
    			<td>&nbsp;Total feedback this month:</td>
    			<td>&nbsp;$this_month</td>
    		</tr>
    		<tr>
    			<td>&nbsp;Total feedback last month:</td>
    			<td>&nbsp;$last_month</td>
    		</tr>
    	</table>
    ";
    
}

?>
