<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'feedback_s';
$module_name = "Feedback Manager";
$module_icon = 'feedback.png';
$module_link = 'feedback.php';
$window_width = '800';
$window_height = '600';
$window_position = '';
$window_attribute = 2;

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 's'; //a = all
$sub_levels['manage_feedback']['name'] = 'Manage Feedback';
$sub_levels['manage_feedback']['subs']['approve']['name'] = 'Approve Feedback';
$sub_levels['manage_feedback']['subs']['delete']['name'] = 'Delete Feedback';
$sub_levels['manage_feedback']['subs']['disapprove']['name'] = 'Disapprove Feedback';
$sub_levels['manage_feedback']['subs']['hold']['name'] = 'Put Feedback on Hold';
$sub_levels['manage_feedback']['subs']['email']['name'] = 'Send Feedback Mail to Establishment';

//$sub_levels['manage_users']['subs']['add']['name'] = 'Add New Users';
//$sub_levels['manage_users']['subs']['delete']['name'] = 'Delete Users';
//$sub_levels['manage_users']['subs']['resend_password']['name'] = 'Resend Passwords';
//$sub_levels['manage_access']['name'] = 'Manage Access';
//$sub_levels['manage_access']['subs']['privileges']['name'] = 'Manage Privileges';
//$sub_levels['manage_access']['subs']['add_blocks']['name'] = 'Add Blocks';
//$sub_levels['manage_access']['subs']['remove_blocks']['name'] = 'Remove Blocks';

//General Settings
//if (!defined('SECURE_PHRASE')) define('SECURE_PHRASE', 'dbweb4_secure');
//if (!defined('LOGIN_TIMEOUT')) define('LOGIN_TIMEOUT', 30);
//$list_length = 20;

?>