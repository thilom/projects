<?php
/**
 * Manage Feedback - Staff Access
 *
 * Manage feedback recieved from visitors to establishments. Enables users to search, approve, hold and delete feedback.
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category feedback
 */

//Vars
$css = '';
$function = '';
$parent_id = '';
$user_access = '';
$access = '';
$module_id = '';
$function_id = '';
$access = array('approve_feedback'=>FALSE,
                'delete_feedback'=>FALSE,
                'disapprove_feedback'=>FALSE,
                'hold_feedback'=>FALSE,
                'email_feedback'=>FALSE);
$function_whitelist = array('list', 'view', 'hold', 'approve', 'disapprove', 'delete', 'edit_feedback', 'email');

if (isset($_GET['f'])) {
    $function_id = in_array($_GET['f'], $function_whitelist)?$_GET['f']:'';
} else {
    $function_id = '';
}

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/feedback_s/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';

//Prepare Statements
$statement = "SELECT function_id, parent_id, access FROM nse_user_access WHERE user_id=? && module_id=?";
$sql_access = $GLOBALS['dbCon']->prepare($statement);

//Get Access Rights
$sql_access->bind_param('ss', $_SESSION['dbweb_user_id'], $module_id);
$sql_access->execute();
$sql_access->bind_result($function, $parent_id, $user_access);
$sql_access->store_result();
while ($sql_access->fetch()) {
    if ($function == 'approve' && 'manage_feedback' == $parent_id && '1' == $user_access) $access['approve_feedback'] = TRUE;
    if ($function == 'delete' && $parent_id == 'manage_feedback' && $user_access == '1') $access['delete_feedback'] = TRUE;
    if ($function == 'disapprove' && $parent_id == 'manage_feedback' && $user_access == '1') $access['disapprove_feedback'] = TRUE;
    if ($function == 'hold' && $parent_id == 'manage_feedback' && $user_access == '1') $access['hold_feedback'] = TRUE;
    if ($function == 'email' && $parent_id == 'manage_feedback' && $user_access == '1') $access['email_feedback'] = TRUE;
}
$sql_access->close();

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/feedback_s/html/feedback.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/feedback_s/html/feedback.js'> </script>";

//MAP
switch ($function_id) {
    case 'list':
        list_feedback();
        break;
    case 'view':
        view_feedback();
        break;
    case 'hold':
        hold_feedback();
        break;
    case 'approve':
        approve_feedback();
        break;
    case 'disapprove':
        disapprove_feedback();
        break;
    case 'delete':
        delete_feedback();
        break;
    case 'edit_feedback':
    	if (isset($_POST['SaveFeedback'])) {
    		edit_feedback_save();
    	} else {
    		edit_feedack_form();
    	}
    	break;
    case 'email':
    	
    	if (isset($_POST['sendEmail'])) {
    		email_feedback_send();
    	} else {
    		email_feedback_form();
    	}
    	break;
    default:
        welcome();
}

/**
 * List all available feedback. Defaults to showing unapprved feedback.
 *
 */
function list_feedback() {
    //Vars
    $start = isset($_GET['s'])?$_GET['s']:'0';
    $feedback_id = '';
    $feedback_date = '';
    $establishment_name = '';
    $firstname = '';
    $surname = '';
    $matches = array();
    $nLine = '';
    $feedback_status = '';
    $where = '';
    $link = '/modules/feedback_s/feedback.php?f=' . $_GET['f'];
    $total_results = 0;
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/feedback_list.html');
    $content = str_replace(array("\r\n", "\r", "\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@', $content, $matches);
    $lContent = $matches[0][0];
    
    //Filters
    if (isset($_COOKIE['uFilter']) && $_COOKIE['uFilter'] != '') {
    	$where = " WHERE a.feedback_Status=";
    	switch ($_COOKIE['uFilter']) {
    		case 'approved':
    			$where .= "'2'";
    			break;
    		case 'disapproved':
    			$where .= "'0'";
    			break;
    		case 'hold':
    			$where .= "'3'";
    			break;
    		case 'waiting':
    			$where .= "'1'";
    			break;
    		default:
    			$where = "";
    	}
    }
    
    //Searches
    if (isset($_GET['sFilter'])) {
    	switch ($_GET['sFilter']) {
    		case 'establishmentName':
    			$where = " WHERE b.establishment_name LIKE '%{$_GET['sString']}%'";
    			break;
    		case 'establishmentCode':
    			$where = " WHERE b.establishment_code LIKE '%{$_GET['sString']}%'";
    			break;
    		case 'visitorName':
    			$where = " WHERE CONCAT(c.firstname, ' ' , c.surname) LIKE '%{$_GET['sString']}%'";
    			break;
    		case 'feedbackText':
    			$where = " WHERE a.visitor_comment LIKE '%{$_GET['sString']}%'";
    			break;
    	}
    	 $link .= "&sFilter={$_GET['sFilter']}&sString={$_GET['sString']}";
    }
    
    if (!isset($_COOKIE['uFilter']) && !isset($_GET['sFilter'])) $where = "WHERE a.feedback_Status='1'";
    
    //Get list of Feedback
    $statement = "SELECT a.feedback_id, DATE_FORMAT(a.feedback_date, '%d %b, %Y'), b.establishment_name, c.firstname, c.surname, a.feedback_status
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				$where
    				ORDER BY a.feedback_id DESC
    				LIMIT ?,?";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('ii', $start, $GLOBALS['list_length']);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_id, $feedback_date, $establishment_name, $firstname, $surname, $feedback_status);
    $sql_feedback->store_result();
    while ($sql_feedback->fetch()) {
        $buttons = '';
        $status = '';
        switch ($feedback_status) {
            case 0:
                $status = "<img src='/i/silk/bullet_red.png'>";
                break;
            case 2:
                $status = "<img src='/i/silk/bullet_green.png'>";
                break;
            case 1:
                $status = "<img src='/i/silk/bullet_go.png'>";
                break;
            case 3:
                $status = "<img src='/i/silk/bullet_orange.png'>";
                break;
        }
        
        //Full name
        $full_name = "$firstname $surname";
        if ($full_name == ' ') $full_name = 'Anonymous';
        
        $line = str_replace('<!-- date -->', $feedback_date, $lContent);
        $line = str_replace('<!-- name -->', $full_name, $line);
        $line = str_replace('<!-- status -->', $status, $line);
        $line = str_replace('<!-- establishment -->', $establishment_name, $line);
        if ($GLOBALS['access']['approve_feedback'] || $GLOBALS['access']['disapprove_feedback'] || $GLOBALS['access']['hold_feedback'] || $GLOBALS['access']['email_feedback']) $buttons .= "<input type=button value='View' class=view_button onClick='document.location=\"/modules/feedback_s/feedback.php?f=view&id=$feedback_id\"'>";
        if ($GLOBALS['access']['delete_feedback']) $buttons .= "<input type=button value='Delete' class=delete_button onClick=delete_feedback('$feedback_id')>";
        $line = str_replace('<!-- buttons -->', $buttons, $line);
        
        $nLine .= $line;
    }
    $sql_feedback->free_result();
    $sql_feedback->close();
    
	//Paging
    $statement = "SELECT COUNT(*)
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				$where";
    $sql_count = $GLOBALS['dbCon']->prepare($statement);
    $sql_count->execute();
    $sql_count->bind_result($total_results);
    $sql_count->store_result();
    $sql_count->fetch();
    $sql_count->close();
    if ($total_results < $GLOBALS['list_length']) {
        $paging = "<td id=td1>&#9668;First</td><td id=td2>&lt;Previous</td><td id=td3>  </td><td id=td4>Next&gt;</td><td id=td5>Last&#9658;</td>";
    } else {
        if ($start != 0) {
            $paging = "<td id=td1><a href='$link&s=0'>&#9668;First</a></td>";
            
            $s = $start - $GLOBALS['list_length'];
            $paging .= "<td id=td2><a href='$link&s=$s'>&lt;Previous</a></td>";
        } else {
            $paging = "<td id=td1>&#9668;First</td><td id=td2>&lt;Previous</td>";
        }
        
        if ($total_results > $GLOBALS['list_length']) {
            $paging .= "<td id=td3>";
            for($x=1; $x<ceil($total_results/$GLOBALS['list_length'])+1; $x++) {
                $ns = ($x-1) * $GLOBALS['list_length'];
                if ($ns == $start) {
                    $paging .= "<span class='active_page'>$x</span> |";
                } else {
                    $paging .= "<a href='$link&s=$ns'>$x</a> |";
                }
            }
            $paging = substr($paging, 0, -1);
            $paging .= "</td>";
        } else {
            $paging .= "<td id=td3>  </td>";
        }
        
        
        if ($start + $GLOBALS['list_length'] > $total_results) {
            $paging .= "<td id=td4>Next&gt;</td><td id=td5>Last&#9658;</td>";
        } else {
            $s = $start + $GLOBALS['list_length'];
            $paging .= "<td id=td4><a href='$link&s=$s'>Next&gt;</a></td>";
            
            $s = (floor($total_results/$GLOBALS['list_length'])) * $GLOBALS['list_length'];
            $paging .= "<td id=td5><a href='$link&s=$s'>Last&#9658;</a></td>";
        }
    }
    
    //Replace Tags
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    $content = str_replace('<!-- paging -->', $paging, $content);
    
    echo $content;
}

/**
 * View selected feedback
 *
 */
function view_feedback() {
    //Vars
    $feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
    $feedback_date = '';
    $establishment_name = '';
    $firstname = '';
    $surname = '';
    $feedback_message = '';
    $feedback_status = '';
    $matches = array();
    $nLine = '';
    $feedback_value = '';
    $feedback_description = '';
    $buttons = '';
    $town_name = '';
    $suburb_name = '';
    $establishment_code = '';
    $location = '';
    $reply = 'No Reply';
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/view_feedback.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@', $content, $matches);
    $lContent = $matches[0][0];
    
    //Get Data
    $statement = "SELECT DATE_FORMAT(a.feedback_date, '%d %M, %Y'), a.visitor_comment, a.feedback_status, a.establishment_code, c.firstname, c.surname
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE feedback_id=? ";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('s', $feedback_id);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_date, $feedback_message, $feedback_status, $establishment_code, $firstname, $surname);
    $sql_feedback->fetch();
    $sql_feedback->free_result();
    $sql_feedback->close();
    if (empty($feedback_message))$feedback_message = 'No Comment';
    
    //Get feedback results
    $statement = "SELECT a.feedback_value, b.category_description
    				FROM nse_feedback_results AS a
    				JOIN nse_feedback_category AS b ON a.category_code=b.category_code
    				WHERE a.feedback_id=?";
    $sql_results = $GLOBALS['dbCon']->prepare($statement);
    $sql_results->bind_param('s', $feedback_id);
    $sql_results->execute();
    $sql_results->bind_result($feedback_value, $feedback_description);
    $sql_results->store_result();
    while ($sql_results->fetch()) {
        $line = str_replace('<!-- feedback_item -->', $feedback_description, $lContent);
        
        for ($x=5; $x>=-1; $x--) {
            if ($x == $feedback_value) {
                $line = str_replace("<!-- feedback_value$x -->", "<img src='/i/silk/asterisk_orange.png'", $line);
            } else {
                $line = str_replace("<!-- feedback_value$x -->", '', $line);
            }
        }
        
        $nLine .= $line;
    }
    
    //Get Establishment Location
    $statement = "SELECT a.establishment_name, c.town_name, d.suburb_name
    				FROM nse_establishment AS a
    				JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_location_town AS c ON b.town_id=c.town_id
    				LEFT JOIN nse_location_suburb AS d ON b.suburb_id=d.suburb_id
    				WHERE a.establishment_code=?";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->bind_result($establishment_name, $town_name, $suburb_name);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
    
    //Assemble Location
    if (!empty($suburb_name)) {
        $location = "$suburb_name, $town_name";
    } else {
        $location = $town_name;
    }
    
    //Calculate status
    switch ($feedback_status) {
        case 0:
            $status = 'Disapproved';
            break;
        case 1:
            $status = 'Awaiting Action';
            break;
        case 2:
            $status = 'Approved (Live)';
            break;
        case 3:
            $status = 'On Hold';
            break;
        default:
            $status = '';
    }
    
    //Generate Buttons
    $buttons .= "<input type=button class=back_button value='Back' onClick='document.location=\"/modules/feedback_s/feedback.php?f=list\"'>";
    if ($GLOBALS['access']['delete_feedback']) $buttons .= "<input type=button class=pause_button value='Hold' onClick='document.location=\"/modules/feedback_s/feedback.php?f=hold&id=$feedback_id\"'>";
    if ($GLOBALS['access']['delete_feedback']) $buttons .= "<input type=button class=email_button value='Email' onClick='document.location=\"/modules/feedback_s/feedback.php?f=email&id=$feedback_id\"'>";
    if ($GLOBALS['access']['delete_feedback']) $buttons .= "<input type=button class=email_button value='Email & Hold' onClick='document.location=\"/modules/feedback_s/feedback.php?f=email&id=$feedback_id&hold\"'>";
    if ($GLOBALS['access']['approve_feedback']) $buttons .= "<input type=button class=ok_button value='Approve' onClick='document.location=\"/modules/feedback_s/feedback.php?f=approve&id=$feedback_id\"'>";
    if ($GLOBALS['access']['approve_feedback']) $buttons .= "<input type=button class=edit_button value='Edit & Approve' onClick='document.location=\"/modules/feedback_s/feedback.php?f=edit_feedback&id=$feedback_id\"'>";
    if ($GLOBALS['access']['disapprove_feedback']) $buttons .= "<input type=button class=cross_button value='Disapprove' onClick='document.location=\"/modules/feedback_s/feedback.php?f=disapprove&id=$feedback_id\"'>";
    if ($GLOBALS['access']['delete_feedback']) $buttons .= "<input type=button class=delete_button value='Delete' onClick=delete_feedback('$feedback_id')>";
    
    //Replace Tags
    $content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
    $content = str_replace('<!-- feedback_name -->', "$firstname $surname", $content);
    $content = str_replace('<!-- feedback_date -->', $feedback_date, $content);
    $content = str_replace('<!-- feedback_message -->', $feedback_message, $content);
    $content = str_replace('<!-- feedback_status -->', $status, $content);
    $content = str_replace('<!-- buttons -->', $buttons, $content);
    $content = str_replace('<!-- location -->', $location, $content);
    $content = str_replace('<!-- reply -->', $reply, $content);
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    
    echo $content;
}

/**
 * Display a form to email feedback to an establishment and select the prefered email addresses.
 */
function email_feedback_form() {
	//Vars
    $feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
    $feedback_date = '';
    $establishment_name = '';
    $firstname = '';
    $surname = '';
    $feedback_message = '';
    $feedback_status = '';
    $matches = array();
    $nLine = '';
    $nEmail = '';
    $feedback_value = '';
    $feedback_description = '';
    $buttons = '';
    $town_name = '';
    $suburb_name = '';
    $establishment_code = '';
    $location = '';
    $reply = 'No Reply';
    $color = '';
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/email_feedback.html');
    preg_match_all('@<!-- email_start -->(.)*<!-- email_end -->@i', $content, $matches);
    $lEmail = $matches[0][0];
    $email_content = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/email_content.html');
    $email_content = str_replace(array("\r\n","\r","\n"), '$%^', $email_content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@', $email_content, $matches);
    $lContent = $matches[0][0];
    
    //Get Data
    $statement = "SELECT DATE_FORMAT(a.feedback_date, '%d %M, %Y'), a.visitor_comment, a.feedback_status, a.establishment_code, c.firstname, c.surname
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE feedback_id=? ";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('s', $feedback_id);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_date, $feedback_message, $feedback_status, $establishment_code, $firstname, $surname);
    $sql_feedback->fetch();
    $sql_feedback->free_result();
    $sql_feedback->close();
    if (empty($feedback_message))$feedback_message = 'No Comment';
    
    //Get feedback results
    $statement = "SELECT a.feedback_value, b.category_description
    				FROM nse_feedback_results AS a
    				JOIN nse_feedback_category AS b ON a.category_code=b.category_code
    				WHERE a.feedback_id=?";
    $sql_results = $GLOBALS['dbCon']->prepare($statement);
    $sql_results->bind_param('s', $feedback_id);
    $sql_results->execute();
    $sql_results->bind_result($feedback_value, $feedback_description);
    $sql_results->store_result();
    while ($sql_results->fetch()) {
    	$color = ($color=='#DDDDDD')?'#E1E1E1':'#DDDDDD';
        $line = str_replace('<!-- feedback_item -->', $feedback_description, $lContent);
        $line = str_replace('<!-- line_color -->', $color, $line);
        for ($x=5; $x>=-1; $x--) {
            if ($x == $feedback_value) {
                $line = str_replace("<!-- feedback_value$x -->", "*", $line);
            } else {
                $line = str_replace("<!-- feedback_value$x -->", '&nbsp;', $line);
            }
        }
        
        $nLine .= $line;
    }
    
    //Get Establishment Location
    $statement = "SELECT a.establishment_name, c.town_name, d.suburb_name
    				FROM nse_establishment AS a
    				JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_location_town AS c ON b.town_id=c.town_id
    				LEFT JOIN nse_location_suburb AS d ON b.suburb_id=d.suburb_id
    				WHERE a.establishment_code=?";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->bind_result($establishment_name, $town_name, $suburb_name);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
    
    //Assemble Location
    if (!empty($suburb_name)) {
        $location = "$suburb_name, $town_name";
    } else {
        $location = $town_name;
    }
    
    //Get general email address
    $send_email = '';
    $statement = "SELECT contact_email FROM nse_establishment_contact WHERE establishment_code=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);
    $sql_email->bind_param('s', $establishment_code);
    $sql_email->execute();
    $sql_email->bind_result($send_email);
    $sql_email->store_result();
    $sql_email->fetch();
    $sql_email->free_result();
    $sql_email->close();
    if (!empty($send_email)) {
    	$checked = empty($nEmail)?'checked':'';
    	$line = str_replace('<!-- email_name -->', "$send_email (General)", $lEmail);
    	$line = str_replace('<!-- checked -->', $checked, $line);
    	$line = str_replace('<!-- checkbox_value -->', $send_email, $line);
    	$nEmail .= $line;
    }
    
    //Get ReservationEmail
    $statement = "SELECT reservation_email FROM nse_establishment_reservation WHERE establishment_code=?";
    $sql_reservation = $GLOBALS['dbCon']->prepare($statement);
    $sql_reservation->bind_param('s', $establishment_code);
    $sql_reservation->execute();
    $sql_reservation->bind_result($send_email);
    $sql_reservation->store_result();
    $sql_reservation->fetch();
    $sql_reservation->free_result();
    $sql_reservation->close();
    if (!empty($send_email)) {
    	$nEmail .= empty($nEmail)?'':'<br />';
    	$checked = empty($nEmail)?'checked':'';
    	$line = str_replace('<!-- email_name -->', "$send_email (Reservations)", $lEmail);
    	$line = str_replace('<!-- checked -->', $checked, $line);
    	$line = str_replace('<!-- checkbox_value -->', $send_email, $line);
    	$nEmail .= $line;
    }
    
    //Add freeform field
    $nEmail .= "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name=email[] /> (Enter Additional Email)";
    
    //Replace Tags
    $email_content = str_replace('<!-- establishment_name -->', $establishment_name, $email_content);
    $email_content = str_replace('<!-- feedback_name -->', "$firstname $surname", $email_content);
    $email_content = str_replace('<!-- feedback_date -->', $feedback_date, $email_content);
    $email_content = str_replace('<!-- feedback_message -->', $feedback_message, $email_content);
    $email_content = str_replace('<!-- location -->', $location, $email_content);
    $email_content = str_replace('<!-- reply -->', $reply, $email_content);
    $email_content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@', $nLine, $email_content);
    $email_content = str_replace('$%^', "\n", $email_content);
    $content = str_replace('<!-- email_content -->', htmlentities($email_content), $content);
    $content = str_replace('<!-- id -->', $feedback_id, $content);
    $content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
    $content = str_replace('<!-- location -->', $location, $content);
    $content = preg_replace('@<!-- email_start -->(.)*<!-- email_end -->@i', $nEmail, $content);
	
    echo $content;
}

/**
 * Send the feedback email
 */
function email_feedback_send() {
	//Vars
	$feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
	$email_content = $_POST['email_content'];
	$mail = new PHPMailer();
	$establishment_name = '';
	$town_name = '';
	$suburb_name = '';
	$feedback_date = '';
	$feedback_message = '';
	$feedback_status = '';
	$establishment_code = '';
	$firstname = '';
	$surname = '';
	$email_list = '';

	//Get Data
	$statement = "SELECT DATE_FORMAT(a.feedback_date, '%d %M, %Y'), a.visitor_comment, a.feedback_status, a.establishment_code, c.firstname, c.surname
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE feedback_id=? ";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('s', $feedback_id);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_date, $feedback_message, $feedback_status, $establishment_code, $firstname, $surname);
    $sql_feedback->fetch();
    $sql_feedback->free_result();
    $sql_feedback->close();
    if (empty($feedback_message))$feedback_message = 'No Comment';
    
	//Get Establishment Location
    $statement = "SELECT a.establishment_name, c.town_name, d.suburb_name
    				FROM nse_establishment AS a
    				JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_location_town AS c ON b.town_id=c.town_id
    				LEFT JOIN nse_location_suburb AS d ON b.suburb_id=d.suburb_id
    				WHERE a.establishment_code=?";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->bind_result($establishment_name, $town_name, $suburb_name);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
    
    //Assemble Location
    if (!empty($suburb_name)) {
        $location = "$suburb_name, $town_name";
    } else {
        $location = $town_name;
    }
    
    //Send email
	foreach ($_POST['email'] as $email) {
		$mail->AddAddress($email);
		$email_list .= " $email,";
	}
	$email_list = substr($email_list, 0, -1);
	//if (isset($_POST['addEmail']) && !empty($_POST['addEmail'])) $mail->AddAddress($_POST['addEmail']);
	try {
		$mail->AddReplyTo('feedback@aatravel.co.za', 'AA Travel Guides');
		$mail->SetFrom('feedback@aatravel.co.za', 'AA Travel Guides');
		$mail->Subject = "Feedback for $establishment_name ($location) from $firstname, $surname ";
		$mail->MsgHTML($email_content);
		$mail->Send();
		echo "<script>alert('Feedback sent to $email_list.');</script>";
			
		//Log
		$GLOBALS['log_tool']->write_entry("Sent feedback (id: $feedback_id) to $establishment_name", $_SESSION['dbweb_user_id']);
    } catch (phpmailerException $e) {
      echo "<script>alert('ERROR!\nFeedback email to $establishment_name ($email) failed');</script>";

      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Feedback (id: $feedback_id) email to $establishment_name($email)Failed. $error", $_SESSION['dbweb_user_id']);
    } catch (Exception $e) {
      echo "<script>alert('ERROR!\nFeedback(id: $feedback_id) email to $establishment_name($email) failed');</script>";

      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Feedback(id: $feedback_id) email to $establishment_name($email) Failed. $error", $_SESSION['dbweb_user_id']);
    }
	
	if (isset($_GET['hold'])) {
		hold_feedback();
	} else {
		echo "<script>document.location='/modules/feedback_s/feedback.php?f=view&id=$feedback_id'</script>";
	}
	
}

/**
 * Places feedback on hold. ie Status is set to 3.
 */
function hold_feedback() {
    $feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
    $establishment_code = '';
    $establishment_name = '';
    
    $statement = "UPDATE nse_establishment_feedback SET feedback_status='3' WHERE feedback_id=?";
    $sql_status = $GLOBALS['dbCon']->prepare($statement);
    $sql_status->bind_param('s', $feedback_id);
    $sql_status->execute();
    
	//Get establishment Code
    $statement = "SELECT a.establishment_code, b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $feedback_id);
    $sql_estab->execute();
    $sql_estab->bind_result($establishment_code, $establishment_name);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();
    
    update_cache($feedback_id);

    //Get template
    $template = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/update_page.html');
    
    $template = str_replace('<!-- code -->', $establishment_code, $template);
    $template = str_replace('<!-- id -->', $feedback_id, $template);
    echo $template;
    flush();
    
    $ch = curl_init("http://www.aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	$GLOBALS['log_tool']->write_entry("Feedback (id: $feedback_id) for $establishment_name($establishment_code) - Status changed to 'On Hold'", $_SESSION['dbweb_user_id']);
	
    if ($res == true) {
    	echo "<script>return_update(true)</script>";
    } else {
    	echo "<script>return_update(false)</script>";
    }
}

/**
 * Sets feedback as approved and displayable on the website. ie. Sets the feedback status to 2.
 */
function approve_feedback() {
    //Vars
	$feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
    $establishment_code = '';
    $establishment_name = '';
	
    $statement = "UPDATE nse_establishment_feedback SET feedback_status='2' WHERE feedback_id=?";
    $sql_status = $GLOBALS['dbCon']->prepare($statement);
    $sql_status->bind_param('s', $feedback_id);
    $sql_status->execute();
    
    //Get establishment Code
    $statement = "SELECT a.establishment_code, b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $feedback_id);
    $sql_estab->execute();
    $sql_estab->bind_result($establishment_code, $establishment_name);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();
    
    update_cache($feedback_id);

    //Get template
    $template = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/update_page.html');
    
    $template = str_replace('<!-- code -->', $establishment_code, $template);
    $template = str_replace('<!-- id -->', $feedback_id, $template);
    echo $template;
    flush();
    
    $ch = curl_init("http://www.aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	$GLOBALS['log_tool']->write_entry("Feedback (id: $feedback_id) for $establishment_name ($establishment_code) - Status changed to 'Approved'", $_SESSION['dbweb_user_id']);
	
    if ($res == true) {
    	
    	echo "<script>return_update(true)</script>";
    } else {
    	echo "<script>return_update(false)</script>";
    }
   
    
    
    //echo "<script>document.location='/modules/feedback_s/feedback.php?f=list'</script>";
}

/**
 * Sets feedback as disapproved and non-displayable on the website. ie. Sets the feedback status to 0.
 */
function disapprove_feedback() {
    $feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
    $establishment_code = '';
    $establishment_name = '';
    
    $statement = "UPDATE nse_establishment_feedback SET feedback_status='0' WHERE feedback_id=?";
    $sql_status = $GLOBALS['dbCon']->prepare($statement);
    $sql_status->bind_param('s', $feedback_id);
    $sql_status->execute();
    
	//Get establishment Code
    $statement = "SELECT a.establishment_code, b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $feedback_id);
    $sql_estab->execute();
    $sql_estab->bind_result($establishment_code, $establishment_name);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();
    
    update_cache($feedback_id);

    //Get template
    $template = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/update_page.html');
    
    $template = str_replace('<!-- code -->', $establishment_code, $template);
    $template = str_replace('<!-- id -->', $feedback_id, $template);
    echo $template;
    flush();
    
    $ch = curl_init("http://www.aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	$GLOBALS['log_tool']->write_entry("Feedback (id: $feedback_id) for $establishment_name ($establishment_code) - Status changed to 'Disapproved'", $_SESSION['dbweb_user_id']);
	
    if ($res == true) {
    	
    	echo "<script>return_update(true)</script>";
    } else {
    	echo "<script>return_update(false)</script>";
    }
}

/**
 * Permanently delete feedback from the DB
 */
function delete_feedback() {
    $feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
    $establishment_name = '';
   
    
    //Get establishment Code
    $statement = "SELECT b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $feedback_id);
    $sql_estab->execute();
    $sql_estab->bind_result($establishment_name);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();
    
    $statement = "DELETE FROM nse_establishment_feedback WHERE feedback_id=?";
    $sql_status = $GLOBALS['dbCon']->prepare($statement);
    $sql_status->bind_param('s', $feedback_id);
    $sql_status->execute();
    
    $statement = "DELETE FROM nse_feedback_results WHERE feedback_id=?";
    $sql_status = $GLOBALS['dbCon']->prepare($statement);
    $sql_status->bind_param('s', $feedback_id);
    $sql_status->execute();
    
    update_cache($feedback_id);
    
    $GLOBALS['log_tool']->write_entry("Feedback (id: $feedback_id) for $establishment_name - Deleted", $_SESSION['dbweb_user_id']);
    
    echo "<script>document.location='/modules/feedback_s/feedback.php?f=list'</script>";
}

/**
 * Edit a visitor comment before approving it.
 */
function edit_feedack_form() {
	//Vars
	$feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
	$feedback_message = '';
	$establishment_code = '';
	$firstname = '';
	$surname = '';
	$establishment_name = '';
	$town_name = '';
	$suburb_name = '';
	
	//Get template
	$content = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/edit_feedback.html');
	
	//Get Data
	$statement = "SELECT a.visitor_comment, a.establishment_code, c.firstname, c.surname
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE feedback_id=? ";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->bind_param('s', $feedback_id);
    $sql_feedback->execute();
    $sql_feedback->bind_result($feedback_message, $establishment_code, $firstname, $surname);
    $sql_feedback->fetch();
    $sql_feedback->free_result();
    $sql_feedback->close();
    
    //Get Establishment Location
    $statement = "SELECT a.establishment_name, c.town_name, d.suburb_name
    				FROM nse_establishment AS a
    				JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_location_town AS c ON b.town_id=c.town_id
    				LEFT JOIN nse_location_suburb AS d ON b.suburb_id=d.suburb_id
    				WHERE a.establishment_code=?";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->bind_result($establishment_name, $town_name, $suburb_name);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
    
    //Assemble Location
    if (!empty($suburb_name)) {
        $location = "$suburb_name, $town_name";
    } else {
        $location = $town_name;
    }
    
    //Replace Tags
    $content = str_replace('<!-- message -->', $feedback_message, $content);
    $content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
    $content = str_replace('<!-- location -->', $location, $content);
    $content = str_replace('<!-- id -->', $feedback_id, $content);
    
	echo $content;
}

/**
 * Update the DB with updated feedback message
 */
function edit_feedback_save() {
	$feedback_id = ctype_digit($_GET['id'])?$_GET['id']:'';
	$establishment_name = '';
	
	//Update Message
	$statement = "UPDATE nse_establishment_feedback SET visitor_comment=? WHERE feedback_id=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bind_param('ss', $_POST['new_comment'], $feedback_id);
	$sql_update->execute();
	$sql_update->close();
	
	//Get establishment Code
    $statement = "SELECT b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $feedback_id);
    $sql_estab->execute();
    $sql_estab->bind_result($establishment_name);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();
	
	$GLOBALS['log_tool']->write_entry("Feedback (id: $feedback_id) for $establishment_name - Edited", $_SESSION['dbweb_user_id']);
	
	approve_feedback();
	
}

/**
 * Default view when opening feedback. Mainly used as an entry page for the module
 *
 */
function welcome() {
    //Vars
    $total = 0;
    $hold = 0;
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/feedback_s/html/welcome.html');
    
    //Get un-approved feedback
    $statement = "SELECT COUNT(*) FROM nse_establishment_feedback WHERE feedback_status='1'";
    $sql_feedback = $GLOBALS['dbCon']->prepare($statement);
    $sql_feedback->execute();
    $sql_feedback->bind_result($total);
    $sql_feedback->store_result();
    $sql_feedback->fetch();
    $sql_feedback->close();
    
    //Get feedback on hold
    $statement = "SELECT COUNT(*) FROM nse_establishment_feedback WHERE feedback_status='3'";
    $sql_hold = $GLOBALS['dbCon']->prepare($statement);
    $sql_hold->execute();
    $sql_hold->bind_result($hold);
    $sql_hold->store_result();
    $sql_hold->fetch();
    $sql_hold->close();
    
    //Replace Tags
    $content = str_replace('<!-- attention -->', $total, $content);
    $content = str_replace('<!-- hold -->', $hold, $content);
    
    echo $content;
}

/**
 * Recalculate average feedback results and saves it to a table for use on the website.
 *
 * @param $feedback_id
 */
function update_cache($feedback_id) {
    //Vars
    $results = array();
	$totals = array();
	$establishment_code = '';
	$feedback_id_bulk = '';
	$code = '';
	$value = '';
	
	//Get Establishment Code
	$statement = "SELECT establishment_code FROM nse_establishment_feedback WHERE feedback_id=?";
	$sql_code = $GLOBALS['dbCon']->prepare($statement);
	$sql_code->bind_param('s', $feedback_id);
	$sql_code->execute();
	$sql_code->bind_result($establishment_code);
	$sql_code->fetch();
	$sql_code->free_result();
	$sql_code->close();

	
	//Clear cache
	$statement = "DELETE FROM nse_feedback_cache WHERE establishment_code=?";
	$sql_cache_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_cache_delete->bind_param('s', $establishment_code);
	$sql_cache_delete->execute();
	
	$statement = "SELECT category_code, feedback_value FROM nse_feedback_results WHERE feedback_id=?";
	$sql_cache_results = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "SELECT feedback_id FROM nse_establishment_feedback WHERE establishment_code=? && feedback_status='2'";
	$sql_cache_ids = $GLOBALS['dbCon']->prepare($statement);
	$sql_cache_ids->bind_param('s', $establishment_code);
	$sql_cache_ids->execute();
	$sql_cache_ids->bind_result($feedback_id_bulk);
	$sql_cache_ids->store_result();
	while ($sql_cache_ids->fetch()) {
		$sql_cache_results->bind_param('s', $feedback_id_bulk);
		$sql_cache_results->execute();
		$sql_cache_results->bind_result($code, $value);
		$sql_cache_results->store_result();
		while ($sql_cache_results->fetch()) {
			if (0 == $value) continue;
			if (!isset($results[$code])) $results[$code] = array();
			$results[$code][] = $value;
		}
	}
	
	$statement = "INSERT INTO nse_feedback_cache (establishment_code, category_code, smilies) VALUES (?,?,?)";
	$sql_cache_insert = $GLOBALS['dbCon']->prepare($statement);
	foreach ($results as $c=>$code) {
		$totals[$c] = round(array_sum($code)/count($code));
		$sql_cache_insert->bind_param('sss', $establishment_code, $c, $totals[$c]);
		$sql_cache_insert->execute();
	}
	
	$tot = (count($totals)!= 0)?round(array_sum($totals)/count($totals)):'';
	$c = "total";
	$sql_cache_insert->bind_param('sss', $establishment_code, $c, $tot);
	$sql_cache_insert->execute();
}

?>