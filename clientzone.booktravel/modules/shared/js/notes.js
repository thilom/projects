function clear_note() {
	if (document.getElementById('new_note').value == 'Type note here and click add') document.getElementById('new_note').value = '';
}

function unclear_note() {
	if (document.getElementById('new_note').value == '') document.getElementById('new_note').value = 'Type note here and click add';
}

function add_note() {
	today = new Date();
	day = today.getDate().toString().length==1?"0"+today.getDate():today.getDate();
	month = today.getMonth()+1;
	month = month.toString().length==1?"0"+month:month;
	hour = today.getHours().toString().length==1?"0"+today.getHours():today.getHours();
	minutes = today.getMinutes().toString().length==1?"0"+today.getMinutes():today.getMinutes();
	date_string = day + '-' + month + '-' + today.getFullYear() + ' ' + hour + ':' + minutes;

	new_note = document.getElementById('new_note').value;
	id = document.getElementById('add_button').name;
	if (id == '') {
		document.getElementById('notes').value = cnt + '|' + date_string + '|' + new_note + '##' + document.getElementById('notes').value;
		cnt++;
	} else {
		notes_line = document.getElementById('notes').value.split('##');
		document.getElementById('notes').value = '';
		for (i=0; i<notes_line.length; i++) {
			if (notes_line[i] == '') continue;
			notes_item = notes_line[i].split('|');
			if (id == notes_item[0]) {
				document.getElementById('notes').value += '##' + notes_item[0] + '|' + notes_item[1] + '|' + new_note;
			} else {
				document.getElementById('notes').value += '##' + notes_line[i];
			}
		}
		cancel_note();
	}

	draw_notes();
	set_change('notes','notes');
}

function edit_note(id) {
	notes_line = document.getElementById('notes').value.split('##');
	for (i=0; i<notes_line.length; i++) {
		if (notes_line[i] == '') continue;
		notes_item = notes_line[i].split('|');
		if (id == notes_item[0]) {
			document.getElementById('new_note').value = notes_item[2];
			document.getElementById('add_button').value = 'Update Note';
			document.getElementById('add_button').name=id;
		}
	}
}

function cancel_note() {
	document.getElementById('new_note').value = 'Type note here and click add';
	document.getElementById('add_button').value = 'Add Note';
	document.getElementById('add_button').name='';
}

function delete_note(id) {
	notes_line = document.getElementById('notes').value.split('##');
	document.getElementById('notes').value = '';
	for (i=0; i<notes_line.length; i++) {
		if (notes_line[i] == '') continue;
		notes_item = notes_line[i].split('|');
		if (id != notes_item[0]) {
			document.getElementById('notes').value += '##' + notes_line[i];
		}
	}
	draw_notes();
}

function draw_notes() {
	notes_table = "<table id=notes_list >";

	notes_line = document.getElementById('notes').value.split('##');
	for (i=0; i<notes_line.length; i++) {
		if (notes_line[i] == '') continue;
		notes_item = notes_line[i].split('|');
		notes_table += "<tr>";
		notes_table += "<td class=note_name>New Note</td>";
		notes_table += "<td class=note_date>" + notes_item[1] + "</td>";
		notes_table += "<td class=note_content>" + notes_item[2] + "</td>";
		notes_table += "<td class=note_buttons><input type=button value='Edit' class=edit_button onclick=edit_note(" + notes_item[0] + ")>";
		notes_table += "<input type=button value='Delete' class=delete_button onclick=delete_note(" + notes_item[0] + ")></td>";
		notes_table += "</tr>";
	}

	notes_table += "</table>";

	document.getElementById('new_notes').innerHTML = notes_table;
	document.getElementById('new_note').value = 'Type note here and click add';
}


