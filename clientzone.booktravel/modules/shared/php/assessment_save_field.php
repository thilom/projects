<?php
/**
 * Updates the assessment fields. Called from the assessment form via AJAX
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category assessments
 **/

//Vars
$assessment_id = 0;
$area_id = $_POST['field'];
$area_content = $_POST['data'];
$establishment_code = $_POST['establishment_code'];


require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Check if draft exists
$statement = "SELECT assessment_id FROM nse_establishment_assessment WHERE (assessment_status='draft' || assessment_status='returned') && establishment_code=? ORDER BY assessment_id LIMIT 1";
$sql_check = $GLOBALS['dbCon']->prepare($statement);
$sql_check->bind_param('s', $establishment_code);
$sql_check->execute();
$sql_check->store_result();
$sql_check->bind_result($assessment_id);
$sql_check->fetch();
$sql_check->free_result();
$sql_check->close();

//Create assessment if it doesn't exist
if ($assessment_id == 0) {
	$statement = "INSERT INTO nse_establishment_assessment (establishment_code, assessment_status) VALUES (?, 'draft')";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_insert->bind_param('s', $establishment_code);
	$sql_insert->execute();
	$assessment_id = $sql_insert->insert_id;
	$sql_insert->close();
}

//Check if area data exists
$statement = "SELECT COUNT(*) FROM nse_establishment_assessment_data WHERE assessment_area_id=? && assessment_id=?";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->bind_param('is', $area_id, $assessment_id);
$sql_count->execute();
$sql_count->store_result();
$sql_count->bind_result($counter);
$sql_count->fetch();
$sql_count->free_result();
$sql_count->close();

//Update or insert area
if ($counter == 0) {
	$statement = "INSERT INTO nse_establishment_assessment_data (assessment_id, assessment_area_id, assessment_content) VALUES (?,?,?)";
	$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessment->bind_param('iss', $assessment_id, $area_id, $area_content);
	$sql_assessment->execute();
	$sql_assessment->close();
} else {
	$statement = "UPDATE nse_establishment_assessment_data SET assessment_content=? WHERE assessment_id=? && assessment_area_id=?";
	$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessment->bind_param('sii', $area_content, $assessment_id, $area_id);
	$sql_assessment->execute();
	$sql_assessment->close();
}


?>
