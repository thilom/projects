<?php
/**
 * Checks the AA status of the establishment
 * Looks for an award winner, publication, Quality assurance and advertiser
 *
 * @author Thilo Muller(2010)
 * @package RVBUS
 * @category general
 */

function get_status($establishment_code) {
    //Vars
    $aa_count = 0;
    $award_count = 0;
    $aa_status = FALSE;
    $grade = '';


    //Get current award year
    $statement = "SELECT MAX(year) FROM award";
    $sql_year = $GLOBALS['dbCon']->prepare($statement);
    $sql_year->execute();
    $sql_year->bind_result($current_award_year);
    $sql_year->fetch();
    $sql_year->free_result();
    $sql_year->close();

    //Prepare statement - Award status
    $statement = "SELECT COUNT(*) FROM award WHERE year=? && code=? LIMIT 1";
    $sql_award = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - aa status publications
    $statement = "SELECT COUNT(*)
                    FROM nse_establishment_publications AS a
                    JOIN nse_publications AS b ON a.publication_id=b.publication_id
                    WHERE a.establishment_code=? && b.expiry_date>NOW()";
    $sql_aa_estab2 = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - update estab
    $statement = "UPDATE nse_establishment SET aa_estab=? WHERE establishment_code=?";
    $sql_update = $GLOBALS['dbCon']->prepare($statement);

    $statement = "SELECT aa_category_code, advertiser, aa_estab FROM nse_establishment WHERE establishment_code=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $establishment_code);
    $sql_estab->execute();
    $sql_estab->store_result();
    $sql_estab->bind_result($grade, $advertiser, $old_status);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();


    //Calculate Grading
    if (!empty($grade) || in_array($advertiser, array('P', 'L', 'A', 'B', 'S'))) {
        $aa_status = TRUE;
    } else {
        $sql_aa_estab2->bind_param('s', $establishment_code);
        $sql_aa_estab2->execute();
        $sql_aa_estab2->store_result();
        $sql_aa_estab2->bind_result($aa_estab_count);
        $sql_aa_estab2->fetch();
        $sql_aa_estab2->free_result();
        if ($aa_estab_count > 0) $aa_status = TRUE;
    }

    //Check award winner
    $sql_award->bind_param('ss', $current_award_year, $establishment_code);
    $sql_award->execute();
    $sql_award->bind_result($award_count);
    $sql_award->fetch();
    $sql_award->free_result();
    if ($award_count == 1) $aa_status = TRUE;
    return $aa_status;

}

?>
