<?php
/**
 * Module settings for Establishment management - Client Access
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category establishment
 */
$module_id = 'documents_c';
$module_name = "Documents";
$module_icon = 'folder-64.png';
$noaccess_icon = 'folder_noaccess.png';
$module_link = 'documents_c.php';
$window_width = '600';
$window_height = '800';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 'c'; //a = all




?>