<?php
//Vars
$assessment_id = $_GET['id'];
$establishment_code = '';
$renewal_date = '';

include $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Get Assessment Data
$statement = "SELECT establishment_code, DATE_FORMAT(renewal_date, '%M %Y')
				FROM nse_establishment_assessment
				WHERE assessment_id=?
				LIMIT 1";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bind_param('i', $assessment_id);
$sql_category->execute();
$sql_category->store_result();
$sql_category->bind_result($establishment_code, $renewal_date);
$sql_category->fetch();
$sql_category->free_result();
$sql_category->close();

$report_file = strtolower("$establishment_code report $renewal_date");
$report_file = str_replace(' ', '_', $report_file) . ".pdf";

if (!is_file($_SERVER['DOCUMENT_ROOT'] . "/reports/$report_file")) include $_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/create_report.php';

header("Content-disposition: attachment; filename=$report_file");
header('Content-type: application/pdf');
readfile($_SERVER['DOCUMENT_ROOT'] . "/reports/$report_file");

?>