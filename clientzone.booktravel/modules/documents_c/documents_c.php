<?php

/**
 * Manage establishment Data
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 */

//Vars
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/documents_c/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/documents_c/html/documents_c.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/documents_c/html/documents_c.js'> </script>";

//Map
switch ($function_id) {
	case 'edit':
		if (isset($_POST['save_estab'])) {
			save_form();
		} else {
			edit_form();
		}
		break;
	default:
		welcome();
}

function welcome() {
	//Vars
	$assessment_id = '';
	$certificate_line = '';
	$class = '';
	$code = '';
	$codes = array();
	$establishment_code = '';
	$establishment_name = '';
	$hires_logo = '';
	$image_count = 0;
	$images = array();
	$logo_line = '';
	$logo_name = '';
	$lowres_logo = '';
	$nLine = '';
	$qa_status = '';
	$renewal_date = '';
	
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/documents_c/html/welcome.html');
	$template = str_replace(array("\r\n","\r","\n"), '$%^', $template);
    preg_match_all('@<!-- block_start -->(.)*<!-- block_end -->@i', $template, $matches);
    $lTemplate = $matches[0][0];
	
    //Get establishment codes
    $statement = "SELECT establishment_code FROM nse_user_establishments WHERE user_id=?";
    $sql_codes = $GLOBALS['dbCon']->prepare($statement);
    $sql_codes->bind_param('i', $_SESSION['dbweb_user_id']);
    $sql_codes->execute();
    $sql_codes->store_result();
    $sql_codes->bind_result($code);
    while ($sql_codes->fetch()) {
    	$codes[] = $code;
    }
    $sql_codes->free_result();
    $sql_codes->close();
    
    //Prepare Statement - Certificates
    $statement = "SELECT b.aa_category_name, DATE_FORMAT(a.renewal_date, '%M %Y'), c.establishment_name, a.assessment_id
    				FROM nse_establishment_assessment AS a
    				JOIN nse_aa_category AS b ON a.qa_status=b.aa_category_code
    				JOIN nse_establishment AS c ON a.establishment_code=c.establishment_code
    				WHERE a.establishment_code=? && assessment_status='complete'";
    $sql_certificates = $GLOBALS['dbCon']->prepare($statement);
    
    //Prepare Statement - Logos
    $statement = "SELECT b.pdf_image, b.lowres_image, b.aa_category_name
    				FROM nse_establishment_assessment AS a
    				JOIN nse_aa_category AS b ON a.qa_status=b.aa_category_code
    				WHERE a.establishment_code=? && assessment_status='complete'
    				ORDER BY a.assessment_id";
    $sql_logo = $GLOBALS['dbCon']->prepare($statement);
    
 
    foreach ($codes as $establishment_code) {
	    $sql_logo->bind_param('s', $establishment_code);
	    $sql_logo->execute();
	    $sql_logo->store_result();
	    $sql_logo->bind_result($hires_logo, $lowres_logo, $logo_name);
	    while ($sql_logo->fetch()) {
	    	$images[$image_count]['image'] = $hires_logo;
	    	$images[$image_count]['title'] = "$logo_name Logo - Hi-res";
	    	$image_count++;
	    	$images[$image_count]['image'] = $lowres_logo;
	    	$images[$image_count]['title'] = "$logo_name Logo - Low-res";
	    	$image_count++;
	    }
	    $sql_logo->free_result();
	    $sql_logo->close();
    }
    
    //Certificates & Reports
    foreach ($codes as $establishment_code) {
    	$sql_certificates->bind_param('s', $establishment_code);
    	$sql_certificates->execute();
    	$sql_certificates->store_result();
    	$sql_certificates->bind_result($qa_status, $renewal_date, $establishment_name, $assessment_id);
    	while ($sql_certificates->fetch()) {
    		$year_line = substr($renewal_date, -4);
    		$year_line = ($year_line - 1) . "/$year_line";
    		$class = $class=='line1'?'line2':'line1';
	    	$certificate_line .= "<TR class=$class onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"$class\"' valign=top>";
	    	$certificate_line .= "<TD class=file_title ><image src='/i/led-icons/doc_pdf.png' />$year_line - $qa_status<br><span class=file_sub>Certificate for $establishment_name</span></TD>";
	    	$certificate_line .= "<TD align=right><input type=button value=Download class=download_button onClick='download_certificate(\"$assessment_id\",\"$establishment_code\")') /></TD></TR>";
    		$class = $class=='line1'?'line2':'line1';
	    	$certificate_line .= "<TR class=$class onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"$class\"' valign=top>";
	    	$certificate_line .= "<TD class=file_title ><image src='/i/led-icons/doc_pdf.png' />$year_line - $qa_status<br><span class=file_sub>QA report for $establishment_name</span></TD>";
	    	$certificate_line .= "<TD align=right><input type=button value=Download class=download_button onClick='download_report(\"$assessment_id\",\"$establishment_code\")') /></TD></TR>";
    	}
    	$sql_certificates->free_result();
    }
    $sql_certificates->close();
    
    $line = str_replace('<!-- header -->', 'Certificates & Reports', $lTemplate);
    $line = str_replace('!line_count!', '1', $line);
    $line = str_replace('<!-- logo_line -->', $certificate_line, $line);
    $nLine .= $line;
    
    //Logos
    foreach ($images AS $logo) {
    	$class = $class=='line1'?'line2':'line1';
    	$logo_line .= "<TR class=$class onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"$class\"'>";
    	$logo_line .= "<TD class=file_title ><image src='/i/led-icons/page_white_picture.png' />{$logo['title']}</TD>";
    	$logo_line .= "<TD align=right><input type=button value=Download class=download_button onClick='download_image(\"{$logo['image']}\")') /></TD></TR>";
	    
    }
    $line = str_replace('<!-- header -->', 'Logos', $lTemplate);
	$line = str_replace('!line_count!', '2', $line);
	$line = str_replace('<!-- logo_line -->', $logo_line, $line);
    $nLine .= $line;
    
    //Forms
    $class = $class=='line1'?'line2':'line1';
    $logo_line = "<TR class=$class onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"$class\"'>";
    $logo_line .= "<TD class=file_title ><image src='/i/led-icons/page_white_word.png' />Signage Order Form</TD>";
    $logo_line .= "<TD align=right><input type=button value=Download class=download_button onClick='download_document(\"AAQA_ORDER_SIGNAGE.doc\")') /></TD></TR>";
	$line = str_replace('<!-- header -->', 'Forms', $lTemplate);
    $line = str_replace('!line_count!', '3', $line);
    $line = str_replace('<!-- logo_line -->', $logo_line, $line);
    $nLine .= $line;
    
    //Replace Tags
    $template = preg_replace('@<!-- block_start -->(.)*<!-- block_end -->@i', $nLine, $template);
    $template = str_replace('$%^', "\n", $template);
    
	echo $template;
}

?>