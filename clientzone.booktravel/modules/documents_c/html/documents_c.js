function show_block(bId) {
	m=1;
	c=1;
	var cu = document.getElementById('content' + bId).style.display;
	if (cu == 'block') {
		document.getElementById('content'+bId).style.display = 'none';
		document.getElementById('header'+bId).className = 'block_header';
	} else {
		while(m) {
			if (document.getElementById('content'+c)) {
				document.getElementById('content'+c).style.display = 'none';
				document.getElementById('header'+c).className = 'block_header';
			} else {
				m=0;
			}
			c++;
		}
		document.getElementById('content'+bId).style.display = 'block';
		document.getElementById('header'+bId).className = 'block_header_active';
	}
}

function tCM(id,met) {
	if (met) {
		var cu = document.getElementById('content' + id).style.display;
		if (cu == 'block') {
			document.getElementById('click_' + id).innerHTML = '[Click to Close]';
		} else {
			document.getElementById('click_' + id).innerHTML = '[Click to Open]';
		}
	} else {
		document.getElementById('click_' + id).innerHTML = '';
	}
}

function download_image(i) {
	document.getElementById('dnld').src = '/modules/documents_c/image_download.php?image=' + i;
}

function download_certificate(i,c) {
	document.getElementById('dnld').src = '/modules/documents_c/certificate_download.php?id=' + i + "&code=" + c;
}

function download_report(i,c) {
	document.getElementById('dnld').src = '/modules/documents_c/report_download.php?id=' + i + "&code=" + c;
}

function download_document(i) {
	document.getElementById('dnld').src = '/modules/documents_c/document_download.php?doc=' + i;
}