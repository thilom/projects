<?php
/* 
 * Help for assessors module. The script reads html files in the files directory and extracts
 * the table of contents. H1 tag is the help title and any anchors are extracted to form the subjects.
 *
 * @author Thilo Muller (2010)
 * @package RVBus
 * @category help system
 */

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/help_a/module.ini.php';

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/help_a/html/help_a.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/help_a/html/help_a.js'> </script>";

//Vars
$files = array();
$template = file_get_contents(SITE_ROOT . '/modules/help_a/html/help.html');
$tidy_config = array(
		    "indent"               => true,
		    "output-xml"           => true,
		    "output-xhtml"         => false,
		    "drop-empty-paras"     => false,
		    "hide-comments"        => true,
		    "numeric-entities"     => true,
		    "doctype"              => "omit",
		    "char-encoding"        => "utf8",
		    "repeated-attributes"  => "keep-last"
		);
$toc = '';


//Get list of files
$dh = opendir(SITE_ROOT . '/modules/help_a/files');
while (false !== ($file = readdir($dh))) {
	if ($file == '.' || $file == '..') continue;
	$files[] = $file;
}
closedir($dh);

//Get data from files
sort($files);
foreach ($files as $file) {
	$content = file_get_contents(SITE_ROOT . '/modules/help_a/files/' . $file);
	$content = tidy_repair_string($content, $tidy_config);
	$doc = DOMDocument::loadXML($content);
	$list = $doc->getElementsByTagName("h1");
	$pdf = substr($file, 0, -4) . 'pdf';
	$toc .= "<div class=toc_l1 onClick=\"document.getElementById('content_iframe').src='/modules/help_a/files/$file'\">" . $list->item(0)->nodeValue;
	if (is_file(SITE_ROOT .'/modules/help_a/pdf/' . $pdf)) $toc .= "<a href='/modules/help_a/pdf/$pdf' target=_blank>(PDF)</a>";
	$toc .= "</div>";
	$list = $doc->getElementsByTagName("a");
	for ($i = 0; $i < $list->length; $i++) {
		$anchor = $list->item($i)->getAttribute('name');
		if (empty($anchor)) continue;
		$toc .= "<div class=toc_l2 onClick=\"document.getElementById('content_iframe').src='/modules/help_a/files/$file#{$anchor}'\">" . $list->item($i)->nodeValue . "</div>";
	}
}

//Replace Tags
$template = str_replace('<!-- toc -->', $toc, $template);

echo $template;


?>
