<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'message_center';
$module_name = "Message Center";
$module_icon = 'mail_post_to-64.png';
$module_link = 'message_center.php';
$window_width = '800';
$window_height = '800';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 's'; //a = all
$sub_levels = array();
$sub_levels['message_center']['name'] = 'Manage Messages';
$sub_levels['message_center']['file'] = 'message_center.php';
$sub_levels['message_center']['subs']['manage']['name'] = 'Manage Messages';



?>