<?php
/**
 * Manage messages displayed to users.
 *
 * @author Thilo Muller (2010)
 * @package RVBUS
 * @category message_center
 *
 */

//Vars
$function_white_list = array('message_list', 'edit_message', 'message_delete');
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/message_center/module.ini.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/message_center/html/message_center.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/message_center/html/message_center.js'> </script>";

//Map
switch ($function_id) {
        case 'edit_message':
            if (isset($_POST['message_save'])) {
                edit_message_save();
            } else {
                edit_message_form();
            }
            break;
	default:
		list_messages();
}

function list_messages() {
    //Vars
    $nLine = '';

    //Template
    $template = file_get_contents(SITE_ROOT . '/modules/message_center/html/message_list.html');
    $template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
    $lTemplate = $matches[0][0];

    //Get message List
    $statement = "SELECT message_id, message_title, audience_client, audience_staff, audience_assessors, audience_reps, message_end, message_stock, audience_everyone FROM nse_messages ORDER BY message_id";
    $sql_message = $GLOBALS['dbCon']->prepare($statement);
    $sql_message->execute();
    $sql_message->store_result();
    $sql_message->bind_result($message_id, $message_title, $client, $staff, $assessors, $reps, $message_end, $message_stock, $everyone);
    if ($sql_message->num_rows == 0) {
        $nLine = "<TD colspan=10 align=center>No Messages</td>";
    } else {
        while ($sql_message->fetch()) {
            $audience = '';
            if (!empty($client)) $audience .= "Client, ";
            if (!empty($staff)) $audience .= "Staff, ";
            if (!empty($assessors)) $audience .= "Assessor, ";
            if (!empty($reps)) $audience .= "Reps, ";
            if (!empty($everyone)) $audience = "Everyone, ";
            $audience = substr($audience, 0, -2);

            $method = "";
            if (!empty($message_end) && $message_end != '0000-00-00') {
                $method = 'Date Limit';
            } else {
                if (!empty($message_stock) || $message_stock == 0) {
                    switch ($message_stock) {
                        case '0':
                            $method = 'Unlimited';
                            break;
                        case '1':
                            $method = 'Once Off';
                            break;
                        default:
                            $method = "$message_stock Views";
                    }
                }
            }

            $status = "<img src='/i/silk/bullet_black.png' title='Not Applicable' />";
            $status = '';

            $buttons = "<input type=button value='Edit' class='edit_button' onCLick='document.location=\"/modules/message_center/message_center.php?f=edit_message&id=$message_id\"' /> ";
            $buttons .= "<input type=button value='Delete' class='delete_button' />";

            $line = str_replace('<!-- message_title -->', $message_title, $lTemplate);
            $line = str_replace('<!-- message_audience -->', $audience, $line);
            $line = str_replace('<!-- message_method -->', $method, $line);
            $line = str_replace('<!-- message_status -->', $status, $line);
            $line = str_replace('<!-- buttons -->', $buttons, $line);

            $nLine .= $line;
        }
    }
    $sql_message->free_result();
    $sql_message->close();

    //Replace Tags
    $template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
    $template = str_replace('#$%', "\n", $template);

    echo $template;
}

function edit_message_form() {
    //Vars
    $template = file_get_contents(SITE_ROOT . '/modules/message_center/html/message_form.html');
    $message_id = $_GET['id'];

    $header = "New Message";
    $message_title = '';
    $message_content = '';
    $audience_staff = '';
    $audience_clients = '';
    $audience_assessors = '';
    $audience_reps = '';
    $audience_everyone = '';
    $message_start = '';
    $message_end = '';
    $message_stock = '';
    $type_permanent = '';
    $type_single = '';
    $type_view_limit = '';
    $type_date = '';
    $div_permanent = 'div_hide';
    $div_single = 'div_hide';
    $div_view_limit = 'div_hide';
    $div_date = 'div_hide';

    if ($message_id != 'new') {
        $header = 'Edit Message';

        $statement = "SELECT message_title, message_content, audience_client, audience_staff, audience_assessors, audience_reps, audience_everyone, message_start, message_end, message_stock FROM nse_messages WHERE message_id=? LIMIT 1";
        $sql_message = $GLOBALS['dbCon']->prepare($statement);
        $sql_message->bind_param('i', $message_id);
        $sql_message->execute();
        $sql_message->bind_result($message_title, $message_content, $audience_clients, $audience_staff, $audience_assessors, $audience_reps, $audience_everyone, $message_start, $message_end, $message_stock);
        $sql_message->fetch();
        $sql_message->free_result();
        $sql_message->close();

        $audience_staff = !empty($audience_staff)?'checked':'';
        $audience_clients = !empty($audience_clients)?'checked':'';
        $audience_assessors= !empty($audience_assessors)?'checked':'';
        $audience_reps = !empty($audience_reps)?'checked':'';
        $audience_everyone = !empty($audience_everyone)?'checked':'';

        if (!empty($message_start) && $message_start != '0000-00-00') {
             $type_date = 'checked';
             $div_date = 'div_show';
        } else {
            if (!empty($message_stock) || $message_stock == 0) {
                switch($message_stock) {
                    case '0':
                        $type_permanent = 'checked';
                        $div_permanent = 'div_show';
                        break;
                    case '1':
                        $type_single = 'checked';
                        $div_single = 'div_show';
                        break;
                    default:
                        $type_view_limit = 'checked';
                        $div_view_limit = 'div_show';
                }
            }
        }

    }

    //Replace Tags
    $template = str_replace('<!-- header -->', $header, $template);
    $template = str_replace('!message_content!', $message_content, $template);
    $template = str_replace('!message_title!', $message_title, $template);
    $template = str_replace('!audience_staff!', $audience_staff, $template);
    $template = str_replace('!audience_clients!', $audience_clients, $template);
    $template = str_replace('!audience_assessors!', $audience_assessors, $template);
    $template = str_replace('!audience_reps!', $audience_reps, $template);
    $template = str_replace('!audience_everyone!', $audience_everyone, $template);
    $template = str_replace('!message_start!', $message_start, $template);
    $template = str_replace('!message_end!', $message_end, $template);
    $template = str_replace('!message_stock!', $message_stock, $template);
    $template = str_replace('!type_permanent!', $type_permanent, $template);
    $template = str_replace('!type_single!', $type_single, $template);
    $template = str_replace('!type_view_limit!', $type_view_limit, $template);
    $template = str_replace('!type_date!', $type_date, $template);
    $template = str_replace('!div_permanent!', $div_permanent, $template);
    $template = str_replace('!div_single!', $div_single, $template);
    $template = str_replace('!div_view_limit!', $div_view_limit, $template);
    $template = str_replace('!div_date!', $div_date, $template);


    echo $template;
}

function edit_message_save() {
    //Vars
    $message_title = $_POST['message_title'];
    $message_content = $_POST['message_content'];
    $audience_clients = isset($_POST['audience_clients'])?'1':'';
    $audience_staff = isset($_POST['audience_staff'])?'1':'';
    $audience_assessors = isset($_POST['audience_assessors'])?'1':'';
    $audience_reps = isset($_POST['audience_reps'])?'1':'';
    $audience_everyone = isset($_POST['audience_everyone'])?'1':'';
    $message_type = $_POST['message_type'];
    $message_stock = $_POST['message_stock'];
    $message_start = $_POST['message_start'];
    $message_end = $_POST['message_end'];
    $message_id = $_GET['id'];

    if ($message_type == 'date') {
        $message_stock = '';
    } else {
        $message_start = '';
        $message_end = '';
    }

    if ($message_type == 'permanent') $message_stock = '0';
    if ($message_type == 'single') $message_stock = '1';

    if ($audience_everyone == '1') {
        $audience_assessors = '';
        $audience_clients = '';
        $audience_staff = '';
    }

    //Save Message
    if ($message_id == 'new') {
        $statement = "INSERT INTO nse_messages (message_title, message_content, message_stock, message_start, message_end, audience_client, audience_staff, audience_assessors, audience_reps, audience_everyone) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bind_param('ssssssssss', $message_title, $message_content, $message_stock, $message_start, $message_end, $audience_clients, $audience_staff, $audience_assessors, $audience_reps, $audience_everyone);
        $sql_insert->execute();
        $sql_insert->close();
        $GLOBALS['log_tool']->write_entry("New Message: $message_title", $_SESSION['dbweb_user_id']);
    } else {
        $statement = "UPDATE nse_messages SET message_title=?, message_content=?, message_stock=?, message_start=?, message_end=?, audience_client=?, audience_staff=?, audience_assessors=?, audience_reps=?, audience_everyone=? WHERE message_id=?";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bind_param('ssssssssssi', $message_title, $message_content, $message_stock, $message_start, $message_end, $audience_clients, $audience_staff, $audience_assessors, $audience_reps, $audience_everyone, $message_id);
        $sql_update->execute();
        $sql_update->close();
        $GLOBALS['log_tool']->write_entry("Update Message: $message_title", $_SESSION['dbweb_user_id']);
    }



    echo "<script >alert('Message Saved');";
    echo "document.location = '/modules/message_center/message_center.php'</script>";
}


?>
