<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Vars
$user_id = $_SESSION['dbweb_user_id'];
$message = '';

//Get user type
$statement = "SELECT top_level_type, assessor, sales_rep FROM nse_user WHERE user_id=? LIMIT 1";
$sql_user = $GLOBALS['dbCon']->prepare($statement);
$sql_user->bind_param('i', $user_id);
$sql_user->execute();
$sql_user->bind_result($user_type, $assessor, $rep);
$sql_user->fetch();
$sql_user->free_result();
$sql_user->close();

if ($assessor == '1') $user_type = 'a';
if ($rep == '1') $user_type = 'r';

//Prepare statement - View count
$statement = "SELECT message_count FROM nse_messages_count WHERE user_id=? && message_id=?";
$sql_count = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Insert counter
$statement = "INSERT INTO nse_messages_count (user_id, message_id, message_count) VALUES (?,?,0)";
$sql_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - increment counter
$statement = "UPDATE nse_messages_count SET message_count=message_count+1 WHERE user_id=? && message_id=?";
$sql_increment = $GLOBALS['dbCon']->prepare($statement);

//Get messages
$statement = "SELECT message_id, message_title, message_content, message_start, message_end, message_stock FROM nse_messages WHERE audience_everyone='1' ";
switch($user_type) {
    case 'c':
        $statement .= "|| audience_client='1' ";
        break;
    case 's':
        $statement .= "|| audience_staff='1' ";
        break;
    case 'a':
        $statement .= "|| audience_assessors='1' ";
        break;
    case 'r':
        $statement .= "|| audience_reps='1' ";
        break;
}
$sql_messages = $GLOBALS['dbCon']->prepare($statement);
$sql_messages->execute();
$sql_messages->store_result();
$sql_messages->bind_result($message_id, $message_title, $message_content, $message_start, $message_end, $message_stock);
while ($sql_messages->fetch()) {
    $include_message = FALSE;
    if (!empty($message_start) && $message_start != '0000-00-00') {
        list($year, $month, $day) = explode('-', $message_start);
        $start = mktime(0,0,0, $month, $day, $year);
        list($year, $month, $day) = explode('-', $message_end);
        $end = mktime(0,0,0, $month, $day, $year);
        $now = time();
        if ($start < $now && $end > $now) $include_message = TRUE;
    } else {
        switch($message_stock) {
            case '0':
                $include_message = TRUE;
                break;
            default:
                $sql_count->bind_param('ii', $user_id, $message_id);
                $sql_count->execute();
                $sql_count->store_result();
                $sql_count->bind_result($view_count);
                if ($sql_count->num_rows == 0) {
                    $sql_insert->bind_param('ii', $user_id, $message_id);
                    $sql_insert->execute();
                    $view_count = 0;
                } else {
                    $sql_count->fetch();
                    $sql_count->free_result();
                }

            

                if ($view_count < $message_stock) {
                    $include_message = TRUE;

                    $sql_increment->bind_param('ii', $user_id, $message_id);
                    $sql_increment->execute();
                }
        }
    }

    if ($include_message) $message .= "<div><b>$message_title</b><br><span style='font-size: 10pt; font-style: italic;'>$message_content</span></div><hr>";
}



$sql_messages->free_result();
$sql_messages->close();



if (empty($message)) {
    $message = '<div style="font-size: 10pt; width: 100%; text-align: center;"> <p>~No Messages Today~</p> </div>';
} else {
    $message = substr($message, 0, -4);
}

echo $message;

?>
