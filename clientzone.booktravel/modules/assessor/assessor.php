<?php
/**
 *
 * --Updated the notes section - Thilo(2010)
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 */


//Vars
$function_white_list = array('new_assessment', 'assess_estab', 'estab_detail', 'estab_view', 'draft', 'overdue', '1month','3months','print', 'book_assessment', 'establishment_editor', 'edit_establishment');
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/assessor/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';
require_once SITE_ROOT . '/modules/login/tcrypt.class.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

////Prepare Statements
//$statement = "SELECT function_id, parent_id, access FROM nse_user_access WHERE user_id=? && module_id=?";
//$sql_access = $GLOBALS['dbCon']->prepare($statement);
//
////Get Access Rights
//$sql_access->bind_param('ss', $_SESSION['dbweb_user_id'], $module_id);
//$sql_access->execute();
//$sql_access->bind_result($function, $parent_id, $user_access);
//$sql_access->store_result();
//while ($sql_access->fetch()) {
//    if ($function == 'add' && 'manage_estab' == $parent_id && '1' == $user_access) $access['add_establishment'] = TRUE;
//    if ($function == 'edit' && $parent_id == 'manage_estab' && $user_access == '1') $access['edit_establishment'] = TRUE;
//    if ($function == 'delete' && $parent_id == 'manage_estab' && $user_access == '1') $access['delete_establishment'] = TRUE;
//    if ($function == 'view' && $parent_id == 'manage_estab' && $user_access == '1') $access['view_establishment'] = TRUE;
//    if ($function == 'changes' && $parent_id == 'manage_estab' && $user_access == '1') $access['manage_changes'] = TRUE;
//}
//$sql_access->close();

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/assessor/html/assessor.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/shared/css/establishment.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/assessor/html/assessor.js'> </script>";
echo "<script src='/modules/shared/js/notes.js'> </script>";

//Assemble Menu Structure
echo "<script src=/shared/G2.js></script>";
echo "<script src=/shared/tabs.js></script>";
echo "<script>tabs(\"";
echo "<a href=# id=Mq_1In onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Manage Assessments</a>";
//echo "<a href=# id=Mq_1Ib onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Quick List</a>";
echo "\")</script>";
echo "<span class=TabP>";
echo "<p id=Mp_In onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";

    //echo "<a href='/modules/assessor/assessor.php?f=estab_detail' onfocus=blur()>Establishment Details</a>";
    echo "<a href='/modules/assessor/assessor.php?f=establishment_editor' onfocus=blur()>Establishment Editor</a>";

    echo "<a href='/modules/assessor/assessor.php?f=new_assessment' onfocus=blur()>Assessments</a>";

    //echo "<a href='/modules/estab_s/estab.php?f=changes' onfocus=blur() >Invoices</a>";
    echo "<a href='#' onfocus=blur() style='color: silver' >Invoices</a>";

echo "</p>";
echo "</span>";

//echo "<span class=TabP>";
//echo "<p id=Mp_Ib onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
//
//    echo "<a href='/modules/assessor/assessor.php?f=draft' onfocus=blur()>Draft Assessments</a>";
//
//    echo "<a href='/modules/assessor/assessor.php?f=overdue' onfocus=blur()>Overdue Assessments</a>";
//
//    echo "<a href='/modules/assessor/assessor.php?f=1month' onfocus=blur()>Due in 1 Month</a>";
//
//     echo "<a href='/modules/assessor/assessor.php?f=3months' onfocus=blur()>Due in 3 Months</a>";
//
//echo "</p>";
//echo "</span>";
echo "<br />";

//Map
switch ($function_id) {
	case 'establishment_editor':
		establishment_list();
		break;
	case 'edit_establishment':
		isset($_POST['save_establishment'])?establishment_edit_save():establishment_edit_form();
		
		break;
	case 'new_assessment';
		new_assessment();
		break;
	case 'assess_estab';
		if (isset($_POST['save_draft'])) {
			new_assessment_save('draft');
		} else if (isset($_POST['save_approval'])) {
			new_assessment_save('waiting');
		} else {
			new_assessment_estab();
		}
		break;
	case 'estab_detail';
		establishment_detail_list();
		break;
	case 'estab_view';
		establishment_detail();
		break;
	case 'overdue';
		overdue_assessment();
		break;
	case '1month';
		one_month();
		break;
	case '3months';
		three_month();
		break;
	case 'draft':
		draft_assessment();
		break;
	case 'print';
		print_estab();
		break;
	case 'book_assessment':
		if (isset($_POST['book'])) {
			book_assessment_save();
		} else {
			book_assessment_form();
		}
		break;
	default:
		welcome();
}

function new_assessment() {
	//Vars
	$booking_id = 0;
	$booking_date = '';
	$establishment_code = '';
	$establishment_count = 0;
	$establishment_name = '';
	$town_id = '';
	$old_next_review = '';
	$province_id = '';
	$town_name = '';
	$province_name = '';
	$nLine = '';
	$assessor_id = $_SESSION['dbweb_user_id'];
	$establishment_count = 0;
	$list_start = isset($_GET['st'])?$_GET['st']:0;
	$ob = 'establishment';
	$assessment_status = '';
	$assessment_date = '';
	$year_entered = '';
	$estab_list = array();
	$sort_order = isset($_GET['order']) && $_GET['order']=='desc'?'DESC':'ASC';
	$order_by = isset($_GET['ob'])?$_GET['ob']:'';

	//Get template & prepare
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessor/html/establishment_list.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
	$lTemplate = $matches[0][0];
	
	//Prepare Statements - Province Name
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=? LIMIT 1";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Town Name
	$statement = "SELECT town_name FROM nse_location_town WHERE town_id=? LIMIT 1";
	$sql_town = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare statement - Booking
	$statement = "SELECT booking_date, booking_id FROM nse_establishment_bookings WHERE establishment_code=? && booking_reason='assessment' ORDER BY booking_date LIMIT 1";
	$sql_book = $GLOBALS['dbCon']->prepare($statement);

        //Get postponements
        $statement = "SELECT postpone_date FROM nse_establishment_assessment_postpone WHERE establishment_code=?";
        $sql_postpone = $GLOBALS['dbCon']->prepare($statement);
	
	//Get list of establishments
	$statement = "SELECT DISTINCT a.establishment_code, a.establishment_name, b.town_id, b.province_id,
					(SELECT renewal_date FROM nse_establishment_assessment WHERE establishment_code=a.establishment_code ORDER BY assessment_id DESC LIMIT 1) AS assessment_date,
					(SELECT assessment_status FROM nse_establishment_assessment WHERE establishment_code=a.establishment_code ORDER BY assessment_id DESC LIMIT 1) AS assessment_status,
					DATE_FORMAT(a.enter_date,'%b %Y'), a.review_date_depricated
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code";
	if (isset($_COOKIE['uEstabFilter'])) {
		$statement .= " LEFT JOIN nse_establishment_assessment AS c ON a.establishment_code=c.establishment_code";
	}
	$statement .= "	WHERE a.assessor_id=?";
	if (isset($_POST['search_string'])) {
		$search_string = "%{$_POST['search_string']}%";
		if ($_POST['search_filter'] == 'establishment_code') $statement .= " && a.establishment_code LIKE ?";
		if ($_POST['search_filter'] == 'establishment_name') $statement .= " && a.establishment_name LIKE ?";
	}
	
	$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
	if (isset($_POST['search_string'])) {
		$sql_establishments->bind_param('is', $assessor_id, $search_string);
	} else {
		$sql_establishments->bind_param('i', $assessor_id);
	}
	$sql_establishments->execute();
	$sql_establishments->store_result();
	$row_count = $sql_establishments->num_rows;
	$sql_establishments->bind_result($establishment_code, $establishment_name, $town_id, $province_id, $assessment_date, $assessment_status, $year_entered, $old_next_review);
	while ($sql_establishments->fetch()) {
		//Reset Vars
		$booking_id = '';
		$booking_date = '';
		$province_name = '';
		$town_name = '';
		$country_name = '';
		$buttons = '';
		$wait_counter = 0;
                $postpone_date = '';

                //Get postponements
                $sql_postpone->bind_param('s', $establishment_code);
                $sql_postpone->execute();
                $sql_postpone->bind_result($postpone_date);
                $sql_postpone->fetch();
                $sql_postpone->free_result();

                //Calculate assessment date
		if (empty($assessment_date)) $assessment_date = $old_next_review;
                if (!empty($postpone_date)) $assessment_date = $postpone_date;
		
		//Get province Name
		$sql_province->bind_param('s', $province_id);
		$sql_province->execute();
		$sql_province->bind_result($province_name);
		$sql_province->store_result();
		$sql_province->fetch();
		$sql_province->free_result();
		
		//Get Town Name
		$sql_town->bind_param('s', $town_id);
		$sql_town->execute();
		$sql_town->bind_result($town_name);
		$sql_town->store_result();
		$sql_town->fetch();
		$sql_town->free_result();
		
		//Check for current booking
		$sql_book->bind_param('s', $establishment_code);
		$sql_book->execute();
		$sql_book->store_result();
		$sql_book->bind_result($booking_date, $booking_id);
		$sql_book->fetch();
		$sql_book->free_result();
		
		$status = '';
		$button_booked = "<input type=button value='Book/Postpone' class=book_button onClick='document.location=\"/modules/assessor/assessor.php?f=book_assessment&code=!establishment_code!\"' />";
		$buttons = "<input type=button value=select class=edit_button onClick=assess_redirect('$establishment_code') />";
		if ($assessment_status == 'waiting') {
			$status = 'Submitted';
			$buttons = "<input type=button value=select class=edit_button disabled title='Assessment waiting for approval' />";
			$button_booked = "<input type=button value=Book class=book_button disabled />";
		} else if ($assessment_status == 'draft') {
			$status = 'Draft';
		} else if ($assessment_status == 'booked') {
			$status = 'Booked';
		} else if ($assessment_status == 'returned') {
			$status = 'Returned';
		} else {
/*	//Put back for PHP version 5.3
  			$date1 = new DateTime($assessment_date);
			$date2 = new DateTime('now');
			$time_diff = $date1->diff($date2, true);
			$time_diff_months = $time_diff->format('%m');
			$time_diff_years = $time_diff->format('%y');
			echo "Months: $time_diff_months, Years: $time_diff_years<br>";
*/
			$date1 = strtotime($assessment_date);
			$date2 = strtotime('now');
			$time_diff = date_diff_for_v52($date1, $date2);
			$time_diff_years = $time_diff['years'];
			$time_diff_months = $time_diff['months'];
			//echo "Months: $time_diff_months, Years: $time_diff_years<p>";
			
			$time_diff = ($time_diff_years * 12) + $time_diff_months;
			$time_diff = $time_diff * (-1);
			if ($time_diff < 0) $status = 'overdue';
			if ($time_diff == 0) $status = 'Due in 1 Month';
			if ($time_diff > 0 && $time_diff <= 2) $status = 'Due in 3 Months';
			if ($time_diff > 2) $status = 'Complete';
			if (empty($assessment_date)) $status = '-';
		}
		
		$buttons = $button_booked . $buttons;
		if ($booking_id != 0 && $assessment_status != 'draft' && $assessment_status != 'waiting' && $assessment_status != 'returned') {
			$status = 'Booked';
		}
		
		if ($status != 'Booked') $booking_date = '';
		
		if (isset($_COOKIE['uEstabFilter'])) {
			if ($_COOKIE['uEstabFilter'] == 'overdue') {
				if ($status != 'overdue') continue;
			}
			if ($_COOKIE['uEstabFilter'] == 'booked') {
				if ($status != 'Booked') continue;
			}
			if ($_COOKIE['uEstabFilter'] == 'draft') {
				if ($status != 'Draft' && $assessment_status != 'returned') continue;
			}
			if ($_COOKIE['uEstabFilter'] == '1month') {
				if ($status != 'Due in 1 Month') continue;
			}
			if ($_COOKIE['uEstabFilter'] == '3month') {
				if ($status != 'Due in 3 Months') continue;
			}
		}
		
		
		$estab_list[$establishment_count]['establishment_code'] = $establishment_code;
		$estab_list[$establishment_count]['establishment_name'] = $establishment_name;
		$estab_list[$establishment_count]['town_name'] = $town_name;
		$estab_list[$establishment_count]['province_name'] = $province_name;
		$estab_list[$establishment_count]['year_entered'] = $year_entered;
		$estab_list[$establishment_count]['assessment_date'] = $assessment_date;
		$estab_list[$establishment_count]['status'] = $status;
		$estab_list[$establishment_count]['buttons'] = $buttons;
		$estab_list[$establishment_count]['booking_date'] = $booking_date;
		$establishment_count++;
	}
	$sql_establishments->free_result();
	$sql_establishments->close();
	if ($row_count == 0) {
		$nLine = "<tr><td colspan=10 align=center>-- No Results --</td></tr>";
		$navigation = '<tr><th colspan=10 >&nbsp;</th></tr>';
	} else {
		//Sorting
		foreach($estab_list as $key=>$data) {
			switch ($order_by) {
				case 'code':
					$sort_array[$key] = $data['establishment_code'];
					break;
				case 'entered':
					$sort_array[$key] = $data['year_entered'];
					break;
				case 'booking_date':
					$sort_array[$key] = $data['booking_date'];
					break;
				case 'status':
					$sort_array[$key] = $data['status'];
					break;
				case 'review':
					$sort_array[$key] = $data['assessment_date'];
					break;
				case 'location':
					$sort_array[$key] = $data['town_name'];
					break;
				default:
					$sort_array[$key] = $data['establishment_name'];
			}
			
		}
		if (count($estab_list) == 0) {
			$nLine = "<tr><td colspan=10 align=center>-- No Results --</td></tr>";
			$navigation = '<tr><th colspan=10 >&nbsp;</th></tr>';
		} else {
			if ($sort_order == 'DESC') {
				asort($sort_array);
			} else {
				arsort($sort_array);
			}
			foreach ($sort_array as $key=>$value) {
				$new_list[] = $estab_list[$key];
			}
			$estab_list = $new_list;
			
			//Create Line
			$counter = -1;
			foreach($estab_list as $data) {
				$counter++;
				if ($counter < $list_start) continue;
				if ($counter > $list_start+$GLOBALS['list_length']-1) break;
				$line = str_replace('<!-- establishment_name -->', $data['establishment_name'], $lTemplate);
				$line = str_replace('<!-- establishment_code -->', $data['establishment_code'], $line);
				$line = str_replace('<!-- location -->', "{$data['town_name']}, {$data['province_name']}", $line);
				$line = str_replace('<!-- entered -->', $data['year_entered'], $line);
				$line = str_replace('<!-- review -->', $data['assessment_date'], $line);
				$line = str_replace('<!-- status -->', $data['status'], $line);
				$line = str_replace('<!-- buttons -->', $data['buttons'], $line);
				$line = str_replace('!establishment_code!', $data['establishment_code'], $line);
				$line = str_replace('<!-- booking_date -->', $data['booking_date'], $line);
				
				$nLine .= $line;
			}
			
			//Navigation
			$establishment_count = count($estab_list);
			$total_pages = ceil($establishment_count/$GLOBALS['list_length']);
			$current_page = ceil($list_start/$GLOBALS['list_length']) + 1;
			$navigation = "<tr><th colspan=10><table width=100%><tr>";
			if ($GLOBALS['list_length'] > $establishment_count || $current_page == 1) {
				$navigation .= "<th >&nbsp;</th>";
			} else {
				if (isset($_GET['st'])) {
					$navigation .= "<th align=left width=100px >&nbsp;<a href='/modules/assessor/assessor.php?f=new_assessment&st=";
					$navigation .= $list_start - $GLOBALS['list_length'];
					$navigation .= "' >&#8249; Previous</a> </th>";
				} else {
					$navigation .= "<th>&nbsp;</th>";
				}
			}
			$navigation .= "<th colspan=5 align=center >Page: ";
			for ($i=1; $i<=$total_pages; $i++) {
				$navigation .= "<a href='/modules/assessor/assessor.php?f=new_assessment&st=";
				$navigation .= ($i-1) * $GLOBALS['list_length'];
				if (isset($_GET['ob'])) $navigation .= "&ob=" . $_GET['ob'];
				if (isset($_GET['order'])) $navigation .= "&order=" . $_GET['order'];
				$navigation .= "' >$i</a> | ";
			}
			$navigation = substr($navigation, 0, -2);
			$navigation .= "</th>";
			
			if ($current_page < $total_pages) {
				$navigation .= "<th align=right width=100px><a href='/modules/assessor/assessor.php?f=new_assessment&st=";
				$navigation .= $list_start + $GLOBALS['list_length'];
				$navigation .= "'>Next &#8250;</a>&nbsp;</th>";
			} else {
				$navigation .= "<th>&nbsp;</th>";
			}
			$navigation .= "</tr></table></th></tr>";
		}
	}
	//Heading
	$heading_titles = array('empty'=>'&nbsp;',
							'code'=>'Code',
							'establishment'=>'Establishment',
							'location'=>'Location',
							'entered' => 'Date Entered',
							'review' => 'Next Review',
							'status'=>'Status',
							'booking_date' => 'Booking Date',
							'nb'=>'&nbsp;');
	$heading_widths = array('empty'=>10,
							'code'=>50,
							'establishment'=>300,
							'location'=>200,
							'entered' => 50,
							'review' => 60,
							'status'=>80,
							'booking_date' => 50,
							'nb'=>145);
	$line_header = "<tr style='background-color: grey'>";
	foreach ($heading_titles as $col_id=>$col_name) {
		$line_header .= "<TH width={$heading_widths[$col_id]}>";
		if (!isset($_POST['search_string']) && ($col_id != 'empty' && $col_id != 'nb')) {
			$line_header .= "<a href='/modules/assessor/assessor.php?f=new_assessment&ob=$col_id";
			if ($col_id == $order_by) {
				if (!isset($_GET['order'])) $line_header .= "&order=desc";
			}
			$line_header .= "' >";
		}
		$line_header .= "$col_name";
		if (!isset($_POST['search_string'])) {
			if ($col_id == $order_by) {
				if ($sort_order == 'DESC') {
					$line_header .= "<img src='/i/arrow_up.png' border=0 title='Ascending Order (A-Z)' />";
				} else {
					$line_header .= "<img src='/i/arrow_down.png' border=0 title='Descending Order (Z-A)' />";
				}
			}
		}
		if (!isset($_POST['search_string'])) $line_header .= "</a>";
		$line_header .= "</TH>";
	}
    $line_header .= "</tr>";
	
	//Replace Tags
	$template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
	$template = str_replace('<!-- navigation -->', $navigation, $template);
	$template = str_replace('<!-- line_header -->', $line_header, $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}

function new_assessment_estab() {
	//Vars
	$establishment_code = $_GET['code'];
	$establishment_name = '';
	$town = '';
	$province = '';
	$country = '';
	$category_id = '';
	$category_name = '';
	$contacts_list = '';
	$location = '';
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessor/html/new_assessment.html');
	$areas = '';
	$scripts = '';
	$assessment_date = '';
	$assessment_type = '';
	$assessment_id = '';
	$qa_status = '';
	$qa_code = '';
	$qa_name = '';
	$ratings = array(1=>'Excellent','Very Good','Good','Fair','Poor');
	$types = array('new establishment'=>'New Establishment', 'award visit'=>'Awards Visit', 'renewal'=>'Renewal');
	$type_list = '';
	$qa_list = '';
	$hide = '';
	$renewal_date = '';
	$category_select = '';
	$assessment_id = '';
	$area_id = '';
	$area_name = '';
	$hide = '';
	$required = '';
	$assessment_notes = '';
	$staff_notes = '';
	$proposed_category = '';
	$hide_default = '';
	
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	//Get Location
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->store_result();
	$sql_location->bind_result($town, $province, $country);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	
	if (!empty($town)) $location .= $town;
	if (!empty($province)) {
		$location .= ", $province";
	} else {
		if (!empty($country)) $location .= ", $country";
	}

	//Prepare statement - Check if user has admin access
	$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
	$sql_admin = $GLOBALS['dbCon']->prepare($statement);

	//Get & Assemble Contact details
	$counter = 0;
	$statement = "SELECT contact_name, contact_designation, contact_tel, contact_cell, contact_email FROM nse_establishment_contacts WHERE establishment_code=?";
	$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
	$sql_contacts->bind_param('s', $establishment_code);
	$sql_contacts->execute();
	$sql_contacts->bind_result($contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
	$sql_contacts->store_result();
	while ($sql_contacts->fetch()) {
		$sql_admin->bind_param('s', $contact_email);
		$sql_admin->execute();
		$sql_admin->store_result();
		$sql_admin->bind_result($user_counter);
		$sql_admin->fetch();
		$contacts_list .= "!|!$counter|$contact_name|$contact_designation|$contact_tel|$contact_cell|$contact_email|$user_counter|0|$contact_email|0";
		$counter++;
	}
	$sql_contacts->free_result();
	$sql_contacts->close();

	//Prepare Statement - Get assessment details
	$statement = "";
	$sql_area_data = $GLOBALS['dbCon']->prepare($statement);
	
	//Check if draft exists
	$statement = "SELECT assessment_id, DATE_FORMAT(assessment_date,'%Y-%m-%d'), assessment_type, qa_status, DATE_FORMAT(renewal_date, '%M %Y'), proposed_category, assessment_notes, staff_notes FROM nse_establishment_assessment WHERE (assessment_status='draft' || assessment_status='returned') && establishment_code=? ORDER BY assessment_id LIMIT 1";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('s', $establishment_code);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($assessment_id, $assessment_date, $assessment_type, $qa_status, $renewal_date, $proposed_category, $assessment_notes, $staff_notes);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	//Prepare Statement - Get area data
	$statement = "SELECT assessment_content, assessment_rating, not_applicable, hide_from_client FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	
	//Get assessment areas
	$statement = "SELECT assessment_area_id, assessment_area_name, hide_client, required FROM nse_assessment_areas WHERE active='Y' ORDER BY display_order";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->execute();
	$sql_areas->store_result();
	$sql_areas->bind_result($area_id, $area_name, $hide_default, $required);
	while ($sql_areas->fetch()) {
		$hide = 'a';
		$assessment_content = '';
		$assessment_rating = '';
		$not_applicable = '';
		
		if (!empty($assessment_id)) {
			$sql_data->bind_param('ss', $assessment_id, $area_id);
			$sql_data->execute();
			$sql_data->store_result();
			$sql_data->bind_result($assessment_content, $assessment_rating, $not_applicable, $hide);
			$sql_data->fetch();
			$sql_data->free_result();
		} else {
			$assessment_content = '';
			$assessment_rating = '';
			$not_applicable = '';
		}
		
		if ($hide == 'a' && $hide_default == '1') $hide = 'Y';
		if (!empty($assessment_rating)) $scripts .= "<script>set_change('$area_id')</script>";
		
		$areas .= "<div class=block_header onClick=show_block('$area_id') id=header$area_id onMouseOver=tCM('$area_id',1) onMouseOut=tCM('$area_id',0) >
						<table width=100% cellpadding=0 cellspacing=0>
							<tr>
								<td class=hRow >$area_name</td>
								<td><span id=click_$area_id class=cMes ></span></td>
								<td align=right><img src='/i/silk/group_delete.png' width=16 height=16 class=hide_indicator id=hide_indicator_$area_id title='NOT visible to clients' />&nbsp;<img src='/i/blank.png' width=16 height=16 class=edit_indicator id=indicator_$area_id /></td>
							</tr>
						</table>
					</div>
					<div class=block_content id=content$area_id>
					<textarea name=area_$area_id id=area_$area_id  onChange=\"set_change('$area_id')\" onBlur=save_field('$area_id')>$assessment_content</textarea>
					<table width=100% >
						<tr>
							<td>
								<table width=480px id=ratings_$area_id >
									<tr>";
		foreach ($ratings as $rate_id=>$rate_name) {
			$areas .= "<td class=field_label><input type=radio name=rate_$area_id value=$rate_id id=rate_{$rate_id}_$area_id";
			if ($rate_id == $assessment_rating) $areas .= " checked ";
			$areas .= "/><label for='rate_{$rate_id}_$area_id' >$rate_name</label></td>";
		}
										
		$areas .="					</tr>
								</table>
							</td>
							<td class=field_label align=right width=500><input type=checkbox name=hide_$area_id value='Y' id=hide_$area_id onCLick=hide('$area_id')";
		if ($hide == 'Y') {
			$areas .= " checked ";
		} else {
			$scripts .= "<script>hide('$area_id')</script>";
		}
		$areas .= " /><label for='hide_$area_id' style='padding-right: 20px'>Hide From Client</label>";
		if ($required == 1) {
			$areas .= "<input type=checkbox name=na_$area_id value='na' id=na_$area_id onCLick=na('$area_id') disabled /><span style='color: grey' >Not Applicable</span></td>
							</tr>
						</table>
						</div>";
		} else {
			$areas .= "<input type=checkbox name=na_$area_id value='na' id=na_$area_id onCLick=na('$area_id') ";
			if ($not_applicable == 'Y') {
				$areas .= " checked ";
				$scripts .= "<script>na('$area_id')</script>";
			}
			$areas .= " /><label for='na_$area_id' >Not Applicable</label></td>
							</tr>
						</table>
						</div>";
		}
	}
	$sql_areas->free_result();
	$sql_areas->close();
	
	//Assessment types
	foreach ($types as $type_id=>$type_name) {
		if ($assessment_type == $type_id) {
			$type_list .= "<option value='$type_id' selected>$type_name</option>";
		} else {
			$type_list .= "<option value='$type_id' >$type_name</option>";
		}
	}
	
	//Proposed Status
	$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category WHERE active='Y' ORDER BY display_order";
	$sql_qa = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa->execute();
	$sql_qa->store_result();
	$sql_qa->bind_result($qa_code, $qa_name);
	while ($sql_qa->fetch()) {
		$qa_list .= "<option value='$qa_code' ";
		$qa_list .= $qa_code==$qa_status?' selected':'';
		$qa_list .= " >$qa_name</option>";
	}
	$sql_qa->free_result();
	$sql_qa->close();
	
	//Get default category
	if (empty($proposed_category)) {
		$statement = "SELECT subcategory_id FROM nse_establishment_restype WHERE establishment_code=? LIMIT 1";
		$sql_category = $GLOBALS['dbCon']->prepare($statement);
		$sql_category->bind_param('s', $establishment_code);
		$sql_category->execute();
		$sql_category->store_result();
		$sql_category->bind_result($proposed_category);
		$sql_category->fetch();
		$sql_category->free_result();
		$sql_category->close();
	}
	
	//Get category list
	$statement = "SELECT subcategory_id, subcategory_name FROM nse_restype_subcategory_lang WHERE language_code='EN' ORDER BY subcategory_name";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);
	$sql_category->execute();
	$sql_category->store_result();
	$sql_category->bind_result($category_id, $category_name);
	while ($sql_category->fetch()) {
		$category_select .= "<option value='$category_id' ";
		if ($category_id == $proposed_category) $category_select .= "selected ";
		$category_select .= "> $category_name </option>";
	}
	$sql_category->free_result();
	$sql_category->close();

	//Get notes
	$notes = "<table id=notes_list >";
	$statement = "SELECT user_name, note_timestamp, note_content FROM nse_establishment_notes WHERE establishment_code=? ORDER BY note_timestamp DESC";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);
	$sql_notes->bind_param('s', $establishment_code);
	$sql_notes->execute();
	$sql_notes->store_result();
	$sql_notes->bind_result($user_name, $note_timestamp, $note_content);
	while ($sql_notes->fetch()) {
		$date = date('d-m-Y h:i',$note_timestamp);
		$notes .= "<tr>";
		$notes .= "<td class=note_name>$user_name</td>";
		$notes .= "<td class=note_date>$date</td>";
		$notes .= "<td class=note_content colspan=2>$note_content</td>";
		$notes .= "</tr>";
	}
	$sql_notes->free_result();
	$sql_notes->close();
	$notes .= "</table>";

	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_replace('<!-- location -->', $location, $template);
	$template = str_replace('<!-- areas -->', $areas, $template);
	$template = str_replace('<!-- scripts -->', $scripts, $template);
	$template = str_replace('<!-- assessment_date -->', $assessment_date, $template);
	$template = str_replace('<!-- renewal_date -->', $renewal_date, $template);
	$template = str_replace('<!-- assessment_type -->', $type_list, $template);
	$template = str_replace('<!-- category_select -->', $category_select, $template);
	$template = str_replace('<!-- qa_list -->', $qa_list, $template);
	$template = str_replace('<!-- contacts_list -->', $contacts_list, $template);
	$template = str_replace('<!-- notes -->', $notes, $template);
	$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);
	
	echo $template;
}

function new_assessment_save($assessment_status) {

	//Vars
	$assessment_id = '';
	$assessor_name = '';
	$establishment_code = $_GET['code'];
	$assessment_date = $_POST['assessment_date'];
	$assessment_type = $_POST['assessment_type'];
	$qa_status = $_POST['qa_status'];
	$proposed_category = $_POST['proposed_category'];
	$contacts_list = $_POST['contacts_list'];
	$count = 0;
	$area_id = '';
	$tc = new tcrypt();
	$notes = $_POST['notes'];
	
	//Delete current Contacts
	$statement = "DELETE FROM nse_establishment_contacts WHERE establishment_code=?";
	$sql_contacts_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_contacts_delete->bind_param('s', $establishment_code);
	$sql_contacts_delete->execute();
	$sql_contacts_delete->close();

	//Prepare statement - check if user has admin access
	$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
	$sql_admin_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare Statement - Add new user
	$statement = "INSERT INTO nse_user (firstname, lastname, email, phone, cell, new_password, top_level_type, user_type_id) VALUES (?,?,?,?,?,?,'c','2')";
	$sql_admin_insert = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Update user
	$statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=? WHERE user_id=?";
	$sql_admin_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Check establishment access
	$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=?";
	$sql_user_access = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Get user id
	$statement = "SELECT user_id FROM nse_user WHERE email=?";
	$sql_admin_id = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Delete admin user
	$statement = "DELETE FROM nse_user WHERE user_id=?";
	$sql_admin_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Delete user/establishment link
	$statement = "DELETE FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
	$sql_estab_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Add user/establishment link
	$statement = "INSERT INTO nse_user_establishments (user_id, establishment_code) VALUES (?,?)";
	$sql_estab_add = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - check user/establishment link
	$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
	$sql_estab_check = $GLOBALS['dbCon']->prepare($statement);

	//Update Contacts
	$statement = "INSERT INTO nse_establishment_contacts (establishment_code, contact_name, contact_designation, contact_tel, contact_cell, contact_email) VALUES (?,?,?,?,?,?)";
	$sql_contacts_insert = $GLOBALS['dbCon']->prepare($statement);
	$contact_line = explode("!|!", $contacts_list);

	foreach ($contact_line as $line) {
		if (empty($line)) continue;
		list($line_id, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $admin_user, $send_password, $old_email, $delete_tag) = explode('|', $line);
		if ($delete_tag == 0) {
			$sql_contacts_insert->bind_param('ssssss', $establishment_code, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
			$sql_contacts_insert->execute();
		}

		//Extract firstname & lastname
		$names = explode(' ', $contact_name);
		if (count($names) == 2) {
			$firstname = $names[0];
			$lastname = $names[1];
		} else {
			$firstname = $contact_name;
			$lastname = '';
		}
		//echo "<p style='color: navy'>$line<br>Email: $old_email<br>Admin User: $admin_user</p>";
		if ($delete_tag == 1 || $admin_user == 0) {
			//echo "DELETE<br>=======<br />Email: $old_email<br>";
			$admin_user_id = 0;
			$sql_admin_id->bind_param('s', $old_email);
			$sql_admin_id->execute();
			$sql_admin_id->store_result();
			$sql_admin_id->bind_result($admin_user_id);
			$sql_admin_id->fetch();
			$sql_admin_id->free_result();

			if ($admin_user_id != 0) {
				$sql_user_access->bind_param('i', $admin_user_id);
				$sql_user_access->execute();
				$sql_user_access->store_result();
				$sql_user_access->bind_result($estab_count);
				$sql_user_access->fetch();
				$sql_user_access->free_result();

				if ($estab_count < 2) {
					//echo "Deleting Admin User<br>";
					$sql_admin_delete->bind_param('i', $admin_user_id);
					$sql_admin_delete->execute();
				}

				//echo "Deleting estab link<p />";
				$sql_estab_delete->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_delete->execute();
			}
		} else if ($admin_user == 1) {
			if ($old_email == 'new') {
				//echo "ADDING USER<br>=======<br />Email: $old_email<br />";
				//Check if user exists
				$admin_user_id = 0;
				$sql_admin_id->bind_param('s', $contact_email);
				$sql_admin_id->execute();
				$sql_admin_id->store_result();
				$sql_admin_id->bind_result($admin_user_id);
				$sql_admin_id->fetch();
				$sql_admin_id->free_result();
				//echo "User ID: $admin_user_id<br>";

				if ($admin_user_id == 0) {
					//Get Password
					$password = $GLOBALS['security']->generate_password(6);
					$password = $tc->encrypt($password);

					//Save user
					//echo "Inserting user<br>";
					$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
					$sql_admin_insert->execute();
					$admin_user_id = $sql_admin_insert->insert_id;
				}

				//Check if user/establishment link exists
				$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_check->execute();
				$sql_estab_check->store_result();
				$sql_estab_check->bind_result($estab_count);
				$sql_estab_check->fetch();
				$sql_estab_check->free_result();

				//Attach establishment
				if ($estab_count == 0) {
					//echo "Inserting estab link<p />";
					$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
					$sql_estab_add->execute();
				}


			} else {
				//echo "UPDATING USER<br>=======<br />Email: $old_email<br />";
				$admin_user_id = 0;
				$sql_admin_id->bind_param('s', $old_email);
				$sql_admin_id->execute();
				$sql_admin_id->store_result();
				$sql_admin_id->bind_result($admin_user_id);
				$sql_admin_id->fetch();
				$sql_admin_id->free_result();
				//echo "User ID: $admin_user_id<br>";

				if ($admin_user_id == 0) {
					//echo "Adding user<br>";
					$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
					$sql_admin_insert->execute();
					$admin_user_id = $sql_admin_insert->insert_id;
				} else {
					//echo "Updating user <br>";
					$sql_admin_update->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $admin_user_id);
					$sql_admin_update->execute();
				}

				//Check if estab exists
				$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_check->execute();
				$sql_estab_check->store_result();
				$sql_estab_check->bind_result($estab_count);
				$sql_estab_check->fetch();
				$sql_estab_check->free_result();

				//Attach establishment
				if ($estab_count == 0) {
					//echo "Adding estab link<p />";
					$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
					$sql_estab_add->execute();
				}
			}
		}
	}
	$sql_contacts_insert->close();
	$sql_admin_check->close();
	$sql_admin_update->close();
	$sql_admin_insert->close();

	//Check if draft exists
	$statement = "SELECT assessment_id FROM nse_establishment_assessment WHERE (assessment_status='draft' || assessment_status='returned') && establishment_code=? ORDER BY assessment_id LIMIT 1";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('s', $establishment_code);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($assessment_id);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	//Get list of areas
	$statement = "SELECT assessment_area_id FROM nse_assessment_areas WHERE active='Y'";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->execute();
	$sql_areas->store_result();
	$sql_areas->bind_result($area_id);
	while ($sql_areas->fetch()) {
		$areas[] = $area_id;
	}
	$sql_areas->free_result();
	$sql_areas->close();
	
	//Calculate renewal date
	if (!empty($renewal_date)) {
		$renewal_date = date('Y-m-01', strtotime($renewal_date));
	}

	//Update Notes
	//Prepare statement - add note
	$statement = "INSERT INTO nse_establishment_notes (establishment_code, user_name, note_timestamp, note_content, module_name ) VALUES (?,?,?,?,'assessments')";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);

	//Get user name
	$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bind_param('i', $_SESSION['dbweb_user_id']);
	$sql_user->execute();
	$sql_user->store_result();
	$sql_user->bind_result($user_name);
	$sql_user->fetch();
	$sql_user->free_result();
	$sql_user->close();

	$line = explode('##',$notes);
	foreach ($line as $note_line) {
		if (empty($note_line)) continue;
		$note_items = explode('|', $note_line);
		//Calculate timestamp
		list($date, $time) = explode(' ', $note_items[1]);
		list($day, $month, $year) = explode('-', $date);
		list($hours, $minutes) = explode(':',$time);
		$timestamp = mktime($hours, $minutes, 0, $month, $day, $year);

		$sql_notes->bind_param('ssss', $establishment_code, $user_name, $timestamp, $note_items[2]);
		$sql_notes->execute();
	}
	$sql_notes->close();

	//Get assessor name
	$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=? LIMIT 1";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('i', $_SESSION['dbweb_user_id']);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_name);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();


	if (empty($assessment_id)) {
		$statement = "INSERT INTO nse_establishment_assessment (establishment_code, assessment_date, assessment_status, assessment_type, qa_status, proposed_category, assessor_name ) VALUES (?,?,?,?,?,?,?)";
		$sql_assessment_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessment_insert->bind_param('sssssss', $establishment_code, $assessment_date, $assessment_status, $assessment_type, $qa_status, $proposed_category, $assessor_name);
		$sql_assessment_insert->execute();
		$assessment_id = $sql_assessment_insert->insert_id;
		$sql_assessment_insert->close();
	} else {
		$statement = "UPDATE nse_establishment_assessment SET assessment_date=?, assessment_status=?, assessment_type=?, qa_status=?, proposed_category=?, assessor_name=? WHERE assessment_id=?";
		$sql_assessment_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessment_update->bind_param('sssssss', $assessment_date, $assessment_status, $assessment_type, $qa_status, $proposed_category, $assessor_name, $assessment_id);
		$sql_assessment_update->execute();
		$sql_assessment_update->close();
	}
	
	//Prepare statement - check area exists
	$statement = "SELECT count(*) FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
	$sql_area_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - update area
	$statement = "UPDATE nse_establishment_assessment_data SET assessment_content=?, assessment_rating=?, not_applicable=?, hide_from_client=? WHERE assessment_id=? && assessment_area_id=?";
	$sql_area_update = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare statement - insert area
	$statement = "INSERT INTO nse_establishment_assessment_data (assessment_content, assessment_rating, not_applicable, assessment_id, assessment_area_id, hide_from_client) VALUES (?,?,?,?,?,?)";
	$sql_area_insert = $GLOBALS['dbCon']->prepare($statement);
	
	foreach ($areas as $area_id) {
		$count = 0;
		$sql_area_check->bind_param('ss', $assessment_id, $area_id);
		$sql_area_check->execute();
		$sql_area_check->store_result();
		$sql_area_check->bind_result($count);
		$sql_area_check->fetch();
		$sql_area_check->free_result();
		
		if (isset($_POST['na_' . $area_id])) {
			$not_applicable = 'Y';
			$assessment_content = '';
			$assessment_rating = '';
		} else {
			$not_applicable = '';
			$assessment_content = $_POST['area_' . $area_id];
			$assessment_rating = isset($_POST['rate_' . $area_id])?$_POST['rate_' . $area_id]:'';
		}
		$hide = isset($_POST['hide_' . $area_id])?'Y':'';
		
		if ($count == 0) {
			$sql_area_insert->bind_param('ssssss', $assessment_content, $assessment_rating, $not_applicable, $assessment_id, $area_id, $hide);
			$sql_area_insert->execute();
		} else {
			$sql_area_update->bind_param('ssssss', $assessment_content, $assessment_rating, $not_applicable, $hide, $assessment_id, $area_id);
			$sql_area_update->execute();
		}
	}
	
	if ($assessment_status == 'draft') {
		//Log Booking
		$GLOBALS['log_tool']->write_entry("Assessment saved as draft ($establishment_code, $assessment_id)", $_SESSION['dbweb_user_id']);

		echo "<script>alert('Saved as Draft'); document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
	} else {
		//Log Booking
		$GLOBALS['log_tool']->write_entry("Assessment submitted for apprval ($establishment_code, $assessment_id)", $_SESSION['dbweb_user_id']);

		echo "<script>alert('Assessment Sent for Approval'); document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
	}
	
}

function draft_assessment() {
	setcookie('uEstabFilter', 'draft', 0, '/');
	
	echo "<script>document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
}

function overdue_assessment() {
	setcookie('uEstabFilter', 'overdue', 0, '/');
	
	echo "<script>document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
}

function one_month() {
	setcookie('uEstabFilter', '1month', 0, '/');
	
	echo "<script>document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
}

function three_month() {
	setcookie('uEstabFilter', '3month', 0, '/');
	
	echo "<script>document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
}

function establishment_detail_list() {
	//Vars
	$establishment_code = '';
	$establishment_name = '';
	$town_id = '';
	$province_id = '';
	$town_name = '';
	$province_name = '';
	$nLine = '';
	
	//Get template & prepare
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessor/html/establishment_list.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
	$lTemplate = $matches[0][0];
	
	//Prepare Statements - Province Name
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=? LIMIT 1";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Town Name
	$statement = "SELECT town_name FROM nse_location_town WHERE town_id=? LIMIT 1";
	$sql_town = $GLOBALS['dbCon']->prepare($statement);
	
	//Get list of establishments
	$statement = "SELECT a.establishment_code, a.establishment_name, b.town_id, b.province_id
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
					WHERE aa_estab='Y' ORDER BY RAND() LIMIT 20";
	$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
	$sql_establishments->execute();
	$sql_establishments->store_result();
	$sql_establishments->bind_result($establishment_code, $establishment_name, $town_id, $province_id);
	while ($sql_establishments->fetch()) {
		//Reset Vars
		$province_name = '';
		$town_name = '';
		$country_name = '';
		$buttons = '';
		$days = rand(0, 365);
		$status_num = rand(0,2);
		$status = array('complete','booked','overdue');
		$wait_counter = 0;
		
		//Get province Name
		$sql_province->bind_param('s', $province_id);
		$sql_province->execute();
		$sql_province->bind_result($province_name);
		$sql_province->store_result();
		$sql_province->fetch();
		$sql_province->free_result();
		
		//Get Town Name
		$sql_town->bind_param('s', $town_id);
		$sql_town->execute();
		$sql_town->bind_result($town_name);
		$sql_town->store_result();
		$sql_town->fetch();
		$sql_town->free_result();
		
		//Create Line
		$line = str_replace('<!-- establishment_name -->', $establishment_name, $lTemplate);
		$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
		$line = str_replace('<!-- location -->', "$town_name, $province_name", $line);
		$line = str_replace('<!-- next_assessment -->', $days, $line);
		$line = str_replace('<!-- status -->', $status[$status_num], $line);
		$line = str_replace('<!-- buttons -->', '<input type=button value="Print" class=print_button onClick=estab_print() /> <input type=button value="View Establishment" class=view_button onClick=estab_view() />', $line);
		
		$nLine .= $line;
	}
	$sql_establishments->free_result();
	$sql_establishments->close();
	
	//Replace Tags
	$template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}

function establishment_detail() {
	//Vars
	$advertiser_code = '';
	$advertiser_name = '';
	$advertiser_options = '';
	$advertiser = '';
	$aa_grade = '';
	
	$billing_line1 = '';
	$billing_line2 = '';
	$billing_line3 = '';
	$billing_code = '';
	$base_tel = '';
	$billing_hash = '';
	
	$company_name = '';
	$company_number = '';
	$contact_line = '';
	$contact_name = '';
	$contact_designation = '';
	$contact_tel = '';
	$contact_cell = '';
	$contact_email = '';
	$contacts_list = '';
	$contact_type = '';
	$contact_value = '';
	$contact_tel1 = '';
	$contact_tel2 = '';
	$contact_fax1 = '';
	$contact_fax2 = '';
	$contact_cell1 = '';
	$contact_cell2 = '';
	$contact_pemail = '';
	$contact_pemail2 = '';
	$contact_pemail3 = '';
	$contact_tel1_description = '';
	$contact_tel2_description = '';
	$contact_fax1_description = '';
	$contact_fax2_description = '';
	$contact_cell1_description = '';
	$contact_cell2_description = '';
	$country_options = '';
	$country_name = '';
	$country_id = '';
	$contact_description = '';
	$cancellation_policy = '';
	$child_policy = '';
	$contact_tel = '';
	$contact_fax = '';
	$category_prefix = '';
	$category = '';
	$category_suffix = '';
	$curr_icon = '';
	$current_restype_id = '';
	$current_star_grading = '';
	$change_name = '';
	$change_value = '';
	$change_list  = '';
	
	$directions = '';
	$default_image = '';
	
	$establishment_name = '';
	$establishment_code = isset($_GET['id'])&&ctype_alnum($_GET['id'])&&strlen($_GET['id'])<8?$_GET['id']:'';
	
	$facilities = '';
	$form_heading = "New Establishment";
	
	$grade_name = '';
	$grade_code = '';
	$grade_options = '';
	$gps_latitude = '';
	$gps_longitude = '';
	$gps_hash = '';
	
	$image_list = '';
	$image_hash = '';
	$image_id = '';
	$image_name = '';
	$image_title = '';
	$image_description = '';
	$icon_id = '';
	$icon_description = '';
	$icon_src = '';
	$icon_category_id = '';
	$icon = '';
	$icon_category_name = '';
	
	$landmark_distance = '';
	$landmark_measurement = '';
	$landmark_type_id = '';
	$landmark_type_name = '';
	$landmark_id = '';
	$landmark_name = '';
	$landmark_list = '';
	$last_updated = '';
	$long_description = '';
	$location_country_id = '';
	$location_province_id = '';
	$location_town_id = '';
	$location_suburb_id = '';
	
	$matches = array();
	$map_type_image = '';
	$map_type_google = '';
	$map_type_none = '';
	$map_type = '';
	$measurement_id = '';
	$measurement_name = '';
	
	
	$noprov = FALSE;
	$nightsbridge_id = '';
	
	$public_contact_harh = '';
	$postal_line1 = '';
	$postal_line2 = '';
	$postal_line3 = '';
	$postal_code = '';
	$primary_image = '';
	$province_id = '';
	$province_name = '';
	$province_options = '';
	$province_options_status = '';
	$pricing = '';
	$price_options = '<option value=""> </option>';
	$poi_entertainment = '';
	$poi_historical = '';
	$poi_other = '';
	$prebook_id = '';
	$poi_list = '';
	$price_prefixes = array('Unit','Double','Suite','Room','Cave','Site');
	$prefix_options = '<option value=""> </option>';
	$publication_list = '';
	$publication_id = '';
	$publication_name = '';
	$publication_expire = '';
	$publication_added = '';
	$publication_year = '';
	
	
	$reservation_fax = '';
	$reservation_tel = '';
	$reservation_email = '';
	$reservation_cell = '';
	$reservation_postal1 = '';
	$reservation_postal2 = '';
	$reservation_postal3 = '';
	$reservation_postal_code = '';
	$restype_options_0 = '<option value=0>-- None --</option>';
	$restype_options_1 = '<option value=0>-- None --</option>';
	$restype_options_2 = '<option value=0>-- None --</option>';
	
	$street_line1 = '';
	$street_line2 = '';
	$street_line3 = '';
	$short_description = '';
	$suburb_id = '';
	$suburb_name = '';
	$suburb_options = '';
	$suburb_options_status = '';
	$star_grading_options_0 = '';
	$star_grading_options_1 = '';
	$star_grading_options_2 = '';
	$subcategory_id = '';
	$subcategory_name = '';
	$sgl_prefix_options = '<option value=""> </option>';
	$sgl_category = '';
	$sgl_prefix = '';
	$sgl_suffix = '';
	$priceoutlow = '';
	$sgloutlo = '';
	$sgl_low_options = '<option value=""> </option>';
	$sgl_options ='<option value=""> </option>';
	$price_low_options = '<option value=""> </option>';
		
	$town_id = '';
	$town_name = '';
	$town_options = '';
	$town_options_status = '';
	$toplevel_id = '';
	$toplevel_name = '';
	
	$vat_number = '';
	
	$website_url = '';
	
	//Get Template
	$template = file_get_contents(SITE_ROOT . '/modules/assessor/html/establishment_details.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	
	//Get Basic Data
	$statement = "SELECT establishment_name, last_updated, postal_address_line1, postal_address_line2, postal_address_line3, postal_address_code, street_address_line1, street_address_line2, street_address_line3, aa_category_code, nightsbridge_bbid, prebook_bid, advertiser, website_url FROM nse_establishment WHERE establishment_code=?";
	$sql_basic = $GLOBALS['dbCon']->prepare($statement);
	$sql_basic->bind_param('s', $establishment_code);
	$sql_basic->execute();
	$sql_basic->bind_result($establishment_name, $last_updated, $postal_line1, $postal_line2, $postal_line3, $postal_code, $street_line1, $street_line2, $street_line3, $aa_grade, $nightsbridge_id, $prebook_id, $advertiser, $website_url);
	$sql_basic->store_result();
	$sql_basic->fetch();
	$sql_basic->free_result();
	$sql_basic->close();

	//Get Billing Data
	$statement = "SELECT billing_line1, billing_line2, billing_line3, billing_code, vat_number, company_name, company_number FROM nse_establishment_billing WHERE establishment_code=?";
	$sql_billing = $GLOBALS['dbCon']->prepare($statement);
	$sql_billing->bind_param('s', $establishment_code);
	$sql_billing->execute();
	$sql_billing->bind_result($billing_line1, $billing_line2, $billing_line3, $billing_code, $vat_number, $company_name, $company_number);
	$sql_billing->store_result();
	$sql_billing->fetch();
	$sql_billing->free_result();
	$sql_billing->close();

	//Get & Assebmle Contact details
	$counter = 0;
	$statement = "SELECT contact_name, contact_designation, contact_tel, contact_cell, contact_email FROM nse_establishment_contacts WHERE establishment_code=?";
	$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
	$sql_contacts->bind_param('s', $establishment_code);
	$sql_contacts->execute();
	$sql_contacts->bind_result($contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
	$sql_contacts->store_result();
	while ($sql_contacts->fetch()) {
		$contacts_list .= "!|!$counter|$contact_name|$contact_designation|$contact_tel|$contact_cell|$contact_email";
		$counter++;
	}
	$sql_contacts->free_result();
	$sql_contacts->close();

	//Get Public Contact Details
	$statement = "SELECT contact_type, contact_value, contact_description FROM nse_establishment_public_contact WHERE establishment_code=?";
	$sql_public_contact = $GLOBALS['dbCon']->prepare($statement);
	$sql_public_contact->bind_param('s', $establishment_code);
	$sql_public_contact->execute();
	$sql_public_contact->bind_result($contact_type, $contact_value, $contact_description);
	$sql_public_contact->store_result();
	if ($sql_public_contact->num_rows == 0) {
		//Get Reservation Data
		$statement = "SELECT reservation_tel, reservation_fax, reservation_cell, reservation_email, reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code FROM nse_establishment_reservation WHERE establishment_code=?";
		$sql_reservation = $GLOBALS['dbCon']->prepare($statement);
		$sql_reservation->bind_param('s', $establishment_code);
		$sql_reservation->execute();
		$sql_reservation->store_result();
		$sql_reservation->bind_result($reservation_tel, $reservation_fax, $reservation_cell, $reservation_email, $reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code);
		$sql_reservation->fetch();
		$sql_reservation->free_result();

		//Get contact Data
		$statement = "SELECT contact_tel, contact_cell, contact_fax FROM nse_establishment_contact WHERE establishment_code = ?";
		$sql_contact = $GLOBALS['dbCon']->prepare($statement);
		$sql_contact->bind_param('s', $establishment_code);
		$sql_contact->execute();
		$sql_contact->bind_result($contact_tel, $contact_cell, $contact_fax);
		$sql_contact->store_result();
		$sql_contact->fetch();
		$sql_contact->free_result();

		//Assemble tel /fax numbers
		$establishment_tel = !empty($contact_tel)?$contact_tel:$establishment_tel;
		$establishment_tel = !empty($base_tel)?$base_tel:$establishment_tel;
		$establishment_tel = !empty($reservation_tel)?$reservation_tel:$establishment_tel;
		$establishment_fax = !empty($contact_fax)?$contact_fax:$establishment_fax;
		if (substr($establishment_tel, 0, 5) == '& Fax') {
			$establishment_fax = substr($establishment_tel, 6);
			$establishment_tel = substr($establishment_tel, 6);
		}
		$establishment_fax = !empty($reservation_fax)?$reservation_fax:$establishment_fax;
		$establishment_cell = !empty($contact_cell)?$contact_cell:$establishment_cell;
		$establishment_cell = !empty($reservation_cell)?$reservation_cell:$establishment_cell;
		if (substr($establishment_tel, -1) == ',' || substr($establishment_tel, -1) == ';') $establishment_tel = substr($establishment_tel, 0, -1);
		if (substr($establishment_fax, -1) == ',' || substr($establishment_fax, -1) == ';') $establishment_fax = substr($establishment_fax, 0, -1);
		if (substr($establishment_cell, -1) == ',' || substr($establishment_cell, -1) == ';') $establishment_cell = substr($establishment_cell, 0, -1);

		$contact_tel1 = $establishment_tel;
		$contact_fax1 = $establishment_fax;
		$contact_cell1 = $establishment_cell;
		$contact_pemail = $reservation_email;
	} else {
		while ($sql_public_contact->fetch()) {
			switch ($contact_type) {
				case 'tel1':
					$contact_tel1 = $contact_value;
					$contact_tel1_description = $contact_description;
					break;
				case 'tel2':
					$contact_tel2 = $contact_value;
					$contact_tel2_description = $contact_description;
					break;
				case 'fax1':
					$contact_fax1 = $contact_value;
					$contact_fax1_description = $contact_description;
					break;
				case 'fax2':
					$contact_fax2 = $contact_value;
					$contact_fax2_description = $contact_description;
					break;
				case 'cell1':
					$contact_cell1 = $contact_value;
					$contact_cell1_description = $contact_description;
					break;
				case 'cell2':
					$contact_cell2 = $contact_value;
					$contact_cell2_description = $contact_description;
					break;
				case 'email':
					$contact_pemail = $contact_value;
					break;
				case 'email2':
					$contact_pemail2 = $contact_value;
					break;
				case 'email3':
					$contact_pemail3 = $contact_value;
					break;
			}
		}
	}
	$sql_public_contact->free_result();
	$sql_public_contact->close();

	//Get publications
	$statement = "SELECT a.publication_id, b.publication_name, a.add_date, b.publication_year, b.expiry_date
					FROM nse_establishment_publications AS a
					JOIN nse_publications AS b ON a.publication_id=b.publication_id
					WHERE a.establishment_code=?";
	$sql_publications = $GLOBALS['dbCon']->prepare($statement);
	$sql_publications->bind_param('s', $establishment_code);
	$sql_publications->execute();
	$sql_publications->store_result();
	$sql_publications->bind_result($publication_id, $publication_name, $publication_added, $publication_year, $publication_expire);
	while ($sql_publications->fetch()) {
		$publication_list .= "$publication_id|$publication_name|$publication_added|$publication_expire#";
	}
	$sql_publications->free_result();
	$sql_publications->close();

	//Get Images
	$statement = "SELECT image_id, thumb_name, image_title, image_description, primary_image FROM nse_establishment_images WHERE establishment_code=? ORDER BY primary_image DESC, image_name ";
	$sql_images = $GLOBALS['dbCon']->prepare($statement);
	$sql_images->bind_param('s', $establishment_code);
	$sql_images->execute();
	$sql_images->bind_result($image_id, $image_name, $image_title, $image_description, $primary_image);
	$sql_images->store_result();
	$counter = 0;
	while ($sql_images->fetch()) {
		if ($counter==0) $default_image = $image_id;
		$image_list .= "$image_id|http://www.aatravel.co.za/res_images/$image_name|$image_title|$image_description|";
		//$image_list .= $counter==0?'1':'0';
		$image_list .= "0!|!";
		$counter++;
	}
	$sql_images->free_result();
	$sql_images->close();

	//Get Descriptions, policies and pricing
	$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=? && language_code='EN' && description_type=?";
	$sql_description = $GLOBALS['dbCon']->prepare($statement);
	$description_type = 'short_description';
	$sql_description->bind_param('ss', $establishment_code, $description_type);
	$sql_description->execute();
	$sql_description->bind_result($short_description);
	$sql_description->store_result();
	$sql_description->fetch();
	$sql_description->free_result();

	$description_type = 'cancellation policy';
	$sql_description->bind_param('ss', $establishment_code, $description_type);
	$sql_description->execute();
	$sql_description->bind_result($cancellation_policy);
	$sql_description->store_result();
	$sql_description->fetch();
	$sql_description->free_result();

	$description_type = 'child policy';
	$sql_description->bind_param('ss', $establishment_code, $description_type);
	$sql_description->execute();
	$sql_description->bind_result($child_policy);
	$sql_description->store_result();
	$sql_description->fetch();
	$sql_description->free_result();

	$sql_description->close();

	if (empty($long_description)) $long_description = $short_description;


	/* POINTS OF INTEREST */

	//Get distance measurements
	$statement = "SELECT landmark_measurement_id, landmark_measurement_name FROM nse_landmark_measurement";
	$sql_measurements = $GLOBALS['dbCon']->prepare($statement);
	$sql_measurements->execute();
	$sql_measurements->store_result();
	$sql_measurements->bind_result($measurement_id, $measurement_name);
	while ($sql_measurements->fetch()) {
		$measurements[$measurement_id] = $measurement_name;
	}
	$sql_measurements->free_result();
	$sql_measurements->close();

	//prepare statement - establishment poi
	$statement = "SELECT b.landmark_id, b.landmark_name, a.landmark_distance, a.landmark_measurement
					FROM nse_establishment_landmarks as a
					JOIN nse_landmark as b ON a.landmark_id=b.landmark_id
					WHERE b.landmark_type_id=? && a.establishment_code=?";
	$sql_landmarks = $GLOBALS['dbCon']->prepare($statement);

	//Get landmarks
	$statement = "SELECT landmark_type_id, landmark_type_name FROM nse_landmark_types ORDER BY display_order";
	$sql_landmark_types = $GLOBALS['dbCon']->prepare($statement);
	$sql_landmark_types->execute();
	$sql_landmark_types->store_result();
	$sql_landmark_types->bind_result($landmark_type_id, $landmark_type_name);
	while($sql_landmark_types->fetch()) {
		$list_count = 1;
		$landmark_list .= "<TR><TD colspan=10 class=field_label>$landmark_type_name</TD></TR>";

		$sql_landmarks->bind_param('ss', $landmark_type_id, $establishment_code);
		$sql_landmarks->execute();
		$sql_landmarks->store_result();
		$sql_landmarks->bind_result($landmark_id, $landmark_name, $landmark_distance, $landmark_measurement);
	while($sql_landmarks->fetch()) {
			$landmark_list .= "<tr>";
			$landmark_list .= "<td width=10px>&nbsp;</td>";
			$landmark_list .= "<td width=10px >$list_count. </td>";
			$landmark_list .= "<td>";
			$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='$landmark_name' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
			$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='$landmark_id' />";
			$landmark_list .= "</td>";
			$landmark_list .= "<td>&nbsp;</td>";
			$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='$landmark_distance' onChange=set_change('poi','poi',this.name) /></td>";
			$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
			foreach ($measurements as $k=>$v) {
				if ($k == $landmark_measurement) {
					$landmark_list .= "<option value='$k' selected>$v</option>";
				} else {
					$landmark_list .= "<option value='$k'>$v</option>";
				}
			}
			$landmark_list .= "</select></td>";
			$landmark_list .= "<td><iframe class=poi_dym id={$landmark_type_id}_iframe_{$list_count}></iframe></td>";
			$landmark_list .= "</tr>";
			$list_count++;
		}
		while ($list_count < 4) {
			$landmark_list .= "<tr>";
			$landmark_list .= "<td width=10px>&nbsp;</td>";
			$landmark_list .= "<td width=10px >$list_count. </td>";
			$landmark_list .= "<td>";
			$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
			$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='' />";
			$landmark_list .= "</td>";
			$landmark_list .= "<td>&nbsp;</td>";
			$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='' onChange=set_change('poi','poi',this.name) /></td>";
			$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
			foreach ($measurements as $k=>$v) {
				$landmark_list .= "<option value='$k'>$v</option>";
			}
			$landmark_list .= "</select></td>";
			$landmark_list .= "<td><iframe class=poi_dym id={$landmark_type_id}_iframe_{$list_count}> </iframe></td>";
			$landmark_list .= "</tr>";
			$list_count++;
		}
		$landmark_list .= "<TR><TD colspan=10>&nbsp;</TD></TR>";
	}
	$sql_landmark_types->free_result();
	$sql_landmark_types->close();
	$sql_landmarks->free_result();
	$sql_landmarks->close();

	//Get Pricing Data
	$statement = "SELECT price_description, category_prefix, category, category_suffix, single_category, single_prefix, single_description, category_low, single_category_low FROM nse_establishment_pricing WHERE establishment_code=?";
	$sql_pricing = $GLOBALS['dbCon']->prepare($statement);
	$sql_pricing->bind_param('s', $establishment_code);
	$sql_pricing->execute();
	$sql_pricing->store_result();
	$sql_pricing->bind_result($pricing, $category_prefix, $category, $category_suffix, $sgl_category, $sgl_prefix, $sgl_suffix, $priceoutlow, $sgloutlo);
	$sql_pricing->fetch();
	$sql_pricing->free_result();
	$sql_pricing->close();

	//Assemble price category
	foreach ($price_codes as $code=>$icon_description) {
		if ($code == $category) {
			$price_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
		} else {
			$price_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
		}
	}
	foreach ($price_codes as $code=>$icon_description) {
		if ($code == $priceoutlow) {
			$price_low_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
		} else {
			$price_low_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
		}
	}
	foreach ($price_codes as $code=>$icon_description) {
		if ($code == $sgl_category) {
			$sgl_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
		} else {
			$sgl_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
		}
	}
	foreach ($price_codes as $code=>$icon_description) {
		if ($code == $sgloutlo) {
			$sgl_low_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
		} else {
			$sgl_low_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
		}
	}

	//Assemble price prefix
	foreach ($price_prefixes as $prefix) {
		if ($category_prefix == $prefix) {
			$prefix_options .= "<OPTION value='$prefix' selected>$prefix</option>";
		} else {
			$prefix_options .= "<OPTION value='$prefix' >$prefix</option>";
		}
	}
	foreach ($price_prefixes as $prefix) {
		if ($sgl_prefix == $prefix) {
			$sgl_prefix_options .= "<OPTION value='$prefix' selected>$prefix</option>";
		} else {
			$sgl_prefix_options .= "<OPTION value='$prefix' >$prefix</option>";
		}
	}

	//Get Current Facilities
	$curr_icons = array();
	$statement = "SELECT icon_id FROM nse_establishment_icon WHERE establishment_code=?";
	$sql_icons = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_icons->bind_param('s', $establishment_code);
	$sql_icons->execute();
	$sql_icons->bind_result($icon);
	$sql_icons->store_result();
	while ($sql_icons->fetch()) {
		$curr_icons[$icon] = $icon;
	}
	$sql_icons->free_result();
	$sql_icons->close();

	//Get establishment location data
	$statement = "SELECT country_id, province_id, town_id, suburb_id, gps_latitude, gps_longitude FROM nse_establishment_location WHERE establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->bind_result($location_country_id, $location_province_id, $location_town_id, $location_suburb_id, $gps_latitude, $gps_longitude);
	$sql_location->store_result();
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();

	//Get Location Data = Country
	$statement = "SELECT country_id, country_name FROM nse_location_country_lang WHERE language_code='EN' ORDER BY country_name";
	$sql_country = $GLOBALS['dbCon']->prepare($statement);
	$sql_country->execute();
	$sql_country->bind_result($country_id, $country_name);
	$sql_country->store_result();
	$country_options .= "<option value='0'>-- Select Country --</option>";
	while ($sql_country->fetch()) {
		if ($location_country_id == $country_id) {
			$country_options .= "<option value='$country_id' selected >$country_name</option>";
		} else {
			$country_options .= "<option value='$country_id'>$country_name</option>";
		}
	}

	//Get Location Data - Province
	$statement = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=? ORDER BY province_name";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	$sql_province->bind_param('s', $location_country_id);
	$sql_province->execute();
	$sql_province->bind_result($province_id, $province_name);
	$sql_province->store_result();
	if ($sql_province->num_rows == 0) {
		$province_options = "<option>-- No Provinces For Selected Country --</option>";
		$province_options_status = 'disabled';
		$noprov = TRUE;
	} else {
		$province_options .= "<option value='0'>-- Select Province --</option>";
		while ($sql_province->fetch()) {
			if ($location_province_id == $province_id) {
				$province_options .= "<option value='$province_id' selected>$province_name</option>";
			} else {
				$province_options .= "<option value='$province_id'>$province_name</option>";
			}
		}
	}

	//Get Location Data - Towns
	if ($noprov) {
		$statement = "SELECT town_id, town_name FROM nse_location_town WHERE country_id=? ORDER BY town_name";
		$sql_towns = $GLOBALS['dbCon']->prepare($statement);
		$sql_towns->bind_param('s', $location_country_id);
	} else {
		$statement = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=? ORDER BY town_name";
		$sql_towns = $GLOBALS['dbCon']->prepare($statement);
		$sql_towns->bind_param('s', $location_province_id);
	}
	$sql_towns->execute();
	$sql_towns->bind_result($town_id, $town_name);
	$sql_towns->store_result();
	if ($sql_towns->num_rows == 0) {
		$town_options = "-- No Towns For Selected Province --";
		$town_options_status = 'disabled';
	} else {
		$town_options .= "<option value='0'>-- Select Town --</option>";
		while ($sql_towns->fetch()) {
			if (strpos($town_name, 'Aaa') !== FALSE) continue;
			if (strpos($town_name, 'Zzz') !== FALSE) continue;
			if (strpos($town_name, '***') !== FALSE) continue;
			if ($location_town_id == $town_id) {
				$town_options .= "<option value='$town_id' selected >$town_name</option>";
			} else {
				$town_options .= "<option value='$town_id' >$town_name</option>";
			}
		}
	}

	//Get Location Data - Suburbs
	$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=? ORDER BY suburb_name";
	$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
	$sql_suburb->bind_param('s', $location_town_id);
	$sql_suburb->execute();
	$sql_suburb->bind_result($suburb_id, $suburb_name);
	$sql_suburb->store_result();
	if ($sql_suburb->num_rows == 0) {
		$suburb_options = "-- No Suburbs For Selected Town --";
		$suburb_options_status = 'disabled';
	} else {
		$suburb_options .= "<option value='0'>-- Select Suburb --</option>";
		while ($sql_suburb->fetch()) {
			if ($location_suburb_id == $suburb_id) {
				$suburb_options .= "<option value='$suburb_id' selected >$suburb_name</option>";
			} else {
				$suburb_options .= "<option value='$suburb_id' >$suburb_name</option>";
			}
		}
	}

	//Get Directions
	$statement = "SELECT directions, map_type FROM nse_establishment_directions WHERE establishment_code=?";
	$sql_directions = $GLOBALS['dbCon']->prepare($statement);
	$sql_directions->bind_param('s', $establishment_code);
	$sql_directions->execute();
	$sql_directions->bind_result($directions, $map_type);
	$sql_directions->store_result();
	$sql_directions->fetch();
	$sql_directions->free_result();
	$sql_directions->close();
	$directions = stripslashes($directions);
	switch ($map_type) {
		case 'none':
			$map_type_none = 'checked';
			break;
		case 'google':
			$map_type_google = 'checked';
			break;
		case 'upload':
			$map_type_image = 'checked';
			break;
		default:
			$map_type_none = 'checked';
	}

	//Get Current Restype
	$counter = 0;
	$statement = "SELECT subcategory_id, star_grading FROM nse_establishment_restype WHERE establishment_code=?";
	$sql_restype_current = $GLOBALS['dbCon']->prepare($statement);
	$sql_restype_current->bind_param('s', $establishment_code);
	$sql_restype_current->execute();
	$sql_restype_current->store_result();
	$sql_restype_current->bind_result($current_restype_id, $current_star_grading);
	while ($sql_restype_current->fetch()) {
		$current_restype[$counter] = $current_restype_id;
		$current_grading[$counter] = $current_star_grading;
		$counter++;
	}
	$sql_restype_current->free_result();
	$sql_restype_current->close();

	//Get AA Grades List
	$grade_options .= "<option value=''>N/A</option>";
	$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category";
	$sql_aa_categories = $GLOBALS['dbCon']->prepare($statement);
	$sql_aa_categories->execute();
	$sql_aa_categories->store_result();
	$sql_aa_categories->bind_result($grade_code, $grade_name);
	while ($sql_aa_categories->fetch()) {
		if ($grade_code == $aa_grade) {
			$grade_options .= "<option value='$grade_code' selected>$grade_name</option>";
		} else {
			$grade_options .= "<option value='$grade_code'>$grade_name</option>";
		}
	}
	$sql_aa_categories->free_result();
	$sql_aa_categories->close();

	//Get Advertisers
	$advertiser_options .= "<option value=''>N/A</option>";
	$statement = "SELECT advertiser_code, advertiser_name FROM nse_advertiser ORDER BY advertiser_name";
	$sql_advertiser = $GLOBALS['dbCon']->prepare($statement);
	$sql_advertiser->execute();
	$sql_advertiser->store_result();
	$sql_advertiser->bind_result($advertiser_code, $advertiser_name);
	while ($sql_advertiser->fetch()) {
		if ($advertiser == $advertiser_code) {
			$advertiser_options .= "<option value='$advertiser_code' selected>$advertiser_name</option>";
		} else {
			$advertiser_options .= "<option value='$advertiser_code'>$advertiser_name</option>";
		}
	}
	$sql_advertiser->free_result();
	$sql_advertiser->close();

	//Calculate Hashes

	$form_heading = "Details for $establishment_name ($establishment_code)";
	
	
	//Get restypes list
	$statement = "SELECT toplevel_id, toplevel_name FROM nse_restype_toplevel ORDER BY toplevel_id";
	$sql_restype_toplevel = $GLOBALS['dbCon']->prepare($statement);
		
	$statement = "SELECT DISTINCT(a.subcategory_name), a.subcategory_id
					FROM nse_restype_subcategory_lang AS a
					JOIN nse_restype_subcategory_parent AS b ON a.subcategory_id=b.subcategory_id
					JOIN nse_restype_category AS c ON c.category_id=b.category_id
					WHERE c.toplevel_id=?
					ORDER BY a.subcategory_name";
	$sql_restypes = $GLOBALS['dbCon']->prepare($statement);
	
	$sql_restype_toplevel->execute();
	$sql_restype_toplevel->store_result();
	$sql_restype_toplevel->bind_result($toplevel_id, $toplevel_name);
	while ($sql_restype_toplevel->fetch()) {
		$restype_options_0 .= "<optgroup label='$toplevel_name' >";
		$restype_options_1 .= "<optgroup label='$toplevel_name' >";
		$restype_options_2 .= "<optgroup label='$toplevel_name' >";
			
		$sql_restypes->bind_param('s', $toplevel_id);
		$sql_restypes->execute();
		$sql_restypes->store_result();
		$sql_restypes->bind_result($subcategory_name, $subcategory_id);
		while ($sql_restypes->fetch()) {
			if (isset($current_restype[0]) && $subcategory_id == $current_restype[0]) {
				$restype_options_0 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_0 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}

			if (isset($current_restype[1]) && $subcategory_id == $current_restype[1]) {
				$restype_options_1 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_1 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
			if (isset($current_restype[2]) && $subcategory_id == $current_restype[2]) {
				$restype_options_2 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_2 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
		}
		$restype_options_0 .= "</optgroup>";
		$restype_options_1 .= "</optgroup>";
		$restype_options_2 .= "</optgroup>";
	}
	$sql_restype_toplevel->free_result();
	$sql_restype_toplevel->close();
	$sql_restypes->free_result();
	$sql_restypes->close();
	
	//Get stargradings list
	for ($x=0; $x<6; $x++) {
		if (isset($current_grading[0]) && $x == $current_grading[0]) {
			$star_grading_options_0 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_0 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[1]) && $x == $current_grading[1]) {
			$star_grading_options_1 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_1 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[2]) && $x == $current_grading[2]) {
			$star_grading_options_2 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_2 .= "<option value=$x>$x</option>";
		}
	}

	//Get Facilities
	$facilities = "";
	$lRowsCount = 0;
	$rRowsCount = 0;
	$colLength = 3;
	$lRow = '<table width=100% cellpadding=0 cellspacing=0>';
	$rRow = '<table width=100%  cellpadding=0 cellspacing=0>';
	
	$statement = "SELECT a.icon_id, a.icon_url, b.icon_description
					FROM nse_icon AS a
					JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
					WHERE a.icon_category_id=?";
	$sql_facility_data = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "SELECT b.icon_name, a.icon_category_id
					FROM nse_icon_category AS a
					JOIN nse_icon_category_lang AS b ON a.icon_category_id=b.icon_category_id
					ORDER BY a.icon_priority ";
	$sql_facilities = $GLOBALS['dbCon']->prepare($statement);
	$sql_facilities->execute();
	$sql_facilities->bind_result($icon_category_name, $icon_category_id);
	$sql_facilities->store_result();
	while ($sql_facilities->fetch()) {
		if ($icon_category_name == 'Affiliations') continue;
		$col = 0;
		$sql_facility_data->bind_param('s', $icon_category_id);
		$sql_facility_data->execute();
		$sql_facility_data->bind_result($icon_id, $icon_src, $icon_description);
		$sql_facility_data->store_result();
		//if ($lRowsCount < $rRowsCount) {
			$lRow .= "<tr><td colspan=10 style='padding: 20px 0px 5px 0px'><b>$icon_category_name</b></td></tr>";
			while ($sql_facility_data->fetch()) {
				//$src_off = str_replace('.gif', '_off.gif', $icon_src);
				if ($col == 0) {
					$lRow .= "<tr valign=top>";
					$lRowsCount++;
				}
				$lRow .= "<td><input type=checkbox name='facilities[]' id='facilities_$icon_id' value='$icon_id' onChange=set_change('facility','facility',this.id) ";
				if (isset($curr_icons[$icon_id])) {
					$lRow .= 'checked';
				}
				$lRow .= " ></td><td><img src='$icon_src' width=33 height=35 style='margin: 1px'></td><td style='font-size: 12px'>$icon_description</td>";
				$col++;
				if ($col == $colLength) {
					$lRow .= "</tr>";
					$col = 0;
				} else {
					$lRow .= "<td>&nbsp;</td>";
				}
			}
			if ($col < $colLength) $lRow .= "";
	}
	$sql_facilities->free_result();
	$sql_facilities->close();
	
	$lRow .= '</table>';
	$rRow .= '</table>';
	$facilities .= "<table width=100% ><tr valign=top>";
	$facilities .= "<td>$lRow</td><td>&nbsp;&nbsp;&nbsp;</td><td>$rRow</td>";
	$facilities.= "</tr></table>";
	
	//Replace Tags
	$template = str_replace('<!-- form_heading -->', $form_heading, $template);
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_Replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- billing1 -->', $billing_line1, $template);
	$template = str_replace('<!-- billing2 -->', $billing_line2, $template);
	$template = str_replace('<!-- billing3 -->', $billing_line3, $template);
	$template = str_replace('<!-- billing_code -->', $billing_code, $template);
	$template = str_replace('<!-- vat_number -->', $vat_number, $template);
	$template = str_replace('<!-- company_number -->', $company_number, $template);
	$template = str_replace('<!-- company_name -->', $company_name, $template);
	$template = str_replace('<!-- contacts_list -->', $contacts_list, $template);
	$template = str_replace('<!-- tel1 -->', $contact_tel1, $template);
	$template = str_replace('<!-- tel2 -->', $contact_tel2, $template);
	$template = str_replace('<!-- fax1 -->', $contact_fax1, $template);
	$template = str_replace('<!-- fax2 -->', $contact_fax2, $template);
	$template = str_replace('<!-- cell1 -->', $contact_cell1, $template);
	$template = str_replace('<!-- cell2 -->', $contact_cell2, $template);
	$template = str_replace('<!-- tel1_description -->', $contact_tel1_description, $template);
	$template = str_replace('<!-- tel2_description -->', $contact_tel2_description, $template);
	$template = str_replace('<!-- fax1_description -->', $contact_fax1_description, $template);
	$template = str_replace('<!-- fax2_description -->', $contact_fax2_description, $template);
	$template = str_replace('<!-- cell1_description -->', $contact_cell1_description, $template);
	$template = str_replace('<!-- cell2_description -->', $contact_cell2_description, $template);
	$template = str_replace('<!-- email -->', $contact_pemail, $template);
	$template = str_replace('<!-- email2 -->', $contact_pemail2, $template);
	$template = str_replace('<!-- email3 -->', $contact_pemail3, $template);
	$template = str_replace('<!-- postal1 -->', $postal_line1, $template);
	$template = str_replace('<!-- postal2 -->', $postal_line2, $template);
	$template = str_replace('<!-- postal3 -->', $postal_line3, $template);
	$template = str_replace('<!-- postal_code -->', $postal_code, $template);
	$template = str_replace('<!-- street1 -->', $street_line1, $template);
	$template = str_replace('<!-- street2 -->', $street_line2, $template);
	$template = str_replace('<!-- street3 -->', $street_line3, $template);
	$template = str_replace('<!-- image_list -->', $image_list, $template);
	$template = str_replace('<!-- short_description -->', $short_description, $template);
	$template = str_replace('<!-- long_description -->', $long_description, $template);
	$template = str_replace('<!-- facilities -->', $facilities, $template);
	$template = str_replace('<!-- province_options -->', $province_options, $template);
	$template = str_replace('<!-- country_options -->', $country_options, $template);
	$template = str_replace('<!-- province_options_status -->', $province_options_status, $template);
	$template = str_replace('<!-- town_options -->', $town_options, $template);
	$template = str_replace('<!-- town_option_status -->', $town_options_status, $template);
	$template = str_replace('<!-- suburb_options -->', $suburb_options, $template);
	$template = str_replace('<!-- suburb_options_status -->', $suburb_options_status, $template);
	$template = str_replace('<!-- dd_lat -->', $gps_latitude, $template);
	$template = str_replace('<!-- dd_lon -->', $gps_longitude, $template);
	$template = str_replace('<!-- pricing -->', $pricing, $template);
	$template = str_replace('<!-- child -->', $child_policy, $template);
	$template = str_replace('<!-- cancellation -->', $cancellation_policy, $template);
	$template = str_replace('<!-- map_type_none -->', $map_type_none, $template);
	$template = str_replace('<!-- map_type_google -->', $map_type_google, $template);
	$template = str_replace('<!-- map_type_image -->', $map_type_image, $template);
	$template = str_replace('<!-- directions -->', $directions, $template);
	$template = str_replace('<!-- price_options -->', $price_options, $template);
	$template = str_replace('<!-- publication_list -->', $publication_list, $template);
	
	$template = str_replace('<!-- sgl_options -->', $sgl_options, $template);
	$template = str_replace('<!-- price_low_options -->', $price_low_options, $template);
	$template = str_replace('<!-- sgl_low_options -->', $sgl_low_options, $template);
	
	$template = str_replace('<!-- price_prefix -->', $prefix_options, $template);
	$template = str_replace('<!-- category_suffix -->', $category_suffix, $template);
	$template = str_replace('<!-- sgl_suffix -->', $sgl_suffix, $template);
	$template = str_replace('<!-- sgl_prefix -->', $sgl_prefix_options, $template);
	$template = str_replace('<!-- restype_0 -->', $restype_options_0, $template);
	$template = str_replace('<!-- restype_1 -->', $restype_options_1, $template);
	$template = str_replace('<!-- restype_2 -->', $restype_options_2, $template);
	$template = str_replace('<!-- star_grading_0 -->', $star_grading_options_0, $template);
	$template = str_replace('<!-- star_grading_1 -->', $star_grading_options_1, $template);
	$template = str_replace('<!-- star_grading_2 -->', $star_grading_options_2, $template);
	$template = str_replace('<!-- poi_list -->', $landmark_list, $template);
	$template = str_Replace('<!-- grade_options -->', $grade_options, $template);
	$template = str_Replace('<!-- nightsbridge -->', $nightsbridge_id, $template);
	$template = str_Replace('<!-- prebook -->', $prebook_id, $template);
	$template = str_replace('<!-- advertiser_options -->', $advertiser_options, $template);
	$template = str_replace('<!-- website -->', $website_url, $template);
	$template = str_replace('<!-- change_list -->', $change_list, $template);
	$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);
	
	$template = str_replace('<!-- billing_hash -->', $billing_hash, $template);
	$template = str_replace('<!-- public_contact_hash -->', $public_contact_hash, $template);
	$template = str_replace('<!-- contacts_hash -->', $contacts_hash, $template);
	$template = str_replace('<!-- image_hash -->', $image_hash, $template);
	$template = str_replace('<!-- description_hash -->', $description_hash, $template);
	$template = str_replace('<!-- gps_hash -->', $gps_hash, $template);
	$template = str_replace('<!-- policy_hash -->', $policy_hash, $template);
	$template = str_replace('<!-- direction_hash -->', $direction_hash, $template);
	$template = str_replace('<!-- price_hash -->', $price_hash, $template);
	$template = str_replace('<!-- icon_hash -->', $icon_hash, $template);
	
	$template = str_replace('<!-- api_key -->', $GLOBALS['api_key'], $template);
	$template = str_replace('<!-- default_image -->', $default_image, $template);
	$template = str_replace('<!-- button_name -->', 'updateData', $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}

function print_estab() {
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/assessor/html/print_establishment.html');
	
	echo $template;
}

function book_assessment_form() {
	//Vars
	$book_date = '';
	$booking_id = 0;
	$country = '';
	$establishment_code = $_GET['code'];
	$establishment_name = '';
	$location = '';
	$province = '';
	$town = '';
	
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessor/html/book_assessment_form.html');
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	//Get Location
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->store_result();
	$sql_location->bind_result($town, $province, $country);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	
	if (!empty($town)) $location .= $town;
	if (!empty($province)) {
		$location .= ", $province";
	} else {
		if (!empty($country)) $location .= ", $country";
	}
	
	//Check for current booking
	$statement = "SELECT booking_date, booking_id FROM nse_establishment_bookings WHERE establishment_code=? && booking_reason='assessment' ORDER BY booking_date LIMIT 1";
	$sql_book = $GLOBALS['dbCon']->prepare($statement);
	$sql_book->bind_param('s', $establishment_code);
	$sql_book->execute();
	$sql_book->store_result();
	$sql_book->bind_result($book_date, $booking_id);
	$sql_book->fetch();
	$sql_book->free_result();
	$sql_book->close();
	
	//Cancel checkbok
	if ($booking_id == 0) {
		$template = preg_replace('@<!-- cancel_start -->(.)*<!-- cancel_end -->@', '', $template);
	}
	
	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_replace('<!-- location -->', $location, $template);
	$template = str_replace('!book_date!', $book_date, $template);
	$template = str_replace('!booking_date!', $booking_id, $template);
	
	echo $template;
	
}

function book_assessment_save() {
	//Vars
        $book_type = $_POST['book_type'];
	$book_date = $_POST['book_date'];
	$booking_id = $_POST['booking_id'];
	$establishment_code = $_GET['code'];
	
	if ($book_type == 'cancel') {
		$statement = "DELETE FROM nse_establishment_bookings WHERE booking_id=?";
		$sql_book = $GLOBALS['dbCon']->prepare($statement);
		$sql_book->bind_param('i', $booking_id);
		$sql_book->execute();
		$sql_book->close();

                //Log Booking
		$GLOBALS['log_tool']->write_entry("Booked assessment canceled ($establishment_code)", $_SESSION['dbweb_user_id']);

		//Redirect
		echo "<script>alert('Assessment Booking Cancelled');document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
		die();
        } else if ($book_type == 'postpone') {
            $statement = "SELECT COUNT(*) FROM nse_establishment_assessment_postpone WHERE establishment_code=?";
            $sql_postpone_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_postpone_check->bind_param('s', $establishment_code);
            $sql_postpone_check->execute();
            $sql_postpone_check->bind_result($postpone_count);
            $sql_postpone_check->fetch();
            $sql_postpone_check->free_result();
            $sql_postpone_check->close();

            if ($postpone_count == 0) {
                $statement = "INSERT INTO nse_establishment_assessment_postpone (establishment_code, postpone_date) VALUES (?,?)";
                $sql_insert = $GLOBALS['dbCon']->prepare($statement);
                $sql_insert->bind_param('ss', $establishment_code, $book_date);
                $sql_insert->execute();
                $sql_insert->close();
            } else {
                $statement = "UPDATE nse_establishment_assessment_postpone SET postpone_date=? WHERE establishment_code=?";
                $sql_update = $GLOBALS['dbCon']->prepare($statement);
                $sql_update->bind_param('ss', $book_date, $establishment_code);
                $sql_update->execute();
                $sql_update->close();
            }

            //Delete Booking
            $statement = "DELETE FROM nse_establishment_bookings WHERE establishment_code=?";
            $sql_clear = $GLOBALS['dbCon']->prepare($statement);
            $sql_clear->bind_param('s', $establishment_code);
            $sql_clear->execute();
            $sql_clear->close();

            //Log Postponement
            $GLOBALS['log_tool']->write_entry("Assessment visit postponed ($establishment_code)", $_SESSION['dbweb_user_id']);

           //Redirect
            echo "<script>alert('Assessment Visit Postponed');document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
            die();
            
	} else {
	
		//Save
		if ($booking_id == 0) {
			$statement = "INSERT INTO nse_establishment_bookings (establishment_code, booking_date, booking_reason )VALUES (?,?,'assessment')";
			$sql_book = $GLOBALS['dbCon']->prepare($statement);
			$sql_book->bind_param('ss', $establishment_code, $book_date);
			$sql_book->execute();
			$sql_book->close();
		} else {
			$statement = "UPDATE nse_establishment_bookings SET booking_date=? WHERE booking_id=? ";
			$sql_book = $GLOBALS['dbCon']->prepare($statement);
			$sql_book->bind_param('si', $book_date, $booking_id);
			$sql_book->execute();
			$sql_book->close();
		}

		//Log Booking
		$GLOBALS['log_tool']->write_entry("Establishment booked for assessment ($establishment_code)", $_SESSION['dbweb_user_id']);

		//Redirect
		echo "<script>alert('Assessment Booking Updated');document.location='/modules/assessor/assessor.php?f=new_assessment'</script>";
	}
		
	
	
}

function welcome() {
	//Get template
	$assessor_id = $_SESSION['dbweb_user_id'];
	$template = file_get_contents(SITE_ROOT . '/modules/assessor/html/welcome.html');
	$overdue_count = 0;
	$month_1_count = 0;
	$month_3_count = 0;
	$establishment_code = '';
	$renewal_date = '';
	$return_count = '';
	$assessment_status = '';
	$old_review_date = '';
	$booking_date = '';
	$booking_id = 0;
	
	//Prepare statement - Booking
	$statement = "SELECT booking_date, booking_id FROM nse_establishment_bookings WHERE establishment_code=? && booking_reason='assessment' ORDER BY booking_date LIMIT 1";
	$sql_book = $GLOBALS['dbCon']->prepare($statement);
	
	//Get number of overdue
	$statement = "SELECT DISTINCT a.establishment_code,
					(SELECT renewal_date FROM nse_establishment_assessment WHERE establishment_code=a.establishment_code ORDER BY assessment_id DESC LIMIT 1) AS assessment_date,
					(SELECT assessment_status FROM nse_establishment_assessment WHERE establishment_code=a.establishment_code ORDER BY assessment_id DESC LIMIT 1) AS assessment_status,
					a.review_date_depricated
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_assessment AS c ON a.establishment_code=c.establishment_code
					WHERE aa_estab='Y' && a.assessor_id=?";
	$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
	$sql_establishments->bind_param('i', $assessor_id);
	$sql_establishments->execute();
	$sql_establishments->store_result();
	$sql_establishments->bind_result($establishment_code, $renewal_date, $assessment_status, $old_review_date);
	while ($sql_establishments->fetch()) {
		//Check for current booking
		$sql_book->bind_param('s', $establishment_code);
		$sql_book->execute();
		$sql_book->store_result();
		$sql_book->bind_result($booking_date, $booking_id);
		$sql_book->fetch();
		$sql_book->free_result();
		if ($booking_id != 0 && $assessment_status != 'draft' && $assessment_status != 'waiting' && $assessment_status != 'returned') {
			$assessment_status = 'booked';
		}
		
		if ($assessment_status == 'draft') continue;
		if ($assessment_status == 'returned') continue;
		if ($assessment_status == 'booked') continue;
		if ($assessment_status == 'waiting') continue;
		if (empty($assessment_status)) continue;
		if (empty($renewal_date)) $renewal_date = $old_review_date;
		$date1 = strtotime($renewal_date);
		$date2 = strtotime('now');
		$time_diff = date_diff_for_v52($date1, $date2);
		$time_diff_years = $time_diff['years'];
		$time_diff_months = $time_diff['months'];
		
		$time_diff = ($time_diff_years * 12) + $time_diff_months;
		$time_diff = $time_diff * (-1);
		if ($time_diff < 0) $overdue_count++;
		if ($time_diff == 0) $month_1_count++;
		if ($time_diff > 0 && $time_diff <= 2) $month_3_count++;
	}
	$sql_establishments->free_result();
	$sql_establishments->close();

	//Get number of returned assessments
	$statement = "SELECT COUNT(*)
					FROM nse_establishment_assessment AS a
					JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE a.assessment_status='returned' && b.assessor_id=?";
	$sql_return = $GLOBALS['dbCon']->prepare($statement);
	$sql_return->bind_param('i', $assessor_id);
	$sql_return->execute();
	$sql_return->store_result();
	$sql_return->bind_result($return_count);
	$sql_return->fetch();
	$sql_return->free_result();
	$sql_return->close();
	
	//Assemble count list
	if ($return_count > 0) {
		if ($return_count == 1) {
			$count_list = "<li style='color: navy; font-weight: bold'>$return_count returned assessment</li>";
		} else {
			$count_list = "<li style='color: navy; font-weight: bold'>$return_count returned assessments</li>";
		}
	} else {
		$count_list = "<li>$return_count returned assessments</li>";
	}
	$count_list .= $overdue_count==1?"<li>$overdue_count establishment is overdue for renewal</li>":"<li>$overdue_count establishments are overdue for renewal</li>";
	$count_list .= $month_1_count==1?"<li>$month_1_count establishment is due for renewal within the next month</li>":"<li>$month_1_count establishments are due for renewal within the next month</li>";
	$count_list .= $month_3_count==1?"<li>$month_3_count establishment is due for renewal within the next 3 months</li>":"<li>$month_3_count establishments are due for renewal within the next 3 months</li>";
	
	//Replace tags
	$template = str_replace('<!-- overdue_count -->', $overdue_count, $template);
	$template = str_replace('<!-- month_1_count -->', $month_1_count, $template);
	$template = str_replace('<!-- month_3_count -->', $month_3_count, $template);
	$template = str_replace('<!-- count_list -->', $count_list, $template);
	
	echo $template;
}

function date_diff_for_v52($date1, $date2) {
	//Vars
	$date['years'] = 0;
	$date['months'] = 0;
	if (empty($date1)) return 0;
	$date_diff = $date2 - $date1;
	//$date['years'] = floor($date_diff/31536000);
	//if ($date['years'] < 0) $date['years'] = 0;
	$date['months'] = floor($date_diff/2628000);
	//if ($date['months'] < 0) ($date['months']++)* (-1);
	
	return $date;
}

function establishment_list() {
	//Vars
	$assessor_id = $_SESSION['dbweb_user_id'];
	$establishment_code = '';
	$establishment_count = 0;
	$establishment_name = '';
	$town_id = '';
	$province_id = '';
	$town_name = '';
	$province_name = '';
	$nLine = '';
	$where = '';
	$link = '';
	$estab_list = array();
	$sort_order = isset($_GET['order']) && $_GET['order']=='desc'?'DESC':'ASC';
	$order_by = isset($_GET['ob'])?$_GET['ob']:'';
	$list_start = isset($_GET['st'])?$_GET['st']:0;
	
	//Get template & prepare
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessor/html/establishment_list_a.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
	$lTemplate = $matches[0][0];
	
	//Prepare Statements - Province Name
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=? LIMIT 1";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Town Name
	$statement = "SELECT town_name FROM nse_location_town WHERE town_id=? LIMIT 1";
	$sql_town = $GLOBALS['dbCon']->prepare($statement);

	if (isset($_POST['search_string'])) {
		switch ($_POST['search_filter']) {
			case 'establishment_name':
				$where = " && a.establishment_name LIKE '%{$_POST['search_string']}%'";
				break;
			case 'establishment_code':
				$where = " && a.establishment_code LIKE '%{$_POST['search_string']}%'";
				break;
		}
		$link .= "&sFilter={$_POST['search_filter']}&sString={$_POST['search_string']}";
	}

	if (isset($_GET['sFilter'])) {
		switch ($_GET['sFilter']) {
			case 'establishment_name':
				$where = " && a.establishment_name LIKE '%{$_GET['sString']}%'";
				break;
			case 'establishment_code':
				$where = " && a.establishment_code LIKE '%{$_GET['sString']}%'";
				break;
		}
		$link .= "&sFilter={$_GET['sFilter']}&sString={$_GET['sString']}";
	}

	//Get list of establishments
	$statement = "SELECT a.establishment_code, a.establishment_name, b.town_id, b.province_id
			FROM nse_establishment AS a
			LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
			WHERE assessor_id=? $where";
	$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
	$sql_establishments->bind_param('i', $assessor_id);
	$sql_establishments->execute();
	$sql_establishments->store_result();
	$row_count = $sql_establishments->num_rows;
	$sql_establishments->bind_result($establishment_code, $establishment_name, $town_id, $province_id);
	while ($sql_establishments->fetch()) {
		//Reset Vars
		$province_name = '';
		$town_name = '';
		$country_name = '';
		$buttons = '';
		$wait_counter = 0;
		
		//Get province Name
		$sql_province->bind_param('s', $province_id);
		$sql_province->execute();
		$sql_province->bind_result($province_name);
		$sql_province->store_result();
		$sql_province->fetch();
		$sql_province->free_result();
		
		//Get Town Name
		$sql_town->bind_param('s', $town_id);
		$sql_town->execute();
		$sql_town->bind_result($town_name);
		$sql_town->store_result();
		$sql_town->fetch();
		$sql_town->free_result();

		//Assemble Buttons
		//$buttons = "<input type=button value='Print' class=print_button onClick=estab_print() />";
		$buttons .= "<input type=button value='Edit' class=edit_button onClick='document.location=\"/modules/assessor/assessor.php?f=edit_establishment&code=$establishment_code\"' />";

		$estab_list[$establishment_count]['establishment_code'] = $establishment_code;
		$estab_list[$establishment_count]['establishment_name'] = $establishment_name;
		$estab_list[$establishment_count]['town_name'] = $town_name;
		$estab_list[$establishment_count]['province_name'] = $province_name;
		$estab_list[$establishment_count]['buttons'] = $buttons;

		$establishment_count++;

//		//Create Line
//		$line = str_replace('<!-- establishment_name -->', $establishment_name, $lTemplate);
//		$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
//		$line = str_replace('<!-- location -->', "$town_name, $province_name", $line);
//		$line = str_replace('<!-- buttons -->', $buttons, $line);
//
//		$nLine .= $line;
	}
	$sql_establishments->free_result();
	$sql_establishments->close();

	if ($row_count == 0) {
		$nLine = "<tr><td colspan=10 align=center>-- No Results --</td></tr>";
		$navigation = '<tr><th colspan=10 >&nbsp;</th></tr>';
	} else {
		//Sorting
		foreach($estab_list as $key=>$data) {
			switch ($order_by) {
				case 'code':
					$sort_array[$key] = $data['establishment_code'];
					break;
				case 'entered':
					$sort_array[$key] = $data['year_entered'];
					break;
				case 'booking_date':
					$sort_array[$key] = $data['booking_date'];
					break;
				case 'status':
					$sort_array[$key] = $data['status'];
					break;
				case 'review':
					$sort_array[$key] = $data['assessment_date'];
					break;
				case 'location':
					$sort_array[$key] = $data['town_name'];
					break;
				default:
					$sort_array[$key] = $data['establishment_name'];
			}

		}
		if (count($estab_list) == 0) {
			$nLine = "<tr><td colspan=10 align=center>-- No Results --</td></tr>";
			$navigation = '<tr><th colspan=10 >&nbsp;</th></tr>';
		} else {
			if ($sort_order == 'DESC') {
				asort($sort_array);
			} else {
				arsort($sort_array);
			}
			foreach ($sort_array as $key=>$value) {
				$new_list[] = $estab_list[$key];
			}
			$estab_list = $new_list;

			//Create Line
			$counter = -1;
			foreach($estab_list as $data) {
				$counter++;
				if ($counter < $list_start) continue;
				if ($counter > $list_start+$GLOBALS['list_length']-1) break;
				$line = str_replace('<!-- establishment_name -->', $data['establishment_name'], $lTemplate);
				$line = str_replace('<!-- establishment_code -->', $data['establishment_code'], $line);
				$line = str_replace('<!-- location -->', "{$data['town_name']}, {$data['province_name']}", $line);
				$line = str_replace('<!-- buttons -->', $data['buttons'], $line);
				$line = str_replace('!establishment_code!', $data['establishment_code'], $line);

				$nLine .= $line;
			}

			//Navigation
			$establishment_count = count($estab_list);
			$total_pages = ceil($establishment_count/$GLOBALS['list_length']);
			$current_page = ceil($list_start/$GLOBALS['list_length']) + 1;
			$navigation = "<tr><th colspan=10><table width=100%><tr>";
			if ($GLOBALS['list_length'] > $establishment_count || $current_page == 1) {
				$navigation .= "<th >&nbsp;</th>";
			} else {
				if (isset($_GET['st'])) {
					$navigation .= "<th align=left width=100px >&nbsp;<a href='/modules/assessor/assessor.php?f=establishment_editor&st=";
					$navigation .= $list_start - $GLOBALS['list_length'];
					$navigation .= $link;
					$navigation .= "' >&#8249; Previous</a> </th>";
				} else {
					$navigation .= "<th>&nbsp;</th>";
				}
			}
			$navigation .= "<th colspan=5 align=center >Page: ";
			for ($i=1; $i<=$total_pages; $i++) {
				$navigation .= "<a href='/modules/assessor/assessor.php?f=establishment_editor&st=";
				$navigation .= ($i-1) * $GLOBALS['list_length'];
				if (isset($_GET['ob'])) $navigation .= "&ob=" . $_GET['ob'];
				if (isset($_GET['order'])) $navigation .= "&order=" . $_GET['order'];
				$navigation .= $link;
				$navigation .= "' >$i</a> | ";
			}
			$navigation = substr($navigation, 0, -2);
			$navigation .= "</th>";

			if ($current_page < $total_pages) {
				$navigation .= "<th align=right width=100px><a href='/modules/assessor/assessor.php?f=establishment_editor&st=";
				$navigation .= $list_start + $GLOBALS['list_length'];
				$navigation .= $link;
				$navigation .= "'>Next &#8250;</a>&nbsp;</th>";
			} else {
				$navigation .= "<th>&nbsp;</th>";
			}
			$navigation .= "</tr></table></th></tr>";
		}
	}
	//Heading
	$heading_titles = array('empty'=>'&nbsp;',
							'code'=>'Code',
							'establishment'=>'Establishment',
							'location'=>'Location',
							'nb'=>'&nbsp;');
	$heading_widths = array('empty'=>10,
							'code'=>50,
							'establishment'=>300,
							'location'=>200,
							'nb'=>145);
	$line_header = "<tr style='background-color: grey'>";
	foreach ($heading_titles as $col_id=>$col_name) {
		$line_header .= "<TH width={$heading_widths[$col_id]}>";
		if (!isset($_POST['search_string']) && ($col_id != 'empty' && $col_id != 'nb')) {
			$line_header .= "<a href='/modules/assessor/assessor.php?f=establishment_editor&ob=$col_id";
			if ($col_id == $order_by) {
				if (!isset($_GET['order'])) $line_header .= "&order=desc";
			}
			$line_header .= $link;
			$line_header .= "' >";
		}
		$line_header .= "$col_name";
		if (!isset($_POST['search_string'])) {
			if ($col_id == $order_by) {
				if ($sort_order == 'DESC') {
					$line_header .= "<img src='/i/arrow_up.png' border=0 title='Ascending Order (A-Z)' />";
				} else {
					$line_header .= "<img src='/i/arrow_down.png' border=0 title='Descending Order (Z-A)' />";
				}
			}
		}
		if (!isset($_POST['search_string'])) $line_header .= "</a>";
		$line_header .= "</TH>";
	}
	$line_header .= "</tr>";

	//Replace Tags
	$template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
	$template = str_replace('<!-- navigation -->', $navigation, $template);
	$template = str_replace('<!-- line_header -->', $line_header, $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}

function establishment_edit_form() {
	//Vars
	$advertiser_code = '';
	$advertiser_name = '';
	$advertiser_options = '';
	$advertiser = '';
	$aa_grade = '';
	$assessor_id = '';
	$assessor_list = '';
	$assessor_name = '';
	$assessor_code = '';
	$awards_list = '';
	
	
	$billing_line1 = '';
	$billing_line2 = '';
	$billing_line3 = '';
	$billing_code = '';
	$base_tel = '';
	$billing_hash = '';
	
	$company_name = '';
	$company_number = '';
	$contact_line = '';
	$contact_name = '';
	$contact_designation = '';
	$contact_tel = '';
	$contact_cell = '';
	$contact_email = '';
	$contacts_list = '';
	$contact_type = '';
	$contact_value = '';
	$contact_tel1 = '';
	$contact_tel2 = '';
	$contact_fax1 = '';
	$contact_fax2 = '';
	$contact_cell1 = '';
	$contact_cell2 = '';
	$contact_pemail = '';
	$contact_pemail2 = '';
	$contact_pemail3 = '';
	$contact_tel1_description = '';
	$contact_tel2_description = '';
	$contact_fax1_description = '';
	$contact_fax2_description = '';
	$contact_cell1_description = '';
	$contact_cell2_description = '';
	$country_options = '';
	$country_name = '';
	$country_id = '';
	$contact_description = '';
	$cancellation_policy = '';
	$child_policy = '';
	$contact_tel = '';
	$contact_fax = '';
	$category_prefix = '';
	$category = '';
	$category_suffix = '';
	$curr_icon = '';
	$current_restype_id = '';
	$current_star_grading = '';
	$change_name = '';
	$change_value = '';
	$change_list  = '';
	
	$directions = '';
	$default_image = '';
	
	$establishment_name = '';
	$establishment_code = isset($_GET['code'])&&ctype_alnum($_GET['code'])&&strlen($_GET['code'])<8?$_GET['code']:'';
	
	$facilities = '';
	$form_heading = "New Establishment";
	
	$grade_name = '';
	$grade_code = '';
	$grade_options = '';
	$gps_latitude = '';
	$gps_longitude = '';
	$gps_hash = '';
	
	$image_list = '';
	$image_hash = '';
	$image_id = '';
	$image_name = '';
	$image_title = '';
	$image_description = '';
	$inactive_estab = '';
	$icon_id = '';
	$icon_description = '';
	$icon_src = '';
	$icon_category_id = '';
	$icon = '';
	$icon_category_name = '';
	$invoice_list = '';
	$invoice_id = '';
	$invoice_item = '';
	$invoice_value = '';
	$invoice_paid = '';
	$invoice_date = '';
	
	$landmark_distance = '';
	$landmark_measurement = '';
	$landmark_type_id = '';
	$landmark_type_name = '';
	$landmark_id = '';
	$landmark_name = '';
	$landmark_list = '';
	$last_updated = '';
	$long_description = '';
	$location_country_id = '';
	$location_province_id = '';
	$location_town_id = '';
	$location_suburb_id = '';
	
	$matches = array();
	$map_type_image = '';
	$map_type_google = '';
	$map_type_none = '';
	$map_type = '';
	$measurement_id = '';
	$measurement_name = '';
	
	$noprov = FALSE;
	$nightsbridge_id = '';
	
	$pastel_code = '';
	$public_contact_harh = '';
	$postal_line1 = '';
	$postal_line2 = '';
	$postal_line3 = '';
	$postal_code = '';
	$primary_image = '';
	$province_id = '';
	$province_name = '';
	$province_options = '';
	$province_options_status = '';
	$pricing = '';
	$price_options = '<option value=""> </option>';
	$poi_entertainment = '';
	$poi_historical = '';
	$poi_other = '';
	$prebook_id = '';
	$poi_list = '';
	$price_prefixes = array('Unit','Double','Suite','Room','Cave','Site');
	$prefix_options = '<option value=""> </option>';
	$publication_list = '';
	$publication_id = '';
	$publication_name = '';
	$publication_expire = '';
	$publication_added = '';
	$publication_year = '';
	
	$representative_list = '';
	$reservation_fax = '';
	$reservation_tel = '';
	$reservation_email = '';
	$reservation_cell = '';
	$reservation_postal1 = '';
	$reservation_postal2 = '';
	$reservation_postal3 = '';
	$reservation_postal_code = '';
	$restype_options_0 = '<option value=0>-- None --</option>';
	$restype_options_1 = '<option value=0>-- None --</option>';
	$restype_options_2 = '<option value=0>-- None --</option>';
	$room_count = '';
	
	$street_line1 = '';
	$street_line2 = '';
	$street_line3 = '';
	$short_description = '';
	$suburb_id = '';
	$suburb_name = '';
	$suburb_options = '';
	$suburb_options_status = '';
	$star_grading_options_0 = '';
	$star_grading_options_1 = '';
	$star_grading_options_2 = '';
	$subcategory_id = '';
	$subcategory_name = '';
	$sgl_prefix_options = '<option value=""> </option>';
	$sgl_category = '';
	$sgl_prefix = '';
	$sgl_suffix = '';
	$priceoutlow = '';
	$sgloutlo = '';
	$sgl_low_options = '<option value=""> </option>';
	$sgl_options ='<option value=""> </option>';
	$price_low_options = '<option value=""> </option>';

	$template = file_get_contents(SITE_ROOT . '/modules/assessor/html/establishment_form.html');
	$town_id = '';
	$town_name = '';
	$town_options = '';
	$town_options_status = '';
	$toplevel_id = '';
	$toplevel_name = '';
	
	$vat_number = '';
	
	$website_url = '';	
	
	//Get Template
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	
	//Get Basic Data
	$statement = "SELECT establishment_name, last_updated, postal_address_line1, postal_address_line2, postal_address_line3, postal_address_code, street_address_line1, street_address_line2, street_address_line3, aa_category_code, nightsbridge_bbid, prebook_bid, advertiser, website_url, pastel_code, active, assessor_id FROM nse_establishment WHERE establishment_code=?";
	$sql_basic = $GLOBALS['dbCon']->prepare($statement);
	$sql_basic->bind_param('s', $establishment_code);
	$sql_basic->execute();
	$sql_basic->bind_result($establishment_name, $last_updated, $postal_line1, $postal_line2, $postal_line3, $postal_code, $street_line1, $street_line2, $street_line3, $aa_grade, $nightsbridge_id, $prebook_id, $advertiser, $website_url, $pastel_code, $inactive_estab, $assessor_id);
	$sql_basic->store_result();
	$sql_basic->fetch();
	$sql_basic->free_result();
	$sql_basic->close();

	//Inactive Establishment
	$inactive_estab = $inactive_estab == '1'?'':'checked';
		
	//Get Billing Data
	$statement = "SELECT billing_line1, billing_line2, billing_line3, billing_code, vat_number, company_name, company_number FROM nse_establishment_billing WHERE establishment_code=?";
	$sql_billing = $GLOBALS['dbCon']->prepare($statement);
	$sql_billing->bind_param('s', $establishment_code);
	$sql_billing->execute();
	$sql_billing->bind_result($billing_line1, $billing_line2, $billing_line3, $billing_code, $vat_number, $company_name, $company_number);
	$sql_billing->store_result();
	$sql_billing->fetch();
	$sql_billing->free_result();
	$sql_billing->close();

	//Prepare statement - Check if user has admin access
	$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
	$sql_admin = $GLOBALS['dbCon']->prepare($statement);

	//Get & Assemble Contact details
	$counter = 0;
	$statement = "SELECT contact_name, contact_designation, contact_tel, contact_cell, contact_email FROM nse_establishment_contacts WHERE establishment_code=?";
	$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
	$sql_contacts->bind_param('s', $establishment_code);
	$sql_contacts->execute();
	$sql_contacts->bind_result($contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
	$sql_contacts->store_result();
	while ($sql_contacts->fetch()) {
		$sql_admin->bind_param('s', $contact_email);
		$sql_admin->execute();
		$sql_admin->store_result();
		$sql_admin->bind_result($user_counter);
		$sql_admin->fetch();
		$contacts_list .= "!|!$counter|$contact_name|$contact_designation|$contact_tel|$contact_cell|$contact_email|$user_counter|0|$contact_email|0";
		$counter++;
	}
	$sql_contacts->free_result();
	$sql_contacts->close();
	
	//Get Public Contact Details
	$statement = "SELECT contact_type, contact_value, contact_description FROM nse_establishment_public_contact WHERE establishment_code=?";
	$sql_public_contact = $GLOBALS['dbCon']->prepare($statement);
	$sql_public_contact->bind_param('s', $establishment_code);
	$sql_public_contact->execute();
	$sql_public_contact->bind_result($contact_type, $contact_value, $contact_description);
	$sql_public_contact->store_result();

	if ($sql_public_contact->num_rows == 0) {

		//Get Reservation Data
		$statement = "SELECT reservation_tel, reservation_fax, reservation_cell, reservation_email, reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code FROM nse_establishment_reservation WHERE establishment_code=?";
		$sql_reservation = $GLOBALS['dbCon']->prepare($statement);
		$sql_reservation->bind_param('s', $establishment_code);
		$sql_reservation->execute();
		$sql_reservation->store_result();
		$sql_reservation->bind_result($reservation_tel, $reservation_fax, $reservation_cell, $reservation_email, $reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code);
		$sql_reservation->fetch();
		$sql_reservation->free_result();

		//Get contact Data
		$statement = "SELECT contact_tel, contact_cell, contact_fax FROM nse_establishment_contact WHERE establishment_code = ?";
		$sql_contact = $GLOBALS['dbCon']->prepare($statement);
		$sql_contact->bind_param('s', $establishment_code);
		$sql_contact->execute();
		$sql_contact->bind_result($contact_tel, $contact_cell, $contact_fax);
		$sql_contact->store_result();
		$sql_contact->fetch();
		$sql_contact->free_result();
			
		//Assemble tel /fax numbers
		$establishment_tel = !empty($contact_tel)?$contact_tel:$establishment_tel;
		$establishment_tel = !empty($base_tel)?$base_tel:$establishment_tel;
		$establishment_tel = !empty($reservation_tel)?$reservation_tel:$establishment_tel;
		$establishment_fax = !empty($contact_fax)?$contact_fax:$establishment_fax;
		if (substr($establishment_tel, 0, 5) == '& Fax') {
			$establishment_fax = substr($establishment_tel, 6);
			$establishment_tel = substr($establishment_tel, 6);
		}
		$establishment_fax = !empty($reservation_fax)?$reservation_fax:$establishment_fax;
		$establishment_cell = !empty($contact_cell)?$contact_cell:$establishment_cell;
		$establishment_cell = !empty($reservation_cell)?$reservation_cell:$establishment_cell;
		if (substr($establishment_tel, -1) == ',' || substr($establishment_tel, -1) == ';') $establishment_tel = substr($establishment_tel, 0, -1);
		if (substr($establishment_fax, -1) == ',' || substr($establishment_fax, -1) == ';') $establishment_fax = substr($establishment_fax, 0, -1);
		if (substr($establishment_cell, -1) == ',' || substr($establishment_cell, -1) == ';') $establishment_cell = substr($establishment_cell, 0, -1);

		$contact_tel1 = $establishment_tel;
		$contact_fax1 = $establishment_fax;
		$contact_cell1 = $establishment_cell;
		$contact_pemail = $reservation_email;
	} else {
		while ($sql_public_contact->fetch()) {
			switch ($contact_type) {
				case 'tel1':
					$contact_tel1 = $contact_value;
					$contact_tel1_description = $contact_description;
					break;
				case 'tel2':
					$contact_tel2 = $contact_value;
					$contact_tel2_description = $contact_description;
					break;
				case 'fax1':
					$contact_fax1 = $contact_value;
					$contact_fax1_description = $contact_description;
					break;
				case 'fax2':
					$contact_fax2 = $contact_value;
					$contact_fax2_description = $contact_description;
					break;
				case 'cell1':
					$contact_cell1 = $contact_value;
					$contact_cell1_description = $contact_description;
					break;
				case 'cell2':
					$contact_cell2 = $contact_value;
					$contact_cell2_description = $contact_description;
					break;
				case 'email':
					$contact_pemail = $contact_value;
					break;
				case 'email2':
					$contact_pemail2 = $contact_value;
					break;
				case 'email3':
					$contact_pemail3 = $contact_value;
					break;
			}
		}
	}
	$sql_public_contact->free_result();
	$sql_public_contact->close();
	
	//Get publications
	$statement = "SELECT a.publication_id, b.publication_name, a.add_date, b.publication_year, b.expiry_date
					FROM nse_establishment_publications AS a
					JOIN nse_publications AS b ON a.publication_id=b.publication_id
					WHERE a.establishment_code=?";
	$sql_publications = $GLOBALS['dbCon']->prepare($statement);
	$sql_publications->bind_param('s', $establishment_code);
	$sql_publications->execute();
	$sql_publications->store_result();
	$sql_publications->bind_result($publication_id, $publication_name, $publication_added, $publication_year, $publication_expire);
	while ($sql_publications->fetch()) {
		$publication_list .= "$publication_id|$publication_name|$publication_added|$publication_expire#";
	}
	$sql_publications->free_result();
	$sql_publications->close();
		
	//Get Images
	$statement = "SELECT image_id, thumb_name, image_title, image_description, primary_image FROM nse_establishment_images WHERE establishment_code=? ORDER BY primary_image DESC, image_name ";
	$sql_images = $GLOBALS['dbCon']->prepare($statement);
	$sql_images->bind_param('s', $establishment_code);
	$sql_images->execute();
	$sql_images->bind_result($image_id, $image_name, $image_title, $image_description, $primary_image);
	$sql_images->store_result();
	$counter = 0;
	while ($sql_images->fetch()) {
		if ($counter==0) $default_image = $image_id;
		$image_list .= "$image_id|http://www.aatravel.co.za/res_images/$image_name|$image_title|$image_description|";
		//$image_list .= $counter==0?'1':'0';
		$image_list .= "0!|!";
		$counter++;
	}
	$sql_images->free_result();
	$sql_images->close();
	
	//Get Descriptions, policies and pricing
	$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=? && language_code='EN' && description_type=?";
	$sql_description = $GLOBALS['dbCon']->prepare($statement);
	$description_type = 'short_description';
	$sql_description->bind_param('ss', $establishment_code, $description_type);
	$sql_description->execute();
	$sql_description->bind_result($short_description);
	$sql_description->store_result();
	$sql_description->fetch();
	$sql_description->free_result();

	$description_type = 'long_description';
	$sql_description->bind_param('ss', $establishment_code, $description_type);
	$sql_description->execute();
	$sql_description->bind_result($long_description);
	$sql_description->store_result();
	$sql_description->fetch();
	$sql_description->free_result();

	//Alternate - Data is returned corrupt when the above mysqli method is used.
	$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code='$establishment_code' && language_code='EN' && description_type='long_description'";
	$sql_description2 = mysql_query($statement);
	list($long_description) = mysql_fetch_array($sql_description2);

	$description_type = 'cancellation policy';
	$sql_description->bind_param('ss', $establishment_code, $description_type);
	$sql_description->execute();
	$sql_description->bind_result($cancellation_policy);
	$sql_description->store_result();
	$sql_description->fetch();
	$sql_description->free_result();

	$description_type = 'child policy';
	$sql_description->bind_param('ss', $establishment_code, $description_type);
	$sql_description->execute();
	$sql_description->bind_result($child_policy);
	$sql_description->store_result();
	$sql_description->fetch();
	$sql_description->free_result();
	$sql_description->close();
		
	if (empty($long_description)) $long_description = $short_description;
	
		
	/* POINTS OF INTEREST */

	//Get distance measurements
	$statement = "SELECT landmark_measurement_id, landmark_measurement_name FROM nse_landmark_measurement";
	$sql_measurements = $GLOBALS['dbCon']->prepare($statement);
	$sql_measurements->execute();
	$sql_measurements->store_result();
	$sql_measurements->bind_result($measurement_id, $measurement_name);
	while ($sql_measurements->fetch()) {
		$measurements[$measurement_id] = $measurement_name;
	}
	$sql_measurements->free_result();
	$sql_measurements->close();

	//prepare statement - establishment poi
	$statement = "SELECT b.landmark_id, b.landmark_name, a.landmark_distance, a.landmark_measurement
					FROM nse_establishment_landmarks as a
					JOIN nse_landmark as b ON a.landmark_id=b.landmark_id
					WHERE b.landmark_type_id=? && a.establishment_code=?";
	$sql_landmarks = $GLOBALS['dbCon']->prepare($statement);
		
	//Get landmarks
	$statement = "SELECT landmark_type_id, landmark_type_name FROM nse_landmark_types ORDER BY display_order";
	$sql_landmark_types = $GLOBALS['dbCon']->prepare($statement);
	$sql_landmark_types->execute();
	$sql_landmark_types->store_result();
	$sql_landmark_types->bind_result($landmark_type_id, $landmark_type_name);
	while($sql_landmark_types->fetch()) {
		$list_count = 1;
		$landmark_list .= "<TR><TD colspan=10 class=field_label>$landmark_type_name</TD></TR>";

		$sql_landmarks->bind_param('ss', $landmark_type_id, $establishment_code);
		$sql_landmarks->execute();
		$sql_landmarks->store_result();
		$sql_landmarks->bind_result($landmark_id, $landmark_name, $landmark_distance, $landmark_measurement);
	while($sql_landmarks->fetch()) {
			$landmark_list .= "<tr>";
			$landmark_list .= "<td width=10px>&nbsp;</td>";
			$landmark_list .= "<td width=10px >$list_count. </td>";
			$landmark_list .= "<td>";
			$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='$landmark_name' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
			$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='$landmark_id' />";
			$landmark_list .= "</td>";
			$landmark_list .= "<td>&nbsp;</td>";
			$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='$landmark_distance' onChange=set_change('poi','poi',this.name) /></td>";
			$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
			foreach ($measurements as $k=>$v) {
				if ($k == $landmark_measurement) {
					$landmark_list .= "<option value='$k' selected>$v</option>";
				} else {
					$landmark_list .= "<option value='$k'>$v</option>";
				}
			}
			$landmark_list .= "</select></td>";
			$landmark_list .= "<td><iframe class=poi_dym id={$landmark_type_id}_iframe_{$list_count}></iframe></td>";
			$landmark_list .= "</tr>";
			$list_count++;
		}
		while ($list_count < 4) {
			$landmark_list .= "<tr>";
			$landmark_list .= "<td width=10px>&nbsp;</td>";
			$landmark_list .= "<td width=10px >$list_count. </td>";
			$landmark_list .= "<td>";
			$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
			$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='' />";
			$landmark_list .= "</td>";
			$landmark_list .= "<td>&nbsp;</td>";
			$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='' onChange=set_change('poi','poi',this.name) /></td>";
			$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
			foreach ($measurements as $k=>$v) {
				$landmark_list .= "<option value='$k'>$v</option>";
			}
			$landmark_list .= "</select></td>";
			$landmark_list .= "<td><iframe class=poi_dym id={$landmark_type_id}_iframe_{$list_count}> </iframe></td>";
			$landmark_list .= "</tr>";
			$list_count++;
		}
		$landmark_list .= "<TR><TD colspan=10>&nbsp;</TD></TR>";
	}
	$sql_landmark_types->free_result();
	$sql_landmark_types->close();
	$sql_landmarks->free_result();
	$sql_landmarks->close();
		
	//Get Pricing Data
	$statement = "SELECT price_description, category_prefix, category, category_suffix, single_category, single_prefix, single_description, category_low, single_category_low FROM nse_establishment_pricing WHERE establishment_code=?";
	$sql_pricing = $GLOBALS['dbCon']->prepare($statement);
	$sql_pricing->bind_param('s', $establishment_code);
	$sql_pricing->execute();
	$sql_pricing->store_result();
	$sql_pricing->bind_result($pricing, $category_prefix, $category, $category_suffix, $sgl_category, $sgl_prefix, $sgl_suffix, $priceoutlow, $sgloutlo);
	$sql_pricing->fetch();
	$sql_pricing->free_result();
	$sql_pricing->close();
		
	//Get Current Facilities
	$curr_icons = array();
	$statement = "SELECT icon_id FROM nse_establishment_icon WHERE establishment_code=?";
	$sql_icons = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_icons->bind_param('s', $establishment_code);
	$sql_icons->execute();
	$sql_icons->bind_result($icon);
	$sql_icons->store_result();
	while ($sql_icons->fetch()) {
		$curr_icons[$icon] = $icon;
	}
	$sql_icons->free_result();
	$sql_icons->close();

	//Get establishment location data
	$statement = "SELECT country_id, province_id, town_id, suburb_id, gps_latitude, gps_longitude FROM nse_establishment_location WHERE establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->bind_result($location_country_id, $location_province_id, $location_town_id, $location_suburb_id, $gps_latitude, $gps_longitude);
	$sql_location->store_result();
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
		
	//Get Location Data = Country
	$statement = "SELECT country_id, country_name FROM nse_location_country_lang WHERE language_code='EN' ORDER BY country_name";
	$sql_country = $GLOBALS['dbCon']->prepare($statement);
	$sql_country->execute();
	$sql_country->bind_result($country_id, $country_name);
	$sql_country->store_result();
	$country_options .= "<option value='0'>-- Select Country --</option>";
	while ($sql_country->fetch()) {
		if ($location_country_id == $country_id) {
			$country_options .= "<option value='$country_id' selected >$country_name</option>";
		} else {
			$country_options .= "<option value='$country_id'>$country_name</option>";
		}
	}

	//Get Location Data - Province
	$statement = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=? ORDER BY province_name";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	$sql_province->bind_param('s', $location_country_id);
	$sql_province->execute();
	$sql_province->bind_result($province_id, $province_name);
	$sql_province->store_result();
	if ($sql_province->num_rows == 0) {
		$province_options = "<option>-- No Provinces For Selected Country --</option>";
		$province_options_status = 'disabled';
		$noprov = TRUE;
	} else {
		$province_options .= "<option value='0'>-- Select Province --</option>";
		while ($sql_province->fetch()) {
			if ($location_province_id == $province_id) {
				$province_options .= "<option value='$province_id' selected>$province_name</option>";
			} else {
				$province_options .= "<option value='$province_id'>$province_name</option>";
			}
		}
	}

	//Get Location Data - Towns
	if ($noprov) {
		$statement = "SELECT town_id, town_name FROM nse_location_town WHERE country_id=? ORDER BY town_name";
		$sql_towns = $GLOBALS['dbCon']->prepare($statement);
		$sql_towns->bind_param('s', $location_country_id);
	} else {
		$statement = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=? ORDER BY town_name";
		$sql_towns = $GLOBALS['dbCon']->prepare($statement);
		$sql_towns->bind_param('s', $location_province_id);
	}
	$sql_towns->execute();
	$sql_towns->bind_result($town_id, $town_name);
	$sql_towns->store_result();
	if ($sql_towns->num_rows == 0) {
		$town_options = "-- No Towns For Selected Province --";
		$town_options_status = 'disabled';
	} else {
		$town_options .= "<option value='0'>-- Select Town --</option>";
		while ($sql_towns->fetch()) {
			if (strpos($town_name, 'Aaa') !== FALSE) continue;
			if (strpos($town_name, 'Zzz') !== FALSE) continue;
			if (strpos($town_name, '***') !== FALSE) continue;
			if ($location_town_id == $town_id) {
				$town_options .= "<option value='$town_id' selected >$town_name</option>";
			} else {
				$town_options .= "<option value='$town_id' >$town_name</option>";
			}
		}
	}

	//Get Location Data - Suburbs
	$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=? ORDER BY suburb_name";
	$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
	$sql_suburb->bind_param('s', $location_town_id);
	$sql_suburb->execute();
	$sql_suburb->bind_result($suburb_id, $suburb_name);
	$sql_suburb->store_result();
	if ($sql_suburb->num_rows == 0) {
		$suburb_options = "-- No Suburbs For Selected Town --";
		$suburb_options_status = 'disabled';
	} else {
		$suburb_options .= "<option value='0'>-- Select Suburb --</option>";
		while ($sql_suburb->fetch()) {
			if ($location_suburb_id == $suburb_id) {
				$suburb_options .= "<option value='$suburb_id' selected >$suburb_name</option>";
			} else {
				$suburb_options .= "<option value='$suburb_id' >$suburb_name</option>";
			}
		}
	}
		
	//Get Directions
	$statement = "SELECT directions, map_type FROM nse_establishment_directions WHERE establishment_code=?";
	$sql_directions = $GLOBALS['dbCon']->prepare($statement);
	$sql_directions->bind_param('s', $establishment_code);
	$sql_directions->execute();
	$sql_directions->bind_result($directions, $map_type);
	$sql_directions->store_result();
	$sql_directions->fetch();
	$sql_directions->free_result();
	$sql_directions->close();
	$directions = stripslashes($directions);
	switch ($map_type) {
		case 'none':
			$map_type_none = 'checked';
			break;
		case 'google':
			$map_type_google = 'checked';
			break;
		case 'upload':
			$map_type_image = 'checked';
			break;
		default:
			$map_type_none = 'checked';
	}

	//Get Current Restype
	$counter = 0;
	$statement = "SELECT subcategory_id, star_grading FROM nse_establishment_restype WHERE establishment_code=?";
	$sql_restype_current = $GLOBALS['dbCon']->prepare($statement);
	$sql_restype_current->bind_param('s', $establishment_code);
	$sql_restype_current->execute();
	$sql_restype_current->store_result();
	$sql_restype_current->bind_result($current_restype_id, $current_star_grading);
	while ($sql_restype_current->fetch()) {
		$current_restype[$counter] = $current_restype_id;
		$current_grading[$counter] = $current_star_grading;
		$counter++;
	}
	$sql_restype_current->free_result();
	$sql_restype_current->close();
		
	//Get AA Grades List
	$grade_options .= "<option value=''>N/A</option>";
	$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category";
	$sql_aa_categories = $GLOBALS['dbCon']->prepare($statement);
	$sql_aa_categories->execute();
	$sql_aa_categories->store_result();
	$sql_aa_categories->bind_result($grade_code, $grade_name);
	while ($sql_aa_categories->fetch()) {
		if ($grade_code == $aa_grade) {
			$grade_options .= "<option value='$grade_code' selected>$grade_name</option>";
		} else {
			$grade_options .= "<option value='$grade_code'>$grade_name</option>";
		}
	}
	$sql_aa_categories->free_result();
	$sql_aa_categories->close();


	$form_heading = "Details for $establishment_name ($establishment_code)";
	
	
	//Get restypes list
	$statement = "SELECT toplevel_id, toplevel_name FROM nse_restype_toplevel ORDER BY toplevel_id";
	$sql_restype_toplevel = $GLOBALS['dbCon']->prepare($statement);
		
	$statement = "SELECT DISTINCT(a.subcategory_name), a.subcategory_id
					FROM nse_restype_subcategory_lang AS a
					JOIN nse_restype_subcategory_parent AS b ON a.subcategory_id=b.subcategory_id
					JOIN nse_restype_category AS c ON c.category_id=b.category_id
					WHERE c.toplevel_id=?
					ORDER BY a.subcategory_name";
	$sql_restypes = $GLOBALS['dbCon']->prepare($statement);
	
	$sql_restype_toplevel->execute();
	$sql_restype_toplevel->store_result();
	$sql_restype_toplevel->bind_result($toplevel_id, $toplevel_name);
	while ($sql_restype_toplevel->fetch()) {
		$restype_options_0 .= "<optgroup label='$toplevel_name' >";
		$restype_options_1 .= "<optgroup label='$toplevel_name' >";
		$restype_options_2 .= "<optgroup label='$toplevel_name' >";
			
		$sql_restypes->bind_param('s', $toplevel_id);
		$sql_restypes->execute();
		$sql_restypes->store_result();
		$sql_restypes->bind_result($subcategory_name, $subcategory_id);
		while ($sql_restypes->fetch()) {
			if (isset($current_restype[0]) && $subcategory_id == $current_restype[0]) {
				$restype_options_0 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_0 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}

			if (isset($current_restype[1]) && $subcategory_id == $current_restype[1]) {
				$restype_options_1 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_1 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
			if (isset($current_restype[2]) && $subcategory_id == $current_restype[2]) {
				$restype_options_2 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_2 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
		}
		$restype_options_0 .= "</optgroup>";
		$restype_options_1 .= "</optgroup>";
		$restype_options_2 .= "</optgroup>";
	}
	$sql_restype_toplevel->free_result();
	$sql_restype_toplevel->close();
	$sql_restypes->free_result();
	$sql_restypes->close();
	
	//Get stargradings list
	for ($x=0; $x<6; $x++) {
		if (isset($current_grading[0]) && $x == $current_grading[0]) {
			$star_grading_options_0 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_0 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[1]) && $x == $current_grading[1]) {
			$star_grading_options_1 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_1 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[2]) && $x == $current_grading[2]) {
			$star_grading_options_2 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_2 .= "<option value=$x>$x</option>";
		}
	}

	//Get Room count
	$statement = 'SELECT room_count, room_type FROM nse_establishment_data WHERE establishment_code=?';
	$sql_rooms = $GLOBALS['dbCon']->prepare($statement);
	$sql_rooms->bind_param('s', $establishment_code);
	$sql_rooms->execute();
	$sql_rooms->store_result();
	$sql_rooms->bind_result($room_count, $room_type);
	$sql_rooms->fetch();
	$sql_rooms->free_result();
	$sql_rooms->close();
	
	//Assemble room type
	$room_options = '';
	$room_types = array('Rooms', 'Units', 'Apartments','Chalets', 'Cottages');
	foreach ($room_types AS $type) {
		if ($type == $room_type) {
			$room_options .= "<option value='$type' selected >$type</options>";
		} else {
			$room_options .= "<option value='$type'>$type</options>";
		}
	}
	
	
	//Get Facilities
	$facilities = "";
	$lRowsCount = 0;
	$rRowsCount = 0;
	$colLength = 3;
	$lRow = '<table width=100% cellpadding=0 cellspacing=0>';
	$rRow = '<table width=100%  cellpadding=0 cellspacing=0>';
	
	$statement = "SELECT a.icon_id, a.icon_url, b.icon_description
					FROM nse_icon AS a
					JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
					WHERE a.icon_category_id=?";
	$sql_facility_data = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "SELECT b.icon_name, a.icon_category_id
					FROM nse_icon_category AS a
					JOIN nse_icon_category_lang AS b ON a.icon_category_id=b.icon_category_id
					ORDER BY a.icon_priority ";
	$sql_facilities = $GLOBALS['dbCon']->prepare($statement);
	$sql_facilities->execute();
	$sql_facilities->bind_result($icon_category_name, $icon_category_id);
	$sql_facilities->store_result();
	while ($sql_facilities->fetch()) {
		if ($icon_category_name == 'Affiliations') continue;
		$col = 0;
		$sql_facility_data->bind_param('s', $icon_category_id);
		$sql_facility_data->execute();
		$sql_facility_data->bind_result($icon_id, $icon_src, $icon_description);
		$sql_facility_data->store_result();
		//if ($lRowsCount < $rRowsCount) {
			$lRow .= "<tr><td colspan=10 style='padding: 20px 0px 5px 0px'><b>$icon_category_name</b></td></tr>";
			while ($sql_facility_data->fetch()) {
				//$src_off = str_replace('.gif', '_off.gif', $icon_src);
				if ($col == 0) {
					$lRow .= "<tr valign=top>";
					$lRowsCount++;
				}
				$lRow .= "<td><input type=checkbox name='facilities[]' id='facilities_$icon_id' value='$icon_id' onChange=set_change('facility','facility',this.id) ";
				if (isset($curr_icons[$icon_id])) {
					$lRow .= 'checked';
				}
				$lRow .= " ></td><td><label for='facilities_$icon_id'><img src='$icon_src' width=33 height=35 style='margin: 1px'></label></td><td style='font-size: 12px'><label for='facilities_$icon_id'>$icon_description</label></td>";
				$col++;
				if ($col == $colLength) {
					$lRow .= "</tr>";
					$col = 0;
				} else {
					$lRow .= "<td>&nbsp;</td>";
				}
			}
			if ($col < $colLength) $lRow .= "";
	}
	$sql_facilities->free_result();
	$sql_facilities->close();
	
	$lRow .= '</table>';
	$rRow .= '</table>';
	$facilities .= "<table width=100% ><tr valign=top>";
	$facilities .= "<td>$lRow</td><td>&nbsp;&nbsp;&nbsp;</td><td>$rRow</td>";
	$facilities.= "</tr></table>";
	
	//Get invoices
	$statement = "SELECT invoice_id, line_date, line_item, line_cost, line_paid FROM nse_establishment_invoice WHERE establishment_code=? ORDER BY line_date DESC";
	$sql_invoice = $GLOBALS['dbCon']->prepare($statement);
	$sql_invoice->bind_param('s', $establishment_code);
	$sql_invoice->execute();
	$sql_invoice->store_result();
	$sql_invoice->bind_result($invoice_id, $invoice_date, $invoice_item, $invoice_value, $invoice_paid);
	while ($sql_invoice->fetch()) {
		$invoice_list .= "$invoice_id|$invoice_date|$invoice_item|$invoice_value|$invoice_paid|0!|!";
	}
	$sql_invoice->free_result();
	$sql_invoice->close();

	//Get notes
	$notes = "<table id=notes_list >";
	$statement = "SELECT user_name, note_timestamp, note_content FROM nse_establishment_notes WHERE establishment_code=? ORDER BY note_timestamp DESC";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);
	$sql_notes->bind_param('s', $establishment_code);
	$sql_notes->execute();
	$sql_notes->store_result();
	$sql_notes->bind_result($user_name, $note_timestamp, $note_content);
	while ($sql_notes->fetch()) {
		$date = date('d-m-Y h:i',$note_timestamp);
		$notes .= "<tr>";
		$notes .= "<td class=note_name>$user_name</td>";
		$notes .= "<td class=note_date>$date</td>";
		$notes .= "<td class=note_content colspan=2>$note_content</td>";
		$notes .= "</tr>";
	}
	$sql_notes->free_result();
	$sql_notes->close();
	$notes .= "</table>";

	//Replace Tags
	$template = str_replace('<!-- form_heading -->', $form_heading, $template);
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_Replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- billing1 -->', $billing_line1, $template);
	$template = str_replace('<!-- billing2 -->', $billing_line2, $template);
	$template = str_replace('<!-- billing3 -->', $billing_line3, $template);
	$template = str_replace('<!-- billing_code -->', $billing_code, $template);
	$template = str_replace('<!-- vat_number -->', $vat_number, $template);
	$template = str_replace('<!-- company_number -->', $company_number, $template);
	$template = str_replace('<!-- company_name -->', $company_name, $template);
	$template = str_replace('<!-- contacts_list -->', $contacts_list, $template);
	$template = str_replace('<!-- tel1 -->', $contact_tel1, $template);
	$template = str_replace('<!-- tel2 -->', $contact_tel2, $template);
	$template = str_replace('<!-- fax1 -->', $contact_fax1, $template);
	$template = str_replace('<!-- fax2 -->', $contact_fax2, $template);
	$template = str_replace('<!-- cell1 -->', $contact_cell1, $template);
	$template = str_replace('<!-- cell2 -->', $contact_cell2, $template);
	$template = str_replace('<!-- tel1_description -->', $contact_tel1_description, $template);
	$template = str_replace('<!-- tel2_description -->', $contact_tel2_description, $template);
	$template = str_replace('<!-- fax1_description -->', $contact_fax1_description, $template);
	$template = str_replace('<!-- fax2_description -->', $contact_fax2_description, $template);
	$template = str_replace('<!-- cell1_description -->', $contact_cell1_description, $template);
	$template = str_replace('<!-- cell2_description -->', $contact_cell2_description, $template);
	$template = str_replace('<!-- email -->', $contact_pemail, $template);
	$template = str_replace('<!-- email2 -->', $contact_pemail2, $template);
	$template = str_replace('<!-- email3 -->', $contact_pemail3, $template);
	$template = str_replace('<!-- postal1 -->', $postal_line1, $template);
	$template = str_replace('<!-- postal2 -->', $postal_line2, $template);
	$template = str_replace('<!-- postal3 -->', $postal_line3, $template);
	$template = str_replace('<!-- postal_code -->', $postal_code, $template);
	$template = str_replace('<!-- street1 -->', $street_line1, $template);
	$template = str_replace('<!-- street2 -->', $street_line2, $template);
	$template = str_replace('<!-- street3 -->', $street_line3, $template);
	$template = str_replace('<!-- image_list -->', $image_list, $template);
	$template = str_replace('<!-- short_description -->', $short_description, $template);
	$template = str_replace('<!-- long_description -->', $long_description, $template);
	$template = str_replace('<!-- facilities -->', $facilities, $template);
	$template = str_replace('<!-- province_options -->', $province_options, $template);
	$template = str_replace('<!-- country_options -->', $country_options, $template);
	$template = str_replace('<!-- province_options_status -->', $province_options_status, $template);
	$template = str_replace('<!-- town_options -->', $town_options, $template);
	$template = str_replace('<!-- town_options_status -->', $town_options_status, $template);
	$template = str_replace('<!-- suburb_options -->', $suburb_options, $template);
	$template = str_replace('<!-- suburb_options_status -->', $suburb_options_status, $template);
	$template = str_replace('<!-- dd_lat -->', $gps_latitude, $template);
	$template = str_replace('<!-- dd_lon -->', $gps_longitude, $template);
	$template = str_replace('<!-- pricing -->', $pricing, $template);
	$template = str_replace('<!-- child -->', $child_policy, $template);
	$template = str_replace('<!-- cancellation -->', $cancellation_policy, $template);
	$template = str_replace('<!-- map_type_none -->', $map_type_none, $template);
	$template = str_replace('<!-- map_type_google -->', $map_type_google, $template);
	$template = str_replace('<!-- map_type_image -->', $map_type_image, $template);
	$template = str_replace('<!-- directions -->', $directions, $template);
	$template = str_replace('<!-- price_options -->', $price_options, $template);
	$template = str_replace('<!-- publication_list -->', $publication_list, $template);
	$template = str_replace('<!-- awards_list -->', $awards_list, $template);
	$template = str_replace('<!-- publications -->', '', $template);
	$template = str_replace('<!-- invoice_list -->', $invoice_list, $template);
	$template = str_replace('!pastel_code!', $pastel_code, $template);
	$template = str_replace('!room_count!', $room_count, $template);
	$template = str_replace('!inactive_estab!', $inactive_estab, $template);
	$template = str_replace('<!-- assessor_list -->', $assessor_list, $template);
	
	$template = str_replace('<!-- sgl_options -->', $sgl_options, $template);
	$template = str_replace('<!-- price_low_options -->', $price_low_options, $template);
	$template = str_replace('<!-- sgl_low_options -->', $sgl_low_options, $template);
	
	$template = str_replace('<!-- price_prefix -->', $prefix_options, $template);
	$template = str_replace('<!-- category_suffix -->', $category_suffix, $template);
	$template = str_replace('<!-- sgl_suffix -->', $sgl_suffix, $template);
	$template = str_replace('<!-- sgl_prefix -->', $sgl_prefix_options, $template);
	$template = str_replace('<!-- restype_0 -->', $restype_options_0, $template);
	$template = str_replace('<!-- restype_1 -->', $restype_options_1, $template);
	$template = str_replace('<!-- restype_2 -->', $restype_options_2, $template);
	$template = str_replace('<!-- star_grading_0 -->', $star_grading_options_0, $template);
	$template = str_replace('<!-- star_grading_1 -->', $star_grading_options_1, $template);
	$template = str_replace('<!-- star_grading_2 -->', $star_grading_options_2, $template);
	$template = str_replace('<!-- poi_list -->', $landmark_list, $template);
	$template = str_Replace('<!-- grade_options -->', $grade_options, $template);
	$template = str_Replace('<!-- nightsbridge -->', $nightsbridge_id, $template);
	$template = str_Replace('<!-- prebook -->', $prebook_id, $template);
	$template = str_replace('<!-- advertiser_options -->', $advertiser_options, $template);
	$template = str_replace('<!-- website -->', $website_url, $template);
	$template = str_replace('<!-- change_list -->', $change_list, $template);
	$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- room_options -->', $room_options, $template);
	$template = str_replace('<!-- notes -->', $notes, $template);
	
	$template = str_replace('<!-- api_key -->', $GLOBALS['api_key'], $template);
	$template = str_replace('<!-- default_image -->', $default_image, $template);
	$template = str_replace('<!-- button_name -->', 'updateData', $template);
	$template = str_replace('#$%', "\n", $template);

	echo $template;
}


function establishment_edit_save() {
	//Vars
	$establishment_code = isset($_GET['code'])?$_GET['code']:$_POST['establishment_code'];
	$aa_estab_count = '';
	$vat_number = $_POST['vat_number'];
	$billing_line1 = count($_POST['billing_line1'])<100?$_POST['billing_line1']:'';
	$billing_line2 = count($_POST['billing_line2'])<100?$_POST['billing_line2']:'';
	$billing_line3 = count($_POST['billing_line3'])<100?$_POST['billing_line3']:'';
	$billing_code = count($_POST['billing_code'])<100?$_POST['billing_code']:'';
	$establishment_name = $_POST['establishment_name'];
	$contacts_list = $_POST['contacts'];
	$contact_tel1 = $_POST['tel1'];
	$contact_tel2 = $_POST['tel2'];
	$contact_fax1 = $_POST['fax1'];
	$contact_fax2 = $_POST['fax2'];
	$contact_cell1 = $_POST['cell1'];
	$contact_cell2 = $_POST['cell2'];
	$contact_email = $_POST['email'];
	$contact_email2 = $_POST['email2'];
	$contact_email3 = $_POST['email3'];
	$contact_tel1_description = $_POST['tel1_description'];
	$contact_tel2_description = $_POST['tel2_description'];
	$contact_fax1_description = $_POST['fax1_description'];
	$contact_fax2_description = $_POST['fax2_description'];
	$contact_cell1_description = $_POST['cell1_description'];
	$contact_cell2_description = $_POST['cell2_description'];
	$postal_line1 = $_POST['postal1'];
	$postal_line2 = $_POST['postal2'];
	$postal_line3 = $_POST['postal3'];
	$postal_code = $_POST['postal_code'];
	$street_line1 = $_POST['street1'];
	$street_line2 = $_POST['street2'];
	$street_line3 = $_POST['street3'];
	$image_list = isset($_POST['images'])?$_POST['images']:'';
	$new_country = isset($_POST['country_id']) && ctype_digit($_POST['country_id'])?$_POST['country_id']:'';
	$new_province = isset($_POST['province_id']) && ctype_digit($_POST['province_id'])?$_POST['province_id']:'';
	$new_town = isset($_POST['town_id']) && ctype_digit($_POST['town_id'])?$_POST['town_id']:'';
	$new_suburb = isset($_POST['suburb_id']) && ctype_digit($_POST['suburb_id'])?$_POST['suburb_id']:'';
	$current_country = '';
	$current_province = '';
	$current_town = '';
	$current_suburb = '';
	$location_change = FALSE;
	$gps_latitude = $_POST['dd_lat'];
	$gps_longitude = $_POST['dd_lon'];
	$cancellation_policy = $_POST['cancellation_policy'];
	$child_policy = $_POST['child_policy'];
	$directions = $_POST['directions'];
	$map_type = $_POST['map_type'];
	$delete_images = array();
	$update_images = array();
	$curr_images = array();
	$curr_image_id = '';
	$curr_image_name = '';
	$curr_thumb_name = '';
	$pricing = $_POST['pricing'];
	$description['short_description'] = $_POST['short_description'];
	$description['long_description'] = $_POST['long_description'];
	$company_name = $_POST['company_name'];
	$company_number = $_POST['company_number'];
	$nightsbridge_code = $_POST['nightsbridge'];
	$prebook_code = $_POST['prebook'];
	$aa_grade = $_POST['aa_grade'];
	$website_url = $_POST['website'];
	$landmark_type_id = '';
	$aa_estab = '';	
	$estab_user_email = '';
	$publications_list = $_POST['publications'];
	$publication_counter = '';
	$publication_id = '';
	$old_establishment_name = '';
	$name_change = false;
	$active_estab = isset($_POST['inactive_estab'])?'':'1';
	$room_count = $_POST['room_count'];
	$room_type = $_POST['room_type'];
	$notes = $_POST['notes'];

	$search_hash = '';
	$search_hashes = array();

	$tc = new tcrypt();

	//Remove funny characters
	$search_string = strtolower($establishment_name);
	$search_string = plainize_name($search_string);

	//split into single words
	$words = explode(' ', $search_string);

	//calculate hash
	foreach ($words as $word) {
		$words2 = explode('-', $word);
		foreach ($words2 as $word2) {
			$search_hashes[] = sha1(trim($word2));
		}
		$search_hashes[] = sha1(trim($word));
		$word = str_replace("'", '', $word);
		$search_hashes[] = sha1(trim($word));
		$word = str_replace("-", '', $word);
		$search_hashes[] = sha1(trim($word));
		$word = str_replace("-", ' ', $word);
	}

	array_unique($search_hashes);

	//Glue back together
	$search_hash = implode(',', $search_hashes);

	//Get Template
	$template = file_get_contents(SITE_ROOT . "/modules/assessor/html/update_complete.html");

	//Check changes
	$changes = explode(',', $_POST['change_list']);

	//Update Billing Info
	if (in_array('billing', $changes)) {
		$statement = "UPDATE nse_establishment_billing SET billing_line1=?, billing_line2=?, billing_line3=?, billing_code=?, vat_number=?, company_number=?, company_name=? WHERE establishment_code=?";
		$sql_billing = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_billing->bind_param('ssssssss', $billing_line1, $billing_line2, $billing_line3, $billing_code, $vat_number,  $company_number, $company_name, $establishment_code);
		$sql_billing->execute();
		$sql_billing->close();
	}

	//Delete current Contacts
	$statement = "DELETE FROM nse_establishment_contacts WHERE establishment_code=?";
	$sql_contacts_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_contacts_delete->bind_param('s', $establishment_code);
	$sql_contacts_delete->execute();
	$sql_contacts_delete->close();

	//Prepare statement - check if user has admin access
	$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
	$sql_admin_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare Statement - Add new user
	$statement = "INSERT INTO nse_user (firstname, lastname, email, phone, cell, new_password, top_level_type, user_type_id) VALUES (?,?,?,?,?,?,'c','2')";
	$sql_admin_insert = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Update user
	$statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=? WHERE user_id=?";
	$sql_admin_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Check establishment access
	$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=?";
	$sql_user_access = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Get user id
	$statement = "SELECT user_id FROM nse_user WHERE email=?";
	$sql_admin_id = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Delete admin user
	$statement = "DELETE FROM nse_user WHERE user_id=?";
	$sql_admin_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Delete user/establishment link
	$statement = "DELETE FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
	$sql_estab_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Add user/establishment link
	$statement = "INSERT INTO nse_user_establishments (user_id, establishment_code) VALUES (?,?)";
	$sql_estab_add = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - check user/establishment link
	$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
	$sql_estab_check = $GLOBALS['dbCon']->prepare($statement);

	//Update Contacts
	$statement = "INSERT INTO nse_establishment_contacts (establishment_code, contact_name, contact_designation, contact_tel, contact_cell, contact_email) VALUES (?,?,?,?,?,?)";
	$sql_contacts_insert = $GLOBALS['dbCon']->prepare($statement);
	$contact_line = explode("!|!", $contacts_list);

	foreach ($contact_line as $line) {
		if (empty($line)) continue;
		list($line_id, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $admin_user, $send_password, $old_email, $delete_tag) = explode('|', $line);
		if ($delete_tag == 0) {
			$sql_contacts_insert->bind_param('ssssss', $establishment_code, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
			$sql_contacts_insert->execute();
		}

		//Extract firstname & lastname
		$names = explode(' ', $contact_name);
		if (count($names) == 2) {
			$firstname = $names[0];
			$lastname = $names[1];
		} else {
			$firstname = $contact_name;
			$lastname = '';
		}
		//echo "<p style='color: navy'>$line<br>Email: $old_email<br>Admin User: $admin_user</p>";
		if ($delete_tag == 1 || $admin_user == 0) {
			//echo "DELETE<br>=======<br />Email: $old_email<br>";
			$admin_user_id = 0;
			$sql_admin_id->bind_param('s', $old_email);
			$sql_admin_id->execute();
			$sql_admin_id->store_result();
			$sql_admin_id->bind_result($admin_user_id);
			$sql_admin_id->fetch();
			$sql_admin_id->free_result();

			if ($admin_user_id != 0) {
				$sql_user_access->bind_param('i', $admin_user_id);
				$sql_user_access->execute();
				$sql_user_access->store_result();
				$sql_user_access->bind_result($estab_count);
				$sql_user_access->fetch();
				$sql_user_access->free_result();

				if ($estab_count < 2) {
					//echo "Deleting Admin User<br>";
					$sql_admin_delete->bind_param('i', $admin_user_id);
					$sql_admin_delete->execute();
				}

				//echo "Deleting estab link<p />";
				$sql_estab_delete->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_delete->execute();
			}
		} else if ($admin_user == 1) {
			if ($old_email == 'new') {
				//echo "ADDING USER<br>=======<br />Email: $old_email<br />";
				//Check if user exists
				$admin_user_id = 0;
				$sql_admin_id->bind_param('s', $contact_email);
				$sql_admin_id->execute();
				$sql_admin_id->store_result();
				$sql_admin_id->bind_result($admin_user_id);
				$sql_admin_id->fetch();
				$sql_admin_id->free_result();
				//echo "User ID: $admin_user_id<br>";

				if ($admin_user_id == 0) {
					//Get Password
					$password = $GLOBALS['security']->generate_password(6);
					$password = $tc->encrypt($password);

					//Save user
					//echo "Inserting user<br>";
					$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
					$sql_admin_insert->execute();
					$admin_user_id = $sql_admin_insert->insert_id;
				}

				//Check if user/establishment link exists
				$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_check->execute();
				$sql_estab_check->store_result();
				$sql_estab_check->bind_result($estab_count);
				$sql_estab_check->fetch();
				$sql_estab_check->free_result();

				//Attach establishment
				if ($estab_count == 0) {
					//echo "Inserting estab link<p />";
					$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
					$sql_estab_add->execute();
				}


			} else {
				//echo "UPDATING USER<br>=======<br />Email: $old_email<br />";
				$admin_user_id = 0;
				$sql_admin_id->bind_param('s', $old_email);
				$sql_admin_id->execute();
				$sql_admin_id->store_result();
				$sql_admin_id->bind_result($admin_user_id);
				$sql_admin_id->fetch();
				$sql_admin_id->free_result();
				//echo "User ID: $admin_user_id<br>";

				if ($admin_user_id == 0) {
					//echo "Adding user<br>";
					$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
					$sql_admin_insert->execute();
					$admin_user_id = $sql_admin_insert->insert_id;
				} else {
					//echo "Updating user <br>";
					$sql_admin_update->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $admin_user_id);
					$sql_admin_update->execute();
				}

				//Check if estab exists
				$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_check->execute();
				$sql_estab_check->store_result();
				$sql_estab_check->bind_result($estab_count);
				$sql_estab_check->fetch();
				$sql_estab_check->free_result();

				//Attach establishment
				if ($estab_count == 0) {
					//echo "Adding estab link<p />";
					$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
					$sql_estab_add->execute();
				}
			}
		}
	}
	$sql_contacts_insert->close();
	$sql_admin_check->close();
	$sql_admin_update->close();
	$sql_admin_insert->close();

	//Update Public Contact Details
	if (in_array('contact', $changes)) {
		$statement = "UPDATE nse_establishment SET postal_address_line1=?, postal_address_line2=?, postal_address_line3=?, postal_address_code=?, street_address_line1=?, street_address_line2=?, street_address_line3=? WHERE establishment_code=?";
		$sql_postal = $GLOBALS['dbCon']->prepare($statement);
		$sql_postal->bind_param('ssssssss', $postal_line1, $postal_line2, $postal_line3, $postal_code, $street_line1, $street_line2, $street_line3, $establishment_code);
		$sql_postal->execute();
		$sql_postal->close();

		//Delete current public contact data
		$statement = "DELETE FROM nse_establishment_public_contact WHERE establishment_code=?";
		$sql_contact_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_contact_delete->bind_param('s', $establishment_code);
		$sql_contact_delete->execute();

		//Insert new contacts
		$statement = "INSERT INTO nse_establishment_public_contact (establishment_code, contact_type, contact_value, contact_description) VALUES (?,?,?,?)";
		$sql_contact_insert = $GLOBALS['dbCon']->prepare($statement);

		if (!empty($contact_tel1)) {
			$contact_type = 'tel1';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_tel1, $contact_tel1_description);
			$sql_contact_insert->execute();
		}

		if (!empty($contact_tel2)) {
			$contact_type = 'tel2';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_tel2, $contact_tel2_description);
			$sql_contact_insert->execute();
		}

		if (!empty($contact_fax1)) {
			$contact_type = 'fax1';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_fax1, $contact_fax1_description);
			$sql_contact_insert->execute();
		}

		if (!empty($contact_fax2)) {
			$contact_type = 'fax2';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_fax2, $contact_fax2_description);
			$sql_contact_insert->execute();
		}

		if (!empty($contact_cell1)) {
			$contact_type = 'cell1';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_cell1, $contact_cell1_description);
			$sql_contact_insert->execute();
		}

		if (!empty($contact_cell2)) {
			$contact_type = 'cell2';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_cell2, $contact_cell2_description);
			$sql_contact_insert->execute();
		}

		if (!empty($contact_email)) {
			$contact_type = 'email';
			$contact_email_description = '';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_email, $contact_email_description);
			$sql_contact_insert->execute();
		}
	if (!empty($contact_email2)) {
			$contact_type = 'email2';
			$contact_email_description = '';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_email2, $contact_email_description);
			$sql_contact_insert->execute();
		}
	if (!empty($contact_email3)) {
			$contact_type = 'email3';
			$contact_email_description = '';
			$sql_contact_insert->bind_param('ssss', $establishment_code, $contact_type, $contact_email3, $contact_email_description);
			$sql_contact_insert->execute();
		}

		$sql_contact_insert->close();
	}

	// Images
	if (in_array('image', $changes)) {
		$statement = "DELETE FROM nse_establishment_images WHERE image_id=?";
		$sql_image_delete = $GLOBALS['dbCon']->prepare($statement);

		$statement = "INSERT INTO nse_establishment_images (establishment_code, image_name, thumb_name, image_title, image_description) VALUES (?,?,?,?,?)";
		$sql_image_insert = $GLOBALS['dbCon']->prepare($statement);

		$statement = "UPDATE nse_establishment_images SET image_name=?, thumb_name=?, image_title=?, image_description=? WHERE image_id=?";
		$sql_image_update = $GLOBALS['dbCon']->prepare($statement);

		//Get current image list
		$statement = "SELECT image_id, image_name, thumb_name FROM nse_establishment_images WHERE establishment_code=?";
		$sql_image_list = $GLOBALS['dbCon']->prepare($statement);
		$sql_image_list->bind_param('s', $establishment_code);
		$sql_image_list->execute();
		$sql_image_list->store_result();
		$sql_image_list->bind_result($curr_image_id, $curr_image_name, $curr_thumb_name);
		while($sql_image_list->fetch()) {
			$curr_images[$curr_image_id]['image'] = $curr_image_name;
			$curr_images[$curr_image_id]['thumb'] = $curr_thumb_name;
		}
		$sql_image_list->free_result();
		$sql_image_list->close();

		$image_line = explode('!|!', $image_list);
		foreach($image_line as $line){
			if (empty($line)) continue;
			$primary_image = isset($_POST['primary_image'])?$_POST['primary_image']:'';
			list($image_id, $image_location, $image_title, $image_description, $process) = explode('|', $line);
			$thumb_name = substr($image_location, strrpos($image_location, '/')+1);
			$image_name = substr($thumb_name, 3);
			switch ($process) {
				case 1:
					if (substr($image_id, 0, 3) == 'new') {
						$update_images[$image_name] = $image_name;
						$update_images[$thumb_name] = $thumb_name;
						$sql_image_insert->bind_param('sssss',$establishment_code, $image_name, $thumb_name, $image_title, $image_description);
						$sql_image_insert->execute();
					} else {
						$update_images[$image_name] = $image_name;
						$update_images[$thumb_name] = $thumb_name;
						$sql_image_update->bind_param('sssss',$image_name, $thumb_name, $image_title, $image_description, $image_id);
						$sql_image_update->execute();
					}
					if ($_POST['primary_image'] == $image_id) $primary_image = $image_id;
					break;
				case 2:
					$update_images[$image_name] = $image_name;
					$update_images[$thumb_name] = $thumb_name;
					$sql_image_insert->bind_param('sssss',$establishment_code, $image_name, $thumb_name, $image_title, $image_description);
					$sql_image_insert->execute();
					if ($_POST['primary_image'] == $image_id) $primary_image = $sql_image_insert->insert_id;
					break;
				case 3:
					$delete_images[] = $curr_images[$image_id]['image'];
					$delete_images[] = $curr_images[$image_id]['thumb'];
					$sql_image_delete->bind_param('s', $image_id);
					$sql_image_delete->execute();
					break;
			}


			//Update Primary Image
			$statement = "UPDATE nse_establishment_images SET primary_image='' WHERE establishment_code=?";
			$sql_primary_reset = $GLOBALS['dbCon']->prepare($statement);
			$sql_primary_reset->bind_param('s', $establishment_code);
			$sql_primary_reset->execute();
			$sql_primary_reset->close();


			$statement = "UPDATE nse_establishment_images SET primary_image='1' where image_id=?";
			$sql_primary_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_primary_update->bind_param('s', $primary_image);
			$sql_primary_update->execute();


		}
		//Upload images
		foreach ($GLOBALS['ftp'] as $server=>$data) {
			$ftp_id = ftp_connect($server);
			ftp_login($ftp_id, $data['username'], $data['password']);
			ftp_pasv($ftp_id, true);
			ftp_chdir($ftp_id, $data['root'] . "res_images/");
			foreach ($delete_images as $image) {
				@ftp_delete($ftp_id, $image);
			}
			foreach ($update_images as $local_image=>$image) {
				ftp_put($ftp_id, $image, SITE_ROOT . "/temp_images/$local_image", FTP_BINARY);
			}

			ftp_close($ftp_id);
		}

	}

	//Get Current Location Data
	$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=?";
	$sql_current_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_current_location->bind_param('s', $establishment_code);
	$sql_current_location->execute();
	$sql_current_location->bind_result($current_country, $current_province, $current_town, $current_suburb);
	$sql_current_location->store_result();
	$sql_current_location->fetch();
	$sql_current_location->free_result();
	$sql_current_location->close();

	//Update Location Data - Country
	if ($current_country != $new_country) {
		$location_change = TRUE;
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='country' && (current_id=? || current_id=?)";
		$sql_country_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_country_update->bind_param('ss', $current_country, $new_country);
		$sql_country_update->execute();
		$sql_country_update->close();

		//$sql_country_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_country_update->bind_param('ss', $current_country, $new_country);
		//$sql_country_update->execute();
		//$sql_country_update->close();
	}	

	//Update Location Data - Province
	if ($current_province != $new_province) {
		$location_change = TRUE;
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='province' && (current_id=? || current_id=?)";
		$sql_province_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_province_update->bind_param('ss', $current_province, $new_province);
		$sql_province_update->execute();
		$sql_province_update->close();

		//$sql_province_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_province_update->bind_param('ss', $current_province, $new_province);
		//$sql_province_update->execute();
		//$sql_province_update->close();
	}

	//Update Location Data - Town
	if ($current_town != $new_town) {
		$location_change = TRUE;
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='town' && (current_id=? || current_id=?)";
		$sql_town_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_town_update->bind_param('ss', $current_town, $new_town);
		$sql_town_update->execute();
		$sql_town_update->close();

		//$sql_town_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_town_update->bind_param('ss', $current_town, $new_town);
		//$sql_town_update->execute();
		//$sql_town_update->close();
		
	}

	//Update Location Data - Suburb
	if ($current_suburb != $new_suburb) {
		$location_change = TRUE;
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='suburb' && (current_id=? || current_id=?)";
		$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb_update->bind_param('ss', $current_suburb, $new_suburb);
		$sql_suburb_update->execute();
		$sql_suburb_update->clnse_exportose();

		//$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_suburb_update->bind_param('ss', $current_suburb, $new_suburb);
		//$sql_suburb_update->execute();
		//$sql_suburb_update->close();
		
	}

	if ($location_change || in_array('gps',$changes)) {
		$statement = "UPDATE nse_establishment_location SET gps_latitude=?, gps_longitude=?, country_id=?, province_id=?, town_id=?, suburb_id=? WHERE establishment_code=?";
		$sql_location_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_location_update->bind_param('sssssss', $gps_latitude, $gps_longitude, $new_country, $new_province, $new_town, $new_suburb, $establishment_code);
		$sql_location_update->execute();
	}

	//Update Publications
	$statement = "SELECT publication_id FROM nse_establishment_publications WHERE establishment_code=?";
	$sql_publication_list = $GLOBALS['dbCon']->prepare($statement);
	$sql_publication_list->bind_param('s', $establishment_code);
	$sql_publication_list->execute();
	$sql_publication_list->store_result();
	$sql_publication_list->bind_result($publication_id);
	while ($sql_publication_list->fetch()) {
		$publication_current[$publication_id] = '';
	}
	$sql_publication_list->free_result();
	$sql_publication_list->close();

	$statement = "SELECT COUNT(*) FROM nse_establishment_publications WHERE establishment_code=? && publication_id=?";
	$sql_publication_check = $GLOBALS['dbCon']->prepare($statement);

	$statement = "INSERT INTO nse_establishment_publications (establishment_code, publication_id, add_date) VALUES (?,?, NOW())";
	$sql_publication_insert = $GLOBALS['dbCon']->prepare($statement);

	$statement = "DELETE FROM nse_establishment_publications WHERE establishment_code=? && publication_id=?";
	$sql_publication_delete = $GLOBALS['dbCon']->prepare($statement);

	$publication_ids = explode(',', $publications_list);
	foreach ($publication_ids as $publication_id) {
		if (empty($publication_id)) continue;
		$sql_publication_check->bind_param('ss', $establishment_code, $publication_id);
		$sql_publication_check->execute();
		$sql_publication_check->store_result();
		$sql_publication_check->bind_result($publication_counter);
		$sql_publication_check->fetch();

		if ($publication_counter == 0) {
			$sql_publication_insert->bind_param('ss',$establishment_code, $publication_id);
			$sql_publication_insert->execute();
		}

		unset($publication_current[$publication_id]);
	}
	$sql_publication_check->close();
	$sql_publication_insert->close();

	if (!empty($publication_current)) {
		foreach ($publication_current as $publication_id=>$v) {
			$sql_publication_delete->bind_param('ss', $establishment_code, $publication_id);
			$sql_publication_delete->execute();
		}
	}
	$sql_publication_delete->close();

	//Update primary details
	if (in_array('primary', $changes)) {
		//Get current name to check for name change
		$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
		$sql_name = $GLOBALS['dbCon']->prepare($statement);
		$sql_name->bind_param('s', $establishment_code);
		$sql_name->execute();
		$sql_name->store_result();
		$sql_name->bind_result($old_establishment_name);
		$sql_name->fetch();
		$sql_name->free_result();
		$sql_name->close();

		if ($old_establishment_name != $establishment_name) $name_change = true;

		//AA Estab
		if (!empty($aa_grade)) $aa_estab = 'Y';		
		$statement = "SELECT COUNT(*)
				FROM nse_establishment_publications AS a
				JOIN nse_publications AS b ON a.publication_id=b.publication_id
				WHERE a.establishment_code=? && b.expiry_date>NOW()";
		$sql_aa_estab = $GLOBALS['dbCon']->prepare($statement);
		$sql_aa_estab->bind_param('s', $establishment_code);
		$sql_aa_estab->execute();
		$sql_aa_estab->store_result();
		$sql_aa_estab->bind_result($aa_estab_count);
		$sql_aa_estab->fetch();
		$sql_aa_estab->free_result();
		$sql_aa_estab->close();

		if ($aa_estab_count > 0) {
			$aa_estab = 'Y';
		}

		$statement = "UPDATE nse_establishment SET establishment_name=?, nightsbridge_bbid=?, prebook_bid=?, aa_category_code=?, website_url=?, last_updated=NOW(),aa_estab=?, search_hash=? WHERE establishment_code=?";
		$sql_primary = $GLOBALS['dbCon']->prepare($statement);
		$sql_primary->bind_param('ssssssss', $establishment_name, $nightsbridge_code, $prebook_code, $aa_grade, $website_url, $aa_estab, $search_hash, $establishment_code);
		$sql_primary->execute();
		$sql_primary->close();

		
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='country' && current_id=?";
		$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb_update->bind_param('s', $current_country);
		$sql_suburb_update->execute();
		$sql_suburb_update->close();

		//$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_suburb_update->bind_param('s', $current_country);
		//$sql_suburb_update->execute();
		//$sql_suburb_update->close();
		

		if ($current_province != '0') {
			$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='province' && current_id=?";
			$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_suburb_update->bind_param('s', $current_province);
			$sql_suburb_update->execute();
			$sql_suburb_update->close();

			//$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
			//$sql_suburb_update->bind_param('s', $current_province);
			//$sql_suburb_update->execute();
			//$sql_suburb_update->close();
		}

		if ($current_town != '0') {
			$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='town' && current_id=?";
			$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_suburb_update->bind_param('s', $current_town);
			$sql_suburb_update->execute();
			$sql_suburb_update->close();

			//$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
			//$sql_suburb_update->bind_param('s', $current_town);
			//$sql_suburb_update->execute();
			//$sql_suburb_update->close();
		}

		if ($current_suburb != '0') {
			$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='suburb' && current_id=?";
			$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_suburb_update->bind_param('s', $current_suburb);
			$sql_suburb_update->execute();
			$sql_suburb_update->close();

			//$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
			//$sql_suburb_update->bind_param('s', $current_suburb);
			//$sql_suburb_update->execute();
			//$sql_suburb_update->close();
		}
	}

	//Update active status
	$statement = "UPDATE nse_establishment SET active=? WHERE establishment_code=?";
	$sql_active = $GLOBALS['dbCon']->prepare($statement);
	$sql_active->bind_param('ss', $active_estab, $establishment_code);
	$sql_active->execute();
	$sql_active->close();

	//Update Notes
	if (in_array('notes', $changes)) {
		//Prepare statement - add note
		$statement = "INSERT INTO nse_establishment_notes (establishment_code, user_name, note_timestamp, note_content, module_name ) VALUES (?,?,?,?,'general')";
		$sql_notes = $GLOBALS['dbCon']->prepare($statement);

		//Get user name
		$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
		$sql_user = $GLOBALS['dbCon']->prepare($statement);
		$sql_user->bind_param('i', $_SESSION['dbweb_user_id']);
		$sql_user->execute();
		$sql_user->store_result();
		$sql_user->bind_result($user_name);
		$sql_user->fetch();
		$sql_user->free_result();
		$sql_user->close();

		$line = explode('##',$notes);
		foreach ($line as $note_line) {
			if (empty($note_line)) continue;
			$note_items = explode('|', $note_line);
			//Calculate timestamp
			list($date, $time) = explode(' ', $note_items[1]);
			list($day, $month, $year) = explode('-', $date);
			list($hours, $minutes) = explode(':',$time);
			$timestamp = mktime($hours, $minutes, 0, $month, $day, $year);

			$sql_notes->bind_param('ssss', $establishment_code, $user_name, $timestamp, $note_items[2]);
			$sql_notes->execute();
		}
		$sql_notes->close();
	}

	//Update Policies
	if (in_array('policy',$changes)) {
		$statement = "SELECT COUNT(*) FROM nse_establishment_descriptions WHERE language_code='EN' && description_type=? && establishment_code=?";
		$sql_policy_check = $GLOBALS['dbCon']->prepare($statement);

		$statement = "INSERT INTO nse_establishment_descriptions (language_code, description_type, establishment_description, establishment_code) VALUES ('EN',?,?,?)";
		$sql_policy_insert = $GLOBALS['dbCon']->prepare($statement);

		$statement = "UPDATE nse_establishment_descriptions SET establishment_description=? WHERE language_code='EN' && description_type=? && establishment_code=?";
		$sql_policy_update = $GLOBALS['dbCon']->prepare($statement);

		$description_type = 'child policy';
		$count = 0;
		$sql_policy_check->bind_param('ss', $description_type, $establishment_code);
		$sql_policy_check->execute();
		$sql_policy_check->bind_result($count);
		$sql_policy_check->store_result();
		$sql_policy_check->fetch();
		$sql_policy_check->free_result();
		if ($count == 0) {
			$sql_policy_insert->bind_param('sss', $description_type, $child_policy, $establishment_code);
			$sql_policy_insert->execute();
		} else {
			$sql_policy_update->bind_param('sss', $child_policy, $description_type, $establishment_code);
			$sql_policy_update->execute();
		}

		$description_type = 'cancellation policy';
		$count = 0;
		$sql_policy_check->bind_param('ss', $description_type, $establishment_code);
		$sql_policy_check->execute();
		$sql_policy_check->bind_result($count);
		$sql_policy_check->store_result();
		$sql_policy_check->fetch();
		$sql_policy_check->free_result();
		if ($count == 0) {
			$sql_policy_insert->bind_param('sss', $description_type, $cancellation_policy, $establishment_code);
			$sql_policy_insert->execute();
		} else {
			$sql_policy_update->bind_param('sss', $cancellation_policy, $description_type, $establishment_code);
			$sql_policy_update->execute();
		}

		$sql_policy_check->close();
		$sql_policy_insert->close();
		$sql_policy_update->close();
	}

	if (!empty($_FILES['directions_map']['name'])) {
		//Resize image & Create thumbnail
		list($src_width, $src_height, $image_type, $html_markup) = getimagesize($_FILES['directions_map']['tmp_name']);
		$file_name1 = strtolower($establishment_code) . image_type_to_extension($image_type);
		$file_name2 = strtolower($establishment_code);
		move_uploaded_file($_FILES['directions_map']['tmp_name'], SITE_ROOT . "/res_maps/$file_name1");

		switch ($image_type) {
			case IMG_GIF:
				$src_image = imagecreatefromgif(SITE_ROOT . "/res_maps/$file_name1");
				break;
			case IMG_JPG:
				$src_image = imagecreatefromjpeg(SITE_ROOT . "/res_maps/$file_name1");
				break;
			case IMG_PNG:
				$src_image = imagecreatefrompng(SITE_ROOT . "/res_maps/$file_name1");
				break;
			case IMG_WBMP:
				$src_image = imagecreatefromwbmp(SITE_ROOT . "/res_maps/$file_name1");
				break;
		}

		if ($src_width > $src_height) {
			$new_height = 200*($src_height/$src_width);
			$new_image = imagecreatetruecolor(200, $new_height);
			imagecopyresampled($new_image, $src_image, 0, 0, 0, 0, 200, $new_height, $src_width, $src_height);
		} else {
			$new_width = 200*($src_width/$src_height);
			$new_image = imagecreatetruecolor($new_height, 200);
			imagecopyresampled($new_image, $src_image, 0, 0, 0, 0, $new_width, 200, $src_width, $src_height);
		}

		imagejpeg($new_image, SITE_ROOT . "/res_maps/TN_$file_name2.jpg");

		foreach ($GLOBALS['ftp'] as $server=>$data) {
			$ftp_id = ftp_connect($server);
			ftp_login($ftp_id, $data['username'], $data['password']);
			ftp_chdir($ftp_id, $data['root'] . "res_maps/");
			ftp_put($ftp_id, "TN_$file_name2.jpg", SITE_ROOT . "/res_maps/TN_$file_name2.jpg", FTP_BINARY);
			ftp_put($ftp_id, "$file_name1", SITE_ROOT . "/res_maps/$file_name1", FTP_BINARY);
			ftp_close($ftp_id);
		}
	}

	//Update Directions
	if (in_array('directions', $changes)) {
		$statement = "SELECT COUNT(*) FROM nse_establishment_directions WHERE establishment_code=? ";
		$sql_directions_count = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_directions_count->bind_param('s', $establishment_code);
		$sql_directions_count->execute();
		$sql_directions_count->store_result();
		$sql_directions_count->bind_result($count);
		$sql_directions_count->fetch();
		$sql_directions_count->free_result();
		$sql_directions_count->close();

		if ($count == 0) {
			$statement = "INSERT INTO nse_establishment_directions (directions, map_type, image_name, establishment_code) VALUES (?,?,?,?)";
		} else {
			$statement = "UPDATE nse_establishment_directions SET directions=?, map_type=?, image_name=? WHERE establishment_code=?";
		}
		$sql_directions = $GLOBALS['dbCon']->prepare($statement);
		$sql_directions->bind_param('ssss',$directions, $map_type, $file_name1, $establishment_code);
		$sql_directions->execute();
		$sql_directions->close();
	}



	//Update Pricing
	if (in_array('price', $changes)) {
		$statement = "UPDATE nse_establishment_pricing SET
						price_description=?
						WHERE establishment_code=?";
		$sql_pricing = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_pricing->bind_param('ss', $pricing, $establishment_code);
		$sql_pricing->execute();
		$sql_pricing->close();
	}
		

	//Update Facilities (icons)
	if (in_array('facility', $changes)) {
		$statement = "DELETE FROM nse_establishment_icon WHERE establishment_code=?";
		$sql_icon_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_icon_delete->bind_param('s', $establishment_code);
		$sql_icon_delete->execute();
		$sql_icon_delete->close();

		$statement = "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES (?,?)";
		$sql_icon_insert = $GLOBALS['dbCon']->prepare($statement);
		foreach ($_POST['facilities'] as $icon_id) {
			$sql_icon_insert->bind_param('ss', $establishment_code, $icon_id);
			$sql_icon_insert->execute();
		}
	}

	//Update Restype
	if (in_array('restype', $changes)) {
		$statement = "DELETE FROM nse_establishment_restype WHERE establishment_code=?";
		$sql_restype_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_restype_delete->bind_param('s', $establishment_code);
		$sql_restype_delete->execute();

		$statement = "INSERT INTO nse_establishment_restype (establishment_code, subcategory_id, star_grading) VALUES (?,?,?)";
		$sql_restype_insert = $GLOBALS['dbCon']->prepare($statement);
		for ($i=0; $i<3; $i++) {
			$restype = $_POST['restype'][$i];
			$star_grading = $_POST['star_grading'][$i];
			if ($restype == 0) continue;
			$sql_restype_insert->bind_param('sss', $establishment_code, $restype, $star_grading);
			$sql_restype_insert->execute();
		}
		$sql_restype_delete->close();
		$sql_restype_insert->close();

		//Update Room count
		$statement = "UPDATE nse_establishment_data SET room_count=?, room_type=? WHERE establishment_code=?";
		$sql_rooms = $GLOBALS['dbCon']->prepare($statement);
		$sql_rooms->bind_param('sss', $room_count, $room_type, $establishment_code);
		$sql_rooms->execute();
		$sql_rooms->close();
	}

	//Update Descriptions
	$description_types = array('short_description','long_description');
	$statement = "DELETE FROM nse_establishment_descriptions WHERE establishment_code=? && description_type=?";
	$sql_description_delete = $GLOBALS['dbCon']->prepare($statement);

	$statement = "INSERT INTO nse_establishment_descriptions (establishment_code, description_type, establishment_description, language_code) VALUES (?,?,?,'EN')";
	$sql_description_insert = $GLOBALS['dbCon']->prepare($statement);

	foreach ($description_types as $type) {
		if (in_array($type, $changes)) {
			$sql_description_delete->bind_param('ss', $establishment_code, $type);
			$sql_description_delete->execute();
			$sql_description_insert->bind_param('sss', $establishment_code, $type, $description[$type]);
			$sql_description_insert->execute();
		}
	}
	$sql_description_delete->close();
	$sql_description_insert->close();

	//Clear Landmarks
	$statement = "DELETE FROM nse_establishment_landmarks WHERE establishment_code=?";
	$sql_landmarks_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_landmarks_delete->bind_param('s', $establishment_code);
	$sql_landmarks_delete->execute();
	$sql_landmarks_delete->close();

	//Prepare landmark queries
	$statement = "INSERT INTO nse_establishment_landmarks (establishment_code, landmark_id, landmark_distance, landmark_measurement) VALUES (?,?,?,?)";
	$sql_landmark_establishment = $GLOBALS['dbCon']->prepare($statement);

	$statement = "INSERT INTO nse_landmark (landmark_name, town_id, landmark_type_id) VALUES (?,?,?)";
	$sql_landmark_insert = $GLOBALS['dbCon']->prepare($statement);

	$statement = "SELECT landmark_id FROM nse_landmark WHERE lower(landmark_name)=? && town_id=? && landmark_type_id=?";
	$sql_landmark_check = $GLOBALS['dbCon']->prepare($statement);

	//Update Landmarks
	$statement = "SELECT landmark_type_id FROM nse_landmark_types";
	$sql_landmark_types = $GLOBALS['dbCon']->prepare($statement);
	$sql_landmark_types->execute();
	$sql_landmark_types->store_result();
	$sql_landmark_types->bind_result($landmark_type_id);
	while ($sql_landmark_types->fetch()) {
		for ($x=1; $x<4; $x++) {
			$landmark_id = $_POST["{$landmark_type_id}_id_$x"];
			$landmark_name = $_POST["{$landmark_type_id}_name_$x"];
			$landmark_measurement = $_POST["{$landmark_type_id}_measurement_$x"];
			$landmark_distance = $_POST["{$landmark_type_id}_distance_$x"];
			if (empty($landmark_name)) continue;
			$town_id = $_POST['town_id'];
			$sql_landmark_check->bind_param('sss', strtolower($landmark_name), $town_id, $landmark_type_id);
			$sql_landmark_check->execute();
			$sql_landmark_check->store_result();
			if ($sql_landmark_check->num_rows > 0) {
				$sql_landmark_check->bind_result($landmark_id);
				$sql_landmark_check->fetch();
			} else {
				$sql_landmark_insert->bind_param('sss',$landmark_name, $town_id, $landmark_type_id);
				$sql_landmark_insert->execute();
				$landmark_id = $sql_landmark_insert->insert_id;
			}
			$sql_landmark_establishment->bind_param('ssss',$establishment_code, $landmark_id, $landmark_distance, $landmark_measurement);
			$sql_landmark_establishment->execute();
		}
	}
	$sql_landmark_types->free_result();
	$sql_landmark_types->close();
	$sql_landmark_establishment->close();
	$sql_landmark_insert->close();
	$sql_landmark_check->close();	

	//Update Remote Establishment Details Page
	$ch = curl_init("http://aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	//$ch = curl_init("http://aatravel.server1/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);

	if ($name_change) {
		//Log change
		$statement = "INSERT INTO nse_establishment_name_history (establishment_code, old_establishment_name, change_date) VALUES (?,?,NOW())";
		$sql_change = $GLOBALS['dbCon']->prepare($statement);
		$sql_change->bind_param('ss', $establishment_code, $old_establishment_name);
		$sql_change->execute();
		$sql_change->close();

		//Update old page to redirect
		$ch = curl_init("http://aatravel.co.za/export/update_old_establishment.php?code=$establishment_code");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$res = curl_exec($ch);
		curl_close($ch);
	}

	include_once SITE_ROOT . '/modules/estab_s/establishment.php';
	$estab = new establishment();
	$estab->index_establishment($establishment_code);

	$GLOBALS['log_tool']->write_entry("Establishment Update (Assessor): $establishment_name ($establishment_code)", $_SESSION['dbweb_user_id']);

	//Get file name
	$name =  stripslashes($establishment_name);
	$name = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $name);
	$name = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $name );
	$name = htmlentities($name);
	$name = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $name);
	$name = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $name);
	$name = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $name);
	$name = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $name);
	$name = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $name);
	$name = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $name);

	$file_name = "/accommodation/". strtolower(str_replace(' ', '_', $establishment_code.'_'.$name . ".html"));

	//Replace Tags
	$template = str_replace('<!-- file_name -->', $file_name, $template);

	//Log Booking
	$GLOBALS['log_tool']->write_entry("Establishment details updated ($establishment_code)", $_SESSION['dbweb_user_id']);

	echo $template;
	echo "<script>update_page('ok')</script>";
}


/**
 * Replaces special characters with the english alphabet (a-Z) equivalent.
 * @param $name
 */
function plainize_name($name) {
	$name = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $name);
	$name = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $name );
	$name = htmlentities($name);
	$name = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $name);
	$name = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $name);
	$name = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $name);
	$name = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $name);
	$name = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $name);
	$name = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $name);

	return $name;
}
?>
