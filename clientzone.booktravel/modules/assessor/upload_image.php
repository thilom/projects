<?php
/**
 * Upload an image to a temporary location(/temp_images/).
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 */

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/estab_s/module.ini.php';

if (isset($_POST['updateImage'])) {
	image_update();
} else {
	image_form();
}

/**
 * Display the image edit/add form
 */
function image_form() {
	//Vars
	$image_id = ctype_alnum($_GET['id'])?$_GET['id']:'';
	$image_name = urldecode($_GET['image']);
	$image_title = urldecode($_GET['title']);
	$image_description = urldecode($_GET['description']);
	
	//Get template
	$content = file_get_contents(SITE_ROOT . '/modules/estab_s/html/upload_form.html');
	
	//Replace Tags
	if (!empty($image_name)) $content = str_replace('<!-- image -->', "<img src='$image_name' width=100 />", $content);
	$content = str_replace('<!-- image_title -->', $image_title, $content);
	$content = str_replace('<!-- image_description -->', $image_description, $content);
	$content = str_replace('<!-- old_image -->', $image_name, $content);
	
	echo $content;
}
/**
 * Moves the uploaded image to /temp_image/ folder. Does NOT save other image details but passes
 * it to a parent JS function.
 */
function image_update() {
	$image_id = ctype_alnum($_GET['id'])?$_GET['id']:'';
	$establishment_code = ctype_alnum($_GET['estab'])&&strlen($_GET['estab'])<8?$_GET['estab']:'';
	$image_title = $_POST['image_title'];
	$image_description = $_POST['image_description'];
	$longest_side_large = 300;
	$longest_side_thumb = 100;
	
	//Upload Image
	if (isset($_FILES['new_image']['tmp_name']) && !empty($_FILES['new_image']['name'])) {
		//Vars
		$last_image = FALSE;
		$image_counter = 0;
		
		move_uploaded_file($_FILES['new_image']['tmp_name'], SITE_ROOT . '/temp_images/' . $_FILES['new_image']['name']);
		
		//Get file name
		while (!$last_image) {
			$file_date = date('Ymd');
			$file = SITE_ROOT . "/temp_images/{$establishment_code}_{$file_date}_{$image_counter}.jpg";
			$thumb = SITE_ROOT . "/temp_images/TN_{$establishment_code}_{$file_date}_{$image_counter}.jpg";
			$image = "/temp_images/TN_{$establishment_code}_{$file_date}_{$image_counter}.jpg";
			if (!is_file($file)) {
				$last_image = TRUE;
			}
			$image_counter++;
		}
		
		//Resize Image
		list($image_width, $image_height, $image_type, $html_attrib) = getimagesize(SITE_ROOT . '/temp_images/' . $_FILES['new_image']['name']);
		switch ($image_type) {
			case IMG_GIF:
				$image_original = imagecreatefromgif(SITE_ROOT . '/temp_images/' . $_FILES['new_image']['name']);
				break;
			case IMG_JPG:
				$image_original = imagecreatefromjpeg(SITE_ROOT . '/temp_images/' . $_FILES['new_image']['name']);
				break;
			case IMG_PNG:
				$image_original = imagecreatefrompng(SITE_ROOT . '/temp_images/' . $_FILES['new_image']['name']);
				break;
			case IMG_WBMP:
				$image_original = imagecreatefromwbmp(SITE_ROOT . '/temp_images/' . $_FILES['new_image']['name']);
				break;
		}
//		echo "$image_width -- $image_height";
		if ($image_width > $image_height) {
			$shortest_side_large = $image_height * ($longest_side_large/$image_width);
			$shortest_side_thumb = $image_height * ($longest_side_thumb/$image_width);
			$image_large = imagecreatetruecolor($longest_side_large, $shortest_side_large);
			$image_thumb = imagecreatetruecolor($longest_side_thumb, $shortest_side_thumb);
			if ($image_width < $longest_side_large) {
				$image_large = $image_original;
			} else {
				imagecopyresampled($image_large, $image_original, 0, 0, 0, 0, $longest_side_large, $shortest_side_large, $image_width, $image_height);
			}
			
			imagecopyresampled($image_thumb, $image_original, 0, 0, 0, 0, $longest_side_thumb, $shortest_side_thumb, $image_width, $image_height);
		} else {
			$shortest_side_large = $image_width * ($longest_side_large/$image_height);
			$shortest_side_thumb = $image_width * ($longest_side_thumb/$image_height);
			
			$image_large = imagecreatetruecolor($shortest_side_large, $longest_side_large);
			$image_thumb = imagecreatetruecolor($shortest_side_thumb, $longest_side_thumb);
			if ($image_width < $longest_side_large) {
				$image_large = $image_original;
			} else {
				imagecopyresampled($image_large, $image_original, 0, 0, 0, 0, $longest_side_large, $shortest_side_large, $image_width, $image_height);
			}
			
			imagecopyresampled($image_thumb, $image_original, 0, 0, 0, 0, $longest_side_thumb, $shortest_side_thumb, $image_width, $image_height);
		}
		
		imagejpeg($image_large, $file);
		imagejpeg($image_thumb, $thumb);

		
	} else {
		$image = $_POST['old_image'];
	}
	
	echo "<script>parent.add_image_return('$image_id','$image','$image_title','$image_description')</script>";
	
}


?>