<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'assessor';
$module_name = "Assessment Manager";
$module_icon = 'contents-64.png';
$module_link = 'assessor.php';
$window_width = '1000';
$window_height = '800';
$window_position = '';

$list_length = 15;

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 'as'; //a = all, c = client, s = staff, as = assessor
$sub_levels = array();
$sub_levels['assessor']['name'] = 'Assesment Manager';
//$sub_levels['assessor']['file'] = 'assessor.php';
$sub_levels['assessor']['subs']['list']['name'] = 'Establishment Detail';
$sub_levels['assessor']['subs']['new']['name'] = 'New Assessment';
$sub_levels['assessor']['subs']['invoice']['name'] = 'Invoices';

$sub_levels['reports']['name'] = 'Reports';
$sub_levels['reports']['subs']['current']['name'] = 'Current Assessments';
$sub_levels['reports']['subs']['overdue']['name'] = 'Overdue Assessments';
$sub_levels['reports']['subs']['current']['name'] = 'Due in 1 Month';
$sub_levels['reports']['subs']['current']['name'] = 'Due in 3 Months';

//$sub_levels['manage_estab']['subs']['edit']['name'] = 'Edit Establishments';
//$sub_levels['manage_estab']['subs']['delete']['name'] = 'Delete Establishments';
//$sub_levels['manage_estab']['subs']['changes']['name'] = 'Manage Establishment Changes';



?>