<?php
/**
 * Module settings for Establishment management - Client Access
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category establishment
 */
$module_id = 'estab_c';
$module_name = "Establishment Manager";
$module_icon = 'gohome-64.png';
$noaccess_icon = 'estab_noaccess.png';
$module_link = 'estab.php';
$window_width = '1000';
$window_height = '800';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 'c'; //a = all




?>