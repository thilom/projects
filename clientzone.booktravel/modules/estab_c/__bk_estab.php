<?php
/**
 * Manage establishment Data
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 */

//Vars
$function_white_list = array('edit', 'estab_view', 'estab_edit', 'estab_add');
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';
$tables_list = array('nse_export','nse_export_eti');

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/estab_c/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/estab_c/html/estab.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/estab_c/html/estab.js'> </script>";

//Map
switch ($function_id) {
	case 'edit':
		if (isset($_POST['save_estab'])) {
			save_form();
		} else {
			edit_form();
		}
		break;
	default:
		welcome();
}

function edit_form() {
	//Vars
	$advertiser_code = '';
	$advertiser_name = '';
	$advertiser_options = '';
	$advertiser = '';
	$aa_grade = '';
	
	$billing_line1 = '';
	$billing_line2 = '';
	$billing_line3 = '';
	$billing_code = '';
	$base_tel = '';
	$billing_hash = '';
	
	$company_name = '';
	$company_number = '';
	$contact_line = '';
	$contact_name = '';
	$contact_designation = '';
	$contact_tel = '';
	$contact_cell = '';
	$contact_email = '';
	$contacts_list = '';
	$contact_type = '';
	$contact_value = '';
	$contact_tel1 = '';
	$contact_tel2 = '';
	$contact_fax1 = '';
	$contact_fax2 = '';
	$contact_cell1 = '';
	$contact_cell2 = '';
	$contact_pemail = '';
	$contact_pemail2 = '';
	$contact_pemail3 = '';
	$contact_tel1_description = '';
	$contact_tel2_description = '';
	$contact_fax1_description = '';
	$contact_fax2_description = '';
	$contact_cell1_description = '';
	$contact_cell2_description = '';
	$country_options = '';
	$country_name = '';
	$country_id = '';
	$contact_description = '';
	$cancellation_policy = '';
	$child_policy = '';
	$contact_tel = '';
	$contact_fax = '';
	$category_prefix = '';
	$category = '';
	$category_suffix = '';
	$curr_icon = '';
	$current_restype_id = '';
	$current_star_grading = '';
	$change_name = '';
	$change_value = '';
	$change_list  = '';
	
	$directions = '';
	$default_image = '';
	
	$establishment_code = isset($_GET['id'])&&ctype_alnum($_GET['id'])&&strlen($_GET['id'])<8?$_GET['id']:'';
	$establishment_name = '';
	
	$facilities = '';
	$form_heading = "New Establishment";
	
	$grade_name = '';
	$grade_code = '';
	$grade_options = '';
	$gps_latitude = '';
	$gps_longitude = '';
	$gps_hash = '';
	
	$image_list = '';
	$image_hash = '';
	$image_id = '';
	$image_name = '';
	$image_title = '';
	$image_description = '';
	$icon_id = '';
	$icon_description = '';
	$icon_src = '';
	$icon_category_id = '';
	$icon = '';
	$icon_category_name = '';
	
	$landmark_distance = '';
	$landmark_measurement = '';
	$landmark_type_id = '';
	$landmark_type_name = '';
	$landmark_id = '';
	$landmark_name = '';
	$landmark_list = '';
	$last_updated = '';
	$long_description = '';
	$location_country_id = '';
	$location_province_id = '';
	$location_town_id = '';
	$location_suburb_id = '';
	
	$matches = array();
	$map_type_image = '';
	$map_type_google = '';
	$map_type_none = '';
	$map_type = '';
	
	$noprov = FALSE;
	$nightsbridge_id = '';
	
	$public_contact_harh = '';
	$postal_line1 = '';
	$postal_line2 = '';
	$postal_line3 = '';
	$postal_code = '';
	$primary_image = '';
	$province_id = '';
	$province_name = '';
	$province_options = '';
	$province_options_status = '';
	$pricing = '';
	$price_options = '';
	$poi_entertainment = '';
	$poi_historical = '';
	$poi_other = '';
	$prebook_id = '';
	
	$reservation_fax = '';
	$reservation_tel = '';
	$reservation_email = '';
	$reservation_cell = '';
	$reservation_postal1 = '';
	$reservation_postal2 = '';
	$reservation_postal3 = '';
	$reservation_postal_code = '';
	$restype_options_0 = '<option value=0>-- None --</option>';
	$restype_options_1 = '<option value=0>-- None --</option>';
	$restype_options_2 = '<option value=0>-- None --</option>';
	
	$street_line1 = '';
	$street_line2 = '';
	$street_line3 = '';
	$short_description = '';
	$suburb_id = '';
	$suburb_name = '';
	$suburb_options = '';
	$suburb_options_status = '';
	$star_grading_options_0 = '';
	$star_grading_options_1 = '';
	$star_grading_options_2 = '';
	$subcategory_id = '';
	$subcategory_name = '';
		
	$town_id = '';
	$town_name = '';
	$town_options = '';
	$town_options_status = '';
	$toplevel_id = '';
	$toplevel_name = '';
	
	$vat_number = '';
	
	$website_url = '';
	
	$contacts_hash = '';
	$public_contact_hash = '';
	$image_hash = '';
	$description_hash = '';
	$gps_hash = '';
	$policy_hash = '';
	$direction_hash = '';
	$price_hash = '';
	$icon_hash = '';
	
	//Arrays
	$price_codes = array('G' => 'Under R100',
						'F' => 'From R100 - R149',
						'E' => 'From R150 - R199',
						'D' => 'From R200 - R299',
						'C' => 'From R300 - R399',
						'B' => 'From R400 - R499',
						'A' => 'From R500 - R999',
						'A1' => 'Over R1000',
						'A2' => 'Over R2000',
						'A3' => 'Over R3000',
						'A4' => 'Over R4000',
						'A5' => 'Over R5000',
						'A6' => 'Over R6000',
						'A7' => 'Over R7000',);
	
	//Get Template
	$template = file_get_contents(SITE_ROOT . '/modules/estab_c/html/establishment_edit_form.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	
	//Get Basic Data
		//Get Changed Data
		$change_counter = 0;
		$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
		$sql_changes = $GLOBALS['dbCon']->prepare($statement);
		$sql_changes->bind_param('s', $establishment_code);
		$sql_changes->execute();
		$sql_changes->store_result();
		$sql_changes->bind_result($change_name, $change_value);
		while ($sql_changes->fetch()) {
			$change_list .= "chgList['$change_name']='" . str_replace("\r\n",'\n', $change_value) . "'\r\n";
			$change_list .= "chgFields[$change_counter] = '$change_name'\r\n";
			$change_counter++;
		}
		$sql_changes->free_result();
		$sql_changes->close();
	
		$statement = "SELECT establishment_name, last_updated, postal_address_line1, postal_address_line2, postal_address_line3, postal_address_code, street_address_line1, street_address_line2, street_address_line3, aa_category_code, nightsbridge_bbid, prebook_bid, advertiser, website_url FROM nse_establishment WHERE establishment_code=?";
		$sql_basic = $GLOBALS['dbCon']->prepare($statement);
		$sql_basic->bind_param('s', $establishment_code);
		$sql_basic->execute();
		$sql_basic->bind_result($establishment_name, $last_updated, $postal_line1, $postal_line2, $postal_line3, $postal_code, $street_line1, $street_line2, $street_line3, $aa_grade, $nightsbridge_id, $prebook_id, $advertiser, $website_url);
		$sql_basic->store_result();
		$sql_basic->fetch();
		$sql_basic->free_result();
		$sql_basic->close();
	
		//Get Billing Data
		$statement = "SELECT billing_line1, billing_line2, billing_line3, billing_code, vat_number, company_name, company_number FROM nse_establishment_billing WHERE establishment_code=?";
		$sql_billing = $GLOBALS['dbCon']->prepare($statement);
		$sql_billing->bind_param('s', $establishment_code);
		$sql_billing->execute();
		$sql_billing->bind_result($billing_line1, $billing_line2, $billing_line3, $billing_code, $vat_number, $company_name, $company_number);
		$sql_billing->store_result();
		$sql_billing->fetch();
		$sql_billing->free_result();
		$sql_billing->close();
	
		//Get & Assebmle Contact details
		$counter = 0;
		$statement = "SELECT contact_name, contact_designation, contact_tel, contact_cell, contact_email FROM nse_establishment_contacts WHERE establishment_code=?";
		$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
		$sql_contacts->bind_param('s', $establishment_code);
		$sql_contacts->execute();
		$sql_contacts->bind_result($contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
		$sql_contacts->store_result();
		while ($sql_contacts->fetch()) {
			$contacts_list .= "!|!$counter|$contact_name|$contact_designation|$contact_tel|$contact_cell|$contact_email";
			$counter++;
		}
		$sql_contacts->free_result();
		$sql_contacts->close();
	
		//Get Public Contact Details
		$statement = "SELECT contact_type, contact_value, contact_description FROM nse_establishment_public_contact WHERE establishment_code=?";
		$sql_public_contact = $GLOBALS['dbCon']->prepare($statement);
		$sql_public_contact->bind_param('s', $establishment_code);
		$sql_public_contact->execute();
		$sql_public_contact->bind_result($contact_type, $contact_value, $contact_description);
		$sql_public_contact->store_result();
		if ($sql_public_contact->num_rows == 0) {
			//Get Reservation Data
			$statement = "SELECT reservation_tel, reservation_fax, reservation_cell, reservation_email, reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code FROM nse_establishment_reservation WHERE establishment_code=?";
			$sql_reservation = $GLOBALS['dbCon']->prepare($statement);
			$sql_reservation->bind_param('s', $establishment_code);
			$sql_reservation->execute();
			$sql_reservation->store_result();
			$sql_reservation->bind_result($reservation_tel, $reservation_fax, $reservation_cell, $reservation_email, $reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code);
			$sql_reservation->fetch();
			$sql_reservation->free_result();
			
			//Get contact Data
			$statement = "SELECT contact_tel, contact_cell, contact_fax FROM nse_establishment_contact WHERE establishment_code = ?";
			$sql_contact = $GLOBALS['dbCon']->prepare($statement);
			$sql_contact->bind_param('s', $establishment_code);
			$sql_contact->execute();
			$sql_contact->bind_result($contact_tel, $contact_cell, $contact_fax);
			$sql_contact->store_result();
			$sql_contact->fetch();
			$sql_contact->free_result();
			
			//Assemble tel /fax numbers
			$establishment_tel = !empty($contact_tel)?$contact_tel:$establishment_tel;
			$establishment_tel = !empty($base_tel)?$base_tel:$establishment_tel;
			$establishment_tel = !empty($reservation_tel)?$reservation_tel:$establishment_tel;
			$establishment_fax = !empty($contact_fax)?$contact_fax:$establishment_fax;
			if (substr($establishment_tel, 0, 5) == '& Fax') {
				$establishment_fax = substr($establishment_tel, 6);
				$establishment_tel = substr($establishment_tel, 6);
			}
			$establishment_fax = !empty($reservation_fax)?$reservation_fax:$establishment_fax;
			$establishment_cell = !empty($contact_cell)?$contact_cell:$establishment_cell;
			$establishment_cell = !empty($reservation_cell)?$reservation_cell:$establishment_cell;
			if (substr($establishment_tel, -1) == ',' || substr($establishment_tel, -1) == ';') $establishment_tel = substr($establishment_tel, 0, -1);
			if (substr($establishment_fax, -1) == ',' || substr($establishment_fax, -1) == ';') $establishment_fax = substr($establishment_fax, 0, -1);
			if (substr($establishment_cell, -1) == ',' || substr($establishment_cell, -1) == ';') $establishment_cell = substr($establishment_cell, 0, -1);
			
			$contact_tel1 = $establishment_tel;
			$contact_fax1 = $establishment_fax;
			$contact_cell1 = $establishment_cell;
			$contact_pemail = $reservation_email;
		} else {
			while ($sql_public_contact->fetch()) {
				switch ($contact_type) {
					case 'tel1':
						$contact_tel1 = $contact_value;
						$contact_tel1_description = $contact_description;
						break;
					case 'tel2':
						$contact_tel2 = $contact_value;
						$contact_tel2_description = $contact_description;
						break;
					case 'fax1':
						$contact_fax1 = $contact_value;
						$contact_fax1_description = $contact_description;
						break;
					case 'fax2':
						$contact_fax2 = $contact_value;
						$contact_fax2_description = $contact_description;
						break;
					case 'cell1':
						$contact_cell1 = $contact_value;
						$contact_cell1_description = $contact_description;
						break;
					case 'cell2':
						$contact_cell2 = $contact_value;
						$contact_cell2_description = $contact_description;
						break;
					case 'email':
						$contact_pemail = $contact_value;
						break;
					case 'email2':
						$contact_pemail2 = $contact_value;
						break;
					case 'email3':
						$contact_pemail3 = $contact_value;
						break;
				}
			}
		}
		$sql_public_contact->free_result();
		$sql_public_contact->close();
	
		//Get Images
		$statement = "SELECT image_id, thumb_name, image_title, image_description, primary_image FROM nse_establishment_images WHERE establishment_code=? ORDER BY primary_image DESC, image_name ";
		$sql_images = $GLOBALS['dbCon']->prepare($statement);
		$sql_images->bind_param('s', $establishment_code);
		$sql_images->execute();
		$sql_images->bind_result($image_id, $image_name, $image_title, $image_description, $primary_image);
		$sql_images->store_result();
		$counter = 0;
		while ($sql_images->fetch()) {
			if ($counter==0) $default_image = $image_id;
			$image_list .= "$image_id|http://www.aatravel.co.za/res_images/$image_name|$image_title|$image_description|";
			//$image_list .= $counter==0?'1':'0';
			$image_list .= "0!|!";
			$counter++;
		}
		$sql_images->free_result();
		$sql_images->close();
	
		//Get Descriptions, policies and pricing
		$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=? && language_code='EN' && description_type=?";
		$sql_description = $GLOBALS['dbCon']->prepare($statement);
		$description_type = 'short_description';
		$sql_description->bind_param('ss', $establishment_code, $description_type);
		$sql_description->execute();
		$sql_description->bind_result($short_description);
		$sql_description->store_result();
		$sql_description->fetch();
		$sql_description->free_result();
		
		
		
//		$description_type = 'long_description';
//		$sql_description->bind_param('ss', $establishment_code, $description_type);
//		$sql_description->execute();
//		$sql_description->bind_result($long_description);
//		$sql_description->store_result();
//		$sql_description->fetch();
//		$sql_description->free_result();
		
		//Alternate - Data is returned corrupt when the above mysqli method is used.
		$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code='$establishment_code' && language_code='EN' && description_type='long_description'";
		$sql_description2 = mysql_query($statement);
		list($long_description) = mysql_fetch_array($sql_description2);
		
		$description_type = 'cancellation policy';
		$sql_description->bind_param('ss', $establishment_code, $description_type);
		$sql_description->execute();
		$sql_description->bind_result($cancellation_policy);
		$sql_description->store_result();
		$sql_description->fetch();
		$sql_description->free_result();
		
		$description_type = 'child policy';
		$sql_description->bind_param('ss', $establishment_code, $description_type);
		$sql_description->execute();
		$sql_description->bind_result($child_policy);
		$sql_description->store_result();
		$sql_description->fetch();
		$sql_description->free_result();
		
		if (empty($long_description)) $long_description = $short_description;
	
		/* POINTS OF INTEREST */
		
		//Get distance measurements
		$statement = "SELECT landmark_measurement_id, landmark_measurement_name FROM nse_landmark_measurement";
		$sql_measurements = $GLOBALS['dbCon']->prepare($statement);
		$sql_measurements->execute();
		$sql_measurements->store_result();
		$sql_measurements->bind_result($measurement_id, $measurement_name);
		while ($sql_measurements->fetch()) {
			$measurements[$measurement_id] = $measurement_name;
		}
		$sql_measurements->free_result();
		$sql_measurements->close();
		
		//prepare statement - establishment poi
		$statement = "SELECT b.landmark_id, b.landmark_name, a.landmark_distance, a.landmark_measurement
						FROM nse_establishment_landmarks as a
						JOIN nse_landmark as b ON a.landmark_id=b.landmark_id
						WHERE b.landmark_type_id=? && a.establishment_code=?";
		$sql_landmarks = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		
		//Get landmarks
		$statement = "SELECT landmark_type_id, landmark_type_name FROM nse_landmark_types ORDER BY display_order";
		$sql_landmark_types = $GLOBALS['dbCon']->prepare($statement);
		$sql_landmark_types->execute();
		$sql_landmark_types->store_result();
		$sql_landmark_types->bind_result($landmark_type_id, $landmark_type_name);
		while($sql_landmark_types->fetch()) {
			$list_count = 1;
			$landmark_list .= "<TR><TD colspan=10 class=field_label>$landmark_type_name</TD></TR>";
			
			$sql_landmarks->bind_param('ss', $landmark_type_id, $establishment_code);
			$sql_landmarks->execute();
			$sql_landmarks->store_result();
			$sql_landmarks->bind_result($landmark_id, $landmark_name, $landmark_distance, $landmark_measurement);
			while($sql_landmarks->fetch()) {
				$landmark_list .= "<tr>";
				$landmark_list .= "<td width=10px>&nbsp;</td>";
				$landmark_list .= "<td width=10px >$list_count. </td>";
				$landmark_list .= "<td>";
				$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='$landmark_name' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
				$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='$landmark_id' />";
				$landmark_list .= "</td>";
				$landmark_list .= "<td>&nbsp;</td>";
				$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='$landmark_distance' onChange=set_change('poi','poi',this.name) /></td>";
				$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
				foreach ($measurements as $k=>$v) {
					if ($k == $landmark_measurement) {
						$landmark_list .= "<option value='$k' selected>$v</option>";
					} else {
						$landmark_list .= "<option value='$k'>$v</option>";
					}
				}
				$landmark_list .= "</select></td>";
				$landmark_list .= "<td><iframe src='http://admin.aatravel.co.za/modules/estab_s/did_you_mean.php' class=poi_dym frameborder='0' style='overflow: hidden' id={$landmark_type_id}_iframe_{$list_count}> </iframe></td>";
				$landmark_list .= "</tr>";
				$list_count++;
			}
			while ($list_count < 4) {
				$landmark_list .= "<tr>";
				$landmark_list .= "<td width=10px>&nbsp;</td>";
				$landmark_list .= "<td width=10px >$list_count. </td>";
				$landmark_list .= "<td>";
				$landmark_list .= "<input type=text name={$landmark_type_id}_name_{$list_count} id={$landmark_type_id}_name_{$list_count} value='' style='width: 300px' onChange=\"poi_check(this.value,'{$landmark_type_id}','{$list_count}');set_change('pio','poi',this.name)\" />";
				$landmark_list .= "<input type=hidden name={$landmark_type_id}_id_{$list_count} id={$landmark_type_id}_id_{$list_count} value='' />";
				$landmark_list .= "</td>";
				$landmark_list .= "<td>&nbsp;</td>";
				$landmark_list .= "<td><input type=text name={$landmark_type_id}_distance_{$list_count} id={$landmark_type_id}_distance_{$list_count} style='width: 50px' value='' onChange=set_change('poi','poi',this.name) /></td>";
				$landmark_list .= "<td><select name={$landmark_type_id}_measurement_{$list_count} id={$landmark_type_id}_measurement_{$list_count} onChange=set_change('poi','poi',this.name) ><option value=''></option>";
				foreach ($measurements as $k=>$v) {
					$landmark_list .= "<option value='$k'>$v</option>";
				}
				$landmark_list .= "</select></td>";
				$landmark_list .= "<td><iframe src='/modules/estab_s/did_you_mean.php' class=poi_dym frameborder='0' id={$landmark_type_id}_iframe_{$list_count}> </iframe></td>";
				$landmark_list .= "</tr>";
				$list_count++;
			}
			$landmark_list .= "<TR><TD colspan=10>&nbsp;</TD></TR>";
		}
		$sql_landmark_types->free_result();
		$sql_landmark_types->close();
		$sql_landmarks->free_result();
		$sql_landmarks->close();
		
		//Get Pricing Data
		$statement = "SELECT price_description, category_prefix, category, category_suffix FROM nse_establishment_pricing WHERE establishment_code=?";
		$sql_pricing = $GLOBALS['dbCon']->prepare($statement);
		$sql_pricing->bind_param('s', $establishment_code);
		$sql_pricing->execute();
		$sql_pricing->store_result();
		$sql_pricing->bind_result($pricing, $category_prefix, $category, $category_suffix);
		$sql_pricing->fetch();
		$sql_pricing->free_result();
		$sql_pricing->close();
		
		//Assemble price category
		foreach ($price_codes as $code=>$icon_description) {
			if ($code == $category) {
				$price_options .= "<OPTION value='$code' selected>$code ($icon_description)</option>";
			} else {
				$price_options .= "<OPTION value='$code'>$code ($icon_description)</option>";
			}
		}
	
		//Get Current Facilities
		$curr_icons = array();
		$statement = "SELECT icon_id FROM nse_establishment_icon WHERE establishment_code=?";
		$sql_icons = $GLOBALS['dbCon']->prepare($statement);
		$sql_icons->bind_param('s', $establishment_code);
		$sql_icons->execute();
		$sql_icons->bind_result($icon);
		$sql_icons->store_result();
		while ($sql_icons->fetch()) {
			$curr_icons[$icon] = $icon;
		}
		$sql_icons->free_result();
		$sql_icons->close();
		
		//Get establishment location data
		$statement = "SELECT country_id, province_id, town_id, suburb_id, gps_latitude, gps_longitude FROM nse_establishment_location WHERE establishment_code=?";
		$sql_location = $GLOBALS['dbCon']->prepare($statement);
		$sql_location->bind_param('s', $establishment_code);
		$sql_location->execute();
		$sql_location->bind_result($location_country_id, $location_province_id, $location_town_id, $location_suburb_id, $gps_latitude, $gps_longitude);
		$sql_location->store_result();
		$sql_location->fetch();
		$sql_location->free_result();
		$sql_location->close();
		
		//Get Location Data = Country
		$statement = "SELECT country_id, country_name FROM nse_location_country_lang WHERE language_code='EN' ORDER BY country_name";
		$sql_country = $GLOBALS['dbCon']->prepare($statement);
		$sql_country->execute();
		$sql_country->bind_result($country_id, $country_name);
		$sql_country->store_result();
		$country_options .= "<option value='0'>-- Select Country --</option>";
		while ($sql_country->fetch()) {
			if ($location_country_id == $country_id) {
				$country_options .= "<option value='$country_id' selected >$country_name</option>";
			} else {
				$country_options .= "<option value='$country_id'>$country_name</option>";
			}
		}
		
		//Get Location Data - Province
		$statement = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=? ORDER BY province_name";
		$sql_province = $GLOBALS['dbCon']->prepare($statement);
		$sql_province->bind_param('s', $location_country_id);
		$sql_province->execute();
		$sql_province->bind_result($province_id, $province_name);
		$sql_province->store_result();
		if ($sql_province->num_rows == 0) {
			$province_options = "<option>-- No Provinces For Selected Country --</option>";
			$province_options_status = 'disabled';
			$noprov = TRUE;
		} else {
			$province_options .= "<option value='0'>-- Select Province --</option>";
			while ($sql_province->fetch()) {
				if ($location_province_id == $province_id) {
					$province_options .= "<option value='$province_id' selected>$province_name</option>";
				} else {
					$province_options .= "<option value='$province_id'>$province_name</option>";
				}
			}
		}
		
		//Get Location Data - Towns
		if ($noprov) {
			$statement = "SELECT town_id, town_name FROM nse_location_town WHERE country_id=? ORDER BY town_name";
			$sql_towns = $GLOBALS['dbCon']->prepare($statement);
			$sql_towns->bind_param('s', $location_country_id);
		} else {
			$statement = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=? ORDER BY town_name";
			$sql_towns = $GLOBALS['dbCon']->prepare($statement);
			$sql_towns->bind_param('s', $location_province_id);
		}
		$sql_towns->execute();
		$sql_towns->bind_result($town_id, $town_name);
		$sql_towns->store_result();
		if ($sql_towns->num_rows == 0) {
			$town_options = "-- No Towns For Selected Province --";
			$town_options_status = 'disabled';
		} else {
			$town_options .= "<option value='0'>-- Select Town --</option>";
			while ($sql_towns->fetch()) {
				if (strpos($town_name, 'Aaa') !== FALSE) continue;
				if (strpos($town_name, 'Zzz') !== FALSE) continue;
				if (strpos($town_name, '***') !== FALSE) continue;
				if ($location_town_id == $town_id) {
					$town_options .= "<option value='$town_id' selected >$town_name</option>";
				} else {
					$town_options .= "<option value='$town_id' >$town_name</option>";
				}
			}
		}
		
		//Get Location Data - Suburbs
		$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=? ORDER BY suburb_name";
		$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb->bind_param('s', $location_town_id);
		$sql_suburb->execute();
		$sql_suburb->bind_result($suburb_id, $suburb_name);
		$sql_suburb->store_result();
		if ($sql_suburb->num_rows == 0) {
			$suburb_options = "-- No Suburbs For Selected Town --";
			$suburb_options_status = 'disabled';
		} else {
			$suburb_options .= "<option value='0'>-- Select Suburb --</option>";
			while ($sql_suburb->fetch()) {
				if ($location_suburb_id == $suburb_id) {
					$suburb_options .= "<option value='$suburb_id' selected >$suburb_name</option>";
				} else {
					$suburb_options .= "<option value='$suburb_id' >$suburb_name</option>";
				}
			}
		}
		
		//Get Directions
		$statement = "SELECT directions, map_type FROM nse_establishment_directions WHERE establishment_code=?";
		$sql_directions = $GLOBALS['dbCon']->prepare($statement);
		$sql_directions->bind_param('s', $establishment_code);
		$sql_directions->execute();
		$sql_directions->bind_result($directions, $map_type);
		$sql_directions->store_result();
		$sql_directions->fetch();
		$sql_directions->free_result();
		$sql_directions->close();
		switch ($map_type) {
			case 'none':
				$map_type_none = 'checked';
				break;
			case 'google':
				$map_type_google = 'checked';
				break;
			case 'upload':
				$map_type_image = 'checked';
				break;
			default:
				$map_type_none = 'checked';
		}
		
		//Get Current Restype
		$counter = 0;
		$statement = "SELECT subcategory_id, star_grading FROM nse_establishment_restype WHERE establishment_code=?";
		$sql_restype_current = $GLOBALS['dbCon']->prepare($statement);
		$sql_restype_current->bind_param('s', $establishment_code);
		$sql_restype_current->execute();
		$sql_restype_current->store_result();
		$sql_restype_current->bind_result($current_restype_id, $current_star_grading);
		while ($sql_restype_current->fetch()) {
			$current_restype[$counter] = $current_restype_id;
			$current_grading[$counter] = $current_star_grading;
			$counter++;
		}
		$sql_restype_current->free_result();
		$sql_restype_current->close();
		
		//Get AA Grades List
		$grade_options = 'N/A';
		$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category";
		$sql_aa_categories = $GLOBALS['dbCon']->prepare($statement);
		$sql_aa_categories->execute();
		$sql_aa_categories->store_result();
		$sql_aa_categories->bind_result($grade_code, $grade_name);
		while ($sql_aa_categories->fetch()) {
			if ($grade_code == $aa_grade) {
				$grade_options = $grade_name;
			}
		}
		$sql_aa_categories->free_result();
		$sql_aa_categories->close();
		
		//Get Advertisers
		$advertiser_options = "N/A";
		$statement = "SELECT advertiser_code, advertiser_name FROM nse_advertiser ORDER BY advertiser_name";
		$sql_advertiser = $GLOBALS['dbCon']->prepare($statement);
		$sql_advertiser->execute();
		$sql_advertiser->store_result();
		$sql_advertiser->bind_result($advertiser_code, $advertiser_name);
		while ($sql_advertiser->fetch()) {
			if ($advertiser == $advertiser_code) {
				$advertiser_options = "$advertiser_name";
			}
		}
		$sql_advertiser->free_result();
		$sql_advertiser->close();
		
		//Calculate Hashes
//		$billing_hash = MD5($billing_line1 . $billing_line2 . $billing_line3 . $billing_code . $vat_number);
//		$contacts_hash = MD5($contacts_list);
//		$public_contact_hash = MD5($establishment_name . $contact_tel1 . $contact_tel2 . $contact_cell1 . $contact_cell2 . $contact_fax1 . $contact_fax2 . $contact_email . $contact_tel1_description . $contact_tel2_description . $contact_cell1_description . $contact_cell2_description . $contact_fax1_description . $contact_fax2_description . $postal_line1 . $postal_line2 . $postal_line3 . $postal_code . $street_line1 . $street_line2 . $street_line3);
//		$image_hash = md5($image_list);
//		$description_hash = MD5($short_description . $long_description);
//		$gps_hash = md5($gps_latitude . $gps_longitude);
//		$policy_hash = MD5($cancellation_policy . $child_policy);
//		$direction_hash = MD5($directions . $map_type);
//		$price_hash = MD5($pricing . $category_prefix . $category . $category_suffix);
//		$icon_hash = MD5(implode('', $curr_icons));
		
		$form_heading = "Details for $establishment_name ($establishment_code)";
	
	
	//Get restypes list
	$statement = "SELECT toplevel_id, toplevel_name FROM nse_restype_toplevel ORDER BY toplevel_id";
	$sql_restype_toplevel = $GLOBALS['dbCon']->prepare($statement);
		
	$statement = "SELECT DISTINCT(a.subcategory_name), a.subcategory_id
					FROM nse_restype_subcategory_lang AS a
					JOIN nse_restype_subcategory_parent AS b ON a.subcategory_id=b.subcategory_id
					JOIN nse_restype_category AS c ON c.category_id=b.category_id
					WHERE c.toplevel_id=?
					ORDER BY a.subcategory_name";
	$sql_restypes = $GLOBALS['dbCon']->prepare($statement);
	
	$sql_restype_toplevel->execute();
	$sql_restype_toplevel->store_result();
	$sql_restype_toplevel->bind_result($toplevel_id, $toplevel_name);
	while ($sql_restype_toplevel->fetch()) {
		$restype_options_0 .= "<optgroup label='$toplevel_name' >";
		$restype_options_1 .= "<optgroup label='$toplevel_name' >";
		$restype_options_2 .= "<optgroup label='$toplevel_name' >";
			
		$sql_restypes->bind_param('s', $toplevel_id);
		$sql_restypes->execute();
		$sql_restypes->store_result();
		$sql_restypes->bind_result($subcategory_name, $subcategory_id);
		while ($sql_restypes->fetch()) {
			if (isset($current_restype[0]) && $subcategory_id == $current_restype[0]) {
				$restype_options_0 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_0 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}

			if (isset($current_restype[1]) && $subcategory_id == $current_restype[1]) {
				$restype_options_1 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_1 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
			if (isset($current_restype[2]) && $subcategory_id == $current_restype[2]) {
				$restype_options_2 .= "<option value='$subcategory_id' selected>$subcategory_name</option>";
			} else {
				$restype_options_2 .= "<option value='$subcategory_id'>$subcategory_name</option>";
			}
		}
		$restype_options_0 .= "</optgroup>";
		$restype_options_1 .= "</optgroup>";
		$restype_options_2 .= "</optgroup>";
	}
	$sql_restype_toplevel->free_result();
	$sql_restype_toplevel->close();
	$sql_restypes->free_result();
	$sql_restypes->close();
	
	//Get stargradings list
	for ($x=0; $x<6; $x++) {
		if (isset($current_grading[0]) && $x == $current_grading[0]) {
			$star_grading_options_0 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_0 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[1]) && $x == $current_grading[1]) {
			$star_grading_options_1 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_1 .= "<option value=$x>$x</option>";
		}
		if (isset($current_grading[2]) && $x == $current_grading[2]) {
			$star_grading_options_2 .= "<option value=$x selected>$x</option>";
		} else {
			$star_grading_options_2 .= "<option value=$x>$x</option>";
		}
	}

	//Get Facilities
	$facilities = "";
	$lRowsCount = 0;
	$rRowsCount = 0;
	$colLength = 3;
	$lRow = '<table width=100% cellpadding=0 cellspacing=0>';
	$rRow = '<table width=100%  cellpadding=0 cellspacing=0>';
	
	$statement = "SELECT a.icon_id, a.icon_url, b.icon_description
					FROM nse_icon AS a
					JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
					WHERE a.icon_category_id=?";
	$sql_facility_data = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "SELECT b.icon_name, a.icon_category_id
					FROM nse_icon_category AS a
					JOIN nse_icon_category_lang AS b ON a.icon_category_id=b.icon_category_id
					ORDER BY a.icon_priority ";
	$sql_facilities = $GLOBALS['dbCon']->prepare($statement);
	$sql_facilities->execute();
	$sql_facilities->bind_result($icon_category_name, $icon_category_id);
	$sql_facilities->store_result();
	while ($sql_facilities->fetch()) {
		$col = 0;
		$sql_facility_data->bind_param('s', $icon_category_id);
		$sql_facility_data->execute();
		$sql_facility_data->bind_result($icon_id, $icon_src, $icon_description);
		$sql_facility_data->store_result();
		//if ($lRowsCount < $rRowsCount) {
			$lRow .= "<tr><td colspan=10 style='padding: 20px 0px 5px 0px'><b>$icon_category_name</b></td></tr>";
			while ($sql_facility_data->fetch()) {
				//$src_off = str_replace('.gif', '_off.gif', $icon_src);
				if ($col == 0) {
					$lRow .= "<tr valign=top>";
					$lRowsCount++;
				}
				$lRow .= "<td><input type=checkbox name='facilities[]' id='facilities_$icon_id' value='$icon_id' onChange=set_change('facility','facility',this.id) ";
				if (isset($curr_icons[$icon_id])) {
					$lRow .= 'checked';
				}
				$lRow .= " ></td><td><img src='$icon_src' width=33 height=35 style='margin: 1px'></td><td style='font-size: 12px'>$icon_description</td>";
				$col++;
				if ($col == $colLength) {
					$lRow .= "</tr>";
					$col = 0;
				} else {
					$lRow .= "<td>&nbsp;</td>";
				}
			}
			if ($col < $colLength) $lRow .= "";
	}
	$sql_facilities->free_result();
	$sql_facilities->close();
	
	$lRow .= '</table>';
	$rRow .= '</table>';
	$facilities .= "<table width=100% ><tr valign=top>";
	$facilities .= "<td>$lRow</td><td>&nbsp;&nbsp;&nbsp;</td><td>$rRow</td>";
	$facilities.= "</tr></table>";
	
	//Replace Tags
	$template = str_replace('<!-- form_heading -->', $form_heading, $template);
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_Replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- billing1 -->', $billing_line1, $template);
	$template = str_replace('<!-- billing2 -->', $billing_line2, $template);
	$template = str_replace('<!-- billing3 -->', $billing_line3, $template);
	$template = str_replace('<!-- billing_code -->', $billing_code, $template);
	$template = str_replace('<!-- vat_number -->', $vat_number, $template);
	$template = str_replace('<!-- company_number -->', $company_number, $template);
	$template = str_replace('<!-- company_name -->', $company_name, $template);
	$template = str_replace('<!-- contacts_list -->', $contacts_list, $template);
	$template = str_replace('<!-- tel1 -->', $contact_tel1, $template);
	$template = str_replace('<!-- tel2 -->', $contact_tel2, $template);
	$template = str_replace('<!-- fax1 -->', $contact_fax1, $template);
	$template = str_replace('<!-- fax2 -->', $contact_fax2, $template);
	$template = str_replace('<!-- cell1 -->', $contact_cell1, $template);
	$template = str_replace('<!-- cell2 -->', $contact_cell2, $template);
	$template = str_replace('<!-- tel1_description -->', $contact_tel1_description, $template);
	$template = str_replace('<!-- tel2_description -->', $contact_tel2_description, $template);
	$template = str_replace('<!-- fax1_description -->', $contact_fax1_description, $template);
	$template = str_replace('<!-- fax2_description -->', $contact_fax2_description, $template);
	$template = str_replace('<!-- cell1_description -->', $contact_cell1_description, $template);
	$template = str_replace('<!-- cell2_description -->', $contact_cell2_description, $template);
	$template = str_replace('<!-- email -->', $contact_pemail, $template);
	$template = str_replace('<!-- email2 -->', $contact_pemail2, $template);
	$template = str_replace('<!-- email3 -->', $contact_pemail3, $template);
	$template = str_replace('<!-- postal1 -->', $postal_line1, $template);
	$template = str_replace('<!-- postal2 -->', $postal_line2, $template);
	$template = str_replace('<!-- postal3 -->', $postal_line3, $template);
	$template = str_replace('<!-- postal_code -->', $postal_code, $template);
	$template = str_replace('<!-- street1 -->', $street_line1, $template);
	$template = str_replace('<!-- street2 -->', $street_line2, $template);
	$template = str_replace('<!-- street3 -->', $street_line3, $template);
	$template = str_replace('<!-- image_list -->', $image_list, $template);
	$template = str_replace('<!-- short_description -->', $short_description, $template);
	$template = str_replace('<!-- long_description -->', $long_description, $template);
	$template = str_replace('<!-- facilities -->', $facilities, $template);
	$template = str_replace('<!-- province_options -->', $province_options, $template);
	$template = str_replace('<!-- country_options -->', $country_options, $template);
	$template = str_replace('<!-- province_options_status -->', $province_options_status, $template);
	$template = str_replace('<!-- town_options -->', $town_options, $template);
	$template = str_replace('<!-- town_option_status -->', $town_options_status, $template);
	$template = str_replace('<!-- suburb_options -->', $suburb_options, $template);
	$template = str_replace('<!-- suburb_options_status -->', $suburb_options_status, $template);
	$template = str_replace('<!-- gps_latitude -->', stripslashes($gps_latitude), $template);
	$template = str_replace('<!-- gps_longitude -->', stripslashes($gps_longitude), $template);
	$template = str_replace('<!-- pricing -->', $pricing, $template);
	$template = str_replace('<!-- child -->', $child_policy, $template);
	$template = str_replace('<!-- cancellation -->', $cancellation_policy, $template);
	$template = str_replace('<!-- map_type_none -->', $map_type_none, $template);
	$template = str_replace('<!-- map_type_google -->', $map_type_google, $template);
	$template = str_replace('<!-- map_type_image -->', $map_type_image, $template);
	$template = str_replace('<!-- directions -->', $directions, $template);
	$template = str_replace('<!-- price_options -->', $price_options, $template);
	$template = str_replace('<!-- category_prefix -->', $category_prefix, $template);
	$template = str_replace('<!-- category_suffix -->', $category_suffix, $template);
	$template = str_replace('<!-- restype_0 -->', $restype_options_0, $template);
	$template = str_replace('<!-- restype_1 -->', $restype_options_1, $template);
	$template = str_replace('<!-- restype_2 -->', $restype_options_2, $template);
	$template = str_replace('<!-- star_grading_0 -->', $star_grading_options_0, $template);
	$template = str_replace('<!-- star_grading_1 -->', $star_grading_options_1, $template);
	$template = str_replace('<!-- star_grading_2 -->', $star_grading_options_2, $template);
	$template = str_replace('<!-- poi_list -->', $landmark_list, $template);
	$template = str_Replace('<!-- grade_options -->', $grade_options, $template);
	$template = str_Replace('<!-- nightsbridge -->', $nightsbridge_id, $template);
	$template = str_Replace('<!-- prebook -->', $prebook_id, $template);
	$template = str_replace('<!-- advertiser_options -->', $advertiser_options, $template);
	$template = str_replace('<!-- website -->', $website_url, $template);
	$template = str_replace('<!-- change_list -->', $change_list, $template);
	
//	$template = str_replace('<!-- billing_hash -->', $billing_hash, $template);
//	$template = str_replace('<!-- public_contact_hash -->', $public_contact_hash, $template);
//	$template = str_replace('<!-- contacts_hash -->', $contacts_hash, $template);
//	$template = str_replace('<!-- image_hash -->', $image_hash, $template);
//	$template = str_replace('<!-- description_hash -->', $description_hash, $template);
//	$template = str_replace('<!-- gps_hash -->', $gps_hash, $template);
//	$template = str_replace('<!-- policy_hash -->', $policy_hash, $template);
//	$template = str_replace('<!-- direction_hash -->', $direction_hash, $template);
//	$template = str_replace('<!-- price_hash -->', $price_hash, $template);
//	$template = str_replace('<!-- icon_hash -->', $icon_hash, $template);
	
	$template = str_replace('<!-- api_key -->', $GLOBALS['api_key'], $template);
	$template = str_replace('<!-- default_image -->', $default_image, $template);
	$template = str_replace('<!-- button_name -->', 'updateData', $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}

function save_form() {
	//Vars
	$establishment_code = ctype_alnum($_GET['id'])&&strlen($_GET['id'])<8?$_GET['id']:'';
	$change_list = explode(',',$_POST['change_list3']);
	
	//Prepare statement - Insert changes
	$statement = "INSERT INTO nse_establishment_updates (establishment_code,field_name,field_value,update_date) VALUES (?,?,?,NOW())";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Update change
	$statement = "UPDATE nse_establishment_updates SET field_value=?, update_date=NOW() WHERE field_name=? && establishment_code=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - check change exists
	$statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE field_name=? && establishment_code=?";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	
	foreach($change_list as $field_name) {
		$entry_count = 0;
		if (substr($field_name,0,10) == 'facilities') {
			$field_value = '';
			foreach($_POST['facilities'] as $icon) {
				list($dummy, $field_icon) = explode('_',$field_name);
				if ($field_icon == $icon) $field_value = 'checked';
			}
		} else {
			$field_value = $_POST[$field_name];
		}
		
		$sql_check->bind_param('ss', $field_name, $establishment_code);
		$sql_check->execute();
		$sql_check->store_result();
		$sql_check->bind_result($entry_count);
		$sql_check->fetch();
		$sql_check->free_result();
		
		if ($entry_count > 0) {
			$sql_update->bind_param('sss', $field_value, $field_name, $establishment_code);
			$sql_update->execute();
		} else {
			$sql_insert->bind_param('sss', $establishment_code, $field_name, $field_value);
			$sql_insert->execute();
		}
	}
	
	$GLOBALS['log_tool']->write_entry("Requested changes to listing: {$_POST['establishment_name']} ($establishment_code)", $_SESSION['dbweb_user_id']);
	
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/estab_c/html/establishment_edit_save.html');
	
	echo $template;
	
}

function welcome() {
	//Vars
	$establishment_code = '';
	$establishment_name = '';
	
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/estab_c/html/welcome.html');
	
	
	//Get Establishment List
	$statement = "SELECT a.establishment_code, b.establishment_name
					FROM nse_user_establishments AS a
					JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE a.user_id=?";
	$sql_list = $GLOBALS['dbCon']->prepare($statement);
	$sql_list->bind_param('s', $_SESSION['dbweb_user_id']);
	$sql_list->execute();
	$sql_list->store_result();
	$sql_list->bind_result($establishment_code, $establishment_name);
	if ($sql_list->num_rows == 0) {
		$content = "<div id='no_estabs' >You have no access to establishment data. </div>";
	} else if ($sql_list->num_rows == 1) {
		$sql_list->fetch();
		$content = "<table width=100% id=icons><tr><td align=right><input type=button value='Close' class=cancel_button onClick=cI()> <input type=button value='Continue' class=ok_button onClick=document.location='/modules/estab_c/estab.php?f=edit&id=$establishment_code' /></td></tr></table>";
	} else {
		$content = "<table width=100% cellpadding=0 cellspacing=0 id=TRW><tr><th align=left>Select the establishment to edit</td><th></th></tr>";
		while ($sql_list->fetch()) {
			$content .= "<tr><td>$establishment_name</td><td align=right><input type=button value='Edit' class=ok_button onClick=document.location='/modules/estab_c/estab.php?f=edit&id=$establishment_code' /></td></tr>";
		}
		$content .= "<tr height=2><th colspan=10></th></tr></table><script>whack();</script> ";
	}
	$sql_list->free_result();
	$sql_list->close();
	
	$template = str_replace('<!-- establishment_list -->', $content, $template);
	
	echo $template;
}
?>