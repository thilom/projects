<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'landmarks';
$module_name = "Landmarks Manager";
$module_icon = 'landmarks-64.png';
$module_link = 'landmarks.php';
$window_width = '800';
$window_height = '700';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 's'; //a = all
$sub_levels = array();
$sub_levels['manage_estab']['name'] = 'Manage Landmarks';
$sub_levels['manage_estab']['subs']['add']['name'] = 'Create Landmarks';
$sub_levels['manage_estab']['subs']['view']['name'] = 'View Landmarks';
$sub_levels['manage_estab']['subs']['edit']['name'] = 'Edit Landmarks';
$sub_levels['manage_estab']['subs']['delete']['name'] = 'Delete Landmarks';



?>