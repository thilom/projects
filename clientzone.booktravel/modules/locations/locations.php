<?php
/**
 * Manage countries, provinces, towns and suburbs.
 *
 * -- Added 'Add town' button for countries without provinces - Thilo (2010)
 *
 * @todo Create 301 redirects for locations that have changed names.
 *
 * @author Thilo Muller (2009)
 * @package RVBus
 * @category locations
 */

//Vars
$function_white_list = array('locations_list', 'locations_edit', 'locations_add');
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';
$access = array('add_location'=>FALSE,
				'edit_location'=>FALSE,
				'delete_location'=>FALSE);

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/locations/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

//Prepare Statements
$statement = "SELECT function_id, parent_id, access FROM nse_user_access WHERE user_id=? && module_id=?";
$sql_access = $GLOBALS['dbCon']->prepare($statement);

//Get Access Rights
$sql_access->bind_param('ss', $_SESSION['dbweb_user_id'], $module_id);
$sql_access->execute();
$sql_access->bind_result($function, $parent_id, $user_access);
$sql_access->store_result();
while ($sql_access->fetch()) {
    if ($function == 'add' && 'manage_estab' == $parent_id && '1' == $user_access) $access['add_establishment'] = TRUE;
    if ($function == 'edit' && $parent_id == 'manage_estab' && $user_access == '1') $access['edit_establishment'] = TRUE;
    if ($function == 'delete' && $parent_id == 'manage_estab' && $user_access == '1') $access['delete_establishment'] = TRUE;
    if ($function == 'view' && $parent_id == 'manage_estab' && $user_access == '1') $access['view_establishment'] = TRUE;
    if ($function == 'changes' && $parent_id == 'manage_estab' && $user_access == '1') $access['manage_changes'] = TRUE;
}
$sql_access->close();

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/locations/html/locations.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/locations/html/locations.js'> </script>";

//Assemble Menu Structure
//echo "<script src=/shared/G2.js></script>";
//echo "<script src=/shared/tabs.js></script>";
//echo "<script>tabs(\"";
//echo "<a href=# id=Mq_1In onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Manage Establishments</a>";
//echo "\")</script>";
//echo "<span class=TabP>";
//echo "<p id=Mp_In onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
//if ($access['add_establishment']) {
//    echo "<a href='/modules/estab_s/estab.php?f=estab_add' onfocus=blur()>Add Establishment</a>";
//} else {
//    echo "<a href='/modules/estab_s/estab.php?f=no_access' onfocus=blur() style='color: silver'>Add Establishment</a>";
//}
//if ($access['edit_establishment'] || $access['view_establishment'] || $access['delete_establishment']) {
//    echo "<a href='/modules/estab_s/estab.php?f=estab_list' onfocus=blur()>Edit/Delete Establishments</a>";
//} else {
//    echo "<a href='/modules/estab_s/estab.php?f=no_access' onfocus=blur() style='color: silver'>Edit/Delete Establishments</a>";
//}
//if ($access['manage_changes']) {
//    echo "<a href='/modules/estab_s/estab.php?f=changes' onfocus=blur()>Manage Changes</a>";
//} else {
//    echo "<a href='/modules/estab_s/estab.php?f=no_access' onfocus=blur() style='color: silver'>Manage Changes</a>";
//}
//echo "</p>";
//echo "</span>";
//echo "<br />";

//Map
switch ($function_id) {
	case 'locations_edit':
		if (isset($_POST['updateLocation'])) {
			locations_edit_save();
		} else {
			locations_edit_form();
		}
		break;
	case 'locations_list':
		locations_list();
		break;
	case 'locations_add':
		if (isset($_POST['insertLocation'])) {
			locations_add_save();
		} else {
			locations_add_form();
		}
		break;
	default:
		welcome();
}

function locations_add_save() {
	$level = $_GET['level'];
	$fields = array('Geography','Arts and Culture','Introduction','History','Science','Entertainment','Sport','Did You Know');
	
	switch ($level) {
		case 'suburb':
			//Vars
			$town_id = $_GET['id'];
			$new_suburb = $_POST['suburb_name'];
			$pseudo_town = isset($_POST['pseudoTown'])?'1':'0';
			$base_directory = '';
			$town_name = '';
			$country_name = '';
			$country_id = '';
			$province_name = '';
			$province_id = '';
			$region_name = '';
			$region_id = '';
			$restype = '';
			$restype_name = '';
			
			//Update suburb
			$statement = "INSERT nse_location_suburb (suburb_name,pseudo_town, town_id) VALUES (?,?,?)";
			$sql_town_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_town_update->bind_param('sss', $new_suburb, $pseudo_town, $town_id);
			$sql_town_update->execute();
			$suburb_id = $sql_town_update->insert_id;
			
			//Update town content
			$statement = "INSERT INTO nse_location_suburb_content (language_code, paragraph_title, paragraph_content, suburb_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				$sql_content_insert->bind_param('sss', $field, $paragraph_content, $suburb_id);
				$sql_content_insert->execute();
			}
			$sql_content_insert->close();
			
			//Get Location Data
			$statement = "SELECT a.town_name, b.province_name, b.country_id, a.province_id
							FROM nse_location_town AS a
							JOIN nse_location_province AS b ON a.province_id=b.province_id
							WHERE a.town_id=?
							LIMIT 1";
			$sql_town = $GLOBALS['dbCon']->prepare($statement);
			$sql_town->bind_param('s', $town_id);
			$sql_town->execute();
			$sql_town->store_result();
			$sql_town->bind_result($town_name, $province_name, $country_id, $province_id);
			$sql_town->fetch();
			$sql_town->free_result();
			$sql_town->close();
			
			$statement = "SELECT country_name
							FROM nse_location_country_lang
							WHERE country_id=? && language_code='EN'";
			$sql_country = $GLOBALS['dbCon']->prepare($statement);
			$sql_country->bind_param('s', $country_id);
			$sql_country->execute();
			$sql_country->store_result();
			$sql_country->bind_result($country_name);
			$sql_country->fetch();
			$sql_country->free_result();
			$sql_country->close();
			
			$statement = "SELECT a.region_name, a.region_id
							FROM nse_location_region_lang AS a
							JOIN nse_location_region_towns AS b ON a.region_id=b.region_id
							WHERE b.country_id=?
							LIMIT 1";
			$sql_region = $GLOBALS['dbCon']->prepare($statement);
			$sql_region->bind_param('s', $country_id);
			$sql_region->execute();
			$sql_region->store_result();
			$sql_region->bind_result($region_name, $region_id);
			$sql_region->fetch();
			$sql_region->free_result();
			$sql_region->close();
			
			//Assemble Directory
			$base_directory = "/accommodation/<!-- restype -->/";
			$base_directory .= str_replace(' ', '_', strtolower($region_name)) . '/';
			$base_directory .= str_replace(' ', '_', strtolower($country_name)) . '/';
			$base_directory .= str_replace(' ', '_', strtolower($province_name));
			
			//Assemble Breadcrumbs
			$trail = "<a href='/index.php' style='color: black'>AA Travel Guides</a> &#187; ";
			$trail .= "<a href='/accommodation/<!-- restype -->/index.html' style='color: black'><!-- restype_name --></a> &#187; ";
			$trail .= "<a href='/accommodation/<!-- restype -->/". str_replace(' ', '_', strtolower($region_name)) . " /' style='color: black'>" . $region_name . "</a>  &#187; ";
			$trail .= "<a href='/accommodation/<!-- restype -->/". str_replace(' ', '_', strtolower($region_name)) . "/". str_replace(' ', '_', strtolower($country_name)) . "/' style='color: black'>" . $country_name . "</a>";
			
			
			//Update cron table
			$statement = "INSERT INTO nse_export
							(level, parent_id, base_dir, current_id, trail, region_id, country_id, province_id, town_id, suburb_id, export_category)
							VALUES
							('suburb', ?,?,?,?,?,?,?,?,?,?)";
			$sql_export = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "SELECT dir_link, breadcrumb FROM nse_export_data";
			$sql_restypes = $GLOBALS['dbCon']->prepare($statement);
			
			$sql_restypes->execute();
			$sql_restypes->store_result();
			$sql_restypes->bind_result($restype, $restype_name);
			while ($sql_restypes->fetch()) {
				$dir = str_replace('<!-- restype -->',$restype, $base_directory);
				$breadcrumb = str_replace('<!-- restype -->',$restype, $trail);
				$breadcrumb = str_replace('<!-- restype_name -->', $restype_name, $breadcrumb);
				
				$sql_export->bind_param('ssssssssss', $town_id, $dir, $suburb_id, $breadcrumb, $region_id, $country_id, $province_id, $town_id, $suburb_id, $restype);
				$sql_export->execute();
			}
			$sql_restypes->free_result();
			$sql_restypes->close();
			
			//Update export tables
			$statement = "UPDATE nse_export SET complete = NULL WHERE level='suburb' && current_id=?";
			$sql_export_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_export_update->bind_param('s', $suburb_id);
			$sql_export_update->execute();
			$sql_export_update->close();
			
			$statement = "UPDATE nse_export SET complete = NULL WHERE level='town' && current_id=?";
			$sql_export_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_export_update->bind_param('s', $town_id);
			$sql_export_update->execute();
			$sql_export_update->close();
			
			
			echo "<script>document.location='/modules/locations/locations.php?f=locations_list&level=suburb&id=$town_id'</script>";
			break;
		case 'town':
			//Vars
			$province_id = isset($_GET['id'])?$_GET['id']:'0';
			$country_id = isset($_GET['c_id'])?$_GET['c_id']:'';
			$new_town = $_POST['town_name'];

			//Get country_id
			if ($country_id != 0) {
				$statement = "SELECT country_id FROM nse_location_province WHERE province_id=?";
				$sql_country = $GLOBALS['dbCon']->prepare($statement);
				$sql_country->bind_param('s', $province_id);
				$sql_country->execute();
				$sql_country->store_result();
				$sql_country->bind_result($country_id);
				$sql_country->fetch();
				$sql_country->free_result();
				$sql_country->close();
			}
			
			//Update town
			$statement = "INSERT nse_location_town (town_name, province_id, country_id) VALUES (?,?,?)";
			$sql_town_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_town_update->bind_param('sss', $new_town, $province_id, $country_id);
			$sql_town_update->execute();
			$town_id = $sql_town_update->insert_id;
			
			//Update town content
			$statement = "INSERT INTO nse_location_town_content (language_code, paragraph_title, paragraph_content, town_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				$sql_content_insert->bind_param('sss', $field, $paragraph_content, $town_id);
				$sql_content_insert->execute();
			}
			$sql_content_insert->close();

			if (isset($_GET['c_id'])) {
				echo "<script>document.location='/modules/locations/locations.php?f=locations_list&level=town&id=$country_id&p=country'</script>";
			} else {
				echo "<script>document.location='/modules/locations/locations.php?f=locations_list&level=town&id=$province_id'</script>";
			}
			break;
		case 'province':
			//Vars
			$country_id = $_GET['id'];
			$new_province = $_POST['province_name'];
			
			
			//Update province
			$statement = "INSERT INTO nse_location_province (province_name, country_id) VALUES (?,?)";
			$sql_province_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_province_update->bind_param('ss', $new_province, $country_id);
			$sql_province_update->execute();
			$province_id = $sql_province_update->insert_id;
			
			//Update Province content
			$statement = "INSERT INTO nse_location_province_content (language_code, paragraph_title, paragraph_content, province_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				
				$sql_content_insert->bind_param('sss', $field, $paragraph_content, $province_id);
				$sql_content_insert->execute();
			}
			$sql_content_insert->close();
			
			echo "<script>document.location='/modules/locations/locations.php?f=locations_list&level=province&id=$country_id'</script>";
			break;
		case 'country':
			//Vars
			$new_country = $_POST['country_name'];
					
			//Insert Country
			$no_province = isset($_POST['no_provinces'])?'1':'0';
			$statement = "INSERT INTO nse_location_country (no_provinces) VALUES (?)";
			$sql_country_province = $GLOBALS['dbCon']->prepare($statement);
			$sql_country_province->bind_param('s', $no_province);
			$sql_country_province->execute();
			$country_id = $sql_country_province->insert_id;
			
			//Insert country Lang
			$statement = "INSERT INTO nse_location_country_lang (country_id, country_name, language_code) VALUES (?,?,'EN')";
			$sql_country = $GLOBALS['dbCon']->prepare($statement);
			$sql_country->bind_param('ss', $country_id, $new_country);
			$sql_country->execute();
			
			//Update country content
			$statement = "INSERT INTO nse_location_country_content (language_code, paragraph_title, paragraph_content, country_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				
				$sql_content_insert->bind_param('sss', $field, $paragraph_content, $country_id);
				$sql_content_insert->execute();
			}
			$sql_content_insert->close();
			echo "<script>document.location='/modules/locations/locations.php?f=locations_list'</script>";
			break;
	}
}

function locations_add_form() {
	$level = $_GET['level'];
	$province_id = isset($_GET['id'])?$_GET['id']:'0';
	$country_id = isset($_GET['c_id'])?$_GET['c_id']:'0';
	$country_name = '';
	
	
	switch ($level) {
		case 'suburb':
			$town_id = $_GET['id'];
			$province_name = '';
			$town_name = '';
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/locations/html/new_suburb_form.html');
			
			//Get country Name
			$statement = "SELECT b.province_name, c.country_name, a.town_name
							FROM nse_location_town AS a
							JOIN nse_location_province AS b ON a.province_id=b.province_id
							JOIN nse_location_country_lang AS c ON b.country_id=c.country_id
							WHERE a.town_id=?
							LIMIT 1";
			$sql_province = $GLOBALS['dbCon']->prepare($statement);
			$sql_province->bind_param('s', $town_id);
			$sql_province->execute();
			$sql_province->store_result();
			$sql_province->bind_result($province_name, $country_name, $town_name);
			$sql_province->fetch();
			$sql_province->free_result();
			$sql_province->close();
			
			//Replace Tags
			$template = str_replace('<!-- province_name -->', $province_name, $template);
			$template = str_replace('<!-- country_name -->', $country_name, $template);
			$template = str_replace('<!-- town_name -->', $town_name, $template);
			$template = str_replace('<!-- town_id -->', $town_id, $template);
			
			break;
		case 'town':
			$province_name = '';
			$country_name = '';
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/locations/html/new_town_form.html');
			
			//Get country Name
			if ($country_id != 0) {
				$statement = "SELECT country_name
						FROM nse_location_country_lang
						WHERE country_id=?
						LIMIT 1";
				$sql_province = $GLOBALS['dbCon']->prepare($statement);
				$sql_province->bind_param('s', $country_id);
				$sql_province->execute();
				$sql_province->store_result();
				$sql_province->bind_result($country_name);
				$sql_province->fetch();
				$sql_province->free_result();
				$sql_province->close();
				//Replace Tags
				$template = str_replace('<!-- province_name -->', $province_name, $template);
				$template = str_replace('<!-- country_name -->', $country_name, $template);
				$template = str_replace('<!-- province_id -->', $province_id, $template);

			} else {
				$statement = "SELECT a.province_name, country_name
								FROM nse_location_province AS a
								JOIN nse_location_country_lang AS b ON a.country_id=b.country_id
								WHERE a.province_id=?
								LIMIT 1";
				$sql_province = $GLOBALS['dbCon']->prepare($statement);
				$sql_province->bind_param('s', $province_id);
				$sql_province->execute();
				$sql_province->store_result();
				$sql_province->bind_result($province_name, $country_name);
				$sql_province->fetch();
				$sql_province->free_result();
				$sql_province->close();
				
				//Replace Tags
				$template = str_replace('<!-- province_name -->', $province_name, $template);
				$template = str_replace('<!-- country_name -->', $country_name, $template);
				$template = str_replace('<!-- province_id -->', $province_id, $template);
			}
			
			
			
			break;
		case 'province':
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/locations/html/new_province_form.html');
			
			//Get country Name
			$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=? LIMIT 1";
			$sql_province = $GLOBALS['dbCon']->prepare($statement);
			$sql_province->bind_param('s', $country_id);
			$sql_province->execute();
			$sql_province->store_result();
			$sql_province->bind_result($country_name);
			$sql_province->fetch();
			$sql_province->free_result();
			$sql_province->close();
			
			//Replace Tags
			$template = str_replace('<!-- country_name -->', $country_name, $template);
			$template = str_replace('<!-- country_id -->', $country_id, $template);
			
			break;
		default:
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/locations/html/new_country_form.html');
	}
	
	echo $template;
}

function locations_edit_save() {
	//Vars
	$level = $_GET['level'];
	$fields = array('Geography','Arts and Culture','Introduction','History','Science','Entertainment','Sport','Did You Know');
	
	
	switch ($level) {
		case 'country':
			//Vars
			$country_id = $_GET['id'];
			$new_country = $_POST['country_name'];
			$old_country = '';
			
			//Get current country
			$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=?";
			$sql_country_old = $GLOBALS['dbCon']->prepare($statement);
			$sql_country_old->bind_param('s', $country_id);
			$sql_country_old->execute();
			$sql_country_old->store_result();
			$sql_country_old->bind_result($old_country);
			$sql_country_old->free_result();
			$sql_country_old->close();
			
			//Update Country
			$no_province = isset($_POST['no_provinces'])?'1':'0';
			$statement = "UPDATE nse_location_country SET no_provinces=? WHERE country_id=?";
			$sql_country_province = $GLOBALS['dbCon']->prepare($statement);
			$sql_country_province->bind_param('ss', $no_province, $country_id);
			$sql_country_province->execute();
			
			//Update country Lang
			$statement = "UPDATE nse_location_country_lang SET country_name=? WHERE country_id=?";
			$sql_country = $GLOBALS['dbCon']->prepare($statement);
			$sql_country->bind_param('ss', $new_country, $country_id);
			$sql_country->execute();
			
			//Update country content
			$statement = "SELECT COUNT(*) FROM nse_location_country_content WHERE language_code='EN' && country_id=? && paragraph_title=?";
			$sql_content_check = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "INSERT INTO nse_location_country_content (language_code, paragraph_title, paragraph_content, country_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "UPDATE nse_location_country_content SET paragraph_content=? WHERE language_code='EN' && country_id=? && paragraph_title=?";
			$sql_content_update = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				
				$sql_content_check->bind_param('ss', $country_id, $field);
				$sql_content_check->execute();
				$sql_content_check->store_result();
				$sql_content_check->bind_result($field_counter);
				$sql_content_check->fetch();
				$sql_content_check->free_result();
				
				if ($field_counter == 0) {
					$sql_content_insert->bind_param('sss', $field, $paragraph_content, $country_id);
					$sql_content_insert->execute();
				} else {
					$sql_content_update->bind_param('sss', $paragraph_content, $country_id, $field);
					$sql_content_update->execute();
				}
			}
			$sql_content_check->close();
			$sql_content_insert->close();
			$sql_content_update->close();
			echo "<script>document.location='/modules/locations/locations.php?f=locations_list'</script>";
			break;
			
		case 'province':
			//Vars
			$province_id = $_GET['id'];
			$old_province = '';
			$new_province = $_POST['province_name'];
			$country_id = '';
			
			
			//Got current province
			$statement = "SELECT province_name, country_id FROM nse_location_province WHERE province_id=?";
			$sql_province_old = $GLOBALS['dbCon']->prepare($statement);
			$sql_province_old->bind_param('s', $province_id);
			$sql_province_old->execute();
			$sql_province_old->store_result();
			$sql_province_old->bind_result($old_province, $country_id);
			$sql_province_old->fetch();
			$sql_province_old->free_result();
			$sql_province_old->close();
			
			//Update province
			$statement = "UPDATE nse_location_province SET province_name=? WHERE province_id=?";
			$sql_province_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_province_update->bind_param('ss', $new_province, $province_id);
			$sql_province_update->execute();
			
			//Update country content
			$statement = "SELECT COUNT(*) FROM nse_location_province_content WHERE language_code='EN' && province_id=? && paragraph_title=?";
			$sql_content_check = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "INSERT INTO nse_location_province_content (language_code, paragraph_title, paragraph_content, province_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "UPDATE nse_location_province_content SET paragraph_content=? WHERE language_code='EN' && province_id=? && paragraph_title=?";
			$sql_content_update = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				
				$sql_content_check->bind_param('ss', $province_id, $field);
				$sql_content_check->execute();
				$sql_content_check->store_result();
				$sql_content_check->bind_result($field_counter);
				$sql_content_check->fetch();
				$sql_content_check->free_result();
				
				if ($field_counter == 0) {
					$sql_content_insert->bind_param('sss', $field, $paragraph_content, $province_id);
					$sql_content_insert->execute();
				} else {
					$sql_content_update->bind_param('sss', $paragraph_content, $province_id, $field);
					$sql_content_update->execute();
				}
			}
			$sql_content_check->close();
			$sql_content_insert->close();
			$sql_content_update->close();
			
			//Update export tables
//			if (strtolower($old_province) != strtolower($new_province)) {
//				$statement = "SELECT ";
//			}
			
			echo "<script>document.location='/modules/locations/locations.php?f=locations_list&level=province&id=$country_id'</script>";
			break;
		case 'town':
			//Vars
			$town_id = $_GET['id'];
			$old_town = '';
			$new_town = $_POST['town_name'];
			$province_id = '';
			
			
			//Got current town
			$statement = "SELECT town_name, province_id FROM nse_location_town WHERE town_id=?";
			$sql_town_old = $GLOBALS['dbCon']->prepare($statement);
			$sql_town_old->bind_param('s', $town_id);
			$sql_town_old->execute();
			$sql_town_old->store_result();
			$sql_town_old->bind_result($old_town, $province_id);
			$sql_town_old->fetch();
			$sql_town_old->free_result();
			$sql_town_old->close();
			
			//Update town
			$statement = "UPDATE nse_location_town SET town_name=? WHERE town_id=?";
			$sql_town_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_town_update->bind_param('ss', $new_town, $town_id);
			$sql_town_update->execute();
			
			//Update town content
			$statement = "SELECT COUNT(*) FROM nse_location_town_content WHERE language_code='EN' && town_id=? && paragraph_title=?";
			$sql_content_check = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "INSERT INTO nse_location_town_content (language_code, paragraph_title, paragraph_content, town_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "UPDATE nse_location_town_content SET paragraph_content=? WHERE language_code='EN' && town_id=? && paragraph_title=?";
			$sql_content_update = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				
				$sql_content_check->bind_param('ss', $town_id, $field);
				$sql_content_check->execute();
				$sql_content_check->store_result();
				$sql_content_check->bind_result($field_counter);
				$sql_content_check->fetch();
				$sql_content_check->free_result();
				
				if ($field_counter == 0) {
					$sql_content_insert->bind_param('sss', $field, $paragraph_content, $town_id);
					$sql_content_insert->execute();
				} else {
					$sql_content_update->bind_param('sss', $paragraph_content, $town_id, $field);
					$sql_content_update->execute();
				}
			}
			$sql_content_check->close();
			$sql_content_insert->close();
			$sql_content_update->close();
			
			//Update export tables
//			if (strtolower($old_province) != strtolower($new_province)) {
//				$statement = "SELECT ";
//			}
			
			echo "<script>document.location='/modules/locations/locations.php?f=locations_list&level=town&id=$province_id'</script>";
			break;
		case 'suburb':
			//Vars
			$suburb_id = $_GET['id'];
			$old_suburb = '';
			$new_suburb = $_POST['suburb_name'];
			$town_id = '';
			$pseudo_town = isset($_POST['pseudoTown'])?'1':'0';
			
			
			//Got current suburb
			$statement = "SELECT suburb_name, town_id FROM nse_location_suburb WHERE suburb_id=?";
			$sql_town_old = $GLOBALS['dbCon']->prepare($statement);
			$sql_town_old->bind_param('s', $suburb_id);
			$sql_town_old->execute();
			$sql_town_old->store_result();
			$sql_town_old->bind_result($old_suburb, $town_id);
			$sql_town_old->fetch();
			$sql_town_old->free_result();
			$sql_town_old->close();
			
			//Update suburb
			$statement = "UPDATE nse_location_suburb SET suburb_name=?, pseudo_town=? WHERE suburb_id=?";
			$sql_town_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_town_update->bind_param('sss', $new_suburb, $pseudo_town, $suburb_id);
			$sql_town_update->execute();
			
			//Update town content
			$statement = "SELECT COUNT(*) FROM nse_location_suburb_content WHERE language_code='EN' && suburb_id=? && paragraph_title=?";
			$sql_content_check = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "INSERT INTO nse_location_suburb_content (language_code, paragraph_title, paragraph_content, suburb_id) VALUES ('EN',?,?,?)";
			$sql_content_insert = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "UPDATE nse_location_suburb_content SET paragraph_content=? WHERE language_code='EN' && suburb_id=? && paragraph_title=?";
			$sql_content_update = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($fields as $field) {
				$field_counter = 0;
				$paragraph_content = $_POST[str_replace(' ', '_', $field)];
				
				$sql_content_check->bind_param('ss', $suburb_id, $field);
				$sql_content_check->execute();
				$sql_content_check->store_result();
				$sql_content_check->bind_result($field_counter);
				$sql_content_check->fetch();
				$sql_content_check->free_result();
				
				if ($field_counter == 0) {
					$sql_content_insert->bind_param('sss', $field, $paragraph_content, $suburb_id);
					$sql_content_insert->execute();
				} else {
					$sql_content_update->bind_param('sss', $paragraph_content, $suburb_id, $field);
					$sql_content_update->execute();
				}
			}
			$sql_content_check->close();
			$sql_content_insert->close();
			$sql_content_update->close();
			
			//Update export tables
//			if (strtolower($old_province) != strtolower($new_province)) {
//				$statement = "SELECT ";
//			}
			
			echo "<script>document.location='/modules/locations/locations.php?f=locations_list&level=suburb&id=$town_id'</script>";
			break;
	}
	
	
}

function locations_edit_form() {
	//Vars
	$level = $_GET['level'];
	$id = $_GET['id'];
	$province_check = '';
	$no_provinces = '';
	$fields = array('Geography','Arts and Culture','Introduction','History','Science','Entertainment','Sport','Did You Know');
	
	//Get Template
	switch ($level) {
		case 'country':
			//Vars
			$country_name = '';
			$paragraph_title = '';
			$paragraph_content = '';
			
			$country_id = $_GET['id'];
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/locations/html/country_form.html");
			
			//Get Country Name
			$statement = "SELECT a.country_name, b.no_provinces
							FROM nse_location_country_lang AS a
							JOIN nse_location_country AS b ON a.country_id=b.country_id
							WHERE a.country_id=?";
			$sql_country = $GLOBALS['dbCon']->prepare($statement);
			$sql_country->bind_param('s', $country_id);
			$sql_country->execute();
			$sql_country->store_result();
			$sql_country->bind_result($country_name, $no_provinces);
			$sql_country->fetch();
			$sql_country->free_result();
			$sql_country->close();
			
			//Get country Data
			$statement = "SELECT paragraph_title, paragraph_content FROM nse_location_country_content WHERE language_code='EN' && country_id=?";
			$sql_content = $GLOBALS['dbCon']->prepare($statement);
			$sql_content->bind_param('s', $country_id);
			$sql_content->execute();
			$sql_content->store_result();
			$sql_content->bind_result($paragraph_title, $paragraph_content);
			while ($sql_content->fetch()) {
				$template = str_replace("<!-- $paragraph_title -->", $paragraph_content, $template);
			}
			$sql_content->free_result();
			$sql_content->close();
			
			//Replace tags
			$province_check = $no_provinces=='1'?'checked':'';
			$template = str_replace('<!-- country_name -->', $country_name, $template);
			$template = str_replace('<!-- province_check -->', $province_check, $template);
			foreach ($fields as $f) {
				$template = str_replace("<!-- $f -->", '', $template);
			}
			
			break;
		case 'province':
			$province_id = $_GET['id'];
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/locations/html/province_form.html");
			$province_name = '';
			$country_name = '';
			$country_id = '';
			
			//Get province & coutry data
			$statement = "SELECT a.province_name, b.country_name, b.country_id
							FROM nse_location_province AS a
							JOIN nse_location_country_lang AS b ON a.country_id=b.country_id
							WHERE a.province_id=?
							LIMIT 1";
			$sql_province = $GLOBALS['dbCon']->prepare($statement);
			$sql_province->bind_param('s', $province_id);
			$sql_province->execute();
			$sql_province->store_result();
			$sql_province->bind_result($province_name, $country_name, $country_id);
			$sql_province->fetch();
			$sql_province->free_result();
			$sql_province->close();

			//Get province Data
			$statement = "SELECT paragraph_title, paragraph_content FROM nse_location_province_content WHERE language_code='EN' && province_id=?";
			$sql_content = $GLOBALS['dbCon']->prepare($statement);
			$sql_content->bind_param('s', $province_id);
			$sql_content->execute();
			$sql_content->store_result();
			$sql_content->bind_result($paragraph_title, $paragraph_content);
			while ($sql_content->fetch()) {
				$template = str_replace("<!-- $paragraph_title -->", $paragraph_content, $template);
			}
			$sql_content->free_result();
			$sql_content->close();
			
			//Replace TAgs
			$template = str_replace('<!-- province_name -->', $province_name, $template);
			$template = str_replace('<!-- country_name -->', $country_name, $template);
			$template = str_replace('<!-- country_id -->', $country_id, $template);
			foreach ($fields as $f) {
				$template = str_replace("<!-- $f -->", '', $template);
			}
			
			break;
		case 'town':
			//Vars
			$town_id = $_GET['id'];
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/locations/html/town_form.html");
			$town_name = '';
			
			//Get town, province & coutry data
			$statement = "SELECT a.town_name, b.province_name, c.country_name, c.country_id, b.province_id
							FROM nse_location_town AS a
							JOIN nse_location_province AS b ON a.province_id=b.province_id
							JOIN nse_location_country_lang AS c ON a.country_id=b.country_id
							WHERE a.town_id=?
							LIMIT 1";
			$sql_town = $GLOBALS['dbCon']->prepare($statement);
			$sql_town->bind_param('s', $town_id);
			$sql_town->execute();
			$sql_town->store_result();
			$sql_town->bind_result($town_name, $province_name, $country_name, $country_id, $province_id);
			$sql_town->fetch();
			$sql_town->free_result();
			$sql_town->close();
			
			//Get province Data
			$statement = "SELECT paragraph_title, paragraph_content FROM nse_location_town_content WHERE language_code='EN' && town_id=?";
			$sql_content = $GLOBALS['dbCon']->prepare($statement);
			$sql_content->bind_param('s', $town_id);
			$sql_content->execute();
			$sql_content->store_result();
			$sql_content->bind_result($paragraph_title, $paragraph_content);
			while ($sql_content->fetch()) {
				$template = str_replace("<!-- $paragraph_title -->", $paragraph_content, $template);
			}
			$sql_content->free_result();
			$sql_content->close();
			
			//Replace Tags
			$template = str_replace('<!-- town_name -->', $town_name, $template);
			$template = str_replace('<!-- province_name -->', $province_name, $template);
			$template = str_replace('<!-- country_name -->', $country_name, $template);
			$template = str_replace('<!-- province_id -->', $province_id, $template);
			foreach ($fields as $f) {
				$template = str_replace("<!-- $f -->", '', $template);
			}
			
			break;
			case 'suburb':
			//Vars
			$suburb_id = $_GET['id'];
			$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/locations/html/suburb_form.html");
			$suburb_name = '';
			$pseudo_town = '';
			
			//Get suburb, town, province & coutry data
			$statement = "SELECT a.suburb_name, b.town_name, c.province_name, d.country_name, b.town_id, a.pseudo_town
							FROM nse_location_suburb AS a
							JOIN nse_location_town AS b ON a.town_id=b.town_id
							JOIN nse_location_province AS c ON b.province_id=c.province_id
							JOIN nse_location_country_lang AS d ON b.country_id=c.country_id
							WHERE a.suburb_id=?
							LIMIT 1";
			$sql_town = $GLOBALS['dbCon']->prepare($statement);
			$sql_town->bind_param('s', $suburb_id);
			$sql_town->execute();
			$sql_town->store_result();
			$sql_town->bind_result($suburb_name, $town_name, $province_name, $country_name, $town_id, $pseudo_town);
			$sql_town->fetch();
			$sql_town->free_result();
			$sql_town->close();
			
			//Get province Data
			$statement = "SELECT paragraph_title, paragraph_content FROM nse_location_suburb_content WHERE language_code='EN' && suburb_id=?";
			$sql_content = $GLOBALS['dbCon']->prepare($statement);
			$sql_content->bind_param('s', $suburb_id);
			$sql_content->execute();
			$sql_content->store_result();
			$sql_content->bind_result($paragraph_title, $paragraph_content);
			while ($sql_content->fetch()) {
				$template = str_replace("<!-- $paragraph_title -->", $paragraph_content, $template);
			}
			$sql_content->free_result();
			$sql_content->close();
			
			$pseudo_town = $pseudo_town=='1'?'checked':'';
			
			//Replace Tags
			$template = str_replace('<!-- suburb_name -->', stripslashes($suburb_name), $template);
			$template = str_replace('<!-- town_name -->', $town_name, $template);
			$template = str_replace('<!-- province_name -->', $province_name, $template);
			$template = str_replace('<!-- country_name -->', $country_name, $template);
			$template = str_replace('<!-- town_id -->', $town_id, $template);
			$template = str_replace('<!-- pseudoTown -->', $pseudo_town, $template);
			foreach ($fields as $f) {
				$template = str_replace("<!-- $f -->", '', $template);
			}
			
			break;
	}
	
	
	//Display
	echo $template;
}

function locations_list() {
	//Vars
	$country_id = '';
	$country_name = '';
	$nLine = '';
	$col_heading = '';
	
	//Get templates
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/locations/html/locations_list.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
	$lTemplate = $matches[0][0];
	
	//Get list
	if (isset($_GET['level']) && $_GET['level'] == 'province') {
		$country_id=$_GET['id'];
		$province_id = '';
		$province_name = '';
		
		//Add 'Back'
		$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list'>.. (Up)</a>", $lTemplate);
		$nLine .= $line;
		
		//Add country
		$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=?";
		$sql_country = $GLOBALS['dbCon']->prepare($statement);
		$sql_country->bind_param('s', $country_id);
		$sql_country->execute();
		$sql_country->store_result();
		$sql_country->bind_result($country_name);
		$sql_country->fetch();
		$sql_country->free_result();
		$sql_country->close();
		$button = "<input type=button value='Add Province' class=add_button onClick='document.location=\"/modules/locations/locations.php?f=locations_add&level=province&id=$country_id\"' />";
		$line = str_replace('<!-- location -->', "<a >-$country_name</a>", $lTemplate);
		$line = str_replace('<!-- buttons -->', $button, $line);
		$nLine .= $line;
		
		$statement = "SELECT province_name, province_id
						FROM nse_location_province
						WHERE country_id=?
						ORDER BY province_name";
		$sql_province = $GLOBALS['dbCon']->prepare($statement);
		$sql_province->bind_param('s', $country_id);
		$sql_province->execute();
		$sql_province->store_result();
		$sql_province->bind_result($province_name, $province_id);
		while ($sql_province->fetch()) {
			$button = "<input type=button value='Edit' class=edit_button onClick='document.location=\"/modules/locations/locations.php?f=locations_edit&level=province&id=$province_id\"' />";
			$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list&level=town&id=$province_id' style='padding-left: 10px'>+$province_name</a>", $lTemplate);
			$line = str_replace('<!-- buttons -->', $button, $line);
			$nLine .= $line;
		}
		$sql_province->free_result();
		$sql_province->close();
		$col_heading = "Province";
	} else if (isset($_GET['level']) && $_GET['level'] == 'town') {
		//Vars
		$province_id = $_GET['id'];
		$town_name = '';
		$town_id = '';
		$province_name = '';
		$country_parent = isset($_GET['p'])?'1':'0';
		
		//Add province, Country and back
		if ($country_parent == '1') {
			$statement = "SELECT b.country_name, b.country_id
							FROM nse_location_country_lang AS b
							WHERE b.country_id=?";
			$sql_province = $GLOBALS['dbCon']->prepare($statement);
			$sql_province->bind_param('s', $province_id);
			$sql_province->execute();
			$sql_province->store_result();
			$sql_province->bind_result($country_name, $country_id);
			$sql_province->fetch();
			$sql_province->free_result();
			$sql_province->close();
			$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list'>.. (Up)</a>", $lTemplate);
			$nLine .= $line;
			$button = "<input type=button value='Add Town' class=add_button onClick='document.location=\"/modules/locations/locations.php?f=locations_add&level=town&c_id=$country_id\"' />";
			$line = str_replace('<!-- location -->', "<a >-$country_name</a>", $lTemplate);
			$line = str_replace('<!-- buttons -->', $button, $line);
			$nLine .= $line;
		} else {
			$statement = "SELECT a.province_name, b.country_name, b.country_id
							FROM nse_location_province AS a
							JOIN nse_location_country_lang AS b ON a.country_id=b.country_id
							WHERE a.province_id=?";
		
			$sql_province = $GLOBALS['dbCon']->prepare($statement);
			$sql_province->bind_param('s', $province_id);
			$sql_province->execute();
			$sql_province->store_result();
			$sql_province->bind_result($province_name, $country_name, $country_id);
			$sql_province->fetch();
			$sql_province->free_result();
			$sql_province->close();
			$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list&level=province&id=$country_id'>.. (Up)</a>", $lTemplate);
			$nLine .= $line;
			$line = str_replace('<!-- location -->', "<a >-$country_name</a>", $lTemplate);
			$nLine .= $line;
			$button = "<input type=button value='Add Town' class=add_button onClick='document.location=\"/modules/locations/locations.php?f=locations_add&level=town&id=$province_id\"' />";
			$line = str_replace('<!-- location -->', "<a style='padding-left: 10px' >-$province_name</a>", $lTemplate);
			$line = str_replace('<!-- buttons -->', $button, $line);
			$nLine .= $line;
		}
		
		//Get Towns
		if ($country_parent == '1') {
			$statement = "SELECT town_name, town_id
							FROM nse_location_town
							WHERE country_id=?
							ORDER BY town_name";
		} else {
			$statement = "SELECT town_name, town_id
							FROM nse_location_town
							WHERE province_id=?
							ORDER BY town_name";
		}
		$sql_town = $GLOBALS['dbCon']->prepare($statement);
		$sql_town->bind_param('s', $province_id);
		$sql_town->execute();
		$sql_town->store_result();
		$sql_town->bind_result($town_name, $town_id);
		while ($sql_town->fetch()) {
			$button = "<input type=button value='Edit' class=edit_button onClick='document.location=\"/modules/locations/locations.php?f=locations_edit&level=town&id=$town_id\"' />";
			$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list&level=suburb&id=$town_id' style='padding-left: 20px'>+$town_name</a>", $lTemplate);
			$line = str_replace('<!-- buttons -->', $button, $line);
			$nLine .= $line;
		}
		$sql_town->free_result();
		$sql_town->close();
		$col_heading = "Towns";
	} else if (isset($_GET['level']) && $_GET['level'] == 'suburb') {
		//Vars
		$town_id = $_GET['id'];
		$suburb_name = '';
		$suburb_id = '';
		
		//Add suburb, province, Country and back
		$statement = "SELECT b.province_name, c.country_name, b.province_id, a.town_name, c.country_id
						FROM nse_location_town AS a
						LEFT JOIN nse_location_province AS b ON a.province_id=b.province_id
						JOIN nse_location_country_lang AS c ON a.country_id=c.country_id
						WHERE a.town_id=?
						LIMIT 1";
		$sql_province = $GLOBALS['dbCon']->prepare($statement);
		$sql_province->bind_param('s', $town_id);
		$sql_province->execute();
		$sql_province->store_result();
		$sql_province->bind_result($province_name, $country_name, $province_id, $town_name, $country_id);
		$sql_province->fetch();
		$sql_province->free_result();
		$sql_province->close();
		if (!empty($province_id)) {
			$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list&level=town&id=$province_id'>.. (Up)</a>", $lTemplate);
		} else {
			$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list&level=town&id=$country_id&p=country'>.. (Up)</a>", $lTemplate);
		}
		$nLine .= $line;
		$line = str_replace('<!-- location -->', "<a >-$country_name</a>", $lTemplate);
		$nLine .= $line;
		if (!empty($province_name)) {
			$line = str_replace('<!-- location -->', "<a style='padding-left: 10px' >-$province_name</a>", $lTemplate);
			$nLine .= $line;
		}
		$button = "<input type=button value='Add Suburb' class=add_button onClick='document.location=\"/modules/locations/locations.php?f=locations_add&level=suburb&id=$town_id\"' />";
		$line = str_replace('<!-- location -->', "<a style='padding-left: 20px' >-$town_name</a>", $lTemplate);
		$line = str_replace('<!-- buttons -->', $button, $line);
		$nLine .= $line;
		
		//Get Towns
		$statement = "SELECT suburb_name, suburb_id
						FROM nse_location_suburb
						WHERE town_id=?
						ORDER BY suburb_name";
		$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb->bind_param('s', $town_id);
		$sql_suburb->execute();
		$sql_suburb->store_result();
		$sql_suburb->bind_result($suburb_name, $suburb_id);
		if ($sql_suburb->num_rows > 0) {
			while ($sql_suburb->fetch()) {
				$button = "<input type=button value='Edit' class=edit_button onClick='document.location=\"/modules/locations/locations.php?f=locations_edit&level=suburb&id=$suburb_id\"' />";
				$line = str_replace('<!-- location -->', "<a style='padding-left: 30px'>-$suburb_name</a>", $lTemplate);
				$line = str_replace('<!-- buttons -->', $button, $line);
				$nLine .= $line;
			}
		} else {
			$line = str_replace('<!-- location -->', "<a style='padding-left: 30px'> -- No Suburbs --</a>", $lTemplate);
			$nLine .= $line;
		}
		$sql_suburb->free_result();
		$sql_suburb->close();
		$col_heading = "Suburbs";
		
	} else { //Get list of countries
		//Vars
		$no_provinces = '';
		
		$nLine = '';
		
		$button = "<input type=button value='Add Country' class=add_button onClick='document.location=\"/modules/locations/locations.php?f=locations_add&level=country\"' />";
		$line = str_replace('<!-- location -->', "", $lTemplate);
		$line = str_replace('<!-- buttons -->', $button, $line);
		$nLine .= $line;
		
		$statement = "SELECT a.country_id, b.country_name, a.no_provinces
						FROM nse_location_country AS a
						JOIN nse_location_country_lang AS b ON a.country_id=b.country_id
						ORDER BY b.country_name";
		$sql_country = $GLOBALS['dbCon']->prepare($statement);
		$sql_country->execute();
		$sql_country->store_result();
		$sql_country->bind_result($country_id, $country_name, $no_provinces);
		while ($sql_country->fetch()) {
			$button = "<input type=button value='Edit' class=edit_button onClick='document.location=\"/modules/locations/locations.php?f=locations_edit&level=country&id=$country_id\"' />";
			if ($no_provinces == '0') {
				$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list&level=province&id=$country_id' title='Click to Expand' >+$country_name</a>", $lTemplate);
			} else {
				$line = str_replace('<!-- location -->', "<a href='/modules/locations/locations.php?f=locations_list&level=town&id=$country_id&p=country' title='Click to Expand' >+$country_name</a>", $lTemplate);
			}
			$line = str_replace('<!-- buttons -->', $button, $line);
			$nLine .= $line;
		}
		$sql_country->free_result();
		$sql_country->close();
		$col_heading = 'Country';
	}
	
	//Replace Tags
	$template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
	$template = str_replace('<!-- col_heading -->', $col_heading, $template);
	$template = str_replace('#$%', "\n", $template);
	
	//Display
	echo $template;
	
	
}

function welcome() {
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/locations/html/welcome.html');
	
	echo $template;
}

?>