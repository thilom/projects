<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'locations';
$module_name = "Locations Manager";
$module_icon = 'locations-64.png';
$module_link = 'locations.php';
$window_width = '800';
$window_height = '700';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 's'; //a = all
$sub_levels = array();
$sub_levels['manage_estab']['name'] = 'Manage Locations';
$sub_levels['manage_estab']['subs']['add']['name'] = 'Create Locations';
$sub_levels['manage_estab']['subs']['view']['name'] = 'View Locations';
$sub_levels['manage_estab']['subs']['edit']['name'] = 'Edit Locations';
$sub_levels['manage_estab']['subs']['delete']['name'] = 'Delete Locations';



?>