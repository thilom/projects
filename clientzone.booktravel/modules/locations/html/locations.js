button_ids = new Array('intro','geo','art','history','science','enter','sport','did');

function flipDiv(id) {
	for (i=0; i<button_ids.length; i++) {
		document.getElementById(button_ids[i] + 'Button').className = 'button_close';
		document.getElementById(button_ids[i] + 'Div').className = 'area_hide';
	}
	
	document.getElementById(id + 'Button').className = 'button_open';
	document.getElementById(id + 'Div').className = 'area_show';
}