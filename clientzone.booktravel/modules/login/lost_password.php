<?php
/**
 * Lost password recovery
 *
 * @author Thilo Muller(2009)
 */

require_once '../../settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/modules/login/tcrypt.class.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';


if (isset($_POST['recover_password'])) {
	if (recover_count()) {
		recover_pass();
	} else {
		recover_limit();
	}
	
} else {
	recover_form();
}

function recover_form() {
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/login/html/login_forgotten_pass.html');
	
	echo $template;
}

function recover_limit() {
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/login/html/login_forgotten_pass_failed.html');
	$template = str_replace('<!-- message -->', 'Your password recovery limit has been reached.', $template);
	
	echo $template;
}

function recover_pass() {
	//Vars
	$template_file = $_SERVER['DOCUMENT_ROOT'] . '/modules/login/html/login_forgotten_pass_complete.html';
	$email = $_POST['email_txt'];
	$encrypted_password = '';
	$firstname = '';
	$lastname = '';
	
	//Get password
	$statement = "SELECT password,iv, firstname, lastname FROM nse_user WHERE login_name=? LIMIT 1";
	$sql_recover = $GLOBALS['dbCon']->prepare($statement);
	$sql_recover->bind_param('s',$email);
	$sql_recover->execute();
	$sql_recover->bind_result($encrypted_password, $iv, $firstname, $lastname);
	$sql_recover->store_result();
	if ($sql_recover->num_rows == 0) {
		$template_file = $_SERVER['DOCUMENT_ROOT'] . '/modules/login/html/login_forgotten_pass_failed.html';
		$template = file_get_contents($template_file);
		$template = str_replace('<!-- message -->', 'Username not found in our database.', $template);
	} else {
		$sql_recover->fetch();
		$tc = new tcrypt();
		$decrypted_password = $tc->decrypt($encrypted_password, $iv);
		
		$email_text_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/login/html/password_recovery_email.txt');
		$email_html_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/login/html/password_recovery_email.html');
		
		//Replace Tags
		$email_text_template = str_replace('<!-- server -->', 'http://' . $_SERVER['HTTP_HOST'] . '/admin/', $email_text_template);
		$email_html_template = str_replace('<!-- server -->', 'http://' .$_SERVER['HTTP_HOST'] . '/admin/', $email_html_template);
		$email_text_template = str_replace('<!-- password -->', $decrypted_password, $email_text_template);
		$email_html_template = str_replace('<!-- password -->', $decrypted_password, $email_html_template);
		$email_text_template = str_replace('<!-- name -->', "$firstname $lastname", $email_text_template);
		$email_html_template = str_replace('<!-- name -->', "$firstname $lastname", $email_html_template);
		$email_text_template = str_replace('<!-- email -->', $email, $email_text_template);
		$email_html_template = str_replace('<!-- email -->', $email, $email_html_template);
		
		$mail = new PHPMailer();
		$mail->AddReplyTo('admin@aatravel.co.za', 'AA Travel Guides');
	    $mail->AddAddress($email, "$firstname $lastname");
	    $mail->SetFrom('admin@aatravel.co.za', 'AA Travel Guides');
	    $mail->Subject = 'AA Travel Guides Administration System - Password Recovery';
	    $mail->AltBody = $email_text_template;
	    $mail->MsgHTML($email_html_template);
	    $mail->Send();
		
		$template = file_get_contents($template_file);
	}
	$sql_recover->free_result();
	$sql_recover->close();
	
	echo $template;
}

function recover_count() {
	if (isset($_SESSION['recover_count'])) {
		$_SESSION['recover_count']++;
	} else {
		$_SESSION['recover_count'] = 0;
	}
	
	if ($_SESSION['recover_count'] > 3000) {
		return FALSE;
	}
	
	return TRUE;
}

?>