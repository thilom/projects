<?php
/**
 *
 */

//Includes
require_once '../../settings/init.php';

//Vars
$search_string = "%{$_GET['st']}%";
$establishment_code = '';
$establishment_name = '';
$town_name = '';
$result_list = '';

//Search
$statement = "SELECT a.establishment_code, a.establishment_name, c.town_name
				FROM nse_establishment AS a
				JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
				JOIN nse_location_town AS c ON b.town_id=c.town_id
				WHERE establishment_name LIKE ?
				ORDER BY establishment_name
				LIMIT 20";
$sql_find = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);
$sql_find->bind_param('s', $search_string);
$sql_find->execute();
$sql_find->bind_result($establishment_code, $establishment_name, $town_name);
$sql_find->store_result();
while ($sql_find->fetch()) {
	$establishment_name = addslashes($establishment_name);
	$town_name = addslashes($town_name);
	$result_list .=  "$establishment_code|$establishment_name($town_name)##";
}
$sql_find->free_result();
$sql_find->close();

echo "<script>parent.draw_result('$result_list');</script>";
?>