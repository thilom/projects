function check_login_frm(f) {
	if (f.login_username_txt.value == "" || f.login_pass_txt.value == "") {
		alert('Please first complete the login form!');
		return false;
	}
	
	return true;
}

function checkMail(email_address) {
    var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if (filter.test(email_address)) {
    	return true;
    } else {
    	return false;
    }
}

function check_frm(f) {
	if (f.login_name_txt.value == "" || f.login_username_txt.value == "" || f.login_pass_txt.value == "") {
		alert('Please complete the entire form!');
		return false;
	}
	
	if (!checkMail(f.login_email_txt.value)) {
		alert('Please enter a valid email!');
		return false;
	}
	
	if (f.login_pass_txt.value != f.login_repass_txt.value) {
		alert('Passwords do not match');
		return false;
	}

	return true;
}