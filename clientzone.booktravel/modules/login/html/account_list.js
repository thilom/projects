function filter(v) {
	if (v == 'all') {
		eraseCookie('uFilter');
	} else {
		createCookie('uFilter', v);
	}
	document.location = document.location;
}

function sFilter(f) {
	v = document.getElementById('searchString').value;
	document.location = '/modules/login/login_list.php?f=' +  f + '&sfilter=' + v;
}

function showFilter() {
	v = readCookie('uFilter');
	document.getElementById('uFilter').value = v;
}

function view_account(v) {
	document.location = "/modules/login/login_list.php?f=view&id=" + v;
}

function edit_account(v) {
	document.location = "/modules/login/login_list.php?f=edit&id=" + v;
}

function addEstab_show() {
	document.getElementById('addEstab').style.display  = 'block';
}

function addEstab(a,b) {
	aE = true;
	len = eC.length;
	for (i=0; i<len; i++) {
		if (eC[i] == a) aE = false;
	}	
	if (aE) {
		eN[len] = b;
		eC[len] = a;
		draw_estabs();
	}
	addEstab_close();
}

function resend_password(v) {
	document.location = "/modules/login/login_list.php?f=pass&id=" + v;
}

function set_privilege(v) {
	document.location = "/modules/login/login_list.php?f=access&id=" + v;
}

function removeEstab(c) {
	eN[c] = '';
	eC[c] = '';
	draw_estabs();
}

function addEstab_close() {
	document.getElementById('addEstab').style.display = 'none';
}

function delete_account(v) {
	if (confirm("WARNING!\n This will permanently remove the account and cannot be undone.\n\n Continue?")) {
		document.location = "/modules/login/login_list.php?f=delete&id=" + v;
	}
}

function draw_estabs() {
	f = '';
	e = "<table cellpadding=0 cellspacing=0 id=TRW onmouseover='whack()' width=100%>";
	for (x=0; x<eN.length; x++) {
		if (eN[x] == '') continue;
		e += "<tr><td>"+ eN[x] +"</td><td align=right><input type=button class=delete_button value='   Remove' onClick=removeEstab("+ x +")></td></tr>";
		f += eC[x] + ",";
	}
		
	e += "</table>";
	e += "<input type=hidden name=estabs value=" + f + " />";
	document.getElementById('estab_list').innerHTML = e;
}

function delete_template(id) {
	if (confirm('WARNING!\nDeleting this template cannot be undone.\n\nContinue?')) {
		document.location = '/modules/login/login_list.php?f=delete_template&id=' + id;
	}
}

function apply_template(id,tID,n) {
	if (tID != '') {
		if (confirm('WARNING!\nThis will permanently overwrite the current user privileges.\n\nContinue?')) {
			document.location = '/modules/login/login_list.php?f=apply_access&id=' + id + "&tID=" + tID + "&new=" + n;
		}
	}
}

function createCookie(name,value) {
	var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function reset_password(v) {
	if (confirm('Reset Password for this user?')) {
		document.location = "/modules/login/login_list.php?f=pass&nID=" + v;
	}
}