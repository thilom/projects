<?php
/**
 * Module settings for user management
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */
$module_id = 'login';
$module_name = "Admin Account Manager";
$module_icon = 'login_icon.png';
$module_link = 'login_list.php';
$window_width = '750';
$window_height = '0';
$window_position = '';

//Include CMS settings
if (!defined('SITE_ROOT')) define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT']);
include_once SITE_ROOT . '/settings/init.php';

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 's'; //a = all
$sub_levels['manage_users']['name'] = 'Accounts Management';
$sub_levels['manage_users']['file'] = 'login_list.php';
$sub_levels['manage_users']['subs']['view']['name'] = 'View Account Details';
$sub_levels['manage_users']['subs']['edit']['name'] = 'Edit Account Details';
$sub_levels['manage_users']['subs']['add']['name'] = 'Add New Accounts';
$sub_levels['manage_users']['subs']['delete']['name'] = 'Delete Accounts';
$sub_levels['manage_users']['subs']['resend_password']['name'] = 'Resend Passwords';
$sub_levels['manage_users']['subs']['reset_password']['name'] = 'Reset Passwords';

$sub_levels['manage_access']['name'] = 'Access Management';
$sub_levels['manage_access']['subs']['privileges']['name'] = 'Manage Account Privileges';
$sub_levels['manage_access']['subs']['add_blocks']['name'] = 'Blocks Accounts';
$sub_levels['manage_access']['subs']['remove_blocks']['name'] = 'Unblocks Accounts';
$sub_levels['manage_templates']['name'] = 'Manage Templates';
$sub_levels['manage_templates']['subs']['edit_templates']['name'] = 'Edit Templates';
$sub_levels['manage_templates']['subs']['delete_templates']['name'] = 'Delete Templates';
$sub_levels['manage_templates']['subs']['add_templates']['name'] = 'Create Templates';


//General Settings
if (!defined('SECURE_PHRASE')) define('SECURE_PHRASE', 'dbweb4_secure');
if (!defined('LOGIN_TIMEOUT')) define('LOGIN_TIMEOUT', 30);
$list_length = 20;

?>