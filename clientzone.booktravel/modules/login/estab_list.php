<?php
//Vars
$matches = array();
$nLine = '';
$searchString = '';
$results = array();
$id = '';
$name = '';

//Includes
require_once '../../settings/init.php';

include_once SITE_ROOT . "/shared/admin_style.php";
echo $css;
echo "<style type=text/css>td{vertical-align:top}span img{margin:0 18 3 0;float:left}span b{font-size:19pt;letter-spacing:-2;display:block;margin:0 0 8 0}</style>";
echo '<link href="/modules/login/html/account_list.css" type="text/css" rel="stylesheet">';

//Get template
$content = file_get_contents(SITE_ROOT . '/modules/login/html/estab_list.html');
$content = str_replace(array("\r\n","\r","\n"), '#$%', $content);
preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
$lContent = $matches[0][0];

if (isset($_POST['sT'])) {
    $searchString = $_POST['sT'];
    $sT = "%{$_POST['sT']}%";
    
    //Get exact match
    $statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE establishment_name=?";
    $sql_exact = $GLOBALS['dbCon']->prepare($statement);
    $sql_exact->bind_param('s', $searchString);
    $sql_exact->execute();
    $sql_exact->bind_result($id, $name);
    $sql_exact->store_result();
    while($sql_exact->fetch()) {
        if(!isset($results[$id])) $results[$id] = $name;
    }
    $sql_exact->close();
    
    //Find exact code
    $statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE establishment_code=?";
    $sql_code = $GLOBALS['dbCon']->prepare($statement);
    $sql_code->bind_param('s', $searchString);
    $sql_code->execute();
    $sql_code->bind_result($id, $name);
    $sql_code->store_result();
    while($sql_code->fetch()) {
        if(!isset($results[$id])) $results[$id] = $name;
    }
    $sql_code->close();
    
    //Find Partial Match
    $statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE establishment_name LIKE ?";
    $sql_partial = $GLOBALS['dbCon']->prepare($statement);
    $sql_partial->bind_param('s', $sT);
    $sql_partial->execute();
    $sql_partial->bind_result($id, $name);
    $sql_partial->store_result();
    while($sql_partial->fetch()) {
        if(!isset($results[$id])) $results[$id] = $name;
    }
    $sql_partial->close();
    
    $results = array_slice($results, 0, 100, true);
    
    
    foreach ($results as $k=>$v) {
        $line = str_replace('<!-- estab_name -->', $v, $lContent);
        $line = str_replace('<!-- estab_name2 -->', addslashes($v), $line);
        $line = str_replace('<!-- id -->', $k, $line);
        
        $nLine .= $line;
    }
}

//Replace Tags
$content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
$content = str_replace('#$%', "\n", $content);
$content = str_replace('<!-- sT -->', $searchString, $content);

echo $content;

?>