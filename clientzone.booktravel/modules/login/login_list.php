<?php
/**
 * Admin User Manager
 *
 * Add, edit, delete, block, unblock and resend passwords of admin users.
 *
 * @author  Muller(2009)
 * @package RVBus
 * @category login
 *
 * @todo Link to '/shared/shared.css' CSS file as well.
 */

//Vars
$css = '';
$function = '';
$parent_id = '';
$user_access = '';
$access = '';
$module_id = '';
$access = array('edit_user'=>FALSE,
                'add_user'=>FALSE,
                'view_user'=>FALSE,
                'delete_user'=>FALSE,
                'resend_password'=>FALSE,
                'privileges'=>FALSE,
                'block_user'=>FALSE,
                'unblock_user'=>FALSE,
                'edit_templates'=>FALSE,
                'add_templates'=>FALSE,
                'delete_templates'=>FALSE,
				'reset_password' => FALSE);
if (!isset($_GET['f'])) $_GET['f'] = '';

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/login/module.ini.php';
require_once SITE_ROOT . '/modules/login/tcrypt.class.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';


//Prepare Statements
$statement = "SELECT function_id, parent_id, access FROM nse_user_access WHERE user_id=? && module_id=?";
$sql_access = $dbCon->prepare($statement);


//Get Access Rights
$sql_access->bind_param('ss', $_SESSION['dbweb_user_id'], $module_id);
$sql_access->execute();
$sql_access->bind_result($function, $parent_id, $user_access);
$sql_access->store_result();
while ($sql_access->fetch()) {
    if ($function == 'edit' && $parent_id == 'manage_users' && $user_access == '1') $access['edit_user'] = TRUE;
    if ($function == 'view' && $parent_id == 'manage_users' && $user_access == '1') $access['view_user'] = TRUE;
    if ($function == 'add' && $parent_id == 'manage_users' && $user_access == '1') $access['add_user'] = TRUE;
    if ($function == 'delete' && $parent_id == 'manage_users' && $user_access == '1') $access['delete_user'] = TRUE;
//    if ($function == 'resend_password' && $parent_id == 'manage_users' && $user_access == '1') $access['resend_password'] = TRUE;
    if ($function == 'privileges' && $parent_id == 'manage_access' && $user_access == '1') $access['privileges'] = TRUE;
    if ($function == 'add_blocks' && $parent_id == 'manage_access' && $user_access == '1') $access['block_user'] = TRUE;
    if ($function == 'remove_blocks' && $parent_id == 'manage_access' && $user_access == '1') $access['unblock_user'] = TRUE;
    if ($function == 'edit_templates' && $parent_id == 'manage_templates' && $user_access == '1') $access['edit_templates'] = TRUE;
    if ($function == 'delete_templates' && $parent_id == 'manage_templates' && $user_access == '1') $access['delete_templates'] = TRUE;
    if ($function == 'add_templates' && $parent_id == 'manage_templates' && $user_access == '1') $access['add_templates'] = TRUE;
//    if ($function == 'reset_password' && $parent_id == 'manage_users' && $user_access == '1') $access['reset_password'] = TRUE;
}
$sql_access->close();

//Generate Menu
include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo "<style type=text/css>td{vertical-align:top}span img{margin:0 18 3 0;float:left}span b{font-size:19pt;letter-spacing:-2;display:block;margin:0 0 8 0}</style>";
echo '<link href="/modules/login/html/account_list.css" type="text/css" rel="stylesheet" />';
echo "<script src=/shared/G.js></script>";
echo "<script src=/shared/tabs.js></script>";
echo "<script src=/modules/login/html/account_list.js></script>";

echo "<script>tabs(\"";
echo "<a href=# id=Mq_1In onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Manage Accounts</a>";
echo "<a href=# id=Mq_1md onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Manage Access</a>";
echo "<a href=# id=Mq_1et onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Manage Templates</a>";
echo "\")</script>";

echo "<span class=TabP>";

echo "<p id=Mp_In onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
if ($access['add_user']) {
    echo "<a href='/modules/login/login_list.php?f=add&id=staff' onfocus=blur()>Add a Staff Account</a>";
    echo "<a href='/modules/login/login_list.php?f=add&id=client' onfocus=blur()>Add a Client Account</a>";
} else {
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Add a Staff Account</a>";
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Add a Client Account</a>";
}
if ($access['edit_user'] || $access['view_user'] || $access['delete_user']) {
    echo "<a href='/modules/login/login_list.php?f=list' onfocus=blur()>Edit/Delete Accounts</a>";
} else {
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Edit/Delete Accounts</a>";
}
//if ($access['resend_password']) {
//    echo "<a href='/modules/login/login_list.php?f=pass' onfocus=blur()>Resend Passwords</a>";
//} else {
//    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Resend Passwords</a>";
//}

echo "</p>";
echo "</span>";

echo "<span class=TabP>";
echo "<p id=Mp_md onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
if ($access['privileges']) {
    echo "<a href='/modules/login/login_list.php?f=access' onfocus=blur()>Account Access</a>";
} else {
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Account Access</a>";
}
if ($access['block_user']) {
    echo "<a href='/modules/login/login_list.php?f=block' onfocus=blur()>Block Account</a>";
} else {
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Block Account</a>";
}
if ($access['unblock_user']) {
    echo "<a href='/modules/login/login_list.php?f=unblock' onfocus=blur()>Unblock Account</a>";
} else {
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Unblock Account</a>";
}
echo "</p>";
echo "</span>";

echo "<span class=TabP>";
echo "<p id=Mp_et onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
if ($access['add_templates']) {
    echo "<a href='/modules/login/login_list.php?f=add_template' onfocus=blur()>Create Templates</a>";
} else {
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Create Templates</a>";
}
if ($access['edit_templates'] || $access['delete_templates']) {
    echo "<a href='/modules/login/login_list.php?f=edit_template' onfocus=blur()>Edit/Delete Templates</a>";
} else {
    echo "<a href='/modules/login/login_list.php?f=no_access' onfocus=blur() style='color: silver'>Edit/Delete Templates</a>";
}
echo "</p>";
echo "</span>";
echo "<br />";

//Map
switch ($_GET['f']) {
    case 'no_access':
        no_access();
        break;
    case 'list':
        list_accounts('edit');
        break;
    case 'view':
        view_account();
        break;
    case 'edit':
        if (isset($_POST['updateAccount'])) {
            update_account();
        } else {
            edit_account();
        }
        break;
    case 'pass':
        if (isset($_GET['id'])) {
            resend_password();
        } else if (isset($_GET['nID'])) {
        	reset_password();
        } else {
            list_accounts('pass');
        }
        break;
    case 'delete':
    	delete_account();
    	break;
    case 'access':
        if (isset($_POST['updateAccess'])) {
            access_form_save();
        } elseif (isset($_GET['id'])) {
            access_form_show();
        } else {
            list_accounts('access');
        }
        break;
    case 'add_template':
        if (isset($_POST['insertTemplate'])) {
            create_template_save();
        } else {
            create_template_form();
        }
        break;
    case 'edit_template':
        if (isset($_POST['updateTemplate'])) {
            edit_template_save();
        } elseif (isset($_GET['id'])) {
            edit_template_form();
        } else {
            template_list();
        }
        break;
    case 'delete_template':
    	delete_template();
    	break;
    case 'apply_access':
    	apply_access();
    	break;
    case 'add':
    	if (isset($_POST['updateAccess'])) {
    		create_account_save_access();
    	} elseif (isset($_POST['updateAccount'])) {
    		create_account_save_data();
    	} else {
    		create_account_form();
    	}
    	break;
    default:
        entry_message();
}

/**
 * List Accounts
 *
 * Displays a list of accounts and sets the funtion buttons according to the parameter passed.
 *
 * @param string $list_type
 * @param  string $message Message to display on-load
 */
function list_accounts($list_type = 'edit', $message = '') {
    //Vars
    $start = isset($_GET['s'])?$_GET['s']:'0';
    $nLine = '';
    $matches = array();
    $account_name = '';
    $account_email = '';
    $account_type = '';
    $firstname = '';
    $lastname = '';
    $user_id = '';
    $sub_results = 0;
    $total_results = 0;
    
    //Setup Link
    
    $link = '/modules/login/login_list.php?f=';
    switch($list_type) {
    	case 'access':
    		$link = 'access';
    		break;
    	case 'pass':
    		$link .= 'pass';
    		break;
    	default:
    		$link .= 'list';
    		break;
    }
    
    //Get Template
    $content = file_get_contents(SITE_ROOT . '/modules/login/html/account_list.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
    $lContent = $matches[0][0];
    
    //Get list
    $filter = '';
    if (isset($_COOKIE['uFilter'])) {
        $filter = 'WHERE ';
        switch ($_COOKIE['uFilter']) {
            case 'staff':
                $filter .= " top_level_type='s'";
                break;
            case 'clients':
                $filter .= " top_level_type='c'";
                break;
            default:
                $filter = '';
        }
    }
     
    if (isset($_GET['sfilter']) && !empty($_GET['sfilter'])) {
        $filter .= empty($filter)?'WHERE ': ' && ';
        $searchString = str_replace(array("'",'"'), '', $_GET['sfilter']);
        $searchString = addslashes($searchString);
        $filter .= "(email LIKE '%$searchString%' || firstname LIKE '%$searchString%' || lastname LIKE '%$searchString%')";
        $link .= "&sfilter={$_GET['sfilter']}";
    }
    
    if (isset($_GET['filter']) && !empty($_GET['filter'])) {
        $filter .= empty($filter)?'WHERE ': ' && ';
        $searchString = str_replace(array("'",'"'), '', $_GET['filter']);
        $searchString = addslashes($searchString);
        $filter .= "lastname LIKE '$searchString%'";
        $link .= "&filter={$_GET['filter']}";
    }
    
    $statement = "SELECT user_id, firstname, lastname, email, top_level_type FROM nse_user $filter LIMIT ?,?";
    $sql_list = $GLOBALS['dbCon']->prepare($statement);
    $sql_list->bind_param('ii', $start, $GLOBALS['list_length']);
    $sql_list->execute();
    $sql_list->bind_result($user_id, $firstname, $lastname, $account_email, $account_type);
    $sql_list->store_result();
    while ($sql_list->fetch()) {
        $sub_results++;
        $account_name = "$lastname, $firstname";
        $account_type = 'c'==$account_type?'Client':'Staff';
        $line = str_replace('<!-- name -->', $account_name, $lContent);
        $line = str_replace('<!-- email -->', $account_email, $line);
        $line = str_replace('<!-- type -->', $account_type, $line);
        $buttons = '';
        switch ($list_type) {
            case 'edit':
                if ($GLOBALS['access']['view_user']) $buttons .= "<input type=button value='View' class=view_button onClick='view_account(\"$user_id\")'>";
                if ($GLOBALS['access']['edit_user']) $buttons .= "<input type=button value='Edit' class=edit_button onClick='edit_account(\"$user_id\")'>";
                if ($GLOBALS['access']['delete_user']) $buttons .= "<input type=button value='Delete' class=delete_button onClick='delete_account(\"$user_id\")'>";
                break;
            case 'pass':
            	if ($GLOBALS['access']['reset_password']) $buttons .= "<input type=button value='Reset' class=secure_button onClick='reset_password(\"$user_id\")'>";
                if ($GLOBALS['access']['resend_password']) $buttons .= "<input type=button value='Resend Password' class=email_button onClick='resend_password(\"$user_id\")'>";
                break;
            case 'access':
                if ($GLOBALS['access']['privileges']) $buttons .= "<input type=button value='Set Privileges' class=secure_button onClick='set_privilege(\"$user_id\")'>";
                break;
            default:
                $buttons = '';
                break;
        }
        
        $line = str_replace('<!-- buttons -->', $buttons, $line);
        
        $nLine .= $line;
    }
    
    //Count results
    $statement = "SELECT COUNT(*) FROM nse_user $filter";
    $sql_count = $GLOBALS['dbCon']->prepare($statement);
    $sql_count->execute();
    $sql_count->bind_result($total_results);
    $sql_count->store_result();
    $sql_count->fetch();
    $sql_count->close();
    if ($total_results < $GLOBALS['list_length']) {
        $paging = "<td id=td1>&#9668;First</td><td id=td2>&lt;Previous</td><td id=td3>  </td><td id=td4>Next&gt;</td><td id=td5>Last&#9658;</td>";
    } else {
        if ($start != 0) {
            $paging = "<td id=td1><a href='$link&s=0'>&#9668;First</a></td>";
            
            $s = $start - $GLOBALS['list_length'];
            $paging .= "<td id=td2><a href='$link&s=$s'>&lt;Previous</a></td>";
        } else {
            $paging = "<td id=td1>&#9668;First</td><td id=td2>&lt;Previous</td>";
        }
        
        if ($total_results > $GLOBALS['list_length']) {
            $paging .= "<td id=td3>";
            for($x=1; $x<ceil($total_results/$GLOBALS['list_length'])+1; $x++) {
                $ns = ($x-1) * $GLOBALS['list_length'];
                if ($ns == $start) {
                    $paging .= "<span class='active_page'>$x</span> |";
                } else {
                    $paging .= "<a href='$link&s=$ns'>$x</a> |";
                }
            }
            $paging = substr($paging, 0, -1);
            $paging .= "</td>";
        } else {
            $paging .= "<td id=td3>  </td>";
        }
        
        
        if ($start + $GLOBALS['list_length'] > $total_results) {
            $paging .= "<td id=td4>Next&gt;</td><td id=td5>Last&#9658;</td>";
        } else {
            $s = $start + $GLOBALS['list_length'];
            $paging .= "<td id=td4><a href='$link&s=$s'>Next&gt;</a></td>";
            
            $s = (floor($total_results/$GLOBALS['list_length'])) * $GLOBALS['list_length'];
            $paging .= "<td id=td5><a href='$link&s=$s'>Last&#9658;</a></td>";
        }
    }
    
    //Replace_tags
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    $content = str_replace('<!-- paging -->', $paging, $content);
    $content = str_replace('<!-- f -->', $_GET['f'], $content);
    
    //Attach message
    if (!empty($message)) {
        $content .= "<script>alert('$message')</script>";
    }
    
    echo $content;
}

/**
 * Account Details View
 *
 * Displays the account details as well as last login and a limited activity log for an account.
 * For clients it also shows the establishments which may be edited by the client.
 *
 */
function view_account() {
    //Vars
    $account_name = '';
    $account_type = '';
    $email = '';
    $phone = '';
    $cell = '';
    $estabs = '';
    $establishment_name = '';
    $log_date = '';
    $log_message = '';
    $login_date = '';
    $ip_address = '';
    
    //get Data
    $statement = "SELECT CONCAT(firstname, ' ', lastname), top_level_type, email, phone, cell FROM nse_user WHERE user_id=?";
    $sql_view = $GLOBALS['dbCon']->prepare($statement);
    $sql_view->bind_param('s', $_GET['id']);
    $sql_view->execute();
    $sql_view->bind_result($account_name, $account_type, $email, $phone, $cell);
    $sql_view->store_result();
    $sql_view->fetch();
    $sql_view->close();
    
    //Log
    $GLOBALS['log_tool']->write_entry("Viewed account detail of $account_name({$_GET['id']})", $_SESSION['dbweb_user_id']);
    
    if ($account_type == 'c') {
        $content = file_get_contents(SITE_ROOT . '/modules/login/html/view_client.html');
        
        //Get establishment_access
        $statement = "SELECT b.establishment_name
        				FROM nse_user_establishments AS a
        				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
        				WHERE user_id=?";
        $sql_estabs = $GLOBALS['dbCon']->prepare($statement);
        $sql_estabs->bind_param('s', $_GET['id']);
        $sql_estabs->execute();
        $sql_estabs->bind_result($establishment_name);
        $sql_estabs->store_result();
        while ($sql_estabs->fetch()) {
            $estabs .= "$establishment_name<br>";
        }
        $estabs = substr($estabs, 0, -4);
        $sql_estabs->close();
        
        $content = str_replace('<!-- estabs -->', $estabs, $content);
        
    } else {
        $content = file_get_contents(SITE_ROOT . '/modules/login/html/view_staff.html');
    }
    
    //Get logs
    $logs = "<table id=logsTable cellspacing=0 cellpadding=2>";
    $statement = "SELECT DATE_FORMAT(log_timestamp, '%d-%m-%y %H:%i'), log_message, ip_address FROM nse_logs WHERE user_id=? ORDER BY log_timestamp DESC LIMIT 50";
    $sql_logs = $GLOBALS['dbCon']->prepare($statement);
    $sql_logs->bind_param('s', $_GET['id']);
    $sql_logs->execute();
    $sql_logs->bind_result($log_date, $log_message, $ip_address);
    $sql_logs->store_result();
    while ($sql_logs->fetch()) {
        if (empty($ip_address)) $ip_address = '&nbsp;';
        $logs .= "<tr><td valign=top class=col1>$log_date</td><td class=col2>$ip_address</td><td class=col1>$log_message</td></tr>";
    }
    $logs .= "</table>";
    $sql_logs->close();
    
    //Get last login date
    $statement = "SELECT DATE_FORMAT(log_timestamp, '%d-%m-%y %H:%i') FROM nse_logs WHERE user_id=? && log_message LIKE '%successful Login%' ORDER BY log_timestamp ASC LIMIT 2";
    $sql_last_login = $GLOBALS['dbCon']->prepare($statement);
    $sql_last_login->bind_param('s', $_GET['id']);
    $sql_last_login->execute();
    $sql_last_login->bind_result($login_date);
    $sql_last_login->store_result();
    $sql_last_login->fetch();
    $sql_last_login->close();
    
    //Check Vars
    if (empty($account_name)) $account_name = '&nbsp;';
    if (empty($account_type)) $account_type = '&nbsp;';
    if (empty($email)) $email = '&nbsp;';
    if (empty($phone)) $phone = '&nbsp;';
    if (empty($cell)) $cell = '&nbsp;';
    
    //Replace Tags
    $content = str_replace('<!-- name -->', $account_name, $content);
    $content = str_replace('<!-- email -->', $email, $content);
    $content = str_replace('<!-- tel -->', $phone, $content);
    $content = str_replace('<!-- cell -->', $cell, $content);
    $content = str_replace('<!-- logs -->', $logs, $content);
    $content = str_replace('<!-- last_login -->', $login_date, $content);
    
    echo $content;
}

/**
 * Delete an account
 *
 * Deletes the account($_GET['id']) from the DB (nse_user) and removes references to the account in nse_user_access and nse_user_establishments.
 *
 */
function delete_account() {
	$statement = "DELETE FROM nse_user WHERE user_id=?";
	$sql_delete1 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete1->bind_param('s', $_GET['id']);
	$sql_delete1->execute();
	
	$statement = "DELETE FROM nse_user_access WHERE user_id=?";
	$sql_delete2 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete2->bind_param('s', $_GET['id']);
	$sql_delete2->execute();
	
	$statement = "DELETE FROM nse_user_establishments WHERE user_id=?";
	$sql_delete3 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete3->bind_param('s', $_GET['id']);
	$sql_delete3->execute();
	
	echo "<script>alert('Account Deleted!'); document.location='/modules/login/login_list.php?f=list'</script>";
	exit();
}

/**
 * Resend Password
 *
 * Decodes the current password and sends it to the corrunt email.
 *
 */
function resend_password() {
    //Vars
    $firstname = '';
    $lastname = '';
    $email = '';
    $nPass = '';
    $type = '';
    $estab_count = '';
    
    //Instantiate Classes
    $mail = new PHPMailer(true);
    $tc = new tcrypt();
    
    //Get Account Name
    $statement = "SELECT firstname, lastname, email, password, iv, top_level_type, login_name FROM nse_user WHERE user_id=?";
    $sql_account = $GLOBALS['dbCon']->prepare($statement);
    $sql_account->bind_param('s', $_GET['id']);
    $sql_account->execute();
    $sql_account->bind_result($firstname, $lastname, $email, $nPass, $iv, $type, $login_name);
    $sql_account->fetch();
    $sql_account->free_result();
    $sql_account->close();
    
    $account_name = "$firstname $lastname";
    $account_name = trim($account_name);
    
    //Check if establishments are connected
    if ($type == 'c') {
    	$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=?";
    	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
    	$sql_estab->bind_param('s', $_GET['id']);
    	$sql_estab->execute();
    	$sql_estab->store_result();
    	$sql_estab->bind_result($estab_count);
    	$sql_estab->fetch();
    	$sql_estab->free_result();
    	$sql_estab->close();
    	
    	if ($estab_count == 0) {
    		echo '<script>alert("Password cannot be sent \r\n \r\nThere are no establishments associated with this user. Edit the user to add establishments and try again.")</script>';
    		echo "<script>document.location='/modules/login/login_list.php?f=pass'</script>";
    		die();
    	}
    }
    
    //Get Email Template
    $email_template = file_get_contents(SITE_ROOT . "/modules/login/html/aa_password_email.html");
    
    //Get Password
    $password = $tc->decrypt($nPass, $iv);
    
    //Replace Tags
    $email_template = str_replace('<!-- name -->', $account_name, $email_template);
    $email_template = str_replace('<!-- password -->', $password, $email_template);
    $email_template = str_replace('<!-- username -->', $login_name, $email_template);
    $email_template = str_replace('<!-- server -->', "<a href='http://admin.aatravel.co.za'>http://admin.aatravel.co.za</a>", $email_template);
    
    //Send Mail
    try {
      $mail->AddReplyTo('admin@aatravel.co.za', 'AA Travel Guides');
      $mail->AddAddress($email, $account_name);
      $mail->SetFrom('admin@aatravel.co.za', 'AA Travel Guides');
      $mail->Subject = 'AA Travel Guides Administration System';
      $mail->MsgHTML($email_template);
      $mail->Send();
      echo "<script>alert('Password resent to $account_name, $email');</script>";
      
      //Log
      $GLOBALS['log_tool']->write_entry("Resent password for $account_name, $email({$_GET['id']})", $_SESSION['dbweb_user_id']);
    } catch (phpmailerException $e) {
      echo "<script>alert('ERROR!\nPassword resend to $account_name, $email failed');</script>";
      
      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Password resend to $account_name, $email({$_GET['id']}) Failed. $error", $_SESSION['dbweb_user_id']);
    } catch (Exception $e) {
      echo "<script>alert('ERROR!\nPassword resend to $account_name, $email failed');</script>";
      
      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Password resend to $account_name, $email({$_GET['id']}) Failed. $error", $_SESSION['dbweb_user_id']);
    }
    
    //Redirect
    echo "<script>document.location='/modules/login/login_list.php?f=pass'</script>";
    
}

/**
 * Update Access Settings
 *
 * Deletes all entries related to a user ($_GET['id']) and then inserts the new access privileges.
 */
function access_form_save($new=FALSE) {
    //Vars
    $user_id = ctype_digit($_POST['id'])?$_POST['id']:'';
    
    //Clear DB
    $statement= "DELETE FROM nse_user_access WHERE user_id=?";
    $sql_clean = $GLOBALS['dbCon']->prepare($statement);
    $sql_clean->bind_param('s', $user_id);
    $sql_clean->execute();
    $sql_clean->close();
    
    //Insert New Access Privileges
    $statement = "INSERT INTO nse_user_access (user_id, module_id, function_id, parent_id, access) VALUES (?,?,?,?,'1')";
    $sql_insert = $GLOBALS['dbCon']->prepare($statement);
    foreach ($_POST['access'] as $post_value) {
        list($module_id, $parent_id, $function_id) = explode('|', $post_value);
        $sql_insert->bind_param('ssss', $user_id, $module_id, $function_id, $parent_id);
        $sql_insert->execute();
    }
    $sql_insert->close();
    
    //Redirect
    if (!$new) {
    	echo "<script>alert('Account access privileges updated')</script>";
    	echo "<script>document.location='/modules/login/login_list.php?f=access';</script>";
    }
}

/**
 * Display the access form
 *
 * Iterates through the module directories and generates the access heirarchy.
 *
 */
function access_form_show() {
    //Vars
    $id = ctype_digit($_GET['id'])?$_GET['id']:'0';
    $firstname = '';
    $lastname = '';
    $email = '';
    $access_type = '';
    $access = '';
    $nLine = '';
    $module_name = '';
    $module_id = '';
    $matches = array();
    $top_level_allowed = '';
    $dir = SITE_ROOT . '/modules/';
    $template_id = '';
    $template_name = '';
    $template_list = '';
    $checked = 0;
    
    //Prepare Statements
    $statement = "SELECT access FROM nse_user_access WHERE module_id=? && function_id=? && parent_id=? && user_id=?";
    $sql_access = $GLOBALS['dbCon']->prepare($statement);
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/login/html/access_form.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
    $lContent = $matches[0][0];
    
    //Get template list
    $statement = "SELECT template_id, template_name FROM nse_access_template";
    $sql_templates = $GLOBALS['dbCon']->prepare($statement);
	$sql_templates->execute();
	$sql_templates->bind_result($template_id, $template_name);
	$sql_templates->store_result();
	while ($sql_templates->fetch()) {
			$template_list .= "<option value=$template_id>$template_name</option>";
	}
	$sql_templates->free_result();
	$sql_templates->close();
    
    //Get account details
    $statement = "SELECT firstname, lastname, email, top_level_type FROM nse_user WHERE user_id=?";
    $sql_account = $GLOBALS['dbCon']->prepare($statement);
    $sql_account->bind_param('s', $id);
    $sql_account->execute();
    $sql_account->bind_result($firstname, $lastname, $email, $access_type);
    $sql_account->fetch();
    $sql_account->free_result();
    $sql_account->close();
    
    //Assemble Account Name
    if (empty($firstname) && empty($lastname)) {
        $account_name = $email;
    } else {
        $account_name = "$firstname $lastname";
        $account_name = trim($account_name);
    }
    
    if (is_dir($dir)) {
    	$dh = opendir($dir);
        if ($dh) {
            while (($file = readdir($dh)) !== false) {
                if (is_dir($dir . $file)) {
                    $sub_levels = array();
                	if ($file == '.' || $file == '..') continue;
                	if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
                	if ('a' != $top_level_allowed && $top_level_allowed == $access_type) {
                	    if (!empty($nLine)) $nLine .= "<tr><th colspan=2>&nbsp;</th></tr>";
                	    $nLine .= "<tr><th colspan=2 class=level_header>$module_name</th></tr>";
                	    foreach ($sub_levels as $k=>$v) {
                	        if (isset($v['name'])) {
                	            if (!isset($v['subs'])) {
                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
                	            } else {
                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
                	                foreach($v['subs'] as $k1=>$v1) {
                	                    $box_name = "{$module_id}|{$k}|{$k1}";
                	                    $access = '';
                	                    $sql_access->bind_param('ssss', $module_id, $k1, $k, $id);
                	                    $sql_access->execute();
                	                    $sql_access->bind_result($access);
                	                    $sql_access->fetch();
                	                    $sql_access->free_result();
                	                    $checked = $access=='1'?'checked':'';
                	                    
                    	                $line = str_replace('<!-- name -->', $v1['name'] , $lContent);
                    	                $line = str_replace('<!-- checked -->', $checked, $line);
                    	                $line = str_replace('<!-- box_name -->', $box_name, $line);
                    	                
                    	                $nLine .= $line;
                	                }
                	            }
                	        }
                	    }
                	}
                }
            }
            closedir($dh);
        }
    }
    
    //Replace Tags
    $content = str_replace('<!-- account_name -->', $account_name, $content);
    $content = str_replace('<!-- template_list -->', $template_list, $content);
    $content = str_replace('<!-- id -->', $id, $content);
    $content = str_replace('<!-- new -->', '0', $content);
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    
    //Serve
    echo $content;
}

/**
 * Apply access levels to a user from a selected template
 */
function apply_access() {
	//Vars
	$template_id = ctype_digit($_GET['tID'])?$_GET['tID']:'';
	$user_id = ctype_digit($_GET['id'])?$_GET['id']:'';
	
	//CLear Current Data
	$statement = "DELETE FROM nse_user_access WHERE user_id=?";
	$sql_clear = $GLOBALS['dbCon']->prepare($statement);
	$sql_clear->bind_param('s', $user_id);
	$sql_clear->execute();
	$sql_clear->close();
	
	//Transfer Access Privileges
	$statement = "INSERT INTO nse_user_access (user_id, module_id, function_id, parent_id, access) VALUES (?,?,?,?,'1')";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$statement = 'SELECT module_id, function_id, parent_id, access FROM nse_access_template_data WHERE template_id=?';
	$sql_template = $GLOBALS['dbCon']->prepare($statement);
	$sql_template->bind_param('s', $template_id);
	$sql_template->execute();
	$sql_template->bind_result($module_id, $function_id, $parent_id, $access);
	$sql_template->store_result();
	while ($sql_template->fetch()) {
		if ($access != 1) continue;
		$sql_user->bind_param('ssss', $user_id, $module_id, $function_id, $parent_id);
		$sql_user->execute();
	}
	$sql_template->free_result();
	$sql_template->close();
	$sql_user->close();
	
	//Redirect
	if (isset($_GET['new']) && $_GET['new'] == '1') {
		echo "<script>alert('User  Account Created')</script>";
    	echo "<script>document.location='/modules/login/login_list.php';</script>";
	} else {
	    echo "<script>alert('Account access privileges updated')</script>";
	    echo "<script>document.location='/modules/login/login_list.php?f=access';</script>";
	}
}

/**
 * Display account edit form
 *
 * Displays a form according to the account type for editing the account.
 *
 */
function edit_account() {
    //Vars
    $firstname = '';
    $lastname = '';
    $email = '';
    $phone = '';
    $cell = '';
    $account_type = '';
    $js_names = '';
    $js_codes = '';
    $name = '';
    $code = '';
    
    //Get Account Data
    $statement = "SELECT firstname, lastname, top_level_type, email, phone, cell, assessor, sales_rep FROM nse_user WHERE user_id=?";
    $sql_view = $GLOBALS['dbCon']->prepare($statement);
    $sql_view->bind_param('s', $_GET['id']);
    $sql_view->execute();
    $sql_view->bind_result($firstname, $lastname, $account_type, $email, $phone, $cell, $assessor, $sales_rep);
    $sql_view->store_result();
    $sql_view->fetch();
    $sql_view->close();
    
    
    //Get template
    if ($account_type == 'c') {
        $content = file_get_contents(SITE_ROOT . '/modules/login/html/edit_client.html');
        
        //Get estab list
        $statement = "SELECT b.establishment_name, a.establishment_code
        				FROM nse_user_establishments AS a
        				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
        				WHERE a.user_id=?";
        $sql_estabs = $GLOBALS['dbCon']->prepare($statement);
        echo mysqli_error($GLOBALS['dbCon']);
        $sql_estabs->bind_param('s', $_GET['id']);
        $sql_estabs->execute();
        $sql_estabs->bind_result($name, $code);
        $sql_estabs->store_result();
        while($sql_estabs->fetch()) {
            $js_names .= "'" . addslashes($name) . "',";
            $js_codes .= "'$code',";
        }
        $js_names = substr($js_names, 0, -1);
        $js_codes = substr($js_codes, 0, -1);
    } else {
        $content = file_get_contents(SITE_ROOT . '/modules/login/html/edit_staff.html');
    }
    
    $assessor_check = $assessor==1?'checked':'';
    $rep_check = $sales_rep==1?'checked':'';
    
    //Replace Tags
    $content = str_replace('<!-- firstname -->', $firstname, $content);
    $content = str_replace('<!-- lastname -->', $lastname, $content);
    $content = str_replace('<!-- email -->', $email, $content);
    $content = str_replace('<!-- tel -->', $phone, $content);
    $content = str_replace('<!-- cell -->', $cell, $content);
    $content = str_replace('<!-- assessor_check -->', $assessor_check, $content);
    $content = str_replace('<!-- rep_check -->', $rep_check, $content);
    $content = str_replace('<!-- js_names -->', $js_names, $content);
    $content = str_replace('<!-- js_codes -->', $js_codes, $content);
    
    echo $content;
}

/**
 * Update Account Details
 *
 * Saves the new user details to the DB
 *
 */
function update_account() {
   //Vars
   $id = $_GET['id'];
   $account_type = '';
   $assessor = '';
   $sales_rep = '';
   
   //Get account Type
   $statement = "SELECT top_level_type FROM nse_user WHERE user_id=?";
   $sql_type = $GLOBALS['dbCon']->prepare($statement);
   $sql_type->bind_param('s', $id);
   $sql_type->execute();
   $sql_type->bind_result($account_type);
   $sql_type->store_result();
   $sql_type->fetch();
   $sql_type->close();
   
   //Save estab access data
   if('c' == $account_type) {
       $estabs = explode(',', $_POST['estabs']);
       
       $statement = "DELETE FROM nse_user_establishments WHERE user_id=?";
       $sql_clear = $GLOBALS['dbCon']->prepare($statement);
       $sql_clear->bind_param('s', $_GET['id']);
       $sql_clear->execute();
       $sql_clear->close();
       
       $statement = "INSERT INTO nse_user_establishments (user_id, establishment_code) VALUES (?,?)";
       $sql_add = $GLOBALS['dbCon']->prepare($statement);
       
       foreach ($estabs as $v) {
           if (empty($v)) continue;
           $sql_add->bind_param('ss', $_GET['id'], $v);
           $sql_add->execute();
       }
       $sql_add->close();
   } else {
   		$assessor = isset($_POST['assessor'])?1:'';
   		$sales_rep = isset($_POST['sales_rep'])?1:'';
   }
   
   //Save data
   $statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=?, assessor=?, sales_rep=? WHERE user_id=?";
   $sql_update = $GLOBALS['dbCon']->prepare($statement);
   $sql_update->bind_param('ssssssss', $_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['tel'], $_POST['cell'], $assessor, $sales_rep, $id);
   $sql_update->execute();
   
   echo "<script>alert('{$_POST['firstname']} {$_POST['lastname']}: Details Updated');document.location='/modules/login/login_list.php?f=list';</script>";
   //list_accounts('edit', "{$_POST['firstname']} {$_POST['lastname']}: Details Updated");
   
}

/**
 * No Access Error
 *
 * Displays the 'No Access' error if the user does not have access a function
 *
 */
function no_access() {
    $content = file_get_contents(SITE_ROOT . '/html/no_entry.html');
    echo $content;
}

/**
 * List all access templates available
 */
function template_list() {
    //Vars
    $nLine = '';
    $template_id = '';
    $template_name = '';
    $matches = array();
    
    //Template
    $content = file_get_contents(SITE_ROOT . '/modules/login/html/template_list.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
    $lContent = $matches[0][0];
    
    //Get Data
    $statement = "SELECT template_id, template_name FROM nse_access_template ORDER BY template_name";
    $sql_templates = $GLOBALS['dbCon']->prepare($statement);
    $sql_templates->execute();
    $sql_templates->bind_result($template_id, $template_name);
    $sql_templates->store_result();
    while ($sql_templates->fetch()) {
        //Assemble Buttons
        $buttons = '';
        if ($GLOBALS['access']['edit_templates']) $buttons .= "<input type=button class=edit_button value='Edit' onCLick='document.location=\"/modules/login/login_list.php?f=edit_template&id=$template_id\"'>";
        if ($GLOBALS['access']['delete_templates']) $buttons .= "<input type=button class=delete_button value='Delete' onClick=delete_template('$template_id')>";
        
        $line = str_replace('<!-- name -->', $template_name, $lContent);
        $line = str_replace('<!-- buttons -->', $buttons, $line);
        
        $nLine .= $line;
    }
    $sql_templates->free_result();
    $sql_templates->close();
    
    //Replace Tags
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    
    echo $content;
}

/**
 * Display a form to edit a current template
 */
function edit_template_form() {
    //Vars
    $id = ctype_digit($_GET['id'])?$_GET['id']:'0';
    $access = '';
    $nLine = '';
    $module_name = '';
    $module_id = '';
    $matches = array();
    $top_level_allowed = '';
    $dir = SITE_ROOT . '/modules/';
    $access_type = 's';
    $template_name = '';
    $button_name = 'updateTemplate';
    
    
    //Get Template Name
    $statement = "SELECT template_name FROM nse_access_template WHERE template_id=?";
    $sql_name = $GLOBALS['dbCon']->prepare($statement);
    $sql_name->bind_param('s', $id);
    $sql_name->execute();
    $sql_name->bind_result($template_name);
    $sql_name->fetch();
    $sql_name->free_result();
    $sql_name->close();
    
    
    //Get template
    $content = file_get_contents(SITE_ROOT . '/modules/login/html/template_form.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
    $lContent = $matches[0][0];
    
    $statement = "SELECT access FROM nse_access_template_data WHERE module_id=? && function_id=? && parent_id=? && template_id=?";
    $sql_access = $GLOBALS['dbCon']->prepare($statement);
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                if (is_dir($dir . $file)) {
                    $sub_levels = array();
                	if ($file == '.' || $file == '..') continue;
                	if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
                	if ('a' != $top_level_allowed && $top_level_allowed == $access_type) {
                	    if (!empty($nLine)) $nLine .= "<tr><th colspan=2>&nbsp;</th></tr>";
                	    $nLine .= "<tr><th colspan=2 class=level_header>$module_name</th></tr>";
                	    foreach ($sub_levels as $k=>$v) {
                	        if (isset($v['name'])) {
                	            if (!isset($v['subs'])) {
                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
                	            } else {
                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
                	                foreach($v['subs'] as $k1=>$v1) {
                	                    $box_name = "{$module_id}|{$k}|{$k1}";
                	                    $access = '';
                	                    $sql_access->bind_param('ssss', $module_id, $k1, $k, $id);
                	                    $sql_access->execute();
                	                    $sql_access->bind_result($access);
                	                    $sql_access->fetch();
                	                    $sql_access->free_result();
                	                    $checked = '1'==$access?'checked':'';
                	                    
                    	                $line = str_replace('<!-- name -->', $v1['name'] , $lContent);
                    	                $line = str_replace('<!-- checked -->', $checked, $line);
                    	                $line = str_replace('<!-- box_name -->', $box_name, $line);
                    	                
                    	                $nLine .= $line;
                	                }
                	            }
                	        }
                	    }
                	}
                }
            }
            closedir($dh);
        }
    }
    
    //Replace Tags
    $content = str_replace('<!-- template_name -->', $template_name, $content);
    $content = str_replace('<!-- button_name -->', $button_name, $content);
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    
    //Serve
    echo $content;
}

/**
 * Update template in DB
 */
function edit_template_save() {
    //Vars
    $template_id = ctype_digit($_GET['id'])?$_GET['id']:'0';
    $template_name = $_POST['template_name'];
    
    //Update name
    $statement = "UPDATE nse_access_template SET template_name=? WHERE template_id=?";
    $sql_name = $GLOBALS['dbCon']->prepare($statement);
    $sql_name->bind_param('ss', $template_name, $template_id);
    $sql_name->execute();
    $sql_name->close();
    
    //Clear current data
    $statement = "DELETE FROM nse_access_template_data WHERE template_id=?";
    $sql_clear = $GLOBALS['dbCon']->prepare($statement);
    $sql_clear->bind_param('s', $template_id);
    $sql_clear->execute();
    $sql_clear->close();
    
    //Insert New Access Privileges
    $statement = "INSERT INTO nse_access_template_data (template_id, module_id, function_id, parent_id, access) VALUES (?,?,?,?,'1')";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    foreach ($_POST['access'] as $post_value) {
        list($module_id, $parent_id, $function_id) = explode('|', $post_value);
        $sql_data->bind_param('ssss', $template_id, $module_id, $function_id, $parent_id);
        $sql_data->execute();
    }
    $sql_data->close();

    //Redirect
    echo "<script>alert('Access Template Updated')</script>";
    echo "<script>document.location='/modules/login/login_list.php?f=edit_template';</script>";
        
}

/**
 * Create Template - Form
 *
 * Displays a form for creating nem access templates.
 *
 */
function create_template_form() {
    //Vars
    $checked = '';
    $nLine = '';
    $dir = SITE_ROOT . '/modules/';
    $top_level_allowed = '';
    $access_type = 's';
    
    //Template
    $content = file_get_contents(SITE_ROOT . '/modules/login/html/template_form.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
    $lContent = $matches[0][0];
    
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                if (is_dir($dir . $file)) {
                    $sub_levels = array();
                	if ($file == '.' || $file == '..') continue;
                	if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
                	if ('a' != $top_level_allowed && $top_level_allowed == $access_type) {
                	    if (!empty($nLine)) $nLine .= "<tr><th colspan=2>&nbsp;</th></tr>";
                	    $nLine .= "<tr><th colspan=2 class=level_header>$module_name</th></tr>";
                	    foreach ($sub_levels as $k=>$v) {
                	        if (isset($v['name'])) {
                	            if (!isset($v['subs'])) {
                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
                	            } else {
                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
                	                foreach($v['subs'] as $k1=>$v1) {
                	                    $box_name = "{$module_id}|{$k}|{$k1}";
                	                    
                    	                $line = str_replace('<!-- name -->', $v1['name'] , $lContent);
                    	                $line = str_replace('<!-- checked -->', $checked, $line);
                    	                $line = str_replace('<!-- box_name -->', $box_name, $line);
                    	                
                    	                $nLine .= $line;
                	                }
                	            }
                	        }
                	    }
                	}
                }
            }
            closedir($dh);
        }
    }
    
    //Replace Tags
    $content = str_replace('<!-- template_name -->', '', $content);
    $content = str_replace('<!-- button_name -->', 'insertTemplate', $content);
    $content = preg_replace('@<!-- save_start -->(.)*<!-- save_end -->@', '', $content);
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    
    echo $content;
}

/**
 * Save the new template to the DB (nse_access_template, nse_access_template_data)
 *
 */
function create_template_save() {
    //Vars
    $template_name = $_POST['template_name'];
    
    //Insert new template
    $statement = "INSERT INTO nse_access_template (template_name) VALUES (?)";
    $sql_add = $GLOBALS['dbCon']->prepare($statement);
    $sql_add->bind_param('s', $template_name);
    $sql_add->execute();
    $template_id = $sql_add->insert_id;
    $sql_add->close();
    
    //Insert New Access Privileges
    $statement = "INSERT INTO nse_access_template_data (template_id, module_id, function_id, parent_id, access) VALUES (?,?,?,?,'1')";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    foreach ($_POST['access'] as $post_value) {
        list($module_id, $parent_id, $function_id) = explode('|', $post_value);
        $sql_data->bind_param('ssss', $template_id, $module_id, $function_id, $parent_id);
        $sql_data->execute();
    }
    $sql_data->close();

    //Redirect
    echo "<script>alert('Access Template Created')</script>";
    
    if ($GLOBALS['access']['add_templates'] || $GLOBALS['access']['delete_templates']) {
        echo "<script>document.location='/modules/login/login_list.php?f=edit_template';</script>";
    } else {
        echo "<script>document.location='/modules/login/login_list.php?f=add_template';</script>";
    }
}

/**
 * Displays  a form according to the type of user to be added.
 */
function create_account_form($data = '') {
	//Vars
	$account_type = '';
    $js_names = '';
    $js_codes = '';
    $name = '';
    $code = '';
	$user_type = isset($_GET['id'])&&($_GET['id']=='staff'||$_GET['id']=='client')?$_GET['id']:'';
	if (empty($data)) {
		$firstname = '';
	    $lastname = '';
	    $email = '';
	    $phone = '';
	    $cell = '';
	} else {
		$firstname = $data['firstname'];
	    $lastname = $data['lastname'];
	    $email = $data['email'];
	    $phone = $data['tel'];
	    $cell = $data['cell'];
	}
	
	//Get template
	if ($user_type == 'staff') {
		$content = file_get_contents(SITE_ROOT . '/modules/login/html/edit_staff.html');
	} else {
		$content = file_get_contents(SITE_ROOT . '/modules/login/html/edit_client.html');
	}
	
	//Replace Tags
	$content = str_replace('<!-- firstname -->', $firstname, $content);
    $content = str_replace('<!-- lastname -->', $lastname, $content);
    $content = str_replace('<!-- email -->', $email, $content);
    $content = str_replace('<!-- tel -->', $phone, $content);
    $content = str_replace('<!-- cell -->', $cell, $content);
    $content = str_replace('<!-- assessor_check -->', '', $content);
    $content = str_replace('<!-- rep_check -->', '', $content);
    $content = str_replace('<!-- js_names -->', $js_names, $content);
    $content = str_replace('<!-- js_codes -->', $js_codes, $content);
	
	echo $content;
}

/**
 * Save user data and display access form
 */
function create_account_save_data() {
	//Vars
	$user_count = 1;
	$template_list = '';
	$user_type = isset($_GET['id'])&&($_GET['id']=='staff'||$_GET['id']=='client')?$_GET['id']:'';
	$email = $GLOBALS['security']->is_valid_email($_POST['email'])?$_POST['email']:'';
	$type_id = $user_type=='staff'?'s':'c';
	$firstname = ctype_alnum($_POST['firstname'])?$_POST['firstname']:'';
	$lastname = ctype_alnum($_POST['lastname'])?$_POST['lastname']:'';
	$phone =  ctype_alnum(str_replace(' ', '', $_POST['tel']))?$_POST['tel']:'';
	$cell = ctype_digit(str_replace(' ', '', $_POST['cell']))?$_POST['cell']:'';
	$dir = SITE_ROOT . '/modules/';
	$template_id = '';
	$template_name = '';
	$nLine = '';
	$top_level_allowed = '';
	$module_name = '';
	$module_id = '';
	$assessor = '';
	$sales_rep = '';
	
	//Encryption
	require_once SITE_ROOT . '/modules/login/tcrypt.class.php';
	$crypt = new tcrypt();
	
	//Generate Password
	$password = $GLOBALS['security']->generate_password(6);
	$encrypted_password = $crypt->encrypt($password);
	
	//Check if user exists
	$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('s', $email);
	$sql_check->execute();
	$sql_check->bind_result($user_count);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	if ($user_count > 0) {
		echo "<script>alert('Email address already in use. Please enter a different address and try again.')</script>";
		create_account_form($_POST);
	} else {
		//Vars
		$assessor = isset($_POST['assessor'])?1:'';
	   	$sales_rep = isset($_POST['sales_rep'])?1:'';
	   		
		//Insert into DB
		$statement = "INSERT INTO nse_user (email, firstname, lastname, phone, cell, top_level_type, new_password, assessor, sales_rep) VALUES (?,?,?,?,?,?,?,?,?)";
		$sql_create = $GLOBALS['dbCon']->prepare($statement);
		$sql_create->bind_param('sssssssss', $email, $firstname, $lastname, $phone, $cell, $type_id, $encrypted_password, $assessor, $sales_rep);
		$sql_create->execute();
		$user_id = $sql_create->insert_id;
		$sql_create->close();
		
		if ($user_type == 'client') {
			$estabs = explode(',', $_POST['estabs']);
	       
	       	$statement = "INSERT INTO nse_user_establishments (user_id, establishment_code) VALUES (?,?)";
	       	$sql_add = $GLOBALS['dbCon']->prepare($statement);
	       	
	       	foreach ($estabs as $v) {
	           if (empty($v)) continue;
	           $sql_add->bind_param('ss', $user_id, $v);
	           $sql_add->execute();
	    	}
	       	$sql_add->close();
	       	
	       	$content = "<script>alert('New Client Added');document.location='/modules/login/login_list.php?f=list'</script>";
		} else if ($assessor == 1) {
	   		$content = "<script>alert('New Assessor Added');document.location='/modules/login/login_list.php?f=list'</script>";
		} else {
			
			//Get template
		    $content = file_get_contents(SITE_ROOT . '/modules/login/html/access_form.html');
		    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
		    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
		    $lContent = $matches[0][0];
		    
		    //Get template list
		    $statement = "SELECT template_id, template_name FROM nse_access_template";
		    $sql_templates = $GLOBALS['dbCon']->prepare($statement);
			$sql_templates->execute();
			$sql_templates->bind_result($template_id, $template_name);
			$sql_templates->store_result();
			while ($sql_templates->fetch()) {
				if (empty($template_id)) continue;
				$template_list .= "<option value=$template_id>$template_name</option>";
			}
			$sql_templates->free_result();
			$sql_templates->close();
			
			//Assemble Account Name
		    if (empty($firstname) && empty($lastname)) {
		        $account_name = $email;
		    } else {
		        $account_name = "$firstname $lastname";
		        $account_name = trim($account_name);
		    }
		    
		    if (is_dir($dir)) {
		    	$dh = opendir($dir);
		        if ($dh) {
		            while (($file = readdir($dh)) !== false) {
		                if (is_dir($dir . $file)) {
		                    $sub_levels = array();
		                	if ($file == '.' || $file == '..') continue;
		                	if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
		                	if ('a' != $top_level_allowed && $top_level_allowed == $type_id) {
		                	    if (!empty($nLine)) $nLine .= "<tr><th colspan=2>&nbsp;</th></tr>";
		                	    $nLine .= "<tr><th colspan=2 class=level_header>$module_name</th></tr>";
		                	    foreach ($sub_levels as $k=>$v) {
		                	        if (isset($v['name'])) {
		                	            if (!isset($v['subs'])) {
		                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
		                	            } else {
		                	                $nLine .= "<tr><th colspan=2 class=level_subheader>{$v['name']}</th></tr>";
		                	                foreach($v['subs'] as $k1=>$v1) {
		                	                    $box_name = "{$module_id}|{$k}|{$k1}";
		                	                    $access = '';
		                	                    $checked = '';
		                	                    
		                    	                $line = str_replace('<!-- name -->', $v1['name'] , $lContent);
		                    	                $line = str_replace('<!-- checked -->', $checked, $line);
		                    	                $line = str_replace('<!-- box_name -->', $box_name, $line);
		                    	                
		                    	                $nLine .= $line;
		                	                }
		                	            }
		                	        }
		                	    }
		                	}
		                }
		            }
		            closedir($dh);
		        }
		    }
		    
		    //Replace Tags
		    $content = str_replace('<!-- account_name -->', $account_name, $content);
		    $content = str_replace('<!-- template_list -->', $template_list, $content);
		    $content = str_replace('<!-- id -->', $id, $content);
		    $content = str_replace('<!-- new -->', '1', $content);
		    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
		    $content = str_replace('$%^', "\n", $content);
		}
	    
	    //Serve
	    echo $content;
		
	}
	
	
}

/**
 * Save access details for a new account
 */
function create_account_save_access() {
	access_form_save(TRUE);
	
    $firstname = '';
    $lastname = '';
    $email = '';
    $nPass = '';
    
    //Instantiate Classes
    $mail = new PHPMailer(true);
    $tc = new tcrypt();
    
    //Get Account Name
    $statement = "SELECT firstname, lastname, email, new_password FROM nse_user WHERE user_id=?";
    $sql_account = $GLOBALS['dbCon']->prepare($statement);
    $sql_account->bind_param('s', $_GET['id']);
    $sql_account->execute();
    $sql_account->bind_result($firstname, $lastname, $email, $nPass);
    $sql_account->fetch();
    $sql_account->free_result();
    $sql_account->close();
    
    $account_name = "$firstname $lastname";
    $account_name = trim($account_name);
    
    //Get Email Template
    $email_template = file_get_contents(SITE_ROOT . "/modules/login/html/aa_password_email.html");
    
    //Get Password
    $password = $tc->decrypt($nPass);
    
    //Replace Tags
    $email_template = str_replace('<!-- name -->', $account_name, $email_template);
    $email_template = str_replace('<!-- password -->', $password, $email_template);
    $email_template = str_replace('<!-- username -->', $email, $email_template);
    $email_template = str_replace('<!-- server -->', "<a href='http://admin.aatravel.co.za'>http://admin.aatravel.co.za</a>", $email_template);
    
    //Send Mail
    try {
      $mail->AddReplyTo('@aatravel.co.za', 'AA Travel Guides');
      $mail->AddAddress($email, $account_name);
      $mail->SetFrom('@aatravel.co.za', 'AA Travel Guides');
      $mail->Subject = 'AA Travel Guides Administration System';
      $mail->MsgHTML($email_template);
      $mail->Send();
      echo "<script>alert('Password resent to $account_name, $email');</script>";
      
      //Log
      $GLOBALS['log_tool']->write_entry("Resent password for $account_name, $email({$_GET['id']})", $_SESSION['dbweb_user_id']);
    } catch (phpmailerException $e) {
      echo "<script>alert('ERROR!\nPassword resend to $account_name, $email failed');</script>";
      
      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Password resend to $account_name, $email({$_GET['id']}) Failed. $error", $_SESSION['dbweb_user_id']);
    } catch (Exception $e) {
      echo "<script>alert('ERROR!\nPassword resend to $account_name, $email failed');</script>";
      
      //Log
      $error = $e->errorMessage();
      $GLOBALS['log_tool']->write_entry("Password resend to $account_name, $email({$_GET['id']}) Failed. $error", $_SESSION['dbweb_user_id']);
    }
	
	echo "<script>alert('User  Account Created')</script>";
    echo "<script>document.location='/modules/login/login_list.php';</script>";
}

/**
 * Permanently delete a template from the DB
 */
function delete_template() {
	//Vars
	$id = ctype_digit($_GET['id'])?$_GET['id']:'';
	
	//Delete from nse_access_template
	$statement = 'DELETE FROM nse_access_template WHERE template_id=?';
	$sql_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_name->bind_param('s', $id);
	$sql_name->execute();
	$sql_name->close();
	
	//Delete data
	$statement = "DELETE FROM nse_access_template_data WHERE template_id=?";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	$sql_data->bind_param('s', $id);
	$sql_data->execute();
	$sql_data->close();
	
	//Redirect
	echo "<script>alert('Access Template Deleted')</script>";
    echo "<script>document.location='/modules/login/login_list.php?f=edit_template';</script>";
	
}

function reset_password() {
	//Vars
	$user_id = $_GET['nID'];
	
	//Create password
	$tc = new tcrypt();
	$password = $GLOBALS['security']->generate_password(6);
	//echo $password;
	$password = $tc->encrypt($password);
	
	//Save
	$statement = "UPDATE nse_user SET new_password=? WHERE user_id=?";
	$sql_password = $GLOBALS['dbCon']->prepare($statement);
	$sql_password->bind_param('ss', $password, $user_id);
	$sql_password->execute();
	$sql_password->close();
	
	//Redirect
	echo "<script>alert('Password Reset')</script>";
    echo "<script>document.location='/modules/login/login_list.php?f=pass';</script>";
	
}

/**
 * Entry Message
 *
 * The message to display when the module is first opened.
 *
 */
function entry_message() {
   $content = file_get_contents(SITE_ROOT . '/modules/login/html/welcome.html');
   
   //Check Permissions
   if (!$GLOBALS['access']['add_user']) {
       $content = preg_replace('@<!-- add_start -->(.)*<!-- add_end -->@', '', $content);
   }
   if (!$GLOBALS['access']['edit_user'] && !$GLOBALS['access']['view_user'] && !$GLOBALS['access']['delete_user']) {
       $content = preg_replace('@<!-- edit_start -->(.)*<!-- edit_end -->@', '', $content);
   }
   
   echo $content;
}
?>