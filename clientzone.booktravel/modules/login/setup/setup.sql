CREATE TABLE `dbweb_user` (
`du_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`du_pass` VARCHAR( 50 ) NOT NULL ,
`du_username` VARCHAR( 100 ) NOT NULL ,
`du_email` VARCHAR( 150 ) NOT NULL ,
`du_name` VARCHAR( 100 ) NOT NULL
) TYPE = MYISAM COMMENT = 'dbweb login table';

CREATE TABLE `login_log` (
`ll_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`ll_time` BIGINT NOT NULL ,
`ll_desc` VARCHAR( 100 ) NOT NULL
) TYPE = MYISAM COMMENT = 'login log ';

CREATE TABLE `login_attempts` (
`la_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`la_date` BIGINT NOT NULL ,
`la_ip` VARCHAR( 100 ) NOT NULL
`la_username` VARCHAR( 100 ) NOT NULL
) TYPE = MYISAM COMMENT = 'login attempts ';
