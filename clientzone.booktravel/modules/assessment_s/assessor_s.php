<?php
/**
 *
 */

//Vars
$function_white_list = array('list_assessments', 'approve_assessment', 'view_assessment', 'welcome_pack', 'certificates','return_assessment', 'new_assessment', 'assess_estab');
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/assessment_s/module.ini.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';
require_once SITE_ROOT . '/modules/login/tcrypt.class.php';
include_once SITE_ROOT . '/modules/shared/php/aa_status.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/assessment_s/html/assessment_s.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/assessment_s/html/assessment_s.js'> </script>";

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/assessment_s/html/assessment.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/shared/css/establishment.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/assessor_s/html/assessor_s.js'> </script>";
echo "<script src='/modules/shared/js/notes.js'> </script>";

//Assemble Menu Structure
echo "<script src=/shared/G2.js></script>";
echo "<script src=/shared/tabs.js></script>";
echo "<script>tabs(\"";
echo "<a href=# id=Mq_1In onmouseover=_MENU(this,1) onmouseout=_MENU(this)>Manage Assessments</a>";
echo "\")</script>";
echo "<span class=TabP>";
echo "<p id=Mp_In onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>";
    echo "<a href='/modules/assessment_s/assessor_s.php?f=list_assessments' onfocus=blur()>List Assessments</a>";
     echo "<a href='/modules/assessment_s/assessor_s.php?f=list_assessments' onfocus=blur()>Approve Assessments</a>";
      echo "<a href='/modules/assessment_s/assessor_s.php?f=new_assessment' onfocus=blur()>New Assessments</a>";
    

echo "</p>";
echo "</span>";
echo "<br />";

switch ($function_id) {
//	case 'certificates':
//		certificate();
//		break;
	case 'welcome_pack':
		if (isset($_POST['send_welcome_pack'])) {
			welcome_pack_send();
		} else {
			welcome_pack_form();
		}
		break;
	case 'view_assessment':
		view_assessment();
		break;
	case 'approve_assessment':
		if (isset($_POST['approve_assessment'])) {
			edit_assessment_form();
		} else if (isset($_POST['disapprove_assessment'])) {
			dissaprove_assessment_form();
		} else if (isset($_POST['dissapprove_email'])) {
			dissapprove_assessment_send();
		} else if (isset($_POST['send_email'])) {
			approve_assessment_email();
		} else if (isset($_GET['return'])) {
			edit_assessment_form();
		} else {
			edit_assessment();
		}
		break;
	case 'list_assessments':
		list_assessments();
		break;
	case 'new_assessment':
		list_establishments();
		break;
	case 'assess_estab':
		if (isset($_POST['approve_assessment'])) {
			edit_assessment_form();
		} else if (isset($_POST['disapprove_assessment'])) {
			dissaprove_assessment_form();
		} else if (isset($_POST['dissapprove_email'])) {
			dissapprove_assessment_send();
		} else if (isset($_POST['send_email'])) {
			approve_assessment_email();
		} else if (isset($_POST['save_draft'])) {
			new_assessment_save('draft');
		} else {
			new_assessment_form();
		}
		
		break;
	default:
		welcome();
}


function list_assessments() {
	//Vars
	$nLine = '';
	$assessment_id = '';
	$assessment_date = '';
	$assessment_status = '';
	$establishment_code = '';
	$establishment_name = '';
	
	//Get template & prepare
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/html/assessment_list.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
	$lTemplate = $matches[0][0];

	//Prepare Statements - Province Name
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	
	//Get list of assessments
	$statement = "SELECT a.assessment_id, a.establishment_code, b.establishment_name, DATE_FORMAT(a.assessment_date, '%Y-%m-%d'), a.assessment_status, assessor_name
					FROM nse_establishment_assessment AS a
					JOIN nse_establishment AS b ON a.establishment_code = b.establishment_code
					WHERE ";
	if (isset($_COOKIE['uEstabFilter'])) {
		switch ($_COOKIE['uEstabFilter']) {
			case 'waiting':
				$statement .= "a.assessment_status='waiting'";
				break;
			case 'below':
				$statement .= "a.assessment_status='below'";
				break;
			case 'complete':
				$statement .= "a.assessment_status='complete'";
				break;
			default:
				$statement .= "a.assessment_status != 'draft' && a.assessment_status != 'returned'";
		}
	} else {
		$statement .= "a.assessment_status != 'draft' && a.assessment_status != 'returned'";
	}
	if (isset($_POST['search_string'])) {
		if (substr($statement,-6) != 'WHERE ') $statement .= " && ";
		$search_string = addslashes($_POST['search_string']);
		if ($_POST['search_filter'] == 'establishment_code') {
			$statement .= "a.establishment_code LIKE '%$search_string%'";
		} else {
			$statement .= "b.establishment_name LIKE '%$search_string%'";
		}
	}
	$statement .= "	ORDER BY a.assessment_date DESC";
	$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessments->execute();
	$sql_assessments->store_result();
	$sql_assessments->bind_result($assessment_id, $establishment_code, $establishment_name, $assessment_date, $assessment_status, $assessor_name);
	while ($sql_assessments->fetch()) {
		//Vars
		$town_name = '';
		$province_name = '';
		$country_name = '';
		
		//Get Location
		$sql_location->bind_param('s', $establishment_code);
		$sql_location->execute();
		$sql_location->store_result();
		$sql_location->bind_result($town_name, $province_name, $country_name);
		$sql_location->fetch();
		$sql_location->free_result();
		
		$location = $town_name;
		if (!empty($province_name)) {
			$location .= ", $province_name";
		} else {
			if (!empty($country_name)) $location .= ", $country_name";
		}
		
		$line = str_replace('<!-- establishment_name -->', $establishment_name, $lTemplate);
		$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
		$line = str_replace('<!-- location -->', $location, $line);
		$line = str_replace('<!-- assessment_date -->', $assessment_date, $line);
		$line = str_replace('<!-- status -->', $assessment_status, $line);
		$line = str_replace('<!-- assessor_name -->', $assessor_name, $line);
		
		if ($assessment_status == 'waiting') {
			$line = str_replace('<!-- buttons -->', "<input type=button value=Edit class=scroll_button onClick=\"document.location='/modules/assessment_s/assessor_s.php?f=approve_assessment&code=$establishment_code&id=$assessment_id'\" />", $line);
		} else {
			$line = str_replace('<!-- buttons -->', "<input type=button value=View class=view_button onClick=\"document.location='/modules/assessment_s/assessor_s.php?f=view_assessment&code=$establishment_code&id=$assessment_id'\" />", $line);
		}
		
		$nLine .= $line;
	}
	$sql_assessments->free_result();
	$sql_assessments->close();
	
	
	//Heading
	$heading_titles = array('empty'=>'&nbsp;',
							'assessor_name'=>'Assessor',
							'establishment'=>'Establishment',
							'location'=>'Location',
							'date'=>'Date',
							'status'=>'Status',
							'nb'=>'&nbsp;');
	$line_header = "<tr style='background-color: grey'>";
	foreach ($heading_titles as $col_id=>$col_name) {
		$line_header .= "<TH>";
		$line_header .= "$col_name";
		$line_header .= "</TH>";
	}
    $line_header .= "</tr>";
	
	//Replace Tags
	$template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
	//$template = str_replace('<!-- navigation -->', $navigation, $template);
	$template = str_replace('<!-- line_header -->', $line_header, $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}


function edit_assessment() {
	//Vars
	$areas = '';
	$area_id = '';
	$area_name = '';
	$assessment_id = $_GET['id'];
	$assessment_date = '';
	$assessment_notes = '';
	$assessment_type = '';
	$buttons = '';
	$category_id = '';
	$category_name = '';
	$category_select = '';
	$country = '';
	$establishment_code = $_GET['code'];
	$establishment_name = '';
	$entered_date = '';
	$hide = '';
	$location = '';
	$province = '';
	$proposed_category = '';
	$qa_code = '';
	$qa_list = '';
	$qa_name = '';
	$qa_status = '';
	$ratings = array(1=>'Excellent','Very Good','Good','Fair','Poor');
	$renewal_date = '';
	$scripts = '';
	$staff_notes = '';
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/html/assessment_form.html');
	$town = '';
	$types = array('new establishment'=>'New Establishment', 'award visit'=>'Awards Visit', 'renewal'=>'Renewal');
	$type_list = '';
	$contacts_list = '';
	
	//Get establishment_name
	$statement = "SELECT establishment_name, website_url, enter_date FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name, $website_url, $entered_date);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();

	//Get Assessor Name
	$statement = "SELECT assessor_name FROM nse_establishment_assessment WHERE assessment_id=?";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('i', $assessment_id);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_name);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();

	//Prepare statement - Check if user has admin access
	$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
	$sql_admin = $GLOBALS['dbCon']->prepare($statement);

	//Get & Assemble Contact details
	$counter = 0;
	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.phone, b.cell, b.email, b.new_password, a.user_id
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE a.establishment_code=?";
	$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
	$sql_contacts->bind_param('s', $establishment_code);
	$sql_contacts->execute();
	$sql_contacts->bind_result($contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $password, $user_id);
	$sql_contacts->store_result();
	while ($sql_contacts->fetch()) {
		$user_counter = empty($password)?0:1;
		$contacts_list .= "!|!$user_id|$contact_name|$contact_designation|$contact_tel|$contact_cell|$contact_email|$user_counter|0|$contact_email|0";
		$counter++;
	}
	$sql_contacts->free_result();
	$sql_contacts->close();

	//Assemble website url
	$website_url = trim($website_url);
	if (substr($website_url, 0, 7) == 'http://') $website_url = substr($website_url,7);
	if (!empty($website_url)) {
		$website_url = "<a href='http://$website_url' target=_blank >http://$website_url</a>";
	} else {
		$website_url = "No Website Listed";
	}
	
	//Get Location
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->store_result();
	$sql_location->bind_result($town, $province, $country);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	
	if (!empty($town)) $location .= $town;
	if (!empty($province)) {
		$location .= ", $province";
	} else {
		if (!empty($country)) $location .= ", $country";
	}
	
	//Get data
	$statement = "SELECT DATE_FORMAT(assessment_date,'%Y-%m-%d'), assessment_type, qa_status, DATE_FORMAT(renewal_date, '%M %Y'), proposed_category, assessment_notes, staff_notes FROM nse_establishment_assessment WHERE assessment_id=? LIMIT 1";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('s', $assessment_id);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($assessment_date, $assessment_type, $qa_status, $renewal_date, $proposed_category, $assessment_notes, $staff_notes);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	//Prepare Statement - Get area data
	$statement = "SELECT assessment_content, assessment_rating, not_applicable, hide_from_client FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	
	//Get and calculate renewal date
	if (empty($renewal_date)) {
		$statement = "SELECT DATE_FORMAT(enter_date, '%m') FROM nse_establishment WHERE establishment_code=?";
		$sql_renewal = $GLOBALS['dbCon']->prepare($statement);
		$sql_renewal->bind_param('s', $establishment_code);
		$sql_renewal->execute();
		$sql_renewal->store_result();
		$sql_renewal->bind_result($renewal_month);
		$sql_renewal->fetch();
		$sql_renewal->free_result();
		$sql_renewal->close();
		$current_year = date('Y');
		if ($renewal_month == '00') {
			$renewal_date = date('F Y', strtotime('today +1 year'));
		} else {
			$renewal_date = $GLOBALS['months'][(int) $renewal_month] . " " . ($current_year+1);
		}
	}
		
	//Get assessment areas
	$statement = "SELECT assessment_area_id, assessment_area_name FROM nse_assessment_areas WHERE active='Y' ORDER BY display_order";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->execute();
	$sql_areas->store_result();
	$sql_areas->bind_result($area_id, $area_name);
	while ($sql_areas->fetch()) {
		$hide = '';
		$assessment_content = '';
		$assessment_rating = '';
		$not_applicable = '';
		
		if (!empty($assessment_id)) {
			$sql_data->bind_param('ss', $assessment_id, $area_id);
			$sql_data->execute();
			$sql_data->store_result();
			$sql_data->bind_result($assessment_content, $assessment_rating, $not_applicable, $hide);
			$sql_data->fetch();
			$sql_data->free_result();
		} else {
			$assessment_content = '';
			$assessment_rating = '';
			$not_applicable = '';
		}
		
		if (!empty($assessment_rating)) $scripts .= "<script>set_change('$area_id')</script>";
		
		$areas .= "<div class=block_header onClick=show_block('$area_id') id=header$area_id onMouseOver=tCM('$area_id',1) onMouseOut=tCM('$area_id',0) >
						<table width=100% cellpadding=0 cellspacing=0>
							<tr>
								<td class=hRow >$area_name</td>
								<td><span id=click_$area_id class=cMes ></span></td>
								<td align=right><img src='/i/silk/group_delete.png' width=16 height=16 class=hide_indicator id=hide_indicator_$area_id title='NOT visible to clients' />&nbsp;<img src='/i/blank.png' width=16 height=16 class=edit_indicator id=indicator_$area_id /></td>
							</tr>
						</table>
					</div>
					<div class=block_content id=content$area_id>
					<textarea name=area_$area_id id=area_$area_id  onChange=\"set_change('$area_id')\" >$assessment_content</textarea>
					<table width=100% >
						<tr>
							<td>
								<table width=480px id=ratings_$area_id >
									<tr>";
		foreach ($ratings as $rate_id=>$rate_name) {
			$areas .= "<td class=field_label><input type=radio name=rate_$area_id value=$rate_id id=rate_{$rate_id}_$area_id";
			if ($rate_id == $assessment_rating) $areas .= " checked ";
			$areas .= "/><label for='rate_{$rate_id}_$area_id' >$rate_name</label></td>";
		}
										
		$areas .="					</tr>
								</table>
							</td>
							<td class=field_label align=right width=500><input type=checkbox name=hide_$area_id value='Y' id=hide_$area_id onCLick=hide('$area_id')";
		if ($hide == 'Y') {
			$areas .= " checked ";
		} else {
			$scripts .= "<script>hide('$area_id')</script>";
		}
		$areas .= " /><label for='hide_$area_id' style='padding-right: 20px'>Hide From Client</label>";
		$areas .= "<input type=checkbox name=na_$area_id value='na' id=na_$area_id onCLick=na('$area_id') ";
		if ($not_applicable == 'Y') {
			$areas .= " checked ";
			$scripts .= "<script>na('$area_id')</script>";
		}
		$areas .= " /><label for='na_$area_id' >Not Applicable</label></td>
						</tr>
					</table>
					</div>";
	}
	$sql_areas->free_result();
	$sql_areas->close();
	
	//Assessment types
	foreach ($types as $type_id=>$type_name) {
		if ($assessment_type == $type_id) {
			$type_list .= "<option value='$type_id' selected>$type_name</option>";
		} else {
			$type_list .= "<option value='$type_id' >$type_name</option>";
		}
	}
	
	//Proposed Status
	$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category WHERE active='Y' ORDER BY display_order";
	$sql_qa = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa->execute();
	$sql_qa->store_result();
	$sql_qa->bind_result($qa_code, $qa_name);
	while ($sql_qa->fetch()) {
		$qa_list .= "<option value='$qa_code' ";
		$qa_list .= $qa_code==$qa_status?' selected':'';
		$qa_list .= " >$qa_name</option>";
	}
	$sql_qa->free_result();
	$sql_qa->close();
	
	//Get category list
	$statement = "SELECT subcategory_id, subcategory_name FROM nse_restype_subcategory_lang WHERE language_code='EN' ORDER BY subcategory_name";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);
	$sql_category->execute();
	$sql_category->store_result();
	$sql_category->bind_result($category_id, $category_name);
	while ($sql_category->fetch()) {
		$category_select .= "<option value='$category_id' ";
		if ($category_id == $proposed_category) $category_select .= "selected ";
		$category_select .= "> $category_name </option>";
	}
	$sql_category->free_result();
	$sql_category->close();
	
	//Buttons
	$buttons = "<input type=button name='return_assessment' class=undo_button value='Return to Assessor' onClick='ret_assessment(\"$assessment_id\",\"$establishment_code\")' />
					<input type=submit name='disapprove_assessment' class=cross_button value='Below Standard' />
					<input type=submit name='approve_assessment' class=ok_button value='Approve Assessment' />";
	//Calculate entered date
	if (empty($entered_date) || $entered_date == '0000-00-00') $entered_date = 'Unknown';

	//Get notes
	$notes = "<table id=notes_list >";
	$statement = "SELECT user_name, note_timestamp, note_content FROM nse_establishment_notes WHERE establishment_code=? ORDER BY note_timestamp DESC";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);
	$sql_notes->bind_param('s', $establishment_code);
	$sql_notes->execute();
	$sql_notes->store_result();
	$sql_notes->bind_result($user_name, $note_timestamp, $note_content);
	while ($sql_notes->fetch()) {
		$date = date('d-m-Y h:i',$note_timestamp);
		$notes .= "<tr>";
		$notes .= "<td class=note_name>$user_name</td>";
		$notes .= "<td class=note_date>$date</td>";
		$notes .= "<td class=note_content colspan=2>$note_content</td>";
		$notes .= "</tr>";
	}
	$sql_notes->free_result();
	$sql_notes->close();
	$notes .= "</table>";

	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_replace('<!-- location -->', $location, $template);
	$template = str_replace('<!-- areas -->', $areas, $template);
	$template = str_replace('<!-- scripts -->', $scripts, $template);
	$template = str_replace('<!-- assessment_date -->', $assessment_date, $template);
	$template = str_replace('<!-- assessment_type -->', $type_list, $template);
	$template = str_replace('<!-- category_select -->', $category_select, $template);
	$template = str_replace('<!-- renewal_date -->', $renewal_date, $template);
	$template = str_replace('<!-- qa_list -->', $qa_list, $template);
	$template = str_replace('<!-- assessor_name -->', $assessor_name, $template);
	$template = str_replace('!assessment_id!', $assessment_id, $template);
	$template = str_replace(array('!establishment_code!','<!-- establishment_code -->'), $establishment_code, $template);
	$template = str_replace('<!-- contacts_list -->', $contacts_list, $template);
	$template = str_replace("<!-- buttons -->", $buttons, $template);
	$template = str_replace('<!-- website_url -->', $website_url, $template);
	$template = str_replace('<!-- notes -->', $notes, $template);
	$template = str_replace('<!-- entered_date -->', $entered_date, $template);

	echo $template;
}


function edit_assessment_form() {
//Vars
	$assessment_id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
	$establishment_code = $_GET['code'];
	$assessment_date = $_POST['assessment_date'];
	$assessment_type = $_POST['assessment_type'];
	$qa_status = $_POST['qa_status'];
	$count = 0;
	$assessment_status = 'complete';
	$email_list = '';
	$area_id = '';
	$establishment_name = '';
	$contact_name = '';
	$contact_designation = '';
	$proposed_category = $_POST['proposed_category'];
	$town = '';
	$country = '';
	$province = '';
	$contact_email = '';
	$contacts_list = $_POST['contacts_list'];
	$renewal_date = $_POST['renewal_date'];
	$tc = new tcrypt();
	$notes = $_POST['notes'];
    $complete_date = '';
    $return_date = '';
    $approved_by = '';
    $returned_by = '';

    //Prepare statement - Remove establishment link
    $statement = "DELETE FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
    $sql_estab_remove = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - New user
    $statement = "INSERT INTO nse_user (firstname, lastname, new_password, phone, cell, email, contact, top_level_type) VALUES (?,?,?,?,?,?,'1','c')";
    $sql_user_insert = $GLOBALS['dbCon']->prepare($statement);

    //repare statement - check if user already exists
    $statement = "SELECT user_id FROM nse_user WHERE firstname=? && lastname=? && email=?";
    $sql_user_check = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Attach estab
    $statement = "INSERT INTO nse_user_establishments (user_id, establishment_code, designation) VALUES (?,?,?)";
    $sql_estab_insert = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - update user
    $statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=? WHERE user_id=?";
    $sql_user_update = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Update estab
    $statement = "UPDATE nse_user_establishments SET designation=? WHERE user_id=? && establishment_code=?";
    $sql_estab_update = $GLOBALS['dbCon']->prepare($statement);

	$contact_line = explode("!|!", $contacts_list);

	foreach ($contact_line as $line) {
		if (empty($line)) continue;
        
		list($line_id, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $admin_user, $send_password, $old_email, $delete_tag) = explode('|', $line);
		if ($delete_tag == 1) {
            $sql_estab_remove->bind_param('is', $line_id, $establishment_code);
            $sql_estab_remove->execute();
		}

        //Extract firstname & lastname
		$names = explode(' ', $contact_name);
		if (count($names) == 2) {
			$firstname = $names[0];
			$lastname = $names[1];
		} else {
			$firstname = $contact_name;
			$lastname = '';
		}

        if (substr($line_id, 0,3) == 'new') {
            $uID = 0;
            $sql_user_check->bind_param('sss', $firstname, $lastname, $contact_email);
            $sql_user_check->execute();
            $sql_user_check->bind_result($uID);
            $sql_user_check->fetch();
            $sql_user_check->free_result();

            if ($uID == 0) {
                $password = '';
                if ($admin_user == '1') {
                    $password = $GLOBALS['security']->generate_password(6);
					$password = $tc->encrypt($password);
                }
                $sql_user_insert->bind_param('ssssss', $firstname, $lastname, $password, $contact_tel, $contact_cell, $contact_email);
                $sql_user_insert->execute();
                $uID = $sql_user_insert->insert_id;

                $sql_estab_insert->bind_param('iss', $uID, $establishment_code, $contact_designation);
                $sql_estab_insert->execute();
            } else {
                $sql_estab_insert->bind_param('iss', $uID, $establishment_code, $contact_designation);
                $sql_estab_insert->execute();
            }
        } else {
            $sql_user_update->bind_param('sssssi', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $line_id);
            $sql_user_update->execute();

            $sql_estab_update->bind_param('sis', $contact_designation, $line_id, $establishment_code);
            $sql_estab_update->execute();
        }

    }


	if (isset($_GET['return'])) {
        $assessment_status = 'returned';
        $return_date = date('Y-m-d');
        $returned_by = $_SESSION['dbweb_user_id'];
    } else {
        $complete_date = date('Y-m-d');
        $approved_by = $_SESSION['dbweb_user_id'];
    }

	//Update Notes
	//Prepare statement - add note
	$statement = "INSERT INTO nse_establishment_notes (establishment_code, user_name, note_timestamp, note_content, module_name ) VALUES (?,?,?,?,'assessments')";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);

	//Get user name
	$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bind_param('i', $_SESSION['dbweb_user_id']);
	$sql_user->execute();
	$sql_user->store_result();
	$sql_user->bind_result($user_name);
	$sql_user->fetch();
	$sql_user->free_result();
	$sql_user->close();

	$line = explode('##',$notes);
	foreach ($line as $note_line) {
		if (empty($note_line)) continue;
		$note_items = explode('|', $note_line);
		//Calculate timestamp
		list($date, $time) = explode(' ', $note_items[1]);
		list($day, $month, $year) = explode('-', $date);
		list($hours, $minutes) = explode(':',$time);
		$timestamp = mktime($hours, $minutes, 0, $month, $day, $year);

		$sql_notes->bind_param('ssss', $establishment_code, $user_name, $timestamp, $note_items[2]);
		$sql_notes->execute();
	}
	$sql_notes->close();

	//Get list of areas
	$statement = "SELECT assessment_area_id FROM nse_assessment_areas WHERE active='Y'";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->execute();
	$sql_areas->store_result();
	$sql_areas->bind_result($area_id);
	while ($sql_areas->fetch()) {
		$areas[] = $area_id;
	}
	$sql_areas->free_result();
	$sql_areas->close();

	//Renewal Date
	//echo "---- TEST START ----<br>";
	//echo "1: $renewal_date<br>";
	$renewal_date = "01 $renewal_date";
	//echo "2: $renewal_date<br>";
	$renewal_date = date('Y-m-d', strtotime($renewal_date));
	//echo "3: $renewal_date<br>";
	//echo "---- TEST END ----<br>";

	if (empty($assessment_id)) {
		$statement = "INSERT INTO nse_establishment_assessment (establishment_code, assessment_date, assessment_status, assessment_type, qa_status, staff_notes, propose_category, renewal_date=?) VALUES (?,?,?,?,?,?,?,?)";
		$sql_assessment_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessment_insert->bind_param('ssssssss', $establishment_code, $assessment_date, $assessment_status, $assessment_type, $qa_status, $staff_notes, $proposed_category, $renewal_date);
		$sql_assessment_insert->execute();
		$assessment_id = $sql_assessment_insert->insert_id;
		$sql_assessment_insert->close();
	} else {
		$statement = "UPDATE nse_establishment_assessment SET assessment_date=?, assessment_status=?, assessment_type=?, qa_status=?, staff_notes=?, proposed_category=?, renewal_date=?, approval_date=?, return_date=?, approved_by=?, returned_by=? WHERE assessment_id=?";
		$sql_assessment_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessment_update->bind_param('ssssssssssss', $assessment_date, $assessment_status, $assessment_type, $qa_status, $staff_notes, $proposed_category, $renewal_date, $complete_date, $return_date,$approved_by,$returned_by, $assessment_id);
		$sql_assessment_update->execute();
		$sql_assessment_update->close();
	}
	
	//Prepare statement - check area exists
	$statement = "SELECT count(*) FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
	$sql_area_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - update area
	$statement = "UPDATE nse_establishment_assessment_data SET assessment_content=?, assessment_rating=?, not_applicable=?, hide_from_client=? WHERE assessment_id=? && assessment_area_id=?";
	$sql_area_update = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare statement - insert area
	$statement = "INSERT INTO nse_establishment_assessment_data (assessment_content, assessment_rating, not_applicable, assessment_id, assessment_area_id, hide_from_client) VALUES (?,?,?,?,?,?)";
	$sql_area_insert = $GLOBALS['dbCon']->prepare($statement);
	
	foreach ($areas as $area_id) {
		$count = 0;
		$sql_area_check->bind_param('ss', $assessment_id, $area_id);
		$sql_area_check->execute();
		$sql_area_check->store_result();
		$sql_area_check->bind_result($count);
		$sql_area_check->fetch();
		$sql_area_check->free_result();
		
		if (isset($_POST['na_' . $area_id])) {
			$not_applicable = 'Y';
			$assessment_content = '';
			$assessment_rating = '';
		} else {
			$not_applicable = '';
			$assessment_content = $_POST['area_' . $area_id];
			$assessment_rating = isset($_POST['rate_' . $area_id])?$_POST['rate_' . $area_id]:'';
		}
		$hide = isset($_POST['hide_' . $area_id])?'Y':'';
		
		if ($count == 0) {
			$sql_area_insert->bind_param('ssssss', $assessment_content, $assessment_rating, $not_applicable, $assessment_id, $area_id, $hide);
			$sql_area_insert->execute();
		} else {
			$sql_area_update->bind_param('ssssss', $assessment_content, $assessment_rating, $not_applicable, $hide, $assessment_id, $area_id);
			$sql_area_update->execute();
		}
	}
	
	if (isset($_GET['return'])) {
		echo "<script>alert('Assessment returned to assessor'); document.location='/modules/assessment_s/assessor_s.php?f=list_assessments';</script>";
	} else {
		//Get establishment_name
		$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
		$sql_estab = $GLOBALS['dbCon']->prepare($statement);
		$sql_estab->bind_param('s', $establishment_code);
		$sql_estab->execute();
		$sql_estab->store_result();
		$sql_estab->bind_result($establishment_name);
		$sql_estab->fetch();
		$sql_estab->free_result();
		$sql_estab->close();
		
		//Get Location
		$statement = "SELECT b.town_name, c.province_name, d.country_name
						FROM nse_establishment_location AS a
						LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
						LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
						LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
						WHERE a.establishment_code=?";
		$sql_location = $GLOBALS['dbCon']->prepare($statement);
		$sql_location->bind_param('s', $establishment_code);
		$sql_location->execute();
		$sql_location->store_result();
		$sql_location->bind_result($town, $province, $country);
		$sql_location->fetch();
		$sql_location->free_result();
		$sql_location->close();
		
		if (!empty($town)) $location = $town;
		if (!empty($province)) {
			$location .= ", $province";
		} else {
			if (!empty($country)) $location .= ", $country";
		}
		
		//Get email addresses
		$counter = 0;
		$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
                        FROM nse_user_establishments AS a
                        JOIN nse_user AS b ON a.user_id=b.user_id
                        WHERE a.establishment_code=?";
		$sql_email = $GLOBALS['dbCon']->prepare($statement);
		$sql_email->bind_param('s', $establishment_code);
		$sql_email->execute();
		$sql_email->store_result();
		$sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
		while ($sql_email->fetch()) {
			$email_list .= "<br /><input type=checkbox name=emails[] id=email_$counter value='$uID' ";
			//if ($counter == 0) $email_list .= " checked ";
			$email_list .= "/><label for=email_$counter >$contact_name ($contact_email) - $contact_designation</label>" . PHP_EOL;
			$counter++;
		}
		$sql_email->free_result();
		$sql_email->close();
		
		//Get Assessor Email Adddress
		$statement = "SELECT b.firstname, b.lastname , b.email, b.user_id
						FROM nse_establishment AS a
						JOIN nse_user AS b ON a.assessor_id=b.user_id
						WHERE a.establishment_code=?";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->bind_param('s', $establishment_code);
		$sql_assessor->execute();
		$sql_assessor->store_result();
		$sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email, $uID);
		$sql_assessor->fetch();
		$sql_assessor->free_result();
		$sql_assessor->close();
		if (!empty($assessor_email)) {
			$email_list .= "<br /><input type=checkbox name=assessor_email id=email_$counter value='$uID' /><label for=email_$counter >Copy Assessor: $assessor_firstname $assessor_lastname ($assessor_email)</label>" . PHP_EOL;
		}
		
		$email_list = substr($email_list, 6);
		
		//Get template
		$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/assessment_s/html/assessment_email_form.html");
		
		//Replace Tags
		$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
		$template = str_replace('<!-- location -->', $location, $template);
		$template = str_replace('<!-- email_list -->', $email_list, $template);
		$template = str_replace('!assessment_id!', $assessment_id, $template);
		
		echo $template;
	}
}


function dissaprove_assessment_form() {
	//Vars
	$location = '';
	$establishment_code = $_GET['code'];
	$assessment_id = $_GET['id'];
	$establishment_name = '';
	$town = '';
	$country = '';
	$province = '';
	$email_list = '';
	$contact_name = '';
	$contact_email = '';
	$contact_designation = '';
	
	
	//Get tamplate
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/html/dissaprove_email_form.html');
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	//Get Location
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->store_result();
	$sql_location->bind_result($town, $province, $country);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	
	if (!empty($town)) $location = $town;
	if (!empty($province)) {
		$location .= ", $province";
	} else {
		if (!empty($country)) $location .= ", $country";
	}
	
	//Get email addresses
	$counter = 0;
	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email
                        FROM nse_user_establishments AS a
                        JOIN nse_user AS b ON a.user_id=b.user_id
                        WHERE a.establishment_code=?";
	$sql_email = $GLOBALS['dbCon']->prepare($statement);
	$sql_email->bind_param('s', $establishment_code);
	$sql_email->execute();
	$sql_email->store_result();
	$sql_email->bind_result($contact_name, $contact_designation, $contact_email);
	while ($sql_email->fetch()) {
		$email_list .= "<br /><input type=checkbox name=emails[] id=email_$counter value='$contact_email'";
		//if ($counter == 0) $email_list .= " checked ";
		$email_list .= "/><label for=email_$counter >$contact_name ($contact_email) - $contact_designation</label>" . PHP_EOL;
		$counter++;
	}
	$sql_email->free_result();
	$sql_email->close();
	
	//Get Assessor Email Adddress
	$statement = "SELECT b.firstname, b.lastname , b.email
					FROM nse_establishment AS a
					JOIN nse_user AS b ON a.assessor_id=b.user_id
					WHERE a.establishment_code=?";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('s', $establishment_code);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();
	if (!empty($assessor_email)) {
		$email_list .= "<br /><input type=checkbox name=assessor_email id=email_$counter value='$assessor_email' /><label for=email_$counter >Copy Assessor: $assessor_firstname $assessor_lastname ($assessor_email)</label>" . PHP_EOL;
	}
	
	$email_list = substr($email_list, 6);
	
	//Update assessment status
	$statement = "UPDATE nse_establishment_assessment SET assessment_status='below' WHERE assessment_id=?";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('s', $assessment_id);
	$sql_status->execute();
	$sql_status->free_result();
	$sql_status->close();
	
	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_replace('<!-- location -->', $location, $template);
	$template = str_replace('<!-- email_list -->', $email_list, $template);
	
	echo $template;
}


function dissapprove_assessment_send() {
	//Vars
	$assessment_id = $_GET['id'];
	$assessment_date = '';
	$assessor_name = '';
	$assessor_email = '';
	$email_counter = 0;
	$email_template = file_get_contents(SITE_ROOT . '/modules/assessment_s/html/failed_email.html');
	$establishment_code = $_GET['code'];
	
	$mail = new PHPMailer();

	//Get assessor
	$statement = "SELECT CONCAT(b.firstname,' ', b.lastname), b.email
					FROM nse_establishment AS a
					LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
					WHERE a.establishment_code=?
					LIMIT 1";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('s', $establishment_code);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_name, $assessor_email);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	$email_list = '';
	
	if (isset($_POST['send_email']) && $_POST['send_email'] == '1') {
		//Prepare statement - email address details
		$statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user_establishments WHERE user_id=?";
		$sql_email = $GLOBALS['dbCon']->prepare($statement);
		
		//Get establishment_name
		$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
		$sql_estab = $GLOBALS['dbCon']->prepare($statement);
		$sql_estab->bind_param('s', $establishment_code);
		$sql_estab->execute();
		$sql_estab->store_result();
		$sql_estab->bind_result($establishment_name);
		$sql_estab->fetch();
		$sql_estab->free_result();
		$sql_estab->close();
		
		//Get assessor
		$statement = "SELECT CONCAT(b.firstname,' ', b.lastname), b.email
						FROM nse_establishment AS a
						LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
						WHERE a.establishment_code=?
						LIMIT 1";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->bind_param('s', $establishment_code);
		$sql_assessor->execute();
		$sql_assessor->store_result();
		$sql_assessor->bind_result($assessor_name, $assessor_email);
		$sql_assessor->fetch();
		$sql_assessor->free_result();
		$sql_assessor->close();
		
		//Create Report
		include 'create_report.php';
		
		foreach ($_POST['emails'] as $uID) {
			
			//Get details
			$contact_name = '';
			$encrypted_password = '';
			$sql_email->bind_param('s', $uID);
			$sql_email->execute();
			$sql_email->store_result();
			$sql_email->bind_result($contact_name, $email);
			$sql_email->fetch();
			$sql_email->free_result();
	
			$email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);
			$email_content = str_replace('<!-- visit_date -->', $assessment_date, $email_content);
			$email_content = str_replace('<!-- assessor -->', $assessor_name, $email_content);
			
			try {
				$mail->ClearAllRecipients();
				$mail->AddAddress($email, $contact_name);
				$mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
				$mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
				$mail->Subject = 'AA Quality Assurance';
				$mail->MsgHTML($email_content);
				$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
				$mail->AddAttachment($report_file, 'report.pdf');
				$mail->Send();
				$email_list .= "$contact_name, ";
				if ($email_counter == 1) $assessor_email_body = $email_content;
				$email_counter++;
				
			} catch (phpmailerException $e) {
				echo $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				echo $e->getMessage(); //Boring error messages from anything else!
			}
		}
		
		if (isset($_POST['assessor_email']) && !empty($_POST['assessor_email'])) {
            $sql_email->bind_param('s', $_POST['assessor_email']);
			$sql_email->execute();
			$sql_email->store_result();
			$sql_email->bind_result($contact_name, $email);
			$sql_email->fetch();
			$sql_email->free_result();
			try {
				$mail->ClearAllRecipients();
				$mail->ClearAttachments();
				$mail->AddAddress($email, $assessor_name);
				$mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
				$mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
				$mail->Subject = "COPY :: AA Quality Assured Status for $establishment_name";
				$mail->MsgHTML($email_content);
				$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
				$mail->AddAttachment($report_file, 'report.pdf');
				$mail->Send();
				$email_list .= "$assessor_name, ";
			} catch (phpmailerException $e) {
				echo $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				echo $e->getMessage(); //Boring error messages from anything else!
			}
		}
		
		$email_list = substr($email_list, 0, -2);
		echo "<script>alert('Email sent to $email_list.');</script>";
	} else {
		//echo "<script>alert('Status set to \"Below Standard\". No emails sent.');</script>";
	}
	
	echo "<script>document.location = '/modules/assessment_s/assessor_s.php?f=list_assessments'</script>";
}


function approve_assessment_email() {
	//Vars
	$email_type = $_POST['send_email'];
	$establishment_code = $_GET['code'];
	$assessment_id = $_REQUEST['id'];
	$assessment_date = '';
	$assessor_name = '';
	$assessor_email = '';
	$attach_file = '';
	$email_counter = 1;
	$status = '';
	$logo = '';
	$establishment_name = '';
	$certificate_file = '';
	$report_file = '';
	$renewal_month = '';
	$renewal_year = '';
	$from_date = '';
	$to_date = '';
	
	$mail = new PHPMailer();
	
	//Get Assessment Data
	$statement = "SELECT DATE_FORMAT(a.assessment_date, '%d %M %Y'), b.aa_category_name, b.lowres_image, DATE_FORMAT(a.renewal_date,'%M'), DATE_FORMAT(a.renewal_date, '%Y')
					FROM nse_establishment_assessment AS a
					JOIN nse_aa_category AS b ON a.qa_status=b.aa_category_code
					WHERE a.assessment_id=? LIMIT 1";
	$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessment->bind_param('i', $assessment_id);
	$sql_assessment->execute();
	$sql_assessment->store_result();
	$sql_assessment->bind_result($assessment_date, $status, $logo, $renewal_month, $renewal_year);
	$sql_assessment->fetch();
	$sql_assessment->free_result();
	$sql_assessment->close();

	//Get assessor
	$statement = "SELECT CONCAT(b.firstname,' ', b.lastname), b.email
					FROM nse_establishment AS a
					LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
					WHERE a.establishment_code=?
					LIMIT 1";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('s', $establishment_code);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_name, $assessor_email);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	//Assemble Establishment link
	$establishment_link = $establishment_code . "_$establishment_name";
	$establishment_link = strtolower(str_replace(' ', '_', $establishment_link));
	$establishment_link = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $establishment_link);
	$establishment_link = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $establishment_link );
	$establishment_link = htmlentities($establishment_link);
	$establishment_link = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $establishment_link);
	$establishment_link = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $establishment_link);
	$establishment_link = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $establishment_link);
	$establishment_link = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $establishment_link);
	$establishment_link = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $establishment_link);
	$establishment_link = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $establishment_link);
	$establishment_link .= ".html";
	
	
	switch($email_type) {
		case 0:
			//Get email template
			$email_template = file_get_contents(SITE_ROOT . '/modules/assessment_s/html/unpaid_email.html');
			
			//Replace Tags
			$email_template = str_replace('<!-- visit_date -->', $assessment_date, $email_template);
			$email_template = str_replace('<!-- status -->', $status, $email_template);
			$email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);
			$email_template = str_replace('<!-- establishment_link -->', $establishment_link, $email_template);
			
			//Prepare statement - email address details
			$statement = "SELECT CONCAT(firstname, ' ' , lastname), email FROM nse_user WHERE user_id=?";
			$sql_email = $GLOBALS['dbCon']->prepare($statement);
			
			//Move uploaded file
			if (is_uploaded_file($_FILES['invoice_file']['tmp_name'])) {
				move_uploaded_file($_FILES['invoice_file']['tmp_name'], SITE_ROOT . '/temp/' . $_FILES['invoice_file']['name']);
				$attach_file = $_FILES['invoice_file']['name'];
			}
			
			foreach ($_POST['emails'] as $uID) {
				//Get details
				$contact_name = '';
				$sql_email->bind_param('s', $uID);
				$sql_email->execute();
				$sql_email->store_result();
				$sql_email->bind_result($contact_name, $email);
				$sql_email->fetch();
				$sql_email->free_result();
				
				
				if (empty($contact_name)) $contact_name = 'QA Member';
				$email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);
				
				try {
					$mail->ClearAllRecipients();
					$mail->AddAddress($email, $contact_name);
					$mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->Subject = 'AA Quality Assurance';
					$mail->MsgHTML($email_content);
					if (!empty($attach_file)) $mail->AddAttachment(SITE_ROOT . "/temp/$attach_file", $attach_file);
					$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
					$mail->Send();
					if ($email_counter == 1) $assessor_email_body = $email_content;
					$email_counter++;
				} catch (phpmailerException $e) {
					echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
					echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
			$sql_email->close();
			
			if (isset($_POST['assessor_email']) && !empty($_POST['assessor_email'])) {
                $sql_email->bind_param('s', $_POST['assessor_email']);
				$sql_email->execute();
				$sql_email->store_result();
				$sql_email->bind_result($contact_name, $email);
				$sql_email->fetch();
				$sql_email->free_result();
				try {
					$mail->ClearAllRecipients();
					$mail->ClearAttachments();
					$mail->AddAddress($email, $assessor_name);
					$mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->Subject = "COPY :: AA Quality Assurance for $establishment_name";
					$mail->MsgHTML($assessor_email_body);
					if (!empty($attach_file)) $mail->AddAttachment(SITE_ROOT . "/temp/$attach_file", $attach_file);
					$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
					$mail->Send();
					
				} catch (phpmailerException $e) {
					echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
					echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
			
			echo "<script>alert('Email sent');document.location='/modules/assessment_s/assessor_s.php?f=list_assessments';</script>";
			
			break;
		case 1:
			include 'create_certificate.php';
			include 'create_report.php';
			
			//Caculate Renewal Date
			$from_year = (int) $renewal_year - 1;
			$from_date = $renewal_month . ' ' . $from_year;
			$to_date = $renewal_month . ' ' . $renewal_year;

			update_website($establishment_code, $assessment_id);
			
			//Get email template
			$email_template = file_get_contents(SITE_ROOT . '/modules/assessment_s/html/paid_email.html');
			
			//Replace Tags
			$email_template = str_replace('<!-- visit_date -->', $assessment_date, $email_template);
			$email_template = str_replace('<!-- status -->', $status, $email_template);
			$email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);
			$email_template = str_replace('<!-- establishment_link -->', $establishment_link, $email_template);
			$email_template = str_replace('<!-- establishment_code -->', $establishment_code, $email_template);
			$email_template = str_replace('<!-- from_date -->', $from_date, $email_template);
			$email_template = str_replace('<!-- to_date -->', $to_date, $email_template);
			
			//Prepare statement - email address details
			$statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
			$sql_email = $GLOBALS['dbCon']->prepare($statement);
			
			foreach ($_POST['emails'] as $uID) {
				//Get details
				$contact_name = '';
				$encrypted_password = '';
				$sql_email->bind_param('s', $uID);
				$sql_email->execute();
				$sql_email->store_result();
				$sql_email->bind_result($contact_name, $email);
				$sql_email->fetch();
				$sql_email->free_result();
				
				if (empty($contact_name)) $contact_name = 'QA Member';
				$email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);
				
				try {
					$mail->ClearAllRecipients();
					$mail->AddAddress($email, $contact_name);
					$mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->Subject = "Your AA Quality Assured Status is hereby confirmed for $establishment_name";
					$mail->MsgHTML($email_content);
					$mail->AddAttachment($certificate_file, 'certificate.pdf');
					$mail->AddAttachment($report_file, 'report.pdf');
					$mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$logo");
					$mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/documents/AAQA_ORDER_SIGNAGE.doc");
					$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
					$mail->Send();
					if ($email_counter == 1) $assessor_email_body = $email_content;
					$email_counter++;
				} catch (phpmailerException $e) {
					echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
					echo $e->getMessage(); //Boring error messages from anything else!
				}
				
				
			}
			
			
			if (isset($_POST['assessor_email']) && !empty($_POST['assessor_email'])) {
                $sql_email->bind_param('s', $_POST['assessor_email']);
				$sql_email->execute();
				$sql_email->store_result();
				$sql_email->bind_result($assessor_name, $email);
				$sql_email->fetch();
				$sql_email->free_result();
				try {
					$mail->ClearAllRecipients();
					$mail->ClearAttachments();
					$mail->AddAddress($email, $assessor_name);
					$mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
					$mail->Subject = "COPY :: AA Quality Assured Status for $establishment_name";
					$mail->MsgHTML($assessor_email_body);
					$mail->AddAttachment($certificate_file, 'certificate.pdf');
					$mail->AddAttachment($report_file, 'report.pdf');
					$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
					$mail->Send();
				} catch (phpmailerException $e) {
					echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
					echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
			$sql_email->close();

			//Log Activity
			$GLOBALS['log_tool']->write_entry("Assessment Approval: $establishment_name ($establishment_code) - $assessment_id", $_SESSION['dbweb_user_id']);
			
			echo "<script>alert('Approval process complete');document.location='/modules/assessment_s/assessor_s.php?f=list_assessments';</script>";
			//echo "<script>alert('Approval process complete');</script>";
			//echo "<a href='/modules/assessment_s/assessor_s.php?f=list_assessments'>Click to continue</a>";
			break;
			
		case 2:
			
			update_website($establishment_code, $assessment_id);
			
			//echo "<script>document.location='/modules/assessment_s/assessor_s.php?f=list_assessments';</script>";
			
			break;
	}
}


function view_assessment() {
	//Vars
	$areas = '';
	$area_id = '';
	$area_name = '';
	$assessment_date = '';
	$assessment_id = $_GET['id'];
	$assessment_notes = '';
	$assessment_type = '';
	$category_name = '';
	$category_select = '';
	$country = '';
	$establishment_code = $_GET['code'];
	$establishment_name = '';
	$hide = '';
	$location = '';
	$province = '';
	$proposed_category = '';
	$qa_list = '';
	$qa_name = '';
	$qa_status = '';
	$ratings = array(1=>'Excellent','Very Good','Good','Fair','Poor');
	$renewal_date = '';
	$scripts = '';
	$staff_notes = '';
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/html/assessment_view.html');
	$town = '';
	$types = array('new establishment'=>'New Establishment', 'award visit'=>'Awards Visit', 'renewal'=>'Renewal');
	$type_list = '';
	
	//Get establishment_name
	$statement = "SELECT establishment_name, website_url FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name, $website_url);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();

	//Get Assessor Name
	$statement = "SELECT assessor_name FROM nse_establishment_assessment WHERE assessment_id=?";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('i', $assessment_id);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_name);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();

	//Assemble website url
	$website_url = trim($website_url);
	if (substr($website_url, 0, 7) == 'http://') $website_url = substr($website_url,7);
	if (!empty($website_url)) {
		$website_url = "<a href='http://$website_url' target=_blank >http://$website_url</a>";
	} else {
		$website_url = "No Website Listed";
	}

	//Get Location
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->store_result();
	$sql_location->bind_result($town, $province, $country);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	
	if (!empty($town)) $location .= $town;
	if (!empty($province)) {
		$location .= ", $province";
	} else {
		if (!empty($country)) $location .= ", $country";
	}
	
	//Get data
	$statement = "SELECT DATE_FORMAT(assessment_date,'%Y-%m-%d'), assessment_type, qa_status, DATE_FORMAT(renewal_date, '%M %Y'), proposed_category, assessment_notes, staff_notes FROM nse_establishment_assessment WHERE assessment_id=? LIMIT 1";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('s', $assessment_id);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($assessment_date, $assessment_type, $qa_status, $renewal_date, $proposed_category, $assessment_notes, $staff_notes);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	//Prepare Statement - Get area data
	$statement = "SELECT assessment_content, assessment_rating, not_applicable, hide_from_client FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	
	//Get assessment areas
	$statement = "SELECT assessment_area_id, assessment_area_name FROM nse_assessment_areas WHERE active='Y' ORDER BY display_order";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->execute();
	$sql_areas->store_result();
	$sql_areas->bind_result($area_id, $area_name);
	while ($sql_areas->fetch()) {
		$hide = '';
		$assessment_content = '';
		$assessment_rating = '';
		$not_applicable = '';
		
		if (!empty($assessment_id)) {
			$sql_data->bind_param('ss', $assessment_id, $area_id);
			$sql_data->execute();
			$sql_data->store_result();
			$sql_data->bind_result($assessment_content, $assessment_rating, $not_applicable, $hide);
			$sql_data->fetch();
			$sql_data->free_result();
		} else {
			$assessment_content = '';
			$assessment_rating = '';
			$not_applicable = '';
		}
		
		if (!empty($assessment_rating)) $scripts .= "<script>set_change('$area_id')</script>";
		
		$areas .= "<div class=block_header onClick=show_block('$area_id') id=header$area_id onMouseOver=tCM('$area_id',1) onMouseOut=tCM('$area_id',0) >
						<table width=100% cellpadding=0 cellspacing=0>
							<tr>
								<td class=hRow >$area_name</td>
								<td><span id=click_$area_id class=cMes ></span></td>
								<td align=right></td>
							</tr>
						</table>
					</div>
					<div class=block_content id=content$area_id>";
		$areas .= $not_applicable=='Y'?'':"	<div style='background-color: white; height: 50; padding: 2px' >$assessment_content</div>";
		$areas .= "
					<table width=100% >
						<tr>
							<td>
								<table width=400px id=ratings_$area_id >
									<tr><td class=field_label >";
		foreach ($ratings as $rate_id=>$rate_name) {
			$areas .= "";
			if ($rate_id == $assessment_rating) {
				$areas .= "<span style='color: black; font-weight: bold; padding-right: 5px' >";
			} else {
				$areas .= "<span style='color: grey; padding-right: 5px' >";
			}
			$areas .= "$rate_name</span> ";
		}
										
		$areas .="					</td></tr>
								</table>
							</td>
							<td class=field_label align=right width=500>";
		if ($hide == 'Y') {
			$areas .= " Hidden From Client </td><td class=field_label width=150px align=right>";
		}
		if ($not_applicable == 'Y') {
			$areas .= " Not Applicable </td>";
		}
		$areas .= " </td>
						</tr>
					</table>
					</div>";
	}
	$sql_areas->free_result();
	$sql_areas->close();
	
	//Assessment types
	$assessment_type = $types[$assessment_type];
	
	//Proposed Status
	$statement = "SELECT aa_category_name FROM nse_aa_category WHERE aa_category_code=? LIMIT 1";
	$sql_qa = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa->bind_param('s', $qa_status);
	$sql_qa->execute();
	$sql_qa->store_result();
	$sql_qa->bind_result($qa_name);
	$sql_qa->fetch();
	$sql_qa->free_result();
	$sql_qa->close();
	
	//Get category list
	$statement = "SELECT subcategory_name FROM nse_restype_subcategory_lang WHERE language_code='EN' && subcategory_id=?";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);
	$sql_category->bind_param('s', $proposed_category);
	$sql_category->execute();
	$sql_category->store_result();
	$sql_category->bind_result($category_name);
	$sql_category->fetch();
	$sql_category->free_result();
	$sql_category->close();
	
	//Assemble certificate link
	$certificate_name = strtolower($establishment_code . "_" . str_replace(' ', '_', $renewal_date));
	if (!is_file($_SERVER['DOCUMENT_ROOT'] . "/certificates/$certificate_name.pdf")) {
		include 'create_certificate.php';
	}
	
	//Assemble report link
	$report_name = strtolower($establishment_code . "_report_" . str_replace(' ', '_', $renewal_date));
	if (!is_file($_SERVER['DOCUMENT_ROOT'] . "/reports/$report_name.pdf")) {
		include 'create_report.php';
	}

	//Get notes
	$notes = "<table id=notes_list >";
	$statement = "SELECT user_name, note_timestamp, note_content FROM nse_establishment_notes WHERE establishment_code=? ORDER BY note_timestamp DESC";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);
	$sql_notes->bind_param('s', $establishment_code);
	$sql_notes->execute();
	$sql_notes->store_result();
	$sql_notes->bind_result($user_name, $note_timestamp, $note_content);
	while ($sql_notes->fetch()) {
		$date = date('d-m-Y h:i',$note_timestamp);
		$notes .= "<tr>";
		$notes .= "<td class=note_name>$user_name</td>";
		$notes .= "<td class=note_date>$date</td>";
		$notes .= "<td class=note_content colspan=2>$note_content</td>";
		$notes .= "</tr>";
	}
	$sql_notes->free_result();
	$sql_notes->close();
	$notes .= "</table>";

	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_replace('<!-- location -->', $location, $template);
	$template = str_replace('<!-- areas -->', $areas, $template);
	$template = str_replace('<!-- scripts -->', $scripts, $template);
	$template = str_replace('<!-- assessment_date -->', $assessment_date, $template);
	$template = str_replace('<!-- assessment_type -->', $assessment_type, $template);
	$template = str_replace('<!-- category -->', $category_name, $template);
	$template = str_replace('<!-- renewal_date -->', $renewal_date, $template);
	$template = str_replace('<!-- status -->', $qa_name, $template);
	$template = str_replace('<!-- assessor_name -->', $assessor_name, $template);
	$template = str_replace('<!-- website_url -->', $website_url, $template);
	$template = str_replace('!code!', $establishment_code, $template);
	$template = str_replace('!id!', $assessment_id, $template);
	$template = str_replace('!certificate_name!', $certificate_name, $template);
	$template = str_replace('!report_name!', $report_name, $template);
	$template = str_replace('!assessment_notes!', $assessment_notes, $template);
	$template = str_replace('!staff_notes!', $staff_notes, $template);
	$template = str_replace('<!-- notes -->', $notes, $template);
	
	echo $template;
	
}


function welcome_pack_form() {
	//Vars
	$establishment_code = $_GET['code'];
	$establishment_name = '';
	$location = '';
	$town = '';
	$province = '';
	$country = '';
	$email_list = '';
	
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/html/welcome_pack_form.html');
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	//Get Location
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->store_result();
	$sql_location->bind_result($town, $province, $country);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	
	if (!empty($town)) $location .= $town;
	if (!empty($province)) {
		$location .= ", $province";
	} else {
		if (!empty($country)) $location .= ", $country";
	}
	
	//Get email addresses
	$counter = 0;
	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
                        FROM nse_user_establishments AS a
                        JOIN nse_user AS b ON a.user_id=b.user_id
                        WHERE a.establishment_code=?";
	$sql_email = $GLOBALS['dbCon']->prepare($statement);
	$sql_email->bind_param('s', $establishment_code);
	$sql_email->execute();
	$sql_email->store_result();
	$sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
	while ($sql_email->fetch()) {
		$email_list .= "<br /><input type=checkbox name=emails[] id=email_$counter value='$uID' ";
		//if ($counter == 0) $email_list .= " checked ";
		$email_list .= "/><label for=email_$counter >$contact_name ($contact_email) - $contact_designation</label>" . PHP_EOL;
		$counter++;
	}
	$sql_email->free_result();
	$sql_email->close();
	
	//Get Assessor Email Adddress
	$statement = "SELECT b.firstname, b.lastname , b.email, b.user_id
					FROM nse_establishment AS a
					JOIN nse_user AS b ON a.assessor_id=b.user_id
					WHERE a.establishment_code=?";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('s', $establishment_code);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email, $uID);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();
	if (!empty($assessor_email)) {
		$email_list .= "<br /><input type=checkbox name=assessor_email id=email_$counter value='$uID' /><label for=email_$counter >Copy Assessor: $assessor_firstname $assessor_lastname ($assessor_email)</label>" . PHP_EOL;
	}
	
	$email_list = substr($email_list, 6);
	
	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_replace('<!-- location -->', $location, $template);
	$template = str_replace('<!-- email_list -->', $email_list, $template);
	
	echo $template;
}


function welcome_pack_send() {
	//Vars
	$assessment_id = $_GET['id'];
	$establishment_code = $_GET['code'];
	$email_counter = 1;
	
	$mail = new PHPMailer();
	
	//Get Assessment Data
	$statement = "SELECT DATE_FORMAT(a.assessment_date, '%d %M %Y'), b.aa_category_name, b.lowres_image, DATE_FORMAT(a.renewal_date,'%M'), DATE_FORMAT(a.renewal_date, '%Y')
					FROM nse_establishment_assessment AS a
					JOIN nse_aa_category AS b ON a.qa_status=b.aa_category_code
					WHERE a.assessment_id=? LIMIT 1";
	$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessment->bind_param('i', $assessment_id);
	$sql_assessment->execute();
	$sql_assessment->store_result();
	$sql_assessment->bind_result($assessment_date, $status, $logo, $renewal_month, $renewal_year);
	$sql_assessment->fetch();
	$sql_assessment->free_result();
	$sql_assessment->close();
	
	//Get assessor
	$statement = "SELECT CONCAT(b.firstname,' ', b.lastname), b.email
					FROM nse_establishment AS a
					LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
					WHERE a.establishment_code=?
					LIMIT 1";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('s', $establishment_code);
	$sql_assessor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_name, $assessor_email);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	//Assemble Establishment link
	$establishment_link = $establishment_code . "_$establishment_name";
	$establishment_link = strtolower(str_replace(' ', '_', $establishment_link));
	$establishment_link = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $establishment_link);
	$establishment_link = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $establishment_link );
	$establishment_link = htmlentities($establishment_link);
	$establishment_link = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $establishment_link);
	$establishment_link = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $establishment_link);
	$establishment_link = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $establishment_link);
	$establishment_link = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $establishment_link);
	$establishment_link = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $establishment_link);
	$establishment_link = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $establishment_link);
	$establishment_link .= ".html";
	
	include 'create_certificate.php';
	include 'create_report.php';
	
	//Password encryption
	$from_year = (int) $renewal_year - 1;
	$from_date = $renewal_month . ' ' . $from_year;
	$to_date = $renewal_month . ' ' . $renewal_year;
	
	//Get email template
	$email_template = file_get_contents(SITE_ROOT . '/modules/assessment_s/html/paid_email.html');
	
	//Replace Tags
	$email_template = str_replace('<!-- visit_date -->', $assessment_date, $email_template);
	$email_template = str_replace('<!-- status -->', $status, $email_template);
	$email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);
	$email_template = str_replace('<!-- establishment_link -->', $establishment_link, $email_template);
	$email_template = str_replace('<!-- establishment_code -->', $establishment_code, $email_template);
	$email_template = str_replace('<!-- from_date -->', $from_date, $email_template);
	$email_template = str_replace('<!-- to_date -->', $to_date, $email_template);
	
	//Prepare statement - email address details
	$statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
	$sql_email = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Password
	$statement = "SELECT new_password FROM nse_user WHERE email=? LIMIT 1";
	$sql_password = $GLOBALS['dbCon']->prepare($statement);

    if (isset($_POST['emails']) && !empty($_POST['emails'])) {
        foreach ($_POST['emails'] as $uID) {
            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('s', $uID);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            if (empty($contact_name)) $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
                $mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
                $mail->Subject = 'Your AA Quality Assured Status is hereby confirmed';
                $mail->MsgHTML($email_content);
                $mail->AddAttachment($certificate_file, 'certificate.pdf');
                $mail->AddAttachment($report_file, 'report.pdf');
                $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$logo");
                $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/documents/AAQA_ORDER_SIGNAGE.doc");
                $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                $mail->Send();
                if ($email_counter == 1) $assessor_email_body = $email_content;
                $email_counter++;
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }


        }
        $sql_password->close();
    }
	
	if (isset($_POST['assessor_email']) && !empty($_POST['assessor_email'])) {

        $sql_email->bind_param('i', $_POST['assessor_email']);
        $sql_email->execute();
        $sql_email->store_result();
        $sql_email->bind_result($contact_name, $email);
        $sql_email->fetch();
        $sql_email->free_result();
		try {
            if (!isset($assessor_email_body) || empty($assessor_email_body)) $assessor_email_body = str_replace('<!-- contact_name -->', '', $email_template);
			$mail->ClearAllRecipients();
			$mail->ClearAttachments();
			$mail->AddAddress($email, $contact_name);
			$mail->SetFrom('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
			$mail->AddReplyTo('marilynn@aatravel.co.za', 'Marilynn Cavendish Davies');
			$mail->Subject = "COPY :: AA Quality Assured Status for $establishment_name";
			$mail->MsgHTML($assessor_email_body);
			$mail->AddAttachment($certificate_file, 'certificate.pdf');
			$mail->AddAttachment($report_file, 'report.pdf');
			$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
			$mail->Send();
		} catch (phpmailerException $e) {
			echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
		}
	}
	
	update_website($establishment_code, $assessment_id);
	
	echo "<script>alert('Welcome pack resent');document.location='/modules/assessment_s/assessor_s.php?f=list_assessments';</script>";
			
}


function welcome() {
	//Vars
	$count = 0;
	
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/assessment_s/html/welcome.html');
	
	//Get assesment count
	$statement = "SELECT count(*) FROM nse_establishment_assessment WHERE assessment_status='waiting'";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->execute();
	$sql_count->store_result();
	$sql_count->bind_result($count);
	$sql_count->fetch();
	$sql_count->free_result();
	$sql_count->close();
	
	//Replace Tags
	$template = str_replace('<!-- waiting_counter -->', $count, $template);
	
	echo $template;
}


function list_establishments() {
	//Vars
	$booking_id = 0;
	$booking_date = '';
	$establishment_code = '';
	$establishment_count = 0;
	$establishment_name = '';
	$town_id = '';
	$old_next_review = '';
	$province_id = '';
	$town_name = '';
	$province_name = '';
	$nLine = '';
	$assessor_id = $_SESSION['dbweb_user_id'];
	$establishment_count = 0;
	$list_start = isset($_GET['st'])?$_GET['st']:0;
	$ob = 'establishment';
	$assessment_status = '';
	$assessment_date = '';
	$year_entered = '';
	$estab_list = array();
	$sort_order = isset($_GET['order']) && $_GET['order']=='desc'?'DESC':'ASC';
	$order_by = isset($_GET['ob'])?$_GET['ob']:'';

	//Get template & prepare
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/html/establishment_list.html');
	$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
	preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $template, $matches);
	$lTemplate = $matches[0][0];
	
	//Prepare Statements - Province Name
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=? LIMIT 1";
	$sql_province = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement - Town Name
	$statement = "SELECT town_name FROM nse_location_town WHERE town_id=? LIMIT 1";
	$sql_town = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare statement - Booking
	$statement = "SELECT booking_date, booking_id FROM nse_establishment_bookings WHERE establishment_code=? && booking_reason='assessment' ORDER BY booking_date LIMIT 1";
	$sql_book = $GLOBALS['dbCon']->prepare($statement);
		
	
	//Get list of establishments
	$statement = "SELECT DISTINCT a.establishment_code, a.establishment_name, b.town_id, b.province_id,
					(SELECT renewal_date FROM nse_establishment_assessment WHERE establishment_code=a.establishment_code ORDER BY assessment_id DESC LIMIT 1) AS assessment_date,
					(SELECT assessment_status FROM nse_establishment_assessment WHERE establishment_code=a.establishment_code ORDER BY assessment_id DESC LIMIT 1) AS assessment_status,
					DATE_FORMAT(a.enter_date,'%Y'), a.review_date_depricated
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code";
	if (isset($_COOKIE['uEstabFilter'])) {
		$statement .= " LEFT JOIN nse_establishment_assessment AS c ON a.establishment_code=c.establishment_code";
	}
	$statement .= "	WHERE aa_estab='Y'";
	if (isset($_POST['search_string'])) {
		$search_string = "%{$_POST['search_string']}%";
		if ($_POST['search_filter'] == 'establishment_code') $statement .= " && a.establishment_code LIKE ?";
		if ($_POST['search_filter'] == 'establishment_name') $statement .= " && a.establishment_name LIKE ?";
	}
	
	$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
	if (isset($_POST['search_string'])) {
		$sql_establishments->bind_param('s', $search_string);
	}
	$sql_establishments->execute();
	$sql_establishments->store_result();
	$row_count = $sql_establishments->num_rows;
	$sql_establishments->bind_result($establishment_code, $establishment_name, $town_id, $province_id, $assessment_date, $assessment_status, $year_entered, $old_next_review);
	while ($sql_establishments->fetch()) {
		//Reset Vars
		$booking_id = '';
		$booking_date = '';
		$province_name = '';
		$town_name = '';
		$country_name = '';
		$buttons = '';
		$wait_counter = 0;
		if (empty($assessment_date)) $assessment_date = $old_next_review;
		
		//Get province Name
		$sql_province->bind_param('s', $province_id);
		$sql_province->execute();
		$sql_province->bind_result($province_name);
		$sql_province->store_result();
		$sql_province->fetch();
		$sql_province->free_result();
		
		//Get Town Name
		$sql_town->bind_param('s', $town_id);
		$sql_town->execute();
		$sql_town->bind_result($town_name);
		$sql_town->store_result();
		$sql_town->fetch();
		$sql_town->free_result();
		
		//Check for current booking
		$sql_book->bind_param('s', $establishment_code);
		$sql_book->execute();
		$sql_book->store_result();
		$sql_book->bind_result($booking_date, $booking_id);
		$sql_book->fetch();
		$sql_book->free_result();
		
		$status = '';
		//$button_booked = "<input type=button value=Book class=book_button onClick='document.location=\"/modules/assessment_s/assessor.php?f=book_assessment&code=!establishment_code!\"' />";
		$buttons = "<input type=button value=select class=edit_button onClick=assess_redirect('$establishment_code') />";
		if ($assessment_status == 'waiting') {
			$status = 'Submitted';
			//$buttons = "<input type=button value=select class=edit_button disabled title='Assessment waiting for approval' />";
			//$button_booked = "<input type=button value=Book class=book_button disabled />";
		} else if ($assessment_status == 'draft') {
			$status = 'Draft';
		} else if ($assessment_status == 'booked') {
			$status = 'Booked';
		} else if ($assessment_status == 'returned') {
			$status = 'Returned';
		} else {
/*	//Put back for PHP version 5.3
  			$date1 = new DateTime($assessment_date);
			$date2 = new DateTime('now');
			$time_diff = $date1->diff($date2, true);
			$time_diff_months = $time_diff->format('%m');
			$time_diff_years = $time_diff->format('%y');
			echo "Months: $time_diff_months, Years: $time_diff_years<br>";
*/
			$date1 = strtotime($assessment_date);
			$date2 = strtotime('now');
			$time_diff = date_diff_for_v52($date1, $date2);
			$time_diff_years = $time_diff['years'];
			$time_diff_months = $time_diff['months'];
			//echo "Months: $time_diff_months, Years: $time_diff_years<p>";
			
			$time_diff = ($time_diff_years * 12) + $time_diff_months;
			$time_diff = $time_diff * (-1);
			if ($time_diff < 0) $status = 'overdue';
			if ($time_diff == 0) $status = 'Due in 1 Month';
			if ($time_diff > 0 && $time_diff <= 2) $status = 'Due in 3 Months';
			if ($time_diff > 2) $status = 'Complete';
			if (empty($assessment_date)) $status = '-';
		}
		
		//$buttons = $button_booked . $buttons;
		if ($booking_id != 0 && $assessment_status != 'draft' && $assessment_status != 'waiting' && $assessment_status != 'returned') {
			$status = 'Booked';
		}
		
		if ($status != 'Booked') $booking_date = '';
		
		if (isset($_COOKIE['uEstabFilter'])) {
			if ($_COOKIE['uEstabFilter'] == 'overdue') {
				if ($status != 'overdue') continue;
			}
			if ($_COOKIE['uEstabFilter'] == 'booked') {
				if ($status != 'Booked') continue;
			}
			if ($_COOKIE['uEstabFilter'] == 'draft') {
				if ($status != 'Draft' && $assessment_status != 'returned') continue;
			}
			if ($_COOKIE['uEstabFilter'] == '1month') {
				if ($status != 'Due in 1 Month') continue;
			}
			if ($_COOKIE['uEstabFilter'] == '3month') {
				if ($status != 'Due in 3 Months') continue;
			}
		}
		
		
		$estab_list[$establishment_count]['establishment_code'] = $establishment_code;
		$estab_list[$establishment_count]['establishment_name'] = $establishment_name;
		$estab_list[$establishment_count]['town_name'] = $town_name;
		$estab_list[$establishment_count]['province_name'] = $province_name;
		$estab_list[$establishment_count]['year_entered'] = $year_entered;
		$estab_list[$establishment_count]['assessment_date'] = $assessment_date;
		$estab_list[$establishment_count]['status'] = $status;
		$estab_list[$establishment_count]['buttons'] = $buttons;
		$estab_list[$establishment_count]['booking_date'] = $booking_date;
		$establishment_count++;
	}
	$sql_establishments->free_result();
	$sql_establishments->close();
	if ($row_count == 0) {
		$nLine = "<tr><td colspan=10 align=center>-- No Results --</td></tr>";
		$navigation = '<tr><th colspan=10 >&nbsp;</th></tr>';
	} else {
		//Sorting
		foreach($estab_list as $key=>$data) {
			switch ($order_by) {
				case 'code':
					$sort_array[$key] = $data['establishment_code'];
					break;
				case 'entered':
					$sort_array[$key] = $data['year_entered'];
					break;
				case 'booking_date':
					$sort_array[$key] = $data['booking_date'];
					break;
				case 'status':
					$sort_array[$key] = $data['status'];
					break;
				case 'review':
					$sort_array[$key] = $data['assessment_date'];
					break;
				case 'location':
					$sort_array[$key] = $data['town_name'];
					break;
				default:
					$sort_array[$key] = $data['establishment_name'];
			}
			
		}
		if (count($estab_list) == 0) {
			$nLine = "<tr><td colspan=10 align=center>-- No Results --</td></tr>";
			$navigation = '<tr><th colspan=10 >&nbsp;</th></tr>';
		} else {
			if ($sort_order == 'DESC') {
				asort($sort_array);
			} else {
				arsort($sort_array);
			}
			foreach ($sort_array as $key=>$value) {
				$new_list[] = $estab_list[$key];
			}
			$estab_list = $new_list;
			
			//Create Line
			$counter = -1;
			foreach($estab_list as $data) {
				$counter++;
				if ($counter < $list_start) continue;
				if ($counter > $list_start+$GLOBALS['list_length']-1) break;
				$line = str_replace('<!-- establishment_name -->', $data['establishment_name'], $lTemplate);
				$line = str_replace('<!-- establishment_code -->', $data['establishment_code'], $line);
				$line = str_replace('<!-- location -->', "{$data['town_name']}, {$data['province_name']}", $line);
				$line = str_replace('<!-- entered -->', $data['year_entered'], $line);
				$line = str_replace('<!-- review -->', $data['assessment_date'], $line);
				$line = str_replace('<!-- status -->', $data['status'], $line);
				$line = str_replace('<!-- buttons -->', $data['buttons'], $line);
				$line = str_replace('!establishment_code!', $data['establishment_code'], $line);
				$line = str_replace('<!-- booking_date -->', $data['booking_date'], $line);
				
				$nLine .= $line;
			}
			
			//Navigation
			$establishment_count = count($estab_list);
			$total_pages = ceil($establishment_count/$GLOBALS['list_length']);
			$current_page = ceil($list_start/$GLOBALS['list_length']) + 1;
			$navigation = "<tr><th colspan=10><table width=100%><tr>";
			if ($GLOBALS['list_length'] > $establishment_count || $current_page == 1) {
				$navigation .= "<th >&nbsp;</th>";
			} else {
				if (isset($_GET['st'])) {
					$navigation .= "<th align=left width=100px >&nbsp;<a href='/modules/assessment_s/assessor_s.php?f=new_assessment&st=";
					$navigation .= $list_start - $GLOBALS['list_length'];
					$navigation .= "' >&#8249; Previous</a> </th>";
				} else {
					$navigation .= "<th>&nbsp;</th>";
				}
			}
			$navigation .= "<th colspan=5 align=center >Page: ";
			for ($i=1; $i<=$total_pages; $i++) {
				$navigation .= "<a href='/modules/assessment_s/assessor_s.php?f=new_assessment&st=";
				$navigation .= ($i-1) * $GLOBALS['list_length'];
				if (isset($_GET['ob'])) $navigation .= "&ob=" . $_GET['ob'];
				if (isset($_GET['order'])) $navigation .= "&order=" . $_GET['order'];
				$navigation .= "' >$i</a> | ";
			}
			$navigation = substr($navigation, 0, -2);
			$navigation .= "</th>";
			
			if ($current_page < $total_pages) {
				$navigation .= "<th align=right width=100px><a href='/modules/assessment_s/assessor_s.php?f=new_assessment&st=";
				$navigation .= $list_start + $GLOBALS['list_length'];
				$navigation .= "'>Next &#8250;</a>&nbsp;</th>";
			} else {
				$navigation .= "<th>&nbsp;</th>";
			}
			$navigation .= "</tr></table></th></tr>";
		}
	}
	//Heading
	$heading_titles = array('empty'=>'&nbsp;',
							'code'=>'Code',
							'establishment'=>'Establishment',
							'location'=>'Location',
							'entered' => 'Year Entered',
							'review' => 'Next Review',
							'status'=>'Status',
							'booking_date' => 'Booking Date',
							'nb'=>'&nbsp;');
	$heading_widths = array('empty'=>10,
							'code'=>50,
							'establishment'=>300,
							'location'=>200,
							'entered' => 50,
							'review' => 60,
							'status'=>80,
							'booking_date' => 50,
							'nb'=>145);
	$line_header = "<tr style='background-color: grey'>";
	foreach ($heading_titles as $col_id=>$col_name) {
		$line_header .= "<TH width={$heading_widths[$col_id]}>";
		if (!isset($_POST['search_string']) && ($col_id != 'empty' && $col_id != 'nb')) {
			$line_header .= "<a href='/modules/assessment_s/assessor_s.php?f=new_assessment&ob=$col_id";
			if ($col_id == $order_by) {
				if (!isset($_GET['order'])) $line_header .= "&order=desc";
			}
			$line_header .= "' >";
		}
		$line_header .= "$col_name";
		if (!isset($_POST['search_string'])) {
			if ($col_id == $order_by) {
				if ($sort_order == 'DESC') {
					$line_header .= "<img src='/i/arrow_up.png' border=0 title='Ascending Order (A-Z)' />";
				} else {
					$line_header .= "<img src='/i/arrow_down.png' border=0 title='Descending Order (Z-A)' />";
				}
			}
		}
		if (!isset($_POST['search_string'])) $line_header .= "</a>";
		$line_header .= "</TH>";
	}
    $line_header .= "</tr>";
	
	//Replace Tags
	$template = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $template);
	$template = str_replace('<!-- navigation -->', $navigation, $template);
	$template = str_replace('<!-- line_header -->', $line_header, $template);
	$template = str_replace('#$%', "\n", $template);
	
	echo $template;
}


function return_assessment() {
	echo "RETURN!";
}


function new_assessment_form() {
	//Vars
	$establishment_code = $_GET['code'];
	$establishment_name = '';
	$town = '';
	$province = '';
	$country = '';
	$category_id = '';
	$category_name = '';
	$location = '';
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/html/assessment_form.html');
	$areas = '';
	$scripts = '';
	$assessment_date = '';
	$assessment_type = '';
	$assessment_id = '';
	$qa_status = '';
	$qa_code = '';
	$qa_name = '';
	$ratings = array(1=>'Excellent','Very Good','Good','Fair','Poor');
	$types = array('new establishment'=>'New Establishment', 'award visit'=>'Awards Visit', 'renewal'=>'Renewal');
	$type_list = '';
	$qa_list = '';
	$hide = '';
	$renewal_date = '';
	$category_select = '';
	$assessment_id = '';
	$area_id = '';
	$area_name = '';
	$hide = '';
	$required = '';
	$assessment_notes = '';
	$staff_notes = '';
	$proposed_category = '';
	$hide_default = '';
	$buttons = "";
	$warning = '';
	
	
	//Get establishment_name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();

	//Get Assessor Name
	$statement = "SELECT CONCAT(b.firstname, ' ' , b.lastname)
			FROM nse_establishment AS a
			JOIN nse_user AS b
			WHERE a.establishment_code=?";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assesor->execute();
	$sql_assessor->store_result();
	$sql_assessor->bind_result($assessor_name);
	$sql_assessor->fetch();
	$sql_assessor->free_result();
	$sql_assessor->close();


	//Get Location
	$statement = "SELECT b.town_name, c.province_name, d.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
					LEFT JOIN nse_location_country_lang AS d ON a.country_id=d.country_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->store_result();
	$sql_location->bind_result($town, $province, $country);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	
	if (!empty($town)) $location .= $town;
	if (!empty($province)) {
		$location .= ", $province";
	} else {
		if (!empty($country)) $location .= ", $country";
	}
	
	//Prepare Statement - Get assessment details
	$statement = "";
	$sql_area_data = $GLOBALS['dbCon']->prepare($statement);
	
	//Check if draft exists
	$statement = "SELECT assessment_id, DATE_FORMAT(assessment_date,'%Y-%m-%d'), assessment_type, qa_status, DATE_FORMAT(renewal_date, '%M %Y'), proposed_category, assessment_notes, staff_notes FROM nse_establishment_assessment WHERE (assessment_status='draft' || assessment_status='returned') && establishment_code=? ORDER BY assessment_id LIMIT 1";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('s', $establishment_code);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($assessment_id, $assessment_date, $assessment_type, $qa_status, $renewal_date, $proposed_category, $assessment_notes, $staff_notes);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	//Prepare Statement - Get area data
	$statement = "SELECT assessment_content, assessment_rating, not_applicable, hide_from_client FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	
	//Get and calculate renewal date
	if (empty($renewal_date)) {
		$statement = "SELECT DATE_FORMAT(renewal_date, '%m') FROM nse_establishment_assessment WHERE assessment_status = 'complete' && establishment_code=? ORDER BY assessment_id DESC LIMIT 1";
		$sql_renewal = $GLOBALS['dbCon']->prepare($statement);
		$sql_renewal->bind_param('s', $establishment_code);
		$sql_renewal->execute();
		$sql_renewal->store_result();
		$sql_renewal->bind_result($renewal_month);
		$sql_renewal->fetch();
		$sql_renewal->free_result();
		$sql_renewal->close();
		if (empty($renewal_month)) {
			$statement = "SELECT DATE_FORMAT(enter_date, '%m') FROM nse_establishment WHERE establishment_code=?";
			$sql_renewal = $GLOBALS['dbCon']->prepare($statement);
			$sql_renewal->bind_param('s', $establishment_code);
			$sql_renewal->execute();
			$sql_renewal->store_result();
			$sql_renewal->bind_result($renewal_month);
			$sql_renewal->fetch();
			$sql_renewal->free_result();
			$sql_renewal->close();
		}
		
		if (empty($renewal_month)) {
			$renewal_date = '';
			$warning = "WARNING! Renewal date could not be calculated";
		} else {
			$renewal_year = date('Y') + 1;
			$renewal_month = (int) $renewal_month;
			$renewal_date = $GLOBALS['months'][$renewal_month] . " $renewal_year";
		}
	}
	
	//Get assessment areas
	$statement = "SELECT assessment_area_id, assessment_area_name, hide_client, required FROM nse_assessment_areas WHERE active='Y' ORDER BY display_order";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->execute();
	$sql_areas->store_result();
	$sql_areas->bind_result($area_id, $area_name, $hide_default, $required);
	while ($sql_areas->fetch()) {
		$hide = 'a';
		$assessment_content = '';
		$assessment_rating = '';
		$not_applicable = '';
		
		if (!empty($assessment_id)) {
			$sql_data->bind_param('ss', $assessment_id, $area_id);
			$sql_data->execute();
			$sql_data->store_result();
			$sql_data->bind_result($assessment_content, $assessment_rating, $not_applicable, $hide);
			$sql_data->fetch();
			$sql_data->free_result();
		} else {
			$assessment_content = '';
			$assessment_rating = '';
			$not_applicable = '';
		}
		
		if ($hide == 'a' && $hide_default == '1') $hide = 'Y';
		if (!empty($assessment_rating)) $scripts .= "<script>set_change('$area_id')</script>";
		
		$areas .= "<div class=block_header onClick=show_block('$area_id') id=header$area_id onMouseOver=tCM('$area_id',1) onMouseOut=tCM('$area_id',0) >
						<table width=100% cellpadding=0 cellspacing=0>
							<tr>
								<td class=hRow >$area_name</td>
								<td><span id=click_$area_id class=cMes ></span></td>
								<td align=right><img src='/i/silk/group_delete.png' width=16 height=16 class=hide_indicator id=hide_indicator_$area_id title='NOT visible to clients' />&nbsp;<img src='/i/blank.png' width=16 height=16 class=edit_indicator id=indicator_$area_id /></td>
							</tr>
						</table>
					</div>
					<div class=block_content id=content$area_id>
					<textarea name=area_$area_id id=area_$area_id  onChange=\"set_change('$area_id')\" >$assessment_content</textarea>
					<table width=100% >
						<tr>
							<td>
								<table width=480px id=ratings_$area_id >
									<tr>";
		foreach ($ratings as $rate_id=>$rate_name) {
			$areas .= "<td class=field_label><input type=radio name=rate_$area_id value=$rate_id id=rate_{$rate_id}_$area_id";
			if ($rate_id == $assessment_rating) $areas .= " checked ";
			$areas .= "/><label for='rate_{$rate_id}_$area_id' >$rate_name</label></td>";
		}
										
		$areas .="					</tr>
								</table>
							</td>
							<td class=field_label align=right width=500><input type=checkbox name=hide_$area_id value='Y' id=hide_$area_id onCLick=hide('$area_id')";
		if ($hide == 'Y') {
			$areas .= " checked ";
		} else {
			$scripts .= "<script>hide('$area_id')</script>";
		}
		$areas .= " /><label for='hide_$area_id' style='padding-right: 20px'>Hide From Client</label>";
		if ($required == 1) {
			$areas .= "<input type=checkbox name=na_$area_id value='na' id=na_$area_id onCLick=na('$area_id') disabled /><span style='color: grey' >Not Applicable</span></td>
							</tr>
						</table>
						</div>";
		} else {
			$areas .= "<input type=checkbox name=na_$area_id value='na' id=na_$area_id onCLick=na('$area_id') ";
			if ($not_applicable == 'Y') {
				$areas .= " checked ";
				$scripts .= "<script>na('$area_id')</script>";
			}
			$areas .= " /><label for='na_$area_id' >Not Applicable</label></td>
							</tr>
						</table>
						</div>";
		}
	}
	$sql_areas->free_result();
	$sql_areas->close();
	
	//Assessment types
	foreach ($types as $type_id=>$type_name) {
		if ($assessment_type == $type_id) {
			$type_list .= "<option value='$type_id' selected>$type_name</option>";
		} else {
			$type_list .= "<option value='$type_id' >$type_name</option>";
		}
	}
	
	//Proposed Status
	$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category WHERE active='Y' ORDER BY display_order";
	$sql_qa = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa->execute();
	$sql_qa->store_result();
	$sql_qa->bind_result($qa_code, $qa_name);
	while ($sql_qa->fetch()) {
		$qa_list .= "<option value='$qa_code' ";
		$qa_list .= $qa_code==$qa_status?' selected':'';
		$qa_list .= " >$qa_name</option>";
	}
	$sql_qa->free_result();
	$sql_qa->close();
	
	//Get default category
	if (empty($proposed_category)) {
		$statement = "SELECT subcategory_id FROM nse_establishment_restype WHERE establishment_code=? LIMIT 1";
		$sql_category = $GLOBALS['dbCon']->prepare($statement);
		$sql_category->bind_param('s', $establishment_code);
		$sql_category->execute();
		$sql_category->store_result();
		$sql_category->bind_result($proposed_category);
		$sql_category->fetch();
		$sql_category->free_result();
		$sql_category->close();
	}
	
	//Get category list
	$statement = "SELECT subcategory_id, subcategory_name FROM nse_restype_subcategory_lang WHERE language_code='EN' ORDER BY subcategory_name";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);
	$sql_category->execute();
	$sql_category->store_result();
	$sql_category->bind_result($category_id, $category_name);
	while ($sql_category->fetch()) {
		$category_select .= "<option value='$category_id' ";
		if ($category_id == $proposed_category) $category_select .= "selected ";
		$category_select .= "> $category_name </option>";
	}
	$sql_category->free_result();
	$sql_category->close();
	
	if (isset($warning) && !empty($warning)) {
		$warning = "<div class=warning>$warning</div>";
		
	}
	
	//Buttons
	$buttons = "<input type=submit name='disapprove_assessment' class=cross_button value='Below Standard' />
					<input type=submit name='approve_assessment' class=ok_button value='Approve Assessment' />
					<input type=submit name='save_draft' class=save_button value='Save As Draft' />";

	//Get notes
	$notes = "<table id=notes_list >";
	$statement = "SELECT user_name, note_timestamp, note_content FROM nse_establishment_notes WHERE establishment_code=? ORDER BY note_timestamp DESC";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);
	$sql_notes->bind_param('s', $establishment_code);
	$sql_notes->execute();
	$sql_notes->store_result();
	$sql_notes->bind_result($user_name, $note_timestamp, $note_content);
	while ($sql_notes->fetch()) {
		$date = date('d-m-Y h:i',$note_timestamp);
		$notes .= "<tr>";
		$notes .= "<td class=note_name>$user_name</td>";
		$notes .= "<td class=note_date>$date</td>";
		$notes .= "<td class=note_content colspan=2>$note_content</td>";
		$notes .= "</tr>";
	}
	$sql_notes->free_result();
	$sql_notes->close();
	$notes .= "</table>";

	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	$template = str_replace('<!-- location -->', $location, $template);
	$template = str_replace('<!-- areas -->', $areas, $template);
	$template = str_replace('<!-- scripts -->', $scripts, $template);
	$template = str_replace('<!-- assessment_date -->', $assessment_date, $template);
	$template = str_replace('<!-- renewal_date -->', $renewal_date, $template);
	$template = str_replace('<!-- assessment_type -->', $type_list, $template);
	$template = str_replace('<!-- category_select -->', $category_select, $template);
	$template = str_replace('<!-- qa_list -->', $qa_list, $template);
	$template = str_replace('<!-- buttons -->', $buttons, $template);
	$template = str_replace('<!-- assessor_name -->', 'eueueu', $template);
	$template = str_replace('!assessment_id!', $assessment_id, $template);
	$template = str_replace('!assessment_notes!', $assessment_notes, $template);
	$template = str_replace('!staff_notes!', $staff_notes, $template);
	$template = str_replace('<!-- warning -->', $warning, $template);
	$template = str_replace('<!-- notes -->', $notes, $template);
	
	echo $template;
}


function new_assessment_save($assessment_status) {
	//Vars
	$assessment_id = '';
	$establishment_code = $_GET['code'];
	$assessment_date = $_POST['assessment_date'];
	$assessment_type = $_POST['assessment_type'];
	$qa_status = $_POST['qa_status'];
	$proposed_category = $_POST['proposed_category'];
	$renewal_date = $_POST['renewal_date'];
	$count = 0;
	$area_id = '';
	$notes = $_POST['notes'];

	//Delete current Contacts
	$statement = "DELETE FROM nse_establishment_contacts WHERE establishment_code=?";
	$sql_contacts_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_contacts_delete->bind_param('s', $establishment_code);
	$sql_contacts_delete->execute();
	$sql_contacts_delete->close();

	//Prepare statement - check if user has admin access
	$statement = "SELECT COUNT(*) FROM nse_user WHERE email=?";
	$sql_admin_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare Statement - Add new user
	$statement = "INSERT INTO nse_user (firstname, lastname, email, phone, cell, new_password, top_level_type, user_type_id) VALUES (?,?,?,?,?,?,'c','2')";
	$sql_admin_insert = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Update user
	$statement = "UPDATE nse_user SET firstname=?, lastname=?, email=?, phone=?, cell=? WHERE user_id=?";
	$sql_admin_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Check establishment access
	$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=?";
	$sql_user_access = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Get user id
	$statement = "SELECT user_id FROM nse_user WHERE email=?";
	$sql_admin_id = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Delete admin user
	$statement = "DELETE FROM nse_user WHERE user_id=?";
	$sql_admin_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Delete user/establishment link
	$statement = "DELETE FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
	$sql_estab_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Add user/establishment link
	$statement = "INSERT INTO nse_user_establishments (user_id, establishment_code) VALUES (?,?)";
	$sql_estab_add = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - check user/establishment link
	$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
	$sql_estab_check = $GLOBALS['dbCon']->prepare($statement);

	//Update Contacts
	$statement = "INSERT INTO nse_establishment_contacts (establishment_code, contact_name, contact_designation, contact_tel, contact_cell, contact_email) VALUES (?,?,?,?,?,?)";
	$sql_contacts_insert = $GLOBALS['dbCon']->prepare($statement);
	$contact_line = explode("!|!", $contacts_list);

	foreach ($contact_line as $line) {
		if (empty($line)) continue;
		list($line_id, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email, $admin_user, $send_password, $old_email, $delete_tag) = explode('|', $line);
		if ($delete_tag == 0) {
			$sql_contacts_insert->bind_param('ssssss', $establishment_code, $contact_name, $contact_designation, $contact_tel, $contact_cell, $contact_email);
			$sql_contacts_insert->execute();
		}

		//Extract firstname & lastname
		$names = explode(' ', $contact_name);
		if (count($names) == 2) {
			$firstname = $names[0];
			$lastname = $names[1];
		} else {
			$firstname = $contact_name;
			$lastname = '';
		}
		echo "<p style='color: navy'>$line<br>Email: $old_email<br>Admin User: $admin_user</p>";
		if ($delete_tag == 1 || $admin_user == 0) {
			//echo "DELETE<br>=======<br />Email: $old_email<br>";
			$admin_user_id = 0;
			$sql_admin_id->bind_param('s', $old_email);
			$sql_admin_id->execute();
			$sql_admin_id->store_result();
			$sql_admin_id->bind_result($admin_user_id);
			$sql_admin_id->fetch();
			$sql_admin_id->free_result();

			if ($admin_user_id != 0) {
				$sql_user_access->bind_param('i', $admin_user_id);
				$sql_user_access->execute();
				$sql_user_access->store_result();
				$sql_user_access->bind_result($estab_count);
				$sql_user_access->fetch();
				$sql_user_access->free_result();

				if ($estab_count < 2) {
					//echo "Deleting Admin User<br>";
					$sql_admin_delete->bind_param('i', $admin_user_id);
					$sql_admin_delete->execute();
				}

				//echo "Deleting estab link<p />";
				$sql_estab_delete->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_delete->execute();
			}
		} else if ($admin_user == 1) {
			if ($old_email == 'new') {
				//echo "ADDING USER<br>=======<br />Email: $old_email<br />";
				//Check if user exists
				$admin_user_id = 0;
				$sql_admin_id->bind_param('s', $contact_email);
				$sql_admin_id->execute();
				$sql_admin_id->store_result();
				$sql_admin_id->bind_result($admin_user_id);
				$sql_admin_id->fetch();
				$sql_admin_id->free_result();
				//echo "User ID: $admin_user_id<br>";

				if ($admin_user_id == 0) {
					//Get Password
					$password = $GLOBALS['security']->generate_password(6);
					$password = $tc->encrypt($password);

					//Save user
					//echo "Inserting user<br>";
					$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
					$sql_admin_insert->execute();
					$admin_user_id = $sql_admin_insert->insert_id;
				}

				//Check if user/establishment link exists
				$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_check->execute();
				$sql_estab_check->store_result();
				$sql_estab_check->bind_result($estab_count);
				$sql_estab_check->fetch();
				$sql_estab_check->free_result();

				//Attach establishment
				if ($estab_count == 0) {
					//echo "Inserting estab link<p />";
					$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
					$sql_estab_add->execute();
				}


			} else {
				//echo "UPDATING USER<br>=======<br />Email: $old_email<br />";
				$admin_user_id = 0;
				$sql_admin_id->bind_param('s', $old_email);
				$sql_admin_id->execute();
				$sql_admin_id->store_result();
				$sql_admin_id->bind_result($admin_user_id);
				$sql_admin_id->fetch();
				$sql_admin_id->free_result();
				//echo "User ID: $admin_user_id<br>";

				if ($admin_user_id == 0) {
					//echo "Adding user<br>";
					$sql_admin_insert->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $password);
					$sql_admin_insert->execute();
					$admin_user_id = $sql_admin_insert->insert_id;
				} else {
					//echo "Updating user <br>";
					$sql_admin_update->bind_param('ssssss', $firstname, $lastname, $contact_email, $contact_tel, $contact_cell, $admin_user_id);
					$sql_admin_update->execute();
				}

				//Check if estab exists
				$sql_estab_check->bind_param('is', $admin_user_id, $establishment_code);
				$sql_estab_check->execute();
				$sql_estab_check->store_result();
				$sql_estab_check->bind_result($estab_count);
				$sql_estab_check->fetch();
				$sql_estab_check->free_result();

				//Attach establishment
				if ($estab_count == 0) {
					//echo "Adding estab link<p />";
					$sql_estab_add->bind_param('is', $admin_user_id, $establishment_code);
					$sql_estab_add->execute();
				}
			}
		}
	}
	$sql_contacts_insert->close();
	$sql_admin_check->close();
	$sql_admin_update->close();
	$sql_admin_insert->close();

	//Check if draft exists
	$statement = "SELECT assessment_id FROM nse_establishment_assessment WHERE assessment_status='draft' && establishment_code=? ORDER BY assessment_id LIMIT 1";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('s', $establishment_code);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($assessment_id);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	//Get list of areas
	$statement = "SELECT assessment_area_id FROM nse_assessment_areas WHERE active='Y'";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->execute();
	$sql_areas->store_result();
	$sql_areas->bind_result($area_id);
	while ($sql_areas->fetch()) {
		$areas[] = $area_id;
	}
	$sql_areas->free_result();
	$sql_areas->close();
	
	//Recalculate renewal date to first day of month
	if (!empty($renewal_date)) {
		$renewal_date = date('Y-m-01', strtotime($renewal_date));
	}
	
	if (empty($assessment_id)) {
		$statement = "INSERT INTO nse_establishment_assessment (establishment_code, assessment_date, assessment_status, assessment_type, qa_status, proposed_category, renewal_date ) VALUES (?,?,?,?,?,?,?)";
		$sql_assessment_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessment_insert->bind_param('sssssss', $establishment_code, $assessment_date, $assessment_status, $assessment_type, $qa_status, $proposed_category, $renewal_date);
		$sql_assessment_insert->execute();
		$assessment_id = $sql_assessment_insert->insert_id;
		$sql_assessment_insert->close();
	} else {
		$statement = "UPDATE nse_establishment_assessment SET assessment_date=?, assessment_status=?, assessment_type=?, qa_status=?, proposed_category=?, renewal_date=? WHERE assessment_id=?";
		$sql_assessment_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessment_update->bind_param('sssssss', $assessment_date, $assessment_status, $assessment_type, $qa_status, $proposed_category, $renewal_date, $assessment_id);
		$sql_assessment_update->execute();
		$sql_assessment_update->close();
	}
	
	//Prepare statement - check area exists
	$statement = "SELECT count(*) FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
	$sql_area_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - update area
	$statement = "UPDATE nse_establishment_assessment_data SET assessment_content=?, assessment_rating=?, not_applicable=?, hide_from_client=? WHERE assessment_id=? && assessment_area_id=?";
	$sql_area_update = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare statement - insert area
	$statement = "INSERT INTO nse_establishment_assessment_data (assessment_content, assessment_rating, not_applicable, assessment_id, assessment_area_id, hide_from_client) VALUES (?,?,?,?,?,?)";
	$sql_area_insert = $GLOBALS['dbCon']->prepare($statement);
	
	foreach ($areas as $area_id) {
		$count = 0;
		$sql_area_check->bind_param('ss', $assessment_id, $area_id);
		$sql_area_check->execute();
		$sql_area_check->store_result();
		$sql_area_check->bind_result($count);
		$sql_area_check->fetch();
		$sql_area_check->free_result();
		
		if (isset($_POST['na_' . $area_id])) {
			$not_applicable = 'Y';
			$assessment_content = '';
			$assessment_rating = '';
		} else {
			$not_applicable = '';
			$assessment_content = $_POST['area_' . $area_id];
			$assessment_rating = isset($_POST['rate_' . $area_id])?$_POST['rate_' . $area_id]:'';
		}
		$hide = isset($_POST['hide_' . $area_id])?'Y':'';
		
		if ($count == 0) {
			$sql_area_insert->bind_param('ssssss', $assessment_content, $assessment_rating, $not_applicable, $assessment_id, $area_id, $hide);
			$sql_area_insert->execute();
		} else {
			$sql_area_update->bind_param('ssssss', $assessment_content, $assessment_rating, $not_applicable, $hide, $assessment_id, $area_id);
			$sql_area_update->execute();
		}
	}

	//Update Notes
	//Prepare statement - add note
	$statement = "INSERT INTO nse_establishment_notes (establishment_code, user_name, note_timestamp, note_content, module_name ) VALUES (?,?,?,?,'assessments')";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);

	//Get user name
	$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bind_param('i', $_SESSION['dbweb_user_id']);
	$sql_user->execute();
	$sql_user->store_result();
	$sql_user->bind_result($user_name);
	$sql_user->fetch();
	$sql_user->free_result();
	$sql_user->close();

	$line = explode('##',$notes);
	foreach ($line as $note_line) {
		if (empty($note_line)) continue;
		$note_items = explode('|', $note_line);
		//Calculate timestamp
		list($date, $time) = explode(' ', $note_items[1]);
		list($day, $month, $year) = explode('-', $date);
		list($hours, $minutes) = explode(':',$time);
		$timestamp = mktime($hours, $minutes, 0, $month, $day, $year);

		$sql_notes->bind_param('ssss', $establishment_code, $user_name, $timestamp, $note_items[2]);
		$sql_notes->execute();
	}
	$sql_notes->close();

	if ($assessment_status == 'draft') {
		echo "<script>alert('Saved as Draft'); document.location='/modules/assessment_s/assessor_s.php?f=new_assessment'</script>";
	} else {
		echo "<script>alert('Assessment Sent for Approval'); document.location='/modules/assessment_s/assessor_s.php?f=new_assessment'</script>";
	}
}


function date_diff_for_v52($date1, $date2) {
	//Vars
	$date['years'] = 0;
	$date['months'] = 0;
	if (empty($date1)) return 0;
	$date_diff = $date2 - $date1;
	//$date['years'] = floor($date_diff/31536000);
	//if ($date['years'] < 0) $date['years'] = 0;
	$date['months'] = floor($date_diff/2628000);
	//if ($date['months'] < 0) ($date['months']++)* (-1);
	
	return $date;
}

function update_website($establishment_code, $assessment_id) {
	//Make assessment active
	$statement = "UPDATE nse_establishment_assessment SET active='Y' WHERE assessment_id=?";
	$sql_activate = $GLOBALS['dbCon']->prepare($statement);
	$sql_activate->bind_param('i', $assessment_id);
	$sql_activate->execute();
	$sql_activate->close();
	
	//Get assessment data
	$statement = "SELECT qa_status, proposed_category FROM nse_establishment_assessment WHERE assessment_id=? LIMIT 1";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	$sql_data->bind_param('i', $assessment_id);
	$sql_data->execute();
	$sql_data->store_result();
	$sql_data->bind_result($status, $category);
	$sql_data->fetch();
	$sql_data->free_result();
	$sql_data->close();
	
	//Update Listing - grading
	$statement = "UPDATE nse_establishment SET aa_category_code=?  WHERE establishment_code=? ";
	$sql_grading = $GLOBALS['dbCon']->prepare($statement);
	$sql_grading->bind_param('ss', $status, $establishment_code);
	$sql_grading->execute();
	$sql_grading->close();
	
	//Update Listing - restype
	$statement = "UPDATE nse_establishment_restype SET subcategory_id=? WHERE establishment_code=?";
	$sql_restype = $GLOBALS['dbCon']->prepare($statement);
	$sql_restype->bind_param('ss', $category, $establishment_code);
	$sql_restype->execute();
	$sql_restype->close();

    //Update Listing - aa status
    $aa_status = (get_status($establishment_code))?'Y':'';
    $statement = "UPDATE nse_establishment SET aa_estab=?  WHERE establishment_code=? ";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('ss', $aa_status, $establishment_code);
	$sql_status->execute();
	$sql_status->close();

	//Update establishment cache
	include_once $_SERVER['DOCUMENT_ROOT'] . '/modules/estab_s/establishment.php';
	$estab = new establishment();
	$estab->index_establishment($establishment_code);
	
	//Update establishment page
	//$ch = curl_init("http://aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	$ch = curl_init("http://aatravel.server1/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	//Get location data
	$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
	$sql_locations = $GLOBALS['dbCon']->prepare($statement);
	$sql_locations->bind_param('s', $establishment_code);
	$sql_locations->execute();
	$sql_locations->store_result();
	$sql_locations->bind_result($country_id, $province_id, $town_id, $suburb_id);
	$sql_locations->fetch();
	$sql_locations->free_result();
	$sql_locations->close();
	
	//Set country pages for regeneration
	$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='country' && current_id=?";
	$sql_country_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_country_update->bind_param('s', $country_id);
	$sql_country_update->execute();
	$sql_country_update->close();
	
	//$sql_country_update = $GLOBALS['dbCon_shell']->prepare($statement);
	//$sql_country_update->bind_param('s', $country_id);
	//$sql_country_update->execute();
	//$sql_country_update->close();
	
	
	//Set province pages for regeneration
	if ($province_id != '0') {
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='province' && current_id=?";
		$sql_province_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_province_update->bind_param('s', $province_id);
		$sql_province_update->execute();
		$sql_province_update->close();
		
		//$sql_province_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_province_update->bind_param('s', $province_id);
		//$sql_province_update->execute();
		//$sql_province_update->close();
	}
	
	//Set town pages for regeneration
	if ($town_id != '0') {
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='town' && current_id=?";
		$sql_town_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_town_update->bind_param('s', $town_id);
		$sql_town_update->execute();
		$sql_town_update->close();
		
		//$sql_town_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_town_update->bind_param('s', $town_id);
		//$sql_town_update->execute();
		//$sql_town_update->close();
	}
	
	//Set suburb pages for regeneration
	if ($suburb_id != '0') {
		$statement = "UPDATE nse_export SET complete = NULL, complete4 = NULL WHERE level='suburb' && current_id=?";
		$sql_suburb_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb_update->bind_param('s', $suburb_id);
		$sql_suburb_update->execute();
		$sql_suburb_update->close();
		
		//$sql_suburb_update = $GLOBALS['dbCon_shell']->prepare($statement);
		//$sql_suburb_update->bind_param('s', $suburb_id);
		//$sql_suburb_update->execute();
		//$sql_suburb_update->close();
	}
}
?>
