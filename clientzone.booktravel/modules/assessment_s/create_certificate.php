<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/tcpdf.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/fpdi/fpdi.php';

//error_reporting(E_ALL);

//Vars
$establishment_code = $_GET['code'];
$establishment_name = '';
$assessment_id = $_REQUEST['id'];
$endorsement = '';
$category = "";
$qa_logo = '';
$assessment_date = '';

//Get Data
$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
$sql_data->bind_param('s', $establishment_code);
$sql_data->execute();
$sql_data->store_result();
$sql_data->bind_result($establishment_name);
$sql_data->fetch();
$sql_data->free_result();
$sql_data->close();


//Get Assessment Status
$statement = "SELECT a.aa_category_name, a.pdf_image, DATE_FORMAT(b.renewal_date, '%M %Y')
				FROM nse_aa_category AS a
				JOIN nse_establishment_assessment AS b ON b.qa_status=a.aa_category_code
				WHERE b.assessment_id=?
				LIMIT 1";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bind_param('i', $assessment_id);
$sql_category->execute();
$sql_category->store_result();
$sql_category->bind_result($endorsement, $qa_logo, $assessment_date);
$sql_category->fetch();
$sql_category->free_result();
$sql_category->close();

//Get Assessment Status
$statement = "SELECT a.subcategory_name
				FROM nse_restype_subcategory_lang AS a
				JOIN nse_establishment_assessment AS b ON b.proposed_category=a.subcategory_id
				WHERE b.assessment_id=?
				LIMIT 1";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bind_param('i', $assessment_id);
$sql_category->execute();
$sql_category->store_result();
$sql_category->bind_result($category);
$sql_category->fetch();
$sql_category->free_result();
$sql_category->close();

//Get rooms / Units
$statement = 'SELECT room_count, room_type FROM nse_establishment_data WHERE establishment_code=?';
$sql_rooms = $GLOBALS['dbCon']->prepare($statement);
$sql_rooms->bind_param('s', $establishment_code);
$sql_rooms->execute();
$sql_rooms->store_result();
$sql_rooms->bind_result($room_count, $room_type);
$sql_rooms->fetch();
$sql_rooms->free_result();
$sql_rooms->close();
if (empty($room_type)) $room_type = 'Rooms';

$filename = strtolower(str_replace(' ', '_', "$establishment_code $assessment_date"));

$certificate =& new FPDI('P','mm','A4',true,'UTF-8',false);
$pagecount = $certificate->setSourceFile($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/pdf_template/QAPD_CERTIFICATE_TEMPLATE.pdf');
$certificate_template = $certificate->importPage(1, '/MediaBox');
$dimensions = $certificate->getTemplateSize($certificate_template);

// remove default header/footer
$certificate->setPrintHeader(false);
$certificate->setPrintFooter(false);

$certificate->addPage();
$certificate->SetAutoPageBreak(false);
$certificate->addFont('garamond', '', $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/fonts/garamondpremrprosmbdit.php');
$certificate->addFont('vera', '', $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/fonts/vera.php');
$certificate->addFont('verabd', '', $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/fonts/verabd.php');
$certificate->useTemplate($certificate_template, -13, -13, $dimensions['w']-9, $dimensions['h']-9);

$certificate->SetFont('verabd', '', 25);
$certificate->ln(42);
$certificate->setLeftMargin(40);
$certificate->Cell(0, 12, 'AA Quality Assured', 0, 1, 'C');
$certificate->Cell(0, 12, 'Accommodation Programme', 0, 1, 'C');

$certificate->SetFont('garamond', '', 45);
$certificate->ln(10);
$certificate->multicell(0, 0, utf8_encode($establishment_name), 0, 'C');

$certificate->SetFont('vera', '', 20);
$certificate->ln(10);
$certificate->Cell(0, 12, 'has been endorsed as', 0, 1, 'C');

$certificate->SetFont('garamond', '', 40);
$certificate->ln(10);
$certificate->Cell(0, 12, $endorsement, 0, 1, 'C');

$certificate->SetFont('vera', '', 20);
$certificate->ln(10);
$certificate->Cell(0, 12, 'in the category', 0, 1, 'C');

$certificate->SetFont('verabd', '', 22);
$certificate->ln(10);
$certificate->setX(70);
$certificate->setLineWidth(1);
$certificate->multiCell(100, 12, $category, 1, 'C');

$certificate->SetFont('verabd', '', 10);
$certificate->setTextColor(250, 250, 250);
$certificate->setXY(15, 275);
$certificate->multiCell(0, 0, "$room_count $room_type", 0, 'L');

$certificate->SetFont('verabd', '', 10);
$certificate->setTextColor(250, 250, 250);
$certificate->setXY(-45, 275);
$certificate->multiCell(0, 0, "Valid Until\n$assessment_date", 0, 'C');

$certificate->setJPEGQuality(100);
$certificate->Image($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$qa_logo", 165, 215, 35, 0, '', '', '', true, 150);

if (is_file($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf")) unlink($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf");
$certificate->output($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf", 'F');
$certificate->Close();

$certificate_file = $_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf";
?>