var change_flag = false;
var cnt = 0;

function showFilter() {
	v = get('uEstabFilter');
	document.getElementById('uFilter').value = v;
}

function filter(v) {
	if (v == 'all') {
		eraseCookie('uEstabFilter');
	} else {
		createCookie('uEstabFilter', v);
	}
	document.location = document.location;
}

function sFilter(c) {
	if (c) {
		document.location = '/modules/assessment_s/assessor_s.php?f=new_assessment';
	} 
}
function sFilter_a(c) {
	if (c) {
		document.location = '/modules/assessment_s/assessor_s.php?f=list_assessments';
	} 
}

function assess_redirect(id) {
	document.location = '/modules/assessment_s/assessor_s.php?f=assess_estab&code=' + id;
}

function estab_view() {
	document.location = '/modules/assessor/assessor.php?f=estab_view';
}

function estab_print() {
	document.location = '/modules/assessor/assessor.php?f=print';
}

function tCM(id,met) {
	if (met) {
		var cu = document.getElementById('content' + id).style.display;
		if (cu == 'block') {
			document.getElementById('click_' + id).innerHTML = '[Click to Close]';
		} else {
			document.getElementById('click_' + id).innerHTML = '[Click to Open]';
		}
	} else {
		document.getElementById('click_' + id).innerHTML = '';
	}
}

function show_block(bId) {
	m=1;
	c=1;
	var cu = document.getElementById('content' + bId).style.display;
	if (cu == 'block') {
		document.getElementById('content'+bId).style.display = 'none';
		document.getElementById('header'+bId).className = 'block_header';
	} else {
		while(m) {
			if (document.getElementById('content'+c)) {
				document.getElementById('content'+c).style.display = 'none';
				document.getElementById('header'+c).className = 'block_header';
			} else {
				m=0;
			}
			c++;
		}
		document.getElementById('content'+bId).style.display = 'block';
		document.getElementById('header'+bId).className = 'block_header_active';
	}
}

function set_change(id2) {
	var append = true;
	document.getElementById('indicator_'+ id2).src = '/i/led-icons/application_edit.png';
	document.getElementById('indicator_'+ id2).title = 'This section has been updated';
	change_flag = true;
}

function na(id) {
	if (document.getElementById('na_' + id).checked) {
		document.getElementById('indicator_'+ id).src = '/i/led-icons/application_delete.png';
		document.getElementById('indicator_'+ id).title = 'Not Applicable';
		document.getElementById('area_'+ id).style.display = 'none';
		document.getElementById('ratings_'+ id).style.display = 'none';
	} else {
		document.getElementById('indicator_'+ id).src = '/i/blank.png';
		document.getElementById('indicator_'+ id).title = '';
		document.getElementById('area_'+ id).style.display = 'block';
		document.getElementById('ratings_'+ id).style.display = 'block';
	}
}
function hide(id) {
	if (!document.getElementById('hide_' + id).checked) {
		document.getElementById('hide_indicator_'+ id).src = '/i/silk/group.png';
		document.getElementById('hide_indicator_'+ id).title = 'Visible to clients';
	} else {
		document.getElementById('hide_indicator_'+ id).src = '/i/silk/group_delete.png';
		document.getElementById('hide_indicator_'+ id).title = 'NOT visible to clients';
	}
}

function cI() {
	if (change_flag) {
		if (confirm('WARNING! Some elements on this page appear to have been edited. All changes will be lost.\n\nContiue?')) {
			bID = window.frameElement.id;
			bID = bID.substring(3);
			parent.WD(bID);
		}
	} else {
		bID = window.frameElement.id;
		bID = bID.substring(3);
		parent.WD(bID);
	}
}

function ret_assessment(id,code) {
	if (confirm('NOTE\n\nPlease make sure you have entered a comment for the assessor in the \'Head Office Notes\' field. If you have not done so please cancel this task, enter a comment and try again.')) {
		document.getElementById('estab_form').action = '/modules/assessment_s/assessor_s.php?f=approve_assessment&return=1&code='+ code + '&id=' + id;
		document.getElementById('estab_form').submit();
	}
}

function toggle_email(m) {
	if (m) {
		document.getElementById('emails').style.display = 'block';
		document.getElementById('email_button').value = 'Send Email';
		document.getElementById('email_button').className = 'email_button';
	} else {
		document.getElementById('emails').style.display = 'none';
		document.getElementById('email_button').value = 'Continue';
		document.getElementById('email_button').className = 'go_button';
	}
}

function add_contact_form(id) {
	contact_name = '';
	designation = '';
	tel = '';
	cell = '';
	email = '';
	admin_user = 0;
	email_password = 0;
	if (id !== 'new') {
		var item = cL.split('!|!');
		for (i=0; i<item.length; i++) {
			iT = item[i].split('|');
			if (iT[0] == id) {
				contact_name = iT[1];
				designation = iT[2];
				tel = iT[3];
				cell = iT[4];
				email = iT[5];
				admin_user = iT[6];
				email_password = iT[7];
			}
		}
	}

	cB = "<table>";
	cB += "<tr><td style='width: 50px' class=field_label>Name</td><td style='width: 200px'><input style='width: 100%' type=text id=add_name value='" + contact_name + "'></td></tr>";
	cB += "<tr valign=top><td class=field_label>Designation</td><td><input style='width: 100%' type=text id=add_designation value='" + designation + "'><br />";
	cB += "<select id=sD style='width: 100%' onchange='change_designation(this.value)' ><option value=''> </option><option value='Owner'>Owner</option><option value='Owner/Manager'>Owner/Manager</option>";
	cB += "<option value='Manager'>Manager</option><option value='Marketing Manager'>Marketing Manager</option></select></td></tr>";
	cB += "<tr><td class=field_label>Tel</td><td><input style='width: 100%' type=text id=add_tel value='" + tel + "'></td></tr>";
	cB += "<tr><td class=field_label>Cell</td><td><input style='width: 100%' type=text id=add_cell value='" + cell + "'></td></tr>";
	cB += "<tr><td class=field_label>Email</td><td><input style='width: 100%' type=text id=add_email value='" + email + "'></td></tr>";
	cB += "<tr><td>&nbsp;</td><td class=field_label title='Allow this user access to the admin system'><input type=checkbox name=admin_user id=admin_user ";
	cB += admin_user==1?"checked":'';
	cB += " >Admin System Access</td></tr>";
	//cB += "<tr><td>&nbsp;</td><td ";
	//cB += admin_user==1?"class=field_label_enabled":"class=field_label_disabled";
	//cB += " id=rPC title='Resend the user thier password to access the admin system'><input type=checkbox name=resend_password id=resend_password "
	//cB += admin_user==1?' ':' disabled ';
	//cB += email_password==1?' checked ':' ';
	//cB += " >Email Password</td></tr>";
	cB += "<tr><td colspan=2>&nbsp;</td></tr>";
	cB += "<tr><td colspan=2 align=right><input type=button value='Cancel' class=cancel_button onClick='document.getElementById(\"contact_form\").style.display=\"none\"' /><input type=button value='OK' class=ok_button onClick=add_contact_update('" + id + "') /></td></tr>";


	cB += "</table>";
	document.getElementById('contact_form').innerHTML = cB;
	document.getElementById('contact_form').style.display = 'block';
}

function change_designation(v) {
	document.getElementById('add_designation').value=v;
	document.getElementById('sD').selectedIndex = 0;
}

function check_admin_user(v) {
	if (v) {
		document.getElementById('rPC').className = 'field_label_enabled';
		document.getElementById('resend_password').disabled = false;
	} else {
		document.getElementById('resend_password').checked = false;
		document.getElementById('resend_password').disabled = true;
		document.getElementById('rPC').className = 'field_label_disabled';
	}
}

function add_contact_update(id) {
	if (id == 'new') {
		cL += "!|!";
		cL += "new" + cnt + "|" + document.getElementById('add_name').value + "|" + document.getElementById('add_designation').value + "|";
		cL += document.getElementById('add_tel').value + "|" + document.getElementById('add_cell').value + "|" + document.getElementById('add_email').value;
		cL += document.getElementById('admin_user').checked?"|1":'|0';
		//cL += document.getElementById('resend_password').checked?"|1":'|0';
		cL += "|0|new|0";

		cnt++;
	} else {
		newList = '';
		var item = cL.split('!|!');
		for (i=0; i<item.length; i++) {
			iT = item[i].split('|');
			if (iT[0] == id) {
				newList += id + "|" + document.getElementById('add_name').value + "|" + document.getElementById('add_designation').value + "|";
				newList += document.getElementById('add_tel').value + "|" + document.getElementById('add_cell').value + "|" + document.getElementById('add_email').value;
				newList += document.getElementById('admin_user').checked?"|1":'|0';
				//newList += document.getElementById('resend_password').checked?"|1":'|0';
				newList += '|0|' + iT[8] + '|' + iT[9];
				newList += "!|!";
			} else {
				newList += item[i] + "!|!";
			}
		}
		cL = newList;
	}

	populate_contact_list();
	document.getElementById('contact_form').style.display = 'none';
}

function remove_contact(id) {
	newList = '';
	var item = cL.split('!|!');
	for (i=0; i<item.length; i++) {
		iT = item[i].split('|');
		if (iT[0] != id) {
			newList += item[i] + "!|!";
		} else {
			if (iT[8] == 'new') continue;
			newList += iT[0] + "|" + iT[1] + "|" + iT[2] + "|";
			newList += iT[3] + "|" + iT[4] + "|" + iT[5];
			newList += '|' + iT[6];
			newList += '|' + iT[7];
			newList += "|" + iT[8] + '|1';
			newList += "!|!";
		}
	}
	cL = newList;
	populate_contact_list();
}

function populate_contact_list() {
	var table = "<table width=100% cellpadding=2 cellspacing=0 id=contact_list>";
	table += "<tr><th>&nbsp;</th><th>Name</th><th>Designation</th><th>Tel</th><th>Cell</th><th>Email</th><th>&nbsp;</th></tr>";

	if (cL) {
		var item = cL.split('!|!');
		cl = 'line1';
		for (i=0; i<item.length; i++) {
			if (item[i] == '') continue;
			iT = item[i].split('|');
			if (iT[9] == 1) continue;
			table += "<tr ";
			table += i%2==0?" class='line1' ":" class='line2' ";
			table += "onMouseOver='this.className=\"line_over\"' onMouseOut='this.className=\"";
			table += i%2==0?"line1\"' ":"line2\"' ";
			table += "><td>"
			table += iT[6]==1?"<img src='/i/silk/shield.png' title='This contact has access to the admin system' />":'';
			table += "</td><td>" + iT[1] + "</td><td>" + iT[2] + "</td><td>" + iT[3] + "</td><td>" + iT[4] + "</td><td>" + iT[5] + "</td>";
			table += "<td align=right><input type=button value='Edit' class=edit_button onClick=\"add_contact_form('"+ iT[0] +"')\" /><input type=button value='Remove' class=delete_button onClick=\"remove_contact('"+ iT[0] +"')\" /></td></tr>";
		}
	}

	table += "<tr><td colspan=10 align=right><input type=button value='Add Contact' class=add_button onClick=add_contact_form('new') /></td>";
	table += "</tr></table>";
	table += "<input type=hidden name=contacts value='"+ cL +"' >";

	document.getElementById('contacts').innerHTML = table;

	document.getElementById('contacts_list').value = cL;

	if (!fRun) {
		set_change('private_contact', 'private_contacts', 'contacts');
	}
}

