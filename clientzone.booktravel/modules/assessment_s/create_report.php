<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/tcpdf.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/fpdi/fpdi.php';

//error_reporting(0);

//Vars
$assessor = '';
$assessment_date = '';
$assessment_name = '';
$assessment_content = '';
$assessment_id = $_REQUEST['id'];
$category = '';
$establishment_code = $_GET['code'];
$establishment_name = '';
$endorsement = '';
$province_name = '';
$qa_logo = '';
$renewal_date = '';
$town_name = '';


//Get Data
$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
$sql_data->bind_param('s', $establishment_code);
$sql_data->execute();
$sql_data->store_result();
$sql_data->bind_result($establishment_name);
$sql_data->fetch();
$sql_data->free_result();
$sql_data->close();

//Get Assessment Status
$statement = "SELECT a.aa_category_name, a.pdf_image, DATE_FORMAT(b.renewal_date, '%M %Y'), DATE_FORMAT(b.assessment_date, '%d %M %Y'), b.assessor_name
				FROM nse_aa_category AS a
				JOIN nse_establishment_assessment AS b ON b.qa_status=a.aa_category_code
				WHERE b.assessment_id=?
				LIMIT 1";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bind_param('i', $assessment_id);
$sql_category->execute();
$sql_category->store_result();
$sql_category->bind_result($endorsement, $qa_logo, $renewal_date, $assessment_date, $assessor);
$sql_category->fetch();
$sql_category->free_result();
$sql_category->close();

//Get Assessment Status
$statement = "SELECT a.subcategory_name
				FROM nse_restype_subcategory_lang AS a
				JOIN nse_establishment_assessment AS b ON b.proposed_category=a.subcategory_id
				WHERE b.assessment_id=?
				LIMIT 1";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bind_param('i', $assessment_id);
$sql_category->execute();
$sql_category->store_result();
$sql_category->bind_result($category);
$sql_category->fetch();
$sql_category->free_result();
$sql_category->close();

//Get Location
$statement = "SELECT b.town_name, c.province_name
				FROM nse_establishment_location AS a
				LEFT JOIN nse_location_town AS b ON a.town_id=b.town_id
				LEFT JOIN nse_location_province AS c ON a.province_id=c.province_id
				WHERE a.establishment_code=? LIMIT 1";
$sql_location = $GLOBALS['dbCon']->prepare($statement);
$sql_location->bind_param('s', $establishment_code);
$sql_location->execute();
$sql_location->store_result();
$sql_location->bind_result($town_name, $province_name);
$sql_location->fetch();
$sql_location->free_result();
$sql_location->close();
if (!empty($province_name)) $town_name .= ", $province_name";

//Get Assessor
$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";


$filename = strtolower(str_replace(' ', '_', "$establishment_code report $renewal_date"));

//Create Report
$report =& new FPDI('P','mm','A4',true,'UTF-8',false);
$report->SetMargins(1.25, 1.25, 1);

// remove default header/footer
$report->setPrintHeader(false);
$report->setPrintFooter(false);

$report->addPage();
$report->SetAutoPageBreak(true, 2);
$report->addFont('vera', '', $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/fonts/vera.php');
$report->addFont('verabd', '', $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/fonts/verabd.php');

$report->setJPEGQuality(100);
$report->Image($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$qa_logo", 2, 2, 35, 0, '', '', '', true, 150);

$report->SetFont('verabd', '', 10);
//$report->setLeftMargin(40);
$report->Cell(40, 0, ' ', 0, 0);
$report->Cell(0, 0, 'Quality Assurred Assessment Report for', 0, 1);
$report->SetFont('verabd', '', 12);
$report->ln(1);
$report->Cell(40, 0, ' ', 0, 0);
$report->Cell(0, 0, "$establishment_name - $town_name.", 0, 1);

$report->SetFont('vera', '', 8);
$report->ln(5);
$report->Cell(40, 0, ' ', 0, 0);
$report->Cell(30, 0, 'Assessment Date: ', 0, 0);
$report->Cell(0, 0, $assessment_date, 0, 1);

$report->Cell(40, 0, ' ', 0, 0);
$report->Cell(30, 0, 'Assessor: ', 0, 0);
$report->Cell(0, 0, $assessor, 0, 1);

$report->Cell(40, 0, ' ', 0, 0);
$report->Cell(30, 0, 'Category: ', 0, 0);
$report->Cell(0, 0, $category, 0, 1);

$report->ln(30);
$report->Cell(0, 0, "Our assessor visited your establishment on the $assessment_date and compiled the following report.", 0, 1);


//Get assessment Data
$statement = "SELECT b.assessment_area_name, a.assessment_content
				FROM nse_establishment_assessment_data AS a
				JOIN nse_assessment_areas AS b ON a.assessment_area_id=b.assessment_area_id
				WHERE a.not_applicable != 'Y' && hide_from_client != 'Y' && a.assessment_id=?
				ORDER BY b.display_order";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
$sql_data->bind_param('i', $assessment_id);
$sql_data->execute();
$sql_data->store_result();
$sql_data->bind_result($assessment_name, $assessment_content);
while ($sql_data->fetch()) {
	if (empty($assessment_content)) continue;
	$report->SetFont('verabd', '', 8);
	$report->ln(10);
	$report->Cell(0, 0, $assessment_name, 0, 1);
	$report->SetFont('vera', '', 8);
	$report->MultiCell(0, 0, $assessment_content, 0, 1);
}
$sql_data->free_result();
$sql_data->close();


if (is_file($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf")) unlink($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf");
$report->output($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf", 'F');
$report->close();

$report_file = $_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf";





























?>