<?php
/**
 * Manage establishment Data
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category establishment
 */

//Vars
$function_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';
$function_white_list = array('passw','notifications');

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/preferences/module.ini.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/preferences/html/prefs.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/preferences/html/prefs.js'> </script>";

//Map
switch ($function_id) {
	case 'notifications':
		if (isset($_POST['update_notifications'])) {
			notifications_update();
		} else {
			notifications_form();
		}
		break;
	case 'passw':
		if (isset($_POST['update_password'])) {
			password_update();
		} else {
			password_form();
		}
		break;
	default:
		welcome();
}

function password_update() {
	//Vars
	$password = $_POST['abd_password'];
	$password_r = $_POST['abd_r_password'];
	$user_id = $_SESSION['dbweb_user_id'];
	$password_error = FALSE;
	$error_message = "Password successfully updated.";
	$goto = '';

	//Includes
	require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/tcrypt.class.php';
	$tc = new tcrypt();
	
	//Check Passwords
	if (strlen($password) < 5) {
		$password_error = TRUE;
		$error_message = "Password change failed. New password is less than 6 characters.";
		$goto = "?f=passw";
	}
	if ($password != $password_r) {
		$password_error = TRUE;
		$error_message = "Password change failed. To ensure that you are not accidenly locked out of the system,
							we require that you repeat the password in the second field.";
		$goto = "?f=passw";
	}
	
	//Update Password
	if (!$password_error) {
		$pass = $tc->encrypt($password);
		$password2 = $pass['data'];
		$iv = $pass['iv'];
		unset($_POST["password"]);

		//Save new password
		$statement = "UPDATE nse_user SET iv=?, password=? WHERE user_id=?";
		$sql_password = $GLOBALS['dbCon']->prepare($statement);
		$sql_password->bind_param('ssi', $iv, $password2, $user_id);
		$sql_password->execute();
	}
	
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/preferences/html/password_complete.html');
	
	//Replace tags
	$template = str_replace('<!-- message -->', $error_message, $template);
	$template = str_replace('<!-- goto -->', $goto, $template);
	
	echo $template;
}

function password_form() {
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/preferences/html/password_form.html');
	
	echo $template;
}

function notifications_form() {
	//Vars
	$user_id = $_SESSION['dbweb_user_id'];
	$notification_code = '';
	$notification_value = '';
	$values = array('change_approval' => '',
					'feedback' => '');
	
	//Get Template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/preferences/html/notifications_form.html');
	
	//Get current settings
	$statement = "SELECT notification_code, notification_value FROM nse_user_notices WHERE user_id=?";
	$sql_notices = $GLOBALS['dbCon']->prepare($statement);
	$sql_notices->bind_param('s', $user_id);
	$sql_notices->execute();
	$sql_notices->store_result();
	$sql_notices->bind_result($notification_code,$notification_value);
	while ($sql_notices->fetch()) {
		if ($notification_value == 1) $values[$notification_code] = 'checked';
	}
	$sql_notices->free_result();
	$sql_notices->close();
	
	//Replace Tags
	foreach ($values as $k=>$v) {
		$template = str_replace("<!-- $k -->", $v, $template);
	}
	
	echo $template;
}

function notifications_update() {
	//Vars
	$user_id = $_SESSION['dbweb_user_id'];
	$values = array('change_approval','feedback');
	
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/preferences/html/notifications_update.html");
	
	//Clear current notification settings
	$statement = "DELETE FROM nse_user_notices WHERE user_id=?";
	$sql_clean = $GLOBALS['dbCon']->prepare($statement);
	$sql_clean->bind_param('s',$user_id);
	$sql_clean->execute();
	$sql_clean->close();
	
	//Insert new settings
	$statement = "INSERT INTO nse_user_notices (user_id,notification_code,notification_value) VALUES (?,?,'1')";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);
	foreach ($values as $k) {
		if (isset($_POST[$k])) {
			$sql_insert->bind_param('ss', $user_id,$k);
			$sql_insert->execute();
		}
	}
	
	echo $template;
}

function welcome() {
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/preferences/html/welcome.html');
	
	echo $template;
}
?>