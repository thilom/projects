<?php
/**
 * Module settings for user preferences
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category preferences
 */
$module_id = 'preferences';
$module_name = "My Preferences";
$module_icon = 'personal-64.png';
$noaccess_icon = 'prefs_noaccess.png';
$module_link = 'prefs.php';
$window_width = '800';
$window_height = '700';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 'a'; //a = all




?>