<?php
/**
 * Module settings for Establishment management - Client Access
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category establishment
 */
$module_id = 'specials';
$module_name = "Promotions Manager";
$module_icon = 'Locker_Favorites_64x64.png';
$noaccess_icon = 'specials_noaccess.png';
$module_link = 'specials.php';
$window_width = '800';
$window_height = '600';
$window_position = '';

//Include CMS settings
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security | NOTE: Once created, array keys should not be changed.
$top_level_allowed = 'c'; //a = all




?>