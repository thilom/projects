<?php
/**
 * Manage special offers for establishments
 */

//Vars
$function_white_list = array('list_specials', 'edit_special', 'add_special', 'delete_special');
$function_id = '';
$parent_id = '';
$user_access = '';
$css = '';
$module_id = '';
$function = '';

//Includes
require_once '../../settings/init.php';
include SITE_ROOT . '/modules/specials/module.ini.php';

if (isset($_GET['f']) && in_array($_GET['f'], $function_white_list)) {
	$function_id = $_GET['f'];
}

include_once SITE_ROOT."/shared/admin_style.php";
echo $css;
echo '<link href="/shared/shared.css" type="text/css" rel="stylesheet" />';
echo '<link href="/modules/specials/html/specials.css" type="text/css" rel="stylesheet" />';
echo "<script src='/modules/specials/html/specials.js'> </script>";

//Map
switch ($function_id) {
	case 'list_specials':
		list_specials();
		break;
	case 'add_special':
		if (isset($_POST['save_special'])) {
			add_special_save();
		} else {
			add_special_form();
		}
		break;
	case 'edit_special':
		if (isset($_POST['save_special'])) {
			edit_special_save();
		} else {
			edit_special_form();
		}
		break;
	case 'delete_special':
		delete_special();
		break;
	default:
		welcome();
}

function add_special_form() {
	//Vars
	$establishment_code = $_GET['id'];
	$establishment_name = '';
	
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/specials/html/add_special_form.html');
	
	//Get establishment name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_establishment = $GLOBALS['dbCon']->prepare($statement);
    $sql_establishment->bind_param('s', $establishment_code);
    $sql_establishment->execute();
    $sql_establishment->store_result();
    $sql_establishment->bind_result($establishment_name);
    $sql_establishment->fetch();
    $sql_establishment->free_result();
    $sql_establishment->close();
    
    //Replace Tags
    $template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
    $template = str_replace('<!-- special_start -->', '', $template);
	$template = str_replace('<!-- special_end -->', '', $template);
	$template = str_replace('<!-- special_name -->', '', $template);
	$template = str_replace('<!-- special_description -->', '', $template);
	$template = str_replace('<!-- type -->', 'Add', $template);
	$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- aa_members_only -->', '', $template);
	
	echo $template;
}

function add_special_save() {
	//Vars
	$specials_start = $_POST['special_start'];
	$specials_end = $_POST['special_end'];
	$specials_name = $_POST['special_name'];
	$specials_description = $_POST['special_description'];
	$establishment_code = $_GET['id'];
	$aa_members_only = isset($_POST['aa_members_only'])?'1':'';
	
	//Save
	$statement = "INSERT INTO nse_establishment_specials (establishment_code, specials_start, specials_end, specials_name, specials_description, aa_members_only) VALUES (?,?,?,?,?,?)";
	$sql_specials = $GLOBALS['dbCon']->prepare($statement);
	$sql_specials->bind_param('ssssss', $establishment_code, $specials_start, $specials_end, $specials_name, $specials_description, $aa_members_only);
	$sql_specials->execute();
	
	$ch = curl_init("http://www.aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	//$ch = curl_init("http://aatravel.server1/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	//Redirect
	echo "<script>alert('Special Offer Added')</script>";
	echo "<script>document.location='/modules/specials/specials.php?f=list_specials&id=$establishment_code'</script>";
}

function edit_special_form() {
	//Vars
	$special_id = $_GET['sID'];
	$specials_start = '';
	$specials_end = '';
	$specials_name = '';
	$specials_description = '';
	$establishment_name = '';
	$establishment_code = $_GET['id'];
	$aa_members_only = '';
	
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/specials/html/add_special_form.html');
	
	//Get establishment name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_establishment = $GLOBALS['dbCon']->prepare($statement);
    $sql_establishment->bind_param('s', $establishment_code);
    $sql_establishment->execute();
    $sql_establishment->store_result();
    $sql_establishment->bind_result($establishment_name);
    $sql_establishment->fetch();
    $sql_establishment->free_result();
    $sql_establishment->close();
	
	//Get current Data
	$statement = "SELECT specials_start, specials_end, specials_name, specials_description, aa_members_only FROM nse_establishment_specials WHERE specials_id=?";
	$sql_specials = $GLOBALS['dbCon']->prepare($statement);
	$sql_specials->bind_param('s', $special_id);
	$sql_specials->execute();
	$sql_specials->store_result();
	$sql_specials->bind_result($specials_start, $specials_end, $specials_name, $specials_description, $aa_members_only);
	$sql_specials->fetch();
	$sql_specials->free_result();
	$sql_specials->close();
	
	//AA members only
	$aa_members_only = $aa_members_only==1?'checked':'';
	
	//Replace Tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
    $template = str_replace('<!-- special_start -->', $specials_start, $template);
	$template = str_replace('<!-- special_end -->', $specials_end, $template);
	$template = str_replace('<!-- special_name -->', $specials_name, $template);
	$template = str_replace('<!-- special_description -->', $specials_description, $template);
	$template = str_replace('<!-- type -->', 'Edit', $template);
	$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);
	$template = str_replace('<!-- aa_members_only -->', $aa_members_only, $template);
	
	echo $template;
	
}

function edit_special_save() {
	//Vars
	$specials_start = $_POST['special_start'];
	$specials_end = $_POST['special_end'];
	$specials_name = $_POST['special_name'];
	$specials_description = $_POST['special_description'];
	$establishment_code = $_GET['id'];
	$specials_id = $_GET['sID'];
	$aa_members_only = isset($_POST['aa_members_only'])?'1':'';
	
	//Save
	$statement = "UPDATE nse_establishment_specials SET specials_start=?, specials_end=?, specials_name=?, specials_description=?, aa_members_only=? WHERE specials_id=?";
	$sql_specials = $GLOBALS['dbCon']->prepare($statement);
	$sql_specials->bind_param('ssssss', $specials_start, $specials_end, $specials_name, $specials_description, $aa_members_only, $specials_id);
	$sql_specials->execute();
	$sql_specials->close();
	
	$ch = curl_init("http://www.aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	//$ch = curl_init("http://aatravel.server1/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	//Redirect
	echo "<script>alert('Special Offer Updated')</script>";
	echo "<script>document.location='/modules/specials/specials.php?f=list_specials&id=$establishment_code'</script>";
	
}

function delete_special() {
	//Vars
	$establishment_code = $_GET['id'];
	$special_id = $_GET['sID'];
	
	//Delete Special
	$statement = "DELETE FROM nse_establishment_specials WHERE specials_id=?";
	$sql_specials = $GLOBALS['dbCon']->prepare($statement);
	$sql_specials->bind_param('i', $special_id);
	$sql_specials->execute();
	
	$ch = curl_init("http://www.aatravel.co.za/export/update_establishment.php?source=etinfo&code=$establishment_code");
	//$ch = curl_init("http://aatravel.server1/export/update_establishment.php?source=etinfo&code=$establishment_code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	//Redirect
	echo "<script>alert('Special Offer Removed')</script>";
	echo "<script>document.location='/modules/specials/specials.php?f=list_specials&id=$establishment_code'</script>";
	
}

//List specials
function list_specials($message='') {
	//Vars
	$establishment_code = $_GET['id'];
	$specials_id = '';
	$specials_name = '';
	$specials_start = '';
	$specials_end = '';
	$nLine = '';
	$establishment_name = '';
	
	//Get Template
    $content = file_get_contents(SITE_ROOT . '/modules/specials/html/list_specials.html');
    $content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
    preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
    $lContent = $matches[0][0];
    
    //Get establishment name
    $statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_establishment = $GLOBALS['dbCon']->prepare($statement);
    $sql_establishment->bind_param('s', $establishment_code);
    $sql_establishment->execute();
    $sql_establishment->store_result();
    $sql_establishment->bind_result($establishment_name);
    $sql_establishment->fetch();
    $sql_establishment->free_result();
    $sql_establishment->close();
    
    //Get List of Specials
    $statement = "SELECT specials_id, specials_start, specials_end, specials_name FROM nse_establishment_specials WHERE establishment_code=? ORDER BY specials_start DESC";
    $sql_specials = $GLOBALS['dbCon']->prepare($statement);
    $sql_specials->bind_param('s', $establishment_code);
    $sql_specials->execute();
    $sql_specials->store_result();
    $sql_specials->bind_result($specials_id, $specials_start, $specials_end, $specials_name);
    while ($sql_specials->fetch()) {
    	$line = str_replace('<!-- start_date -->', $specials_start, $lContent);
    	$line = str_replace('<!-- end_date -->', $specials_end, $line);
    	$line = str_replace('<!-- name -->', $specials_name, $line);
    	$line = str_replace('<!-- special_id -->', $specials_id, $line);
    	
    	$nLine .= $line;
    }
    $sql_specials->free_result();
    $sql_specials->close();
    
    //Replace_tags
    $content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
    $content = str_replace('$%^', "\n", $content);
    $content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
    $content = str_replace('<!-- establishment_code -->', $establishment_code, $content);
    $content = str_replace('<!-- f -->', $_GET['f'], $content);
    
    //Attach message
    if (!empty($message)) {
        $content .= "<script>alert('$message')</script>";
    }
    
    echo $content;
}

//The welcome page for the module
function welcome() {
	//Vars
	$establishment_code = '';
	$establishment_name = '';
	
	//Get template
	$template = file_get_contents(SITE_ROOT . '/modules/specials/html/welcome.html');
	
	
	//Get Establishment List
	$statement = "SELECT a.establishment_code, b.establishment_name
					FROM nse_user_establishments AS a
					JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE a.user_id=?";
	$sql_list = $GLOBALS['dbCon']->prepare($statement);
	$sql_list->bind_param('s', $_SESSION['dbweb_user_id']);
	$sql_list->execute();
	$sql_list->store_result();
	$sql_list->bind_result($establishment_code, $establishment_name);
	if ($sql_list->num_rows == 0) {
		$content = "<div id='no_estabs' >You have no access to establishment data. </div>";
	} else if ($sql_list->num_rows == 1) {
		$sql_list->fetch();
		$content = "<table width=100% id=icons><tr><td align=right><input type=button value='Close' class=cancel_button onClick=cI()> <input type=button value='Continue' class=ok_button onClick=document.location='/modules/specials/specials.php?f=list_specials&id=$establishment_code' /></td></tr></table>";
	} else {
		$content = "<table width=100% cellpadding=0 cellspacing=0 id=TRW><tr><th align=left>Select the establishment to edit</td><th></th></tr>";
		while ($sql_list->fetch()) {
			$content .= "<tr><td>$establishment_name</td><td align=right><input type=button value='Edit' class=ok_button onClick=document.location='/modules/specials/specials.php?f=list_specials&id=$establishment_code' /></td></tr>";
		}
		$content .= "<tr height=2><th colspan=10></th></tr></table><script>whack();</script> ";
	}
	$sql_list->free_result();
	$sql_list->close();
	
	$template = str_replace('<!-- establishment_list -->', $content, $template);
	
	echo $template;
}
?>