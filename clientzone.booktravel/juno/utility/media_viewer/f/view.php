<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/dir.php";
include $SDR."/utility/media_viewer/f/getter.php";

if(isset($_GET["jget"]))
{
	include $SDR."/utility/media_viewer/f/format.php";
	$s=(isset($_GET["hs"])?substr(trim($_GET["hs"]),0,32):0);
	if($_GET["ht"]>0 || $_GET["ht"]<0)
		$h=j_med_sub($_GET["jget"],$s,$j_fmt[$_GET["ht"]]);
	elseif($_GET["ht"])
		$h=j_med_sub($_GET["jget"],$s,$_GET["ht"]);
	else
		$h=j_med_sub($_GET["jget"]);
	echo "<script>window.parent.JL_i_get(0,\"".$h."\",\"".$_GET["jid"]."\")</script>";
	die();
}

elseif(isset($_GET["vfle"]))
{
	if(strpos($_GET["vfle"],".htm")!==false)
		header("Location: ".$ROOT.$_GET["vfle"]);
	else
	{
		echo "<html><body><h5>".$_GET["vfle"]."</h5><hr><br>";
		echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		$s=file_get_contents($SDR.$_GET["vfle"]);
		if(strpos($_GET["vfle"],".txt")!==false){}
		else
		{
			$s=str_replace("<","&lt;",$s);
			$s=str_replace(">","&gt;",$s);
			$s=str_replace("\n","<br>",$s);
			$s=str_replace("\r","",$s);
			$s=str_replace("\t","&nbsp; ",$s);
		}
		echo $s;
		echo "</body></html>";
	}
	die();
}

include $SDR."/utility/media_viewer/f/format.php";
$t=0;
$n="Media";
$l=(isset($_REQUEST["hl"])?$_REQUEST["hl"]:0);
if(isset($_REQUEST["ht"])){$t=$_REQUEST["ht"];$n=($t==-1?"Document":($t>0?$j_fmtN[$t]:strtoupper(substr($_REQUEST["ht"],1))));}
elseif(isset($_GET["docs"])){$t=-1;$n="Document";$l=-1;}
elseif(isset($_GET["image"])){$t=1;$n=$j_fmtN[1];$l=1;}
elseif(isset($_GET["sound"])){$t=2;$n=$j_fmtN[2];$l=2;}
elseif(isset($_GET["movie"])){$t=3;$n=$j_fmtN[3];$l=3;}
elseif(isset($_GET["flash"])){$t=4;$n=$j_fmtN[4];$l=4;}
elseif(isset($_GET["web"])){$t=5;$n=$j_fmtN[5];$l=5;}
elseif(isset($_GET["js"])){$t=6;$n=$j_fmtN[6];$l=6;}
elseif(isset($_GET["css"])){$t=7;$n=$j_fmtN[7];$l=7;}
$s=(isset($_POST["hs"])?substr(trim($_POST["hs"]),0,32):"");

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>j_i0=1</script>";
echo "<script src=".$ROOT."/utility/media_viewer/f/view.js></script>";
$dir=(isset($_REQUEST["dir"])?(strtolower($_REQUEST["dir"])=="root"?"":$_REQUEST["dir"]):"");
$h=j_med_sub($dir,($s?$s:0),($t<0?-1:($t>0?$j_fmt[$t]:$t)));
$o="";
foreach($j_fmt as $k => $v)
{
	if(!$l || $l==$k)
	{
		$o.="o|".$j_fmtN[$k]."}";
		$o.=$k."|All}";
		foreach($j_fmt[$k] as $a => $b)
			$o.="|".substr($b,1)."}";
		$o.="e}";
	}
}
if(!$l || $l==-1)
{
	$o.="o|Other}";
	$o.="|pdf}";
	$o.="-1|documents}";
	$o.="e}";
}
$m=J_Size($j_mem);
$c=$j_filecount;
echo "<script>JL_i_med(\"".$dir."\",".$l.",".($c?"\"".$h."\"":0).",".(is_numeric($t)?$t:"\"".$t."\"").",".$c.",'".$m."','".$o."','".$n."'".(empty($s)?"":",\"".$s."\"").")</script>";
if(!isset($_REQUEST["ht"]))
{
	$i="media";
	if($l==-1)$i="docs";
	elseif($l==1)$i="images";
	elseif($l==2)$i="sound";
	elseif($l==3)$i="movie";
	elseif($l==4)$i="flash";
	elseif($l==5)$i="web";
	elseif($l==6)$i="go";
	elseif($l==7)$i="css";
	$J_title1="Media Viewer";
	$J_title1=$n." Files";
	$J_icon="<img src=".$ROOT."/ico/set/folder_media.png>";
	$J_icon="<img src=".$ROOT."/ico/set/folder_".$i.".png>";
	$J_label=7;
	$J_left="j_P.j_y+200";
	$J_width=430;
	$J_height=560;
	$J_nostart=1;
	$J_nomax=1;
	$J_join="Media Viewer";
	include $SDR."/system/deploy.php";
}
?>