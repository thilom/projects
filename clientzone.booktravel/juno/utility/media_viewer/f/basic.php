<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

$t="File";
$f="/".trim($_GET["file"],"/");
$J_width=200;
$J_height=100;
$J_icon="<img src=".$ROOT."/ico/set/page.png>";
if(is_file($SDR.$f))
{
	include $SDR."/system/dir.php";
	include $SDR."/system/get.php";
	$J_width=640;
	$J_height=480;
	if($s=getimagesize($SDR.$f))
	{
		echo "<body style=margin:0>";
		echo "<table width=100% height=100%><tr><td style=vertical-align:middle><center><img src=".$ROOT.$f."></center></td></tr></table>";
		$t="Image";
		$J_width=$s[0]+40;
		$J_height=$s[1]+40;
		$J_icon="<img src=".$ROOT."/ico/set/picview.png>";
	}
	else
	{
		echo "<body>";
		if(strpos($f,".pdf")!==false || strpos($f,".htm")!==false)
			$delay=1;
		else
		{
			$s=file_get_contents($SDR.$f);
			$s=str_replace("&","&amp;",$s);
			$s=str_replace("<","&lt;",$s);
			$s=str_replace(">","&gt;",$s);
			$s=str_replace("\n","<br>",$s);
			$s=str_replace("\r","",$s);
			$s=str_replace("\t","&nbsp; ",$s);
			echo $s;
		}
	}
	$n=$f;
	if(strpos($n,"/")!==false)
		$n=substr($n,strrpos($n,"/")+1);
	$J_foot="<a href=".$ROOT."/utility/force_download.php?file=".$f." onfocus=blur() onmouseover=\"J_TT(this,'<b>Download</b><br>".$n."<br>".J_Size(filesize($SDR.$f))."')\" class=jW3a><img src=".$ROOT."/ico/set/download.png></a>";
}

$J_title1=$t;
$J_label=7;
$J_nostart=1;
include $SDR."/system/deploy.php";
if(isset($delay))
	echo "<script>location=ROOT+'".$f."'</script>";
echo "</body></html>";
?>