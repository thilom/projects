<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/dir.php";

$f=$_GET["f"];
$fmt=0;
if($f)
{
	include $SDR."/utility/media_viewer/f/format.php";
	$fmt=array();
	foreach($j_fmt[$f] as $k => $v)
		$fmt[$v]=1;
}

$q=array();

function findItms($dir="")
{
	global $SDR,$ROOT,$t,$fmt,$q;
	$d=opendir($SDR.$dir);
	while(($i=readdir($d))!==false)
	{
		if($i=="." || $i==".."){}
		else
		{
			if(is_file($SDR.$dir."/".$i))
			{
				$j=strtolower($i);
				if(str_replace($t,"",$j,$f) && $f && (!$fmt || isset($fmt[substr($j,strrpos($j,"."))]) ))
					$q[$j]=array(trim($dir,"/")."/".$i,J_Size(filesize($SDR.$dir."/".$i)));
			}
			elseif(is_dir($SDR.$dir."/".$i))
				findItms($dir."/".$i);
		}
	}
	closedir($d);
}

$t=explode(" ",strtolower(trim($_GET["t"])));
findItms();
$f="";
if(count($q))
{
	ksort($q);
	foreach($q as $k => $v)
	{
		$f.=$v[0]."^".$v[1]."|";
	}
}

echo "<script>window.parent.J_medR(\"".$f."\")</script>";

?>