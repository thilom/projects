<?php
require_once $SDR."/utility/dompdf/dompdf_config.inc.php";
$dompdf=new DOMPDF();
if(isset($pdf_width) && isset($pdf_height)) // mm
{
	$mm=0.35278;
	$dompdf=new DOMPDF();
	$dompdf->set_paper(array(0,0,$pdf_width/$mm,$pdf_height/$mm),"");
}
$dompdf->load_html($pdf_html);
$dompdf->render();
file_put_contents($pdf_filename,$dompdf->output());
?>