<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
if(isset($_SESSION["j_user"]["id"]))
{
	include $SDR."/system/dir.php";
	include $SDR."/system/activity.php";
	$r=1;
	$n=0;
	if(isset($_GET["delete"]))
	{
		$t=0;
		$d=$_GET["delete"];
		if(is_dir($SDR.$d))
		{
			if(J_DeleteDir($d,1))$r=0;
			else $r=2;
		}
		elseif(file_exists($SDR.$d))
		{
			J_SetPerm($d);
			if(@unlink($SDR.$d))
			{
				$r=0;
				J_act("FILE",5,$d);
			}
			else $r=3;
		}
	}
	elseif(isset($_GET["rename"]))
	{
		$t=1;
		$d=$_GET["rename"];
		if(is_file($SDR.$d) || is_dir($SDR.$d))
		{
			$n=$_GET["to"];
			if(is_file($SDR.$d))
			{
				J_SetPerm($d);
				$n=str_replace("..",".",preg_replace('~[^a-zA-Z0-9_\~\-\.]~',"",str_replace(" ","_",$_GET["to"])));
				if($n)
				{
					if(strpos($n,".")!==false)
						$n=substr($n,0,strrpos($n,".")).strtolower(substr($n,strrpos($n,".")));
					elseif(strpos($d,".")!==false)
						$n=substr($d,0,strrpos($d,".")).strtolower(substr($d,strrpos($d,".")));
					$n=(strpos($d,"/")!==false?substr($d,0,strrpos($d,"/")):$d)."/".$n;
					if($d==$n)$r=3;
					elseif(rename($SDR.$d,$SDR.$n))
					{
						$r=0;
						J_act("FILE",10,$d." to ".$n);
					}
				}
			}
			else
			{
				$n=(strpos($d,"/")!==false?substr($d,0,strrpos($d,"/")):$d)."/".$n;
				if($d==$n)$r=3;
				else
				{
					J_SetPermDir($d);
					if(rename($SDR.$d,$SDR.$n))
					{
						$r=0;
						J_act("FILE",10,$d." to ".$n);
					}
				}
			}
		}
	}
	elseif(isset($_GET["folder"]))
	{
		$t=2;
		$d=$_GET["d"];
		if(is_dir($SDR.$d))
		{
			J_SetPerm($d);
			$n=trim(preg_replace('~[^a-zA-Z0-9_]~',"",str_replace(" ","_",str_replace("-","_",$_GET["folder"]))));
			$J_err="";
			J_MakeDir($d,$n);
			if($J_err)echo $J_err;
			else
			{
				$r=0;
				J_act("FILE",3,$d.$n);
			}
		}
	}
	elseif(isset($_GET["move"]))
	{
		$t=3;
		$m=$_GET["move"];
		$to=str_replace(".","",preg_replace('~[^a-zA-Z0-9/_%\~\-]~',"",str_replace(" ","_",$_GET["to"])));
		if(!$to)$j_err=3;
		else
		{
			if((is_file($SDR.$m) || is_dir($SDR.$m)) && is_dir($SDR.$to))
			{
				J_SetPerm($m);
				if(is_file($SDR.$m))
				{
					J_SetPerm($to);
					$f=$m;
					if(strpos($m,"/")!==false)
						$f=substr($m,strrpos($m,"/"));
					$j_err=(rename($SDR.$m,$SDR.$to.$f)?0:2);
				}
				else
				{
					$n="";
					if(strpos($m,"/")!==false)
						$n=substr($m,0,strrpos($m,"/"));
					J_SetPermDir($m);
					$j_err=(rename($SDR.$m,$SDR.str_replace($n,$to,$m))?0:2);
				}
			}
		}
		if(!$j_err)
		{
			$r=0;
			J_act("FILE",11,$m." to ".$to);
		}
	}
	echo "<script>window.parent.JB_return(".$t.",".$r.(!$r&&$n?",\"".$n."\"":"").")</script>";
}
else
	echo "<script>alert('System timed out!');var w=window.parent.parent;w.location=w.location.href</script>";
?>