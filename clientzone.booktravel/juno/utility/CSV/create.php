<?php

$csv_file_name=$EPOCH."_".rand(100,999).".csv";

include_once $SDR."/system/dir.php";
J_PrepFile("/stuff/temp",$csv_file_name,"X");

$w=fopen($SDR."/stuff/temp/".$csv_file_name,"w+");
fwrite($w,$csv_content);
fclose($w);

if(!isset($no_csv_download))
{
	$file=$ROOT."/stuff/temp/".$csv_file_name;
	$csv_name=isset($csv_name)?$csv_name:(isset($_GET["csv_name"])?$_GET["csv_name"]:"");
	$file_name=($csv_name?str_ireplace(".csv","",preg_replace("~[^a-zA-Z0-9_\.]~","",str_replace(" ","-",$csv_name))):"Untitled").".csv";
	include_once $SDR."/utility/force_download.php";
}

?>