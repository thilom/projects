<?php
/**
 * Variables for credit card processing with iVeri
 *
 * @author Thilo Muller(2010)
 * @package RVBUS
 * @package creditcard
 */


$process_vars['aatravel']['groupid'] = '253157';
$process_vars['aatravel']['live_application_id'] = '8277748F-6F89-498E-98EA-5482CCB4518E';
$process_vars['aatravel']['test_application_id'] = 'B4C5CAD4-369F-42D7-B89B-DB595CC2FD1A';

$process_vars['etravel']['groupid'] = '256055';
$process_vars['etravel']['live_application_id'] = '63EDEBFC-BE16-4E64-97FF-B2275C41E4ED';
$process_vars['etravel']['test_application_id'] = 'D6419FE8-A6E0-40CB-88B9-5C1ECE877EFF';


?>
