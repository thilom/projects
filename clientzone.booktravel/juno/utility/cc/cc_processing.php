<?php
/**
 * Handles the credit card processing with iVeri
 * 
 * @author Thilo Muller(2010)
 * @package RVBUS
 * @package creditcard
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
cc_form('aatravel',34);

/**
 * Display the initial form
 * @param string $site 'aatravel' or 'etravel'
 * @param int $test_mode 0=Live, 1=Test
 */
function cc_form($site, $user_id, $test_mode=1) {
    include 'init.php';

    //Vars
    $template = file_get_contents('cc_template.html');
    $application_id = $test_mode==1?$process_vars[$site]['test_application_id']:$process_vars[$site]['live_application_id'];
    $cc_number = $test_mode==1?'4242424242424242':'';
    $cc_ccv = $test_mode==1?'123':'';
    $cc_exp_month = $test_mode==1?'07':'';
    $cc_exp_year = $test_mode==1?'2012':'';

    //Get client details
    $statement = "SELECT firstname, lastname, email FROM nse_user WHERE user_id=? LIMIT 1";
    $sql_user = $GLOBALS['dbCon']->prepare($statement);
    $sql_user->bind_param('i', $user_id);
    $sql_user->execute();
    $sql_user->bind_result($user_firstname, $user_lastname, $user_email);
    $sql_user->fetch();
    $sql_user->free_result();
    $sql_user->close();


    //Replace Vars
    $template = str_replace('!application_id!', $application_id, $template);
    $template = str_replace('!email!', $user_email, $template);
    $template = str_replace('!firstname!', $user_firstname, $template);
    $template = str_replace('!lastname!', $user_lastname, $template);
    $template = str_replace('!cc_number!', $cc_number, $template);
    $template = str_replace('!cc_ccv!', $cc_ccv, $template);
    $template = str_replace('!cc_exp_month!', $cc_exp_month, $template);
    $template = str_replace('!cc_exp_year!', $cc_exp_year, $template);

    echo $template;
}

?>
