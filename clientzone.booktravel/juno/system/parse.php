<?php
if(!function_exists("J_clean"))
{
	function J_clean($v)
	{
		if($v)
		{
			$v=str_replace("  "," ",$v);
			$v=str_replace("{","(",$v);
			$v=str_replace("}",")",$v);
			$v=str_replace("|","",$v);
			$v=str_replace("\v","",$v);
			$v=str_replace("\t","",$v);
			$v=str_replace("\r","",$v);
			$v=str_replace("\"","'",$v);
			$v=str_replace("\n\n","\n",$v);
			$v=trim($v);
		}
		return $v;
	}

function J_parseValue($v="",$c=array())
{
	global $insrt;
	$v=stripslashes($v);
	$v=trim($v);
	if(isset($c["int"])||isset($c["date"])) // int
	{
		$v=strip_tags($v);
		if($v && isset($c["date"]))
			$v=J_dateParse($v,($c["date"]==2?1:($c["date"]==3?2:0)));
		else
		{
			$v=preg_replace('~[^0-9]~s',"",$v)*1;
			if(!is_numeric($v))
				$v=0;
		}
	}
	elseif(isset($c["float"])) // float
	{
		$v=strip_tags($v);
		$v=preg_replace('~[^0-9\.\-]~s',"",$v)*1;
		if($c["float"]>1)
			$v=($v?number_format($v,$c["float"],".",""):0);
		else
			$v=($v?number_format($v,2,".",""):0);
	}
	else
	{
		$v=str_replace("{","(",$v);
		$v=str_replace("}",")",$v);
		$v=str_replace("|","",$v);
		$v=str_replace("\v","",$v);
		$v=str_replace("\t","",$v);
		$v=str_replace("\r","",$v);
		$v=str_replace("\"","'",$v);
		$v=str_replace("  "," ",$v);
		$v=str_replace("\n","<br>",$v);
		$v=str_replace("<br/>","<br>",$v);
		$v=str_replace("<br />","<br>",$v);
		if(isset($c["tagsbr"])) // strip all tags except <br>
			$v=strip_tags($v,"<br>");
		elseif(isset($c["tagsall"])) // strip all tags
		{
			$v=str_replace("\n"," ",$v);
			$v=str_replace("<br>"," ",$v);
			$v=str_replace("<br/>"," ",$v);
			$v=strip_tags($v);
		}
		if(isset($c["basic"])) // basic text + numbers w no specials
			$v=preg_replace('~[^a-zA-Z0-9 \(\)\-\'\.]~s',"",$v);
		elseif(isset($c["keyboard"])) // basic text + numbers + basic specials
			$v=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_+=<>/\\\'\[\]\(\)\-\*\.\$\?]~s',"",$v);
		if(isset($c["caps"])) // cap words
			$v=ucwords(strtolower($v));
		elseif(isset($c["ucfirst"])) // cap first
			$v=ucfirst($v);
		elseif(isset($c["lower"])) // lowercase
			$v=strtolower($v);
		elseif(isset($c["upper"])) // uppercase
			$v=strtoupper($v);
		if(isset($c["length"]))
			$v=substr($v,0,$c["length"]); // length
		$v=trim($v);
		$v=addslashes($v);
	}
	return $v;
}

function J_dateParse($v=0,$bc=0,$fmt=0,$addday=0)
{
	if($v)
	{
		if($bc)
		{
			$v=preg_replace("~[^0-9\-]~s","",$v);
			if(is_numeric($v))
			{
				if($bc==1)
				{
					$v=substr($v,0,8);
					$v=substr($v,4,4).substr($v,2,2).substr($v,0,2)."9999";
				}
				elseif($bc==2)
					$v=substr($v,0,8)."9999";
				$v=strrev($v);
				$i=strrev(substr($v,0,2))*1;
				$h=strrev(substr($v,2,2))*1;
				$d=strrev(substr($v,4,2))*1;
				$m=strrev(substr($v,6,2))*1;
				$y=strrev(substr($v,8))*1;
				$v=($y>-999999999?$y:"2000");
				$v.=($m>0&&$m<13?($m<10?"0":"").$m:99);
				$v.=($d>0&&$d<32?($d<10?"0":"").$d:99);
				$v.=($h>0&&$h<25?($h<10?"0":"").$h:99);
				$v.=($i>0&&$i<61?($i<10?"0":"").$i:99);
			}
			else
				$v=0;
		}
		else
		{
			$v=preg_replace("~[^0-9]~s","",$v);
			if(is_numeric($v))
			{
				$D=substr($v,0,2)*1;
				$M=substr($v,2,2)*1;
				$Y=substr($v,4,4)*1;
				$H=substr($v,8,2)*1;
				$m=substr($v,10,2)*1;
				if($fmt)
					$v=($Y>1999 && $Y<2030?$Y:2000)."-".($M>0 && $M<13?($M<10?"0":"").$M:01)."-".($D>0 && $D<32?($D<10?"0":"").$D:01).($fmt==1?(" ".($H>-1 && $H<25?($H<10?"0":"").$H:00).":".($m>-1 && $m<61?($m<10?"0":"").$m:00).":00"):"");
				else
					$v=mktime(date($H>-1 && $H<25?$H:"H"),date($m>-1 && $m<61?$m:"i"),0,date($M>0 && $M<13?$M:"m"),date($D>0 && $D<32?$D:"d")+$addday,date($Y>1969 && $Y<2030?$Y:"Y"));
			}
		}
	}
	return $v;
}

function J_BCparse($v=0,$f=0)
{
	if($v)
	{
		$b=strpos($v,"-")!==false?"bc":"";
		$v=strrev(str_replace("-","",$v));
		$i=strrev(substr($v,0,2))*1;
		$h=strrev(substr($v,2,2))*1;
		$d=strrev(substr($v,4,2))*1;
		$m=strrev(substr($v,6,2))*1;
		$y=strrev(substr($v,8))*1;
		if($f=="year")
			$v=$y.$b;
		elseif($f==1)
		{
			$v=($b?"-":"").($y?$y:"00")."/";
			$v.=($m>0&&$m<13?($m<10?"0":"").$m."/":"00/");
			$v.=($d>0&&$d<32?($d<10?"0":"").$d:"00");
		}
		else
		{
			$v=($d>0&&$d<32?($d<10?"0":"").$d."/":"");
			$v.=($m>0&&$m<13?($m<10?"0":"").$m."/":"");
			$v.=$y.$b;
			$v.=($h>0&&$h<25?" ".($h<10?"0":"").$h.":".($i>0&&$i<61?($i<10?"0":"").$i:"00"):"");
		}
	}
	return $v;
}

function J_intVal($v=0,$l=0)
{
	if($v)
	{
		$v=preg_replace("~[^0-9\-\.]~s","",$v);
		$v=intval($v);
		$v=is_numeric($v)?$v:0;
		if($l>0)$v=substr(0,$l);
	}
	return $v;
}

function J_floatVal($v=0,$l=0)
{
	if($v)
	{
		$v=preg_replace("~[^0-9\-\.]~s","",$v);
		$v=floatval($v);
		$v=is_numeric($v)?$v:0;
		if($l>0)
			$v=substr(0,$l);
	}
	return $v;
}

}
?>