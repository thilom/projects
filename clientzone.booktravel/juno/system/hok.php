<?php
include_once $SDR."/system/dir.php";
include_once $SDR."/system/get.php";

function strip($v="")
{
	$v=strip_tags($v,"<b><br><img>");
	$v=str_replace("\n","",$v);
	$v=str_replace("\r","",$v);
	$v=str_replace("\t","",$v);
	$v=preg_replace('~[^a-zA-Z0-9 ,:+<>=_£%!@/\$\-\:\&\?\*\.\(\)]~',"",$v);
	return $v;
}

function getImg($s)
{
	global $ROOT;
	$v=preg_replace('~<var>.*?</var>~',"",$s);
	$v=strip(str_replace("//","/",str_replace(array($ROOT,'".$ROOT."'),"",J_Extract($v,'J_icon="<img src=(.*)>";'))));
	return $v;
}

$exc=array();
$exc["/system/hok.php"]=1;
$cur=array();
$cur["/apps/system/access/users.php"]=1;
$sup=(isset($_SESSION["j_user"]["id"]) && J_getSecure()?1:0);
$J_width=0;
$J_height=0;

function J_home($d,$r="")
{
	$v="";
	global $SDR,$ROOT,$EPOCH,$exc,$cur,$sup,$J_width,$J_height;
	$ico_cnt=0;
	$q=array();
	$d="/".trim($d,"/");
	$y="";
	if(is_dir($SDR.$d) && is_readable($SDR.$d))
	{
		$e=opendir($SDR.$d);
		while(($i=readdir($e))!==false)
		{
			if($i!="." && $i!=".." && is_file($SDR.$d."/".$i) && strpos($i,".php")!==false && $i!="home.php")
			{
				$s=file_get_contents($SDR.$d."/".$i);
				if(strpos($s,"J_home=")!==false)
				{
					$alias=str_replace("(.*?)","",strip(J_Extract($s,'J_url="(.*?)";')));
					$u=($alias?$alias:$d."/".$i);
					if(!isset($exc[$u]))
					{
						$h=strip(J_Extract($s,'J_home=(.*?);'));
						$title=str_replace($r,"",strip(J_Extract($s,'J_title1="(.*?)";')));
						$img=getImg($s);
						$tooltip=(strpos($s,"J_tooltip=")!==false?trim(str_replace(array("'","\""),"",strip(J_Extract($s,'J_tooltip="(.*?)";')))):"");
						if($alias && is_file($SDR.$u))
						{
							$s=file_get_contents($SDR.$u);
							$title=$title?$title:str_replace($r,"",strip(J_Extract($s,'J_title1="(.*?)";')));
							$img=$img?$img:getImg($s);
							$tooltip=$tooltip?$tooltip:(strpos($s,"J_tooltip=")!==false?trim(str_replace(array("'","\""),"",strip(J_Extract($s,'J_tooltip="(.*?)";')))):"");
						}
						$a=(strpos($s,"/system/secure.php")===false || (strpos($s,"/system/secure.php")!==false && ($sup || J_getSecure($u)))?1:"");
						if($a)
						{
							$v=$a."~";
							$v.=($a?$u:"")."~";
							$v.=$title."~";
							$v.=$img."~";
							$v.=$a?$tooltip:"";
							$v.="|";
							$q[$h.$title.(rand(0,99))]=$v;
							$ico_cnt++;
						}
					}
				}
			}
		}
		closedir($e);
	}
	$v="";
	ksort($q);
	foreach($q as $k => $u)
		$v.=$u;
	if($ico_cnt<5)$J_width=310;
	elseif($ico_cnt<10)$J_width=430;
	elseif($ico_cnt<17)$J_width=540;
	elseif($ico_cnt<33)$J_width=660;
	elseif($ico_cnt<49)$J_width=780;
	elseif($ico_cnt<64)$J_width=900;
	else $J_width=1020;
	if($ico_cnt<3)$J_height=160;
	elseif($ico_cnt<7)$J_height=260;
	elseif($ico_cnt<17)$J_height=360;
	elseif($ico_cnt<21)$J_height=470;
	elseif($ico_cnt<26)$J_height=580;
	else $J_height=670;
	return $v;
}
?>