<?php
function J_GetPerm($i="")
{
	global $SDR;
	$i=$SDR."/".trim($i,"/");
	if(is_file($i)||is_dir($i))return substr(sprintf("%o",fileperms($i)),-4);
	else return false;
}

$J_err="";

function J_SetPerm($i=0,$m="0777")
{
	global $SDR,$ROOT,$J_err;
	$e="";
	$i="/".trim($i,"/");
	$s=$SDR.$i;
	if(!is_file($s) && !is_dir($s))$e.="set_perm: Item not found ".$s."<br>";
	elseif(J_GetPerm($i)!=$m)
	{
		$m=octdec($m);
		if(function_exists("chmod") && @chmod($s,$m)){}
		else
		{
			global $j_ftp_host,$j_ftp_user,$j_ftp_pass,$j_ftp_root;
			include_once $SDR."/set/ftp.php";
			if(!isset($j_ftp_host) && !$j_ftp_host)$e.="- no host<br>";
			if(!isset($j_ftp_user) && !$j_ftp_user)$e.="- no username<br>";
			if(!isset($j_ftp_pass) && !$j_ftp_pass)$e.="- no password<br>";
			if(!$e)
			{
				if(!$con=ftp_connect($j_ftp_host))$e.="set_perm: Cannot connect<br>";
				elseif(!@ftp_login($con,$j_ftp_user,$j_ftp_pass))$e.="set_perm: Failed login<br>";
				elseif(!@ftp_chmod($con,$m,$j_ftp_root.$ROOT.$i))$e.="set_perm: FTP chmod: ".$root.$i."<br>";
				if($con)ftp_close($con);
			}
		}
	}
	if($e)
	{
		$J_err.=$e;
		return false;
	}
	else return true;
}

function J_SetPermDir($dir="",$m="0777")
{
	global $SDR;
	$dir="/".trim($dir,"/");
	if(is_dir($SDR.$dir))
	{
		if(J_GetPerm($dir)!=$m)
			J_SetPerm($dir);
		if(is_readable($SDR.$dir))
		{
			$d=opendir($SDR.$dir);
			while(($i=readdir($d))!==false)
			{
				if($i=="." || $i==".."){}
				else
				{
					$p=$dir."/".$i;
					if(J_GetPerm($p)!=$m)
					{
						if(is_dir($SDR.$p))J_SetPermDir($p,$m);
						else J_SetPerm($p,$m,1);
					}
				}
			}
			closedir($d);
		}
	}
}

function J_MakeFile($d=0,$i=0,$c=0)
{
	global $SDR,$J_err;
	include $SDR."/set/ftp.php";
	$e="";
	$d="/".trim($d,"/")."/";
	$i=str_replace(" ","_",$i);
	$i=preg_replace('~[^a-zA-Z0-9_&\-\.]~',"",$i);
	$s=$SDR.$d.$i;
	if(is_file($s))
	{
		if(!J_SetPerm($d.$i))
			$e.="create_file: File exists but will not chmod<br>";
	}
	else
	{
		if(!J_SetPerm($d))
			$e.="create_file: Directory will not chmod<br>";
		if(@file_put_contents($s,$c))
			J_SetPerm($d.$i);
		elseif(!isset($j_ftp_root) || !isset($j_ftp_host) || !$con=ftp_connect($j_ftp_host))
		{
			$e.="set_perm: Cannot connect<br>";
			if(!isset($j_ftp_host))$e.="- no host<br>";
			if(!isset($j_ftp_user))$e.="- no username<br>";
			if(!isset($j_ftp_pass))$e.="- no password<br>";
			if(!isset($j_ftp_root))$e.="- no root<br>";
		}
		else
		{
			$o=fopen($SDR."/tmp".$i,"a+");
			if(!ftp_login($con,$j_ftp_user,$j_ftp_pass))
				$e.="create_file: Failed login<br>";
			ftp_fput($con,$j_ftp_root.$d.$i,$o,FTP_ASCII);
			fclose($o);
			@unlink($SDR."/tmp".$i);
			ftp_close($con);
			if(is_file($s))
			{
				J_SetPerm($d.$i);
				$o=fopen($s,"w+");
				fwrite($o,$c);
				fclose($o);
			}
			else $e.="create_file: File was not created<br>";
		}
	}
	if($e)
	{
		$J_err.=$e;
		return false;
	}
	else return true;
}

function J_MakeDir($d=0,$i=0,$m="0777")
{
	global $SDR,$J_err;
	include_once $SDR."/set/ftp.php";
	$e="";
	$d="/".trim($d,"/");
	$i="/".trim($i,"/");
	$s=$SDR.$d.$i;
	if(is_dir($s))
	{
		if(!J_SetPerm($d.$i,$m))
			$e.="create_dir: Dir exists but will not chmod<br>";
	}
	else
	{
		if(!J_SetPerm($d))
			$e.="create_file: Directory will not chmod<br>";
		if(function_exists("mkdir") && mkdir($s,octdec($m)))
			J_SetPerm($d.$i,$m);
		elseif(!isset($j_ftp_root) || !isset($j_ftp_host) || !$con=ftp_connect($j_ftp_host))
		{
			$e.="set_perm: Cannot connect<br>";
			if(!isset($j_ftp_host))$e.="- no host<br>";
			if(!isset($j_ftp_user))$e.="- no username<br>";
			if(!isset($j_ftp_pass))$e.="- no password<br>";
			if(!isset($j_ftp_root))$e.="- no root<br>";
		}
		else
		{
			if(!ftp_login($con,$j_ftp_user,$j_ftp_pass))
				$e.="create_dir: Failed login<br>";
			@ftp_mkdir($con,$j_ftp_root.$d.$i);
			if(is_dir($s))
				J_SetPerm($d.$i,$m);
			else
				$e.="ftp_mkdir: Dir was not created<br>";
			ftp_close($con);
		}
	}
	if($e)
	{
		$J_err.=$e;
		return false;
	}
	else return true;
}

$J_filelist="";
$J_filecount=0;

function J_DirList($dir="",$all=0,$has="",$path=0)
{
	global $SDR,$J_filecount,$J_filelist;
	$J_filelist=array();
	$dir="/".trim($dir,"/");
	if(is_dir($SDR.$dir) && is_readable($SDR.$dir))
	{
		$d=opendir($SDR.$dir);
		while(false!==($i=readdir($d)))
		{
			if($i=="." || $i==".."){}
			else
			{
				if(!$has || ($has && strpos($i,$has)!==false))
				{
					if($all==1)$J_filelist[]=$i;
					elseif($all==2 && is_dir($SDR.$dir."/".$i))$J_filelist[]=$i;
					elseif(!$all && is_file($SDR.$dir."/".$i))
					{
						$J_filelist[]=($path?$dir:"").$i;
						$J_filecount++;
					}
				}
			}
		}
		closedir($d);
	}
}

function J_DirSub($dir,$q1=0,$q2=0,$l=0,$has1=0,$has2=0,$has3=0,$has4=0,$s=0)
{
	if(!$l || ($l && $l>=$s))
	{
		global $SDR,$j_filecount;
		$a=array();
		if(!$s)$j_filecount=0;
		if($q1==1)
		{
			$t=0;
			if(!$s)
			{
				global $j_mem;
				$j_mem=0;
			}
		}
		$c=0;
		if(is_dir($SDR.$dir) && is_readable($SDR.$dir))
		{
			$d=opendir($SDR.$dir);
			while(FALSE!==($i=readdir($d)))
			{
				if($i=="." || $i==".."){}
				else
				{
					$it=$i;
					$p=($q1==2?"/".substr(J_GetPerm($dir."/".$i),1):"");
					$m="";
					if(is_dir($SDR.$dir."/".$i))
					{
						$h=($has4 && strpos($i,$has4)!==false);
						$r=J_DirSub($dir."/".$i,$q1,$q2,($h?1:$l),($h?".php":$has1),($h?0:$has2),($h?0:$has3),($h?0:$has4),($h?1:$s+1));
						if($q1==1)
						{
							$t=$t+$r[0];
							if(!$s)$j_mem=$j_mem+$r[0];
							$m="/".J_Size($r[0]);
						}
						$cc="";
						if($q2)
						{
							$c=$c+$r[1];
							$cc="/".$r[1];
						}
						if($l<$s || !$s || $h)$a["0".$i]="1/".$i.$p.$m.$cc."}".($l?$r[2]:"")."3}";
					}
					elseif
					(
					!$has1 || 
					($has1 && strpos($i,$has1)!==false) || 
					($has2 && strpos($i,$has2)!==false) || 
					($has3 && strpos($i,$has3)!==false)
					)
					{
						if($q1==1)
						{
							$m=filesize($SDR.$dir."/".$i);
							if(!$s)$j_mem=$j_mem+$m;
							$t=$t+$m;
							$m="/".J_Size($m);
						}
						if($l || !$s)$a["9".$i]="/".$i.$p.$m."}";
						$c++;
					}
				}
			}
			closedir($d);
		}
		$d="";
		if(count($a))
		{
			ksort($a);
			foreach($a as $k => $v)$d.=$v;
		}
		if($s)
		{
			$r=array();
			if($q1==1)$r[0]=$t;
			$r[1]=$c;
			if($l)$r[2]=$d;
			return $r;
		}
		else
		{
			$j_filecount=$j_filecount+$c;
			return $d;
		}
	}
}

function J_Size($v=0)
{
	if($v<1000)$v=round($v,0)."b";
	elseif($v<1000000)$v=round(($v/1000),0)."K";
	elseif($v<1000000000)$v=round(($v/1000000),1)."M";
	else $v=round(($v/1000000000),1)."G";
	return $v;
}

function J_DirItem($d,$x,$f=0)
{
	global $SDR;
	$d=$SDR."/".trim($d,"/");
	$y="";
	if(is_dir($d) && is_readable($d))
	{
		$o=opendir($d);
		while(($i=readdir($o))!==false)
		{
			if($i=="." || $i==".."){}
			else
			{
				if($f==2)
				{
					if(strpos($i,$x)!==false)$y=$i;
				}
				elseif($f==1)
				{
					if(is_dir($d."/".$i) && strpos($i,$x)!==false)$y=$i;
				}
				elseif(is_file($d."/".$i) && strpos($i,$x)!==false)$y=$i;
			}
		}
		closedir($o);
	}
	return $y;
}

function J_DeleteDir($dir,$e=0)
{
	global $SDR;
	$dir="/".trim($dir,"/");
	if(!file_exists($SDR.$dir) || !is_dir($SDR.$dir) || !is_readable($SDR.$dir))return false;
	else
	{
		J_SetPerm($dir);
		$d=opendir($SDR.$dir);
		while(($i=readdir($d))!==false)
		{
			if($i=="." || $i==".."){}
			else
			{
				$p=$dir."/".$i;
				if(is_dir($SDR.$p))
					J_DeleteDir($p,1);
				else
				{
					J_SetPerm($p);
					unlink($SDR.$p);
				}
			}
		}
		closedir($d);
		if($e)
			return rmdir($SDR.$dir);
		else
			return false;
	}
}

function J_PrepFile($d,$f="",$c="")
{
	global $SDR;
	$d=explode("/",trim($d,"/"));
	$b="";
	foreach($d as $k => $v) // go thru dirs
	{
		J_SetPerm($b."/".$v);
		if(!is_dir($SDR.$b."/".$v))
			J_MakeDir($b,$v);
		$b.="/".$v;
	}
	if($f) // add file
	{
		if(is_file($SDR.$b."/".$f))
			J_SetPerm($b."/".$f);
		J_MakeFile($b,$f,$c); // will over-write existing
	}
}
?>