<?php
/**
 * Records an activity in Juno.
 *
 * --updated to use prepared statements - Thilo Muller(03-11-2010)
 * --fixed DB error which prevented entry form being added to table - Thilo Muller(03-11-2010)
 * --updated to include establishment code - Thilo Muller(04-11-2010)
 *
 * The action field ($a) should be one of the following:
 * 0 = view
 * 1 = login
 * 2 = search
 * 3 = add
 * 4 = edit
 * 5 = delete
 * 6 = upload
 * 7 = send
 * 8 = execute
 * 9 = logoff
 * 10 = rename
 * 11 = move
 * 12 = approve
 * 13 = cancel
 *
 * @author Jono Darvall(2010);
 * @package RVBUS
 * @category JUNO_GEN
 *
 */

/**
 * Insert a log entry into the DB (nse_activity).
 *
 * @global string $ROOT
 * @global string $URL
 * @global int $EPOCH
 * @param string $n Name of the function eg. EST MANAGER, ASS MANAGER
 * @param int $a Action type. 0=view, 1=login, 2=search, 3=add, 4=edit, 5=delete, 6=upload, 7=send, 8=execute, 9=logoff, 10=rename, 11=move
 * @param string $d Short description of the log entry.
 * @param string $p Reference usualy an ID of an item e.g. assessment_id, invoice_id
 * @param string $e Establishment Code
 */
function J_act($n,$a=0,$d="",$p=0, $e='')
{
	if(isset($_SESSION["j_user"]["id"]))
	{
		$d=trim(substr(preg_replace('~[^a-zA-Z0-9 /:@_\(\)\-\.]~s',"",strip_tags($d)),0,256));
		$n=strtoupper(trim(substr(preg_replace('~[^a-zA-Z0-9 ]~',"",strip_tags($n)),0,16)));
		if(!is_int($a))$a=0;
			global $ROOT,$URL,$EPOCH;
			  $statement = "INSERT INTO nse_activity (j_act_date,j_act_user,j_act_ref,j_act_name,j_act_action,j_act_description,j_act_page,j_act_ip, establishment_code)
									VALUES
									(?,?,?,?,?,?,?,?,?)";
							  
			$sql_insert = $GLOBALS['dbCon']->prepare($statement);
			  $sql_insert->bind_param('sisssssss',$EPOCH, $_SESSION["j_user"]["id"], $p, $n ,$a, $d , $URL , $_SERVER["REMOTE_ADDR"] , $e);
			  $sql_insert->execute();
	}
}
?>