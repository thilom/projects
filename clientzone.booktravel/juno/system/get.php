<?php

if (!function_exists('J_ID')) {

	function J_ID($t, $f, $w="") {
		$i = mysql_fetch_array(mysql_query("SELECT MAX(" . $f . ") AS id FROM " . $t . ($w ? " WHERE " . $w : "")));
		return $i["id"];
	}

}

if (!function_exists('J_Value')) {
	function J_Value($c, $t, $p, $v="", $w="") {
	$o = "";
	if ($v) {
		$s = "";
		$c = explode("~", $c);
		foreach ($c as $k => $q) {
			$q = explode("|", $q);
			$c[$k] = $q;
			$s.=($s ? "," : "") . $q[0];
		}
		if ($t == 'people') {
			$statement = "SELECT firstname,lastname FROM nse_user WHERE user_id=? LIMIT 1";
			$sql_contact = $GLOBALS['dbCon']->prepare($statement);
			$sql_contact->bind_param('i', $v);
			$sql_contact->execute();
			$sql_contact->store_result();
			$sql_contact->bind_result($firstname, $lastname);
			$sql_contact->fetch();
			$sql_contact->free_result();
			$sql_contact->close();
			$o = "$firstname $lastname";
		} elseif ($t == 'establishment') {
			$statement = "SELECT a.establishment_name, c.town_name
											FROM nse_establishment AS a
											LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
											LEFT JOIN nse_nlocations_towns AS c ON b.town_id=c.town_id
											WHERE a.establishment_code=?";
			$sql_location = $GLOBALS['dbCon']->prepare($statement);
			$sql_location->bind_param('s', $v);
			$sql_location->execute();
			$sql_location->bind_result($establishment_name, $town_name);
			$sql_location->fetch();
			$sql_location->free_result();
			$sql_location->close();
			$o = $establishment_name . ($town_name ? ", " . $town_name : "");
		} else {
			$r = mysql_query("SELECT " . $s . " FROM " . $t . " WHERE " . ($w ? $w : $p . "='" . $v . "'") . " LIMIT 1");
			if (mysql_num_rows($r)) {
				$g = mysql_fetch_array($r);
				foreach ($c as $k => $q) {
					$o.=($o ? " " : "");
					if (isset($q[1])) {
						$v = J_Value($q[1], $q[2], $q[3], $g[$q[0]]);
						if ($v && isset($q[4]))
							$v = str_replace("^", $v, $q[4]);
						$o.=$v;
						echo $v;
					}
					else
						$o.=$g[$q[0]];
				}
			}
		}
	}
	return $o;
}
}

if (!function_exists('J_Count')) {
	function J_Count($t, $w="") {
	$r = mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM " . $t . ($w ? " WHERE " . $w : "") . " LIMIT 1"));
	return $r[0];
}
}

if (!function_exists('J_Extract')) {
	function J_Extract($s, $v) {
	$a = "";
	preg_match_all("~" . $v . "~", $s, $m);
	if (count($m[1]))
		$a = $m[1][0];
	return $a;
}
}

if (!function_exists('J_Proportion')) {
	function J_Proportion($x=0, $y=0, $m=200, $n=8) {
	if ($x > $m || $y > $m) {
		if ($x > $y) {
			$y = $y / ($x / $m);
			$y = ($y < $n ? $n : $y);
			$x = $m;
		} else {
			$x = $x / ($y / $m);
			$x = ($x < $n ? $n : $x);
			$y = $m;
		}
	}
	return array(floor($x), floor($y));
}
}

if (!function_exists('J_imgFit')) {
	function J_imgFit($x=0, $y=0, $cx=0, $cy=0) {
	if ($cx) {
		$y = $y / ($x / $cx);
		$x = $cx;
	} else {
		$x = $x / ($y / $cy);
		$y = $cy;
	}
	return array(floor($x), floor($y));
}
}

if (!function_exists('J_Search')) {
	function J_Search($v="", $r="") {
	$s = "";
	if ($v) {
		$v = stripslashes($v) . " ";
		$v = preg_replace("~[^a-zA-Z0-9 !&,;:@_=+%#/<>'\$\*\.\-\(\)\[\]\{\}]~s", "", $v);
		$v = str_replace("  ", " ", $v);
		$v = trim($v);
		$v = substr($v, 0, 64);
		$v = trim($v);
		$v = strtolower($v);
		$v = explode(" ", $v);
		$r = explode("~", trim($r, "~"));
		foreach ($v as $k => $v) {
			$j = "";
			if (!empty($v) && !isset($a[$v])) {
				$a[$v] = $v;
				foreach ($r as $rk => $rv)
					$j.=($j ? " OR " : "") . "lower(" . $rv . ") LIKE '%" . addslashes($v) . "%'";
			}
			if ($j)
				$s.=" AND (" . $j . ")";
		}
	}
	return $s;
}
}

if (!function_exists('J_getSecure')) {
	function J_getSecure($p=0) {
	global $EPOCH;
	return mysql_num_rows(mysql_query("SELECT j_uac_id FROM nse_user_pages WHERE j_uac_user=" . $_SESSION["j_user"]["id"] . " AND j_uac_role='" . $_SESSION["j_user"]["role"] . "' AND j_uac_expiry>" . $EPOCH . " AND (j_uac_page='SuperUser'" . ($p ? " OR j_uac_page LIKE '" . $p . "%'" : "") . ") LIMIT 1"));
}
}

if (!function_exists('J_getCurator')) {
	function J_getCurator() {
	global $EPOCH;
	return (J_getSecure() || mysql_num_rows(mysql_query("SELECT j_ucu_id FROM nse_user_curator WHERE j_ucu_user=" . $_SESSION["j_user"]["id"] . " AND j_ucu_expiry>" . $EPOCH . " LIMIT 1")) ? 1 : 0);
}
}

?>