<?php
function J_css_prep($s)
{
$s=str_replace("  "," ",$s);
$s="\n".$s;
$s=str_replace(" -","-",$s);
$s=str_replace("- ","-",$s);
$s=str_replace("{;","{",$s);
$s=str_replace(" {","{",$s);
$s=str_replace("{","{ ",$s);
$s=str_replace("} ","}",$s);
$s=str_replace(" }","}",$s);
$s=str_replace("}",";}",$s);
$s=str_replace(";}","; }",$s);
$s=str_replace(" (","(",$s);
$s=str_replace("( ","(",$s);
$s=str_replace(" )",")",$s);
$s=str_replace(") ",")",$s);
$s=str_replace(";;",";",$s);
$s=str_replace(";","; ",$s);
$s=str_replace("::",":",$s);
$s=str_replace(": ",":",$s);
$s=str_replace(" :",":",$s);
$s=str_replace(" (","(",$s);
$s=str_replace("( ","(",$s);
$s=str_replace(" )",")",$s);
$s=str_replace(") ",")",$s);
$s=str_replace("\n\n","\n",$s);
$s=str_replace(" \n","\n ",$s);
$s=str_replace("\n","\n ",$s);
$s=str_replace("\t","",$s);
$s=str_replace("\v","",$s);
$s=str_replace("\r","",$s);
return $s;
}

function J_css_clnstr($v="")
{
$v=str_replace("*","\*",$v);
$v=str_replace(".","\.",$v);
$v=str_replace("?","\?",$v);
$v=str_replace("(","\(",$v);
$v=str_replace(")","\)",$v);
return $v;
}

function J_css_extract($s,$e,$a)
{
$z="";
preg_match_all("~\n ".J_css_clnstr($e)."{(.*?)}~i",$s,$m);
if(count($m[1]))
{
$m=$m[1][0];
preg_match_all("~ ".J_css_clnstr($a).":(.*?);~i",$m,$m);
if(count($m[1]))
$z=$m[1][0];
}
return $z;
}
function J_css_insert($s,$e,$a,$v="")
{
$f=J_css_clnstr($e);
$b=J_css_clnstr($a);
preg_match_all("~\n ".$f."{(.*?)}~i",$s,$m);
if(count($m[1]))
{
$m=$m[1][0];
preg_match_all("~ ".$b."\:(.*?);~i",$m,$n);
if(count($n[1]))
{
if(empty($v))
$m=preg_replace("~ ".$b.":.*?;~i","",$m);
else
$m=preg_replace("~ ".$b.":.*?;~i"," ".$a.":".$v.";",$m);
$s=preg_replace("~\n ".$f."{.*?}~i","\n ".$e."{ ".$m."}",$s);
}
elseif(!empty($v))
$s=preg_replace("~\n ".$f."{.*?}~i","\n ".$e."{ ".$m.$a.":".$v."; }",$s);
}
elseif(!empty($v))
$s=$s."\n ".$e."{ ".$a.":".$v."; }";
return $s;
}
function J_css_color($s)
{
$s=str_replace(" ","",$s);
$s=strtoupper($s);
return $s;
}
function J_css_finish($s)
{
$s=str_replace("\n","",$s);
$s=str_replace("\r","",$s);
$s=str_replace("\t","",$s);
$s=str_replace("\v","",$s);
$s=str_replace("  "," ",$s);
$s=str_replace("  "," ",$s);
$s=str_replace("{ ","{",$s);
$s=str_replace(" {","{",$s);
$s=str_replace("{;","{",$s);
$s=str_replace(" }","}",$s);
$s=str_replace("} ","}",$s);
$s=str_replace(";}","}",$s);
$s=str_replace("; ",";",$s);
$s=str_replace(" ;",";",$s);
$s=str_replace(": ",":",$s);
$s=str_replace(" :",":",$s);
$s=str_replace(", ",",",$s);
$s=str_replace(" ,",",",$s);
$s=str_replace(")",") ",$s);
$s=str_replace("{ ","{",$s);
$s=str_replace(") }",")}",$s);
$s=str_replace(") ;",");",$s);
$s=str_replace("}","}\n",$s);
$s=str_replace("}\n*/","}*/",$s);
$s=str_replace("*/ ","*/",$s);
$s=str_replace("*/","*/\n",$s);
$s=trim($s);
return $s;
}
?>