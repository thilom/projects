<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/dir.php";
include_once $SDR."/system/get.php";
if(!function_exists("J_start"))
{
	function J_start($u,$r=0)
	{
		global $SDR;
		$d=$_SESSION["j_user"]["id"];
		if(!file_exists($SDR."/stuff/people/".$d."/set.php"))
			J_PrepFile("stuff/people/".$d,"set.php","<?php\n?>");
		if($o=fopen($SDR."/stuff/people/".$d."/set.php","r+"))
		{
			$c=file_get_contents($SDR."/stuff/people/".$d."/set.php");
			if(strpos($c,"<?php\n")!==false && strpos($c,"?>")!==false)
			{
				$c=str_replace("\r","",$c);
				$c=str_replace("\n\n","\n",$c);
				$s=J_Extract($c,'j_startWin="(.*?)";')."|";
				if($r)
					$s=trim(preg_replace("#".str_replace("?","\?",str_replace(".","\.",$u))."~.*?\|#","",$s),"|");
				else
					$s=$s."|".$u;
				$c=preg_replace('~\$j_startWin=".*?";~i',"",$c);
				$c.="\n\$j_startWin=\"".trim(str_replace("||","|",$s),"|")."\";\n";
				$c=str_replace("?>","",$c)."\n?>";
			}
			else
			{
				$c="<?php\n";
				if(!$r)
					$c.="\$j_startWin=\"".$u."\";\n";
				$c.="?>";
			}
			$o=fopen($SDR."/stuff/people/".$d."/set.php","w+");
			fwrite($o,$c);
			fclose($o);
		}
	}
}
if(isset($_GET["j_set"]) || isset($j_set))
	J_start(isset($_GET["j_start"])?$_GET["j_start"]:$URL,isset($_GET["j_startX"])||isset($j_startX)?1:0);
?>