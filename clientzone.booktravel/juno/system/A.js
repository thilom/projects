var j_days,j_months,j_Wl1=J_cookie_get("jWL")?1:0;
j_G["opt"]=[];
j_G["opt"]["days"]="";
j_G["opt"]["months"]="";
j_days=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
j_months=["-","January","February","March","April","May","June","July","August","September","October","November","December"];
j_G["icoA"]=[8,4,2,1,1,0,-1,-1,-2,-4,-8,4,2,1,1,0,-1,-1,-2,-4,2,1,1,0,-1,-1,-2];

for(i in j_days)
{
	if(i>0)
		j_G["opt"]["days"]+="<option value="+i+">"+j_days[i];
}

for(i in j_months)
	j_G["opt"]["months"]+="<option value="+i+">"+j_months[i];

function J_date(v)
{
	var t=new Date(),d=t.getDate(),m=t.getMonth()+1,y=t.getFullYear(),h=t.getHours(),i=t.getMinutes();
	d=(d<10?"0":"")+d,m=(m<10?"0":"")+m,h=(h<10?"0":"")+h,i=(i<10?"0":"")+i;
	v=v.replace(/Y/g,y).replace(/m/g,m).replace(/M/g,j_months[m*1]).replace(/d/g,d).replace(/H/gi,h).replace(/i/g,i);
	return v;
};

function J_epochParse(v)
{
	return new Date(v*1000).toLocaleString();
};

function J_datePicker(s,d,n,a,v)
{
	J_note(s.j_E,J_datepick(s,v),0,d?d:1,3,-1,0,n?n:3,0,0,0,a?a:0);
};

function J_value(v)
{
	if(j_G["lastObj"])
	{
		if(typeof j_G["lastObj"][0].J_Field=="function")
			j_G["lastObj"][0].J_Field(v);
		else
			j_G["lastObj"][1].value=v;
	}
};

function J_moveUp(t,v)
{
	p=t.parentNode;
	if(v==1&&t.previousSibling)
		p.insertBefore(t,t.previousSibling);
	else if(v==-1&&t.nextSibling&&t.nextSibling.nextSibling)
		p.insertBefore(t,t.nextSibling.nextSibling);
	else if(v==-1&&t==p.lastChild)
		p.insertBefore(t,p.firstChild);
	else if(v)
		p.appendChild(t);
	else
		p.removeChild(t);
};

function J_U(t)
{
	t.className="jf"+(t.className=="jf0"?1:0);
	var n=t.nextSibling;
	n.style.display=(t.className=="jf1"?"block":"");
};

function J_V(v,s)
{
	v=s?s.$(v):$(v);
	v.className=v.className==""?"ji":"";
};

function J_ajax_K(w,f,t,q,s,d,m,p,r,x,y)
{
	var a=j_Wi[w]["F"][f];
	if(a["ajax"]&&a["ajax"][0])
		clearTimeout(a["ajax"][0]);
	if(q==1)
	{
		a=a["ajax"][t];
		J_note(a["obj"],"<img src="+ROOT+"/ico/set/load.gif class=jO50>",0,4,0,-1);
		q=a["query"];
		q=q+(q.indexOf("?")<0?"?":"&")+"d="+t+(q.indexOf("v=")>1?"":"&v="+a["obj"].value.replace(/[^a-zA-Z0-9 ,;:@#!%&_'\$\.\-]/g,"").replace(/&/g,"+++").replace(/  /g," "));
		m=q.match("{");
		if(m&&m.length)
		{
			s=0,y=j_Wi[w]["F"][f]["W"];
			while(m[s])
			{
				q=q.replace(/{(.*?)}/,function(a,b){b=y.$N(b);if(b){if(b[1])b=b[1].value;else b=b[0].value;}else b=0;return b;});
				s++;
			}
		}
		s=j_FF?a["obj"].ownerDocument.defaultView:a["obj"].ownerDocument.parentWindow;
		if(w)
			j_Wi[w]["F"][f]["W"].$("j_IF").src=q;
		else
			$("j_IF").src=q;
		a["close"]=setTimeout("J_ajax_G("+w+","+f+",'"+t+"',-1)",30000);
	}
	else
	{
		if(!a["ajax"]||!a["ajax"]["obj"])
			a["ajax"]=[0];
		a=a["ajax"];
		var o;
		if(t.id&&a[t.id]&&a[t.id]["query"])
		{
			o=t.id;
			clearTimeout(a[o]["close"]);
		}
		else
		{
			if(!t.id)
				t.id="j_K"+Math.floor(Math.random()*10000);
			o=t.id;
			a[o]={"obj":t,"query":ROOT+q,"width":s?s:0,"direction":d>0?d:4,"time":m?m:0,"padding":p?p:0,"wrap":r?1:0,"x":x?x:0,"y":y?y:0,"close":0};
		}
		a[o]["query"]=ROOT+q;
		a[0]=setTimeout("J_ajax_K("+w+","+f+",'"+o+"',1)",1300);
		t=t.nextSibling;
		if(t&&t.tagName=="INPUT")
			t.value="";
	}
};

function J_ajax_C(w,f,t,o)
{
	var a=j_Wi[w]["F"][f];
	if(o||(t.id&&a["ajax"]&&a["ajax"][t.id]))
	{
		if(t.id)
		{
			a["ajax"][t.id]["obj"]=t;
			t=t.id;
		}
		a=a["ajax"][t];
		if(a["obj"]&&a["res"])
			J_note(a["obj"],a["res"],a["width"]<0?a["obj"].clientWidth:a["width"],a["direction"],a["time"],a["padding"],a["wrap"],2,a["x"],a["y"],240);
		else
			J_note0(3);
	}
	else
	{
		if(!a["ajax"])
			a["ajax"]=[[],[0]];
		a=a["ajax"];
		if(!t.id)
			t.id="j_C"+Math.floor(Math.random()*10000);
		a[t.id]={"obj":t};
	}
};

function J_ajax_N(w,f,d,v,n)
{
	if(j_Wi[w]&&j_Wi[w]["F"][f])
	{
		if(n=="vP")
		{
			w=j_Wi[w]["F"][f]["W"];
			if(typeof w.J_ajax_P=="function")
				w.J_ajax_P(d,v,n);
		}
		else
		{
			w=j_Wi[w]["F"][f]["ajax"][d];
			clearTimeout(w["close"]);
			w=w["obj"];
			if(w.nextSibling)
			{
				w.value=n;
				w.nextSibling.value=v;
			}
			else
				w.value=n;
		}
		J_note0(3);
	}
};

function J_ajax_G(w,f,d,v)
{
	var a=j_Wi[w];
	if(a["F"])
	{
		a=a[f]["ajax"][d];
		if(a["obj"])
		{
			clearTimeout(a["close"]);
			J_note0(2);
			if(v<0)
				J_note(a["obj"],"<var>Failed to retrieve data!</var><tt style=font-size:8pt>Slow internet or a disconnect occured. Try again.</tt>",140,4,4);
			else if(v)
			{
				a["res"]=v;
				J_ajax_C(w,f,d,1);
			}
			else
				J_note(a["obj"],"<var>No items found</var>",0,4,4,0,1);
		}
	}
};

function J_ajax_O(w,f,d,v)
{
	var a=j_Wi[w]["F"][f]["ajax"][d];
	if(a["obj"])
	{
		clearTimeout(a["close"]);
		if(v)
		{
			var i=0,u,g=1,n=a["obj"].nextSibling?a["obj"].nextSibling:0,q=J_trim(a["obj"].value.toLowerCase()),z="";
			v=v.split("|");
			while(v[i])
			{
				u=v[i].split("~");
				if(u[0]=="/")
					z+=u[1]?"<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf0>"+u[1]+"</a><div>":"</div>";
				else
				{
					z+="<a href=go: onclick=\"J_ajax_V("+w+","+f+",'"+d+"',this);return false\" onfocus=blur() rel=\""+u[0]+"\" rev=\""+u[1]+"\">"+u[1]+"</a>";
					if(n&&g&&u[1]&&u[1].toLowerCase()==q)
					{
						a["obj"].value=u[1];
						n.value=u[0];
						g=0;
					}
				}
				i++;
			}
			a["res"]="<b class=jA>"+z+"</b>";
			a["padding"]=-1;
			v=v.length-1;
			J_ajax_C(w,f,d,1);
		}
		else
		{
			v=0;
			a["res"]="";
		}
		J_note(v?$T($("jnote2"),"div")[0]:a["obj"],"<var style=font-size:8pt>"+v+" found"+(v==100?"<b>Only 100 at a time<br>Narrow your search</b>":"")+"</var>",0,v?1:4,1,0,1,v?3:2);
	}
};

function J_ajax_V(w,f,d,t)
{
	var o=j_Wi[w]["F"][f]["ajax"][d]["obj"],n=o.nextSibling;
	w=j_Wi[w]["F"][f]["W"];
	o.value=t.rev;
	if(n&&n.tagName=="INPUT")
		n.value=t.rel;
	J_note0(3);
	if(typeof w.J_ajax_Z=="function")
		w.J_ajax_Z(o,d,t?t:0);
};

function J_limit(d,n,l1,l2,f)
{
	setTimeout("J_icoAlert("+d+")",500);
	var a,b,c=0,i=0,o="";
	if(n)
	{
		l2=l2?l2:100;
		while(i<n)
		{
			c++;
			a=(c-1)*l2;
			b=c*l2;
			if(c==21)
				o+="<a href=go: onclick=\"J_V('jlimM');return false\" onmouseover=\"J_TT(this,'Show all')\" onfocus=blur()>&#9658;</a><span id=jlimM class=ji>"
			if(i+l2>=n)
				o+="</span>";
			o+="<a href=go: onfocus=blur()";
			if(l1==i)
				o+=" class=jL_s onclick='return false'";
			else
				o+=" onclick=\""+(f?f+"("+a+","+l2+")":"j_P.J_limit_go(self,"+a+","+l2+")")+";return false\"";
			i=i+l2;
			o+=" onmouseover=\"J_TT(this,'"+a+" - "+(i>=n?n:b)+"')\">"+c+"</a>";
		}
		l2=l1+l2;
		o="<tt id=jL_c><a href=go: onclick='return false' onfocus=blur() class=jL_p>"+l1+"-"+(n<l2?n:l2)+" of "+n+"</a>"+o+"</tt>";
	}
	return o;
};

function J_limit_go(w,a,b)
{
	if(typeof w.J_opaq=="function")
		w.J_opaq();
	w.location=((w.location.href+"&").replace(/&jL1=.*?&/,"&").replace(/&jL2=.*?&/,"&")+"&jL1="+a+"&jL2="+b).replace(/&&/g,"&");
};

function J_checkbox(t)
{
	var s=j_FF?t.ownerDocument.defaultView:t.ownerDocument.parentWindow;
	if(s.j_E&&s.j_E.tagName!="INPUT")
	{
		t=$T(t,"input")[0];
		if(t&&(t.type.toUpperCase()=="CHECKBOX"||t.type.toUpperCase()=="RADIO"))
			t.checked=t.checked?false:true;
	}
};

function J_ucwords(s)
{
	s=s.toLowerCase()+"";
	return s.replace(/^(.)|\s(.)/g,function($1){return $1.toUpperCase()});
};

function J_trim(v)
{
	return v.replace(/^\s\s*/,"").replace(/\s\s*$/,"");
};

function J_sortParse(v)
{
	v=v?v:0;
	return isNaN(v*1)?(v.toLowerCase().replace(/[^0-9a-z]/g,"")+"00000000").substr(0,8):(v*1)+10000000000000000;
};

function J_numSep(v)
{
	if(v)
	{
		v=v+"";
		var r=new RegExp("(-?[0-9]+)([0-9]{3})")
		while(r.test(v))
		  v=v.replace(r,"$1 $2");
	}
	return v;
};