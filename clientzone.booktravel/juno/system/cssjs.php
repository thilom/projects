<?php
if(!function_exists("J_cssjs_0"))
{
	function J_cssjs_0($d,$f=0)
	{
		global $SDR;
		if(is_dir($SDR.$d) && $o=opendir($SDR.$d))
		{
			while(($i=readdir($o))!==false)
			{
				if($i=="." || $i==".."){}
				else
				{
					if(is_dir($SDR.$d."/".$i) && $f<1)J_cssjs_0($d."/".$i,$f+1);
					if(is_file($SDR.$d."/".$i) && (strpos($i,".css")!==false || strpos($i,".js")!==false))
					{
						$u=$i;
						if(strpos($u,"~")!==false)$u=substr($u,strrpos($u,"~")+1);
						$_SESSION["j_cssjs"][strpos($i,".css")!==false?0:1][$d."/".$u]=$d."/".$i;
					}
				}
			}
			closedir($o);
		}
	}
	function J_cssjs_1()
	{
		J_cssjs_0("/apps");
		J_cssjs_0("/developer");
		J_cssjs_0("/pages");
		J_cssjs_0("/style");
		J_cssjs_0("/system");
		J_cssjs_0("/utility");
	}
	function J_cssjs($v,$t=0)
	{
		global $SDR,$ROOT;
		if(isset($_SESSION["j_cssjs"][0][$v]) && is_file($SDR.$_SESSION["j_cssjs"][0][$v]))return $t?$_SESSION["j_cssjs"][0][$v]:"<link rel=stylesheet href=".$ROOT.$_SESSION["j_cssjs"][0][$v].">";
		elseif(isset($_SESSION["j_cssjs"][1][$v]) && is_file($SDR.$_SESSION["j_cssjs"][1][$v]))return $t?$_SESSION["j_cssjs"][1][$v]:"<script src=".$ROOT.$_SESSION["j_cssjs"][1][$v]."></script>";
		else
		{
			J_cssjs_0(substr($v,0,strrpos($v,"/")));
			if(isset($_SESSION["j_cssjs"][0][$v]) && is_file($SDR.$_SESSION["j_cssjs"][0][$v]))return $t?$_SESSION["j_cssjs"][0][$v]:"<link rel=stylesheet href=".$ROOT.$_SESSION["j_cssjs"][0][$v].">";
			elseif(isset($_SESSION["j_cssjs"][1][$v]) && is_file($SDR.$_SESSION["j_cssjs"][1][$v]))return $t?$_SESSION["j_cssjs"][1][$v]:"<script src=".$ROOT.$_SESSION["j_cssjs"][1][$v]."></script>";
			else return $ROOT.$v." missing<br>";
		}
	}
}
if(!isset($_SESSION["j_cssjs"]))
{
	$_SESSION["j_cssjs"]=array();
	J_cssjs_1();
}
?>