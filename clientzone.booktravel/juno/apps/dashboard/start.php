<?php
/**
 * Dashboard
 *
 * Iterates through the '/modules' directory looking for 'dashboard.php'. If found
 * the result of the file is inserted into the dashboard.
 *
 * @author Thilo Muller(2009)
 * @package juno
 * @category feedback
 */

require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";

include $SDR."/apps/message_center/dashboard.php";

$J_title1="Dashboard";
$J_width=320;
$J_height=320;
$J_icon="<img src=".$ROOT."/apps/message_center/i/mail_post_to-64.png>";
$J_tooltip="";
$J_label=21;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>