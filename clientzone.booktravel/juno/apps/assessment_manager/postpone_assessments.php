<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/apps/notes/f/insert.php";


include $SDR."/system/secure.php";
$disp = '';
$establishment_code=isset($_GET["id"])?$_GET["id"]:0;
$eid=isset($_GET["eid"])?$_GET["eid"]:0;
$counter = 0;
$assessment_date = '';
$message_type = '';

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/postpone/postpone.js></script>";
echo "<body>";

//Get assessment Data
$statement = "SELECT establishment_code, invoice_id, DATE_FORMAT(assessment_date,'%d/%m/%Y') FROM nse_establishment_assessment WHERE assessment_id=?";
$sql_data = $GLOBALS['dbCon']->Prepare($statement);
$sql_data->bind_param('i', $GLOBALS['assessment_id']);
$sql_data->execute();
$sql_data->bind_result($establishment_code, $invoice_id, $assessment_date);
$sql_data->fetch();
$sql_data->free_result();
$sql_data->close();

if ($assessment_date == '00/00/0000') $assessment_date = '';

//Get Establishment name
$statement = "SELECT establishment_name, DATE_FORMAT(enter_date, '%m'), DATE_FORMAT(review_date_depricated,'%d/%m/%Y') FROM nse_establishment WHERE establishment_code=? ";
$sql_name = $GLOBALS['dbCon']->Prepare($statement);
$sql_name->bind_param('s', $establishment_code);
$sql_name->execute();
$sql_name->bind_result($establishment_name, $enter_month, $old_review_date);
$sql_name->fetch();
$sql_name->free_result();
$sql_name->close();

if (isset($_POST['save'])) {
    postpone_save();
} else {
    postpone_form();
}

function postpone_form() {
    global $assessment_date, $establishment_code, $enter_month, $old_review_date;
    $counter = 0;
    $message = "";
    $postpane_date = '';



    //Check for postponement
    $statement = "SELECT postpone_date FROM nse_establishment_assessment_postpone WHERE establishment_code=?";
    $sql_date = $GLOBALS['dbCon']->prepare($statement);
    $sql_date->bind_param('s', $establishment_code);
    $sql_date->execute();
    $sql_date->bind_result($postpone_date);
    $sql_date->fetch();
    $sql_date->free_result();
    $sql_date->close();

    if (empty($assessment_date)) {
        $assessment_date = $old_review_date;
    }


    if (empty($postpone_date)) {
        $message = "<b>NOTE: </b>This establishment is currently scheduled for re-assessment on $assessment_date";
    } else {
        list($year, $month, $day) = explode('-',$postpone_date);
        $postpone_tst = mktime(0, 0, 0, $month, $day, $year);

        list($day, $month, $year) = explode('/',$assessment_date);
        $assessment_tst = mktime(0, 0, 0, $month, $day, $year);

        if ($assessment_tst < $postpone_tst) {
            $message = "<b>NOTE: </b>This establishments assessment has previously been pontponed to $postpone_date";
            $assessment_date = $postpone_date;
        } else {
            $message = "<b>NOTE: </b>This establishment is currently scheduled for re-assessment on $assessment_date";
        }
    }


    $template = file_get_contents($GLOBALS['SDR'] . '/apps/assessment_manager/postpone/html/postpone_form.html');
    $template = str_replace('!postpone_date!', $assessment_date, $template);
    $template = str_replace('<!-- message -->', $message, $template);

    echo $template;
}

function postpone_save() {
    global $establishment_code;
    //Vars
    $new_date = $_POST['postpone_date'];
	$postpone_reason = $_POST['postpone_reason'];
    $check = '';

    list($d,$m,$y) = explode('/',$new_date);
    $db_date = "$y-$m-$d";

    //Check if postponement exists
    $statement = "SELECT postpone_date FROM nse_establishment_assessment_postpone WHERE establishment_code=?";
    $sql_check = $GLOBALS['dbCon']->prepare($statement);
    $sql_check->bind_param('s', $establishment_code);
    $sql_check->execute();
    $sql_check->bind_result($check);
    $sql_check->fetch();
    $sql_check->close();

    //Update postpone date
    if (empty($check)) {
        $statement = "INSERT INTO nse_establishment_assessment_postpone (establishment_code, postpone_date) VALUES (?,?)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bind_param('ss', $establishment_code, $db_date);
        $sql_insert->execute();
    } else {
        $statement = "UPDATE nse_establishment_assessment_postpone SET postpone_date=? WHERE establishment_code=?";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bind_param('ss', $db_date, $establishment_code);
        $sql_update->execute();
    }

    //Insert Log
	$note_content = "Assessment Postponed to $db_date. $postpone_reason";
	insertNote($establishment_code, 3, 0, $note_content);

    echo "<div style=text-align:center>Assessment Postponed</div></body></html>";
    echo "<script>";
	 echo "w=j_P.j_Wi[j_W]";
	 echo ";w.['table'].style.width=500";
	 echo ";w.['table'].style.height=100";
    echo ";setTimeout('j_P.J_WX(self)',2000)";
	 echo ";J_icoAlert()";
	 echo "</script>";
	die();
}

echo "<script>J_ass('$eid',$assessment_id)</script>";
$J_title1=$establishment_name;
$J_title2="Postpone Assessment";
$J_title3="Code: $establishment_code";
$J_icon="<img src=".$ROOT."/ico/set/postpone.png>";
$J_nostart=1;
$J_label=23;
$J_height=440;
$J_width=580;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>