<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$assessments = array();
$template = file_get_contents('balance_sheet.html');
$assessor_id = isset($_GET['id'])?$_GET['id']:$_SESSION['j_user']['id'];
$establishment_count = '';
$assessor_name = 'All Assessors';
$waiting_count = 0;
$complete_count = 0;
$draft_count = 0;
$return_count = 0;
$auto_count = 0;
$other_count = 0;
$total_calculated_assessments = 0;
$all_waiting_count = 0;
$all_complete_count = 0;
$all_draft_count = 0;
$all_return_count = 0;
$all_auto_count = 0;
$all_other_count = 0;
$all_total_calculated_assessments = 0;
$total_assessment_count = 0;
$invoice_paid_r = 0;
$invoice_uninvoiced = 0;
$invoice_partial_r = 0;
$invoice_paid = 0;
$invoice_unpaid = 0;
$invoice_unpaid_r = 0;
$invoice_partial = 0;
$invoice_partial_ir = 0;
$invoice_totals = 0;
$invoice_totals_paid = 0;
$due_unknown = 0;
$due_over = 0;
$due_1month = 0;
$due_2month = 0;
$due_3month = 0;
$due_4month = 0;
$due_more = 0;

//Get data
$report_date = date('l d F Y');

//Get assessor name
if ($assessor_id != 'all') {
	$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
	$sql_assessor_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor_name->bind_param('s', $assessor_id);
	$sql_assessor_name->execute();
	$sql_assessor_name->bind_result($assessor_name);
	$sql_assessor_name->fetch();
	$sql_assessor_name->close();
}

//Get establishment count
$statement = "SELECT COUNT(*) FROM nse_establishment ";
$statement .= $assessor_id!='all'?'WHERE assessor_id=?':'WHERE assessor_id!=""';
$sql_estab_count = $GLOBALS['dbCon']->prepare($statement);
if ($assessor_id!='all') $sql_estab_count->bind_param('s', $assessor_id);
$sql_estab_count->execute();
$sql_estab_count->bind_result($establishment_count);
$sql_estab_count->fetch();
$sql_estab_count->close();

//Unbooked assessments
$statement = "SELECT count(*)
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
				WHERE (a.assessment_date='' OR a.assessment_date IS NULL) 
					AND a.assessment_status != 'complete' AND a.assessment_status != 'auto' AND a.assessment_status != 'waiting' ";
$statement .= $assessor_id!='all'?'AND b.assessor_id=?':'AND b.assessor_id!=""';
$sql_unbooked = $GLOBALS['dbCon']->prepare($statement);
if ($assessor_id!='all') $sql_unbooked->bind_param('s', $assessor_id);
$sql_unbooked->execute();
$sql_unbooked->bind_result($unbooked);
$sql_unbooked->fetch();
$sql_unbooked->close();

//Get cancelled count
$cancel_count=0;
$cancelled = array();
$statement = "SELECT a.establishment_code
				FROM nse_establishment_qa_cancelled AS a
				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
				WHERE DATEDIFF(NOW(), a.cancelled_date) < 365 ";
$statement .= $assessor_id!='all'?'AND b.assessor_id=?':'AND b.assessor_id!=""';
$sql_cancelled = $GLOBALS['dbCon']->prepare($statement);
if ($assessor_id!='all') $sql_cancelled->bind_param('s', $assessor_id);
$sql_cancelled->execute();
$sql_cancelled->store_result();
$sql_cancelled->bind_result($cancel_code);
while ($sql_cancelled->fetch()) {
	$cancelled[] = $cancel_code;
	$cancel_count++;
}
$sql_cancelled->close();

//Get Total assessments (12 Months)
$statement = "SELECT DATE_FORMAT(a.assessment_date, '%Y-%c-%e'), a.assessment_id
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
				 ";
$statement .= $assessor_id!='all'?' WHERE b.assessor_id=?':' WHERE b.assessor_id!=""';
$sql_total_assessments = $GLOBALS['dbCon']->prepare($statement);
if ($assessor_id!='all') $sql_total_assessments->bind_param('i', $assessor_id);
$sql_total_assessments->execute();
$sql_total_assessments->store_result();
$sql_total_assessments->bind_result($assessment_date, $assessment_id);
while ($sql_total_assessments->fetch()) {
	if (!empty($assessment_date)) {
		$datetime1 = date($assessment_date);
		$datetime2 = mktime();
		$interval = dateDiff($datetime1, $datetime2);
		$date_diff = $interval[0];
		if ($date_diff < 365) {
			$total_assessment_count++;
			$assessments[] = $assessment_id;
		}
	} else {
		$total_assessment_count++;
		$assessments[] = $assessment_id;
	}
}
$sql_total_assessments->close();

//Assessments
$statement = "SELECT a.assessment_Status, DATE_FORMAT(a.assessment_date, '%Y-%c-%e'), a.renewal_date, b.enter_date
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code";
$statement .= $assessor_id!='all'?' WHERE b.assessor_id=?':' WHERE b.assessor_id!=""';
$sql_status = $GLOBALS['dbCon']->prepare($statement);
if ($assessor_id!='all') $sql_status->bind_param('s', $assessor_id);
$sql_status->execute();
$sql_status->bind_result($assessment_status, $assessment_date, $renewal_date, $enter_date);
while ($sql_status->fetch()) {
	$this_year = FALSE;
	if (!empty($assessment_date)) {
		$datetime1 = date($assessment_date);
		$datetime2 = mktime();
		$interval = dateDiff($datetime1, $datetime2);
		$date_diff = $interval[0];
		if ($date_diff < 365) $this_year = TRUE;
	} else {
		$this_year = TRUE;
	}

	switch($assessment_status) {
		case 'complete':
			$all_complete_count++;
			if ($this_year) $complete_count++;
			break;
		case 'draft':
			$all_draft_count++;
			if ($this_year) $draft_count++;
			break;
		case 'return':
			$all_return_count++;
			if ($this_year) $return_count++;
			break;
		case 'auto':
			$all_auto_count++;
			if ($this_year) $auto_count++;
			break;
		case 'waiting':
			$all_waiting_count++;
			if ($this_year) $waiting_count++;
			break;
		default:
			$all_other_count++;
			if ($this_year) $other_count++;
			break;
	}
	$all_total_calculated_assessments++;
	if ($this_year) $total_calculated_assessments++;
}
$sql_status->close();

//Get invoice Data
$statement = "SELECT j_inv_total, j_inv_paid, invoice_id
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_invoice AS b ON a.invoice_id=b.j_inv_id
				WHERE a.assessment_id=?
				LIMIT 1";
$sql_invoice = $GLOBALS['dbCon']->prepare($statement);
foreach ($assessments AS $assessment_id) {
	$sql_invoice->bind_param('i', $assessment_id);
	$sql_invoice->execute();
	$sql_invoice->bind_result($inv_total, $inv_paid, $invoice_id);
	$sql_invoice->fetch();
	$sql_invoice->free_result();

	if (empty($invoice_id)) {
		$invoice_uninvoiced++;
	} else if ($inv_paid == 0) {
		$invoice_unpaid++;
		$invoice_unpaid_r += $inv_total;
	} else if ($inv_paid == $inv_total) {
		$invoice_paid++;
		$invoice_paid_r += $inv_paid;
	} else if ($inv_paid != $inv_total) {
		$invoice_partial++;
		$invoice_partial_r += $inv_total;
		$invoice_partial_ir += $inv_paid;
	}
}
$invoice_total_count = $invoice_paid + $invoice_partial + $invoice_unpaid + $invoice_uninvoiced;
$invoice_totals_r = $invoice_paid_r + $invoice_unpaid_r + $invoice_partial_r;
$invoice_totals_paid = $invoice_paid_r + $invoice_partial_ir;

//Prepare statement - get last assessment renewal date
$statement = "SELECT renewal_date FROM nse_establishment_assessment WHERE establishment_code=? ORDER BY assessment_date LIMIT 1";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);

//Due dates
$estabs = array();
$statement = "SELECT establishment_code, enter_date, review_date_depricated FROM nse_establishment ";
$statement .= $assessor_id!='all'?' WHERE assessor_id=?':' WHERE assessor_id!=""';
$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
if ($assessor_id!='all') $sql_establishments->bind_param('s', $assessor_id);
$sql_establishments->execute();
$sql_establishments->store_result();
$sql_establishments->bind_result($establishment_code, $enter_date, $review_date_depricated);
while ($sql_establishments->fetch()) {
//	if () {
		$estabs[$establishment_code]['enter_date'] = $enter_date;
		$estabs[$establishment_code]['old_date'] = $review_date_depricated;
//	}
}
$sql_establishments->close();
foreach ($estabs as $establishment_code=>$estab_data) {
	$renewal_date = '';
	$sql_assessments->bind_param('s', $establishment_code);
	$sql_assessments->execute();
	$sql_assessments->bind_result($renewal_date);
	$sql_assessments->fetch();
	$sql_assessments->free_result();
	if (empty($renewal_date)) $renewal_date = $estab_data['old_date'];
	if (empty($renewal_date) && !empty($estab_data['enter_date'])) {
		list($year, $month, $day) = explode('-', $estab_data['enter_date']);
		$current_month = date('m', time());
		$year = $month<$current_month?date('Y',time()):date('Y',time())+1;
		$renewal_date = "$year-$month-01";
	} else if (empty($renewal_date) && empty($estab_data['enter_date'])) {
		$renewal_date = '-2';
	}
	if ($renewal_date == -2) {
		$due_unknown++;
	} else {
		list($year, $month, $day) = explode('-', $renewal_date);
		$current_year = date('Y', time());
		$current_month = date('m', time());

		$month2_month = $current_month+1;
		$month2_year = $current_year;
		if ($month2_month > 12) {
			$month2_month = $month2_month-12;
			$month2_year = $month2_year +1;
		}
		$month2_name = date('F', mktime(0, 0, 0, $month2_month, 1, $current_year));

		$month3_month = $current_month+2;
		$month3_year = $current_year;
		if ($month3_month > 12) {
			$month3_month = $month3_month-12;
			$month3_year = $month3_year +1;
		}
		$month3_name = date('F', mktime(0, 0, 0, $month3_month, 1, $current_year));

		$month4_month = $current_month+3;
		$month4_year = $current_year;
		if ($month4_month > 12) {
			$month4_month = $month4_month-12;
			$month4_year = $month4_year +1;
		}
		$month4_name = date('F', mktime(0, 0, 0, $month4_month, 1, $current_year));


//		echo "$establishment_code :: $renewal_date<br>";

		if (in_array($establishment_code, $cancelled)) {
			
		} else if ($year < $current_year) {
			$due_over++;
		} else if ($year == $current_year && $month < $current_month ) {
			$due_over++;
		} else if ($year == $current_year && $month == $current_month) {
			$due_1month++;
		} else if ($month2_month == $month) {
			$due_2month++;
		} else if ($month3_month == $month) {
			$due_3month++;
		} else if ($month4_month == $month) {
			$due_4month++;
		} else {
			$due_more++;
		}
	}
}
$due_total = $due_unknown + $due_1month + $due_2month + $due_3month + $due_4month + $due_more + $due_over;

//Replace Tags
$template = str_replace('<!-- date -->', $report_date, $template);
$template = str_replace('<!-- establishment_count -->', $establishment_count, $template);
$template = str_replace('<!-- assessor_name -->', $assessor_name, $template);
$template = str_replace('<!-- unbooked_assessments -->', $unbooked, $template);
$template = str_replace('<!-- cancel_count -->', $cancel_count, $template);
$template = str_replace('<!-- total_assessment_count -->', $total_assessment_count, $template);
$template = str_replace('<!-- all_waiting_count -->', $all_waiting_count, $template);
$template = str_replace('<!-- all_complete_count -->', $all_complete_count, $template);
$template = str_replace('<!-- all_draft_count -->', $all_draft_count, $template);
$template = str_replace('<!-- all_other_count -->', $all_other_count, $template);
$template = str_replace('<!-- all_return_count -->', $all_return_count, $template);
$template = str_replace('<!-- all_auto_count -->', $all_auto_count, $template);
$template = str_replace('<!-- all_total_calculated_assessments -->', $all_total_calculated_assessments, $template);
$template = str_replace('<!-- 12_waiting_count -->', $waiting_count, $template);
$template = str_replace('<!-- 12_complete_count -->', $complete_count, $template);
$template = str_replace('<!-- 12_draft_count -->', $draft_count, $template);
$template = str_replace('<!-- 12_other_count -->', $other_count, $template);
$template = str_replace('<!-- 12_return_count -->', $return_count, $template);
$template = str_replace('<!-- 12_auto_count -->', $auto_count, $template);
$template = str_replace('<!-- 12_total_calculated_assessments -->', $total_calculated_assessments, $template);
$template = str_replace('<!-- invoice_paid_count -->', number_format($invoice_paid,0), $template);
$template = str_replace('<!-- invoice_paid_total -->', number_format($invoice_paid_r,0), $template);
$template = str_replace('<!-- invoice_partial_count -->', number_format($invoice_partial,0), $template);
$template = str_replace('<!-- invoice_partial_total -->', number_format($invoice_partial_r,0), $template);
$template = str_replace('<!-- invoice_partial_total_paid -->', number_format($invoice_partial_ir,0), $template);
$template = str_replace('<!-- invoice_unpaid_total -->', number_format($invoice_unpaid_r,0), $template);
$template = str_replace('<!-- invoice_unpaid_count -->', number_format($invoice_unpaid,0), $template);
$template = str_replace('<!-- uninvoice_count -->', number_format($invoice_uninvoiced,0), $template);
$template = str_replace('<!-- invoice_count_total -->', number_format($invoice_total_count,0), $template);
$template = str_replace('<!-- invoice_paid_all_total -->', number_format($invoice_totals_paid,0), $template);
$template = str_replace('<!-- invoice_all_total -->', number_format($invoice_totals_r,0), $template);
$template = str_replace('<!-- due_over -->', $due_over, $template);
$template = str_replace('<!-- due_1month -->', $due_1month, $template);
$template = str_replace('<!-- due_2month -->', $due_2month, $template);
$template = str_replace('<!-- 2month -->', $month2_name, $template);
$template = str_replace('<!-- due_3month -->', $due_3month, $template);
$template = str_replace('<!-- 3month -->', $month3_name, $template);
$template = str_replace('<!-- due_4month -->', $due_4month, $template);
$template = str_replace('<!-- 4month -->', $month4_name, $template);
$template = str_replace('<!-- rest_of_year -->', $due_more, $template);
$template = str_replace('<!-- due_unknown -->', $due_unknown, $template);
$template = str_replace('<!-- due_total -->', $due_total, $template);

echo $template;

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/f_staff/res.js></script>";


function dateDiff($date1, $date2) {
	$date1 = is_int($date1) ? $date1 : strtotime($date1);
        $date2 = is_int($date2) ? $date2 : strtotime($date2);

        if (($date1 !== false) && ($date2 !== false)) {
            if ($date2 >= $date1) {
                $diff = ($date2 - $date1);

                if ($days = intval((floor($diff / 86400))))
                    $diff %= 86400;
                if ($hours = intval((floor($diff / 3600))))
                    $diff %= 3600;
                if ($minutes = intval((floor($diff / 60))))
                    $diff %= 60;

                return array($days, $hours, $minutes, intval($diff));
            }
        }

        return false;
}
?>
