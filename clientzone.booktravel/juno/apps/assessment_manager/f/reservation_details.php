<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

//Initialize vars
$cell1_value = '';
$cell1_description = '';
$cell2_value = '';
$cell2_description = '';
$tel1_value = '';
$tel1_description = '';
$tel2_value = '';
$tel2_description = '';
$fax1_value = '';
$fax1_description = '';
$fax2_value = '';
$fax2_description = '';
$email_value = '';
$email_description = '';
$email2_value = '';
$email2_description = '';
$email3_value = '';
$email3_description = '';
$website_url = '';
$street_address_line1 = '';
$street_address_line2 = '';
$street_address_line3 = '';
$reservation_postal1 = '';
$reservation_postal2 = '';
$reservation_postal3 = '';
$reservation_postal_code = '';
$establishment_code = $_GET["id"];
$no_change = TRUE;
$changed_fields = array();
$estab_fields = array('website_url','street_address_line1', 'street_address_line2', 'street_address_line3');
$contact_fields = array('cell1_value', 'cell1_description', 'cell2_value','cell2_description',
                    'tel1_value','tel1_description','tel2_value','tel2_description',
                    'fax1_value','fax1_description','fax2_value','fax2_description',
                    'email_value','email_description','email2_value','email2_description','email3_value','email3_description');
$res_fields = array('reservation_postal1', 'reservation_postal2', 'reservation_postal3', 'reservation_postal_code');


if (isset($_GET["j_hide"])) {
    //Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (in_array($field_name, $estab_fields) || in_array($field_name, $res_fields) || in_array($field_name, $contact_fields)) $changes .= "$field_name,";
    }
    $sql_updates->free_result();
    $sql_updates->close();

    echo "<html>";
    echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<script src=".$ROOT."/system/P.js></script>";
    echo "<script src=".$ROOT."/apps/establishment_manager/sections/reservation_details.js></script>";
    echo "<script>J_start('" . $establishment_code . "'" . (isset($_GET["view"]) ? ",1" : "") . ")</script>";
    echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
    die();
}

$SUB_ACCESS = array("edit-data"=>0);
include $SDR . "/system/secure.php";
if (isset($_GET["view"]))
    $SUB_ACCESS["edit-data"] = 0;

if (count($_POST)) {
    //Get tel, cell, fax & email details
    $statement = "SELECT contact_type, contact_value, contact_description FROM nse_establishment_public_contact WHERE establishment_code=?";
    $sql_public = $GLOBALS['dbCon']->prepare($statement);
    $sql_public->bind_param('s', $establishment_code);
    $sql_public->execute();
    $sql_public->store_result();
    $sql_public->bind_result($contact_type, $contact_value, $contact_description);
    while ($sql_public->fetch()) {
        switch ($contact_type) {
            case 'cell1':
                $cell1_value = $contact_value;
                $cell1_description = $contact_description;
                break;
            case 'cell2':
                $cell2_value = $contact_value;
                $cell2_description = $contact_description;
                break;
            case 'tel1':
                $tel1_value = $contact_value;
                $tel1_description = $contact_description;
                break;
            case 'tel2':
                $tel2_value = $contact_value;
                $tel2_description = $contact_description;
                break;
            case 'fax1':
                $fax1_value = $contact_value;
                $fax1_description = $contact_description;
                break;
            case 'fax2':
                $fax2_value = $contact_value;
                $fax2_description = $contact_description;
                break;
            case 'email':
                $email_value = $contact_value;
                $email_description = $contact_description;
                break;
            case 'email2':
                $email2_value = $contact_value;
                $email2_description = $contact_description;
                break;
            case 'email3':
                $email3_value = $contact_value;
                $email3_description = $contact_description;
                break;
        }
    }
    $sql_public->free_result();
    $sql_public->close();

    //Get website url & street address
    $statement = "SELECT website_url, street_address_line1, street_address_line2, street_address_line3 FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_website = $GLOBALS['dbCon']->prepare($statement);
    $sql_website->bind_param('s', $establishment_code);
    $sql_website->execute();
    $sql_website->bind_result($website_url, $street_address_line1, $street_address_line2, $street_address_line3);
    $sql_website->fetch();
    $sql_website->free_result();
    $sql_website->close();

    //Get postal address
    $statement = "SELECT reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code FROM nse_establishment_reservation WHERE establishment_code=? LIMIT 1";
    $sql_postal = $GLOBALS['dbCon']->prepare($statement);
    $sql_postal->bind_param('s', $establishment_code);
    $sql_postal->execute();
    $sql_postal->bind_result($reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code);
    $sql_postal->fetch();
    $sql_postal->free_result();
    $sql_postal->close();
    if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only

        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
            $NO_CHANGE = FALSE;
            if (isset($$field_name) && $field_value != $$field_name) {
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated by client", '', $establishment_code);
        }

    } else {
        //Prepare statement - get submitted changes
        $statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert permanent log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
        $sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statment - clear temp log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {

            if (!isset($$field_name)) $$field_name = '';
            if ($field_value != $$field_name) {

                $submitted_by = '';
                $submitted_date = '';
                $submitted_field_value = '';

                //Get client submitted changes
                $sql_submitted_data->bind_param('ss', $establishment_code, $field_name);
                $sql_submitted_data->execute();
                $sql_submitted_data->store_result();
                if ($sql_submitted_data->num_rows == 1) {
                    $sql_submitted_data->bind_result($submitted_by, $submitted_field_value, $submitted_date);
                    $sql_submitted_data->fetch();
                    $sql_submitted_data->free_result();
                }

                //Write to permanent log
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();

                //Clear client log
                $sql_clear_log->bind_param('ss', $establishment_code, $field_name);
                $sql_clear_log->execute();

                $changed_fields[$field_name] = $field_value;
            }
        }
        $sql_clear_log->close();
        $sql_log_insert->close();
        $sql_submitted_data->close();

        //Check if reservation entry exists
        $count = 0;
        $statement = "SELECT COUNT(*) FROM nse_establishment_reservation WHERE establishment_code=?";
        $sql_reservation_check = $GLOBALS['dbCon']->prepare($statement);
        $sql_reservation_check->bind_param('s', $establishment_code);
        $sql_reservation_check->execute();
        $sql_reservation_check->bind_result($count);
        $sql_reservation_check->fetch();
        $sql_reservation_check->free_result();
        $sql_reservation_check->close();

        //Insert entry if it doesn't exist
        if ($count == 0) {
            $statement = "INSERT INTO nse_establishment_reservation (establishment_code) VALUES (?)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bind_param('s', $establishment_code);
            $sql_insert->execute();
            $sql_insert->close();
        }

        //Update DB

        if (!empty($changed_fields)) {

            //Prepare statement - check for res field
            $statement = "SELECT COUNT(*) FROM nse_establishment_public_contact WHERE establishment_code=? && contact_type=?";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);


            foreach ($changed_fields as $field_name => $field_value) {
                $no_change = FALSE;
                if (in_array($field_name, $estab_fields)) {
                    $statement = "UPDATE nse_establishment SET $field_name=? WHERE establishment_code=?";
                    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
                    $sql_estab->bind_param('ss',  $field_value, $establishment_code);
                    $sql_estab->execute();
                    $sql_estab->close();
                }

                if (in_array($field_name, $contact_fields)) {
                    list($contact_type, $value_type) = explode('_', $field_name);
                    $eCount = 0;
                    $sql_check->bind_param('ss', $establishment_code, $contact_type);
                    $sql_check->execute();
                    $sql_check->bind_result($eCount);
                    $sql_check->fetch();
                    $sql_check->free_result();

                    if ($eCount == 0) {
                        $statement = "INSERT INTO nse_establishment_public_contact (establishment_code, contact_type, ". ($value_type=='value'?'contact_value':'contact_description') .") VALUES (?,?,?)";
                        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
                        $sql_insert->bind_param('sss', $establishment_code, $contact_type, $field_value);
                        $sql_insert->execute();
                        $sql_insert->close();
                    } else {
                        $statement = "UPDATE nse_establishment_public_contact SET ". (($value_type=='value'?'contact_value':'contact_description')) ."=? WHERE establishment_code=? && contact_type=?";
                        $sql_update = $GLOBALS['dbCon']->prepare($statement);
                        $sql_update->bind_param('sss', $field_value, $establishment_code, $contact_type);
                        $sql_update->execute();
                        $sql_update->close();
                    }
                }

                if (in_array($field_name, $res_fields)) {
                    $statement = "UPDATE nse_establishment_reservation SET $field_name=? WHERE establishment_code=?";
                    $sql_reservation = $GLOBALS['dbCon']->prepare($statement);
                    $sql_reservation->bind_param('ss',  $field_value, $establishment_code);
                    $sql_reservation->execute();
                    $sql_reservation->close();
                }
            }
			$note_content = "Establishment Data updated (Reservations Tab)";
			insertNote($establishment_code, 4, 0, $note_content);

        }
		 unset($_SESSION["juno"]["accounts"]["company"][$establishment_code]);
    }

    if (!$no_change) {
        //Check if log entry exists for today
        $log_count = 0;
        $day = date('d');
        $month = date('m');
        $year = date('y');
        $start_date = mktime(0, 0, 0, $month, $day, $year);
        $end_date = mktime(23, 59, 59, $month, $day, $year);
        $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated or approved'";
        $sql_check = $GLOBALS['dbCon']->prepare($statement);
        $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
        $sql_check->execute();
        $sql_check->bind_result($log_count);
        $sql_check->fetch();
        $sql_check->free_result();
        $sql_check->close();

		//Clear out empty fields
		$statement = "DELETE FROM nse_establishment_public_contact WHERE contact_value=''";
		$sql_clear = $GLOBALS['dbCon']->prepare($statement);
		$sql_clear->execute();
		$sql_clear->close();

        if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated or approved", '', $establishment_code);
		timestamp_establishment($establishment_code);
    }
}


//Check if new version exists
$res_count = 0;
$statement = "SELECT COUNT(*) FROM nse_establishment_public_contact WHERE establishment_code=?";
$sql_res_check = $GLOBALS['dbCon']->prepare($statement);
$sql_res_check->bind_param('s', $establishment_code);
$sql_res_check->execute();
$sql_res_check->bind_result($res_count);
$sql_res_check->fetch();
$sql_res_check->close();

if ($res_count > 0) {

	//Get tel, cell, fax & email details
	$statement = "SELECT contact_type, contact_value, contact_description FROM nse_establishment_public_contact WHERE establishment_code=?";
	$sql_public = $GLOBALS['dbCon']->prepare($statement);
	$sql_public->bind_param('s', $establishment_code);
	$sql_public->execute();
	$sql_public->store_result();
	$sql_public->bind_result($contact_type, $contact_value, $contact_description);
	while ($sql_public->fetch()) {
		switch ($contact_type) {
			case 'cell1':
				$cell1_value = $contact_value;
				$cell1_description = $contact_description;
				break;
			case 'cell2':
				$cell2_value = $contact_value;
				$cell2_description = $contact_description;
				break;
			case 'tel1':
				$tel1_value = $contact_value;
				$tel1_description = $contact_description;
				break;
			case 'tel2':
				$tel2_value = $contact_value;
				$tel2_description = $contact_description;
				break;
			case 'fax1':
				$fax1_value = $contact_value;
				$fax1_description = $contact_description;
				break;
			case 'fax2':
				$fax2_value = $contact_value;
				$fax2_description = $contact_description;
				break;
			case 'email':
				$email_value = $contact_value;
				$email_description = $contact_description;
				break;
			case 'email2':
				$email2_value = $contact_value;
				$email2_description = $contact_description;
				break;
			case 'email3':
				$email3_value = $contact_value;
				$email3_description = $contact_description;
				break;
		}
	}
	$sql_public->free_result();
	$sql_public->close();
} else {
	$statement = "SELECT reservation_tel, reservation_fax, reservation_cell, reservation_email FROM nse_establishment_reservation WHERE establishment_code=?";
	$sql_reservation2 = $GLOBALS['dbCon']->prepare($statement);
	$sql_reservation2->bind_param('s', $establishment_code);
	$sql_reservation2->execute();
	$sql_reservation2->store_result();
	$sql_reservation2->bind_result($tel1_value, $fax1_value, $cell1_value, $email_value);
	$sql_reservation2->fetch();
	$sql_reservation2->free_result();

	if (empty($tel1_value) && empty($fax1_value) && empty($cell1_value)) {
		$statement = "SELECT contact_tel, contact_cell, contact_fax FROM nse_establishment_contact WHERE establishment_code=? LIMIT 1";
		$sql_contact = $GLOBALS['dbCon']->prepare($statement);
		$sql_contact->bind_param('s', $establishment_code);
		$sql_contact->execute();
		$sql_contact->bind_result($tel1_value, $cell1_value, $fax1_value);
		$sql_contact->fetch();
		$sql_contact->close();
	}
}

//Get website url & street address
$statement = "SELECT website_url, street_address_line1, street_address_line2, street_address_line3 FROM nse_establishment WHERE establishment_code=? LIMIT 1";
$sql_website = $GLOBALS['dbCon']->prepare($statement);
$sql_website->bind_param('s', $establishment_code);
$sql_website->execute();
$sql_website->bind_result($website_url, $street_address_line1, $street_address_line2, $street_address_line3);
$sql_website->fetch();
$sql_website->free_result();
$sql_website->close();

//Get postal address
$statement = "SELECT reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code FROM nse_establishment_reservation WHERE establishment_code=? LIMIT 1";
$sql_postal = $GLOBALS['dbCon']->prepare($statement);
$sql_postal->bind_param('s', $establishment_code);
$sql_postal->execute();
$sql_postal->bind_result($reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code);
$sql_postal->fetch();
$sql_postal->free_result();
$sql_postal->close();

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    $$field_name = $field_value;
    if (in_array($field_name, $estab_fields) || in_array($field_name, $res_fields) || in_array($field_name, $contact_fields)) $changes .= "$field_name,";
}
$sql_updates->free_result();
$sql_updates->close();

//HTML Start
echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/reservation_details.js></script>";
echo "<script>E_s(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ")</script>";
echo "<script>changes='$changes'</script>";
if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

//Tel
if ($SUB_ACCESS["edit-data"] && $_SESSION['j_user']['role'] != 'c') {
    echo "<tr><th rowspan=2>Telephone</th><td><input type=text name=tel1_value value='$tel1_value' onclick=j_P.J_telFormat(this) class=jW100></td><td><input type=text name=tel1_description value='$tel1_description' class=jW100></td></tr>";
    echo "<tr><td><input type=text name=tel2_value value='$tel2_value' onclick=j_P.J_telFormat(this) class=jW100></td><td><input type=text name=tel2_description value='$tel2_description' class=jW100></td></tr>";
    echo "<tr><th colspan=9>&nbsp;</th></tr>";
} else {
    echo "<tr><th>Telephone</th><td>" . (!empty($tel1_value) ? $tel1_value : "") . (!empty($tel1_value) && !empty($tel1_description) ? " </td><td> " . $tel1_description : "</td><td>");
    if (!empty($tel2_value)) {
        echo "<tr><th></th><td >" . (!empty($tel2_value) ? $tel2_value : "") . (!empty($tel2_value) && !empty($tel2_description) ? " </td><td> " . $tel2_description : "</td><td>") . "<br>&nbsp;</td></tr>";
	} else {
		echo "<br>&nbsp;</td></tr>";
	}
}


//Cell
if ($SUB_ACCESS["edit-data"]  && $_SESSION['j_user']['role'] != 'c') {
    echo "<tr><th rowspan=2>Cell</th><td><input type=text name=cell1_value value='$cell1_value' onclick=j_P.J_telFormat(this) class=jW100></td><td><input type=text name=cell1_description value='$cell1_description' class=jW100></td></tr>";
    echo "<tr><td><input type=text name=cell2_value value='$cell2_value' onclick=j_P.J_telFormat(this) class=jW100></td><td><input type=text name=cell2_description value='$cell2_description' class=jW100></td></tr>";
    echo "<tr><th colspan=9>&nbsp;</th></tr>";
} else {
    echo "<tr><th>Cell</th><td>" . (!empty($cell1_value) ? $cell1_value : "") . (!empty($cell1_value) ? " </td><td> " . $cell1_description : "");
    if (!empty($cell2_value)) {
        echo "<tr><th></th><td>" . (!empty($cell2_value) ? $cell2_value : "") . (!empty($cell2_value) && $cell2_description ? "</td><td>" . $cell2_description : "") . "<br>&nbsp;</td></tr>";
	} else {
		echo "<br>&nbsp;</td></tr>";
	}
}


//Fax
if ($SUB_ACCESS["edit-data"] && $_SESSION['j_user']['role'] != 'c') {
    echo "<tr><th rowspan=2>Fax</th><td><input type=text name=fax1_value value='$fax1_value' onclick=j_P.J_telFormat(this) class=jW100></td><td><input type=text name=fax1_description value='$fax1_description' class=jW100></td></tr>";
    echo "<tr><td><input type=text name=fax2_value value='$fax2_value' onclick=j_P.J_telFormat(this) class=jW100></td><td><input type=text name=fax2_description value='$fax2_description' class=jW100></td></tr>";
    echo "<tr><th colspan=9>&nbsp;</th></tr>";
} elseif (!empty($fax1_value)) {
    echo "<tr><th>Fax</th><td>" . (!empty($fax1_value) ? $fax1_value : "") . (!empty($fax1_value) && !empty($fax1_description) ? "</td><td>" . $fax1_description : "</td><td>");
    if (!empty($fax2_value)) {
        echo "<tr><th></th><td>" . (!empty($fax2_value) ? $fax2_value : "") . (!empty($fax2_value) && !empty ($fax2_description) ? "</td><td>" . $fax2_description : "</td><td>") . "<br>&nbsp;</td></tr>";
	} else {
		echo "<br>&nbsp;</td></tr>";
	}
}


//Email
if ($SUB_ACCESS["edit-data"] && $_SESSION['j_user']['role'] != 'c') {
    echo "<tr><th rowspan=3>Email</th><td><input type=text name=email_value value='$email_value' class=jW100></td><td><input type=text name=email_description value='$email_description' class=jW100></td></tr>";
    echo "<tr><td><input type=text name=email2_value value='$email2_value' class=jW100></td><td><input type=text name=email2_description value='$email2_description' class=jW100></td></tr>";
    echo "<tr><td><input type=text name=email3_value value='$email3_value' class=jW100></td><td><input type=text name=email3_description value='$email3_description' class=jW100></td></tr>";
    echo "<tr><th colspan=9>&nbsp;</th></tr>";
} else {
    echo "<tr><th>Email</th><td>" . (!empty($email_value) ? $email_value : "") . (!empty($email_value) && !empty($email_description) ? "</td><td>" . $email_description : "</td><td>");
    if (!empty($email2_value)) {
        echo "<tr><th></th><td>" . (!empty($email2_value) ? $email2_value : "") . (!empty($email2_value) && !empty($email2_description) ? "</td><td>" . $email2_description : "</td><td>");
		if (!empty($email3_value)) {

			echo "<tr><th></th><td>" . (!empty($email3_value) ? $email3_value : "") . (!empty($email3_value) && !empty($email3_description) ? "</td><td>" . $email3_description : "</td><td>") . "<br>&nbsp;</td></tr>";
		} else {
			echo "<br>&nbsp;</td></tr>";
		}
	} else {
		echo "<br>&nbsp;</td></tr>";
	}
}

//Website
if ($SUB_ACCESS["edit-data"]) {
    echo "<tr><th>Website</th><td colspan=2><input type=text name=website_url value='$website_url' class=jW100></td></tr>";
} elseif (!empty($website_url)) {
    echo "<tr><th>Website</th><td >$website_url</td></tr>";
}

//Postal Address
echo "<tr><th colspan=9>&nbsp;</th></tr>";
echo "<tr class=hed><th colspan=9>Postal Address for Enquiries</th></tr>";

echo "<tr><th>&nbsp;</th>" . ($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c' ? "<td colspan=2><input type=text name=reservation_postal1 value='$reservation_postal1' class=jW100>" : "<td colspan=2>$reservation_postal1") . "</td></tr>";
echo "<tr><th>&nbsp;</th>" . ($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c' ? "<td colspan=2><input type=text name=reservation_postal2 value='$reservation_postal2' class=jW100>" : "<td colspan=2>$reservation_postal2") . "</td></tr>";
echo "<tr><th>&nbsp;</th>" . ($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c' ? "<td colspan=2><input type=text name=reservation_postal3 value='$reservation_postal3' class=jW100>" : "<td colspan=2>$reservation_postal3") . "</td></tr>";
echo "<tr><th>Code</th>" . ($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c' ? "<td colspan=2><input type=text name=reservation_postal_code value='$reservation_postal_code' class=jW100>" : "<td colspan=2>$reservation_postal_code") . "</td></tr>";

//Physical address
echo "<tr><th colspan=9>&nbsp;</th></tr>";
echo "<tr class=hed><th colspan=9>Physical/Street Address</th></tr>";

echo "<tr><th>&nbsp;</th>" . ($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c' ? "<td colspan=2><input type=text name=street_address_line1 value='$street_address_line1' class=jW100>" : "<td colspan=2>$street_address_line1") . "</td></tr>";
echo "<tr><th>&nbsp;</th>" . ($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c' ? "<td colspan=2><input type=text name=street_address_line2 value='$street_address_line2' class=jW100>" : "<td colspan=2>$street_address_line2") . "</td></tr>";
echo "<tr><th>&nbsp;</th>" . ($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c' ? "<td colspan=2><input type=text name=street_address_line3 value='$street_address_line3' class=jW100>" : "<td colspan=2>$street_address_line3") . "</td></tr>";

echo "<script>E_d(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ")</script>";

if ($SUB_ACCESS["edit-data"])
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/saver.js></script>";

if (!isset($_GET["j_IF"])) {
    $J_title2 = "Establishment Manager";
    $J_title1 = "Reservation Details";
    $J_width = 480;
    $J_height = 480;
    $J_icon = "<img src=" . $ROOT . "/ico/set/gohome-edit.png>";
    $J_tooltip = "";
    $J_label = 22;
    $J_nostart = 1;
    $J_nomax = 1;
    include $SDR . "/system/deploy.php";
}

echo "</body></html>";
?>