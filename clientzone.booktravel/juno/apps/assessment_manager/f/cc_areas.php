<?php
/**
 * Draw the assessment areas for editing (Caravan & Camping)
 * 
 * @author Thilo Muller(2010,2011)
 * @version $Id$
 */


include_once $SDR."/system/activity.php";

//Vars
$nLine = '';
$rating_list = '<option value=" "> ';
$ratings = array(1=>'Excellent','Very Good','Good', 'Fair','Poor');

//Prepare statement - Area Data
$statement = "SELECT assessment_area_id, assessment_area_name FROM nse_assessment_areas WHERE area_text_id=?";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Get area Data
$statement = "SELECT assessment_content, assessment_rating, not_applicable, hide_from_client FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
$sql_content = $GLOBALS['dbCon']->prepare($statement);

//Get establishment code
$statement = "SELECT establishment_code FROM nse_establishment_assessment WHERE assessment_id=?";
$sql_establishment = $GLOBALS['dbCon']->prepare($statement);
$sql_establishment->bind_param('i', $assessment_id);
$sql_establishment->execute();
$sql_establishment->bind_result($establishment_code);
$sql_establishment->fetch();
$sql_establishment->free_result();
$sql_establishment->close();

//Are there a previous assessment
$statement = "SELECT COUNT(*) as counter FROM nse_establishment_assessment WHERE establishment_code=? && assessment_type_2='cc'";
$sql_past_assessments = $GLOBALS['dbCon']->prepare($statement);
$sql_past_assessments->bind_param('s', $establishment_code);
$sql_past_assessments->execute();
$sql_past_assessments->bind_result($assessments_count);
$sql_past_assessments->fetch();
$sql_past_assessments->close();


if (count($_POST) > 0) {
	
    //Prepare statement - Insert data
    $statement = "INSERT INTO nse_establishment_assessment_data (assessment_id, assessment_area_id, assessment_content, assessment_rating, not_applicable, hide_from_client) VALUES (?,?,?,?,?,?)";
    $sql_insert = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Update data
    $statement = "UPDATE nse_establishment_assessment_data SET assessment_content=?, assessment_rating=?, not_applicable=?, hide_from_client=? WHERE assessment_id=? AND assessment_area_id=?";
    $sql_update = $GLOBALS['dbCon']->prepare($statement);

    foreach ($areas as $area_text_id) {

        //Get area ID
        $sql_data->bind_param('s', $area_text_id);
        $sql_data->execute();
        $sql_data->bind_result($area_id, $area_name);
        $sql_data->fetch();
        $sql_data->free_result();

        //Check if entry already exists
        $sql_content->bind_param('ii', $assessment_id, $area_id);
        $sql_content->execute();
        $sql_content->store_result();
        $content_count = $sql_content->num_rows;
        $sql_content->free_result();

        //Vars
        $content = $_POST["{$area_text_id}_text"];
        $na = isset($_POST["{$area_text_id}_na"])?'1':'';
        $hide = isset($_POST["{$area_text_id}_hide"])?'1':'';
        $rate = $_POST["{$area_text_id}_rate"];

        if ($content_count == 0) {
            $sql_insert->bind_param('iissss', $assessment_id, $area_id, $content, $rate, $na, $hide);
            $sql_insert->execute();
        } else {
            $sql_update->bind_param('ssssii', $content, $rate, $na, $hide, $assessment_id, $area_id);
            $sql_update->execute();
        }
    }

    //Check if log entry exists for today
    $log_count = 0;
    $day = date('d');
    $month = date('m');
    $year = date('y');
    $start_date = mktime(0, 0, 0, $month, $day, $year);
    $end_date = mktime(23, 59, 59, $month, $day, $year);
    $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && j_act_ref=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Assessment saved'";
    $sql_check = $GLOBALS['dbCon']->prepare($statement);
    $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $assessment_id, $start_date, $end_date);
    $sql_check->execute();
    $sql_check->bind_result($log_count);
    $sql_check->fetch();
    $sql_check->free_result();
    $sql_check->close();

    if ($log_count == 0) J_act('ASS MANAGER', 4, "Assessment saved", $assessment_id, $establishment_code);

}

$template = str_replace(array("/r/n","/r","/n"), '#$%', $template);
$template = str_replace(array("\r\n","\r","\n"), '#$%', $template);
preg_match_all('@<!-- item_start -->(.)*<!-- item_end -->@i', $template, $matches);
$lTemplate = $matches[0][0];

//Prepare statement - Get area Data
$statement = "SELECT assessment_content, assessment_rating, not_applicable, hide_from_client FROM nse_establishment_assessment_data WHERE assessment_id=? && assessment_area_id=?";
$sql_content2 = $GLOBALS['dbCon']->prepare($statement);

foreach ($areas as $area_text_id) {
    $area_content = '';
    $assessment_rating = '';
    $not_applicable = '';
    $hide_from_client = '';
    $rate_static = '';

    $sql_data->bind_param('s', $area_text_id);
    $sql_data->execute();
    $sql_data->bind_result($area_id, $area_name);
    $sql_data->fetch();
    $sql_data->free_result();

    $sql_content2->bind_param('ii', $assessment_id, $area_id);
    $sql_content2->execute();
    $sql_content2->bind_result($area_content, $assessment_rating, $not_applicable, $hide_from_client);
    $sql_content2->fetch();
    $sql_content2->free_result();

    $rating_list = "<option value='' > ";
    foreach ($ratings as $rate_id=>$rate_name) {
        if ($assessment_rating == $rate_id) {
            $rating_list .= "<option value=$rate_id selected >$rate_name";
            $rate_static = $rate_name;
        } else {
            $rating_list .= "<option value=$rate_id >$rate_name";
        }
    }

    $na = $not_applicable=='1'?'checked':'';
    $na_static = $not_applicable=='1'?'Not Applicable':'';
    $hide = $hide_from_client=='1'?'checked':'';
    $hide_static = $hide_from_client=='1'&&$not_applicable!='1'?'Hidden From Client':'';
    if (empty($area_content)) $area_content = '&nbsp;';

	//Import button
	$import_button = $assessments_count>1?"<td style='text-align: right; width: 80px'><input type='button' value='Import' title='Import from previous assessment' onClick=\"import_data('$establishment_code','$area_text_id','$area_name')\"></td>":"";
	
    $line = str_replace('<!-- heading -->', $area_name, $lTemplate);
    $line = str_replace('!id!', $area_text_id, $line);
    $line = str_replace('<!-- rating -->', $rating_list, $line);
    $line = str_replace('<!-- text -->', $area_content, $line);
    $line = str_replace('!na!', $na, $line);
    $line = str_replace('!hide!', $hide, $line);
    $line = str_replace('<!-- na_static -->', $na_static, $line);
    $line = str_replace('<!-- hide_static -->', $hide_static, $line);
    $line = str_replace('<!-- rate_static -->', $rate_static, $line);
    $line = str_replace('<!-- import_button -->', $import_button, $line);

    $nLine .= $line;
}


//Replace tags
$template = preg_replace('@<!-- item_start -->(.)*<!-- item_end -->@i', $nLine, $template);
$template = str_replace('#$%', "\r\n", $template);

echo $template;

?>