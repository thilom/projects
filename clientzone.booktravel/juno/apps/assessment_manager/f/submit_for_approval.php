<?php

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include_once $SDR."/system/activity.php";

//Vars
$assessment_id = $_GET['id'];

//Get establishment code
$statement = "SELECT establishment_code FROM nse_establishment_assessment WHERE assessment_id=? LIMIT 1";
$sql_establishment = $GLOBALS['dbCon']->prepare($statement);
$sql_establishment->bind_param('i', $assessment_id);
$sql_establishment->execute();
$sql_establishment->bind_result($establishment_code);
$sql_establishment->fetch();
$sql_establishment->close();


if (isset($_GET['submit'])) {
	$statement = "UPDATE nse_establishment_assessment SET assessment_status='waiting', submitted_by=?, submitted_date=NOW() WHERE assessment_id=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bind_param('ii', $_SESSION['j_user']['id'], $assessment_id);
	$sql_update->execute();
	$sql_update->close();

	J_act('ASS MANAGER', 4, "Assessment submitted for approval", $assessment_id, $establishment_code);
	echo "<div style='width=600px; padding: 10px; background-color: silver; border: 1px dotted black; text-align: center'>Assessment Submitted for Approval<br>
				<div style='text-align: right; width: 100%'>";
//	echo "<input type='button' value='Close' onClick='parent.WD({})'>";
	echo "</div></div>";
	
} else {
	
	//Ceck for date
	$statement = "SELECT DATE_FORMAT(a.assessment_date,'%e %M %Y'), b.establishment_name 
					FROM nse_establishment_assessment AS a
					JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE assessment_id=? 
					LIMIT 1";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	$sql_data->bind_param('i', $_GET['id']);
	$sql_data->execute();
	$sql_data->bind_result($assessment_date, $establishment_name);
	$sql_data->fetch();
	$sql_data->close();
	
	if (empty($assessment_date) || $assessment_date == '00-00-0000') {
		$assessment_date = '<span style="color: red">No Date - Cannot submit report</span>';
	}
	echo "<div style='width=600px; padding: 10px; background-color: silver; border: 1px dotted black'><b>Sending for approval</b><br>
				<span style='margin-top: 10px; padding-left: 20px'>Establishment: $establishment_name </span><br>
				<span style='padding-left: 20px'>Date check: $assessment_date<span><br>
				<div style='text-align: right; width: 100%'>";
	echo "<input type='button' value='Cancel' onClick='document.location=\"/juno/apps/assessment_manager/view_and_edit_assessments.php?id={$_GET['id']}\"'>";
	if (!empty($assessment_date) || $assessment_date != '00-00-0000') {
		echo "<input type='button' value='Continue' onClick='document.location=\"/juno/apps/assessment_manager/f/submit_for_approval.php?id={$_GET['id']}&submit\"'>";
	}
	echo "</div></div>";
}


echo "<script>w=j_P.j_Wi[j_W]";
echo ";w.['table'].style.width=500";
echo ";w.['table'].style.height=100";
echo ";setTimeout('j_P.J_WX(self)',2000)";
echo ";J_icoAlert()";
echo "</script>";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

?>
