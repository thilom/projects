<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$id = $_GET["id"];
$establishment_code = $_GET['id'];
$no_change = TRUE;
$pricing_fields = array('price_description', 'price_in_high', 'price_in_low', 'price_out_high', 'price_out_low', 'category_prefix', 'category_suffix', 'single_prefix', 'single_description');

if (isset($_GET["j_hide"])) {
    //Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (in_array($field_name, $pricing_fields)) $changes .= "$field_name,";
    }
    $sql_updates->free_result();
    $sql_updates->close();

    echo "<html>";
    echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<script src=".$ROOT."/system/P.js></script>";
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/pricing.js></script>";
    echo "<script>J_start('" . $id . "'" . (isset($_GET["view"]) ? ",1" : "") . ")</script>";
    echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
    echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

    die();
}

$SUB_ACCESS = array("edit-data"=>0);
include $SDR . "/system/secure.php";
if (isset($_GET["view"]))
    $SUB_ACCESS["edit-data"] = 0;

if (count($_POST)) {
    //Get pricing Data
    $statement = "SELECT price_description, price_in_high, price_in_low, price_out_high, price_out_low, category_prefix,
                        category_suffix, single_prefix, single_description FROM nse_establishment_pricing WHERE establishment_code=?";
    $sql_price = $GLOBALS['dbCon']->prepare($statement);
    $sql_price->bind_param('s', $establishment_code);
    $sql_price->execute();
    $sql_price->bind_result($price_description, $price_in_high, $price_in_low, $price_out_high, $price_out_low, $category_prefix,
                        $category_suffix, $single_prefix, $single_description);
    $sql_price->fetch();
    $sql_price->free_result();
    $sql_price->close();

    if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only

        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
            if (isset($$field_name) && $field_value != $$field_name) {
                $no_change = FALSE;
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data update by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data update by client", '', $establishment_code);
        }
    } else {
        //Prepare statement - get submitted changes
        $statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert permanent log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
        $sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statment - clear temp log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {

            if (!isset($$field_name)) $$field_name = '';
            if ($field_value != $$field_name) {

                $submitted_by = '';
                $submitted_date = '';
                $submitted_field_value = '';

                //Get client submitted changes
                $sql_submitted_data->bind_param('ss', $establishment_code, $field_name);
                $sql_submitted_data->execute();
                $sql_submitted_data->store_result();
                if ($sql_submitted_data->num_rows == 1) {
                    $sql_submitted_data->bind_result($submitted_by, $submitted_field_value, $submitted_date);
                    $sql_submitted_data->fetch();
                    $sql_submitted_data->free_result();
                }

                //Write to permanent log
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();

                //Clear client log
                $sql_clear_log->bind_param('ss', $establishment_code, $field_name);
                $sql_clear_log->execute();

                $changed_fields[$field_name] = $field_value;
            }
        }
        $sql_clear_log->close();
        $sql_log_insert->close();
        $sql_submitted_data->close();

        //Check if entry exists in DB
        $count = 0;
        $statement = "SELECT  COUNT(*) FROM nse_establishment_pricing WHERE establishment_code=?";
        $sql_check = $GLOBALS['dbCon']->prepare($statement);
        $sql_check->bind_param('s', $establishment_code);
        $sql_check->execute();
        $sql_check->bind_result($count);
        $sql_check->fetch();
        $sql_check->close();

        //Insert if it doesn't exist
        if ($count == 0) {
            $statement = "INSERT INTO nse_establishment_pricing (establishment_code) VALUES (?)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bind_param('s', $establishment_code);
            $sql_insert->execute();
            $sql_insert->close();
        }


        //Save Data
        if (!empty($changed_fields)) {
            foreach ($changed_fields as $field_name => $field_value) {
                $no_change = FALSE;
                if (in_array($field_name, $pricing_fields)) {
                    $statement = "UPDATE nse_establishment_pricing SET $field_name=? WHERE establishment_code=?";
                    $sql_directions = $GLOBALS['dbCon']->prepare($statement);
                    $sql_directions->bind_param('ss',  $field_value, $establishment_code);
                    $sql_directions->execute();
                    $sql_directions->close();
                }
            }
        }

        if (!$no_change) {
            $note_content = "Establishment Data updated (Pricing Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
			timestamp_establishment($establishment_code);
        }
    }
}

$description = "";
$cat_prefix = "Room";
$cat_hi = "";
$cat_low = "";
$cat_suffix = "";
$sngl_prefix = "Room";
$sngl_cat_hi = "";
$sngl_cat_low = "";
$sngl_suffix = "";
$price_description = '';
$category_prefix = '';
$price_in_high = '';
$price_in_low = '';
$price_out_high = '';
$price_out_low = '';
$single_prefix = '';

$q = "SELECT * FROM nse_establishment_pricing WHERE establishment_code='" . $id . "' LIMIT 1";
$q = mysql_query($q);
if (mysql_num_rows($q)) {
    $g = mysql_fetch_array($q);
    $price_description = $g["price_description"];
    $category_prefix = $g["category_prefix"] ? $g["category_prefix"] : "Room";
    $cat_hi = $g["category"];
    $cat_low = $g["category_low"];
    $category_suffix = $g["category_suffix"];
    $single_prefix = $g["single_prefix"] ? $g["single_prefix"] : "Room";
    $sngl_cat_hi = $g["single_category"];
    $sngl_cat_low = $g["single_category_low"];
    $single_description = $g["single_description"];
    $price_in_high = $g['price_in_high'];
    $price_in_low = $g['price_in_low'];
    $price_out_high = $g['price_out_high'];
    $price_out_low = $g['price_out_low'];
}

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    $$field_name = $field_value;
    if ($field_name == 'suburb') $suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
    if ($field_name == 'town') $town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
    if ($field_name == 'province') $province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
    if ($field_name == 'country') $country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
    switch ($field_name) {
        case 'image_name':
            $new_image = $$field_name;
        default:
            if (in_array($field_name, $pricing_fields)) $changes .= "$field_name,";
    }
}
$sql_updates->free_result();
$sql_updates->close();

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/pricing.js></script>";
echo "<script>E_s(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ")</script>";
echo "<script>changes='$changes'</script>";
if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";


echo "<tr><th>Description</th><td colspan=9 width=90%>" . ($SUB_ACCESS["edit-data"] ? "<textarea name=price_description class=jW100 rows=6>" . $price_description . "</textarea>" : $price_description) . "</td></tr>";

if ($SUB_ACCESS["edit-data"]) {
    $pre = "<option value=Unit>Unit<option value=Double>Double<option value=Single>Single<option value=Suite>Suite<option value=Room>Room<option value=Site>Site";
    $cats = "<option value=G>G (Under R100)<option value=F>F (From R100 - R149)<option value=E>E (From R150 - R199)<option value=D>D (From R200 - R299)<option value=C>C (From R300 - R399)<option value=B>B (From R400 - R499)<option value=A>A (From R500 - R999)<option value=A1>A1 (Over R1000)<option value=A2>A2 (Over R2000)<option value=A3>A3 (Over R3000)<option value=A4>A4 (Over R4000)<option value=A5>A5 (Over R5000)<option value=A6>A6 (Over R6000)<option value=A7>A7 (Over R7000)";
} else {
    $pre = array("" => "", "Single"=>"Single", "Unit" => "Unit", "Double" => "Double", "Suite" => "Suite", "Room" => "Room", "Site" => "Site");
    $cats = array("" => "", "G" => "G (Under R100)", "F" => "F (From R100 - R149)", "E" => "E (From R150 - R199)", "D" => "D (From R200 - R299)", "C" => "C (From R300 - R399)", "B" => "B (From R400 - R499)", "A" => "A (From R500 - R999)", "A1" => "A1 (Over R1000)", "A2" => "A2 (Over R2000)", "A3" => "A3 (Over R3000)", "A4" => "A4 (Over R4000)", "A5" => "A5 (Over R5000)", "A6" => "A6 (Over R6000)", "A7" => "A7 (Over R7000)");
}


echo "<tr class=hed><th></th><th>Type</th><th width=25%><p>Highest Rate</th><th width=25%>Lowest Rate</th><th>Description</th></tr>";
echo "<tr><th>In-Season</th>";
if ($SUB_ACCESS["edit-data"]) {
    echo "<td><select name=category_prefix class=jW100><option value=''>" . str_replace("=" . $category_prefix . ">", "=" . $category_prefix . " selected>", $pre) . "</select></td>";
    echo "<td><input type=text name=price_in_high value='$price_in_high' class=jW100 /></td>";
    echo "<td><input type=text name=price_in_low value='$price_in_low' class=jW100 /></td>";
    echo "<td><input type=text name=category_suffix value=\"" . (empty($category_suffix)?'pp Sharing':$category_suffix) . "\" class=jW100></td>";
} else {
    echo "<td>" . $pre[$category_prefix] . "</td>";
    echo "<td>" . $cats[$cat_hi] . "</td>";
    echo "<td>" . $cats[$cat_low] . "</td>";
    echo "<td>" . $category_suffix . "</td>";
}
echo "</tr>";
echo "<tr><th>Out-of-Season</th>";
if ($SUB_ACCESS["edit-data"]) {
    echo "<td><select name=single_prefix class=jW100><option value=''>" . str_replace("=" . $single_prefix . ">", "=" . $single_prefix . " selected>", $pre) . "</select></td>";
    echo "<td><input type=text name=price_out_high value='$price_out_high' class=jW100 /></td>";
    echo "<td><input type=text name=price_out_low value='$price_out_low' class=jW100 /></td>";
    echo "<td><input type=text name=single_description value=\"" . (empty($single_description)?'pp Sharing B&B':$single_description) . "\" class=jW100></td>";
} else {
    echo "<td>" . $pre[$single_prefix] . "</td>";
    echo "<td>" . $cats[$sngl_cat_hi] . "</td>";
    echo "<td>" . $cats[$sngl_cat_low] . "</td>";
    echo "<td>" . $single_description . "</td>";
}
echo "</tr>";


//Old Data

if ($_SESSION['j_user']['role'] != 'c' && empty($price_in_high) && empty($price_in_low) && empty($price_out_high) && empty($price_out_low) && !empty($sngl_cat_hi)) {
    echo "<tr><th>Old Rate Data (for information only)</th>";
    echo "<tr class=hed><th></th><th>Type</th><th width=25%><p>Highest Rate</th><th width=25%>Lowest Rate</th><th>Description</th></tr>";
    echo "<tr><th>In-Season</th>";

    echo "<td>" . $pre[$category_prefix] . "</td>";
    echo "<td>" . $cats[$cat_hi] . "</td>";
    echo "<td>" . $cats[$cat_low] . "</td>";
    echo "<td>" . $category_suffix . "</td>";

    echo "</tr>";
    echo "<tr><th>Out-of-Season</th>";
    echo "<td>" . $pre[$single_prefix] . "</td>";
    echo "<td>" . $cats[$sngl_cat_hi] . "</td>";
    echo "<td>" . $cats[$sngl_cat_low] . "</td>";
    echo "<td>" . $single_description . "</td>";

    echo "</tr>";
}

echo "<script>E_r(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ")</script>";

if ($SUB_ACCESS["edit-data"])
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/saver.js></script>";

if (!isset($_GET["j_IF"])) {
    $J_title2 = "Establishment Manager";
    $J_title1 = "Pricing";
    $J_width = 480;
    $J_height = 480;
    $J_icon = "<img src=" . $ROOT . "/ico/set/gohome-edit.png>";
    $J_tooltip = "";
    $J_label = 22;
    $J_nostart = 1;
    $J_nomax = 1;
    include $SDR . "/system/deploy.php";
}

echo "</body></html>";
?>