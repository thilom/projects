sav=0

function saver()
{
	if(confirm("WARNING! You have unsaved entries in the last panel!\nDo you want to save your changes?"))
		$T("form")[0].submit()
	else
		sav=0
}

document.onkeyup=function(e){if(j_E.tagName=="INPUT"||j_E.tagName=="TEXTAREA"){if(!sav)sav=1}}

document.onchange=function(e){if(j_E.tagName=="SELECT"||j_E.tagName=="OPTION"||j_E.tagName=="INPUT"||j_E.tagName=="TEXTAREA"){if(!sav)sav=1}}

document.onmousedown=function(e){j_P.J_WZ(j_W,1);j_E=j_FF?e.target:event.srcElement;J_PX();j_x=j_FF?e.clientX:event.clientX;j_y=j_FF?e.clientY:event.clientY;j_yo=j_y;j_xo=j_x;j_P.j_x=j_tWx+j_x;j_P.j_y=j_tWy+j_y;j_P.j_b=j_FF?e.which:event.button+1;j_P.J_closer();
if(j_E.tagName=="INPUT"&&(j_E.type.toUpperCase()=="CHECKBOX"||j_E.type.toUpperCase()=="RADIO"))
{if(!sav)sav=1}
}

window.onresize=function()
{
	j_P.J_WFu(j_W)
	J_DW()
	if(document.body)
		document.body.style.overflow=j_ww<21?"hidden":"auto"
	if(sav && j_ww<21)
	{
		if(sav>0)
		{
			sav=-1
			setTimeout("saver()",500)
		}
		else
			sav=0
	}
}