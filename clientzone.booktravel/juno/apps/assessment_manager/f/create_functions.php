<?php
require_once $SDR . '/utility/FPDF/tcpdf.php';
require_once $SDR . '/utility/FPDF/fpdi/fpdi.php';

function create_report($assessment_id) {
    //Get Assessment Status
    $statement = "SELECT a.aa_category_name, a.pdf_image, DATE_FORMAT(b.renewal_date, '%M %Y'), DATE_FORMAT(b.assessment_date, '%d %M %Y'), b.assessor_name, b.establishment_code
				FROM nse_establishment_assessment AS b
				LEFT JOIN nse_aa_category AS a ON b.qa_status=a.aa_category_code
				WHERE b.assessment_id=?
				LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($endorsement, $qa_logo, $renewal_date, $assessment_date, $assessor, $establishment_code);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();
	if (empty($qa_logo)) $qa_logo = 'Quality Assured Logo_1.jpg';

     //Get Data
    $statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->store_result();
    $sql_data->bind_result($establishment_name);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    //Get Assessment Status
    $statement = "SELECT a.subcategory_name
                    FROM nse_restype_subcategory_lang AS a
                    JOIN nse_establishment_assessment AS b ON b.proposed_category=a.subcategory_id
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($category);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();

    //Get Location
    $statement = "SELECT b.town_name, c.province_name
                    FROM nse_establishment_location AS a
                    LEFT JOIN nse_nlocations_towns AS b ON a.town_id=b.town_id
                    LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
                    WHERE a.establishment_code=? LIMIT 1";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->store_result();
    $sql_location->bind_result($town_name, $province_name);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
    if (!empty($province_name)) $town_name .= ", $province_name";

    //Get Assessor
    $statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";


    $filename = strtolower(str_replace(' ', '_', "$establishment_code report $renewal_date"));

    //Create Report
    $report = new FPDI('P','mm','A4',true,'UTF-8',false);
    $report->SetMargins(1.25, 1.25, 1);

    // remove default header/footer
    $report->setPrintHeader(false);
    $report->setPrintFooter(false);

    $report->addPage();
    $report->SetAutoPageBreak(true, 2);
    $report->addFont('vera', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/vera.php');
    $report->addFont('verabd', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/verabd.php');

    $report->setJPEGQuality(100);
    $report->Image($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$qa_logo", 2, 2, 35, 0, '', '', '', true, 150);

    $report->SetFont('verabd', '', 10);
    //$report->setLeftMargin(40);
    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(0, 0, 'Quality Assurred Assessment Report for', 0, 1);
    $report->SetFont('verabd', '', 12);
    $report->ln(1);
    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(0, 0, "$establishment_name - $town_name.", 0, 1);

    $report->SetFont('vera', '', 8);
    $report->ln(5);
    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(30, 0, 'Assessment Date: ', 0, 0);
    $report->Cell(0, 0, $assessment_date, 0, 1);

    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(30, 0, 'Assessor: ', 0, 0);
    $report->Cell(0, 0, $assessor, 0, 1);

    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(30, 0, 'Category: ', 0, 0);
    $report->Cell(0, 0, $category, 0, 1);

    $report->ln(30);
    $report->Cell(0, 0, "Our assessor visited your establishment on the $assessment_date and compiled the following report.", 0, 1);


    //Get assessment Data
    $statement = "SELECT b.assessment_area_name, a.assessment_content
                    FROM nse_establishment_assessment_data AS a
                    JOIN nse_assessment_areas AS b ON a.assessment_area_id=b.assessment_area_id
                    WHERE a.not_applicable != 'Y' && hide_from_client != 'Y' && a.assessment_id=?
                    ORDER BY b.display_order";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    $sql_data->bind_param('i', $assessment_id);
    $sql_data->execute();
    $sql_data->store_result();
    $sql_data->bind_result($assessment_name, $assessment_content);
    while ($sql_data->fetch()) {
        if (empty($assessment_content)) continue;
        $report->SetFont('verabd', '', 8);
        $report->ln(10);
        $report->Cell(0, 0, $assessment_name, 0, 1);
        $report->SetFont('vera', '', 8);
        $report->MultiCell(0, 0, $assessment_content, 0, 1);
    }
    $sql_data->free_result();
    $sql_data->close();


    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf")) unlink($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf");
    $report->output($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf", 'F');
    $report->close();

    $report_file = "/reports/$filename.pdf";

    return $report_file;

}

function create_certificate($assessment_id) {
    //Get Assessment Status
    $statement = "SELECT a.aa_category_name, a.pdf_image, DATE_FORMAT(b.renewal_date, '%M %Y'), establishment_code
                    FROM nse_aa_category AS a
                    JOIN nse_establishment_assessment AS b ON b.qa_status=a.aa_category_code
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($endorsement, $qa_logo, $assessment_date, $establishment_code);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();

    //Get Data
    $statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->store_result();
    $sql_data->bind_result($establishment_name);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    //Get Assessment Status
    $statement = "SELECT a.subcategory_name
                    FROM nse_restype_subcategory_lang AS a
                    JOIN nse_establishment_assessment AS b ON b.proposed_category=a.subcategory_id
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($category);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();

    //Get rooms / Units
    $statement = 'SELECT room_count, room_type FROM nse_establishment_data WHERE establishment_code=?';
    $sql_rooms = $GLOBALS['dbCon']->prepare($statement);
    $sql_rooms->bind_param('s', $establishment_code);
    $sql_rooms->execute();
    $sql_rooms->store_result();
    $sql_rooms->bind_result($room_count, $room_type);
    $sql_rooms->fetch();
    $sql_rooms->free_result();
    $sql_rooms->close();
    if (empty($room_type)) $room_type = 'Rooms';

    $filename = strtolower(str_replace(' ', '_', "$establishment_code $assessment_date"));

    $certificate = new FPDI('P','mm','A4',true,'UTF-8',false);
    $pagecount = $certificate->setSourceFile($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/pdf_templates/AAQAA-Certificate-Template-01.pdf');
    $certificate_template = $certificate->importPage(1, '/MediaBox');
    $dimensions = $certificate->getTemplateSize($certificate_template);

    // remove default header/footer
    $certificate->setPrintHeader(false);
    $certificate->setPrintFooter(false);

    $certificate->addPage();
    $certificate->SetAutoPageBreak(false);
    $certificate->addFont('garamond', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/garamondpremrprosmbdit.php');
//	$certificate->addFont('corsiva', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/f5c0e145b1fb8af5b4105c50f6fa2d1f_mtcorsva.php');
    $certificate->addFont('vera', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/vera.php');
    $certificate->addFont('verabd', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/verabd.php');
    $certificate->useTemplate($certificate_template, 0, 0, $dimensions['w'], $dimensions['h']);

    $certificate->SetFont('verabd', '', 27);
    $certificate->ln(58);
    $certificate->setLeftMargin(0);
    $certificate->Cell(0, 0, 'AA Quality Assured', 0, 1, 'C');
    $certificate->Cell(0, 0, 'Accommodation Programme', 0, 1, 'C');

	if (strlen($establishment_name) > 40) {
		$certificate->SetFont('garamond', '', 30);
	} else {
		$certificate->SetFont('garamond', '', 45);
	}

    $certificate->ln(10);
	$certificate->setX(20);
    $certificate->multicell(170, 0, trim(utf8_encode($establishment_name)), 0, 'C' );

    $certificate->SetFont('vera', '', 20);
    $certificate->ln(10);
    $certificate->Cell(0, 12, 'has been endorsed as', 0, 1, 'C');

    $certificate->SetFont('garamond', '', 40);
    $certificate->ln(10);
    $certificate->Cell(0, 12, $endorsement, 0, 1, 'C');

    $certificate->SetFont('vera', '', 20);
    $certificate->ln(10);
    $certificate->Cell(0, 12, 'in the category', 0, 1, 'C');

    $certificate->SetFont('verabd', '', 22);
    $certificate->ln(10);
    $certificate->setX(0);
    $certificate->setLineWidth(1);
    $certificate->multiCell(0, 12, $category, 0, 'C');

	$certificate->SetFont('vera', '', 14);
	$certificate->setTextColor(0, 0, 0);
    $certificate->SetY(255);
    $certificate->Cell(0, 12, 'C.E.O.', 0, 1, 'C');

	$certificate->SetLineWidth(0.3);
	$certificate->Line(70, 256, 130, 256);

    $certificate->SetFont('verabd', '', 10);
    $certificate->setTextColor(250, 250, 250);
    $certificate->setXY(15, 275);
    $certificate->multiCell(0, 0, "$room_count $room_type", 0, 'L');

    $certificate->SetFont('verabd', '', 10);
    $certificate->setTextColor(250, 250, 250);
    $certificate->setXY(-45, 271);
    $certificate->multiCell(0, 0, "Valid Until:\n$assessment_date", 0, 'R');



    $certificate->setJPEGQuality(100);
    $certificate->Image($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/pdf_templates/v_sig.jpg', 82, 245, 35, 0, '', '', '', true, 150);


    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf")) unlink($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf");
    $certificate->output($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf", 'F');
    $certificate->Close();

    $certificate_file = "/certificates/$filename.pdf";

    return $certificate_file;
}

function create_certificate_cc($assessment_id) {
	//Get Assessment Status
    $statement = "SELECT a.aa_category_name, a.pdf_image, DATE_FORMAT(b.renewal_date, '%M %Y'), establishment_code
                    FROM nse_aa_category AS a
                    JOIN nse_establishment_assessment AS b ON b.qa_status=a.aa_category_code
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($endorsement, $qa_logo, $assessment_date, $establishment_code);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();

    //Get Data
    $statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->store_result();
    $sql_data->bind_result($establishment_name);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    //Get Assessment Status
    $statement = "SELECT a.subcategory_name
                    FROM nse_restype_subcategory_lang AS a
                    JOIN nse_establishment_assessment AS b ON b.proposed_category=a.subcategory_id
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($category);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();

    //Get rooms / Units
    $statement = 'SELECT caravan_sites+camp_sites+caravan_camping_sites FROM nse_establishment_data WHERE establishment_code=?';
    $sql_rooms = $GLOBALS['dbCon']->prepare($statement);
    $sql_rooms->bind_param('s', $establishment_code);
    $sql_rooms->execute();
    $sql_rooms->store_result();
    $sql_rooms->bind_result($room_count);
    $sql_rooms->fetch();
    $sql_rooms->free_result();
    $sql_rooms->close();
    if (empty($room_type)) $room_type = 'Sites';

    $filename = strtolower(str_replace(' ', '_', "$establishment_code $assessment_date"));

    $certificate = new FPDI('P','mm','A4',true,'UTF-8',false);
    $pagecount = $certificate->setSourceFile($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/pdf_templates/AAQAA-Certificate-Template-cc.pdf');
    $certificate_template = $certificate->importPage(1, '/MediaBox');
    $dimensions = $certificate->getTemplateSize($certificate_template);

    // remove default header/footer
    $certificate->setPrintHeader(false);
    $certificate->setPrintFooter(false);

    $certificate->addPage();
    $certificate->SetAutoPageBreak(false);
    $certificate->addFont('garamond', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/garamondpremrprosmbdit.php');
//	$certificate->addFont('corsiva', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/f5c0e145b1fb8af5b4105c50f6fa2d1f_mtcorsva.php');
    $certificate->addFont('vera', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/vera.php');
    $certificate->addFont('verabd', '', $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/FPDF/fonts/verabd.php');
    $certificate->useTemplate($certificate_template, 0, 0, $dimensions['w'], $dimensions['h']);

    $certificate->SetFont('verabd', '', 27);
    $certificate->ln(58);
    $certificate->setLeftMargin(0);
    $certificate->Cell(0, 0, 'AA Quality Assured', 0, 1, 'C');
    $certificate->Cell(0, 0, 'Accommodation Programme', 0, 1, 'C');

    $certificate->SetFont('garamond', '', 45);
    $certificate->ln(10);
	$certificate->setX(20);
    $certificate->multicell(170, 0, trim(utf8_encode($establishment_name)), 0, 'C' );

    $certificate->SetFont('vera', '', 20);
    $certificate->ln(10);
    $certificate->Cell(0, 12, 'has been endorsed as', 0, 1, 'C');

    $certificate->SetFont('garamond', '', 40);
    $certificate->ln(10);
    $certificate->Cell(0, 12, $endorsement, 0, 1, 'C');

    $certificate->SetFont('vera', '', 20);
    $certificate->ln(10);
    $certificate->Cell(0, 12, 'in the category', 0, 1, 'C');

    $certificate->SetFont('verabd', '', 22);
    $certificate->ln(10);
    $certificate->setX(0);
    $certificate->setLineWidth(1);
    $certificate->multiCell(0, 12, $category, 0, 'C');

	$certificate->SetFont('vera', '', 14);
	$certificate->setTextColor(0, 0, 0);
    $certificate->SetY(255);
    $certificate->Cell(0, 12, 'C.E.O.', 0, 1, 'C');

	$certificate->SetLineWidth(0.3);
	$certificate->Line(70, 256, 130, 256);

    $certificate->SetFont('verabd', '', 10);
    $certificate->setTextColor(250, 250, 250);
    $certificate->setXY(15, 275);
    $certificate->multiCell(0, 0, "$room_count $room_type", 0, 'L');

    $certificate->SetFont('verabd', '', 10);
    $certificate->setTextColor(250, 250, 250);
    $certificate->setXY(-45, 271);
    $certificate->multiCell(0, 0, "Valid Until:\n$assessment_date", 0, 'R');

    $certificate->setJPEGQuality(100);
    $certificate->Image($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/pdf_templates/v_sig.jpg', 82, 245, 35, 0, '', '', '', true, 150);


    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf")) unlink($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf");
    $certificate->output($_SERVER['DOCUMENT_ROOT'] . "/certificates/$filename.pdf", 'F');
    $certificate->Close();

    $certificate_file = "/certificates/$filename.pdf";

    return $certificate_file;
}

?>