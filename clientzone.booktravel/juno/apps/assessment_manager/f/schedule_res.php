<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/f/schedule_res.js></script>";

$r=$_SESSION["j_user"]["role"];
$id=($r=="a"||$r=="c"||$r=="r"?$_SESSION["j_user"]["id"]:(isset($_GET["id"])?$_GET["id"]:0));
include $SDR."/system/parse.php";
$ds=isset($_GET["ds"])?date("Y-m-d 00:00:00",J_dateParse($_GET["ds"])):date("Y-m-d 00:00:00",mktime(0,0,0,date("m"),date("d")-7,date("Y")));
$de=isset($_GET["de"])?date("Y-m-d 00:00:00",J_dateParse($_GET["de"])):date("Y-m-d 00:00:00",mktime(0,0,0,date("m")+1,date("d"),date("Y")));
$s="";
$u="";

$q="SELECT a.establishment_code, a.assessment_date, a.assessment_id, e.establishment_name, a.invoice_id, u.firstname, u.lastname, e.assessor_id
    FROM nse_establishment_assessment AS a
    LEFT JOIN nse_establishment AS e ON e.establishment_code=a.establishment_code
    LEFT JOIN nse_user AS u ON u.user_id=e.assessor_id
    WHERE a.assessment_date>'$ds' AND a.assessment_date<'$de' ";
if($id)
	$q.="AND e.assessor_id=".$id." ";
$q.="ORDER BY a.assessment_date DESC,u.firstname,u.lastname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$a=array();
	$y=0;
	$m=0;
	$d=0;
	while($g=mysql_fetch_array($q))
	{
		$e=explode("-",$g["assessment_date"]);
		$yy="";
		if($e[0]!=$y)
		{
			$y=$e[0];
			$yy=$y;
		}
		$mm="";
		if($e[1]!=$m)
		{
			$m=$e[1]*1;
			$mm=$m;
		}
		$dd="";
		$wd="";
		$x=explode(" ",$e[2]);
		$e[2]=$x[0];
		if($e[2]!=$d)
		{
			$d=$e[2];
			$dd=$d;
			$wd=date("N",mktime(0,0,0,date($e[1]),date($e[2]),date($e[0])));
		}
		$tt=explode(" ",$g["assessment_date"]);
		$tt=substr($tt[1],0,5);
		$s.=$yy."~".$mm."~".$dd."~".$wd."~".$tt."~".$g["assessment_id"]."~".$g["establishment_code"]."~".$g["establishment_name"]."~".$g["assessor_id"]."~".$g["invoice_id"];
		$p="";
		if($g["invoice_id"])
		{
			$gi=mysql_fetch_array(mysql_query("SELECT j_inv_paid,j_inv_total FROM nse_invoice WHERE j_inv_id=".$g["invoice_id"]." LIMIT 1"));
			if($gi["j_inv_paid"]==$gi["j_inv_total"])
				$p=1;
		}
		$s.="~".$p;
		$s.="|";
		if(!isset($a[$g["assessor_id"]]))
			$a[$g["assessor_id"]]=$g["firstname"]." ".$g["lastname"];
	}
	if(count($a))
	{
		foreach($a as $k => $v)
			$u.=$k."~".$v."|";
	}
}

$dsp=isset($_GET["ds"])?date("d/m/Y",J_dateParse($_GET["ds"])):date("d/m/Y",mktime(0,0,0,date("m"),date("d")-7,date("Y")));
$dep=isset($_GET["de"])?date("d/m/Y",J_dateParse($_GET["de"])):date("d/m/Y",mktime(0,0,0,date("m")+3,date("d"),date("Y")));

echo "<script>A_ss('".$_SESSION["j_user"]["role"]."',\"".$s."\",\"".$u."\",\"".$dsp."\",\"".$dep."\",".$id.",\"".J_Value("","people","",$id)."\",".(isset($_GET["a"])?1:0).")</script>";

?>