<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$id=$_GET["id"];
$establishment_code = $_GET['id'];
$no_change = '';
$policy_fields = array('child_policy','cancellation_policy');
$cancellation_policy = '';
$cancellation_policy_exists = false;
$childs_policy_exists = false;

if(isset($_GET["j_hide"])) {
    //Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (in_array($field_name, $policy_fields)) $changes .= "$field_name,";
    }
    $sql_updates->free_result();
    $sql_updates->close();

	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/policies.js></script>";
	echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
  echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
	die();
}

$SUB_ACCESS=array("edit-data"=>0);
include $SDR."/system/secure.php";
if(isset($_GET["view"]))
	$SUB_ACCESS["edit-data"]=0;

if(count($_POST)) {
    //Get current content
    $statement = "SELECT description_type, establishment_description FROM nse_establishment_descriptions WHERE establishment_code=?";
    $sql_current = $GLOBALS['dbCon']->prepare($statement);
    $sql_current->bind_param('s', $establishment_code);
    $sql_current->execute();
    $sql_current->store_result();
    $sql_current->bind_result($type, $content);
    while ($sql_current->fetch()) {
        if ($type == 'cancellation policy') {
			$cancellation_policy = $content;
			$cancellation_policy_exists = true;

		}
        if ($type == 'child policy') {
			$child_policy = $content;
			$child_policy_exists = true;

		}
    }
    $sql_current->free_result();
    $sql_current->close();

    //Insert missing entries
    if (!$cancellation_policy_exists) {
        $statement = "INSERT INTO nse_establishment_descriptions (establishment_code, description_type) VALUES (?,'cancellation policy')";
        $sql_cancel_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_cancel_insert->bind_param('s', $establishment_code);
        $sql_cancel_insert->execute();
        $sql_cancel_insert->close();
        $cancellation_policy = '';
    }
    if (!$child_policy_exists) {
        $statement = "INSERT INTO nse_establishment_descriptions (establishment_code, description_type) VALUES (?,'child policy')";
        $sql_child_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_child_insert->bind_param('s', $establishment_code);
        $sql_child_insert->execute();
        $sql_child_insert->close();
        $child_policy = '';
    }

    if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only
        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
            if (isset($$field_name) && $field_value != $$field_name) {
                $no_change = FALSE;
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated by client", '', $establishment_code);
        }

    } else {
        //Prepare statement - get submitted changes
        $statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert permanent log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
        $sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statment - clear temp log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

		foreach ($_POST as $field_name=>$field_value) {

            if (!isset($$field_name)) $$field_name = '';
            if ($field_value != $$field_name) {

                $submitted_by = '';
                $submitted_date = '';
                $submitted_field_value = '';

                //Get client submitted changes
                $sql_submitted_data->bind_param('ss', $establishment_code, $field_name);
                $sql_submitted_data->execute();
                $sql_submitted_data->store_result();
                if ($sql_submitted_data->num_rows == 1) {
                    $sql_submitted_data->bind_result($submitted_by, $submitted_field_value, $submitted_date);
                    $sql_submitted_data->fetch();
                    $sql_submitted_data->free_result();
                }

                //Write to permanent log
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();

                //Clear client log
                $sql_clear_log->bind_param('ss', $establishment_code, $field_name);
                $sql_clear_log->execute();

                $changed_fields[$field_name] = $field_value;
            }
        }
        $sql_clear_log->close();
        $sql_log_insert->close();
        $sql_submitted_data->close();


        //Save Data

        $statement = "UPDATE nse_establishment_descriptions SET establishment_description=? WHERE establishment_code=? AND description_type=?";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        foreach ($_POST as $key=>$value) {

            if ($$key != $value) {
				$key = str_replace('_', ' ',$key);
                $no_change = FALSE;
                $sql_update->bind_param('sss', $value, $establishment_code, $key);
                $sql_update->execute();
            }
        }
        $sql_update->close();

        if (!$no_change) {
            $note_content = "Establishment Data updated (Policies Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
			timestamp_establishment($establishment_code);
        }

    }
}



$cancellation_policy="";
$child_policy="";
$q="SELECT * FROM nse_establishment_descriptions WHERE establishment_code='".$id."' AND (description_type='cancellation policy' OR description_type='child policy')";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["description_type"]=="cancellation policy" && $g["establishment_description"])
			$cancellation_policy=$g["establishment_description"];
		if($g["description_type"]=="child policy" && $g["establishment_description"])
			$child_policy=$g["establishment_description"];
	}
}

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    $$field_name = $field_value;
    if (in_array($field_name, $policy_fields)) $changes .= "$field_name,";
}
$sql_updates->free_result();
$sql_updates->close();

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/policies.js></script>";
echo "<script>E_s(".($SUB_ACCESS["edit-data"]?1:"").")</script>";

echo "<tr><th>Cancellations</th><td width=90% style='padding: 4px'>".($SUB_ACCESS["edit-data"]?"<textarea name='cancellation_policy' rows=5 class=jW100>".$cancellation_policy."</textarea>":$cancellation_policy)."</td></tr>";
echo "<tr><th>Children</th><td style='padding: 4px'>".($SUB_ACCESS["edit-data"]?"<textarea name='child_policy' rows=5 class=jW100>".$child_policy."</textarea>":$child_policy)."</td></tr>";

echo "<script>E_d(".($SUB_ACCESS["edit-data"]?1:"").")</script>";

if($SUB_ACCESS["edit-data"])
{
	echo "<script>changes='$changes'</script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
}

if(!isset($_GET["j_IF"]))
{
	$J_title2="Establishment Manager";
	$J_title1="Policies";
	$J_width=480;
	$J_height=480;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}

echo "</body></html>";
?>