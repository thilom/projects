<?php
/**
 * Approve assessments routine for staff.
 *
 * @author Thilo Muller(2010,2011)
 * @version $Id: approve_assessment.php 1221 2011-09-23 09:46:42Z web37 $
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
require_once $SDR . '/utility/PHPMailer/class.phpmailer.php';
include $SDR."/apps/assessment_manager/f/create_functions.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/system/error.php";

//Set error handler
set_error_handler('error_handler');

//Vars
$assessment_id = $_GET['id'];

if (isset($_POST['save'])) {
    approval_form_save();
} else {
    approval_form_show();
}

function approval_form_show() {
    $counter = 0;
    $email_list = "<table width=100% id=jTR style='border: solid 1px black'><tr style='background-color: gray'><th>&nbsp;</th><th>Contact</th><th>Email</th><th>Designation</th></tr>";
    $req = '';

    //Get assessment Data
    $statement = "SELECT establishment_code, invoice_id FROM nse_establishment_assessment WHERE assessment_id=?";
    $sql_data = $GLOBALS['dbCon']->Prepare($statement);
    $sql_data->bind_param('i', $GLOBALS['assessment_id']);
    $sql_data->execute();
    $sql_data->bind_result($establishment_code, $invoice_id);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    if ($invoice_id == 0) {
        $message = "There is <b style='color: red'>NO INVOICE</b> associated with this assessment. Welcome pack should not be sent to selected contacts.";
    } else {
        //Check payment
        $statement = "SELECT j_inv_total_vat, j_inv_paid FROM nse_invoice WHERE j_inv_id=?";
        $sql_invoice = $GLOBALS['dbCon']->prepare($statement);
        echo mysqli_error($GLOBALS['dbCon']);
        $sql_invoice->bind_param('i', $invoice_id);
        $sql_invoice->execute();
        $sql_invoice->bind_result($inv_total, $inv_paid);
        $sql_invoice->fetch();
        $sql_invoice->free_result();
        $sql_invoice->close();

        if ($inv_total != $inv_paid) {
            $message = "This assessment has an outstanding invoice. The welcome pack should <b>NOT</b> be sent instead a request for payment should be sent to the selected contacts ";
            $req = "<tt><br><input type=checkbox name=req value=1> Send 'Request for Payment' instead of welcome pack</tt>";
        } else {
            $message = "This assessment is fully paid and the selected contacts will receive a welcome pack.";
        }
    }


    $statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);
    $sql_email->bind_param('s', $establishment_code);
    $sql_email->execute();
    $sql_email->store_result();
    $sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
    while ($sql_email->fetch()) {
        $email_list .= "<tr><td  class=jdbO><input type=checkbox name=emails[] id=email_$counter value='$uID' />";
        $email_list .= "</td><td  class=jdbR><label for=email_$counter >$contact_name</label></td><td  class=jdbY>$contact_email</td><td  class=jdbB>$contact_designation</td></tr>" . PHP_EOL;
        $counter++;
    }
    $sql_email->free_result();
    $sql_email->close();


    //Get Assessor Email Adddress
    $statement = "SELECT b.firstname, b.lastname , b.email, b.user_id
                    FROM nse_establishment AS a
                    JOIN nse_user AS b ON a.assessor_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('s', $establishment_code);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email, $uID);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();
    if (!empty($assessor_email)) {
        $email_list .= "<tr><td class=jdbO><input type=checkbox name=assessor_email id=email_$counter value='$uID' /></td><td class=jdbR><label for=email_$counter >$assessor_firstname $assessor_lastname</label></td><td class=jdbY>$assessor_email</td><td class=jdbB>Assessor</td></tr>" . PHP_EOL;
    }

    $email_list .= '</table >';
    //$email_list = substr($email_list, 6);

    //Get template
    $template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/juno/apps/assessment_manager/f/html/assessment_email_form.html");

    //Replace Tags
    $template = str_replace('<!-- email_list -->', $email_list, $template);
    $template = str_replace('<!-- message -->', $message, $template);
    $template = str_replace('<!-- req -->', $req, $template);
    $template = str_replace('!assessment_id!', $GLOBALS['assessment_id'], $template);

    echo $template;
}

function approval_form_save() {
    global $assessment_id;

    //Get assessment Data
    $statement = "SELECT establishment_code, room_count, room_type, qa_status, proposed_category, assessment_type_2, caravan_sites, camp_sites, caravan_camping_sites, awards_category
					FROM nse_establishment_assessment
					WHERE assessment_id=?
					LIMIT 1";
    $sql_assessment = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessment->bind_param('i', $assessment_id);
    $sql_assessment->execute();
    $sql_assessment->bind_result($establishment_code, $room_count, $room_type, $qa_status, $proposed_category, $assessment_type_2, $caravan_sites, $camp_sites, $caravan_camping_sites, $award_category);
    $sql_assessment->fetch();
    $sql_assessment->free_result();
    $sql_assessment->close();

    //Update Live
    $statement = "UPDATE nse_establishment SET aa_category_code=? WHERE establishment_code=?";
    $sql_update_qa = $GLOBALS['dbCon']->prepare($statement);
    $sql_update_qa->bind_param('ss', $qa_status, $establishment_code);
    $sql_update_qa->execute();
    $sql_update_qa->close();

    //Update room count
	if ($assessment_type_2 == 'cc') {
		$statement = "UPDATE nse_establishment_data SET caravan_sites=?, camp_sites=?, caravan_camping_sites=? WHERE establishment_code=?";
		$sql_update_room = $GLOBALS['dbCon']->prepare($statement);
		$sql_update_room->bind_param('iiis', $caravan_sites, $camp_sites, $caravan_camping_sites, $establishment_code);
		$sql_update_room->execute();
		$sql_update_room->close();
	} else {
		$statement = "UPDATE nse_establishment_data SET room_count=?, room_type=? WHERE establishment_code=?";
		$sql_update_room = $GLOBALS['dbCon']->prepare($statement);
		$sql_update_room->bind_param('iss', $room_count, $room_type, $establishment_code);
		$sql_update_room->execute();
		$sql_update_room->close();
	}

//	//Get awards year
//	$statement = "SELECT MAX(year)+1
//					FROM award";
//	$sql_awards_year = $GLOBALS['dbCon']->prepare($statement);
//	$sql_awards_year->execute();
//	$sql_awards_year->bind_result($awards_year);
//	$sql_awards_year->fetch();
//	$sql_awards_year->close();
//
//	//Check if an awards entry existst for this year
//	$awards_count = 0;
//	$statement = "SELECT COUNT(*)
//					FROM nse_establishment_awards
//					WHERE establishment_code=? AND awards_year=?";
//	$sql_award_check = $GLOBALS['dbCon']->prepare($statement);
//	$sql_award_check->bind_param('ss', $establishment_code, $awards_year);
//	$sql_award_check->execute();
//	$sql_award_check->bind_result($awards_count);
//	$sql_award_check->fetch();
//	$sql_award_check->close();
//
//	if ($awards_count == 0) {
//		$statement = "INSERT INTO nse_establishment_awards
//							(establishment_code, awards_year, awards_category)
//						VALUES
//							(?,?,?)";
//		$sql_award_insert = $GLOBALS['dbCon']->prepare($statement);
//		$sql_award_insert->bind_param('ssi', $establishment_code, $awards_year, $award_category);
//		$sql_award_insert->execute();
//		$sql_award_insert->close();
//	} else {
//		//Update awards
//		$statement = "UPDATE nse_establishment_awards
//						SET awards_category=?
//						WHERE establishment_code=? AND awards_year=?
//						LIMIT 1";
//		$sql_award_update = $GLOBALS['dbCon']->prepare($statement);
//		$sql_award_update->bind_param('iss', $award_category, $establishment_code, $awards_year);
//		$sql_award_update->execute();
//		$sql_award_update->close();
//	}


    /* Update Res Type */
	//Does an entry already exist
	$res_count = 0;
	$statement = "SELECT count(*) FROM nse_establishment_restype WHERE establishment_code=?";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bind_param('s', $establishment_code);
	$sql_count->execute();
	$sql_count->bind_result($res_count);
	$sql_count->fetch();
	$sql_count->close();

	//Save res type
	if ($res_count == 0) {
		$statement = "INSERT INTO nse_establishment_restype (establishment_code, subcategory_id) VALUES (?,?)";
		$sql_update_cat = $GLOBALS['dbCon']->prepare($statement);
		$sql_update_cat->bind_param('is', $proposed_category, $establishment_code);
		$sql_update_cat->execute();
		$sql_update_cat->close();
	} else {
		$statement = "UPDATE nse_establishment_restype SET subcategory_id=? WHERE establishment_code=? LIMIT 1";
		$sql_update_cat = $GLOBALS['dbCon']->prepare($statement);
		$sql_update_cat->bind_param('is', $proposed_category, $establishment_code);
		$sql_update_cat->execute();
		$sql_update_cat->close();
	}

    //Update assessment
    $statement = "UPDATE nse_establishment_assessment SET assessment_status='complete', approval_date=NOW(), approved_by=? WHERE assessment_id=?";
    $sql_update_assessment = $GLOBALS['dbCon']->prepare($statement);
    $sql_update_assessment->bind_param('ii', $_SESSION['j_user']['id'], $assessment_id);
    $sql_update_assessment->execute();
    $sql_update_assessment->close();

    $mail = new PHPMailer();

    //Get Invoice Data
    $statement = "SELECT establishment_code, invoice_id FROM nse_establishment_assessment WHERE assessment_id=?";
    $sql_data = $GLOBALS['dbCon']->Prepare($statement);
    $sql_data->bind_param('i', $GLOBALS['assessment_id']);
    $sql_data->execute();
    $sql_data->bind_result($establishment_code, $invoice_id);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    //Get Assessment Data
    $statement = "SELECT DATE_FORMAT(a.assessment_date, '%d %M %Y'), a.assessment_date, b.aa_category_name, b.lowres_image, DATE_FORMAT(a.renewal_date,'%M'), DATE_FORMAT(a.renewal_date, '%Y')
                    FROM nse_establishment_assessment AS a
                    JOIN nse_aa_category AS b ON a.qa_status=b.aa_category_code
                    WHERE a.assessment_id=? LIMIT 1";
    $sql_assessment = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessment->bind_param('i', $assessment_id);
    $sql_assessment->execute();
    $sql_assessment->store_result();
    $sql_assessment->bind_result($assessment_date, $assessment_date_ymd, $status, $logo, $renewal_month, $renewal_year);
    $sql_assessment->fetch();
    $sql_assessment->free_result();
    $sql_assessment->close();

    //Get assessor
    $statement = "SELECT CONCAT(b.firstname,' ', b.lastname), b.email
                    FROM nse_establishment AS a
                    LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
                    WHERE a.establishment_code=?
                    LIMIT 1";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('s', $establishment_code);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($assessor_name, $assessor_email);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();

    //Get establishment_name & enter date
    $statement = "SELECT establishment_name,enter_date, cc_enter_date FROM nse_establishment WHERE establishment_code=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $establishment_code);
    $sql_estab->execute();
    $sql_estab->store_result();
    $sql_estab->bind_result($establishment_name, $enter_date, $cc_enter_date);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();

    //Assemble Establishment link
    $establishment_link = $establishment_code . "_$establishment_name";
    $establishment_link = strtolower(str_replace(' ', '_', $establishment_link));
    $establishment_link = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $establishment_link);
    $establishment_link = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $establishment_link );
    $establishment_link = htmlentities($establishment_link);
    $establishment_link = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $establishment_link);
    $establishment_link = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $establishment_link);
    $establishment_link = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $establishment_link);
    $establishment_link = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $establishment_link);
    $establishment_link = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $establishment_link);
    $establishment_link = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $establishment_link);
    $establishment_link .= ".html";


    if (isset($_POST['req'])) {
        //Get email template
        $email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/f/html/assessment_unpaid_email.html');

        //Replace Tags
        $email_template = str_replace('<!-- visit_date -->', $assessment_date, $email_template);
        $email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);

        //Prepare statement - user details
        $statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
        $sql_email = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST['emails'] AS $uID) {
            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('s', $uID);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            if (empty($contact_name)) $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom($assessor_email, $assessor_name);
                $mail->AddReplyTo($assessor_email, $assessor_name);
                $mail->Subject = "AA Quality Assurance for $establishment_name";
                $mail->MsgHTML($email_content);
                $mail->Send();
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }


        if (isset($_POST['assessor_email'])) {

            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('i', $_POST['assessor_email']);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom($assessor_email, $assessor_name);
                $mail->AddReplyTo($assessor_email, $assessor_name);
                $mail->Subject = "AA Quality Assurance for $establishment_name - Request for Payment";
                $mail->MsgHTML($email_content);
                $mail->Send();
                J_act('ASS MANAGER', 7, "QA approved & request for payment emailed to $contact_name($email)", $assessment_id, $establishment_code);
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }

		if (isset($_POST['email_john'])) {

            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('i', $_POST['email_john']);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom($assessor_email, $assessor_name);
                $mail->AddReplyTo($assessor_email, $assessor_name);
                $mail->Subject = "AA Quality Assurance for $establishment_name - Request for Payment";
                $mail->MsgHTML($email_content);
                $mail->Send();
                J_act('ASS MANAGER', 7, "QA approved & request for payment emailed to $contact_name($email)", $assessment_id, $establishment_code);
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }

        echo "<script>j_P.J_WTS(j_W,500,100)</script>";
        echo "<div style='text-align: center'>Request for payment sent.</div>";
    } else {

		//update enter date on first assessment
		if ($assessment_type_2 == 'cc') {
			if (!isset($cc_enter_date) || empty($cc_enter_date) || $cc_enter_date == '0000-00-00') {
				$statement = "UPDATE nse_establishment SET cc_enter_date=? WHERE establishment_code=?";
				$sql_update_enter_date = $GLOBALS['dbCon']->prepare($statement);
				$sql_update_enter_date->bind_param('ss', $assessment_date_ymd, $establishment_code);
				$sql_update_enter_date->execute();
				$sql_update_enter_date->close();
			}
		} else {
			if (!isset($enter_date) || empty($enter_date) || $enter_date == '0000-00-00') {
				$statement = "UPDATE nse_establishment SET enter_date=? WHERE establishment_code=?";
				$sql_update_enter_date = $GLOBALS['dbCon']->prepare($statement);
				$sql_update_enter_date->bind_param('ss', $assessment_date_ymd, $establishment_code);
				$sql_update_enter_date->execute();
				$sql_update_enter_date->close();
			}
		}

		//Clear QA Out field
		$statement ="UPDATE nse_establishment_qa_cancelled SET cancelled_date='' WHERE establishment_code=?";
		$sql_qa = $GLOBALS['dbCon']->prepare($statement);
		$sql_qa->bind_param('s', $establishment_code);
		$sql_qa->execute();
		$sql_qa->close();

        $report_file = create_report($assessment_id);
        $certificate_file = create_certificate($assessment_id);
        $email_counter = 1;

        $certificate_file = $_SERVER['DOCUMENT_ROOT'] . $certificate_file;
        $report_file = $_SERVER['DOCUMENT_ROOT'] . $report_file;

        $from_year = (int) $renewal_year - 1;
        $from_date = $renewal_month . ' ' . $from_year;
        $to_date = $renewal_month . ' ' . $renewal_year;

        //Get email template
        $email_template = file_get_contents(SITE_ROOT . '/juno/apps/assessment_manager/f/html/welcome_pack_email.html');

        //Replace Tags
        $email_template = str_replace('<!-- visit_date -->', $assessment_date, $email_template);
        $email_template = str_replace('<!-- status -->', $status, $email_template);
        $email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);
        $email_template = str_replace('<!-- establishment_link -->', $establishment_link, $email_template);
        $email_template = str_replace('<!-- establishment_code -->', $establishment_code, $email_template);
        $email_template = str_replace('<!-- from_date -->', $from_date, $email_template);
        $email_template = str_replace('<!-- to_date -->', $to_date, $email_template);

        //Prepare statement - user details
        $statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
        $sql_email = $GLOBALS['dbCon']->prepare($statement);

        if (isset($_POST['emails']) && !empty($_POST['emails'])) {
            foreach ($_POST['emails'] AS $uID) {
                //Get details
                $contact_name = '';
                $encrypted_password = '';
                $sql_email->bind_param('s', $uID);
                $sql_email->execute();
                $sql_email->store_result();
                $sql_email->bind_result($contact_name, $email);
                $sql_email->fetch();
                $sql_email->free_result();

                if (empty($contact_name)) $contact_name = 'QA Member';
                $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

                try {
                    $mail->ClearAllRecipients();
                    $mail->AddAddress($email, $contact_name);
                    $mail->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
                    $mail->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
                    $mail->Subject = "Your AA Quality Assured Status is hereby confirmed for $establishment_name";
                    $mail->MsgHTML($email_content);
                    $mail->AddAttachment($certificate_file, 'certificate.pdf');
                    $mail->AddAttachment($report_file, 'report.pdf');
                    $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$logo");
                    $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/documents/AAQA_ORDER_SIGNAGE.doc");
                    $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                    $mail->Send();
                    J_act('ASS MANAGER', 7, "QA approved & welcome pack emailed to $contact_name($email)", $assessment_id, $establishment_code);
                    if ($email_counter == 1) $assessor_email_body = $email_content;
                    $email_counter++;
                } catch (phpmailerException $e) {
                    echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
			mail('rory@rmfweb.com', "Error Message in ". __FILE__ ." (PHPMailer)", __LINE__ . ' :: ' . $e->errorMessage());
                } catch (Exception $e) {
                    echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
			mail('rory@rmfweb.com', "Error Message in ". __FILE__ ." (PHPMailer)", __LINE__ . ' :: ' . $e->errorMessage());
                }
            }
        }


        if (isset($_POST['assessor_email']) && !empty($_POST['assessor_email'])) {

            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('i', $_POST['assessor_email']);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            $contact_name = 'QA Member';
            $assessor_email_body = str_replace('<!-- contact_name -->', $contact_name, $assessor_email_body);

            try {
                $mail->ClearAllRecipients();
                $mail->ClearAttachments();
                $mail->AddAddress($email, $assessor_name);
                $mail->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->Subject = "COPY :: AA Quality Assured Status for $establishment_name";
                $mail->MsgHTML($assessor_email_body);
                $mail->AddAttachment($certificate_file, 'certificate.pdf');
                $mail->AddAttachment($report_file, 'report.pdf');
                $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                $mail->Send();
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }

		if (isset($_POST['email_john'])) {

            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('i', $_POST['email_john']);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            $contact_name = 'QA Member';
            $assessor_email_body = str_replace('<!-- contact_name -->', $contact_name, $assessor_email_body);

            try {
                $mail->ClearAllRecipients();
                $mail->ClearAttachments();
                $mail->AddAddress($email, $assessor_name);
                $mail->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->Subject = "COPY :: AA Quality Assured Status for $establishment_name";
                $mail->MsgHTML($assessor_email_body);
                $mail->AddAttachment($certificate_file, 'certificate.pdf');
                $mail->AddAttachment($report_file, 'report.pdf');
                $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/modules/assessment_s/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                $mail->Send();
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }

        J_act('ASS MANAGER', 4, "Assessment Approved ($establishment_code)", $assessment_id);

        echo "<script>j_P.j_Wi[j_W]['width']= '500px'; j_P.j_Wi[j_W]['table'].style.width='500px';</script>";
        echo "<script>j_P.j_Wi[j_W]['height']= '100px'; j_P.j_Wi[j_W]['table'].style.height='100px';</script>";
        echo "<div style='text-align: center'>Assessment approved & welcome pack sent.</div>";
        echo '<script>setTimeout("j_P.J_WX(self)",2000)</script>';
    }

}


echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>J_tr()</script>";


?>
