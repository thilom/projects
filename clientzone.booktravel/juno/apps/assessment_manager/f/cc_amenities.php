<?php
/**
 * Management section of assessments
 *
 * @author Thilo Muller(2010,2011)
 * @version $Id: management.php 200 2011-02-08 11:40:01Z web37 $
 */


include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/f/cc_amenities.js></script>";

if(isset($_GET["j_hide"]))
{
	unset($_GET["j_hide"]);
	echo "<script>J_start(".$_GET["id"].")</script>";
	die();
}

echo "<script src=".$ROOT."/apps/assessment_manager/f/saver.js></script>";
echo "<body>";

$assessment_id=isset($_GET["eid"])?$_GET["eid"]:0;

//Get assessment Status
$statement = "SELECT assessment_status FROM nse_establishment_assessment WHERE assessment_id=? LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('i', $assessment_id);
$sql_assessment->execute();
$sql_assessment->bind_result($status);
$sql_assessment->fetch();
$sql_assessment->free_result();
$sql_assessment->close();

//Get template
if ($status == 'complete' || $status == 'waiting') {
    if ($status == 'waiting' && $_SESSION['j_user']['role'] == 's') {
        $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/cc_management_staff_edit.html");
    } else {
        $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/cc_management_view_only.html");
    }
} else if ($_SESSION['j_user']['role'] == 's' || $_SESSION['j_user']['role'] == 'd') {
    $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/cc_management_staff_edit.html");
} else if ($_SESSION['j_user']['role'] == 'a') {
    $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/cc_management_staff_edit.html");
} else {
    $template = '';
}

$areas = array('cc_grounds',
                'cc_sites',
                'cc_services',
                'cc_public_areas',
                'cc_security',
                'cc_signage',
                'cc_ablution',
				'cc_entertainment');

include 'cc_areas.php';

echo "</body></html>";
?>