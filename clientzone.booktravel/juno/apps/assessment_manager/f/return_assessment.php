<?php

/**
 * Sets an assessment as 'Returned to Assessor'.
 *
 * @version $Id: return_assessment.php 170 2011-02-04 04:37:15Z web37 $
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include_once $SDR."/system/activity.php";

//Vars
$assessment_id = $_GET['id'];

if (isset($_POST['save'])) {
    return_save();
} else {
    return_form();
}

function return_form() {

    //Get template
    $template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/juno/apps/assessment_manager/f/html/return_assessment_form.html");

    echo $template;
}

function return_save() {
    global $assessment_id;
    $establishment_code = '';
	$user_name = '';
	$time = mktime();

    $statement = "UPDATE nse_establishment_assessment SET assessment_status='returned', returned_by=?, return_date=NOW() WHERE assessment_id=?";
    $sql_update = $GLOBALS['dbCon']->prepare($statement);
    echo mysqli_error($GLOBALS['dbCon']);
    $sql_update->bind_param('ii', $_SESSION['j_user']['id'], $assessment_id);
    $sql_update->execute();
    $sql_update->close();

    //Get establishment code
    $statement = "SELECT establishment_code FROM nse_establishment_assessment WHERE assessment_id=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $assessment_id);
    $sql_estab->execute();
    $sql_estab->bind_result($establishment_code);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();

    //Assemble description
    $description = "Assessment returned to assessor: {$_POST['return_note']}";

    J_act('ASS MANAGER', 4, $description, $assessment_id, $establishment_code);

	//Get username
	$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bind_param('i', $_SESSION['j_user']['id']);
	$sql_user->execute();
	$sql_user->bind_result($user_name);
	$sql_user->fetch();
	$sql_user->close();

	//Add entry in notes
	$statement = "INSERT INTO nse_establishment_notes (establishment_code, user_name, note_timestamp, note_content, module_name) VALUES (?,?,?,?,'assessments')";
	$sql_notes = $GLOBALS['dbCon']->prepare($statement);
	$sql_notes->bind_param('ssss', $establishment_code, $user_name, $time, $description);
	$sql_notes->execute();
	$sql_notes->close();

    echo "<script>j_P.j_Wi[j_W]['width']= '500px'; j_P.j_Wi[j_W]['table'].style.width='500px';</script>";
    echo "<script>j_P.j_Wi[j_W]['height']= '100px'; j_P.j_Wi[j_W]['table'].style.height='100px';</script>";
    echo "<div style='text-align: center'>Assessment Returned to Assessor</div>";
    echo '<script>setTimeout("j_P.J_WX(self)",2000)</script>';
}




echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>J_tr()</script>";
?>
