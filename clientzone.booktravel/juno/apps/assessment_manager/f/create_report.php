<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/tcpdf.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/fpdi/fpdi.php';
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';
include_once $SDR."/system/activity.php";

echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/f/assessment.js></script>";


//Vars
$assessor = '';
$assessment_date = '';
$assessment_name = '';
$assessment_content = '';
$assessment_id = $_REQUEST['id'];
$category = '';
$establishment_name = '';
$endorsement = '';
$province_name = '';
$qa_logo = '';
$renewal_date = '';
$town_name = '';

$mail = new PHPMailer();

if (isset($_POST['save'])) {
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    //Get Assessor details
    $statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('i', $_SESSION['j_user']['id']);
    $sql_assessor->execute();
    $sql_assessor->bind_result($assessor_name, $assessor_email);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();

    //Get Assessment Status
    $statement = "SELECT a.aa_category_name, a.pdf_image, DATE_FORMAT(b.renewal_date, '%M %Y'), establishment_code
                    FROM nse_aa_category AS a
                    JOIN nse_establishment_assessment AS b ON b.qa_status=a.aa_category_code
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($endorsement, $qa_logo, $assessment_date, $establishment_code);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();

    $report_file = strtolower(str_replace(' ', '_', "$establishment_code report $assessment_date"));
    $report_file = $_SERVER['DOCUMENT_ROOT'] . "/reports/$report_file.pdf";



    //Prepare statement - email address details
	$statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
	$sql_email = $GLOBALS['dbCon']->prepare($statement);
    if (isset($_POST['emails']) && !empty($_POST['emails'])) {
        foreach ($_POST['emails'] AS $uID) {
            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('s', $uID);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            if (empty($contact_name)) $contact_name = 'QA Member';

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom($assessor_email, $assessor_name);
                $mail->AddReplyTo($assessor_email, $assessor_name);
                $mail->Subject = $subject;
                $mail->MsgHTML($message);
                $mail->AddAttachment($report_file, 'assessment_report.pdf');
                $mail->Send();
                J_act('ASS MANAGER', 7, "Client report emailed to $contact_name($email)", $assessment_id, $establishment_code);
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }
    }


    if (isset($_POST['assessor_email'])) {

        //Get details
        $contact_name = '';
        $encrypted_password = '';
        $sql_email->bind_param('i', $_POST['assessor_email']);
        $sql_email->execute();
        $sql_email->store_result();
        $sql_email->bind_result($contact_name, $email);
        $sql_email->fetch();
        $sql_email->free_result();

        if (empty($contact_name)) $contact_name = 'QA Member';

        try {
            $mail->ClearAllRecipients();
            $mail->AddAddress($email, $contact_name);
            $mail->SetFrom($assessor_email, $assessor_name);
            $mail->AddReplyTo($assessor_email, $assessor_name);
            $mail->Subject = $subject;
            $mail->MsgHTML($message);
            $mail->AddAttachment($report_file, 'certificate.pdf');
            $mail->Send();
        } catch (phpmailerException $e) {
            echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
        }
    }

    echo "<script>j_P.j_Wi[j_W]['width']= '500px'; j_P.j_Wi[j_W]['table'].style.width='500px';</script>";
    echo "<script>j_P.j_Wi[j_W]['height']= '100px'; j_P.j_Wi[j_W]['table'].style.height='100px';</script>";
    echo "<div style='text-align: center'>Report Sent to Contacts</div>";
    echo '<script>setTimeout("j_P.J_WX(self)",2000)</script>';

} else {



    //Get Assessment Status
    $statement = "SELECT a.aa_category_name, a.pdf_image, DATE_FORMAT(b.renewal_date, '%M %Y'), DATE_FORMAT(b.assessment_date, '%d %M %Y'), b.assessor_name, b.establishment_code
                    FROM nse_aa_category AS a
                    JOIN nse_establishment_assessment AS b ON b.qa_status=a.aa_category_code
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($endorsement, $qa_logo, $renewal_date, $assessment_date, $assessor, $establishment_code);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();
    if (empty($qa_logo)) $qa_logo = 'Quality Assured Logo_1.jpg';

     //Get Data
    $statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->store_result();
    $sql_data->bind_result($establishment_name);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    //Get Assessment Status
    $statement = "SELECT a.subcategory_name
                    FROM nse_restype_subcategory_lang AS a
                    JOIN nse_establishment_assessment AS b ON b.proposed_category=a.subcategory_id
                    WHERE b.assessment_id=?
                    LIMIT 1";
    $sql_category = $GLOBALS['dbCon']->prepare($statement);
    $sql_category->bind_param('i', $assessment_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($category);
    $sql_category->fetch();
    $sql_category->free_result();
    $sql_category->close();

    //Get Location
    $statement = "SELECT b.town_name, c.province_name
                    FROM nse_establishment_location AS a
                    LEFT JOIN nse_nlocations_towns AS b ON a.town_id=b.town_id
                    LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
                    WHERE a.establishment_code=? LIMIT 1";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);

    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->store_result();
    $sql_location->bind_result($town_name, $province_name);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
    if (!empty($province_name)) $town_name .= ", $province_name";

    //Get Assessor
    $statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";


    $filename = strtolower(str_replace(' ', '_', "$establishment_code report $renewal_date"));

    //Create Report
    $report = new FPDI('P','mm','A4',true,'UTF-8',false);
    $report->SetMargins(5.25, 5.25, 5.25);

    // remove default header/footer
    $report->setPrintHeader(false);
    $report->setPrintFooter(false);

    $report->addPage();
    $report->SetAutoPageBreak(true, 2);
    $report->addFont('vera', '', $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/fonts/vera.php');
    $report->addFont('verabd', '', $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/fonts/verabd.php');

    $report->setJPEGQuality(100);
    $report->Image($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$qa_logo", 5.25, 5.25, 35, 0, '', '', '', true, 150);

    $report->SetFont('verabd', '', 10);
    //$report->setLeftMargin(40);
    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(0, 0, 'Quality Assurred Assessment Report for', 0, 1);
    $report->SetFont('verabd', '', 12);
    $report->ln(1);
    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(0, 0, "$establishment_name - $town_name.", 0, 1);

    $report->SetFont('vera', '', 8);
    $report->ln(5);
    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(30, 0, 'Assessment Date: ', 0, 0);
    $report->Cell(0, 0, $assessment_date, 0, 1);

    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(30, 0, 'Assessor: ', 0, 0);
    $report->Cell(0, 0, $assessor, 0, 1);

    $report->Cell(40, 0, ' ', 0, 0);
    $report->Cell(30, 0, 'Category: ', 0, 0);
    $report->Cell(0, 0, $category, 0, 1);

    $report->ln(30);
    $report->Cell(0, 0, "Our assessor visited your establishment on the $assessment_date and compiled the following report.", 0, 1);


    //Get assessment Data
    $statement = "SELECT b.assessment_area_name, a.assessment_content
                    FROM nse_establishment_assessment_data AS a
                    JOIN nse_assessment_areas AS b ON a.assessment_area_id=b.assessment_area_id
                    WHERE a.not_applicable != '1' && hide_from_client != '1' && a.assessment_id=?
                    ORDER BY b.display_order";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    $sql_data->bind_param('i', $assessment_id);
    $sql_data->execute();
    $sql_data->store_result();
    $sql_data->bind_result($assessment_name, $assessment_content);
    while ($sql_data->fetch()) {
        if (empty($assessment_content)) continue;
        $report->SetFont('verabd', '', 8);
        $report->ln(10);
        $report->Cell(0, 0, $assessment_name, 0, 1);
        $report->SetFont('vera', '', 8);
        $report->MultiCell(0, 0, $assessment_content, 0, 1);
    }
    $sql_data->free_result();
    $sql_data->close();


    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf")) unlink($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf");
    $report->output($_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf", 'F');
    $report->close();

//    $report_file = $_SERVER['DOCUMENT_ROOT'] . "/reports/$filename.pdf";
    $report_file = "/reports/$filename.pdf";
    $report_name = "Assessment Report For $establishment_name ending $assessment_date";

    $template = file_get_contents($SDR . '/apps/assessment_manager/f/html/create_report.html');

    //Replace tags
    $template = str_replace('!report_link!', $report_file, $template);
    $template = str_replace('<!-- report_name -->', $report_name, $template);

    $counter = 0;
    $email_list = "<table width=100% id=jTR style='border: solid 1px black'><tr style='background-color: gray'><th>&nbsp;</th><th>Contact</th><th>Email</th><th>Designation</th></tr>";

    //Get assessment Data
    $statement = "SELECT establishment_code, invoice_id FROM nse_establishment_assessment WHERE assessment_id=?";
    $sql_data = $GLOBALS['dbCon']->Prepare($statement);
    $sql_data->bind_param('i', $GLOBALS['assessment_id']);
    $sql_data->execute();
    $sql_data->bind_result($establishment_code, $invoice_id);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    if ($invoice_id == 0) {
        $message = "<b>NOTE!</b> There is <b style='color: red'>NO INVOICE</b> associated with this assessment. Report will not be sent to selected contacts.";
    } else {
        //Check payment
        $statement = "SELECT j_inv_total_vat, j_inv_paid FROM nse_invoice WHERE j_inv_id=?";
        $sql_invoice = $GLOBALS['dbCon']->prepare($statement);
        echo mysqli_error($GLOBALS['dbCon']);
        $sql_invoice->bind_param('i', $invoice_id);
        $sql_invoice->execute();
        $sql_invoice->bind_result($inv_total, $inv_paid);
        $sql_invoice->fetch();
        $sql_invoice->free_result();
        $sql_invoice->close();

        if ($inv_total != $inv_paid) {
            $message = "<b>NOTE!</b> This assessment has an outstanding invoice. The report will <b>NOT</b> be sent to selected contacts";
        } else {
            $message = "Select recipients for the report below";
        }
    }

    $statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);
    $sql_email->bind_param('s', $establishment_code);
    $sql_email->execute();
    $sql_email->store_result();
    $sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
    while ($sql_email->fetch()) {
        $email_list .= "<tr><td  class=jdbO><input type=checkbox name=emails[] id=email_$counter value='$uID' />";
        $email_list .= "</td><td  class=jdbR><label for=email_$counter >$contact_name</label></td><td  class=jdbY>$contact_email</td><td  class=jdbB>$contact_designation</td></tr>" . PHP_EOL;
        $counter++;
    }
    $sql_email->free_result();
    $sql_email->close();


    //Get Assessor Email Adddress
    $statement = "SELECT b.firstname, b.lastname , b.email, b.user_id
                    FROM nse_establishment AS a
                    JOIN nse_user AS b ON a.assessor_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('s', $establishment_code);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email, $uID);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();
    if (!empty($assessor_email)) {
        $email_list .= "<tr><td class=jdbO><input type=checkbox name=assessor_email id=email_$counter value='$uID' /></td><td class=jdbR><label for=email_$counter >$assessor_firstname $assessor_lastname</label></td><td class=jdbY>$assessor_email</td><td class=jdbB>Assessor</td></tr>" . PHP_EOL;
    }

    $email_list .= '</table >';

    //Replace Tags
    $template = str_replace('<!-- email_list -->', $email_list, $template);
    $template = str_replace('<!-- email_message -->', $message, $template);
    $template = str_replace('!establishment_name!', $establishment_name, $template);
    $template = str_replace('!assessment_id!', $GLOBALS['assessment_id'], $template);

    echo $template;

}
?>