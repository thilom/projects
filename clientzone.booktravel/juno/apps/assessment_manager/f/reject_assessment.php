<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include_once $SDR."/system/activity.php";
require_once $SDR . '/utility/PHPMailer/class.phpmailer.php';
include $SDR."/apps/assessment_manager/f/create_functions.php";

//Vars
$assessment_id = $_GET['id'];


isset($_POST['save'])?reject_save():reject_form();

function reject_form() {
    $counter = 0;
    $email_list = "<table width=100% id=jTR style='border: solid 1px black'><tr style='background-color: gray'><th>&nbsp;</th><th>Contact</th><th>Email</th><th>Designation</th></tr>";
    $message = "<b>NOTE: </b>You are about to mark an assessment as being 'Below Standard'. ";

    //Get assessment Data
    $statement = "SELECT establishment_code, invoice_id FROM nse_establishment_assessment WHERE assessment_id=?";
    $sql_data = $GLOBALS['dbCon']->Prepare($statement);
    $sql_data->bind_param('i', $GLOBALS['assessment_id']);
    $sql_data->execute();
    $sql_data->bind_result($establishment_code, $invoice_id);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    if ($invoice_id == 0) {
        $message .= "There is <b style='color: red'>NO INVOICE</b> associated with this assessment.";
    } else {
        //Check payment
        $statement = "SELECT j_inv_total_vat, j_inv_paid FROM nse_invoice WHERE j_inv_id=?";
        $sql_invoice = $GLOBALS['dbCon']->prepare($statement);
        echo mysqli_error($GLOBALS['dbCon']);
        $sql_invoice->bind_param('i', $invoice_id);
        $sql_invoice->execute();
        $sql_invoice->bind_result($inv_total, $inv_paid);
        $sql_invoice->fetch();
        $sql_invoice->free_result();
        $sql_invoice->close();

        if ($inv_total != $inv_paid) {
            $message .= "This assessment has an outstanding invoice.";
        } else {
            $message .= "";
        }
    }


    $statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);
    $sql_email->bind_param('s', $establishment_code);
    $sql_email->execute();
    $sql_email->store_result();
    $sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
    while ($sql_email->fetch()) {
        $email_list .= "<tr><td  class=jdbO><input type=checkbox name=emails[] id=email_$counter value='$uID' />";
        $email_list .= "</td><td  class=jdbR><label for=email_$counter >$contact_name</label></td><td  class=jdbY>$contact_email</td><td  class=jdbB>$contact_designation</td></tr>" . PHP_EOL;
        $counter++;
    }
    $sql_email->free_result();
    $sql_email->close();


    //Get Assessor Email Adddress
    $statement = "SELECT b.firstname, b.lastname , b.email, b.user_id
                    FROM nse_establishment AS a
                    JOIN nse_user AS b ON a.assessor_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('s', $establishment_code);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email, $uID);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();
    if (!empty($assessor_email)) {
        $email_list .= "<tr><td class=jdbO><input type=checkbox name=assessor_email id=email_$counter value='$uID' /></td><td class=jdbR><label for=email_$counter >$assessor_firstname $assessor_lastname</label></td><td class=jdbY>$assessor_email</td><td class=jdbB>Assessor</td></tr>" . PHP_EOL;
    }

    $email_list .= '</table >';
    $email_list .= "<tt>Leave all contacts unselected if you do not want to send an email.</tt>";
    //$email_list = substr($email_list, 6);

    //Get template
    $template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/juno/apps/assessment_manager/f/html/reject_assessment_form.html");

    //Replace Tags
    $template = str_replace('<!-- email_list -->', $email_list, $template);
    $template = str_replace('<!-- message -->', $message, $template);
    $template = str_replace('!assessment_id!', $GLOBALS['assessment_id'], $template);

    echo $template;
}

function reject_save() {
    $assessment_id = $_GET['id'];
    $assessor_name = '';
    $report_file = create_report($assessment_id);
    $counter = 1;
    $email_counter = 1;

    //Get assessor name & establishment name
    $statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), c.establishment_name, c.establishment_code
        FROM nse_establishment_assessment AS a
	LEFT JOIN nse_establishment AS c ON a.establishment_code=c.establishment_code
        LEFT JOIN nse_user AS b ON c.assessor_id=b.user_id
        WHERE assessment_id=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('i', $assessment_id);
    $sql_assessor->execute();
    $sql_assessor->bind_result($assessor_name, $establishment_name, $establishment_code);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();

	//Update status
	$statement = "UPDATE nse_establishment_assessment
				SET assessment_status = 'below'
				WHERE assessment_id=?
				LIMIT 1";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('i', $assessment_id);
	$sql_status->execute();
	$sql_status->close();

    //Get email template
    $email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/f/html/below_standard_email.html');
    $assessor_email_body = $email_template;

    //Replace Tags
    $email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);

    //Prepare statement - user details
    $statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);

    if (isset($_POST['emails']) && !empty($_POST['emails'])) {
        foreach ($_POST['emails'] AS $uID) {
            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('s', $uID);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            if (empty($contact_name)) $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
	       $mail = new PHPMailer();
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->Subject = 'AA Quality Assurance';
                $mail->MsgHTML($email_content);
                $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $report_file, 'assessment_report.pdf');
                $mail->Send();
                if ($email_counter == 1) $assessor_email_body = $email_content;
                $email_counter++;
                J_act('ASS MANAGER', 7, "Assessment marked 'Below Standard' & emailed to $contact_name($email)", $assessment_id, $establishment_code);
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }
    }

    if (isset($_POST['assessor_email'])) {

        //Get details
        $contact_name = '';
        $encrypted_password = '';
        $sql_email->bind_param('i', $_POST['assessor_email']);
        $sql_email->execute();
        $sql_email->store_result();
        $sql_email->bind_result($contact_name, $email);
        $sql_email->fetch();
        $sql_email->free_result();

        $contact_name = 'QA Member';
        $email_content = str_replace('<!-- contact_name -->', $contact_name, $assessor_email_body);

        try {
	   $mail2= new PHPMailer();
            $mail2->ClearAllRecipients();
            $mail2->ClearAttachments();
            $mail2->AddAddress($email, $assessor_name);
            $mail2->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
            $mail2->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
            $mail2->AddBCC('info@essentialtravelinfo.com', 'Quality Assurance');
            $mail2->Subject = "COPY :: AA Quality Assured Status for $establishment_name";
            $mail2->MsgHTML($email_content);
            $mail2->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
            $mail2->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $report_file, 'assessment_report.pdf');
            $mail2->Send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    echo "<script>j_P.j_Wi[j_W]['width']= '500px'; j_P.j_Wi[j_W]['table'].style.width='500px';</script>";
    echo "<script>j_P.j_Wi[j_W]['height']= '100px'; j_P.j_Wi[j_W]['table'].style.height='100px';</script>";
    echo "<div style='text-align: center'>Assessment marked as 'Below Standard'</div>";
    echo '<script>setTimeout("j_P.J_WX(self)",2000)</script>';
}





echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>J_tr()</script>";
?>
