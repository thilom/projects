<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';
include_once $SDR."/system/activity.php";
include $SDR."/apps/assessment_manager/f/create_functions.php";

include $SDR."/system/get.php";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

//Vars
$assessment_id = $_GET['id'];


if (isset($_POST['save'])) {
    resend_form_save();
} else {
    resend_form_show();
}

function resend_form_show() {
    $counter = 0;
    $email_list = "<TABLE width=100% id=jTR style='border: solid 1px black'><tr style='background-color: gray'><td>&nbsp;</td><td>Contact</td><td>Email</td><td>Designation</td></tr>";
    $req = '';

    //Get assessment Data
    $statement = "SELECT establishment_code, invoice_id FROM nse_establishment_assessment WHERE assessment_id=?";
    $sql_data = $GLOBALS['dbCon']->Prepare($statement);
    $sql_data->bind_param('i', $GLOBALS['assessment_id']);
    $sql_data->execute();
    $sql_data->bind_result($establishment_code, $invoice_id);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();

    if ($invoice_id == 0) {
        $message = "<b style='color: red'>WARNING! </b>There is <b style='color: red'>NO INVOICE</b> associated with this assessment. The welcome pack should only be sent if you are sure the client is paid up.";
    } else {
        //Check payment
        $statement = "SELECT j_inv_total_vat, j_inv_paid FROM nse_invoice WHERE j_inv_id=?";
        $sql_invoice = $GLOBALS['dbCon']->prepare($statement);
        $sql_invoice->bind_param('i', $invoice_id);
        $sql_invoice->execute();
        $sql_invoice->bind_result($inv_total, $inv_paid);
        $sql_invoice->fetch();
        $sql_invoice->free_result();
        $sql_invoice->close();

        if ($inv_total != $inv_paid) {
            $message = "<b style='color: red'>WARNING! </b>This assessment has an outstanding invoice. The welcome pack should only be sent if you are sure the client is paid up.";
            $req = "<tt><br><input type=checkbox name=req value=1> Send 'Request for Payment' instead of welcome pack</tt>";
        } else {
            $message = "This assessment is fully paid and the selected contacts will receive a welcome pack.";

        }
    }


    $statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);
    $sql_email->bind_param('s', $establishment_code);
    $sql_email->execute();
    $sql_email->store_result();
    $sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
    while ($sql_email->fetch()) {
        $email_list .= "<tr><td class=jdbO><input type=checkbox name=emails[] id=email_$counter value='$uID' >";
        $email_list .= "</td><td class=jdbR><label for=email_$counter >$contact_name</label></td><td class=jdbY>$contact_email</td><td class=jdbB>$contact_designation</td></tr>" . PHP_EOL;
        $counter++;
    }
    $sql_email->free_result();
    $sql_email->close();


    //Get Assessor Email Adddress
    $statement = "SELECT b.firstname, b.lastname , b.email, b.user_id
                    FROM nse_establishment AS a
                    JOIN nse_user AS b ON a.assessor_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('s', $establishment_code);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email, $uID);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();
    if (!empty($assessor_email)) {
        $email_list .= "<tr><td class=jdbO><input type=checkbox name=assessor_email id=email_$counter value='$uID' ></td><td class=jdbR><label for=email_$counter >$assessor_firstname $assessor_lastname</label></td><td class=jdbY>$assessor_email</td><td class=jdbB>Assessor</td></tr>" . PHP_EOL;
    }

    $email_list .= '</TABLE >';
    //$email_list = substr($email_list, 6);

    //Get template
    $template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/juno/apps/assessment_manager/f/html/assessment_email_form.html");

    //Replace Tags
    $template = str_replace('<!-- email_list -->', $email_list, $template);
    $template = str_replace('<!-- message -->', $message, $template);
    $template = str_replace('<!-- req -->', $req, $template);
    $template = str_replace('!assessment_id!', $GLOBALS['assessment_id'], $template);

    echo $template;
}

function resend_form_save() {
    global $assessment_id;

    $mail = new PHPMailer();

    //Get assessment Data
    $statement = "SELECT establishment_code, invoice_id FROM nse_establishment_assessment WHERE assessment_id=?";
    $sql_data = $GLOBALS['dbCon']->Prepare($statement);
    $sql_data->bind_param('i', $GLOBALS['assessment_id']);
    $sql_data->execute();
    $sql_data->bind_result($establishment_code, $invoice_id);
    $sql_data->fetch();
    $sql_data->free_result();
    $sql_data->close();


    //Get Assessment Data
    $statement = "SELECT DATE_FORMAT(a.assessment_date, '%d %M %Y'), b.aa_category_name, b.lowres_image,
					DATE_FORMAT(a.renewal_date,'%M'), DATE_FORMAT(a.renewal_date, '%Y'), a.assessment_type_2,
					assessment_status
                    FROM nse_establishment_assessment AS a
                    JOIN nse_aa_category AS b ON a.qa_status=b.aa_category_code
                    WHERE a.assessment_id=? LIMIT 1";
    $sql_assessment = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessment->bind_param('i', $assessment_id);
    $sql_assessment->execute();
    $sql_assessment->store_result();
    $sql_assessment->bind_result($assessment_date, $status, $logo, $renewal_month, $renewal_year, $assessment_type_2, $assessment_status);
    $sql_assessment->fetch();
    $sql_assessment->free_result();
    $sql_assessment->close();

    //Get assessor
    $statement = "SELECT CONCAT(b.firstname,' ', b.lastname), b.email
                    FROM nse_establishment AS a
                    LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
                    WHERE a.establishment_code=?
                    LIMIT 1";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('s', $establishment_code);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($assessor_name, $assessor_email);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();

    //Get establishment_name
    $statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
    $sql_estab->bind_param('s', $establishment_code);
    $sql_estab->execute();
    $sql_estab->store_result();
    $sql_estab->bind_result($establishment_name);
    $sql_estab->fetch();
    $sql_estab->free_result();
    $sql_estab->close();

    //Assemble Establishment link
    $establishment_link = $establishment_code . "_$establishment_name";
    $establishment_link = strtolower(str_replace(' ', '_', $establishment_link));
    $establishment_link = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $establishment_link);
    $establishment_link = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $establishment_link );
    $establishment_link = htmlentities($establishment_link);
    $establishment_link = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $establishment_link);
    $establishment_link = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $establishment_link);
    $establishment_link = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $establishment_link);
    $establishment_link = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $establishment_link);
    $establishment_link = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $establishment_link);
    $establishment_link = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $establishment_link);
    $establishment_link .= ".html";


    if (isset($_POST['req'])) {
        //Get email template
        $email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/f/html/assessment_unpaid_email.html');

        //Replace Tags
        $email_template = str_replace('<!-- visit_date -->', $assessment_date, $email_template);
        $email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);

        //Prepare statement - user details
        $statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
        $sql_email = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST['emails'] AS $uID) {
            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('s', $uID);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            if (empty($contact_name)) $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom($assessor_email, $assessor_name);
                $mail->AddReplyTo($assessor_email, $assessor_name);
                $mail->Subject = "AA Quality Assurance for $establishment_name";
                $mail->MsgHTML($email_content);
                $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                $mail->Send();
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }


        if (isset($_POST['assessor_email'])) {

            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('i', $_POST['assessor_email']);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom($assessor_email, $assessor_name);
                $mail->AddReplyTo($assessor_email, $assessor_name);
                $mail->Subject = "AA Quality Assurance for $establishment_name - Request for Payment";
                $mail->MsgHTML($email_content);
                $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                $mail->Send();
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }

        echo "<script>j_P.j_Wi[j_W]['width']= '500px'; j_P.j_Wi[j_W]['table'].style.width='500px';</script>";
        echo "<script>j_P.j_Wi[j_W]['height']= '100px'; j_P.j_Wi[j_W]['table'].style.height='100px';</script>";
        echo "<div style='text-align: center'>Request for payment sent.</div>";
        echo '<script>setTimeout("j_P.J_WX(self)",2000)</script>';
    } else {
        $report_file = create_report($assessment_id);
        $certificate_file = $assessment_type_2=='cc'?create_certificate_cc($assessment_id):create_certificate($assessment_id);
        $email_counter = 1;

        $certificate_file = $_SERVER['DOCUMENT_ROOT'] . $certificate_file;
        $report_file = $_SERVER['DOCUMENT_ROOT'] . $report_file;

        $from_year = (int) $renewal_year - 1;
        $from_date = $renewal_month . ' ' . $from_year;
        $to_date = $renewal_month . ' ' . $renewal_year;

        //Get email template
		echo $assessment_status;
		if ($assessment_status == 'below') {
			$email_template = file_get_contents(SITE_ROOT . '/juno/apps/assessment_manager/f/html/below_standard_email.html');
		} else {
			$email_template = file_get_contents(SITE_ROOT . '/juno/apps/assessment_manager/f/html/welcome_pack_email.html');
		}

        //Replace Tags
        $email_template = str_replace('<!-- visit_date -->', $assessment_date, $email_template);
        $email_template = str_replace('<!-- status -->', $status, $email_template);
        $email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);
        $email_template = str_replace('<!-- establishment_link -->', $establishment_link, $email_template);
        $email_template = str_replace('<!-- establishment_code -->', $establishment_code, $email_template);
        $email_template = str_replace('<!-- from_date -->', $from_date, $email_template);
        $email_template = str_replace('<!-- to_date -->', $to_date, $email_template);


        //Prepare statement - user details
        $statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
        $sql_email = $GLOBALS['dbCon']->prepare($statement);

        if (isset($_POST['emails']) && !empty($_POST['emails'])) {
            foreach ($_POST['emails'] AS $uID) {
                //Get details
                $contact_name = '';
                $encrypted_password = '';
                $sql_email->bind_param('s', $uID);
                $sql_email->execute();
                $sql_email->store_result();
                $sql_email->bind_result($contact_name, $email);
                $sql_email->fetch();
                $sql_email->free_result();

                if (empty($contact_name)) $contact_name = 'QA Member';
                $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

                try {
                    $mail->ClearAllRecipients();
                    $mail->AddAddress($email, $contact_name);
                    $mail->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
                    $mail->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
                    $mail->Subject = "Your AA Quality Assured Status is hereby confirmed for $establishment_name";
                    $mail->MsgHTML($email_content);
                    $mail->AddAttachment($certificate_file, 'certificate.pdf');
                    $mail->AddAttachment($report_file, 'report.pdf');
                    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$logo")) $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/i/qa_logos/$logo");
                    $mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . "/documents/AAQA_ORDER_SIGNAGE.doc");
                    $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                    $mail->Send();
                    if ($email_counter == 1) $assessor_email_body = $email_content;
                    $email_counter++;

                    J_act('ASS MANAGER', 7, "Welcome pack resent to $contact_name($email)", $assessment_id, $establishment_code);


                } catch (phpmailerException $e) {
                    echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
                } catch (Exception $e) {
                    echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
                }
            }
        }


        if (isset($_POST['assessor_email'])) {

            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('i', $_POST['assessor_email']);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            $contact_name = 'QA Member';

			//Get email template
			$assessor_email_body = file_get_contents(SITE_ROOT . '/juno/apps/assessment_manager/f/html/welcome_pack_email_assessor.html');

			//Replace Tags
			$assessor_email_body = str_replace('<!-- visit_date -->', $assessment_date, $assessor_email_body);
			$assessor_email_body = str_replace('<!-- status -->', $status, $assessor_email_body);
			$assessor_email_body = str_replace('<!-- assessor_name -->', $assessor_name, $assessor_email_body);
			$assessor_email_body = str_replace('<!-- establishment_link -->', $establishment_link, $assessor_email_body);
			$assessor_email_body = str_replace('<!-- establishment_code -->', $establishment_code, $assessor_email_body);
			$assessor_email_body = str_replace('<!-- from_date -->', $from_date, $assessor_email_body);
			$assessor_email_body = str_replace('<!-- to_date -->', $to_date, $assessor_email_body);

            try {
                $mail->ClearAllRecipients();
                $mail->ClearAttachments();
                $mail->AddAddress($email, $assessor_name);
                $mail->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
                $mail->Subject = "COPY :: AA Quality Assured Status for $establishment_name";
                $mail->MsgHTML($assessor_email_body);
                $mail->AddAttachment($certificate_file, 'certificate.pdf');
                $mail->AddAttachment($report_file, 'report.pdf');
                $mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
                $mail->Send();
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }


        echo "<script>j_P.j_Wi[j_W]['width']= '500px'; j_P.j_Wi[j_W]['table'].style.width='500px';</script>";
        echo "<script>j_P.j_Wi[j_W]['height']= '100px'; j_P.j_Wi[j_W]['table'].style.height='100px';</script>";
        echo "<div style='text-align: center'>Welcome pack sent.</div>";
//        echo '<script>setTimeout("j_P.J_WX(self)",2000)</script>';
    }


}


echo "<html>";


echo "<script>J_tr()</script>";
echo "<script>J_footer('<a href=go: onclick=\"return false\" onfocus=blur() onmouseover=\"J_TT(this,\'Establishment manager\')\" class=jW3a><img src=/juno/ico/set/gohome-64.png></a>')</script>";





 ?>