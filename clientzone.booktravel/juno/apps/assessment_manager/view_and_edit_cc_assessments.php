<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$SUB_ACCESS=array("approve-assessments"=>0,"re-open-assessments"=>0,"re-send-welcome-pack"=>0,"view-and-send-certificate"=>0,"view-and-send-client-report"=>0);
include $SDR."/system/secure.php";
$disp = '';
$assessment_id=isset($_GET["id"])?$_GET["id"]:0;
$eid=isset($_GET["eid"])?$_GET["eid"]:0;
$ust = '0';
$message = '';
$camp_sites = '';
$caravan_sites = '';
$caravan_camping_sites = '';


echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/f/cc_assessment.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/f/saver.js></script>";
echo "<body>";

if (isset($_POST['save'])) {
    $renewal_date = isset($_POST['renewal_date'])?$_POST['renewal_date']:'';
    $assessment_type = $_POST['assessment_type'];
    $proposed_status = $_POST['qa_status'];
//    $proposed_category = $_POST['proposed_category'];
    $caravan_sites = $_POST['caravan_sites'];
    $camp_sites = $_POST['camp_sites'];
    $caravan_camping_sites = $_POST['caravan_camping_sites'];

    //Save Data
    $statement = "UPDATE nse_establishment_assessment SET assessment_type=?, qa_status=?, camp_sites=?, caravan_sites=?, caravan_camping_sites=?, awards_potential=? ";
    if (!empty($renewal_date)) {
        $renewal_date = "01 $renewal_date";
        $renewal_date = date('Y-m-d', strtotime($renewal_date));
        $statement .= ", renewal_date='$renewal_date'";
    }
    $statement .= " WHERE assessment_id=?";
    $sql_update = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
    $sql_update->bind_param('ssssssi', $assessment_type, $proposed_status, $camp_sites, $caravan_sites, $caravan_camping_sites, $award_potential, $assessment_id);
    $sql_update->execute();
}

//Get assessment Data
$statement = "SELECT establishment_code, DATE_FORMAT(assessment_date, '%d %M %Y %H:%i'), assessment_status, 
					assessor_name, DATE_FORMAT(renewal_date, '%M %Y'), assessment_type, qa_status, proposed_category, 
					DATE_FORMAT(submitted_date,'%W %d %M %Y'), camp_sites, caravan_sites, caravan_camping_sites
                    FROM nse_establishment_assessment WHERE assessment_id=?
                    LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('i', $assessment_id);
$sql_assessment->execute();
$sql_assessment->bind_result($establishment_code, $assessment_date, $status, $assessor_name, $renewal_date, 
								$assessment_type, $qa_status, $proposed_category, $submitted_date, $camp_sites, 
								$caravan_sites, $caravan_camping_sites);
$sql_assessment->fetch();
$sql_assessment->free_result();
$sql_assessment->close();

//Assessment Date
if (empty($assessment_date)) $assessment_date = '<span style="color: red; font-size: 10pt">WARNING! This assessment has not been scheduled</span>';

//Get establishment name
$statement = "SELECT establishment_name, assessor_id, cc_enter_date, website_url FROM nse_establishment WHERE establishment_code=? LIMIT 1";
$sql_name = $GLOBALS['dbCon']->prepare($statement);
$sql_name->bind_param('s', $establishment_code);
$sql_name->execute();
$sql_name->bind_result($establishment_name, $assessor_id, $enter_date, $website_url);
$sql_name->fetch();
$sql_name->free_result();
$sql_name->close();

//Get Assessor name
if (empty($assessor_name)) {
    $statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('i', $assessor_id);
    $sql_assessor->execute();
    $sql_assessor->bind_result($assessor_name);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();
}

//Calculate renewal date
if (empty($renewal_date)) {
    $statement = "SELECT DATE_FORMAT(enter_date, '%m') FROM nse_establishment WHERE establishment_code=?";
    $sql_renewal = $GLOBALS['dbCon']->prepare($statement);
    $sql_renewal->bind_param('s', $establishment_code);
    $sql_renewal->execute();
    $sql_renewal->store_result();
    $sql_renewal->bind_result($renewal_month);
    $sql_renewal->fetch();
    $sql_renewal->free_result();
    $sql_renewal->close();
    $current_year = date('Y');
    if ($renewal_month == '00' || empty($renewal_month)) {
        $renewal_date = date('F Y', strtotime('today +1 year'));
		$db_renewal_date = date('Y-m-d', strtotime('today +1 year'));
    } else {
		$current_month = date('n');
		$this_year = date('Y');
		$renew_year = (int) $renewal_month < $current_month?$this_year+1:$this_year;
        $renewal_date = $GLOBALS['months'][(int) $renewal_month] . " $renew_year" ;
		$db_renewal_date = "$renew_year-$renewal_month-01";
    }


	//Insert renewal Date
	$statement = "UPDATE nse_establishment_assessment SET renewal_date=? WHERE assessment_id=?";
	$sql_renewal_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_renewal_insert->bind_param('ss', $db_renewal_date, $assessment_id);
	$sql_renewal_insert->execute();
	$sql_renewal_insert->close();

}

//Assessment types
$type_list = '';
$type_list_static = '';
$types = array('new establishment'=>'New Establishment', 'award visit'=>'Awards Visit', 'renewal'=>'Renewal');
foreach ($types as $type_id=>$type_name) {
    if ($assessment_type == $type_id) {
        $type_list .= "<option value='$type_id' selected>$type_name</option>";
        $type_list_static = $type_name;
    } else {
        $type_list .= "<option value='$type_id' >$type_name</option>";
    }
}

//Proposed Status
$qa_list = '';
$qa_list_static = '';
$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category WHERE active='Y' ORDER BY display_order";
$sql_qa = $GLOBALS['dbCon']->prepare($statement);
$sql_qa->execute();
$sql_qa->store_result();
$sql_qa->bind_result($qa_code, $qa_name);
while ($sql_qa->fetch()) {
    $qa_list .= "<option value='$qa_code' ";
    $qa_list .= $qa_code==$qa_status?' selected':'';
    $qa_list .= " >$qa_name</option>";
    if ($qa_code==$qa_status) $qa_list_static = $qa_name;
}
$sql_qa->free_result();
$sql_qa->close();

//Get category list
$category_select = '';
$category_select_static = '';
$statement = "SELECT subcategory_id, subcategory_name FROM nse_restype_subcategory_lang WHERE language_code='EN' ORDER BY subcategory_name";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->execute();
$sql_category->store_result();
$sql_category->bind_result($category_id, $category_name);
while ($sql_category->fetch()) {
    $category_select .= "<option value='$category_id' ";
    if ($category_id == $proposed_category) {
        $category_select .= "selected ";
        $category_select_static = $category_name;
    }
    $category_select .= "> $category_name </option>";
}
$sql_category->free_result();
$sql_category->close();

//Assemble website url
$website_url = trim($website_url);
if (substr($website_url, 0, 7) == 'http://') $website_url = substr($website_url,7);
if (!empty($website_url)) {
    $website_url = "<a href='http://$website_url' target=_blank >http://$website_url</a>";
} else {
    $website_url = "No Website Listed";
}


//Get caravan data and copy to assessments table
if (empty($camp_sites) && empty($caravan_sites) && empty($caravan_camping_sites)) {
	$statement = "SELECT camp_sites, caravan_sites, caravan_camping_sites 
					FROM nse_establishment_data 
					WHERE establishment_code=? 
					LIMIT 1";
	$sql_camp = $GLOBALS['dbCon']->prepare($statement);
	$sql_camp->bind_param('s', $establishment_code);
	$sql_camp->execute();
	$sql_camp->bind_result($camp_sites, $caravan_sites, $caravan_camping_sites);
	$sql_camp->fetch();
	$sql_camp->close();
	
	if (!empty($camp_sites) && !empty($caravan_sites) && !empty($caravan_camping_sites)) {
		$statement = "UPDATE nse_establishment_assessment SET camp_sites=?, caravan_sites=?, caravan_camping_sites=? WHERE assessment_id=? LIMIT 1";
		$sql_camp_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_camp_insert->bind_param('iiii', $camp_sites, $caravan_sites, $caravan_camping_sites, $assessment_id);
		$sql_camp_insert->execute();
		$sql_camp_insert->close();
	}
}


//Get template
if ($status == 'complete') {
    $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/details_cc_view_only.html");
} else if ($_SESSION['j_user']['role'] == 's' || $_SESSION['j_user']['role'] == 'd' ) {
    $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/details_cc_staff_edit.html");
} else if ($_SESSION['j_user']['role'] == 'a') {
    if ($status == 'waiting') {
        $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/details_cc_view_only.html");
    } else {
        $template = file_get_contents($SDR . "/apps/assessment_manager/f/html/details_cc_assessor_edit.html");
    }
} else {
    $template = '';
}



if ($status == 'waiting') {
    $ust = '1';
    $message = "<tt><b>NOTE!</b> This assessment is waiting for approval by head office.";
    if (!empty($submitted_date)) $message .= " Submitted $submitted_date.";
    $message .= "</tt><hr>";
}

if ($status == 'complete') $ust = '2';

if ($status == 'returned') {
    $statement = "SELECT j_act_description FROM nse_activity WHERE j_act_name='ASS MANAGER' && j_act_ref=? && j_act_description LIKE '%Assessment returned%' LIMIT 1";
    $sql_returned = $GLOBALS['dbCon']->prepare($statement);
    $sql_returned->bind_param('s', $establishment_code);
    $sql_returned->execute();
    $sql_returned->bind_result($return_message);
    $sql_returned->fetch();
    $sql_returned->free_result();
    $sql_returned->close();
    
    $message = "<tt>" . str_replace('Assessment returned to assessor:', '<b>Assessment returned to assessor:</b>',$return_message) . "</tt><hr>";

}



//Replace Tags
$template = str_replace('<!-- assessor_name -->', $assessor_name, $template);
$template = str_replace('<!-- entered_date -->', $enter_date, $template);
$template = str_replace('<!-- website_url -->', $website_url, $template);
$template = str_replace('<!-- assessment_date -->', $assessment_date, $template);
$template = str_replace('<!-- renewal_date -->', $renewal_date, $template);
$template = str_replace('<!-- assessment_type -->', $type_list, $template);
$template = str_replace('<!-- assessment_type_static -->', $type_list_static, $template);
$template = str_replace('<!-- qa_list -->', $qa_list, $template);
$template = str_replace('<!-- category_select -->', $category_select, $template);
$template = str_replace('<!-- qa_list_static -->', $qa_list_static, $template);
$template = str_replace('<!-- category_select_static -->', $category_select_static, $template);
$template = str_replace('<!-- message -->', $message, $template);
$template = str_replace('!camp_sites!', $camp_sites, $template);
$template = str_replace('!caravan_sites!', $caravan_sites, $template);
$template = str_replace('!caravan_camping_sites!', $caravan_camping_sites, $template);


echo $template;

//Button display
$dsp = '';
if ($status == 'complete' || ($status == 'waiting' && $_SESSION['j_user']['role']=='s')) {
	$dsp = $SUB_ACCESS["re-send-welcome-pack"]==1?'1':'0';
	$dsp .= $SUB_ACCESS["view-and-send-certificate"]==1?'1':'0';
	$dsp .= $SUB_ACCESS["view-and-send-client-report"]==1?'1':'0';
	$dsp .= $SUB_ACCESS["re-open-assessments"]==1?'1':'0';
	$dsp .= $SUB_ACCESS["approve-assessments"]==1?'1':'0';
}

echo "<script>J_ass('$eid',$assessment_id,'{$_SESSION['j_user']['role']}','$ust','$dsp','$establishment_code')</script>";
$J_title1="Assessment";
$J_title1=$establishment_name;
$J_title2="Assessment";
$J_title3="Code: $establishment_code";
$J_icon="<img src=".$ROOT."/ico/set/edit.png>";
$J_nostart=1;
$J_label=23;
$J_height=640;
$J_width=$assessment_id?820:480;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>