<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$establishment_code = $_GET['t'];
$template = file_get_contents('res.html');
$invoices = '';
$radio_state = 'checked';
$invoiced = array();

//Get establishment data
$statement = "SELECT a.establishment_name, CONCAT(b.firstname, ' ' , b.lastname) 
				FROM nse_establishment AS a
				LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
				WHERE a.establishment_code=?
				LIMIT 1";
$sql_establishment = $GLOBALS['dbCon']->prepare($statement);
$sql_establishment->bind_param('s', $establishment_code);
$sql_establishment->execute();
$sql_establishment->bind_result($establishment_name, $assessor_name);
$sql_establishment->fetch();
$sql_establishment->close();

if (COUNT($_POST) > 0) {
	
	//Create Assessment
	$statement = "INSERT INTO nse_establishment_assessment
					(establishment_code, assessment_status, assessor_name, active, invoice_id, assessment_type_2)
					VALUES 
					(?,'draft',?,'Y',?,?)";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_insert->bind_param('ssss', $establishment_code, $assessor_name, $_POST['invoice'], $_POST['assessment_type']);
	$sql_insert->execute();
	
	echo "<div style='width: 100%; border: 1px dotted black; margin-top: 50px; text-align: center; padding-top: 20px; padding-bottom: 20px; background-color: silver'>Assessment Created</div>";
	die();
}

//Get current assessments
$assessments = "<table width=100%><tr><th>Assessment Date</th><th>Status</th><th>Type</th><th></th></tr>";
$statement = "SELECT DATE_FORMAT(assessment_date, '%d %M %Y'), assessment_status, invoice_id, assessment_type, assessment_type_2
				FROM nse_establishment_assessment
				WHERE establishment_code=?
				ORDER BY assessment_date DESC";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
$sql_assessments->bind_param('s', $establishment_code);
$sql_assessments->execute();
$sql_assessments->bind_result($assessment_date, $assessment_status, $invoice_id, $assessment_type, $assessment_type_2);
while ($sql_assessments->fetch()) {
	if ($assessment_type_2 == 'fa') $assessment_type_2 = 'Fixed Accommodation';
	if ($assessment_type_2 == 'cc') $assessment_type_2 = 'Caravan & Camping';
	$assessments .= "<tr><td>$assessment_date</td>";
	$assessments .= "<td>$assessment_status</td>";
	$assessments .= "<td>$assessment_type</td>";
	$assessments .= "<td>$assessment_type_2</td></tr>";
	$invoiced[] = $invoice_id;
}
$sql_assessments->close();
$assessments .= "</table>";

//Get list of invoices
$statement = "SELECT a.j_invit_name, a.j_invit_date, c.j_inv_id
				FROM nse_invoice_item AS a
				LEFT JOIN nse_inventory AS b ON b.j_in_id=a.j_invit_inventory
				LEFT JOIN nse_invoice AS c ON a.j_invit_invoice=c.j_inv_id
				WHERE c.j_inv_to_company=? AND (b.j_in_category=5 || b.j_in_category=6 || b.j_in_category=13 || b.j_in_category=15 || b.j_in_category=16)
				ORDER BY a.j_invit_date DESC";
$sql_invoices = $GLOBALS['dbCon']->prepare($statement);
$sql_invoices->bind_param('s', $establishment_code);
$sql_invoices->execute();
$sql_invoices->bind_result($invoice_item, $invoice_date, $invoice_id);
while ($sql_invoices->fetch()) {
	$invoice_date = date('d M Y', $invoice_date);
	if (in_array($invoice_id, $invoiced)) {
		$invoices .= "<input type=radio name='invoice' value='$invoice_id' $radio_state disabled ><span style='color: gray; text-decoration: line-through'> $invoice_date - $invoice_item</span><br>";
	} else {
		$invoices .= "<input type=radio name='invoice' value='$invoice_id' $radio_state > $invoice_date - $invoice_item<br>";
		$radio_state = '';
	}
	
}
$sql_invoices->close();

//Replace tags
$template = str_replace('<!-- establishment -->', "$establishment_name ($establishment_code)", $template);
$template = str_replace('<!-- invoices -->', $invoices, $template);
$template = str_replace('<!-- assessments -->', $assessments, $template);

echo $template;


?>
