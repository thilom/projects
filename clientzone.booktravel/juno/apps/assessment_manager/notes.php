<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$id=$_GET["id"];

if(isset($_GET["j_hide"]))
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/assessment_manager/f/notes.js></script>";
	unset($_GET["j_hide"]);
	echo "<script>J_start('".$_GET["id"]."')</script>";
	die();
}

$SUB_ACCESS=array("edit"=>0);
include $SDR."/system/secure.php";
$edit=$SUB_ACCESS["edit"];

$eid=mysql_fetch_array(mysql_query("SELECT establishment_code FROM nse_establishment_assessment WHERE assessment_id=".$id." LIMIT 1"));
$eid=$eid["establishment_code"];

$notes="";
$q="SELECT * FROM nse_notes WHERE establishment_code='".$eid."' AND (note_app=4 OR note_share=4 OR (note_app=2 AND note_staff=0)) ORDER BY note_date DESC";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	include $SDR."/system/get.php";
	while($g=mysql_fetch_array($q))
	{
		$notes.=$g["note_id"]."~";
		$notes.=date("d/m/Y",$g["note_date"])."~";
		$notes.=$g["note_user"]."~";
		$notes.=J_Value("","people","",$g["note_user"])."~";
		$notes.=stripslashes($g["note_content"])."~";
		if($g["note_reminder"]&&$g["note_user"]==$_SESSION["j_user"]["id"])
			$notes.=date("d/m/Y",$g["note_reminder"]);
		$notes.="|";
	}
}

// required
$app=4; // accounts // see custom/extra.js for list of ids

// feed this to note reader
include $SDR."/apps/notes/view.php";

?>