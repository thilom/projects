<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$search_string = isset($_GET['t'])&&$_GET['t']!='establishment'?$_GET['t']:'';
$jL0 = 0;
$line_count = 0;
$d="";
$r="";
$estab_counter=0;
$establishments_a = array();
$line_counter = -1;
$file_counter = 1;
$files = '';
$inv_date2 = '';

$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
    $s[$v]=1;
    $ddd[$v]=1;
}
$t=array("X","code","name","country","province","town","suburb","ass", "due","qa","invoice","invoice_date","payment","renewal");
$d="";$n=1;
foreach($t as $k => $v)
{
    if(isset($ddd[$v]))
    {
        $d.=$n;
        $n++;
    }
    $d.="~";
}
include_once $SDR."/system/get.php";

//Prepare statement - Locations
$statement = "SELECT b.country_name, c.province_name, d.town_name, e.suburb_name
                FROM nse_establishment_location AS a
                LEFT JOIN nse_nlocations_countries AS b ON a.country_id=b.country_id
                LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
                LEFT JOIN nse_nlocations_towns AS d ON a.town_id=d.town_id
                LEFT JOIN nse_nlocations_suburbs As e ON a.suburb_id=e.suburb_id
                WHERE a.establishment_code=?";
$sql_locations = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Invoices
$statement = "SELECT b.j_invit_total, b.j_invit_paid, a.j_inv_id, b.j_invit_vat, j_inv_date
                FROM nse_invoice AS a
                LEFT JOIN nse_invoice_item AS b ON a.j_inv_id=b.j_invit_invoice
                LEFT JOIN nse_inventory AS c ON b.j_invit_inventory=c.j_in_id
                LEFT JOIN nse_inventory_category_name AS d ON d.j_icn_id=c.j_in_category
                WHERE a.j_inv_to_company=? && d.j_icn_name='Quality Assurance' && j_invit_date>?
                ORDER BY j_invit_id DESC";
$sql_invoice = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - postponements
$statement = "SELECT postpone_date FROM nse_establishment_assessment_postpone WHERE establishment_code=?";
$sql_postpone = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - assessor name
$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=? LIMIT 1";
$sql_assessor = $GLOBALS['dbCon']->prepare($statement);

//prepare statement - Assessment data
$statement = "SELECT renewal_date, assessment_status
                FROM nse_establishment_assessment
				WHERE establishment_code=?
				ORDER BY assessment_date DESC
				LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);


$statement = "SELECT a.establishment_code, a.establishment_name, a.enter_date, a.review_date_depricated, 
					a.assessor_id, a.aa_category_code, b.cancelled_date
                FROM nse_establishment AS a
				LEFT JOIN nse_establishment_qa_cancelled AS b ON a.establishment_code=b.establishment_code";

if (isset($_GET['cou']) || isset($_GET['pro']) || isset($_GET['cit']) || isset($_GET['sub'])) {
    $statement .= " LEFT JOIN nse_establishment_location AS c ON a.establishment_code=c.establishment_code ";
}

if ($_SESSION['j_user']['role'] == 's') {
   if (isset($_GET['ass'])) $statement .= " WHERE b.assessor_id={$_GET['ass']}";
} else {
   if (!isset($_GET['qas'])) {
       $statement .= " WHERE (a.assessor_id={$_SESSION['j_user']['id']} || a.assessor_id='' || a.assessor_id IS NULL)";
   } else {
     $statement .= " WHERE a.assessor_id={$_SESSION['j_user']['id']}";
   }
}

if (isset($_GET['sub'])) {
    $statement .= " && c.suburb_id={$_GET['sub']} ";
} else if (isset($_GET['cit'])) {
    $statement .= " && c.town_id={$_GET['cit']} ";
} else if (isset($_GET['pro'])) {
    $statement .= " && c.province_id={$_GET['pro']} ";
} else if (isset($_GET['cou'])) {
    $statement .= " && c.country_id={$_GET['cou']} ";
}

if (!empty($search_string)) { //Add search criteria if the search string has been entered
    $search_string = substr($search_string, 0, strpos($search_string, '('));
    $search_string = str_replace(';a', '&', $search_string);
    $search_string = trim($search_string);
    $statement .= " && a.establishment_name LIKE '%$search_string%'";
}

$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
$sql_assessments->execute();
$sql_assessments->store_result();
$sql_assessments->bind_result(  $establishment_code, $establishment_name, $enter_date, $old_due, $assessor_id, $qa_status, $cancel_date);
while ($sql_assessments->fetch()) {
    $postpone = '';
	$renewal_date = '';
	$status = '';

	//Get assessment Data
	$sql_assessment->bind_param('s', $establishment_code);
	$sql_assessment->execute();
	$sql_assessment->bind_result($renewal_date, $status);
	$sql_assessment->fetch();
	$sql_assessment->free_result();

	if ($status != 'complete') {
		$renewal_date = '';
		$statement = "SELECT renewal_date FROM nse_establishment_assessment WHERE establishment_code=? AND assessment_status='complete' ORDER BY assessment_date DESC LIMIT 1";
		$sql_previous_renewal = $GLOBALS['dbCon']->prepare($statement);
		$sql_previous_renewal->bind_param('s', $establishment_code);
		$sql_previous_renewal->execute();
		$sql_previous_renewal->bind_result($renewal_date);
		$sql_previous_renewal->fetch();
		$sql_previous_renewal->close();
	}

    //Get Assessor
    $sql_assessor->bind_param('s', $assessor_id);
    $sql_assessor->execute();
    $sql_assessor->bind_result($assessor);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    if ($assessor == 0) $assessor = '-';

    //Get location
    $country = '';
    $province = '';
    $town = '';
    $suburb = '';
    $location = '';
    $sql_locations->bind_param('s', $establishment_code);
    $sql_locations->execute();
    $sql_locations->bind_result($country, $province, $town, $suburb);
    $sql_locations->fetch();
    $sql_locations->free_result();

    if (empty($suburb)) $suburb = "-";
    if (empty($town)) $town = "-";
    if (empty($province)) $province = "-";
    if (empty($country)) $country = "-";

    if (empty($enter_date)) $enter_date = '-';
	if (empty($renewal_date)) {
        $renewal_date = $old_due;
        if (empty($renewal_date)) $renewal_date = '-';
    }
	
	if (empty($renewal_date) && $enter_date != '-') {
		list($year2, $month2, $day2) = explode('-', $enter_date);
		$current_month2 = date('m', time());
		$year2 = $month2<$current_month2?date('Y',time()):date('Y',time())+1;
		$renewal_date = "$year2-$month2-01";
	}
    

    //Get postponement
    $sql_postpone->bind_param('s', $establishment_code);
    $sql_postpone->execute();
    $sql_postpone->bind_result($postpone);
    $sql_postpone->fetch();
    $sql_postpone->free_result();
    if (!empty($postpone) && $postpone != '0000-00-00') $renewal_date = $postpone . "*";

	if (!empty($cancel_date) && $cancel_date != '0000-00-00') $renewal_date = '-';


    //Calculate due date
    $date1 = strtotime($renewal_date);
    $date2 = strtotime('now');
    $time_diff = date_diff_for_v52($date1, $date2);
    $time_diff_years = $time_diff['years'];
    $time_diff_months = $time_diff['months'];
	$due_month = date('n', $date1);

    $time_diff = ($time_diff_years * 12) + $time_diff_months;
    $time_diff = $time_diff * (-1);
    if ($time_diff < 0) $due = 'overdue';
    if ($time_diff == 0) $due = '1 Month';
    if ($time_diff > 0 && $time_diff <= 1) $due = '2 Months';
    if ($time_diff > 1 && $time_diff <= 2) $due = '3 Months';
	if ($time_diff > 2 && $time_diff <= 3) $due = '4 Months';
    if ($time_diff > 3) $due = '-';
    if (empty($renewal_date)) $due = 'unknown';

    //Check for last invoice and payment
    $invoice_id = 0;
    $total_invoice = 0;
    $total_payments = 0;
	$inv_date = '';
    $invoice_date = strtotime("today - 11 months");
    $sql_invoice->bind_param('ss', $establishment_code, $invoice_date);
    $sql_invoice->execute();
    $sql_invoice->store_result();
    $sql_invoice->bind_result($invoice, $payment, $inv_id, $inv_vat, $inv_date);
    while ($sql_invoice->fetch()) {
		$inv_date2 = !empty($inv_date)?date('Y-m-d', $inv_date):'';
        if ($invoice_id == 0 || $invoice_id == $inv_id) {
            $total_invoice += $invoice;
            $total_payments += $payment;
        } else {
            break;
        }
    }
    $sql_invoice->free_result();

	//Calculate invoice time diff
	if (!empty($inv_date)) {
		$inv_full_date = date('d F Y', $inv_date);
		$inv_datediff = date_diff_days($inv_full_date);
	} else {
		$inv_datediff = -1;
	}

    if (($due == 'overdue' || $due == '1 Month' || $due == '2 Months' || $due == '3 Months' || $due == '4 Months'  ) && $status == 'complete') {
        $status= '-';
    }

	if (!empty($status)) $due .="/$status";

	//Set cancel status
	if (!empty($cancel_date) && $cancel_date != '0000-00-00') {
		$due = "Cancelled($cancel_date)";
	}

    if (isset($_GET['filter'])) {
        switch ($_GET['filter']) {
            case 1:
                if ($due != 'overdue') continue 2;
                break;            
            case 2:
                if ($due != '1 Month') continue 2;
                break;
            case 3:
                if ($due != '2 Months' && $due != '1 Month') continue 2;
                break;
            case 4:
                if ($due != '3 Months' && $due != '2 Month' && $due != '1 Month') continue 2;
                break;
			case 5:
                if ($due != '4 Months' && $due != '3 Month' && $due != '2 Month' && $due != '1 Month') continue 2;
                break;
			case 6:
                if (empty($cancel_date) || $cancel_date == '0000-00-00') continue 2;
                break;
			case 7:
                if ($due_month != date('n') || $time_diff > 6) continue 2;
                break;
			case 8:
                if ($due_month != (date('n')+1) || $time_diff > 6) continue 2;
                break;
			case 9:
                if ($due_month != (date('n')+2) || $time_diff > 6) continue 2;
                break;
			case 10:
                if ($due_month != (date('n')+3) || $time_diff > 6) continue 2;
                break;
			case 11: //Invoiced in last 7 days
                if ($inv_datediff == -1 || $inv_datediff > 7) continue 2;
                break;
			case 12: //Invoiced this month
                if ($inv_datediff == -1 || $inv_datediff <7 || $inv_datediff > 31) continue 2;
                break;
        }
    }

	
	if ($jL2 == 0) { //CSV File
		$line_counter++;
		$establishments_a = array();

        if ($line_counter == 0) {
            if (isset($s['code'])) $headers[1] = 'Establishment Code';
            if (isset($s['name'])) $headers[2] = 'Establishment Name';
            if (isset($s['country'])) $headers[3] = 'Country';
            if (isset($s['province'])) $headers[4] = 'Province';
            if (isset($s['town']))$headers[5] = 'Town';
            if (isset($s['suburb'])) $headers[6] = 'Suburb';
            if (isset($s['ass'])) $headers[7] = 'Assessor';
            if (isset($s['due'])) $headers[8] = 'Due In';
            if (isset($s['qa'])) $headers[9] = 'QA Status';
            if (isset($s['invoice'])) $headers[10] = 'Invoices';
            if (isset($s['invoice_date'])) $headers[10] = 'Invoice Date';
            if (isset($s['payment'])) $headers[11] = 'Payments';
            if (isset($s['renewal'])) $headers[12] = 'Renewal Date';

            $fh = fopen($SDR."/stuff/temp/{$_SESSION['j_user']['id']}_$file_counter.csv", 'w');
            fputcsv($fh, $headers);
            $files .= "<li><a href='/juno/stuff/temp/{$_SESSION['j_user']['id']}_$file_counter.csv'>Report Part $file_counter.csv</a></li>";
        }

		if (isset($s['code'])) $establishments_a[1] = $establishment_code;
		if (isset($s['name'])) $establishments_a[2] = $establishment_name;
		if (isset($s['country'])) $establishments_a[3] = $country;
		if (isset($s['province'])) $establishments_a[4] = $province;
		if (isset($s['town']))$establishments_a[5] = $town;
		if (isset($s['suburb'])) $establishments_a[6] = $suburb;
		if (isset($s['ass'])) $establishments_a[7] = $assessor;
		if (isset($s['due'])) $establishments_a[8] = $due;
		if (isset($s['qa'])) $establishments_a[9] = $qa_status;
		if (isset($s['invoice'])) $establishments_a[10] = $total_invoice;
		if (isset($s['invoice_date'])) $establishments_a[11] = $inv_date2;
		if (isset($s['payment'])) $establishments_a[12] = $total_payments;
		if (isset($s['renewal'])) $establishments_a[13] = $renewal_date;

		fputcsv($fh, $establishments_a);

		if ($line_counter == 10000) {
            fclose($fh);
            $line_counter=-1;
            $file_counter++;
        }

	} elseif ($line_count >= $jL1 && $line_count < $jL1+$jL2) {
        $r .= "$estab_counter~";
        if (isset($s['code'])) $r .="$establishment_code~";
        if (isset($s['name'])) $r .= str_replace(array("\r\n","\r","\n"), '', $establishment_name) . "~";
        if (isset($s['country'])) $r .="$country~";
        if (isset($s['province'])) $r .="$province~";
        if (isset($s['town'])) $r .="$town~";
        if (isset($s['suburb'])) $r .="$suburb~";
        if (isset($s['ass'])) $r .="$assessor~";

        if (isset($s['due'])) $r .="$due~";
        if (isset($s['qa'])) $r .="$qa_status~";
        if (isset($s['invoice'])) $r .="$total_invoice~";
        if (isset($s['invoice_date'])) $r .="$inv_date2~";
        if (isset($s['payment'])) $r .="$total_payments~";
        if (isset($s['renewal'])) $r .="$renewal_date";
        $r .="|";
        $estab_counter++;
    }
    $line_count++;
}
$sql_assessments->free_result();
$sql_assessments->close();

if ($jL2 == 0) {
	echo "<script src=".$ROOT."/system/P.js></script>";
    $page = "<body style='color:#999;padding:30 40 40 160;background:url(/juno/ico/set/spreadsheet.png) no-repeat 40px 40px'><tt style=width:260><hr>CSV Files Created. Right click to save.<ul>$files</ul></tt></body></html>";
    echo $page;
} else {
	$r=str_replace("\r","",$r);
	$r=str_replace("\n","",$r);
	$r=str_replace("\"","",$r);
	$r=str_replace("~|","|",$r);

	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/assessment_manager/reports_ass/res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$d."\",".$line_count.",".$jL1.",".$jL2.")</script>";
}





function date_diff_for_v52($date1, $date2) {
	//Vars
	$date['years'] = 0;
	$date['months'] = 0;
	if (empty($date1)) return 0;
	$date_diff = $date2 - $date1;
	//$date['years'] = floor($date_diff/31536000);
	//if ($date['years'] < 0) $date['years'] = 0;
	$date['months'] = floor($date_diff/2628000);
	//if ($date['months'] < 0) ($date['months']++)* (-1);

	return $date;
}

/**
 * Return the number of days between given date and today
 *
 * @param string $start
 * @return int
 */
function date_diff_days($start) {
	$now = mktime();
	$start = date(strtotime($start));

	$diff = $now-$start;
	$days_diff = floor($diff/(60*60*24));
//	echo date('d F Y', $now) . "::" .  date('d F Y', $start) . ":: $days_diff<br>";
	return $days_diff;
}

?>