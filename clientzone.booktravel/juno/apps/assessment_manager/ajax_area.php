<?php

/**
 * Get previous area data
 * 
 * @author Thilo Muller(2011)
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$establishment_code = $_GET['eID'];
$area_text_id = $_GET['aID'];

//Get Assessment ID
$statement = "SELECT assessment_id FROM nse_establishment_assessment WHERE assessment_status='complete' AND establishment_code=? ORDER BY assessment_date LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->bind_result($assessment_id);
$sql_assessment->fetch();
$sql_assessment->close();

//Get area Id
$statement = "SELECT assessment_area_id FROM nse_assessment_areas WHERE area_text_id=? LIMIT 1";
$sql_area_id = $GLOBALS['dbCon']->prepare($statement);
$sql_area_id->bind_param('s', $area_text_id);
$sql_area_id->execute();
$sql_area_id->bind_result($area_id);
$sql_area_id->fetch();
$sql_area_id->close();

//Get area data
$statement = "SELECT assessment_content FROM nse_establishment_assessment_data WHERE assessment_id=? AND assessment_area_id=? LIMIT 1";
$sql_area_data = $GLOBALS['dbCon']->prepare($statement);
$sql_area_data->bind_param('ii', $assessment_id, $area_id);
$sql_area_data->execute();
$sql_area_data->bind_result($area_data);
$sql_area_data->fetch();
$sql_area_data->close();

echo $area_data;

?>
