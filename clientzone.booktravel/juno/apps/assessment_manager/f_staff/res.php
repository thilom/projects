<?php

include_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";
$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 100);
$search_string = isset($_GET['t']) ? $_GET['t'] : '';
$jL0 = 0;
$line_count = 0;
$d = "";
$r = "";

if (strpos($_GET["d"], 'payment') > 0) {
	$_GET["d"] = $_GET["d"] . "~paydate";
}

$dd = explode("~", trim($_GET["d"], "~"));
$ddd = array();
$s = array();
foreach ($dd as $k => $v) {
	$s[$v] = 1;
	$ddd[$v] = 1;
}
$t = array("X", "code", "name", "assessment_date", "submit_date", "country", "province", "town", "suburb", "ass", "date_entered", "status", "invoice", "payment", "paydate", "type");
$d = "";
$n = 1;
foreach ($t as $k => $v) {
	if (isset($ddd[$v])) {
		$d.=$n;
		$n++;
	}
	$d.="~";
}
include_once $SDR . "/system/get.php";

//Prepare statement - Locations
$statement = "SELECT b.country_name, c.province_name, d.town_name, e.suburb_name
                FROM nse_establishment_location AS a
                LEFT JOIN nse_nlocations_countries AS b ON a.country_id=b.country_id
                LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
                LEFT JOIN nse_nlocations_towns AS d ON a.town_id=d.town_id
                LEFT JOIN nse_nlocations_suburbs As e ON a.suburb_id=e.suburb_id
                WHERE a.establishment_code=?";
$sql_locations = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Invoices
$statement = "SELECT b.j_invit_total, b.j_invit_paid, a.j_inv_id, b.j_invit_vat, e.j_invp_date
                FROM nse_invoice AS a
                LEFT JOIN nse_invoice_item AS b ON a.j_inv_id=b.j_invit_invoice
                LEFT JOIN nse_inventory AS c ON b.j_invit_inventory=c.j_in_id
                LEFT JOIN nse_inventory_category_name AS d ON d.j_icn_id=c.j_in_category
		LEFT JOIN nse_invoice_item_paid AS e ON a.j_inv_id = e.j_invp_invoice
                WHERE a.j_inv_to_company=? AND (c.j_in_category=5 OR c.j_in_category=6 OR c.j_in_category=15 OR c.j_in_category=16) AND j_invit_date>?
                ORDER BY j_invit_id DESC";
$sql_invoice = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Prepare statement - assessor name
$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=? LIMIT 1";
$sql_assessor = $GLOBALS['dbCon']->prepare($statement);

$statement = "SELECT a.assessment_id, DATE_FORMAT(a.assessment_date, '%d-%m-%Y'), DATE_FORMAT(a.submitted_date, '%d-%m-%Y'), a.establishment_code,b.establishment_name, b.enter_date, a.renewal_date, a.assessment_status, b.review_date_depricated, b.assessor_id, a.assessment_type_2
                FROM nse_establishment_assessment AS a
                JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code";
if (isset($_GET['ass'])) $statement .= " WHERE b.assessor_id={$_GET['ass']}";


if (!empty($search_string)) { //Add search criteria if the search string has been entered
	$search_string = trim($search_string);
	$statement .= strpos($statement, 'WHERE') > 0 ? " &&" : " WHERE ";
	$statement .= " b.establishment_code = '$search_string'";
}

$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
$sql_assessments->execute();
$sql_assessments->store_result();
$sql_assessments->bind_result($assessment_id, $assessment_date, $submit_date, $establishment_code, $establishment_name, $enter_date, $renewal_date, $status, $old_due, $assessor_id, $assessment_type_2);
while ($sql_assessments->fetch()) {

	//Get Assessor
	$sql_assessor->bind_param('s', $assessor_id);
	$sql_assessor->execute();
	$sql_assessor->bind_result($assessor);
	$sql_assessor->fetch();
	$sql_assessor->free_result();

	//Get location
	$country = '';
	$province = '';
	$town = '';
	$suburb = '';
	$location = '';
	$sql_locations->bind_param('s', $establishment_code);
	$sql_locations->execute();
	$sql_locations->bind_result($country, $province, $town, $suburb);
	$sql_locations->fetch();
	$sql_locations->free_result();

	if (empty($suburb))
		$suburb = "-";
	if (empty($town))
		$town = "-";
	if (empty($province))
		$province = "-";
	if (empty($country))
		$country = "-";

	if (empty($enter_date))
		$enter_date = '-';
	if (empty($renewal_date)) {
		$renewal_date = $old_due;
		if (empty($renewal_date))
			$renewal_date = '-';
	}

	//Calculate due date
	$date1 = strtotime($renewal_date);
	$date2 = strtotime('now');
	$time_diff = date_diff_for_v52($date1, $date2);
	$time_diff_years = $time_diff['years'];
	$time_diff_months = $time_diff['months'];

	$time_diff = ($time_diff_years * 12) + $time_diff_months;
	$time_diff = $time_diff * (-1);
	if ($time_diff < 0)
		$due = 'overdue';
	if ($time_diff == 0)
		$due = '1 Month';
	if ($time_diff > 0 && $time_diff <= 2)
		$due = '3 Months';
	if ($time_diff > 2)
		$due = '-';
	if (empty($renewal_date))
		$due = '-';

	//Check for last invoice and payment
	$invoice_id = 0;
	$total_invoice = 0;
	$total_payments = 0;
	$invoice_date = strtotime("today - 11 months");
	$sql_invoice->bind_param('ss', $establishment_code, $invoice_date);
	$sql_invoice->execute();
	$sql_invoice->store_result();
	$sql_invoice->bind_result($invoice, $payment, $inv_id, $inv_vat, $payment_date);
	while ($sql_invoice->fetch()) {
		if ($invoice_id == 0 || $invoice_id == $inv_id) {
			$total_invoice += $invoice;
			$total_payments += $payment;
		} else {
			break;
		}
	}
	$sql_invoice->free_result();

	$total_invoice = number_format($total_invoice, 2);
	$total_payments = number_format($total_payments, 2);

	//Payment date format
	if ($payment_date != 0) {
		$payment_date = date('d-m-Y',$payment_date);
	}

	if (($due == 'overdue' || $due == '1 Month' || $due == '3 Months' ) && $status == 'complete') {
		$status = '-';
	}

	if ($status == 'waiting' || $status == 'draft' || $status == 'returned') {
		$due = '-';
	}

//	if ($due == 'overdue') continue ;
	if ($status == 'draft')
		continue;
	if ($status == 'auto')
		continue;
	if ($status == 'returned')
		continue;
//    if ($due == '1 Month') continue ;
//    if ($due == '3 Months') continue ;

	if (isset($_GET['filter'])) {
		switch ($_GET['filter']) {

			case 1:
				if ($status != 'waiting')
					continue 2;
				break;
			case 2:
				if ($status != 'complete')
					continue 2;
				break;
		}
	}

	if ($line_count >= $jL1 && $line_count < $jL1 + $jL2) {
		$r .= "$assessment_id~";
		if (isset($s['code']))
			$r .="$establishment_code~";
		if (isset($s['name']))
			$r .="$establishment_name~";
		if (isset($s['assessment_date']))
			$r .="$assessment_date~";
		if (isset($s['submit_date']))
			$r .="$submit_date~";
		if (isset($s['country']))
			$r .="$country~";
		if (isset($s['province']))
			$r .="$province~";
		if (isset($s['town']))
			$r .="$town~";
		if (isset($s['suburb']))
			$r .="$suburb~";
		if (isset($s['ass']))
			$r .="$assessor~";
		if (isset($s['date_entered']))
			$r .="$enter_date~";

		if (isset($s['status']))
			$r .="$status~";
		if (isset($s['invoice']))
			$r .="$total_invoice~";
		if (isset($s['payment']))
			$r .="$total_payments~";
		if (isset($s['payment']))
			$r .="$payment_date~";
		if (isset($s['type']))
			$r .="$assessment_type_2";
		$r .="|";
	}
	$line_count++;
}
$sql_assessments->free_result();
$sql_assessments->close();


$r = str_replace("\r", "", $r);
$r = str_replace("\n", "", $r);
$r = str_replace("\"", "", $r);
$r = str_replace("~|", "|", $r);


echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/assessment_manager/f_staff/res.js></script>";
echo "<script>J_in_r(\"" . $r . "\",\"" . $d . "\"," . $line_count . "," . $jL1 . "," . $jL2 . ")</script>";

function date_diff_for_v52($date1, $date2) {
	//Vars
	$date['years'] = 0;
	$date['months'] = 0;
	if (empty($date1))
		return 0;
	$date_diff = $date2 - $date1;
	//$date['years'] = floor($date_diff/31536000);
	//if ($date['years'] < 0) $date['years'] = 0;
	$date['months'] = floor($date_diff / 2628000);
	//if ($date['months'] < 0) ($date['months']++)* (-1);

	return $date;
}

?>