<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

include $SDR."/system/secure.php";
require_once $SDR . '/utility/PHPMailer/class.phpmailer.php';

$disp = '';
$assessment_id=isset($_GET["id"])?$_GET["id"]:0;
$eid=isset($_GET["eid"])?$_GET["eid"]:0;
$counter = 0;
$assessment_date = '';
$message_type = '';

	$hour_range = array();
	for ($x=5;$x<22;$x++) {
		$hour_range[] = str_pad($x, 2, 0, STR_PAD_LEFT ).":00";
		$hour_range[] = str_pad($x, 2, 0, STR_PAD_LEFT ).":30";
	}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/assessment_manager/book/book.js></script>";
echo "<body>";

//Get assessment Data
$statement = "SELECT establishment_code, invoice_id, DATE_FORMAT(assessment_date,'%d/%m/%Y'), DATE_FORMAT(assessment_date,'%H:%i')  FROM nse_establishment_assessment WHERE assessment_id=?";
$sql_data = $GLOBALS['dbCon']->Prepare($statement);
$sql_data->bind_param('i', $GLOBALS['assessment_id']);
$sql_data->execute();
$sql_data->bind_result($establishment_code, $invoice_id, $assessment_date, $assessment_time);
$sql_data->fetch();
$sql_data->free_result();
$sql_data->close();

if ($assessment_date == '00/00/0000') $assessment_date = '';

//Get Establishment name
$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? ";
$sql_name = $GLOBALS['dbCon']->Prepare($statement);
$sql_name->bind_param('s', $establishment_code);
$sql_name->execute();
$sql_name->bind_result($establishment_name);
$sql_name->fetch();
$sql_name->free_result();
$sql_name->close();

if (isset($_POST['save'])) {
    booking_save();
} else {
    booking_form();
}

function booking_form() {
    global $assessment_date, $establishment_code, $assessment_time;
    $counter = 0;
    $email_list = "<TABLE width=100% id=jTR style='border: solid 1px black'><tr style='background-color: gray'><td>&nbsp;</td><td>Contact</td><td>Email</td><td>Designation</td></tr>";
    $statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);
    $sql_email->bind_param('s', $establishment_code);
    $sql_email->execute();
    $sql_email->store_result();
    $sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
    while ($sql_email->fetch()) {
        $email_list .= "<tr><td class=jdbO><input type=checkbox name=emails[] id=email_$counter value='$uID' >";
        $email_list .= "</td><td class=jdbR><label for=email_$counter >$contact_name</label></td><td class=jdbY>$contact_email</td><td class=jdbB>$contact_designation</td></tr>" . PHP_EOL;
        $counter++;
    }
    $sql_email->free_result();
    $sql_email->close();

	//Assessment Time
	$book_time = '<option value=""> ';
	foreach ($GLOBALS['hour_range'] as $t) {
		 if ($assessment_time == $t) {
				$book_time .= "<option value='$t' selected>$t";
		 } else {
			$book_time .= "<option value='$t'>$t";
		 }
	}

    //Get Assessor Email Adddress
    $statement = "SELECT b.firstname, b.lastname , b.email, b.user_id
                    FROM nse_establishment AS a
                    JOIN nse_user AS b ON a.assessor_id=b.user_id
                    WHERE a.establishment_code=?";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->bind_param('s', $establishment_code);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($assessor_firstname, $assessor_lastname, $assessor_email, $uID);
    $sql_assessor->fetch();
    $sql_assessor->free_result();
    $sql_assessor->close();
    if (!empty($assessor_email)) {
        $email_list .= "<tr><td class=jdbO><input type=checkbox name=assessor_email id=email_$counter value='$uID' ></td><td class=jdbR><label for=email_$counter >$assessor_firstname $assessor_lastname</label></td><td class=jdbY>$assessor_email</td><td class=jdbB>Assessor</td></tr>" . PHP_EOL;
    }

    $email_list .= '</TABLE >';

    //Message Type
    $message_type = "<option value='new'>New Booking";
    $message_type .= "<option value='rebook'";
    $message_type .= empty($assessment_date)?"disabled":"";
    $message_type .= ">Re-scheduled Booking";

    $template = file_get_contents($GLOBALS['SDR'] . '/apps/assessment_manager/book/html/book_form.html');
    $template = str_replace('<!-- email_list -->', $email_list, $template);
    $template = str_replace('!assessment_date!', $assessment_date, $template);
    $template = str_replace('<!-- message_type -->', $message_type, $template);
    $template = str_replace('<!-- book_time -->', $book_time, $template);

    echo $template;
}

function booking_save() {
    global $assessment_id;
    $new_date = $_POST['assessment_date'];
	$new_time = $_POST['book_time'];
    $email_counter = 1;
    $email_template = '';

    $mail = new PHPMailer();

    list($d,$m,$y) = explode('/',$new_date);
	if (isset($new_time) && !empty($new_time)) {
		list($h,$i) = explode(':', $new_time);
	} else {
		$h = '00';
		$i = '00';
	}
    $db_date = "$y-$m-$d $h:$i:00";

    if ($new_date != $GLOBALS['assessment_date'] || $new_time != $GLOBALS['assessment_time']) {
        $statement = "UPDATE nse_establishment_assessment SET assessment_date=?, assessment_status='draft' WHERE assessment_id=?";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bind_param('si', $db_date, $GLOBALS['assessment_id']);
        $sql_update->execute();
    }

    if (isset($_POST['send_confirmation']) && $_POST['send_confirmation'] == 'Y') {
        $email_type = $_POST['type'];
        
        if ($email_type == 'rebook') {
            $email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/book/html/rebook_assessment_email.html');
            $subject = "AA Quality Assured Assessment visit change";
        } else {
            $email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/book/html/book_assessment_email.html');
            $subject = "AA Quality Assured Assessment visit confirmation";
        }
    }

    //CGet assessment data
    $statement = "SELECT CONCAT(c.firstname, ' ', c.lastname), b.establishment_name, DATE_FORMAT(a.assessment_date, '%d %M %Y'), c.email
                    FROM nse_establishment_assessment AS a
                    JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                    LEFT JOIN nse_user AS c ON b.assessor_id=c.user_id
                    WHERE a.assessment_id=?";
    $sql_assessment = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessment->bind_param('i', $assessment_id);
    $sql_assessment->execute();
    $sql_assessment->bind_result($assessor_name, $establishment_name, $visit_date, $assessor_email);
    $sql_assessment->fetch();
    $sql_assessment->free_result();
    $sql_assessment->close();

    //Replace Tags
    $email_template = str_replace('<!-- assessor_name -->', $assessor_name, $email_template);
    $email_template = str_replace('<!-- visit_date -->', $visit_date, $email_template);
    $email_template = str_replace('<!-- establishment_name -->', $establishment_name, $email_template);
    $assessor_email_body = $email_template;


    //Prepare statement - user details
    $statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
    $sql_email = $GLOBALS['dbCon']->prepare($statement);

    if (isset($_POST['emails']) && !empty($_POST['emails'])) {
        foreach ($_POST['emails'] AS $uID) {
            //Get details
            $contact_name = '';
            $encrypted_password = '';
            $sql_email->bind_param('s', $uID);
            $sql_email->execute();
            $sql_email->store_result();
            $sql_email->bind_result($contact_name, $email);
            $sql_email->fetch();
            $sql_email->free_result();

            if (empty($contact_name)) $contact_name = 'QA Member';
            $email_content = str_replace('<!-- contact_name -->', $contact_name, $email_template);

            try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($email, $contact_name);
                $mail->SetFrom($assessor_email, $assessor_name);
                $mail->AddReplyTo($assessor_email, $assessor_name);
                $mail->Subject = $subject;
                $mail->MsgHTML($email_content);
                $mail->Send();
                if ($email_counter == 1) $assessor_email_body = $email_content;
                $email_counter++;
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }
        }
    }


    if (isset($_POST['assessor_email'])) {

        //Get details
        $contact_name = '';
        $encrypted_password = '';
        $sql_email->bind_param('i', $_POST['assessor_email']);
        $sql_email->execute();
        $sql_email->store_result();
        $sql_email->bind_result($contact_name, $email);
        $sql_email->fetch();
        $sql_email->free_result();

        $contact_name = 'QA Member';
        $email_content = str_replace('<!-- contact_name -->', $contact_name, $assessor_email_body);

        try {
            $mail->ClearAllRecipients();
            $mail->ClearAttachments();
            $mail->AddAddress($email, $assessor_name);
            $mail->SetFrom($assessor_email, $assessor_name);
            $mail->AddReplyTo($assessor_email, $assessor_name);
            $mail->Subject = $subject;
            $mail->MsgHTML($email_content);
            $mail->Send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    echo "<div style=text-align:center>Booking date updated</div></body></html>";
    echo "<script>";
	 echo "w=j_P.j_Wi[j_W]";
	 echo ";w.['table'].style.width=500";
	 echo ";w.['table'].style.height=";
	 echo ";setTimeout('j_P.J_WX(self)',2000)";
	 echo ";J_icoAlert()";
    echo "</script>";
	 die();
}

echo "<script>J_ass('$eid',$assessment_id)</script>";
$J_title1=$establishment_name;
$J_title2="Book Assessment";
$J_title3="Code: $establishment_code";
$J_icon="<img src=".$ROOT."/ico/set/edit.png>";
$J_nostart=1;
$J_label=23;
$J_height=440;
$J_width=380;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>
