<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";

J_SetPerm($SDR."set/init.php");
$s=file_get_contents($SDR."/set/init.php");

if(isset($_POST["e"]))
{
	$v="error_reporting(".($_POST["e"]).")";
	$s=preg_replace('~error_reporting\(.*?\)~',$v,$s);
	if($o=fopen($SDR."/juno/set/init.php","r+"))
	{
		include_once $SDR."/system/activity.php";
		J_act("System Errors",4);
		$o=fopen($SDR."/juno/set/init.php","w+");
		fwrite($o,$s);
		fclose($o);
	}
}

$c=J_Extract($s,'error_reporting\((.*?)\)');

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";
echo "<tt>If error reporting is on, system errors will display on pages. Once the system if completed and errors are no longer required, turn errors off.</tt><hr>";
echo "<form method=post>";
echo "<select name=e>";
echo "<option value=0>Off";
echo "<option value=E_ALL".($c=="E_ALL"?" selected":"").">E_ALL";
echo "<option value='E_ALL | E_STRICT'".($c=="E_ALL | E_STRICT"?" selected":"").">E_STRICT";
echo "</select>";
echo "<hr><input type=submit value=OK>";
echo "</form>";
$J_title1="Errors";
$J_icon="<img src=".$ROOT."/ico/set/alert.png>";
$J_label=1;
$J_nostart=1;
$J_nomax=1;
$J_width=320;
$J_height=200;
$J_home=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>