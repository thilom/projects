<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";

J_SetPerm($SDR."set/init.php");
$s=file_get_contents($SDR."/set/init.php");

if(isset($_POST["d"]))
{
	$v='session_name("'.$_POST["n"].'")';
	$s=preg_replace('~session_name\(".*?"\)~',$v,$s);
	$v='_maxlifetime",'.$_POST["d"].')';
	$s=preg_replace('~_maxlifetime",.*?\)~i',$v,$s);
	if($o=fopen($SDR."/juno/set/init.php","r+"))
	{
		include_once $SDR."/system/activity.php";
		J_act("System Sessions",4);
		$o=fopen($SDR."/juno/set/init.php","w+");
		fwrite($o,$s);
		fclose($o);
	}
}

$d=J_Extract($s,'_maxlifetime",(.*?)\)');
$n=J_Extract($s,'session_name\("(.*?)"\);');

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";
echo "<tt>This setting will determine how long a session lasts after the last page opened. If the session times-out the site will restart itself. Note that some hosts do not allow this setting - you may have to ask the host to make an exception for you.</tt><hr>";
echo "<form method=post>";
echo "<table class=jW100>";
echo "<tr><td>Name</td><td><input type=text name=n value=".$n." class=jW100></td></tr>";
echo "<tr><td>Duration</td><td><select name=d class=jW100>";
echo "<option value=1800".($d=="1800"?" selected":"").">30 min";
echo "<option value=3600".($d=="3600"?" selected":"").">1 hour";
echo "<option value=21600".($d=="21600"?" selected":"").">6 hours";
echo "<option value=43200".($d=="43200"?" selected":"").">12 hours";
echo "<option value=86400".($d=="86400"?" selected":"").">24 hours";
echo "</select></td></tr>";
echo "</table>";
echo "<hr><input type=submit value=OK>";
echo "</form>";
$J_title1="Sessions";
$J_icon="<img src=".$ROOT."/ico/set/recycle.png>";
$J_nostart=1;
$J_nomax=1;
$J_width=320;
$J_height=240;
$J_label=1;
$J_home=0;
$J_join="SysSet";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>