<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/settings/database.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";

J_SetPerm($SDR."set/init.php");
$s=file_get_contents($SDR."/set/init.php");

if(isset($_POST["d"]))
{
	$v='mysql_connect("localhost","'.$_POST["d"].'","'.$_POST["p"].'")';
	$s=preg_replace('~mysql_connect\(.*?\)~',$v,$s);
	$v='mysql_select_db("'.$_POST["d"].'")';
	$s=preg_replace('~mysql_select_db\(".*?"\)~',$v,$s);
	if($o=fopen($SDR."/juno/set/init.php","r+"))
	{
		$o=fopen($SDR."/juno/set/init.php","w+");
		fwrite($o,$s);
		fclose($o);
		include_once $SDR."/system/activity.php";
		J_act("System Database",4);
	}
}

$c=J_Extract($s,'mysql_connect\((.*?)\)');
$c=str_replace('"',"",$c);
$c=str_replace(" ","",$c);
$c=explode(",",$c);

function strip($v="")
{
	return str_replace("\"","\\\"",$v);
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";
echo "<tt>FTP details are required to access key system files and to handle file and folder permissions used by the system. The FTP settings are the same as those used to upload the system to this location.</tt><hr>";
echo "<form method=post>";
echo "<table width=320>";
echo "<tr><td>Database</td><td width=90%><input type=text name=d value=\"".strip($c[1])."\" title='Requires value' class=jW100></td></tr>";
echo "<tr><td>Password</td><td><input type=text name=p value=\"".strip($c[2])."\" title='Requires value' class=jW100></td></tr>";
echo "</table>";
echo "<hr><input type=submit value=OK>";
echo "</form>";
$J_title1="Database Connection";
$J_icon="<img src=".$ROOT."/ico/set/database.png>";
$J_nostart=1;
$J_nomax=1;
$J_label=1;
$J_width=400;
$J_height=220;
$J_home=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>