<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";
J_SetPerm("/set",0777);
J_SetPerm("/set/index_head.php",0777);
$s=file_get_contents($SDR."/set/general.php");

if(isset($_POST["d"]))
{
	$d=preg_replace("~[^a-zA-Z0-9 ,:;'!@#%&+={}\-\(\)\[\]\$\.]~","",($_POST["d"]?$_POST["d"]:"JUNO SYSTEM"));
	$s=str_replace(array("\r","\t","\v"),"",$s);
	if(preg_match('#\$title.*?=.*?".*?";#i',$s))
		$s=preg_replace('~\$title=".*?";~i','$title="'.$d.'";',$s,1);
	else
		$s=str_replace('<?php\n','<?php\n$title="'.$d.'";\n',$s);
	if($h=fopen($SDR."/set/general.php","r+"))
	{
		include_once $SDR."/system/activity.php";
		J_act("System Title",4);
		$h=fopen($SDR."/set/general.php","w+");
		fwrite($h,$s);
		fclose($h);
		$r=1;
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";
echo "<form method=post>";
echo "<input type=text name=d value=\"".J_Extract($s,"title=\"(.*?)\";\n")."\" class=jW100><hr><input type=submit value=OK>";
if(isset($r))
	echo " <input type=button value=Restart onclick='j_P.location=j_P.location.href'>";
echo "</form>";
$J_title1="Title";
$J_icon="<img src=".$ROOT."/ico/set/paper2.png>";
$J_nostart=1;
$J_nomax=1;
$J_width=320;
$J_height=80;
$J_label=1;
$J_home=0;
$J_join="SysSet";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>