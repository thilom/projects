<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";

J_SetPerm($SDR."admin/ftp.php");
$s=file_get_contents($SDR."/set/ftp.php");

if(isset($_POST["h"]))
{
	$_SESSION["J_host"]=$_POST["h"];
	$_SESSION["J_root"]=$_POST["r"];
	$_SESSION["J_user"]=$_POST["u"];
	$_SESSION["J_pass"]=$_POST["p"];
	$v='_SESSION["J_host"]:"'.$_POST["h"].'"';
	$s=preg_replace('~_SESSION\["J_host"\]\:".*?"~',$v,$s);
	$v='_SESSION["J_root"]:"'.$_POST["r"].'"';
	$s=preg_replace('~_SESSION\["J_root"\]\:".*?"~',$v,$s);
	$v='_SESSION["J_user"]:"'.$_POST["u"].'"';
	$s=preg_replace('~_SESSION\["J_user"\]\:".*?"~',$v,$s);
	$v='_SESSION["J_pass"]:"'.$_POST["p"].'"';
	$s=preg_replace('~_SESSION\["J_pass"\]\:".*?"~',$v,$s);

	if($o=fopen($SDR."/set/ftp.php","r+"))
	{
		include_once $SDR."/system/activity.php";
		J_act("System FTP",4);
		$o=fopen($SDR."/set/ftp.php","w+");
		fwrite($o,$s);
		fclose($o);
	}

	function J_dirPermi($dir="")
	{
		global $SDR;
		if(is_dir($SDR.$dir))
			J_SetPerm($SDR.$dir);
		if(is_dir($SDR.$dir) && is_readable($SDR.$dir))
		{
			$d=opendir($SDR.$dir);
			while(FALSE!==($i=readdir($d)))
			{
				if($i=="." || $i==".."){}
				else
				{
					if(is_dir($SDR.$dir."/".$i))
					{
						J_SetPerm($dir."/".$i);
						J_dirPermi($dir."/".$i);
					}
					else
						J_SetPerm($dir."/".$i);
				}
			}
			closedir($d);
		}
	}
	J_dirPermi("");
	echo "<script>parent.j_1.src='".$ROOT."/system/set.php?J_active=1&J_framesize=240'</script>";
}

$j_h=J_Extract($s,'_SESSION\["J_host"\]\:"(.*?)"');
$j_r=J_Extract($s,'_SESSION\["J_root"\]\:"(.*?)"');
$j_u=J_Extract($s,'_SESSION\["J_user"\]\:"(.*?)"');
$j_p=J_Extract($s,'_SESSION\["J_pass"\]\:"(.*?)"');

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";
echo "<hr><tt>FTP details are required to access key system files and to handle file and folder permissions used by the system. The FTP settings are the same as those used to upload the system to this location.<br><var>If you fail to set the FTP details here, go to you control panel and change the file permissions to 0777 (all checkboxes on).</var></tt><hr>";
echo "<form method=post>";
echo "<table width=280>";
echo "<tr><td>Domain</td><td width=80%><input type=text name=h title='Requires value' value='".(isset($j_h)?$j_h:(isset($_SESSION['J_host'])?$_SESSION['J_host']:$_SERVER['HTTP_HOST']))."' onmouseover=\"J_TT(this,'The domain name for this site. A domain must be already setup and point to this sites root directory.')\" class=jW100></td></tr>";
echo "<tr><td>Root</td><td><input type=text name=r value='".(isset($j_r)?$j_r:(isset($_SESSION['J_root'])?$_SESSION['J_root']:""))."' onmouseover=\"J_TT(this,'The folder in which the index.php file is found')\" class=jW100></td></tr>";
echo "<tr><td>Username</td><td><input type=text name=u value='".(isset($j_u)?$j_u:(isset($_SESSION['J_user'])?$_SESSION['J_user']:""))."' title='Requires value' onmouseover=\"J_TT(this,'The user name used to access this site')\" class=jW100></td></tr>";
echo "<tr><td>Password</td><td><input type=password name=p value='".(isset($j_p)?$j_p:(isset($_SESSION['J_pass'])?$_SESSION['J_pass']:""))."' title='Requires value' onmouseover=\"J_TT(this,'The users password for this site')\" class=jW100></td></tr>";
echo "</table><hr><input type=submit value=OK>";
echo "</form>";
$J_title1="FTP";
$J_icon="<img src=".$ROOT."/ico/set/net_set.png>";
$J_label=1;
$J_nostart=1;
$J_nomax=1;
$J_width=400;
$J_height=320;
$J_home=0;
$J_join="SysSet";
include $SDR."/system/deploy.php";
echo "</body></html>";
if(!isset($_SESSION["J_host"]) && isset($_GET["j_setup"]) && $_GET["j_setup"]==1)
{
	unset($_GET["j_setup"]);
	echo "<script>alert('ATTENTION!\\nFTP Settings need to be set first.')</script>";
}
?>