<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";

J_SetPerm("/set",0777);
J_SetPerm($SDR."/set/index_head.php");
$s=file_get_contents($SDR."/set/index_head.php");

if(isset($_POST["d"]))
{
	$m="";
	$s=preg_replace('~echo "<meta name=description content=\'.*?\'>";\n~i',"",$s);
	$s=preg_replace('~echo "<meta name=keywords content=\'.*?\'>";\n~i',"",$s);
	$s=preg_replace('~echo "<meta name=author content=\'.*?\'>";\n~i',"",$s);
	$s=preg_replace('~echo "<meta name=copyright content=\'.*?\'>";\n~i',"",$s);
	$s=preg_replace('~echo "<meta name=rating content=\'.*?\'>";\n~i',"",$s);
	$s=preg_replace('~echo "<meta http-equiv=content-language content=\'.*?\'>";\n~i',"",$s);
	$s=preg_replace('~echo "<meta http-equiv=content-type content=\'text/html;charset=.*?\'>";\n~i',"",$s);

	function J_cleen($v)
	{
		$v=str_replace("  "," ",$v);
		$v=preg_replace("~[^a-zA-Z0-9 ,\-\.]~s","",$v);
		$v=substr($v,0,256);
		$v=trim($v);
		$v=strtolower($v);
		return $v;
	}

	$z=J_cleen($_POST["mtd"]);
	if($z)$m.='echo "<meta name=description content=\''.$z.'\'>";'."\n";
	$z=J_cleen($_POST["mtk"]);
	if($z)$m.='echo "<meta name=keywords content=\''.$z.'\'>";'."\n";
	$z=J_cleen($_POST["mta"]);
	if($z)$m.='echo "<meta name=author content=\''.$z.'\'>";'."\n";
	$z=J_cleen($_POST["mtc"]);
	if($z)$m.='echo "<meta name=copyright content=\''.$z.'\'>";'."\n";
	$z=$_POST["mtr"];
	if($z)$m.='echo "<meta name=rating content=\''.$z.'\'>";'."\n";
	$z=$_POST["mtl"];
	if($z)$m.='echo "<meta http-equiv=content-language content=\''.$z.'\'>";'."\n";
	$z=$_POST["mts"];
	if($z)$m.='echo "<meta http-equiv=content-type content=\'text/html;charset='.$z.'\'>";'."\n";

	if($m)
		$s=str_replace("?>",$m."?>",$s);

	if($w=fopen($SDR."/set/index_head.php","r+"))
	{
		include_once $SDR."/system/activity.php";
		J_act("System",4,"Index Meta Tags");
		$w=fopen($SDR."/set/index_head.php","w+");
		fwrite($w,$s);
		fclose($w);
		$r=1;
	}
}

echo "<html>";
echo "<style type=text/css>#jtb input,#jtb select,#jtb textarea{width:100%}</style>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>if(j_P.j_G['edmt'])j_P.J_app('".$ROOT."/system/f/metas.js')</script> ";
echo "<body>";
echo "<tt>Determine language, page behavior and search engine helpers. Note only 160 characters per tag</tt><hr>";
echo "<form method=post>";
echo "<table width=100%>";
echo "<tbody id=jtb>";
echo "<tr><td>Description</td><td width=90%><textarea name=mtd rows=3>".J_Extract($s,"<meta name=description content='(.*?)'>")."</textarea></td></tr>";
echo "<tr><td>Keywords</td><td><textarea name=mtk rows=3>".J_Extract($s,"<meta name=keywords content='(.*?)'>")."</textarea></td></tr>";
echo "<tr><td>Author</td><td><input type=text name=mta value='".J_Extract($s,"<meta name=author content='(.*?)'>")."'></td></tr>";
echo "<tr><td>Copyright</td><td><input type=text name=mtc value='".J_Extract($s,"<meta name=copyright content='(.*?)'>")."'></td></tr>";
$z=J_Extract($s,"<meta name=rating content='(.*?)'>");
echo "<tr><td>Rating</td><td><select name=mtr><option value=0><script>document.write(j_P.j_meta_rating".($z?".replace('=".$z.">','=".$z." selected>')":"").")</script></select></td></tr>";
$z=strtoupper(J_Extract($s,"<meta http-equiv=content-language content='(.*?)'>"));
echo "<tr><td>Language</td><td><select name=mtl><option value=0><script>document.write(j_P.j_meta_lang".($z?".replace('=".$z.">','=".$z." selected>')":"").")</script></select></td></tr>";
$z=J_Extract($s,"<meta http-equiv=content-type content='text/html;charset=(.*?)'>");
echo "<tr><td>Charset</td><td><select name=mts><option value=0><script>document.write(j_P.j_meta_charset".($z?".replace('=".$z.">','=".$z." selected>')":"").")</script></select></td></tr>";
echo "</tbody>";
echo "<tr><td></td><td><input type=submit name=d value=OK></td></tr>";
echo "</table>";
echo "</form>";
$J_title1="Meta Tags";
$J_icon="<img src=".$ROOT."/ico/set/appgo.png>";
$J_nostart=1;
$J_nomax=1;
$J_width=480;
$J_height=420;
$J_label=1;
$J_home=0;
$J_join="SysSet";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>