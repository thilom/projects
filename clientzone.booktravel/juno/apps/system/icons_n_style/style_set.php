<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";

$set=J_DirItem("/style/","~",1);
$set=trim($set,"~");

if(isset($_POST["d"]))
{
	if($_POST["d"]!=$set)
	{
		$d="/style/";
		J_SetPerm($d);
		J_SetPerm($d.$_POST["d"]);
		J_SetPerm($d."~".$set);
		J_SetPerm($d."set");
		rename($SDR.$d."set",$SDR.$d.$set);
		rename($SDR.$d.$_POST["d"],$SDR.$d."set");
		rename($SDR.$d."~".$set,$SDR.$d."~".$_POST["d"]);
		$r=rand(100,999);
		$n=0;
		if($n=J_DirItem("/style/set/","page.css") && is_file($SDR.$d.$n))
			rename($SDR.$d."set/".$n,$SDR.$d.$r."~page.css");
		if($n=J_DirItem("/style/set/","gui.js") && is_file($SDR.$d.$n))
			rename($SDR.$d."set/".$n,$SDR.$d.$r."~gui.js");
		$set=$_POST["d"];
		$r=1;
		include_once $SDR."/system/activity.php";
		J_act("System Style Set",4,$_POST["d"]);
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";
echo "<tt>This setting will determine which style set will be used through out the system. You can swap these around at will. NOTE! That changes made in one set are not available in another.</tt><hr>";
echo "<form method=post>";
echo "<select name=d>";
J_DirList("/style/",2);
$f="";
if(count($J_filelist))
{
	asort($J_filelist);
	foreach($J_filelist as $k => $v)
		$f.=($v=="settings" || $v=="gui" || strpos($v,"~")!==false?"":"<option value=".($v=="set"?$set:$v).($v=="set"?" selected":"").">".($v=="set"?$set:$v));
}

echo $f;
echo "</select>";
echo "<hr><input type=submit value=OK>";
if(isset($r))
	echo " <input type=button value=Restart onclick=j_P.location.reload(true)>";
echo "</form>";
$J_title1="Stylesheet Set";
$J_icon="<img src=".$ROOT."/ico/set/style.png>";
$J_nostart=1;
$J_nomax=1;
$J_width=320;
$J_height=290;
$J_label=1;
$J_home=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>