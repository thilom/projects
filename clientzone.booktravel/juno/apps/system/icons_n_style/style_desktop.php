<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/css.php";

J_SetPerm("/style/set");
$p="/style/set/gui.css";
J_SetPerm($p);
$s=file_get_contents($SDR.$p);
$s=J_css_prep($s);
$rep=array("","repeat-y","repeat-x","no-repeat");

if(count($_POST))
{
	$d="/style/set/desktop/";
	$t=rand(100,999);
	$x=array("",".gif",".jpg",".png");

	function J_doBg($i)
	{
		global $SDR,$ROOT,$d,$t,$x,$J_err;
		$a=J_DirItem($d,"BG".$i.".png");
		if(!$a)$a=J_DirItem($d,"BG".$i.".jpg");
		if(!$a)$a=J_DirItem($d,"BG".$i.".gif");
		if(isset($_FILES["i".$i]) && $_FILES["i".$i]["size"])
		{
			$s=getimagesize($_FILES["i".$i]["tmp_name"]);
			if($s[2]==1 || $s[2]==2 || $s[2]==3)
			{
				if($a)
				{
					J_SetPerm($d.$a);
					unlink($SDR.$d.$a);
				}
				$e=$d.$t."~BG".$i.$x[$s[2]];
				move_uploaded_file($_FILES["i".$i]["tmp_name"],$SDR.$e);
				$_POST["d".$i]=$ROOT.$e;
			}
		}
		elseif(!isset($_POST["d".$i]))
		{
			$_POST["r".$i]="";
			$_POST["p".$i]="";
		}
	}

	J_SetPerm($SDR."style/set/desktop");
	$i=0;
	while($i<5)
	{
		J_doBg($i);
		$k=($i?($i==4?"#jWB":"#jB".$i):"body");
		if(!$i || $i==4)
			$s=J_css_insert($s,$k,"background-color",J_css_color($_POST["c".$i]));
		$m=isset($_POST["d".$i])?"url(".$_POST["d".$i].")":"";
		$s=J_css_insert($s,$k,"background-image",$m);
		$s=J_css_insert($s,$k,"background-repeat",($m?$rep[$_POST["r".$i]]:""));
		$s=J_css_insert($s,$k,"background-position",($m?strtolower($_POST["p".$i]):""));
		$i++;
	}

	$s=J_css_insert($s,"#jWB","opacity",($_POST["ob"]>0?$_POST["ob"]/100:$_POST["ob"]));
	$s=J_css_insert($s,"#jWB","filter",($_POST["ob"]>0?"alpha(opacity=".$_POST["ob"].")":""));
	$s=J_css_insert($s,"#jDI a","color",($_POST["ic"]?$_POST["ic"]:""));
	$s=J_css_insert($s,"#jDI a:hover","color",($_POST["ich"]?$_POST["ich"]:""));

	$n=$SDR.$p;
	if($h=fopen($n,"r+"))
	{
		include_once $SDR."/system/activity.php";
		J_act("System Desktop",4);
		$h=fopen($n,"w+");
		fwrite($h,J_css_finish($s));
		fclose($h);
		$s=J_css_prep(file_get_contents($n));
	}
	echo "<script>window.parent.location='".$ROOT.(file_exists($SDR."/set/index.php")?"/set/index":"/index").".php?page=/apps/system/icons_n_style/style_desktop.php'</script>";
	die();
}

include $SDR."/system/get.php";

function doIm($i,$c="")
{
	global $SDR,$ROOT;
	$e="";
	$d="/style/set/desktop/";
	$v=J_DirItem($d,"BG".$i.".png");
	if(!$v)$v=J_DirItem($d,"BG".$i.".jpg");
	if(!$v)$v=J_DirItem($d,"BG".$i.".gif");
	if($v && is_file($SDR.$d.$v))
	{
		$s=getimagesize($SDR.$d.$v);
		$s=J_Proportion($s[0],$s[1],140,20);
		$e="<tr><td>Display</td><td><input type=checkbox name=d".$i." value=".$ROOT.$d.$v." onmouseover=\"J_TT(this,'<img src=".$ROOT.$d.$v." style=height:".$s[1]."px;width:".$s[0]."px>')\"".($c?" checked":"")."</td></tr>";
	}
	return $e;
}

echo "<html>";
echo "<style type=text/css>.js{border-style:solid;border-width:1px;border-color:#888;white-space:nowrap}</style>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>hmi='Only .png .jpg and .gif files allowed. Ensure images are optimised for quick loading';hmp='You can use 2 values here,<br>ie: top center, or right bottom<br>or use 50px 50px, or 99px center'";
echo ";function hcP(t,o){j_P.J_Color(t,o)}";
echo "</script>";
echo "<body>";
echo "<form method=post enctype=multipart/form-data>";
echo "<table width=240>";
$i=0;
while($i<5)
{
	$k=(!$i?"body":($i==4?"#jWB":"#jB".$i));
	echo "<tr><td colspan=2><b>".(!$i?"Background":($i==4?"Focus":($i==3?"Logo":"Layer ".$i)))."</b></td></tr>";
	if(!$i||$i==4)
	{
		$a=J_css_extract($s,$k,"background-color");
		echo "<tr><td>Color</td><td width=90%><input type=text name=c".$i." value='".$a."' onclick=hcP(this,\$('s1')) class=jW100></td><td><span id=s1 class=js style=".($a?"background-color:".$a.";":"").">&nbsp; &nbsp; &nbsp;</span></tr>";
	}
	echo "<tr><td>Image</td><td><input type=file name=i".$i." onmouseover=J_TT(this,hmi)></td></tr>";
	echo doIm($i,(J_css_extract($s,$k,"background-image")?1:0));
	$a=J_css_extract($s,$k,"background-repeat");
	echo "<tr><td>Repeat</td><td><select name=r".$i." class=jW100><option value=0><option value=1".($a==$rep[1]?" selected":"").">Repeat Y<option value=2".($a==$rep[2]?" selected":"").">Repeat X<option value=3".($a==$rep[3]?" selected":"").">No Repeat</select></td></tr>";
	echo "<tr><td>Position</td><td><input type=text name=p".$i." value='".J_css_extract($s,$k,"background-position")."' class=jW100 onmouseover=J_TT(this,hmp)></td></tr>";
	if($i!=4)echo "<tr><td colspan=2><hr></td></tr>";
	$i++;
}
$a=J_css_extract($s,"#jWB","opacity")*100;
echo "<tr><td>Opacity</td><td><select name=ob>";
$o=str_replace("value=".$a.">","value=".$a." selected>","<option value=0>0%<option value=10>10%<option value=20>20%<option value=30>30%<option value=40>40%<option value=50>50%<option value=60>60%<option value=70>70%<option value=80>80%<option value=90>90%<option value=100>100%");
echo $o;
echo "</select></td></tr>";
echo "<tr><td colspan=2><hr></td></tr>";
echo "<tr><td colspan=2><b>Icons</b></td></tr>";
$a=J_css_extract($s,"#jDI a","color");
$a=($a?$a:"#000000");
echo "<tr><td>Color</td><td><input type=text name=ic value='".$a."' class=jW100 onclick=hcP(this,\$('bi'))></td><td><span id=bi class=js style=background-color:".$a.">&nbsp; &nbsp; &nbsp;</span></tr>";
$a=J_css_extract($s,"#jDI a:hover","color");
$a=($a?$a:"#FFFFFF");
echo "<tr><td>Hover</td><td><input type=text name=ich value='".$a."' class=jW100 onclick=hcP(this,\$('bi'))></td><td><span id=bi class=js style=background-color:".$a.">&nbsp; &nbsp; &nbsp;</span></tr>";
echo "</table>";
echo "<hr><input type=submit value=OK onclick=J_opaq()></form>";
$J_title1="Style Desktop";
$J_icon="<img src=".$ROOT."/ico/set/style_desktop.png>";
$J_nostart=1;
$J_nomax=1;
$J_width=430;
$J_height=600;
$J_label=1;
$J_home=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>