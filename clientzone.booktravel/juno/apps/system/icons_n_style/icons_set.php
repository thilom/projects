<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
$set=J_DirItem("/ico/","~",1);
$set=trim($set,"~");

if(isset($_POST["d"]))
{
	if($_POST["d"]!=$set)
	{
		include_once $SDR."/system/activity.php";
		J_act("System Icon Set",4,$_POST["d"]);
		$d="/ico/";
		J_SetPerm($SDR.$d);
		J_SetPerm($d.$_POST["d"]);
		J_SetPerm($d."~".$set);
		J_SetPerm($d."set");
		rename($SDR.$d."set",$SDR.$d.$set);
		rename($SDR.$d.$_POST["d"],$SDR.$d."set");
		rename($SDR.$d."~".$set,$SDR.$d."~".$_POST["d"]);
		$r=1;
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body><tt>This setting will determine which icon set will be used through out the system. You can swap these around at will. NOTE! Icon sets are subject to refresh in some browsers.</tt><hr><form method=post>";
echo "<select name=d>";
J_DirList("/ico/",2);
$f="";
if(count($J_filelist))
{
	asort($J_filelist);
	foreach($J_filelist as $k => $v)
		$f.=($v=="settings" || strpos($v,"~")!==false?"":"<option value=".($v=="set"?$set:$v).($v=="set"?" selected":"").">".($v=="set"?$set:$v));
}
echo $f;
echo "</select>";
echo "<hr><input type=submit value=OK>";
if(isset($r))
	echo " <input type=button value=Restart onclick=j_P.location.reload(true)>";
echo "</form>";
$J_title1="Icon Set";
$J_icon="<img src=".$ROOT."/ico/set/forward.png>";
$J_nostart=1;
$J_nomax=1;
$J_width=320;
$J_height=280;
$J_label=1;
$J_home=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>