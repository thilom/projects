<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/css.php";

$q="/style/set/gui.css";
J_SetPerm($q);
$s=J_css_prep(file_get_contents($SDR.$q));

$res=array();
$res["Default"]=1;
$res["Admin"]=1;
$res["Pages"]=1;
$res["Comments"]=1;
$res["Applications"]=1;
$res["People"]=1;
$res["Companies"]=1;
$res["Media"]=1;
$res["Access"]=1;
$res["Database"]=1;
$res["Help"]=1;
$rep=array("","repeat-y","repeat-x","no-repeat");

if(isset($_POST["d"]))
{
	J_SetPerm("style/");
	$d="/style/set/labels/";
	J_SetPerm($d);
	$tt=rand(100,999);
	$x=array("",".gif",".jpg",".png");

	function J_doImg($i,$h=0)
	{
		global $SDR,$d,$tt,$x;
		$e="";
		$a=J_DirItem($d,"_".$i.".png");
		if(!$a)$a=J_DirItem($d,"_".$i.".jpg");
		if(!$a)$a=J_DirItem($d,"_".$i.".gif");
		if($a)
		{
			if(isset($_POST["d_".$i]))
				$e="url(".$ROOT.$d.$a.")";
			else
			{
				J_SetPerm($d.$a);
				@unlink($SDR.$d.$a);
			}
		}

		if(isset($_FILES["u_".$i]) && $_FILES["u_".$i]['size'])
		{
			$s=getimagesize($_FILES["u_".$i]["tmp_name"]);
			if($s[2]==1 || $s[2]==2 || $s[2]==3)
			{
				if($a)
				{
					J_SetPerm($d.$a);
					unlink($SDR.$d.$a);
				}
				$e=$d.$tt."_".($h?$h:$i).$x[$s[2]];
				copy($_FILES["u_".$i]["tmp_name"],$SDR.$e);
				$e="url(".$e.")";
			}
		}
		return $e;
	}

	function J_doName($v="Untitled")
	{
		global $res;
		$v=ucwords(strtolower(substr($v,0,16)));
		if(isset($res[$v]))$v=$v."_1";
		return $v;
	}

	foreach($_POST as $k => $v)
	{
		$i=substr($k,2);
		if(isset($_POST["x_".$i]) && $_POST["x_".$i]==1)
		{
			$m=J_css_extract($s,".jW #jlabel_".$i,"background-image");
			if($m)
			{
				$m=str_replace("url(","",str_replace(")","",str_replace(" ","",$m)));
				J_SetPerm($d.$m);
				@unlink($SDR.$d.$m);
			}
			$s=preg_replace('~/\*jlabel_'.$i.'{.*?}\*/~',"",$s);
			$s=preg_replace("~\.jW #jlabel_".$i."{.*?}~","",$s);
			$s=preg_replace("~#jWI #jlabel_".$i."{.*?}~","",$s);
		}
		else
		{
			$i=substr($k,0,2);
			if($i=="c_")
			{
				$i=substr($k,2);
				$c=J_css_color($_POST["c_".$i]);
				$t=J_css_color($_POST["t_".$i]);
				$m=($_FILES["u_".$i] || !isset($_POST["d_".$i])?1:0);
				$r=$rep[$_POST["r_".$i]];
				$p=strtolower($_POST["p_".$i]);
				$c=($c=="#FFF" || $c=="#FFFFFF"?"":$c);
				$t=($t=="#000" || $t=="#000000"?"":$t);
				if(isset($_POST["n_".$i]))
				{
					if($_POST["l_".$i] && ($c || $t || $m))
					{
						$j=0;
						while($j<64)
						{
							if(strpos($s,"#jWI #jlabel_".$j."{")!==false){}
							else
							{
								preg_match_all("~\\n #jWI #jlabel_".($j-1)."{(.*?)}~i",$s,$u);
								$n=J_doName($_POST["l_".$i]);
								$m=J_doImg($i,$j);
								$a="#jWI #jlabel_".($j-1)."{".$u[1][0]."}";
								$b="\n /*jlabel_".$j."{ name:".$n."; }*/";
								$b.="\n .jW #jlabel_".$j."{ ";
								$b.=($c?"background-color:".$c."; ":"");
								$b.=($t?"color:".$t."; ":"");
								if($m)
								{
									$b.="background-image:".$m."; ";
									if($r)$b.="background-repeat:".$r."; ";
									if($p)$b.="background-position:".$p."; ";
								}
								$b.=" }";
								$b.="\n #jWI #jlabel_".$j."{ ".($c?"background-color:".$c."; ":"")."}";
								$s=str_replace($a,$a.$b,$s);
								break;
							}
							$j++;
						}
					}
				}
				else
				{
					if(isset($_POST["l_".$i]))
					{
						$n=J_doName($_POST["l_".$i]);
						$s=J_css_insert($s,"/*jlabel_".$i,"name",$n);
					}
					$m=J_doImg($i);
					$s=J_css_insert($s,".jW #jlabel_".$i,"background-color",$c);
					$s=J_css_insert($s,".jW #jlabel_".$i,"color",$t);
					$s=J_css_insert($s,".jW #jlabel_".$i,"background-image",($m?$m:""));
					$s=J_css_insert($s,".jW #jlabel_".$i,"background-repeat",($m && $r?$r:""));
					$s=J_css_insert($s,".jW #jlabel_".$i,"background-position",($m && $p?$p:""));
					$s=J_css_insert($s,"#jWI #jlabel_".$i,"background-color",$c);
				}
			}
		}
	}

	$n=$SDR.$q;
	if($w=fopen($n,"r+"))
	{
		include_once $SDR."/system/activity.php";
		J_act("System Labels",4);
		$w=fopen($n,"w+");
		fwrite($w,J_css_finish($s));
		fclose($w);
	}
}

include $SDR."/system/get.php";

echo "<html>";
echo "<style type=text/css>th input{font-weight:bold;width:100px}.j{border-style:solid;border-width:1px;border-color:#888;white-space:nowrap}#jdp{width:256px;height:256;background-repeat:no-repeat;background-position:center}#jtb{color:#ccc}</style>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>hmi='Only .png .jpg and .gif files allowed. Ensure images are optimised for quick loading';hmp='You can use 2 values here,<br>ie: top center, or right bottom<br>or use 50px 50px, or 99px center'";
echo ";function hcP(t,o){j_P.J_Color(t,o)}";
echo "</script>";
echo "<body><tt>Labels are used to color code frames and frame-incicator links (found at the bottom of the desktop). You can also create labels for contributors. Note: only hex colors allowed!</tt><hr>";
echo "<form method=post enctype=multipart/form-data><table id=jTR><tbody id=jtb>";
echo "<tr style=font-size:9pt class=jO50><th></th><th>Label</th><th colspan=2>Background</th><th colspan=2>Text Color</th><th width=1></th><th>Image</th><th>Repeat</th><th>Position</th></tr>";
$o1="";
$o2="";
$i=0;
$x=0;
while($i<64)
{
	$n=J_css_extract($s,"/*jlabel_".$i,"name");
	if($n)
	{
		$c=J_css_extract($s,".jW #jlabel_".$i,"background-color");
		$t=J_css_extract($s,".jW #jlabel_".$i,"color");
		$m=J_css_extract($s,".jW #jlabel_".$i,"background-image");
		$r=J_css_extract($s,".jW #jlabel_".$i,"background-repeat");
		$p=J_css_extract($s,".jW #jlabel_".$i,"background-position");
		$c=($c==""?"#FFFFFF":$c);
		$t=($t==""?"#000000":$t);
		$o="<tr><th>".$i."</th><th><input type=text name=l_".$i." value='".$n."'".(isset($res[$n])?" disabled":"")."></th><td><input type=text name=c_".$i." value='".$c."' size=7 onclick=hcP(this,\$('c".$i."'))></td><td><span id=c".$i.($c?" style=background-color:".$c:"")." class=j>&nbsp; &nbsp; &nbsp;</span></td><td><input type=text name=t_".$i." value='".$t."' onclick=hcP(this,\$('t".$i."')) size=7></td><td><span id=t".$i.($t?" style=background-color:".$t:"")." class=j>&nbsp; &nbsp; &nbsp;</span></td>";
		$o.="<td>";
		if($m)
		{
			$m=str_replace(" ","",str_replace("url(/juno","",str_replace(")","",$m)));
			if(is_file($SDR.$m))
			{
				$s=getimagesize($SDR.$m);
				$s=J_Proportion($s[0],$s[1],140);
				$o.="<input type=checkbox name=d_".$i." value=1 onmouseover=\"J_TT(this,'<img src=".$m." style=width:".$s[0]."px;height:".$s[1]."px>')\" checked>";
			}
		}
		$o.="</td>";
		$o.="<td><input type=file name=u_".$i." onmouseover=J_TT(this,hmi)></td>";
		$o.="<td><select name=r_".$i."><option value=0><option value=1".($r==$rep[1]?" selected":"").">Repeat Y<option value=2".($r==$rep[2]?" selected":"").">Repeat X<option value=3".($r==$rep[3]?" selected":"").">No Repeat</select></td>";
		$o.="<td><input type=text name=p_".$i." value='".$p."' onmouseover=J_TT(this,hmp) size=12></td>";
		if(!isset($res[$n]))$o.="<td><input type=button value=&#10006 onclick=J_xr(this,".$i.") onmouseover=J_TT(this,'Remove')></td>";
		$o.="</tr>";
		if(!isset($res[$n]))$o.="<input type=hidden name=x_".$i.">";
		if(isset($res[$n]))$o1.=$o;
		else $o2.=$o;
		$x++;
	}
	$i++;
}
echo $o1.$o2;

echo "</tbody><tr><td colspan=99><input type=button value=&#10010; onclick=J_newRow() onmouseover=J_TT(this,'Add')></td></tr></table><hr><input type=submit name=d value=OK>";
if(isset($z))echo " <input type=button value=Restart onclick=j_P.location.reload(true)>";
echo "</form>";
echo "<script>var hn=64,hc=".$x.";";
echo "function J_newRow(){var tr=document.createElement('tr');var td=document.createElement('th');td.innerHTML=hc;tr.appendChild(td);var td=document.createElement('th');td.innerHTML=\"<input type=text name=l_\"+hn+\">\";tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<input type=text name=c_\"+hn+\" value=#FFFFFF onclick=hcP(this,\$('c\"+hn+\"')) size=7>\";tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<span id=c\"+hn+\" value=#FFFFFF class=j>&nbsp; &nbsp; &nbsp;</span>\";tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<input type=text name=t_\"+hn+\" value=#000000 onclick=hcP(this,\$('t\"+hn+\"')) size=7>\";tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<span id=t\"+hn+\" style=background-color:#000000 class=j>&nbsp; &nbsp; &nbsp;</span>\";tr.appendChild(td);td=document.createElement('td');tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<input type=file name=u_\"+hn+\" onmouseover=J_TT(this,hmi)>\";tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<select name=r_\"+hn+\"><option value=0><option value=1>Repeat Y<option value=2>Repeat X<option value=3>No Repeat</select>\";tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<input type=text name=p_\"+hn+\" value='' onmouseover=J_TT(this,hmp) size=12>\";tr.appendChild(td);td=document.createElement('td');td.innerHTML=\"<input type=button value=&#10006 onclick=J_xr(this)><input type=hidden name=n_\"+hn+\" value=1>\";tr.appendChild(td);\$('jtb').appendChild(tr);hc++;hn++;J_tr()}";
echo "function J_xr(t,v){if(confirm('Sure?')){t=t.parentNode.parentNode;t.parentNode.removeChild(t);if(v)\$N('x_'+v)[0].value=1}}";
echo "J_tr()";
echo "</script>";
$J_title1="Style Labels";
$J_icon="<img src=".$ROOT."/ico/set/style_lable.png>";
$J_nostart=1;
$J_width=900;
$J_height=800;
$J_label=1;
$J_home=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>