<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";

if(isset($_GET["d"]))
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/system/f/permissions.js></script>";
	$d="/".trim($_GET["d"],"/");
	$f=is_dir($SDR."/".$d)?1:0;
	$w=J_GetPerm($d);
	$u=substr($w,1,1);
	$g=substr($w,2,1);
	$w=substr($w,3,1);
	$d=$d?trim($d,"/"):"";
	echo "<script>setP(";
	echo "\"".$d."\"";
	echo ",".$f;
	echo ",".$u;
	echo ",".$g;
	echo ",".$w;
	if(isset($_GET["hrl"]))
		echo ",\"".$_GET["hrl"]."\"";
	echo ")</script>";
	$n=substr($d,(strpos($d,"/")!==false?strrpos($d,"/")+1:0));
	$J_framesize=260;
	include $SDR."/system/deploy.php";
	echo "</body></html>";
	die();
}

if(isset($_GET["j_hide"]))
{
	echo "<html><script src=".$ROOT."/system/P.js></script>"."<body></body></html>";
	die();
}

elseif(isset($_GET["jget"]))
{
	$h=J_DirSub($_GET["jget"],2);
	echo "<script>window.parent.JL_p_get(0,\"".$h."\",\"".$_GET["jid"]."\")</script>";
	die();
}

elseif(isset($_POST["d"]))
{
	include_once $SDR."/system/activity.php";
	$m="0".$_POST["u"].$_POST["g"].$_POST["w"];
	$d="/".trim($_POST["d"],"/");
	if(isset($_POST["a"]))
	{
		function J_dirPermi($dir)
		{
			global $SDR,$m;
			if(is_dir($SDR.$dir) && is_readable($SDR.$dir))
			{
				$d=opendir($SDR.$dir);
				while(FALSE!==($i=readdir($d)))
				{
					if($i=="." || $i==".."){}
					else
					{
						if(is_dir($SDR.$dir."/".$i))
						{
							J_SetPerm($dir."/".$i,$m);
							J_dirPermi($dir."/".$i);
						}
						else
							J_SetPerm($dir."/".$i,$m);
					}
				}
				closedir($d);
			}
		}
		J_SetPerm($d,$m);
		J_dirPermi($d);
		J_act("System Permissions",4,$d." + subs",$m);
	}
	else
	{
		J_SetPerm($d,$m);
		J_act("System Permissions",4,$d,$m);
	}
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script>j_P.J_frame(j_W,0,ROOT+'/apps/system/permissions.php');j_P.J_display(j_W,0,-1);J_icoAlert()</script>";
	die();
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/system/f/permissions.js></script>";
echo "<script>JL_d(\"".J_GetPerm("")."\",\"".J_DirSub("",2)."\")</script>";
$J_framesize=-2;
$J_title1="Permissions";
$J_icon="<img src=".$ROOT."/ico/set/page_lock.png>";
$J_nostart=1;
$J_width=720;
$J_height=800;
$J_label=1;
$J_home=0;
$J_join="SysSet";
$J_frame1=$ROOT."/apps/system/permissions.php?j_hide";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>