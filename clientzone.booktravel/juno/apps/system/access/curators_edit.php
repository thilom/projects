<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

if(isset($_SESSION["j_user"]) && (J_getSecure() || mysql_num_rows(mysql_query("SELECT j_ucu_id FROM nse_user_curator WHERE j_ucu_user=".$_SESSION["j_user"]["id"]." AND j_ucu_expiry>".$EPOCH." LIMIT 1"))))
{}
else
{
	include $SDR."/system/secure.php";
	die();
}

if(count($_POST))
{
	include_once $SDR."/system/get.php";
	include_once $SDR."/system/parse.php";
	include_once $SDR."/system/activity.php";
	include_once $SDR."/apps/system/email/f/send.php";
	$c=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1"));
	$curator="<a href='mailto:".$c["email"]."'>".$c["firstname"]." ".$c["lastname"]."</a>";
	$dmy=date("d/m/Y H:i",$EPOCH);
	foreach($_POST as $k => $v)
	{
		$id=substr($k,0,2);
		if($id=="e_")
		{
			$id=substr($k,2);
			$u=0;
			$x=J_dateParse($v);
			if(isset($_POST["u_".$id]))
			{
				if(isset($_POST["x_".$id]) && $x>$EPOCH)
				{
					if($x!=J_dateParse($_POST["eo_".$id]))
					{
						$u=$_POST["u_".$id];
						mysql_query("UPDATE nse_user_curator SET j_ucu_expiry=".$x.",j_ucu_curator=".$_SESSION["j_user"]["id"]." WHERE j_ucu_id=".$id);
						J_act("Curator",4,"Expiry: ".$_POST["e_".$id],$u);
						$m="<b>Curator Extension</b><hr>Your Curatorship has been extended to ". $_POST["e_".$id]." on ".$j_site_name."<hr><span style='font-size:9pt'>Excecuted by: ".$curator." (".$dmy.")</span>";
						J_sendMail("Curatorship Extension",$m,$u,0,0);
					}
				}
				else
				{
					mysql_query("DELETE FROM nse_user_curator WHERE j_ucu_id=".$id);
					J_act("Curator",5,"",$u);
					$m="<b>Curator Termination</b><hr>You are no longer a Curator for ".$j_site_name."<hr><span style='font-size:9pt'>Excecuted by: ".$curator." (".$dmy.")</span>";
					J_sendMail("Curator Termination",$m,$u,0,0);
				}
			}
			elseif(isset($_POST["n_".$id]) && $_POST["n_".$id]>0)
			{
				if($_POST["n_".$id]>0)
				{
					$u=$_POST["n_".$id];
					mysql_query("INSERT INTO nse_user_curator (j_ucu_user,j_ucu_expiry,j_ucu_date,j_ucu_curator) VALUES (".$_POST["n_".$id].",".J_dateParse($_POST["e_".$id]).",".$EPOCH.",".$_SESSION["j_user"]["id"].")");
					J_act("Curator",3,"Exp: ".$_POST["e_".$id],$u);
					$m="<b>Curatorship</b><hr>You have been made a Curator for ".$j_site_name." until ". $_POST["e_".$id].".<br>Please contact ".$j_site_name." Curators or administrators is you have any queries.<hr><span style='font-size:9pt'>Excecuted by: ".$curator." (".$dmy.")</span>";
					J_sendMail("Curatorship",$m,$u,0,0);
				}
			}
		}
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/system/access/f/curators_edit.js></script>";
echo "<body>";

$c="";
$r=mysql_query("SELECT * FROM nse_user_curator LEFT JOIN nse_user ON j_ucu_user=user_id WHERE j_ucu_expiry>".$EPOCH." ORDER BY firstname,lastname");
if(mysql_num_rows($r))
{
	include_once $SDR."/system/get.php";
	while($g=mysql_fetch_array($r))
	{
		$c.=$g["j_ucu_id"]."~";
		$c.=$g["user_id"]."~";
		$c.=$g["firstname"]." ".$g["lastname"]."~";
		$c.=date("d/m/Y",$g["j_ucu_expiry"])."~";
		$c.="|";
	}
}
echo "<script>j_accc(\"".$c."\")</script>";

$J_title1="Curators Edit";
$J_icon="<img src=".$ROOT."/ico/set/edit.png>";
$J_nomax=1;
$J_nostart=1;
$J_label=8;
$J_height=360;
$J_width=520;
$J_home=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>