<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
if(!J_getCurator())
{
	include $SDR."/system/secure.php";
	die();
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/system/access/f/users.js></script>";
$id=isset($_GET["id"])?$_GET["id"]:0;
$person=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$id." LIMIT 1"));
$role=(isset($_POST["role"])?$_POST["role"]:(isset($_GET["role"])?$_GET["role"]:($id==$_SESSION["j_user"]["id"]?$_SESSION["j_user"]["role"]:substr($person["roles"],0,1))));
unset($_POST["role"]);
include $SDR."/set/roles.php";

if(!$id)
{
	echo "<script>J_axs_s()</script>";
	$J_title1="Assign Access";
	$J_icon="<img src=".$ROOT."/ico/set/lock.png>";
	$J_home=0;
	$J_nomax=1;
	$J_nostart=1;
	$J_label=8;
	$J_height=160;
	$J_width=280;
	$J_join="SysCur";
	include $SDR."/system/deploy.php";
	echo "</body></html>";
	die();
}

if(count($_POST))
{
	include $SDR."/system/dir.php";
	include $SDR."/system/parse.php";
	include_once $SDR."/system/activity.php";
	$set=isset($_POST["set"])?preg_replace('~[^a-zA-Z0-9_\-]~',"",$_POST["set"]):0;
	$setexpiry=isset($_POST["setexpiry"])?J_dateParse($_POST["setexpiry"]):0;
	$newset=!$set&&$_POST["newset"]?preg_replace('~[^a-zA-Z0-9_\-]~',"",$_POST["newset"]):0;
	if($newset)
	{
		J_act("Access",(mysql_num_rows(mysql_query("SELECT j_uas_id FROM nse_user_pages_set WHERE j_uas_name='".$newset."' LIMIT 1"))?4:3),"Set - ".$newset,$id);
		mysql_query("DELETE FROM nse_user_pages_set WHERE j_uas_name='".$newset."'");
	}
	elseif($_POST["delset"])
	{
		mysql_query("DELETE FROM nse_user_pages_set WHERE j_uas_name='".$_POST["delset"]."'");
		J_act("Access",5,"Set - ".$_POST["delset"]);
	}
	if(isset($_POST["delref"]))
	{
		mysql_query("UPDATE nse_user_pages SET j_uac_set='' WHERE j_uac_page!='SuperUser' AND j_uac_user=".$id);
		J_act("Access",4,"All preset references",$id);
	}
	unset($_POST["set"]);
	unset($_POST["setexpiry"]);
	unset($_POST["newset"]);
	unset($_POST["delset"]);
	unset($_POST["delref"]);

	$newsetpages=array();
	$err="";
	$a="";
	$ta=0;
	$msg="";
	$ccm=ucfirst(trim(substr(str_replace("  "," ",str_replace("..",".",str_replace("\n",". ",preg_replace('~[^a-zA-Z0-9 ,;:!@#%&+=_\-\*\(\)\[\]\$\.]~s',"",strip_tags($_POST["msg"]))))),0,255)));
	unset($_POST["msg"]);

	if(!isset($_POST["SuperUser"]))
	{
		$x=(mysql_num_rows(mysql_query("SELECT j_uac_id FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_role='".$role."' AND j_uac_page='SuperUser' LIMIT 1"))?1:0);
		$s="DELETE FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_role='".$role."' AND j_uac_page='SuperUser'";
		mysql_query($s);
		if(mysql_error())
		{
			$err=1;
			echo mysql_error()."<tt>".$s."</tt><hr>";
		}
		else
		{
			J_act("Access",5,"SuperUser (".$roles[$role].")",$id);
			if($x)
				$msg.="<i style='color:#DD0000'><b>Super-User status</b> REVOKED</span><br>";
		}
	}
	else
	{
		$x=J_dateParse($_POST["SuperUser"]);
		if($x>$EPOCH)
		{
			$r=mysql_query("SELECT j_uac_id,j_uac_expiry FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_role='".$role."' AND j_uac_page='SuperUser' LIMIT 1");
			if(mysql_num_rows($r))
			{
				$g=mysql_fetch_array($r);
				$s="UPDATE nse_user_pages SET j_uac_expiry='".$x."',j_uac_curator=".$_SESSION["j_user"]["id"].",j_uac_date=".$EPOCH." WHERE j_uac_id=".$g["j_uac_id"];
				mysql_query($s);
				if(mysql_error())
				{
					$err=1;
					echo mysql_error()."<tt>".$s."</tt><hr>";
				}
				else
				{
					$ta=1;
					J_act("Access",4,"SuperUser (".$roles[$role].") (".$_POST["SuperUser"].")",$id);
					$msg.="<span style='color:green'><b>SuperUser</b> (Role ".$roles[$role].") expires: ".$_POST["SuperUser"]."</span><br>";
				}
			}
			else
			{
				$s="INSERT INTO nse_user_pages (j_uac_expiry,j_uac_user,j_uac_page,j_uac_date,j_uac_curator,j_uac_role) VALUES (".$x.",".$id.",'SuperUser',".$EPOCH.",".$_SESSION["j_user"]["id"].",'".$role."')";
				mysql_query($s);
				if(mysql_error())
				{
					$err=1;
					echo mysql_error()."<tt>".$s."</tt><hr>";
				}
				else
				{
					$ta=1;
					J_act("Access",3,"SuperUser (".$roles[$role].") (".date("d/m/Y",$x).")",$id);
					$msg.="<b>SuperUser</b> (Role ".$roles[$role].") expires: ".$_POST["SuperUser"]."<br>";
				}
			}
		}
	}
	unset($_POST["SuperUser"]);

	$setmask=$_POST["setmask"];
	$newsetmask=$_POST["newsetmask"];
	unset($_POST["setmask"]);
	unset($_POST["newsetmask"]);

	// delete access
	if($set && !$setmask)
		mysql_query("DELETE FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_page!='SuperUser' AND (j_uac_set='".$set."' OR j_uac_role='".$role."')");
	elseif($newset && !$newsetmask)
	{
		mysql_query("DELETE FROM nse_user_pages WHERE j_uac_page!='SuperUser' AND (j_uac_set='".$newset."' OR j_uac_role='".$role."')");
		mysql_query("DELETE FROM nse_user_pages_set WHERE j_uas_name='".$newset."')");
	}


	if(!$set)
	{
		foreach($_POST as $k => $v)
		{
			$x=substr($k,0,2);
			$x=J_dateParse($v);
			$k=str_replace("^",".",$k);
			if(strpos($k,"#")!==false)
			{
				$kk=substr($k,0,strpos($k,"#"));
				$k=$kk.str_replace("_"," ",substr($k,strpos($k,"#")));
			}
			else
				$c=file_get_contents($SDR.$k);
			if($c && strpos($c,"\$J_url=")!==false)
				$k=J_Extract($c,'J_url="(.*?)";');
			$kk=(strpos($k,"#")!==false?substr($k,0,strpos($k,"#")):$k);
			if($kk && file_exists($SDR.$kk) && $x>$EPOCH)
			{
				$r=mysql_query("SELECT j_uac_id,j_uac_expiry FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_role='".$role."' AND j_uac_page='".$k."' AND j_uac_page!='SuperUser' LIMIT 1");
				if(mysql_num_rows($r))
				{
					$g=mysql_fetch_array($r);
					$r=($g["j_uac_expiry"]?1:0);
					if($x!=$g["j_uac_expiry"] || $newset)
					{
						mysql_query("UPDATE nse_user_pages SET ".($x!=$g["j_uac_expiry"]||$newset?"j_uac_expiry=".$x.",j_uac_curator=".$_SESSION["j_user"]["id"].",j_uac_date=".$EPOCH:"").($newset?",j_uac_set='".$newset."'":"")." WHERE j_uac_id=".$g["j_uac_id"]);
						$ta=1;
						J_act("Access",4,$k." (".$roles[$role].") (".$v.($r?"":" REQ").")",$id);
						if($newset)
						{
							if(!$newsetmask || mysql_num_rows(mysql_query("SELECT j_uas_id FROM nse_user_pages_set WHERE j_uas_page='".$k."' AND j_uas_name='".$newset."' LIMIT 1")))
								mysql_query("INSERT INTO nse_user_pages_set (j_uas_name,j_uas_page) VALUES ('".$newset."','".$k."')");
							$newsetpages[]=$k;
						}
					}
				}
				else
				{
					mysql_query("INSERT INTO nse_user_pages (j_uac_expiry,j_uac_user,j_uac_page,j_uac_date,j_uac_curator,j_uac_role) VALUES (".$x.",".$id.",'".$k."',".$EPOCH.",".$_SESSION["j_user"]["id"].",'".$role."')");
					if(strpos($k,"re-open assessments")!==false)
						$ta=1;
					J_act("Access",3,$k." (".$v.")",$id);
					if($newset)
					{
						if(!$newsetmask || mysql_num_rows(mysql_query("SELECT j_uas_id FROM nse_user_pages_set WHERE j_uas_page='".$k."' AND j_uas_name='".$newset."' LIMIT 1")))
							mysql_query("INSERT INTO nse_user_pages_set (j_uas_name,j_uas_page) VALUES ('".$newset."','".$k."')");
						$newsetpages[]=$k;
					}
				}
			}
			else
			{
				mysql_query("DELETE FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_page='".$k."' AND j_uac_role='".$role."' AND j_uac_page!='SuperUser'");
				if(file_exists($SDR.$k))
					J_act("Access",5,$k." (".$roles[$role].") (".$v.")",$id);
			}
		}
	}


	if($set)
	{
		J_act("Access",4,"Applied Set: ".$roles[$role],$id);
		$q="SELECT * FROM nse_user_pages_set WHERE j_uas_name='".$set."'";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
			{
				$r="SELECT j_uac_expiry FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_role='".$role."' AND j_uac_page='".$g["j_uas_page"]."'";
				if(mysql_num_rows(mysql_query($r)))
				{
					if($setexpiry<$EPOCH)
						mysql_query("DELETE FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_page='".$g["j_uas_page"]."' AND j_uac_role='".$role."'");
					else
					{
						$e=mysql_fetch_array(mysql_query($r));
						mysql_query("UPDATE nse_user_pages SET j_uac_set='".$set."'".($e["j_uac_expiry"]?"":",j_uac_expiry=".$setexpiry.",j_uac_curator=".$_SESSION["j_user"]["id"])." WHERE j_uac_user=".$id." AND j_uac_role='".$role."' AND j_uac_page='".$g["j_uas_page"]."'");
					}
				}
				elseif($setexpiry>$EPOCH)
					mysql_query("INSERT INTO nse_user_pages (j_uac_expiry,j_uac_user,j_uac_page,j_uac_date,j_uac_curator,j_uac_set,j_uac_role) VALUES (".$setexpiry.",".$id.",'".$g["j_uas_page"]."',".$EPOCH.",".$_SESSION["j_user"]["id"].",'".$set."','".$role."')");
			}
		}
	}
	elseif($newset)
	{
		$newdate=mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")+5);
		$q=mysql_query("SELECT user_id FROM nse_user WHERE user_id!=".$id." AND roles LIKE '%".$role."%'");
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
			{
				foreach($newsetpages as $k => $v)
				{
					if(!mysql_num_rows(mysql_query("SELECT j_uac_id FROM nse_user_pages WHERE j_uac_user=".$g["user_id"]." AND j_uac_set='".$newset."' AND j_uac_page='".$v."'")))
					{
						mysql_query("INSERT INTO nse_user_pages (j_uac_user,j_uac_page,j_uac_date,j_uac_curator,j_uac_expiry,j_uac_set,j_uac_role) VALUES (".$g["user_id"].",'".$v."',".$EPOCH.",".$_SESSION["j_user"]["id"].",".$newdate.",'".$newset."','".$role."')");
					}
				}
			}
		}
	}


	include $SDR."/apps/system/email/f/send.php";
	$c=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1"));
	$m="<b style='font-size:9pt'>".strtoupper($j_site_name)."</b><br>";
	$m.="<b>ACCESS CONTROL</b><br>";
	$m.="<span style='font-size:9pt'>Edited by: ".$c["firstname"]." ".$c["lastname"]." (Role ".$roles[$role].")</span><hr>";
	$j_site_name=ucfirst($j_site_name);
	$d=date("l H:i, jS F, Y",$EPOCH);
	$m.=$ccm."<br><br>";
	$m.="<span style='font-size:9pt'>Your access on ".$j_site_name." has been updated. ".$d.".";
	if($ta)
		$m.="</span><hr>".$msg;
	else
		$m.="</span><hr>All access privileges have been revoked.";
	J_sendMail("Access Control",$m,$id,$c);
	if(!$err && !isset($_GET["ap"]) && !$set)
	{
		echo "<script>window.parent.J_WX(self)</script>";
		die();
	}
}


$peo=array();
// exclude files
$exc_file=array(
"/apps/system/access/users.php"=>1
,"/system/hok.php"=>1
);
$exc_dir=array(
".svn"=>1
,"_notes"=>1
);
$cnt=0;

function J_DirAcs($per,$dir="",$top=0)
{
	global $SDR,$EPOCH,$peo,$exc_file,$exc_dir,$role,$cnt;
	$dir="/".trim($dir,"/")."/";
	$o=array();
	$n=array(0,0,"",0);
	if(is_dir($SDR.$dir) && is_readable($SDR.$dir))
	{
		$d=opendir($SDR.$dir);
		while(false!==($i=readdir($d)))
		{
			if($i=="." || $i==".."){}
			else
			{
				if(is_file($SDR.$dir.$i))
				{
					if(strpos($i,".php")!==false && !isset($exc_file[$dir.$i]))
					{
						$c=file_get_contents($SDR.$dir.$i);
						if($c && strpos($c,"\$J_url=")!==false) // an alias
						{
							$c=str_replace("'","",str_replace("\"","",str_replace("= ","=",str_replace(" =","=",str_replace(" ;",";",$c)))));
							preg_match_all('~\$J_url=(.*?);~',$c,$m);
							if(count($m[1]))
							{
								$m=$m[1][0];
								$z_dir=(strpos($m,"/")!==false?substr($m,0,strrpos($m,"/")+1):"");
								$i=(strpos($m,"/")!==false?substr($m,strrpos($m,"/")+1):$m);
								$i=(strpos($i,".php")!==false?substr($i,0,strrpos($i,".php")+4):$i);
								$zn=$i;
								preg_match_all('~\$J_title1=(.*?);~',$c,$m);
								if(count($m[1]))
								{
									$m=$m[1][0];
									$zn=(strpos($m,"/")!==false?substr($m,strrpos($m,"/")+1):$m);
								}
								if(!isset($exc_file[$z_dir.$i]) && file_exists($SDR.$z_dir.$i))
								{
									$c=file_get_contents($SDR.$z_dir.$i);
									if(strpos($c,"system/secure.php")!==false)
									{
										$n[0]++;
										$a="";
										$r=mysql_query("SELECT j_uac_expiry,j_uac_curator FROM nse_user_pages WHERE j_uac_user=".$per." AND j_uac_role='".$role."' AND j_uac_page LIKE '".$z_dir.$i."%' LIMIT 1");
										if(mysql_num_rows($r))
										{
											$g=mysql_fetch_array($r);
											if($g["j_uac_expiry"])
											{
												$a=date("d/m/Y",$g["j_uac_expiry"])."|".$g["j_uac_curator"];
												if(!isset($peo[$g["j_uac_curator"]]))
													$peo[$g["j_uac_curator"]]=J_Value("firstname~lastname","nse_user","user_id",$g["j_uac_curator"]);
												$n[1]++;
											}
										}
										else
											$a="|";
										$o["1".$i."-".$cnt]=$z_dir.$i."|".$a."|".$zn."^";
										$cnt++;
										if(strpos($c,'$SUB_ACCESS')!==false)
										{
											preg_match_all('~\$SUB_ACCESS.*?=.*?array\((.*?)\);~',$c,$m);
											if(count($m[1]))
											{
												$m=explode(",",$m[1][0]);
												foreach($m as $k => $v)
												{
													$v=substr($v,0,strpos($v,"="));
													$v=trim(preg_replace('~[^a-zA-Z0-9 _\-]~',"",$v));
													$is=$i."#".$v;
													$a="";
													$n[0]++;
													$r=mysql_query("SELECT j_uac_expiry,j_uac_curator FROM nse_user_pages WHERE j_uac_user=".$per." AND j_uac_role='".$role."' AND j_uac_page='".$z_dir.$is."' LIMIT 1");
													if(mysql_num_rows($r))
													{
														$g=mysql_fetch_array($r);
														if($g["j_uac_expiry"])
														{
															$a=date("d/m/Y",$g["j_uac_expiry"])."|".$g["j_uac_curator"];
															if(!isset($peo[$g["j_uac_curator"]]))
																$peo[$g["j_uac_curator"]]=J_Value("firstname~lastname","nse_user","user_id",$g["j_uac_curator"]);
															$n[1]++;
														}
													}
													else
														$a="|";
													$o["1".$is."-".$cnt]=$z_dir.$is."|".$a."|".$zn."^";
													$cnt++;
												}
											}
										}
									}
								}
							}
						}
						elseif(strpos($c,"system/secure.php")!==false)
						{
							$n[0]++;
							$a="";
							$r=mysql_query("SELECT j_uac_expiry,j_uac_curator FROM nse_user_pages WHERE j_uac_user=".$per." AND j_uac_role='".$role."' AND j_uac_page LIKE '".$dir.$i."%' LIMIT 1");
							if(mysql_num_rows($r))
							{
								$g=mysql_fetch_array($r);
								if($g["j_uac_expiry"])
								{
									$a=date("d/m/Y",$g["j_uac_expiry"])."|".$g["j_uac_curator"];
									if(!isset($peo[$g["j_uac_curator"]]))
										$peo[$g["j_uac_curator"]]=J_Value("firstname~lastname","nse_user","user_id",$g["j_uac_curator"]);
									$n[1]++;
								}
							}
							$o["1".$i]=$i."|".$a."^";
							if(strpos($c,'$SUB_ACCESS')!==false)
							{
								preg_match_all('~\$SUB_ACCESS.*?=.*?array\((.*?)\);~',$c,$m);
								if(count($m[1]))
								{
									$m=explode(",",$m[1][0]);
									foreach($m as $k => $v)
									{
										$v=substr($v,0,strpos($v,"="));
										$v=trim(preg_replace('~[^a-zA-Z0-9 _\-]~',"",$v));
										$is=$i."#".$v;
										$a="";
										$n[0]++;
										$r=mysql_query("SELECT j_uac_expiry,j_uac_curator FROM nse_user_pages WHERE j_uac_user=".$per." AND j_uac_role='".$role."' AND j_uac_page='".$dir.$is."' LIMIT 1");
										if(mysql_num_rows($r))
										{
											$g=mysql_fetch_array($r);
											if($g["j_uac_expiry"])
											{
												$a=date("d/m/Y",$g["j_uac_expiry"])."|".$g["j_uac_curator"];
												if(!isset($peo[$g["j_uac_curator"]]))
													$peo[$g["j_uac_curator"]]=J_Value("firstname~lastname","nse_user","user_id",$g["j_uac_curator"]);
												$n[1]++;
											}
										}
										$o["1".$is]=$is."|".$a."^";
									}
								}
							}
						}
					}
				}
				elseif(is_dir($SDR.$dir.$i) && (!$top || isset($top[$i])))
				{
					if(isset($exc_dir[$i]) || isset($exc_dir[$dir.$i])) // exclude dir
					{}
					else
					{
						$r=J_DirAcs($per,$dir.$i);
						$n[0]+=$r[0];
						$n[1]+=$r[1];
						$n[3]+=$r[3];
						if($r[0]||$r[3])
						$o["0".$i]="|".$i."|".($r[0]&&$r[0]==$r[1]?2:($r[1]?1:""))."|".($r[3]?$r[3]:"")."^".$r[2]."*^";
					}
				}
			}
		}
		closedir($d);
	}
	$n[2]="";
	if(count($o))
	{
		ksort($o);
		foreach($o as $k => $v)
			$n[2].=$v;
	}
	return $n;
}

$o="";
$peo[$id]=$person["firstname"]." ".$person["lastname"]." ".$person["email"];
$r=J_DirAcs($id,"",array("apps"=>1));
$o.="|".$id."|".($r[0]&&$r[0]==$r[1]?2:($r[1]?1:""))."|".($r[3]?$r[3]:"")."|1^".$r[2]."*^";
$po="";
foreach($peo as $k => $v)
	$po.=$k."~".$v."|";

$s=0;
$r=mysql_query("SELECT j_uac_expiry FROM nse_user_pages WHERE j_uac_user=".$id." AND j_uac_role='".$role."' AND j_uac_page='SuperUser' LIMIT 1");
if(mysql_num_rows($r))
{
	$g=mysql_fetch_array($r);
	$s=date("d/m/Y",$g["j_uac_expiry"]);
}

$m="";
$r=mysql_query("SELECT DISTINCT j_uas_name FROM nse_user_pages_set ORDER BY j_uas_name");
if(mysql_num_rows($r))
{
	while($g=mysql_fetch_array($r))
	$m.=$g["j_uas_name"]."~";
}
$dr="";
foreach($roles as $k => $v)
	$dr.=$k."~".$v."|";

$r="";
$q="SELECT roles FROM nse_user WHERE user_id=".$id;
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$g=mysql_fetch_array($q);
	$g=str_split($g["roles"]);
	foreach($g as $k => $v)
		$r.=$v."~".$roles[$v]."|";
}

if(isset($err))
	echo $err;

echo "<script>J_acc(";
echo $id;
echo ",\"".$m."\"";
echo ",\"".date("d/m/Y",mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")+5))."\"";
echo ",\"".$o."\"";
echo ",\"".$s."\"";
echo ",\"".$po."\"";
echo ",".date("Ymd",$EPOCH);
echo ",'".$r."'";
echo ",'".$role."'";
echo ",'".$dr."'";
if(isset($_GET["j_IF"]))
{
	echo ",\"".$person["firstname"]." ".$person["lastname"]."\"";
	echo ",'".$person["email"]."'";
}
echo ")</script>";
$J_title1="Assign Access";
$J_title3="<b>".$person["firstname"]." ".$person["lastname"]."</b> | ".$person["email"]. " | ".$id;
$J_icon="<img src=".$ROOT."/ico/set/lock.png>";
$J_home=0;
$J_nomax=1;
$J_nostart=1;
$J_label=8;
$J_height=160;
$J_width=280;
$J_join="SysCur";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>