<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";

if(isset($_GET["v"]))
{
	include $SDR."/apps/system/email/_header.php";
	echo $j_email_header;
	echo "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam ultrices vestibulum elit. Mauris congue sapien sed dolor. Pellentesque sem augue, porttitor id, placerat ac, congue ac, eros. Etiam fermentum consectetuer pede.<hr><a href='example: Link'>Example link</a>";
	echo $j_email_footer;
	die();
}

J_SetPerm("system/email_header.php");
$s=file_get_contents($SDR."/apps/system/email/_header.php");

if(isset($_POST["e"]))
{
	function J_p2($v)
	{
		$v=str_replace("\n","<br>",$v);
		$v=str_replace("\r","",$v);
		$v=str_replace("\t","",$v);
		$v=str_replace("\"",'\"',$v);
		return $v;
	}
	$s=preg_replace('~\$j_email_header=".*?";~','$j_email_header="'.J_p2($_POST["h"]).'";',$s);
	$s=preg_replace('~\$j_email_footer=".*?";~','$j_email_footer="'.J_p2($_POST["f"]).'";',$s);
	if($o=fopen($SDR."/apps/system/email/_header.php","r+"))
	{
		$o=fopen($SDR."/apps/system/email/_header.php","w+");
		fwrite($o,$s);
		fclose($o);
	}
	$s=file_get_contents($SDR."/apps/system/email/_header.php");
}

function J_p1($v)
{
	$v=str_replace("<br>","\n",$v);
	return $v;
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<style type=text/css>textarea{font-size:12pt}</style>";
echo "<body onload=j_P.J_resize(j_W,'200,-2,300');j_P.j_Wi[j_W]['F'][3]['F'].src='headers.php?v'>";
echo "<h1>Email Headers</h1>";
echo "<hr>Set up how system emails appear to users. Note that some of these settings do not work in some online mail services. Also note that images are not a good idea. These mails are not intented as content-rich delivery mechanisms - rather for simple communication.<hr>";
echo "<form method=post>";
echo "<table width=100%>";
echo "<tr><td>Header</td><td width=98%><textarea name=h rows=8 class=jW100>".J_p1(J_Extract($s,'\$j_email_header="(.*?)";'))."</textarea></td></tr>";
echo "<tr><td>Footer</td><td><textarea name=f rows=8 class=jW100>".J_p1(J_Extract($s,'\$j_email_footer="(.*?)";'))."</textarea></td></tr>";
echo "<tr><td></td><td><input type=submit name=e value=OK></td></tr>";
echo "</table>";
echo "</form>";
$J_title1="Mail Headers";
$J_framesize=-2;
$J_icon="<img src=".$ROOT."/ico/set/mod.png>";
$J_icon="<img src=".$ROOT."/ico/set/sys.png>";
$J_home=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>