<?php //Connects to database
function connect($dbUser,$dbPass,$dbName,$dbHost) {
   mysql_connect($dbHost,$dbUser,$dbPass);
   mysql_select_db($dbName);
}

//Query the database and put the results in an array
function dbQuery($query) {
   $rsQuery = mysql_query($query);
   if (@@mysql_error()) {
       $queryArr["error"]["isError"] = true;
       $queryArr["error"]["errorMsg"] = @@mysql_error();
       echo @@mysql_error();
     } else {
       $queryArr["error"]["isError"] = false;
       $queryArr["error"]["errorMsg"] = "No Errors";
       if (is_resource($rsQuery)) {
           $queryArr["queryInfo"]["numRows"] = mysql_numrows($rsQuery);
       } else {
           $queryArr["queryInfo"]["numRows"] = 0;
       }
       
       $queryArr["queryInfo"]["insertID"] = false;
       if (mysql_insert_id()) {
           $queryArr["queryInfo"]["insertID"] = mysql_insert_id();    
       }
       if (is_resource($rsQuery)) {
           for($i=0;$i<mysql_num_fields($rsQuery);$i++) {
                 $field = mysql_fetch_field($rsQuery, $i);
                 for($ii=0;$ii<mysql_num_rows($rsQuery);$ii++) {
                   $queryArr[$field->name][$ii] = mysql_result($rsQuery,$ii,$field->name);
                 }
           }
       }
     }
     unset($rsQuery);
     return $queryArr;
}

//Make strings safe to prevent SQL injection
function sfs($string) {
   return mysql_real_escape_string($string);
}

//Make integers save to prevent SQL injection
function nfs($string) {
   return mysql_real_escape_string(intval($string));
}

connect("c1aatravel","fV7hvjMG7wwNuQGU","c1aatravel","localhost");

/*$query = "SELECT DISTINCT
	nse_establishment.establishment_code
	,establishment_name
	,aa_category_name
	,restype_name
	,nse_restype_subcategory_lang.subcategory_name
FROM nse_establishment
	INNER JOIN nse_establishment_awards ON nse_establishment.establishment_code = nse_establishment_awards.establishment_code
	LEFT JOIN nse_aa_category ON nse_establishment.aa_category_code = nse_aa_category.aa_category_code
	LEFT JOIN nse_establishment_restype ON nse_establishment.establishment_code = nse_establishment_restype.establishment_code
	LEFT JOIN nse_restype ON nse_establishment_awards.awards_category = nse_restype.restype_id
	LEFT JOIN nse_restype_subcategory_lang ON nse_establishment_restype.subcategory_id = nse_restype_subcategory_lang.subcategory_id
WHERE nse_establishment_awards.in_awards LIKE 'Y'";*/
//echo $_GET["startDate"];

if (isset($_GET["startDate"]) && isset($_GET["endDate"])) {
	$startDatePart = explode("/",$_GET["startDate"]);
	$endDatePart = explode("/",$_GET["endDate"]);
	$startDate = date("Y-m-d", mktime(0, 0, 0, intval($startDatePart[1]),intval($startDatePart[0]),intval($startDatePart[2])));
	$endDate = date("Y-m-d", mktime(0, 0, 0, intval($endDatePart[1]),intval($endDatePart[0]),intval($endDatePart[2])));
	mkdir("gen_report/".$startDate.$endDate);
	mkdir("gen_report/".$startDate.$endDate."/gen_reports");
	$savePath = "gen_report/".$startDate.$endDate;
} else {
	exit();
}

$query = "SELECT
	nse_establishment.establishment_code
	,establishment_name
	,aa_category_name
	,restype_name
	,nse_restype_subcategory_lang.subcategory_name
FROM nse_establishment
	INNER JOIN nse_establishment_feedback ON nse_establishment_feedback.establishment_code = nse_establishment.establishment_code
	LEFT JOIN nse_establishment_awards ON nse_establishment.establishment_code = nse_establishment_awards.establishment_code
	LEFT JOIN nse_aa_category ON nse_establishment.aa_category_code = nse_aa_category.aa_category_code
	LEFT JOIN nse_establishment_restype ON nse_establishment.establishment_code = nse_establishment_restype.establishment_code
	LEFT JOIN nse_restype ON nse_establishment_awards.awards_category = nse_restype.restype_id
	LEFT JOIN nse_restype_subcategory_lang ON nse_establishment_restype.subcategory_id = nse_restype_subcategory_lang.subcategory_id
WHERE nse_establishment_feedback.feedback_date > '".sfs($startDate)."' AND nse_establishment_feedback.feedback_date < '".sfs($endDate)."'
GROUP BY nse_establishment_feedback.establishment_code";
//echo $query;
//exit();

$rsGetEstablishments = dbQuery($query);
echo "<a href='".$savePath."/report.xls'>Download Report</a><br /><br />";
$awards_report = "<style>
	body {
		padding:0;
		margin:0;
	}
	table {
		border-collapse:collapse;
		border:1px solid black;
		white-space:nowrap;
	}
	th {
		border-collapse:collapse;
		border:1px solid black;
		white-space:nowrap;
		font-size:11px;
		font-family:arial;
	}
	td {
		border-collapse:collapse;
		border:1px solid black;
		white-space:nowrap;
		font-size:11px;
		font-family:arial;
	}
</style>";
$awards_report .= "<div id='xls' title='test.xls'><table>";
	$awards_report .= "<tr>";
		$awards_report .= "<th>Code</th>";
		$awards_report .= "<th>Establishment Name</th>";
		$awards_report .= "<th>Awards Category</th>";
		$awards_report .= "<th>Category</th>";
		$awards_report .= "<th>QA Category</th>";
		$awards_report .= "<th>Contact Name</th>";
		$awards_report .= "<th>Contact Email</th>";
		$awards_report .= "<th>Forms</th>";
		for($ai=1;$ai<26;$ai++) {
			$awards_report .= "<th>".$ai."</th>";
		}
	$awards_report .= "</tr>";
	//echo $awards_report;
	//$awards_report = "";
	for($i=0;$i<$rsGetEstablishments["queryInfo"]["numRows"];$i++) {
		$code = $rsGetEstablishments["establishment_code"][$i];
		
		$query = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS fullname, a.designation, b.phone, b.cell, b.email
				FROM nse_user_establishments AS a
				LEFT JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code LIKE '".sfs($code)."'
				ORDER BY a.`awards_contact` DESC, firstname DESC
				LIMIT 1";
		$rsGetAwardsContact = dbQuery($query);
	
		if ($rsGetEstablishments["establishment_code"][$i] != '') {
		$query = "SELECT * FROM nse_survey_complete WHERE ifkEstabId = '".sfs($rsGetEstablishments["establishment_code"][$i])."' AND dCreated > '".sfs($startDate)."' AND dCreated < '".sfs($endDate)."'";
		$rsGetSurveys = dbQuery($query);
		$formCount = 0;
		for ($ii=0;$ii<$rsGetSurveys["queryInfo"]["numRows"];$ii++) {
			$formCount++;
			$query = "SELECT * FROM nse_survey_answers WHERE ifkCompleteId = ".nfs($rsGetSurveys["ipkCompleteId"][$ii]);
			$rsGetAnswers = dbQuery($query);
			for ($iii=0;$iii<$rsGetAnswers["queryInfo"]["numRows"];$iii++) {
				if(!isset($score[$code][$rsGetAnswers["ifkQuestionId"][$iii]])) {
					$score[$code][$rsGetAnswers["ifkQuestionId"][$iii]] = 0;
				}
				if(!isset($scorecount[$code][$rsGetAnswers["ifkQuestionId"][$iii]])) {
					$scorecount[$code][$rsGetAnswers["ifkQuestionId"][$iii]];
				}
				if ($rsGetAnswers["iValue"][$iii] != "0") {
					$score[$code][$rsGetAnswers["ifkQuestionId"][$iii]] += $rsGetAnswers["iValue"][$iii];
					$scorecount[$code][$rsGetAnswers["ifkQuestionId"][$iii]]++;
				}
				
			}
			
		}
		$awards_report .= "<tr>";
			$awards_report .= "<td>'<a href='http://".$_SERVER["SERVER_NAME"]."/juno/apps/misc_tools/".$savePath."/gen_reports/".$rsGetEstablishments["establishment_code"][$i].".xls'>".$rsGetEstablishments["establishment_code"][$i]."</a></td>";
			//$awards_report .= "<td>'".$rsGetEstablishments["establishment_code"][$i]."</td>";
			$awards_report .= "<td>".$rsGetEstablishments["establishment_name"][$i]."</td>";
			$awards_report .= "<td>".$rsGetEstablishments["restype_name"][$i]."</td>";
			$awards_report .= "<td>".$rsGetEstablishments["aa_category_name"][$i]."</td>";
			$awards_report .= "<td>".$rsGetEstablishments["subcategory_name"][$i]."</td>";
			$awards_report .= "<td>".$rsGetAwardsContact["fullname"][0]."</td>";
			$awards_report .= "<td>".$rsGetAwardsContact["email"][0]."</td>";
			$awards_report .= "<td>".$formCount."</td>";
			for($ai=1;$ai<26;$ai++) {
				if ($scorecount[$code][$ai] == 0) {
					$awards_report .= "<td>0</td>";
				} else {
					$awards_report .= "<td>".round(($score[$code][$ai]/$scorecount[$code][$ai]*20),2)."</td>";
				}
			}
		$awards_report .= "</tr>";
		//echo $awards_report;
		//$awards_report = "";
		//$query = "";
		
		//$awards_report = "";
		}
		
		$resTable = "<style>
			body {
				padding:0;
				margin:0;
			}
			table {
				border-collapse:collapse;
				border:1px solid black;
				white-space:nowrap;
			}
			th {
				border-collapse:collapse;
				border:1px solid black;
				white-space:nowrap;
				font-size:11px;
				font-family:arial;
			}
			td {
				border-collapse:collapse;
				border:1px solid black;
				white-space:nowrap;
				font-size:11px;
				font-family:arial;
			}
		</style><table>";
		for ($ii=0;$ii<$rsGetSurveys["queryInfo"]["numRows"];$ii++) {
			
			$query = "SELECT * FROM nse_survey_answers WHERE ifkCompleteId = ".nfs($rsGetSurveys["ipkCompleteId"][$ii]);
			$rsGetAnswers = dbQuery($query);
			$resTable .= "<tr>";
			$resTable .= "<td>".$rsGetSurveys["sEmail"][$ii]."</td>";
			for ($iii=0;$iii<$rsGetAnswers["queryInfo"]["numRows"];$iii++) {
				$resTable .= "<td>".$rsGetAnswers["iValue"][$iii]."</td>";
			}
			$resTable .= "<td>".$rsGetSurveys["tComments"][$ii]."</td>";
			$resTable .= "</tr>";
		}
		$resTable .= "</table>";
		file_put_contents($savePath."/gen_reports/".$rsGetEstablishments["establishment_code"][$i].".xls",$resTable);
		//exit;
	}
$awards_report .= "</table></div>";
//echo $awards_report;
//$awards_report = "";

//header("content-type:application/xls");
//header('Content-Disposition: attachment; filename="awards_report.xls"');
echo $awards_report;
file_put_contents($savePath."/report.xls",$awards_report);
//echo $resTable;

//echo $awards_report;



?>