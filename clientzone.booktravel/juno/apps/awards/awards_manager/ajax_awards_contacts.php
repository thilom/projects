<?php

//Vars
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
$establishment_code = $_GET['code'];
$contact_list = '';

$statement = "SELECT a.user_id, a.designation
				FROM nse_user_establishments AS a
				WHERE a.establishment_code=?";
$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
$sql_contacts->bind_param('s', $establishment_code);
$sql_contacts->execute();
$sql_contacts->bind_result($user_id, $designation);
while ($sql_contacts->fetch()) {
	$contact_list .= "|||$user_id~$designation";
}
$sql_contacts->close();

?>
