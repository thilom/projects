<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/awards/awards_manager/awards_forms.js></script>";

if(isset($_GET["j_hide"])) {
	unset($_GET["j_hide"]);
	$encoded_name = urlencode($_GET['est']); //URL encode string
	echo "<script>J_start('$encoded_name','{$_GET['year']}')</script>";
	die();
}

//Vars
$template = file_get_contents('awards_forms.html');
$invoices = array();
$invoice_list = '';
$award_year = $_GET['year'];
$establishment_code = $_GET['est'];

//Get establishment code
//$establishment_name = urldecode($_GET['est']);
//$establishment_name = str_replace('_', ' ', $establishment_name);
//if (substr($establishment_name, -2) == '))') {
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//} else {
//	$establishment_name = substr($_GET['est'], 0, strrpos($_GET['est'], '('));
//}

//Get establishment Code
//$statement = "SELECT establishment_code 
//				FROM nse_establishment
//				WHERE establishment_name=?
//				LIMIT 1";
//$sql_name = $GLOBALS['dbCon']->prepare($statement);
//$sql_name->bind_param('s', $establishment_name);
//$sql_name->execute();
//$sql_name->bind_result($establishment_code);
//$sql_name->fetch();
//$sql_name->close();

if (count($_POST) > 0) {
//	echo $_POST['trackers'];
	
	//Expand trackers
	$track_line = explode('|', $_POST['trackers']);
	
	
	foreach ($track_line as $line) {
		if (empty($line)) continue; //Skip empty lines
		
		$track_items = explode('~', $line);
		$invoice_id = substr($track_items[0], 2);
		
		//Does the entry already exist
		$statement = "SELECT count(*) AS count 
						FROM nse_establishment_awards_forms
						WHERE invoice_id=? AND tracking_number=?";
		$sql_check = $GLOBALS['dbCon']->prepare($statement);
		$sql_check->bind_param('is', $invoice_id, $track_items[2]);
		$sql_check->execute();
		$sql_check->bind_result($count);
		$sql_check->fetch();
		$sql_check->close();
		
		if (empty($track_items[1])) {
			$award_date = date("Y/m/d");
		} else {
			list($day, $month, $year) = explode('/', $track_items[1]);
			$award_date = "$year/$month/$day";
		}
		
		if ($count == 0) {
			$statement = "INSERT INTO nse_establishment_awards_forms
								(establishment_code, awards_year, invoice_id, tracking_number, tracking_date, tracking_note)
							VALUES
								(?,?,?,?,?,?)";
			$sql_insert = $GLOBALS['dbCon']->prepare($statement);
			$sql_insert->bind_param('ssisss', $establishment_code, $award_year, $invoice_id, $track_items[2], $award_date, $track_items[3]);
			$sql_insert->execute();
		}
	}
	
	echo "<div style='width: 200px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Tracking Data Saved</div>";
	echo "<div style='width: 200px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='document.location=document.location' ></div>";
	
	
	$invoice_id = '';
	die();
}

//Get forms invoices
$counter = 0;
$statement = "SELECT a.j_invit_invoice, c.j_inv_number, a.j_invit_date, a.j_invit_name, a.j_invit_total, a.j_invit_paid
				FROM nse_invoice_item AS a
				LEFT JOIN nse_inventory AS b ON a.j_invit_inventory= b.j_in_id
				LEFT JOIN nse_invoice AS c ON a.j_invit_invoice=c.j_inv_id
				WHERE b.j_in_category=18 AND a.j_invit_company=?";
$sql_invoice = $GLOBALS['dbCon']->prepare($statement);
$sql_invoice->bind_param('s', $establishment_code);
$sql_invoice->execute();
$sql_invoice->store_result();
$sql_invoice->bind_result($j_invit_invoice, $j_inv_number, $j_invit_date, $j_invit_name, $j_invit_total, $j_invit_paid);
while($sql_invoice->fetch()) {
	$invoices[$counter]['invoice'] = $j_invit_invoice;
	$invoices[$counter]['invoice2'] = $j_inv_number;
	$invoices[$counter]['date'] = date('d F Y', $j_invit_date);
	$invoices[$counter]['name'] = $j_invit_name;
	$invoices[$counter]['total'] = number_format($j_invit_total, 2);
	$invoices[$counter]['paid'] = number_format($j_invit_paid, 2);
	$counter++;
}
$sql_invoice->free_result();
$sql_invoice->close();

//Assemble invoice list
$invoice_id = '';
$total = 0;
$paid = 0;

$statement = "SELECT tracking_number, tracking_note, tracking_date
				FROM nse_establishment_awards_forms
				WHERE invoice_id=? 
				ORDER BY tracking_date";
$sql_tracking = $GLOBALS['dbCon']->prepare($statement);

if (empty($invoices)) {
	echo "<div style='width: 100%; text-align: center; margin-top: 20px'>~No Form Invoices Available~</div>";
} else {
	foreach ($invoices as $invoice_data) {
		if ($invoice_id != $invoice_data['invoice'] && !empty($invoice_id)) {
			$invoice_list .= "<tr><td>Total</td><td style='border-top: 1px solid black; border-bottom: 4px double black'>". number_format($total,2) . "</td><td style='border-top: 1px solid black; border-bottom: 4px double black'>" . number_format($paid,2) . "</td></tr>";
			$invoice_list .= "</table></td><td>";
			$invoice_list .= "<div style='width: 400px; display: inline-block;'>";
			$invoice_list .= "<table width=100% id='in$invoice_id'>";
			$invoice_list .= "<tr><th>Tracking<input type='button' value='Add' name='trackButton' id='in$invoice_id')\"></th></tr>";

			//Add current tracking data
			$sql_tracking->bind_param('s', $invoice_data['invoice']);
			$sql_tracking->execute();
			$sql_tracking->bind_result($tracking_number, $tracking_note, $tracking_date);
			while ($sql_tracking->fetch()) {
				$invoice_list .= "<tr><td>$tracking_date</td><td>$tracking_number</td></tr>";
				$invoice_list .= "<tr><td colspan=2 style='border-bottom: 1px dotted gray'>Note: $tracking_note</td></tr>";
			}
			$sql_tracking->free_result();


			$invoice_list .= "</table></div>";
			$invoice_list .= "</td></tr></table></div>";
			$total = 0;
			$paid = 0;
		}

		if ($invoice_id != $invoice_data['invoice']) {

			$invoice_list .= "<div style='background-color: silver; width: 100%; padding: 5px; margin: 10px'>";
			$invoice_list .= "<table style='width: 100%'><tr><td>Invoice No: ";
			$invoice_list .= str_pad($invoice_data['invoice2'], 8, 0, STR_PAD_LEFT);
			$invoice_list .= "</td><td style='text-align: right'>Invoice Date: ";
			$invoice_list .= $invoice_data['date'];
			$invoice_list .= "</td></tr><tr><td width=70%>";
			$invoice_list .= "<table style='background-color: white; width: 100%; '>";
			$invoice_list .= "<tr><th style='width: 500px' >&nbsp;</th><th >price</th><th >Paid</th></tr>";
		}

		$invoice_list .= "<tr><td>{$invoice_data['name']}</td><td>{$invoice_data['total']}</td><td></td></tr>";
		$total += $invoice_data['total'];
		$paid += $invoice_data['paid'];

		$invoice_id = $invoice_data['invoice'];

	}


	$invoice_list .= "<tr><td>Total</td><td style='border-top: 1px solid black; border-bottom: 4px double black'>" . number_format($total,2) . "</td><td style='border-top: 1px solid black; border-bottom: 4px double black'>" . number_format($paid,2) . "</td></tr>";
	$invoice_list .= "</table></td><td>";
	$invoice_list .= "<div style='width: 400px; display: inline-block;'>";
	$invoice_list .= "<table width=100% id='in$invoice_id'>";
	$invoice_list .= "<tr><th>Tracking<input type='button' value='Add' name='trackButton' id='in$invoice_id' ></th></tr>";
	//Add current tracking data
	$sql_tracking->bind_param('s', $invoice_data['invoice']);
	$sql_tracking->execute();
	$sql_tracking->bind_result($tracking_number, $tracking_note, $tracking_date);
	while ($sql_tracking->fetch()) {
		$invoice_list .= "<tr><td>$tracking_date</td><td>$tracking_number</td></tr>";
		$invoice_list .= "<tr><td colspan=2 style='border-bottom: 1px dotted gray'>Note: $tracking_note</td></tr>";
	}
	$sql_tracking->free_result();
	$invoice_list .= "</table></div>";
	$invoice_list .= "</td></tr></table></div>";
}

//Replace Tags
$template = str_replace('<!-- invoice_list -->', $invoice_list, $template);

echo $template;

?>
