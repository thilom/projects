<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/awards/awards_manager/awards_details.js></script>";

if(isset($_GET["j_hide"])) {
	unset($_GET["j_hide"]);
	$encoded_name = urlencode($_GET['est']); //URL encode string
	echo "<script>J_start('$encoded_name','{$_GET['year']}')</script>";
	die();
}

//Vars
$template = file_get_contents('awards_details.html');
$awards_year = $_GET['year'];
$in_awards = '';
$code_of_conduct_signed = ''; 
$visit_date = ''; 
$awards_category = '';
$category_options = '';
$overnight_coupon = '';
$opted_out = '';
$participation = '';
$current_participation = array();
$market_list = '';
$market = '';
$hall_of_fame = '';
$hall_of_fame_list = "<option value='0'>None</option>";
$establishment_code = $_GET['est'];

//Get establishment code
//$establishment_name = urldecode($_GET['est']);
//$establishment_name = str_replace('_', ' ', $establishment_name);
//if (substr($establishment_name, -2) == '))') {
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//} else {
//	$establishment_name = substr($_GET['est'], 0, strrpos($_GET['est'], '('));
//}


//Get establishment Code
//$statement = "SELECT establishment_code 
//				FROM nse_establishment
//				WHERE establishment_name=?
//				LIMIT 1";
//$sql_name = $GLOBALS['dbCon']->prepare($statement);
//$sql_name->bind_param('s', $establishment_name);
//$sql_name->execute();
//$sql_name->bind_result($establishment_code);
//$sql_name->fetch();
//$sql_name->close();

if (count($_POST) > 0) {
	$in_awards = isset($_POST['in_awards'])?'Y':'';
	$code_of_conduct = isset($_POST['code_of_conduct'])?'Y':'';
	$overnight_coupon = isset($_POST['overnight_coupon'])?'Y':'';
	$opted_out = isset($_POST['opted_out'])?'Y':'';
	$visit_date = $_POST['visit_date']; 
	$awards_category = $_POST['awards_category'];
	$market_id = $_POST['market'];
	$hall_of_fame = $_POST['hall_of_fame'];
	
	//Check if entry exists
	$count = 0;
	$statement = "SELECT COUNT(*) 
					FROM nse_establishment_awards
					WHERE establishment_code=? AND awards_year=?";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bind_param('ss', $establishment_code, $awards_year);
	$sql_count->execute();
	$sql_count->bind_result($count);
	$sql_count->fetch();
	$sql_count->close();

	if ($count > 0) {
		$statement = "UPDATE nse_establishment_awards
						SET in_awards=?, code_of_conduct_signed=?, visit_date=?, awards_category=?, overnight_coupon=?, opted_out=?, market=?, hall_of_fame=?
						WHERE establishment_code=? AND awards_year=?
						LIMIT 1";
		$sql_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_update->bind_param('ssssssssss', $in_awards, $code_of_conduct, $visit_date, $awards_category, $overnight_coupon, $opted_out, $market_id, $hall_of_fame, $establishment_code, $awards_year);
		$sql_update->execute();
		$sql_update->close();
	} else {
		$statement = "INSERT INTO nse_establishment_awards
							(in_awards, code_of_conduct_signed, visit_date, awards_category, establishment_code, awards_year, overnight_coupon, opted_out, market, hall_of_fame)
						VALUES 
							(?,?,?,?,?,?,?,?,?,?)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_insert->bind_param('ssssssssss', $in_awards, $code_of_conduct, $visit_date, $awards_category, $establishment_code, $awards_year, $overnight_coupon, $opted_out, $market_id, $hall_of_fame);
		$sql_insert->execute();
		$sql_insert->close();
	}
	
	//Save Participation preference
	$statement = "DELETE FROM nse_establishment_awards_input
					WHERE establishment_code = ? AND award_year = ?"; //Remove all instances from DB
	$sql_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete->bind_param('ss', $establishment_code, $awards_year);
	$sql_delete->execute();	
	if (isset($_POST['participation'])) {
		$statement = "INSERT INTO nse_establishment_awards_input
							(establishment_code, award_year, input_id)
						VALUES
							(?,?,?)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		foreach ($_POST['participation'] as $input_id) {
			$sql_insert->bind_param('ssi', $establishment_code, $awards_year, $input_id);
			$sql_insert->execute();
		}
		$sql_insert->close();
	}
	
	echo "<div style='width: 200px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Details Saved</div>";
	echo "<div style='width: 200px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='document.location=document.location' ></div>";
	die();
}



//Get awards data
$statement = "SELECT in_awards, code_of_conduct_signed, visit_date, awards_category, overnight_coupon, opted_out, market, hall_of_fame
				FROM nse_establishment_awards
				WHERE establishment_code=? AND awards_year=?
				LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
$sql_data->bind_param('ss', $establishment_code, $awards_year);
$sql_data->execute();
$sql_data->bind_result($in_awards, $code_of_conduct_signed, $visit_date, $awards_category, $overnight_coupon, $opted_out, $market, $hall_of_fame);
$sql_data->fetch();
$sql_data->close();

//Get categories
$statement = "SELECT restype_id, restype_name
				FROM nse_restype
				ORDER BY restype_name";
$sql_restype = $GLOBALS['dbCon']->prepare($statement);
$sql_restype->execute();
$sql_restype->bind_result($restype_id, $restype_name);
while ($sql_restype->fetch()) {
	if ($restype_id == $awards_category) {
		$category_options .= "<option value='$restype_id' selected >$restype_name</option>";
	} else {
		$category_options .= "<option value='$restype_id'>$restype_name</option>";
	}
	
}
$sql_restype->close();

//Hall of fame
for ($i=1; $i<=5; $i++) {
	if ($i == $hall_of_fame) {
		$hall_of_fame_list .= "<option value='$i' selected >Year $i</option>";
	} else {
		$hall_of_fame_list .= "<option value='$i'>Year $i</option>";
	}
}

//Get markets
$statement = "SELECT market_id, market_name
				FROM nse_awards_markets
				ORDER BY market_id";
$sql_markets = $GLOBALS['dbCon']->prepare($statement);
$sql_markets->bind_result($market_id, $market_name);
$sql_markets->execute();
while ($sql_markets->fetch()) {
	if ($market == $market_id) {
		$market_list .= "<option value='$market_id' selected >$market_name</option>";
	} else {
		$market_list .= "<option value='$market_id'>$market_name</option>";
	}
	
}
$sql_markets->close();

//Get last QA visit
$assessment_date = '';
$statement = "SELECT DATE_FORMAT(a.assessment_date, '%e %M %Y') AS assessment_date, a.assessment_status, 
				b.restype_name, a.awards_motivation
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_restype AS b ON a.awards_category=b.restype_id
				WHERE a.establishment_code=? AND a.assessment_status='complete'
				ORDER BY a.assessment_date DESC
				LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->bind_result($assessment_date, $assessment_status, $proposed_awards_category, $awards_motivation);
$sql_assessment->fetch();
$sql_assessment->close();

if (empty($assessment_date)) {
	$last_qa_date = "None"; 
} else {
	switch ($assessment_status) {
		case 'draft':
			$last_qa_date = "$assessment_date (Draft)";
			break;
		case 'waiting':
			$last_qa_date = "$assessment_date (Waiting for approval)";
			break;
		case 'complete':
			$last_qa_date = "$assessment_date (Complete)";
			break;
		default:
			$last_qa_date = "$assessment_date";
	}
}

//Current Participation prefecences
$statement = "SELECT input_id 
				FROM nse_establishment_awards_input
				WHERE establishment_code=? AND award_year=?";
$sql_current_input = $GLOBALS['dbCon']->prepare($statement);
$sql_current_input->bind_param('ss', $establishment_code, $awards_year);
$sql_current_input->execute();
$sql_current_input->bind_result($current_input);
while ($sql_current_input->fetch()) {
	$current_participation[] = $current_input;
}
$sql_current_input->close();

//Participation preference
$statement = "SELECT input_id, input_name 
				FROM nse_awards_input_methods
				ORDER BY input_id";
$sql_methods = $GLOBALS['dbCon']->prepare($statement);
$sql_methods->execute();
$sql_methods->bind_result($input_id, $input_name);
while ($sql_methods->fetch()) {
	if (in_array($input_id, $current_participation)) {
		$participation .= "<input type='checkbox' value='$input_id' name='participation[]' checked >$input_name<br>";
	} else {
		$participation .= "<input type='checkbox' value='$input_id' name='participation[]' >$input_name<br>";
	}
	
}
$sql_methods->close();

//Checkboxes
$in_awards = $in_awards=='Y'?'checked':'';
$code_of_conduct_signed = $code_of_conduct_signed=='Y'?'checked':'';
$overnight_coupon = $overnight_coupon=='Y'?'checked':'';
$opted_out = $opted_out=='Y'?'checked':'';

//Replace Tags
$template = str_replace('<!-- awards_year -->', $awards_year, $template);	
$template = str_replace('<!-- in_awards -->', $in_awards, $template);	
$template = str_replace('<!-- code_of_conduct -->', $code_of_conduct_signed, $template);	
$template = str_replace('<!-- visit_date -->', $visit_date, $template);	
$template = str_replace('<!-- category_options -->', $category_options, $template);	
$template = str_replace('<!-- last_qa_date -->', $last_qa_date, $template);	
$template = str_replace('<!-- overnight_coupon -->', $overnight_coupon, $template);	
$template = str_replace('<!-- opted_out -->', $opted_out, $template);	
$template = str_replace('<!-- participation -->', $participation, $template);	
$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);	
$template = str_replace('<!-- market_list -->', $market_list, $template);	
$template = str_replace('<!-- hall_of_fame -->', $hall_of_fame_list, $template);	
$template = str_replace('<!-- proposed_awards_category -->', $proposed_awards_category, $template);	
$template = str_replace('<!-- awards_motivation -->', $awards_motivation, $template);	
	
//	include $SDR."/system/deploy.php";
	
echo $template;
?>
