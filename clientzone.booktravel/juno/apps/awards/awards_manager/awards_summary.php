<?php
/** 
 * Establishment overview panel
 *
 * @version $Id$
 * @author Thilo Muller(2011)
 * 
 */


require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

include $SDR."/system/get.php";

//Vars
$room_type = '';
$room_count = '';
$establishment_code = $_GET['est'];

//Get establishment code
//$establishment_name = urldecode($_GET['est']);
//$establishment_name = str_replace('_', ' ', $establishment_name);
//if (substr($establishment_name, -2) == '))') {
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//} else {
//	$establishment_name = substr($_GET['est'], 0, strrpos($_GET['est'], '('));
//}


//Get Establishment Data
$statement = "SELECT a.establishment_name, a.website_url, b.aa_category_name, a.assessor_id , a.rep_id1, 
						 a.active, DATE_FORMAT(a.enter_date,'%d %M %Y'), DATE_FORMAT(a.creation_date,'%d %M %Y')
                        FROM nse_establishment AS a
						LEFT JOIN nse_aa_category AS b ON a.aa_category_code=b.aa_category_code
                        WHERE establishment_code=? LIMIT 1";
$sql_estab = $GLOBALS['dbCon']->prepare($statement);
$sql_estab->bind_param('s', $establishment_code);
$sql_estab->execute();
$sql_estab->bind_result($establishment_name, $website_url, $aa_endorsement, $assessor_id, $rep_id1, $active, $entered_date, $creation_date);
$sql_estab->fetch();
$sql_estab->free_result();
$sql_estab->close();

//Get location data
$country=0;
$country_name="";
$province=0;
$province_name="";
$town=0;
$town_name="";
$suburb=0;
$suburb_name="";
$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
$sql_location = $GLOBALS['dbCon']->prepare($statement);
$sql_location->bind_param('s', $establishment_code);
$sql_location->execute();
$sql_location->bind_result($country_id, $province_id, $town_id, $suburb_id);
$sql_location->fetch();
$sql_location->free_result();
$sql_location->close();
if (!empty($country_id)) {
    $country=$country_id;
    $country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
}
if (!empty($province_id)) {
    $province=$province_id;
    $province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
}
if(!empty($town_id)) {
    $town=$town_id;
    $town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
}
if(!empty($suburb_id)) {
    $suburb=$suburb_id;
    $suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
}


//HTML start
echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/awards/awards_manager/awards_summary.js></script>";

//Get QA Out date
$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=?";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);
$sql_qa_out->bind_param('s', $establishment_code);
$sql_qa_out->execute();
$sql_qa_out->bind_result($qa_out);
$sql_qa_out->fetch();
$sql_qa_out->close();
if (!isset($qa_out) || $qa_out == '0000-00-00') $qa_out = '-';

//Assemble locations
$location_list = '';
$location_list .= "$country_name";
if (!empty($province_name)) $location_list .= " &#9658; $province_name";
$location_list .= " &#9658; $town_name";
if (isset($suburb_name) && !empty($suburb_name)) $location_list .= " &#9658; $suburb_name";

$regions_list = '';
$statement = "SELECT region_name FROM nse_nlocations_cregion_locations
INNER JOIN nse_nlocations_cregions ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
AND parent_type LIKE 'province'
WHERE location_id=? AND location_type LIKE 'town'";
$sql_regions = $GLOBALS['dbCon']->prepare($statement);
$sql_regions->bind_param('i', $town_id);
$sql_regions->execute();
$sql_regions->store_result();
$sql_regions->bind_result($region_name);
while ($sql_regions->fetch()) {
	$regions_list .= "$region_name<br>";
}
$sql_regions->close();


//Get assessment data
$statement = "SELECT DATE_FORMAT(assessment_date, '%d %M %Y'), assessment_status 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='fa' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->store_result();
$sql_assessment->bind_result($assessment_date, $assessment_status);
if ($sql_assessment->num_rows > 0) {
	$sql_assessment->fetch();
	if (isset($assessment_date) && ($assessment_date == '0000-00-00') || empty($assessment_date)) $assessment_status = '-';
	switch ($assessment_status) {
		case 'waiting':
			$status = "Waiting for approval";
			break;
		case 'draft':
		case 'returned':
			$status = empty($assessment_date)?'':'In Draft';
			break;
		case 'complete':
			$status = 'Complete';
			break;
		default:
			$status = 'Unknown';
	}

	$assessment_data = "$assessment_date ($status)";
} else {
	$assessment_data = 'None';
}

$statement = "SELECT DATE_FORMAT(assessment_date, '%d %M %Y'), assessment_status 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='cc' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->store_result();
$sql_assessment->bind_result($assessment_date_cc, $assessment_status_cc);
if ($sql_assessment->num_rows > 0) {
	$sql_assessment->fetch();
	if (isset($assessment_date) && ($assessment_date == '0000-00-00') || empty($assessment_date)) $assessment_status = '-';
	switch ($assessment_status) {
		case 'waiting':
			$status_cc = "Waiting for approval";
			break;
		case 'draft':
		case 'returned':
			$status_cc = empty($assessment_date)?'':'In Draft';
			break;
		case 'complete':
			$status_cc = 'Complete';
			break;
		default:
			$status_cc = 'Unknown';
	}

	$assessment_data_cc = "$assessment_date_cc ($status_cc)";
} else {
	$assessment_data_cc = '-';
}


//Creation Date
if (empty($creation_date)) $creation_date = 'Unknown';

//Last visit date - fa
if (!empty($assessment_date)) {
	$vDate = '';
	$statement = "SELECT QVISITDATE FROM nse_establishment_dbase WHERE establishment_code=?";
	$sql_visit = $GLOBALS['dbCon']->prepare($statement);
	$sql_visit->bind_param('s', $establishment_code);
	$sql_visit->execute();
	$sql_visit->bind_result($vDate);
	$sql_visit->fetch();
	$sql_visit->close();
	if (!empty($vDate) && strpos($vDate, '/') !== FALSE) {
		list($month,$year) = explode('/', $vDate);
		$last_visit = "01-$month-" . ($year<40?"20":"19") . $year;
		$last_visit = strtotime($last_visit);
		$last_visit = date("F Y", $last_visit);
		$last_visit .= " (from DBase)";
	} else {
		$last_visit = 'Unknown';
	}
} else {
	$last_visit = $assessment_date;
}

//Next renewal date - fa
$renewal_fa_date = '';
$statement = "SELECT DATE_FORMAT(renewal_date, '%d %M %Y') 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='fa' AND assessment_status='complete' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_fa_renewal = $GLOBALS['dbCon']->prepare($statement);
$sql_fa_renewal->bind_param('s', $establishment_code);
$sql_fa_renewal->execute();
$sql_fa_renewal->bind_result($renewal_fa_date);
$sql_fa_renewal->fetch();
$sql_fa_renewal->close();

//Next renewal date - Caravan & Camping
$renewal_cc_date = '-';
$statement = "SELECT DATE_FORMAT(renewal_date, '%d %M %Y') 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='cc' AND assessment_status='complete' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_fa_renewal = $GLOBALS['dbCon']->prepare($statement);
$sql_fa_renewal->bind_param('s', $establishment_code);
$sql_fa_renewal->execute();
$sql_fa_renewal->bind_result($renewal_cc_date);
$sql_fa_renewal->fetch();
$sql_fa_renewal->close();


//Get assessor Name
$assessor_name = '';
if (!empty($assessor_id)) {
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) 
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('i', $assessor_id);
	$sql_assessor->execute();
	$sql_assessor->bind_result($assessor_name);
	$sql_assessor->fetch();
	$sql_assessor->close();
}

//Get rep1 Name
$rep_name1 = '';
if (!empty($rep_id1)) {
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) 
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_rep1 = $GLOBALS['dbCon']->prepare($statement);
	$sql_rep1->bind_param('i', $rep_id1);
	$sql_rep1->execute();
	$sql_rep1->bind_result($rep_name1);
	$sql_rep1->fetch();
	$sql_rep1->close();
}

//Get rep2 Name
$rep_name2 = '';
if (!empty($rep_id2)) {
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) 
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_rep2 = $GLOBALS['dbCon']->prepare($statement);
	$sql_rep2->bind_param('i', $rep_id2);
	$sql_rep2->execute();
	$sql_rep2->bind_result($rep_name2);
	$sql_rep2->fetch();
	$sql_rep2->close();
	$rep_name2 = " ,$rep_name2";
}

//Latest award
$result = '';
$statement = "SELECT d.result_name, a.year, b.category_description, c.restype_name
				FROM award AS a
				LEFT JOIN nse_award_category AS b ON a.category=b.category_code
				LEFT JOIN nse_restype AS c ON a.category=c.restype_id
				LEFT JOIN nse_award_result AS d ON a.status=d.result_code
				WHERE a.code=?
				ORDER BY a.year DESC
				LIMIT 1";
$sql_award = $GLOBALS['dbCon']->prepare($statement);
$sql_award->bind_param('s', $establishment_code);
$sql_award->execute();
$sql_award->bind_result($result, $year, $old_category, $new_category);
$sql_award->fetch();
$sql_award->close();
if (empty($result)) {
	$award = 'none';
} else {
	$award = "$result ($year) - ";
	$award .= empty($old_category)?$new_category:$old_category;
}

//QA Category
$qa_category = '';
$statement = "SELECT b.subcategory_name, d.category_name, f.toplevel_name
				FROM nse_establishment_restype AS a
				LEFT JOIN nse_restype_subcategory_lang AS b ON a.subcategory_id=b.subcategory_id
				LEFT JOIN nse_restype_subcategory_parent AS c ON b.subcategory_id=c.subcategory_id
				LEFT JOIN nse_restype_category_lang AS d ON c.category_id=d.category_id
				LEFT JOIN nse_restype_category AS e ON d.category_id=e.category_id
				LEFT JOIN nse_restype_toplevel AS f ON e.toplevel_id=f.toplevel_id
				WHERE a.establishment_code = ?
				LIMIT 1";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bind_param('s', $establishment_code);
$sql_category->execute();
$sql_category->bind_result($subcategory, $category, $top);
$sql_category->fetch();
$sql_category->close();
$qa_category = "$top &#9658; $subcategory";

//Get Room count
$statement = "SELECT room_count, room_type
				FROM nse_establishment_data
				WHERE establishment_code=?
				LIMIT 1";
$sql_room_count = $GLOBALS['dbCon']->prepare($statement);
$sql_room_count->bind_param('s', $establishment_code);
$sql_room_count->execute();
$sql_room_count->bind_result($room_count, $room_type);
$sql_room_count->fetch();
$sql_room_count->close();
if (empty($room_type)) $room_type = 'Rooms';

//Top Block
echo "<tr><th>Establishment Name</th><td>$establishment_name</td><th style='width: 150px'>Assessor</th><td>$assessor_name</td></tr>";
echo "<tr><th>Establishment Code</th><td>$establishment_code</td><th>Rep</th><td>$rep_name1 $rep_name2</td></tr>";
echo "<tr><th>System Enter Date</th><td colspan=4>$creation_date</td></tr>";
echo "<tr><th>Latest Award</th><td colspan=4>$award</td></tr>";
echo "<tr><td colspan=4>&nbsp;</td></tr>";
echo "<tr><td colspan=5>Fixed Accommodation</td></tr>";
echo "<tr><th>QA Out</th><td>$qa_out</td><th>Endorsement</th><td>$aa_endorsement</td></tr>";
echo "<tr><th>Last Assessment</th><td >$assessment_data</td><th>Next Assessment</th><td>$renewal_fa_date</td></tr>";
echo "<tr><th>QA Enter Date</th><td colspan=4>$entered_date</td></tr>";
echo "<tr><th>QA Category</th><td>$qa_category</td><th>Room Count</th><td>$room_count $room_type</td></tr>";
echo "<tr><td colspan=6>&nbsp;</td></tr>";

echo "<tr><td colspan=6>Caravan & Camping</td></tr>";
echo "<tr><th>Last Assessment</th><td>$assessment_data_cc</td><th>Next Assessment</th><td>$renewal_cc_date</td></tr>";

echo "<tr><td colspan=6>&nbsp;</td></tr>";
echo "<tr><th>Location</th><td colspan=4>$location_list</td></tr>";
echo "<tr valign=top><th>Regions</th><td colspan=4>$regions_list</td></tr>";

$est_name = str_replace(' ', '_', $_GET['est']);
$est_name = urlencode($est_name);
echo "<script>J_awards('$est_name','{$_GET['year']}','$establishment_code')</script>";

$encoded_name = urlencode($establishment_name);
echo "<script>E_d('$encoded_name','$establishment_code')</script>";

//if(!isset($_GET["view"]) && (isset($j_IF) || isset($_GET["j_IF"])))
//{}
//else
//{
	$J_title2="Awards Manager";
	$J_title1=str_replace("\"","",$establishment_name);
	$J_title3="Establishment Code: ".$establishment_code;
	$J_width=1000;
	$J_height=640;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
//}

echo "</body></html>";

?>
