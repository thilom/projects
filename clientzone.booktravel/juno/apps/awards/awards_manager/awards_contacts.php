<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

//Vars
//$template = file_get_contents('contacts_v2.html');
$user_table = '';
$establishment_code = $_GET['est'];

//Get establishment code
//$establishment_name = urldecode($_GET['est']);
//$establishment_name = str_replace('_', ' ', $establishment_name);
//if (substr($establishment_name, -2) == '))') {
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//	$establishment_name = substr($establishment_name, 0, strrpos($establishment_name, '('));
//} else {
//	$establishment_name = substr($_GET['est'], 0, strrpos($_GET['est'], '('));
//}

//Get establishment Code
//$statement = "SELECT establishment_code
//				FROM nse_establishment
//				WHERE establishment_name=?
//				LIMIT 1";
//$sql_name = $GLOBALS['dbCon']->prepare($statement);
//$sql_name->bind_param('s', $establishment_name);
//$sql_name->execute();
//$sql_name->bind_result($establishment_code);
//$sql_name->fetch();
//$sql_name->close();

//Reset reload option
if (isset($_SESSION['reload'])) {
	unset($_SESSION['reload']);
}

if(isset($_GET["j_hide"])) {
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/contacts_v2.js></script>";
	unset($_GET["j_hide"]);
	echo "<script>J_start('".$establishment_code."'".(isset($_GET["view"])?",1":"").")</script>";
	die();
}

//Prepare statement - User Access
$statement = "SELECT COUNT(*) 
				FROM nse_user_pages 
				WHERE j_uac_user=? AND j_uac_role='c'";
$sql_access = $GLOBALS['dbCon']->prepare($statement);

//Get list of contacts
$statement = "SELECT a.user_id, a.designation, a.awards_contact, a.ads_contact, a.accounts_contact, 
						a.qa_contact, a.materials_contact, a.proofing_contact, CONCAT(b.firstname, ' ', b.lastname),
						b.phone, b.cell, b.email
				FROM nse_user_establishments AS a
				LEFT JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code=?";
$sql_users = $GLOBALS['dbCon']->prepare($statement);
$sql_users->bind_param('s', $establishment_code);
$sql_users->execute();
$sql_users->store_result();
$sql_users->bind_result($user_id, $designation, $awards_contact, $ads_contact, $accounts_contact, 
						$qa_contact, $materials_contact, $proofing_contact, $fullname,
						$phone, $cell, $email);
while ($sql_users->fetch()) {
	if (empty($fullname)) continue;
	
	//Get access
	$access = '';
	$sql_access->bind_param('s', $user_id);
	$sql_access->execute();
	$sql_access->bind_result($access);
	$sql_access->fetch();
	$sql_access->free_result();

	$user_table .= "<tr>";
	
	if ($access < 1) {
		$user_table .= "<td>&nbsp;</td>";
	} else {
		$user_table .= "<td><img src='/juno/ico/set/key.png'></td>";
	}
	$user_table .= "<td>";
	
	if ($awards_contact == 'Y') $user_table .= "<img src='/juno/ico/set/emerald_16.png'>";
	if ($ads_contact == 'Y') $user_table .= "<img src='/juno/ico/set/slide.png'>";
	if ($qa_contact == 'Y') $user_table .= "<img src='/juno/ico/set/medal_gold_1.png'>";
	if ($accounts_contact == 'Y') $user_table .= "<img src='/juno/ico/set/money_dollar.png'>";
	if ($materials_contact == 'Y') $user_table .= "<img src='/juno/ico/set/script.png'>";
	if ($proofing_contact == 'Y') $user_table .= "<img src='/juno/ico/set/text_signature.png'>";
	
	$user_table .= "</td>
						<td>$fullname</td>
						<td>$designation</td>
						<td>$phone</td>
						<td>$cell</td>
						<td>$email</td>";
	
	$user_table .= "<td style='text-align: right'>";
	$user_table .= "<input type='button' value='' 
						style='background-image: URL(/juno/ico/set/cross.png); background-repeat: no-repeat; width: 20px; height: 20px' 
						onClick='removeContact(\"$establishment_code\",$user_id)' title='Remove Contact from this establishment'>";
	$user_table .= "<input type='button' value='' 
						style='background-image: URL(/juno/ico/set/pencil.png); background-repeat: no-repeat; width: 20px; height: 20px' 
							onClick='editContact(\"$establishment_code\",$user_id)' title='Edit Contact' >";
	$user_table .= "<input type='button' value='' 
						style='background-image: URL(/juno/ico/set/email_go.png); background-repeat: no-repeat; width: 20px; height: 20px' 
							onclick=\"J_W(ROOT+'/apps/people/send_login.php?id=$user_id')\" title='Send Login Details'";
	
	$user_table .= "</td>
					</tr>";
}
$sql_users->close();

//Replace Tags
$template = str_replace('<!-- user_table -->', $user_table, $template);
$template = str_replace('<!-- establishment_code -->', $establishment_code, $template);

echo $template;
?>