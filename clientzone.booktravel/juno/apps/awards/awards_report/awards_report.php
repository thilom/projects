<?php

if (!isset($_GET['s'])) {
	die();
}
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$d="";
$awards_year = $_GET['year'];



// for display
$dd=explode("~",trim($_GET["d"],","));
$ddd=array();
$s=array();
foreach($dd as $k => $v) {
    if (empty($v)) continue;
	$s[$v]=1;
	$ddd[$v]=1;
}

$t=array("X","code","name","assessor","rep","qa","qa_cat","next_ass","in_awards","opted_out","code_of_conduct","overnight_coupon","awards_visit","awards_category","hall_of_fame","participation_preference","predominant_market","contact_name","designation","contact_tel","contact_cell","contact_email","country","province","town","suburb");
$da="";
$n=1;
foreach($t as $k => $v) {
	if(isset($ddd[$v]))	{
		$da.=$n;
		$n++;
	}
	$da.=",";
}


//Vars
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$r = '';
$establishments = array();

/* Prepared statements - start */

//Users
$statement = "SELECT CONCAT(firstname, ' ', lastname)
				FROM nse_user
				WHERE user_id=?
				LIMIT 1";
$sql_user = $GLOBALS['dbCon']->prepare($statement);

//Current QA
$statement = "SELECT b.aa_category_name, c.subcategory_name
				FROM nse_establishment AS a
				LEFT JOIN nse_aa_category AS b ON a.aa_category_code = b.aa_category_code
				LEFT JOIN nse_establishment_restype AS d ON a.establishment_code=d.establishment_code
				LEFT JOIN nse_restype_subcategory_lang AS c ON d.subcategory_id=c.subcategory_id
				WHERE a.establishment_code=?
				LIMIT 1";
$sql_qa = $GLOBALS['dbCon']->prepare($statement);

//Awards Data
$statement = "SELECT a.in_awards, a.opted_out, a.code_of_conduct_signed, a.overnight_coupon, 
						a.visit_date, a.hall_of_fame, b.market_name, c.restype_name
				FROM nse_establishment_awards AS a
				LEFT JOIN nse_awards_markets AS b ON a.market=b.market_id
				LEFT JOIN nse_restype AS c ON a.awards_category=c.restype_id
				WHERE a.establishment_code=? AND a.awards_year=?
				LIMIT 1";
$sql_awards = $GLOBALS['dbCon']->prepare($statement);


//Participation preferences
$statement = "SELECT b.input_name 
				FROM nse_establishment_awards_input AS a
				LEFT JOIN nse_awards_input_methods AS b ON a.input_id=b.input_id
				WHERE a.establishment_code=? AND award_year=?";
$sql_inputs = $GLOBALS['dbCon']->prepare($statement);

//Location
$statement = "SELECT b.country_name, c.province_name, d.town_name, e.suburb_name
				FROM nse_establishment_location AS a
				LEFT JOIN nse_nlocations_countries AS b ON a.country_id=b.country_id
				LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
				LEFT JOIN nse_nlocations_towns AS d ON a.town_id=d.town_id
				LEFT JOIN nse_nlocations_suburbs AS e ON a.suburb_id=e.suburb_id
				WHERE a.establishment_code=?
				LIMIT 1";
$sql_location = $GLOBALS['dbCon']->prepare($statement);

//Contact Details
$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.phone, b.cell, b.email
				FROM nse_user_establishments AS a
				LEFT JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code=?				
				ORDER BY a.`awards_contact` DESC, firstname DESC
				LIMIT 1";
$sql_contacts = $GLOBALS['dbCon']->prepare($statement);

//Next assessment
$statement = "SELECT renewal_date
				FROM nse_establishment_assessment
				WHERE establishment_code=? AND assessment_status='complete'
				ORDER BY assessment_date DESC
				LIMIT 1";
$sql_renewal = $GLOBALS['dbCon']->prepare($statement);

//QA Out
$statement = "SELECT cancelled_date
				FROM nse_establishment_qa_cancelled
				WHERE establishment_code = ?
				LIMIT 1";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);

//QA Limit - Current QA
$statement = "SELECT assessment_date
				FROM nse_establishment_assessment
				WHERE establishment_code=?
				ORDER BY assessment_date DESC
				LIMIT 1";
$sql_current_qa = $GLOBALS['dbCon']->prepare($statement);

/* Prepared statements - end */

//Get initial establishment list
$statement = "SELECT a.establishment_code, a.establishment_name, a.assessor_id, a.rep_id1
				FROM nse_establishment AS a
				LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code";
if (isset($_GET['t']) && !empty($_GET['t'])) {
	$estab = substr($_GET['t'], 0, strrpos($_GET['t'], '('));
	$estab = trim($estab);
	$statement .= " WHERE (a.establishment_code LIKE '%{$estab}%' OR a.establishment_name LIKE '%{$estab}%')";
}
if (!empty($_GET['country'])) {
	$statement .= strpos($statement, 'WHERE') > 0?" AND":" WHERE";
	$statement .= " b.country_id={$_GET['country']}";
}
if (!empty($_GET['province'])) {
	$statement .= strpos($statement, 'WHERE') > 0?" AND":" WHERE";
	$statement .= " b.province_id={$_GET['province']}";
}
if (!empty($_GET['town'])) {
	$statement .= strpos($statement, 'WHERE') > 0?" AND":" WHERE";
	$statement .= " b.town_id={$_GET['town']}";
}
if (!empty($_GET['suburb'])) {
	$statement .= strpos($statement, 'WHERE') > 0?" AND":" WHERE";
	$statement .= " b.suburb_id={$_GET['suburb']}";
}
$statement .= " ORDER BY establishment_name";
$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
$sql_establishments->execute();
$sql_establishments->bind_result($establishment_code, $establishment_name, $assessor_id, $rep_id);
while ($sql_establishments->fetch()) {
	$establishments[$establishment_code]['name'] = $establishment_name;
	$establishments[$establishment_code]['assessor'] = $assessor_id;
	$establishments[$establishment_code]['rep'] = $rep_id;
}
$sql_establishments->close();

foreach ($establishments as $establishment_code=>$establishment_data) {
	
	//Vars
	$qa_out = '';
	$assessment_date = '';
	$in_awards = '';
	
	if (isset($_GET['qa']) && $_GET['qa'] == 1) {
		//QA Out
		$sql_qa_out->bind_param('s', $establishment_code);
		$sql_qa_out->execute();
		$sql_qa_out->bind_result($qa_out);
		$sql_qa_out->fetch();
		$sql_qa_out->free_result();
		
		if (!empty($qa_out)) {
			unset($establishments[$establishment_code]);
			continue;
		}
	}
	
	if (isset($_GET['qa']) && ($_GET['qa'] == 1 || $_GET['qa'] == 2)) {
		//Current QA
		$sql_current_qa->bind_param('s', $establishment_code);
		$sql_current_qa->execute();
		$sql_current_qa->bind_result($assessment_date);
		$sql_current_qa->fetch();
		$sql_current_qa->free_result();
		if (empty($assessment_date)) {
			unset($establishments[$establishment_code]);
			continue;
		}
	} 
	
	if (isset($_GET['awards']) && $_GET['awards'] != 1) {
		//in/out awards
		$sql_awards->bind_param('ss', $establishment_code, $awards_year);
		$sql_awards->execute();
		$sql_awards->bind_result($in_awards, $opted_out, $code_of_conduct, $overnight_coupon, $awards_visit, $hall_of_fame, $predominant_market, $awards_category);
		$sql_awards->fetch();
		$sql_awards->free_result();
	
		if ($_GET['awards'] == 2) {
			if ($in_awards != 'Y') {
				unset($establishments[$establishment_code]);
				continue;
			}
		}
		
		if ($_GET['awards'] == 3) {
			if ($in_awards == 'Y') {
				unset($establishments[$establishment_code]);
				continue;
			}
		}
		
		if ($_GET['awards'] == 4) {
			if (!empty($in_awards) || $opted_out == 'Y') {
				unset($establishments[$establishment_code]);
				continue;
			}
		}
	}
	
}


$establishments_a = array();
$line_counter = -1;
$file_counter = 1;
$files = '';
foreach ($establishments as $establishment_code=>$establishment_data) {

	$assessor_name = '';
	$rep_name = '';
	$aa_category = '';
	$restype = '';
	$in_awards = '';
	$opted_out = '';
	$code_of_conduct = '';
	$overnight_coupon = '';
	$awards_visit = '';
	$hall_of_fame = '';
	$predominant_market = '';
	$awards_category = '';
	$input_method = '';
	$country = '';
	$province = '';
	$town = '';
	$suburb = '';
	$contact_name = '';
	$designation = '';
	$contact_tel = '';
	$contact_cell = '';
	$contact_email = '';
	$next_ass = '';
	
	
	//Get Assessor Name
	$sql_user->bind_param('i', $establishments[$establishment_code]['assessor']);
	$sql_user->execute();
	$sql_user->bind_result($assessor_name);
	$sql_user->fetch();
	$sql_user->free_result();
	$establishments[$establishment_code]['assessor'] = $assessor_name;
	
	//Get Rep Name
	$sql_user->bind_param('i', $establishments[$establishment_code]['rep']);
	$sql_user->execute();
	$sql_user->bind_result($rep_name);
	$sql_user->fetch();
	$sql_user->free_result();
	$establishments[$establishment_code]['rep'] = $rep_name;
	
	//QA stuff
	$sql_qa->bind_param('s', $establishment_code);
	$sql_qa->execute();
	$sql_qa->bind_result($aa_category, $restype);
	$sql_qa->fetch();
	$sql_qa->free_result();
	$establishments[$establishment_code]['qa'] = $aa_category;
	$establishments[$establishment_code]['qa_cat'] = $restype;
	
	//Awards Stuff
	$sql_awards->bind_param('ss', $establishment_code, $awards_year);
	$sql_awards->execute();
	$sql_awards->bind_result($in_awards, $opted_out, $code_of_conduct, $overnight_coupon, $awards_visit, $hall_of_fame, $predominant_market, $awards_category);
	$sql_awards->fetch();
	$sql_awards->free_result();
	$establishments[$establishment_code]['in_awards'] = $in_awards;
	$establishments[$establishment_code]['opted_out'] = $opted_out;
	$establishments[$establishment_code]['code_of_conduct'] = $code_of_conduct;
	$establishments[$establishment_code]['overnight_coupon'] = $overnight_coupon;
	$establishments[$establishment_code]['awards_visit'] = $awards_visit;
	$establishments[$establishment_code]['hall_of_fame'] = $hall_of_fame;
	$establishments[$establishment_code]['predominant_market'] = $predominant_market;
	$establishments[$establishment_code]['awards_category'] = $awards_category;
	
	//Input methods
	$establishments[$establishment_code]['participation_preference'] = '';
	$sql_inputs->bind_param('ss', $establishment_code, $awards_year);
	$sql_inputs->execute();
	$sql_inputs->bind_result($input_method);
	while ($sql_inputs->fetch()) {
		$establishments[$establishment_code]['participation_preference'] .= " $input_method,";
	}
	$sql_inputs->free_result();
	if (!empty($establishments[$establishment_code]['participation_preference'])) {
		$establishments[$establishment_code]['participation_preference'] = substr($establishments[$establishment_code]['participation_preference'], 0, -1);
	}

	//Location
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->bind_result($country, $province, $town, $suburb);
	$sql_location->fetch();
	$sql_location->free_result();
	$establishments[$establishment_code]['country'] = $country;
	$establishments[$establishment_code]['province'] = $province;
	$establishments[$establishment_code]['town'] = $town;
	$establishments[$establishment_code]['suburb'] = $suburb;
	
	//Contacts
	$sql_contacts->bind_param('s', $establishment_code);
	$sql_contacts->execute();
	$sql_contacts->bind_result($contact_name, $designation, $contact_tel, $contact_cell, $contact_email);
	$sql_contacts->fetch();
	$sql_contacts->free_result();
	$establishments[$establishment_code]['contact_name'] = $contact_name;
	$establishments[$establishment_code]['designation'] = $designation;
	$establishments[$establishment_code]['contact_tel'] = $contact_tel;
	$establishments[$establishment_code]['contact_cell'] = $contact_cell;
	$establishments[$establishment_code]['contact_email'] = $contact_email;
	
	//Next assessment
	$sql_renewal->bind_param('s', $establishment_code);
	$sql_renewal->execute();
	$sql_renewal->bind_result($next_ass);
	$sql_renewal->fetch();
	$sql_renewal->free_result();
	$establishments[$establishment_code]['next_ass'] = $next_ass;
	
	
}

if ($jL2 == 0) {
    $page = "<body style='color:#999;padding:30 40 40 160;background:url(/juno/ico/set/spreadsheet.png) no-repeat 40px 40px'><tt style=width:260><hr>CSV Files Created. Right click to save.<ul>$files</ul></tt></body></html>";
    echo $page;
}

//Concat establishments
foreach ($establishments as $establishment_code => $data) {
	$r .= "$establishment_code~";
	if (isset($s['code'])) $r .= "$establishment_code~";
	if (isset($s['name'])) $r .= "{$data['name']}~";
	if (isset($s['assessor'])) $r .= "{$data['assessor']}~";
	if (isset($s['rep'])) $r .= "{$data['rep']}~";
	if (isset($s['qa'])) $r .= "{$data['qa']}~";
	if (isset($s['qa_cat'])) $r .= "{$data['qa_cat']}~";
	if (isset($s['next_ass'])) $r .= "{$data['next_ass']}~";
	if (isset($s['in_awards'])) $r .= "{$data['in_awards']}~";
	if (isset($s['opted_out'])) $r .= "{$data['opted_out']}~";
	if (isset($s['code_of_conduct'])) $r .= "{$data['code_of_conduct']}~";
	if (isset($s['overnight_coupon'])) $r .= "{$data['overnight_coupon']}~";
	if (isset($s['awards_visit'])) $r .= "{$data['awards_visit']}~";
	if (isset($s['awards_category'])) $r .= "{$data['awards_category']}~";
	if (isset($s['hall_of_fame'])) $r .= "{$data['hall_of_fame']}~";
	if (isset($s['participation_preference'])) $r .= "{$data['participation_preference']}~";
	if (isset($s['predominant_market'])) $r .= "{$data['predominant_market']}~"; 
	if (isset($s['contact_name'])) $r .= "{$data['contact_name']}~"; 
	if (isset($s['designation'])) $r .= "{$data['designation']}~"; 
	if (isset($s['contact_tel'])) $r .= "{$data['contact_tel']}~"; 
	if (isset($s['contact_cell'])) $r .= "{$data['contact_cell']}~"; 
	if (isset($s['contact_email'])) $r .= "{$data['contact_email']}~"; 
	if (isset($s['country'])) $r .= "{$data['country']}~"; 
	if (isset($s['province'])) $r .= "{$data['province']}~"; 
	if (isset($s['town'])) $r .= "{$data['town']}~"; 
	if (isset($s['suburb'])) $r .= "{$data['suburb']}~"; 
    $r .= '|';
}

$r=str_replace("\r","",$r);
$r=str_replace("\n","",$r);
$r=str_replace("\"","",$r);
$r=str_replace(",|","|",$r);

//Draw CSV - Open file
$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/juno/stuff/temp/{$_SESSION['j_user']['id']}.csv", 'w');

//Draw CSV - Header
$csv_header = array();
if (isset($s['code'])) $csv_header[] = "Establishment Code";
if (isset($s['name'])) $csv_header[] = "Establishment Name";
if (isset($s['assessor'])) $csv_header[] = "Assessor";
if (isset($s['rep'])) $csv_header[] = "Rep";
if (isset($s['qa'])) $csv_header[] = "Endorsement";
if (isset($s['qa_cat'])) $csv_header[] = "QA Category";
if (isset($s['next_ass'])) $csv_header[] = "Renewal Date";
if (isset($s['in_awards'])) $csv_header[] = "In Awards";
if (isset($s['opted_out'])) $csv_header[] = "Opted Out";
if (isset($s['code_of_conduct'])) $csv_header[] = "Code Of Conduct";
if (isset($s['overnight_coupon'])) $csv_header[] = "Overnight Coupon";
if (isset($s['awards_visit'])) $csv_header[] = "Awards Visit Date";
if (isset($s['awards_category'])) $csv_header[] = "Awards Category";
if (isset($s['hall_of_fame'])) $csv_header[] = "Hall Of Fame";
if (isset($s['participation_preference'])) $csv_header[] = "Participation Preference";
if (isset($s['predominant_market'])) $csv_header[] = "Predominant Market";
if (isset($s['contact_name'])) $csv_header[] = "Contact Name"; 
if (isset($s['designation'])) $csv_header[] = "Contact Designation"; 
if (isset($s['contact_tel'])) $csv_header[] = "Contact Tel"; 
if (isset($s['contact_cell'])) $csv_header[] = "Contact Cell"; 
if (isset($s['contact_email'])) $csv_header[] = "Contact Email"; 
if (isset($s['country'])) $csv_header[] = "Country"; 
if (isset($s['province'])) $csv_header[] = "Province";
if (isset($s['town'])) $csv_header[] = "Town"; 
if (isset($s['suburb'])) $csv_header[] = "Suburb";
fputcsv($fp, $csv_header);

//Draw CSV - Content
foreach ($establishments as $establishment_code => $data) {
	$csv_content = array();
	if (isset($s['code'])) $csv_content[] = "'$establishment_code";
	if (isset($s['name'])) $csv_content[] = $data['name'];
	if (isset($s['assessor'])) $csv_content[]  = $data['assessor'];
	if (isset($s['rep'])) $csv_content[]  = $data['rep'];
	if (isset($s['qa'])) $csv_content[]  = $data['qa'];
	if (isset($s['qa_cat'])) $csv_content[]  = $data['qa_cat'];
	if (isset($s['next_ass'])) $csv_content[]  = $data['next_ass'];
	if (isset($s['in_awards'])) $csv_content[]  = $data['in_awards'];
	if (isset($s['opted_out'])) $csv_content[]  = $data['opted_out'];
	if (isset($s['code_of_conduct'])) $csv_content[]  = $data['code_of_conduct'];
	if (isset($s['overnight_coupon'])) $csv_content[]  = $data['overnight_coupon'];
	if (isset($s['awards_visit'])) $csv_content[]  = $data['awards_visit'];
	if (isset($s['awards_category'])) $csv_content[]  = $data['awards_category'];
	if (isset($s['hall_of_fame'])) $csv_content[]  = $data['hall_of_fame'];
	if (isset($s['participation_preference'])) $csv_content[]  = $data['participation_preference'];
	if (isset($s['predominant_market'])) $csv_content[]  = $data['predominant_market']; 
	if (isset($s['contact_name'])) $csv_content[]  = $data['contact_name']; 
	if (isset($s['designation'])) $csv_content[]  = $data['designation']; 
	if (isset($s['contact_tel'])) $csv_content[]  = $data['contact_tel']; 
	if (isset($s['contact_cell'])) $csv_content[]  = $data['contact_cell']; 
	if (isset($s['contact_email'])) $csv_content[]  = $data['contact_email']; 
	if (isset($s['country'])) $csv_content[]  = $data['country']; 
	if (isset($s['province'])) $csv_content[]  = $data['province']; 
	if (isset($s['town'])) $csv_content[]  = $data['town']; 
	if (isset($s['suburb'])) $csv_content[]  = $data['suburb']; 
	
	fputcsv($fp, $csv_content);
}

//Draw CSV - Close File
fclose($fp);

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/jsscripts/list_item_remover.js></script>";
echo "<script src=".$ROOT."/apps/awards/awards_report/res.js></script>";
echo "<script>J_in_r(\"".$r."\",\"".$da."\",".count($establishments).",".$jL1.",".$jL2.",'{$_SESSION['j_user']['id']}')</script>";


?>