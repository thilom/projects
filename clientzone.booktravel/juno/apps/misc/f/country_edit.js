function J_m_C(e,d,n)
{
	var a=""
	if(e&&!d)
		a+="<tt>Type in a few letters of the country you want to edit - then select from the return menu</tt><hr><input type=text onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_nlocations_countries&c1=country_id&c2=country_name'0,4,9) onclick=J_ajax_C(this) title='Requires value' class=jW100><input type=hidden name=n value=''><hr><input type=button onclick=\"J_opaq();location=ROOT+'/apps/misc/country_edit.php?e='+$N('n')[0].value\" value=OK>"
	else
		a+="<var><b>IMPORTANT!</b> Ensure you check for existing countires to avoid duplicates. Only letters and spaces allowed, all special characters are stripped out.</var><hr><form method=post><input type=text name=n"+(n?" value=\""+n+"\"":"")+" onkeyup=J_ajax_K(this,'/apps/misc/country_edit.php?dup=1',0,4,9,0,1) onclick=J_ajax_C(this) title='Requires value' class=jW100><hr><input type=submit value=OK>"+(d?" <input type=button value=Delete onclick=\"if(confirm('WARNING!\\nPermanently delete?'))location=ROOT+'/apps/misc/country_edit.php?del="+d+"'\">":"")+"</form>"
	a+="<iframe id=j_IF></iframe>"
	document.write(a)
	J_validate()
	j_P.j_Wi[j_W]["h1"].innerHTML=(e?"Edit":"Add")+" Country"
	J_footer(e?"<a href=go: onclick=\"w=j_Wi["+j_W+"]['F'][0]['W'];w.J_opaq();w.location='"+ROOT+"/apps/misc/country_edit.php';return false\" onfocus=blur() class=jW3a onmouseover=\"J_TT(this,'Add a country')\"><img src="+ROOT+"/ico/set/add.png></a>":"<a href=go: onclick=\"w=j_Wi["+j_W+"]['F'][0]['W'];w.J_opaq();w.location='"+ROOT+"/apps/misc/country_edit.php?e=0';return false\" onfocus=blur() class=jW3a onmouseover=\"J_TT(this,'Edit countries')\"><img src="+ROOT+"/ico/set/edit.png></a>")
}

function J_ret(d,v)
{
	J_ajax_G(d,v?"<var>A country with this name exists</var>":"<b>No duplicates found</b>")
}