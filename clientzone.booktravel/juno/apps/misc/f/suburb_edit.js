function J_m_S(e,d,n,c,cn,s,sn,t,tn)
{
 var a=""
 if(e&&!d)
		a+="<tt>Type in a few letters of the suburb you want to edit - then select from the return menu</tt><hr><input type=text onkeyup=J_ajax_K(this,'/system/ajax.php?t=juno_suburb&c1=j_sub_id&c2=j_sub_name',0,4,9) onclick=J_ajax_C(this) title='Requires value' class=jW100><input type=hidden name=n value=''><hr><input type=button onclick=\"J_opaq();location=ROOT+'/apps/misc/suburb_edit.php?e='+$N('n')[0].value\" value=OK>"
	else
	 a+="<var><b>IMPORTANT!</b> Ensure you check for existing suburbs to avoid duplicates. Only letters and spaces allowed, all special characters are stripped out.</var><hr><form method=post><table id=jTR class=jW100><tr><td>Country</td><td width=99%><input type=text"+(c?" value=\""+cn+"\"":"")+" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_nlocations_countries&c1=country_id&c2=country_name',0,4,9) onclick=J_ajax_C(this) class=jW100><input type=hidden name=c value="+(c?c:0)+"></td></tr><tr><td>State</td><td><input type=text"+(s?" value=\""+sn+"\"":"")+" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_nlocations_provinces&c1=province_id&c2=province_name',320,4) onclick=J_ajax_C(this) class=jW100><input type=hidden name=s value="+(s?s:0)+"></td></tr><tr><tr><td>City</td><td><input type=text"+(t?" value=\""+tn+"\"":"")+" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_nlocations_towns&c1=town_id&c2=town_name',0,4,9) onclick=J_ajax_C(this) class=jW100><input type=hidden name=t value="+(t?t:0)+"></td></tr><tr><td>Suburb</td><td><input type=text name=n"+(n?" value=\""+n+"\"":"")+" onkeyup=J_ajax_K(this,'/apps/misc/suburb_edit.php?dup=1',0,4,9,0,1) onclick=J_ajax_C(this) title='Requires value' class=jW100 style=font-weight:bold></td></tr></table><hr><input type=submit value=OK>"+(d?" <input type=button value=Delete onclick=\"if(confirm('WARNING!\\nPermanently delete?'))location=ROOT+'/apps/misc/suburb_edit.php?del="+d+"'\">":"")+"</form>"
	a+="<iframe id=j_IF></iframe>"
	document.write(a)
	J_validate()
	j_P.j_Wi[j_W]["h1"].innerHTML=(e?"Edit":"Add")+" Suburb"
	J_footer(e?"<a href=go: onclick=\"w=j_Wi["+j_W+"]['F'][0]['W'];w.J_opaq();w.location='"+ROOT+"/apps/misc/suburb_edit.php';return false\" onfocus=blur() class=jW3a onmouseover=\"J_TT(this,'Add a suburb')\"><img src="+ROOT+"/ico/set/add.png></a>":"<a href=go: onclick=\"w=j_Wi["+j_W+"]['F'][0]['W'];w.J_opaq();w.location='"+ROOT+"/apps/misc/suburb_edit.php?e=0';return false\" onfocus=blur() class=jW3a onmouseover=\"J_TT(this,'Edit suburbs')\"><img src="+ROOT+"/ico/set/edit.png></a>")
}

function J_ret(d,v)
{
	J_ajax_G(d,v?"<var>A suburb with this name exists</var>":"<b>No duplicates found</b>")
}