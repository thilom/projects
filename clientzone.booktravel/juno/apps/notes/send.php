<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
$id=$_GET["id"];
$gi=mysql_fetch_array(mysql_query("SELECT * FROM nse_notes WHERE note_id=".$id." LIMIT 1"));

if(count($_POST))
{
	include $SDR."/system/activity.php";
	include $SDR."/system/get.php";
	include $SDR."/apps/system/email/f/address_parser.php";
	if($sender["email"] && count($target))
	{
		$u=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$gi["note_user"]." LIMIT 1"));
		$est=J_Value("","establishment","",$gi["establishment_code"]);
		include $SDR."/custom/email_headers.php";
		$subject="AA TRAVEL GUIDES NOTE";
		$message=substr(preg_replace('~[^a-zA-Z0-9 ,;:"@#_<>+=%&\*\-\$\'\(\)\[\]\?\.]~',"",strip_tags(str_replace("\n","<br>",str_replace("\r","",$_POST["message"])),"<br>")),0,100000);
		$msg=$j_email_header;
		$msg.="<b>AA TRAVEL GUIDES NOTE</b><hr>";
		$msg.=($message?$message."<br><br><hr>":"");
		$msg.="<b style='font-size:8pt'>NOTE: ".date("d/m/Y",$gi["note_date"]);
		if($est)
			$msg.=" - ".$est;
		$msg.="</b><br>";
		$msg.=stripslashes($gi["note_content"])."<br>";
		$msg.="<div style='font-size:8pt'>Author: <a href='mailto:".$u["email"]."'>".$u["firstname"]." ".$u["lastname"]."</a></div>";
		$msg.="<br><br><br>";
		$msg.=$j_email_footer;

		require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
		$mail=new PHPMailer(true);
		try
		{
			$mail->AddReplyTo($sender["email"],$sender["name"]);
			$mail->SetFrom($sender["email"],$sender["name"]);
			foreach($target as $k => $v)
				$mail->AddAddress($v["email"],$v["name"]);
			$mail->Subject =$subject;
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
			$mail->MsgHTML($msg);
			$mail->Send();
			// sys activity
			foreach($target as $k => $v)
				J_act("NOTES",7,$sender["name"]." sent ".($est?$est." ":"")."note: ".$id." to: ".$v["name"]." (".$v["email"].")",$id, $gi["establishment_code"]);
			echo "Message Sent OK";
			echo "<script>setTimeout(\"window.parent.J_WX(self)\",2000)</script>";
		}
		catch (phpmailerException $e){echo $e->errorMessage();}
		catch (Exception $e){echo $e->getMessage();}
		die();
	}
	else
		echo "<script>alert('Target email address failure!')</script>";
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/system/email/f/people.js></script>";
echo "<body>";
echo "<script>J_send_people(1)</script>";
$J_title1="Send Note";
$J_icon="<img src=".$ROOT."/ico/set/email.png>";
$J_label=0;
$J_width=360;
$J_height=320;
$J_nomax=1;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>