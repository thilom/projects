<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/get.php";
include_once $SDR."/apps/notes/f/note_apps.php";

if(isset($_GET["conotes"])) // notes by company and app
{
	// accounts edit rights
	if($_GET["app"]==2 && $_SESSION["j_user"]["role"]=="b")
		$edit=1;

	$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
	$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:50);

	$est=J_Value("","establishment","",$_GET["eid"]);
	$pp=array();
	$peo="";
	$notes="";
	$note_cnt=0;
	$q="SELECT * FROM nse_notes WHERE establishment_code='".$_GET["eid"]."' AND note_app=".$_GET["app"]." ORDER BY note_date DESC";
	$jL0=mysql_num_rows(mysql_query($q));
	$q.=" LIMIT ".$jL1.",".$jL2;
	$q=mysql_query($q);
	$note_cnt=mysql_num_rows($q);
	if($note_cnt)
	{
		include_once $SDR."/apps/notes/f/note_apps.php";
		while($g=mysql_fetch_array($q))
		{
			$notes.=$g["note_id"]."~";
			$notes.=date("d/m/Y",$g["note_date"])."~";
			$notes.="~";
			$notes.=$g["note_user"]."~";
			$notes.=stripslashes($g["note_content"])."~";
			if($g["note_reminder"]&&$g["note_user"]==$_SESSION["j_user"]["id"])
				$notes.=date("d/m/Y",$g["note_reminder"]);
			$notes.="~";
			$notes.=$g["establishment_code"]."~";
			$notes.="|";
			if($g["note_user"] && !isset($pp[$g["note_user"]]))
				$pp[$g["note_user"]]=J_Value("","people","",$g["note_user"]);
		}
		foreach($pp as $k => $v){$peo.=$k."~".$v."|";}
	}
}

elseif(isset($_GET["my"])) // all my notes only
{
	$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
	$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:50);

	$my=J_Value("","people","",$_SESSION["j_user"]["id"]);
	$co=array();
	$eco="";
	$notes="";
	$note_cnt=0;
	$q="SELECT * FROM nse_notes WHERE note_user=".$_SESSION["j_user"]["id"]." ORDER BY note_date DESC";
	$jL0=mysql_num_rows(mysql_query($q));
	$q.=" LIMIT ".$jL1.",".$jL2;
	$q=mysql_query($q);
	$note_cnt=mysql_num_rows($q);
	if($note_cnt)
	{
		include_once $SDR."/apps/notes/f/note_apps.php";
		while($g=mysql_fetch_array($q))
		{
			$notes.=$g["note_id"]."~";
			$notes.=date("d/m/Y",$g["note_date"])."~";
			$notes.="~";
			$notes.="~";
			$notes.=stripslashes($g["note_content"])."~";
			if($g["note_reminder"]&&$g["note_user"]==$_SESSION["j_user"]["id"])
				$notes.=date("d/m/Y",$g["note_reminder"]);
			$notes.="~";
			$notes.=$g["establishment_code"]."~";
			$notes.=$g["note_app"]?$g["note_app"]:"";
			$notes.="|";
			if($g["establishment_code"] && !isset($co[$g["establishment_code"]]))
				$co[$g["establishment_code"]]=J_Value("name","establishment","id",$g["establishment_code"]);
		}
		foreach($co as $k => $v){$eco.=$k."~".$v."|";}
	}
}

elseif(isset($_GET["remind"])) // all my reminders
{
	$my=J_Value("","people","",$_SESSION["j_user"]["id"]);
	$reminder=1;
	$co=array();
	$eco="";
	$notes="";
	$note_cnt=0;
	$q="SELECT * FROM nse_notes WHERE note_user=".$_SESSION["j_user"]["id"]." AND note_reminder<".mktime(0,0,0,date("m"),date("d")+1,date("Y"))." AND (note_reminder>=".mktime(0,0,0,date("m"),date("d"),date("Y"))." OR note_action=1) ORDER BY note_date DESC";
	$q=mysql_query($q);
	$note_cnt=mysql_num_rows($q);
	if($note_cnt)
	{
		include_once $SDR."/apps/notes/f/note_apps.php";
		while($g=mysql_fetch_array($q))
		{
			$notes.=$g["note_id"]."~";
			$notes.=date("d/m/Y",$g["note_date"])."~";
			$notes.="~";
			$notes.="~";
			$notes.=stripslashes($g["note_content"])."~";
			$notes.=date("d/m/Y",$g["note_reminder"]);
			$notes.="~";
			$notes.=$g["establishment_code"]."~";
			$notes.=$g["note_app"]?$g["note_app"]:"";
			$notes.="|";
			if($g["establishment_code"] && !isset($co[$g["establishment_code"]]))
				$co[$g["establishment_code"]]=J_Value("name","establishment","id",$g["establishment_code"]);
		}
		foreach($co as $k => $v){$eco.=$k."~".$v."|";}
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/notes/f/view.js></script>";
echo "<script>vnote(";
echo (isset($edit)?$edit:0);
echo ",".(isset($send)?$send:0);
echo ",\"".str_replace("~|","|",$notes)."\"";
echo ",\"".(isset($app)?$app:(isset($_GET["app"])?$_GET["app"]:0))."\"";
echo ",\"".(isset($eid)?$eid:(isset($_GET["eid"])?$_GET["eid"]:0))."\"";
echo ",\"".(isset($iid)?$iid:(isset($_GET["iid"])?$_GET["iid"]:0))."\"";
echo ",".(isset($eco)&&$eco?"\"".$eco."\"":0);
echo ",".(isset($peo)&&$peo?"\"".$peo."\"":0);
echo ",".(isset($my)&&$my?"\"".$my."\"":0);
echo ",".(isset($est)&&$est?"\"".$est."\"":0);
echo ",".(isset($reminder)&&$reminder?"\"".$reminder."\"":0);
echo ",".(isset($_GET["app"])&&$_GET["app"]&&isset($_GET["eid"])&&$_GET["eid"]?1:0);
echo ",\"".$_SESSION["j_user"]["role"]."\"";
if(isset($jL0))
	echo ",".$jL0.",".$jL1.",".$jL2;
echo ")</script>";

$J_title1=isset($my)?$my:(isset($est)?$est:(isset($J_title1)&&$J_title1?$J_title1:"Notes"));
$J_title2=isset($my)||isset($est)?"All ".(isset($_GET["app"])?$note_apps[$_GET["app"]]." ":"")."notes generated ".(isset($_GET["app"])?"for":"by").":":(isset($J_title2)&&$J_title2?$J_title2:"");
$J_icon="<img src=".$ROOT."/ico/set/note.png>";
$J_label=1;
$J_width=isset($reminder)?1000:(isset($_GET["my"])||isset($est)?1600:800);
$J_height=480;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>