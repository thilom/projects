<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$id=isset($_GET["id"])?$_GET["id"]:0;
$eid=isset($_POST["eid"])?$_POST["eid"]:(isset($_GET["eid"])?$_GET["eid"]:0);
$app=isset($_POST["app"])?$_POST["app"]:(isset($_GET["app"])?$_GET["app"]:0);
$iid=isset($_GET["iid"])?$_GET["iid"]:0;
$reload=isset($_GET["reload"])?$_GET["reload"]:0;
$frame=isset($_GET["frame"])?$_GET["frame"]:0;
$r=$_SESSION["j_user"]["role"];

if(isset($_GET["del"]))
{
	mysql_query("DELETE FROM nse_notes WHERE note_id=".$id);
	echo "<script>p=window.parent;p.J_reload(".$reload.");p.J_WX(self)</script>";
	die();
}

elseif(count($_POST) && isset($_SESSION["j_user"]["id"]))
{
	// prep values
	include_once $SDR."/system/parse.php";
	include_once $SDR."/apps/notes/f/insert.php";
	$_POST["cont"]=addslashes(preg_replace("~[^a-zA-Z0-9 ,:;'!@#%&_=+/\*\(\)\[\]\$\-\.]~","",str_replace("\n","<br>",$_POST["cont"])));
	$_POST["act"]=(($r!="c")&&isset($_POST["act"])?1:0);
	$_POST["staff"]=(($r=="b"||$r=="d"||$r=="s")&&isset($_POST["remind"])&&$_POST["remind"]&&isset($_POST["staff"])?1:0);
	$_POST["can"]=(isset($_POST["can"])?1:0);

	if($id) // update
	{
		$q="UPDATE nse_notes SET
		note_content='".$_POST["cont"]."'
		,note_reminder=".(isset($_POST["remind"])&&$_POST["remind"]?J_dateParse($_POST["remind"]):0)."
		,note_action=".$_POST["act"]."
		,note_cancellation=".$_POST["can"]."
		".(isset($_POST["eid"])?",establishment_code='".$_POST["eid"]:"").
		($r!="c"?",note_staff=".$_POST["staff"]:"")."
		WHERE note_id=".$id;
		mysql_query($q);
		include_once $SDR."/system/activity.php";
		J_act("NOTE",4,$note_apps[$app]." - ".substr($_POST["cont"],0,32),$id,$eid);
	}
	else // insert
		$id=insertNote($eid,isset($app)?$app:$_POST["app"],$iid,$_POST["cont"],$_POST["remind"],$_POST["staff"],$_POST["can"],0,0,$_POST["act"]);
	// close window
	die("<script>w=window.parent;".($reload?"w.J_reload(".$reload.($frame?",".$frame:"").");":"")."w.J_WX(self)</script>");
}


echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/notes/f/edit.js></script>";
if($id)
	$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_notes WHERE note_id=".$id." LIMIT 1"));
echo "<script>enote(";
echo date("Ymd",$EPOCH);
echo ",".($r=="b"||$r=="d"||$r=="s"?1:0);
echo ",".$id;
echo ",'".$eid."'";
echo ",".($id&&$g["note_app"]?$g["note_app"]:(isset($app)?$app:0));
echo ",'".$iid."'";
echo ",\"".($id?str_replace("\n","<br>",stripslashes($g["note_content"])):"")."\"";
echo ",\"".($id&&$g["note_reminder"]?date("d/m/Y",$g["note_reminder"]):"")."\"";
echo ",".($id&&$g["note_staff"]>0?1:0);
echo ",".($id&&$g["note_cancellation"]>0?1:0);
echo ",".($id&&$g["note_action"]>0?1:0);
echo ",".($id&&$g["note_user"]==$_SESSION["j_user"]["id"]?1:0);
echo ($reload?",".$reload:"");
echo ")</script>";

$J_title1=($id?"Edit":"New")." Note";
$J_icon="<img src=".$ROOT."/ico/set/note.png><var><img src=".$ROOT."/ico/set/".($id?"edit":"add").".png></var>";
$J_label=1;
$J_width=560;
$J_height=360;
$J_nomax=1;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>