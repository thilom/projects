<?php

/**
 * Insert a note into the notes system
 *
 * @global string $SDR
 * @global string $EPOCH
 * @global string $note_apps
 * @global string $j_email_header
 * @global string $j_email_footer
 * @param string $eid // establishment id
 * @param int $app // which app
 * @param string $iid // item id - must have item type in front: INV123
 * @param string $cont // content for note
 * @param int $remind // epoch
 * @param int $staff
 * @param int $cancel
 * @param int $share
 * @param int $notify
 * @param int $action // the reminder will persist untill dismissed
 * @param string $variable // will retrieve people according to variable name
 * @return mixed
 */
function insertNote($eid=0,$app=0,$iid="",$cont="",$remind=0,$staff=0,$cancel=0,$share=0,$notify=0,$action=0,$variable="")
{
	global $SDR,$ROOT,$EPOCH,$note_apps;
	include_once $SDR."/system/get.php";
	include_once $SDR."/system/parse.php";
	include_once $SDR."/system/activity.php";
	$cont=addslashes(preg_replace("~[^a-zA-Z0-9 ,:;'!@#%&_=+/\*\(\)\[\]\$\-\.]~","",str_replace("\n","<br>",$cont)));
	$remind=$remind?J_dateParse($remind):0;
	$r=$_SESSION["j_user"]["role"];
	$staff=($r=="b"||$r=="d"||$r=="s")&&$staff?1:0;
	$share=$share>0?$share:0;
	mysql_query("INSERT INTO nse_notes (establishment_code,note_user,note_date,note_app,note_item,note_content,note_reminder,note_staff,note_cancellation,note_share,note_action) VALUES ('".$eid."',".$_SESSION["j_user"]["id"].",".$EPOCH.",".$app.",'".$iid."','".$cont."',".$remind.",".$staff.",".$cancel.",".$share.",".$action.")");
	$nid=J_ID("nse_notes","note_id");
	J_act("NOTE",3,$note_apps[$app]." - ".substr($cont,0,32),$iid,$eid);
	// if cancellation, then email
	 if(($notify && $notify!=-1) || ($cancel && ($app==2 || $app==3 || $app==4)))
	{
		include_once $SDR."/utility/PHPMailer/class.phpmailer.php";
		include_once $SDR."/custom/email_headers.php";
		global $j_email_header,$j_email_footer;
		$est=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$eid."' LIMIT 1"));

		// email to
		if($cancel)
		{
			$to=array();

			if($app==2) // accounts people
				$q="SELECT * FROM nse_variables WHERE variable_name='notify_invoice_cancel' AND variable_value!='".$_SESSION["j_user"]["id"]."'";
			elseif($app==3) // assessments
			{
				$q="SELECT * FROM nse_variables WHERE variable_name='notify_assessment_cancel'";
				$to[]=$est["assessor_id"];
			}
			elseif($app==4) // establishment
			{
				$q="SELECT * FROM nse_variables WHERE variable_name='notify_establishment_cancel'";
				$to[]=$est["assessor_id"];
			}
			elseif($app==7) // establishment
			{
				$q="SELECT * FROM nse_variables WHERE variable_name='notify_ad_cancel'";
				$to[]=$est["assessor_id"];
				$to[]=$est["rep_id1"];
			}
		}
		elseif($variable)
		{
			$q="SELECT * FROM nse_variables WHERE variable_name='".$variable."' AND variable_value!='".$_SESSION["j_user"]["id"]."'";
			if($variable=="invoice_delete")
				$to[]=$est["assessor_id"];
		}

		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
				$to[]=$g["variable_value"];
		}

		$mail=new PHPMailer(true);
		try
		{
			$sender=mysql_fetch_array(mysql_query("SELECT email,firstname,lastname FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1"));
			$mail->AddReplyTo($sender["email"],$sender["firstname"]." ".$sender["lastname"]);
			$mail->SetFrom($sender["email"],$sender["firstname"]." ".$sender["lastname"]);
			foreach($to as $k => $v)
			{
				if($v>0)
				{
					$q="SELECT email,firstname,lastname FROM nse_user WHERE user_id=".$v." LIMIT 1";
					$q=mysql_query($q);
					if(mysql_num_rows($q))
					{
						$g=mysql_fetch_array($q);
						$mail->AddAddress($g["email"],$g["firstname"]." ".$g["lastname"]);
						J_act($note_apps[$app],7,$notify?$notify:($cancel?"Cancel Note (".$note_apps[$app].")":"Notification"),0,$eid);
					}
				}
			}
			$msg="";
			$msg=$j_email_header;
			$msg.="<b>".($notify?$notify:($cancel?"Cancel Notification":"Notification"))."</b> (".$est["establishment_name"].")<br>";
			$msg.="<b style='font-size:9pt'>Section: ".$note_apps[$app]."</b><br>";
			$msg.="<b style='font-size:9pt'>By: ".ucwords(strtolower($sender["firstname"]." ".$sender["lastname"]))."</b><br>";
			$msg.="<b style='font-size:9pt'>Date: ".date("d/m/Y H:i",$EPOCH)."</b><br>";
			$msg.="<hr>";
			$msg.=$cont;
			if($app==2 && $iid)
				$msg.="<hr><a href='".$ROOT."/apps/accounts/invoices/view.php?id=".$iid."' style='font-size:9pt'><b>View Invoice</b></a>";
			$msg.="<br><br><br>";
			$msg.=$j_email_footer;
			$mail->Subject=($notify?$notify:"Cancel Notification")." (".$est["establishment_name"].")";
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
			$mail->MsgHTML($msg);
			$mail->Send();
		}
		catch (phpmailerException $e){}
		catch (Exception $e){}
	}
	return $nid;
}
?>
