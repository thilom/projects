today=0

function enote(tdy,stff,id,eid,app,iid,cont,alrm,stfo,can,act,s,r)
{
	today=tdy
	var a=""
	a+="<body style=margin:8>"
	a+="<form method=post>"
	a+="<table width=100% height=100% cellspacing=0>"
	a+="<tr><td height=99%><textarea name=cont style=height:100%;width:100%;font-size:16px>"+(cont?cont.replace(/<br>/,"\n"):"")+"</textarea></td></tr>"
	a+="<tr><td height=20%>"
	a+="<tt onclick=j_P.J_checkbox(this);J_rem(this) onmouseover=\"J_TT(this,'<b>Reminder</b> Set a date for when you wish to be reminded about this note')\"><input type=checkbox name=remind value='"+alrm+"'"+(alrm?" checked":"")+"> Reminder <var id=alrm>"+(alrm.length>1?alrm:"")+"</var></tt>"
	if(stff)
	{
		a+="<tt onclick=j_P.J_checkbox(this) onmouseover=\"J_TT(this,'This note will persist until dismissed')\""+(alrm?"":" class=ji")+" style='padding:0 0 0 16'><input type=checkbox name=act value=1"+(act?" checked":"")+"> Action</tt>"
		a+="<tt onclick=j_P.J_checkbox(this) onmouseover=\"J_TT(this,'This note will only be viewed by staff')\"><input type=checkbox name=staff value=1"+(stfo||!id?" checked":"")+"> Staff Only</tt>"
	}
	a+="<tt onclick=j_P.J_checkbox(this) onmouseover=\"J_TT(this,'Is this note contain any form of cancellation')\"><input type=checkbox name=can value=1"+(can?" checked":"")+"> Cancellation</tt>"
	a+="</td></tr>"
	a+="<tr><td height=20><input type=submit value=OK>"
	if(stff&&id)
			a+=" <input type=button value=Delete onclick=\"location='"+ROOT+"/apps/notes/edit.php?del=1&id="+id+(r?"&reload="+r:"")+"'\">"
	a+="</td></tr>"
	a+="</table>"
	a+="</form>"
	document.write(a)
	if(s&&id)
		J_footer("<a href=go: onclick=\"J_W(ROOT+'/apps/notes/send.php?id="+id+"');return false\" onfocus=blur() class=jW3a onmouseover=\"J_TT(this,'Send this note')\"><img src="+ROOT+"/ico/set/email.png></a>")
}

function J_rem(t)
{
	j_E=t.firstChild
	j_P.J_datePicker(self,2)
	if(j_E.checked==true)
	{
		j_E.value=j_P.J_date("d/m/Y")
		$("alrm").innerHTML=j_P.J_date("d/m/Y")
		Jact(1)
	}
	else
	{
		j_E.value=0
		$("alrm").innerHTML=""
		Jact(0)
	}
}

function J_dp_aft(t,r)
{
	var v=t.value,x=J_test(v)
	t.checked=x?true:false
	if(!x)
		t.value=""
	$("alrm").innerHTML=t.value
	Jact(x)
}

function Jact(x)
{
	var t=$N("act")
	if(t.length)
	{
		if(!x)
			t[0].checked=false
		t[0].parentNode.className=x?"":"ji"
	}
}

function J_test(v)
{
	if(v)
	{
		var v=v.replace(/[^0-9]/g,""),d=v.substr(0,2),m=v.substr(2,2),y=v.substr(4,4)
		return (y+""+m+""+d)*1>=today?1:0
	}
	else
		return 0
}