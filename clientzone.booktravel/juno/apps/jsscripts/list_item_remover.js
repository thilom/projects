// INSTRUCTIONS
// include this script in your res.js
// in the function that displays the icon platform add:
// w = j_Wi["+j_W+"]['F']["+j_Wn+"]['W']
// <a href=go: onclick=\"J_note0();"+w+".J_list_item_remove();return false\" onmouseover="J_TT(this,'Remove from this list any row whose column contains:<b>"+t.innerHTML.replace(/[^a-zA-Z0-9 \/\-\.]/g,"")+"</b>')" onfocus=blur()><img style=height:32;width:32;margin:2 src="+ROOT+"/ico/set/cancel.png></a>
// 

function J_list_item_remove()
{
	var t=j_E.parentNode,v=j_E.innerHTML,i,n=0
	while(t.childNodes[n])
	{
		if(t.childNodes[n]==j_E)
			break
		n++
	}
	t=$T($("jTB"),"tr")
	i=t.length-1
	while(i>0)
	{
		if(v==t[i].childNodes[n].innerHTML)
			t[i].parentNode.removeChild(t[i])
		i--
	}
	J_tr()
}