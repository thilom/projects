
function change_type(tID) {
    document.getElementById('div_permanent').className = 'div_hide';
    document.getElementById('div_single').className = 'div_hide';
    document.getElementById('div_view_limit').className = 'div_hide';
    document.getElementById('div_date').className = 'div_hide';

    document.getElementById(tID).className = 'div_show';
}

