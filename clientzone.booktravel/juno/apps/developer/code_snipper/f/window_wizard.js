function D_c(v)
{
	var i,u,a="<tt>Use this tool to setup code for a new page</tt><hr><br><table id=jTR class=jW100>"
	a+="<tr><td>Security</td><td width=99%><input type=checkbox checked onclick=D_x() onmouseover=\"J_TT(this,'Secure the page')\"></td></tr>"
	a+="<tr><td colspan=2 onmouseover=\"J_TT(this,'This is access for various parts of a page')\"><tt onclick=J_V('sa') style=cursor:pointer><b>Sub Access &#9660;</b></tt></td></tr><tbody id=sa class=ji>"
	i=1
	while(i<6)
	{
		a+="<tr><td>Sub "+i+"</td><td><input type=text onkeyup=D_x() class=jW50></td></tr>"
		i++
	}
	a+="</tbody>"
	a+="<tr><td colspan=2><tt onclick=J_V('ci') style=cursor:pointer><b>Includes &#9660;</b></tt></td></tr><tbody id=ci class=ji>"
	a+="<tr><td colspan=2><tt>Below are some of the commonly used include files.</tt></td></tr>"
	a+="<tr><td colspan=2 onmouseover=\"J_TT(this,'Contains functions that pertain to files and directories')\"><input type=checkbox onclick=D_x()> system/dir.php</td></tr>"
	a+="<tr><td colspan=2 onmouseover=\"J_TT(this,'Contains functions that get values from database or files')\"><input type=checkbox onclick=D_x()> system/get.php</td></tr>"
	a+="<tr><td colspan=2 onmouseover=\"J_TT(this,'Contains functions that parse/process dates and strings')\"><input type=checkbox onclick=D_x()> system/parse.php</td></tr>"
	a+="<tr><td colspan=2 onmouseover=\"J_TT(this,'The activity log function')\"><input type=checkbox onclick=D_x()> system/activity.php</td></tr>"
	a+="</tbody>"
	a+="<tr><td>Title 1</td><td><input type=text onkeyup=D_x() value=Untitled onmouseover=\"J_TT(this,'Main large title of the window')\" class=jW100></td></tr>"
	a+="<tr><td>Title 2</td><td><input type=text onkeyup=D_x() onmouseover=\"J_TT(this,'Small title that appears above the main-title')\" class=jW100></td></tr>"
	a+="<tr><td>Title 3</td><td><input type=text onkeyup=D_x() onmouseover=\"J_TT(this,'Small descriptor that appears below the main-title')\" class=jW100></td></tr>"
	a+="<tr><td>Icon</td><td><input type=text onkeyup=D_x() ondblclick=D_gf(this,1) onmouseover=\"J_TT(this,'Icon for the window - <b style=color:#DD0000>Double-click for icon finder</b>')\" class=jW100></td></tr>"
	a+="<tr><td onmouseover=\"J_TT(this,'Append another icon to the one above')\" onclick=w_iV('iv') style=cursor:pointer nowrap><tt><b>Icon variant &#9658;</b></tt></td><td><input type=text id=iv onkeyup=D_x() ondblclick=D_gf(this,1) onmouseover=\"J_TT(this,'<b style=color:#DD0000>Double-click for icon finder</b>')\" class=ji style=width:100%></td></tr>"
	a+="<tr><td>Tool-tip</td><td><textarea onkeyup=D_x() onmouseover=\"J_TT(this,'Add descriptive text about the window that appears when the user mouse-overs the icon')\" class=jW100></textarea></td></tr>"
	a+="<tr><td>Label</td><td><select onchange=D_x() onmouseover=\"J_TT(this,'Colour for the window')\">"
	v=v.split("|")
	i=0
	while(v[i])
	{
		u=v[i].split("~")
		a+="<option value="+u[0]+">"+u[1]
		i++
	}
	a+="</select></td></tr>"
	a+="<tr><td>Width</td><td><input type=text onkeyup=D_x() value=1000 onmouseover=\"J_TT(this,'Fix the initial width of the new window')\" size=4></td></tr>"
	a+="<tr><td>Height</td><td><input type=text onkeyup=D_x() value=720 onmouseover=\"J_TT(this,'Fix the initial height of the new window')\" size=4></td></tr>"
	a+="<tr><td>Top</td><td><input type=text onkeyup=D_x() onmouseover=\"J_TT(this,'Fix the initial top position of the new window')\" size=4></td></tr>"
	a+="<tr><td>Left</td><td><input type=text onkeyup=D_x() onmouseover=\"J_TT(this,'Fix the initial left position of the new window')\" size=4></td></tr>"
	a+="<tr><td nowrap>Frame size</td><td><input type=text onkeyup=D_x() onmouseover=\"J_TT(this,'The size of the first frame panel - if there are more than one')\" size=4></td></tr>"
	a+="<tr><td>No-Max</td><td><input type=checkbox onclick=D_x() onmouseover=\"J_TT(this,'Prohibit the window from being maximized')\"></td></tr>"
	a+="<tr><td>No-Start</td><td><input type=checkbox checked onclick=D_x() onmouseover=\"J_TT(this,'Prohibit the window from being saved for opening automatically on system start')\"></td></tr>"
	a+="<tr><td>Footer</td><td><textarea onkeyup=D_x() class=jW100></textarea></td></tr>"
	a+="<tr><td>Home</td><td><input type=text onkeyup=D_x() size=4 onmouseover=\"J_TT(this,'If you want this page to be included on a directories home-landing-page, then put a value of 0 upward. This number also determines the sort order of icons shown in the home window. Also ensure this page is in the same directory as the home.php page')\"></td></tr>"
	a+="<tr><td>Join</td><td><input type=text onkeyup=D_x()  onmouseover=\"J_TT(this,'Give the window a join name. If other windows have the same join name they will not display as reference icons at the bottom of the screen, but be added to the mouseover list above the reference icon')\"></td></tr>"
	a+="<tr><td>Group</td><td><input type=text onkeyup=D_x()  onmouseover=\"J_TT(this,'Give the window a group name. If other windows have the same join name they will not display as reference icons at the bottom of the screen, but be added to the mouseover list above this windows reference icon')\"></td></tr>"
	a+="<tr><td>Frame 1</td><td><input type=text onkeyup=D_x() ondblclick=D_gf(this) onmouseover=\"J_TT(this,'Another frame to be loaded - <b style=color:#DD0000>Double-click for page browser</b>')\" class=jW100></td></tr>"
	a+="<tr><td colspan=2><tt onclick=J_V('mf') style=cursor:pointer><b>More frames &#9660;</b></tt></td></tr><tbody id=mf class=ji>"
	i=2
	while(i<17)
	{
		a+="<tr><td>Frame "+i+"</td><td><input type=text onkeyup=D_x() ondblclick=D_gf(this) onmouseover=\"J_TT(this,'Another frame to be loaded - <b style=color:#DD0000>Double-click for page browser</b>')\" class=jW100></td></tr>"
		i++
	}
	a+="</tbody>"
	a+="</table>"+D_f("window create")+"</body></html>"
	document.write(a)
	J_tr()
	D_x()
}

function D_x()
{
	var n,v,i=$T("input"),t=$T("textarea"),s=$T("select"),a="",z=""
	a+="<b class=jO20>&lt;?php</b><br>"
	z+="<?php\n"
	a+="<b>require_once $_SERVER[\"DOCUMENT_ROOT\"].\""+ROOT+"/set/init.php\";</b><br>"
	z+="require_once $_SERVER[\"DOCUMENT_ROOT\"].\""+ROOT+"/set/init.php\";\n"
	if(i[0].checked==true)
	{
		c=1,v=""
		while(c<6)
		{
			v+=i[c].value?(v?",":"")+"\""+i[c].value+"=>0\"":""
			c++
		}
		if(v)
		{
			a+="<b>$SUB_ACCESS=array(</b>"+v+"<b>);</b><br>"
			z+="$SUB_ACCESS=array("+v+");\n"
		}
	}
	a+=i[0].checked==true?"<b>include_once </b>$SDR.\"/system/secure.php<b>\";</b><br>":""
	z+=i[0].checked==true?"include_once $SDR.\"/system/secure.php\";\n":""
	a+=i[6].checked==true?"<b>include_once </b>$SDR.\"/system/dir.php<b>\";</b><br>":""
	z+=i[6].checked==true?"include_once $SDR.\"/system/dir.php\";\n":""
	a+=i[7].checked==true?"<b>include_once </b>$SDR.\"/system/get.php<b>\";</b><br>":""
	z+=i[7].checked==true?"include_once $SDR.\"/system/get.php\";\n":""
	a+=i[8].checked==true?"<b>include_once </b>$SDR.\"/system/parse.php<b>\";</b><br>":""
	z+=i[8].checked==true?"include_once $SDR.\"/system/parse.php\";\n":""
	a+=i[9].checked==true?"<b>include_once </b>$SDR.\"/apps/system/activity.php<b>\";</b><br>":""
	z+=i[9].checked==true?"include_once $SDR.\"/apps/system/activity.php\";\n":""
	a+="<br><b class=jO30>echo \"&lt;html&gt;\";<br>echo \"&lt;script src=\".$ROOT.\"/system/P.js&gt;&lt;/script&gt;\";<br>echo \"&lt;body&gt;\";</b><br><br><br><b class=jO20>// Your PHP code goes here</b><br><br><br>"
	z+="\necho \"<html>\";\necho \"<script src=\".$ROOT.\"/system/P.js></script>\";\necho \"<body>\";\n\n\n// Your PHP code goes here\n\n\n"
	a+="<b>$J_title1=\"</b>"+(i[10].value?i[10].value.replace(/"/g,""):"Untitled")+"<b>\";</b><br>"
	z+="$J_title1=\""+(i[10].value?i[10].value.replace(/"/g,""):"Untitled")+"\";\n"
	a+=i[11].value?"<b>$J_title2=\"</b>"+i[11].value.replace(/"/g,"")+"<b>\";</b><br>":""
	z+=i[11].value?"$J_title2=\""+i[11].value.replace(/"/g,"")+"\";\n":""
	a+=i[12].value?"<b>$J_title3=\"</b>"+i[12].value.replace(/"/g,"")+"<b>\";</b><br>":""
	z+=i[12].value?"$J_title3=\""+i[12].value.replace(/"/g,"")+"\";\n":""
	a+=i[13].value?"<b>$J_icon=\"</b>&lt;img src=\".$ROOT.\""+i[13].value.replace(/"/g,"")+"&gt;"+(i[14].value?"&lt;var&gt;&lt;img src=\".$ROOT.\""+i[14].value.replace(/"/g,"")+"&gt;&lt;/var&gt;":"")+"<b>\";</b><br>":""
	z+=i[13].value?"$J_icon=\"<img src=\".$ROOT.\""+i[13].value.replace(/"/g,"")+">"+(i[14].value?"<var><img src=\".$ROOT.\""+i[14].value.replace(/"/g,"")+"></var>;":"")+"\";\n":""
	a+=t[0].value?"<b>$J_tool=\"</b>"+t[0].value.replace(/\\n/g,"<br>").replace(/[^a-zA-Z0-9 ,:;'<>=+!@#%&_\*\(\)\$\/\.\-]/g,"")+"<b>\";</b><br>":""
	z+=t[0].value?"$J_tool=\""+t[0].value.replace(/\\n/g,"<br>").replace(/[^a-zA-Z0-9 ,:;'<>=+!@#%&_\*\(\)\$\/\.\-]/g,"")+"\";\n":""
	a+="<b>$J_label=</b>"+s[0].value+"<b>;</b><br>"
	z+="$J_label="+s[0].value+";\n"
	v=i[15].value.replace(/[^0-9]/g,"")*1
	a+=v&&v!=1000?"<b>$J_width=</b>"+v+"<b>;</b><br>":""
	z+=v&&v!=1000?"$J_width="+v+";\n":""
	v=i[16].value.replace(/[^0-9]/g,"")*1
	a+=v&&v!=720?"<b>$J_height=</b>"+v+"<b>;</b><br>":""
	z+=v&&v!=720?"$J_height="+v+";\n":""
	v=i[17].value.replace(/[^0-9]/g,"")*1
	a+=v?"<b>$J_top=</b>"+v+"<b>;</b><br>":""
	z+=v?"$J_top="+v+";\n":""
	v=i[18].value.replace(/[^0-9]/g,"")*1
	a+=v?"<b>$J_left=</b>"+v+"<b>;</b><br>":""
	z+=v?"$J_left="+v+";\n":""
	v=i[19].value.replace(/[^0-9]/g,"")*1
	v=i[25].value&&!v?280:!i[25].value?0:0
	a+=v?"<b>$J_framesize=</b>"+v+"<b>;</b><br>":""
	z+=v?"$J_framesize="+v+";\n":""
	a+=i[20].checked==true?"<b>$J_nomax=</b>1<b>;</b><br>":""
	z+=i[20].checked==true?"$J_nomax=1;\n":""
	a+=i[21].checked==true?"<b>$J_nostart=</b>1<b>;</b><br>":""
	z+=i[21].checked==true?"$J_nostart=1;\n":""
	v=i[22].value.replace(/[^0-9]/g,"")*1
	a+=v?"<b>$J_home=</b>"+v+"<b>;</b><br>":""
	z+=v?"$J_home="+v+";\n":""
	v=i[23].value.replace(/[^a-zA-Z0-9 \-]/g,"")
	a+=v?"<b>$J_join=\"</b>"+v+"<b>\";</b><br>":""
	z+=v?"$J_join=\""+v+"\";\n":""
	v=i[24].value.replace(/[^a-zA-Z0-9 \-]/g,"")
	a+=v?"<b>$J_group=\"</b>"+v+"<b>\";</b><br>":""
	z+=v?"$J_group=\""+v+"\";\n":""
	a+=t[1].value?"<b>$J_foot=\"</b>"+t[1].value.replace(/\\n/g,"<br>").replace(/[^a-zA-Z0-9 ,:;'<>=+!@#%&_\*\(\)\$\/\.\-]/g,"")+"<b>\";</b><br>":""
	z+=t[1].value?"$J_foot=\""+t[1].value.replace(/\\n/g,"<br>").replace(/[^a-zA-Z0-9 ,:;'<>=+!@#%&_\*\(\)\$\/\.\-]/g,"")+"\";\n":""
	c=25,n=1
	while(i[c])
	{
		if(i[c].value)
		{
			a+="<b>$J_frame"+n+"=\"</b>"+i[c].value+"<b>\";</b><br>"
			z+="$J_frame"+n+"=\""+i[c].value+"\";\n"
			n++
		}
		c++
	}
	a+="<b>include </b>$SDR.\"/system/deploy.php<b>\";</b><br><b class=jO30>echo \"&lt;/body&gt;&lt;/html&gt;\";</b><br><b class=jO20>?&gt;</b>"
	z+="include $SDR.\"/system/deploy.php\";\necho \"</body></html>\";\n?>"
	j_P.j_Wi[j_W]["save"]=z

	out.innerHTML="<tt>Code:</tt><hr><br><div style=white-space:nowrap>"+a+"</div><br><br><hr><input type=button value=Save onclick=J_W(ROOT+'/utility/browse/f/save.php?w="+j_W+"&all_acts=1')>"
}

function D_gf(t,v)
{
	j_P.j_G["lastObj"]=[self,t]
	J_W(ROOT+"/utility/media_viewer/f/view.php?"+(v?"image&dir":"web&dir"))
}

function J_Field(v)
{
	v=v.replace(/\/\//,"/")
	j_P.j_G["lastObj"][1].value=v.replace(ROOT,'').replace(/\/\//,"/")
	D_x()
}

function w_iV(v)
{
	J_V(v)
	v=$(v)
	v.value=v.className?"":"/ico/set/home_sys.png"
	D_x()
}
