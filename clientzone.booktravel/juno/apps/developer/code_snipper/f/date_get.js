function D_c()
{
	document.write("<tt>Get most date components from client-side date-time. This function allows for user preferred date format output, this is in the form of a string. Click on the field below to see the options available.</tt><hr><br><table id=jTR class=jW100><tr><td>Input</td><td><input type=text onkeyup=D_x() value='d/m/Y' onclick=\"J_note(this,'<tt><strong>Y</strong> Full year eg: 2000<br><strong>M</strong> Month name eg: September<br><strong>m</strong> Month eg: 09<br><strong>d</strong> Day eg: 08<br><strong>h</strong> Hour eg: 12<br><strong>i</strong> Minute eg: 15<br>You can add your own separators, like / or - or ,</tt>',320,3,5,0)\"></td></tr></table>"+D_f("date")+"</body></html>")
	J_tr()
	D_x()
}

function D_x()
{
	var v=$T("input")[0].value
	out.innerHTML="<tt>Code</tt><hr><br><b>j_P.J_date('"+v+"')</b><br><br><hr><tt>Returns: <b>"+j_P.J_date(v)+"</b></tt>"
}