a="j_Wi["+j_W+"]['F'][1]['W'].D_v('"
d_obj="<b class=jA><a href=go: onclick=\""+a+"$(\\\'id\\\')');return false\" onfocus=blur()>Get element by id</a><a href=go: onclick=\""+a+"$N(\\\'name\\\')[0]');return false\" onfocus=blur()>Get element by name</a><a href=go: onclick=\""+a+"$T(\\\'tagname\\\')[0]');return false\" onfocus=blur()>Get element by tag-name</a><a href=go: onclick=\""+a+"\\\'x\\\'');return false\" onfocus=blur()>Position at mouse click</a><a href=go: onclick=\""+a+"\\\'p\\\'');return false\" onfocus=blur()>Last note position</a></b>"

function D_c()
{
	var a="<tt>Notes are floating containers that can hold imager, media, links, etc. These are useful for information or for user input of information. Notes can be invoked from anywhere and from any object.</tt><hr><br>"
	a+="<table id=jTR class=jW100>"
	a+="<tr><td width=1>Object</td><td><input type=text onmouseover=\"J_TT(this,'The object around which the note will align.')\" onclick=\"J_note(this,d_obj,0,3,0,-1,1)\"></td></tr>"
	a+="<tr><td>Contents</td><td><textarea onkeyup=D_x(this) class=jW100 rows=4>Any HTML here</textarea></td></tr>"
	a+="<tr><td>Width</td><td><input type=text size=2 onkeyup=D_x(this) onmouseover=\"J_TT(this,'Width of the note')\"></td></tr>"
	a+="<tr><td>Position</td><td><select onchange=D_x(this)><option value=1>Bottom<option value=2>Left<option value=3>Top<option value=4>Right</select></td></tr>"
	a+="<tr><td>Delay</td><td><input type=text size=2 onkeyup=D_x(this) onmouseover=\"J_TT(this,'Will disappear after x seconds. blank = no disappear')\"></td></tr>"
	a+="<tr><td>Padding</td><td><input type=text size=2 onkeyup=D_x(this) onmouseover=\"J_TT(this,'Padding around the inner-edge of the note')\"></td></tr>"
	a+="<tr><td>No-wrap</td><td><select onchange=D_x() onmouseover=\"J_TT(this,'Make text wrap or not')\"><option value=0>None<option value=1>Yes</select></td></tr>"
	a+="<tr><td>Note</td><td><select onchange=D_x() onmouseover=\"J_TT(this,'There are "+(j_P.j_G["jnote"]["amo"]-2)+" notes available')\">"
	i=2
	while(i<j_P.j_G["jnote"]["amo"])
	{
		a+="<option value="+i+">"+i
		i++
	}
	a+="</select></td></tr>"
	a+="<tr><td>X-offset</td><td><input type=text size=2 onkeyup=D_x(this) onmouseover=\"J_TT(this,'The distance plus extra horizontally')\"></td></tr>"
	a+="<tr><td>Y-offset</td><td><input type=text size=2 onkeyup=D_x(this) onmouseover=\"J_TT(this,'The distance plus extra vertically')\"></td></tr>"
	a+="<tr><td>Height</td><td><input type=text size=2 onkeyup=D_x(this) onmouseover=\"J_TT(this,'The height of the note. If the contents height exceeds this figure, then a scrollbar is added')\"></td></tr>"
	a+="<tr><td>Ancestor</td><td><select onchange=D_x() onmouseover=\"J_TT(this,'This controls the visibility of the note - ie: if the note was launced from another note, and the other note disappears, then this one will disappear after a short delay')\"><option value=''>"
	i=2
	while(i<j_P.j_G["jnote"]["amo"])
	{
		a+="<option value="+i+">"+i
		i++
	}
	a+="</select></td></tr>"
	a+="</table>"+D_f("date")+"</body></html>"
	document.write(a)
	J_tr()
	D_x()
}

function D_x()
{
	var i=$T("input"),t=$T("textarea"),s=$T("select"),v=[],a="",c=""
	v[0]=i[0].value
	v[1]=t[0].value.replace(/'/g,"").replace(/\n/g,"")
	v[2]=(i[1].value+"").replace(/[^0-9]/g,"")*1;v[2]=v[2]>0?v[2]:0
	v[3]=s[0].value*1;v[3]=v[3]!=1?v[3]:0
	v[4]=(i[2].value+"").replace(/[^0-9]/g,"")*1;v[4]=v[4]>0?v[4]:0
	v[5]=(i[3].value+"").replace(/[^0-9]/g,"")*1;v[5]=v[5]>0?v[5]:0
	v[6]=s[1].value*1
	v[7]=s[2].value*1;v[7]=v[7]>2?v[7]:0
	v[8]=(i[4].value+"").replace(/[^0-9]/g,"")*1;v[8]=v[8]>0?v[8]:0
	v[9]=(i[5].value+"").replace(/[^0-9]/g,"")*1;v[9]=v[9]>0?v[9]:0
	v[10]=(i[6].value+"").replace(/[^0-9]/g,"")*1;v[10]=v[10]>0?v[10]:0
	v[11]=s[3].value*1

	a+="<tt>Code</tt><hr><br>"
	a+="<b class=jO30>&lt;script&gt;</b><br>"
	a+="<b class=jO50 onmouseover=\"J_TT(this,'Optional')\">onclick=\"</b><b>"
	c+="J_note(^^"
	c+=",'"+(v[1]?v[1]:"Must have contents")+"'"
	if(v[2]||v[3]||v[4]||v[5]||v[6]||v[7]||v[8]||v[9]||v[10]||v[11])
	{
		c+=","+v[2]
		if(v[3]||v[4]||v[5]||v[6]||v[7]||v[8]||v[9]||v[10]||v[11])
		{
			c+=","+v[3]
			if(v[4]||v[5]||v[6]||v[7]||v[8]||v[9]||v[10]||v[11])
			{
				c+=","+v[4]
				if(v[5]||v[6]||v[7]||v[8]||v[9]||v[10]||v[11])
				{
					c+=","+v[5]
					if(v[6]||v[7]||v[8]||v[9]||v[10]||v[11])
					{
						c+=","+v[6]
						if(v[7]||v[8]||v[9]||v[10]||v[11])
						{
							c+=","+v[7]
							if(v[8]||v[9]||v[10]||v[11])
							{
								c+=","+v[8]
								if(v[9]||v[10]||v[11])
								{
									c+=","+v[9]
									if(v[10]||v[11])
									{
										c+=","+v[10]
										if(v[11])
											c+=","+v[11]
									}
								}
							}
						}
					}
				}
			}
		}
	}
	c+=")"
	a+=c.replace("^^",	v[0]?v[0]:0)
	a+="</b><b class=jO50>\"</b>"
	a+="<br><b class=jO30>&lt;/script&gt;</b><br><br><hr>"
	a+="<input type=button value=Test onclick=\""+c.replace("^^","this")+"\">"
	a+="<tt class=jO50><br>If the note does not line up with the object, it may be because it needs a width or no-wrap</tt>"
	out.innerHTML=a
}

function D_v(v)
{
	j_E.value=v
	D_x()
}
