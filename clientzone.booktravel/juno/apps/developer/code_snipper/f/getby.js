function D_c()
{
	document.write("<tt>You can reference object within a page and thereafter influence them.</tt><hr><br><table id=jTR class=jW100><tr><td>Get elements by id</td><td><b>$(\"id\")</b></td></tr><tr><td>Get elements by name</td><td><b>$T(\"name\")</b></td></tr><tr><td>Get elements by tag-name</td><td><b>$T(\"tag-name\")</b></td></tr></table></body></html>")
	J_tr()
	D_x()
}

function D_x()
{
	var a=""
	a+="<tt><b>Remember</b> That $N and $T return an array of found objects. If you want to reference these objects, then you must do as follows:<hr>var a=$T(\"input\")<br><b>a[0]</b> is the first object<hr>You can also reference all the objects in an array.<br><br>a=$T(\"input\")<br>var i=0<br><b>while(a[i])</b><br>{<br>&nbsp;a[i].style.background='red'<br>&nbsp;i++<br>}</tt>"
	out.innerHTML=a
}