j_P.j_Wi[j_W]["F"][0]["sup"].innerHTML="<img src="+ROOT+"/ico/set/search.gif id=jtab onmouseover=J_OP(this,100) onmouseout=J_OP(this,70) class=jO70 onclick=J_resize("+j_W+",'280,-1')>"

function D_s(){document.write("<style type=text/css>i{display:none}</style><body style='color:#999;padding:30px 40px 40px 160px;background:url("+ROOT+"/ico/set/code_screen_128.png) no-repeat 40px 40px'><tt style=width:260px><b>There are a lot of code snippets to make designing the system easier. Before creating new code, refer to this window first to avoid re-inventing the wheel</b><hr><b>NOTICE</b> "+j_P.j_G["sec"]+"</tt></body></html>")}

function J_cs_s(p)
{
c={
"Arrays":[0,"Constantly available arrays and values"]
,"Days of the week":["a",""]
,"Month names":["a",""]

,"General Operations":[0,"Scripts and operations in the parent"]
,"Get object by id":["getby",""]
,"Get object by name":["getby",""]
,"Get object by tag-name":["getby",""]
,"Ajax retrieval":["a","Get values from the database without leaving the page"]
,"Hide and Show objects":["hide_obj",""]
,"Hide and Show List-items":["hide_list","Specifically designed for collapsable lists"]
,"Get todays date":["date_get","Get todays date in different formats"]
,"Convert epoch date":["convert_epoch","Turn an epoch number date into a recognisable date string"]
,"Opacity changer":["opacity_changer","Quick object opacity fade/changer"]
,"Move list objects around":["list_mmove","Items can be moved up or down, great for lists and table rows"]
,"Checkbox text clicker":["checkbox","Clicking the text next a checkbox will toggle the checkbox (form)"]
,"Create a Cookie":["cookie_create","Create or delete a cookie"]
,"Read a Cookie":["cookie_read","Read from a cookie"]
,"List page bracketer":["list_bracket","Include bracketed links at the bottom of a search results page"]
,"Upper-case words":["strings","Make the first letter of words uppercase (strings)"]
,"Upper-case":["strings","Make the first letter of words uppercase (strings)"]
,"Lower-case":["strings","Make the first letter of words uppercase (strings)"]
,"Trim string":["strings","Remove leading or trailing spaces from a string"]
,"Icon alert":["icon_alert","Draw the users attention to an event by bouncing the referal icons at the bottom"]
,"Position getter":["position_get","Get the xy position of any object"]
,"Load a script":["a","Load new script pages into the parent"]
,"Create a desktop icon":["di","Load a new icon onto the desktop"]
,"Set file permissions":["pi",""]

,"Windows":[0,"Code that creates or manipulates windows"]
,"Window create wizard":["window_wizard","Create a new window"]
,"Window components":["window_parts","List of all the windows parts that can be refrred to or manipulated"]
,"Open a window":["a","Open a window using an existing file"]
,"Close window":["a",""]
,"Hide all windows":["a",""]
,"Minimise a window":["a",""]
,"Maximise a window":["a",""]
,"Resize a window":["a",""]
,"Reposition a window":["a",""]
,"Change window titles":["a","Change the titles found within the top panel of a window"]
,"Add footer items":["a","Add icons, images, logos, etc to the very bottom panel of a window"]
,"Change a window's tooltip":["a",""]

,"Frames":[0,"Code that influences frames within windows"]
,"Add a frame":["a",""]
,"Resize frames":["a","Open and close frames"]
,"Show or Hide a frame":["a",""]
,"Frame tabs":["a","Add a resizing tab to a frame"]

,"Page codes":[0,"Code that effects pages"]
,"Reload a page":["a",""]

,"Page operations":[0,"Scripts and operations in pages"]

,"Contextual Items":[0,"Floating items like ajax inputs, date-picker, notes, tooltips, etc"]
,"Notes":["note","A platform for presenting links, media, info, etc"]
,"Close Notes":["close_notes","Close all notes or a specific one"]
,"Tooltip":["a","Floating information tooltip"]
,"Date-Picker":["date_picker",""]
,"Colour-Picker":["a",""]
,"Close all contextual items":["close_context",""]

}

	var a=j_P.j_Wi[j_W],i,j=0
	a["F"][j_Wn]["sup"].innerHTML="<img src="+ROOT+"/ico/set/portfolio.gif id=jtab onmouseover=J_OP(this,100) onmouseout=J_OP(this,70) class=jO70 onclick=\"J_resize("+j_W+",'300,-1')\">"
	a="<body class=j_List>"
	a+="<blockquote>"
	a+="<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf1>Find</a><div style='display:block;padding:0 0 16px 0'><input type=text onkeyup=D_f(this) class=jW100 onmouseover=\"J_TT(this,'Use multiple words for narrower results')\"><span id=dsr></span></div>"
	a+="<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf0>Codes</a><div>"
	a+="<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf0>JavaScript</a><div>"
	for(i in c)
	{
		if(c[i][0])
		{
			a+="<a href=go: onclick=\"D_l('"+c[i][0]+"','"+i+"');return false\" onfocus=blur()"+(c[i][1]?" onmouseover=\"J_TT(this,'"+c[i][1]+"')\"":"")+">"+i+"</a>"
			if(p&&p==c[i][0])
			{
				chk=setTimeout("C_chk(0,\""+p+"\",\""+i+"\")",400)
				p=0
			}
		}
		else
			a+=(j?"</div>":"")+"<a href=go: onclick=\"J_U(this);return false\" class=jf0 onfocus=blur()"+(c[i][1]?" onmouseover=\"J_TT(this,'"+c[i][1]+"')\"":"")+">"+i+"</a><div>"
		j++
	}
	a+="</div></div></div></blockquote>"
	document.write(a)
}

function C_chk(n,p,i)
{
	clearTimeout(chk)
	if(j_P.j_Wi[j_W]["F"][2])
		D_l(p,i)
	else if(n>10)
	{
		if(confirm("WARNING!\nLoad failure.\nDo you want to try again?"))
			C_chk(n,p,i)
	}
	else
		chk=setTimeout("C_chk("+(n+1)+",\""+p+"\",\""+i+"\")",400)
}

function D_f(t,p)
{
	var a="",v=p?t:j_P.J_trim(t.value.toLowerCase()),i,j,y,n,s,z=0
	if(v.length>2)
	{
		s=[]
		v=v.split(" ")
		if(v[0])
		{
			n=v.length
			for(i in c)
			{
				if(c[i][0])
				{
					y=0
					for(j in v)
					{
						if(i.toLowerCase().indexOf(v[j])>-1||c[i][1].toLowerCase().indexOf(v[j])>-1)
							y++
					}
					if(y==n)
					{
						if(!p || (p&&c[i][0]!=p))
							s[c[i][0]]=[i,"<a href=go: onclick=\"D_l('"+c[i][0]+"','"+i+"');return false\" onfocus=blur()"+(c[i][1]?" onmouseover=\"J_TT(this,'"+c[i][1]+"')\"":"")+"> "+i+"</a>"]
					}
				}
			}
		}
		s.sort()
		for(i in s)
			a+=s[i][1]
	}
	if(p)
		return a?"<br><hr><tt class=as><b>Also see:</b>"+a.replace(/> /g," class=dl>&#9658; ")+"</tt>":""
	else
		t.nextSibling.innerHTML=a
}

function D_l(p,t)
{
	if(p=="di")
		J_W(ROOT+"/apps/system/icons_n_style/icons_desktop.php")
	else if(p=="pi")
		J_W(ROOT+"/apps/system/permissions.php")
	else
	{
		J_frame(1,ROOT+"/apps/developer/code_snipper/f/gen.php?p="+p+"&t="+j_P.J_ucwords(t))
		j_P.J_resize(j_W,"-2,-1,0")
	}
}
