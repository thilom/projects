function D_c()
{
	var a="<tt>The Date-picker is invoked by the onclick event and is only intended for the input tag. By default the date-picker outputs dates as YYY/MM/DD, if there is time (HH:MM) this will remain untouched in the field. The date-picker allows for optional user preferred date format output, this is in the form of a string. Click on the field below to see the options available.</tt><hr><br>"
	a+="<table id=jTR class=jW100>"
	a+="<tr><td>Position</td><td><select onchange=D_x(this)><option value=1>Bottom<option value=2>Left<option value=3>Top<option value=4>Right</select></td></tr>"
	a+="<tr><td>Delay</td><td><input type=text size=2 onkeyup=D_x(this) onmouseover=\"J_TT(this,'Will disappear after x seconds. blank = no disappear')\"></td></tr>"
	a+="<tr><td>Custom</td><td><input type=text onkeyup=D_x(this) onclick=\"J_note(this,'<tt>"
	a+="<strong>Y</strong> Full year eg: 2000<br>"
	a+="<strong>y</strong> Short year eg: 10<br>"
	a+="<strong>M</strong> Month name eg: September<br>"
	a+="<strong>m</strong> Month eg: 09<br>"
	a+="<strong>d</strong> Day eg: 08<br>"
	a+="You can add your own separators, like / or - or ,"
	a+="</tt>',320,3,5,0)\"></td></tr>"
	a+="<tr><td>Tooltip</td><td><input type=text onkeyup=D_x(this) value='YYYY/MM/DD HH:MM' onmouseover=\"J_TT(this,this.value)\"></td></tr>"
	a+="</table>"
	a+="<br><hr><tt class=jO40><b>NOTE!</b> When the value YYY/MM/DD is posted use the function: J_dateParse($value) to convert to Epoch date. Remember to include /system/parse.php"
	a+="<hr>If you need to do more things with the date event, place the function J_dp_aft(t) on your page and the date-picker will refer to it immediately after user action"
	a+="</tt>"+D_f("date")+"</body></html>"
	document.write(a)
	J_tr()
	D_x()
}

function D_x()
{
	var i=$T("input"),s=$T("select"),a=""
	a+="<tt>Code</tt><hr><br>"
	a+="<b class=jO30>&lt;input type=text</b> <b>onclick=\"j_P.J_datePicker(self"
	if(s[0].value*1>1||i[0].value*1>0||i[1].value)
	{
		a+=","+s[0].value
		if(i[0].value*1>0||i[1].value)
			a+=","+(i[0].value*1>0?i[0].value:1)
		if(i[1].value)
			a+=",0,'"+i[1].value.replace(/'/g,"")+"'"
	}
	a+=")\"</b>"
	if(i[2].value)
		a+="<b class=jO60> onmouseover=\"J_TT(this,'"+i[2].value.replace(/'/g,"")+"')\""
	a+="</b><b class=jO30>&gt;</b>"
	a+="<br><br><hr>"
	a+="<tt>Test</tt>"
	a+="<input type=text onclick=\"j_P.J_datePicker(self"
	if(s[0].value*1>1||i[0].value*1>0||i[1].value)
	{
		a+=","+s[0].value
		if(i[0].value*1>0||i[1].value)
			a+=","+(i[0].value*1>0?i[0].value:1)
		if(i[1].value)
			a+=",0,'"+i[1].value.replace(/'/g,"")+"'"
	}
	a+=")\""
	if(i[2].value)
		a+=" onmouseover=\"J_TT(this,'"+i[2].value.replace(/'/g,"")+"')\""
	a+=">"
	out.innerHTML=a
}
