j_P.j_Wi[j_W]["F"][4]["sup"].innerHTML="<img src="+ROOT+"/apps/developer/database_inspector/f/row.gif onmouseover=\"J_OP(this,60);J_TT(this,'Click to open<br>Double-click to hide',1)\" onmouseout=\"J_OP(this,30);J_T0()\" class=jO30 id=jtab onclick=J_display("+j_W+",1,-1);J_resize("+j_W+",'20,0,20,20,-1') ondblclick=J_display("+j_W+",4,-1)>"
j_db_ov=[]
document.write("<style type=text/css>#jTR{width:100%;border-style:solid;border-width:2px;border-color:#CCC;margin:0 0 20px 0}td,th{padding:8px}textarea,td input{width:100%;font-size:9pt}</style><body onload=J_tr()>")

function J_db_rr(t,v,n,c,qr)
{
	j_P.J_app(ROOT+"/system/char.js")
	var i=0,b,j,m,c,a
	if(c)
	{
		j_P.j_Wi[j_W]["db"]["cols"][t]=[]
		c=c.split("}")
		while(c[i])
		{
			o=c[i].split("|")
			j_P.j_Wi[j_W]["db"]["cols"][t][i]=[o[0],o[1],o[2],o[3]]
			i++
		}
	}
	a="<h6 style=color:#AAA>TABLE <span style=color:#777>"+t+"</span> "+n+" Row"+(n!=1?"s":"")+"<h6><br>"
	a+="<form method=post onsubmit="+(v?"'return J_db_onsub()'":"J_opaq()")+">"
	a+="<input type=hidden name=t value="+t+">"
	if(v)
	{
		v=v.split("\|")
		v.pop()
		for(i in v)
		{
			a+="<table id=jTR><tbody class=jtbody>"
			b=v[i].split("\~")
			for(j in b)
			{
				if(j_P.j_Wi[j_W]["db"]["cols"][t][j])
				{
					c=j_P.j_Wi[j_W]["db"]["cols"][t][j]
					m=" onmouseover=\"J_TT(this,'Type "+c[1]+"<br>Length "+c[2]+(b[j].length==10&&!isNaN(b[j]*1)?"<br>"+j_P.J_epochParse(b[j]):"")+"')\""
					a+="<tr><th width=1 style='padding:6px 0 0 6px'><input type=checkbox onclick=J_db_rv(this) value="+c[0]+" onmouseover=\"J_TT(this,'Update')\">"+(j==0?"<input type=hidden name="+i+"d~"+c[0]+" value=\""+b[0]+"\">":"")+"</th><th>"+c[0]+"</th><td width=90%>"+(c[1]=="BLOB"?"<textarea name="+i+"~"+c[0]+" onchange=J_db_oc(this) rows=2 onfocus='this.rows=8' onblur='this.rows=2'"+m+">"+b[j]+"</textarea>":"<input name="+i+"~"+c[0]+" onchange=J_db_oc(this) value=\""+b[j]+"\""+m+">")+"</td></tr>"
					j_db_ov[c[0]]=b[j]
				}
			}
			a+="</tbody><tr style=background:#FFDFDF><th style='padding:6px 0 0 6px'><input type=checkbox name="+i+"__del value=Delete onclick=J_db_del(this)></th><th colspan=2>Delete</th></tr>"
			a+="</table>"
		}
	}
	else
	{
		a+="<input type=hidden name=a value=1>"
		a+="<table id=jTR>"
		c=j_P.j_Wi[j_W]["db"]["cols"][t]
		for(i in c)
		{
			if(c[i])
			{
				m=" onmouseover=\"J_TT(this,'Type "+c[i][1]+"<br>Length "+c[i][2]+"')\""
				a+="<tr><th>"+c[i][0]+"</th><td width=90%>"+(c[i][1]=="BLOB"||c[i][2]>256?"<textarea name=~~"+c[i][0]+" rows=2 onfocus='this.rows=16' onblur='this.rows=2'"+m+"></textarea>":"<input name=~~"+c[i][0]+m+">")+"</td></tr>"
			}
		}
		a+="</table>"
	}
	a+="<input type=checkbox name=sh0w_sql value=1><b style=font-size:9pt> Show SQL</b><hr><tt style=color:#666><b>WARNING!</b> All values have stripslashes() then addslashes() applied. Use the 'Show SQL' option to view queries</tt><hr>"
	a+="<input type=submit value=OK>"
	a+="</form></body></html>"
	document.write(a)
	a=$T("textarea")
	i=0
	while(a[i])
	{
		a[i].value=a[i].value.replace(/<~txt/g,"<textarea").replace(/<~\/txt/g,"</textarea")
		i++
	}
	if(typeof j_P.HE_chars=="function"){}else j_P.J_app(j_P.j_G["edch"])
	if(qr)
	{
		qr=j_P.j_Wi[j_W]["F"][3]["W"]
		qr.J_opaq()
		j_P.J_resize(j_W,'20,0,20,-1,20')
		qr.location=qr.location.href
	}
	else j_P.J_resize(j_W,"20,0,20,20,-1")
}

function J_db_del(t)
{
	if(t.checked==true&&!confirm("Permanently delete row?"))t.checked=false
	t.parentNode.parentNode.style.background=t.checked==false?"#FFDFDF":"#FFB7B7"
}

function J_db_oc(t)
{
	t=t.parentNode.parentNode
	t.style.background="#DFFFFF"
	$T(t,"input")[0].checked=true
}

function J_db_rv(t)
{
	var f=$T(t.parentNode.parentNode,"td")[0].firstChild
	if(t.checked==true)
		J_db_oc(f)
	else
	{
		t.checked=false
		t.parentNode.parentNode.style.background=""
		if(confirm("Restore original value?"))
			f.value=j_db_ov[t.value]
	}
}

function J_db_onsub()
{
	J_opaq()
	a=$T("tbody")
	i=0
	while(a[i])
	{
		if(a[i].className=="jtbody")
		{
			b=$T(a[i],"tr")
			j=0
			while(b[j])
			{
				c=$T(b[j],"input")[0]
				f=$T(b[j],"td")[0].firstChild
				if(c.checked==true)
					f.value=j_P.HE_chars(f.value)
				else
					f.disabled=true
				c.disabled=true
				j++
			}
		}
		i++
	}
}