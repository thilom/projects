<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

if(count($_POST))
{
	include_once $SDR."/system/activity.php";
	$error="";
	$tp=array();
	$tp["INT"]="INT";
	$tp["STRING"]="VARCHAR";
	$tp["VARCHAR"]="VARCHAR";
	$tp["BLOB"]="TEXT";
	$tp["TEXT"]="TEXT";
	function strip($v="",$d="")
	{
		$v=trim($v);
		$v=str_replace(" ","_",$v);
		$v=strtolower($v);
		$v=preg_replace("~[^a-z0-9_]~s","",$v);
		$v=substr($v,0,64);
		return $v;
	}

	$_POST["name"]=strip($_POST["name"]);
	if($_POST["t"]=="" && $_POST["name"]!="")
	{
		$s="CREATE TABLE IF NOT EXISTS ".$_POST["name"]." (";
		$s.=$_POST["prime"]." INT NOT NULL AUTO_INCREMENT PRIMARY KEY";
		$s.=")";
		mysql_query($s);
		if(mysql_error())$error.=$s."<tt>".$s."</tt><hr>";
		else J_act("Database",3,"Create ".$_POST["name"]);
	}
	elseif($_POST["t"]!=$_POST["name"])
	{
		$s="RENAME TABLE ".$_POST["t"]." TO ".$_POST["name"];
		mysql_query($s);
		if(mysql_error())$error.=$s."<tt>".$s."</tt><hr>";
		else
		{
			$_POST["t"]=$_POST["name"];
			J_act("Database",4,"Rename ".$_POST["name"]);
		}
	}
	$_POST["t"]=$_POST["name"];

	if(!$error)
	{
		foreach($_POST as $k=>$v)
		{
			if(strpos($k,"__l__")!==false)
			{
				$n=substr($k,5);
				if(!isset($_POST["__X__".$n]))
				{
					$s="ALTER TABLE ".$_POST["t"]." DROP COLUMN ".$n;
					mysql_query($s);
					if(mysql_error())$error.=$s."<hr>";
					else J_act("Database",5,"Column ".$n);
				}
				elseif($v!=$_POST["__lO__".$n])
				{
					$r=mysql_query("SELECT ".$n." FROM ".$_POST["t"]);
					$length=mysql_field_len($r,0);
					if($k!=$v)
					{
						$v=strip($v);
						$s="ALTER TABLE ".$_POST["t"]." MODIFY ".$_POST["__C__".$n]." ".$tp[$_POST["__T__".$n]]."(".$_POST["__l__".$n].")";
						mysql_query($s);
						if(mysql_error())$error.=$s."<tt>".$s."</tt><hr>";
						else J_act("Database",4,"Modify ".$_POST["t"]." (".$_POST["__C__".$n]." to ".$tp[$_POST["__T__".$n]].")");
					}
				}
			}
			elseif(strpos($k,"__D__")!==false)
			{
				$n=substr($k,5);
				if($v!=$_POST["__DO__".$n])
				{
					$v=strip($v);
					$s="ALTER TABLE ".$_POST["t"]." ALTER COLUMN ".$n." SET DEFAULT ".$v;
					mysql_query($s);
					if(mysql_error())$error.=$s."<tt>".$s."</tt><hr>";
					else J_act("Database",4,"Column ".$n." default=".$v);
				}
			}
			elseif(strpos($k,"__C__")!==false)
			{
				$n=substr($k,5);
				if($n!=$v)
				{
					$v=strip($v);
					$s="ALTER TABLE ".$_POST["t"]." CHANGE ".$n." ".$v." ".$tp[$_POST["__T__".$n]]."(".$_POST["__l__".$n].")";
					mysql_query($s);
					if(mysql_error())$error.=$s."<tt>".$s."</tt><hr>";
					else J_act("Database",4,"Alter ".$_POST["t"]." change ".$n." ".$v." ".$tp[$_POST["__T__".$n]]." (".$_POST["__l__".$n].")");
				}
			}
		}
	}
	$n=0;
	while($n<180)
	{
		if(isset($_POST["n".$n]) && $_POST["n".$n])
		{
			$_POST["n".$n]=strip($_POST["n".$n]);
			if($_POST["n".$n])
			{
				$s="ALTER TABLE ".$_POST["t"]." ADD ".$_POST["n".$n];
				if($_POST["t".$n]=="TEXT")$s.=" TEXT";
				else
				{
					$s.=" ".$_POST["t".$n]." (";
					if($_POST["l".$n]>0)$s.=$_POST["l".$n];
					else
					{
						if($_POST["t".$n]=="INT")$s.=11;
						elseif($_POST["t".$n]=="VARCHAR")$s.=256;
					}
					$s.=")";
					if($_POST["d".$n])
					{
						$_POST["d".$n]=strip($_POST["d".$n],1);
						if($_POST["d".$n])$s.=$_POST["d".$n]?" DEFAULT '".$_POST["l".$n]."'":"";
					}
				}
				mysql_query($s);
				if(mysql_error())$errors.=$s."<tt>".$s."</tt><hr>";
				else J_act("Database",3,"Add table ".$_POST["t"]);
			}
		}
		$n++;
	}
	if($error)echo "<b>Error</b><hr>".$error;
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/developer/database_inspector/f/tab.js></script>";
if(isset($_GET["c"])||count($_POST))
{
	include $SDR."/apps/developer/database_inspector/f/cols.php";
	$c=J_cols_full(isset($_POST["t"])?$_POST["t"]:$_GET["t"]);
}
echo "<script>J_db_tv('".(isset($_POST["t"])?$_POST["t"]:(isset($_GET["t"])?$_GET["t"]:""))."',".(isset($c)?"'".$c."'":0).(count($_POST)?",1":"").")</script>";
?>