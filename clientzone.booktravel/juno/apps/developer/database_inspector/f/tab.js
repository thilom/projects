j_P.j_Wi[j_W]["F"][1]["sup"].innerHTML="<img src="+ROOT+"/apps/developer/database_inspector/f/tab.gif onmouseover=\"J_OP(this,60);J_TT(this,'Click to open<br>Double-click to hide',1)\" onmouseout=\"J_OP(this,30);J_T0()\" class=jO30 id=jtab onclick=J_resize("+j_W+",'320,-1,20,20,20') ondblclick=J_display("+j_W+",1,-1)>"
j_db_1="<b>Column name</b>Special characters and spaces are not allowed in column names"
document.write("<style type=text/css>tr{zoom:1}th{font-size:8pt}td input{font-weight:bold;width:100%}td{font-size:9pt;color:#888}</style><body onload=J_tr()>")

function J_db_tv(t,c,r)
{
	var i=0,u,a,g
	a="<tt class=jO50><b>WARNING!</b> It is strongly recommended that all tables have the first column as a primary key of type integer that auto-increments. This will make all rows easily identifyable.</tt><hr>"
	a+="<form method=post>"
	a+="<table id=jTR width=100%>"
	a+="<tr><td colspan=9><input type=hidden name=t value="+t+"><input type=text name=name value='"+(t?t:"untitled_table")+"' title='Requires value' onblur=J_db_tcn(this) onmouseover=\"J_TT(this,'<b>Table Name</b><br>Special characters and spaces are not allowed in table names')\"></td></tr>"
	a+="<tr style=opacity:0.4;filter:alpha(opacity=40)><th style=width:95%>Column</th><th>Type</th><th>Length</th><th>Default</th><th style=width:1px></th></tr>"
	if(!t)
	{
		a+="<tr><td><input type=text name=prime title='Requires value' onmouseover=J_TT(this,j_db_1) onblur=J_db_tcn(this)></td><td colspan=4 nowrap>INT NOT NULL INCREMENT PRIMARY</td></tr>"
		nR=1
	}
	else
	{
		if(c)j_P.j_Wi[j_W]["db"]["cols"][t]=[]
		g=c?c.split("}"):j_P.j_Wi[j_W]["db"]["cols"][t]
		while(g[i])
		{
			u=c?g[i].split("|"):j_P.j_Wi[j_W]["db"]["cols"][t][i]
			if(c)j_P.j_Wi[j_W]["db"]["cols"][t][i]=[u[0],u[1],u[2],u[3]]
			a+="<tr><td><input type=text name=__C__"+u[0]+" value="+u[0]+" onmouseover=J_TT(this,j_db_1) title='Requires value' onblur=J_db_tcn(this)><input type=hidden name=__T__"+u[0]+" value="+u[1]+"></td><td>"+u[1]+"</td><th><input type=hidden name=__lO__"+u[0]+" value="+u[2]+"><input type=text name=__l__"+u[0]+" value="+u[2]+" onmouseover=\"J_TT(this,'Length')\" title='Requires value' onblur=J_db_tcn(this,1) style=width:40px></th><td><input type=hidden name=__DO__"+u[0]+" value='"+u[3]+"'><input type=text name=__D__"+u[0]+" value='"+u[3]+"' onmouseover=\"J_TT(this,'Default value')\" style=width:100px></td><td><input type=checkbox name=__X__"+u[0]+" value=1 onmouseover=\"J_TT(this,'Remove')\" onclick=J_db_tcr(this) checked></td></tr>"
			i++
		}
		nR=i
	}
	a+="<tr><th colspan=9><input type=button value=&#10010; onmouseover=\"J_TT(this,'Add columns')\" onclick=J_db_tca() style=float:right><input type=submit value=OK></th></tr>"
	a+="</table></form></body></html>"
	document.write(a)
 	J_validate()
	 if(r)
	{
		j_P.j_Wi[j_W]["F"][0]["W"].J_opaq()
		j_P.j_Wi[j_W]["F"][0]["F"].src=ROOT+"/apps/developer/database_inspector/inspector.php?r=1"
	}
}

function J_db_tcn(t,n)
{
	t.value=n?t.value.replace(/[^0-9]/g,""):j_P.J_trim(t.value).toLowerCase().replace(/ /g,"_").replace(/[^a-z0-9_]/g,"")
}

function J_db_tcr(t)
{
	var p=t.parentNode.parentNode
	if(j_FF)p.style.opacity=t.checked==true?"":0.3
	else p.style.filter=t.checked==true?"":"alpha(opacity=30)"
	t.blur()
}

function J_db_tca()
{
	nR++
	var n=document.createElement("TR")
	var t=document.createElement("TD")
	t=document.createElement("TD")
	t.innerHTML="<input type=text name=n"+nR+" title='Requires value' onmouseover=J_TT(this,j_db_1) onblur=J_db_tcn(this)>"
	n.appendChild(t)
	t=document.createElement("TD")
	t.innerHTML="<select name=t"+nR+(nR?" onmouseover=\"J_TT(this,'Choose column types carefully as they cannot be changed afterward')\"><option value=INT>INT<option value=FLOAT>FLOAT<option value=VARCHAR selected>STRING<option value=TEXT>BLOB":"<option value=INT>Primary Key")+"</select>"
	n.appendChild(t)
	t=document.createElement("TD")
	t.innerHTML="<input type=text name=l"+nR+" title='Requires value' style=width:40px>"
	n.appendChild(t)
	t=document.createElement("TD")
	t.innerHTML="<input type=text name=d"+nR+" style=width:100px>"
	n.appendChild(t)
	t=document.createElement("TD")
	t.innerHTML="<input type=checkbox checked onclick=J_db_tcd(this)>"
	n.appendChild(t)
	t=$T("TBODY")[0]
	t.insertBefore(n,t.lastChild)
	j_VaL2=0
	J_validate()
	J_tr()
	window.scrollTo(0,9999)
}

function J_db_tcd(t)
{
	t=t.parentNode.parentNode
	t.parentNode.removeChild(t)
	J_tr()
}