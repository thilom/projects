<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/developer/database_inspector/f/inspector.js></script>";

if(isset($_GET["s"]))
{
	echo "<script>J_db_z()</script>";
	die();
}

elseif(isset($_GET["del"]) || isset($_GET["tnc"]))
{
	include_once $SDR."/system/activity.php";
	if(isset($_GET["del"]))
	{
		mysql_query("DROP TABLE ".$_GET["del"]);
		if(mysql_error())echo "<script>alert('WARNING\\nTable ".$_GET["del"]." could not be deleted')</script>";
		else J_act("Database",5,"Drop ".$_GET["del"]);
	}
	elseif(isset($_GET["tnc"]))
	{
		mysql_query("TRUNCATE TABLE ".$_GET["tnc"]);
		if(mysql_error())echo "<script>alert('WARNING\\nTable ".$_GET["tnc"]." could not be truncated')</script>";
		else J_act("Database",5,"Truncate ".$_GET["tnc"]);
	}
}

$t="";
$r=mysql_query("SHOW TABLES");
if(mysql_num_rows($r))
{
	while($g=mysql_fetch_array($r))
	{
		foreach($g as $k => $v)
		{
			$a=mysql_query("SELECT COUNT(*) FROM ".$v." LIMIT 1");
			if($a)
			{
				$cnt=mysql_fetch_row($a);
				if(mysql_error())echo mysql_error()."<hr>";
				$t.=$v."~".$cnt[0]."~".mysql_num_fields(mysql_query("SELECT * FROM ".$v." LIMIT 1"))."|";
				break;
			}
		}
	}
}
echo "<script>J_db_s('".$t."',".(isset($_GET["r"])?1:0).",".(isset($_GET["del"])?"'".$_GET["del"]."'":0).(isset($_GET["jsy"])?",".$_GET["jsy"]:"").")</script>";
if(!isset($_GET["r"])&&!isset($_GET["del"]))
{
	$J_title1="Database Inspector";
	$J_icon="<img src=".$ROOT."/ico/set/database.png>";
	$J_label=9;
	$J_width=1020;
	$J_framesize=320;
	$J_home=0;
	$J_frame1=$ROOT."/system/blank.htm?j_hide";
	$J_frame2=$ROOT."/system/blank.htm?j_hide";
	$J_frame3=$ROOT."/system/blank.htm?j_hide";
	$J_frame4=$ROOT."/apps/developer/database_inspector/inspector.php?s=4";
	include $SDR."/system/deploy.php";
}
echo "</body></html>";
?>