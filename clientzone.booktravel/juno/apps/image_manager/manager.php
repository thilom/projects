<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/image_manager/f/manager.js></script>";

if(isset($_GET["j_IF"]))
{
	echo "<script>J_pix_s()</script>";
	die();
}

$id=isset($_GET["id"])?$_GET["id"]:0;
echo "<script>J_in_s(".($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s" || $_SESSION["j_user"]["role"]=="b"?1:0).")</script>";

$J_title1="Image Manager";
$J_framesize=300;
$J_width=640;
$J_height=480;
$J_icon="<img src=".$ROOT."/ico/set/img.png>";
$J_tooltip="Manage Establishment Images";
$J_label=21;
$J_home=1;
$J_nomax=1;
if($id)
	$J_frame1=$ROOT."/apps/image_manager/f/gallery.php?j_hide=1&id=".$id;
else
	$J_frame1=$ROOT."/apps/image_manager/manager.php";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>