<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

$id=isset($_GET["id"])?$_GET["id"]:0;
$locw="/home/booktravel/public_html/res_images/";
$loch=$_SERVER["DOCUMENT_ROOT"]."/hires_images/";

function strip($v="",$p=0)
{
	if($v)
	{
		$v=str_replace("\r","",$v);
		$v=str_replace("\n"," ",$v);
		$v=str_replace("  "," ",$v);
		$v=strip_tags($v);
		$v=preg_replace('~[^a-zA-Z0-9 !@#%&:;,\'\(\)\*\$\?\.\-]~',"",$v);
		$v=trim($v);
		if($p)
			$v=addslashes($v);
		else
			$v=stripslashes($v);
	}
	return $v;
}

if(count($_POST))
{
	include $SDR."/system/activity.php";

	$g=mysql_fetch_array(mysql_query("SELECT establishment_name FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));
	$est=$g["establishment_name"];

	$prime=-1;
	if(isset($_POST["primary_image"]))
	{
		$prime=$_POST["primary_image"];
		unset($_POST["primary_image"]);
	}

	$di=$EPOCH;

	foreach($_POST as $k => $v)
	{
		if(strpos($k,"image_id_")!==false)
		{
			$d=substr($k,9);
			$iid=$v;
			if(isset($_POST["delete_".$d]))
			{
				$s="DELETE FROM nse_establishment_images WHERE image_id=".$iid;
				mysql_query($s);
				if(isset($_POST["thumb_".$d])&&$_POST["thumb_".$d])
					unlink($locw.$_POST["thumb_".$d]);
				if(isset($_POST["web_".$d])&&$_POST["web_".$d])
					unlink($locw.$_POST["web_".$d]);
				if(isset($_POST["hires_".$d])&&$_POST["hires_".$d])
					unlink($loch.$_POST["hires_".$d]);
				if(isset($_POST["orig_".$d])&&$_POST["orig_".$d])
					unlink($loch.$_POST["orig_".$d]);
				J_act("IMG MANAGER",5,"Pic",$iid,$id);
			}
			else
			{
				$thumb=isset($_POST["thumb_".$d])?$_POST["thumb_".$d]:"";
				$web=isset($_POST["web_".$d])?$_POST["web_".$d]:"";
				$hires=isset($_POST["hires_".$d])?$_POST["hires_".$d]:"";
				$original=isset($_POST["orig_".$d])?$_POST["orig_".$d]:"";
				if(isset($_FILES["up_".$d]) && $_FILES["up_".$d]["size"])
				{
					$upload=1;
					$iFile = '/home/booktravel/public_html/tmp/' . $_FILES["up_".$d]["name"];
					move_uploaded_file($_FILES["up_".$d]["tmp_name"], '/home/essentialtravel/public_html/tmp/' . $_FILES["up_".$d]["name"]);
					$s=getimagesize($iFile);
					// thumb
					if(!$thumb)
						$thumb="TN_".$id."_".$di.".jpg";
					exec("/usr/bin/convert ".$iFile." -colorspace RGB -geometry '90x90>' -sharpen 0x1.0 -quality 70 ".$locw.$thumb);
					// web
					if(!$web)
						$web=$id."_".$di.".jpg";
					exec("/usr/bin/convert ".$iFile." -colorspace RGB -geometry '250x250>' -sharpen 0x1.0 -quality 80 ".$locw.$web);
					// hi-rez
					if($s[0]*$s[1]>300*250) // 50x40mm@150ppi
					{
						if(!$hires)
							$hires=$id."_".$di."_H600.jpg";
						if(isset($s["channels"]) && $s["channels"]==4)
							exec("/usr/bin/convert ".$iFile." -scale '1000x1000>' -sharpen 0x1.0 -quality 100 ".$loch.$hires);
						else
						{
							$c="/usr/bin/convert -scale '1000x1000>' -sharpen 0x1.0 -profile ".$SDR."/utility/color_profiles/AdobeRGB1998.icc ".$_FILES["up_".$d]["tmp_name"]." -profile ".$SDR."/utility/color_profiles/USWebCoatedSWOP.icc -quality 100 ".$loch.$hires;
							exec($c,$output);
							if(count($output))
							{
								foreach($output as $k => $v)
									$v."<hr>";
							}
						}
						//if($s[0]*$s[1]>660*470) // ORIGINAL A6@150ppi
						{
							if(!$original)
								$original=$id."_".$di."_H1400.jpg";
							exec("/usr/bin/convert ".$iFile." -scale '3000x3000>' -quality 100 ".$loch.$original);
						}
					}
					elseif(isset($_POST["hires_".$d]) && $_POST["hires_".$d])
					{
						unlink($loch.$hires);
						unlink($loch.$original);
						$hires="";
						$original="";
					}
					$di++;
					unlink($iFile);
				}
				if($iid)
				{
					$s="UPDATE nse_establishment_images SET
					primary_image='".($d==$prime && isset($_POST["no_web_".$d])?1:"")."'
					,image_title='".strip($_POST["image_title_".$d],1)."'
					,image_description='".strip($_POST["image_description_".$d],1)."'
					,thumb_name='".$thumb."'
					,image_name='".$web."'
					,hi_res='".$hires."'
					,original='".$original."'
					,no_web=".(isset($_POST["no_web_".$d])?0:1);
					if(isset($upload))
						$s.=",user=".$_SESSION["j_user"]["id"].",date=".$EPOCH;
					$s.=" WHERE image_id=".$iid;
					mysql_query($s);
					if(mysql_error())echo mysql_error()."<tt>".$s."</tt><hr>";
					J_act("IMG MANAGER",4,"Pic",$iid,$id);
				}
				elseif($thumb || $web || $hires || $original || $_POST["image_title_".$d] || $_POST["image_description_".$d])
				{
					$s="INSERT INTO nse_establishment_images (
					establishment_code
					,date
					,primary_image
					,image_title
					,image_description
					,thumb_name
					,image_name
					,hi_res
					,original
					,no_web
					,user
					) VALUES (
					'".$id."'
					,".$EPOCH."
					,'".($d==$prime?1:"")."'
					,'".strip($_POST["image_title_".$d],1)."'
					,'".strip($_POST["image_description_".$d],1)."'
					,'".$thumb."'
					,'".$web."'
					,'".$hires."'
					,'".$original."'
					,".(isset($_POST["no_web_".$d])?0:1)."
					,".$_SESSION["j_user"]["id"]."
					)";
					mysql_query($s);
					if(mysql_error())echo mysql_error()."<tt>".$s."</tt><hr>";
					$iid=J_ID("nse_establishment_images","image_id");
					J_act("IMG MANAGER",3,"Pic",$iid,$id);
				}
			}
		}
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/image_manager/f/gallery.js></script>";

$img="";

$q="SELECT * FROM nse_establishment_images WHERE establishment_code='".$id."' ORDER BY primary_image DESC,image_title";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	include $SDR."/system/dir.php";
	while($g=mysql_fetch_array($q))
	{
		$s0=($g["thumb_name"]&&file_exists($locw.$g["thumb_name"])?getimagesize($locw.$g["thumb_name"]):array("",""));
		$s1=($g["image_name"]&&file_exists($locw.$g["image_name"])?getimagesize($locw.$g["image_name"]):array("",""));
		$s2=($g["hi_res"]&&file_exists($loch.$g["hi_res"])?getimagesize($loch.$g["hi_res"]):array("",""));
		$s3=($g["original"]&&file_exists($loch.$g["original"])?getimagesize($loch.$g["original"]):array("",""));

		$m0=($g["thumb_name"]&&file_exists($locw.$g["thumb_name"])?filesize($locw.$g["thumb_name"]):"");
		$m1=($g["image_name"]&&file_exists($locw.$g["image_name"])?filesize($locw.$g["image_name"]):"");
		$m2=($g["hi_res"]&&file_exists($loch.$g["hi_res"])?filesize($loch.$g["hi_res"]):"");
		$m3=($g["original"]&&file_exists($loch.$g["original"])?filesize($loch.$g["original"]):"");

		$img.=$g["image_id"]."~";
		$img.=$g["primary_image"]."~";
		$img.=$g["thumb_name"]."~";
		$img.=$s0[0]."~";
		$img.=$s0[1]."~";
		$img.=J_size($m0)."~"; // 5
		$img.=$g["image_name"]."~";
		$img.=$s1[0]."~";
		$img.=$s1[1]."~";
		$img.=J_size($m1)."~";
		$img.=$g["hi_res"]."~"; // 10
		$img.=$s2[0]."~";
		$img.=$s2[1]."~";
		$img.=J_size($m2)."~";
		$img.=$g["original"]."~";
		$img.=$s3[0]."~"; // 15
		$img.=$s3[1]."~";
		$img.=J_size($m3)."~";
		$img.=strip($g["image_title"])."~";
		$img.=strip($g["image_description"])."~";
		$img.=($g["date"]?date("d/m/Y",$g["date"]):"")."~"; // 20
		$img.=($g["user"]?J_Value("firstname~lastname","nse_user","user_id",$g["user"]):"")."~";
		$img.=($g["no_web"]?1:"");
		$img.="|";
	}
	$img=str_replace(array("\n","\r"),"",$img);
}

$g=mysql_fetch_array(mysql_query("SELECT establishment_name FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));
$est=$g["establishment_name"];

echo "<script>A_gal('$id',\"$est\",\"$img\"";
echo isset($_GET['est_man'])?",1":'';
echo ")</script>";
$_SESSION["juno"]["est_img"][$id]=1;
?>