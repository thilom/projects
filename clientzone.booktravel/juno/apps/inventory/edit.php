<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

$id=isset($_GET["id"])?$_GET["id"]:0;

if(count($_POST))
{
	include $SDR."/system/parse.php";
	$err="";
	$j_insert=0;
	$c=array(
	"aa_entity"=>array(),
	"j_in_category"=>array("int"=>1),
	"j_in_name"=>array("tagsall"=>1,"ucfirst"=>1),
	"j_in_code"=>array("keyboard"=>1,"tagsall"=>1),
	"j_in_description"=>array("keyboard"=>1,"tagsall"=>1),
	"j_in_price"=>array("float"=>4),
	"j_in_max_room"=>array("int"=>1),
	"j_in_commission_percent"=>array("float"=>1),
	"j_in_commission_fixed"=>array("float"=>1)
	);
	$s="";
	$i="";
	foreach($_POST as $k =>$v)
	{
		if(strpos($k,"j_in_")!==false || $k=="aa_entity")
		{
			if($id)
				$s.=($s?",":"").$k."='".J_parseValue($v,$c[$k])."'";
			else
			{
				$i.=($i?",":"").$k;
				$s.=($s?",":"")."'".J_parseValue($v,$c[$k])."'";
			}
		}
	}
	if($id)
		mysql_query("UPDATE nse_inventory SET ".$s." WHERE j_in_id=".$id);
	else
	{
		mysql_query("INSERT INTO nse_inventory (".$i.",j_in_created) VALUES (".$s.",".$EPOCH.")");
		//echo $i."<hr>".$s;
		if(mysql_error())$err=mysql_error()."<tt>".$s."</tt><hr>";
		$id=J_ID("nse_inventory","j_in_id");
    $j_insert=1;
	}
  include_once $SDR."/system/activity.php";
  J_act("Inventory",(isset($j_insert)?3:4),$v,"IN".$id);
	echo "<script>window.parent.J_WX(self)</script>";
	die();
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/inventory/edit/edit.js></script>";

echo "<form method=post>";

if($id)
	$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_inventory WHERE j_in_id=".$id." LIMIT 1"));
else
	$g=array("aa_entity"=>"","j_in_category"=>0,"j_in_name"=>"Untitled","j_in_code"=>"","j_in_description"=>"","j_in_price"=>"","j_in_max_room"=>0,"j_in_commission_percent"=>0,"j_in_commission_fixed"=>0);

$g["j_in_category_name"]="";
if($g["j_in_category"])
	$g["j_in_category_name"]=J_Value("j_icn_name","nse_inventory_category_name","j_icn_id",$g["j_in_category"]);

echo "<table id=jTR width=100%><tbody id=jb>";

echo "<tr><th>Company</th><td><select name=aa_entity class=jW100>";
$q="SELECT establishment_code,establishment_name FROM nse_establishment WHERE aa_entity='Y' ORDER BY establishment_name LIMIT 5";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($ge=mysql_fetch_array($q))
		echo "<option value=".$ge["establishment_code"].($g["aa_entity"]==$ge["establishment_code"]?" selected":"").">".$ge["establishment_name"];
}
echo "</select></td></tr>";

echo "<tr><th>Category</th><td><select name=j_in_category class=jW100>";
$q = "SELECT * FROM nse_inventory_category_name ORDER BY j_icn_name";
$q = mysql_query($q);
if (mysql_num_rows($q))
{
	while ($ge = mysql_fetch_array($q))
		echo "<option value=".$ge["j_icn_id"].($ge["j_icn_id"]==$g["j_in_category"]?" selected":"").">".$ge["j_icn_name"];
}
echo "</td></tr>";

echo "<tr><th>Name</th><td><input type=text name=j_in_name value=\"".$g["j_in_name"]."\" class=jW100 style=font-weight:bold title='Requires value'></td></tr>";

echo "<tr><th>Code</th><td><input type=text name=j_in_code value=\"".$g["j_in_code"]."\" class=jW100></td></tr>";

echo "<tr><th>Info</th><td><textarea name=j_in_description rows=6 class=jW100>".str_replace("<br>","\n",$g["j_in_description"])."</textarea></td></tr>";

echo "<tr><th>Price</th><td><input type=text name=j_in_price value=\"".$g["j_in_price"]."\" onmouseover=\"J_TT(this,'<b>Excluding VAT</b><br>Number format: 1000.00')\" class=jW100></td></tr>";

echo "<tr><th>Rooms</th><td><input type=text name=j_in_max_room value=\"".$g["j_in_max_room"]."\" onmouseover=\"J_TT(this,'Number format: 0-9')\" size=6></td></tr>";

echo "<tr><th>Commission</th><td><input type=text name=j_in_commission_percent value=\"".$g["j_in_commission_percent"]."\" onkeyup=dC(this) onmouseover=\"J_TT(this,'Percentage')\" size=6> %</td></tr>";
echo "<tr><th>Commission</th><td><input type=text name=j_in_commission_fixed value=\"".$g["j_in_commission_fixed"]."\" onmouseover=\"J_TT(this,'Fixed amount')\" size=6> Fixed</td></tr>";

echo "</tbody></table>";
echo "<br><input type=submit value=OK>";
echo "</form>";

$J_title1="Add Item";
$J_title1=($id?"Edit":"Add")." Item";
$J_title3=($id?"<b style=font-size:14px>".$g["j_in_name"]."</b>".($id&&$g["j_in_code"]?"<br>Code: ".$g["j_in_code"]:""):"")." | Id: ".$id;
$J_icon="<img src=".$ROOT."/ico/set/inventory_edit.png>";
$J_nostart=1;
$J_nomax=1;
$J_label=15;
$J_height=420;
$J_width=560;
$J_home=0;
include $SDR."/system/deploy.php";
echo "<script>J_in_foot(".$id.(count($_POST)?",1".($j_insert?",1,\"".$g["j_in_name"]."\"":""):"").")</script>";
echo "</body></html>";
?>