<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/dir.php";
include_once $SDR."/system/get.php";
include_once $SDR."/apps/accounts/invoices/default.php";
include_once $SDR."/apps/accounts/invoices/get.php";

$id=$_GET["id"];
$v="";
$head="";
$fromCo="";
$toCo="";
$fromPer="";
$fromAss="";
$fromRep="";
$fromRep2="";
$toPer="";
$fromCnt="";
$toCnt="";
$condition="";
$bank="";
$foot="";
$lock="";
$edit=0;

$q=mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$id." LIMIT 1");
if(mysql_num_rows($q))
	$in=mysql_fetch_array($q);
else
{
	echo "<html><link rel=stylesheet href=".$ROOT."/style/set/page.css><body><tt><var><b>WARNING!</b></var> This item could not be found.</tt></body></html>";
	die();
}

if(!$in["j_inv_closed"] && $in["j_inv_date"]<$time_lock)
{
	mysql_query("UPDATE nse_invoice SET j_inv_closed=".$EPOCH." WHERE j_inv_id=".$id);
	$in["j_inv_closed"]=$EPOCH;
}
else
{
	$y=$in["j_inv_date"];
	$d=date("d",$y);
	$m=date("m",$y);
	$y=date("Y",$y);
	$lock=(mktime(date("H"),date("i"),date("s"),$m,$d+$invoice_default_lock,$y)-$EPOCH)/60/60/24;
}

$v.=$in["j_inv_head"]."~";
$v.=date("d/m/Y H:i",$in["j_inv_date"])."~";
$v.=$in["j_inv_order"]."~";
$v.=$in["j_inv_reference"]."~";
$v.=$in["j_inv_number"]."~";
$v.=$in["j_inv_increment_id"]."~"; // 5
$v.=$in["j_inv_from_company"]."~";
$v.=$in["j_inv_to_company"]."~";
$v.=$in["j_inv_from_person"]."~";
$v.=$in["j_inv_from_assessor"]."~";
$v.=$in["j_inv_from_rep"]."~"; // 10
$v.=$in["j_inv_from_rep2"]."~";
$v.=$in["j_inv_to_person"]."~";
$v.=$in["j_inv_from_contact"]."~";
$v.=$in["j_inv_to_contact"]."~";
$v.=$in["j_inv_vat"]."~"; // 15
$v.=$in["j_inv_info"]."~";
$v.=$in["j_inv_comment"]."~";
$v.=$in["j_inv_condition"]."~";
$v.=($in["j_inv_bank"]?$in["j_inv_bank"]:"")."~";
$v.=($in["j_inv_foot"]?$in["j_inv_foot"]:"")."~"; // 20
$v.="~";
$v.=$in["j_inv_currency"]."~";
$v.=$in["j_inv_total_price"]."~";
$v.=$in["j_inv_total_discount"]."~";
$v.=$in["j_inv_total_vat"]."~"; // 25
$v.=$in["j_inv_total"]."~";
$v.=$in["j_inv_paid"]."~";
$v.=$in["j_inv_type"]."~";
$v.=$in["j_inv_order_id"]."~";
$v.=$in["j_inv_invoice_id"]."~"; // 30
$v.="~";

$fromCo=($in["j_inv_from_company"]?J_inv_getto($in["j_inv_from_company"]):$invoice_default_company_info);
$toCo=($in["j_inv_to_company"]?J_inv_getto($in["j_inv_to_company"]):$invoice_default_company_info);
$fromPer=J_inv_getpeo($in["j_inv_from_person"]);
$fromAss=J_inv_getpeo($in["j_inv_from_person"]);
$fromRep=J_inv_getpeo($in["j_inv_from_rep"]);
$fromRep2=J_inv_getpeo($in["j_inv_from_rep"]);
$fromCnt=J_inv_getpeo($in["j_inv_from_contact"]);
$toPer=J_inv_getpeo($in["j_inv_to_person"]);
$toCnt=J_inv_getpeo($in["j_inv_to_contact"]);
$head=J_inv_getele($in["j_inv_head"]);
$condition=J_inv_getele($in["j_inv_condition"]);
$bank=J_inv_getele($in["j_inv_bank"]);
$foot=J_inv_getele($in["j_inv_foot"]);

// order gets shared contents from the invoice
$i=$in["j_inv_invoice_id"]?$in["j_inv_invoice_id"]:$id;
$r=mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=".$i." ORDER BY j_invit_date,j_invit_name");
if(mysql_num_rows($r))
{
	while($i=mysql_fetch_array($r))
	{
		$v.=$i["j_invit_id"]."`";
		$v.=($i["j_invit_inventory"]?$i["j_invit_inventory"]:"")."`";
		$v.=($i["j_invit_date"]?date("d/m/Y",$i["j_invit_date"]):"")."`";
		$v.=$i["j_invit_name"]."`";
		$v.=$i["j_invit_description"]."`";
		$v.=$i["j_invit_code"]."`"; // 5
		$v.=$i["j_invit_quantity"]."`";
		$v.=$i["j_invit_price"]."`";
		$v.=$i["j_invit_discount"]."`";
		$v.=$i["j_invit_vat"]."`";
		$v.=$i["j_invit_total"]."`"; // 10
		$pp=0;
		$rp=mysql_query("SELECT j_invp_paid FROM nse_invoice_item_paid WHERE j_invp_invoice_item=".$i["j_invit_id"]);
		if(mysql_num_rows($rp))
		{
			while($p=mysql_fetch_array($rp))
				$pp+=$p["j_invp_paid"];
		}
		$v.=($pp?$pp:"");
		$v.="|";
	}
}
$v=stripit($v);

echo "<html>";
echo "<title>AA TRAVEL GUIDES ".strtoupper($types[$in["j_inv_type"]])." : ".($in["j_inv_type"]==9?$in["j_inv_order"]:$in["j_inv_number"]).($in["j_inv_date"]?" ".date("D d/m/Y",$in["j_inv_date"]):"")."</title>";
echo "<script>ROOT='".$ROOT."'</script>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<link rel=stylesheet href=".$ROOT."/apps/accounts/invoices/style.css>";
echo "<script src=".$ROOT."/apps/accounts/invoices/view.js></script>";
echo "<body>";
echo "<script>";
echo "inv_id=".$id;
echo ";J_inv_view(";
echo "0,0,0";
echo ",\"".$v."\"";
echo ",".($in["j_inv_cancel_date"]?"\"".date("d/m/Y",$in["j_inv_cancel_date"])."\"":0);
echo ",".($in["j_inv_cancel_user"]?"\"".J_Value("","people","",$in["j_inv_cancel_user"])."\"":0);
echo ",".($fromCo?"\"".$fromCo."\"":0);
echo ",".($toCo?"\"".$toCo."\"":0);
echo ",".($fromPer?"\"".$fromPer."\"":0);
echo ",".($fromAss?"\"".$fromAss."\"":0);
echo ",".($fromRep?"\"".$fromRep."\"":0);
echo ",".($fromRep2?"\"".$fromRep2."\"":0);
echo ",".($toPer?"\"".$toPer."\"":0);
echo ",".($fromCnt?"\"".$fromCnt."\"":0);
echo ",".($toCnt?"\"".$toCnt."\"":0);
echo ",".($head?"\"".$head."\"":0);
echo ",".($condition?"\"".$condition."\"":0);
echo ",".($bank?"\"".$bank."\"":0);
echo ",".($foot?"\"".$foot."\"":0);
echo ")</script>";

$_SESSION["juno"]["accounts"]=array();
?>