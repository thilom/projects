<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/get.php";
include_once $SDR."/apps/accounts/invoices/get.php";

$co=isset($eid)?$eid:(isset($co)?$co:(isset($_GET['co'])?$_GET['co']:0));

if(!isset($Aowner))
{
	$Aowner=array("owner"=>1,"owners"=>1,"partner"=>2,"partners"=>2,"partners"=>2,"ceo"=>2,"trustee"=>3,"trustees"=>3);
	$Amanager=array("ownermanager"=>1,"ownersmanagers"=>1,"gm"=>2,"generalmanager"=>2,"operationsmanager"=>3,"manager"=>4,"managers"=>4,"manageress"=>4,"director"=>5,"directors"=>5);
	$Aaccounts=array("accountant"=>1,"account"=>1,"accounts"=>1,"owneraccounts"=>1,"bookkeeper"=>2,"admin"=>3,"administrator"=>3,"administration"=>3);
}

$Rowner=99;
$Rmanager=99;
$Raccounts=99;

$owner=0;
$manager=0;
$accounts=0;
$anybody=0;
$per=0;
$acc=0;

$statement = "SELECT u.user_id,e.designation FROM nse_user_establishments AS e JOIN nse_user AS u ON e.user_id=u.user_id WHERE e.establishment_code=?";
// echo $statement;
$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
// echo mysqli_error($GLOBALS['dbCon']);
$sql_contacts->bind_param('s',$co);
$sql_contacts->execute();
$sql_contacts->store_result();
$sql_contacts->bind_result($user_id,$desig);
while($sql_contacts->fetch())
{
	$desig=preg_replace('~[^a-z]~',"",strtolower($desig));
	if(isset($Aowner[$desig]) && $Aowner[$desig]<$Rowner)$owner=$user_id;
	elseif(isset($Amanager[$desig]) && $Amanager[$desig]<$Rmanager)$manager=$user_id;
	elseif(isset($Aaccounts[$desig]) && $Aaccounts[$desig]<$Raccounts)$accounts=$user_id;
	elseif(!$anybody)$anybody=$user_id;
}
$sql_contacts->free_result();
$sql_contacts->close();

if($manager)$per=$manager;
elseif($owner)$per=$owner;
elseif($accounts)$per=$accounts;
elseif($anybody)$per=$anybody;

if($accounts)$acc=$acc;
elseif(!$per && $manager)$per=$manager;
elseif(!$per && $owner)$acc=$owner;
elseif(!$per && $anybody)$acc=$anybody;

if($per)$per=J_inv_getpeo($per);
if($acc)$acc=J_inv_getpeo($acc);

if(!isset($no_js))
	echo "<script>window.parent.J_est_people(".($per?"\"".$per."\"":0).",".($acc?"\"".$acc."\"":0).")</script>";
?>