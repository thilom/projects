onc=[]

function A_v(d,dn,ds,de,o,con1,con2,con3,ass1,ass2,rep1,rep2,note,x)
{
	var i,u,v,j,tt=0,tp=0,a=""
	a+="<style type=text/css>.jdbB,.jdbR,.jdbG{font-weight:bold;text-align:right;width:20;white-space:nowrap}.jdbW{width:90%}</style>"
	a+="<body style=margin:20 class=jSCH>"
	a+="<table id=jTR class=jW100>"
	a+="<tr class=jhed><th>Date</th><th>Item</th><th>Total</th><th>Owed</th><th>Paid</th><th>Pay&nbsp;Date</th></tr>"

	if(o)
	{
		invtype=["Unknown","Pro-forma","Invoice","Credit Note","Debit Note","","","","","Order"]
		o=o.split("|")
		i=0
		while(o[i])
		{
			u=o[i].split("~")
			if(u[0]=="b")
				a+="<tr class=jhed><th colspan=9>"+u[1]+"</th></tr>"
			else
			{
				onc[i]="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_view.php?id="+u[0]+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Open invoice')\"><img src="+ROOT+"/ico/set/spreadsheet.png height=32 width=32></a><a href=go: onclick=\"J_W(ROOT+'/apps/accounts/payments/view.php?id="+u[0]+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'View payment details')\"><img src="+ROOT+"/ico/set/accounts_pay.png height=32 width=32></a><a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/notes.php?id="+u[0]+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'View invoice notes')\"><img src="+ROOT+"/ico/set/note.png height=32 width=32></a>"
				u[4]=u[4]*1
				u[5]=u[5]*1
				a+="<tr onclick=\"J_V('i"+i+"');J_note('x',onc["+i+"],0,3,0,2,1)\" style=cursor:pointer><td>"+u[2]+"</td><td class=jdbW><b>"+invtype[u[1]]+" "+u[3]+"</b></td><td class=jdbB>"+u[4].toFixed(2)+"</td><td class=jdbR>"+(u[4]-u[5]).toFixed(2)+"</td><td class=jdbG>"+(0-u[5]).toFixed(2)+"</td><td>"+u[7]+"</td></tr>"
				tt+=u[4]
				tp+=u[5]
				a+="<tbody id=i"+i+" onclick=\"J_note('x',onc["+i+"],0,3,0,2,1)\" class=ji style=cursor:pointer>"
				if(u[6])
				{
					u=u[6].split("^")
					j=0
					while(u[j])
					{
						v=u[j].split("`")
						v[1]=v[1]?v[1]*1:0
						v[2]=v[2]?v[2]*1:0
						a+="<tr style="+(j_FF?"opacity:0.5":"filter:alpha(opacity=50)")+"><td></td><td><b>"+v[0]+"</b></td><td class=jdbB>"+v[1].toFixed(2)+"</td><td class=jdbR>"+(v[1]-v[2]).toFixed(2)+"</td><td class=jdbG>"+(0-v[2]).toFixed(2)+"</td><td class=jdbY"+(v[4]?" onmouseover=\"J_TT(this,'Created: "+v[4]+"')\"":"")+">"+(v[3]?v[3]:"")+"</td></tr>"
						j++
					}
				}
				else
					a+="<tr class=jhed><th colspan=9>No invoice items found</th></tr>"
				a+="</tbody>"
			}
			i++
		}
		a+="<tr class=jhed><th colspan=9 class=jdbC>Totals</th></tr>"
		a+="<tr style=background:#FFF><td colspan=2></td><td class=jdbB>"+tt.toFixed(2)+"</td><td class=jdbR>"+(tt-tp).toFixed(2)+"</td><td class=jdbG>"+tp.toFixed(2)+"</td><td></td></tr>"
	}
	else
		a+="<tr><td><tt>No invoices since "+ds+"</td></tr>"
	a+="</table>"
	a+="<br><tt class=jO40>"+j_P.j_G["sec"]+"</tt>"
	document.write(a)

	J_tr()
	a=""
	if(note)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/notes/view.php?conotes=1&eid="+d+"&app=2');return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/note.png></a>"
	a+="<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/sections/detail.php?"+(x?"":"view=1&")+"id="+d+"');return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/gohome-64.png></a>"
	if(con1)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?&id="+con1+"');return false\" onmouseover=\"J_TT(this,'<b>Establishment contact</b><br>"+con2+"<br>"+con3+"')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/phone1.png></a>"
	if(ass1)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?&id="+ass1+"');return false\" onmouseover=\"J_TT(this,'<b>Assessors details</b><br>"+ass2+"')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/user_male.png></a>"
	if(rep1)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?&id="+rep1+"');return false\" onmouseover=\"J_TT(this,'<b>Reps details</b><br>"+rep2+"')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/user_female.png></a>"
	J_footer(a)
}