<?php
function increment($id,$temp=0)
{
	$n=0;
	$q=mysql_query("SELECT j_invinc_number FROM nse_invoice_increment WHERE j_invinc_id=".$id." LIMIT 1");
	if(mysql_num_rows($q))
	{
		$g=mysql_fetch_array($q);
		$n=$g["j_invinc_number"]+1;
		if(!$temp)
			mysql_query("UPDATE nse_invoice_increment SET j_invinc_number=".$n." WHERE j_invinc_id=".$id);
	}
	return $n;
}

function J_invoice_increment($id=0,$co=0,$temp=0)
{
	global $URL;
	$i="";
	$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_increment WHERE ".($id?"j_invinc_id=".$id:("j_invinc_default=1".($co?" AND j_invinc_company='".$co."'":"")))." LIMIT 1"));
	$id=$g["j_invinc_id"];
	$c=explode("^",$g["j_invinc_components"]);
	$num_done=0;
	if(count($c))
	{
		foreach($c as $k => $v)
		{
			if($v=="day")$i.=date("d",$EPOCH);
			elseif($v=="month")$i.=date("m",$EPOCH);
			elseif($v=="abbr year")$i.=date("y",$EPOCH);
			elseif($v=="full year")$i.=date("Y",$EPOCH);
			else
			{
				$m=0;
				// ensure the number is not drawn repeatedly
				if(!$num_done && ($v=="count" || strpos($v,"pad")!==false))
				{
					$m=increment($id,$temp);
					$num_done=1;
				}
				if($v=="count")
					$i+=$m;
				elseif(strpos($v,"pad")!==false)
					$i.=J_Invoice_inc_pad($m,substr($v,3,1));
				else
					$i.=$v;
			}
		}
	}
	return $i;
}

function J_Invoice_inc_pad($m,$p)
{
	if(!$m)$m=0;
	if($p==3)$m="000".($m>"999"?0:$m);
	elseif($p==4)$m="0000".($m>"9999"?0:$m);
	elseif($p==5)$m="00000".($m>"99999"?0:$m);
	elseif($p==6)$m="000000".($m>"999999"?0:$m);
	elseif($p==7)$m="0000000".($m>"9999999"?0:$m);
	elseif($p==8)$m="00000000".($m>"99999999"?0:$m);
	elseif($p==9)$m="000000000".($m>"999999999"?0:$m);
	$m=strrev(substr(strrev($m),0,$p));
	return $m;
}

if(isset($_GET["inc"]))
	echo "<script>window.parent.J_inv_inc(0,".$_GET["inc"].",\"".J_invoice_increment($_GET["inc"])."\")</script>";
?>