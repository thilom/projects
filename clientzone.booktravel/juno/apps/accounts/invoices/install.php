<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$s="CREATE TABLE IF NOT EXISTS nse_invoice
(
j_inv_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
,j_inv_order VARCHAR(64)
,j_inv_reference VARCHAR(64)
,j_inv_number VARCHAR(64)
,j_inv_increment_id MEDIUMINT(4) DEFAULT 0
,j_inv_type TINYINT(1) DEFAULT 1
,j_inv_date INT(11) DEFAULT 0
,j_inv_from_company INT(11) DEFAULT 0
,j_inv_from_person INT(11) DEFAULT 0
,j_inv_from_rep INT(11) DEFAULT 0
,j_inv_from_contact INT(11) DEFAULT 0
,j_inv_to_company INT(11) DEFAULT 0
,j_inv_to_person INT(11) DEFAULT 0
,j_inv_to_contact INT(11) DEFAULT 0
,j_inv_closed INT(11) DEFAULT 0
,j_inv_vat FLOAT(11) DEFAULT 0
,j_inv_currency VARCHAR(4)
,j_inv_head INT(11) DEFAULT 0
,j_inv_info TEXT
,j_inv_comment TEXT
,j_inv_condition MEDIUMINT(3) DEFAULT 0
,j_inv_bank MEDIUMINT(3) DEFAULT 0
,j_inv_foot MEDIUMINT(3) DEFAULT 0
,j_inv_total_price FLOAT(11) DEFAULT 0
,j_inv_total_discount FLOAT(11) DEFAULT 0
,j_inv_total_vat FLOAT(11) DEFAULT 0
,j_inv_total FLOAT(11) DEFAULT 0
,j_inv_paid FLOAT(11) DEFAULT 0
,j_inv_temp INT(11) DEFAULT 0
,j_inv_user INT(11) DEFAULT 0
,j_inv_creator INT(11) DEFAULT 0
,j_inv_after TEXT
)";
mysql_query($s);
if(mysql_error())echo mysql_error()."<hr>";

$s="CREATE TABLE IF NOT EXISTS nse_invoice_item
(
j_invit_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
,j_invit_invoice INT(11) DEFAULT 0
,j_invit_inventory INT(11) DEFAULT 0
,j_invit_code VARCHAR(64)
,j_invit_serial VARCHAR(64)
,j_invit_date INT(11)
,j_invit_name VARCHAR(512)
,j_invit_description VARCHAR(1024)
,j_invit_price FLOAT(11) DEFAULT 0
,j_invit_quantity FLOAT(11) DEFAULT 0
,j_invit_discount FLOAT(11) DEFAULT 0
,j_invit_vat_percent FLOAT(8) DEFAULT 0
,j_invit_vat FLOAT(11) DEFAULT 0
,j_invit_total FLOAT(11) DEFAULT 0
,j_invit_paid FLOAT(11) DEFAULT 0
,j_invit_comment TEXT
,j_invit_reference VARCHAR(64)
,j_invit_type TINYINT(3) DEFAULT 0
,j_invit_user INT(11) DEFAULT 0
,j_invit_deleted TINYINT(1) DEFAULT 0
)";
mysql_query($s);
if(mysql_error())echo mysql_error()."<hr>";

$s="CREATE TABLE IF NOT EXISTS nse_invoice_item_paid
(
j_invp_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
,j_invp_invoice INT(11) DEFAULT 0
,j_invp_invoice_item VARCHAR(64)
,j_invp_date INT(11) DEFAULT 0
,j_invp_bank VARCHAR(64)
,j_invp_type TINYINT(2) DEFAULT 0
,j_invp_reference TINYINT(1) DEFAULT 0
,j_invp_paid VARCHAR(64)
,j_invp_comment TEXT
,j_invp_user INT(11) DEFAULT 0
)";
mysql_query($s);
if(mysql_error())echo mysql_error()."<hr>";

$s="CREATE TABLE IF NOT EXISTS nse_invoice_element
(
j_invele_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
,j_invele_company INT(11) DEFAULT 0
,j_invele_type TINYINT(3) DEFAULT 0
,j_invele_default TINYINT(1) DEFAULT 0
,j_invele_title VARCHAR(64)
,j_invele_text TEXT
)";
mysql_query($s);
if(mysql_error())echo mysql_error()."<hr>";

$s="CREATE TABLE IF NOT EXISTS nse_invoice_increment
(
j_invinc_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
,j_invinc_name VARCHAR(64)
,j_invinc_company INT(11) DEFAULT 0
,j_invinc_components VARCHAR(256)
,j_invinc_default TINYINT(1) DEFAULT 0
)";
mysql_query($s);
if(mysql_error())echo mysql_error()."<hr>";

if(!is_dir($SDR."/stuff/account"))
{
	include_once $SDR."/system/dir.php";
	J_MakeDir("/stuff/","accounts");
	if(isset($J_err) && $J_err)echo $J_err."<hr>";
}
?>