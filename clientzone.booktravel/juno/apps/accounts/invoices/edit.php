<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";

$r = $_SESSION["j_user"]["role"];
if (isset($_GET["nostaff"]) && ($r != "b" && $r != "d" && $r != "s")) {
	header("location: " . $ROOT . "/apps/accounts/create_orders/create_order.php?no_ads=1");
	die();
}

$SUB_ACCESS = array("unlock" => 0);
if (isset($security_exception))
	$SUB_ACCESS["unlock"] = 1;
else
	include_once $SDR . "/system/secure.php";

include_once $SDR . "/system/get.php";
include_once $SDR . "/apps/accounts/invoices/default.php";
include_once $SDR . "/apps/notes/f/insert.php";
include_once $SDR . "/apps/accounts/invoices/increment.php";

$err = "";
$id = (isset($_GET["del"]) ? $_GET["del"] : (isset($id) ? $id : (isset($_POST["j_inv_id"]) ? $_POST["j_inv_id"] : (isset($_GET["id"]) ? $_GET["id"] : 0))));


if (isset($_GET["del"]) || count($_POST)) {
	include_once $SDR . "/apps/notes/f/insert.php";
	include_once $SDR . "/system/activity.php";

	if ($id > 0)
		$g = mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=" . $id));




// deletion
	if (isset($_GET["del"])	|| (count($_POST) && ($_POST["j_inv_type"] == 3 || $_POST["j_inv_type"] == 4) && isset($_POST["j_inv_cancel_date"]))) {
		$id = isset($_GET["del"]) ? $_GET["del"] : $id;
		mysql_query("DELETE FROM nse_invoice WHERE j_inv_id=" . $id);
		mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_invoice=" . $id);
		mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_invoice=" . $id);
		mysql_query("DELETE FROM nse_invoice_commission WHERE j_invcom_invoice=" . $id);
		// notes
		insertNote($g["j_inv_to_company"], 2, "INV" . $id, "Deleted " . $types[$g["j_inv_type"]] . " No: " . $g["j_inv_number"], 0, 1, 0, 0, "Deleted Invoice No: " . $g["j_inv_number"], 0, "notify_invoice_delete");
		J_act($types[$g["j_inv_type"]], 5, ($g["j_inv_type"] == 9 ? $g["j_inv_order"] : $g["j_inv_number"]), $id, $g["j_inv_type"] == 9 ? $_POST["j_inv_from_contact"] : $_POST["j_inv_to_contact"]);
		unlink($SDR . "/stuff/accounts/pdf_invoice/" . $g["j_inv_number"] . "_" . $id . ".pdf");
		// update tally
		if ($g["j_inv_to_company"]) {
			$company_id = $g["j_inv_to_company"];
			include $SDR . "/apps/accounts/invoices/tally.php";
		}
		echo "<script>var j_P=window.parent,w=j_P.j_G['searchWin'];if(w)j_P.j_Wi[w]['F'][1]['W'].location=j_P.j_Wi[w]['F'][1]['W'].location;j_P.J_WX(self)</script>";
		die();
	} elseif (count($_POST)) {
		include_once $SDR . "/system/parse.php";
		include_once $SDR . "/apps/accounts/commission/f/insert.php";
		include_once $SDR . "/apps/accounts/invoices/create_credit_note.php";
		unset($_POST["j_inv_id"]);

		// clean values
		function J_strip($v="", $l=0) {
			$v = stripslashes($v);
			$v = strip_tags($v, "<br>");
			$v = str_replace("{", "(", $v);
			$v = str_replace("}", ")", $v);
			$v = str_replace("  ", " ", $v);
			$v = str_replace("  ", " ", $v);
			$v = str_replace("\"", "'", $v);
			$v = str_replace("\r", "", $v);
			$v = str_replace("\v", "", $v);
			$v = str_replace("\t", "", $v);
			$v = str_replace("\n", "<br>", $v);
			$v = preg_replace('~[^a-zA-Z0-9 _,:;<>!@%&=+#/\$\'\(\)\-\?\.\*]~s', "", $v);
			if ($l)
				$v = trim(substr($v, 0, $l - 1));
			$v = addslashes($v);
			return $v;
		}

		// tidy textarea values
		if (isset($_POST["j_inv_info"]))
			$_POST["j_inv_info"] = str_replace("\n", "<br>", $_POST["j_inv_info"]);
		if (isset($_POST["j_inv_comment"]))
			$_POST["j_inv_comment"] = str_replace("\n", "<br>", $_POST["j_inv_comment"]);

		// get credit note parent from invoice number
		if ($_POST["j_inv_type"] == 3) {
			if ((!$id && $_POST["j_inv_reference"]) || ($id && $_POST["j_inv_reference"] != $g["j_inv_reference"])) {
				$q = mysql_query("SELECT j_inv_id,j_inv_order FROM nse_invoice WHERE j_inv_number='" . $_POST["j_inv_reference"] . "' LIMIT 1");
				if (mysql_num_rows($q)) {
					$q = mysql_fetch_array($q);
					$_POST["j_inv_credit_id"] = $q["j_inv_id"];
					$order_number = $q["j_inv_order"];
				} else {
					$_POST["j_inv_credit_id"] = 0;
					$order_number = "";
				}
			}
		}

		$ad_id = 0;
		$last_item_id = 0;
		$last_item_inventory = 0;
		$last_item_company = "";
		$inventory_items = "";
		$assoc = ($id && isset($g["j_inv_type"]) && $g["j_inv_type"] ? ($g["j_inv_type"] < 6 && $g["j_inv_order_id"] ? $g["j_inv_order_id"] : ($g["j_inv_type"] == 9 && $g["j_inv_invoice_id"] ? $g["j_inv_invoice_id"] : 0)) : 0); // get associated linked item

//		if (isset($_POST["j_inv_cancel_date"]))
//			$_POST["j_inv_type"] = 2;

		// cancel
		if ((!$id && isset($_POST["j_inv_cancel_date"])) || ($id && isset($_POST["j_inv_cancel_date"]) && !$g["j_inv_cancel_date"])) {
				insertNote($_POST["j_inv_to_company"], 2, "INV" . $id, "Cancelled " . $types[$_POST["j_inv_type"]] . " No: " . $_POST["j_inv_number"], 0, 1, 1, 4);
				$_POST["j_inv_cancel_date"] = $EPOCH;
				$_POST["j_inv_cancel_user"] = $_SESSION["j_user"]["id"];
				// create credit note
				if ($_POST["j_inv_type"] == 2 ) $insert_creditNote = 0;
		} elseif ($id && !isset($_POST["j_inv_cancel_date"]) && $g["j_inv_cancel_date"]) {
			insertNote($g["j_inv_to_company"], 2, "INV" . $id, "Cancel Removed from " . $types[$_POST["j_inv_type"]] . " No: " . $g["j_inv_number"], 0, 1, 1, 4);
			$_POST["j_inv_cancel_date"] = 0;
			$_POST["j_inv_cancel_user"] = 0;
			delete_CreditNote($id);
			$delete_creditNote = 1;
		} else {
			unset($_POST["j_inv_cancel_date"]);
			unset($_POST["j_inv_cancel_user"]);
		}

		if (!isset($_POST["j_inv_number"])) $_POST["j_inv_number"] = "";
		$invoice_number = $_POST["j_inv_number"];
		if (!isset($order_number)) $order_number = $_POST["j_inv_order"];

		// locking
		if ($SUB_ACCESS["unlock"]) $_POST["j_inv_locked"] = isset($_POST["j_inv_locked"]) ? 1 : 0;

		if (!$id) { // insert
			if ($_POST["j_inv_type"] == 9)
				$order_number = J_invoice_increment($_POST["j_inv_increment_id"], $_POST["j_inv_from_company"]);
			else
				$invoice_number = J_invoice_increment($_POST["j_inv_increment_id"], $_POST["j_inv_to_company"]);
			mysql_query("INSERT INTO nse_invoice (j_inv_created,j_inv_creator,j_inv_user,j_inv_increment_id,j_inv_number) VALUES (" . $EPOCH . "," . $_SESSION["j_user"]["id"] . "," . $_SESSION["j_user"]["id"] . "," . $_POST["j_inv_increment_id"] . ",'" . $invoice_number . "')");
			// get max id
			$id = J_ID("nse_invoice", "j_inv_id");
			$insert = 1;
			// note
			insertNote($types[$_POST["j_inv_type"]] . " No: " . ($_POST["j_inv_type"] == 9 ? $order_number : $invoice_number), 0, 1, 0, 0, "New " . $types[$_POST["j_inv_type"]] . " No: " . ($_POST["j_inv_type"] == 9 ? $order_number : $invoice_number), 0, 0, 0, 0, "notify_invoice_new");
			// record action
			J_act($types[$_POST["j_inv_type"]], 3, ($_POST["j_inv_type"] == 9 ? $order_number : $invoice_number), $id, $_POST["j_inv_type"] == 9 ? $_POST["j_inv_from_contact"] : $_POST["j_inv_to_contact"]);
			// clear
			unset($_POST["j_inv_number"]);
			unset($_POST["j_inv_increment_id"]);

			// create invoice for order of a new ad
			if ($_POST["j_inv_type"] == 9 && isset($_GET["set"]) && $_GET["set"] == "ad") {
				$invi = 0;
				$invoice_number = "";
				$r = mysql_query("SELECT j_invinc_id FROM nse_invoice_increment WHERE j_invinc_company=" . $invoice_default_company);
				if (mysql_num_rows($r)) {
					$n = mysql_fetch_array($r);
					$invi = $n["j_invinc_id"];
					$invoice_number = J_invoice_increment($invi);
				}
				$q = "INSERT INTO nse_invoice (j_inv_created,j_inv_creator,j_inv_user,j_inv_order_id,j_inv_order,j_inv_increment_id,j_inv_number) VALUES (" . $EPOCH . "," . $_SESSION["j_user"]["id"] . "," . $_SESSION["j_user"]["id"] . "," . $id . ",'" . $order_number . "'," . $invi . ",'" . $invoice_number . "')";
				mysql_query($q);
				$ad_id = J_ID("nse_invoice", "j_inv_id");
				J_act("Invoice", 3, $invoice_number . " (AD:" . $ad_id . ")", $id, $_POST["j_inv_type"] == 9 ? $_POST["j_inv_from_contact"] : $_POST["j_inv_to_contact"]);
				mysql_query("UPDATE nse_invoice SET j_inv_invoice_id=" . $ad_id . ",j_inv_number='" . $invoice_number . "' WHERE j_inv_id=" . $id);
				insertNote($_POST["j_inv_to_company"], 2, "INV" . $id, "Created Pro-Forma No: " . $invoice_number . " for AD" . $ad_id . " (Order:" . $order_number . ")", 0, 1);
			}
		} else {
			$ad_id = $g["j_inv_invoice_id"];
			insertNote($g["j_inv_to_company"], 2, "INV" . $id, "Edited " . $types[$g["j_inv_type"]] . " No: " . $invoice_number, 0, 1);
			J_act($types[$g["j_inv_type"]], 4, $_POST[$g["j_inv_type"] == 9 ? "j_inv_order" : "j_inv_number"], $id, $g["j_inv_type"] == 9 ? $_POST["j_inv_from_contact"] : $_POST["j_inv_to_contact"]);
			$i = mysql_fetch_array(mysql_query("SELECT j_inv_increment_id FROM nse_invoice WHERE j_inv_id=" . $id . " LIMIT 1"));
			$invoice_number = $_POST["j_inv_number"];
			unset($_POST["j_inv_number"]);
			unset($_POST["j_inv_increment_id"]);
		}

		$_POST["j_inv_date"] = J_dateParse($_POST["j_inv_date"]);
		if (isset($_POST["j_inv_type"])) {
			if ($_POST["j_inv_type"] < 6)
				$_POST["j_inv_head"] = $_POST["j_inv_type"];
			elseif (!$_POST["j_inv_type"])
				$_POST["j_inv_head"] = 1;
		}

		// uploaded file
		$coid = ($_POST["j_inv_type"] == 9 && $_POST["j_inv_from_company"] ? $_POST["j_inv_from_company"] : $_POST["j_inv_to_company"]);
		if ($coid && $_FILES["doc"] && $_FILES["doc"]["size"]) {
			$u = $_FILES["doc"]["name"];
			$n = date("Ymd", $EPOCH);
			$n.="_" . ((stripos($u, "order") !== false || $_POST["j_inv_type"] == 9) && $order_number ? $order_number : $invoice_number);
			$f = substr($u, 0, strrpos($u, "."));
			if (strpos($f, ".") !== false)
				$f = substr($f, 0, strpos($f, "."));
			$x = substr($u, strrpos($u, "."));
			$n.="_" . substr($f, 0, 16);
			$n.=$x;
			include $SDR . "/system/dir.php";
			J_PrepFile("/stuff/establishments/" . $coid . "/files/accounts");
			move_uploaded_file($_FILES["doc"]["tmp_name"], $SDR . "/stuff/establishments/" . $coid . "/files/accounts/" . $n);
			insertNote($coid, 2, "INV" . $id, "Uploaded document " . $n, 0, 1);
		}

		$s = "";
		$ass = "";
		foreach ($_POST as $k => $v) {
			if (strpos($k, "j_inv_") !== false) {
				$s.="," . $k . "='" . J_strip($v) . "'";
				// associate stuff
				if ($k == "j_inv_type" || $k == "j_inv_head" || $k == "j_inv_info" || $k == "j_inv_comment" || $k == "ad_condition" || $k == "j_inv_date" || $k == "j_inv_from_person" || $k == "j_inv_to_person") {

				} else { // update associate item -ie: linked order or invoice
					if ($k == "j_inv_to_company")
						$k = "j_inv_from_company";
					elseif ($k == "j_inv_from_company")
						$k = "j_inv_to_company";
					elseif ($k == "j_inv_to_contact")
						$k = "j_inv_from_contact";
					elseif ($k == "j_inv_from_contact")
						$k = "j_inv_to_contact";
					elseif ($k == "j_inv_to_person")
						$k = "j_inv_from_person";
					$ass.="," . $k . "='" . J_strip($v) . "'";
				}
			}
		}

		// fix type & header
		if (isset($_POST["j_inv_type"])) {
			if (isset($_POST["j_inv_total"]) && $_POST["j_inv_type"] < 6 && isset($g["j_inv_paid"])) {
				$_POST["j_inv_type"] = (J_strip($_POST["j_inv_total"]) >= $g["j_inv_paid"] ? 2 : 1);
			}
			$_POST["j_inv_head"] = $_POST["j_inv_type"];
		}

		// update this item

		mysql_query("UPDATE nse_invoice SET j_inv_user=" . $_SESSION["j_user"]["id"] . $s . " WHERE j_inv_id=" . $id);
		if ($assoc) // update associate item -ie: linked order or invoice
			mysql_query("UPDATE nse_invoice SET j_inv_user=" . $_SESSION["j_user"]["id"] . $ass . " WHERE j_inv_id=" . $assoc);

		if ($ad_id) { // pro-forma for ad order - generate invoice
			mysql_query("UPDATE nse_invoice SET j_inv_user=" . $_SESSION["j_user"]["id"] . $s . " WHERE j_inv_id=" . $ad_id);
			// get establishment accounts person
			$no_js = 1; // prevent js from drawing - not required
			include_once $SDR . "/custom/lib/get_est_people.php";
			$q = "UPDATE nse_invoice SET
			j_inv_type=1
			,j_inv_head=1
			,j_inv_from_company='" . $_POST["j_inv_to_company"] . "'
			,j_inv_to_company='" . $_POST["j_inv_from_company"] . "'
			,j_inv_from_person='" . $invoice_default_from_ad_person . "'
			,j_inv_to_person='" . $_POST["j_inv_from_contact"] . "'
			,j_inv_from_contact='" . $_POST["j_inv_to_contact"] . "'
			,j_inv_to_contact='" . ($accounts ? $accounts : $_POST["j_inv_from_contact"]) . "'
			,j_inv_condition='" . $invoice_default_condition . "'
			,j_inv_bank='" . $invoice_default_bank . "'
			WHERE j_inv_id=" . $ad_id;
			mysql_query($q);
			$fromCompany = $_POST["j_inv_from_company"];
		}

		if (isset($insert_creditNote))
			$insert_creditNote = new_CreditNote
					(
					$EPOCH
					, $id
					, $order_number
					, $invoice_number
					, $_POST["j_inv_from_company"]
					, $_POST["j_inv_to_company"]
					, $_POST["j_inv_from_person"]
					, $_POST["j_inv_from_assessor"]
					, $_POST["j_inv_from_rep"]
					, $_POST["j_inv_from_rep2"]
					, $_POST["j_inv_to_person"]
					, $_POST["j_inv_from_contact"]
					, $_POST["j_inv_to_contact"]
					, $_POST["j_inv_vat"]
					, "Created automatically when invoice no: " . $invoice_number . " was cancelled by: " . J_Value("", "people", "", $_SESSION["j_user"]["id"])
					, ""
					, 0
					, $invoice_default_foot
					, $_POST["j_inv_currency"]
					, $_POST["j_inv_total_price"]
					, $_POST["j_inv_total_discount"]
					, $_POST["j_inv_total_vat"]
					, $_POST["j_inv_total"]
			);

		// do all invoice items
		if ($id) {
			$a = "";
			foreach ($_POST as $k => $v) {
				if (strpos($k, "j_invit_id_") !== false) {
					$d = substr($k, 11);
					if ($v > -1) {
						$iinvid = ($ad_id ? $ad_id : $id);
						$inserted_item = 0;
						$idate = J_dateParse($_POST["j_invit_date_" . $d]);
						$iserial = J_strip($_POST["j_invit_serial_" . $d], 64);
						$icode = J_strip($_POST["j_invit_code_" . $d], 64);
						$iinventory = $_POST["j_invit_inventory_" . $d];
						$icompany = $_POST["j_invit_company_" . $d] ? $_POST["j_invit_company_" . $d] : (isset($_GET["eid"]) && $_GET["eid"] ? $_GET["eid"] : ($_POST["j_inv_type"] == 9 ? $_POST["j_inv_from_company"] : $_POST["j_inv_to_company"]));
						if (!$v) {
							//Get item group
							$statement = "SELECT j_in_category FROM nse_inventory WHERE j_in_id=? LIMIT 1";
							$sql_cat = $GLOBALS['dbCon']->prepare($statement);
							$sql_cat->bind_param('i', $iinventory);
							$sql_cat->execute();
							$sql_cat->bind_result($item_category);
							$sql_cat->fetch();
							$sql_cat->free_result();
							$sql_cat->close();

							if (($item_category == 5 || $item_category == 13 || $item_category == 6 || $item_category == 15 || $item_category == 16) && !isset($assessment_created)) {
								$qa_category = $item_category == 6 || $item_category == 16 ? 'cc' : 'fa';

								//Get assessor name
								if ($qa_category == 'cc') {
									$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname)
													FROM nse_establishment AS a
													LEFT JOIN nse_user AS b ON a.cc_assessor=b.user_id
													WHERE a.establishment_code=?";
								} else {
									$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname)
													FROM nse_establishment AS a
													LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
													WHERE a.establishment_code=?";
								}
								$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
								$sql_assessor->bind_param('s', $icompany);
								$sql_assessor->execute();
								$sql_assessor->bind_result($assessor_name);
								$sql_assessor->fetch();
								$sql_assessor->free_result();
								$sql_assessor->close();

								$statement = "INSERT INTO nse_establishment_assessment
												(establishment_code, assessment_status, assessor_name, active, invoice_id, assessment_type_2)
												VALUES (?,'draft',?,'Y',?,?)";
								$sql_insert3 = $GLOBALS['dbCon']->prepare($statement);
								$sql_insert3->bind_param('ssis', $icompany, $assessor_name, $iinvid, $qa_category);
								$sql_insert3->execute();
								$assessment_created = TRUE;
							}

							$q = "INSERT INTO nse_invoice_item (j_invit_invoice,j_invit_created,j_invit_user) VALUES (" . $iinvid . "," . $EPOCH . "," . $_SESSION["j_user"]["id"] . ")";
							mysql_query($q);
							$inserted_item = 1;
							$v = J_ID("nse_invoice_item", "j_invit_id");
							$last_item_id = $v;
							// notes
							insertNote($icompany, 2, "INV" . $id, "Added to " . $types[$_POST["j_inv_type"]] . " " . $invoice_number . " - " . J_strip($_POST["j_invit_name_" . $d], 512), 0, 1);
						}
						$inventory_items.=$_POST["j_invit_name_" . $d] . "<br>";
						$s = "UPDATE nse_invoice_item SET
						j_invit_inventory=" . $iinventory . "
						,j_invit_code='" . $icode . "'
						,j_invit_serial='" . $iserial . "'
						,j_invit_date=" . $idate . "
						,j_invit_name='" . J_strip($_POST["j_invit_name_" . $d], 512) . "'
						,j_invit_description='" . J_strip($_POST["j_invit_description_" . $d], 1024) . "'
						,j_invit_quantity=" . J_intVal($_POST["j_invit_quantity_" . $d]) . "
						,j_invit_price=" . J_floatVal($_POST["j_invit_price_" . $d]) . "
						,j_invit_discount=" . J_floatVal($_POST["j_invit_discount_" . $d]) . "
						,j_invit_vat=" . J_floatVal($_POST["j_invit_vat_" . $d]) . "
						,j_invit_vat_percent=" . J_floatVal($_POST["j_invit_vat_percent_" . $d]) . "
						,j_invit_disc_percent=" . J_floatVal($_POST["j_invit_disc_percent_" . $d]) . "
						,j_invit_total=" . J_floatVal($_POST["j_invit_total_" . $d]) . "
						,j_invit_user=" . $_SESSION["j_user"]["id"] . "
						,j_invit_company='" . $icompany . "'";
						// credit note parent item reference
						if ($inserted_item && $_POST["j_inv_type"] == 3 && isset($_GET["citem"]) && $_GET["citem"] > 0) {
							$s.=",j_invit_credit_id=" . $_GET["citem"];
							unset($_GET["citem"]);
						}
						$s.=" WHERE j_invit_id=" . $v . " AND j_invit_invoice=" . $iinvid;
						mysql_query($s);
						$last_item_inventory = $iinventory;
						$last_item_company = $icompany;
						if ($inserted_item) {
							$rep = $_POST["j_inv_from_rep"] ? $_POST["j_inv_from_rep"] : ($_POST["j_inv_from_assessor"] ? $_POST["j_inv_from_assessor"] : 0);
							$rep = comm_rep($icompany, $rep);
							if ($_POST["j_inv_type"] < 6 && insert_comm($iinvid, $v, $rep)) {
								rep_to_invoice($iinvid, $rep);
								$u = J_Value("", "people", "", $rep);
								insertNote($icompany, 2, "INV" . $iinvid, "Added commission for: " . $u, 0, 1);
								J_act($types[$_POST["j_inv_type"]], 5, $invoice_number . " / Removed commission for: " . $u, $iinvid);
							}
						}
						// add to inserted credit note
						if (isset($insert_creditNote) && $insert_creditNote)
							addto_CreditNote
									(
									$insert_creditNote
									, $v
									, $iinventory
									, $icode
									, $iserial
									, 0
									, J_strip($_POST["j_invit_name_" . $d], 512)
									, J_strip($_POST["j_invit_description_" . $d], 1024)
									, J_intVal($_POST["j_invit_quantity_" . $d])
									, J_floatVal($_POST["j_invit_price_" . $d])
									, J_floatVal($_POST["j_invit_discount_" . $d])
									, J_floatVal($_POST["j_invit_disc_percent_" . $d])
									, J_floatVal($_POST["j_invit_vat_" . $d])
									, J_floatVal($_POST["j_invit_vat_percent_" . $d])
									, J_floatVal($_POST["j_invit_total_" . $d])
									, $icompany
							);
					}
					else {
						$v = 0 - $v;
						$invit = mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_id=" . $v . " LIMIT 1"));
						mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_invoice=" . $id . " AND j_invit_id=" . $v);
						mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_invoice_item=" . $v);
						mysql_query("DELETE FROM nse_invoice_comission WHERE j_invcom_item=" . $v);
						adjust_CreditNote_items(0, $v);
						mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_credit_id=" . $v);
						// notes
						insertNote($_POST["j_inv_to_company"], 2, "INV" . $id, "Removed from Pro-Forma " . $invoice_number . ": " . J_strip($invit["j_invit_name"], 512), 0, 1);
						if ($invit["j_invit_company"] && $invit["j_invit_company"] != $_POST["j_inv_to_company"])
							insertNote($invit["j_invit_company"], 2, "INV" . $id, "Removed from Pro-Forma " . $invoice_number . ": " . J_strip($invit["j_invit_name"], 512), 0, 1);
					}
					if (!$last_item_id)
						$last_item_id = $v;
				}
			}
		}
	}


	// update tally
	if ($_POST["j_inv_to_company"]) {
		$company_id = $_POST["j_inv_to_company"];
		include $SDR . "/apps/accounts/invoices/tally.php";
	}

	// draw pdf
	// this is now also done for sending and download
	$xid = $id;
	include_once $SDR . "/apps/accounts/invoices/pdf_invoice.php";

	if ($ad_id) {
		$v = $_SESSION["j_user"]["role"];
		if (($v == "c" || $v == "b" || $v == "a" || $v == "r" || $v == "d") && isset($insert))
			include $SDR . "/apps/accounts/invoices/notify_for_ads.php";
	}

	if (isset($_GET["set"])) {
		if ($_GET["set"] == "ad") {
			header("Location: " . $ROOT . "/apps/ad_manager/f/edit.php?new_order=1&eid=" . $_GET["eid"] . "&invoice_id=" . $ad_id . "&order_id=" . $id . "&order_item_id=" . $last_item_id . "&inventory_id=" . ($last_item_inventory ? $last_item_inventory : (isset($_GET["pid"]) && $_GET["pid"] ? $_GET["pid"] : $_GET["inventory_id"])) . (isset($noti) ? "&noti=" . preg_replace('@[^a-zA-Z0-9 ~\-]@', "", $noti) : ""));
			die();
		}
	}

	// done with credit note insert - continue as normal
	unset($creditnote);
	unset($_GET["creditnote"]);
	unset($_GET["cid"]);
	unset($_GET["citem"]);
	unset($_GET["cinventory"]);

	if (
			(!$SUB_ACCESS["unlock"] && isset($g["j_inv_locked"]) && $g["j_inv_locked"])
			||
			($_SESSION["j_user"]["role"] != "d" && $_SESSION["j_user"]["role"] != "b" && $_POST["j_inv_type"] == 2)
	) {

	} // redirect later
	else // update search window
		echo "<script>var j_P=window.parent;j_P.J_reload(0,1,0,'searchWin')</script>";
}
// save end



if (isset($_GET["num"])) {
	$q = mysql_query("SELECT j_inv_id FROM nse_invoice WHERE j_inv_number='" . $_GET["num"] . "' LIMIT 1");
	if (mysql_num_rows($q)) {
		$g = mysql_fetch_array($q);
		$id = $g["j_inv_id"];
	}
}

$id = isset($id) ? $id : (isset($_GET["id"]) ? $_GET["id"] : 0);
$oid = isset($oid) ? $oid : (isset($_GET["oid"]) ? $_GET["oid"] : 0);
$creditnote = isset($creditnote) || isset($_GET["creditnote"]) ? 1 : 0;
$debitnote = isset($debitnote) || isset($_GET["debitnote"]) ? 1 : 0;
$ad = isset($ad) ? $ad : (isset($_GET["ad"]) ? $_GET["ad"] : 0);

$v = (isset($_v) ? $_v : "");
$head = isset($head) ? $head : "";
$fromCo = isset($fromCo) ? $fromCo : "";
$toCo = isset($toCo) ? $toCo : "";
$itCo = "";
$fromPer = isset($fromPer) ? $fromPer : "";
$fromAss = isset($fromAss) ? $fromAss : "";
$fromRep = isset($fromRep) ? $fromRep : "";
$fromRep2 = isset($fromRep2) ? $fromRep2 : "";
$toPer = isset($toPer) ? $toPer : "";
$fromCnt = isset($fromCnt) ? $fromCnt : "";
$toCnt = isset($toCnt) ? $toCnt : "";
$condition = isset($condition) ? $condition : "";
$bank = isset($bank) ? $bank : "";
$foot = isset($foot) ? $foot : "";
$lock = isset($lock) ? $lock : "";


if ($id) {
	$q = mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=" . $id . " LIMIT 1");
	if (mysql_num_rows($q))
		$in = mysql_fetch_array($q);
	else {
		echo "<html><link rel=stylesheet href=" . $ROOT . "/style/set/page.css><body><tt><var><b>WARNING!</b></var> This item could not be found.</tt></body></html>";
		die();
	}
	$oid = $in["j_inv_type"] == 9 ? 1 : 0;

	// redirect to view mode
	if ((!$SUB_ACCESS["unlock"] && $in["j_inv_locked"]) || ($_SESSION["j_user"]["role"] != "d" && $_SESSION["j_user"]["role"] != "b" && $in["j_inv_type"] == 2)) {
		header("Location: " . $ROOT . "/apps/accounts/invoices/invoice_view.php?id=" . $id . "&x=1");
		die();
	}

	include_once $SDR . "/apps/accounts/invoices/get.php";

	$v = ($in["j_inv_head"] ? $in["j_inv_head"] : "") . "~";
	$v.=date("d/m/Y H:i", $in["j_inv_date"]) . "~";
	$v.=$in["j_inv_order"] . "~";
	$v.=$in["j_inv_reference"] . "~";
	$v.=$in["j_inv_number"] . "~";
	$v.=$in["j_inv_increment_id"] . "~"; // 5
	$v.=$in["j_inv_from_company"] . "~";
	$v.=$in["j_inv_to_company"] . "~";
	$v.=$in["j_inv_from_person"] . "~";
	$v.=$in["j_inv_from_assessor"] . "~";
	$v.=$in["j_inv_from_rep"] . "~"; // 10
	$v.=$in["j_inv_from_rep2"] . "~";
	$v.=$in["j_inv_to_person"] . "~";
	$v.=$in["j_inv_from_contact"] . "~";
	$v.=$in["j_inv_to_contact"] . "~";
	$v.=$in["j_inv_vat"] . "~"; // 15
	$v.=$in["j_inv_info"] . "~";
	$v.=$in["j_inv_comment"] . "~";
	$v.=($in["j_inv_condition"] ? $in["j_inv_condition"] : "") . "~";
	$v.=($in["j_inv_bank"] ? $in["j_inv_bank"] : "") . "~";
	$v.=($in["j_inv_foot"] ? $in["j_inv_foot"] : "") . "~"; // 20
	$v.="~";
	$v.=$in["j_inv_currency"] . "~";
	$v.=$in["j_inv_total_price"] . "~";
	$v.=$in["j_inv_total_discount"] . "~";
	$v.=$in["j_inv_total_vat"] . "~"; // 25
	$v.=$in["j_inv_total"] . "~";
	$v.=$in["j_inv_type"] . "~";
	$v.=$in["j_inv_order_id"] . "~";
	$v.=$in["j_inv_invoice_id"] . "~";
	$v.=($in["j_inv_locked"] ? 1 : "") . "~"; // 30

	if ($in["j_inv_from_company"] && !isset($_SESSION["juno"]["accounts"]["company"][$in["j_inv_from_company"]]))
		$fromCo = ($in["j_inv_from_company"] ? J_inv_getto($in["j_inv_from_company"]) : $invoice_default_company_info);
	if (!isset($_SESSION["juno"]["accounts"]["company"][$in["j_inv_to_company"]]))
		$toCo = ($in["j_inv_to_company"] ? J_inv_getto($in["j_inv_to_company"]) : $invoice_default_company_info);
	if (!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_from_person"]]))
		$fromPer = J_inv_getpeo($in["j_inv_from_person"]);
	if (!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_from_assessor"]]))
		$fromAss = J_inv_getpeo($in["j_inv_from_assessor"]);
	if (!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_from_rep"]]))
		$fromRep = J_inv_getpeo($in["j_inv_from_rep"]);
	if (!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_from_rep2"]]))
		$fromRep2 = J_inv_getpeo($in["j_inv_from_rep2"]);
	if (!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_from_contact"]]))
		$fromCnt = J_inv_getpeo($in["j_inv_from_contact"]);
	//if(!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_to_person"]]))
	$toPer = J_inv_getpeo($in["j_inv_to_person"]);
	//if(!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_to_contact"]]))
	$toCnt = J_inv_getpeo($in["j_inv_to_contact"]);
	if (!isset($_SESSION["juno"]["accounts"]["element"][$in["j_inv_head"]]))
		$head = J_inv_getele($in["j_inv_head"]);
	if (!isset($_SESSION["juno"]["accounts"]["element"][$in["j_inv_condition"]]))
		$condition = J_inv_getele($in["j_inv_condition"]);
	if (!isset($_SESSION["juno"]["accounts"]["element"][$in["j_inv_bank"]]))
		$bank = J_inv_getele($in["j_inv_bank"]);
	if (!isset($_SESSION["juno"]["accounts"]["element"][$in["j_inv_foot"]]))
		$foot = J_inv_getele($in["j_inv_foot"]);

	// order gets shared contents from the invoice
	$i = $in["j_inv_invoice_id"] ? $in["j_inv_invoice_id"] : $id;
	$r = mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=" . $i . " ORDER BY j_invit_date,j_invit_name");
	if (mysql_num_rows($r)) {
		while ($i = mysql_fetch_array($r)) {
			$v.=$i["j_invit_id"] . "`";
			$v.=$i["j_invit_inventory"] . "`";
			$v.=($i["j_invit_date"] ? date("d/m/Y", $i["j_invit_date"]) : "") . "`";
			$v.=$i["j_invit_name"] . "`";
			$v.=$i["j_invit_description"] . "`";
			$v.=$i["j_invit_code"] . "`"; // 5
			$v.=$i["j_invit_quantity"] . "`";
			$v.=$i["j_invit_price"] . "`";
			$v.=$i["j_invit_discount"] . "`";
			$v.=$i["j_invit_vat"] . "`";
			$v.=$i["j_invit_total"] . "`"; // 10
			$v.=$i["j_invit_serial"] . "`";
			$v.=$i["j_invit_vat_percent"] . "`";
			$v.=$i["j_invit_disc_percent"] . "`";
			$v.=$i["j_invit_company"] . "`";
			$v.="|";
		}
	}
	$v = stripit($v);

	// get associated credit note parent invoice
	if ($in["j_inv_type"] == 3 && isset($_GET["num"])) {
		$q = mysql_query("SELECT j_inv_number FROM nse_invoice WHERE j_inv_id='" . $in["j_inv_reference"] . "' LIMIT 1");
		if (mysql_num_rows($q)) {
			$gc = mysql_fetch_array($q);
			$creditnoteparent = $gc["j_inv_number"];
		}
	}
	// get associated credit notes for this invoice
	$qc = mysql_query("SELECT j_inv_id,j_inv_number,j_inv_total FROM nse_invoice WHERE j_inv_credit_id=" . $id);
	if (mysql_num_rows($qc)) {
		$creds = "";
		$credItems = "";
		while ($gc = mysql_fetch_array($qc)) {
			$creds.=$gc["j_inv_id"] . "~" . $gc["j_inv_number"] . "~" . $gc["j_inv_total"] . "|";
			$qi = mysql_query("SELECT j_invit_id FROM nse_invoice_item WHERE j_invit_invoice=" . $gc["j_inv_id"]);
			if (mysql_num_rows($qi)) {
				while ($gi = mysql_fetch_array($qi)) {
					$qc2 = mysql_query("SELECT j_invit_invoice FROM nse_invoice_item WHERE j_invit_credit_id=" . $gi["j_invit_id"] . " LIMIT 1");
					if (mysql_num_rows($qc2)) {
						$gi2 = mysql_fetch_array($qi);
						$credItems.=$gi["j_invit_id"] . "~" . $gi["j_invit_invoice"] . "|";
					}
				}
			}
		}
		$creds = trim(str_replace(array("\"", "'"), "", $creds), "|");
		$credItems = trim($credItems, "|");
	}
}

$u = $_SESSION["j_user"]["id"];

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<link rel=stylesheet href=" . $ROOT . "/apps/accounts/invoices/style.css>";
echo "<script src=" . $ROOT . "/apps/accounts/invoices/edit.js></script>";
echo "<body>";

if ($err)
	echo "<tt style=color:#CC0000><b>WARNING!</b> You have SQL errors. To solve this problem, please copy the contents of this page and send to the system administrator.</tt><hr>" . $err . "<br><br>";

$uid = $_SESSION["j_user"]["id"];
include_once $SDR . "/system/dir.php";

if (!$id) {
	if ($creditnote || $debitnote) {
		if (isset($eid) && $eid && !isset($_SESSION["juno"]["accounts"]["company"][$eid]))
			$toCo = J_inv_getto($eid);
		if ($creditnote && !isset($add_items))
			$add_items = "160";
	}
}

echo "<script>";

if (isset($creds) && $creds) { // credit notes for this invoice
	echo "creditNotes=\"" . $creds . "\";";
	echo "creditItems=\"" . $credItems . "\";";
}
if (isset($cid) && $cid) // new credit note - parent invoice id
	echo "cid=\"" . $cid . "\";";
if (isset($insert_creditNote) && $insert_creditNote)
	echo "autoCreditNote=" . $insert_creditNote . ";";
elseif (isset($delete_creditNote))
	echo "autoDeleteCreditNote=1;";
if (J_getSecure("/apps/accounts/credit_note_add.php"))
	echo "cnoteIssue=1;";
if (isset($add_items_price) && $add_items_price) // credit notes for this invoice
	echo "addItemPrice=\"" . $add_items_price . "\";";
if ($SUB_ACCESS["unlock"])
	echo "unLock=1;";

echo "J_inv_edit(";
echo isset($var) ? "\"" . $var . "\"" : 0;
echo "," . ($id && $in["j_inv_cancel_date"] ? "\"" . date("d/m/Y", $in["j_inv_cancel_date"]) . "\"" : 0);
echo "," . ($id && $in["j_inv_cancel_user"] ? "\"" . J_Value("", "people", "", $in["j_inv_cancel_user"]) . "\"" : 0);
echo "," . $oid;
echo "," . (isset($creditnoteparent) ? "'" . $creditnoteparent . "'" : $creditnote);
echo "," . ($debitnote ? 1 : 0);
echo ",'" . $_SESSION["j_user"]["role"] . "'";
echo "," . ($id && ($in["j_inv_creator"] == $uid || $in["j_inv_from_person"] == $uid || $in["j_inv_from_rep"] == $uid || (!$oid && $in["j_inv_from_contact"] == $uid) || ($oid && ($in["j_inv_to_person"] == $uid || $in["j_inv_to_contact"] == $uid))) ? 1 : 0);
echo "," . $id;
if ($id)
	echo ",0,0";
else {
	$invoice_number = "";
	$r = "SELECT j_invinc_id FROM nse_invoice_increment WHERE ";
	if ($oid)
		$r.="j_invinc_id=" . $invoice_default_order_increment;
	elseif ($creditnote)
		$r.="j_invinc_id=" . $invoice_default_creditnote_increment;
	elseif ($debitnote)
		$r.="j_invinc_id=" . $invoice_default_debitnote_increment;
	else
		$r.="j_invinc_company=" . $invoice_default_company;
	$r.=" LIMIT 1";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		$g = mysql_fetch_array($r);
		$invoice_number = J_invoice_increment($g["j_invinc_id"], $invoice_default_company, 1);
		echo ",\"" . $invoice_number . "\"," . $g["j_invinc_id"];
	}
	else
		echo ",0,0";
}
echo ",\"" . (isset($v) ? $v : "") . "\"";
echo "," . ($fromCo ? "\"" . $fromCo . "\"" : 0);
echo "," . ($toCo ? "\"" . $toCo . "\"" : 0);
echo "," . ($itCo ? "\"" . $toCo . "\"" : 0);
echo "," . ($fromPer ? "\"" . $fromPer . "\"" : 0);
echo "," . ($fromAss ? "\"" . $fromAss . "\"" : 0);
echo "," . ($fromRep ? "\"" . $fromRep . "\"" : 0);
echo "," . ($fromRep2 ? "\"" . $fromRep2 . "\"" : 0);
echo "," . ($toPer ? "\"" . $toPer . "\"" : 0);
echo "," . ($fromCnt ? "\"" . $fromCnt . "\"" : 0);
echo "," . ($toCnt ? "\"" . $toCnt . "\"" : 0);
echo ",\"" . $head . "\"";
echo ",\"" . $condition . "\"";
echo ",\"" . $bank . "\"";
echo ",\"" . $foot . "\"";
echo "," . (isset($add_items) ? "\"" . $add_items . "\"" : 0);
echo "," . (isset($eid) && !$oid ? "'" . $eid . "'" : (isset($_GET["eid"]) && !$oid ? "'" . $_GET["eid"] . "'" : 0));
echo count($_POST) ? ",1" : "";
echo ")";
if (isset($var) && $var == "ad") // clear footer
	echo ";j_P.j_Wi[j_W]['foot'].innerHTML=''";
echo "</script>";

$J_title1 = "Edit Invoices";
if ($id) {
	if ($oid)
		$J_title1 = ($in["j_inv_order"] ? $in["j_inv_order"] : "") . ($in["j_inv_date"] ? ($in["j_inv_order"] ? " -" : "") : "");
	else
		$J_title1 = ($in["j_inv_number"] ? $in["j_inv_number"] : "") . ($in["j_inv_date"] ? ($in["j_inv_number"] ? " -" : "") : "");
	$J_title1.=" " . date("D d/m/Y", $in["j_inv_date"]);
}
else
	$J_title1 = ($invoice_number ? $invoice_number . " - " : "") . date("d/m/Y H:i", $EPOCH);
$J_title2 = ($id ? "" : "New ") . ($id ? $types[$in["j_inv_type"]] : ($oid ? "Order" : ($creditnote ? "Credit Note" : $debitnote ? "Debit Note" : "Pro-Forma")));
$J_title3 = ($id && ($in["j_inv_to_company"] || $in["j_inv_to_person"]) ? "<big>To: " . ($in["j_inv_to_company"] ? J_Value("name", "establishment", "id", $in["j_inv_to_company"]) : J_Value("name", "people", "id", $in["j_inv_to_person"])) . "</big>" : "") . ($_SESSION["j_user"]["role"] == "d" ? "id:" . $id : "");
$J_icon = "<img src=" . $ROOT . "/ico/set/spreadsheet" . ($oid || ($id && $in["j_inv_type"] == 9) ? "_yellow" : ($creditnote || ($id && $in["j_inv_type"] == 3) ? "_red" : ($debitnote || ($id && $in["j_inv_type"] == 4) ? "_pink" : ""))) . ".png>";
$J_label = 19;
$J_width = 1024;
$J_height = 640;
include $SDR . "/system/deploy.php";
echo "</body></html>";
?>