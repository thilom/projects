<?php
// notification
if(($invoice_default_to_ad_person || $invoice_default_order_contact))
{
	$n_ADp=0;
	$n_ACp=0;
	$n_EPp=0;
	$noti="";

	if($invoice_default_to_ad_person)
		$n_ADp=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$invoice_default_to_ad_person." LIMIT 1"));

	if($invoice_default_order_contact)
		$n_ACp=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$invoice_default_order_contact." LIMIT 1"));

	$n_EPp=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1"));

	$n_est=explode("~",J_inv_getto($fromCompany));

	include_once $SDR."/custom/email_headers.php";
	$subject="ADS ORDER ".$order_number;
	$msg=$j_email_header;
	$msg.="<b>New ad Ordered ".$order_number."</b><br>";
	$msg.="<b style='font-size:9pt'>Date ".date("d/m/Y H:i",$EPOCH)."</b><br>";
	$msg.="<b style='font-size:9pt'>Establishment: ".$n_est[1]." (".$n_est[0].")</b><br>";
	$msg.="<b style='font-size:9pt'>Order Created By ".$n_EPp["firstname"]." ".$n_EPp["lastname"]."</b><br>";
	$msg.="<hr>";

	if(isset($inventory_items))
	{
		$msg.="<b>Items Ordered</b><br>";
		$msg.=$inventory_items;
		$msg.="<hr>";
	}
	$msg.="<span style='font-size:8pt'>Notification of order sent to:<br>";
	if($n_ADp["email"]!=$n_EPp["email"])
	{
		$msg.="<a href='mailto:".$n_ADp["email"]."'>".$n_ADp["firstname"]." ".$n_ADp["lastname"].($n_ADp["cell"]?" (".$n_ADp["cell"].")":"")."</a><br>";
		$noti.=$n_ADp["firstname"]." ".$n_ADp["lastname"]."~";
	}
	if($n_ACp["email"]!=$n_EPp["email"])
	{
		$msg.="<a href='mailto:".$n_ACp["email"]."'>".$n_ACp["firstname"]." ".$n_ACp["lastname"].($n_ACp["cell"]?" (".$n_ACp["cell"].")":"")."</a><br>";
		$noti.=$n_ACp["firstname"]." ".$n_ACp["lastname"]."~";
	}
	$msg.="<a href='mailto:".$n_EPp["email"]."'>".$n_EPp["firstname"]." ".$n_EPp["lastname"].($n_EPp["cell"]?" (".$n_EPp["cell"].")":"")."</a><br>";
	$noti.=$n_EPp["firstname"]." ".$n_EPp["lastname"]."~";

	$msg.="</span><br>";
	$msg.="<br><br><br>";
	$msg.=$j_email_footer;
	require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
	$mail=new PHPMailer(true);
	try
	{
		$mail->From=$n_EPp["email"];
		$mail->FromName=strtoupper($_SERVER["HTTP_HOST"]);
		$mail->AddReplyTo($n_EPp["email"],$n_EPp["email"]);
		$mail->AddAddress($n_EPp["email"],$n_EPp["email"]);
		if($n_ADp["email"]!=$n_EPp["email"])
		  $mail->AddAddress($n_ADp["email"],$n_ADp["email"]);
		if($n_ACp["email"]!=$n_EPp["email"])
		  $mail->AddAddress($n_ACp["email"],$n_ACp["email"]);
		$mail->SetFrom($n_EPp["email"],$n_EPp["email"]);
		$mail->Subject =$subject;
		$mail->MsgHTML($msg);
		$ifile=$SDR."/stuff/accounts/pdf_invoice/".$order_number."_".$id.".pdf";
		if($order_number && file_exists($ifile))
			$mail->AddAttachment($ifile);
		$ifile=$SDR."/stuff/accounts/pdf_invoice/".$invoice_number."_".$ad_id.".pdf";
		if($invoice_number && file_exists($ifile))
			$mail->AddAttachment($ifile);
		$mail->Send();
	}
	catch (phpmailerException $e)
	{
	  echo $e->errorMessage();
	}
	catch (Exception $e)
	{
	  echo $e->getMessage();
	}
}
?>