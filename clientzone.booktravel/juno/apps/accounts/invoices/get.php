<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/get.php";

function stripit($v="")
{
	$v=stripslashes($v);
	$v=str_replace("\r","",$v);
	$v=str_replace("\t","",$v);
	$v=str_replace("\n","<br>",$v);
	$v=str_replace("\"","'",$v);
	return $v;
}

function J_inv_getto($v=0,$t=0)
{
	$o="";
	$billing1 = '';
    $statement = "SELECT a.establishment_code, a.establishment_name, b.town_id, c.reservation_email, c.reservation_tel, c.reservation_fax, a.street_address_line1, a.street_address_line2, a.street_address_line3
                FROM nse_establishment AS a
                LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
                LEFT JOIN nse_establishment_reservation AS c ON a.establishment_code=c.establishment_code
                WHERE a.establishment_code=?
                LIMIT 1";
    $sql_base = $GLOBALS['dbCon']->prepare($statement);
    $sql_base->bind_param('s', $v);
    $sql_base->execute();
    $sql_base->store_result();

    if ($sql_base->num_rows == 0) {
        $v = 0;
    } else {
        $sql_base->bind_result($establishment_code, $establishment_name, $town_id, $email, $tel, $fax, $street1, $street2 ,$street3);
        $sql_base->fetch();

        $statement = "SELECT billing_line1, billing_line2, billing_line3, billing_code, company_name, company_number, vat_number FROM nse_establishment_billing WHERE establishment_code=? LIMIT 1";
        $sql_billing = $GLOBALS['dbCon']->prepare($statement);
        $sql_billing->bind_param('s',$v);
        $sql_billing->execute();
        $sql_billing->store_result();
        if ($sql_billing->num_rows == 0) {
            $billing = '';
            $company_name = '';
            $company_number = '';
            $vat = '';
        } else {
            $sql_billing->bind_result($billing1, $billing2, $billing3, $billing4, $company_name, $company_number, $vat);
            $sql_billing->fetch();
        }
        $sql_billing->free_result();
        $sql_billing->close();

		//Get contact data
		$statement = "SELECT contact_type, contact_value FROM nse_establishment_public_contact WHERE establishment_code=?";
		$sql_contacts = $GLOBALS['dbCon']->prepare($statement);
		$sql_contacts->bind_param('s', $v);
		$sql_contacts->execute();
		$sql_contacts->store_result();
		$sql_contacts->bind_result($contact_type, $contact_value);
		while ($sql_contacts->fetch()) {
			switch ($contact_type) {
				case 'email':
					$contact_email = $contact_value;
					break;
				case 'tel1':
					$contact_tel = $contact_value;
					break;
				case 'fax1':
					$contact_fax = $contact_value;
					break;
			}
		}
		$sql_contacts->close();

		//Assemble contact details
		$email = empty($contact_email)?$email:$contact_email;
		$tel = empty($contact_tel)?$tel:$contact_tel;
		$fax = empty($contact_fax)?$fax:$contact_fax;

        $statement = "SELECT town_name FROM nse_nlocations_towns WHERE town_id=?";
        $sql_town = $GLOBALS['dbCon']->prepare($statement);
        $sql_town->bind_param('i', $town_id);
        $sql_town->execute();
        $sql_town->bind_result($town_name);
        $sql_town->fetch();
        $sql_town->free_result();
        $sql_town->close();

        //Assemble street address
        $street_address = '';
        if (!empty($street1)) $street_address .= "$street1,";
        if (!empty($street2)) $street_address .= "$street2,";
        if (!empty($street3)) $street_address .= "$street3,";
        if (!empty($street_address)) $street_address = substr($street_address, 0, -1);

        //Assemble Billing address
        $billing = '';
        if (!empty($billing1)) {
            if (substr($billing1, -1) == ',') $billing1 = substr($billing1, 0, -1);
            $billing .= "$billing1,";
        }
        if (!empty($billing2)) $billing .= "$billing2,";
        if (!empty($billing3)) $billing .= "$billing3,";
        if (!empty($billing4)) $billing .= "$billing4,";
        if (!empty($billing)) $billing = substr($billing, 0, -1);

        if ($billing == ",,,") $billing = $street_address;
        $postal = $billing;

        //Company name
        if(empty($company_name))
					$company_name=$establishment_name;
        $o=$establishment_code."~";
        $o.=$company_name."~";
        $o.=$town_name."~";
        $o.=$email."~";
        $o.=$tel."~";
        $o.=$fax."~";
        $o.=$postal."~";
        $o.=$street_address==$billing?'':$street_address."~";
        $o.=($t?$establishment_code:"")."~";
        $o.=$company_number."~";
        $o.=$vat;
        $o=stripit($o);
        $o=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+\~\'\?\(\)\*\$\.\-]~',"",$o);
        $o=str_replace("<br>",", ",$o);
    }
   $sql_base->free_result();
   $sql_base->close();
	return $o;
}

function J_inv_getpeo($v=0)
{
	$o="";
	if($v)
	{
		$r=mysql_query("SELECT * FROM nse_user WHERE user_id=".$v." LIMIT 1");
		if(mysql_num_rows($r))
		{
			$_SESSION["juno"]["accounts"]["user"][$v]=1;
			$g=mysql_fetch_array($r);
			$o=$g["user_id"]."~";
			$o.=$g["firstname"]." ".$g["lastname"]."~";
			$o.= "~";
			$o.=$g["email"]."~";
			$o.=($g["cell"]?$g["cell"]:$g["phone"])."~";
			$o.="~";
			$o.="~";
			$o.="~";
			$o.=$g["user_id"]."~";
			$o.= "~";
			$o=stripit($o);
			$o=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+\~\'\?\(\)\*\$\.\-]~',"",$o);
			$o=str_replace("<br>",", ",$o);
		}
	}
   return $o;
}

function J_inv_getele($v=0,$t=0)
{
	$r=mysql_query("SELECT * FROM nse_invoice_element WHERE ".($v?"j_invele_id=".$v:"j_invele_type=".$t." AND j_invele_default=1 LIMIT 1"));
	if(mysql_num_rows($r))
	{
		$g=mysql_fetch_array($r);
		$_SESSION["juno"]["accounts"]["element"][$g["j_invele_id"]]=1;
		$v=$g["j_invele_id"]."~";
		$v.=$g["j_invele_title"]."~";
		$v.=str_replace("\"","`",stripslashes($g["j_invele_text"]));
		$v=stripit($v);
		$v=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+/\~\'\?\(\)\*\$\.\-]~',"",$v);
	}
	else
		$v="";
	return $v;
}

function J_inv_getitem($v=0)
{
	global $invoice_default_vat;
	$r=mysql_query("SELECT * FROM nse_inventory WHERE j_in_id=".$v." LIMIT 1");
	if(mysql_num_rows($r))
	{
		$g=mysql_fetch_array($r);
		$v=$g["j_in_id"]."~";
		$v.=$g["j_in_name"].(isset($_GET["ad_item_co"])?" (".preg_replace("~[^a-zA-Z0-9 ,'@\-\(\)]~","",J_Value("","establishment","",$_GET["ad_item_co"])).")":"")."~";
		$v.=substr($g["j_in_description"],0,256)."~";
		$v.=($g["j_in_code"]?$g["j_in_code"]:"")."~";
		$v.=($g["j_in_price"]?$g["j_in_price"]:"")."~";
		$v=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+\~\'\?\(\)\*\$\.\-]~',"",$v);
	}
	else
		$v="";
	return $v;
}

if(isset($_GET["ret"]))
{
	if($_GET["ret"]==1)$r=J_inv_getto($_GET["v"]);
	elseif($_GET["ret"]==2)$r=J_inv_getpeo($_GET["v"]);
	elseif($_GET["ret"]==3)$r=J_inv_getele($_GET["v"]);
	elseif($_GET["ret"]==9)$r=J_inv_getitem($_GET["v"]);
	echo "<script>window.parent.J_inv_put('".$_GET["d"]."',\"".$r."\",1)</script>";
}
?>