<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
require_once $SDR."/apps/accounts/invoices/default.php";
require_once $SDR."/apps/accounts/invoices/get.php";
include_once $SDR."/apps/accounts/invoices/types.php";

$xid=(isset($xid)?$xid:(isset($_GET["xid"])?$_GET["xid"]:$_POST["xid"]));

$gin=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$xid." LIMIT 1"));

$pdf_filename=$SDR."/stuff/accounts/pdf_invoice/".($gin["j_inv_type"]==9?$gin["j_inv_order"]:$gin["j_inv_number"])."_".$xid.".pdf";

@unlink($SDR."/stuff/accounts/pdf_invoice/.pdf");
@unlink($SDR."/stuff/accounts/pdf_invoice/_.pdf");
@unlink($SDR."/stuff/accounts/pdf_invoice/_".$xid.".pdf");


if(!function_exists("d_company"))
{
	function d_company($a)
	{
		$v="";
		if($a && count($a))
		{
			$a=explode("~",$a);
			$v.="<b>".strtoupper($a[1])."</b><br>";
			if(isset($a[2]) && $a[2])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>City</b>&nbsp;".ucfirst($a[2])."</span><br>";
			if(isset($a[3]) && $a[3])
			{
				$a[3]=strtolower($a[3]);
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Email</b>&nbsp;<a href=mailto:".$a[3].">".$a[3]."</a></span><br>";
			}
			if(isset($a[4]) && $a[4])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Tel</b>&nbsp;".$a[4]."</span><br>";
			if(isset($a[5]) && $a[5])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Fax</b>&nbsp;".$a[5]."</span><br>";
			if(isset($a[6]) && $a[6])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Postal</b>&nbsp;".str_replace("  "," ",str_replace(",",", ",str_replace("<br>",", ",$a[6])))."</span><br>";
			if(isset($a[7]) && $a[7])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Address</b>&nbsp; ".str_replace("  "," ",str_replace(",",", ",str_replace("<br>",", ",$a[7])))."</span><br>";
			if(isset($a[8]) && $a[8])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Est. Code</b>&nbsp; ".str_replace("<br>",", ",$a[8])."<br>";
			if(isset($a[9]) && $a[9])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Reg No.</b>&nbsp; ".$a[9]."</span><br>";
			if(isset($a[10]) && $a[10])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>VAT No.</b> ".$a[10]."</span><br>";
		}
		$v=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+\'\?\(\)\*\$\.\-]~',"",$v);
		return $v;
	}

	function d_person($a)
	{
		$v="";
		if($a && count($a))
		{
			$a=explode("~",$a);
			$v.="<b>".strtoupper($a[1])."</b><br>";
			if(isset($a[2]) && $a[2])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>City</b>&nbsp; ".ucfirst($a[2])."</span><br>";
			if(isset($a[3]) && $a[3])
			{
				$a[3]=strtolower($a[3]);
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Email</b>&nbsp; <a href=mailto:".$a[3].">".$a[3]."</a></span><br>";
			}
			if(isset($a[4]) && $a[4])
				$v.="<span style='font-size:8pt'><b style='color:#808080'>Tel</b>&nbsp; ".$a[4]."</span><br>";
		}
		$v=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+\'\?\(\)\*\$\.\-]~',"",$v);
		return $v;
	}
}

// html
$_h="<html>";
$_h.="<head>";
$_h.="<title>AA TRAVEL GUIDES ".strtoupper($types[$gin["j_inv_type"]])." ".$gin["j_inv_number"].($gin["j_inv_date"]?" (".date("d/m/Y H:i",$gin["j_inv_date"]).")":"")."</title>";
$_h.="</head>";
$_h.="<body style='font-family:helvetica'>";

// header
$_h.="<table width='100%' cellspacing='0' cellpadding='0'>";
$_h.="<tr>";
$_h.="<td style='padding:0;border:0'>";
$_h.="<div style='width:100%;padding:8px 16px 2px 16px;margin:32px 0px 0px 0px;font-size:18pt;letter-spacing:-1px;background:#FFC20F;border:0'>";
$_h.="<b>AA TRAVEL GUIDES</b> (Pty) LTD</div>";
$_h.="<b style='font-size:18pt;letter-spacing:-1px;display:block;margin:4px 16px 4px 16px;border:0'>".$types[$gin["j_inv_type"]]."</b>";
$_h.="</td>";
$_h.="<td style='width:50px;padding:0;text-align:right;border:0'>";
$_h.="<img src='".$SDR."/stuff/accounts/headers/logo.png'>";
$_h.="</td>";
$_h.="</tr>";
$_h.="</table>";

$_h.="<br>";
$_h.="<br>";

$_h.="<table width='100%' cellspacing='0' cellpadding='3' style='border-collapse:collapse;font-size:9pt;border-style:solid;border-width:2;border-color:#AAAAAA'>";
$_h.="<tr>";
$_h.="<td width='50%' bgcolor='#EEEEEE' style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>Date</b> ".($gin["j_inv_date"]?date("d/m/Y H:i",$gin["j_inv_date"]):"-")."</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC' width='50%'><b style='color:#808080'>Order No</b> ".$gin["j_inv_order"]."</td>";
$_h.="</tr>";
$_h.="<tr>";
$_h.="<td bgcolor='#EEEEEE' style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>Reference</b> ".$gin["j_inv_reference"]."</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>Invoice No.</b> ".$gin["j_inv_number"]."</td>";
$_h.="</tr>";
$_h.="<tr>";
$_h.="<td bgcolor='#EEEEEE' style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>From</b> ".d_company($gin["j_inv_from_company"]?J_inv_getto($gin["j_inv_from_company"]):$invoice_default_company_info)."</td>";
$_h.="<td><b style='color:#808080'>To</b> ".d_company($gin["j_inv_to_company"]?J_inv_getto($gin["j_inv_to_company"]):$invoice_default_company_info)."</td>";
$_h.="</tr>";
$_h.="<tr>";
$_h.="<td style='#EEEEEE' style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>From Person:</b> ".($gin["j_inv_from_person"]?d_person(J_inv_getpeo($gin["j_inv_from_person"])):"");
if($gin["j_inv_from_rep"])
	$_h.="<b style='color:#808080'>Rep:</b> ".($gin["j_inv_from_rep"]?d_person(J_inv_getpeo($gin["j_inv_from_rep"])):"");
$_h.="</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>To Person</b> ".($gin["j_inv_to_person"]?d_person(J_inv_getpeo($gin["j_inv_to_person"])):"")."</td>";
$_h.="</tr>";
if($gin["j_inv_from_contact"] || $gin["j_inv_to_contact"])
{
	$_h.="<tr>";
	$_h.="<td bgcolor='#EEEEEE' style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>Accounts/Queries</b> ".($gin["j_inv_from_contact"]?d_person(J_inv_getpeo($gin["j_inv_from_contact"])):"")."</td>";
	$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'><b style='color:#808080'>Accounts/Queries</b> ".($gin["j_inv_to_contact"]?d_person(J_inv_getpeo($gin["j_inv_to_contact"])):"")."</td>";
	$_h.="</tr>";
}
if($gin["j_inv_info"])
	$_h.="<tr><td colspan=2 style='border-style:solid;border-width:1;border-color:#CCCCCC'>".$gin["j_inv_info"]."</td></tr>";
$_h.="</table>";
$_h.="<br>";
$_h.="<br>";

$_h.="<table width='100%' cellspacing='0' cellpadding='3' style='border-collapse:collapse;font-size:9pt;border-style:solid;border-width:2;border-color:#AAAAAA'>";
$_h.="<tr bgcolor='#DDDDDD' style='color:#808080;font-size:8pt;font-weight:bold'>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Date</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Item</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Code</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Qty</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Unit</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Price</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Dscnt</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>VAT</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Total</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Paid</td>";
$_h.="</tr>";

$tq=0;
$tu=0;
$pay_ref="";

$sql="SELECT * FROM nse_invoice_item WHERE j_invit_invoice=";
$sql.=($gin["j_inv_type"]==9 && $gin["j_inv_invoice_id"]?$gin["j_inv_invoice_id"]:$xid);
$sql.=" ORDER BY j_invit_date,j_invit_name";
$rin=mysql_query($sql);
if(mysql_num_rows($rin))
{
	while($itm=mysql_fetch_array($rin))
	{
		$_h.="<tr>";
		$_h.="<td bgcolor='#DDF7F7' style='border-style:solid;border-width:1;border-color:#CCCCCC'>".($itm["j_invit_date"]?date("d/m/Y",$itm["j_invit_date"]):"")."</td>";
		$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'><b>".$itm["j_invit_name"]."</b></td>";
		$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>".$itm["j_invit_code"]."</td>";
		$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#DDE9F7'>".$itm["j_invit_quantity"]."</td>";
		$tq+=$itm["j_invit_quantity"];
		$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#E9E9E9'>".number_format($itm["j_invit_price"],2,"."," ")."</td>";
		$tu+=$itm["j_invit_price"];
		$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7F7DD'>".number_format(($itm["j_invit_quantity"]*$itm["j_invit_price"]),2,"."," ")."</td>";
		$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7E9DD'>".number_format($itm["j_invit_discount"],2,"."," ")."</td>";
		$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7DDDD'>".number_format($itm["j_invit_vat"],2,"."," ")."</td>";
		$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#DDF7DD'><b>".number_format($itm["j_invit_total"],2,"."," ")."</b></td>";
		$pp=0;
		$rip=mysql_query("SELECT * FROM nse_invoice_item_paid WHERE j_invp_invoice_item=".$itm["j_invit_id"]);
		if(mysql_num_rows($rip))
		{
			while($pid=mysql_fetch_array($rip))
			{
				if($pid["j_invp_paid"] && ($pid["j_invp_date"] || $pid["j_invp_bank"] || $pid["j_invp_reference"]))
					$pay_ref.=($pid["j_invp_date"]?date("d/m/Y",$pid["j_invp_date"])." | ":"").$gin["j_inv_currency"]." ".number_format($pid["j_invp_paid"],2,".","").($pid["j_invp_bank"]?" | ".$pid["j_invp_bank"]:"").($pid["j_invp_reference"]?" | ".$pid["j_invp_reference"]:"").", ";
				$pp+=$pid["j_invp_paid"];
			}
		}
		$_h.="<td style='".($pp<$itm["j_invit_total"]?"color:#FF0000;":"")."text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7DDDD'>".($pp?number_format($pp,2,"."," "):"0.00")."</td>";
		$_h.="</tr>";
	}
}

$_h.="<tr style='font-size:10pt;font-weight:bold'>";
$_h.="<td colspan=3 style='text-align:right;font-size:8pt;color:#CCCCCC;border-style:solid;border-width:1;border-color:#CCCCCC'>Currency: ".$gin["j_inv_currency"]." ~ VAT: ".$gin["j_inv_vat"]."%</td>";
$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#DDE9F7'>".$tq."</td>";
$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#E9E9E9'>".str_replace(" ","&nbsp;",number_format($tu,2,"."," "))."</td>";
$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7F7DD'>".str_replace(" ","&nbsp;",number_format($gin["j_inv_total_price"],2,"."," "))."</td>";
$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7E9DD'>".str_replace(" ","&nbsp;",number_format($gin["j_inv_total_discount"],2,"."," "))."</td>";
$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7DDDD'>".str_replace(" ","&nbsp;",number_format($gin["j_inv_total_vat"],2,"."," "))."</td>";
$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#DDF7DD'>".str_replace(" ","&nbsp;",number_format($gin["j_inv_total"],2,"."," "))."</td>";
$_h.="<td style='".($gin["j_inv_paid"]<$gin["j_inv_total"]?"color:#FF0000;":"")."text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#F7DDDD'>".str_replace(" ","&nbsp;",number_format($gin["j_inv_paid"],2,"."," "))."</td>";
$_h.="</tr>";

$_h.="</table>";
$_h.="<br>";

if($pay_ref)
	$_h.="<span style='font-size:8pt;color:#808080'><b>Payment Reference</b>&nbsp; ".trim($pay_ref,", ")."</span><br>";
$_h.="<br>";

$_h.="<span style='font-size:8pt'>";

if($gin["j_inv_comment"])
	$_h.="<span style='font-size:9pt'><b>Comments:</b> ".$gin["j_inv_comment"]."</span><br><br>";

if($gin["j_inv_condition"])
{
	$condition=J_inv_getele($gin["j_inv_condition"]);
	if($condition)
	{
		$condition=explode("~",$condition);
		$_h.="<span>".$condition[2]."</span><br><br>";
	}
}

if($gin["j_inv_bank"])
{
	$bank=J_inv_getele($gin["j_inv_bank"]);
	if($bank)
	{
		$bank=explode("~",$bank);
		$_h.="<span>".$bank[2]."</span><br><br>";
	}
}

if($gin["j_inv_foot"])
{
	$foot=J_inv_getele($gin["j_inv_foot"]);
	if($foot)
	{
		$foot=explode("~",$foot);
		$_h.="<span>".$foot[2]."</span><br><br>";
	}
}

$_h.="</span>";

$_h.="</body>";
$_h.="</html>";

//echo $_h;

$pdf_html=$_h;

include $SDR."/utility/dompdf/pdf.php";
?>