<?php
include_once $SDR."/apps/accounts/invoices/get.php";

// in case there is no variables
$invoice_default_company=0;
$invoice_default_company_info="0~AA Travel Guides~Johannesburg~info@aatravel.co.za~011 713-2000~011 728-3086~P.O.Box 787040<br>Sandton<br>2146<br>Johannesburg~11 1st Street<br>Abbottsford<br>2192<br>Johannesburg~~198900510807~4110116797";
$invoice_default_assessor=(isset($_SESSION["j_user"]) && $_SESSION["j_user"]["role"]=="a"?$_SESSION["j_user"]["id"]:0);
$invoice_default_from_person=isset($_SESSION["j_user"]["id"])?$_SESSION["j_user"]["id"]:0;
$invoice_default_rep=(isset($_SESSION["j_user"]) && $_SESSION["j_user"]["role"]=="r"?$_SESSION["j_user"]["id"]:0);
$invoice_invoice_default_assessor=0;
$invoice_default_from_contact=3180;
$invoice_default_order_contact=3180;
$invoice_default_to_person=3605;
$invoice_default_to_ad_person=326;
$invoice_default_from_ad_person=3223;
$invoice_default_increment=1;
$invoice_default_order_increment=2;
$invoice_default_creditnote_increment=3;
$invoice_default_debitnote_increment=4;
$invoice_default_vat=14;
$invoice_default_currency="ZAR";
$invoice_default_order_head=9;
$invoice_default_creditnote_head=3;
$invoice_default_debitnote_head=4;
$invoice_default_order_condition=114;
$invoice_default_order_ads_condition=116;
$invoice_default_lock=60;
$invoice_default_head=1;
$invoice_default_condition=10;
$invoice_default_bank=11;
$invoice_default_foot=12;
$time_lock=mktime(date("H"),date("i"),date("s"),date("m"),date("d")-$invoice_default_lock,date("Y"));
// end
include_once $SDR."/apps/accounts/invoices/types.php";

// get db variables
$array=array();
$q="SELECT * FROM nse_variables WHERE variable_name LIKE 'invoice_default_%'";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
		$array[$g["variable_name"]]=$g["variable_value"];
}
extract($array); // convert array to variables


// get variables into javascript parent
if(isset($_SESSION["juno"]) && !isset($_SESSION["juno"]["accounts"]))
{
	$_SESSION["juno"]["accounts"]=array();
	include_once $SDR."/system/get.php";

	$invoice_default_head=0;
	$invoice_default_condition=0;
	$invoice_default_bank=0;
	$invoice_default_foot=0;

	$i=1;
	while($i<5)
	{
		$r=mysql_query("SELECT j_invele_id FROM nse_invoice_element WHERE j_invele_company=".$invoice_default_company." AND j_invele_type=".$i." AND j_invele_default=1 LIMIT 1");
		if(mysql_num_rows($r))
		{
			$g=mysql_fetch_array($r);
			$g=$g["j_invele_id"];
			if($i==1)$invoice_default_head=$g;
			elseif($i==2)$invoice_default_condition=$g;
			elseif($i==3)$invoice_default_bank=$g;
			elseif($i==4)$invoice_default_foot=$g;
		}
		$i++;
	}

	$_SESSION["juno"]["accounts"]["default_head"]=$invoice_default_head;
	$_SESSION["juno"]["accounts"]["default_condition"]=$invoice_default_condition;
	$_SESSION["juno"]["accounts"]["default_bank"]=$invoice_default_bank;
	$_SESSION["juno"]["accounts"]["default_foot"]=$invoice_default_foot;

	echo "<script src=".$ROOT."/apps/accounts/invoices/default.js></script>";
	echo "<script>J_inv_ld(";
	echo $invoice_default_company;
	echo ",".$invoice_default_from_person;
	echo ",".$invoice_invoice_default_assessor;
	echo ",".$invoice_default_rep;
	echo ",".$invoice_default_to_person;
	echo ",".$invoice_default_to_ad_person;
	echo ",".$invoice_default_from_contact;
	echo ",".$invoice_default_order_contact;
	echo ",".$invoice_default_head;
	echo ",".$invoice_default_order_head;
	echo ",".$invoice_default_creditnote_head;
	echo ",".$invoice_default_debitnote_head;
	echo ",".$invoice_default_condition;
	echo ",".$invoice_default_order_condition;
	echo ",".$invoice_default_order_ads_condition;
	echo ",".$invoice_default_bank;
	echo ",".$invoice_default_foot;
	echo ",".$invoice_default_vat;
	echo ",\"".$invoice_default_currency."\"";
	echo ",\"".($invoice_default_company?J_inv_getto($invoice_default_company):$invoice_default_company_info)."\"";
	echo ",\"";
	$t=J_inv_getpeo($invoice_default_from_person);if($t)echo $t."|";
	$t=J_inv_getpeo($invoice_default_to_person);if($t)echo $t."|";
	$t=J_inv_getpeo($invoice_default_to_ad_person);if($t)echo $t."|";
	$t=J_inv_getpeo($invoice_default_assessor);if($t)echo $t."|";
	$t=J_inv_getpeo($invoice_default_rep);if($t)echo $t."|";
	$t=J_inv_getpeo($invoice_default_from_contact);if($t)echo $t."|";
	$t=J_inv_getpeo($invoice_default_order_contact);if($t)echo $t."|";
	echo "\"";
	echo ",\"";
	$t=J_inv_getele($invoice_default_head,1);if($t)echo $t."|";
	$t=J_inv_getele($invoice_default_order_head,1);if($t)echo $t."|";
	$t=J_inv_getele($invoice_default_creditnote_head,1);if($t)echo $t."|";
	$t=J_inv_getele($invoice_default_debitnote_head,1);if($t)echo $t."|";
	echo "\"";
	echo ",\"";
	$t=J_inv_getele($invoice_default_condition,2);if($t)echo $t."|";
	$t=J_inv_getele($invoice_default_order_condition,2);if($t)echo $t."|";
	$t=J_inv_getele($invoice_default_order_ads_condition,2);if($t)echo $t."|";
	echo "\"";
	echo ",\"".J_inv_getele($invoice_default_bank,3)."\"";
	echo ",\"";
	$t=J_inv_getele($invoice_default_foot,4);if($t)echo $t."|";
	echo "\"";
	function getInvOpt($t)
	{
		$v="";
		$r=mysql_query("SELECT * FROM nse_invoice_element WHERE j_invele_type=".$t." ORDER BY j_invele_company,j_invele_title");
		if(mysql_num_rows($r))
		{
			$c=-1;
			while($g=mysql_fetch_array($r))
			{
				if($c!=$g["j_invele_company"])
				{
					$c=$g["j_invele_company"];
					$v.="~".J_Value("name","establishment","id",$g["j_invele_company"])."|";
				}
				$v.=$g["j_invele_id"]."~".$g["j_invele_title"]."|";
			}
		}
		return $v;
	}
	echo ",\"".getInvOpt(1)."\"";
	echo ",\"".getInvOpt(2)."\"";
	echo ",\"".getInvOpt(3)."\"";
	echo ",\"".getInvOpt(4)."\"";
	$v=0;
	if($invoice_default_company)
	{
		$r=mysql_query("SELECT j_invinc_id FROM nse_invoice_increment WHERE j_invinc_default=1 AND j_invinc_company=".$invoice_default_company." LIMIT 1");
		if(mysql_num_rows($r))
		{
			$r=mysql_fetch_array($r);
			$v=$r["j_invinc_id"];
		}
	}
	echo ",".$v;
	$v="";
	$r=mysql_query("SELECT * FROM nse_invoice_increment ORDER BY j_invinc_company,j_invinc_name");
	if(mysql_num_rows($r))
	{
		$t=-1;
		while($g=mysql_fetch_array($r))
		{
			if($t!=$g["j_invinc_company"])
			{
				$t=$g["j_invinc_company"];
				$v.="~".J_Value("name","establishment","id",$g["j_invinc_company"])."|";
			}
			$v.=$g["j_invinc_id"]."~".$g["j_invinc_name"]."|";
		}
	}
	echo ",\"".$v."\"";
	echo ",\"".$invoice_default_order_increment."\"";
	echo ")</script>";
}
?>