<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/parse.php";
// constrain_user_establishments functions
include $SDR."/custom/constrain_user_establishments.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$d="";

$dd=explode("~",$_GET["d"]."cou~prv~twn~id~est~date~ass~num~con~condet~price~disc~vat~total~paid~paydate");
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","cou","prv","twn","id","est","date","ass","num","con","condet","price","disc","vat","total","paid","paydate");
$d="";$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}

$s="SELECT ";
$s.="DISTINCT a.assessment_id";
$s.=",a.assessment_date";
$s.=",a.establishment_code";
$s.=",a.invoice_id";
$s.=",e.establishment_name";
$s.=",e.assessor_id";
$s.=",l.country_id";
$s.=",l.province_id";
$s.=",l.town_id";
$s.=",i.j_inv_id";
$s.=",i.j_inv_number";
$s.=" FROM nse_establishment_assessment AS a ";
$s.="JOIN nse_establishment AS e ON a.establishment_code=e.establishment_code ";
$s.="LEFT JOIN nse_establishment_location AS l ON a.establishment_code=l.establishment_code ";
$s.="JOIN nse_invoice AS i ON a.invoice_id=i.j_inv_id ";
$s.="WHERE i.j_inv_type<6 ";
if(isset($_GET["est"]))
	$s.="AND a.establishment_code='".$_GET["est"]."' ";
if(isset($_GET["ds"]))
	$s.="AND a.assessment_date>='".substr($_GET["ds"],6)."-".substr($_GET["ds"],3,2)."-".substr($_GET["ds"],0,2)." 00:00:00' ";
if(isset($_GET["de"]))
	$s.="AND a.assessment_date>='".substr($_GET["de"],6)."-".substr($_GET["de"],3,2)."-".((substr($_GET["de"],0,2)*1)+1)." 00:00:00' ";
if(isset($_GET["cou"]))
	$s.="AND l.country_id=".$_GET["cou"]." ";
if(isset($_GET["prv"]))
	$s.="AND l.province_id=".$_GET["prv"]." ";
if(isset($_GET["pay"]))
	$s.="AND ".($_GET["pay"]==1?"i.j_inv_paid=0":($_GET["pay"]==2?"(i.j_inv_paid!=0 AND i.j_inv_paid<i.j_inv_total)":"j_inv_paid!=0"))." ";
if(isset($_GET["can"]))
	$s.="AND j_inv_cancel_user".($_GET["can"]==1?"=":">")."0 ";
if(isset($_GET["cn"]))
	$s.="AND i.j_inv_cancel_user".($_GET["cn"]==1?"=":">")."0 ";
$jL0=mysql_num_rows(mysql_query($s));
$s.="ORDER BY a.assessment_date DESC ";
$s.="LIMIT ".$jL1.",".$jL2;
$rs=mysql_query($s);
$r="";
$c="";
$p="";
$t="";
$e="";
$a="";

if(mysql_num_rows($rs))
{

	if(isset($_GET["csv"]))
	{
		$hed="";
		$tot="";
		$i=0;
		$c=0;
		$t_up=0;
		$t_ds=0;
		$t_vt=0;
		$t_tt=0;
		$t_tp=0;
	}
	else
	{
		$cc=array();
		$pp=array();
		$tt=array();
		$ee=array();
		$aa=array();
	}

	include_once $SDR."/system/get.php";

	$inventory="";
	$q=mysql_query("SELECT j_in_id FROM nse_inventory WHERE j_in_id!=1 AND j_in_category=5");
	while($g=mysql_fetch_array($q))
		$inventory.=($inventory?" OR ":"")." j_invit_inventory=".$g["j_in_id"];

	function get_paid($invoice_id=0)
	{
		global $inventory;
		$p=array(0,0,0,0,0,"");
		$q="SELECT * FROM nse_invoice_item
		LEFT JOIN nse_invoice_item_paid ON j_invit_id=j_invp_invoice_item
		JOIN nse_inventory ON j_invit_inventory=j_in_id
		WHERE j_invit_invoice=".$invoice_id."
		AND (".$inventory.")
		ORDER BY j_invp_paid";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			$d=0;
			while($g=mysql_fetch_array($q))
			{
				$p[0]+=$g["j_invit_price"];
				$p[1]+=$g["j_invit_discount"];
				$p[2]+=$g["j_invit_vat"];
				$p[3]+=$g["j_invit_total"];
				$p[4]+=$g["j_invp_paid"];
			}
		}
		return $p;
	}

	include $SDR."/custom/lib/est_items.php";

	while($g=mysql_fetch_array($rs))
	{
		$m=get_paid($g["assessment_id"]); // get payments for assessments only

		if(isset($_GET["csv"]))
		{
			$r.=",".str_replace(array(",","\n")," ",J_Value("country_name","nse_nlocations_countries","country_id",$g["country_id"]));
			$r.=",".str_replace(array(",","\n")," ",J_Value("province_name","nse_nlocations_provinces","province_id",$g["province_id"]));
			$r.=",".str_replace(array(",","\n")," ",J_Value("town_name","nse_nlocations_towns","town_id",$g["town_id"]));
			$r.=",".$g["establishment_code"];
			$r.=",".str_replace(array(",","\n")," ",$g["establishment_name"]);
			$r.=",".str_replace("-","/",substr($g["assessment_date"],0,10));
			$r.=",".str_replace(array(",","\n")," ",J_Value("","people","",$g["assessor_id"]));
			$r.=",".str_replace(array(",","\n")," ",$g["j_inv_number"]);
			$eid=$g["establishment_code"];
			$no_js=1;
			include $SDR."/custom/lib/get_est_people.php";
			$tpd=($accounts?$accounts:($manager?$manager:($owner?$owner:$anybody)));
			$r.=",".str_replace(array(",","\n")," ",J_Value("","people","",$tpd));
			$r.=",".str_replace(array(",","\n")," ",numbers($g["establishment_code"],$tpd));
			$r.=",".number_format($m[0],2,"."," ");
			$r.=",".number_format($m[1],2,"."," ");
			$r.=",".number_format($m[2],2,"."," ");
			$r.=",".number_format($m[3],2,"."," ");
			$r.=",".number_format($m[4],2,"."," ");
			$r.=",".$m[5];
			$r.="\n";
			$t_up+=$m[0];
			$t_ds+=$m[1];
			$t_vt+=$m[2];
			$t_tt+=$m[3];
			$t_tp+=$m[4];
		}
		else
		{
			$r.=$g["assessment_id"]."~";
			$r.=($g["country_id"]?$g["country_id"]:"")."~";
			if(!isset($cc[$g["country_id"]]))
				$cc[$g["country_id"]]=J_Value("country_name","nse_nlocations_countries","country_id",$g["country_id"]);
			$r.=($g["province_id"]?$g["province_id"]:"")."~";
			if(!isset($pp[$g["province_id"]]))
				$pp[$g["province_id"]]=J_Value("province_name","nse_nlocations_provinces","province_id",$g["province_id"])."~";
			$r.=($g["town_id"]?$g["town_id"]:"")."~";
			if(!isset($pp[$g["town_id"]]))
				$tt[$g["town_id"]]=J_Value("town_name","nse_nlocations_towns","town_id",$g["town_id"])."~";
			$r.=$g["establishment_code"]."~";
			$r.="~";
			if(!isset($ee[$g["establishment_code"]]))
				$ee[$g["establishment_code"]]=$g["establishment_name"];
			$r.=str_replace("-","/",substr($g["assessment_date"],0,10))."~";
			$r.=($g["assessor_id"]?$g["assessor_id"]:"")."~";
			if(!isset($aa[$g["assessor_id"]]))
				$aa[$g["assessor_id"]]=J_Value("","people","",$g["assessor_id"]);
			$r.=$g["j_inv_number"]."~";
			$eid=$g["establishment_code"];
			$no_js=1;
			include $SDR."/custom/lib/get_est_people.php";
			$tpd=($accounts?$accounts:($manager?$manager:($owner?$owner:$anybody)));
			$r.=$tpd."~";
			$aa[$tpd]=J_Value("","people","",$tpd);
			$r.=numbers($g["establishment_code"],$tpd)."~";
			$r.=$m[0]."~";
			$r.=$m[1]."~";
			$r.=$m[2]."~";
			$r.=$m[3]."~";
			$r.=$m[4]."~";
			$r.=$m[5]."~";
			$r.=$g["invoice_id"]."~";
			$r.="|";
		}
	}
	if(isset($_GET["csv"]))
	{
		$hed.=",COUNTRY";
		$hed.=",PROVINCE";
		$hed.=",TOWN";
		$hed.=",CODE";
		$hed.=",ESTABLISHMENT";
		$hed.=",DATE";
		$hed.=",ASSESSOR";
		$hed.=",INVOICE";
		$hed.=",CONTACT";
		$hed.=",PHONE";
		$hed.=",PRICE";
		$hed.=",DISCOUNT";
		$hed.=",VAT";
		$hed.=",TOTAL";
		$hed.=",PAID";
		$hed.=",PAID DATE";
		$c=$c+11;
		$i=1;
		while($i<$c)
		{
			$tot.=",";
			$i++;
		}
		$tot.=",".number_format($t_up,2,"."," ");
		$tot.=",".number_format($t_ds,2,"."," ");
		$tot.=",".number_format($t_vt,2,"."," ");
		$tot.=",".number_format($t_tt,2,"."," ");
		$tot.=",".number_format($t_tp,2,"."," ");
		$tot.="\n";
		$csv=$hed."\n".$r.$tot;
	}
	else
	{
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($cc as $k => $v){$c.=$k."~".$v."|";}
		foreach($pp as $k => $v){$p.=$k."~".$v."|";}
		foreach($tt as $k => $v){$t.=$k."~".$v."|";}
		foreach($ee as $k => $v){$e.=$k."~".$v."|";}
		foreach($aa as $k => $v){$a.=$k."~".$v."|";}
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"ACC_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	include $SDR."/system/get.php";
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/accounts/invoices/ass_res.js></script>";
	echo "<script>J_inv_r(\"".$r."\",\"".$d."\",\"".$c."\",\"".$p."\",\"".$t."\",\"".$e."\",\"".$a."\",".$jL0.",".$jL1.",".$jL2.($_SESSION["j_user"]["role"]=="s"||$_SESSION["j_user"]["role"]=="b"||$_SESSION["j_user"]["role"]=="d"||J_getSecure()?",1":"").")</script>";
}
?>