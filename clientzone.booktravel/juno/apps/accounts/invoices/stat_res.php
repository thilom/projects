<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/parse.php";
include_once $SDR."/system/get.php";
// constrain_user_establishments functions
include $SDR."/custom/constrain_user_establishments.php";

$r=$_SESSION["j_user"]["role"];
$rep=0;
if($r=="a"||$r=="r")
	$rep=$_SESSION["j_user"]["id"];

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$date=isset($_GET["ds"])?J_dateParse($_GET["ds"]):$EPOCH;
$y=date("Y",$date);
$m=date("m",$date);
$curr_s=mktime(0,0,0,date($m)+1,date(1),date($y));
$curr_e=mktime(0,0,0,date($m),date(1),date($y));
$d30=mktime(0,0,0,date($m)-1,date(1),date($y));
$d60=mktime(0,0,0,date($m)-2,date(1),date($y));
$d90=mktime(0,0,0,date($m)-3,date(1),date($y));
$d="";
$r="";

$ttcur=0;
$tt30=0;
$tt60=0;
$tt90=0;
$tt120=0;

$s="SELECT
DISTINCT establishment_code
,establishment_name
,assessor_id
,rep_id1
FROM nse_invoice
JOIN nse_establishment ON j_inv_to_company=establishment_code
LEFT JOIN nse_invoice_item_paid ON j_inv_id=j_invp_invoice
WHERE (j_inv_date<".$curr_s." OR j_invp_date<".$curr_s.")";
if(isset($_GET["tp"]))
	$s.=" AND j_inv_type=1";
else
	$s.=" AND (j_inv_type=2 OR j_inv_type=3 OR j_inv_type=4)";
if(isset($_GET["est"]))
	$s.=" AND establishment_code='".$_GET["est"]."'";
elseif($r=="c") // constrain user establishments
	$s.=user_establishments_constrain(1,"j_inv_to_company");
if($rep)
	$s.=" AND (assessor_id=".$rep." OR rep_id1=".$rep." OR j_inv_from_assessor=".$rep." OR j_inv_from_rep=".$rep." OR j_inv_from_rep2=".$rep.")";
else if(isset($_GET["ass"]))
	$s.=" AND (assessor_id=".$_GET["ass"]." OR rep_id1=".$_GET["ass"]." OR j_inv_from_assessor=".$_GET["ass"]." OR j_inv_from_rep=".$_GET["ass"]." OR j_inv_from_rep2=".$_GET["ass"].")";
$jL0=mysql_num_rows(mysql_query($s));
$s.=" ORDER BY establishment_name";
$s.=" LIMIT ".$jL1.",".$jL2;
//echo $s."<hr>";
$rs=mysql_query($s);
$p="";

if(mysql_num_rows($rs))
{
	$dd=explode("~",$_GET["d"]."id~est~d120a~d120b~d90a~d90b~d60a~d60b~d30a~d30b~curra~currb~owe~paid~last");
	$ddd=array();
	$s=array();
	foreach($dd as $k => $v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
	$t=array("X","id","est","d120a","d120b","d90a","d90b","d60a","d60b","d30a","d30b","curra","currb","owe","paid","pay","paydate","qai","qao","vis","ass","rep","con","condet","ema","last");
	$d="";$n=1;
	foreach($t as $k => $v)
	{
		if(isset($ddd[$v]))
		{
			$d.=$n;
			$n++;
		}
		$d.="~";
	}

	$ema="";

	include $SDR."/custom/lib/est_items.php";

	function paid($e,$ds=0,$de=0)
	{
		global $last;
		$p=0;
		$q="SELECT j_invp_paid,j_invp_date,j_invp_invoice FROM nse_invoice_item_paid JOIN nse_invoice ON j_invp_invoice=j_inv_id WHERE j_inv_to_company='".$e."' AND j_invp_date>".($ds?$ds:1)." AND j_invp_date<".$de." ORDER BY j_invp_date";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
		 while($g=mysql_fetch_array($q))
			{
				$p+=$g["j_invp_paid"];
			 $last=array($g["j_invp_invoice"],date("d/m/Y",$g["j_invp_date"]));
		 }
		}
		return $p;
	}

	if(isset($_GET["csv"]))
	{
	}
	else
		$pp=array();

	while($g=mysql_fetch_array($rs))
	{

		if(!isset($_GET["csv"]))
		{
			$r.="~";
			$r.=$g["establishment_code"]."~";
			$r.=$g["establishment_name"]."~";
			$to=0;
			$tp=0;
			$last=array(0,"");
			// 120+ days
			$t=0;
			$pd=paid($g["establishment_code"],0,$d90);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date<".$d90);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"")."~";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"")."~";
			// 90 days
			$t=0;
			$pd=paid($g["establishment_code"],$d90,$d60);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d90." AND j_inv_date<".$d60);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"")."~";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"")."~";
			// 60 days
			$t=0;
			$pd=paid($g["establishment_code"],$d60,$d30);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d60." AND j_inv_date<".$d30);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"")."~";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"")."~";
			// 30 days
			$t=0;
			$pd=paid($g["establishment_code"],$d30,$curr_e);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d30." AND j_inv_date<".$curr_e);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"")."~";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"")."~";
			// current
			$t=0;
			$pd=paid($g["establishment_code"],$curr_e,$curr_s);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$curr_e." AND j_inv_date<".$curr_s);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"")."~";
			$r.=($pd?number_format(0-$pd,2,".",""):"")."~";
			$r.=($to?number_format($to,2,".",""):"")."~";
			$r.=($tp?number_format(0-$tp,2,".",""):"")."~";
			if(isset($s["pay"]))
			{
				if($last[0])
				{
					$gp=mysql_fetch_array(mysql_query("SELECT j_inv_paid FROM nse_invoice WHERE j_inv_id=".$last[0]." LIMIT 1"));
					$r.=($gp["j_inv_paid"]?number_format($gp["j_inv_paid"],2,".",""):"");
				}
				$r.="~";
			}
			if(isset($s["paydate"]))
				$r.=$last[1]."~";
			if(isset($s["qai"]))
				$r.=entered($g["establishment_code"])."~";
			if(isset($s["qao"]))
				$r.=qa_out($g["establishment_code"])."~";
			if(isset($s["vis"]))
				$r.=visit($g["establishment_code"])."~";
			if(isset($s["ass"]))
			{
				$u=($g["assessor_id"]?$g["assessor_id"]:"");
				$r.=$u."~";
				if($u && !isset($pp[$u]))
					$pp[$u]=J_Value("","people","",$u);
			}
			if(isset($s["rep"]))
			{
				$u=($g["rep_id1"]?$g["rep_id1"]:"");
				$r.=$u."~";
				if($u && !isset($pp[$u]))
					$pp[$u]=J_Value("","people","",$u);
			}
			if(isset($s["con"]) || isset($s["condet"]) || isset($s["ema"]))
			{
				$ema="";
				$eid=$g["establishment_code"];
				$no_js=1;
				include $SDR."/custom/lib/get_est_people.php";
				$tpd=($accounts?$accounts:($manager?$manager:($owner?$owner:$anybody)));
				if(isset($s["con"]))
				{
					$r.=$tpd."~";
						$pp[$tpd]=J_Value("","people","",$tpd)."~".numbers($g["establishment_code"],$tpd);
				}
				if(isset($s["condet"]))
					$r.="~";
				if(isset($s["ema"]))
					$r.=$ema."~";
			}
			$r.=($last[0]?$last[0]:"")."~";
			$r.="|";
		}
		else
		{
			$r.=$g["establishment_code"].",";
			$r.=$g["establishment_name"].",";
			$to=0;
			$tp=0;
			$last=array(0,"");
			// 120+ days
			$t=0;
			$pd=paid($g["establishment_code"],0,$d90);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date<".$d90);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"").",";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"").",";
			// 90 days
			$t=0;
			$pd=paid($g["establishment_code"],$d90,$d60);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d90." AND j_inv_date<".$d60);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"").",";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"").",";
			// 60 days
			$t=0;
			$pd=paid($g["establishment_code"],$d60,$d30);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d60." AND j_inv_date<".$d30);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"").",";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"").",";
			// 30 days
			$t=0;
			$pd=paid($g["establishment_code"],$d30,$curr_e);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d30." AND j_inv_date<".$curr_e);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?(number_format($t,2,".","")*1):"").",";
			$r.=($pd?(number_format(0-$pd,2,".","")*1):"").",";
			// current
			$t=0;
			$pd=paid($g["establishment_code"],$curr_e,$curr_s);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$curr_e." AND j_inv_date<".$curr_s);
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"").",";
			$r.=($pd?number_format(0-$pd,2,".",""):"").",";
			$r.=($to?number_format($to,2,".",""):"").",";
			$r.=($tp?number_format(0-$tp,2,".",""):"").",";
			if(isset($s["pay"]))
			{
				if($last[0])
				{
					$gp=mysql_fetch_array(mysql_query("SELECT j_inv_paid FROM nse_invoice WHERE j_inv_id=".$last[0]." LIMIT 1"));
					$r.=($gp["j_inv_paid"]?number_format($gp["j_inv_paid"],2,".",""):"");
				}
				$r.=",";
			}
			if(isset($s["paydate"]))
				$r.=$last[1].",";
			if(isset($s["qai"]))
				$r.=entered($g["establishment_code"]).",";
			if(isset($s["qao"]))
				$r.=qa_out($g["establishment_code"]).",";
			if(isset($s["vis"]))
				$r.=visit($g["establishment_code"]).",";
			if(isset($s["ass"]))
			{
				$u=($g["assessor_id"]?$g["assessor_id"]:"");
				$r.=$u.",";
				if($u && !isset($pp[$u]))
					$pp[$u]=J_Value("","people","",$u);
			}
			if(isset($s["rep"]))
			{
				$u=($g["rep_id1"]?$g["rep_id1"]:"");
				$r.=$u.",";
				if($u && !isset($pp[$u]))
					$pp[$u]=J_Value("","people","",$u);
			}
			if(isset($s["con"]) || isset($s["condet"]) || isset($s["ema"]))
			{
				$ema="";
				$eid=$g["establishment_code"];
				$no_js=1;
				include $SDR."/custom/lib/get_est_people.php";
				$tpd=($accounts?$accounts:($manager?$manager:($owner?$owner:$anybody)));
				if(isset($s["con"]))
				{
					$r.=$tpd.",";
						$pp[$tpd]=J_Value("","people","",$tpd).",".numbers($g["establishment_code"],$tpd);
				}
				if(isset($s["condet"]))
					$r.=",";
				if(isset($s["ema"]))
					$r.=$ema.",";
			}
			$r.=($last[0]?$last[0]:"").",";
			$r.="\n";
		}
	}

	if(isset($_GET["csv"]))
	{
		$hed="CODE";
		$hed.=",ESTABLISHMENT";
		$hed.=",120+ DAYS";
		$hed.=",90 DAYS";
		$hed.=",60 DAYS";
		$hed.=",30 DAYS";
		$hed.=",CURRENT";
		$hed.=",OWED";
		if(isset($s["paid"]))
			$hed.=",PAID";
		if(isset($s["pay"]))
			$hed.=",LAST PAID";
		if(isset($s["paydate"]))
			$hed.=",PAID DATE";
		if(isset($s["qai"]))
			$hed.=",QA IN";
		if(isset($s["qao"]))
			$hed.=",QA OUT";
		if(isset($s["vis"]))
			$hed.=",VISITED";
		if(isset($s["ass"]))
			$hed.=",ASSESSOR";
		if(isset($s["rep"]))
			$hed.=",REP";
		if(isset($s["con"]))
			$hed.=",CONTACT";
		if(isset($s["condet"]))
			$hed.=",TEL";
		if(isset($s["ema"]))
			$hed.=",EMAIL";
		$csv=$hed."\n".$r;
	}
	else
	{
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($pp as $k => $v){$p.=$k."~".$v."|";}
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"AGE_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/accounts/invoices/stat_res.js></script>";
	echo "<script>J_inv_r(\"".$r."\",\"".date("Y/m/d",$date)."\",\"".date("Y/m/d",$curr_s)."\",\"".$d."\",\"".$p."\",".$jL0.",".$jL1.",".$jL2.($_SESSION["j_user"]["role"]=="s"||$_SESSION["j_user"]["role"]=="b"||$_SESSION["j_user"]["role"]=="d"?",1":"").")</script>";
}
?>