<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/parse.php";
include_once $SDR."/system/get.php";
// constrain_user_establishments functions
include $SDR."/custom/constrain_user_establishments.php";

$r=$_SESSION["j_user"]["role"];
$rep=0;
if($r=="a"||$r=="r")
	$rep=$_SESSION["j_user"]["id"];

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$date=isset($_GET["ds"])?J_dateParse($_GET["ds"]):$EPOCH;
$y=date("Y",$date);
$m=date("m",$date);
$curr_s=mktime(0,0,0,date($m)+1,date(1),date($y));
$curr_e=mktime(0,0,0,date($m),date(1),date($y));
$d30=mktime(0,0,0,date($m)-1,date(1),date($y));
$d60=mktime(0,0,0,date($m)-2,date(1),date($y));
$d90=mktime(0,0,0,date($m)-3,date(1),date($y));
$d="";
$r="";

$fixed=isset($_GET["fix"])?1:0;

$ttcur=0;
$tt30=0;
$tt60=0;
$tt90=0;
$tt120=0;

$s="SELECT
DISTINCT e.establishment_code
,e.establishment_name
,e.assessor_id
,e.rep_id1
FROM nse_invoice AS i
LEFT JOIN nse_invoice_item_paid AS p ON i.j_inv_id=p.j_invp_invoice
JOIN nse_establishment AS e ON i.j_inv_to_company=e.establishment_code";
if(isset($_GET["rd"])) // exclude date irrelevant renewals
	$s.=" LEFT JOIN nse_establishment_assessment AS a ON i.j_inv_id=a.invoice_id";
$s.=" WHERE i.j_inv_type<6
 AND (i.j_inv_date<".$curr_s." OR p.j_invp_date<".$curr_s.")";
if($fixed)
	$s.=" AND i.j_inv_created<".$curr_s;
if(isset($_GET["rd"])) // exclude date irrelevant renewals
	$s.=" AND (a.assessment_id='' OR DATE_FORMAT(a.assessment_date,'%Y-%m-%d 00:00:00')<='".date("Y-m-d 00:00:00",$curr_s)."')";
if(isset($_GET["est"]))
	$s.=" AND e.establishment_code='".$_GET["est"]."'";
if($rep) // isolate on rep
	$s.=" AND (e.assessor_id=".$rep." OR e.rep_id1=".$rep." OR i.j_inv_from_assessor=".$rep." OR i.j_inv_from_rep=".$rep." OR i.j_inv_from_rep2=".$rep.")";
else if(isset($_GET["ass"])) // isolate assessors
	$s.=" AND (e.assessor_id=".$_GET["ass"]." OR e.rep_id1=".$_GET["ass"]." OR i.j_inv_from_assessor=".$_GET["ass"]." OR i.j_inv_from_rep=".$_GET["ass"]." OR i.j_inv_from_rep2=".$_GET["ass"].")";
$jL0=mysql_num_rows(mysql_query($s));
$s.=" ORDER BY e.establishment_name";
$s.=" LIMIT ".$jL1.",".$jL2;
//echo $s."<hr>";
$rs=mysql_query($s);
$p="";

if(mysql_num_rows($rs))
{
	$dd=explode("~",$_GET["d"]."id~est~d120~d90~d60~d30~curr~owe~paid~last");
	$ddd=array();
	$s=array();
	foreach($dd as $k => $v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
	$t=array("X","id","est","d120","d90","d60","d30","curr","owe","paid","pay","paydate","qai","qao","vis","ass","rep","con","condet","ema","last");
	$d="";$n=1;
	foreach($t as $k => $v)
	{
		if(isset($ddd[$v]))
		{
			$d.=$n;
			$n++;
		}
		$d.="~";
	}

	$ema="";

	include $SDR."/custom/lib/est_items.php";

	function paid($e,$ds,$de)
	{
		global $last,$fixed;
		$v=0;
		$q="SELECT j_invp_paid,j_invp_date,j_invp_invoice FROM nse_invoice_item_paid JOIN nse_invoice ON j_invp_invoice=j_inv_id WHERE j_inv_to_company='".$e."' AND j_invp_date>=".$ds." AND j_invp_date<".$de;
		if($fixed)
			$q.=" AND j_invp_created>=".$ds." AND j_invp_created<".$de;
		$q.=" ORDER BY j_invp_date";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
		 while($g=mysql_fetch_array($q))
			{
				$v+=$g["j_invp_paid"];
				$last=array($g["j_invp_invoice"],date("d/m/Y",$g["j_invp_date"]));
		 }
		}
		return $v;
	}

	if(isset($_GET["csv"]))
	{
		$hed="";
		$tot="";
		$i=0;
		$c=0;
		$t_120=0;
		$t_90=0;
		$t_60=0;
		$t_30=0;
		$t_cur=0;
		$t_owe=0;
		$t_pay=0;
	}
	else
		$pp=array();

	while($g=mysql_fetch_array($rs))
	{

		if(!isset($_GET["csv"]))
		{
			$r.="~";
			$r.=$g["establishment_code"]."~";
			$r.=$g["establishment_name"]."~";
			$to=0;
			$tp=0;
			$last=array(0,"");
			// 120+ days
			$t=0;
			$pd=paid($g["establishment_code"],1,$d90);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date<".$d90.($fixed?" AND j_inv_created<".$d90:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00")."~";
			// 90 days
			$t=0;
			$pd=paid($g["establishment_code"],$d90,$d60);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$d90." AND j_inv_date<".$d60.($fixed?" AND j_inv_created>=".$d90."  AND j_inv_created<".$d60:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00")."~";
			// 60 days
			$t=0;
			$pd=paid($g["establishment_code"],$d60,$d30);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$d60." AND j_inv_date<".$d30.($fixed?" AND j_inv_created>=".$d60."  AND j_inv_created<".$d30:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00")."~";
			// 30 days
			$t=0;
			$pd=paid($g["establishment_code"],$d30,$curr_e);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$d30." AND j_inv_date<".$curr_e.($fixed?" AND j_inv_created>=".$d30."  AND j_inv_created<".$curr_e:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00")."~";
			// current
			$t=0;
			$pd=paid($g["establishment_code"],$curr_e,$curr_s);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$curr_e." AND j_inv_date<".$curr_s.($fixed?" AND j_inv_created>=".$curr_e." AND j_inv_created<".$curr_s:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00")."~";
			$r.=($to?number_format($to,2,".",""):"0.00")."~";
			$r.=($tp?number_format($tp,2,".",""):"0.00")."~";
			if(isset($s["pay"]))
			{
				if($last[0])
				{
					$gp=mysql_fetch_array(mysql_query("SELECT j_inv_paid FROM nse_invoice WHERE j_inv_id=".$last[0]." LIMIT 1"));
					$r.=($gp["j_inv_paid"]?number_format($gp["j_inv_paid"],2,".",""):"");
				}
				$r.="~";
			}
			if(isset($s["paydate"]))
				$r.=$last[1]."~";
			if(isset($s["qai"]))
				$r.=entered($g["establishment_code"])."~";
			if(isset($s["qao"]))
				$r.=qa_out($g["establishment_code"])."~";
			if(isset($s["vis"]))
			{
				$v=visit($g["establishment_code"]);
				$r.=$v[0]."~";
			}
			if(isset($s["ass"]))
			{
				$u=($g["assessor_id"]?$g["assessor_id"]:"");
				$r.=$u."~";
				if($u && !isset($pp[$u]))
					$pp[$u]=J_Value("","people","",$u);
			}
			if(isset($s["rep"]))
			{
				$u=($g["rep_id1"]?$g["rep_id1"]:"");
				$r.=$u."~";
				if($u && !isset($pp[$u]))
					$pp[$u]=J_Value("","people","",$u);
			}
			if(isset($s["con"]) || isset($s["condet"]) || isset($s["ema"]))
			{
				$ema="";
				$eid=$g["establishment_code"];
				$no_js=1;
				include $SDR."/custom/lib/get_est_people.php";
				$tpd=($accounts?$accounts:($manager?$manager:($owner?$owner:$anybody)));
				if(isset($s["con"]))
				{
					$r.=$tpd."~";
						$pp[$tpd]=J_Value("","people","",$tpd)."~".numbers($g["establishment_code"],$tpd);
				}
				if(isset($s["condet"]))
					$r.="~";
				if(isset($s["ema"]))
					$r.=$ema."~";
			}
			$r.=($last?$last:"")."~";
			$r.="|";
		}
		else
		{
			$r.=$g["establishment_code"].",";
			$r.=trim(str_replace(array(",","\n")," ",$g["establishment_name"])).",";
			$to=0;
			$tp=0;
			$last=array(0,"");
			// 120+ days
			$t=0;
			$pd=paid($g["establishment_code"],1,$d90);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date<".$d90.($fixed?" AND j_inv_created<".$d90:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00").",";
			$t_120+=$t;
			// 90 days
			$t=0;
			$pd=paid($g["establishment_code"],$d90,$d60);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$d90." AND j_inv_date<".$d60.($fixed?" AND j_inv_created>=".$d90."  AND j_inv_created<".$d60:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00").",";
			$t_90+=$t;
			// 60 days
			$t=0;
			$pd=paid($g["establishment_code"],$d60,$d30);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$d60." AND j_inv_date<".$d30.($fixed?" AND j_inv_created>=".$d60."  AND j_inv_created<".$d30:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00").",";
			$t_60+=$t;
			// 30 days
			$t=0;
			$pd=paid($g["establishment_code"],$d30,$curr_e);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$d30." AND j_inv_date<".$curr_e.($fixed?" AND j_inv_created>=".$d30."  AND j_inv_created<".$curr_e:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00").",";
			$t_30+=$t;
			// current
			$t=0;
			$pd=paid($g["establishment_code"],$curr_e,$curr_s);
			$qq=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='".$g["establishment_code"]."' AND j_inv_type<6 AND j_inv_date>=".$curr_e." AND j_inv_date<".$curr_s.($fixed?" AND j_inv_created>=".$curr_e." AND j_inv_created<".$curr_s:""));
			if(mysql_num_rows($qq))
			{
				while($v=mysql_fetch_array($qq))
					$t+=$v["j_inv_total"];
			}
			$t=$t-$pd;
			$to+=$t;
			$tp+=$pd;
			$r.=($t?number_format($t,2,".",""):"0.00").",";
			$r.=($to?number_format($to,2,".",""):"0.00").",";
			$r.=($tp?number_format($tp,2,".",""):"0.00").",";
			$t_cur+=$t;
			$t_owe+=$to;
			$t_pay+=$tp;
			if(isset($s["pay"]))
			{
				if($last[0])
				{
					$gp=mysql_fetch_array(mysql_query("SELECT j_inv_paid FROM nse_invoice WHERE j_inv_id=".$last[0]." LIMIT 1"));
					$r.=($gp["j_inv_paid"]?number_format($gp["j_inv_paid"],2,".",""):"");
				}
				$r.=",";
			}
			if(isset($s["paydate"]))
				$r.=$last[1].",";
			if(isset($s["qai"]))
				$r.=entered($g["establishment_code"]).",";
			if(isset($s["qao"]))
				$r.=qa_out($g["establishment_code"]).",";
			if(isset($s["vis"]))
			{
				$v=visit($g["establishment_code"]);
				$r.=$v[0].",";
			}
			if(isset($s["ass"]))
			{
				$u=($g["assessor_id"]?$g["assessor_id"]:"");
				$r.=($u?J_Value("","people","",$u):"").",";
			}
			if(isset($s["rep"]))
			{
				$u=($g["rep_id1"]?$g["rep_id1"]:"");
				$r.=($u?J_Value("","people","",$u):"").",";
			}
			if(isset($s["con"]) || isset($s["condet"]) || isset($s["ema"]))
			{
				$ema="";
				$eid=$g["establishment_code"];
				$no_js=1;
				include $SDR."/custom/lib/get_est_people.php";
				$tpd=($accounts?$accounts:($manager?$manager:($owner?$owner:$anybody)));
				if(isset($s["con"]))
					$r.=J_Value("","people","",$tpd).",";
				if(isset($s["condet"]))
					$r.=numbers($g["establishment_code"],$tpd).",";
				if(isset($s["ema"]))
					$r.=$ema.",";
			}
			$r.="\n";
		}
	}

	if(isset($_GET["csv"]))
	{
		$hed.="CODE";
		$hed.=",ESTABLISHMENT";
		$hed.=",120+ DAYS";
		$hed.=",90 DAYS";
		$hed.=",60 DAYS";
		$hed.=",30 DAYS";
		$hed.=",CURRENT";
		$hed.=",OWED";
		if(isset($s["paid"]))
			$hed.=",PAID";
		if(isset($s["pay"]))
			$hed.=",LAST PAID";
		if(isset($s["paydate"]))
			$hed.=",PAID DATE";
		if(isset($s["qai"]))
			$hed.=",QA IN";
		if(isset($s["qao"]))
			$hed.=",QA OUT";
		if(isset($s["vis"]))
			$hed.=",VISITED";
		if(isset($s["ass"]))
			$hed.=",ASSESSOR";
		if(isset($s["rep"]))
			$hed.=",REP";
		if(isset($s["con"]))
			$hed.=",CONTACT";
		if(isset($s["condet"]))
			$hed.=",TEL";
		if(isset($s["ema"]))
			$hed.=",EMAIL";
		$csv=$hed."\n".$r."\n,,".$t_120.",".$t_90.",".$t_60.",".$t_30.",".$t_cur.",".$t_owe.",".$t_pay;
	}
	else
	{
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($pp as $k => $v){$p.=$k."~".$v."|";}
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"AGE_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/accounts/invoices/age_res.js></script>";
	echo "<script>J_inv_r(\"".$r."\",\"".date("Y/m/d",$date)."\",\"".date("Y/m/d",$curr_s)."\",\"".$d."\",\"".$p."\",".$jL0.",".$jL1.",".$jL2.",".(isset($_GET["ityp"])?$_GET["ityp"]:0).",".(isset($_GET["rd"])?1:0).",".$fixed.($_SESSION["j_user"]["role"]=="s"||$_SESSION["j_user"]["role"]=="b"||$_SESSION["j_user"]["role"]=="d"?",1":"").")</script>";
}
?>