<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/parse.php";
// constrain_user_establishments functions
include $SDR . "/custom/constrain_user_establishments.php";

$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 100);
$d = "";

/* Publications */
if (isset($_GET['pb'])) {
	$pb_sql = ' ';
	$publication_id = $_GET['pb'];
	$statement = "SELECT j_inp_inventory
				FROM nse_inventory_publications
				WHERE j_inp_publication=?";
	$sql_publications = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_publications->bind_param('s', $_GET['pb']);
	$sql_publications->execute();
	$sql_publications->bind_result($inv_code);
	while ($sql_publications->fetch()) {
		$pb_sql .= " j_invit_inventory='$inv_code' OR";
	}
	$sql_publications->close();

	$statement = "SELECT j_in_id
				FROM nse_inventory
				WHERE publication_id=? ";
	$sql_publications2 = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_publications2->bind_param('s', $_GET['pb']);
	$sql_publications2->execute();
	$sql_publications2->bind_result($inv_code);
	while ($sql_publications2->fetch()) {
		$pb_sql .= " j_invit_inventory='$inv_code' OR";
	}
	$sql_publications2->close();

	$pb_sql = substr($pb_sql, 0, -3);
	$pb_sql .= " ";
}


/* Search */
$s = "SELECT DISTINCT a.j_inv_id
,a.j_inv_credit_id
,a.j_inv_type
,a.j_inv_date
,a.j_inv_number
,a.j_inv_order
,a.j_inv_reference
,a.j_inv_from_company
,a.j_inv_from_company
,a.j_inv_from_company
,a.j_inv_from_person
,a.j_inv_from_person
,b.assessor_id AS j_inv_from_assessor
,b.rep_id1 AS j_inv_from_rep
,a.j_inv_to_company
,a.j_inv_to_person
,a.j_inv_to_contact
,a.j_inv_total_price
,a.j_inv_total_discount
,a.j_inv_total_vat
,a.j_inv_total
,a.j_inv_paid
,a.j_inv_cancel_user
,a.j_inv_creator
,a.j_inv_created";
$s.=" FROM nse_invoice AS a
		LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code ";
$s.="WHERE a.j_inv_temp=0 ";
if (isset($_GET["ds"])) {
	$z = J_dateParse($_GET["ds"]);
	if ($z)
		$s.="AND a.j_inv_date>=" . $z . " ";
}
if (isset($_GET["de"])) {
	$z = J_dateParse($_GET["de"], 0, 0, 1);
	if ($z) {
		$s.="AND a.j_inv_date<" . $z . " ";
		if (isset($_GET["go"]))
			$s.="AND a.j_inv_created<" . $z . " ";
	}
}
if (isset($_GET["tc"])) {
	// check if user has establishment access - returns 0 if not accessible
	$eid = check_user_establishment($_GET["tc"]);
	$s.="AND a.j_inv_to_company='" . $eid . "' ";
}
else // constrain user establishments
	$s.=user_establishments_constrain(1, "j_inv_to_company");
if (isset($_GET["fp"]))
	$s.="AND a.j_inv_from_person='" . $_GET["fp"] . "' ";
if (isset($_GET["fa"]))
	$s.="AND a.j_inv_from_assessor='" . $_GET["fa"] . "' ";
if (isset($_GET["ra"]))
	$s.="AND b.assessor_id='" . $_GET["ra"] . "' ";
if (isset($_GET["fr"]))
	$s.="AND a.j_inv_from_rep='" . $_GET["fr"] . "' ";
if (isset($_GET["rr"]))
	$s.="AND b.rep_id1='" . $_GET["rr"] . "' ";
if (isset($_GET["tt"])) {
	if ($_GET["tt"] == 12)
		$s.="AND (a.j_inv_type=1 OR a.j_inv_type=2) ";
	elseif ($_GET["tt"] > 0)
		$s.="AND a.j_inv_type=" . $_GET["tt"] . " ";
	else
		$s.="AND a.j_inv_type!=" . (0 - $_GET["tt"]) . " ";
}
if (isset($_GET["tq"]))
	$s.="AND " . ($_GET["tq"] == 1 ? "a.j_inv_paid=0" : ($_GET["tq"] == 2 ? " (a.j_inv_total-j_inv_paid>1 AND a.j_inv_paid!=0)" : "a.j_inv_paid!=0")) . " AND a.j_inv_type<3 ";
if (isset($_GET["cn"]))
	$s.="AND a.j_inv_cancel_date" . ($_GET["cn"] == 1 ? "=" : ">") . "0 ";
if (isset($_GET["t"])) {
	include $SDR . "/system/get.php";
	$s.=J_Search($_GET["t"], "j_inv_order~j_inv_number~j_inv_reference");
}
if (isset($_GET["prv"]))
	$s.=" AND j_inv_to_company IN (SELECT DISTINCT establishment_code FROM nse_establishment_location WHERE province_id=" . $_GET["prv"] . " GROUP BY establishment_code)";
if (isset($_GET['pb'])) {
	$s.=" AND j_inv_id IN (SELECT DISTINCT j_invit_invoice FROM nse_invoice_item WHERE ";
	$s .= "  $pb_sql";
	$s.=" GROUP BY j_invit_invoice)";
}
if (isset($_GET["i1"])) {
	include $SDR . "/custom/lib/inventory_groups.php";
	$s.=" AND j_inv_id IN (SELECT DISTINCT j_invit_invoice FROM nse_invoice_item WHERE ";
	if ($_GET["i1"] == 1 || $_GET["i1"] == 3 || $_GET["i1"] == 4)
		$s.=getQA();
	elseif ($_GET["i1"] > 0)
		$s.="j_invit_inventory=" . $_GET["i1"];
	else
		$s.=get_Pub_comps($_GET["i1"]);
	$s.=" GROUP BY j_invit_invoice)";
}
//echo mysql_error()."<hr>".$s."<hr>";
$jL0 = mysql_num_rows(mysql_query($s));
$s.=" ORDER BY j_inv_date DESC";
if (isset($_GET["go"]) && isset($_GET["jL2"]) && !$_GET["jL2"])
	$jL2 = 1000000;
else
	$s.=" LIMIT " . $jL1 . "," . $jL2;
//echo $s;
$rs = mysql_query($s);
$r = "";
$eco = "";
$ecu = "";

if (isset($_GET["csv"])) {
	$hed = "";
	$tot = "";
	$i = 0;
	$c = 0;
	$t_up = 0;
	$t_ds = 0;
	$t_vt = 0;
	$t_tt = 0;
	$t_tp = 0;
}


if (mysql_num_rows($rs)) {
	$dd = explode("~", $_GET["d"] . "to_co~price~disc~vat~total~paid~paydate~create");
	$s = array();
	foreach ($dd as $k => $v)
		$s[$v] = 1;
	$t = array("X", "type", "date", "num", "order", "ref", "fr_co", "fr_per", "fr_ass", "fr_rep", "to_co", "to_con", "to_conno", "to_email", "price", "disc", "vat", "total", "paid", "paydate", "can", "vis", "ren", "qao", "cre");
	$d = "";
	$n = 1;
	foreach ($t as $k => $v) {
		if (isset($s[$v])) {
			$d.=$n;
			$n++;
		}
		$d.="~";
	}
	$co = array();
	$cu = array();
	$cp = array();
	include_once $SDR . "/apps/accounts/invoices/types.php";
	include_once $SDR . "/system/get.php";

	function getPayDate($i) {
		$d = "";
		$q = mysql_query("SELECT j_invp_date FROM nse_invoice_item_paid WHERE j_invp_invoice=" . $i . " ORDER BY j_invp_date DESC LIMIT 1");
		if (mysql_num_rows($q)) {
			$g = mysql_fetch_array($q);
			$d = date("Y/m/d", $g["j_invp_date"]);
		}
		return $d;
	}

	include $SDR . "/custom/lib/est_items.php";

	while ($g = mysql_fetch_array($rs)) {

		if (isset($s["vis"]))
			$visit = visit($g["j_inv_to_company"]);
		if (isset($s["ren"]))
			$rnw = renew($g["j_inv_to_company"]);
		if (isset($s["qao"]))
			$qaout = qa_out($g["j_inv_to_company"]);

		if (isset($_GET["csv"])) {
			$r.=$types[$g["j_inv_type"]];
			if (!$i) {
				$hed.="TYPE";
				$c++;
			}
			if (isset($s["date"])) {
				$r.="," . ($g["j_inv_date"] ? date("Y/m/d", $g["j_inv_date"]) : "");
				if (!$i) {
					$hed.=",DATE";
					$c++;
				}
			}
			if (isset($s["num"])) {
				$r.="," . ($g["j_inv_number"] ? " " . str_replace(",", " ", $g["j_inv_number"]) : "");
				if (!$i) {
					$hed.=",NUMBER";
					$c++;
				}
			}
			if (isset($s["order"])) {
				$r.="," . ($g["j_inv_order"] ? " " . str_replace(",", " ", $g["j_inv_order"]) : "");
				if (!$i) {
					$hed.=",ORDER";
					$c++;
				}
			}
			if (isset($s["ref"])) {
				$r.="," . ($g["j_inv_reference"] ? " " . str_replace(",", " ", $g["j_inv_reference"]) : "");
				if (!$i) {
					$hed.=",REFERENCE";
					$c++;
				}
			}
			if (isset($s["fr_co"])) {
				$r.="," . ($g["j_inv_from_company"] ? str_replace(",", " ", J_Value("name", "establishment", "id", $g["j_inv_from_company"])) : "AA Travel Guides");
				if (!$i) {
					$hed.=",FROM COMPANY";
					$c++;
				}
			}
			if (isset($s["fr_per"])) {
				$r.="," . ($g["j_inv_from_person"] ? str_replace(",", " ", J_Value("firstname~lastname", "nse_user", "user_id", $g["j_inv_from_person"])) : "AA Travel Guides");
				if (!$i) {
					$hed.=",FROM PERSON";
					$c++;
				}
			}
			if (isset($s["fr_ass"])) {
				$r.="," . ($g["j_inv_from_assessor"] ? str_replace(",", " ", J_Value("firstname~lastname", "nse_user", "user_id", $g["j_inv_from_assessor"])) : "");
				if (!$i) {
					$hed.=",ASSESSOR";
					$c++;
				}
			}
			if (isset($s["fr_rep"])) {
				$r.="," . ($g["j_inv_from_rep"] ? str_replace(",", " ", J_Value("firstname~lastname", "nse_user", "user_id", $g["j_inv_from_rep"])) : "");
				if (!$i) {
					$hed.=",REP";
					$c++;
				}
			}
			$r.="," . ($g["j_inv_to_company"] ? str_replace(",", " ", J_Value("name", "establishment", "id", $g["j_inv_to_company"])) : "AA Travel Guides");
			if (!$i) {
				$hed.=",TO COMPANY";
				$c++;
			}
			if (isset($s["to_con"])) {
				$r.="," . ($g["j_inv_to_contact"] || $g["j_inv_to_person"] ? str_replace(",", " ", J_Value("", "people", "", $g["j_inv_to_contact"] ? $g["j_inv_to_contact"] : $g["j_inv_to_person"])) : "");
				if (!$i) {
					$hed.=",TO PERSON";
					$c++;
				}
			}
			if (isset($s["to_conno"])) {
				$r.="," . numbers($g["j_inv_to_company"], ($g["j_inv_to_contact"] ? $g["j_inv_to_contact"] : $g["j_inv_to_person"]));
				if (!$i) {
					$hed.=",TEL/CELL";
					$c++;
				}
			}
			$r.="," . ($g["j_inv_total_price"] ? number_format($g["j_inv_total_price"], 2, ".", " ") : "");
			$t_up+=$g["j_inv_total_price"];
			$r.="," . ($g["j_inv_total_discount"] ? number_format($g["j_inv_total_discount"], 2, ".", " ") : "");
			$t_ds+=$g["j_inv_total_discount"];
			$r.="," . ($g["j_inv_total_vat"] ? number_format($g["j_inv_total_vat"], 2, ".", " ") : "");
			$t_vt+=$g["j_inv_total_vat"];
			$r.="," . ($g["j_inv_total"] ? $g["j_inv_total"] : "");
			$t_tt+=$g["j_inv_total"];
			$r.="," . ($g["j_inv_type"] < 3 && $g["j_inv_paid"] ? number_format($g["j_inv_paid"], 2, ".", " ") : "");
			if ($g["j_inv_type"] < 3)
				$t_tp+=$g["j_inv_paid"];
			$r.="," . ($g["j_inv_type"] < 4 ? getPayDate($g["j_inv_id"]) : "");
			if (isset($s["can"]))
				$r.="," . ($g["j_inv_cancel_user"] ? "X" : "");
			if (isset($s["vis"]))
				$r.="," . $visit[0];
			if (isset($s["ren"]))
				$r.="," . $rnw[0];
			if (isset($s["qao"]))
				$r.="," . $qaout;
			if (isset($s["cre"]))
				$r.="," . $g['j_inv_created'];
			$r.="\n";
			$i++;
		} else {
			$r.=$g["j_inv_id"] . "~";
			$r.=($g["j_inv_type"] ? $g["j_inv_type"] : "") . "~";
			if (isset($s["date"]))
				$r.=($g["j_inv_date"] ? date("Y/m/d", $g["j_inv_date"]) : "") . "~";
			if (isset($s["num"]))
				$r.=$g["j_inv_number"] . "~";
			if (isset($s["order"]))
				$r.=$g["j_inv_order"] . "~";
			if (isset($s["ref"]))
				$r.=$g["j_inv_reference"] . "~";
			if (isset($s["fr_co"])) {
				$r.=($g["j_inv_from_company"] ? $g["j_inv_from_company"] : "") . "~";
				if ($g["j_inv_from_company"] && !isset($co[$g["j_inv_from_company"]]))
					$co[$g["j_inv_from_company"]] = J_Value("name", "establishment", "id", $g["j_inv_from_company"]);
			}
			if (isset($s["fr_per"])) {
				$r.=($g["j_inv_from_person"] ? $g["j_inv_from_person"] : "") . "~";
				if ($g["j_inv_from_person"] && !isset($cp[$g["j_inv_from_person"]]))
					$cu[$g["j_inv_from_person"]] = J_Value("firstname~lastname", "nse_user", "user_id", $g["j_inv_from_person"]);
			}
			if (isset($s["fr_ass"])) {
				$r.=($g["j_inv_from_assessor"] ? $g["j_inv_from_assessor"] : "") . "~";
				if ($g["j_inv_from_assessor"] && !isset($cp[$g["j_inv_from_assessor"]]))
					$cu[$g["j_inv_from_assessor"]] = J_Value("firstname~lastname", "nse_user", "user_id", $g["j_inv_from_assessor"]);
			}
			if (isset($s["fr_rep"])) {
				$r.=($g["j_inv_from_rep"] ? $g["j_inv_from_rep"] : "") . "~";
				if ($g["j_inv_from_rep"] && !isset($cp[$g["j_inv_from_rep"]]))
					$cu[$g["j_inv_from_rep"]] = J_Value("firstname~lastname", "nse_user", "user_id", $g["j_inv_from_rep"]);
			}
			$r.=($g["j_inv_to_company"] ? $g["j_inv_to_company"] : "") . "~";
			if ($g["j_inv_to_company"] && !isset($co[$g["j_inv_to_company"]]))
				$co[$g["j_inv_to_company"]] = J_Value("name", "establishment", "id", $g["j_inv_to_company"]);
			if (isset($s["to_con"])) {
				if ($g["j_inv_to_contact"] || $g["j_inv_to_person"]) {
					$tpd = $g["j_inv_to_contact"] ? $g["j_inv_to_contact"] : $g["j_inv_to_person"];
					$r.=$tpd;
					if ($tpd && !isset($cp[$tpd]))
						$cu[$tpd] = J_Value("", "people", "", $tpd);
				}
				$r.="~";
			}
			if (isset($s["to_conno"]))
				$r.=numbers($g["j_inv_to_company"], ($g["j_inv_to_contact"] ? $g["j_inv_to_contact"] : $g["j_inv_to_person"])) . "~";

			//User Email
			if (isset($s["to_email"])) {
				$user_email = '';
				$user_id = ($g["j_inv_to_contact"] ? $g["j_inv_to_contact"] : $g["j_inv_to_person"]);
				$statement = "SELECT email FROM nse_user WHERE user_id=? AND (phone!='' OR cell!='') LIMIT 1";
				$sql_email = $GLOBALS['dbCon']->prepare($statement);
				$sql_email->bind_param('i', $user_id);
				$sql_email->execute();
				$sql_email->bind_result($user_email);
				$sql_email->fetch();
				$sql_email->close();

				$r .= "$user_email~";
			}

			$r.=$g["j_inv_total_price"] . "~";
			$r.=$g["j_inv_total_discount"] . "~";
			$r.=$g["j_inv_total_vat"] . "~";
			$r.=$g["j_inv_total"] . "~";
			$r.=($g["j_inv_type"] < 3 ? $g["j_inv_paid"] : "") . "~";
			$r.=($g["j_inv_type"] < 3 ? getPayDate($g["j_inv_id"]) : "") . "~";
			if (isset($s["can"]))
				$r.=($g["j_inv_cancel_user"] ? 1 : "") . "~";
			if (isset($s["vis"]))
				$r.=$visit[0] . "~";
			if (isset($s["ren"]))
				$r.=$rnw[0] . "~";
			if (isset($s["qao"]))
				$r.= "". ($qaout=='//'?'':$qaout) . "~";
			if (isset($s["cre"])) {
				$j_inv_created = date('Y/m/d', $g['j_inv_created']);
				$r.=$j_inv_created . "~";
			}
			$r.=$g["j_inv_creator"] . "~";
			$r.=($g["j_inv_cancel_user"] ? "X~" : "~");
			$r.="|";
		}
	}

	if (isset($_GET["csv"])) {
		$hed.=",UNIT PRICE";
		$hed.=",DISCOUNT";
		$hed.=",VAT";
		$hed.=",TOTAL";
		$hed.=",PAID";
		$hed.=",DATE";
		if (isset($s["can"]))
			$hed.=",CANCELLED";
		if (isset($s["vis"]))
			$hed.=",LAST VISIT";
		if (isset($s["ren"]))
			$hed.=",QA RENEW";
		if (isset($s["qao"]))
			$hed.=",QA OUT";
		$i = 1;
		while ($i < $c) {
			$tot.=",";
			$i++;
		}
		$tot.="," . number_format($t_up, 2, ".", " ");
		$tot.="," . number_format($t_ds, 2, ".", " ");
		$tot.="," . number_format($t_vt, 2, ".", " ");
		$tot.="," . number_format($t_tt, 2, ".", " ");
		$tot.="," . number_format($t_tp, 2, ".", " ");
		$tot.="\n";
		$csv = $hed . "\n" . $r . $tot;
	} else {
		$r = str_replace("\n", "", $r);
		$r = str_replace("\"", "", $r);
		$r = str_replace("~|", "|", $r);
		foreach ($co as $k => $v) {
			$eco.=$k . "~" . $v . "|";
		}
		foreach ($cu as $k => $v) {
			$ecu.=$k . "~" . $v . "|";
		}
	}
}

if (isset($_GET["csv"])) {
	$csv_content = $csv;
	$csv_name = $_GET["csv_name"] ? $_GET["csv_name"] : "ACC_" . $jL1 . "-" . $jL2 . "_" . date("d-m-Y", $EPOCH) . ".csv";
	include $SDR . "/utility/CSV/create.php";
} else {
	echo "<html>";
	echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
	echo "<script src=" . $ROOT . "/system/P.js></script>";
	echo "<script src=" . $ROOT . "/apps/accounts/invoices/res.js></script>";
	echo "<script>J_inv_r(\"" . $r . "\",\"" . $d . "\",\"" . $eco . "\",\"" . $ecu . "\"," . $jL0 . "," . $jL1 . "," . $jL2 . ($_SESSION["j_user"]["role"] == "b" || $_SESSION["j_user"]["role"] == "d" ? ",1" : "") . ")</script>";
}
?>
