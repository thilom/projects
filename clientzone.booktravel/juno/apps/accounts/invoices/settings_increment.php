<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

if(isset($_GET["del"]))
{
	mysql_query("DELETE FROM nse_invoice_increment WHERE j_invinc_id=".$_GET["id"]);
	unset($_GET["id"]);
}

elseif(count($_POST))
{
	function strip($v="")
	{
		return preg_replace('~[^a-zA-Z0-9 #_\-\[\]\.//]~s',"",$v);
	}
	$err="";
	$c="";
	$n=0;
	if(isset($_POST["j_invinc_default"]))
		mysql_query("UPDATE nse_invoice_increment SET j_invinc_default=0 WHERE j_invinc_company=".($_POST["j_invinc_company"]?$_POST["j_invinc_company"]:0));
	if($_GET["id"])
	{
		$s="UPDATE nse_invoice_increment SET ";
		foreach($_POST as $k => $v)
		{
			if(strpos($k,"C")!==false)
				$c.=strip($v)."^";
			else
			{
				$s.=($n?",":"").strip($k)."='".strip($v)."'";
				$n++;
			}
		}
		$s.=",j_invinc_components='".$c."' WHERE j_invinc_id=".$_GET["id"];
		mysql_query($s);
		if(mysql_error())
			$err.=mysql_error()."<tt>".$s."</tt><hr>";
	}
	else
	{
		$s="INSERT INTO nse_invoice_increment (j_invinc_company,j_invinc_name,j_invinc_components".(isset($_POST["j_invinc_default"])?",j_invinc_default":"").") VALUES (";
		foreach($_POST as $k => $v)
		{
			if(strpos($k,"C")!==false)
				$c.=strip($v)."^";
			else
			{
				$s.=($n?",":"")."'".strip($v)."'";
				$n++;
			}
		}
		$s.=",'".$c."')";
		mysql_query($s);
		if(mysql_error())
			$err.=mysql_error()."<tt>".$s."</tt><hr>";
	}
	unset($_GET["id"]);
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

if(isset($_GET["id"]))
{
	if(isset($err)&&$err)
		echo $err."<br>";
	echo "<body onload=J_inv_V();J_tr()>";
	echo "<script>hmm_up='Move up';hmm_de='Remove';j_op=\"<select class=jW100 onchange=j_Wi[\"+j_W+\"]['F'][1]['W'].J_inv_R(value)><option value=0><option value=''>Text<option value=day>Day (01)<option value=month>Month (01)<option value='abbr year'>Abbreviated Year (01)<option value='full year'>Full Year (2001)<option value=count>Invoice Count<option value='pad3 count'>Pad3 Count<option value='pad4 count'>Pad4 Count<option value='pad5 count'>Pad5 Count<option value='pad6 count'>Pad6 Count<option value='pad7 count'>Pad7 Count<option value='pad8 count'>Pad8 Count<option value='pad9 count'>Pad9 Count</select>\"</script>";
	echo "<h4>Invoice Number Incrementation</h4><hr>";
	echo "<form method=post><table id=jTR>";

	if($_GET["id"])
		$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_increment WHERE j_invinc_id=".$_GET["id"]." LIMIT 1"));

	echo "<tr><th>Company</th><td colspan=3><input type=text value='".(isset($g)||isset($_POST["j_invinc_company"])?J_Value("name","establishment","id",(isset($g)?$g["j_invinc_company"]:$_POST["j_invinc_company"])):"")."' onkeyup=\"J_ajax_K(this,'/system/ajax.php?t=establishment',320,4) \" onmouseover=\"J_TT(this,'Type in a few letters of the company')\" class=jW100>";
	echo "<input type=hidden name=j_invinc_company value=".(isset($g)?$g["j_invinc_company"]:(isset($_POST["j_invinc_company"])?$_POST["j_invinc_company"]:0))."></td></tr>";
	echo "<tr><th>Name</th><td colspan=3><input type=text name=j_invinc_name value=\"".(isset($g)?$g["j_invinc_name"]:(isset($_POST["j_invinc_name"])?$_POST["j_invinc_name"]:""))."\" class=jW100 title='Requires value'></td></tr>";
	echo "<tr><th class=jO20>Sample</th><td colspan=3><input type=text id=jsv class=jW100 style=background:transparent;border:0></td></tr>";
	echo "<tbody id=jtb>";
	$n=0;
	if(isset($g))
	{
		$c=explode("^",$g["j_invinc_components"]);
		{
			foreach($c as $k => $v)
			{
				if($v)
					echo "<tr><td></td><td><input type=text name=C".$n." value=".$v." onclick=J_note(this,j_op,110,2,9,-1,0,2) onkeyup=J_inv_V()></td><td><input type=button onclick=J_inv_U(this) value=&#9650; onmouseover=J_TT(this,hmm_up)></td><td><input type=button onclick=J_inv_X(this) value=&#10006; onmouseover=J_TT(this,hmm_de)></td></tr>";
				$n++;
			}
		}
	}
	echo "</tbody>";
	echo "<tr><th>Number</th><td".(isset($g)?" onmouseover=\"J_TT(this,'Incremental Number is currently at ".$g["j_invinc_number"]."')\"":"").">".($_GET["id"]?$g["j_invinc_number"]:"<input type=text name=j_invinc_number value=0>")."</td></tr>";
	echo "</table>";
	echo "<hr><table><tr><td><input type=button onclick=J_inv_A() value=&#10010; onmouseover=\"J_TT(this,'Add a component')\"></td><td><tt style=line-height:100% class=jO40><b>NOTE!</b> Padded invoice counts reset to 0 when the full total of the number is reached</tt></td></tr></table>";
	echo "<hr><input type=submit value=OK style=font-weight:bold>";
	if($_GET["id"])
	{
		$i=J_ID("nse_invoice","j_inv_id","j_inv_increment_id=".$_GET["id"]);
		echo " <input type=button value=Delete onclick=\"if(confirm('WARNING!\\n".($i?"There are ".$i." invoice".($i!=1?"s":"")."using this incrementor\\nDeletion will break this series of invoices.":"Deletion may effect the incremental options for various invoices created.")."\\n\\nContinue?'))location=location.href+'&del=1'\">";
	}
	echo " <b onmouseover=\"J_TT(this,'If set this incrementation will be automatically inserted in new invoices')\"><input type=checkbox name=j_invinc_default value=1".((isset($g)&&$g["j_invinc_default"])||isset($_POST["j_invinc_default"])?" checked":"")."> Default</b></form>";
	echo "<script>j_inc=".$n.";
	function J_inv_A(){var n,td;j_inc++;n=document.createElement('tr');td=document.createElement('td');n.appendChild(td);td=document.createElement('td');td.innerHTML='<input type=text name=C'+j_inc+' onclick=J_note(this,j_op,110,2,9,-1,0,2) onkeyup=J_inv_V()>';n.appendChild(td);td=document.createElement('td');td.innerHTML='<input type=button onclick=J_inv_U(this) value=&#9650; onmouseover=J_TT(this,hmm_up)>';n.appendChild(td);td=document.createElement('td');td.innerHTML='<input type=button onclick=J_inv_X(this) value=&#10006; onmouseover=J_TT(this,hmm_de)>';n.appendChild(td);\$('jtb').appendChild(n)}
	function J_inv_U(t){t=t.parentNode.parentNode;if(t.previousSibling)\$('jtb').insertBefore(t,t.previousSibling);else \$('jtb').appendChild(t);J_inv_V()}
	function J_inv_X(t){var t=t.parentNode.parentNode;t.parentNode.removeChild(t);J_inv_V()}
	function J_inv_D(t){var d=new Date();v=t==1?d.getDate():t==2?d.getMonth()+1:t==3?(d.getFullYear()+'').substr(2):d.getFullYear();return (v<10?'0':'')+v}
	function J_inv_R(v){j_E.value=v;J_inv_V()}
	function J_inv_V(){var o=\$T(\$('jtb'),'input'),i=0,v='';while(o[i]){if(o[i].type=='text'){if(o[i].value=='day')v+=J_inv_D(1);else if(o[i].value=='month')v+=J_inv_D(2);else if(o[i].value=='abbr year')v+=J_inv_D(3);else if(o[i].value=='full year')v+=J_inv_D(4);else if(o[i].value=='count')v+=j_inc;else if(o[i].value=='pad3 count')v+='256';else if(o[i].value=='pad4 count')v+='0256';else if(o[i].value=='pad5 count')v+='00256';else if(o[i].value=='pad6 count')v+='000256';else if(o[i].value=='pad7 count')v+='0000256';else if(o[i].value=='pad8 count')v+='00000256';else if(o[i].value=='pad9 count')v+='000000256';else v+=o[i].value}i++}\$('jsv').value=v}</script>";
	echo "<iframe id=j_IF></iframe>";
}
else
{
	echo "<style type=text/css>#jTR tr{cursor:pointer}</style>";
	echo "<body onload=J_tr()>";
	echo "<h4>Invoice Number Incrementation</h4><hr><tt>You can set up any form of invoice incrementation and save them for various types of invoices or invoices from different companies or divisions</tt><hr>";
	$s="SELECT * FROM nse_invoice_increment ORDER BY j_invinc_company,j_invinc_name";
	$r=mysql_query($s);
	if(mysql_num_rows($r))
	{
		echo "<table id=jTR>";
		while($g=mysql_fetch_array($r))
		{
			echo "<tr onclick=J_inc(".$g["j_invinc_id"].")><td>".J_Value("name","establishment","id",$g["j_invinc_company"])."</td><td>".($g["j_invinc_default"]?"<b onmouseover=\"J_TT(this,'Default incrementor')\">".$g["j_invinc_name"]."</b>":$g["j_invinc_name"])."</td></tr>";
		}
		echo "</table>";
	}
	else
		echo "<tt>No incrementors available</tt>";
	echo "<hr><input type=button onclick=J_inc(0) value=&#10010; onmouseover=\"J_TT(this,'Add an incrementor')\">";
		echo "<script>function J_inc(d){J_frame(1,ROOT+'/apps/accounts/invoices/settings_increment.php?id='+d)}</script>";
}

echo "</body></html>";
?>