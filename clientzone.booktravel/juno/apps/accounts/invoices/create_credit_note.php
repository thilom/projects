<?php
include_once $SDR."/system/get.php";
include_once $SDR."/system/parse.php";
include_once $SDR."/apps/accounts/invoices/default.php";
include_once $SDR."/apps/accounts/invoices/increment.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR."/system/activity.php";


function new_CreditNote(
$j_inv_date=0
,$j_inv_credit_id=0
,$j_inv_order=""
,$j_inv_reference=""
,$j_inv_from_company=""
,$j_inv_to_company=""
,$j_inv_from_person=0
,$j_inv_from_assessor=0
,$j_inv_from_rep=0
,$j_inv_from_rep2=0
,$j_inv_to_person=0
,$j_inv_from_contact=0
,$j_inv_to_contact=0
,$j_inv_vat=0
,$j_inv_info=""
,$j_inv_comment=""
,$j_inv_condition=0
,$j_inv_foot=0
,$j_inv_currency=""
,$j_inv_total_price=0
,$j_inv_total_discount=0
,$j_inv_total_vat=0
,$j_inv_total=0
)
{
	global $EPOCH,$invoice_default_company,$invoice_default_creditnote_increment;
	$j_inv_number="";
	$j_inv_increment_id=0;

	if($invoice_default_creditnote_increment)
	{
		$q=mysql_query("SELECT j_invinc_id FROM nse_invoice_increment WHERE j_invinc_id=".$invoice_default_creditnote_increment." LIMIT 1");
		if(mysql_num_rows($q))
		{
			$q=mysql_fetch_array($q);
			$j_inv_increment_id=$q["j_invinc_id"];
			$j_inv_number=J_invoice_increment($j_inv_increment_id,isset($_POST["j_inv_from_company"])?$_POST["j_inv_from_company"]:$invoice_default_company);
		}
	}

	$q="INSERT INTO nse_invoice
	(
	j_inv_head
	,j_inv_type
	,j_inv_date
	,j_inv_credit_id
	,j_inv_order
	,j_inv_reference
	,j_inv_number
	,j_inv_increment_id
	,j_inv_from_company
	,j_inv_to_company
	,j_inv_from_person
	,j_inv_from_assessor
	,j_inv_from_rep
	,j_inv_from_rep2
	,j_inv_to_person
	,j_inv_from_contact
	,j_inv_to_contact
	,j_inv_vat
	,j_inv_info
	,j_inv_comment
	,j_inv_condition
	,j_inv_foot
	,j_inv_currency
	,j_inv_total_price
	,j_inv_total_discount
	,j_inv_total_vat
	,j_inv_total
	,j_inv_user
	,j_inv_creator
	,j_inv_created
	)
	VALUES
	(
	3
	,3
	,".$EPOCH."
	,".$j_inv_credit_id."
	,'".$j_inv_order."'
	,'".$j_inv_reference."'
	,'".$j_inv_number."'
	,'".$j_inv_increment_id."'
	,'".$j_inv_from_company."'
	,'".$j_inv_to_company."'
	,'".$j_inv_from_person."'
	,'".$j_inv_from_assessor."'
	,'".$j_inv_from_rep."'
	,'".$j_inv_from_rep2."'
	,'".$j_inv_to_person."'
	,'".$j_inv_from_contact."'
	,'".$j_inv_to_contact."'
	,'".$j_inv_vat."'
	,'".$j_inv_info."'
	,'".$j_inv_comment."'
	,'".$j_inv_condition."'
	,'".$j_inv_foot."'
	,'".$j_inv_currency."'
	,'".(0-$j_inv_total_price)."'
	,'".(0-$j_inv_total_discount)."'
	,'".(0-$j_inv_total_vat)."'
	,'".(0-$j_inv_total)."'
	,".$_SESSION["j_user"]["id"]."
	,".$_SESSION["j_user"]["id"]."
	,".$EPOCH."
	)";
	mysql_query($q);
	$id=J_ID("nse_invoice","j_inv_id");

	insertNote("Credit Note No: ".$j_inv_number,0,1,0,0,"New Credit Note No: ".$j_inv_number);
	J_act("Credit Note",3,$j_inv_number,$id,$j_inv_to_contact);

	return $id;
}

function addto_CreditNote(
$j_invit_invoice
,$j_invit_credit_id=0 // corresponding item in invoice
,$j_invit_inventory=0
,$j_invit_code=""
,$j_invit_serial=""
,$j_invit_date=0
,$j_invit_name="Undefined"
,$j_invit_description=""
,$j_invit_quantity=1
,$j_invit_price=0
,$j_invit_discount=0
,$j_invit_disc_percent=0
,$j_invit_vat=0
,$j_invit_vat_percent=0
,$j_invit_total=0
,$j_invit_company=""
)
{
	global $EPOCH;

	$q="INSERT INTO nse_invoice_item
	(
		j_invit_invoice
		,j_invit_credit_id
		,j_invit_user
		,j_invit_inventory
		,j_invit_code
		,j_invit_serial
		,j_invit_date
		,j_invit_name
		,j_invit_description
		,j_invit_quantity
		,j_invit_price
		,j_invit_discount
		,j_invit_disc_percent
		,j_invit_vat
		,j_invit_vat_percent
		,j_invit_total
		,j_invit_company
		,j_invit_created
	)
	VALUES
	(
	".$j_invit_invoice."
	,".$j_invit_credit_id."
	,".$_SESSION["j_user"]["id"]."
	,".$j_invit_inventory."
	,'".$j_invit_code."'
	,'".$j_invit_serial."'
	,'".($j_invit_date?$j_invit_date:$EPOCH)."'
	,'".$j_invit_name."'
	,'".$j_invit_description."'
	,".(0-$j_invit_quantity)."
	,".(0-$j_invit_price)."
	,".(0-$j_invit_discount)."
	,".$j_invit_disc_percent."
	,".(0-$j_invit_vat)."
	,".$j_invit_vat_percent."
	,".(0-$j_invit_total)."
	,'".$j_invit_company."'
	,'".$EPOCH."'
	)";
	mysql_query($q);

	$q="SELECT j_inv_number FROM nse_invoice WHERE j_inv_id=".$j_invit_invoice." LIMIT 1";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		$q=mysql_fetch_array($q);
		insertNote($j_invit_company,2,"INV".$j_invit_invoice,"Added to Credit Note ".$q["j_inv_number"]." - ".$j_invit_name,0,1);
	}

}

function adjust_CreditNote_items($id=0,$cid=0)
{
	if($cid)
	{
		$q=mysql_query("SELECT j_invit_id FROM nse_invoice_item WHERE j_invit_credit_id=".$cid." LIMIT 1");
		if(mysql_num_rows($q))
		{
			$g=mysql_fetch_array($q);
			$id=$g["j_invit_id"];
		}
	}
	if($id)
	{
		$q=mysql_query("SELECT * FROM nse_invoice_item JOIN nse_invoice ON j_invit_invoice=j_inv_id WHERE j_invit_id=".$id." LIMIT 1");
		if(mysql_num_rows($q))
		{
			$g=mysql_fetch_array($q);
			$p=$g["j_inv_total_price"]-$g["j_invit_price"];
			$d=$g["j_inv_total_discount"]-$g["j_invit_discount"];
			$v=$g["j_inv_total_vat"]-$g["j_invit_vat"];
			$t=$g["j_inv_total"]-$g["j_invit_total"];
			$i=trim($g["j_inv_info"]."<br>Item ".$g["j_invit_name"]." removed by ".J_Value("","people","",$_SESSION["j_user"]["id"]),"<br>");
			mysql_query("UPDATE nse_invoice SET 	j_inv_total_price=".$p.",j_inv_total_discount=".$d.",j_inv_total_vat=".$v.",j_inv_total=".$t.",j_inv_info='".$i."' WHERE id=".$g["j_inv_id"]);
			$co=($g["j_invit_company"]?$g["j_invit_company"]:$g["j_inv_to_company"]);
			insertNote($co,2,"INV".$g["j_inv_id"],"Removed from Credit Note ".$g["j_inv_number"]." - ".$g["j_invit_name"],0,1);
			J_act("Credit Note Item",5,"From invoice no: ".$g["j_inv_number"],$id,$co);
		}
		mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_id=".$id);
	}
}

function delete_CreditNote($id)
{
	if($id)
	{
		$q=mysql_query("SELECT * FROM nse_invoice WHERE j_inv_credit_id=".$id);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
			{
				mysql_query("DELETE FROM nse_invoice WHERE j_inv_id=".$g["j_inv_id"]);
				mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_invoice=".$g["j_inv_id"]);
				insertNote($g["j_inv_to_company"],2,"INV".$g["j_inv_credit_id"],"Deleted Credit Note ".$g["j_inv_number"],0,1);
				J_act("Credit Note",5,"No: ".$g["j_inv_number"],$g["j_inv_id"],$g["j_inv_to_company"]);
			}
		}
	}
}
?>
