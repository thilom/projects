<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$v=strtolower(str_replace(array("\"","'"),"",str_replace("+++","&",$_GET["v"])));
function J_stripit($v=""){return preg_replace("~[^a-zA-Z0-9 !&£,;:@_=+%#/<>\$\'\*\.\-\(\)\[\]\{\}]~s","",$v);}

$o="";
if($_GET["t"]=="inventory" || $_GET["t"]=="inventory_stock")
{
	$a=array();
	if($_GET["t"]=="inventory_stock")
		$r=mysql_query("SELECT DISTINCT j_in_code,j_in_id,j_in_name FROM nse_inventory WHERE j_in_stock!=0".J_Search($v,"j_in_name~j_in_code")." GROUP BY j_in_code ORDER BY j_in_name,j_in_code LIMIT 100");
	else
		$r=mysql_query("SELECT j_in_id,j_in_name,j_in_code FROM nse_inventory WHERE j_in_id>0".J_Search($v,"j_in_name~j_in_code")." ORDER BY j_in_name,j_in_code LIMIT 100");
	if(mysql_num_rows($r))
	{
		while($g=mysql_fetch_array($r))
		{
			$name=J_stripit($g["j_in_name"]);
			$code=J_stripit($g["j_in_code"]);
			$o.=$g["j_in_id"]."~";
			$o.=$name;
			if($code || $serial)$o.=" (";
			$o.=$code;
			if($code && $serial)$o.="/";
			$o.=$serial;
			if($code || $serial)$o.=")";
			$o.="|";
		}
	}
}
elseif($_GET["t"]=="establishment")
{
	$st = J_Search($v, 'a.establishment_name~a.establishment_code');
	$statement = "SELECT a.establishment_code, a.establishment_name, c.town_name
						FROM nse_establishment AS a
						LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
						LEFT JOIN nse_nlocations_towns AS c ON b.town_id=c.town_id
						WHERE a.establishment_code > 0 $st
						ORDER BY a.establishment_name LIMIT 100";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->execute();
	$sql_location->bind_result($establishment_code, $establishment_name, $town_name);
	while ($sql_location->fetch())
		$o.= $establishment_code."~".J_stripit($establishment_name)." (" .(J_stripit($town_name))."|";
	$sql_location->free_result();
	$sql_location->close();
}

elseif($_GET["t"]=="users")
{
	$st = '%$v%';
	$statement = "SELECT DESTINCT u.user_id, u.firstname, u.lastname, e.designation FROM nse_user JOIN nse_user_establishments ON u.user_id=e.user_id WHERE ";
	if(isset($_GET['uid']))
		$statement .="u.user_id=?";
	else
		$statement.="u.firstname LIKE ? OR u.lastname LIKE ? ";
	$statement .="ORDER BY u.firstname, u.lastname";
	$sql_users = $GLOBALS['dbCon']->prepare($statement);
	if(isset($_GET['uid']))
		 $sql_users->bind_param('sss', $v, $v, $_GET['uid']);
	else
		 $sql_users->bind_param('ss', $v, $v);
	$sql_users->execute();
	$sql_users->store_result();
	$sql_users->bind_result($user_id, $firstname, $lastname, $designation);
	while($sql_users->fetch())
		$o.=$user_id."~".J_stripit("$firstname $lastname").($designation?" (". J_stripit("$designation").")":"")."|";
	$sql_users->free_result();
	$sql_users->close();
}

else
{
	$s="SELECT ".$_GET["c1"].(isset($_GET["c2"])?",".$_GET["c2"]:"").(isset($_GET["c3"])?",".$_GET["c3"]:"").(isset($_GET["c4"])?",".$_GET["c4"]:"").(isset($_GET["c5"])?",".$_GET["c5"]:"")." FROM ".$_GET["t"]." WHERE ".(isset($_GET["where"])?$_GET["where"]:($_GET["c1"]."!='".(isset($_GET["o1"])?$_GET["o1"]:0)."'"));
	if($v)
		$s.=J_Search($v,(isset($_GET["c2"])&&$_GET["c2"]?$_GET["c2"]."~":"").(isset($_GET["c3"])&&$_GET["c3"]?$_GET["c3"]."~":"").(isset($_GET["c4"])&&$_GET["c4"]?$_GET["c4"]."~":"").(isset($_GET["c5"])&&$_GET["c5"]?$_GET["c5"]."~":""));
	if(isset($_GET["c2"]))$s.=" ORDER BY ".$_GET["c2"];
	$s.=" LIMIT 100";
	$r=mysql_query($s);
	if(mysql_error())echo "<script>alert(\"AJAX ERROR\\nCut & paste this error to system admin\\n\\n".$s."\")</script>";
	elseif(mysql_num_rows($r))
	{
		while($g=mysql_fetch_array($r))
		{
			$o.=J_stripit($g[$_GET["c1"]])."~";
			if(isset($_GET["c2"]))$o.=J_stripit($g[$_GET["c2"]])."~";
			if(isset($_GET["c3"]))$o.=J_stripit($g[$_GET["c3"]])."~";
			if($_GET["t"]!="juno_currency")
			{
				if(isset($_GET["c4"]))$o.=J_stripit($g[$_GET["c4"]])."~";
				if(isset($_GET["c5"]))$o.=J_stripit($g[$_GET["c5"]]);
			}
			$o.="|";
		}
	}
	if($_GET["t"]=="juno_currency")
		$o=str_replace("~|","|",$o);
}
echo "<script>window.parent.J_ajax_R('".$_GET["d"]."'".($o?",\"".$o."\"":"").")</script>";
?>