<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
if(!isset($_GET["j_W"]) || isset($_GET["vid"]))
{}
else
	include_once $SDR."/system/secure.php";
include_once $SDR."/system/parse.php";
include_once $SDR."/system/get.php";
include_once $SDR."/apps/accounts/invoices/default.php";
include_once $SDR."/apps/accounts/invoices/get.php";

$id=isset($_GET["vid"])?$_GET["vid"]:$_GET["id"];
$est=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));
$est["establishment_name"]=J_Value("","establishment","",$id);

$d=isset($_GET["ds"])&&$_GET["ds"]?J_dateParse($_GET["ds"]):$EPOCH;
$ds=mktime(0,0,0,date("m",$d),1,date("Y",$d));
$de=0;
if(isset($_GET["de"]))
{
	$d=isset($_GET["de"])&&$_GET["de"]?J_dateParse($_GET["de"]):0;
	$de=mktime(0,0,0,date("m",$d),1,date("Y",$d));
}
if(!$de || $de>=$ds)
	$de=mktime(0,0,0,date("m",$d)-6,1,date("Y",$d));
$pdf=isset($pdf)||isset($_GET["pdf"])?1:0;
$invoice_default_from_contact=3223; // marie du plessis

if(!$pdf)
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	if(!isset($_GET["vid"]))
		echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<body>";
}

function d_company($a)
{
	$v="";
	if($a && count($a))
	{
		$a=explode("~",$a);
		$v.="<b>".strtoupper($a[1])."</b><br>";
		if(isset($a[2]) && $a[2])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>City</b>&nbsp;".ucfirst($a[2])."</span><br>";
		if(isset($a[3]) && $a[3])
		{
			$a[3]=strtolower($a[3]);
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Email</b>&nbsp;<a href=mailto:".$a[3].">".$a[3]."</a></span><br>";
		}
		if(isset($a[4]) && $a[4])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Tel</b>&nbsp;".$a[4]."</span><br>";
		if(isset($a[5]) && $a[5])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Fax</b>&nbsp;".$a[5]."</span><br>";
		if(isset($a[6]) && $a[6])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Postal</b>&nbsp;".str_replace("  "," ",str_replace(",",", ",str_replace("<br>",", ",$a[6])))."</span><br>";
		if(isset($a[7]) && $a[7])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Address</b>&nbsp; ".str_replace("  "," ",str_replace(",",", ",str_replace("<br>",", ",$a[7])))."</span><br>";
		if(isset($a[8]) && $a[8])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Est. Code</b>&nbsp; ".str_replace("<br>",", ",$a[8])."<br>";
		if(isset($a[9]) && $a[9])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Reg No.</b>&nbsp; ".$a[9]."</span><br>";
		if(isset($a[10]) && $a[10])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>VAT No.</b> ".$a[10]."</span><br>";
	}
	$v=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+\'\?\(\)\*\$\.\-]~',"",$v);
	return $v;
}

function d_person($a)
{
	$v="";
	if($a && count($a))
	{
		$a=explode("~",$a);
		$v.="<b>".strtoupper($a[1])."</b><br>";
		if(isset($a[2]) && $a[2])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>City</b>&nbsp; ".ucfirst($a[2])."</span><br>";
		if(isset($a[3]) && $a[3])
		{
			$a[3]=strtolower($a[3]);
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Email</b>&nbsp; <a href=mailto:".$a[3].">".$a[3]."</a></span><br>";
		}
		if(isset($a[4]) && $a[4])
			$v.="<span style='font-size:8pt'><b style='color:#808080'>Tel</b>&nbsp; ".$a[4]."</span><br>";
	}
	$v=preg_replace('~[^a-zA-Z0-9 ,;:!@#%&_<>=+\'\?\(\)\*\$\.\-]~',"",$v);
	return $v;
}

$no_js=1;
$eid=$id;
include $SDR."/custom/lib/get_est_people.php";

// html
$_h="";

if($pdf)
{
	$_h="<html>";
	$_h.="<head>";
	$_h.="<title>AA TRAVEL GUIDES LEDGER ".date("Y/m/d",$ds)."</title>";
	$_h.="</head>";
	$_h.="<body style='font-family:helvetica'>";
}
elseif(isset($_GET["vid"]))
	$_h.="<center><div style=width:800>";

$_h.="<table width='100%' cellspacing='0' cellpadding='0' bgcolor='#16459D'><tr>";
$_h.="<td width='90'><img src=".($pdf?"'".$SDR."/stuff/accounts/headers/logo_pdf.jpg' style='height:133;width:80'":$ROOT."/stuff/accounts/headers/logo.png")."></td>";
$_h.="<td><br><br>";
$_h.="<b style='font-size:14pt;color:#FFFFFF'>AA TRAVEL GUIDES</b><br>";
$_h.="<b style='font-size:20pt;color:#FFFFFF'>LEDGER</b><br>";
$_h.="<b style='font-size:12pt;color:#FFFFFF'>For: ".$est["establishment_name"]."<br>From ".date("F Y",$de)." to ".date("F Y",$ds)."</b>";
$_h.="</td>";
$_h.="</tr></table>";

if($pdf)
	$_h.="<br>";
$_h.="<br>";

$_h.="<table width='100%' cellspacing='0' cellpadding='0' style='border-collapse:collapse;font-size:10pt;border-style:solid;border-width:2;border-color:#AAAAAA'>";
$_h.="<tr>";
$_h.="<td style='background:#EEEEEE;border-style:solid;border-width:1;border-color:#CCCCCC;padding:".($pdf?"8":"8 8 8 12").";width:50%'><b style='font-size:9pt;color:#808080'>From</b><br>".d_company($invoice_default_company_info)."</td>";
$_h.="<td style='padding:".($pdf?"8":"8 8 8 12")."'><b style='font-size:9pt;color:#808080'>To</b><br>".d_company(J_inv_getto($id))."</td>";
$_h.="</tr>";
$_h.="<tr>";
if($est["assessor_id"] || $per)
{
	$_h.="<td style='background:#EEEEEE;border-style:solid;border-width:1;border-color:#CCCCCC;padding:".($pdf?"8":"8 8 8 12")."'>";
	if($est["assessor_id"])
		$_h.="<b style='font-size:9pt;color:#808080'>Assessor:</b><br>".d_person(J_inv_getpeo($est["assessor_id"]))."<br>";
	if($est["rep_id1"])
		$_h.="<b style='font-size:9pt;color:#808080'>Rep:</b><br>".d_person(J_inv_getpeo($est["rep_id1"]));
	$_h.="</td>";
	$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC;padding:".($pdf?"8":"8 8 8 12")."'><b style='font-size:9pt;color:#808080'>Company Contact</b><br>".($per?d_person($per):"")."</td>";
	$_h.="</tr>";
}
if($invoice_default_from_contact || $acc)
{
	$_h.="<tr>";
	$_h.="<td bgcolor='#EEEEEE' style='border-style:solid;border-width:1;border-color:#CCCCCC;padding:".($pdf?"8":"8 8 8 12")."'><b style='font-size:9pt;color:#808080'>Accounts/Queries</b><br>".($invoice_default_from_contact?d_person(J_inv_getpeo($invoice_default_from_contact)):"")."</td>";
	$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC;padding:".($pdf?"8":"8 8 8 12")."'><b style='font-size:9pt;color:#808080'>Accounts/Queries</b><br>".($acc?d_person($acc):"")."</td>";
	$_h.="</tr>";
}
$_h.="</table>";

$_h.="<br>";
$_h.="<br>";

$_h.="<table width='100%' cellspacing='0' cellpadding='3' style='border-collapse:collapse;font-size:12pt;border-style:solid;border-width:2;border-color:#AAAAAA'>";
$_h.="<tr bgcolor='#DDDDDD' style='color:#808080;font-size:8pt;font-weight:bold'>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC;width:80'>Date</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC;width:80'>Type</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC;width:80'>Reference</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>Description</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC;width:80'>Debit</td>";
$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC;width:80'>Credit</td>";
$_h.="</tr>";

// Current
$t=array("Payment","Pro-Forma","Tax&nbsp;Invoice","Credit&nbsp;Note","Debit&nbsp;Note");
$a=array();

$db=0;
$cr=0;

$ys=date("Y",$ds);
$ms=date("m",$ds);
$ye=date("Y",$de);
$me=date("m",$de);

// bring forward
$m1=mktime(0,0,0,date(1),date(1),date(2010));
$m2=mktime(0,0,0,date($me),date(1),date($ye));

$q="SELECT j_inv_id,j_inv_type,j_inv_date,j_inv_number,j_inv_total FROM nse_invoice WHERE j_inv_type<6 AND j_inv_to_company='".$id."' AND j_inv_date>=".$m1." AND j_inv_date<".$m2." AND j_inv_total!=0 ORDER BY j_inv_date DESC";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while ($g = mysql_fetch_array($q))
	{
		$q1="SELECT j_invit_id,j_invit_name,j_invit_total FROM nse_invoice_item WHERE j_invit_invoice='".$g["j_inv_id"]."' AND j_invit_total!=0";
		$q1=mysql_query($q1);
		if(mysql_num_rows($q1))
		{
			while ($g1 = mysql_fetch_array($q1))
				$db+=$g1["j_invit_total"];
		}
	}
}
$q = "SELECT j_inv_number,j_invit_name,j_invp_id,j_invp_date,j_invp_reference,j_invp_paid,j_invp_bank,j_invp_type FROM nse_invoice_item_paid LEFT JOIN nse_invoice ON j_invp_invoice=j_inv_id LEFT JOIN nse_invoice_item ON j_invp_invoice_item=j_invit_id WHERE j_inv_to_company='".$id."' AND j_invp_date>=".$m1." AND j_invp_date<".$m2;
$q = mysql_query($q);
if (mysql_num_rows($q))
{
	while ($g = mysql_fetch_array($q))
		$cr+=0-$g["j_invp_paid"];
}

$_h.="<tr><td colspan='4' style='color:#808080;text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#EEEEEE'>Brought Forward to ".date("d F, Y",mktime(0,0,0,date($me),date(1)-1,date($ye)))."</td>";
$_h.="<td colspan=2 style='text-align:center;font-weight:bold;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#EEEEEE'>".number_format($db+$cr,2,"."," ")."</td>";
$_h.="</tr>";


while($ye<=$ys) // years
{
	while(($me<13 && $ye<$ys) || ($me<=$ms && $ye==$ys)) // months
	{
		$m1=mktime(0,0,0,date($me),date(1),date($ye));
		$m2=mktime(0,0,0,date($me)+1,date(1),date($ye));
		$m3=mktime(0,0,0,date($me)+1,date(1)-1,date($ye));

		$q="SELECT j_inv_id,j_inv_type,j_inv_date,j_inv_number,j_inv_total FROM nse_invoice WHERE j_inv_type<6 AND j_inv_to_company='".$id."' AND j_inv_date>=".$m1." AND j_inv_date<".$m2." AND j_inv_total!=0 ORDER BY j_inv_date DESC";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			while ($g = mysql_fetch_array($q))
			{
				$q1="SELECT j_invit_id,j_invit_name,j_invit_total FROM nse_invoice_item WHERE j_invit_invoice='".$g["j_inv_id"]."' AND j_invit_total!=0";
				$q1=mysql_query($q1);
				if(mysql_num_rows($q1))
				{
					while ($g1 = mysql_fetch_array($q1))
						$a[$g["j_inv_date"].$g1["j_invit_id"]."it"]=array($g["j_inv_date"],$g["j_inv_type"],$g["j_inv_number"],str_replace(" (".$est["establishment_name"].")","",$g1["j_invit_name"]),$g1["j_invit_total"]);
				}
			}
		}
		$q = "SELECT j_inv_number,j_invit_name,j_invp_id,j_invp_date,j_invp_reference,j_invp_paid,j_invp_bank,j_invp_type FROM nse_invoice_item_paid LEFT JOIN nse_invoice ON j_invp_invoice=j_inv_id LEFT JOIN nse_invoice_item ON j_invp_invoice_item=j_invit_id WHERE j_inv_to_company='".$id."' AND j_invp_date>=".$m1." AND j_invp_date<".$m2;
		$q = mysql_query($q);
		if (mysql_num_rows($q))
		{
			while ($g = mysql_fetch_array($q))
				$a[$g["j_invp_date"].$g["j_invp_id"]."p"]=array($g["j_invp_date"],0,($g["j_inv_number"]?$g["j_inv_number"]:""),($g["j_invit_name"]?str_replace(" (".$est["establishment_name"].")","",$g["j_invit_name"]):"Unspecified"),0-$g["j_invp_paid"],$g["j_invp_bank"],$g["j_invp_type"]);
		}

		if(count($a))
		{
			$_h.="<tr><td colspan='6' style='border-style:solid;border-width:1;border-color:#CCCCCC;font-weight:bold' bgcolor='#DDDDDD'>01 - ".date("d F, Y",$m3)."</td></tr>";

			ksort($a);
			foreach($a as $k => $v)
			{
				$_h.="<tr>";
				$_h.="<td style='width:20;border-style:solid;border-width:1;border-color:#CCCCCC'>".date("d/m/Y",$v[0])."</td>";
				$_h.="<td style='width:20;border-style:solid;border-width:1;border-color:#CCCCCC'>".$t[$v[1]]."</td>";
				$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>".$v[2]."</td>";
				$_h.="<td style='border-style:solid;border-width:1;border-color:#CCCCCC'>".$v[3]."</td>";
				$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC'>".($v[4]<0?"":number_format($v[4],2,"."," "))."</td>";
				$_h.="<td style='text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC'>".($v[4]<0?number_format($v[4],2,"."," "):"")."</td>";
				$_h.="</tr>";
				$db+=$v[4]<0?0:$v[4];
				$cr+=$v[4]<0?$v[4]:0;
			}

			$_h.="<tr><td colspan='4' style='color:#808080;text-align:right;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#EEEEEE'>Brought Forward</td>";
			$_h.="<td colspan=2 style='text-align:center;font-weight:bold;border-style:solid;border-width:1;border-color:#CCCCCC' bgcolor='#EEEEEE'>".number_format($db+$cr,2,"."," ")."</td>";
			$_h.="</tr>";

			$a=array();
		}

		$me++;
	}

	$me=1;
	$ye++;
}

$_h.="</table>";

$_h.="<br>";
$_h.="<br>";

$foot=J_inv_getele($invoice_default_foot);
if($foot)
{
	$foot=explode("~",$foot);
	$_h.="<div style='font-size:9pt;color:#808080;border-width:1px 0 0 0;border-color:#808080;border-style:solid'>".$foot[2]."</div><br>";
}

if(isset($_GET["vid"]))
	echo $_h."</div></center></body></html>";

elseif($pdf)
{
	$_h.="</body>";
	$_h.="</html>";

	@unlink($SDR."/stuff/accounts/pdf_statement/.pdf");
	@unlink($SDR."/stuff/accounts/pdf_statement/_.pdf");
	@unlink($SDR."/stuff/accounts/pdf_statement/_".$id.".pdf");
	$pdf_filename=$SDR."/stuff/accounts/pdf_statement/ledger_".date("Y-M",$ds)."_".$id.".pdf";
	$pdf_html=$_h;
	include $SDR."/utility/dompdf/pdf.php";
	$file=$ROOT."/stuff/accounts/pdf_statement/ledger_".date("Y-M",$ds)."_".$id.".pdf";
	if(!isset($send))
		include $SDR."/utility/force_download.php";
}

else
{
	echo $_h;

	$J_title1="Detailed Ledger";
	$J_title1=$est["establishment_name"];
	$J_title2="Detailed Ledger";
	$J_title3="From ".date("F Y",$de)." to ".date("F Y",$ds);
	$J_icon="<img src=".$ROOT."/ico/set/accounts_statements.png><var><img src=".$ROOT."/ico/set/spreadsheet.png></var>";
	$J_label=19;
	$r=$_SESSION["j_user"]["role"];
	$a="<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/edit.php?id=".$id."');return false\" onmouseover=\"J_TT(this,'Establishment manager')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/gohome-64.png></a>";
	if($acc||$per)
	{
		$v=$acc?$acc:$per;
		if(strpos($v,"~")!==false)
			$v=substr($v,0,strpos($v,"~"));
		$a.="<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?id=".$v."');return false\" onmouseover=\"J_TT(this,'Establishment contact')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/phone1.png></a>";
	}
	if($est["assessor_id"])
		$a.="<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?id=".$est["assessor_id"]."');return false\" onmouseover=\"J_TT(this,'Assessor')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/user_male.png></a>";
	if($est["rep_id1"])
		$a.="<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?id=".$est["rep_id1"]."');return false\" onmouseover=\"J_TT(this,'Rep')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/user_male.png></a>";
	$a.="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/age_view.php?id=".$id."');return false\" onmouseover=\"J_TT(this,'View account history')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/accounts_age.png></a>";
	if($r=="b"||$r=="d")
		$a.="<a href=go: onclick=\"J_W(ROOT+'/apps/notes/view.php?conotes=1&eid=".$id."&app=2');return false\" onmouseover=\"J_TT(this,'View account notes')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/note.png></a>";
	$a.="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/detailed_ledger_send.php?id=".$id."&de=".date("d/m/Y",$de)."&ds=".date("d/m/Y",$ds)."');return false\" onmouseover=\"J_TT(this,'Send Ledger')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/email.png></a>";
	$a.="<a href=go: onclick=\"j_Wi[".$_GET["j_W"]."]['F'][0]['W'].location=ROOT+'/apps/accounts/invoices/detailed_ledger.php?id=".$id."&de=".date("d/m/Y",$de)."&ds=".date("d/m/Y",$ds)."&pdf=1';return false\" onmouseover=\"J_TT(this,'Download PDF')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/pdf.png></a>";
	$a.="<div onmouseover=\"J_TT(this,'Select a new date range for this Ledger')\" style='margin:8 0 0 8'><input value=".date("d/m/Y",$de)." onclick=J_datePicker(self) size=8' style='margin:0 0 0 4'><input value=".date("d/m/Y",$ds)." onclick=J_datePicker(self) size=8 style='margin:0 4 0 4'><input type=button value=OK onclick=\"var w=j_Wi[".$_GET["j_W"]."]['F'][0]['W'];w.J_opaq();w.location=ROOT+'/apps/accounts/invoices/detailed_ledger.php?j_W=".$_GET["j_W"]."&id=".$id."&de='+this.previousSibling.previousSibling.value+'&ds='+this.previousSibling.value\"></div>";
	$J_foot=$a;
	include $SDR."/system/deploy.php";
	echo "<script>j_P.j_Wi[".$_GET["j_W"]."]['h3'].innerHTML=\"<b>From ".date("F Y",$de)." to ".date("F Y",$ds)."</b>\"</script>";
	echo "</body></html>";
}
?>