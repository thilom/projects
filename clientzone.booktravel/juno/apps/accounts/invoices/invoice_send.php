<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
$id=$_GET["id"];
$gi=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$id." LIMIT 1"));
include_once $SDR."/apps/accounts/invoices/types.php";

if(count($_POST))
{
	include $SDR."/system/activity.php";
	include $SDR."/apps/system/email/f/address_parser.php";
	if($sender["email"] && count($target))
	{
		$date=($gi["j_inv_date"]?date("d/m/Y",$gi["j_inv_date"]):"");
		$number=$gi["j_inv_type"]==9?$gi["j_inv_order"]:$gi["j_inv_number"];
		$f=$SDR."/stuff/accounts/pdf_invoice/".$number."_".$id.".pdf";
		$pdf=0;
		$xid=$id;
		include_once $SDR."/apps/accounts/invoices/pdf_invoice.php";
		if(file_exists($f))
			$pdf=$f;
		include $SDR."/custom/email_headers.php";
		$subject="AA TRAVEL GUIDES".strtoupper($types[$gi["j_inv_type"]])." (".$number.")";
		$message=substr(preg_replace('~[^a-zA-Z0-9 ,;:"@#_<>+=%&\*\-\$\'\(\)\[\]\?\.]~',"",strip_tags(str_replace("\n","<br>",str_replace("\r","",$_POST["message"])),"<br>")),0,100000);
		$msg=$j_email_header;
		$msg.="<b>AA TRAVEL GUIDES".strtoupper($types[$gi["j_inv_type"]])." No. ".$number.($date?" (".$date.")":"")."</b><hr>";
		$msg.=($message?$message."<br><br><hr>":"");
		$msg.="<a href='http://admin.aatravel.co.za/juno/apps/accounts/invoices/tab_view.php?id=".$id."&j_sent=".$EPOCH."' style='text-decoration:none'>";
		$msg.="<b>&#9658; VIEW ".strtoupper($types[$gi["j_inv_type"]])." ".$number."</b></a><br>";
		$msg.=($pdf?"<i style='font-size:8pt;color:#808080'>* Also see attached PDF ".$types[$gi["j_inv_type"]]:"");
		$msg.="<br><br><br>";
		$msg.=$j_email_footer;

		require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
		$mail=new PHPMailer(true);
		try
		{
			$mail->AddReplyTo($sender["email"],$sender["name"]);
			$mail->SetFrom($sender["email"],$sender["name"]);
			foreach($target as $k => $v)
				$mail->AddAddress($v["email"],$v["name"]);
			$mail->Subject =$subject;
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
			$mail->MsgHTML($msg);
			if($pdf)
				$mail->AddAttachment($pdf);
			$mail->Send();
			// insert into establishmernt notes
			if($gi["j_inv_to_company"])
			{
				include_once $SDR."/apps/notes/f/insert.php";
				$notes="";
				foreach($target as $k => $v)
				{
					// notes
					insertNote($gi["j_inv_to_company"],2,"INV".$gi["j_inv_id"],"Emailed Invoice No: ".preg_replace("~[^a-zA-Z0-9 \-]~","",$gi["j_inv_number"])." to: ".$v["name"]." (".$v["email"].")",0,1,0,4);
					// sys activity
					J_act("Accounts",7,$sender["name"]." sent Invoice No: ".preg_replace("~[^a-zA-Z0-9 \-]~","",$gi["j_inv_number"])." to: ".$v["name"]." (".$v["email"].")",$gi["j_inv_id"], $gi["j_inv_to_company"]);
				}
			}
			echo "Message Sent OK";
			echo "<script>setTimeout(\"window.parent.J_WX(self)\",2000)</script>";
		}
		catch (phpmailerException $e){echo $e->errorMessage();}
		catch (Exception $e){echo $e->getMessage();}
		die();
	}
	else
		echo "<script>alert('Target email address failure!')</script>";
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/system/email/f/people.js></script>";
echo "<body>";
echo "<script>J_send_people(1)</script>";
$J_title1="Send Invoice";
$J_title1="Send ".$types[$gi["j_inv_type"]];
$J_title3="<b>".($gi["j_inv_type"]==9?$gi["j_inv_order"]:$gi["j_inv_number"])."</b>".($gi["j_inv_date"]?" ".date("D d/m/Y",$gi["j_inv_date"]):"");
$J_icon="<img src=".$ROOT."/ico/set/email.png>";
$J_label=0;
$J_width=360;
$J_height=320;
$J_nomax=1;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>