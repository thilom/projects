function J_inv_ld(dco,dfp,dfa,drr,dtp,dtap,dct,doct,dh,doh,dcnh,ddnh,dc,doc,dac,db,df,dv,dm,vco,vus,veh,vec,vb,vef,oh,oc,ob,of,inc_d,inc_o,inc_oinc)
{
	var i,a
	j_P=parent
	j_P.j_G["accounts"]=[]
	a=j_P.j_G["accounts"]
	a["default"]={"vat":14,"currency":"ZAR","company":0,"assessor":0,"rep":0,"contact":0,"head":0,"condition":0,"bank":0,"foot":0}
	a["display"]={"company":["None"],"assessor":["None"],"contact":["None"],"head":["<h1>INVOICE</h1>"],"condition":["None"],"bank":["None"],"foot":["None"],"inventory":["None"],"item":["None"]}
	a=a["default"]
	a["increment"]=inc_d
	a["order_increment"]=inc_oinc
	a["company"]=dco
	a["from_person"]=dfp
	a["assessor"]=dfa
	a["rep"]=drr
	a["to_person"]=dtp
	a["to_ad_person"]=dtap
	a["contact"]=dct
	a["to_contact"]=doct
	a["head"]=dh
	a["order_head"]=doh
	a["creditnote_head"]=dcnh
	a["debitnote_head"]=ddnh
	a["condition"]=dc
	a["order_condition"]=doc
	a["ad_condition"]=dac
	a["bank"]=db
	a["foot"]=df
	a["vat"]=dv
	a["currency"]=dm
	a=j_P.j_G["accounts"]["display"]
	a["company"]=[]
	a["user"]=[]
	a["head"]=[[0,"Default","<h1>Invoice</h1>"]]
	a["condition"]=[[0,"None","<tt>Conditions</tt>"]]
	a["bank"]=[[0,"None","<tt>Bank Account</tt>"]]
	a["foot"]=[[0,"None","<tt>Footer</tt>"]]
	vco=vco.split("~");a["company"][vco[0]]=vco
	vus=vus.split("|");vus.pop();for(i in vus){vus[i]=vus[i].split("~");a["user"][vus[i][0]]=vus[i]}
	veh=veh.split("|");veh.pop();for(i in veh){veh[i]=veh[i].split("~");a["head"][veh[i][0]]=veh[i]}
	vec=vec.split("|");vec.pop();for(i in vec){vec[i]=vec[i].split("~");a["condition"][vec[i][0]]=vec[i]}
	vb=vb.split("~");a["bank"][vb[0]]=vb
	vef=vef.split("|");vef.pop();for(i in vef){vef[i]=vef[i].split("~");a["foot"][vef[i][0]]=vef[i]}
	a["inventory"]=[]
	a["item"]=[]
	function __p(v)
	{
		var i,u,r=""
		if(v)
		{
			v=v.split("|")
			v.pop()
			for(i in v)
			{
				u=v[i].split("~")
				if(!u[0])
					r+="<a href=go: onclick=\"return false\" onfocus=blur() class=jAh>"+(u[1]?u[1].replace(/'/g,""):"Generic")+"</a>"
				else
					r+="<a href=go: onclick=\"return false\" rev="+u[0]+" onfocus=blur()>"+u[1]+"</a>"
			}
		}
		return r
	}
	j_P.j_G["accounts"]["menu"]=[]
	a=j_P.j_G["accounts"]["menu"]
	a["head"]="<a href=go: onclick=\"return false\" rev=0 onfocus=blur()>Default</a>"+__p(oh)
	a["condition"]="<a href=go: onclick=\"return false\" rev=0 onfocus=blur()>None</a>"+__p(oc)
	a["bank"]="<a href=go: onclick=\"return false\" rev=0 onfocus=blur()>None</a>"+__p(ob)
	a["foot"]="<a href=go: onclick=\"return false\" rev=0 onfocus=blur()>None</a>"+__p(of)
	a["increment"]="<a href=go: onclick=\"return false\" rev=0 onfocus=blur()>None</a>"+__p(inc_o)
	j_P.j_G["accounts"]["ref"]={"j_inv_from_company":"company","j_inv_to_company":"company","j_inv_from_person":"user","j_inv_from_assessor":"user","j_inv_from_rep":"user","j_inv_to_person":"user","j_inv_from_contact":"user","j_inv_to_contact":"user","j_inv_head":"head","j_inv_bank":"bank","j_inv_condition":"condition","j_inv_foot":"foot"}
}