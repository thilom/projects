<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/secure.php";

$inv=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$_GET["id"]." LIMIT 1"));

// get notes
// these are for invoices only
$notes="";
$q="SELECT * FROM nse_notes WHERE note_item='INV".$_GET["id"]."' ORDER BY note_date DESC";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	include $SDR."/system/get.php";
	while($g=mysql_fetch_array($q))
	{
		$notes.=$g["note_id"]."~";
		$notes.=date("d/m/Y",$g["note_date"])."~";
		$notes.=$g["note_user"]."~";
		$notes.=J_Value("","people","",$g["note_user"])."~";
		$notes.=stripslashes($g["note_content"])."~";
		if($g["note_reminder"]&&$g["note_user"]==$_SESSION["j_user"]["id"])
			$notes.=date("d/m/Y",$g["note_reminder"]);
		$notes.="|";
	}
}

// wiindow attributes
$J_title1=($inv["j_inv_number"]?$inv["j_inv_number"]:"").($inv["j_inv_date"]?" ".date("D d/m/Y",$inv["j_inv_date"]):"");
$J_title2="Invoice Notes";

// user editing of notes?
$r=$_SESSION["j_user"]["role"];
$edit=$r=="b"||$r=="d"||$r=="s"?1:0;

// required
$app=2; // accounts // see custom/extra.js for list of ids
$eid=$inv["j_inv_to_company"]; // establishment code
$iid="INV".$inv["j_inv_id"]; // item id // in this case an identifier with invoice id

// feed this to note reader
include $SDR."/apps/notes/view.php";
?>