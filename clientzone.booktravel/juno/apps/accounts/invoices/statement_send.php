<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/secure.php";
include_once $SDR."/system/parse.php";
include_once $SDR."/system/get.php";

$id=$_GET["id"];
$est=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));

if(count($_POST))
{
	include $SDR."/apps/system/email/f/address_parser.php";
	if($sender["email"] && count($target))
	{
		$pdf=1;
		$send=1;
		include $SDR."/apps/accounts/invoices/statement.php";
		$f=$SDR."/stuff/accounts/pdf_statement/statement_".date("Y-M",$ds)."_".$id.".pdf";
		$pdf=0;
		if(file_exists($f))
			$pdf=$f;
		include $SDR."/custom/email_headers.php";
		$subject="AA TRAVEL GUIDES - STATEMENT ".date("F Y",$ds);
		$message=substr(preg_replace('~[^a-zA-Z0-9 ,;:"@#_<>+=%&\*\-\$\'\(\)\[\]\?\.]~',"",strip_tags(str_replace("\n","<br>",str_replace("\r","",$_POST["message"])),"<br>")),0,100000);
		$msg=$j_email_header;
		$msg.="<b>AA TRAVEL GUIDES</b> STATEMENT ".date("F Y",$ds)."<hr>";
		$msg.=($message?$message."<br><br><hr>":"");
		$msg.="<a href='http://admin.aatravel.co.za/".$ROOT."/apps/accounts/invoices/statement.php?vid=".$id."&ds=".date("d/m/Y",$ds)."&j_sent=".$EPOCH."' style='text-decoration:none'><b>&#9658; VIEW STATEMENT</b></a><br>";
		$msg.=($pdf?"<i style='font-size:8pt;color:#808080'>* Also see attached PDF </i>":"");
		$msg.="<br><br><br>";
		$msg.=$j_email_footer;

		require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
		$mail=new PHPMailer(true);
		try
		{
			$mail->AddReplyTo($sender["email"],$sender["name"]);
			$mail->SetFrom($sender["email"],$sender["name"]);
			foreach($target as $k => $v)
				$mail->AddAddress($v["email"],$v["name"]);
//			$mail->AddAddress('kim@aatravel.co.za','Kim');
//			$mail->AddAddress('meloth14@gmail.com','Thilo Muller');
			$mail->Subject =$subject;
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
			$mail->MsgHTML($msg);
			if($pdf)
				$mail->AddAttachment($pdf);
			foreach($target as $k => $v) {
				echo "{$v["email"]}<br>";
			}
			$mail->Send();
			// insert into establishmernt notes
			include_once $SDR."/apps/notes/f/insert.php";
			include_once $SDR."/system/activity.php";
			$notes="";
			foreach($target as $k => $v)
			{
				// notes
				insertNote($id,2,0,"Emailed Statement: ".date("F Y",$ds)." to: ".$v["name"]." (".$v["email"].")",0,1,0,4);
				// sys activity
				J_act("Accounts",7,$sender["name"]." sent Statement: ".date("F Y",$ds)." to: ".$v["name"]." (".$v["email"].")",0, $id);
			}
			mysql_query("INSERT INTO nse_accounts_send (establishment_code,date,item,user_id,file) VALUES ('".$id."',".$EPOCH.",'statement',".$_SESSION["j_user"]["id"].",'".($pdf?substr($pdf,strrpos($pdf,"/")+1):"")."')");
			$ok=1;
			if(!isset($bulk))
			{
				echo "Message Sent OK";
				echo "<script>setTimeout(\"window.parent.J_WX(self)\",2000)</script>";
			}
		}
		catch (phpmailerException $e){if(!isset($bulk))echo $e->errorMessage();}
		catch (Exception $e){if(!isset($bulk))echo $e->getMessage();}
		if(!isset($bulk))
			die();
	}
	elseif(!isset($bulk))
	{
		echo "<script>alert('Target email address failure!')</script>";
		die();
	}
}

if(!isset($bulk)) // bulk sending
{
	$ds=isset($_GET["ds"])&&$_GET["ds"]?J_dateParse($_GET["ds"]):$EPOCH;

	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/system/email/f/people.js></script>";
	echo "<body>";
	echo "<script>J_send_people(1)</script>";
	$J_title1="Send Statement";
	$J_title3="<b>".$est["establishment_name"]."</b><br>For: ".date("F, Y",$ds);
	$J_icon="<img src=".$ROOT."/ico/set/email.png>";
	$J_label=0;
	$J_width=360;
	$J_height=320;
	$J_nomax=1;
	$J_nostart=1;
	include $SDR."/system/deploy.php";
	echo "</body></html>";
}
?>