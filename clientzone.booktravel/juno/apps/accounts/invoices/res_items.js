function J_invL(d,r,v)
{
	j_P.J_note0()
	if(j_it[d])
	{
		if(j_it[d]==1)J_note(j_E,"<var>No&nbsp;items&nbsp;found</var>",0,1,3)
		else J_note(j_E,j_it[d],640,1,5,-1,0,2)
	}
	else if(r)
	{
		if(v)
		{
			var i=0,u,ti=0,ts=0,tu=0,td=0,tv=0,tt=0,x,p=""
			p+="<style type=text/css>.jACCinit{width:100%;background:#F5F5F5}.jACCinit td{border-style:solid;border-width:1;border-color:#CCC;padding:4;font-size:9pt;vertical-align:top}.jACCinit th{background:#EEE;color:#BBB;text-align:left}.jACCinit .jdbY,.jACCinit .jdbO,.jACCinit .jdbR,.jACCinit .jdbG{text-align:right}.jACCinit .jdbG{font-weight:bold}</style>"
			p+="<table class=jACCinit><tr><th>Date</th><th>Item</th><th>Code</th><th>No</th><th>Price</th><th>Sub</th><th>Disc</th><th>VAT</th><th>Total</th></tr>"
			v=v.split("|")
			while(v[i])
			{
				u=v[i].split("~")
				p+="<tr "+(u[0]?"onclick=J_W(ROOT+'/apps/inventory/view.php?id="+u[0]+"') onmouseover=\"J_TT(this,'View item details',1);this.style.background='#FFF'\" style=cursor:pointer":"onmouseover=\"this.style.background='#FFF'\"")+" onmouseout=\"this.style.background=''\">"
				p+="<td class=jdbC>"+(u[1]?u[1]:"")+"</td>"
				p+="<td width=90%><b style=white-space:nowrap>"+(u[2]?u[2]:"")+"</b></td>"
				p+="<td nowrap>"+(u[3]?u[3]:"")+"</td>"
				x=parseFloat(u[4])
				ti+=x
				p+="<td class=jdbG>"+x+"</td>"
				x=parseFloat(u[5])
				tu+=x
				p+="<td class=jdbY>"+(x?x.toFixed(2):"0.00")+"</td>"
				x=x*parseFloat(u[4])
				ts+=x
				p+="<td class=jdbY>"+(x?x.toFixed(2):"0.00")+"</td>"
				x=parseFloat(u[6])
				td+=x
				p+="<td class=jdbO>"+(x?x.toFixed(2):"0.00")+"</td>"
				x=parseFloat(u[7])
				tv+=x
				p+="<td class=jdbR>"+(x?x.toFixed(2):"0.00")+"</td>"
				x=parseFloat(u[8])
				tt+=x
				p+="<td class=jdbG><b>"+(x?x.toFixed(2):"0.00")+"<b></td>"
				p+="</tr>"
				i++
			}
			p+="<tr><td colspan=3></td>"
			p+="<td class=jdbG>"+ti+"</td>"
			p+="<td class=jdbY>"+tu.toFixed(2)+"</td>"
			p+="<td class=jdbY>"+ts.toFixed(2)+"</td>"
			p+="<td class=jdbO>"+td.toFixed(2)+"</td>"
			p+="<td class=jdbR>"+tv.toFixed(2)+"</td>"
			p+="<td class=jdbG>"+tt.toFixed(2)+"</td>"
			p+="</tr></table>"
			j_it[d]=p
			J_note(j_E,j_it[d],0,1,5,-1,0,2,0,-8)
		}
		else
		{
			j_it[d]=1
			J_note(j_E,"<var>No&nbsp;items&nbsp;found</var>",0,1,3)
		}
	}
	else
	{
		J_note(j_E,"<center><img src="+ROOT+"/ico/set/load.gif height=16 width=16><b>Retrieving</b></center>",0,1,5)
		$("j_IF").src=ROOT+"/apps/accounts/invoices/search_items.php?i=1&d="+d
	}
}