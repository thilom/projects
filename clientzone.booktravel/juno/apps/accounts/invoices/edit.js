document.write("<style type=text/css>"+(j_FF?"":".jinvD input{width:75}")+"</style>")
jinE=[]
jvat=0
jNi=1
j_it=0
jclck=0
jid=0
jDone=0
oid=0
ado=0
add_it=[]
types={0:"Undefined",1:"Pro-Forma",2:"Tax Invoice",3:"Credit Note",4:"Debit Note",9:"Order"}
type=0
cnote=0
dnote=0
role="c"
locked=0

function J_inv_edit(vari,c_dt,c_pr,o_id,c_note,d_note,e,pp,d,n,ni,v,vfco,vtco,itco,vfpr,vfas,vfrp,vfrp2,vtpr,vfct,vtct,vh,vc,vb,vf,addit,aditco,pt)
{
	w="j_Wi["+j_W+"]['F']["+j_Wn+"]['W']"
	jid=d
	staff=e=="d"||e=="b"||e=="s"?1:0
	role=e
	oid=o_id?o_id:0
	if(c_note)
		cnote=c_note
	else if(d_note || (d&&v[27]*1==4))
		dnote=1
	ado=vari=="ad"?1:0 // if an ad order
	var i,c,u,a=j_P.j_G["accounts"],the_co=0
	j_Def=a["default"]
	j_Dsp=a["display"]
	j_Opt=a["menu"]
	j_Ref=a["ref"]
	a=j_Dsp

	v=v?v.split("~"):[]

	type=d?v[27]*1:cnote?3:dnote?4:oid?9:1
	ordid=v[28]?v[28]:0
	invid=v[29]?v[29]:0
	locked=d&&v[30]?1:0

	if(vfco){vfco=vfco.split("~");a["company"][vfco[0]]=vfco}
	if(vtco){vtco=vtco.split("~");a["company"][vtco[0]]=vtco}
	if(itco){co=co.split("|");u=co[i].split("~");a["company"][u[0]]=u}
	if(vfpr){vfpr=vfpr.split("~");a["user"][vfpr[0]]=vfpr}
	if(vfas){vfas=vfas.split("~");a["user"][vfas[0]]=vfas}
	if(vfrp){vfrp=vfrp.split("~");a["user"][vfrp[0]]=vfrp}
	if(vfrp2){vfrp2=vfrp2.split("~");a["user"][vfrp2[0]]=vfrp2}
	if(vfct){vfct=vfct.split("~");a["user"][vfct[0]]=vfct}
	if(vtpr){vtpr=vtpr.split("~");a["user"][vtpr[0]]=vtpr}
	if(vtct){vtct=vtct.split("~");a["user"][vtct[0]]=vtct}
	if(vh){vh=vh.split("~");a["head"][vh[0]]=vh}
	if(vc){vc=vc.split("~");a["condition"][vc[0]]=vc}
	if(vb){vb=vb.split("~");a["bank"][vb[0]]=vb}
	if(vf){vf=vf.split("~");a["foot"][vf[0]]=vf}

	a="<form method=post onsubmit=J_opaq() enctype=multipart/form-data>"
	u="<input type=hidden name=j_inv_"
	a+=u+"id value="+d+">"
	a+=u+"increment_id value="+(ni?ni:v[5]?v[5]:j_Def["increment"])+">"
	jvat=v[15]?v[15]:j_Def["vat"]
	a+=u+"vat value="+jvat+">"
	dm=v[22]?v[22]:j_Def["currency"]
	a+=u+"currency value="+dm+">"

	a+="<div class=jinv>"
	u=v[0]?v[0]:oid?j_Def["order_head"]:cnote?3:dnote?4:j_Def["head"]
	a+="<input type=hidden name=j_inv_head value="+u+">"
	a+="<div"+(type==3||type==4||type==9?"":" onclick=J_inv_get(this) id=j_inv_head")+" onmouseover=\"J_TT(this,'<b>Header</b>')\">"+(j_Dsp["head"][u]?j_Dsp["head"][u][2]:j_Dsp["head"][0][2])+"</div>"

	a+="<table class=jinv_a>"

	u=(v[1]?v[1]:j_P.J_date("d/m/Y H:i"))
	a+="<tr class=jinv_tp><th>Date "+(staff?"<input type=text name=j_inv_date value=\""+u+"\" onclick=j_P.J_datePicker(self,3) class=jinvc onmouseover=\"J_TT(this,'<b>Date</b><br>DD/MM/YYYY HH:MM')\">":u)+"</th>"

	u=(v[2]?v[2]:oid&&n?n:"")
	a+="<td>Order No <input type="+(staff?"text id=j_inv_number class=jinvc":"hidden")+" name=j_inv_order value=\""+u+"\""+(d?"":" onmouseover=\"J_TT(this,'<b>Provisional Number</b><br>Actual number will be inserted after save')\" style=color:#BBB")+">"+(staff?"":u)+"</td></tr>"

	u=(v[3]?v[3]:"")
	a+="<tr class=jinv_tp><th>"+(cnote?"Inv/":"")+"Reference <input type="+(staff?"text":"hidden")+" name=j_inv_reference class=jinvc value=\""+(u?u:cnote&&cnote!=1?cnote:"")+"\""+(cnote?" style=font-weight:bold onmouseover=\"J_TT(this,'<b>ATTENTION!</b> You can link this credit note to an invoice. When referenceing to an invoice, ensure the entire invoice number is used')\"":"")+">"+(staff?"":u)+"</th>"

	u=(v[4]?v[4]:!oid&&n?n:"")
	a+="<td>"+(cnote?"C/Note":dnote?"D/Note":"Invoice")+" No. <input type="+(staff&&type!=2?"text id=j_inv_number":"hidden")+" name=j_inv_number value=\""+u+"\""+(d?" style=font-weight:bold":" onmouseover=\"J_TT(this,'<b>Provisional Number</b><br>Actual number will be inserted after save')\" style=color:#BBB")+">"+(staff&&type!=2?"":u)+"</td></tr>"

	u=v[6]?v[6]:0
	frmco=u
	if(oid&&u)
		the_co=u
	a+="<input type=hidden name=j_inv_from_company value="+u+">"
	a+="<tr><th"+(oid?" style=cursor:default":" onclick=J_inv_get(this) id=j_inv_from_company")+" class=jinvc onmouseover=\"J_TT(this,'<b>From Company</b>')\">"+(!u&&oid?"":J_inv_cod("j_inv_from_company",j_Dsp["company"][u]))+"</th>"

	u=v[7]?v[7]:aditco?aditco:0
	if(!oid&&u)
		the_co=u
	a+="<input type=hidden name=j_inv_to_company value="+u+">"
	a+="<td id=j_inv_to_company"+(oid?" style=cursor:default":" onclick=J_inv_get(this)")+" class=jinvc onmouseover=\"J_TT(this,'<b>To Company</b>')\">"+((oid||u)&&j_Dsp["company"][u]?J_inv_cod("j_inv_to_company",j_Dsp["company"][u]):"<u>To</u>")+"</td></tr>"

	u=v[8]?v[8]:d?0:j_Def["from_person"]
	a+="<input type=hidden name=j_inv_from_person value="+u+">"
	a+="<tr><th"+(staff||oid?" onclick=J_inv_get(this)":"")+" id=j_inv_from_person class=jinvc onmouseover=\"J_TT(this,'<b>From Person</b>')\">"+(u&&j_Dsp["user"][u]?J_inv_cod("j_inv_from_person",j_Dsp["user"][u],v[8]?1:0):"<u>From Person</u>")+"</th>"

	u=v[12]?v[12]:d?0:oid?j_Def["to_ad_person"]:0
	a+="<input type=hidden name=j_inv_to_person value="+u+">"
	a+="<td rowspan=4"+(staff?" onclick=J_inv_get(this)":"")+" id=j_inv_to_person class=jinvc onmouseover=\"J_TT(this,'<b>To Person</b>')\">"+(u&&j_Dsp["user"][u]?J_inv_cod("j_inv_to_person",j_Dsp["user"][u]):"<u>Attention</u>")+"</td></tr>"

	u=v[9]?v[9]:j_Def["assessor"]
	a+="<input type=hidden name=j_inv_from_assessor value="+u+">"
	a+="<tr><th"+(staff||oid?" onclick=J_inv_get(this)":"")+" id=j_inv_from_assessor class=jinvc onmouseover=\"J_TT(this,'<b>From Assessor</b>')\">"+(u&&j_Dsp["user"][u]?J_inv_cod("j_inv_from_assessor",j_Dsp["user"][u],v[9]?1:0):"<u>From Assessor</u>")+"</th>"

	u=v[10]?v[10]:d?0:j_Def["rep"]
	a+="<input type=hidden name=j_inv_from_rep value="+u+">"
	a+="<tr><th onclick=J_inv_get(this) id=j_inv_from_rep class=jinvc onmouseover=\"J_TT(this,'<b>From Rep</b>')\">"+(u&&j_Dsp["user"][u]?J_inv_cod("j_inv_from_rep",j_Dsp["user"][u],v[10]?1:0):"<u>Rep</u>")+"</th>"

	u=v[11]?v[11]:0
	a+="<input type=hidden name=j_inv_from_rep2 value="+u+">"
	a+="<tr><th onclick=J_inv_get(this) id=j_inv_from_rep2 class=jinvc onmouseover=\"J_TT(this,'<b>From Rep</b>')\">"+(u&&j_Dsp["user"][u]?J_inv_cod("j_inv_from_rep2",j_Dsp["user"][u],v[11]?1:0):"<u>Rep</u>")+"</th>"

	u=v[13]?v[13]:d||oid?0:j_Def["contact"]
	a+="<input type=hidden name=j_inv_from_contact value="+u+">"
	a+="<tr><th"+(staff||oid?" onclick=J_inv_get(this)":"")+" id=j_inv_from_contact class=jinvc onmouseover=\"J_TT(this,'<b>From Accounts/Admin</b>')\">"+(u&&j_Dsp["user"][u]?J_inv_cod("j_inv_from_contact",j_Dsp["user"][u],v[13]?1:0):"<u>Accounts/Admin</u>")+"</th>"

	u=v[14]?v[14]:oid?j_Def["to_contact"]:0
	a+="<input type=hidden name=j_inv_to_contact value="+u+">"
	a+="<td"+(staff||!oid?" onclick=J_inv_get(this)":"")+" id=j_inv_to_contact class=jinvc onmouseover=\"J_TT(this,'<b>To Accounts/Admin</b>')\">"+(u&&j_Dsp["user"][u]?J_inv_cod("j_inv_to_contact",j_Dsp["user"][u]):"<u>Accounts/Admin</u>")+"</td></tr>"

	a+="<tr><td id=j_inv_info class=jinvinf colspan=2><textarea name=j_inv_info rows="+(j_FF?2:4)+" onfocus='this.rows=parseInt(this.rows)*2' onblur='this.rows=parseInt(this.rows)/2'>"+(v[16]?v[16].replace(/<br>/g,"\n"):"")+"</textarea></td></tr>"

	a+="</table>"

	a+="<table class=jinv_tb id=jTR>"
	a+="<tr id=jinv_h>"
	a+="<th>Date</th>"
	a+="<th>Item</th>"
	a+="<th>Code</th>"
	a+="<th>Qty</th>"
	a+="<th>Unit</th>"
	a+="<th>Price</th>"
	a+="<th"+(type==3||type==4||type==9?" class=ji":"")+">Disc</th>"
	a+="<th>Vat</th>"
	a+="<th>Total</th>"
	a+="</tr>"
	a+="<tbody id=jinvi>"
	var tq=0,tp=0,q="<input type=hidden name=j_invit_"
	if(v[31])
	{
		v[31]=v[31].split("|")
		i=0
		while(v[31][i])
		{
			u=v[31][i].split("`")
			u[6]=parseFloat(u[6]?u[6]:0)
			if(type==3&&u[6]>0)u[6]=0-u[6]
			u[7]=parseFloat(u[7]?u[7]:0)
			if(type==3&&u[7]>0)u[7]=0-u[7]
			u[8]=parseFloat(u[8]?u[8]:0)
			if(type==3&&u[8]<0)u[8]=0-u[8]
			u[9]=parseFloat(u[9]?u[9]:0)
			if(type==3&&u[9]>0)u[9]=0-u[9]
			u[10]=parseFloat(u[10]?u[10]:0)
			if(type==3&&u[10]>0)u[10]=0-u[10]
			u[12]=parseFloat(u[12]?u[12]:0)
			if(type==3&&u[12]>0)u[12]=0-u[12]
			c=u[5]=="Cancelled"?1:0
			a+="<tr"+(c?" style=background:red":"")+">"
			a+="<td class=jdbC><input type=text name=j_invit_date_"+i+" value='"+(u[2]?u[2]:"")+"' onclick=j_P.J_datePicker(self,3) onmouseover=\"J_TT(this,'<b>Date</b>DD/MM/YYYY')\"></td>"
			a+="<td id=j_invit_inventory_"+i+" class=jdbW onclick=J_inv_enter(this)><b>"+(u[3]?u[3]:"")+"</b><a href=go: onclick='if(!jclck);J_inv_U(this);return false' onmouseover=\"J_TT(this,'View description',1);jclck=1\" onmouseout=\"J_T0();jclck=0\" onfocus=blur()>&nbsp; &#9660;</a><div class=ji><i>"+(u[4]?u[4]:"")+"</i><em>"+(u[11]?u[11]:"")+"</em></div></td>"
			a+="<td class=jinB onclick=J_inv_enter(this) onmouseover=\"J_TT(this,'Edit item number/code.<br><var style=color:#CC0000>Type in <b>Cancelled</b> to mark this item as cancelled</var>')\">"+u[5]+"</td>"
			tq+=c?0:u[6]
			a+="<th class=jdbB id=0 "+(oid&&ado?"onmouseover=\"J_TT(this,'<b style=color:#CC0000>WARNING!</b> Quantity cannot be changed on this order')\"":"onclick=J_inv_enter(this) onmouseover=\"J_TT(this,'Edit quantity')\"")+">"+u[6]+"</th>"
			tp+=c?0:u[7]
			a+="<th class=jdbK onclick=J_inv_enter(this) id=1 onmouseover=\"J_TT(this,'Edit unit price')\">"+u[7].toFixed(2)+"</th>"
			var tv=u[6]*u[7]
			if(type==3&&tv>0)
				tv=0-tv
			a+="<th class=jdbY>"+tv.toFixed(2)+"</th>"
			a+="<th class="+(type==3||type==4||type==9?"ji":"jdbO onclick=J_inv_enter(this) onmouseover=\"J_TT(this,'Edit discount')\"")+" id=3>"+u[8].toFixed(2)+"</th>"
			a+="<th class=jdbR onclick=J_inv_enter(this) id=4 onmouseover=\"J_TT(this,'Edit vat/tax percentage. You can set the global tax default in settings')\">"+u[9].toFixed(2)+"</th>"
			a+="<th class=jdbG>"+u[10].toFixed(2)+"</th>"
			a+="<th class=ji>"
			a+=q+"id_"+i+" value="+(u[0]?u[0]:0)+">"
			a+=q+"inventory_"+i+" value="+(u[1]?u[1]:0)+">"
			a+=q+"code_"+i+" value="+(u[5]?u[5]:"")+">"
			a+=q+"quantity_"+i+" value="+(u[6]?u[6]:0)+">"
			a+=q+"price_"+i+" value="+(u[7]?u[7]:0)+">"
			a+=q+"discount_"+i+" value="+(u[8]?u[8]:0)+">"
			a+=q+"vat_"+i+" value="+(u[9]?u[9]:0)+">"
			a+=q+"total_"+i+" value="+(!c&&u[10]?u[10]:0)+">"
			a+=q+"name_"+i+" value=\""+(u[3]?u[3]:"")+"\">"
			a+="<textarea name=j_invit_description_"+i+">"+(u[4]?u[4]:"")+"</textarea>"
			a+=q+"serial_"+i+" value=\""+(u[11]?u[11]:"")+"\">"
			a+=q+"vat_percent_"+i+" value="+(u[12]?u[12]:0)+">"
			a+=q+"disc_percent_"+i+" value="+(u[13]?u[13]:0)+">"
			a+=q+"company_"+i+" value="+(u[14]?u[14]:0)+">"
			a+="</th></tr>"
			i++
		}
		a+=""
		jNi=i
	}
	a+="</tbody><tr id=jintt><td class=jinv_w colspan=3>"
	if(oid&&!d){}
	else
		a+="<i><input type=button onclick=J_inv_add() value=&#10010; class=jO40 onmouseover=\"j_P.J_OP(this,100);J_TT(this,'Add an item',1)\" onmouseout=\"j_P.J_OP(this,40);J_T0()\"></i>"
	a+="</td>"
	a+="<th class=jdbB>"+tq+"</th>"
	a+="<th class=jdbK>"+parseFloat(tp).toFixed(2)+"</th>"
	a+="<th class=jdbY>"+(v[23]?parseFloat(v[23]).toFixed(2):"0.00")+"</th>"
	a+="<th class="+(type==3||type==4||type==9?"ji":"jdbO")+">"+(v[24]?parseFloat(v[24]).toFixed(2):"0.00")+"</th>"
	a+="<th class=jdbR>"+(v[25]?parseFloat(v[25]).toFixed(2):"0.00")+"</th>"
	a+="<th class=jdbG>"+(v[26]?parseFloat(v[26]).toFixed(2):"0.00")+"</th>"
	a+="</tr>"
	a+="</table>"

	q="<input type=hidden name=j_inv_"
	a+=q+"total_price value="+(v[23]?parseFloat(v[23]).toFixed(2):"0.00")+">"
	a+=q+"total_discount value="+(v[24]?parseFloat(v[24]).toFixed(2):"0.00")+">"
	a+=q+"total_vat value="+(v[25]?parseFloat(v[25]).toFixed(2):"0.00")+">"
	a+=q+"total value="+(v[26]?parseFloat(v[26]).toFixed(2):"0.00")+">"

	a+="<i class=jinvcur><b id=j_inv_currency>*Currency: "+dm+" ~ VAT: "+jvat+"%</b></i>"

	if(!cnote&&!dnote)
		a+="<textarea id=j_inv_comment name=j_inv_comment onmouseover=\"J_TT(this,'<b>Comments</b><br>Leave comments or instructions for "+(oid?"AA Travel Guides":"the customer")+"')\" rows="+(j_FF?2:4)+" onfocus='this.rows=parseInt(this.rows)*2' onblur='this.rows=parseInt(this.rows)/2'>"+(v[17]?v[17].replace(/<br>/g,"\n"):"")+"</textarea>"

	if(role=="d"||role=="b")
	{
		if(!oid&&!cnote&&!dnote)
		{
			u=d?(v[19]?v[19]:0):j_Def["bank"]
			a+="<input type=hidden name=j_inv_bank value="+u+">"
			a+="<div onclick=J_inv_get(this) id=j_inv_bank onmouseover=\"J_TT(this,'<b>Bank Account</b><br>Choose bank account details into which customers must pay')\">"+(u&&j_Dsp["bank"][u]?j_Dsp["bank"][u][2]:j_Dsp["bank"][0][2])+"</div>"
		}

		if(!cnote&&!dnote)
		{
			u=v[18]?v[18]:d?0:ado?j_Def["ad_condition"]:oid?j_Def["order_condition"]:j_Def["condition"]
			a+="<input type=hidden name=j_inv_condition value="+u+">"
			a+="<div onclick=J_inv_get(this) id=j_inv_condition onmouseover=\"J_TT(this,'<b>Terms & Conditions</b><br>Select terms and conditions or instructions, etc')\">"+(u&&j_Dsp["condition"][u]?j_Dsp["condition"][u][2]:"")+"</div>"
		}

		u=d?(v[20]?v[20]:0):j_Def["foot"]
		a+="<input type=hidden name=j_inv_foot value="+u+">"
		a+="<div onclick=J_inv_get(this) id=j_inv_foot onmouseover=\"J_TT(this,'<b>Footer</b><br>This is for additional info or company info')\">"+(u&&j_Dsp["foot"][u]?j_Dsp["foot"][u][2]:j_Dsp["foot"][0][2])+"</div>"
	}

	if(d&&!staff&&pp!="Y") {
		a+="<b>ATTENTION!</b> This invoice cannot be edit by you. This may be because the invoice is closed or you do not have adequate access to do so. Contact the system curators to assist in editing this invoice."
	} else {
		a+="<hr><br><table><tr><td><input type=file name=doc onmouseover=\"J_TT(this,'Upload a document')\"></td><td width=99%><tt><b><var>IMPORTANT!</var></b> If you have a document or hardcopy of the order. Please scan it and upload it here.<br>Please name the file as order-form or signed-order, the system will add a date and invoice number to the file for you.</tt></td></table>"
		if(d&&window.unLock)
			a+="<hr><tt onclick=j_P.J_checkbox(this) style=cursor:pointer><input type=checkbox name=j_inv_locked"+(locked?" checked":"")+"> <b>Lock this "+types[type]+" from editing</b></tt>"
		u=(!addit&&c_dt?" checked":"")
		if (type!=3 && type!=4) {
			a+=(d&&staff?"<tt onclick=j_P.J_checkbox(this)><hr><input type=checkbox name=j_inv_cancel_date value=1"+u+"> <b>Cancel"+(u?"led":"")+"</b>"+(u?" "+c_dt+" by "+c_pr:"")+"</tt>":u?"<input type=hidden name=j_inv_cancel_date value=1>":"")
		}
		a+="<hr><table style=width:1><tr>"
		if(type<3&&staff)
			a+="<td><select name=j_inv_type id=j_inv_type onmouseover=\"J_TT(this,'<b>Type</b>')\">"+("<option value=1>Pro-forma<option value=2>Tax Invoice").replace("="+type+">","="+type+" selected>")+"</select></td>"
		else
			a+="<input type=hidden name=j_inv_type value="+type+">"
		if(staff||pp=="Y"||type==2)
		{
			a+="<td><input type=submit value=Save></td>"
			if(d&&staff&&type!=2)
				a+="<td><input type=button value=Delete onclick=\"if(confirm('WARNING!\\nInvoices are permanently deleted and\\nonly recoverable if previously preserved by a backup module\\n\\nContinue?')){J_opaq();location=ROOT+'/apps/accounts/invoices/edit.php?del="+d+"'}\"></td>"
			a+="</tr></table>"
		}
	}
	if(addit)
		a+="<input type=hidden name=addit value="+ado+">"

	a+="</form>"
	if(!addit&&c_dt)
		a+="<img src="+ROOT+"/apps/accounts/invoices/cancel.gif class=j_cancel>"
	a+="<iframe id=j_IF"
	//a+=" style=top:0;left:0"
	a+="></iframe>"
	a+="</body></html>"
	document.write(a)

	J_tr()

	if(j_Wn==0)
	{
		a="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_search.php');return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/search.png onmouseover=J_TT(this,'Search')></a>"
		if(d)
		{
			a+="<a href=go: onclick=\""+w+".J_opaq();"+w+".location='"+ROOT+"/apps/accounts/invoices/invoice_view.php?id="+d+"';return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/spreadsheet.png onmouseover=J_TT(this,'View')></a>"
			a+="<a href="+ROOT+"/apps/accounts/invoices/tab_view.php?id="+d+" target=_blank onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/print.png onmouseover=J_TT(this,'Print')></a>"
			a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_send.php?id="+d+"&invn="+(oid?v[2]:v[4]?v[4]:"")+"');return false\" onmouseover=J_TT(this,'Send') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/email.png></a>"
			if(the_co)
			{
				a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/age_view.php?id="+the_co+"');return false\" onmouseover=\"J_TT(this,'Account history')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_statements.png></a>"
				a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/statement.php?id="+the_co+"');return false\" onmouseover=J_TT(this,'Statement') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_age.png></a>"
			}
			if(staff||role=="a"||role=="r")
			{
				a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/notes.php?id="+d+"');return false\" onmouseover=J_TT(this,'Notes') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/note.png></a>"
				if(type<3)
				{
					a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/payments/edit.php?id="+d+"');return false\" onmouseover=J_TT(this,'Payments') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_payEdit.png></a>"
					a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/commission/edit_invoice_commission.php?id="+d+"');return false\" onmouseover=J_TT(this,'Commission') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/user_male.png></a>"
				}
			}
			a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/documents.php?id="+d+"');return false\" onmouseover=\"J_TT(this,'Manage documents')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/file_drawer.png></a>"
			a+="<a href="+ROOT+"/apps/accounts/invoices/pdf_download.php?id="+d+" onmouseover=\"J_TT(this,'Download PDF')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/pdf.png></a>"
		}
		if(cnote||type==3)
		{
			if(d&&cnote>1)
				a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/"+(role=="b"||role=="d"?"edit":"view")+".php?num="+cnote+"');return false\" onmouseover=\"J_TT(this,'Invoice for Credit Note')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_invEdit.png></a>"
		}
		else if(d&&window.creditNotes)
		{
			if(window.creditItems)
			{
				creditItems=creditItems.split("|")
				cItems=[]
				i=0
				while(creditItems[i])
				{
					u=creditItems[i].split("~")
					cItems[u[0]]=u[1]
					i++
				}
				creditItems=""
			}

			a+="<a href=go: onclick="
			creditNotes=creditNotes.split("|")
			if(creditNotes.length>1)
			{
				a+="\"J_note(this,'<b class=jA>"
				e=role=="b"||role=="d"?"edit":"view"
				i=0
				while(creditNotes[i])
				{
					u=creditNotes.split("~")
					a+="<a href=go: onclick=J_W(ROOT+'/apps/accounts/invoices/"+e+".php?id="+u[0]+"')>Credit Note: "+u[1]+" | Value: "+u[3]+"</a>"
					i++
				}
				a+="</b>');return false\""
			}
			else
			{
				u=creditNotes[0].split("~")
				a+="\"J_W(ROOT+'/apps/accounts/invoices/edit.php?id="+u[0]+"');return false\""
			}
			a+=" onmouseover=\"J_TT(this,'Credit Note"+(creditNotes.length!=1?"s":"")+"')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/spreadsheet_red_edit.png></a>"
		}
		J_footer(a)
	}
	if(!d&&ado)
	{
		j_P.j_Wi[j_W]["F"][j_Wn]["sup"].innerHTML="<img src="+ROOT+"/ico/set/order.gif onmouseover=J_OP(this,60) onmouseout=J_OP(this,30) class=jO30 id=jtab onclick=J_resize("+j_W+",'360,-1,0,0,0')>"
		$("j_IF").src=ROOT+"/apps/accounts/invoices/get_est_people.php?co="+frmco
	}
	jDone=1

	if(window.autoCreditNote && confirm("ATTENTION!\nA Credit Note was automatically issued on the cancelled invoice\nWould you like to view it?\n\n"))
		J_W(ROOT+"/apps/accounts/invoices/"+(role=="b"||role=="d"?"edit":"view")+".php?id="+autoCreditNote)
	else if(window.autoDeleteCreditNote)
		alert("ATTENTION!\nThe credit note associated with this invoice has been removed.\nAny commission associated with this invoice has not been re-instated")
	if(addit)
	{
		J_opaq()
		add_it=addit.split("~")
		setTimeout("J_inv_addit()",500)
	}
	else if(aditco)
		setTimeout("J_inv_put('j_inv_to_company','"+aditco+"')",99)
	else if(pt)
		J_icoAlert()
}

function J_inv_addit() // add invoice items after onload
{
	if(add_it.length)
	{
		var v=jNi
		J_inv_add()
		J_inv_put("j_invit_inventory_"+v,add_it[0])
		add_it.shift()
	}
	if(!add_it.length)
	{
		J_opaq(1)
		add_it=""
	}
}

function J_inv_set(s,v)
{
	return s.replace(/ class=jAs/g,"").replace(" rev="+v+" "," rev="+v+" class=jAs ").replace(" rev="+v+">"," rev="+v+" class=jAs>")
}

function Jv(d)
{
	return $N(d)[0].value
}

function J_inv_get(t)
{
	var i
	if(t.id=="j_inv_number")
		J_note("x",j_Opt["increment"]?"<b class=jA onclick=\"if(j_E.rev)"+w+".J_inv_inc(j_E.rev)\" onmouseover=\"J_TT(this,'<b>Invoice Number Incrementation</b><br>Use these preset incrementors to generate an invoice number')\">"+J_inv_set(j_Opt["increment"],$N("j_inv_increment_id")[0].value)+"</i>":"<b>Invoice Number Incrementation</b><br>None available! Type in an invoice number or create incrementors in invoice settings",0,4,0,j_Opt["increment"]?-1:0)
	else if(t.id=="j_inv_from_company"||t.id=="j_inv_to_company")
	{
		i=$N(t.id)[0].value
		if(i!="")
			J_note("x","<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/edit.php?id="+i+"&view=1');return false\" onfocus=blur() onmouseover=\"J_TT(this,'View establishment details')\"><img src="+ROOT+"/ico/set/gohome-64.png height=32 width=32></a>",0,4,0,3,0,4)
		J_note("x","<input type=text onkeyup="+w+".J_inv_ajx(this,'"+t.id+"') onmouseover=\"J_TT(this,'Type in a few letters of the establishment')\">",0,2,0,-1,0,6)
	}
	else if(t.id=="j_inv_from_person"||t.id=="j_inv_from_assessor"||t.id=="j_inv_from_rep"||t.id=="j_inv_from_rep2"||t.id=="j_inv_to_person"||t.id=="j_inv_from_contact"||t.id=="j_inv_to_contact")
	{
		i=$N(t.id)[0].value*1
		if(i>0)
			J_note("x","<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?id="+i+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'View person')\"><img src="+ROOT+"/ico/set/user_both.png height=32 width=32></a>",0,4,0,3,0,4)
		J_note("x","<input type=text onkeyup="+w+".J_inv_ajx(this,'"+t.id+"') onmouseover=\"J_TT(this,'Type a few letters of the persons name or surname')\">",0,2,0,-1,0,6)
	}
	else if(t.id=="j_inv_head"||t.id=="j_inv_bank"||t.id=="j_inv_condition"||t.id=="j_inv_foot")
		J_note("x","<b class=jA onclick=\"if(j_E.rev)"+w+".J_inv_put('"+t.id+"',j_E.rev)\">"+J_inv_set(j_Opt[j_Ref[t.id]],Jv(t.id))+"</i>",160,4,0,-1,0,2,0,0,240)
	else
		J_inv_act(t)
}

function J_inv_act(t)
{
	var s="<a href=go: onclick='return false' onfocus=blur()><img style=height:32;width:32;margin:2 src="+ROOT+"/ico/set/",u="<u onmouseover=J_note0(2)>",d=parseInt($N(t.id)[0].value),i=j_Ref[t.id]
	if(i=="company")
		J_note(t,u+(d?s+"gohome-64.png onclick=J_W('"+ROOT+"/apps/establishment_manager/edit.php?id="+d+"') onmouseover=\"J_TT(this,'View this Establishment')\"></a>"+s+"cancel.png onclick=\"J_note0();var w="+w+";w.$N('"+t.id+"')[0].value=0;w.$('"+t.id+"').innerHTML=''\" onmouseover=\"J_TT(this,'Clear Establishment')\"></a>":"")+"</u>",0,4,0,2,1,2,-10)
	else if(i=="user")
		J_note(t,u+s+"user_add.png onclick=J_W('"+ROOT+"/apps/people/edit_person.php?id=0') onmouseover=\"J_TT(this,'Create a person')\"></a>"+(d?s+"user_both.png onclick=J_W('"+ROOT+"/apps/people/view.php?id="+d+"') onmouseover=\"J_TT(this,'View this person')\"></a>"+s+"cancel.png onclick=\"J_note0();var w="+w+";w.$N('"+t.id+"')[0].value=0;w.$('"+t.id+"').innerHTML=''\" onmouseover=\"J_TT(this,'Clear person')\"></a>":"")+"</u>",0,4,0,2,1,2,-10)
	else
	{
		var a=s+"edit.png onclick=\""+w+".J_inv_ited('"+t.id+"')\" onmouseover=\"J_TT(this,'Edit item')\"></a>"
		if(oid&&!jid&&ado){}
		else if(type!=2)
			a+=s+"inventory.png onclick=\""+w+".J_invii('"+t.id+"')\" onmouseover=\"J_TT(this,'<b>Inventory Reference</b><br>Retrieve an inventory item which can be inserted"+(oid?"":" and edited")+"')\"></a>"
		if(oid&&!jid&&ado){}
		else if(type!=2||(type==2&&staff))
			a+=(!oid&&jid?s+"accounts_payEdit.png onclick=\"J_W('"+ROOT+"/apps/accounts/payments/edit.php?id="+jid+"')\" onmouseover=\"J_TT(this,'View payments')\"></a>":"")
		if(jid&&type<3&&(window.cnoteIssue||role=="d"))
		{
			u=t.parentNode.lastChild.childNodes
			if(window.cItems && cItems[u[0].value])
				a+=s+"spreadsheet_red.png onclick=\"J_W(ROOT+'/apps/accounts/invoices/edit.php?id="+cItems[u[0].value]+"');return false\" onmouseover=\"J_TT(this,'There is a Credit Note for this item')\"></a>"
			else
				a+=s+"spreadsheet_red_edit.png onclick=\"J_W(ROOT+'/apps/accounts/invoices/credit_note_item.php?cid="+jid+"&citem="+u[0].value+"&cinventory="+u[1].value+"&cquantity="+u[3].value+"&cprice="+u[4].value+"');return false\" onmouseover=\"J_TT(this,'<b>Create a Credit Note</b> for this item. NOTE! If you want to create a credit note for all items within this invoice - simply cancell the invoice.')\"></a>"
		}
		if(oid&&!jid&&ado){}
		else if(type!=2)
			a+=s+"cancel.png onclick=\""+w+".J_invDi('"+t.id+"')\" onmouseover=\"J_TT(this,'Remove item')\"></a>"
		J_note(t,a,0,3,0,2,1,6)
	}
}

function J_invii(d)
{
	J_note("p","<input type=text onkeyup="+w+".J_inv_ajx(this,'"+d+"') onmouseover=\"J_TT(this,'Type in a few letters of the item.')\">",0,3,0,-1,0,6)
}

function J_invDi(d)
{
	j_P.J_note0()
	d=$(d)
	var i,v,a=J_inv_LC(d)
	d=d.parentNode
	v=a[0].value*1
	if(v>0)
	{
		i=document.createElement("input")
		i.type="hidden"
		i.name=a[0].name
		i.value=0-v
		$T("form")[0].appendChild(i)
	}
	d.parentNode.removeChild(d)
	J_inv_tot()
}

function J_inv_ited(d)
{
	var t=$(d)
	J_note(t,"<input type=text onkeyup="+w+".J_inv_itedV('"+d+"','b',value) value=\""+$T(t,"b")[0].innerHTML+"\" onmouseover=J_TT(this,'Item') class=jW100 style=font-weight:bold><br><textarea onkeyup="+w+".J_inv_itedV('"+d+"','i',value) rows=4 onmouseover=J_TT(this,'Description') class=jW100 style=font-size:9pt>"+$T(t,"i")[0].innerHTML+"</textarea><br><input type=text onkeyup="+w+".J_inv_itedV('"+d+"','em',value) value='"+$T(t,"em")[0].innerHTML.replace(/purchase order no:/gi,"")+"' onmouseover=\"J_TT(this,'Numbers')\" class=jW100><br><tt style=font-size:8pt><br><strong style=color:#DD0000 style=#DD0000>WARNING!</strong> If this item was originally assigned as an inventory item, it will remain so regardless of changes made here. Rather delete inventory items and create custom items from scratch</tt>",t.clientWidth,3,0,8,0,6)
	$T(t,"div")[0].className=""
}

function J_inv_itedV(d,i,v)
{
	d=$(d)
	v=v.replace(/[^a-zA-Z0-9 !@#%&:;',\?\*\$\-\.\(\)\[\]]/g,"")
	$T(d,i)[0].innerHTML=v
	i=J_inv_LC(d)
	$T(d,"div")[0].className=""
	i[8].value=$T(d,"b")[0].innerHTML
	i[9].value=$T(d,"i")[0].innerHTML
	i[10].value=$T(d,"em")[0].innerHTML
}

function J_inv_ajx(t,d,g)
{
	clearTimeout(j_it)
	if(g)
	{
		var c=$N(oid?"j_inv_from_company":"j_inv_to_company")[0].value,v=j_P.$T(j_P.$("jnote6"),"input")[0].value
		c=c?c:0
		$("j_IF").src=ROOT+"/system/ajax.php?d="+d+"&v="+v.replace(/&/g,"+++")+(d.indexOf("company")>0?"&t=establishment":oid&&d=="j_inv_from_contact"?"&t=user"+(c?"&co="+c:""):d.indexOf("_from_")>0?"&t=user"+(oid?"":d.indexOf("assessor")>0?"&role=a":d.indexOf("rep")>0?"&role=r":d.indexOf("contact")>0?"&role=s":""):d.indexOf("_to_")>0?(oid?"&t=user&role=s":"&t=user"+(c?"&co="+c:"")):"&t=inventory&inv=1")
		j_P.J_note0(5)
		J_note(j_P.$("jnote6"),"<img src="+ROOT+"/ico/set/load.gif>",0,4,0,4,1,4,0,-4)
		J_T0()
	}
	else
		j_it=setTimeout("J_inv_ajx(0,'"+d+"',1)",1200)
}

function J_ajax_R(d,v)
{
	var n=0,a="",z=d.indexOf("_company")||d.indexOf("_person")||d.indexOf("_assessor")||d.indexOf("_rep")||d.indexOf("_contact")||d.indexOf("inventory")?1:0
	if(v)
	{
		var i=0,u
		v=v.split("|")
		n=v.length-1
		n=n>0?n:0
		while(v[i])
		{
			u=v[i].split("~")
			a+="<a href=go: onclick=\"return false\" rev="+u[0]+" onfocus=blur()>"+u[1]+"</a>"
			i++
		}
		J_note(z?j_P.$("jnote6"):"p","<b class=jA onclick=\"if(j_E.rev)"+w+".J_inv_put('"+d+"',j_E.rev)\"><a href=go: onclick=\"return false\" rev=0 onfocus=blur()>None</a>"+J_inv_set(a,$N(d).value)+"</b>",!d.indexOf("j_invit_inventory_")?300:200,4,0,-1,0,5,0,-3,240)
	}
	J_note(z?j_P.$("jnote6"):"p","<var><b>"+n+" Found</b></var>",0,3,2,0,1,4,0,-3)
}

function J_inv_LC(t)
{
	return t.parentNode.lastChild.childNodes
}

function J_inv_put(d,v,r)
{
	var i=!d.indexOf("j_invit_inventory_")?"inventory":j_Ref[d]
	if(r)
	{
		if(v)
		{
			v=v.split("~")
			if(add_it.length)
			{
				$(d).innerHTML=J_inv_cod(d,v,1)
				if(window.addItemPrice)
				{
					if(addItemPrice)
					{
						addItemPrice=addItemPrice.split("~")
						var td=$T($("jTR"),"tr")[1].childNodes
						td[3].innerHTML=addItemPrice[0]
						td[4].innerHTML=parseFloat(addItemPrice[1]).toFixed(2)
						J_inv_tot()
						addItemPrice=0
					}
				}
				J_inv_addit()
			}
			else
			{
				j_Dsp[i][v[0]]=v
				$(d).innerHTML=J_inv_cod(d,v,1)
				$N(d)[0].value=v[0]
			}
		}
		else
			J_note(j_P.$("jnote2"),"<var><b>None found</b></var>",0,3,2)
	}
	else if(!add_it.length&&j_Dsp[i]&&j_Dsp[i][v])
	{
		j_P.J_note0(1)
		if(i=="inventory")
		{
			J_inv_cod(d,j_Dsp[i][v])
			if(add_it.length)
				J_inv_addit()
		}
		else
		{
			if(!$(d))
				alert(d)
			$(d).innerHTML=i=="company"||i=="user"||i=="inventory"?J_inv_cod(d,j_Dsp[i][v]):j_Dsp[i][v][2]
			$N(d)[0].value=v
		}
	}
	else
	{
		v=ROOT+"/apps/accounts/invoices/get.php?d="+d+"&v="+v+"&ret="+(j_Ref[d]=="company"?1:j_Ref[d]=="user"?2:!d.indexOf("j_invit_inventory_")?9:3)
		$("j_IF").src=v
		j_P.clearTimeout(j_P.j_G["jnote"][2]["time"])
		J_note(j_P.j_E,"<b>Retrieving details</b>",0,4,4,0,1,3)
	}
}

function J_inv_U(t)
{
	t=t.nextSibling
	t.className=t.className?"":"ji"
}

function J_est_people(p,a)
{
	if(oid)
		J_inv_put("j_inv_from_contact",p,1)
	else
	{
		if(p)
			J_inv_put("j_inv_to_person",p,1)
		if(a)
			J_inv_put("j_inv_to_contact",a,1)
	}
	j_P.J_note0(3)
}

function J_inv_cod(d,r,c)
{
	j_P.J_note0(3)
	var v,i,s=0,a=""
	if(!d.indexOf("j_invit_inventory_"))
	{
		a="<b>"+r[1]+"</b><a href=more: onclick='J_inv_U(this);return false' onmouseover=\"J_TT(this,'View description',1);jclck=1\" onmouseout=\"J_T0();jclck=0\" onfocus=blur()>&nbsp; &#9660;</a><div class=ji><i>"+r[2]+"</i><em>"+(r[3]?r[3]:"")+"</em></div>"
		d=$(d)
		d.innerHTML=a
		d.nextSibling.innerHTML=r[3]?r[3]:""
		s=d.nextSibling.nextSibling.innerHTML*1
		if(s>0){}else d.nextSibling.nextSibling.innerHTML=1
		r[4]=type==3||type==4?0-parseFloat(r[4]):parseFloat(r[4])
		d.nextSibling.nextSibling.nextSibling.innerHTML=r[4]>-999999?r[4].toFixed(2):"00.00"
		$T(d,"div")[0].className=""
		s=J_inv_LC(d)
		s[1].value=r[0]
		s[2].value=r[3]
		if(s[3].value*1>0||s[3].value*1<0){}
		else s[3].value=1
		s[4].value=r[4]
		s[8].value=r[1]
		s[9].value=r[2]
		s[10].value=r[3]
		s=0
		if(jinE)
			jinE=[$("j_invit_inventory_"+(jNi-1)).nextSibling.nextSibling.nextSibling]
		J_inv_tot()
	}
	else
	{
		if(d.indexOf("company")>-1)
		{
			a+="<u>"+(d=="j_inv_from_company"?"From":"To")+"</u>"
			v=[0,"<dfn> </dfn><b>^</b>","<dfn> </dfn><tt><i>City</i> ^</tt>","<dfn> </dfn><tt><i>Email</i> ^</tt>","<dfn> </dfn><tt><i>Tel</i> ^</tt>","<dfn> </dfn><tt><i>Fax</i> ^</tt>","<dfn> </dfn><tt><i>Postal</i> ^</tt>","<dfn> </dfn><tt><i>Address</i> ^</tt>","<dfn> </dfn><tt><i>Code</i> ^</tt>","<dfn> </dfn><tt><i>Reg No</i> ^</tt>","<dfn> </dfn><tt><i>Vat No</i> ^</tt>"]
			if(jDone&&(d=="j_inv_to_company"||(oid&&d=="j_inv_from_company")))
			{
				$("j_IF").src=ROOT+"/custom/lib/get_est_people.php?co="+r[0]
				J_note($(oid?"j_inv_from_contact":"j_inv_to_person"),"<center><img src="+ROOT+"/ico/set/load.gif><br>Retrieving people for <b>"+r[1]+"</b><center>",0,4,9,0,-1,3)
			}
		}
		else
		{
			a+="<u>"+(d.indexOf("j_inv_from_person")>-1?"From Person":d.indexOf("j_inv_from_assessor")>-1?"From Assessor":d.indexOf("j_inv_from_rep")>-1?"Rep":d.indexOf("j_inv_from_rep2")>-1?"Rep":d.indexOf("person")>0?"Attention":"Accounts/Admin")+"</u>"
			v=[0,"<dfn> </dfn><b>^</b>","<dfn> </dfn><tt><i>City</i> ^</tt>","<dfn> </dfn><tt><i>Email</i> ^</tt>","<dfn> </dfn><tt><i>Tel</i> ^</tt>","<dfn> </dfn><tt><i>Fax</i> ^</tt>","<dfn> </dfn><tt><i>Postal</i> ^</tt>","<dfn> </dfn><tt><i>Address</i> ^</tt>","<dfn> </dfn><tt><i>Code</i> ^</tt>","<dfn> </dfn><tt><i>Vat No</i> ^</tt>"]
			s=[$N("j_inv_from_company")[0],$N("j_inv_to_company")[0]]
			s=(c||d=="j_inv_from_rep"||d=="j_inv_from_rep2"||d=="j_inv_from_contact"||d=="j_inv_to_contact"||((d=="j_inv_from_person"||d=="j_inv_from_assessor")&&s[0]&&s[0].value)||(d=="j_inv_to_person"&&s[1]&&s[1].value)?5:0)
		}
		i=1
		while(v[i])
		{
			if(i==s)
				break
			else if(r&&r[i])
			{
				if(i==1)
					r[i]=r[i].toUpperCase()
				a+=v[i].replace("^",r[i].replace(/<br>/g,", ").replace(/,/g,", ").replace(/,,/g,", ").replace(/,  /g,", "))
			}
			i++
		}
	}
	return a
}

function J_inv_inc(v,r,d)
{
	var f=$N("j_inv_number")[0],h=$N("j_inv_increment_id")[0]
	if(r)
	{
		f.value=d
		h.value=r
		j_P.J_note0(2)
		J_note(f,"<b>Generated</b>",0,4,4,0,1,3)
	}
	else if(v)
	{
		$("j_IF").src=ROOT+"/apps/accounts/invoices/increment_get.php?inc="+v
		J_note(j_P.$("jnote2"),"<b>Generating</b>",0,4,4,0,1,3)
	}
	else
	{
		f.value=""
		h.value=0
	}
}

function J_inv_enter(t)
{
	jinE=[t]
	var e
	if(t.className=="jdbW")
		J_inv_get(t)
	else
	{
		var v
		if(t.className=="jdbO")
		{
			e=J_inv_LC(t)
			e=parseFloat(e[12].value)
			v=e?e:t.innerHTML
		}
		else if(t.className=="jdbR")
		{
			v=J_inv_LC(t)
			if(type==3&&v>0)
				v=0-v
			v=parseFloat(v[11].value)+"%"
		}
		else
			v=t.innerHTML
		J_note(t,"<input id=invfld type=text onkeyup="+w+"."+(t.className=="jdbO"?"J_inv_disc_type(this,1)":"J_inv_val(this)")+" value=\""+v+"\" class=jW100 style=border:0>"+(t.className=="jdbO"?"<select onchange="+w+".J_inv_disc_type(this) class=jW100 style=border:0;display:block onmouseover=\"J_TT(this,'Is discount a fixed amount or percent?')\"><option value=0>R<option value=1"+(e?" selected":"")+">%</select>":""),t.className=="jinB"?120:t.className=="jdbO"||t.className=="jdbR"?50:80,3,0,-1,0,2)
		j_P.$("invfld").focus()
	}
}

function J_inv_disc_type(t,v)
{
	if(v)
	{
		v=J_inv_LC(jinE[0])
		v[12].value=t.nextSibling.value*1?t.value:0
		if(!t.nextSibling.value*1)
			v[5].value=t.value
	}
	else
	{
		v=J_inv_LC(jinE[0])
		v[5].value=0
		v[12].value=0
		t.previousSibling.value=t.value*1?0:"0.00"
		t=t.previousSibling
	}
	J_inv_val(t)
}

function J_inv_val(t)
{
	var c,v=t.value
	if(jinE[0].className=="jinB")
	{
		v=v.replace(/[^0-9A-Za-z ',;:!%$#&@+=_\n\/\[\]\(\)\.\*\?\-]/g,"")
		c=jinE[0]==$T(jinE[0].parentNode,"td")[2]?1:0
		if(c)
		{
			v=v.replace(/cancelled/i,"Cancelled")
			jinE[0].parentNode.style.background=v=="Cancelled"?"red":""
		}
		jinE[0].innerHTML=v
		J_inv_LC(jinE[0])[2].value=v
		if(c)
			J_inv_tot()
	}
	else
	{
		if(jinE[0].className=="jdbB")
		{
			v=v.replace(/[^0-9\-\.]/g,"")*1
			v=isNaN(v)?1:v
			if(type==3&&v>0)
				v=0-v
			J_inv_LC(jinE[0])[3].value=v
			jinE[0].innerHTML=v
		}
		else if(jinE[0].className=="jdbK")
		{
			v=v.replace(/[^0-9\-\.]/g,"")*1
			v=isNaN(v)?1:v
			if(type==3&&v>0)
				v=0-v
			J_inv_LC(jinE[0])[4].value=v
			jinE[0].innerHTML=v.toFixed(2)
		}
		else if(jinE[0].className=="jdbO")
		{
			v=v.replace(/[^0-9\-\.]/g,"")*1
			if(v>0)
				v=0-v
			J_inv_LC(jinE[0])[5].value=v<0?v:0
		}
		else if(jinE[0].className=="jdbR")
		{
			v=v.replace(/[^0-9\-\.]/g,"")
			if(isNaN(v))
				v=v.replace(/[^0-9\.]/g,"")*1
			if(type==3&&v>0)
				v=0-v
			J_inv_LC(jinE[0])[11].value=v>-9999999?v:0
		}
		else
		{
			if(type==3&&v>0)
				v=0-v
			jinE[0].innerHTML=parseFloat(v).toFixed(2)
		}
		J_inv_tot()
	}
}

function J_inv_tot()
{
	var t=$T(jinE[0].parentNode,"th"),i=0,v=[],c,j,u,l,m,z,p,canc=$T(jinE[0].parentNode,"td")[2].innerHTML=="Cancelled"?1:0,val=J_inv_LC(t[0])
	if(canc)
		t[5].innerHTML="0.00"
	else
	{
		while(i<6)
		{
			if(i==0)c=val[3]
			else if(i==1)c=val[4]
			else if(i==3)c=val[5]
			else if(i==4)c=val[11]
			else c=val[0]
			c=c.value.replace(/[^0-9\-\.]/g,"")*1
			v[i]=isNaN(c)?0:c
			if(type==3&&v[i]>0)
				v[i]=0-v[i]
			if(i==2)
			{
				v[9]=v[0]*v[1]
				if(type==3&&v[9]>0)
					v[9]=0-v[9]
			}
			else if(i==3)
			{
				v[3]=0
				if(v[9]>0)
				{
					l=parseFloat(val[12].value)
					if(l)
					{
						l=l>0?l:0
						v[3]=v[9]*(l/100)
					}
					else
					{
						v[3]=parseFloat(val[5].value)
						if(v[3]>v[9])
							v[3]=v[9]
					}
					if(type<3&&v[3]>0)
						v[3]=0-v[3]
					v[9]+=v[3]
				}
				t[3].innerHTML=v[3].toFixed(2)
				val[5].value=v[3]
			}
			else if(i==4)
			{
				c=c>-9999999?c:0
				v[4]=v[9]*(c/100)
				if(type==3&&v[4]>0)
					v[4]=0-v[4]
				t[4].innerHTML=v[4].toFixed(2)
				val[6].value=v[4]
			}
			else if(i==5)
				v[9]+=v[4]
			if(i==2||i==5)
			{
				t[i].innerHTML=v[9].toFixed(2)
				val[7].value=v[9]
			}
			i++
		}
	}
	t=$T($("jinvi"),"tr"),i=0,l=t.length,v=[0,0,0,0,0,0],p=[3,4,-1,5,6,7]
	while(i<l)
	{
		z=t[i].lastChild.childNodes
		if($T(t[i],"td")[2].innerHTML=="Cancelled")
			z[7].value=0
		else
		{
			j=0
			while(p[j])
			{
				if(p[j]<0)
					c=z[3].value*z[4].value
				else
					c=z[p[j]].value*1
				c=isNaN(c)?0:c
				v[j]+=c
				j++
			}
		}
		i++
	}
	u=$T($("jintt"),"th")
	i=0
	while(u[i])
	{
		u[i].innerHTML=i==0?v[i]:v[i].toFixed(2)
		if(i==2)$N("j_inv_total_price")[0].value=v[2]
		else if(i==3)$N("j_inv_total_discount")[0].value=v[3]
		else if(i==4)$N("j_inv_total_vat")[0].value=v[4]
		else if(i==5)$N("j_inv_total")[0].value=v[5]
		i++
	}
}

function J_inv_add()
{
	var a,t,n=document.createElement("tr")
	t=document.createElement("td")
	t.className="jdbC"
	a=$N("j_inv_date")[0].value
	if(!a)a=j_P.J_date("d/m/Y H:i")
	if(a.indexOf(" ")>0)a.substr(0,a.indexOf(" "))
	t.innerHTML="<input type=text name=j_invit_date_"+jNi+" value='"+a+"' onclick=j_P.J_datePicker(self,3) onmouseover=\"J_TT(this,'<b>Date</b>DD/MM/YYYY')\">"
	n.appendChild(t)
	t=document.createElement("td")
	t.id="j_invit_inventory_"+jNi
	t.className="jdbW"
	t.onclick=new Function("if(!jclck)J_inv_enter(this)")
	t.innerHTML="<b>Undefined</b><a href=go: onclick='J_inv_U(this);return false' onmouseover=\"J_TT(this,'View more',1);jclck=1\" onmouseout=\"J_T0();jclck=0\" onfocus=blur()>&nbsp; &#9660;</a><div class=ji><i></i><em></em></div>"
	n.appendChild(t)
	t=document.createElement("td")
	t.onclick=new Function("J_inv_enter(this)")
	t.className="jinB"
	t.onmouseover=new Function("J_TT(this,'<b>Item Code</b><br><var style=color:#CC0000>Type in <b>Cancelled</b> to mark this item as cancelled</var>')")
	n.appendChild(t)
	var i="<input type=hidden name=j_invit_",v=[0,"0.00","0.00","0.00","0.00","0.00",i+"id_"+jNi+" value=0>"+i+"inventory_"+jNi+" value=0>"+i+"code_"+jNi+" value=''>"+i+"quantity_"+jNi+" value=0>"+i+"price_"+jNi+" value=0>"+i+"discount_"+jNi+" value=0>"+i+"vat_"+jNi+" value=0>"+i+"total_"+jNi+" value=0>"+i+"name_"+jNi+" value=Undefined><textarea name=j_invit_description_"+jNi+"></textarea>"+i+"serial_"+jNi+" value=0>"+i+"vat_percent_"+jNi+" value="+jvat+">"+i+"disc_percent_"+jNi+" value=0>"+i+"company_"+jNi+" value=0>"]
	i=0
	jNi++
	a=["jdbB","jdbK","jdbY",type==3||type==4?"ji":"jdbO","jdbR","jdbG","ji"]
	while(i<7)
	{
		t=document.createElement("th")
		if(i!=2&&i!=5&&i!=6)
		{
			t.onclick=new Function("J_inv_enter(this)")
			t.onmouseover=new Function("J_TT(this,'Edit')")
		}
		t.id=i
		t.className=a[i]
		t.innerHTML=v[i]
		n.appendChild(t)
		i++
	}
	$("jinvi").appendChild(n)
	J_tr()
}