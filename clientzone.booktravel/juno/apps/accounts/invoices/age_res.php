<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include_once $SDR . "/system/parse.php";
include_once $SDR . "/system/get.php";

$date = isset($_GET["ds"]) ? J_dateParse($_GET["ds"]) : $EPOCH;
$y = date("Y", $date);
$m = date("m", $date);
$curr_s = mktime(0, 0, 0, date($m) + 1, date(1), date($y));
$curr_e = mktime(0, 0, 0, date($m), date(1), date($y));
$d30 = mktime(0, 0, 0, date($m) - 1, date(1), date($y));
$d60 = mktime(0, 0, 0, date($m) - 2, date(1), date($y));
$d90 = mktime(0, 0, 0, date($m) - 3, date(1), date($y));
$t_120 = 0;
$t_90 = 0;
$t_60 = 0;
$t_30 = 0;
$t_cur = 0;
$t_owe  = 0;
$t_pay = 0;

$p = "";
$x = "";
$fixed = isset($_GET["fix"]) ? 1 : 0;
$rep = 0;
$pp = array();

// if NOT for send_bulk_statements
if (!isset($send_bulk_statements)) {
	// constrain_user_establishments functions
	include_once $SDR . "/custom/constrain_user_establishments.php";
	$r = $_SESSION["j_user"]["role"];
	if ($r == "a" || $r == "r")
		$rep = $_SESSION["j_user"]["id"];

	$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
	$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 100);

	$owe1 = isset($_GET["min"]) && $_GET["min"] ? preg_replace("~[^0-9\.\-]~", "", $_GET["min"]) : 0;
	if ($owe1 > -9999999999) {
		
	}else
		$owe1 = 0;
	$owe2 = 0 - $owe1;
	$d = "";
	$r = "";

	// columns
	$dd = explode("~", $_GET["d"] . "id~est~d120~d90~d60~d30~curr~owe~paid~last");
	$ddd = array();
	$s = array();
	foreach ($dd as $k => $v) {
		$s[$v] = 1;
		$ddd[$v] = 1;
	}
	$t = array("X", "id", "est", "d120", "d90", "d60", "d30", "curr", "owe", "paid", "pay", "paydate", "qai", "qao", "vis", "ass", "rep", "con", "condet", "ema", "last", "tvl");
	$d = "";
	$n = 1;
	foreach ($t as $k => $v) {
		if (isset($ddd[$v])) {
			$d.=$n;
			$n++;
		}
		$d.="~";
	}

	$ema = "";
}

$x = "SELECT
DISTINCT e.establishment_code
,e.establishment_name
,e.assessor_id
,e.rep_id1";
if (isset($s["tvl"]))
	$x.=",d.tv_licence";
$x.=" FROM nse_invoice AS i";
if (isset($_GET["tp"]) && $_GET["tp"] == 2) {
	
}
else
	$x.=" LEFT JOIN nse_invoice_item_paid AS p ON i.j_inv_id=p.j_invp_invoice";
$x.=" JOIN nse_establishment AS e ON i.j_inv_to_company=e.establishment_code";
if (isset($_GET["rd"])) // exclude date irrelevant renewals
	$x.=" LEFT JOIN nse_establishment_assessment AS a ON i.j_inv_id=a.invoice_id";
if (isset($s["tvl"]))
	$x.=" JOIN nse_establishment_data AS d ON e.establishment_code=d.establishment_code";
if (isset($_GET["tp"]) && $_GET["tp"] == 2)
	$x.=" WHERE i.j_inv_date<" . $curr_s . (isset($fixed) ? " AND i.j_inv_created<" . $curr_s : "");
elseif (isset($fixed))
	$x.=" WHERE ((i.j_inv_date<" . $curr_s . " AND i.j_inv_created<" . $curr_s . ") OR (p.j_invp_date<" . $curr_s . " AND p.j_invp_created<" . $curr_s . "))";
else
	$x.=" WHERE (i.j_inv_date<" . $curr_s . " OR p.j_invp_date<" . $curr_s . ")";
if (isset($_GET["tp"])) {
	if ($_GET["tp"] == 1)
		$x.=" AND (i.j_inv_type=2 OR i.j_inv_type=3 OR i.j_inv_type=4)";
	else
		$x.=" AND i.j_inv_type=1";
}
else
	$x.=" AND (i.j_inv_type<6 AND i.j_inv_type != 1 )";
if ($fixed) {
	if (isset($_GET["tp"]) && $_GET["tp"] == 2)
		$x.=" AND i.j_inv_created<" . $curr_s;
	else
		$x.=" AND (i.j_inv_created<" . $curr_s . " OR p.j_invp_created<" . $curr_s . ")";
}
if (isset($_GET["rd"])) // exclude date irrelevant renewals
	$x.=" AND (a.assessment_id='' OR DATE_FORMAT(a.assessment_date,'%Y-%m-%d 00:00:00')<='" . date("Y-m-d 00:00:00", $curr_s) . "')";
if (isset($_GET["est"]))
	$x.=" AND e.establishment_code='" . $_GET["est"] . "'";
if ($rep) // isolate on rep
	$x.=" AND (e.assessor_id=" . $rep . " OR e.rep_id1=" . $rep . " OR i.j_inv_from_assessor=" . $rep . " OR i.j_inv_from_rep=" . $rep . " OR i.j_inv_from_rep2=" . $rep . ")";
else if (isset($_GET["ass"])) // isolate assessors
	$x.=" AND (e.assessor_id=" . $_GET["ass"] . " OR e.rep_id1=" . $_GET["ass"] . " OR i.j_inv_from_assessor=" . $_GET["ass"] . " OR i.j_inv_from_rep=" . $_GET["ass"] . " OR i.j_inv_from_rep2=" . $_GET["ass"] . ")";

if (isset($_GET["prv"]))
	$x.=" AND j_inv_to_company IN (SELECT DISTINCT establishment_code FROM nse_establishment_location WHERE province_id=" . $_GET["prv"] . " GROUP BY establishment_code)";
if (isset($_GET["i1"])) {
	include $SDR . "/custom/lib/inventory_groups.php";
	$x.=" AND j_inv_id IN (SELECT DISTINCT j_invit_id FROM nse_invoice_item WHERE j_invit_inventory=" . ($_GET["i1"] == 1 ? getQA() : $_GET["i1"]) . " GROUP BY j_invit_id)";
}

$x.=" ORDER BY e.establishment_name";
//echo $x . "<hr>";

$rs = mysql_query($x);
if (mysql_num_rows($rs)) {

	include_once $SDR . "/custom/lib/est_items.php";

	function paid($e, $ds=-1, $de=0) {
		global $last, $fixed, $curr_s;
		$v = 0;
		$q = "SELECT j_invp_paid,j_invp_date,j_invp_invoice FROM nse_invoice_item_paid JOIN nse_invoice ON j_invp_invoice=j_inv_id WHERE j_inv_to_company='" . $e . "' AND j_invp_date>=" . $ds . " AND j_invp_date<" . $de;
		if ($fixed)
			$q.=" AND j_invp_created<" . $curr_s;
		$q.=" ORDER BY j_invp_date";
		$q = mysql_query($q);
		if (mysql_num_rows($q)) {
			while ($g = mysql_fetch_array($q)) {
				$v+=$g["j_invp_paid"];
				$last = array($g["j_invp_invoice"], date("d/m/Y", $g["j_invp_date"]));
			}
		}
		return $v;
	}

//	if (!isset($_GET["csv"])) {
		$n++;
		$jL0 = mysql_num_rows($rs);

		while ($g = mysql_fetch_array($rs)) {
			if ($n >= $jL1 || isset($_GET['csv'])) {
				if ($n < $jL2 || isset($_GET['csv'])) {
					if (!isset($send_bulk_statements)) {
						$x = "~";
						$x.= $g["establishment_code"] . "~";
						$x.= isset($_GET["csv"])?str_replace(","," ",$g["establishment_name"]):$g["establishment_name"] ;
						$x .= "~";
						$last = array(0, "");
					}
					$to = 0;
					$tp = 0;
					// 120+ days
					$t = 0;
					if (isset($_GET["tp"]) && $_GET["tp"] == 2) {
						$pd = 0;
					} else {
						$pd = paid($g["establishment_code"], 0, $d90);
					}
					$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date<" . $d90 . ($fixed ? " AND j_inv_created<" . $curr_s : "");
					if (isset($_GET["tp"])) {
						if ($_GET["tp"] == 2) {
							$qq.=" AND j_inv_type=1";
						} else {
							$qq.=" AND j_inv_type>1 AND j_inv_type<6";
						}
					} else {
						$qq.=" AND j_inv_type<6";
					}
					$qq = mysql_query($qq);
					if (mysql_num_rows($qq)) {
						while ($v = mysql_fetch_array($qq)) {
							$t+=$v["j_inv_total"];
						}
					}
					$to+=$t;
					
					$tp+=$pd;
					$t = $t - $pd;
					
					
					if (!isset($send_bulk_statements)) {
						$x.=($t ? number_format($t, 2, ".", "") : "0.00") . "~";
						$tt120 = $t ? number_format($t, 2, ".", "") : 0;
					}
//					echo "t: $t | $t_120<br>";
					// 90 days
					$t = 0;
					if (isset($_GET["tp"]) && $_GET["tp"] == 2)
						$pd = 0;else
						$pd = paid($g["establishment_code"], $d90, $d60);
					$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $d90 . " AND j_inv_date<" . $d60 . ($fixed ? " AND j_inv_created<" . $curr_s : "");
					if (isset($_GET["tp"])) {
						if ($_GET["tp"] == 2)
							$qq.=" AND j_inv_type=1";
						else
							$qq.=" AND j_inv_type>1 AND j_inv_type<6";
					}
					else
						$qq.=" AND j_inv_type<6";
//					var_dump($qq);
					$qq = mysql_query($qq);
					if (mysql_num_rows($qq)) {
						while ($v = mysql_fetch_array($qq))
							$t+=$v["j_inv_total"];
					}
					$to+=$t;
					$tp+=$pd;
					$t = $t - $pd;
					if (!isset($send_bulk_statements)) {
						$x.=($t ? number_format($t, 2, ".", "") : "0.00") . "~";
						$tt90 = $t ? number_format($t, 2, ".", "") : 0;
					}
					// 60 days
					$t = 0;
					if (isset($_GET["tp"]) && $_GET["tp"] == 2)
						$pd = 0;else
						$pd = paid($g["establishment_code"], $d60, $d30);
					$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $d60 . " AND j_inv_date<" . $d30 . ($fixed ? "  AND j_inv_created<" . $curr_s : "");
					if (isset($_GET["tp"])) {
						if ($_GET["tp"] == 2)
							$qq.=" AND j_inv_type=1";
						else
							$qq.=" AND j_inv_type>1 AND j_inv_type<6";
					}
					else
						$qq.=" AND j_inv_type<6";
					$qq = mysql_query($qq);
					if (mysql_num_rows($qq)) {
						while ($v = mysql_fetch_array($qq))
							$t+=$v["j_inv_total"];
					}
					$to+=$t;
					$tp+=$pd;
					$t = $t - $pd;
					if (!isset($send_bulk_statements)) {
						$x.=($t ? number_format($t, 2, ".", "") : "0.00") . "~";
						$tt60 = $t ? number_format($t, 2, ".", "") : 0;
					}
					// 30 days
					$t = 0;
					if (isset($_GET["tp"]) && $_GET["tp"] == 2)
						$pd = 0;else
						$pd = paid($g["establishment_code"], $d30, $curr_e);
					$qq = "SELECT j_inv_date,j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $d30 . " AND j_inv_date<" . $curr_e . ($fixed ? " AND j_inv_created<" . $curr_s : "");
					if (isset($_GET["tp"])) {
						if ($_GET["tp"] == 2)
							$qq.=" AND j_inv_type=1";
						else
							$qq.=" AND j_inv_type>1 AND j_inv_type<6";
					}
					else
						$qq.=" AND j_inv_type<6";
					$qq = mysql_query($qq);
					if (mysql_num_rows($qq)) {
						while ($v = mysql_fetch_array($qq))
							$t+=$v["j_inv_total"];
					}
					$to+=$t;
					$tp+=$pd;
					$t = $t - $pd;
					if (!isset($send_bulk_statements)) {
						$x.=($t ? number_format($t, 2, ".", "") : "0.00") . "~";
						$tt30 = $t ? number_format($t, 2, ".", "") : 0;
					}
					// current
					$t = 0;
					if (isset($_GET["tp"]) && $_GET["tp"] == 2)
						$pd = 0;else
						$pd = paid($g["establishment_code"], $curr_e, $curr_s);
					$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $curr_e . " AND j_inv_date<" . $curr_s . ($fixed ? " AND j_inv_created<" . $curr_s : "");
					if (isset($_GET["tp"])) {
						if ($_GET["tp"] == 2)
							$qq.=" AND j_inv_type=1";
						else
							$qq.=" AND j_inv_type>1 AND j_inv_type<6";
					}
					else
						$qq.=" AND j_inv_type<6";
					$qq = mysql_query($qq);
					if (mysql_num_rows($qq)) {
						while ($v = mysql_fetch_array($qq))
							$t+=$v["j_inv_total"];
					}
					$to+=$t;
					$tp+=$pd;
					$t = $t - $pd;
					if (!isset($send_bulk_statements)) {
						$x.=($t ? number_format($t, 2, ".", "") : "0.00") . "~";
						$ttcurr = $t ? number_format($t, 2, ".", "") : 0;
					}
					$to = $to - $tp;
					if (!isset($send_bulk_statements)) {
						$x.=($to ? number_format($to, 2, ".", "") : "0.00") . "~";
						$x.=($tp ? number_format(0 - $tp, 2, ".", "") : "0.00") . "~";
						if (isset($s["pay"])) {
							if ($last[0]) {
								$gp = mysql_fetch_array(mysql_query("SELECT j_inv_paid FROM nse_invoice WHERE j_inv_id=" . $last[0] . " LIMIT 1"));
								$x.=($gp["j_inv_paid"] ? number_format(0 - $gp["j_inv_paid"], 2, ".", "") : "");
							}
							$x.="~";
						}
						if (isset($s["paydate"]))
							$x.=$last[1] . "~";
						if (isset($s["qai"]))
							$x.=entered($g["establishment_code"]) . "~";
						if (isset($s["qao"]))
							$x.=qa_out($g["establishment_code"]) . "~";
						if (isset($s["vis"])) {
							$v = visit($g["establishment_code"]);
							$x.=$v[0] . "~";
						}
						if (isset($s["ass"])) {
							$u = ($g["assessor_id"] ? $g["assessor_id"] : "");
							$x.=$u . "~";
							if ($u && !isset($pp[$u]))
								$pp[$u] = J_Value("", "people", "", $u);
						}
						if (isset($s["rep"])) {
							$u = ($g["rep_id1"] ? $g["rep_id1"] : "");
							$x.=$u . "~";
							if ($u && !isset($pp[$u]))
								$pp[$u] = J_Value("", "people", "", $u);
						}
						if (isset($s["con"]) || isset($s["condet"]) || isset($s["ema"])) {
							$ema = "";
							$eid = $g["establishment_code"];
							$no_js = 1;
							include $SDR . "/custom/lib/get_est_people.php";
							$tpd = ($accounts ? $accounts : ($manager ? $manager : ($owner ? $owner : $anybody)));
							if (isset($s["con"])) {
								$x.=$tpd . "~";
								$pp[$tpd] = J_Value("", "people", "", $tpd) . "~" . numbers($g["establishment_code"], $tpd);
							}
							if (isset($s["condet"]))
								$x.="~";
							if (isset($s["ema"]))
								$x.=$ema . "~";
						}
						if (isset($s["tvl"]))
							$x.=($g["tv_licence"] ? $g["tv_licence"] : "") . "~";
						$x.=($last ? $last : "");
						$x.="|";
					}

					if (isset($send_bulk_statements)) {
						if ($to > $owe) {
							$r.=sent($g["establishment_code"]) . "~";
							$r.=$g["establishment_code"] . "~";
							$r.=$g["establishment_name"] . "~";
							$r.=number_format($to, 2, ".", " ") . "~";

							if (isset($s["ass"])) {
								$u = ($g["assessor_id"] ? $g["assessor_id"] : "");
								$r.=$u . "~";
								if ($u && !isset($pp[$u]))
									$pp[$u] = J_Value("", "people", "", $u);
							}
							if (isset($s["rep"])) {
								$u = ($g["rep_id1"] ? $g["rep_id1"] : "");
								$r.=$u . "~";
								if ($u && !isset($pp[$u]))
									$pp[$u] = J_Value("", "people", "", $u);
							}
							$eid = $g["establishment_code"];
							include $SDR . "/custom/lib/get_est_people.php";
							$tpd = ($accounts ? $accounts : ($manager ? $manager : ($owner ? $owner : $anybody)));
							if (!isset($pp[$tpd]))
								$pp[$tpd] = trim(J_Value("", "people", "", $tpd)) . "~" . str_replace("(0) ", "(0)", trim(trim(numbers($g["establishment_code"], $tpd), ";"))) . "~" . $email_address_found;
							$r.=$tpd . "~";
							if (isset($s["condet"]))
								$r.="~";
							$r.="|";
						}
					}
					elseif ($to > $owe1 || $to < $owe2) {
						$r.=$x;
						$n++;
						$t_120 += $tt120;
						$t_90 += $tt90;
						$t_60 += $tt60;
						$t_30 += $tt30;
						$t_cur += $ttcurr;
						$t_owe += $to;
						$t_pay += $tp;
					}
				}
				else
					break;
			}
		}
//	} else {
//		$hed = "";
//		$tot = "";
//		$i = 0;
//		$c = 0;
//		$t_120 = 0;
//		$t_90 = 0;
//		$t_60 = 0;
//		$t_30 = 0;
//		$t_cur = 0;
//		$t_owe = 0;
//		$t_pay = 0;
//
//		while ($g = mysql_fetch_array($rs)) {
//			$r.=$g["establishment_code"] . ",";
//			$r.=trim(str_replace(array(",", "\n"), " ", $g["establishment_name"])) . ",";
//			$to = 0;
//			$tp = 0;
//			$last = array(0, "");
//			// 120+ days
//			$t = 0;
//			if (isset($_GET["tp"]) && $_GET["tp"] == 2)
//				$pd = 0;else
//				$pd = paid($g["establishment_code"], 0, $d90);
//			$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date<" . $d90 . ($fixed ? " AND j_inv_created<" . $curr_s : "");
//			if (isset($_GET["tp"])) {
//				if ($_GET["tp"] == 2)
//					$qq.=" AND j_inv_type=1";
//				else
//					$qq.=" AND j_inv_type>1 AND j_inv_type<6";
//			}
//			else
//				$qq.=" AND j_inv_type<6";
//			$qq = mysql_query($qq);
//			if (mysql_num_rows($qq)) {
//				while ($v = mysql_fetch_array($qq))
//					$t+=$v["j_inv_total"];
//			}
//			$to+=$t;
//			$tp+=$pd;
//			$t = $t - $pd;
//			$r.=($t ? number_format($t, 2, ".", "") : "0.00") . ",";
//			$t_120+=$t;
//			// 90 days
//			$t = 0;
//			if (isset($_GET["tp"]) && $_GET["tp"] == 2)
//				$pd = 0;else
//				$pd = paid($g["establishment_code"], $d90, $d60);
//			$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $d90 . " AND j_inv_date<" . $d60 . ($fixed ? " AND j_inv_created<" . $curr_s : "");
//			if (isset($_GET["tp"])) {
//				if ($_GET["tp"] == 2)
//					$qq.=" AND j_inv_type=1";
//				else
//					$qq.=" AND j_inv_type>1 AND j_inv_type<6";
//			}
//			else
//				$qq.=" AND j_inv_type<6";
//			$qq = mysql_query($qq);
//			if (mysql_num_rows($qq)) {
//				while ($v = mysql_fetch_array($qq))
//					$t+=$v["j_inv_total"];
//			}
//			$to+=$t;
//			$tp+=$pd;
//			$t = $t - $pd;
//			$r.=($t ? number_format($t, 2, ".", "") : "0.00") . ",";
//			$t_90+=$t;
//			// 60 days
//			$t = 0;
//			if (isset($_GET["tp"]) && $_GET["tp"] == 2)
//				$pd = 0;else
//				$pd = paid($g["establishment_code"], $d60, $d30);
//			$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $d60 . " AND j_inv_date<" . $d30 . ($fixed ? " AND j_inv_created<" . $curr_s : "");
//			if (isset($_GET["tp"])) {
//				if ($_GET["tp"] == 2)
//					$qq.=" AND j_inv_type=1";
//				else
//					$qq.=" AND j_inv_type>1 AND j_inv_type<6";
//			}
//			else
//				$qq.=" AND j_inv_type<6";
//			$qq = mysql_query($qq);
//			if (mysql_num_rows($qq)) {
//				while ($v = mysql_fetch_array($qq))
//					$t+=$v["j_inv_total"];
//			}
//			$to+=$t;
//			$tp+=$pd;
//			$t = $t - $pd;
//			$r.=($t ? number_format($t, 2, ".", "") : "0.00") . ",";
//			$t_60+=$t;
//			// 30 days
//			$t = 0;
//			if (isset($_GET["tp"]) && $_GET["tp"] == 2)
//				$pd = 0;else
//				$pd = paid($g["establishment_code"], $d30, $curr_e);
//			$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $d30 . " AND j_inv_date<" . $curr_e . ($fixed ? " AND j_inv_created<" . $curr_s : "");
//			if (isset($_GET["tp"])) {
//				if ($_GET["tp"] == 2)
//					$qq.=" AND j_inv_type=1";
//				else
//					$qq.=" AND j_inv_type>1 AND j_inv_type<6";
//			}
//			else
//				$qq.=" AND j_inv_type<6";
//			$qq = mysql_query($qq);
//			if (mysql_num_rows($qq)) {
//				while ($v = mysql_fetch_array($qq))
//					$t+=$v["j_inv_total"];
//			}
//			$to+=$t;
//			$tp+=$pd;
//			$t = $t - $pd;
//			$r.=($t ? number_format($t, 2, ".", "") : "0.00") . ",";
//			$t_30+=$t;
//			// current
//			$t = 0;
//			if (isset($_GET["tp"]) && $_GET["tp"] == 2)
//				$pd = 0;else
//				$pd = paid($g["establishment_code"], $curr_e, $curr_s);
//			$qq = "SELECT j_inv_total FROM nse_invoice WHERE j_inv_to_company='" . $g["establishment_code"] . "' AND j_inv_date>=" . $curr_e . " AND j_inv_date<" . $curr_s . ($fixed ? " AND j_inv_created<" . $curr_s : "");
//			if (isset($_GET["tp"])) {
//				if ($_GET["tp"] == 2)
//					$qq.=" AND j_inv_type=1";
//				else
//					$qq.=" AND j_inv_type>1 AND j_inv_type<6";
//			}
//			else
//				$qq.=" AND j_inv_type<6";
//			$qq = mysql_query($qq);
//			if (mysql_num_rows($qq)) {
//				while ($v = mysql_fetch_array($qq))
//					$t+=$v["j_inv_total"];
//			}
//			$t = $t - $pd;
//			$to+=$t;
//			$tp+=$pd;
//			$r.=($t ? number_format($t, 2, ".", "") : "0.00") . ",";
//			$r.=($to ? number_format($to - $tp, 2, ".", "") : "0.00") . ",";
//			$r.=($tp ? number_format(0 - $tp, 2, ".", "") : "0.00") . ",";
//			$t_cur+=$t;
//			$t_owe+=$to - $tp;
//			$t_pay+=$tp;
//			if (isset($s["pay"])) {
//				if ($last[0]) {
//					$gp = mysql_fetch_array(mysql_query("SELECT j_inv_paid FROM nse_invoice WHERE j_inv_id=" . $last[0] . " LIMIT 1"));
//					$r.=($gp["j_inv_paid"] ? number_format($gp["j_inv_paid"], 2, ".", "") : "");
//				}
//				$r.=",";
//			}
//			if (isset($s["paydate"]))
//				$r.=$last[1] . ",";
//			if (isset($s["qai"]))
//				$r.=entered($g["establishment_code"]) . ",";
//			if (isset($s["qao"]))
//				$r.=qa_out($g["establishment_code"]) . ",";
//			if (isset($s["vis"])) {
//				$v = visit($g["establishment_code"]);
//				$r.=$v[0] . ",";
//			}
//			if (isset($s["ass"])) {
//				$u = ($g["assessor_id"] ? $g["assessor_id"] : "");
//				$r.=($u ? J_Value("", "people", "", $u) : "") . ",";
//			}
//			if (isset($s["rep"])) {
//				$u = ($g["rep_id1"] ? $g["rep_id1"] : "");
//				$r.=($u ? J_Value("", "people", "", $u) : "") . ",";
//			}
//			if (isset($s["con"]) || isset($s["condet"]) || isset($s["ema"])) {
//				$ema = "";
//				$eid = $g["establishment_code"];
//				$no_js = 1;
//				include $SDR . "/custom/lib/get_est_people.php";
//				$tpd = ($accounts ? $accounts : ($manager ? $manager : ($owner ? $owner : $anybody)));
//				if (isset($s["con"]))
//					$r.=J_Value("", "people", "", $tpd) . ",";
//				if (isset($s["condet"]))
//					$r.=numbers($g["establishment_code"], $tpd) . ",";
//				if (isset($s["ema"]))
//					$r.=$ema . ",";
//				if (isset($s["tvl"]))
//					$r.=($g["tv_licence"] ? $g["tv_licence"] : "") . "~";
//			}
//			$r.="\n";
//		}
//	}

	if (isset($_GET["csv"])) {
		$row_count = 0;
		$r_csv = str_replace('~', ',', $r);
		$r_csv = str_replace('|', "\n", $r_csv);
		
		$hed=",CODE";
		$hed.=",ESTABLISHMENT";
		$hed.=",120+ DAYS";
		$hed.=",90 DAYS";
		$hed.=",60 DAYS";
		$hed.=",30 DAYS";
		$hed.=",CURRENT";
		$hed.=",OWED";
		if (isset($s["paid"]))
			$hed.=",PAID";
		if (isset($s["pay"]))
			$hed.=",LAST PAID";
		if (isset($s["paydate"]))
			$hed.=",PAID DATE";
		if (isset($s["qai"]))
			$hed.=",QA IN";
		if (isset($s["qao"]))
			$hed.=",QA OUT";
		if (isset($s["vis"]))
			$hed.=",VISITED";
		if (isset($s["ass"]))
			$hed.=",ASSESSOR";
		if (isset($s["rep"]))
			$hed.=",REP";
		if (isset($s["con"]))
			$hed.=",CONTACT";
		if (isset($s["condet"]))
			$hed.=",TEL";
		if (isset($s["ema"]))
			$hed.=",EMAIL";
		if (isset($s["tvl"]))
			$hed.=",TV LICENSE";
		$csv = $hed . "\n" . $r_csv . "\n,,," . $t_120 . "," . $t_90 . "," . $t_60 . "," . $t_30 . "," . $t_cur . "," . $t_owe . "," . $t_pay;
		
		//  . "\n,," . $t_120 . "," . $t_90 . "," . $t_60 . "," . $t_30 . "," . $t_cur . "," . $t_owe . "," . $t_pay
	} else {
		$r = str_replace("\n", "", $r);
		$r = str_replace("\"", "", $r);
		$r = str_replace("~|", "|", $r);
		foreach ($pp as $k => $v) {
			$p.=$k . "~" . $v . "|";
		}
	}
}

if (isset($_GET["csv"])) {
	$csv_content = $csv;
	$csv_name = $_GET["csv_name"] ? $_GET["csv_name"] : "AGE_" . date("d-m-Y", $EPOCH) . ".csv";
	include $SDR . "/utility/CSV/create.php";
} elseif (!isset($send_bulk_statements)) {
	echo "<html>";
	echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
	echo "<script src=" . $ROOT . "/system/P.js></script>";
	echo "<script src=" . $ROOT . "/apps/accounts/invoices/age_res.js></script>";
	echo "<script>J_inv_r(\"" . $r . "\",\"" . date("Y/m/d", $date) . "\",\"" . date("Y/m/d", $curr_s) . "\",\"" . $d . "\",\"" . $p . "\"," . $jL0 . "," . $jL1 . "," . $jL2 . "," . (isset($_GET["ityp"]) ? $_GET["ityp"] : 0) . "," . (isset($_GET["rd"]) ? 1 : 0) . "," . $fixed . ($_SESSION["j_user"]["role"] == "s" || $_SESSION["j_user"]["role"] == "b" || $_SESSION["j_user"]["role"] == "d" ? ",1" : "") . ")</script>";
}
?>