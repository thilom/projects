function J_inv_view(e,ee,d,v,c_dt,c_pr,vfco,vtco,vfpr,vfas,vfrp,vfrp2,vtpr,vfct,vtct,vh,vc,vb,vf,uac)
{
	var i,u,a,z,y
	if(d)
	{
		jid=d
		a=j_P.j_G["accounts"]
		y=a["default"]
		z=a["display"]
		jco="<a href=go: onclick='return false' onfocus=blur()><img style=height:32;width:32;margin:2 src="+ROOT+"/ico/set/gohome-64.png onclick=J_W('"+ROOT+"/apps/establishment_manager/edit.php?id=`') onmouseover=\"J_TT(this,'Establishment Manager')\"></a>"
		jin="<a href=go: onclick='return false' onfocus=blur()><img style=height:32;width:32;margin:2 src="+ROOT+"/ico/set/inventory.png onclick=J_W('"+ROOT+"/apps/inventory/view.php?id=`') onmouseover=J_TT(this,'View Item')></a>"
	}
	else z={"company":[],"user":[],"head":[[0,0,"<h1>Invoice</h1>"]],"condition":[],"bank":[],"foot":[]}

	if(v)v=v.split("~")

	type=v[27]*1
	ordid=v[28]?v[28]:0
	invid=v[29]?v[29]:0
	oid=v[28]*1==3?1:0

	if(vfco){vfco=vfco.split("~");z["company"][vfco[0]]=vfco}
	if(vtco){vtco=vtco.split("~");z["company"][vtco[0]]=vtco}
	if(vfpr){vfpr=vfpr.split("~");z["user"][vfpr[0]]=vfpr}
	if(vfas){vfas=vfas.split("~");z["user"][vfas[0]]=vfas}
	if(vfrp){vfrp=vfrp.split("~");z["user"][vfrp[0]]=vfrp}
	if(vfrp2){vfrp2=vfrp2.split("~");z["user"][vfrp2[0]]=vfrp2}
	if(vfct){vfct=vfct.split("~");z["user"][vfct[0]]=vfct}
	if(vtpr){vtpr=vtpr.split("~");z["user"][vtpr[0]]=vtpr}
	if(vtct){vtct=vtct.split("~");z["user"][vtct[0]]=vtct}
	if(vh){vh=vh.split("~");z["head"][vh[0]]=vh}
	if(vc){vc=vc.split("~");z["condition"][vc[0]]=vc}
	if(vb){vb=vb.split("~");z["bank"][vb[0]]=vb}
	if(vf){vf=vf.split("~");z["foot"][vf[0]]=vf}

	a=""

	if(c_dt)
		a="<var><b>Cancelled</b> "+c_dt+" by "+c_pr+"<br></var><hr>"

	a+="<div class=jinv"+(d?"":" style=width:940")+">"
	a+=v[0]&&z["head"][v[0]]?z["head"][v[0]][2]:z["head"][0][2]
	a+="<table class=jinv_a>"
	a+="<tr><th class=jinB><b>Date</b> "+(v[1]?v[1]:"")+"</th>"
	a+="<td><b>Order No.</b> "+(v[2]?v[2]:"")+"</td></tr>"
	a+="<tr><th class=jinB><b>"+(type==3?"Inv/":"")+"Reference</b> "+(v[3]?v[3]:"")+"</th>"
	a+="<td><b>"+(type==3?"C/Note":"Invoice")+" No.</b> "+(v[4]?v[4]:"")+"</td></tr>"

	a+="<tr><th onclick=A_v(this,1)><u>From</u><input type=hidden value="+v[8]+">"+J_inv_cod(1,z["company"][v[6]])+"</th>"

	a+="<td"+(d&&z["company"][v[7]]?" onclick=A_v(this,1) style=cursor:pointer":"")+"><u>Company</u>"+(z["company"][v[7]]?"<input type=hidden value="+v[7]+">"+J_inv_cod(1,z["company"][v[7]]):"")+"</td></tr>"

	if(v[8]||v[9]||v[10]||v[11]||v[12])
	{
		a+="<tr><th onclick=A_v(this)><u>From</u>"+(v[8]?"<input type=hidden value="+v[8]+">"+J_inv_cod(2,z["user"][v[8]],v[8]?2:0):"")+"</th>"
		a+="<td onclick=A_v(this)"+(v[10]||v[11]||v[12]?" rowspan="+(v[10]&&v[11]&&v[12]?4:v[10]&&v[11]?3:2):"")+"><u>Attention</u>"+(v[12]?"<input type=hidden value="+v[12]+">"+J_inv_cod(2,z["user"][v[12]],v[12]?2:0):"")+"</td></tr>"
		a+="<tr><th onclick=A_v(this)><u>Assessor</u>"+(v[9]?"<input type=hidden value="+v[9]+">"+J_inv_cod(2,z["user"][v[9]],v[9]?2:0):"")+"</th><tr>"
		a+="<tr><th onclick=A_v(this)><u>Rep</u>"+(v[10]?"<input type=hidden value="+v[10]+">"+J_inv_cod(2,z["user"][v[10]],v[10]?2:0):"")+"</th><tr>"
		a+="<tr><th onclick=A_v(this)><u>Rep</u>"+(v[11]?"<input type=hidden value="+v[11]+">"+J_inv_cod(2,z["user"][v[11]],v[11]?2:0):"")+"</th><tr>"
	}
	if(v[13]||v[14])
	{
		a+="<tr><th onclick=A_v(this)><u>Accounts/Admin</u>"+(v[13]?"<input type=hidden value="+v[13]+">"+J_inv_cod(2,z["user"][v[13]],3):"")+"</th>"
		a+="<td onclick=A_v(this)><u>Accounts/Admin</u>"+(v[14]?"<input type=hidden value="+v[14]+">"+J_inv_cod(2,z["user"][v[14]],3):"")+"</td></tr>"
	}
	if(v[16])
		a+="<tr><td colspan=2 class=jinvinf>"+v[16]+"</td></tr>"
	a+="</table>"
	a+="<table class=jinv_tb id=jTR>"
	a+="<tr id=jinv_h>"
	a+="<th>Date</th>"
	a+="<th>Item</th>"
	a+="<th>Code</th>"
	a+="<th>Qty</th>"
	a+="<th>Unit</th>"
	a+="<th>Price</th>"
	if(type!=3&&type!=9)
		a+="<th>Disc</th>"
	a+="<th>Vat</th>"
	a+="<th>Total</th>"
	a+="<th id=plb>Paid</th>"
	a+="</tr>"
	a+="<tbody id=jinvv>"
	var tq=0,tp=0
	y=d?" onclick=J_invP("+jid+") onmouseover=\"J_TT(this,'View payment details')\" style=cursor:pointer":""
	if(v[32])
	{
		v[32]=v[32].split("|")
		i=0
		while(v[32][i])
		{
			u=v[32][i].split("`")
			u[6]=parseFloat(u[6]?u[6]:0)
			u[7]=parseFloat(u[7]?u[7]*1:0).toFixed(2)
			u[8]=parseFloat(u[8]?u[8]*1:0).toFixed(2)
			u[9]=parseFloat(u[9]?u[9]*1:0).toFixed(2)
			a+="<tr>"
			a+="<td class=jdbC>"+(u[2]?u[2]:"")+"</td>"
			a+="<td class=jdbW><b>"+(u[3]?u[3]:"Undefined")+"</b><div>"+(u[4]?u[4]:"")+"</div></td>"
			a+="<th class=jinB>"+u[5]+"</th>"
			a+="<th class=jdbB>"+u[6]+"</th>"
			tq+=u[6]*1
			a+="<th class=jdbK>"+u[7]+"</th>"
			tp+=u[7]*1
			a+="<th class=jdbY>"+(u[6]*u[7]).toFixed(2)+"</th>"
			if(type!=3&&type!=9)
				a+="<th class=jdbO>"+u[8]+"</th>"
			a+="<th class=jdbR>"+u[9]+"</th>"
			u[10]=u[10]*1
			a+="<th class=jdbG>"+u[10].toFixed(2)+"</th>"
			u[11]=u[11]*1
			a+="<th class=jdbR"+y+(u[11]<u[10]?(y?";":" style=")+"color:red":"")+" id=plb>"+(0-u[11]).toFixed(2)+"</th>"
			a+="</tr>"
			i++
		}
	}
	v[26]=v[26]?parseFloat(v[26])*1:0
	v[27]=v[27]?parseFloat(v[27])*1:0
	a+="</tbody>"
	a+="<tr id=jintt>"
	a+="<td colspan=3></td>"
	a+="<th class=jdbB>"+tq+"</th>"
	a+="<th class=jdbK>"+parseFloat(tp).toFixed(2)+"</th>"
	a+="<th class=jdbY>"+(v[23]?parseFloat(v[23]).toFixed(2):"0.00")+"</th>"
	if(type!=3&&type!=9)
		a+="<th class=jdbO>"+(v[24]?parseFloat(v[24]).toFixed(2):"0.00")+"</th>"
	a+="<th class=jdbR>"+(v[25]?parseFloat(v[25]).toFixed(2):"0.00")+"</th>"
	a+="<th class=jdbG>"+v[26].toFixed(2)+"</th>"
	a+="<th class=jdbR"+y+(v[27]<v[26]?(y?";":" style=")+"color:red":"")+" id=plb>"+(0-v[27]).toFixed(2)+"</th>"
	a+="</tr>"
	a+="</table>"
	a+="<i class=jinvcur><b id=j_inv_currency>*Currency: "+(v[22]?v[22]:y["currency"])+" ~ VAT: "+(v[15]?v[15]:y["vat"])+"%</b></i>"
	if(v[17]||v[18]||v[19])
	{
		a+="<span class=jinvF>"
		if(v[17])
			a+="<span>"+v[17]+"<hr></span>"
		if(v[19]&&z["bank"][v[19]])
			a+="<span>"+z["bank"][v[19]][2]+"<hr></span>"
		if(v[18]&&z["condition"][v[18]])
			a+="<span class=j_condition>"+z["condition"][v[18]][2]+"<hr></span>"
		a+="</span>"
	}
	if(v[20]&&z["foot"][v[20]])
		a+="<span class=j_foot>"+z["foot"][v[20]][2]+"</span>"

	if(!d)
		a+="<hr><br><a href="+ROOT+"/apps/accounts/invoices/pdf_download.php?id="+inv_id+" onfocus=blur()><tt><center><img src="+ROOT+"/ico/set/pdf.png height=48><br><b>Download PDF</b></center></tt></a>"

	if(c_dt)
		a+="<img src="+ROOT+"/apps/accounts/invoices/cancel.gif class=j_cancel>"
	a+="</body></html>"
	document.write(a)
	if(d)
	{
		J_tr()
		z="j_Wi["+j_W+"]['F'][0]['W']"
		a="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_search.php');return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/search.png onmouseover=J_TT(this,'Search')></a>"
		a+="<a href="+ROOT+"/apps/accounts/invoices/tab_view.php?id="+d+" target=_blank onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/print.png onmouseover=J_TT(this,'Print')></a>"
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_send.php?id="+d+"&invn="+v[4]+"');return false\" onmouseover=J_TT(this,'Send') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/email.png></a>"
		if(uac=="d"||uac=="s")
			a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/notes.php?id="+d+"');return false\" onmouseover=J_TT(this,'Notes') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/note.png></a>"
		if(v[27]>0) // payments
			a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/payments/view.php?id="+d+"');return false\" onmouseover=J_TT(this,'Payments') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_payEdit.png></a>"
		if(e||ee)
		{
			if(!ee&&v[20])
				a+="<a href=go: onclick=\""+z+".J_opaq();"+z+".location='"+ROOT+"/apps/accounts/invoices/edit.php?id="+d+"';return false\" onmouseover=\"J_TT(this,'<b>Closed ("+v[20]+")</b>This invoice may not be edited unless the closed status is removed by the Curator or a Super-User')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/no_entry.png></a>"
			else
				a+="<a href=go: onclick=\""+z+".J_opaq();"+z+".location='"+ROOT+"/apps/accounts/invoices/edit.php?id="+d+"';return false\" onmouseover=J_TT(this,'Edit') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/edit.png></a>"
		}
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/documents.php?id="+d+"&v=1');return false\" onmouseover=\"J_TT(this,'View documents')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/file_drawer.png></a>"
		a+="<a href="+ROOT+"/apps/accounts/invoices/pdf_download.php?id="+d+" onmouseover=\"J_TT(this,'Download PDF')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/pdf.png></a>"
		J_footer(a)
	}
}

function J_invP(d)
{
	J_W(ROOT+"/apps/accounts/payments/view.php?id="+d)
}

function J_inv_cod(d,r,c)
{
	var v,s,a=""
	if(d==1)
		v=[0,"<dfn> </dfn><b>^</b>","<dfn> </dfn><tt><i>City</i> ^</tt>","<dfn> </dfn><tt><i>Email</i> <a href='mailto:^' onfocus=blur()>^</a></tt>","<dfn> </dfn><tt><i>Tel</i> ^</tt>","<dfn> </dfn><tt><i>Fax</i> ^</tt>","<dfn> </dfn><tt><i>Postal</i> ^</tt>","<dfn> </dfn><tt><i>Address</i> ^</tt>","<dfn> </dfn><tt><i>Code</i> ^</tt>","<dfn> </dfn><tt><i>Reg No</i> ^</tt>","<dfn> </dfn><tt><i>Vat No</i> ^</tt>"]
	else
		v=[0,"<dfn> </dfn><b>^</b>","<dfn> </dfn><tt><i>City</i> ^</tt>","<dfn> </dfn><tt><i>Email</i> <a href='mailto:^' onfocus=blur()>^</a></tt>","<dfn> </dfn><tt><i>Tel</i> ^</tt>","<dfn> </dfn><tt><i>Fax</i> ^</tt>","<dfn> </dfn><tt><i>Postal</i> ^</tt>","<dfn> </dfn><tt><i>Address</i> ^</tt>","<dfn> </dfn><tt><i>Code</i> ^</tt>","<dfn> </dfn><tt><i>Vat No</i> ^</tt>"]
	i=1,s=c==2?4:c==3?5:0
	while(v[i])
	{
		if(i==s)
			break
		else if(r&&r[i])
		{
			if(i==1)
				r[i]=r[i].toUpperCase()
			a+=v[i].replace(/\^/g,r[i].replace(/<br>/g,", ").replace(/,/g,", ").replace(/,,/g,", ").replace(/,  /g,", "))
		}
		i++
	}
	return a
}

function A_v(t,v)
{
	t=$T(t,"input")[0]
	if(t)
	{
		t=t.value
		J_note("x","<a href=go: onclick=\"J_W(ROOT+'/apps/"+(v?"establishment_manager/edit.php?id="+t+"&view=1":"people/view.php?id="+t)+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'View "+(v?"establishment":"persons")+" details')\"><img src="+ROOT+"/ico/set/"+(v?"gohome-64":"user_both")+".png height=32 width=32></a>",0,3,4,3,0,4)
	}
}
