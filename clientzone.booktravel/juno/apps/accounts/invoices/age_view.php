<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/get.php";
include_once $SDR."/system/parse.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

$id=$_GET["id"];
$date=isset($_GET["ds"])?J_dateParse($_GET["ds"]):$EPOCH;
$y=date("Y",$date);
$m=date("m",$date);
$curr_s=mktime(0,0,0,date($m)+1,date(1),date($y));
$curr_e=mktime(0,0,0,date($m),date(1),date($y));
$d30=mktime(0,0,0,date($m)-1,date(1),date($y));
$d60=mktime(0,0,0,date($m)-2,date(1),date($y));
$d90=mktime(0,0,0,date($m)-3,date(1),date($y));
$firstdate=0;
$o="";
$contact=0;
$to_per=0;
$d="";

$q="SELECT * FROM nse_invoice WHERE j_inv_to_company='".$id."' AND j_inv_type<6 ORDER BY j_inv_date DESC";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$bcurr=0;
	$b30=0;
	$b60=0;
	$b90=0;
	$b120=0;

	while($g=mysql_fetch_array($q))
	{
		if(!$bcurr && $g["j_inv_date"]>$curr_e)
		{
			$bcurr=1;
			$o.="b~Current (".date("M",$curr_e).")|";
		}
		elseif(!$b30 && $g["j_inv_date"]<$curr_e && $g["j_inv_date"]>$d30)
		{
			$b30=1;
			$o.="b~30 Days (".date("M",$d30).")|";
		}
		elseif(!$b60 && $g["j_inv_date"]<$d30 && $g["j_inv_date"]>$d60)
		{
			$b60=1;
			$o.="b~60 Days (".date("M",$d60).")|";
		}
		elseif(!$b90 && $g["j_inv_date"]<$d60 && $g["j_inv_date"]>$d90)
		{
			$b90=1;
			$o.="b~90 Days (".date("M",$d90).")|";
		}
		elseif(!$b120 && $g["j_inv_date"]<$d90)
		{
			$b120=1;
			$o.="b~120+ Days (".date("M",$g["j_inv_date"]).")|";
		}

		if(!$contact && $g["j_inv_to_contact"]>0)
			$contact=$g["j_inv_to_contact"];
		elseif(!$to_per && $g["j_inv_to_person"]>0)
			$to_per=$g["j_inv_to_person"];

		$d=date("Y/m/d",$g["j_inv_date"]);
		$firstdate=$d;
		$o.=$g["j_inv_id"]."~";
		$o.=$g["j_inv_type"]."~";
		$o.=$d."~";
		$o.=$g["j_inv_number"]."~";
		$o.=$g["j_inv_total"]."~";
		$o.=$g["j_inv_paid"]."~";
		$pd="";

		$q1="SELECT * FROM nse_invoice_item WHERE j_invit_invoice=".$g["j_inv_id"]." ORDER BY j_invit_date,j_invit_name LIMIT 100";
		$q1=mysql_query($q1);
		if(mysql_num_rows($q1))
		{
			while($g1=mysql_fetch_array($q1))
			{
				$o.=$g1["j_invit_name"]."`";
				$o.=$g1["j_invit_total"]."`";
				$tp=0;
				$pd="";
				$pc="";
				$r2=mysql_query("SELECT j_invp_paid,j_invp_date,j_invp_created FROM nse_invoice_item_paid WHERE j_invp_invoice=".$g["j_inv_id"]." AND j_invp_date!=0 ORDER BY j_invp_date");
				if(mysql_num_rows($r2))
				{
					while($g2=mysql_fetch_array($r2))
					{
						$tp+=$g2["j_invp_paid"];
						$pd=date("Y/m/d",$g2["j_invp_date"]);
						$pc=date("Y/m/d",$g2["j_invp_created"]);
					}
					$o.=$tp."`";
					$o.=$pd."`";
					$o.=$pc;
					$o.="^";
				}
			}
		}
		$o.="~";
		$o.=$pd;
		$o.="|";
	}
}

$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));

include $SDR."/custom/lib/est_items.php";

$n=J_Value("","establishment","",$id);
$n=($n?$n:"Untitled Establishment");

echo "<script src=".$ROOT."/apps/accounts/invoices/age_view.js></script>";
echo "<script>A_v(";
echo "\"".$id."\"";
echo ",\"".$n."\"";
echo ",\"".date("Y/m/d",$curr_s)."\"";
echo ",\"".$d."\"";
echo ",\"".$o."\"";
echo ",".($contact?$contact:$to_per);
echo ",\"".($contact||$to_per?J_Value("","people","",$contact?$contact:$to_per):"")."\"";
echo ",\"".($contact||$to_per?numbers($id,($contact?$contact:$to_per)):"")."\"";
echo ",".($g["assessor_id"]?$g["assessor_id"]:0);
echo ",\"".($g["assessor_id"]?J_Value("","people","",$g["assessor_id"]):"")."\"";
echo ",".($g["rep_id1"]?$g["rep_id1"]:0);
echo ",\"".($g["rep_id1"]?J_Value("","people","",$g["rep_id1"]):"")."\"";
$r=$_SESSION["j_user"]["role"];
if(J_getSecure($URL) || J_getSecure())
{
	echo ",".($r=="b"||$r=="d"||$r=="s"?1:0);
	echo ",1";
}
echo ")</script>";

$J_title1="Invoice History";
$J_title2=$n;
$J_title3="From first invoice ".$d." to ".date("Y/m/d",$curr_s)." (Drawn ".date("Y/m/d",$date).")";
$J_icon="<img src=".$ROOT."/ico/set/accounts_age.png>";
$J_label=19;
$J_width=820;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>