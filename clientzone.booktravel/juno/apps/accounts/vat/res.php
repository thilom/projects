<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/parse.php";

// payments
if(count($_POST))
{
	include_once $SDR."/apps/notes/f/insert.php";
	include_once $SDR."/system/activity.php";
	function stripNum($v=0)
	{
		$v=preg_replace("~[^0-9\.]~","",$v)*1;
		return $v>0?$v:0;
	}

	foreach($_POST as $k => $v)
	{
		$d=substr($k,0,2);
		if($d=="i_")
		{
			$d=substr($k,2);
			$v=stripNum($v);
			$_POST["p_".$d]=stripNum($_POST["p_".$d]);
			if($v!=$_POST["p_".$d])
			{
				mysql_query("UPDATE nse_invoice SET j_inv_vat_paid='".$v."',j_inv_vat_paid_user=".($v>0?$_SESSION["j_user"]["id"]:0).",j_inv_vat_paid_date=".($v>0?$EPOCH:0)." WHERE j_inv_id=".$d);
				$g=mysql_fetch_array(mysql_query("SELECT j_inv_number,j_inv_to_company FROM nse_invoice WHERE j_inv_id=".$d." LIMIT 1"));
				insertNote($g["j_inv_to_company"],2,"INV".$d,"Paid VAT Invoice: ".$g["j_inv_number"]." | VAT: ".$v,0,1);
				J_act("Invoice",3,$g["j_inv_number"],$d,$g["j_inv_to_company"]);
			}
		}
	}
}


// constrain_user_establishments functions
include $SDR."/custom/constrain_user_establishments.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$d="";

$s="SELECT DISTINCT j_inv_id";
$s.=",j_inv_date";
$s.=",j_inv_number";
$s.=",j_inv_to_company";
$s.=",j_inv_total";
$s.=",j_inv_total_vat";
$s.=",j_inv_vat_paid";
$s.=",j_inv_vat_paid_date";
$s.=",j_inv_vat_paid_user ";
$s.="FROM nse_invoice ";
$s.="WHERE j_inv_temp=0 ";
$s.="AND j_inv_type<6 ";
$s.="AND j_inv_total_vat>0 ";
$s.="AND j_inv_paid>0 ";
$s.="AND j_inv_total<=j_inv_paid ";
if(isset($_GET["ds"]))
{
	$z=J_dateParse($_GET["ds"]);
	if($z)
		$s.="AND j_inv_date>=".$z." ";
}
if(isset($_GET["de"]))
{
	$z=J_dateParse($_GET["de"],0,0,1);
	if($z)
		$s.="AND j_inv_date<".$z." ";
}
if(isset($_GET["tc"]))
{
	// check if user has establishment access - returns 0 if not accessible
	$eid=check_user_establishment($_GET["tc"]);
	$s.="AND j_inv_to_company='".$eid."' ";
}
else // constrain user establishments
	$s.=user_establishments_constrain(1,"j_inv_to_company");
$jL0=mysql_num_rows(mysql_query($s));
$s.=" ORDER BY j_inv_date DESC";
$s.=" LIMIT ".$jL1.",".$jL2;
//echo $s."<hr>";
$rs=mysql_query($s);

$r="";

if(isset($_GET["csv"]))
{
	$hed="";
	$tot="";
	$i=0;
	$c=0;
	$t_vt=0;
	$t_tt=0;
	$t_tp=0;
}

if(mysql_num_rows($rs))
{
	$dd=explode("~",$_GET["d"]."date~num~to_co~total~vat~paid");
	$ddd=array();
	$s=array();
	foreach($dd as $k => $v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
	$t=array("X","date","num","to_co","total","vat","paid","paydate","payby");
	$d="";
	$n=1;
	foreach($t as $k => $v)
	{
		if(isset($ddd[$v]))
		{
			$d.=$n;
			$n++;
		}
		$d.="~";
	}

	include_once $SDR."/system/get.php";

	if(!isset($_GET["csv"]))
	{
		$eco="";
		$eus="";
		$co=array();
		$us=array();
	}

	while($g=mysql_fetch_array($rs))
	{
		if(isset($_GET["csv"]))
		{
			$r.=",".($g["j_inv_date"]?date("Y/m/d",$g["j_inv_date"]):"");
			$r.=",".($g["j_inv_number"]?" ".str_replace(","," ",$g["j_inv_number"]):"");
			$r.=",".($g["j_inv_to_company"]?str_replace(","," ",J_Value("name","establishment","id",$g["j_inv_to_company"])):"AA Travel Guides");
			$r.=",".($g["j_inv_total"]?number_format($g["j_inv_total"],2,"."," "):"");
			$r.=",".($g["j_inv_total_vat"]?number_format($g["j_inv_total_vat"],2,"."," "):"");
			$r.=",".($g["j_inv_paid"]?number_format($g["j_inv_paid"],2,"."," "):"");
			if(isset($s["paydate"]))
				$r.=",".($g["j_inv_vat_paid_date"]?date("Y/m/d",$g["j_inv_vat_paid_date"]):"");
			if(isset($s["payby"]))
				$r.=",".($g["j_inv_vat_paid_user"]?J_Value("","people","",$g["j_inv_vat_paid_user"]):"");
			$r.="\n";
			$t_vt+=$g["j_inv_total_vat"];
			$t_tt+=$g["j_inv_total"];
			$t_tp+=$g["j_inv_vat_paid"];
			$i++;
		}
		else
		{
			$r.=$g["j_inv_id"]."~";
			$r.=($g["j_inv_date"]?date("Y/m/d",$g["j_inv_date"]):"")."~";
			$r.=$g["j_inv_number"]."~";
			$r.=($g["j_inv_to_company"]?$g["j_inv_to_company"]:"")."~";
			$r.=$g["j_inv_total"]."~";
			$r.=$g["j_inv_total_vat"]."~";
			$r.=$g["j_inv_vat_paid"]."~";
			if(isset($s["paydate"]))
				$r.=($g["j_inv_vat_paid_date"]?date("Y/m/d",$g["j_inv_vat_paid_date"]):"")."~";
			if(isset($s["payby"]))
				$r.=($g["j_inv_vat_paid_user"]?$g["j_inv_vat_paid_user"]:"")."~";
			$r.="|";
			if($g["j_inv_to_company"] && !isset($co[$g["j_inv_to_company"]]))
				$co[$g["j_inv_to_company"]]=J_Value("","establishment","",$g["j_inv_to_company"]);
			if($g["j_inv_vat_paid_user"] && !isset($us[$g["j_inv_vat_paid_user"]]))
				$us[$g["j_inv_vat_paid_user"]]=J_Value("","people","",$g["j_inv_vat_paid_user"]);
		}
	}

	if(isset($_GET["csv"]))
	{
		$hed.=",DATE";
		$hed.=",NUMBER";
		$hed.=",ESTABLISHMENT";
		$hed.=",TOTAL";
		$hed.=",VAT";
		$hed.=",PAID";
		$hed.=",DATE";
		$hed.=",PAID BY";
		$tot.=",,,";
		$tot.=",".number_format($t_vt,2,"."," ");
		$tot.=",".number_format($t_tt,2,"."," ");
		$tot.=",".number_format($t_tp,2,"."," ");
		$tot.="\n";
		$csv=$hed."\n".$r.$tot;
	}
	else
	{
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($co as $k => $v){$eco.=$k."~".$v."|";}
		foreach($us as $k => $v){$eus.=$k."~".$v."|";}
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"ACC_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/accounts/vat/res.js></script>";
	echo "<script>J_inv_r(\"".$d."\",\"".$r."\",\"".(isset($_GET["ds"])?$_GET["ds"]:"")."\",\"".(isset($_GET["de"])?$_GET["de"]:"")."\",\"".$eco."\",\"".$eus."\",".$jL0.",".$jL1.",".$jL2.($_SESSION["j_user"]["role"]=="b"||$_SESSION["j_user"]["role"]=="d"?",1":"").")</script>";
}
?>