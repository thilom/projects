<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

include $SDR."/system/get.php";

$role=$_SESSION["j_user"]["role"];
$id=($role=="d"||$role=="b"||$role=="s")&&isset($_GET["id"])&&$_GET["id"]?$_GET["id"]:$_SESSION["j_user"]["id"];
$i=0;
$de="";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/commission/f/rep_commission_report.js></script>";
echo "<body style='margin:0'>";

$s="SELECT
DISTINCT j_invit_id
,j_invit_date
,j_invit_inventory
,j_invit_company
,j_invit_name
,j_invit_discount
,j_invit_total
,j_invit_quantity
,j_invit_price
,j_invit_paid
,j_inv_id
,j_inv_date
,j_inv_number
,j_inv_to_company
,j_invp_date
,j_invcom_id
,j_invcom_fixed
,j_invcom_percent
,j_invcom_paid
,j_invcom_paid_date
,j_in_name
";
$s.="FROM nse_invoice_commission ";
$s.="LEFT JOIN nse_invoice_item ON j_invcom_item=j_invit_id ";
$s.="LEFT JOIN nse_invoice ON j_invit_invoice=j_inv_id ";
$s.="LEFT JOIN nse_invoice_item_paid ON j_invit_id=j_invp_invoice_item ";
$s.="LEFT JOIN nse_inventory ON j_invit_inventory=j_in_id ";
$s.="WHERE ";
$s.="j_inv_cancel_user=0 ";
$s.="AND j_inv_type<3 ";
$s.="AND j_invcom_user=".$id." ";
$s.="AND j_invit_total>=j_invp_paid ";
$s.="ORDER BY j_inv_date DESC ";
$q=mysql_query($s);
if(mysql_num_rows($q))
{

	function rMonth()
	{
		global $i,$y,$month,$m,$mi,$mii,$iiv,$md,$mt,$mtv,$mp,$mro,$mrp;
		if($i)
		{
			echo "</tbody>";
			echo "<tr onmouseover=im(this)><td colspan=3 class=jdbWW>".$month[$m]." Unpaid items: ".$mi;
			echo "</td>";
			echo "<td class=jdbR onmouseover=\"Pv(this,'".number_format($mtv,2,"."," ")."')\">".number_format($mt,2,"."," ")."</td>";
			//echo "<td class=jdbG>".number_format($mp,2,"."," ")."</td>";
			echo "<td class=jdbR>".number_format($mro,2,"."," ")."</td>";
			echo "<td class=jdbG>".number_format($mrp,2,"."," ")."</td>";
			echo "</tr>";
		}
		$mi=0;
		$md=0;
		$mt=0;
		$mtv=0;
		$mp=0;
		$mro=0;
		$mrp=0;
	}

	function comm_share($i)
	{
		$o="";
		$q=mysql_query("SELECT firstname,lastname,j_invcom_percent,j_invcom_fixed FROM nse_invoice_commission JOIN nse_user ON j_invcom_user=user_id WHERE j_invcom_item=".$i." AND (j_invcom_percent>0 OR j_invcom_fixed>0) ORDER BY firstname");
		$n=mysql_num_rows($q);
		if($n)
		{
			$n=$n>1?1:0;
		 while($g=mysql_fetch_array($q))
			{
			 if($n)
				$o.=ucwords(preg_replace("~[^a-zA-Z0-9 ]~","",$g["firstname"]." ".$g["lastname"]))." (".($g["j_invcom_percent"]?$g["j_invcom_percent"]."%":$g["j_invcom_fixed"]).")~";
			elseif($g["j_invcom_percent"])
				$o=$g["j_invcom_percent"]."%";
			else
				$o="!";
			}
		 if($o)
			 $o=" onmouseover=\"rS(this,'".$o."')\"";
		}
		return $o;
	}

	$y=0;
	$m=0;
	$i=0;
	$ti=0;
	$yi=0;
	$mi=0;
	$tt=0;
	$yt=0;
	$mt=0;
	$ttv=0;
	$ytv=0;
	$mtv=0;
	$tp=0;
	$yp=0;
	$mp=0;
	$td=0;
	$yd=0;
	$md=0;
	$tro=0;
	$yro=0;
	$mro=0;
	$trp=0;
	$yrp=0;
	$mrp=0;
	$tii=array();
	$yii=array();
	$mii=array();
	$vii=array();
	$cc=array();
	$month=array("-","January","February","March","April","May","June","July","August","September","October","November","December");
	echo "<form method=post>";
	echo "<table id=jTR onkeyup=rCP() ondblclick=rCP(1)>";

	while($g=mysql_fetch_array($q))
	{
		$y=date("Y",$g["j_inv_date"]?$g["j_inv_date"]:$g["j_invit_date"]);
		$mm=date("m",$g["j_inv_date"]?$g["j_inv_date"]:$g["j_invit_date"])*1;
		if($m!=$mm)
		{
			if(!$de)
				$de=date("d/m/Y",$g["j_inv_date"]);
			rMonth();
			$m=$mm;
			echo "<tr onclick=io(this)><th colspan=9 class=jdbB>".$y." ".$month[$m]."</th></tr><tbody>";
			echo "<script>rH(1)</script>";
			$mc=0;
		}

		echo "<tr onclick=P_(".$g["j_inv_id"].",'".$g["j_inv_to_company"]."')>";
		$ds=$g["j_inv_date"]?date("d/m/Y",$g["j_inv_date"]):($g["j_invit_date"]?date("d/m/Y",$g["j_invit_date"]):"?");
		echo "<td class=jdbC>".$ds."</td>";
		$c=$g["j_invit_company"]?$g["j_invit_company"]:$g["j_inv_to_company"];
		if(!isset($cc[$c]))
			$cc[$c]=J_Value("","establishment","",$c);
		echo "<td>".$cc[$c]."</td>";
		if(!isset($iiv[$g["j_invit_inventory"]]))
			$iiv[$g["j_invit_inventory"]]=$g["j_in_name"]?$g["j_in_name"]:$g["j_invit_name"];
		echo "<th onmouseover=vn(this,'".$g["j_inv_number"]."')>".$iiv[$g["j_invit_inventory"]]."</th>";
		$d=$g["j_invit_discount"];
		$t=$g["j_invit_price"]*$g["j_invit_quantity"];
		$tv=$g["j_invit_total"];
		echo "<td class=jdbR onmouseover=\"Pv(this,'".number_format($g["j_invit_total"],2,"."," ")."')\">".number_format($t,2,"."," ")."</td>";
		$p=($g["j_invp_date"]?$g["j_invit_paid"]:0);
		//echo "<td class=jdbG".($g["j_invp_date"]?" onmouseover=\"Rp(this,'".date("d/m/Y",$g["j_invp_date"])."')\"":"").">".($p?number_format($p,2,"."," "):"")."</td>";
		$ro=$g["j_invcom_percent"]?(($g["j_invit_price"]*$g["j_invit_quantity"])-$g["j_invit_discount"])*($g["j_invcom_percent"]/100):($g["j_invcom_fixed"]?$g["j_invcom_fixed"]:0);
		echo "<td class=jdbR".comm_share($g["j_invit_id"]).">".number_format($ro,2,"."," ")."</td>";
		$rp=$g["j_invcom_paid"];
		echo "<td class=jdbG".($rp?" lang='".number_format($rp,2,"."," ")."'":"").">";
		echo "<input type=text name=p_".$i." value=".($rp?number_format($rp,2,"."," "):"").">";
		echo "<input type=hidden name=i_".$i." value=".$g["j_invit_id"].">";
		echo "</td>";
		echo "<td lang=".($g["j_invcom_paid_date"]?date("d/m/Y",$g["j_invcom_paid_date"]).">".date("d/m/Y",$g["j_invcom_paid_date"]):"''>")."</td>";
		echo "</tr>";

		$ti+=1;
		$yi+=1;
		$mi+=1;
		//$td+=$d;
		//$yd+=$d;
		//$md+=$d;
		$tt+=$t;
		$yt+=$t;
		$mt+=$t;
		$ttv+=$tv;
		$ytv+=$tv;
		$mtv+=$tv;
		$tp+=$p;
		$yp+=$p;
		$mp+=$p;
		$tro+=$ro;
		$yro+=$ro;
		$mro+=$ro;
		$trp+=$rp;
		$yrp+=$rp;
		$mrp+=$rp;
		$ii=$g["j_invit_inventory"];

		if(!isset($tii[$ii]))
			$tii[$ii]=array(0,0,0);
		$tii[$ii][0]+=1;
		$tii[$ii][1]+=$t;
		$tii[$ii][2]+=$rp;
		if(!isset($yii[$ii]))
			$yii[$ii]=array(0,0,0);
		$yii[$ii][0]+=1;
		$yii[$ii][1]+=$t;
		$yii[$ii][2]+=$rp;
		if(!isset($mii[$ii]))
			$mii[$ii]=array(0,0,0);
		$mii[$ii][0]+=1;
		$mii[$ii][1]+=$t;
		$mii[$ii][2]+=$rp;
		if(!isset($iiv[$ii]))
			$iiv[$ii]=$g["j_in_name"];

		$i++;
	}
	rMonth();
	echo "</tbody>";
	echo "<tr><th colspan=9 class=jdbW>Total</th></tr>";
	echo "<tr onclick=ir(this) onmouseover=im(this)><td colspan=3 class=jdbWW>".$ds." - ".$de." items: ".$ti;
	echo "</td>";
	echo "<td class=jdbR onmouseover=\"Pv(this,'".number_format($ttv,2,"."," ")."')\">".number_format($tt,2,"."," ")."</td>";
	//echo "<td class=jdbG>".number_format($tp,2,"."," ")."</td>";
	echo "<td class=jdbR>".number_format($tro,2,"."," ")."</td>";
	echo "<td class=jdbG id=tt>".number_format($trp,2,"."," ")."</td>";
	echo "</tr>";
	echo "</table>";
	echo "<br><input type=submit value=PAY>";
}
else
	echo "<tt><b>No commission found</b></tt>";

echo "<script>Jrr(".$id.",'".$role."',1)</script>";

$J_title1="My Commission Payment";
$J_title1=J_Value("","people","",$id);
$J_title2="Rep Commission Payment";
$J_title3=($i?$ds." - ".$de:"");
$J_icon="<img src=".$ROOT."/ico/set/edit.png><var><img src=".$ROOT."/ico/set/user_male.png></var>";
$J_home=1;
$J_width=1440;
$J_height=640;
$J_label=19;
$J_nostart=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>