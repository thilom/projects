<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
$SUB_ACCESS=array("edit"=>0);
include $SDR."/system/secure.php";
include $SDR."/system/get.php";
include_once $SDR."/apps/accounts/invoices/types.php";

if(isset($_GET["assoc"]))
{
	$o="";
	$q = mysql_query("SELECT user_id,firstname,lastname,uc_percent,uc_fixed FROM nse_user_commission JOIN nse_user ON uc_partner=user_id WHERE uc_user=".$_GET["assoc"]." ORDER BY firstname");
	if (mysql_num_rows($q))
	{
		while ($g = mysql_fetch_array($q))
			$o.=$g["user_id"]."~".ucwords($g["firstname"]." ".$g["lastname"])."~".($g["uc_percent"]?$g["uc_percent"]:"")."~".($g["uc_fixed"]?$g["uc_fixed"]:"")."|";
	}
	echo "<script>window.parent.rP(\"".$o."\")</script>";
	die();
}

$id=isset($_GET["id"])?$_GET["id"]:0;
$invoice=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$id));

if(count($_POST)&&$SUB_ACCESS["edit"])
{
	include_once $SDR."/system/activity.php";
	include_once $SDR."/apps/notes/f/insert.php";
	include_once $SDR."/apps/accounts/commission/f/insert.php";
	foreach($_POST as $k => $v)
	{
		$d=substr($k,0,2);
		if($d=="n_" && $v)
		{
			$d=substr($k,2);
			if(insert_comm($id, $_POST["i_".$d], $v, $p, $a))
			{
				$u=J_Value("","people","",$v);
				insertNote($invoice["j_inv_to_company"],2,"INV".$id,"Added commission for: ".$u,0,1);
				J_act($types[$invoice["j_inv_type"]],5,($invoice["j_inv_type"]==9?$invoice["j_inv_order"]:$invoice["j_inv_number"])." / Added commission for: ".$u,$id);
			}
		}
		elseif($d=="c_")
		{
			$d=substr($k,2);
			$p=$_POST["p_".$d]?preg_replace("~[^0-9\.]~","",$_POST["p_".$d])*1:0;
			$a=!$p&&$_POST["a_".$d]?preg_replace("~[^0-9\.]~","",$_POST["a_".$d])*1:0;
			$u=$_POST["u_".$d]?$_POST["u_".$d]:(isset($_POST["ou_".$d])?$_POST["ou_".$d]:0);
			$u=J_Value("","people","",$u);
			if($v)
			{
				if($_POST["u_".$d]>0)
				{
				 $q="UPDATE nse_invoice_commission SET
				j_invcom_user=".$_POST["u_".$d]."
				,j_invcom_percent=".$p."
				,j_invcom_fixed=".$a."
				,j_invcom_date=".$EPOCH."
				WHERE j_invcom_id=".$v;
				 mysql_query($q);
				insertNote($invoice["j_inv_to_company"],2,"INV".$id,"Updated commission for: ".$u,0,1);
				J_act($types[$invoice["j_inv_type"]],5,($invoice["j_inv_type"]==9?$invoice["j_inv_order"]:$invoice["j_inv_number"])." / Updated commission for: ".$u,$id);
			 }
			 else
				{
				 mysql_query("DELETE FROM nse_invoice_commission WHERE j_invcom_id=".$v);
				insertNote($invoice["j_inv_to_company"],2,"INV".$id,"Removed commission for: ".$u,0,1);
				J_act($types[$invoice["j_inv_type"]],5,($invoice["j_inv_type"]==9?$invoice["j_inv_order"]:$invoice["j_inv_number"])." / Removed commission for: ".$u,$id);
				}
			}
			elseif($_POST["u_".$d])
			{
				if(insert_comm($id, $_POST["i_".$d], $_POST["u_".$d], $p, $a))
				{
					$u=J_Value("","people","",$_POST["u_".$d]);
					insertNote($invoice["j_inv_to_company"],2,"INV".$id,"Added commission for: ".$u,0,1);
					J_act($types[$invoice["j_inv_type"]],5,($invoice["j_inv_type"]==9?$invoice["j_inv_order"]:$invoice["j_inv_number"])." / Added commission for: ".$u,$id);
				}
			}
		}
	}
	echo "<script>window.parent.J_WX(self)</script>";
	die();
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/commission/f/edit_invoice_commission.js></script>";

$i=0;

$q = "SELECT
j_invit_id
,j_invit_date
,j_invit_name
,j_invit_quantity
,j_invit_price
,j_invit_discount
,j_invit_vat
,j_invit_total
,j_invit_paid
,j_invcom_id
,j_invcom_user
,j_invcom_percent
,j_invcom_fixed
,j_invcom_paid
,j_invcom_paid_user
,j_invcom_paid_date
,j_in_commission_percent
,j_in_commission_fixed
,user_id
,firstname
,lastname
,j_invp_date
FROM nse_invoice_item
LEFT JOIN nse_invoice_commission ON j_invcom_item=j_invit_id
LEFT JOIN nse_user ON j_invcom_user=user_id
LEFT JOIN nse_invoice_item_paid ON j_invp_invoice_item=j_invit_id
LEFT JOIN nse_inventory ON j_invit_inventory=j_in_id
WHERE j_invit_invoice=".$id."
ORDER BY j_invit_date,firstname DESC";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$pp=array();
	$chk=array();
	echo "<script>rrH(".($SUB_ACCESS["edit"]?1:"").")</script>";

	while($g=mysql_fetch_array($q))
	{
		echo "<tr>";
		if(isset($chk[$g["j_invit_id"]]))
			echo "<th colspan=5></th>";
		else
		{
			$chk[$g["j_invit_id"]]=1;
			echo "<th class=jdbW>".$g["j_invit_name"]."</th>";
			echo "<td class=jdbC>".number_format($g["j_invit_price"]*$g["j_invit_quantity"],2,"."," ")."</td>";
			echo "<td class=jdbO>".number_format($g["j_invit_discount"],2,"."," ")."</td>";
			echo "<td class=jdbG>".number_format($g["j_invit_total"],2,"."," ")."</td>";
			echo "<td class=jdbG".($g["j_invp_date"]?" onmouseover=\"J_TT(this,'Paid: ".date("d/m/Y",$g["j_invp_date"])."')\"":"").">".number_format($g["j_invit_paid"]?$g["j_invit_paid"]:0,2,"."," ")."</td>";
		}
		if($SUB_ACCESS["edit"])
		{
			echo "<td>";
			echo "<input type=hidden value=".($g["j_in_commission_percent"]?$g["j_in_commission_percent"]:0).">";
			echo "<input type=hidden name=c_".$i." value=".($g["j_invcom_id"]?$g["j_invcom_id"]:0).">";
			echo "<input type=hidden name=ou_".$i." value=".$g["j_invcom_user"].">";
			echo "<input type=hidden name=i_".$i." value=".$g["j_invit_id"].">";
			echo "<input type=text value=\"".J_Value("","people","",$g["j_invcom_user"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=ra',320,4) onclick=rZ() onmouseover=\"J_TT(this,'<b>Person</b><br>Type in a few letters of the person you require')\"><input type=hidden name=u_".$i." value=".($g["j_invcom_user"]?$g["j_invcom_user"]:0)."></td>";
			echo "<td><input name=p_".$i." type=text value=\"".($g["j_invcom_percent"]?$g["j_invcom_percent"]."%":"")."\" class=jW100 onkeyup=rC(this) onclick=rC(this)><input type=hidden value=".($g["j_invcom_percent"]?$g["j_invcom_percent"]:0)."></td>";
			echo "<td><input name=a_".$i." type=text value=\"".($g["j_invcom_fixed"]?number_format($g["j_invcom_fixed"],2,"."," "):"")."\" class=jW100 onkeyup=rC(this,1)></td>";
			$i++;
		}
		else
		{
			echo "<td>".($g["j_invcom_user"]?$pp[$g["j_invcom_user"]]:"")."</td>";
			echo "<td>".($g["j_invcom_percent"]?$g["j_invcom_percent"]."%":"")."</td>";
			echo "<td>".($g["j_invcom_fixed"]?number_format($g["j_invcom_fixed"],2,"."," "):"")."</td>";
		}
		echo "<td class=jdbG".($g["j_invcom_paid"]?" onmousover=\"J_TT(this,'Paid: ".date("d/m/Y",$g["j_invcom_paid_date"])."<br><b>By: ".J_Value("","people","",$g["j_invcom_paid_user"])."</b>')\">".$g["j_invcom_paid"]:">")."</td>";
		echo "</tr>";
		$i++;
	}
	echo "</table>";
}
echo "<script>Jrr(".$id.($SUB_ACCESS["edit"]?",'".$_SESSION["j_user"]["role"]."'":"").")</script>";

$J_title1="Invoice Commission";
$J_title3="<span style=font-size:12pt;line-height:100%><b>".J_Value("","establishment","",$invoice["j_inv_to_company"])."</b><br>Invoice Number: ".$invoice["j_inv_number"]."<br>Date: ".date("d/m/Y",$invoice["j_inv_date"])."</span>";
$J_icon="<img src=".$ROOT."/ico/set/search.png><var><img src=".$ROOT."/ico/set/user_male.png></var>";
$J_label=19;
$J_width=1440;
$J_height=($i*40)+20;
$J_nostart=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>
