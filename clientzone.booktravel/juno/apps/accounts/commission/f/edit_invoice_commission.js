a=""
a+="<style type=text/css>"
a+="#jTR td,#jTR th{border-style:solid;border-color:#BBB;border-width:1;padding:4 8}"
a+=".jhed{background:#CCC;color:#888;font-size:9pt;white-space:nowrap}"
a+=".jdbG,.jdbR,.jdbY,.jdbO,.jdbC{white-space:nowrap;font-weight:bold;text-align:right;width:20}"
a+=".jdbW{width:90%}"
a+="#jTR input{font-weight:bold}"
a+="</style>"
a+="<body style=margin:0>"
document.write(a)
iid=999
lst=0

function rrH(x)
{
	var a=""
	a+="<iframe id=j_IF></iframe>"
	if(x)
		a+="<form method=post>"
	a+="<table id=jTR class=jW100>"
	a+="<tr class=jhed><th>Item</th><th>Price</th><th>Discount</th><th>Total</th><th>Paid</th><th>Rep/Assessor</th><th>Percent</th><th>Fixed Amount</th><th>Paid</th></tr>"
	document.write(a)
}

function Jrr(i,x)
{
	var a=""
	if(x)
		document.write("<input type=submit value=OK style=margin:20></form>")
	if(x=="b"||x=="d")
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/notes.php?id="+i+"');return false\" onmouseover=J_TT(this,'Notes') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/note.png></a>"
	a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/edit.php?id="+i+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Edit invoice')\" class=jW3a><img src="+ROOT+"/ico/set/spreadsheet.png></a>"
	if(x=="b"||x=="d")
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/payments/edit.php?id="+i+"');return false\" onmouseover=J_TT(this,'Payments') onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_payEdit.png></a>"
	J_footer(a)
	J_tr()
}

function rC(t,v)
{
	var p,d,v,i,a=t.parentNode.parentNode,o=a.childNodes,z=o[5]?0:4
	if(v)
	{
		if(t.value)
			o[6-z].firstChild.value=""
	}
	else
	{
		if(z)
		{
			while(a.previousSibling)
			{
				if(a.childNodes[5])
					break
				a=a.previousSibling
			}
			a=a.childNodes
		}
		else
			a=o
		if(t.value)
		{
			o[7-z].firstChild.value=""
			v=t.value.replace(/[^0-9\.]/g,"")*1
			p=parseFloat(a[1].innerHTML.replace(/[^0-9\.]/g,""))
			d=parseFloat(a[2].innerHTML.replace(/[^0-9\.]/g,""))
			i=parseFloat(a[5].firstChild.value.replace(/[^0-9\.]/g,""))*1
			J_note(t,v+"% Commission<br><b>"+((p-d)*(v/100)).toFixed(2)+"</b>"+(i?"<div class=jO50>* Default: "+i+"%</div>":""),0,3,0,0,1)
		}
		else
			J_note(t,"Default: "+parseFloat(a[5].firstChild.value)+"%",0,3,0,0,1)
	}
}

function J_ajax_Z(o)
{
	var v=o.nextSibling.value,a=o.parentNode.parentNode,o=a.childNodes,z=o[5]?0:4
	if(z)
	{
		while(a.previousSibling)
		{
			if(a.childNodes[5])
				break
			a=a.previousSibling
		}
		a=a.childNodes
	}
	else
		a=o
	o=o[6-z]
	o.firstChild.value=a[5].firstChild.value
	rC(o.firstChild)
	J_note(lst[0],"<img src="+ROOT+"/ico/set/load.gif>",0,2,0,-1,1)
	$("j_IF").src=ROOT+"/apps/accounts/commission/edit_invoice_commission.php?assoc="+v
}

function rP(r,c)
{
	if(r)
	{
		var i=0,u
		if(c)
		{
			u=lst[0].parentNode
			u.nextSibling.firstChild.value=u.firstChild.value
			u=u.parentNode.nextSibling
			while(u)
			{
				if(u.childNodes[5])
					break
				if(!u.style.display)
					lst[1][lst[1].length]=u
				u=u.nextSibling
			}
		}
		else
		{
			r=r.split("|")
			while(r[i])
			{
				u=r[i].split("~")
				rA(lst[0].previousSibling.value,u[0]*1,u[1],u[2],u[3])
				i++
			}
		}
		// adjust percentages/fixed
		var f,ff,p,pp,v,o=lst[0].parentNode.parentNode.childNodes,z=o[5]?0:4
		p=o[6-z].firstChild.value.replace(/[^0-9\.]/g,"")*1
		f=o[7-z].firstChild.value.replace(/[^0-9\.]/g,"")*1
		r=0
		if(p||f)
		{
			for(i in lst[1])
			{
				pp=lst[1][i].childNodes[2].lastChild.value.replace(/[^0-9\.]/g,"")*1
				ff=lst[1][i].childNodes[3].lastChild.value.replace(/[^0-9\.]/g,"")*1
				if(p && pp)
				{
					v=p*(pp/100)
					q=p-v
					r+=v
					lst[1][i].childNodes[2].firstChild.value=v.toFixed(2)+"%"
					if(q<0.01)
						break
				}
				else if(f && ff)
				{
					f=f-ff
					if(f<0)
						f=0
					o[7-z].firstChild.value=f
					r=1
					if(!f)
						break
				}
			}
			if(r)
			{
				if(p)
				{
					p=p-r
					o[6-z].firstChild.value=p>0?p.toFixed(2)+"%":""
					o=lst[0].parentNode.nextSibling
				}
				else
					o=lst[0].parentNode.nextSibling.nextSibling
				j_P.J_closer()
				J_note(o,"Commission adjusted across partners",0,3,0,0,1)
			}
		}
	}
	else
		J_note(lst[0],"No commission parners found",0,2,4,0,1)
	lst=[]
}

function rZ()
{
	lst=[j_E,[]]
	J_note(j_E.parentNode,(j_E.parentNode.parentNode.nextSibling&&!j_E.parentNode.parentNode.nextSibling.childNodes[5]?"<a href=go: onclick=\"j_Wi["+j_W+"]['F'][0]['W'].rP(1,1);return false\" onfocus=blur()><img src="+ROOT+"/ico/set/calc-48.png height=24 onmouseover=\"J_TT(this,'Recalculate commission for this item from this person onward')\"></a> ":"")+"<a href=go: onclick=\"j_Wi["+j_W+"]['F'][0]['W'].rA("+j_E.previousSibling.value+");return false\" onfocus=blur()><img src="+ROOT+"/ico/set/add.png height=24 onmouseover=\"J_TT(this,'Add more commission for this item')\"></a> "+(j_E.nextSibling.value*1>0?"<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/commission/pay_commission.php?id="+j_E.nextSibling.value+"');return false\" onfocus=blur()><img src="+ROOT+"/ico/set/user_male.png height=24 onmouseover=\"J_TT(this,'Pay commission on this item')\"></a> ":"")+"<a href=go: onclick=\"j_Wi["+j_W+"]['F'][0]['W'].rX("+j_E.previousSibling.value+");return false\" onfocus=blur()><img src="+ROOT+"/ico/set/cancel.png height=24 onmouseover=\"J_TT(this,'Remove this commission for this item')\"></a>",0,1,0,0,1,5)
}

function rX()
{
	var t=j_E.parentNode.parentNode,o=t.childNodes,z=o[5]?0:4
	o[5-z].lastChild.previousSibling.value=""
	o[5-z].lastChild.value=0
	o[6-z].firstChild.value=""
	o[7-z].firstChild.value=""
	if(!o[5])
		t.style.display="none"
}

function rA(i,ri,rn,rp,rf)
{
	var tr=document.createElement("tr"),td=document.createElement("td")
	td.colSpan=5
	tr.appendChild(td)
	td=document.createElement("td")
	td.innerHTML="<input type=hidden name=i_"+iid+" value="+i+"><input type=text "+(rn?"value=\""+rn+"\" ":"")+"onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=ra',320,4) onclick=rZ()><input "+(ri?"value=\""+ri+"\" ":"")+"type=hidden name=n_"+iid+" value=0>"
	tr.appendChild(td)
	td=document.createElement("td")
	td.innerHTML="<input name=p_"+iid+" "+(rp?"value=\""+rp+"\" ":"")+"type=text class=jW100 onkeyup=rC(this) onclick=rC(this)><input type=hidden value="+(rp?rp:0)+">"
	tr.appendChild(td)
	td=document.createElement("td")
	td.innerHTML="<input name=a_"+iid+" "+(rf?"value=\""+rf+"\" ":"")+"type=text class=jW100 onkeyup=rC(this,1)>"
	tr.appendChild(td)
	td=j_E.parentNode.parentNode
	if(td.nextSibling)
	{
		var y=0
		while(td.nextSibling)
		{
			td=td.nextSibling
			if(td.childNodes[5])
			{
				j_E.parentNode.insertBefore(tr,td)
				y=1
				break
			}
		}
		if(!y)
		td.parentNode.appendChild(tr)
	}
	else
		td.parentNode.appendChild(tr)
	J_tr()
	iid++
	if(ri)
		lst[1][lst[1].length]=tr
}