a="<style type=text/css>"
a+="#jTR td,#jTR th{border-style:solid;border-color:#BBB;border-width:1;padding:4 8}"
a+="td div{cursor:pointer;font-weight:normal;text-align:right}"
a+=".jdbB{cursor:pointer}"
a+=".jdbB b,.jdbW{font-size:14pt}"
a+="td.jdbWW{background:#FFF;text-align:right;padding:4 20 4 8;color:#888}"
a+="#ir{font-size:9pt;margin:4 20 20 auto;background:#FFF}"
a+=".jdbG,.jdbR,.jdbY{white-space:nowrap;font-weight:bold;text-align:right;width:20}"
a+=".rH{font-size:8pt;color:#888;background:#DDD;white-space:nowrap}"
a+=".rH th{padding:2 4}"
a+="#jTR input{width:80px;font-weight:bold}"
a+="</style>"
document.write(a)

function rH(t)
{
	document.write("<tr class=rH><th>Date</th><th>Establishment</th><th>Inventory Item</th>"+(t?"":"<th>Discount</th>")+"<th>Item Excl VAT</th>"+(t?"":"<th>Paid Incl VAT</th>")+"<th>Commission</th><th>Paid</th><th>Pay Date</th></tr>")
}

rl=0

function Jrr(d,r,p)
{
	rl=r
	var a=""
	a+="<a href=go: onclick=\"J_reload("+j_W+");return false\" onfocus=blur() onmouseover=\"J_TT(this,'Refresh report')\" class=jW3a><img src="+ROOT+"/ico/set/reload.png></a>"
	a+="<a href=go: onclick=\"J_W(ROOT+'/apps/people/edit_person.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Edit rep')\" class=jW3a><img src="+ROOT+"/ico/set/user_male.png></a>"
	if(p)
	{
		a+="<a href=go: onclick=\"J_note(this,'<center><b>Master payment</b> Enter a master payment to spread payments from this date/invoice forward.<input type=text onkeyup=j_Wi["+j_W+"][\\\'F\\\'][0][\\\'W\\\'].rCPX(this.value) class=jW100 style=\\\'display:block;margin:4 0 4 0\\\'><var id=rpM></var></center>',180,3);return false\" onfocus=blur() onmouseover=\"J_TT(this,'Pay multiple commission with a single total')\" class=jW3a><img src="+ROOT+"/ico/set/tick.png></a>"
		a+="<a href=go: onclick=\"j_Wi["+j_W+"][\'F\'][0][\'W\'].rCPX();return false\" onfocus=blur() onmouseover=\"J_TT(this,'Reset payments')\" class=jW3a><img src="+ROOT+"/ico/set/cancel.png></a>"
	}
	else if(r=="b"||r=="d"||r=="s")
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/commission/pay_commission.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Pay commission')\" class=jW3a><img src="+ROOT+"/ico/set/edit.png></a>"
	J_footer(a)
	J_tr()
	if(p)
		rCP()
}

function Rn(i,e)
{
	if(rl!="c")
	{
		var a=""
		a="<a href=go: onfocus=blur() onclick=\"J_W(ROOT+'/apps/accounts/invoices/"+(rl=="b"||rl=="d"?"edit":"invoice_view.php")+".php?id="+i+"');return false\" onmouseover=\"J_TT(this,'Invoice')\"><img src="+ROOT+"/ico/set/spreadsheet.png height=32></a>"
		a+="<a href=go: onfocus=blur() onclick=\"J_W(ROOT+'/apps/accounts/commission/edit_invoice_commission.php?id="+i+"');return false\" onmouseover=\"J_TT(this,'Invoice commission details')\"><img src="+ROOT+"/ico/set/user_male.png height=32></a>"
		a+="<a href=go: onfocus=blur() onclick=\"J_W(ROOT+'/apps/establishment_manager/manager.php?id="+e+"');return false\" onmouseover=\"J_TT(this,'Establishment manager')\"><img src="+ROOT+"/ico/set/gohome-64.png height=32></a>"
		J_note("x",a,0,3,0,0,1)
	}
}

function Rp(t,v)
{
	J_TT(t,"Paid: "+v)
}

function Pv(t,v)
{
	if(v)
		J_TT(t,"<b>Incl VAT:</b> <b style=color:#CC0000>"+v+"</b>")
}

function im(t)
{
	J_TT(t,"View inventory breakdown")
}

function vn(t,v)
{
	J_TT(t,"Invoice number "+(v?v:"none"))
}

function ir(t)
{
	t=$T(t,"table")[0]
	t.className=t.className?"":"ji"
}

function io(t)
{
	t=t.parentNode.nextSibling
	t.className=t.className?"":"ji"
}

function rS(t,v)
{
	J_TT(t,v.replace(/~/g,"<br>").replace(/!/g,"Fixed amount"))
}

// pay functions
ai=0

function rAi()
{
	if(!ai)
	{
		ai=[]
		var p=$T($("jTR"),"td"),i=0
		while(p[i])
		{
			if(p[i].className=="jdbG")
				ai[ai.length]=[p[i],p[i].firstChild.tagName=="INPUT"?p[i].firstChild:0,p[i].firstChild.tagName=="INPUT"?p[i].previousSibling.innerHTML.replace(/[^0-9\.]/g,"")*1:0]
			i++
		}
	}
}

function rCP(v,m)
{
	var i,p,pv,mv=0,tt=0
	rAi()
	if(v)
		j_E.value=(j_E.parentNode.previousSibling.innerHTML.replace(/[^0-9\.]/g,"")*1).toFixed(2)
	else if(j_E)
	{
		if(j_E.tagName=="INPUT")
		{
			p=j_E.value.replace(/[^0-9\.]/g,"")*1
			i=j_E.parentNode.previousSibling.innerHTML.replace(/[^0-9\.]/g,"")*1
			if(p>i)
				j_E.value=i
		}
	}
	else if(m)
	{
		m=m.replace(/[^0-9\.]/g,"")*1
		m=m?m:0
		i=ai.length-1
		while(ai[i])
		{
			if(ai[i][1])
			{
				v=m>ai[i][2]?ai[i][2]:m
				ai[i][1].value=v?v.toFixed(2):""
				ai[i][0].style.background=ai[i][1].value?"greenyellow":"coral"
				m=m>ai[i][2]?m-ai[i][2]:0
			}
			i--
		}
		pv=1
	}

	i=0
	while(ai[i])
	{
		if(ai[i][1])
		{
			p=ai[i][1].value.replace(/[^0-9\.]/g,"")*1
			if(p>0)
			{
				mv+=p
				tt+=p
				ai[i][0].parentNode.style.background=p>=ai[i][2]?"greenyellow":"coral"
				ai[i][0].style.background=p>0?"greenyellow":""
				ai[i][0].nextSibling.innerHTML=j_P.J_date("Y/m/d")
			}
			else
			{
				ai[i][1].value=""
				ai[i][0].style.background=""
				ai[i][0].parentNode.style.background="coral"
				ai[i][0].nextSibling.innerHTML=ai[i][0].nextSibling.lang
			}
		}
		else if(ai[i][0].id=="tt")
			ai[i][0].innerHTML=tt.toFixed(2)
		else
		{
			ai[i][0].innerHTML=mv.toFixed(2)
			mv=0
		}
		i++
	}
	if(pv)
		j_P.$("rpM").innerHTML="Paid: "+tt.toFixed(2)
}

function rCPX(m)
{
	if(!m)
		j_P.J_closer()
	rAi()
	var i=0
	while(ai[i])
	{
		if(ai[i][1])
		{
			ai[i][1].value=ai[i][0].lang
			ai[i][0].nextSibling.style.background=ai[i][0].lang*1>0?"greenyellow":"coral"
		}
		i++
	}
	j_E=0
	rCP(0,m)
}

function P_(i,e)
{
	if(rl=="c"||(j_E.tagName=="INPUT"&&j_E.parentNode.className=="jdbG")||j_E.className=="jdbG")
	{}
	else
	{
		var a=""
		a="<a href=go: onfocus=blur() onclick=\"J_W(ROOT+'/apps/accounts/invoices/"+(rl=="b"||rl=="d"?"edit":"invoice_view.php")+".php?id="+i+"');return false\" onmouseover=\"J_TT(this,'Invoice')\"><img src="+ROOT+"/ico/set/spreadsheet.png height=32></a>"
		a+="<a href=go: onfocus=blur() onclick=\"J_W(ROOT+'/apps/accounts/commission/edit_invoice_commission.php?id="+i+"');return false\" onmouseover=\"J_TT(this,'Invoice commission details')\"><img src="+ROOT+"/ico/set/user_male.png height=32></a>"
		a+="<a href=go: onfocus=blur() onclick=\"J_W(ROOT+'/apps/establishment_manager/manager.php?id="+e+"');return false\" onmouseover=\"J_TT(this,'Establishment manager')\"><img src="+ROOT+"/ico/set/gohome-64.png height=32></a>"
		J_note("x",a,0,3,0,0,1)
	}
}