<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/vat2/report.js></script>";

if(isset($_GET["s"]))
{
	function plus($y,$m=0)
	{
		$v=0;
		if($m)
		{
			$mm=$m+1;
			$mm>12?12:$mm;
		}
		$ds=mktime(0,0,0,$m?$m:1,1,$y);
		$de=mktime(0,0,0,$m?$mm:1,1,$m?$y:$y+1);
		$q="SELECT j_inv_vat_paid FROM nse_invoice WHERE j_inv_vat_paid>0 AND j_inv_vat_paid_date>".$ds." AND  j_inv_vat_paid_date<".$de;
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
				$v+=$g["j_inv_vat_paid"];
		}
		return $v;
	}
	$mnth=array("-","January","February","March","April","May","June","July","August","September","October","November","December");
	$pie=array();
	$o="";
	$tp=0;
	$y=date("Y",$EPOCH);
	while($y>2009)
	{
		$ty=plus($y);
		if($ty)
		{
			$tp+=$ty;
			$o.=$y."|";
			$m=12;
			while($m>0)
			{
				$tm=plus($y,$m);
				if($tm)
				{
					$o.="~~".$m."~".number_format($tm,2,"."," ")."|";
					if(count($pie)<13)
						$pie[]=array($y." ".$mnth[$m],$tm);
				}
				$m--;
			}
			$o.="~".number_format($ty,2,"."," ")."|";
		}
		$y-=1;
	}
	$o.="~~~~".number_format($tp,2,"."," ")."|";

	echo "<body><script>J_inv_s(\"".$o."\")</script>";
	if(count($pie))
	{
		echo "<script src=http://www.google.com/jsapi></script>";
		echo "<script>";
		echo "google.load('visualization',1,{packages:['piechart']});";
		echo "google.setOnLoadCallback(drawChart);";
		echo "function drawChart()";
		echo "{";
		echo "var d=new google.visualization.DataTable();";
		echo "d.addColumn('string','To Paid Invoices');";
		echo "d.addColumn('number','VAT Payments');";
		echo "d.addRows(".count($pie).");";
		foreach($pie as $k => $v)
		{
			echo "d.setValue(".$k.",0,\"".$v[0]."\");";
			echo "d.setValue(".$k.",1,".$v[1].");";
		}
		echo "var c=new google.visualization.PieChart($('c0'));";
		echo "c.draw(d,{width:480,height:320,tooltipWidth:'120',legendFontSize:'11pt'})";
		echo "}</script>";
	}
	echo "</body></html>";
	die();
}

include $SDR."/apps/accounts/invoices/default.php";
echo "<body class=j_List>";
$s=$SDR."/stuff/people/".$_SESSION["j_user"]["id"]."/set.php";
if(is_file($s))include $s;
$s=(isset($j_vatReport)?$j_vatReport:"");

$role=$_SESSION["j_user"]["role"];

echo "<script>J_inv_N(";
echo "'".$s."'";
echo ",'".date('d/m/Y',mktime(0,0,0,date("m")-2,1,date("Y")))."'";
echo ",'".date('d/m/Y',$EPOCH)."'";
echo ",".(isset($_GET["eid"])?"'".$_GET["eid"]."',\"".J_Value("","","",preg_replace("~[a-zA-Z0-9 ,:;'&!@#%+-_\$\(\)\-\.]~","",$_GET["eid"]))."\"":"0,0");
echo ",'".$role."'";
echo ")</script>";

$J_home=8;
$J_title1="VAT Report";
$J_icon="<img src=".$ROOT."/ico/set/collection_account_64.png>";
$J_label=19;
$J_width=1440;
$J_framesize=240;
$J_frame1=$ROOT."/apps/accounts/vat2_report.php?s=1".(isset($_GET["eid"])?"&eid=1":"");
include $SDR."/system/deploy.php";
echo "</body></html>";
?>