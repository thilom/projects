<?php
require_once	$_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/parse.php";
include_once $SDR."/system/get.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$date=isset($_GET["ds"])?J_dateParse($_GET["ds"]):$EPOCH;
$y=date("Y",$date);
$m=date("m",$date);
$curr_s=mktime(0,0,0,date($m)+1,date(1),date($y));
$curr_e=mktime(0,0,0,date($m),date(1),date($y));
$d30=mktime(0,0,0,date($m)-1,date(1),date($y));
$d60=mktime(0,0,0,date($m)-2,date(1),date($y));
$d90=mktime(0,0,0,date($m)-3,date(1),date($y));
$to=0;

function paid($ds=-1,$de=0)
{
	global $curr_s;
	$v=0;
	$q="SELECT j_invp_paid,j_invp_date,j_invp_invoice FROM nse_invoice_item_paid JOIN nse_invoice ON j_invp_invoice=j_inv_id WHERE j_invp_date>=".$ds." AND j_invp_date<".$de." AND j_invp_created<".$curr_s;
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
			$v+=$g["j_invp_paid"];
	}
	return $v;
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/dashboard/res.js></script>";
echo "<body>";
echo "<tt class=jO70>General report as of ".$_GET["ds"]."</tt><hr>";

$t_pf=0;
$t_inv=0;
$t_crn=0;
$t_dbn=0;
$t_vat=0;
$t_pf=0;
$tt_inv=0;
$tt_pay=0;
$tt_vat=0;

echo "<h6 class=jO30>Fixed Age Analysis</h6>";
echo "<table id=jTR class=aa>";
echo "<tr class=j_in_nam><th>Period &nbsp;</th><th>Age &nbsp;</th><th>All types incl VAT &nbsp;</th><th>VAT &nbsp;</th><th>Paid &nbsp;</th><th>% &nbsp;</th><th width=60>Pro-Formas &nbsp;</th><th width=60>Invoices &nbsp;</th><th width=60>Credits &nbsp;</th><th width=60>Debits &nbsp;</th></tr>";

$to=0;
//120
echo "<tr onclick=D_note(5,'".date("d/m/Y",$d90)."')><td class=jdbC><b>120 Days</b></td>";
$t=0;
$pf=0;
$inv=0;
$crn=0;
$dbn=0;
$vat=0;
$pd=paid(0,$d90);
$tt_pay+=$pd;
$q=mysql_query("SELECT j_inv_type,j_inv_total,j_inv_total_vat FROM nse_invoice WHERE j_inv_type>1 AND j_inv_type<6 AND j_inv_date<".$d90." AND j_inv_created<".$curr_s);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["j_inv_type"]==1)
			$pf+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==2)
			$inv+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==3)
			$crn+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==4)
			$dbn+=$g["j_inv_total"];
		$t+=$g["j_inv_total"];
		$vat+=$g["j_inv_total_vat"];
	}
	$t_inv+=$inv;
	$t_crn+=$crn;
	$t_dbn+=$dbn;
	$t_pf+=$pf;
	$tt_vat+=$vat;
	$tt_inv+=$t;
}
echo "<td class=jdbC><b>";
$v=$t-$pd;
$to+=$v;
echo number_format($v,2,"."," ");
echo "</b></td>";
echo "<td class=jdbY>".number_format($t,2,"."," ")."</td>";
echo "<td class=jdbR>".number_format($vat,2,"."," ")."</td>";
echo "<td class=jdbG>".number_format(0-$pd,2,"."," ")."</td>";
echo "<td class=jdbG>".($t?number_format(($pd/$t)*100,1,"."," ")."%":"-")."</td>";
echo "<td>".number_format($pf,2,"."," ")."</td>";
echo "<td>".number_format($inv,2,"."," ")."</td>";
echo "<td class=jdbO>".number_format($crn,2,"."," ")."</td>";
echo "<td>".number_format($dbn,2,"."," ")."</td>";
echo "</tr>";
//90
echo "<tr onclick=D_note(4,'".date("d/m/Y",$d90)."','".date("d/m/Y",$d60)."')><td class=jdbC><b>90 Days</b></td>";
$t=0;
$pf=0;
$inv=0;
$crn=0;
$dbn=0;
$vat=0;
$pd=paid($d90,$d60);
$tt_pay+=$pd;
$q=mysql_query("SELECT j_inv_type,j_inv_total,j_inv_total_vat FROM nse_invoice WHERE j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d90." AND j_inv_date<".$d60." AND j_inv_created<".$curr_s);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["j_inv_type"]==1)
			$pf+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==2)
			$inv+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==3)
			$crn+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==4)
			$dbn+=$g["j_inv_total"];
		$t+=$g["j_inv_total"];
		$vat+=$g["j_inv_total_vat"];
	}
	$t_inv+=$inv;
	$t_crn+=$crn;
	$t_dbn+=$dbn;
	$t_pf+=$pf;
	$tt_vat+=$vat;
	$tt_inv+=$t;
}
echo "<td class=jdbC><b>";
$v=$t-$pd;
$to+=$v;
echo number_format($v,2,"."," ");
echo "</b></td>";
echo "<td class=jdbY>".number_format($t,2,"."," ")."</td>";
echo "<td class=jdbR>".number_format($vat,2,"."," ")."</td>";
echo "<td class=jdbG>".number_format(0-$pd,2,"."," ")."</td>";
echo "<td class=jdbG>".($t?number_format(($pd/$t)*100,1,"."," ")."%":"-")."</td>";
echo "<td>".number_format($pf,2,"."," ")."</td>";
echo "<td>".number_format($inv,2,"."," ")."</td>";
echo "<td class=jdbO>".number_format($crn,2,"."," ")."</td>";
echo "<td>".number_format($dbn,2,"."," ")."</td>";
echo "</tr>";
//60
echo "<tr onclick=D_note(3,'".date("d/m/Y",$d60)."','".date("d/m/Y",$d30)."')><td class=jdbC><b>60 Days</b></td>";
$t=0;
$pf=0;
$inv=0;
$crn=0;
$dbn=0;
$vat=0;
$pd=paid($d60,$d30);
$q=mysql_query("SELECT j_inv_type,j_inv_total,j_inv_total_vat FROM nse_invoice WHERE j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d60." AND j_inv_date<".$d30." AND j_inv_created<".$curr_s);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["j_inv_type"]==1)
			$pf+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==2)
			$inv+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==3)
			$crn+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==4)
			$dbn+=$g["j_inv_total"];
		$t+=$g["j_inv_total"];
		$vat+=$g["j_inv_total_vat"];
	}
	$t_inv+=$inv;
	$t_crn+=$crn;
	$t_dbn+=$dbn;
	$t_pf+=$pf;
	$tt_vat+=$vat;
	$tt_inv+=$t;
}
echo "<td class=jdbC><b>";
$v=$t-$pd;
$to+=$v;
echo number_format($v,2,"."," ");
echo "</b></td>";
echo "<td class=jdbY>".number_format($t,2,"."," ")."</td>";
echo "<td class=jdbR>".number_format($vat,2,"."," ")."</td>";
echo "<td class=jdbG>".number_format(0-$pd,2,"."," ")."</td>";
echo "<td class=jdbG>".($t?number_format(($pd/$t)*100,1,"."," ")."%":"-")."</td>";
echo "<td>".number_format($pf,2,"."," ")."</td>";
echo "<td>".number_format($inv,2,"."," ")."</td>";
echo "<td class=jdbO>".number_format($crn,2,"."," ")."</td>";
echo "<td>".number_format($dbn,2,"."," ")."</td>";
echo "</tr>";
$tt_pay+=$pd;
//30
echo "<tr onclick=D_note(2,'".date("d/m/Y",$d30)."','".date("d/m/Y",$curr_e)."')><td class=jdbC><b>30 Days</b></td>";
$t=0;
$pf=0;
$inv=0;
$crn=0;
$dbn=0;
$vat=0;
$pd=paid($d30,$curr_e);
$tt_pay+=$pd;
$q=mysql_query("SELECT j_inv_type,j_inv_total,j_inv_total_vat FROM nse_invoice WHERE j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$d30." AND j_inv_date<".$curr_e." AND j_inv_created<".$curr_s);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["j_inv_type"]==1)
			$pf+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==2)
			$inv+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==3)
			$crn+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==4)
			$dbn+=$g["j_inv_total"];
		$t+=$g["j_inv_total"];
		$vat+=$g["j_inv_total_vat"];
	}
	$t_inv+=$inv;
	$t_crn+=$crn;
	$t_dbn+=$dbn;
	$t_pf+=$pf;
	$tt_vat+=$vat;
	$tt_inv+=$t;
}
echo "<td class=jdbC><b>";
$v=$t-$pd;
$to+=$v;
echo number_format($v,2,"."," ");
echo "</b></td>";
echo "<td class=jdbY>".number_format($t,2,"."," ")."</td>";
echo "<td class=jdbR>".number_format($vat,2,"."," ")."</td>";
echo "<td class=jdbG>".number_format(0-$pd,2,"."," ")."</td>";
echo "<td class=jdbG>".($t?number_format(($pd/$t)*100,1,"."," ")."%":"-")."</td>";
echo "<td>".number_format($pf,2,"."," ")."</td>";
echo "<td>".number_format($inv,2,"."," ")."</td>";
echo "<td class=jdbO>".number_format($crn,2,"."," ")."</td>";
echo "<td>".number_format($dbn,2,"."," ")."</td>";
echo "</tr>";
//current
echo "<tr onclick=D_note(1,'".date("d/m/Y",$curr_e)."','".date("d/m/Y",$curr_s)."')><td class=jdbC><b>Current</b></td>";
$t=0;
$pf=0;
$inv=0;
$crn=0;
$dbn=0;
$vat=0;
$pd=paid($curr_e,$curr_s);
$tt_pay+=$pd;
$q=mysql_query("SELECT j_inv_type,j_inv_total,j_inv_total_vat FROM nse_invoice WHERE j_inv_type>1 AND j_inv_type<6 AND j_inv_date>=".$curr_e." AND j_inv_date<".$curr_s." AND j_inv_created<".$curr_s);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["j_inv_type"]==1)
			$pf+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==2)
			$inv+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==3)
			$crn+=$g["j_inv_total"];
		elseif($g["j_inv_type"]==4)
			$dbn+=$g["j_inv_total"];
		$t+=$g["j_inv_total"];
		$vat+=$g["j_inv_total_vat"];
	}
	$t_inv+=$inv;
	$t_crn+=$crn;
	$t_dbn+=$dbn;
	$t_pf+=$pf;
	$tt_vat+=$vat;
	$tt_inv+=$t;
}
echo "<td class=jdbC><b>";
$v=$t-$pd;
$to+=$v;
echo number_format($v,2,"."," ");
echo "</b></td>";
echo "<td class=jdbY>".number_format($t,2,"."," ")."</td>";
echo "<td class=jdbR>".number_format($vat,2,"."," ")."</td>";
echo "<td class=jdbG>".number_format(0-$pd,2,"."," ")."</td>";
echo "<td class=jdbG>".($t?number_format(($pd/$t)*100,1,"."," ")."%":"-")."</td>";
echo "<td>".number_format($pf,2,"."," ")."</td>";
echo "<td>".number_format($inv,2,"."," ")."</td>";
echo "<td class=jdbO>".number_format($crn,2,"."," ")."</td>";
echo "<td>".number_format($dbn,2,"."," ")."</td>";
echo "</tr>";

//owed
echo "<tr onclick=D_note(0,'".date("d/m/Y",$curr_e)."')><td></td>";
echo "<td class=jdbC><b>".number_format($to,2,"."," ")."</b></td>";
echo "<td class=jdbY>".number_format($tt_inv,2,"."," ")."</td>";
echo "<td class=jdbR>".number_format($tt_vat,2,"."," ")."</td>";
echo "<td class=jdbG>".number_format(0-$tt_pay,2,"."," ")."</td>";
echo "<td class=jdbG>".($tt_inv?number_format(($tt_pay/$tt_inv)*100,1,"."," ")."%":"-")."</td>";
echo "<td>".number_format($t_pf,2,"."," ")."</td>";
echo "<td>".number_format($t_inv,2,"."," ")."</td>";
echo "<td class=jdbO>".number_format($t_crn,2,"."," ")."</td>";
echo "<td>".number_format($t_dbn,2,"."," ")."</td>";
echo "</tr>";

echo "</table>";
echo "<br>";




function i_age($in,$d1=0,$d2=0)
{
	global $curr_s;
	$v=array("invoice"=>0,"paid"=>0,"age"=>0);
	$q="SELECT j_invit_total,j_invit_paid ";
	$q.="FROM nse_invoice_item ";
	$q.="JOIN nse_invoice ON j_invit_invoice=j_inv_id ";
	$q.="WHERE j_invit_inventory=".$in." ";
	$q.="AND j_inv_type>1 AND j_inv_type<6 ";
	if($d1)
		$q.="AND j_inv_date>=".$d1." ";
	$q.="AND j_inv_date<".$d2." AND j_inv_created<".$curr_s;
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
		{
			$v["invoice"]+=$g["j_invit_total"];
			$v["paid"]+=$g["j_invit_paid"];
		}
	}
	return $v;
}

$ina=array();

$q="SELECT j_in_id,j_in_name,j_inp_publication,j_in_max_room,j_in_category FROM nse_inventory
	LEFT JOIN nse_inventory_publications ON j_inp_inventory=j_in_id
	ORDER BY j_in_category,j_in_name";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["j_inp_publication"])
			$n=J_Value("publication_name","nse_publications","publication_id",$g["j_inp_publication"]);
		elseif($g["j_in_category"]==5)
			$n="Quality Assurance";
		elseif($g["j_in_category"]==6)
			$n="Quality Assurance (Caravan & Camping)";
		elseif($g["j_in_category"]==15)
			$n="Quality Assurance New";
		elseif($g["j_in_category"]==16)
			$n="Quality Assurance New (Caravan & Camping)";
		else
			$n=$g["j_in_name"];

		if(!isset($ina[$n]))
			$ina[$n]=array("120"=>array("invoice"=>0,"paid"=>0),"90"=>array("invoice"=>0,"paid"=>0),"60"=>array("invoice"=>0,"paid"=>0),"30"=>array("invoice"=>0,"paid"=>0),"curr"=>array("invoice"=>0,"paid"=>0));

		$v=i_age($g["j_in_id"],0,$d90);
		$ina[$n]["120"]["invoice"]+=$v["invoice"];
		$ina[$n]["120"]["paid"]+=$v["paid"];
		$v=i_age($g["j_in_id"],$d90,$d60);
		$ina[$n]["90"]["invoice"]+=$v["invoice"];
		$ina[$n]["90"]["paid"]+=$v["paid"];
		$v=i_age($g["j_in_id"],$d60,$d30);
		$ina[$n]["60"]["invoice"]+=$v["invoice"];
		$ina[$n]["60"]["paid"]+=$v["paid"];
		$v=i_age($g["j_in_id"],$d30,$curr_s);
		$ina[$n]["30"]["invoice"]+=$v["invoice"];
		$ina[$n]["30"]["paid"]+=$v["paid"];
		$v=i_age($g["j_in_id"],$curr_s,$curr_e);
		$ina[$n]["curr"]["invoice"]+=$v["invoice"];
		$ina[$n]["curr"]["paid"]+=$v["paid"];
	}
}

echo "<h6 class=jO30>Product Analysis</h6>";
echo "<table id=jTR class=aa>";
echo "<tr class=j_in_nam><th>Product &nbsp;</th><th>120 Days &nbsp;</th><th>90 Days &nbsp;</th><th>60 Days &nbsp;</th><th>30 Days &nbsp;</th><th>Current &nbsp;</th><th width=60>Owed &nbsp;</th></tr>";

ksort($ina);
foreach($ina as $k => $v)
{
	$inv=0;
	$pay=0;
	echo "<tr>";
	echo "<td><b>".$k."</b></td>";
	$inv+=$v["120"]["invoice"];
	$pay+=$v["120"]["paid"];
	echo "<td onmouseover=\"J_TT(this,'Invoices: ".number_format($v["120"]["invoice"],2,"."," ")."<br>Paid: ".number_format($v["120"]["paid"],2,"."," ")." (".($v["120"]["invoice"]?number_format(($v["120"]["paid"]/$v["120"]["invoice"])*100,1,"."," ")."%":"-").")')\">".number_format($v["120"]["invoice"]-$v["120"]["paid"],2,"."," ")."</td>";
	$inv+=$v["90"]["invoice"];
	$pay+=$v["90"]["paid"];
	echo "<td onmouseover=\"J_TT(this,'Invoices: ".number_format($v["90"]["invoice"],2,"."," ")."<br>Paid: ".number_format($v["90"]["paid"],2,"."," ")." (".($v["90"]["invoice"]?number_format(($v["90"]["paid"]/$v["90"]["invoice"])*100,1,"."," ")."%":"-").")')\">".number_format($v["90"]["invoice"]-$v["90"]["paid"],2,"."," ")."</td>";
	$inv+=$v["60"]["invoice"];
	$pay+=$v["60"]["paid"];
	echo "<td onmouseover=\"J_TT(this,'Invoices: ".number_format($v["60"]["invoice"],2,"."," ")."<br>Paid: ".number_format($v["60"]["paid"],2,"."," ")." (".($v["60"]["invoice"]?number_format(($v["60"]["paid"]/$v["60"]["invoice"])*100,1,"."," ")."%":"-").")')\">".number_format($v["60"]["invoice"]-$v["60"]["paid"],2,"."," ")."</td>";
	$inv+=$v["30"]["invoice"];
	$pay+=$v["30"]["paid"];
	echo "<td onmouseover=\"J_TT(this,'Invoices: ".number_format($v["30"]["invoice"],2,"."," ")."<br>Paid: ".number_format($v["30"]["paid"],2,"."," ")." (".($v["30"]["invoice"]?number_format(($v["30"]["paid"]/$v["30"]["invoice"])*100,1,"."," ")."%":"-").")')\">".number_format($v["30"]["invoice"]-$v["30"]["paid"],2,"."," ")."</td>";
	$inv+=$v["curr"]["invoice"];
	$pay+=$v["curr"]["paid"];
	echo "<td class=jdbG onmouseover=\"J_TT(this,'Invoices: ".number_format($v["curr"]["invoice"],2,"."," ")."<br>Paid: ".number_format($v["curr"]["paid"],2,"."," ")." (".($v["curr"]["invoice"]?number_format(($v["curr"]["paid"]/$v["curr"]["invoice"])*100,1,"."," ")."%":"-").")')\">".number_format($v["curr"]["invoice"]-$v["curr"]["paid"],2,"."," ")."</td>";
	echo "<td class=jdbR onmouseover=\"J_TT(this,'Invoices: ".number_format($inv,2,"."," ")."<br>Paid: ".number_format($pay,2,"."," ")." (".($inv?number_format(($pay/$inv)*100,1,"."," ")."%":"-").")')\">".number_format($inv-$pay,2,"."," ")."</td>";
	echo "</tr>";
}

echo "</table>";
echo "<br>";


echo "<script>D_end()</script>";
echo "</body></html>";
?>
