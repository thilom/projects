<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/parse.php";

//Vars
$period = substr($_GET['period'], 2);
$d = "X~Type~Number~Code~Establishment Name~Document Date~Value (Excl. VAT)~VAT";
$d_csv = "Type~Number~Code~Establishment Name~Document Date~Value (Excl. VAT)~VAT";
$months = array(1=>'January','February','March','April','May','June','July','August','September','October','November','December');
$result = '';
$result_csv = '';
$document_types = array(2=>'Tax Invoice', 3=>'Credit Note');
$total_invoice = 0;
$total_vat = 0;
$vat_value = isset($_POST['sars_payment'])?(float)$_POST['sars_payment']:0;
$vat_id = 0;
$snapshot = array();
$payment_info = '';
$total_payments = 0;

//Assemble Dates
list($month, $year) = explode('-',$period);
$month++; //Increment by 1 since JS starts months at 0
$vat_period = $month . $year;
$last_day = date('t', mktime(0, 0, 0, $month, 1, $year));
$end_date = mktime(23, 59, 59, $month, $last_day, $year);
$start_month = $month-1;
$start_year = $year;
if ($start_month<=0) {
	$start_month += 12;
	$start_year -= 1;
}
$start_date = mktime(0, 0, 0, $start_month, 1, $start_year);

//Get payment info
$statement = "SELECT DATE_FORMAT(payment_date, '%d %M %Y'), payment_value
			FROM nse_vat_payments
			WHERE vat_period = ?";
$sql_payments = $GLOBALS['dbCon']->prepare($statement);
$sql_payments->bind_param('s', $vat_period);
$sql_payments->execute();
$sql_payments->bind_result($payment_date, $payment_value);
while ($sql_payments->fetch()) {
	$payment_display = number_format($payment_value,2,'.',' ');
	$payment_info .= "Paid R$payment_value on $payment_date<br>";
	$total_payments += $payment_value;
}
$sql_payments->close();
$payment_info = substr($payment_info, 0, -4);

if (isset($_POST) && count($_POST) > 0) {
	//Add payment to DB
	$statement = "INSERT INTO nse_vat_payments
					(vat_period, payment_date, payment_value)
				VALUES
					(?,NOW(),?)";
	$sql_insert_payment = $GLOBALS['dbCon']->prepare($statement);
	$sql_insert_payment->bind_param('ss', $vat_period, $vat_value);
	$sql_insert_payment->execute();
	$vat_id = $sql_insert_payment->insert_id;
	$sql_insert_payment->close();
}

//Get all invoices
$statement = "SELECT a.j_inv_id, a.j_inv_to_company, b.establishment_name, a.j_inv_date, a.j_inv_type, a.j_inv_total_price, a.j_inv_total_vat, a.j_inv_number
			FROM nse_invoice AS a
			LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code
			WHERE (a.j_inv_type = 2 OR a.j_inv_type=3) AND a.j_inv_date < '$end_date' AND a.j_inv_date > '$start_date'
			ORDER BY a.j_inv_date";
$sql_invoices = $GLOBALS['dbCon']->prepare($statement);
$sql_invoices->execute();
$sql_invoices->bind_result($invoice_id, $establishment_code, $establishment_name, $invoice_date, $invoice_type, $price, $vat, $invoice_number);
while ($sql_invoices->fetch()) {
	$establishment_name = addslashes($establishment_name);
	$display_date = date('d-m-Y', $invoice_date);
	$display_price = number_format($price,2,'.',' ');
	$display_vat = number_format($vat,2,'.',' ');
	$csv_price = number_format($price,2,'.','');
	$csv_vat = number_format($vat,2,'.','');
	$csv_establishment_name = str_replace(',','',$establishment_name);
	$result .= "$invoice_id~{$document_types[$invoice_type]}~$invoice_number~$establishment_code~$establishment_name~$display_date~$display_price~$display_vat|";
	$result_csv .= "{$document_types[$invoice_type]}~$invoice_number~$establishment_code~$csv_establishment_name~$display_date~$csv_price~$csv_vat|";

	if (isset($_POST) && count($_POST) > 0) {
		$snapshot[$invoice_id]['type'] = $invoice_type;
		$snapshot[$invoice_id]['number'] = $invoice_number;
		$snapshot[$invoice_id]['code'] = $establishment_code;
		$snapshot[$invoice_id]['date'] = date('Y-m-d', $invoice_date);
		$snapshot[$invoice_id]['price'] = number_format($price,2,'.','');
		$snapshot[$invoice_id]['vat'] = number_format($vat,2,'.','');
	}

	$total_invoice += $price;
	$total_vat += $vat;
}
$sql_invoices->close();

//Save Snapshot
if (isset($_POST) && count($_POST) > 0) {

	//Prepare statement - insert snapshot
	$statement = "INSERT INTO nse_vat_snapshot
					(vat_id, j_inv_type, j_inv_number, establishment_code, j_inv_date, j_inv_total, j_inv_vat)
				VALUES
					(?,?,?,?,?,?,?)";
	$sql_snapshot = $GLOBALS['dbCon']->prepare($statement);

	foreach ($snapshot as $data) {
		$sql_snapshot->bind_param('sssssss', $vat_id, $data['type'], $data['number'], $data['code'], $data['date'], $data['price'], $data['vat']);
		$sql_snapshot->execute();
	}
}

$total_invoice = number_format($total_invoice,2,'.', ' ');
$total_display_vat = number_format($total_vat,2,'.', ' ');
$result .= "~~~~~~$total_invoice~$total_display_vat|";

//Heading
$heading = "VAT report for period ending $last_day {$months[$month]} $year";

//VAT for payment field
$payment_vat = $total_vat - $total_payments;
if ($payment_vat < 0) $payment_vat = 0;
$payment_vat = number_format($payment_vat,2,'.', '');

//CSV file
$file = "/apps/reports/files/vat_report_{$_SESSION['j_user']['id']}.csv";
$fh = fopen($SDR.$file, 'w');
fputs($fh, str_replace('~',',',$d_csv) . PHP_EOL);
fputs($fh, str_replace('|',"\n\r",str_replace('~',',',$result_csv)));
fclose($fh);


echo "<html><head>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/accounts/vat2/vat2_res.js></script>";
echo "</head><body  class=jSCH>";
echo "<div id='resultContent'></div>";
echo "</body>";
echo "<script>J_in_r('$result','$d','$heading','$payment_vat','$payment_info','$file')</script>";

?>