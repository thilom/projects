<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/parse.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$d="";

$s="SELECT
user_id
,firstname
,lastname
,phone
,cell
,email
,rep_commission
";
$s.="FROM nse_user ";
$s.="WHERE (roles LIKE '%a%' OR roles LIKE '%r%')";
if(isset($_GET["rep"]))
	$s.="AND user_id=".$_GET["rep"];
$jL0=mysql_num_rows(mysql_query($s));
$s.=" ORDER BY firstname,lastname";
$s.=" LIMIT ".$jL1.",".$jL2;
$rs=mysql_query($s);
$r="";

if(mysql_num_rows($rs))
{
	$dd=explode("~",$_GET["d"]."tot~ipay~owe~pay");
	$ddd=array();
	$s=array();
	foreach($dd as $k => $v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
	$t=array("X","nam","tel","cel","ema","com","tot","ipay","owe","pay");
	$d="";
	$n=1;
	foreach($t as $k => $v)
	{
		if(isset($ddd[$v]))
		{
			$d.=$n;
			$n++;
		}
		$d.="~";
	}
	// calculate values
	function getInvoices($id)
	{
		$r="";
		$t=0;
		$p=0;
		$ro=0;
		$rp=0;

		$s="SELECT 
		DISTINCT j_invit_id
		,j_invit_total
		,j_invit_price
		,j_invit_discount
		,j_invit_paid
		,j_invcom_amount
		,j_invcom_percent
		,j_invcom_paid
		";
		// $s.="FROM nse_invoice_commission ";
		// $s.="LEFT JOIN nse_invoice_item ON j_invit_id=j_invcom_item ";
		$s.="FROM nse_invoice_item "; // testing
		$s.="LEFT JOIN nse_invoice_commission ON j_invit_id=j_invcom_item "; // testing
		$s.="LEFT JOIN nse_invoice_item_paid ON j_invit_id=j_invp_invoice_item ";
		$s.="LEFT JOIN nse_invoice ON j_invit_invoice=j_inv_id ";
		$s.="WHERE j_invcom_user=".$id." AND j_inv_cancel_user=0 AND j_inv_type<3 ";
		$q=mysql_query($s);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
			{
				$t+=$g["j_invit_total"];
				$p+=$g["j_invit_paid"];
				$ro+=$g["j_invcom_percent"]?($g["j_invit_price"]-$g["j_invit_discount"])*($g["j_invcom_percent"]/100):$g["j_invcom_amount"];
				$rp+=$g["j_invcom_paid"];
			}
		}
		return $t."~".$p."~".$ro."~".$rp."~";
	}

	while($g=mysql_fetch_array($rs))
	{
		$r.=$g["user_id"]."~";
		$r.=$g["firstname"]." ".$g["lastname"]."~";
		if(isset($s["tel"]))
			$r.=$g["phone"]."~";
		if(isset($s["cel"]))
			$r.=$g["cell"]."~";
		if(isset($s["ema"]))
			$r.=$g["email"]."~";
		if(isset($s["com"]))
			$r.=$g["rep_commission"]."~";
		$r.=getInvoices($g["user_id"]);
		$r.="|";
	}

	$r=str_replace("\n","",$r);
	$r=str_replace("\"","",$r);
	$r=str_replace("~|","|",$r);
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/reps_commission/f/rep_res.js></script>";
echo "<script>J_inv_r(\"".$r."\",\"".$d."\",".$jL0.",".$jL1.",".$jL2.")</script>";
?>