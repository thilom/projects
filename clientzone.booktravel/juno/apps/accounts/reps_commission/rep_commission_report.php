<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

include $SDR."/system/get.php";

$role=$_SESSION["j_user"]["role"];
$id=($role=="d"||$role=="b")&&isset($_GET["id"])&&$_GET["id"]?$_GET["id"]:$_SESSION["j_user"]["id"];

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/reps_commission/f/rep_commission_report.js></script>";
echo "<body style='margin:0'>";

$s="SELECT
DISTINCT j_invit_id
,j_invit_inventory
,j_invit_company
,j_invit_name
,j_invit_discount
,j_invit_total
,j_invit_paid
,j_inv_id
,j_inv_date
,j_inv_number
,j_inv_to_company
,j_invcom_id
,j_invcom_amount
,j_invcom_percent
,j_invcom_paid
,j_invcom_paid_date
,j_in_name
";
$s.="FROM nse_invoice_item "; // testing
$s.="LEFT JOIN nse_invoice_commission ON j_invit_id=j_invcom_item "; // testing
//$s.="FROM nse_invoice_commission ";
//$s.="LEFT JOIN nse_invoice_item ON j_invcom_item=j_invit_id ";
$s.="LEFT JOIN nse_invoice ON j_invit_invoice=j_inv_id ";
$s.="LEFT JOIN nse_invoice_item_paid ON j_invit_id=j_invp_invoice_item ";
$s.="LEFT JOIN nse_inventory ON j_invit_inventory=j_in_id ";
$s.="WHERE ";
$s.="j_inv_cancel_user=0 ";
$s.="AND j_inv_type<3 ";
//$s.="AND j_invcom_user=".$id." ";
$s.="ORDER BY j_inv_date DESC ";
$s.="LIMIT 0,100";
$q=mysql_query($s);
if(mysql_num_rows($q))
{

	function rYear()
	{
		global $i,$y,$yi,$yii,$iiv,$yd,$yt,$yp,$yro,$yrp;
		if($i)
		{
			echo "<tr onclick=ir(this) onmouseover=im(this)><td colspan=3 class=jdbWW>".$y." items: ".$yi;
			rsum($yii);
			echo "</td>";
			echo "<td class=jdbY>".number_format($yd,2,"."," ")."</td>";
			echo "<td class=jdbR>".number_format($yt,2,"."," ")."</td>";
			echo "<td class=jdbG>".number_format($yp,2,"."," ")."</td>";
			echo "<td class=jdbR>".number_format($yro,2,"."," ")."</td>";
			echo "<td class=jdbG>".number_format($yrp,2,"."," ")."</td>";
			echo "</tr>";
		}
		$yi=0;
		$yd=0;
		$yt=0;
		$yp=0;
		$yro=0;
		$yrp=0;
	}

	function raYear()
	{
		global $i,$y,$yy;
		if($i && $y-1!=$yy)
		{
			$yz=$y;
			while($yz>$yy)
			{
				echo "<tr><th colspan=9 class=jdbK><b>".$yz."</b></th></tr>";
				echo "<tr>";
				echo "<td colspan=3>Items: 0</td>";
				echo "<td class=jdbY>0.00</td>";
				echo "<td class=jdbR>0.00</td>";
				echo "<td class=jdbG>0.00</td>";
				echo "<td class=jdbR>0.00</td>";
				echo "<td class=jdbG>0.00</td>";
				echo "</tr>";
				$yz--;
			}
		}
	}

	function rMonth()
	{
		global $i,$y,$month,$m,$mi,$mii,$iiv,$md,$mt,$mp,$mro,$mrp;
		if($i)
		{
			echo "</tbody>";
			echo "<tr onclick=ir(this) onmouseover=im(this)><td colspan=3 class=jdbWW>".$month[$m]." items: ".$mi;
			rsum($mii);
			echo "</td>";
			echo "<td class=jdbY>".number_format($md,2,"."," ")."</td>";
			echo "<td class=jdbR>".number_format($mt,2,"."," ")."</td>";
			echo "<td class=jdbG>".number_format($mp,2,"."," ")."</td>";
			echo "<td class=jdbR>".number_format($mro,2,"."," ")."</td>";
			echo "<td class=jdbG>".number_format($mrp,2,"."," ")."</td>";
			echo "</tr>";
		}
		$mi=0;
		$md=0;
		$mt=0;
		$mp=0;
		$mro=0;
		$mrp=0;
	}

	function raMonth()
	{
		global $i,$month,$y,$m,$mm;
		if($i && $m-1!=$mm)
		{
			$yz=$y;
			$mz=$m-1;
			while($mz>$mm&&$yz>=$yy)
			{
				if($y!=$yz&&$yz>$yy)
					echo "<tr><th colspan=9 class=jdbK><b>".$yz."</b></th></tr>";
				echo "<tr class=jdbK><th colspan=9>".$month[$mz]."</th></tr>";
				echo "<tr>";
				echo "<td colspan=3 class=jdbK>".$month[$mz]." items: 0</td>";
				echo "<td class=jdbY>0.00</td>";
				echo "<td class=jdbR>0.00</td>";
				echo "<td class=jdbG>0.00</td>";
				echo "<td class=jdbR>0.00</td>";
				echo "<td class=jdbG>0.00</td>";
				echo "</tr>";
				if($y!=$yz&&$yz>$yy)
				{
					echo "<tr>";
					echo "<td colspan=3 class=jdbK>Items: 0</td>";
					echo "<td class=jdbY>0.00</td>";
					echo "<td class=jdbR>0.00</td>";
					echo "<td class=jdbG>0.00</td>";
					echo "<td class=jdbR>0.00</td>";
					echo "<td class=jdbG>0.00</td>";
					echo "</tr>";
				}
				$mz--;
				if($mz==0)
				{
					$yz--;
					$mz=12;
				}
			}
		}
	}

	function rsum($a)
	{
		global $iiv;
		if(count($a))
		{
			echo "<table id=ir class=ji>";
			$t=array();
			foreach($a as $k => $v)
				$t[$iiv[$k]]=array($v[0],number_format($v[1],2,"."," "),($v[2]?number_format($v[2],2,"."," "):""));
			ksort($t);
			foreach($t as $k => $v)
				echo "<tr><th class=jdbG>".$v[0]."</th><th>".$k."</th><th class=jdbR>".$v[1]."</th><th class=jdbG>".$v[2]."</th></tr>";
			echo "</table>";
			$a=array();
		}
	}

	$y=0;
	$m=0;
	$i=0;
	$ti=0;
	$yi=0;
	$mi=0;
	$tt=0;
	$yt=0;
	$mt=0;
	$tp=0;
	$yp=0;
	$mp=0;
	$td=0;
	$yd=0;
	$md=0;
	$tro=0;
	$yro=0;
	$mro=0;
	$trp=0;
	$yrp=0;
	$mrp=0;
	$tii=array();
	$yii=array();
	$mii=array();
	$vii=array();
	$cc=array();
	$month=array("-","January","February","March","April","May","June","July","August","September","October","November","December");
	echo "<table id=jTR>";

	while($g=mysql_fetch_array($q))
	{
		$yy=date("Y",$g["j_inv_date"]);
		$mm=date("m",$g["j_inv_date"])*1;
		if($m!=$mm)
		{
			//raMonth();
			rMonth();
			$m=$mm;
			$mc=1;
		}
		if($y!=$yy)
		{
			//raYear();
			rYear();
			if(!$i)
				$de=date("d/m/Y",$g["j_inv_date"]);
			$y=$yy;
			echo "<tr><th colspan=9 class=jdbW>".$y."</th></tr>";
		}
		if($mc)
		{
			echo "<tr onclick=io(this)><th colspan=9 class=jdbB>".$y." ".$month[$m]."</th></tr><tbody class=ji>";
			echo "<script>rH()</script>";
			$mc=0;
		}

		echo "<tr onclick=Rn(".$g["j_inv_id"].",".($g["j_invcom_id"]?$g["j_invcom_id"]:0).",'".$g["j_inv_to_company"]."')>";
		$ds=$g["j_inv_date"]?date("d/m/Y",$g["j_inv_date"]):"?";
		echo "<td class=jdbC>".$ds."</td>";
		$c=$g["j_invit_company"]?$g["j_invit_company"]:$g["j_inv_to_company"];
		if(!isset($cc[$c]))
			$cc[$c]=J_Value("","establishment","",$c);
		echo "<td>".$cc[$c]."</td>";
		if(!isset($iiv[$g["j_invit_inventory"]]))
			$iiv[$g["j_invit_inventory"]]=$g["j_in_name"]?$g["j_in_name"]:$g["j_invit_name"];
		echo "<th onmouseover=vn(this,'".$g["j_inv_number"]."')>".$iiv[$g["j_invit_inventory"]]."</th>";
		$d=$g["j_invit_discount"];
		echo "<td class=jdbY>".($d?number_format($d,2,"."," "):"")."</td>";
		$t=$g["j_invit_total"];
		echo "<td class=jdbR>".number_format($t,2,"."," ")."</td>";
		$p=($g["j_invit_paid"]?$g["j_invit_paid"]:0);
		echo "<td class=jdbG>".($p?number_format($p,2,"."," "):"")."</td>";
		$ro=$g["j_invcom_percent"]?($g["j_invit_price"]-$g["j_invit_discount"])*($g["j_invcom_percent"]/100):$g["j_invcom_amount"];
		echo "<td class=jdbR>".number_format($ro,2,"."," ")."</td>";
		$rp=$g["j_invcom_paid"];
		echo "<td class=jdbG>".($rp?number_format($rp,2,"."," "):"")."</td>";
		echo "<td>".($g["j_invcom_paid_date"]?date("d/m/Y",$g["j_invcom_paid_date"]):"")."</td>";
		echo "</tr>";

		$ti+=1;
		$yi+=1;
		$mi+=1;
		$td+=$d;
		$yd+=$d;
		$md+=$d;
		$tt+=$t;
		$yt+=$t;
		$mt+=$t;
		$tp+=$p;
		$yp+=$p;
		$mp+=$p;
		$tro+=$ro;
		$yro+=$ro;
		$mro+=$ro;
		$trp+=$rp;
		$yrp+=$rp;
		$mrp+=$rp;
		$ii=$g["j_invit_inventory"];

		if(!isset($tii[$ii]))
			$tii[$ii]=array(0,0,0);
		$tii[$ii][0]+=1;
		$tii[$ii][1]+=$t;
		$tii[$ii][2]+=$rp;
		if(!isset($yii[$ii]))
			$yii[$ii]=array(0,0,0);
		$yii[$ii][0]+=1;
		$yii[$ii][1]+=$t;
		$yii[$ii][2]+=$rp;
		if(!isset($mii[$ii]))
			$mii[$ii]=array(0,0,0);
		$mii[$ii][0]+=1;
		$mii[$ii][1]+=$t;
		$mii[$ii][2]+=$rp;
		if(!isset($iiv[$ii]))
			$iiv[$ii]=$g["j_in_name"];

		$i++;
	}
	rMonth();
	rYear();
	echo "</tbody>";
	echo "<tr><th colspan=9 class=jdbW>Total</th></tr>";
	echo "<tr onclick=ir(this) onmouseover=im(this)><td colspan=3 class=jdbWW>".$ds." - ".$de." items: ".$ti;
	rsum($tii);
	echo "</td>";
	echo "<td class=jdbY>".number_format($td,2,"."," ")."</td>";
	echo "<td class=jdbR>".number_format($tt,2,"."," ")."</td>";
	echo "<td class=jdbG>".number_format($tp,2,"."," ")."</td>";
	echo "<td class=jdbR>".number_format($tro,2,"."," ")."</td>";
	echo "<td class=jdbG>".number_format($trp,2,"."," ")."</td>";
	echo "</tr>";
	echo "</table>";
}

echo "<script>Jrr(".$id.",'".$role."')</script>";

$J_title1="Rep Commission Report";
$J_title1=J_Value("","people","",$id);
$J_title2="Rep Commission Report";
$J_title3=($i?$ds." - ".$de:"");
$J_icon="<img src=".$ROOT."/ico/set/accounts_statements.png><var><img src=".$ROOT."/ico/set/user_male.png></var>";
$J_home=1;
$J_width=1440;
$J_height=640;
$J_label=19;
$J_nostart=0;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>