<?php

/**
 * Draw an debt collection report
 *
 * @author Thilo Muller(2011)
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";

//Vars
$d = "x~code~name~d120~d90~d60~d30~curr~tot~{$_GET['d']}";
$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 100);
$r = '';
$search_data['company']= $_GET['company'];
$search_data['date']= $_GET['date'];
$search_data['type']= $_GET['type'];
$search_data['company']= $_GET['company'];
$search_data['min']= $_GET['min'];
$estab_count = 0;
$file = '';

// Display stuff
$dd = explode("~", trim($d, "~"));
$ddd = array();
$s = array();
foreach ($dd as $k => $v) {
	if (empty($v))
		continue;
	$s[$v] = 1;
	$ddd[$v] = 1;
}
$t = array("X", "code", "name", "d120","d90",'d60','d30','curr', 'tot','ass','rep','ent','out','vis','ren','tvl','conN','conT','conC','conE');
$da = "";
$n = 1;
foreach ($t as $k => $v) {
	if (isset($ddd[$v])) {
		$da.=$n;
		$n++;
	}
	$da.="~";
}


/* PUT STUFF HERE */

//Invoice Search Date
list($day, $month, $year) = explode('/', $search_data['date']);
$search_date = mktime(23, 59, 59, $month, $day, $year);

//Get list of establishment for which there are invoices
$statement = "SELECT DISTINCT a.j_inv_to_company, b.establishment_name
			FROM nse_invoice AS a
			LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code";
if (isset($_GET['company']) && !empty($_GET['company'])) $statement .= " WHERE a.j_inv_to_company = '{$_GET['company']}'";
$statement .= " ORDER BY b.establishment_name";
$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
$sql_establishments->execute();
$sql_establishments->bind_result($establishment_code, $establishment_name);
while ($sql_establishments->fetch()) {
	$establishment_list[$establishment_code]['name'] = $establishment_name;
	$establishment_list[$establishment_code]['d120'] = 0;
	$establishment_list[$establishment_code]['d90'] = 0;
	$establishment_list[$establishment_code]['d60'] = 0;
	$establishment_list[$establishment_code]['d30'] = 0;
	$establishment_list[$establishment_code]['current'] = 0;
	$establishment_list[$establishment_code]['total'] = 0;
}
$sql_establishments->close();

//Prepare statement - get invoices
$statement = "SELECT a.j_inv_id, a.j_inv_date, a.j_inv_total, a.j_inv_paid, j_inv_number
			FROM nse_invoice AS a
			WHERE j_inv_to_company=? AND j_inv_type=? AND a.j_inv_date<?";
if ($search_data['type'] == '1') {
	$statement .= " AND (j_inv_cancel_date = 0 OR j_inv_cancel_date IS NULL OR j_inv_cancel_date = '')";
}
$sql_invoice = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Prepare statement - Credit notes
$statement = "SELECT a. j_inv_reference, a.j_inv_total
			FROM nse_invoice AS a
			WHERE j_inv_to_company=? AND j_inv_type=3 AND a.j_inv_date<?";
$sql_credits = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Payments
$statement = "SELECT a.j_invp_invoice, a.j_invp_paid
			FROM nse_invoice_item_paid AS a
			LEFT JOIN nse_invoice AS b ON a.j_invp_invoice=b.j_inv_id
			WHERE a.j_invp_date<? AND b.j_inv_to_company=?";
$sql_payments = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);



//Calculate invoice data
foreach ($establishment_list as $establishment_code=>$data) {
	if ($establishment_code == 'O') continue;

//	echo "$establishment_code - {$establishment_list['RO6105']['d120']}<br>";

	//Payments
	$payments = array();
	$sql_payments->bind_param('ss', $search_date, $establishment_code);
	$sql_payments->execute();
	$sql_payments->store_result();
	$sql_payments->bind_result($payment_invoice_id, $invoice_payments);
	while ($sql_payments->fetch()) {
		if (isset($payments[$payment_invoice_id])) {
			$payments[$payment_invoice_id] += $invoice_payments;
		} else {
			$payments[$payment_invoice_id] = $invoice_payments;
		}
	}
	$sql_payments->free_result();

	//Credit Notes
	$credit_notes = array();
	$sql_credits->bind_param('ss', $establishment_code, $search_date);
	$sql_credits->execute();
	$sql_credits->bind_result($cr_reference, $cr_total);
	while ($sql_credits->fetch()) {
		$credit_notes[$cr_reference] = $cr_total;
	}
	$sql_credits->free_result();


	//Invoices
	$outstanding = 0;
	$sql_invoice->bind_param('sss', $establishment_code, $search_data['type'], $search_date);
	$sql_invoice->execute();
	$sql_invoice->bind_result($invoice_id, $invoice_date, $invoice_total, $invoice_paid, $invoice_number);
	while ($sql_invoice->fetch()) {
		$outstanding = $invoice_total;

		if (isset($credit_notes[$invoice_number])) $outstanding += $credit_notes[$invoice_number];
		if (isset($payments[$invoice_id])) $outstanding -= $payments[$invoice_id];

		$invoice_age = intval(($search_date-$invoice_date)/86400);
		if ($invoice_age < 30) {
			$establishment_list[$establishment_code]['current'] += $outstanding;
		} else if ($invoice_age<60) {
			$establishment_list[$establishment_code]['d30'] += $outstanding;
		} else if ($invoice_age<90) {
			$establishment_list[$establishment_code]['d60'] += $outstanding;
		} else if ($invoice_age<120) {
			$establishment_list[$establishment_code]['d90'] += $outstanding;
		} else {
			$establishment_list[$establishment_code]['d120'] += $outstanding;
		}


		$establishment_list[$establishment_code]['total'] += $outstanding;

		//Format number
		$establishment_list[$establishment_code]['current'] = number_format($establishment_list[$establishment_code]['current'],2,'.','');
		$establishment_list[$establishment_code]['d30'] = number_format($establishment_list[$establishment_code]['d30'],2,'.','');
		$establishment_list[$establishment_code]['d60'] = number_format($establishment_list[$establishment_code]['d60'],2,'.','');
		$establishment_list[$establishment_code]['d90'] = number_format($establishment_list[$establishment_code]['d90'],2,'.','');
		$establishment_list[$establishment_code]['d120'] = number_format($establishment_list[$establishment_code]['d120'],2,'.','');
		$establishment_list[$establishment_code]['total'] = number_format($establishment_list[$establishment_code]['total'],2,'.','');

	}
	$sql_invoice->free_result();
}

//Remove establishment with a balance of less than min
foreach ($establishment_list as $establishment_code=>$data) {
	if ($data['total'] < $search_data['min']) unset($establishment_list[$establishment_code]);
}

//Prepare statement - Asserror & Rep
$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), CONCAT(c.firstname, ' ', c.lastname), DATE_FORMAT(enter_date, '%d %M %Y')
			FROM nse_establishment AS a
			LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
			LEFT JOIN nse_user AS c ON a.rep_id1=c.user_id
			WHERE a.establishment_code=?
			LIMIT 1";
$sql_users = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - QA Out
$statement = "SELECT IF(cancelled_date='0000-00-00','', DATE_FORMAT(cancelled_date, '%d %M %Y'))
			FROM nse_establishment_qa_cancelled
			WHERE establishment_code = ?
			LIMIT 1";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - TV
$statement = "SELECT tv_licence
			FROM nse_establishment_data
			WHERE establishment_code = ?
			LIMIT 1";
$sql_tv = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Contact
$statement = "SELECT CONCAT(b.firstname, ' ' , b.lastname), b.phone, b.cell, b.email, a.accounts_contact
			FROM nse_user_establishments AS a
			LEFT JOIN nse_user AS b ON a.user_id=b.user_id
			WHERE a.establishment_code=?
			ORDER BY a.accounts_contact DESC
			LIMIT 1";
$sql_contact = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Assessments
$statement = "SELECT DATE_FORMAT(assessment_date, '%d %M %Y'), DATE_FORMAT(renewal_date, '%d %M %Y')
			FROM nse_establishment_assessment
			WHERE establishment_code=?
			ORDER BY assessment_date DESC
			LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Assemble for JS
$counter = 0;
$file_counter = 0;
foreach ($establishment_list as $establishment_code=>$data) {

	//Vars
	$assessor = '';
	$rep = '';
	$qa_out = '';
	$enter_date = '';
	$contact_tel = '';
	$contact_name = '';
	$contact_cell = '';
	$contact_email = '';
	$accounts_contact = '';
	$assessment_date = '';
	$renewal_date = '';

	//Assessor & Rep
	$sql_users->bind_param('s', $establishment_code);
	$sql_users->execute();
	$sql_users->bind_result($assessor, $rep, $enter_date);
	$sql_users->fetch();
	$sql_users->free_result();

	//QA Out
	$sql_qa_out->bind_param('s', $establishment_code);
	$sql_qa_out->execute();
	$sql_qa_out->bind_result($qa_out);
	$sql_qa_out->fetch();
	$sql_qa_out->free_result();

	//TV
	$sql_tv->bind_param('s', $establishment_code);
	$sql_tv->execute();
	$sql_tv->bind_result($tv_licence);
	$sql_tv->fetch();
	$sql_tv->free_result();

	//Contact Details
	$sql_contact->bind_param('s', $establishment_code);
	$sql_contact->execute();
	$sql_contact->bind_result($contact_name, $contact_tel, $contact_cell, $contact_email, $accounts_contact);
	$sql_contact->fetch();
	$sql_contact->free_result();

	//Assessments
	$sql_assessment->bind_param('s', $establishment_code);
	$sql_assessment->execute();
	$sql_assessment->bind_result($assessment_date, $renewal_date);
	$sql_assessment->fetch();
	$sql_assessment->free_result();

	$r .= "$counter~$establishment_code~{$data['name']}~{$data['d120']}~{$data['d90']}~{$data['d60']}~{$data['d30']}~{$data['current']}~{$data['total']}~";
	if (isset($s['ass'])) $r .= "$assessor~";
	if (isset($s['rep'])) $r .= "$rep~";
	if (isset($s['ent'])) $r .= "$enter_date~";
	if (isset($s['out'])) $r .= "$qa_out~";
	if (isset($s['vis'])) $r .= "$assessment_date~";
	if (isset($s['ren'])) $r .= "$renewal_date~";
	if (isset($s['tvl'])) $r .= "$tv_licence~";
	if (isset($s['conN'])) {
		if ($accounts_contact == 'Y') {
			$r .= "<img src='/juno/ico/set/money_dollar.png' width=10px>";
		}
		$r .= "$contact_name~";
	}
	if (isset($s['conT'])) $r .= "$contact_tel~";
	if (isset($s['conC'])) $r .= "$contact_cell~";
	if (isset($s['conE'])) $r .= "$contact_email~";

	$r .= "|";
	//CSV
	if ($counter == 0) {

		if (isset($s['code'])) $headers[1] = 'Code';
		if (isset($s['name'])) $headers[2] = 'Establishment Name';
		if (isset($s['d120'])) $headers[3] = '+120 Days';
		if (isset($s['d90'])) $headers[4] = '90 Days';
		if (isset($s['d60'])) $headers[5] = '60 Days';
		if (isset($s['d30'])) $headers[6] = '30 Days';
		if (isset($s['curr'])) $headers[7] = 'Current';
		if (isset($s['tot'])) $headers[8] = 'Total Due';
		if (isset($s['ass'])) $headers[9] = 'Assessor';
		if (isset($s['rep'])) $headers[10] = 'Rep';
		if (isset($s['ent'])) $headers[11] = 'Enter Date';
		if (isset($s['out'])) $headers[12] = 'QA Out';
		if (isset($s['vis'])) $headers[13] = 'Last Visit';
		if (isset($s['ren'])) $headers[14] = 'Renewal Date';
		if (isset($s['tvl'])) $headers[15] = 'TV';
		if (isset($s['conN'])) $headers[16] = 'Contact Name';
		if (isset($s['conT'])) $headers[17] = 'Contact Tel';
		if (isset($s['conC'])) $headers[18] = 'Contact Cell';
		if (isset($s['conE'])) $headers[19] = 'Contact Email';

		$fh = fopen($SDR."/apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv", 'w');
		fputcsv($fh, $headers);
		$file = "/juno/apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv";
        }

	$csv_data = array();
	if (isset($s['code'])) $csv_data[1] = $establishment_code;
	if (isset($s['name'])) $csv_data[2] = $data['name'];
	if (isset($s['d120'])) $csv_data[3] = $data['d120'];
	if (isset($s['d90'])) $csv_data[4] = $data['d90'];
	if (isset($s['d60'])) $csv_data[5] = $data['d60'];
	if (isset($s['d30'])) $csv_data[6] = $data['d30'];
	if (isset($s['curr'])) $csv_data[7] = $data['current'];
	if (isset($s['tot'])) $csv_data[8] = $data['total'];
	if (isset($s['ass'])) $csv_data[9] = "$assessor";
	if (isset($s['rep'])) $csv_data[10] = "$rep";
	if (isset($s['ent'])) $csv_data[11] = "$enter_date";
	if (isset($s['out'])) $csv_data[12] = "$qa_out";
	if (isset($s['vis'])) $csv_data[13] = "$assessment_date";
	if (isset($s['ren'])) $csv_data[14] = "$renewal_date";
	if (isset($s['tvl'])) $csv_data[15] = "$tv_licence";
	if (isset($s['conN'])) $csv_data[16] = "$contact_name";
	if (isset($s['conT'])) $csv_data[17] = "$contact_tel";
	if (isset($s['conC'])) $csv_data[18] = "$contact_cell";
	if (isset($s['conE'])) $csv_data[19] = "$contact_email";

        fputcsv($fh, $csv_data);
	$counter++;
}


$estab_count = count($establishment_list);

$r = str_replace("\r", "", $r);
$r = str_replace("\n", "", $r);
$r = str_replace("\"", "", $r);
$r = str_replace("~|", "|", $r);
//echo $r;
//print_r(implode(',',$list));

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/jsscripts/list_item_remover.js></script>";
echo "<script src=" . $ROOT . "/apps/accounts/debt_collection/debt_res.js></script>";

if ($jL2 == 0) {
    $page = "<body style='color:#999;padding:30 40 40 160;background:url(/juno/ico/set/spreadsheet.png) no-repeat 40px 40px'><tt style=width:260><hr>CSV Files Created. Right click to save.<ul>$files</ul></tt></body></html>";
    echo $page;
} else {
	$msg = 'Debt Collection Report for ' . ($search_data['type']==2?'Tax Invoices':'Pro-formas') . ' Ending ' . date('d M Y', $search_date);
	echo "<script>J_in_r(\"$r\",\"$da\",$estab_count,$jL1,$jL2,'$file','$msg')</script>";
}





?>