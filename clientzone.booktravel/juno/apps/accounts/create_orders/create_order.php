<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/secure.php";
include $SDR . "/system/get.php";

if (isset($establishment_code)) {
	$eid = $establishment_code;
} else {
	$eid = isset($_GET["eid"]) ? $_GET["eid"]:0;
}

if (count($_POST)) {
	$eid = $_POST["co"];
	$info = isset($_POST["info"]) ? $_POST["info"] : "";
	$comments = isset($_POST["comments"]) ? $_POST["comments"] : "";
	$assessor = isset($_POST["assessor"]) ? $_POST["assessor"] : "";
	$rep = isset($_POST["rep"]) ? $_POST["rep"] : "";
	$rep2 = isset($_POST["rep2"]) ? $_POST["rep2"] : "";

	include $SDR . "/apps/accounts/create_orders/f/create_inc.php";

	if ($_POST["aft"] == 0)
		echo "<script>window.parent.J_WX(self)</script>";
	else if ($_POST["aft"] == 1)
		echo "<script>w=window.parent;w.J_W('" . $ROOT . "/apps/accounts/invoices/invoice_view.php?id=" . $order_id . "');w.J_W('" . $ROOT . "/apps/accounts/invoices/invoice_view.php?id=" . $invoice_id . "');w.J_WX(self)</script>";
}


//Get list of companies
$entity_list = '';
$statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE aa_entity='Y'";
$sql_company = $GLOBALS['dbCon']->prepare($statement);
$sql_company->execute();
$sql_company->store_result();
$sql_company->bind_result($entity_code, $entity_name);

while($sql_company->fetch()) {
	$entity_list .= "<OPTION value='$entity_code'>$entity_name";
}

$sql_company->free_result();
$sql_company->close();

$inventory = isset($inventory) ? $inventory : (isset($_GET["inventory"]) ? $_GET["inventory"] : 0);

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/accounts/create_orders/f/create.js></script>";
echo "<tr><th>AA Entity</th><td><SELECT name=aa_entity onChange='updateProducts(this.value)'>$entity_list</SELECT></td></tr>";
echo "<tr><th>Company</th><td><input type=text value=\"" . ($eid ? J_Value("", "establishment", "", $eid) : "") . "\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=establishment',200,4,9) onclick=J_ajax_C(this) onmouseover=J_TT(this,o_ee) class=o_b style=background:#F8DCDC><input type=hidden name=co value=" . $eid . "></td></tr>";
echo "<tr><th>From</th><td><input type=text value=\"" . J_Value("", "people", "", $_SESSION["j_user"]["id"]) . "\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email',320,4) onclick=J_ajax_C(this) onmouseover=J_TT(this,o_pp) class=o_b><input type=hidden name=from value=" . $_SESSION["j_user"]["id"] . "></td></tr>";
echo "<tr><th>Assessor</th><td><input type=text value=\"" . ($_SESSION["j_user"]["role"] == "a" ? J_Value("", "people", "", $_SESSION["j_user"]["id"]) : "") . "\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=a',320,4) onclick=J_ajax_C(this) onmouseover=J_TT(this,o_pp)><input type=hidden name=ass value=" . ($_SESSION["j_user"]["role"] == "a" ? $_SESSION["j_user"]["id"] : 0) . "></td></tr>";
echo "<tr><th>Rep</th><td><input type=text value=\"" . ($_SESSION["j_user"]["role"] == "r" ? J_Value("", "people", "", $_SESSION["j_user"]["id"]) : "") . "\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=r',320,4) onclick=J_ajax_C(this) onmouseover=J_TT(this,o_pp)><input type=hidden name=rep value=" . ($_SESSION["j_user"]["role"] == "r" ? $_SESSION["j_user"]["id"] : 0) . "></td></tr>";
echo "<tr><th>Rep2</th><td><input type=text value=\"\" onmouseover=J_TT(this,o_pp)><input type=hidden name=rep2 value=0></td></tr>";
echo "<tr><th>Info</th><td><textarea name=info class=jW100></textarea></td></tr>";
echo "<tr><th id=o_it colspan=2 style=background:#F8DCDC>";
echo "<br><tt><b>Inventory Items</b></tt><div id=inv_items>";

$i = 0;

if ($inventory)
{
	function inVnt($d)
	{
		global $i;
		$v = "";
		if ($d)
			{
			$q = "SELECT * FROM nse_inventory WHERE j_in_id=" . $d . " LIMIT 1";
			$q = mysql_query($q);
			if (mysql_num_rows($q))
			{
				$g = mysql_fetch_array($q);
				$v = "<tt><input type=checkbox name=i_" . $i . " value=" . $d . "> " . $g["j_in_name"] . "</tt>";
			}
		}
		return $v;
	}

	$inventory = explode("~", $inventory);

	foreach ($inventory as $k => $v)
		echo inVnt($v);
} else {

	if(isset($_GET["no_ads"]))
		$q = "SELECT * FROM nse_inventory WHERE j_in_category>4 ORDER BY j_in_category,j_in_year DESC,j_in_name";
	else
		$q = "SELECT * FROM nse_inventory ORDER BY j_in_category,j_in_year DESC,j_in_name";
	$q = mysql_query($q);
	if (mysql_num_rows($q)) {
		while ($g = mysql_fetch_array($q)) {
			echo "<tt><input type=checkbox name=i_" . $i . " value=" . $g["j_in_id"] . "> " . $g["j_in_name"] . "</tt>";
			$i++;
		}
	}
}

echo "</div><br></th></tr>";
echo "<tr><th>Comment</th><td><textarea name=comm class=jW100></textarea></td></tr>";
echo "<script>O_tail()</script>";

$J_home = 9;
$J_title1 = "Create an Order";
$J_icon = "<img src=" . $ROOT . "/ico/set/spreadsheet_yellow.png><var><img src=" . $ROOT . "/ico/set/add.png></var>";
$J_label = 21;
$J_width = 480;
include $SDR . "/system/deploy.php";
echo "</body></html>";
?>