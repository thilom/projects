<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/dashboard/dashboard.js></script>";

if(isset($_GET["s"]))
{
	echo "<body><script>J_inv_s()</script></body></html>";
	die();
}

echo "<body class=j_List>";

echo "<script>J_inv_N(";
echo "\"".date('d/m/Y',mktime(0,0,0,date("m")+1,date(1)-1,date("Y")))."\"";
echo ")</script>";

$J_home=0;
$J_title1="Accounts Dashboard";
$J_icon="<img src=".$ROOT."/ico/set/sys.png>";
$J_label=19;
$J_width=1440;
$J_framesize=240;
$J_frame1=$ROOT."/apps/accounts/dashboard.php?s=1";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>