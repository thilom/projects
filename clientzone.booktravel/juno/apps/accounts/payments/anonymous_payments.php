<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

// get invoice items
if(isset($_GET["inv"]))
{
	$i="";
	$o="";
	$c="";
	$cn="";
	$q=mysql_query("SELECT j_inv_id,j_inv_to_company FROM nse_invoice WHERE j_inv_type<3 AND j_inv_number='".$_GET["inv"]."' LIMIT 1");
	if(mysql_num_rows($q))
	{
		$g=mysql_fetch_array($q);
		$i=$g["j_inv_id"];
		$c=$g["j_inv_to_company"];
		$cn=($c?J_Value("","establishment","",$c):"");
		$q=mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=".$g["j_inv_id"]);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
				$o.=$g["j_invit_id"]."~".$g["j_invit_name"]."~".$g["j_invit_total"]."|";
		}
	}
	die("<script>window.parent.J_payRet(".($i?"\"".$i."\"":0).",".($c?"\"".$c."\"":0).",".($cn?"\"".$cn."\"":0).",".($o?"\"".$o."\"":0).")</script>");
}





$id=isset($_POST["id"])?$_POST["id"]:(isset($_GET["id"])?$_GET["id"]:0);
$pay=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_item_paid WHERE j_invp_id=".$id." LIMIT 1"));
$inv=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice JOIN nse_invoice_item ON j_inv_id=j_invit_invoice WHERE j_inv_id=".($pay["j_invp_invoice"]?$pay["j_invp_invoice"]:0)." LIMIT 1"));
$est=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment JOIN nse_invoice ON establishment_code=j_inv_to_company WHERE j_inv_id=".($pay["j_invp_invoice"]?$pay["j_invp_invoice"]:0)." LIMIT 1"));

include_once $SDR."/system/activity.php";

if(isset($_GET["delete"]))
{
	include_once $SDR."/apps/notes/f/insert.php";
	mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_id=".$id);
	// correct invoice paid
	if($pay["j_invp_invoice"])
	{
		if($pay["j_invp_unknown_payment"])
			mysql_query("DELETE FROM nse_invoice WHERE j_invp_id=".$pay["j_invp_invoice"]);
		else
		{
			$q=mysql_query("SELECT j_inv_total FROM nse_invoice WHERE j_inv_id=".$pay["j_invp_invoice"]." LIMIT 1");
			if(mysql_num_rows($q))
			{
				$g=mysql_fetch_array($q);
				mysql_query("UPDATE nse_invoice SET j_inv_total_paid='".($g["j_inv_total"]-$pay["j_invp_paid"])."' WHERE j_inv_id=".$pay["j_invp_invoice"]);
			}
		}
	}
	// correct invoice item
	if($pay["j_invp_invoice_item"])
	{
		if($pay["j_invp_unknown_payment"])
			mysql_query("DELETE FROM nse_invoice_item WHERE j_invp_id=".$pay["j_invp_invoice_item"]);
		else
		{
			$q=mysql_query("SELECT j_invit_paid FROM nse_invoice_item WHERE j_invit_id=".$pay["j_invp_invoice_item"]." LIMIT 1");
			if(mysql_num_rows($q))
			{
				$g=mysql_fetch_array($q);
				mysql_query("UPDATE nse_invoice_item SET j_invit_total_paid='".($g["j_invit_paid"]-$pay["j_invp_paid"])."' WHERE j_invit_id=".$pay["j_invp_invoice_item"]);
			}
		}
	}
	// notes
	J_act("Payment",3,"Anonymous Payment Deleted | Bank: ".$pay["j_invp_bank"].($pay["j_invp_reference"]?" (Ref: ".$pay["j_invp_reference"].")":""),$id,$pay["j_invp_unknown_payment_company"]);
	if($pay["j_invp_unknown_payment_company"])
		insertNote($pay["j_invp_unknown_payment_company"],2,"INV".($pay["j_invp_invoice"]?$pay["j_invp_invoice"]:"PAY".$id),"Anonymous Payment Deleted | ".$pay["j_invp_paid"].($pay["j_invp_reference"]?" (Ref: ".$pay["j_invp_reference"].")":""));
	die("<script>alert('Unknown payment deleted (id: ".$id.")');window.parent.J_WX(self)</script>");
}

// update or insert
elseif(count($_POST))
{
	include $SDR."/system/parse.php";
	include_once $SDR."/apps/notes/f/insert.php";

	if($id)
	{
		$t_invoice=$pay["j_invp_invoice"];
		$t_item=$pay["j_invp_invoice_item"];
		$t_unknown=$pay["j_invp_unknown_payment"];

		if($_POST["invoice_number"])
		{
			$q=mysql_query("SELECT j_inv_id FROM nse_invoice WHERE j_inv_number='".$_POST["invoice_number"]."' LIMIT 1");
			if(mysql_num_rows($q))
			{
				$g=mysql_fetch_array($q);
				$t_invoice=$g["j_inv_id"];
				if($_POST["j_invp_invoice_item"] && $pay["j_invp_unknown_payment"]) // unlink dummy invoice
				{
					$t_item=$_POST["j_invp_invoice_item"];
					$_POST["j_invp_unknown_payment"]=0;
					mysql_query("DELETE FROM nse_invoice WHERE j_inv_id=".$pay["j_invp_invoice"]);
					mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_invoice=".$pay["j_invp_invoice"]);
				}
			}
		}
		$_POST["j_invp_invoice"]=$t_invoice;
		$_POST["j_invp_invoice_item"]=$t_item;
		$_POST["j_invp_unknown_payment"]=$t_unknown;
	}
	else
	{
		$_POST["j_invp_invoice"]=0;
		$_POST["j_invp_invoice_item"]=0;
	}

	if(!$_POST["invoice_number"] && !$pay["j_invp_invoice"])
	{
		include_once $SDR."/apps/accounts/invoices/default.php";
		$no_js=1;
		$co=$_POST["j_invp_unknown_payment_company"];
		include_once $SDR."/custom/lib/get_est_people.php";

		$_POST["j_invp_date"]=J_dateParse($_POST["j_invp_date"]);

		$s="INSERT INTO nse_invoice
		(
		j_inv_type
		,j_inv_head
		,j_inv_date
		,j_inv_number
		,j_inv_increment_id
		,j_inv_reference
		,j_inv_from_person
		,j_inv_from_assessor
		,j_inv_from_rep
		,j_inv_from_contact
		,j_inv_to_person
		,j_inv_to_contact
		,j_inv_to_company
		,j_inv_currency
		,j_inv_vat
		,j_inv_paid
		,j_inv_comment
		,j_inv_condition
		,j_inv_bank
		,j_inv_foot
		,j_inv_creator
		,j_inv_created
		) VALUES (
		2
		,2
		,".$_POST["j_invp_date"]."
		,'Unknown Payment'
		,".$invoice_default_increment."
		,'Unknown Payment'
		,".$_SESSION["j_user"]["id"]."
		,".($est["assessor_id"]?$est["assessor_id"]:0)."
		,".($est["rep_id1"]?$est["rep_id1"]:0)."
		,".$invoice_default_from_contact."
		,".($primary_person?$primary_person:0)."
		,".($accounts?$accounts:0)."
		,'".$_POST["j_invp_unknown_payment_company"]."'
		,'".$invoice_default_currency."'
		,'".$invoice_default_vat."'
		,'".$_POST["j_invp_paid"]."'
		,'This invoice was generated via an unknown payment'
		,".$invoice_default_condition."
		,".$invoice_default_bank."
		,".$invoice_default_foot."
		,".$_SESSION["j_user"]["id"]."
		,".$EPOCH."
		)";
		mysql_query($s);
		echo $s;
		$pay["j_invp_invoice"]=J_ID("nse_invoice","j_inv_id");
		mysql_query("INSERT INTO nse_invoice_item (j_invp_invoice,j_invp_inventory,j_invp_paid,j_invp_created) VALUES (".$pay["j_invp_invoice"].",164,".$_POST["j_invp_paid"].",".$EPOCH.")");
		$pay["j_invp_invoice_item"]=J_ID("nse_invoice_item","j_invit_id");
		$inv=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice JOIN nse_invoice_item ON j_inv_id=j_invit_invoice WHERE j_inv_id=".$pay["j_invp_invoice_item"]." LIMIT 1"));
		$est=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment JOIN nse_invoice ON establishment_code=j_inv_to_company WHERE j_inv_id=".$pay["j_invp_invoice"]." LIMIT 1"));
		$_POST["j_invp_invoice"]=$pay["j_invp_invoice"];
		$_POST["j_invp_invoice_item"]=$pay["j_invp_invoice_item"];
	}

	function strp($v="",$c=0)
	{
		$v=ucfirst(preg_replace("~[^a-zA-Z0-9 !@#_+=&%';:,<>\?\(\)\[\]\*\$\-\.]~","",str_replace("\n","<br>",str_replace(array("\t","\r"),"",$v))));
		if($c)
			$v=ucwords(strtolower($v));
		return addslashes($v);
	}
	unset($_POST["id"]);
	$_POST["j_invp_unknown_payment_company"]=($_POST["j_invp_unknown_payment_company"]?$_POST["j_invp_unknown_payment_company"]:"");
	$_POST["invoice_number"]=isset($_POST["invoice_number"])?strp($_POST["invoice_number"]):"";
	$_POST["j_invp_date"]=J_dateParse($_POST["j_invp_date"]);
	$_POST["j_invp_bank"]=strp($_POST["j_invp_bank"],1);
	$_POST["j_invp_reference"]=strp($_POST["j_invp_reference"]);
	$_POST["j_invp_comment"]=strp($_POST["j_invp_comment"]);
	$_POST["j_invp_invoice_item"]=isset($_POST["j_invp_invoice_item"])?$_POST["j_invp_invoice_item"]:0;

	// update payment on invoice if changes
	if($id)
	{
		// update previous invoice
		if($inv["j_inv_id"])
		{
			mysql_query("UPDATE nse_invoice SET j_inv_to_company='".$_POST["j_invp_unknown_payment_company"]."',j_invp_date=".$_POST["j_invp_date"]." WHERE j_inv_id=".$_POST["j_invp_invoice"]);
			mysql_query("UPDATE nse_invoice_item SET j_invit_company='".$_POST["j_invp_unknown_payment_company"]."',j_invit_date=".$_POST["j_invp_date"]." WHERE j_inv_id=".$_POST["j_invp_invoice"]);
		}
		// update previous invoice payments
		if($inv["j_inv_id"] && ($pay["j_invp_paid"]!=$_POST["j_invp_paid"]))
		{
			if($pay["j_invp_unknown_payment"])
				mysql_query("UPDATE nse_invoice SET j_inv_paid='".$pay["j_invp_paid"]."' WHERE j_inv_id=".$inv["j_inv_id"]);
			else
				mysql_query("UPDATE nse_invoice SET j_inv_type=2,j_inv_head=2,j_inv_paid='".($inv["j_inv_paid"]-$pay["j_invp_paid"])."' WHERE j_inv_id=".$inv["j_inv_id"]);
		}
		// update current invoice payment
		if($_POST["j_invp_invoice"] && ($pay["j_invp_paid"]!=$_POST["j_invp_paid"]))
		{
			mysql_query("UPDATE nse_invoice SET j_inv_type=2,j_inv_head=2,j_inv_paid='".($inv["j_inv_paid"]+$_POST["j_invp_paid"])."' WHERE j_inv_id=".$_POST["j_invp_invoice"]);
			if($pay["j_invp_invoice"]!=$_POST["j_invp_invoice"])
				insertNote($_POST["j_invp_unknown_payment_company"],2,"INV".$_POST["j_invp_invoice"],"Anonymous Payment | ".$_POST["j_invp_paid"].($_POST["j_invp_reference"]?" (Ref: ".$_POST["j_invp_reference"].")":""));
		}
		// update invoice item payments
		if($pay["j_invp_invoice_item"] && ($pay["j_invp_invoice_item"]!=$_POST["j_invp_invoice_item"] || ($pay["j_invp_paid"]!=$_POST["j_invp_paid"])))
		{
			if($pay["j_invp_unknown_payment"])
				mysql_query("UPDATE nse_invoice_item SET j_invit_paid='".($inv["j_invit_paid"]-$pay["j_invp_paid"])."' WHERE j_invit_id=".$pay["j_invp_invoice_item"]);
			else // update current invoice item
				mysql_query("UPDATE nse_invoice_item SET j_inv_paid='".$_POST["j_invp_paid"]."' WHERE j_invit_id=".$_POST["j_invp_invoice_item"]);
		}

		$q="UPDATE nse_invoice_item_paid SET
		j_invp_unknown_payment_company='".$_POST["j_invp_unknown_payment_company"]."'
		,j_invp_invoice='".$_POST["j_invp_invoice"]."'
		,j_invp_invoice_item='".$_POST["j_invp_invoice_item"]."'
		,j_invp_date='".$_POST["j_invp_date"]."'
		,j_invp_bank='".$_POST["j_invp_bank"]."'
		,j_invp_type='".$_POST["j_invp_type"]."'
		,j_invp_reference='".$_POST["j_invp_reference"]."'
		,j_invp_paid='".($_POST["j_invp_paid"]*1)."'
		,j_invp_comment='".$_POST["j_invp_comment"]."'
		,j_invp_unknown_payment=".$_POST["j_invp_unknown_payment"]."
		WHERE j_invp_id=".$id;
		mysql_query($q);
		if(mysql_error())
			echo mysql_error()."<tt>".$q."</tt><hr>";
		J_act("Payment",4,"Anonymous Payment | Bank: ".$_POST["j_invp_bank"].($_POST["j_invp_reference"]?" (Ref: ".$_POST["j_invp_reference"].")":""),$id,$_POST["j_invp_unknown_payment_company"]);
		if($_POST["j_invp_unknown_payment_company"] && ($pay["j_invp_unknown_payment_company"]!=$_POST["j_invp_unknown_payment_company"] || $pay["j_invp_invoice"]!=$_POST["j_invp_invoice"]))
			insertNote($_POST["j_invp_unknown_payment_company"],2,"INV".($_POST["j_invp_invoice"]?$_POST["j_invp_invoice"]:"PAY".$id),"Anonymous Payment | ".$_POST["j_invp_paid"].($_POST["j_invp_reference"]?" (Ref: ".$_POST["j_invp_reference"].")":""));
	}
	else
	{
		$q="INSERT INTO nse_invoice_item_paid
		(
		j_invp_invoice
		,j_invp_invoice_item
		,j_invp_unknown_payment_company
		,j_invp_date
		,j_invp_bank
		,j_invp_type
		,j_invp_reference
		,j_invp_paid
		,j_invp_comment
		,j_invp_account
		,j_invp_unknown_payment
		,j_invp_created
		) VALUES (
		'".$_POST["j_invp_invoice"]."'
		,'".$_POST["j_invp_invoice_item"]."'
		,'".$_POST["j_invp_unknown_payment_company"]."'
		,'".$_POST["j_invp_date"]."'
		,'".$_POST["j_invp_bank"]."'
		,'".$_POST["j_invp_type"]."'
		,'".$_POST["j_invp_reference"]."'
		,'".($_POST["j_invp_paid"]*1)."'
		,'".$_POST["j_invp_comment"]."'
		,".$_POST["j_invp_account"]."
		,1
		,".$EPOCH."
		)";
		mysql_query($q);
		if(mysql_error())
			echo mysql_error()."<tt>".$q."</tt><hr>";
		$id=J_ID("nse_invoice_item_paid","j_invp_id");
		J_act("Payment",3,"Anonymous Payment | Bank: ".$_POST["j_invp_bank"].($_POST["j_invp_reference"]?" (Ref: ".$_POST["j_invp_reference"].")":""),$id,$_POST["j_invp_unknown_payment_company"]);
		if($_POST["j_invp_unknown_payment_company"])
			insertNote($_POST["j_invp_unknown_payment_company"],2,"INV".($_POST["j_invp_invoice"]?$_POST["j_invp_invoice"]:"PAY".$id),"Anonymous Payment | ".$_POST["j_invp_paid"].($_POST["j_invp_reference"]?" (Ref: ".$_POST["j_invp_reference"].")":""));
		$pay=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_item_paid WHERE j_invp_id=".$id." LIMIT 1"));
	}
die();
	die("<script>window.parent.J_WX(self)</script>");
}



echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/payments/anonymous_payments.js></script>";

// get aa accounts
$acc="";
$qa=mysql_query("SELECT * FROM nse_invoice_account ORDER BY j_invac_name");
if(mysql_num_rows($qa))
{
	while($ga=mysql_fetch_array($qa))
		$acc.=$ga["j_invac_id"]."~".$ga["j_invac_name"]."|";
}

echo "<script>J_apay(";
echo "\"".str_replace("\"","'",$acc)."\"";
if($id)
{
	$invNo="";
	$items="";
	if($pay["j_invp_invoice"] && $pay["j_invp_unknown_payment"]==0)
	{
		$q=mysql_query("SELECT j_inv_number,j_inv_to_company FROM nse_invoice WHERE j_inv_id=".$pay["j_invp_invoice"]." LIMIT 1");
		if(mysql_num_rows($q))
		{
			$g=mysql_fetch_array($q);
			$invNo=$g["j_inv_number"];
			// update anon company if invoice has company
			if(!$pay["j_invp_unknown_payment_company"] && $g["j_inv_to_company"])
			{
				mysql_query("UPDATE nse_invoice_item_paid SET j_invp_unknown_payment_company='".$g["j_inv_to_company"]."' WHERE j_invp_id=".$id);
				$pay["j_invp_unknown_payment_company"]=$g["j_inv_to_company"];
			}
			// get invoice items
			$q=mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=".$pay["j_invp_invoice"]);
			if(mysql_num_rows($q))
			{
				while($g=mysql_fetch_array($q))
					$items.=$g["j_invit_id"]."~".$g["j_invit_name"]."~".$g["j_invit_total"]."|";
			}
		}
	}
	echo ",".$id;
	echo ",\"";
	echo $invNo."~";
	echo $pay["j_invp_invoice_item"]."~";
	echo $pay["j_invp_unknown_payment_company"]."~";
	echo ($pay["j_invp_unknown_payment_company"]?J_Value("","establishment","",$pay["j_invp_unknown_payment_company"]):"")."~";
	echo date("d/m/Y",$pay["j_invp_date"])."~";
	echo stripslashes($pay["j_invp_bank"])."~";
	echo $pay["j_invp_type"]."~";
	echo stripslashes($pay["j_invp_reference"])."~";
	echo $pay["j_invp_paid"]."~";
	echo stripslashes($pay["j_invp_comment"])."~";
	echo $pay["j_invp_account"]."~";
	echo "\"";
	echo ",\"".str_replace("\"","'",$items)."\"";
}
echo ")</script>";


$J_title1="Unknown Payment";
$J_title1=($id?"":"New ")."Unknown Payment";
$J_title1=($pay["j_invp_unknown_payment_company"]?J_Value("","establishment","",$pay["j_invp_unknown_payment_company"]):"");
$J_icon="<img src=".$ROOT."/ico/set/accounts_payEdit.png>";
$J_label=19;
$J_width=360;
$J_height=370;
$J_nomax=1;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>