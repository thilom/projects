<?php
// notify assessor and rep

// get sender
$sender=array("email"=>"","name"=>"");
$ge=mysql_query("SELECT firstname,lastname,email FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1");
if(mysql_num_rows($ge))
{
	$ge=mysql_fetch_array($ge);
	if($ge["email"])
		$sender=array("email"=>$ge["email"],"name"=>preg_replace("~[^a-zA-Z0-9 \-]~","",preg_replace("~[^a-zA-Z0-9 \-]~","",ucwords(strtolower($ge["firstname"]." ".$ge["lastname"])))));
}

// get ass and rep
$targ=array();
$target=array();
$ge=mysql_query("SELECT assessor_id,rep_id1 FROM nse_establishment WHERE establishment_code='".$in["j_inv_to_company"]."' LIMIT 1");
if(mysql_num_rows($ge))
{
	$ge=mysql_fetch_array($ge);
	if($ge["assessor_id"])
		$targ[]=$ge["assessor_id"];
	if($ge["rep_id1"])
		$targ[]=$ge["rep_id1"];
}

$chk=array();
foreach($targ as $k => $v)
{
	// get the recipients from database
	$ge=mysql_query("SELECT firstname,lastname,email FROM nse_user WHERE user_id=".$v." LIMIT 1");
	if(mysql_num_rows($ge))
	{
		$ge=mysql_fetch_array($ge);
		if(!isset($chk[$ge["email"]]))
		{
			$chk[$ge["email"]]=1;
			$target[]=array("id"=>$v,"email"=>$ge["email"],"name"=>ucwords(strtolower(preg_replace("~[^a-zA-Z0-9 \-]~","",(trim($ge["firstname"])." ".trim($ge["lastname"]))))));
		}
	}
}

if($notify && $sender["email"] && count($target))
{
	include $SDR."/custom/email_headers.php";
	$est=J_Value("","establishment","",$in["j_inv_to_company"]);
	$subject="AA TRAVEL GUIDES - PAYMENT RECIEVED (".$est.")";
	$msg=$j_email_header;
	$msg.="<b>PAYMENT RECIEVED</b><hr>";
	$msg.="<b>From: ".$est."</b><br>";
	$msg.="Date: ".date("F Y",$EPOCH)."<hr>";
	$msg.=$notify;
	$msg.="<br><br><br>";
	$msg.=$j_email_footer;

	require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
	$mail=new PHPMailer(true);
	try
	{
		$mail->AddReplyTo($sender["email"],$sender["name"]);
		$mail->SetFrom($sender["email"],$sender["name"]);
		include_once $SDR."/apps/notes/f/insert.php";
		include_once $SDR."/system/activity.php";
		foreach($target as $k => $v)
		{
			$mail->AddAddress($v["email"],$v["name"]);
			// notes
			insertNote($id,2,0,"Emailed Payment Notification to: ".$v["name"]." (".$v["email"].")",0,1,0,4);
			// sys activity
			J_act("Accounts",7,$sender["name"]." sent Payment Notification to: ".$v["name"]." (".$v["email"].")",0,$id);
		}
		$mail->Subject =$subject;
		$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
		$mail->MsgHTML($msg);
		$mail->Send();
	}
	catch (phpmailerException $e){echo $e->errorMessage();}
	catch (Exception $e){echo $e->getMessage();}
}

?>