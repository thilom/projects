j_iid=["none","","none","","none","none","none","none"]
pTypes=["Cash","Cheque","EFT","Debit Order","Credit Card"]
iad=0

function J_pay(d,e,est,x,v,vtco,vfpr,vfrp,vtpr,vb)
{
	var i,u,a=j_P.j_G["accounts"]
	axs=x
	j_Dsp=a["display"]
	a=j_Dsp

	if(vtco){vtco=vtco.split("~");a["company"][vtco[0]]=vtco}
	if(vfpr){vfpr=vfpr.split("~");a["user"][vfpr[0]]=vfpr}
	if(vfrp){vfrp=vfrp.split("~");a["user"][vfrp[0]]=vfrp}
	if(vtpr){vtpr=vtpr.split("~");a["user"][vtpr[0]]=vtpr}
	if(vb){vb=vb.split("~");a["bank"][vb[0]]=vb}

	v=d?v.split("~"):[]
	v[0]=v[0]?v[0]:""
	v[0]=v[0]&&a["company"][v[0]]?"<b>"+a["company"][v[0]][1]+"</b>":""
	v[6]=v[6]?v[6]:""
	v[6]=v[6]&&a["user"][v[6]]?"From: "+a["user"][v[6]][1]:""
	v[7]=v[7]?v[7]:""
	v[7]=v[7]&&a["user"][v[7]]?"Rep: "+a["user"][v[7]][1]:""
	v[8]=v[8]?v[8]:""
	v[8]=v[8]&&a["user"][v[8]]?"Attention: "+a["user"][v[8]][1]:""

	a="<div id=jcd>"
	a+="<tt>"
	if(est)
		a+="<img src="+ROOT+"/ico/set/gohome-64.png onclick=\"J_W(ROOT+'/apps/establishment_manager/edit.php?id="+est+"')\" onmouseover=\"J_TT(this,'Establishment Manager')\" style='float:left;height:43;margin:0 4 0 0;cursor:pointer'>"
	a+=v[0]+(v[8]?" | "+v[8]:"")+"<br>"
	a+=v[6]?v[6]:""
	a+=v[7]?" | "+v[7]:""
	a+="<br>"
	a+=(v[4]?"Invoice No: "+v[4]:"")+(v[1]?(v[4]?" | ":"")+"Date: "+v[1]:"")
	a+="</tt><br>"

	a+="<table class=jinv_tb id=jTR style=width:100%><tr id=jinv_h onmouseover=\"J_TT(this,'Show columns')\" onclick=J_inp()><th style=display:none>Date</th><th>Item</th><th style=display:none>Code</th><th style=display:none>Qty</th><th style=display:none>Unit</th><th style=display:none>Price</th><th style=display:none>Disc</th><th style=display:none>Vat</th><th>Total</th><th id=jci>Paid</th><th>Date</th><th>Bank</th><th>Reference</th><th>Type</th><th>Comment</th><th>Person</th></tr><tbody id=jinvi>"
	var tq=0,tp=0,ti=0,p=" style=display:none"
	if(d&&v[15])
	{
		var r1,rs,s,tr,j
		i=0
		v[15]=v[15].split("|")
		while(v[15][i])
		{
			u=v[15][i].split("`")
			s=u[14].split("*")
			rs=s.length-1>1?" rowspan="+(s.length-1):""
			u[0]=u[0]?u[0]:0
			u[6]=parseFloat(u[6]?u[6]:0)
			u[7]=parseFloat(u[7]?u[7]:0)
			u[8]=parseFloat(u[8]?u[8]:0)
			u[9]=parseFloat(u[9]?u[9]:0)
			u[10]=parseFloat(u[10]?u[10]:0)
			u[12]=parseFloat(u[12]?u[12]:0)
			a+="<tr>"
			a+="<td class=jdbC"+p+rs+">"+(u[2]?u[2]:"")+"</td>"
			a+="<td class=jdbW"+rs+"><b>"+(u[3]?u[3]:"")+"</b><a href=go: onclick='J_U(this);return false' onmouseover=\"J_TT(this,'View description')\" onfocus=blur()>&nbsp; &#9660;</a><div class=ji><i>"+(u[4]?u[4]:"")+"</i><em>"+(u[11]?u[11]:"")+"</em></div></td>"
			a+="<td class=jinB"+p+rs+">"+u[5]+"</td>"
			tq+=u[6]
			a+="<th class=jdbB"+p+rs+">"+u[6]+"</th>"
			tp+=u[7]
			a+="<th class=jdbK"+p+rs+">"+u[7].toFixed(2)+"</th>"
			a+="<th class=jdbY"+p+rs+">"+(u[6]*u[7]).toFixed(2)+"</th>"
			a+="<th class=jdbO"+p+rs+(u[8]?" onmouseover=\"J_TT(this,'"+(u[13]?u[13]+"%":"Fixed amount")+"')\"":"")+">"+u[8].toFixed(2)+"</th>"
			a+="<th class=jdbR"+p+rs+(u[9]?" onmouseover=\"J_TT(this,'"+u[12]+"% VAT')\"":"")+">"+u[9].toFixed(2)+"</th>"
			a+="<th class=jdbG"+rs+">"+u[10].toFixed(2)+"</th>"
			if(s.length-1<1)
			{
				a+="<th class=jdbR></th>"
				a+="<th></th>"
				a+="<td></td>"
				a+="<td></td>"
				a+="<td></td>"
				a+="<td></td>"
				a+="<td></td>"
				a+="<td></td>"
				a+="</tr>"
				iad++
			}
			else
			{
				j=0,tr=""
				while(s[j])
				{
					u[13]=parseFloat(u[13]?u[13]:0)
					t=s[j].split("^")
					t[5]=parseFloat(t[5]?t[5]:0)
					ti+=t[5]
					a+=tr
					a+="<th class=jdbR>"+(0-t[5]).toFixed(2)+"</th>"
					a+="<td>"+(t[3]?t[3]:"")+"</td>"
					a+="<td>"+(t[1]?t[1]:"")+"</td>"
					a+="<td>"+(t[4]?t[4]:"")+"</td>"
					a+="<td>"+(t[2]?pTypes[t[2]]:"")+"</td>"
					a+="<td class=p_comm>"+(t[6]?"<tt>"+t[6]+"</tt>":"")+"</td>"
					a+="<td nowrap>"+(t[9]?j_P.j_G["accounts"]["display"]["user"][t[8]][1]:"")+"</td>"
					a+="</tr>"
					j++
					tr="<tr>"
				}
			}
			i++
		}
	}
	v[13]=v[13]?parseFloat(v[13]).toFixed(2):0
	ti=ti?ti.toFixed(2):0
	a+="</tbody><tr id=jintt><td style=display:none></td><td></td><td style=display:none></td><th class=jdbB style=display:none>"+tq+"</th><th class=jdbK style=display:none>"+parseFloat(tp).toFixed(2)+"</th><th class=jdbY style=display:none>"+(v[10]?parseFloat(v[10]).toFixed(2):"0.00")+"</th><th class=jdbO style=display:none>"+(v[11]?parseFloat(v[11]).toFixed(2):"0.00")+"</th><th class=jdbR style=display:none>"+(v[12]?parseFloat(v[12]).toFixed(2):"0.00")+"</th><th class=jdbG>"+(v[13]?v[13]:"0.00")+"</th><th class=jdbR style=cursor:default"+(ti<v[13]?";color:red":"")+">"+(ti?(0-ti).toFixed(2):"0.00")+"</th><th colspan=7></th></tr>"
	a+="</table>"

	u=d?(v[9]?v[9]:0):0
	if(u&&j_Dsp["bank"][u])
		a+="<br><tt class=jO50>"+j_Dsp["bank"][u][2].replace(/<br>/g,", ")+"</tt>"

	a+="</div>"
	a+="</body></html>"
	document.write(a)

	J_tr()
	w="j_Wi["+j_W+"]['F']["+j_Wn+"]['W']"
	a="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/payment_search.php');return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_pay.png onmouseover=J_TT(this,'Search')></a>"
	a+=e?"<a href=go: onclick=\"J_W('"+ROOT+"/apps/accounts/invoices/age_view.php?id="+est+"');return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/accounts_statements.png onmouseover=\"J_TT(this,'View Invoice history')\"></a>":""
	a+="<a href=go: onclick=\"J_W('"+ROOT+"/apps/accounts/invoices/invoice_view.php?id="+d+"');return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/spreadsheet.png onmouseover=\"J_TT(this,'View Invoice')\"></a>"
	a+=(e?"<a href=go: onclick=\""+w+".J_opaq();"+w+".location='"+ROOT+"/apps/accounts/payments/edit.php?id="+d+"';return false\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/edit.png onmouseover=\"J_TT(this,'Edit Payment')\"></a>":"")
	J_footer(a)
	j_P.j_Wi[j_W]["table"].style.width=1020
	j_nostart=1
	j_P.J_WS(self,-1,(v[4]?"No: "+v[4]:"")+(v[1]?(v[4]?" (":"")+v[1]+(v[4]?")":""):""),"Payment",v[0]+(v[8]?" | "+v[8]:""),"<img src="+ROOT+"/ico/set/accounts_payEdit.png>",19,0,0,$("jcd").clientHeight+40,1020)
}

function J_inpH()
{
	J_note($("jinv_h"),"<tt>Click header to show columns</tt>",90,2,5)
	J_note($("jci"),"<tt>Click these column items</tt>",80,3,5,0,0,3)
}

function J_inp()
{
	var a="<a href=go: onfocus=blur() onclick=\"j_Wi["+j_W+"]['F']["+j_Wn+"]['W'].J_inq(",b=");return false\""
	J_note("x","<b class=jA>"+a+"0"+b+">Date</a>"+a+"2"+b+">Code</a>"+a+"4"+b+">Unit Price</a>"+a+"5"+b+">Price</a>"+a+"6"+b+">Discount</a>"+a+"7"+b+">VAT</a></b>",0,3,2,-1,1)
}

function J_inq(v)
{
	j_iid[v]=j_iid[v]==""?"none":""
	var t=$T($("jTR"),"tr"),i=0
	while(t[i])
	{
		if(t[i].childNodes[v])
			t[i].childNodes[v].style.display=j_iid[v]
		i++
	}
	t=j_P.j_Wi[j_W]
}