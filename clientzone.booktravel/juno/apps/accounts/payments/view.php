<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include $SDR."/apps/accounts/invoices/get.php";
include $SDR."/apps/accounts/invoices/default.php";

$err="";
$id=isset($id)?$id:(isset($_GET["id"])?$_GET["id"]:0);

$in=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$id));

$toCo="";
$fromPer="";
$fromRep="";
$toPer="";
$bank="";

$v=($in["j_inv_to_company"]?$in["j_inv_to_company"]:"")."~";
$v.=date("d/m/Y H:i",$in["j_inv_date"])."~";
$v.=$in["j_inv_order"]."~";
$v.=$in["j_inv_reference"]."~";
$v.=$in["j_inv_number"]."~";
$v.=$in["j_inv_to_company"]."~"; // 5
$v.=$in["j_inv_from_person"]."~";
$v.=$in["j_inv_from_rep"]."~";
$v.=$in["j_inv_to_person"]."~";
$v.=($in["j_inv_bank"]?$in["j_inv_bank"]:"")."~";
$v.=$in["j_inv_total_price"]."~"; // 10
$v.=$in["j_inv_total_discount"]."~";
$v.=$in["j_inv_total_vat"]."~";
$v.=$in["j_inv_total"]."~";
$v.=$in["j_inv_type"]."~";

if(!isset($_SESSION["juno"]["accounts"]["company"][$in["j_inv_to_company"]]))
	$toCo=J_inv_getto($in["j_inv_to_company"]);
if(!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_from_person"]]))
	$fromPer=J_inv_getpeo($in["j_inv_from_person"]);
if(!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_from_rep"]]))
	$fromRep=J_inv_getpeo($in["j_inv_from_rep"]);
if(!isset($_SESSION["juno"]["accounts"]["user"][$in["j_inv_to_person"]]))
	$toPer=J_inv_getpeo($in["j_inv_to_person"]);
if(!isset($_SESSION["juno"]["accounts"]["element"][$in["j_inv_bank"]]))
	$bank=J_inv_getele($in["j_inv_bank"]);

$r=mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=".$id." ORDER BY j_invit_date,j_invit_name LIMIT 200");
if(mysql_num_rows($r))
{
	while($i=mysql_fetch_array($r))
	{
		$v.=$i["j_invit_id"]."`";
		$v.=$i["j_invit_inventory"]."`";
		$v.=($i["j_invit_date"]?date("d/m/Y",$i["j_invit_date"]):"")."`";
		$v.=$i["j_invit_name"]."`";
		$v.=$i["j_invit_description"]."`";
		$v.=$i["j_invit_code"]."`"; // 5
		$v.=$i["j_invit_quantity"]."`";
		$v.=$i["j_invit_price"]."`";
		$v.=$i["j_invit_discount"]."`";
		$v.=$i["j_invit_vat"]."`";
		$v.=$i["j_invit_total"]."`"; // 10
		$v.=$i["j_invit_serial"]."`";
		$v.=$i["j_invit_vat_percent"]."`";
		$v.=$i["j_invit_disc_percent"]."`";

		$rp=mysql_query("SELECT * FROM nse_invoice_item_paid WHERE j_invp_invoice_item=".$i["j_invit_id"]);
		if(mysql_num_rows($rp))
		{
			while($p=mysql_fetch_array($rp))
			{
				$v.=$p["j_invp_id"]."^";
				$v.=$p["j_invp_bank"]."^";
				$v.=$p["j_invp_type"]."^";
				$v.=date("d/m/Y",($p["j_invp_date"]?$p["j_invp_date"]:$EPOCH))."^";
				$v.=$p["j_invp_reference"]."^";
				$v.=$p["j_invp_paid"]."^"; // 5
				$v.=$p["j_invp_comment"]."^";
				$v.=($p["j_invp_user"]?$p["j_invp_user"]:"")."^";
				$v.="*";
			}
		}
		$v.="|";
	}
}
$v=stripit($v);

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<link rel=stylesheet href=".$ROOT."/apps/accounts/invoices/style.css>";
echo "<script src=".$ROOT."/apps/accounts/payments/view.js></script>";
echo "<body>";
if($err)echo "<tt style=color:#CC0000><b>WARNING!</b> You have SQL errors. To solve this problem, please copy the contents of this page and send to the system administrator.</tt><hr>".$err."<br><br>";

$u=$_SESSION["j_user"]["id"];

echo "<script>J_pay(";
echo $id;
echo ",".($u==$in["j_inv_from_person"] || $u==$in["j_inv_from_rep"] || $u==$in["j_inv_from_contact"] || $u==$in["j_inv_creator"] || $u==$in["j_inv_user"] || $_SESSION["j_user"]["role"]=="b" || $_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s"?1:0);
echo ",".($in["j_inv_to_company"]?"'".$in["j_inv_to_company"]."'":0);
echo ",'".$_SESSION["j_user"]["role"]."'";
echo ",\"".$v."\"";
echo ",".($toCo?"\"".$toCo."\"":0);
echo ",".($fromPer?"\"".$fromPer."\"":0);
echo ",".($fromRep?"\"".$fromRep."\"":0);
echo ",".($toPer?"\"".$toPer."\"":0);
echo ",".($bank?"\"".$bank."\"":0);
echo ")</script>";
?>