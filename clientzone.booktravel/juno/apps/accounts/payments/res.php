<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/parse.php";
// constrain_user_establishments functions
include $SDR."/custom/constrain_user_establishments.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$d="";

$dd=explode("~",(isset($_GET["d"])?$_GET["d"]:"")."co~total~paid~payid");
$ddd=array();
$sq=array();
foreach($dd as $k => $v)
{
	$sq[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","date","inum","ornum","ref","co","ass","rep","pri","dis","vat","total","paid","pdate","pbank","ptype","pref","payid");
$d="";$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}

$s="SELECT ";
$s.="DISTINCT j_invp_id";
if(isset($sq["date"]))$s.=",j_inv_date";
if(isset($sq["inum"]))$s.=",j_inv_number";
if(isset($sq["ornum"]))$s.=",j_inv_order";
if(isset($sq["ref"]))$s.=",j_inv_reference";
if(isset($sq["co"]))$s.=",j_inv_to_company";
if(isset($sq["ass"]))$s.=",j_inv_from_assessor";
if(isset($sq["rep"]))$s.=",j_inv_from_rep";
if(isset($sq["pri"]))$s.=",j_inv_total_price";
if(isset($sq["dis"]))$s.=",j_inv_total_discount";
if(isset($sq["vat"]))$s.=",j_inv_total_vat";
$s.=",j_inv_total";
$s.=",j_invp_paid";
if(isset($sq["pdate"]))$s.=",j_invp_date";
if(isset($sq["pbank"]))$s.=",j_invp_bank";
if(isset($sq["ptype"]))$s.=",j_invp_type";
if(isset($sq["pref"]))$s.=",j_invp_reference";
$s.=",j_invp_invoice_item";
$s.=",j_inv_id";
$s.=",j_inv_order_id";
$s.=" FROM nse_invoice_item_paid ";
$s.="LEFT JOIN nse_invoice ON j_invp_invoice=j_inv_id ";
$s.="LEFT JOIN nse_invoice_item ON j_invit_id=j_invp_invoice_item ";
$s.="WHERE j_invp_id>0 ";
if(isset($_GET["ds"]))
{
	$z=J_dateParse($_GET["ds"]);
	if($z)
		$s.="AND j_invp_date>=".$z." ";
}
if(isset($_GET["de"]))
{
	$z=J_dateParse($_GET["de"],0,0,1);
	if($z)
	{
		$s.="AND j_invp_date<".$z." ";
		if(isset($_GET["go"]))
			$s.="AND j_invp_created<".$z." ";
	}
}
if(isset($_GET["co"]))
{
	// check if user has establishment access - returns 0 if not accessible
	$eid=check_user_establishment($_GET["co"]);
	$s.="AND j_inv_to_company='".$eid."' ";
}
else // constrain user establishments
	$s.=user_establishments_constrain(1,"j_inv_to_company");
if(isset($_GET["t"]))$s.="AND j_inv_number LIKE '%".preg_replace("~[^a-zA-Z0-9 &\-\.]~","",$_GET["t"])."%' ";
if(isset($_GET["fp"]))$s.="AND j_inv_total".($_GET["fp"]==1?"=":">")."j_inv_paid ";
if(isset($_GET["pt"]))$s.="AND j_invp_type=".$_GET["pt"]." ";
$jL0=mysql_num_rows(mysql_query($s));
$s.="ORDER BY j_inv_id DESC ";
if(isset($_GET["go"]) && isset($_GET["jL2"]) && !$_GET["jL2"])
	$jL2=1000000;
else
	$s.=" LIMIT ".$jL1.",".$jL2;
$rs=mysql_query($s);
//echo $s."<hr>";
$r="";
$eco="";
$ecu="";
$ecp="";

$tp=0;
$td=0;
$tv=0;
$tt=0;
$tpy=0;

if(mysql_num_rows($rs))
{
	$invA=array();
	$co=array();
	$cu=array();
	$ptype=array("Cash","Cheque","EFT","Debit Order","Credit Card");

	include_once $SDR."/system/get.php";

	while($g=mysql_fetch_array($rs))
	{
		if(!isset($_GET["csv"]))
			$r.=$g["j_inv_id"]."~";
		if(isset($sq["date"]))
			$r.=($g["j_inv_date"]?date("Y/m/d",$g["j_inv_date"]):"")."~";
		if(isset($sq["inum"]))
			$r.=$g["j_inv_number"]."~";
		if(isset($sq["ornum"]))
			$r.=$g["j_inv_order"]."~";
		if(isset($sq["ref"]))
			$r.=$g["j_inv_reference"]."~";
		$cmpny=($g["j_inv_to_company"]?$g["j_inv_to_company"]:"");
		if(isset($_GET["csv"]))
			$r.=J_Value("","establishment","",$cmpny)."~";
		else
		{
			$r.=$cmpny."~";
			if($cmpny && !isset($co[$cmpny]))
				$co[$cmpny]=J_Value("","establishment","",$cmpny);
		}
		if(isset($sq["ass"]))
		{
			if(isset($_GET["csv"]))
				$r.=($g["j_inv_from_assessor"]?J_Value("","people","",$g["j_inv_from_assessor"]):"")."~";
			else
			{
				$r.=($g["j_inv_from_assessor"]?$g["j_inv_from_assessor"]:"")."~";
				if($g["j_inv_from_assessor"] && !isset($cp[$g["j_inv_from_assessor"]]))
					$cu[$g["j_inv_from_assessor"]]=J_Value("","people","",$g["j_inv_from_assessor"]);
			}
		}
		if(isset($sq["rep"]))
		{
			if(isset($_GET["csv"]))
				$r.=J_Value("","people","",$g["j_inv_from_rep"])."~";
			else
			{
				$r.=($g["j_inv_from_rep"]?$g["j_inv_from_rep"]:"")."~";
				if($g["j_inv_from_rep"] && !isset($cp[$g["j_inv_from_rep"]]))
					$cu[$g["j_inv_from_rep"]]=J_Value("","people","",$g["j_inv_from_rep"]);
			}
		}
		if(isset($sq["pri"]))
		{
			$r.=(!isset($invA[$g["j_inv_id"]])?$g["j_inv_total_price"]:"-")."~";
			$tp+=(!isset($invA[$g["j_inv_id"]])?$g["j_inv_total_price"]:0);
		}
		if(isset($sq["dis"]))
		{
			$r.=(!isset($invA[$g["j_inv_id"]])?$g["j_inv_total_discount"]:"-")."~";
			$td+=(!isset($invA[$g["j_inv_id"]])?$g["j_inv_total_discount"]:0);
		}
		if(isset($sq["vat"]))
		{
			$r.=(!isset($invA[$g["j_inv_id"]])?$g["j_inv_total_vat"]:"-")."~";
			$tv+=(!isset($invA[$g["j_inv_id"]])?$g["j_inv_total_vat"]:0);
		}
		$r.=(!isset($invA[$g["j_inv_id"]])?($g["j_inv_total"]?$g["j_inv_total"]:""):"-")."~";
		$tt+=(!isset($invA[$g["j_inv_id"]])?($g["j_inv_total"]?$g["j_inv_total"]:0):0);
		$r.=$g["j_invp_paid"]."~";
		$tpy+=($g["j_invp_paid"]?$g["j_invp_paid"]:0);
		if(isset($sq["pdate"]))
			$r.=($g["j_invp_date"]?date("Y/m/d",$g["j_invp_date"]):"")."~";
		if(isset($sq["pbank"]))
			$r.=$g["j_invp_bank"]."~";
		if(isset($sq["ptype"]))
		{
			if(isset($_GET["csv"]))
				$r.=$ptype[$g["j_invp_type"]]."~";
			else
				$r.=$g["j_invp_type"]."~";
		}
		if(isset($sq["pref"]))
			$r.=$g["j_invp_reference"]."~";
		if(!isset($_GET["csv"]))
			$r.=$g["j_invp_id"]."~";
		$r.="|";
		$invA[$g["j_inv_id"]]=1;
	}

	$r=str_replace("\"","",$r);
	$r=str_replace("\n","",$r);
	$r=str_replace("~|","|",$r);

	if(isset($_GET["csv"]))
	{
		$r=str_replace(",",";",$r);
		$r=str_replace("~",",",$r);
		$r=str_replace("|","\n",$r);

		$h="";
		$t="";
		if(isset($sq["date"]))
		{
			$h.="DATE,";
			$t.=",";
		}
		if(isset($sq["inum"]))
		{
			$h.="NUMBER,";
			$t.=",";
		}
		if(isset($sq["ornum"]))
		{
			$h.="ORDER,";
			$t.=",";
		}
		if(isset($sq["ref"]))
		{
			$h.="REF,";
			$t.=",";
		}
		$h.="COMPANY,";
		$t.=",";
		if(isset($sq["ass"]))
		{
			$h.="ASSESSOR,";
			$t.=",";
		}
		if(isset($sq["rep"]))
		{
			$h.="REP,";
			$t.=",";
		}
		if(isset($sq["pri"]))
		{
			$h.="PRICE,";
			$t.=number_format($tp,2,"."," ").",";
		}
		if(isset($sq["dis"]))
		{
			$h.="DISCOUNT,";
			$t.=number_format($td,2,"."," ").",";
		}
		if(isset($sq["vat"]))
		{
			$h.="VAT,";
			$t.=number_format($tv,2,"."," ").",";
		}
		$h.="TOTAL,";
		$t.=number_format($tt,2,"."," ").",";
		$h.="PAID,";
		$t.=number_format($tpy,2,"."," ").",";
		if(isset($sq["pdate"]))
			$h.="DATE,";
		if(isset($sq["pbank"]))
			$h.="BANK,";
		if(isset($sq["ptype"]))
			$h.="TYPE,";
		if(isset($sq["pref"]))
			$h.="REF,";
		$h.="\n";

		$r=$h.$r.$t;
	}
	else
	{
		foreach($co as $k => $v){$eco.=$k."~".$v."|";}
		foreach($cu as $k => $v){$ecu.=$k."~".$v."|";}
	}
}



if(isset($_GET["csv"]))
{
	$csv_content=$r;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"ACC_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/accounts/payments/res.js></script>";
	echo "<script>J_inv_r(\"".$r."\",\"".$d."\",\"".$eco."\",\"".$ecu."\",".$jL0.",".$jL1.",".$jL2.")</script>";
}
?>