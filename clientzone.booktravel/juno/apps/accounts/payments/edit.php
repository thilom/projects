<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";
include $SDR."/apps/accounts/invoices/get.php";
include $SDR."/apps/accounts/invoices/default.php";

if(isset($_GET["payid"]))
{
	$g=mysql_query("SELECT j_invp_invoice,j_invp_invoice_item FROM nse_invoice_item_paid WHERE j_invp_id=".$_GET["payid"]);
	if(mysql_num_rows($g))
	{
		$g=mysql_fetch_array($g);
		if(!isset($_GET["id"]))
			$id=$g["j_invp_invoice"];
		if(!$g["j_invp_invoice"] || !$g["j_invp_invoice_item"])
		{
			header("location:".$ROOT."/apps/accounts/payments/anonymous_payments.php?id=".$_GET["payid"]);
			die();
		}
	}
	elseif(isset($_GET["id"]) || (isset($id) || $id))
	{}
	else
		die("<script>alert('WARNING!\\nThis item could not be found');window.parent.J_WX(self)</script>");
}

$err="";
$id=isset($id)?$id:(isset($_GET["id"])?$_GET["id"]:0);
$in=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$id));

// access
if(
$_SESSION["j_user"]["role"]=="s"
|| $_SESSION["j_user"]["role"]=="b"
|| $_SESSION["j_user"]["role"]=="d"
|| $_SESSION["j_user"]["id"]==$in["j_inv_from_person"]
|| $_SESSION["j_user"]["id"]==$in["j_inv_from_assessor"]
|| $_SESSION["j_user"]["id"]==$in["j_inv_from_rep"]
|| $_SESSION["j_user"]["id"]==$in["j_inv_from_contact"]
)
{}
else
	header("Location: ".$ROOT."/apps/accounts/payments/view.php?id=".$id);


if(isset($_GET["del"]))
{
	$id=$_GET["del"];
	include_once $SDR."/system/activity.php";
	J_act("Payment",5,"No: ".$in["j_inv_number"].($in["j_inv_reference"]?" (".$in["j_inv_reference"].")":""),$id,$in["j_inv_to_company"]);
	mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_invoice=".$id);
	mysql_query("UPDATE nse_invoice SET j_inv_paid=0 AND j_inv_type=1 WHERE j_inv_id=".$id);
	echo "<script>";
	if($in["j_inv_cancel_date"])
	{
		$q="SELECT j_inv_id FROM nse_invoice WHERE j_inv_credit_id=".$in["j_inv_id"];
		$q=mysql_query($q);
		$n=mysql_num_rows($q);
		if($n)
		{
			$o="";
			while($g=mysql_fetch_array($q))
				$o.="window.parent.J_W('".$ROOT."/apps/accounts/invoices/edit.php?id=".$g["j_inv_id"]."');";
			echo "if(confirm('WARNING!\\nThere ".($n!=1?"are ".$n." credit notes":"is a credit note")." associated with this payment.\\nDo you want to view ".($n!=1?"them":"it")."?')){".$o."};";
		}
	}
	// update tally
	if($in["j_inv_to_company"])
	{
		$company_id=$in["j_inv_to_company"];
		include $SDR."/apps/accounts/invoices/tally.php";
	}
	echo "window.parent.J_WX(self)</script>";
	die();
}

elseif(count($_POST))
{
	include $SDR."/system/parse.php";
	include_once $SDR."/system/activity.php";
	include_once $SDR."/apps/notes/f/insert.php";
	$insert=0;
	$tt=0;
	$items=array();
	$ip=0;
	$note="";
	$notify="";

	foreach($_POST as $k => $v)
	{
		if(strpos($k,"j_invp_id_")!==false)
		{
			$r=substr($k,strrpos($k,"_")+1);
			$d=$v;
			$p=(J_parseValue($_POST["j_invp_paid_".$r],array("float"=>1))*1);
			if($p)
			{
				$ins=0;
				$it=$_POST["j_invp_invoice_item_".$r];
				if(!isset($items[$it]))
					$items[$it]=0;
				if(!$d)
				{
					mysql_query("INSERT INTO nse_invoice_item_paid (j_invp_invoice,j_invp_invoice_item,j_invp_created) VALUES (".$id.",".$it.",".$EPOCH.")");
					$d=J_ID("nse_invoice_item_paid","j_invp_id");
					J_act("Payment",3,"No: ".$in["j_inv_number"],$id,$in["j_inv_to_company"]);
					$ins=1;
					$insert++;
				}
				else
					J_act("Payment",4,"No: ".$in["j_inv_number"],$id,$in["j_inv_to_company"]);
				$s="UPDATE nse_invoice_item_paid SET
				j_invp_bank='".J_parseValue($_POST["j_invp_bank_".$r],array("basic"=>1,"tagsall"=>1,"caps"=>1))."'
				,j_invp_date='".($_POST["j_invp_date_".$r]?J_parseValue($_POST["j_invp_date_".$r],array("date"=>1)):$EPOCH)."'
				,j_invp_type='".J_parseValue($_POST["j_invp_type_".$r],array("int"=>1))."'
				,j_invp_reference='".J_parseValue($_POST["j_invp_reference_".$r],array("basic"=>1))."'
				,j_invp_paid='".$p."'
				,j_invp_comment='".J_parseValue($_POST["j_invp_comment_".$r],array("keyboard"=>1,"tagsbr"=>1,"ucfirst"=>1))."'
				,j_invp_account=".$_POST["j_invp_account_".$r]."
				,j_invp_user=".$_SESSION["j_user"]["id"]."
				";

				
				
				// send notification to rep/assessor
				$notify.="Amount: R".number_format($p,2,"."," ")."<br>";
				$notify.="Invoice: ".J_Value("j_inv_number","nse_invoice","j_inv_id",$id)."<br>";
				$notify.="Item: ".J_Value("j_invit_name","nse_invoice_item","j_invit_id",$it)."<hr>";
				J_act("Payment",4,"No: ".$in["j_inv_number"]." - Payment amount of ".$p,$id,$in["j_inv_to_company"]);

				$tt+=$p; // new total for payments
				if(!$ins) // total paid now
				{
					$ip+=$p;
					if($ins || ($d && $gp["j_invp_paid"]!=$p)) // only if change
						$note.="Payment: ".$p."~";
				}

				$items[$it]+=$p;
				$s.=" WHERE j_invp_id=".$d;
				mysql_query($s);
			}
			elseif($d)
			{
				$g=mysql_fetch_array(mysql_query("SELECT j_invp_paid FROM nse_invoice_item_paid WHERE j_invp_id=".$d." LIMIT 1"));
				$note.="Payment Deleted (".$g["j_invp_paid"].")~";
				mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_id=".$d);
				J_act("Payment",5,"No: ".$in["j_inv_number"]." - Deleted a payment row",$id,$in["j_inv_to_company"]);
			}
		}
	}

	// Get current invoice type
	$statement = "SELECT j_inv_type
				FROM nse_invoice
				WHERE j_inv_id=?
				LIMIT 1";
	$sql_type = $GLOBALS['dbCon']->prepare($statement);
	$sql_type->bind_param('i', $id);
	$sql_type->execute();
	$sql_type->bind_result($invoice_type);
	$sql_type->fetch();
	$sql_type->close();
	
//	var_dump($_POST);
//	die();
	
	//Update invoice date
	if ($invoice_type != '2') {
		list($day, $month, $year) = explode('/', $_POST['j_invp_date_0']);
		$date = mktime(0, 0, 0, $month, $day, $year);
		$statement = "UPDATE nse_invoice
					SET j_inv_date=?
					WHERE j_inv_id=?";
		$sql_date = $GLOBALS['dbCon']->prepare($statement);
		$sql_date->bind_param('ss', $date, $id);
		$sql_date->execute();
		$sql_date->close();
	}

	
	foreach($items as $k => $v)
		mysql_query("UPDATE nse_invoice_item SET j_invit_paid='".$v."' WHERE j_invit_id=".$k);
	// update invoice
	mysql_query("UPDATE nse_invoice SET j_inv_paid=".$tt.",j_inv_type=2,j_inv_head=2 WHERE j_inv_id=".$id);

	
	if($in["j_inv_to_company"])
	{
		include $SDR."/apps/accounts/payments/get_payments.php";
		$gp=get_payments($id);
		$t=$in["j_inv_total"]*1;
		if($insert)
			$note="Payments: ".$tt." / Total: ".$in["j_inv_total"]." / Outstanding: ".($t-$gp)."~".$note;
		elseif($ip)
			$note="New Payment/s: ".$ip." of ".$tt." / Total: ".$in["j_inv_total"]." / Outstanding: ".($t-$gp)."~".$note;
		$p=mysql_fetch_array(mysql_query("SELECT firstname,lastname FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]));
		$p=ucwords(strtolower(preg_replace("~[^a-zA-Z0-9 \-]~","",$p["firstname"]." ".$p["lastname"])));
		$note=explode("~",$note);
		foreach($note as $k => $n)
		{
			if($n)
				insertNote($in["j_inv_to_company"],2,"INV".$in["j_inv_id"],"Invoice ".$in["j_inv_number"]." - ".$n,0,0,0,4);
		}

		// notify assessor
		if($notify)
			include $SDR."/apps/accounts/payments/verified_notification.php";
	}

	// update tally
	if($in["j_inv_to_company"])
	{
		$company_id=$in["j_inv_to_company"];
		include $SDR."/apps/accounts/invoices/tally.php";
	}

	// if credit notes - alert
	$o="";
	if($in["j_inv_cancel_date"])
	{
		$q=mysql_query("SELECT j_inv_id FROM nse_invoice WHERE j_inv_credit_id=".$in["j_inv_id"]);
		$n=mysql_num_rows($q);
		if($n)
		{
			while($g=mysql_fetch_array($q))
				$o.="window.parent.J_W('".$ROOT."/apps/accounts/invoices/edit.php?id=".$g["j_inv_id"]."');";
			$o="if(confirm('WARNING!\\nThere ".($n!=1?"are ".$n." credit notes":"is a credit note")." associated with this payment.\\nDo you want to view ".($n!=1?"them":"it")."?')){".$o."};";
		}
	}

	if(isset($_POST["noti"]) && $notify)
	{
		echo "<html>";
		echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<body>Notification sent to:<br>";
		foreach($target as $k => $v)
			echo "<b>".$v["name"]."</b> (".$v["email"].")<br>";
		echo "<script>".$o."setTimeout(\"window.parent.J_WX(self)\",3000)</script>";
		echo "</body></html>";
	}
	else
		echo "<script>".$o."window.parent.J_WX(self)</script>";
	die();
}





// invoice
$veriPer="";

$v=($in["j_inv_to_company"]?$in["j_inv_to_company"]:"")."~";
$v.=date("d/m/Y H:i",$in["j_inv_date"])."~";
$v.=$in["j_inv_order"]."~";
$v.=$in["j_inv_reference"]."~";
$v.=$in["j_inv_number"]."~";
$v.=$in["j_inv_to_company"]."~"; // 5
$v.=$in["j_inv_from_person"]."~";
$v.=$in["j_inv_from_rep"]."~";
$v.=$in["j_inv_to_person"]."~";
$v.=($in["j_inv_bank"]?$in["j_inv_bank"]:"")."~";
$v.=$in["j_inv_total_price"]."~"; // 10
$v.=$in["j_inv_total_discount"]."~";
$v.=$in["j_inv_total_vat"]."~";
$v.=$in["j_inv_total"]."~";
$v.=$in["j_inv_type"]."~";

// invoice items
$r=mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=".$id." ORDER BY j_invit_date,j_invit_name");
if(mysql_num_rows($r))
{
	while($i=mysql_fetch_array($r))
	{
		$v.=$i["j_invit_id"]."`";
		$v.=$i["j_invit_inventory"]."`";
		$v.=($i["j_invit_date"]?date("d/m/Y",$i["j_invit_date"]):"")."`";
		$v.=$i["j_invit_name"]."`";
		$v.=$i["j_invit_description"]."`";
		$v.=$i["j_invit_code"]."`"; // 5
		$v.=$i["j_invit_quantity"]."`";
		$v.=$i["j_invit_price"]."`";
		$v.=$i["j_invit_discount"]."`";
		$v.=$i["j_invit_vat"]."`";
		$v.=$i["j_invit_total"]."`"; // 10
		$v.=$i["j_invit_serial"]."`";
		$v.=$i["j_invit_vat_percent"]."`";
		$v.=$i["j_invit_disc_percent"]."`";

		// payments
		$rp=mysql_query("SELECT * FROM nse_invoice_item_paid WHERE j_invp_invoice_item=".$i["j_invit_id"]." ORDER BY j_invp_date,j_invp_paid");
		if(mysql_num_rows($rp))
		{
			while($p=mysql_fetch_array($rp))
			{
				$v.=$p["j_invp_id"]."^";
				$v.=$p["j_invp_bank"]."^";
				$v.=$p["j_invp_type"]."^";
				$v.=date("d/m/Y",($p["j_invp_date"]?$p["j_invp_date"]:$EPOCH))."^";
				$v.=$p["j_invp_reference"]."^";
				$v.=$p["j_invp_paid"]."^"; // 5
				$v.=$p["j_invp_comment"]."^";
				$v.=$p["j_invp_account"]."^";
				$v.="*";
			}
		}

		$v.="|";
	}
}
$v=stripit($v);

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<link rel=stylesheet href=".$ROOT."/apps/accounts/invoices/style.css>";
echo "<script src=".$ROOT."/apps/accounts/payments/edit.js></script>";
echo "<body>";
if($err)echo "<tt style=color:#CC0000><b>WARNING!</b> You have SQL errors. To solve this problem, please copy the contents of this page and send to the system administrator.</tt><hr>".$err."<br><br>";

$veriPer.=J_inv_getpeo($_SESSION["j_user"]["id"]);

// get aa accounts
$acc="";
$qa=mysql_query("SELECT * FROM nse_invoice_account ORDER BY j_invac_name");
if(mysql_num_rows($qa))
{
	while($ga=mysql_fetch_array($qa))
		$acc.=$ga["j_invac_id"]."~".$ga["j_invac_name"]."|";
}

echo "<script>J_pay(";
echo "\"".$acc."\"";
echo ",".$id;
echo ",".($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="b" || $_SESSION["j_user"]["role"]=="s"?1:0);
echo ",".($in["j_inv_to_company"]?"'".$in["j_inv_to_company"]."'":0);
echo ",'".$_SESSION["j_user"]["role"]."'";
echo ",\"".$v."\"";
echo ",".($in["j_inv_to_company"]?"\"".J_inv_getto($in["j_inv_to_company"])."\"":0);
echo ",".($in["j_inv_from_person"]?"\"".J_inv_getpeo($in["j_inv_from_person"])."\"":0);
echo ",".($in["j_inv_from_rep"]?"\"".J_inv_getpeo($in["j_inv_from_rep"])."\"":0);
echo ",".($in["j_inv_to_person"]?"\"".J_inv_getpeo($in["j_inv_to_person"])."\"":0);
echo ",".($in["j_inv_to_company"]?"\"".$veriPer."\"":0);
echo ",".($in["j_inv_bank"]?"\"".J_inv_getele($in["j_inv_bank"])."\"":0);
echo ")</script>";
?>