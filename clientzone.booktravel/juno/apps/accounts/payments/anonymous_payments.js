function J_apay(acc,d,v,o)
{
	var i,u,a=("Absa,African Bank,Capitec,First National Bank,Nedbank,Standard Bank,Rand Merchant Bank").split(",")
	a.sort()
	i=0
	jbanks="<b class=jA>"
	while(a[i])
	{
		jbanks+="<a href=go: onfocus=blur() onclick=\"j_Wi["+j_W+"]['F'][0]['W'].j_E.value=this.innerHTML;return false\">"+a[i]+"</a>"
		i++
	}
	jbanks+="</b>"

	v=v?v.split("~"):[]

	a="<body style=margin:20>"
	a+="<form method=post onsubmit='return P_sub()'>"
	a+="<input type=hidden name=id value="+(d?d:0)+">"
	a+="<table id=jTR width=100%>"
	a+="<tr><th>Account</th><td width=99%><select name=j_invp_account class=jW100><option value=0>Unknown"
	if(acc)
	{
		acc=acc.split("|")
		i=0
		while(acc[i])
		{
			u=acc[i].split("~")
			a+="<option value="+u[0]+(d&&u[i]==v[10]?" selected":"")+">"+u[1]
			i++
		}
	}
	a+="</select>"
	if(d)
	{
		a+="<tr><th>Invoice&nbsp;No.</th><td width=99%><input type=text name=invoice_number value='"+(v[0]?v[0]:"")+"' class=jW100 onkeyup=J_apai(this)><span id=invit>"
		if(o)
		{
			o=J_invItems(o)
			a+=o.replace("="+v[1]+">","="+v[1]+" checked>")
		}
		a+="</span></td></tr>"
	}
	a+="<tr><th>Company</th><td><input type=text value='"+(v[2]?v[3]:"")+"' onkeyup=J_ajax_K(this,'/system/ajax.php?t=establishment&constrain=1',320,4) onclick=J_ajax_C(this) class=jW100><input name=j_invp_unknown_payment_company type=hidden value='"+(v[2]?v[2]:0)+"'>"
	a+="<tr><th>Date</th><td><input type=text value='"+(v[4]?v[4]:j_P.J_date("d/m/Y"))+"' name=j_invp_date class=jW100 onclick=j_P.J_datePicker(self,4)></td></tr>"
	a+="<tr><th>Bank</th><td><input type=text value='"+(v[5]?v[5]:"")+"' name=j_invp_bank class=jW100 onclick=J_note(this,jbanks,0,4,0,-1,1)></td></tr>"
	a+="<tr><th>Type</th><td><select name=j_invp_type class=jW100>"
	pTypes=["Cash","Cheque","EFT","Debit Order","Credit Card"]
	i=0
	while(pTypes[i])
	{
		a+="<option value="+i+(v[6]==i?" selected":"")+">"+pTypes[i]
		i++
	}
	a+="</select></td></tr>"
	a+="<tr><th>Reference</th><td><input type=text value='"+(v[7]?v[7]:"")+"' name=j_invp_reference class=jW100></td></tr>"
	a+="<tr><th>Paid</th><td><input type=text value='"+(v[8]?(v[8]*1).toFixed(2):"0.00")+"' name=j_invp_paid class=jW100 onblur=\"value=(value.replace(/[^0-9\.]/g,'')*1).toFixed(2)\" style=font-weight:bold></td></tr>"
	a+="<tr><th>Comment</th><td><textarea name=j_invp_comment class=jW100>"+(v[9]?v[9].replace(/<br>/g,"\n"):"")+"</textarea></td></tr>"
	a+="</table><br>"
	a+="<input type=submit value=OK onmouseover=focus()>"
	if(d)
		a+=" <input type=button value=Delete onclick=\"if(confirm('WARNING!\\nPermanently delete?')){J_opaq();location=location.href+'&delete=1&id="+d+"'}\">"
	a+="</form>"
	a+="<iframe id=j_IF></iframe>"
	document.write(a)
	J_tr()
}

function J_apai(t)
{
	$("j_IF").src=ROOT+"/apps/accounts/payments/anonymous_payments.php?inv="+t.value
	$("invit").innerHTML="<tt><img src="+ROOT+"/ico/set/load.gif style=float:left:margin:4 4 0 0> Retrieving invoice items for selection</tt>"
}

function J_invItems(o)
{
	o=o.split("|")
	var i=0,u,a=""
	while(o[i])
	{
		u=o[i].split("~")
		a+="<tt onclick=j_P.J_checkbox(this)><hr><input type=radio name=j_invp_invoice_item value="+u[0]+"> <b>"+u[1]+"</b> ("+(u[2]*1).toFixed(2)+")</tt>"
		i++
	}
	return a
}

function J_payRet(d,c,cn,o)
{
	var i,u,e=$("invit"),a=""
	if(!d)
		a="<tt><var><b>No invoice with this number found!</b></var></tt>"
	else if(o)
	{
		if(c)
		{
			i=$N("j_invp_unknown_payment_company")[0]
			i.value=c
			i.previousSibling.value=cn
		}
		a+=J_invItems(o)
	}
	else
		a="<tt><var><b>No items found for this invoice!</b> Ensure this invoice has items before commiting payment</var></tt>"
	e.innerHTML=a
}

function P_sub()
{
	var o=$("invit")
	if(o)
	{
		o=$T(o,"input")
		if(o.length)
		{
			var i=0,c=0
			while(o[i])
			{
				if(o[i].checked==true)
				{
					c=1
					break
				}
				i++
			}
			if(c)
				return true
			else
			{
				alert("ATTENTION!\nAn invoice item must be selected")
				return false
			}
		}
		else
		{
			alert("ATTENTION!\nAn invoice item must be selected")
			return false
		}
	}
	else
		return true
}