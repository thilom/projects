<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include $SDR."/system/parse.php";
// constrain_user_establishments functions
include $SDR."/custom/constrain_user_establishments.php";

$mn=0;
$mx=0;
$c="";
$w=" WHERE j_inv_temp=0";

if(isset($_GET["de"]))
{
	$c.=$_GET["de"];
	$mx=J_dateParse($_GET["de"],0,0,1)+1;
	if($mx)$w.=" AND j_inv_date<".$mx;
}
$c.="~";
if(isset($_GET["ds"]))
{
	$c.=$_GET["ds"];
	$mn=J_dateParse($_GET["ds"])+1;
	if($mn>=$mx)$mn=0;
	if($mn)$w.=" AND j_inv_date>".$mn;
}
$c.="~";
if(isset($_GET["fp"]))
{
	$c.=$_GET["fp"];
	$w.=" AND j_inv_from_person=".$_GET["fp"];
}
$c.="~";
if(isset($_GET["fr"]))
{
	$c.=$_GET["fr"];
	$w.=" AND j_inv_from_rep=".$_GET["fr"];
}
$c.="~";
if(isset($_GET["tc"]))
{
	// check if user has establishment access - returns 0 if not accessible
	$eid=check_user_establishment($_GET["tc"]);
	$w.=" AND j_inv_to_company=".$eid;
}
else // constrain user establishments
	$w.=user_establishments_constrain(1,"j_inv_from_company~j_inv_to_company");
$c.="~";
if(isset($_GET["tp"]))
{
	$c.=$_GET["tp"];
	$w.=" AND j_inv_to_person=".$_GET["tp"];
}
$c.="~";
if(isset($_GET["ii"]))
{
	$c.=$_GET["ii"];
	$w.=" AND j_inv_id IN (SELECT DISTINCT j_invit_id FROM nse_invoice_item WHERE j_invit_inventory=".$_GET["ii"]." GROUP BY j_invit_id)";
}
$c.="~";

$cfp=array();
$cfr=array();
$ctc=array();
$cii=array();

$u=$c;
$c=explode("~",$c);
$s="";
$s.=" | From <b>AA TRAVEL GUIDES</b>";
if($c[1])$s.=" | ".($c[0]?"To ":"UpTo ")."<b>".$c[1]."</b>";
if($c[2])$s.=" | From Establishment: <b>".J_Value("name","establishment","id",$c[2])."</b>";
if($c[3])$s.=" | From Person: <b>".J_Value("firstname~lastname","nse_user","user_id",$c[3])."</b>";
if($c[4])$s.=" | From Rep: <b>".J_Value("firstname~lastname","nse_user","user_id",$c[4])."</b>";
if($c[5])$s.=" | To Company: <b>".J_Value("name","establishment","id",$c[5])."</b>";
if($c[6])$s.=" | To Person: <b>".J_Value("name","people","id",$c[6])."</b>";
if($c[7])$s.=" | Inventory: <b>".J_Value("j_invit_name~j_invit_code","nse_invoice_item","j_invit_inventory",$c[7])."</b>";
$f="";
if(isset($_GET["j_from"]))
	$f="<br><b>Created by</b> ".J_Value("firstname~lastname","nse_user","user_id",$_GET["j_from"]);

echo "<html><head>";
if(isset($_GET["j_sent"]) && !isset($_GET["j_inw"]))
	echo "<script>function $(d){return document.getElementById(d)}</script>";
else
{
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/accounts/report/invoice_report_res1.js></script>";
}
echo "<script src=http://www.google.com/jsapi></script>";
echo "</head><body>";
if(!isset($_GET["j_inw"]))
	echo "<tt class=jO40><b>Criteria</b> ".($s?$s:"None").$f."</tt><hr>";


// to company paid
$r=mysql_query("SELECT j_inv_to_company,COUNT(j_inv_to_company) FROM nse_invoice".$w." AND j_inv_paid=j_inv_total GROUP BY j_inv_to_company ORDER BY COUNT(j_inv_to_company) LIMIT 10");
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','To Paid Invoices');";
echo "d.addColumn('number','Invoices');";
$i=mysql_num_rows($r);
if($i)
{
	echo "d.addRows(".$i.");";
	$i=0;
	while($g=mysql_fetch_array($r))
	{
		echo "d.setValue(".$i.",0,\"".($g["j_inv_to_company"]?J_Value("name","establishment","id",$g["j_inv_to_company"]):"None")."\");";
		echo "d.setValue(".$i.",1,".$g["COUNT(j_inv_to_company)"].");";
		$ctc[$g["j_inv_to_company"]]=$g["COUNT(j_inv_to_company)"];
		$i++;
	}
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c0'));c.draw(d,{width:420,height:240,is3D:true,title:'Top ".$i." Fully Paid Invoices',tooltipWidth:'120',legendFontSize:'11pt'})}</script>";


// to company part-paid
$r=mysql_query("SELECT j_inv_to_company,COUNT(j_inv_to_company) FROM nse_invoice".$w." AND j_inv_paid!=0 AND j_inv_paid<j_inv_total GROUP BY j_inv_to_company ORDER BY COUNT(j_inv_to_company) LIMIT 10");
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','To Partially Paid Invoices');";
echo "d.addColumn('number','Invoices');";
$i=mysql_num_rows($r);
if($i)
{
	echo "d.addRows(".$i.");";
	$i=0;
	while($g=mysql_fetch_array($r))
	{
		echo "d.setValue(".$i.",0,\"".($g["j_inv_to_company"]?J_Value("name","establishment","id",$g["j_inv_to_company"]):"None")."\");";
		echo "d.setValue(".$i.",1,".$g["COUNT(j_inv_to_company)"].");";
		$i++;
	}
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c1'));c.draw(d,{width:420,height:240,is3D:true,title:'Top ".$i." Partially Paid Invoices',tooltipWidth:'120',legendFontSize:'11pt'})}</script>";


// to company un-paid
$r=mysql_query("SELECT j_inv_to_company,COUNT(j_inv_to_company) FROM nse_invoice".$w." AND j_inv_paid=0 GROUP BY j_inv_to_company ORDER BY COUNT(j_inv_to_company) LIMIT 10");
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','To Un-Paid Invoices');";
echo "d.addColumn('number','Invoices');";
$i=mysql_num_rows($r);
if($i)
{
	echo "d.addRows(".$i.");";
	$i=0;
	while($g=mysql_fetch_array($r))
	{
		echo "d.setValue(".$i.",0,\"".($g["j_inv_to_company"]?J_Value("name","establishment","id",$g["j_inv_to_company"]):"None")."\");";
		echo "d.setValue(".$i.",1,".$g["COUNT(j_inv_to_company)"].");";
		$i++;
	}
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c2'));c.draw(d,{width:420,height:240,is3D:true,title:'Top ".$i." Un-Paid Invoices',tooltipWidth:'120',legendFontSize:'11pt'})}</script>";


// to highest value
$r=mysql_query("SELECT j_inv_total,j_inv_to_company FROM nse_invoice".$w." ORDER BY j_inv_total DESC LIMIT 10");
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Highest Value');";
echo "d.addColumn('number','Invoices');";
$i=mysql_num_rows($r);
if($i)
{
	echo "d.addRows(".$i.");";
	$i=0;
	while($g=mysql_fetch_array($r))
	{
		echo "d.setValue(".$i.",0,\"".($g["j_inv_to_company"]?J_Value("name","establishment","id",$g["j_inv_to_company"]):"None")."\");";
		echo "d.setValue(".$i.",1,".$g["j_inv_total"].");";
		$i++;
	}
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c3'));c.draw(d,{width:420,height:240,is3D:true,title:'Top ".$i." Highest Value Invoices',tooltipWidth:'120',legendFontSize:'11pt'})}</script>";


// from person
$r=mysql_query("SELECT j_inv_from_person,COUNT(j_inv_from_person) FROM nse_invoice".$w." GROUP BY j_inv_from_person ORDER BY COUNT(j_inv_from_person) LIMIT 10");
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','From person');";
echo "d.addColumn('number','Invoices');";
$i=mysql_num_rows($r);
if($i)
{
	echo "d.addRows(".$i.");";
	$i=0;
	while($g=mysql_fetch_array($r))
	{
		echo "d.setValue(".$i.",0,\"".($g["j_inv_from_person"]?J_Value("firstname~lastname","nse_user","user_id",$g["j_inv_from_person"]):"None")."\");";
		echo "d.setValue(".$i.",1,".$g["COUNT(j_inv_from_person)"].");";
		$cfp[$g["j_inv_from_person"]]=$g["COUNT(j_inv_from_person)"];
		$i++;
	}
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c4'));c.draw(d,{width:420,height:240,is3D:true,title:'Top ".$i." Invoicing People',tooltipWidth:'120',legendFontSize:'11pt'})}</script>";


// from rep
$r=mysql_query("SELECT j_inv_from_rep,COUNT(j_inv_from_rep) FROM nse_invoice".$w.(isset($_SESSION["j_user"]) && $_SESSION["j_user"]["role"]=="a"?" AND j_inv_from_person=".$_SESSION["j_user"]["id"]:"")." GROUP BY j_inv_from_rep ORDER BY COUNT(j_inv_from_rep) LIMIT 10");
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','From Rep');";
echo "d.addColumn('number','Invoices');";
$i=mysql_num_rows($r);
if($i)
{
	echo "d.addRows(".$i.");";
	$i=0;
	while($g=mysql_fetch_array($r))
	{
		echo "d.setValue(".$i.",0,\"".($g["j_inv_from_rep"]?J_Value("firstname~lastname","nse_user","user_id",$g["j_inv_from_rep"]):"None")."\");";
		echo "d.setValue(".$i.",1,".$g["COUNT(j_inv_from_rep)"].");";
		$cfr[$g["j_inv_from_rep"]]=$g["COUNT(j_inv_from_rep)"];
		$i++;
	}
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c5'));c.draw(d,{width:420,height:240,is3D:true,title:'".(isset($_SESSION["j_user"]) && $_SESSION["j_user"]["role"]=="a"?"Your ":"")."Top ".$i." Reps',tooltipWidth:'120',legendFontSize:'11pt'})}</script>";


// sold items
$r=mysql_query("SELECT DISTINCT j_invit_inventory,j_invit_name,COUNT(j_invit_inventory) FROM nse_invoice_item LEFT JOIN nse_invoice ON j_inv_id=j_invit_invoice".$w." GROUP BY j_invit_inventory ORDER BY COUNT(j_invit_inventory) DESC LIMIT 10");
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Inventory Items');";
echo "d.addColumn('number','Items');";
$i=mysql_num_rows($r);
if($i)
{
	echo "d.addRows(".$i.");";
	$i=0;
	while($g=mysql_fetch_array($r))
	{
		if(!empty($g["j_invit_inventory"]))
		{
			echo "d.setValue(".$i.",0,\"".($g["j_invit_name"]?substr($g["j_invit_name"],0,16):"None")."\");";
			echo "d.setValue(".$i.",1,".$g["COUNT(j_invit_inventory)"].");";
			$cii[$g["j_invit_inventory"]]=$g["COUNT(j_invit_inventory)"];
			$i++;
		}
	}
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c6'));c.draw(d,{width:420,height:240,is3D:true,title:'Top ".$i." Invoice Items',tooltipWidth:'120',legendFontSize:'11pt'})}</script>";


if(!$mn)
{
	$mn=mysql_fetch_array(mysql_query("SELECT MIN(j_inv_date) AS min FROM nse_invoice"));
	$mn=$mn["min"]-1;
}
if(!$mx)
{
	$mx=mysql_fetch_array(mysql_query("SELECT MAX(j_inv_date) AS max FROM nse_invoice"));
	$mx=$mx["max"]+1;
}
if($mn>=$mx)$mn=0;
$w="";
if(isset($_GET["fp"]))$w.=" AND j_inv_from_person=".$_GET["fp"];
if(isset($_GET["fr"]))$w.=" AND j_inv_from_rep=".$_GET["fp"];
if(isset($_GET["tc"]))$w.=" AND j_inv_to_company=".$_GET["tc"];
$d=date("d",$mn);
$m=date("m",$mn);
$y=date("Y",$mn);


// freq invoice
$a=array();
$i=0;
$de=0;
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m)+$i,1,date($y));
	$de=mktime(0,0,0,date($m)+$i+1,1,date($y));
	$r=mysql_fetch_row(mysql_query("SELECT COUNT(j_inv_id) FROM nse_invoice WHERE j_inv_date>".$ds." AND j_inv_date<".$de.$w.(isset($_SESSION["j_user"]) && $_SESSION["j_user"]["role"]=="a"?" AND j_inv_from_person=".$_SESSION["j_user"]["id"]:"")));
	if($r[0])$a[$ds]=$r[0];
	$i++;
}
$b=array();
foreach($a as $k => $v)
{
	$b[$k]=$v;
}
arsort($b);
$q="";
$i=0;
foreach($b as $k => $v)
{
	$q.="d.setValue(".$i.",0,'".date("M Y",$k)."');d.setValue(".$i.",1,".$a[$k].");";
	$i++;
	if($i==10)break;
}
echo "<script>google.load('visualization',1,{packages:['piechart']});google.setOnLoadCallback(drawChart);";
echo "function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Best Months');";
echo "d.addColumn('number','Invoices');";
if($q)
{
	echo "d.addRows(".$i.");";
	echo $q;
}
else
	echo "d.addRows(1);d.setValue(0,0,'None');d.setValue(0,1,1);";
echo "var c=new google.visualization.PieChart($('c7'));c.draw(d,{width:420,height:240,is3D:true,title:'".(isset($_SESSION["j_user"]) && $_SESSION["j_user"]["role"]=="a"?"Your ":"")."Top ".$i." Invoice Months',tooltipWidth:'120'})}</script>";


// freq to company
$i=0;
$de=0;
$q="";
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m)+$i,1,date($y));
	$de=mktime(0,0,0,date($m)+$i+1,1,date($y));
	$q.="d.setValue(".$i.",0,'".date("Y M",$ds)."');";
	$n=1;
	foreach($ctc as $k => $v)
	{
		$r=mysql_fetch_row(mysql_query("SELECT COUNT(j_inv_id) FROM nse_invoice WHERE j_inv_date>".$ds." AND j_inv_date<".$de.$w." AND j_inv_to_company='".$k."'"));
		$q.="d.setValue(".$i.",".$n.",".$r[0].");";
		$n++;
	}
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
foreach($ctc as $k => $v)
	echo "d.addColumn('number',\"".($k?J_Value("name","establishment","id",$k):"None")." (".$v.")\");";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c8'));c.draw(d,{width:890,height:320,legend:'right',title:'Companies Invoice Frequency',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";


// freq totals and payment
$i=0;
$de=0;
$q="";
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m)+$i,1,date($y));
	$de=mktime(0,0,0,date($m)+$i+1,1,date($y));
	$t=0;
	$p=0;
	$q.="d.setValue(".$i.",0,'".date("Y M",$ds)."');";
	$s="SELECT j_inv_total,j_inv_paid FROM nse_invoice WHERE j_inv_date>".$ds." AND j_inv_date<".$de.$w;
	$s=mysql_query($s);
	if(mysql_num_rows($s))
	{
		while($g=mysql_fetch_array($s))
		{
			$t+=$g["j_inv_total"];
			$p+=$g["j_inv_paid"];
		}
	}
	$q.="d.setValue(".$i.",1,".$t.");";
	$q.="d.setValue(".$i.",2,".$p.");";
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
echo "d.addColumn('number','R".number_format($t,2,"."," ")." Invoiced');";
echo "d.addColumn('number','R".number_format($p,2,"."," ")." Paid');";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c9'));c.draw(d,{width:890,height:320,legend:'right',title:'Invoice Totals and Payments',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";


// freq from people
$i=0;
$de=0;
$q="";
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m)+$i,1,date($y));
	$de=mktime(0,0,0,date($m)+$i+1,1,date($y));
	$q.="d.setValue(".$i.",0,'".date("Y M",$ds)."');";
	$n=1;
	foreach($cfp as $k => $v)
	{
		$r=mysql_fetch_row(mysql_query("SELECT COUNT(j_inv_id) FROM nse_invoice WHERE j_inv_date>".$ds." AND j_inv_date<".$de.$w." AND j_inv_from_person=".$k));
		$q.="d.setValue(".$i.",".$n.",".$r[0].");";
		$n++;
	}
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
foreach($cfp as $k => $v)
	echo "d.addColumn('number',\"".($k?J_Value("name","people","id",$k):"None")." (".$v.")\");";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c10'));c.draw(d,{width:890,height:320,legend:'right',title:'From Person Invoice Frequency',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";


// freq from reps
$i=0;
$de=0;
$q="";
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m)+$i,1,date($y));
	$de=mktime(0,0,0,date($m)+$i+1,1,date($y));
	$q.="d.setValue(".$i.",0,'".date("Y M",$ds)."');";
	$n=1;
	foreach($cfp as $k => $v)
	{
		$r=mysql_fetch_row(mysql_query("SELECT COUNT(j_inv_id) FROM nse_invoice WHERE j_inv_date>".$ds." AND j_inv_date<".$de.$w." AND j_inv_from_rep=".$k.(isset($_SESSION["j_user"]) && $_SESSION["j_user"]["role"]=="a"?" AND j_inv_from_person=".$_SESSION["j_user"]["id"]:"")));
		$q.="d.setValue(".$i.",".$n.",".$r[0].");";
		$n++;
	}
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
foreach($cfp as $k => $v)
	echo "d.addColumn('number',\"".($k?J_Value("name","people","id",$k):"None")." (".$v.")\");";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c11'));c.draw(d,{width:890,height:320,legend:'right',title:'From Rep Invoice Frequency',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";


// freq items
$i=0;
$de=0;
$q="";
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m)+$i,1,date($y));
	$de=mktime(0,0,0,date($m)+$i+1,1,date($y));
	$q.="d.setValue(".$i.",0,'".date("Y M",$ds)."');";
	$n=1;
	foreach($cii as $k => $v)
	{
		$r=mysql_fetch_row(mysql_query("SELECT COUNT(j_invit_id) FROM nse_invoice_item WHERE j_invit_date>".$ds." AND j_invit_date<".$de.$w." AND j_invit_inventory=".$k));
		$q.="d.setValue(".$i.",".$n.",".$r[0].");";
		$n++;
	}
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
foreach($cii as $k => $v)
	echo "d.addColumn('number',\"".($k?J_Value("j_invit_name","nse_invoice_item","j_invit_inventory",$k):"None")." (".$v.")\");";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c12'));c.draw(d,{width:890,height:320,legend:'right',title:'Invoice Inventory Item Frequency',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";


echo "<br>";
echo "<span id=c0></span>";
echo "<span id=c1></span>";
echo "<br><hr><br>";
echo "<span id=c2></span>";
echo "<span id=c3></span>";
echo "<br><hr><br>";
echo "<span id=c4></span>";
echo "<span id=c5></span>";
echo "<br><hr><br>";
echo "<span id=c6></span>";
echo "<span id=c7></span>";
echo "<br><hr><br>";
echo "<div id=c8></div>";
echo "<br><hr><br>";
echo "<div id=c9></div>";
echo "<br><hr><br>";
echo "<div id=c10></div>";
echo "<br><hr><br>";
echo "<div id=c11></div>";
echo "<br><hr><br>";
echo "<div id=c12></div>";
echo "<br><hr><br>";

if(isset($_GET["j_sent"]))
{
	if(isset($_GET["j_inw"]))
	{
		$J_title1="Sent Accounts Summary Report";
		$J_title3="<b>Criteria</b> ".($s?$s:"None").$f;
		$J_icon="<img src=".$ROOT."/ico/set/chart.png>";
		$J_label=19;
		$J_width=1024;
		$J_height=860;
		$J_nostart=1;
		include $SDR."/system/deploy.php";
	}
	echo "</body></html>";
}
else
	echo "<script>J_rep()</script>";
?>