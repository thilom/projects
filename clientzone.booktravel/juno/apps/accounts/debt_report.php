<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/accounts/debt_collection/debt_search.js></script>";

if(isset($_GET["s"])) {
	$d=$_SESSION["j_user"]["id"];
	echo "<body><script>J_inv_s(".(isset($_GET["eid"])?1:"").")</script></body></html>";
	die();
}

include $SDR."/apps/accounts/invoices/default.php";
echo "<body class=j_List>";
$s=$SDR."/stuff/people/".$_SESSION["j_user"]["id"]."/set.php";
if(is_file($s))include $s;
$s=(isset($j_invoiceAgeSearch)?$j_invoiceAgeSearch:"");

$role=$_SESSION["j_user"]["role"];

echo "<script>J_inv_N(";
echo "'".$s."'";
echo ",'".(isset($_GET["ds"])?$_GET["ds"]:date('d/m/Y',mktime(0,0,0,date("m")+1,date(1)-1,date("Y"))))."'";
echo ",".(isset($_GET["eid"])?"'".$_GET["eid"]."',\"".J_Value("","","",preg_replace("~[a-zA-Z0-9 ,:;'&!@#%+-_\$\(\)\-\.]~","",$_GET["eid"]))."\"":"0,0");
echo ",'".$role."'";
if(isset($_GET["ds"]))
	echo ",1";
echo ")</script>";

$J_home=3;
$J_title1="Debt Collection Report";
$J_icon="<img src=".$ROOT."/ico/set/loan_64.png>";
$J_label=19;
$J_width=1440;
$J_framesize=240;
$J_frame1=$ROOT."/apps/accounts/debt_collection/debt_report_start.php";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>