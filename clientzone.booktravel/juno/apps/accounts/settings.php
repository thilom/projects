<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
if(isset($_GET["s"]))
{
	die();
}
echo "<body class=j_List>";
echo "<blockquote>";

echo "<a href=go: onclick='J_U(this);return false'onfocus=blur() class=jf0>Currency</a>";
echo "<div>";
echo "<a href=go: onclick=\"jAs('general.php?t=7');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Add a currency for invoices')\">Add a Currency</a>";
echo "<a href=go: onclick=\"jAs('general.php?t=0');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set the default currency for invoices')\">Company Currency</a>";
echo "</div>";

echo "<a href=go: onclick='J_U(this);return false'onfocus=blur() class=jf0>Invoices</a>";
echo "<div>";
//echo "<a href=go: onclick=\"jAs('general.php?t=1');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set the VAT for invoices')\">Value Added Tax</a>";
echo "<a href=go: onclick=\"jAs('increment.php');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set the auto-increment of invoice numbers')\">Invoice Numbers</a>";
echo "<a href=go: onclick=\"jAs('invoice_defaults.php');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set default behavior and people for invoices')\">General Defaults</a>";
echo "<a href=go: onclick=\"jAs('general.php?t=2');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set the period after which invoices are locked from editing')\">Locks</a>";
echo "</div>";

echo "<a href=go: onclick='J_U(this);return false'onfocus=blur() class=jf0>Notifications</a>";
echo "<div>";
echo "<a href=go: onclick=\"jAs('noti.php?t=7');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set who gets notified')\">New Pro-Forma</a>";
echo "<a href=go: onclick=\"jAs('noti.php?t=7');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set who gets notified')\">Invoice Cancel</a>";
echo "<a href=go: onclick=\"jAs('noti.php?t=7');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set who gets notified')\">QA Cancel</a>";
echo "<a href=go: onclick=\"jAs('noti.php?t=7');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Set who gets notified')\">Assessment Cancel</a>";
echo "</div>";

echo "<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf0 onmouseover=\"J_TT(this,'These are extra graphic or info areas added to invoices')\">Elements</a>";
echo "<div>";
echo "<a href=go: onclick=\"jAs('elements.php?t=1');return false\" onfocus=blur() onmouseover=\"J_TT(this,'These are the top most graphical areas of invoices')\">Headers</a>";
echo "<a href=go: onclick=\"jAs('elements.php?t=2');return false\" onfocus=blur() onmouseover=\"J_TT(this,'These are terms and conditions found directly below the invoiced items area')\">Conditions</a>";
echo "<a href=go: onclick=\"jAs('elements.php?t=3');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Each invoice should have bank account details')\">Bank Accounts</a>";
echo "<a href=go: onclick=\"jAs('elements.php?t=4');return false\" onfocus=blur() onmouseover=\"J_TT(this,'The bottom most graphical area')\">Footers</a>";
echo "</div>";
echo "</blockquote>";
echo "<script>function jAs(a){J_frame(1,ROOT+'/apps/accounts/settings/'+a)}</script>";

$J_home=99;
$J_title1="Settings";
$J_icon="<img src=".$ROOT."/ico/set/set.png>";
$J_label=19;
$J_width=720;
$J_height=640;
$J_framesize=240;
$J_nomax=1;
$J_frame1=$ROOT."/apps/accounts/settings.php?s=1";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>