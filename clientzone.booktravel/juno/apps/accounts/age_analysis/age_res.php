<?php

/**
 * Draw an age analysis report
 *
 * @author Thilo Muller(2011)
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";

//Vars
$d = "x~code~name~d120~d90~d60~d30~curr~tot";
$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 100);
$r = '';
$search_data['company']= $_GET['company'];
$search_data['date']= $_GET['date'];
$search_data['type']= $_GET['type'];
$search_data['company']= $_GET['company'];
$search_data['min']= $_GET['min'];
$estab_count = 0;

// Display stuff
$dd = explode("~", trim($d, "~"));
$ddd = array();
$s = array();
foreach ($dd as $k => $v) {
	if (empty($v))
		continue;
	$s[$v] = 1;
	$ddd[$v] = 1;
}
$t = array("X", "code", "name", "d120","d90",'d60','d30','curr', 'tot');
$da = "";
$n = 1;
foreach ($t as $k => $v) {
	if (isset($ddd[$v])) {
		$da.=$n;
		$n++;
	}
	$da.="~";
}


/* PUT STUFF HERE */

//Invoice Search Date
list($day, $month, $year) = explode('/', $search_data['date']);
$search_date = mktime(23, 59, 59, $month, $day, $year);

//Get list of establishment for which there are invoices
$statement = "SELECT DISTINCT a.j_inv_to_company, b.establishment_name
			FROM nse_invoice AS a
			LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code";
if (isset($_GET['company']) && !empty($_GET['company'])) $statement .= " WHERE a.j_inv_to_company = '{$_GET['company']}'";
$statement .= " ORDER BY b.establishment_name";
$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
$sql_establishments->execute();
$sql_establishments->bind_result($establishment_code, $establishment_name);
while ($sql_establishments->fetch()) {
	$establishment_list[$establishment_code]['name'] = $establishment_name;
	$establishment_list[$establishment_code]['d120'] = 0;
	$establishment_list[$establishment_code]['d90'] = 0;
	$establishment_list[$establishment_code]['d60'] = 0;
	$establishment_list[$establishment_code]['d30'] = 0;
	$establishment_list[$establishment_code]['current'] = 0;
	$establishment_list[$establishment_code]['total'] = 0;
}
$sql_establishments->close();

//Prepare statement - get invoices
$statement = "SELECT a.j_inv_id, a.j_inv_date, a.j_inv_total, a.j_inv_paid, j_inv_number
			FROM nse_invoice AS a
			WHERE j_inv_to_company=? AND j_inv_type=? AND a.j_inv_date<?";
if ($search_data['type'] == '1') {
	$statement .= " AND (j_inv_cancel_date = 0 OR j_inv_cancel_date IS NULL OR j_inv_cancel_date = '')";
}
$sql_invoice = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Credit notes
$statement = "SELECT a. j_inv_reference, a.j_inv_total
			FROM nse_invoice AS a
			WHERE j_inv_to_company=? AND j_inv_type=3 AND a.j_inv_date<?";
$sql_credits = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Payments
$statement = "SELECT a.j_invp_invoice, a.j_invp_paid
			FROM nse_invoice_item_paid AS a
			LEFT JOIN nse_invoice AS b ON a.j_invp_invoice=b.j_inv_id
			WHERE a.j_invp_date<? AND b.j_inv_to_company=?";
$sql_payments = $GLOBALS['dbCon']->prepare($statement);



//Calculate invoice data
foreach ($establishment_list as $establishment_code=>$data) {
	if ($establishment_code == 'O') continue;


	//Payments
	$payments = array();
	$sql_payments->bind_param('ss', $search_date, $establishment_code);
	$sql_payments->execute();
	$sql_payments->store_result();
	$sql_payments->bind_result($payment_invoice_id, $invoice_payments);
	while ($sql_payments->fetch()) {
		if (isset($payments[$payment_invoice_id])) {
			$payments[$payment_invoice_id] += $invoice_payments;
		} else {
			$payments[$payment_invoice_id] = $invoice_payments;
		}
	}
	$sql_payments->free_result();

	//Credit Notes
	$credit_notes = array();
	$sql_credits->bind_param('ss', $establishment_code, $search_date);
	$sql_credits->execute();
	$sql_credits->bind_result($cr_reference, $cr_total);
	while ($sql_credits->fetch()) {
		$credit_notes[$cr_reference] = $cr_total;
	}
	$sql_credits->free_result();

	//Invoices
	$outstanding = 0;
	$sql_invoice->bind_param('sss', $establishment_code, $search_data['type'], $search_date);
	$sql_invoice->execute();
	$sql_invoice->bind_result($invoice_id, $invoice_date, $invoice_total, $invoice_paid, $invoice_number);
	while ($sql_invoice->fetch()) {
		$outstanding = $invoice_total;

		if (isset($credit_notes[$invoice_number])) {
			$outstanding += $credit_notes[$invoice_number];
		}
		if (isset($payments[$invoice_id])) {
			$outstanding -= $payments[$invoice_id];
		}
		$invoice_age = intval(($search_date-$invoice_date)/86400);
//		if ($establishment_code == '011401') echo "$invoice_age :: $outstanding<br>";

		if ($invoice_age < 30) {
			$establishment_list[$establishment_code]['current'] += $outstanding;
		} else if ($invoice_age<60) {
			$establishment_list[$establishment_code]['d30'] += $outstanding;
		} else if ($invoice_age<90) {
			$establishment_list[$establishment_code]['d60'] += $outstanding;
		} else if ($invoice_age<120) {
			$establishment_list[$establishment_code]['d90'] += $outstanding;
		} else {
//			if ($establishment_code == '011401') echo "$outstanding<br>";
			$establishment_list[$establishment_code]['d120'] += $outstanding;
		}


		$establishment_list[$establishment_code]['total'] += $outstanding;

		//Format number
		$establishment_list[$establishment_code]['current'] = number_format($establishment_list[$establishment_code]['current'],2,'.','');
		$establishment_list[$establishment_code]['d30'] = number_format($establishment_list[$establishment_code]['d30'],2,'.','');
		$establishment_list[$establishment_code]['d60'] = number_format($establishment_list[$establishment_code]['d60'],2,'.','');
		$establishment_list[$establishment_code]['d90'] = number_format($establishment_list[$establishment_code]['d90'],2,'.','');
		$establishment_list[$establishment_code]['d120'] = number_format($establishment_list[$establishment_code]['d120'],2,'.','');
		$establishment_list[$establishment_code]['total'] = number_format($establishment_list[$establishment_code]['total'],2,'.','');

	}
	$sql_invoice->free_result();
}

//Remove establishment with a balance of less than min
foreach ($establishment_list as $establishment_code=>$data) {
	if ($data['total'] < $search_data['min']) unset($establishment_list[$establishment_code]);
}

//Assemble for JS
$counter = 0;
$file_counter = 0;
foreach ($establishment_list as $establishment_code=>$data) {
	$r .= "$counter~$establishment_code~{$data['name']}~{$data['d120']}~{$data['d90']}~{$data['d60']}~{$data['d30']}~{$data['current']}~{$data['total']}|";
	//CSV
	if ($counter == 0) {

		if (isset($s['code'])) $headers[1] = 'Code';
		if (isset($s['name'])) $headers[2] = 'Establishment Name';
		if (isset($s['d120'])) $headers[3] = '+120 Days';
		if (isset($s['d90'])) $headers[4] = '90 Days';
		if (isset($s['d60'])) $headers[5] = '60 Days';
		if (isset($s['d30'])) $headers[6] = '30 Days';
		if (isset($s['curr'])) $headers[7] = 'Current';
		if (isset($s['tot'])) $headers[8] = 'Total Due';

		$fh = fopen($SDR."/apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv", 'w');
		fputcsv($fh, $headers);
		$file = "/juno/apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv";
        }

	$csv_data = array();
	if (isset($s['code'])) $csv_data[1] = $establishment_code;
	if (isset($s['name'])) $csv_data[2] = $data['name'];
	if (isset($s['d120'])) $csv_data[3] = $data['d120'];
	if (isset($s['d90'])) $csv_data[4] = $data['d90'];
	if (isset($s['d60'])) $csv_data[5] = $data['d60'];
	if (isset($s['d30'])) $csv_data[6] = $data['d30'];
	if (isset($s['curr'])) $csv_data[7] = $data['current'];
	if (isset($s['tot'])) $csv_data[8] = $data['total'];

        fputcsv($fh, $csv_data);
	$counter++;
}


$estab_count = count($establishment_list);

$r = str_replace("\r", "", $r);
$r = str_replace("\n", "", $r);
$r = str_replace("\"", "", $r);
$r = str_replace("~|", "|", $r);
//echo $r;
//print_r(implode(',',$list));

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/jsscripts/list_item_remover.js></script>";
echo "<script src=" . $ROOT . "/apps/accounts/age_analysis/res.js></script>";

if ($jL2 == 0) {
    $page = "<body style='color:#999;padding:30 40 40 160;background:url(/juno/ico/set/spreadsheet.png) no-repeat 40px 40px'><tt style=width:260><hr>CSV Files Created. Right click to save.<ul>$files</ul></tt></body></html>";
    echo $page;
} else {
	$msg = 'Age Analysis of ' . ($search_data['type']==2?'Tax Invoices':'Pro-formas') . ' Ending ' . date('d M Y', $search_date);
	echo "<script>J_in_r(\"$r\",\"$da\",$estab_count,$jL1,$jL2,'$file','$msg')</script>";
}





?>