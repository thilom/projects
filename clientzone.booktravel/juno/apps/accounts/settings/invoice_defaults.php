<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

if(count($_POST))
{
	foreach($_POST as $k => $v)
	{
		if(strpos($k,"invoice_default_")!==false)
		{
			mysql_query("DELETE FROM nse_variables WHERE variable_name='".$k."'");
			$q="INSERT INTO nse_variables (variable_name,variable_value) VALUES ('".$k."','".$v."')";
			mysql_query($q);
		}
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<style type=text/css>#jTR{width:75%}#jTR th{white-space:nowrap;width:20;color:#888}#jTR h4{color:#444}#jTR td input{width:99%}</style>";
echo "<body><tt><b>WARNING!</b> Some of these settings may be over-ridden by various factors</tt><br>";

$g=array(
				"invoice_default_increment"=>0
				,"invoice_default_from_company"=>0
				,"invoice_default_from_person"=>0
				,"invoice_default_assessor"=>0
				,"invoice_default_rep"=>0
				,"invoice_default_from_contact"=>0
				,"invoice_default_vat"=>0
				,"invoice_default_currency"=>""
				,"invoice_default_header"=>0
				,"invoice_default_condition"=>0
				,"invoice_default_bank"=>0
				,"invoice_default_foot"=>0
				,"invoice_default_order_increment"=>0
				,"invoice_default_order_contact"=>0
				,"invoice_default_order_header"=>0
				,"invoice_default_order_condition"=>0
				,"invoice_default_to_ad_person"=>0
				,"invoice_default_from_ad_person"=>0
				,"invoice_default_order_ads_condition"=>0
				,"invoice_default_creditnote_increment"=>0
				,"invoice_default_creditnote_head"=>0
				,"invoice_default_debitnote_increment"=>0
				,"invoice_default_debitnote_head"=>0
				,"invoice_default_statement_emailer"=>0
				);
$q="SELECT * FROM nse_variables WHERE variable_name LIKE 'invoice_default_%'";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($a=mysql_fetch_array($q))
		$g[$a["variable_name"]]=$a["variable_value"];
}

echo "<form method=post>";

echo "<table id=jTR>";
echo "<tr><th><h4>Invoice Defaults</h4></th></tr>";
echo "<tr><th>Default Increment</th><td><input type=text value=\"".J_Value("j_invinc_name","nse_invoice_increment","j_invinc_id",$g["invoice_default_increment"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_increment&c1=j_invinc_id&c2=j_invinc_name',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_increment value=".$g["invoice_default_increment"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up increments')\" onclick=\"jAs('increment.php')\"></th><tr>";
echo "<tr><th>Default From Company</th><td><input type=text value=\"".J_Value("","establishment","",$g["invoice_default_from_company"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=establishment',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_from_company value=".$g["invoice_default_from_company"]."></td><th></th><tr>";
echo "<tr><th>Default From Person</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_from_person"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=s',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_from_person value=".$g["invoice_default_from_person"]."></td><th></th><tr>";
echo "<tr><th>Default Assessor</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_assessor"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=ar',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_assessor value=".$g["invoice_default_assessor"]."></td><th></th><tr>";
echo "<tr><th>Default Rep</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_rep"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=ar',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_rep value=".$g["invoice_default_rep"]."></td><th></th><tr>";
echo "<tr><th>Default From Accounts</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_from_contact"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_from_contact value=".$g["invoice_default_from_contact"]."></td><th></th><tr>";
echo "<tr><th>Default VAT</th><td><input type=text name=invoice_default_vat value=".$g["invoice_default_vat"]."></td><th></th><tr>";
echo "<tr><th>Default Currency</th><td><input type=text value='".$g["invoice_default_currency"]."' onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_currency&c1=currency_code&c2=currency_name',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_currency value='".$g["invoice_default_currency"]."'></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up currency')\" onclick=\"jAs('general.php?t=7')\"></th><tr>";
echo "<tr><th>Default Header</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_header"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_header value=".$g["invoice_default_header"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up headers')\" onclick=\"jAs('elements.php?t=1')\"></th><tr>";
echo "<tr><th>Default Conditions</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_condition"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=2',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_condition value=".$g["invoice_default_condition"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up conditions')\" onclick=\"jAs('elements.php?t=2')\"></th><tr>";
echo "<tr><th>Default Bank</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_bank"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=3',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_bank value=".$g["invoice_default_bank"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up bank accounts')\" onclick=\"jAs('elements.php?t=3')\"></th><tr>";
echo "<tr><th>Default Footer</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_foot"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=4',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_foot value=".$g["invoice_default_foot"]." onclick=\"jAs('elements.php?t=4')\"></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up footers')\"></th><tr>";

echo "<tr><th><h4>Orders Defaults</h4></th></tr>";
echo "<tr><th>Order Increment</th><td><input type=text value=\"".J_Value("j_invinc_name","nse_invoice_increment","j_invinc_id",$g["invoice_default_order_increment"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_increment&c1=j_invinc_id&c2=j_invinc_name',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_order_increment value=".$g["invoice_default_order_increment"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up increments')\" onclick=\"jAs('increment.php')\"></th><tr>";
echo "<tr><th>Order To Accounts</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_order_contact"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_order_contact value=".$g["invoice_default_order_contact"]."></td><th></th><tr>";
echo "<tr><th>Order Header</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_order_header"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_order_header value=".$g["invoice_default_order_header"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up headers')\" onclick=\"jAs('elements.php?t=1')\"></th><tr>";
echo "<tr><th>Order Conditions</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_order_condition"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=2',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_order_condition value=".$g["invoice_default_order_condition"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up conditions')\" onclick=\"jAs('elements.php?t=2')\"></th><tr>";

echo "<tr><th><h4>Ad Order/Invoice Defaults</h4></th></tr>";
echo "<tr><th>Order To Person</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_to_ad_person"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_to_ad_person value=".$g["invoice_default_to_ad_person"]."></td><th></th><tr>";
echo "<tr><th>Order From Person</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_from_ad_person"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_from_ad_person value=".$g["invoice_default_from_ad_person"]."></td><th></th><tr>";
echo "<tr><th>Ads Conditions</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_order_ads_condition"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=2',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_order_ads_condition value=".$g["invoice_default_order_ads_condition"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up conditions')\" onclick=\"jAs('elements.php?t=2')\"></th><tr>";

echo "<tr><th><h4>Credit Notes Defaults</h4></th></tr>";
echo "<tr><th>Credit Increment</th><td><input type=text value=\"".J_Value("j_invinc_name","nse_invoice_increment","j_invinc_id",$g["invoice_default_creditnote_increment"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_increment&c1=j_invinc_id&c2=j_invinc_name',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_creditnote_increment value=".$g["invoice_default_creditnote_increment"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up increments')\" onclick=\"jAs('increment.php')\"></th><tr>";
echo "<tr><th>Credit Header</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_creditnote_head"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_creditnote_head value=".$g["invoice_default_creditnote_head"]." onclick=\"jAs('elements.php?t=1')\"></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up headers')\"></th><tr>";

echo "<tr><th><h4>Debit Notes Defaults</h4></th></tr>";
echo "<tr><th>Debit Increment</th><td><input type=text value=\"".J_Value("j_invinc_name","nse_invoice_increment","j_invinc_id",$g["invoice_default_debitnote_increment"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_increment&c1=j_invinc_id&c2=j_invinc_name',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_debitnote_increment value=".$g["invoice_default_debitnote_increment"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up increments')\" onclick=\"jAs('increment.php')\"></th><tr>";
echo "<tr><th>Debit Header</th><td><input type=text value=\"".J_Value("j_invele_title","nse_invoice_element","j_invele_id",$g["invoice_default_debitnote_head"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=nse_invoice_element&c1=j_invele_id&c2=j_invele_title&where=j_invele_type=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_debitnote_head value=".$g["invoice_default_debitnote_head"]."></td><th><input type=button value=&#9650; onmouseover=\"J_TT(this,'Set-up headers')\" onclick=\"jAs('elements.php?t=1')\"></th><tr>";

echo "<tr><th><h4>Statements</h4></th></tr>";
echo "<tr><th>Email Person</th><td><input type=text value=\"".J_Value("","people","",$g["invoice_default_statement_emailer"])."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this)><input type=hidden name=invoice_default_statement_emailer value=".$g["invoice_default_statement_emailer"]."></td><th></th><tr>";
echo "</table><br>";
echo "<input type=submit value=OK>";
echo "</form>";

echo "<iframe id=j_IF></iframe>";
echo "<script>J_tr()</script>";
echo "<script>function jAs(a){J_frame(1,ROOT+'/apps/accounts/settings/'+a)}</script>";
echo "</body></html>";
?>