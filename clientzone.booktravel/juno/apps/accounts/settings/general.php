<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/dir.php";
include $SDR."/system/get.php";

$f="/apps/accounts/invoices/default.php";
J_SetPerm($f);
$c=file_get_contents($SDR.$f);

if(count($_POST))
{
	if($_GET["t"]==7)
	{
		function strip($v="")
		{
			return str_replace(array("\"","'"),"",$v);
		}
		function stripcode($v="")
		{
			return preg_replace("~[^A_Z]~","",strtoupper($v));
		}

		foreach($_POST as $k => $v)
		{
			if(substr($k,"O_"))
			{
				mysql_query("UPDATE nse_currency SET currency_code='".stripcode($_POST["o_c".$v])."',currency_symbol='".$_POST["o_s".$v]."',currency_country='".$_POST["o_l".$v]."',currency_number='".$_POST["o_n".$v]."' WHERE currency_id=".$v);
			}
			elseif(substr($k,"N_") && $v)
			{
				$sql="INSERT INTO nse_currency (currency_code,currency_symbol,currency_country,currency_number) VALUES ('".stripcode($_POST["n_c".$v])."','".$_POST["n_s".$v]."','".$_POST["n_l".$v]."','".$_POST["n_n".$v]."')";
				mysql_query($sql);
			}
		}
	}
	else
	{
		unset($_SESSION["juno"]["accounts"]);
		include $SDR."/system/parse.php";
		$t=$_GET["t"];
		$r=array("company_currency","default_vat","default_lock","default_company","default_contact","default_assessor","default_company_info","currency");
		$r=$r[$t];
		$v=isset($_POST["v"])?$_POST["v"]:"";
		if($t==1)$v=J_floatVal($v);
		elseif($t==2)$v=J_intVal($v);
		if(!$t || $t==6 || is_numeric($v))
		{
			if($t==6)
			{
				$v="";
				foreach($_POST as $k => $s)
					$v.=($k=="name"?"":"~").preg_replace('~[^a-zA-Z0-9 &@!<>:;+#/\'\?\.\,\-\(\)]~',"",str_replace("\n","<br>",str_replace("~","-",$s)));
				$v="\"0~".$v."\"";
			}
			elseif($t==2 && $v<0)$v=0-$v;
			elseif($t==1 && $v<0)$v=0-$v;
			else $v=(is_numeric($v)?$v:'"'.$v.'"');
			if($o=fopen($SDR.$f,"w+"))
			{
				$c=preg_replace('~'.$r.'=.*?;~',$r.'='.$v.';',$c);
				$o=fopen($SDR.$f,"w+");
				fwrite($o,$c);
				fclose($o);
			}
		}
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body>";

$h=array("Currency","VAT","Time Lock","Company","Contact","Assessor","Company Info","Add a Currency");
echo "<h4>Default ".$h[$_GET["t"]]."</h4><hr><form method=post>";

if($_GET["t"]==7)
{
	echo "<tt>Get all the currencies at <a href=http://en.wikipedia.org/wiki/ISO_4217 target=_blank onfocus=blur()>www.wikipedia.org</a></tt><br>";
	echo "<table id=jTR><tr class=j_hed><th>Code</th><th>Symbol</th><th>Name</th><th>Counties</th><th>Number</th></tr>";
	$q="SELECT * FROM nse_currency ORDER BY currency_code";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
			echo "<tr><th><input type=hidden name=O_d value='".$g["currency_id"]."' title='Requires value'><input type=text name=o_c value='".$g["currency_code"]."' size=4 title='Requires value'></th><th><input type=text name=o_s value='".$g["currency_symbol"]."' size=3 title='Requires value'></th><td><input type=text name=o_n value='".$g["currency_name"]."' title='Requires value'></td><td><input type=text name=o_l value='".$g["currency_country"]."' class=jW100></td><td><input type=text name=o_o value='".$g["currency_number"]."' size=4></td></tr>";
	}
	echo "<tr><th><input type=text name=N_c size=4></th><th><input type=text name=n_s size=3></th><td><input type=text name=n_n></td><td><input type=text name=n_l sclass=jW100></td><td><input type=text name=n_o size=4></td></tr>";
	echo "</table><br>";
	echo "<script>J_tr();J_validate</script>";
}

elseif($_GET["t"]==6)
{
	$o=explode("~",str_replace("<br>","\n",J_Extract($c,'default_company_info="(.*?)";')));
	echo "<tt>If there is no default company, then use this form to add this company information to all invoices</tt><hr>";
	echo "<form method=post>";
	echo "<table width=300>";
	echo "<tr><td width=1>Name</td><td><input type=text value=\"".(isset($o[0])?$o[0]:"")."\" name=name class=jW100><td><tr>";
	echo "<tr><td>City</td><td><input type=text value=\"".(isset($o[1])?$o[1]:"")."\" name=city class=jW100><td><tr>";
	echo "<tr><td>Email</td><td><input type=text value=\"".(isset($o[2])?$o[2]:"")."\" name=email class=jW100><td><tr>";
	echo "<tr><td>Tel</td><td><input type=text value=\"".(isset($o[3])?$o[3]:"")."\" name=tel class=jW100><td><tr>";
	echo "<tr><td>Fax</td><td><input type=text value=\"".(isset($o[4])?$o[4]:"")."\" name=fax class=jW100><td><tr>";
	echo "<tr><td>Postal</td><td><textarea name=postal rows=4 class=jW100>".(isset($o[5])?$o[5]:"")."</textarea><td><tr>";
	echo "<tr><td>Address</td><td><textarea name=address rows=4 class=jW100>".(isset($o[6])?$o[6]:"")."</textarea><td><tr>";
	echo "<tr><td>Code</td><td><input type=text value=\"".(isset($o[7])?$o[7]:"")."\" name=code class=jW100><td><tr>";
	echo "<tr><td>Reg No.</td><td><input type=text value=\"".(isset($o[8])?$o[8]:"")."\" name=regno class=jW100><td><tr>";
	echo "<tr><td>Tax No.</td><td><input type=text value=\"".(isset($o[9])?$o[9]:"")."\" name=taxno class=jW100><td><tr>";
	echo "</table>";
}

elseif($_GET["t"]==5)
{
	echo "<tt>This person will always be loaded into the assessor field of new invoices.</tt><hr>";
	$o1=preg_replace('~[^0-9]~s',"",J_Extract($c,'default_assessor=(.*?);'));
	$o2=($o1>0?J_Value("firstname~lastname|town_name|nse_nlocations_towns|town_id| (^)","nse_user","user_id",$o1):"''");
	echo "<input type=text".($o2?" value=\"".$o2."\"":"")." onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email',320,4) onclick=J_ajax_C(this) ondblclick=J_ajax_A(this,'/apps/people/edit_person.php?id=0')><input type=hidden name=v value=".($o1?$o1:0).">";
}

elseif($_GET["t"]==4)
{
	echo "<tt>This person will always be loaded into the 'contact' field of new invoices. This is a secondary contact person other than the assessor - ie: accounts department or admin.</tt><hr>";
	$o1=preg_replace('~[^0-9]~s',"",J_Extract($c,'default_contact=(.*?);'));
	$o2=($o1>0?J_Value("firstname~lastname|town_name|nse_nlocations_towns|town_id| (^)","nse_user","user_id",$o1):"''");
	echo "<input type=text".($o2?" value=\"".$o2."\"":"")." onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email',320,4) onclick=J_ajax_C(this) ondblclick=J_ajax_A(this,'/apps/people/edit_person.php?id=0')><input type=hidden name=v value=".($o1?$o1:0).">";
}

elseif($_GET["t"]==3)
{
	echo "<tt>This company will always be loaded into the 'from' field of new invoices.</tt><hr>";
	$o1=preg_replace('~[^0-9]~s',"",J_Extract($c,'default_company=(.*?);'));
	$o2=($o1>0?J_Value("name","establishment","id",$o1):"''");
	echo "<input type=text".($o2?" value=\"".$o2."\"":"")." onkeyup=J_ajax_K(this,'/system/ajax.php?t=establishment',320,4) onclick=J_ajax_C(this) ondblclick=J_ajax_A(this,'/apps/company/edit.php')><input type=hidden name=v value=".($o1?$o1:0).">";
}

elseif($_GET["t"]==2)
	echo "<tt>The amount of days to elapse before an invoice locks - afterwhich only admin (level 8 users) may edit or unlock invoices. If you dont want to lock invoices, set to 0</tt><hr><input type=text name=v value='".str_replace(array('"',"'"),"",J_Extract($c,'default_lock=(.*?);'))."' size=4> Days";
else if($_GET["t"]==1)
	echo "<input type=text name=v value='".str_replace(array('"',"'"),"",J_Extract($c,'default_vat=(.*?);'))."' size=4>%";
echo "<iframe id=j_IF></iframe>";
echo " <input type=submit value=OK></form>";

echo "</body></html>";
?>