<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

if(isset($_GET["del"]))
{
	$c=array("","head","conditions","bank","foot");
	$cnt=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_invoice WHERE j_inv_".$c[$_GET["t"]]."=".$_GET["del"]." LIMIT 1"));
	$in=$cnt[0];
	if(!$in)
	{
		$_POST["del"]=$_GET["del"];
		$_POST["t"]=$_GET["t"];
	}
}

if(count($_POST))
{
	include_once $SDR."/system/get.php";
	if(count($_POST))unset($_SESSION["juno"]["accounts"]);
	$a=0;
	if(isset($_POST["del"]))
	{
		$t=array("","j_inv_head","j_inv_conditions","j_inv_bank","j_inv_foot");
		$p=$t[$_POST["t"]];
		if(isset($_POST["r"]))$a=$_POST["r"];
		else
		{
			$a=J_Value("j_invele_id","nse_invoice_element",0,0,"j_invele_default=1 AND j_invele_type=".$_POST["t"]);
		}
		mysql_query("DELETE FROM nse_invoice_element WHERE j_invele_id=".$_POST["del"]);
		mysql_query("UPDATE nse_invoice SET ".$p."=".($a?$a:0).",j_invele_company=".$_POST["c"]." WHERE ".$t."=".$_POST["del"]);
		die(header("Location: ".$ROOT."/apps/accounts/invoices/settings_elements.php?t=".$_POST["t"]));
	}
	else
	{
		function stripit($v="")
		{
			$v=str_replace("\r","",$v);
			$v=str_replace("\t","",$v);
			$v=preg_replace('@[~|]@s',"",$v);
			$v=str_replace("\n","<br>",$v);
			$v=addslashes($v);
			return $v;
		}
		$a=(isset($_POST["d"])?1:0);
		if($_POST["id"])
		{
			$s="UPDATE nse_invoice_element SET j_invele_title='".stripit($_POST["n"])."',j_invele_text='".stripit($_POST["h"])."',j_invele_company=".$_POST["c"].",j_invele_default=".$a." WHERE j_invele_id=".$_POST["id"];
			mysql_query($s);
			$d=$_POST["id"];
		}
		else
		{
			$s="INSERT INTO nse_invoice_element (j_invele_title,j_invele_text,j_invele_type,j_invele_default,j_invele_company) VALUES ('".stripit($_POST["n"])."','".stripit($_POST["h"])."',".$_POST["t"].",".$a.",".$_POST["c"].")";
			mysql_query($s);
			$d=J_ID("nse_invoice_element","j_invele_id");
		}
		if($a)mysql_query("UPDATE nse_invoice_element SET j_invele_default=0 WHERE j_invele_type=".$_GET["t"]." AND j_invele_id!=".$d);
		unset($_GET["e"]);
	}
	if($a)
	{
		include $SDR."/system/dir.php";
		$f="/apps/accounts/invoices/edit.php";
		J_SetPerm($f);
		$c=file_get_contents($SDR.$f);
		if($o=fopen($SDR.$f,"w+"))
		{
			$v=array("","default_head","default_condition","default_bank","default_foot");
			$v=$v[$_REQUEST["t"]];
			$c=preg_replace('~'.$v.'=.*?;~',$v.'='.$d.';',$c);
			$o=fopen($SDR.$f,"w+");
			fwrite($o,$c);
			fclose($o);
		}
	}
}

include_once $SDR."/system/get.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<link rel=stylesheet href=".$ROOT."/apps/accounts/invoices/style.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

$h=array("","Header","Condition","Bank Account","Footer");

if(isset($_GET["del"]))
{
	$item=J_Value("j_invele","nse_invoice_element","j_invele_id",$_GET["del"]);
	$in=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_invoice WHERE j_inv_".$c[$_GET["t"]]."=".$_GET["del"]." LIMIT 1"));
	echo "<body><h4>Delete ".$h[$_GET["t"]]." ( ".$item." )</h4><hr><tt><b>There are ".$in[0]." invoice".($in[0]!=1?"s":"")." using this element</b><br>By deleting ".$h[$_GET["t"]]." will affect invoices currently using this element. You can opt to replace the deleted ".$h[$_GET["t"]]." in invoices that using it. If not the default will be inserted into these items. If you are deleteing an element that is the default, then go back and assign a new default and only then delete this item.</tt><hr>";
	echo "<form method=post>";
	$r=mysql_query("SELECT * FROM nse_invoice_element WHERE j_invele_type=".$_GET["t"]." AND j_invele_id!=".$_GET["del"]." ORDER BY j_invele_title");
	if(mysql_num_rows($r))
	{
		echo "<select name=r>";
		while($g=mysql_fetch_array($r))
		{
			echo "<option value=j_ed(".$g["j_invele_id"].")>".$g["j_invele_title"];
		}
		echo "</select><hr>";
		echo "<input type=hidden name=del value=".$_GET["del"].">";
		echo "<input type=hidden name=t value=".$_GET["t"].">";
		echo "<input type=submit value=OK> <input type=button onclick=history.go(-1) value=Cancel>";
		echo "</form>";
	}
}
elseif(isset($_GET["e"]))
{
	$d=$_GET["e"];
	$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_element WHERE j_invele_id=".$d." LIMIT 1"));
	echo "<body onload=J_tr();J_validate(self)>";
	echo "<h4>".($_GET["e"]?"Edit":"Add")." ".$h[$_GET["t"]]."s</h4><hr>";
	echo "<form method=post>";
	echo "<input type=hidden name=id value=".$_GET["e"].">";
	echo "<input type=hidden name=t value=".$_GET["t"].">";
	echo "<table id=jTR width=100%>";
	echo "<tr><td>Company</td><th width=95%><input type=text value=\"".($d?J_Value("name","establishment","id",$g["j_invele_company"]):"")."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=company',320,4) onclick=J_ajax_C(this) class=jW100><input type=hidden name=c value=".($d?$g["j_invele_company"]:0)."></th></tr>";
	echo "<tr><td>Name</td><th><input type=text name=n value=\"".($d?stripslashes($g["j_invele_title"]):"")."\" title='Requires value' class=jW100></th></tr>";
	echo "<tr><td>Content</td><th><textarea name=h title='Requires value' rows=8 class=jW100>".($d?stripslashes(str_replace("<br>","\n",$g["j_invele_text"])):"")."</textarea></th></tr>";
	echo "<tr><td>Default</td><th><input type=checkbox name=d value=1".($d&&$g["j_invele_default"]?" checked":"")."></th></tr>";
	echo "</table><hr>";
	echo "<input type=submit value=OK>".($_GET["e"]?" <input type=button value=Delete onclick=\"if(confirm('WARNING!\\nPermanently remove conditions from all invoices?'))location=ROOT+'/apps/accounts/invoices/settings_elements.php?del=".$_GET["e"]."&t=".$_GET["t"]."'\">":"")." <input type=button value=Back onclick=\"location=ROOT+'/apps/accounts/invoices/settings_elements.php?t=".$_GET["t"].($_GET["e"]?"&id=".$_GET["e"]:"")."'\">";
	echo "</form>";
	echo "<iframe id=j_IF></iframe>";
}
else
{
	echo "<style type=text/css>tr{cursor:pointer}</style>";
	echo "<script>function j_ed(v){J_opaq();location=ROOT+'/apps/accounts/invoices/settings_elements.php?e='+v+'&t=".$_GET["t"]."'}</script>";
	echo "<body onload=J_tr()>";
	echo "<h4>".$h[$_GET["t"]]."s</h4><hr>";
	$r=mysql_query("SELECT * FROM nse_invoice_element WHERE j_invele_type=".$_GET["t"]." ORDER BY j_invele_company,j_invele_title");
	if(mysql_num_rows($r))
	{
		echo "<table id=jTR>";
		while($g=mysql_fetch_array($r))
		{
			$c=J_Value("name","establishment","id",$g["j_invele_company"]);
			echo "<tr onclick=j_ed(".$g["j_invele_id"].")><td>".($c?$c." ~ ":"")."</td><td>".($g["j_invele_default"]?"<b onmouseover=\"J_TT(this,'<b>Default</b>This element will be automatically inserted into new invoices')\">".$g["j_invele_title"]."</b>":$g["j_invele_title"])."</td></tr>";
		}
		echo "</table><hr>";
	}
	echo "<input type=button value=&#10010; onclick=j_ed(0)>";
	echo "<script src=".$ROOT."/apps/accounts/invoices/default.js></script>";
}

echo "</body></html>";
?>