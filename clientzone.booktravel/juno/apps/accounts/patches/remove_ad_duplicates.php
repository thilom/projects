<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$i=0;

// get all invoices
$q="SELECT * FROM nse_invoice ORDER BY j_inv_type";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{

		$store_payment=array();
		$store_invoice=0;

		// invoices & pro-formas
		if($g["j_inv_type"]<6)
		{
			// get all inner invoice items - that are related to ads
			$q1="SELECT * FROM nse_invoice_item JOIN nse_inventory ON j_invit_inventory=j_in_id WHERE j_invit_invoice=".$g["j_inv_id"]." AND (j_in_category=3 OR j_in_category=4) LIMIT 1";
			$q1=mysql_query($q1);
			// for checks later
			if(mysql_num_rows($q1))
			{
					// check if there is an ad with this invoice number
					$q1="SELECT * FROM nse_ads WHERE invoice_id=".$g["j_inv_id"];
					$q1=mysql_query($q1);
					if(mysql_num_rows($q1))
						$store_invoice=$g["j_inv_id"];
					else
					{
						echo $i.") INV:  ".$g["j_inv_number"]." / EST: ".$g["j_inv_to_company"]." (".$g["j_inv_id"].")<hr>";
						$i++;
						mysql_query("DELETE FROM nse_invoice WHERE j_inv_id=".$g["j_inv_id"]);
						mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_invoice=".$g["j_inv_id"]);
						// see if there is a payment on this invoice
						$q2="SELECT * FROM nse_invoice WHERE j_inv_order_id=".$g["j_inv_id"];
						$q2=mysql_query($q2);
						if(mysql_num_rows($q2))
						{
							$g2=mysql_fetch_array($q2);
							echo "<b style=color:blue>".$i.") INV:  ".$g["j_inv_number"]." / EST: ".$g["j_inv_to_company"]." (".$g["j_inv_id"].")</b><hr>";
							// at the time i did this there where no payments marked for deletion
							//mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_invoice=".$g["j_inv_id"]);
						}
						if($g["j_inv_invoice_id"]) // if associated order
						{
							mysql_query("DELETE FROM nse_invoice WHERE j_inv_id=".$g["j_inv_invoice_id"]);
							mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_invoice=".$g["j_inv_invoice_id"]);
							mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_invoice=".$g["j_inv_invoice_id"]);
							echo "<div style=color:red>".$i.") INV:  ".$g["j_inv_number"]." / EST: ".$g["j_inv_from_company"]." (".$g["j_inv_id"].")</div><hr>";
							$i++;
						}
					}

			}
		}
		if($g["j_inv_type"]==9) // unlinked orders - have no associated invoice
		{
			$q1="SELECT * FROM nse_invoice WHERE j_inv_order_id=".$g["j_inv_id"]." LIMIT 1";
			$q1=mysql_query($q1);
			if(!mysql_num_rows($q1))
			{
				mysql_query("DELETE FROM nse_invoice WHERE j_inv_id=".$g["j_inv_id"]);
				mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_invoice=".$g["j_inv_id"]);
				mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_invoice=".$g["j_inv_id"]);
				echo "<div style=color:red>".$i.") INV:  ".$g["j_inv_number"]." / EST: ".$g["j_inv_from_company"]." (".$g["j_inv_id"].")</div><hr>";
				$i++;
			}
		}

	}
}

?>