<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

$id=isset($_POST["id"])?$_POST["id"]:(isset($_GET["id"])?$_GET["id"]:0);

$adb=mysql_fetch_array(mysql_query("SELECT * FROM nse_ads WHERE ad_id=".$id." LIMIT 1"));
$eid=$adb["establishment_code"];
$est=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$eid."' LIMIT 1"));
$pub=mysql_fetch_array(mysql_query("SELECT * FROM nse_inventory LEFT JOIN nse_inventory_publications ON j_in_id=j_inp_inventory WHERE j_in_id=".$adb["inventory_id"]." LIMIT 1"));

if(count($_POST))
{
	include $SDR."/apps/ad_manager/f/ads_history.php";
	include_once $SDR."/system/activity.php";

	$ad_pdf_low = '';

	$g=mysql_fetch_array(mysql_query("SELECT firstname,lastname,email FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1"));
	$from=array($g["email"],ucwords(strtolower($g["firstname"]." ".$g["lastname"])));
	$to=array();
	$i=0;
	foreach($_POST as $k => $v)
	{
		if(strpos($k,"a_")!==false)
		{
			if($v)
			{
				if(is_numeric($v))
					$to[$i]=array(J_Value("email","nse_user","user_id",$v),J_Value("","people","",$v));
				else
				{
					$n=$v;
					$q="SELECT firstname,lastname FROM nse_user WHERE email='".$v."'";
					$q=mysql_query($q);
					if(mysql_num_rows($q))
					{
						$e=mysql_fetch_array($q);
						$n=$e["firstname"]." ".$e["lastname"];
					}
					$to[$i]=array($v,$n);
				}
				$i++;
			}
		}
	}
	$ad_strip_pdf=(isset($_POST["pdftopstrip"])&&file_exists($SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_strip_pdf"])?$SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_strip_pdf"]:0);
	$ad_pdf=(isset($_POST["pdfad"])&&file_exists($SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_pdf"])?$SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_pdf"]:0);
	if (isset($_POST["pdfadlow"])) {
		$lowpdf = str_replace('.pdf','_low.pdf',$adb['last_pdf']);
		if (file_exists($SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$lowpdf)) {
			$ad_pdf_low = $SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$lowpdf;
		}
	}
	$order_number="";
	$order_date="";
	$order_pdf=0;
	if(isset($_POST["order"]))
	{
		$s="SELECT * FROM nse_invoice WHERE j_inv_id=".$adb["order_id"]." LIMIT 1";
		$s=mysql_query($s);
		if(mysql_num_rows($s))
		{
			$g=mysql_fetch_array($s);
			$order_number=$g["j_inv_order"];
			$order_date=($g["j_inv_date"]?date("d/m/Y",$g["j_inv_date"]):"");
			$f=$SDR."/stuff/accounts/pdf_invoice/".$order_number."_".$g["j_inv_id"].".pdf";
			$order_pdf=file_exists($f)?$f:0;
			if(!$order_pdf)
			{
				$xid=$g["j_inv_id"];
				include_once $SDR."/apps/accounts/invoices/pdf_invoice.php";
				$order_pdf=file_exists($f)?$f:0;
			}
		}
	}
	$certificate=0;
	if(isset($_POST["certificate"]))
	{
		$q="SELECT assessment_id FROM nse_establishment_assessment WHERE establishment_code='".$eid."' AND assessment_status='complete' ORDER BY assessment_date DESC LIMIT 1";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			echo "<br><blockquote>";
			include $SDR."/apps/assessment_manager/f/create_functions.php";
			$g=mysql_fetch_array($q);
			$f=create_certificate($g["assessment_id"]);
			if($f)
				$certificate=DOC_ROOT.$f;
		}
	}
	include $SDR."/custom/email_headers.php";

//	$subject="ACCOMMODATION DIRECTORY 2012 (".$est["establishment_name"].")";
	$subject=$pub["j_in_name"]." (".$est["establishment_name"].")";
	$m=preg_replace('~[^a-zA-Z0-9 ,;:"@#_<>+=%&\*\-\$\'\(\)\[\]\?\.]~',"",strip_tags(str_replace("\n","<br>",str_replace("\r","",$_POST["m"])),"<br>"));

	$msg=$j_email_header;
	$msg.="<b style='font-size:9pt'>".$est["establishment_name"]."</b><br>";
	$msg.="<b>".$pub["j_in_name"]."</b><hr>";
	$msg.=($m?$m."<br><br><hr>":"");
	$msg.="<div style='font-size:9pt'>";
	if($ad_strip_pdf)
		$msg.="<b>See Attached AD TOP-STRIP PDF (".$adb["last_strip_pdf"].")</b><br>";
	if($ad_pdf)
		$msg.="<b>See Attached AD PDF (".$adb["last_pdf"].")</b><br>";
	if($ad_pdf_low)
		$msg.="<b>See Attached AD PDF (".$lowpdf.")</b><br>";
	if($order_pdf)
		$msg.="<b>See Attached AD ORDER PDF (".$order_number.")</b><br>";
	if($certificate)
		$msg.="<b>See Attached QA CERTIFICATE</b><br>";
	if(isset($_POST["rates1"]))
		$msg.="<b>See Attached Rate-Sheet</b><br>";
	if($ad_pdf|| $ad_pdf_low || $order_pdf||$certificate||isset($_POST["rates1"]))
		$msg.="<hr>";
	$msg.="<b>Sent by:</b><br>";
	$msg.="<a href='mailto:".$from[0]."' onfocus=blur()>".$from[1]."</a><br>";
	if(count($to)>1)
	{
		$msg.="<b>Also sent to:</b><br>";
		foreach($to as $k => $v)
			$msg.="<a href='mailto:".$v[0]."' onfocus=blur()>".$v[1]."</a><br>";
	}
	$msg.="</div>";
	$msg.="<hr>";
	$msg.="<br><br><br>";
	$msg.=$j_email_footer;
	$msg=str_replace("\n","<br>",$msg);

	require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
	$mail=new PHPMailer(true);
	try
	{
		$mail->AddReplyTo($from[0],$from[1]);
		$mail->SetFrom($from[0],$from[1]);
		foreach($to as $k => $v)
		{
			$mail->AddAddress($v[0],$v[1]);
			adHist($id,"Sent to: ".$v[1]." (".$v[0].")");
			J_act("Ad",7,$pub["j_in_name"],$id,$est["establishment_code"]);
		}
		$mail->Subject=$subject;
		$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
		$mail->MsgHTML($msg);
		if($ad_strip_pdf)
			$mail->AddAttachment($ad_strip_pdf);
		if($ad_pdf)
			$mail->AddAttachment($ad_pdf);
		if($ad_pdf_low)
			$mail->AddAttachment($ad_pdf_low);
		if($order_pdf)
			$mail->AddAttachment($order_pdf);
		if($certificate)
			$mail->AddAttachment($certificate);
		if(isset($_POST["rates1"]))
			$mail->AddAttachment($SDR."/apps/ad_manager/f/attach/Icons_and_Rates.pdf");
		$mail->Send();
		echo "Message Sent OK";
		echo "<script>setTimeout(\"window.parent.J_WX(self)\",1000)</script>";
		die();
	}
	catch (phpmailerException $e){echo $e->errorMessage();}
	catch (Exception $e){echo $e->getMessage();}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body style=margin:16>";
echo "<form method=post>";
echo "<table width=100% height=100% cellspacing=0>";
echo "<tr><td height=20><tt>";

$c=array();
$i=0;
$p="";

$q="SELECT variable_value,firstname,lastname FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='ad_editor' ORDER BY firstname,lastname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		$v=$g["variable_value"];
		if($v && !isset($c[$v]))
		{
			$c[$v]=1;
			$p.="<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=a_".$i." value=".$v."> ".ucwords(strtolower($g["firstname"]." ".$g["lastname"]))."</tt>";
			$i++;
		}
	}
}

if($i)
	echo "<b>AA TRAVEL GUIDES</b>".$p."<br>";

$q=mysql_query("SELECT * FROM nse_ads_people AS a
 LEFT JOIN nse_user AS u ON a.adpar_person=u.user_id
 LEFT JOIN nse_user_establishments AS e ON a.adpar_person=e.user_id
  WHERE
 a.adpar_establishment='".$eid."'
 ORDER BY u.firstName,u.lastname,a.adpar_nondb
");
if(mysql_num_rows($q))
{
	echo "<b>Establishment</b>";
	while($g=mysql_fetch_array($q))
	{
		if($g["adpar_person"])
		{
			if(!isset($c[$g["adpar_person"]]))
			{
				$c[$g["adpar_person"]]=1;
				echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=a_".$i." value=\"".$g["adpar_person"]."\"> ".$g["firstname"]." ".$g["lastname"].($g["designation"]?" (".$g["designation"].")":"")."</tt>";
			}
		}
		elseif($g["adpar_nondb"])
		{
			$a=explode(" ",$g["adpar_nondb"]);
			$n="";$e="";
			foreach($a as $k => $v)
			{
				if($v)
				{
					if(strpos($v,"@")!==false)
						$e=$v;
					else
						$n.=$v." ";
				}
			}
			if($e)
				echo "<input type=checkbox name=a_".$i." value=\"".$e."\"> ".($n?trim($n):$e)."<br>";
		}
		$i++;
	}
	echo "<br>";
}
echo "<b>Email</b><input type=text name=a_".$i." class=jW100></tt><br></td></tr>";

echo "<tr><td height=95%><tt><b>Message</b></tt><textarea id=message name=m rows=12 class=jW100 style=height:100%></textarea><tt class=jO50><a href=go: onclick=\"J_W(ROOT+'/apps/system/email/f/edit_item.php?j_Wr='+j_W+'&file=/apps/ad_manager/f/attach/letter".(strpos($pub["j_in_name"],"SA Best")!==false?"_sabest":"1").".php&pub_name=".str_replace(" ","~",str_replace("&","+++",preg_replace("~[^a-zA-Z0-9 :@!&\(\)\[\]\.\-]~","",strtoupper($pub["j_in_name"]))))."');return false\" onfocus=blur() style=text-decoration:none>&#9650; <b>Insert Cover Letter</b></a></tt></td></tr>";

echo "<tr><td><br><br><tt><b>Attach</b></tt>";

if($adb["last_strip_pdf"] && file_exists($SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_strip_pdf"]))
	echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=pdftopstrip value=1 checked> PDF Top-strip</tt>";
if($adb["last_pdf"] && file_exists($SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_pdf"]))
	echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=pdfad value=1> PDF Ad (High Res)</tt>";
	echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=pdfadlow value=1 checked> PDF Ad Low Res)</tt>";
echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=rates1 value=1> Rate Sheet</tt>";
echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=certificate value=1> QA Certificate</tt>";
echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=order value=1> Order</tt>";
echo "<hr></td></tr>";

echo "<tr><td><input type=submit value=OK></td></tr></table>";
echo "</form>";
echo "</body></html>";

$J_title1="Send Ad";
$J_title3="<b>".$pub["j_in_name"]."</b>";
$J_icon="<img src=".$ROOT."/ico/set/email.png>";
$J_label=0;
$J_width=480;
$J_height=420;
$J_nomax=1;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>