<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

if(count($_POST))
{
	include_once $SDR."/system/get.php";
	include_once $SDR."/system/activity.php";

	if(isset($_GET["edit"]))
	{
		mysql_query("DELETE FROM nse_variables WHERE variable_name='ad_editor'");
		foreach($_POST as $k => $v)
		{
			if(strpos($k,"a_")!==false)
			{
				mysql_query("INSERT INTO nse_variables (variable_name,variable_value) VALUES ('ad_editor',".$v.")");
				J_act("ADS SETTINGS",3,"Ads Edit Rights - ".J_Value("","people","",$v),$v);
			}
		}
	}
}


echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/ad_manager/f/settings.js></script>";

if(isset($_GET["s"]))
{
	echo "<body><script>A_in_s()</script></body></html>";
	die();
}


elseif(isset($_GET["edit"]))
{
	echo "<style type=text/css>b input{width:300;font-weight:bold}</style>";
	echo "<body>";
	echo "<h4>Edit Rights</h4>";
	echo "<tt>These are people that are nominally allowed to edit. These people also appear in the send ads screen list of possible recipients</tt>";
	echo "<br>";
	echo "<form method=post>";
	echo "<b>";
	$i=0;
	$q="SELECT variable_value,firstname,lastname FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='ad_editor' ORDER BY firstname,lastname";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
		{
			echo "<tt><input type=text value=\"".ucwords(strtolower($g["firstname"]." ".$g["lastname"]))."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)><input type=hidden name=a_".$i." value=".$g["variable_value"]."></tt>";
			$i++;
		}
	}
	echo "<script>N_but();id=".$i."</script>";
	echo "</b>";
	echo "<hr><input type=submit value=OK onclick=J_opaq()>";
	echo "</form>";
	echo "<iframe id=j_IF></iframe>";
	echo "</body></html>";
	die();
}

echo "<body class=j_List>";
echo "<script>A_in_N()</script>";
$J_home=9;
$J_title1="Settings";
$J_tooltip="Set who can edit ads or who gets notifications";
$J_icon="<img src=".$ROOT."/ico/set/set.png>";
$J_label=21;
$J_width=560;
$J_framesize=260;
$J_tooltip="Search client Ads database";
$J_frame1=$ROOT."/apps/ad_manager/settings.php?s=1";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>