<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/ad_manager/f/manager.js></script>";

if(isset($_GET["j_IF"]) && !isset($_GET["del"]))
{
	echo "<script>J_in_s(1)</script>";
	die();
}

$id=isset($_GET["id"])?$_GET["id"]:"";
$aid=isset($_GET["aid"])?$_GET["aid"]:"";
$est="";
if($id)
{
	$g=mysql_fetch_array(mysql_query("SELECT establishment_name FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));
	$est=$g["establishment_name"];
}

echo "<script>J_in_s(j_Wn,'".$id."',\"".$est."\",".($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s" || $_SESSION["j_user"]["role"]=="b"?1:0).",".(isset($_GET["ins"])||isset($_GET["del"])?1:0).")</script>";

if($id)
{
	$o=array("","");
	$r=mysql_query("SELECT DISTINCT a.ad_id
	,i.j_in_name
	,p.publication_year
	,p.publication_name
	,p.closed
	FROM nse_ads AS a
	LEFT JOIN nse_inventory AS i ON a.inventory_id=i.j_in_id
	LEFT JOIN nse_inventory_publications AS ip ON i.j_in_id=ip.j_inp_inventory
	LEFT JOIN nse_publications AS p ON ip.j_inp_publication=p.publication_id
	WHERE a.establishment_code='".$id."'
	ORDER BY p.publication_year DESC");
	if(mysql_num_rows($r))
	{
		$n="";
		$c="";
		$k="";
		while($g=mysql_fetch_array($r))
		{
			$i=$g["closed"]?0:1;
			$k=substr($g["j_in_name"],0,strpos($g["j_in_name"]," - ")+3);
			if($c!=$k)
			{
				$c=$k;
				if($o[$i])
					$o[$i].="</div>";
				$o[$i].="<a href=go: onclick='J_U(this);return false' onfocus=blur() class=jf0>".trim($c," - ")."</a><div>";
			}
			$o[$i].="<a href=go: onclick=\"Aa(".$g["ad_id"].");return false\" onfocus=blur()>".str_replace($c,"",$g["j_in_name"])."</a>";
		}
	}

	$r=mysql_query("SELECT * FROM nse_inventory
	LEFT JOIN nse_inventory_publications ON j_in_id=j_inp_inventory
	LEFT JOIN nse_publications ON j_inp_publication=publication_id
	WHERE closed=0
	AND (j_in_category=3 OR j_in_category=4)
	ORDER BY j_inp_year DESC,j_in_name");
	if(mysql_num_rows($r))
	{
		echo "<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf".($o[1]?0:1).">Order an Ad</a><div".($o[1]?"":" style=display:block")." id=a_db><tt style='margin:0 0 4 0'><var>Place your order for an ad in our following publications:</var></tt>";
		$p="";
		$c="";
		$k="";
		while($g=mysql_fetch_array($r))
		{
			$k=substr($g["j_in_name"],0,strpos($g["j_in_name"]," - ")+3);
			if($c!=$k)
			{
				if($c)
					echo "</div>";
				$c=$k;
				echo "<a href=go: onclick='J_U(this);return false' onfocus=blur() class=jf0>".trim($c," - ")."</a><div>";
			}
			echo "<a href=go: onclick='Ap(".$g["j_in_id"].");return false' onmouseover=\"J_TT(this,'<b>Material Deadline</b><br>".$g["expiry_date"]."')\"";
			echo " onfocus=blur()>".str_replace($c,"",$g["j_in_name"])."</a>";
		}
		echo "</div><br></div>";
	}

	if($o[1])
		echo "<a href=go: onclick='J_U(this);return false' onfocus=blur() class=jf1>New Ads</a><div style=display:block>".$o[1]."</div></div>";

	if($o[0])
		echo "<a href=go: onclick='J_U(this);return false' onfocus=blur() class=jf0>Closed Ads</a><div>".$o[0]."</div></div>";

	echo "</blockquote>";
}

if(!isset($_GET["ins"])) // refresh
{
	$J_title2=$id?"Ad Editor":"Establishment Manager";
	$J_title1="Ad Editor";
	$J_title1=$est?$est:"Ad Editor";
	if($id)
		$J_title3="Establishment Code: ".$id;
	$J_framesize=360;
	$J_width=1000;
	$J_height=600;
	$J_icon="<img src=".$ROOT."/ico/set/gimp.png>";
	$J_tooltip="Create or edit an ad for publication";
	$J_label=21;
	$J_home=1;
	$J_nostart=1;
	if($aid)
		$J_frame1=$ROOT."/apps/ad_manager/f/edit.php?id=".$aid."&eid=".$id;
	elseif(!isset($_GET["del"]))
		$J_frame1=$ROOT."/apps/ad_manager/manager.php";
	include $SDR."/system/deploy.php";
}
echo "</body></html>";
?>