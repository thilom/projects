<?php

$a = 4;
unset($_POST["id"]);
$s = array
	(
	"order_item_id" => array("int" => 1)
	, "inventory_id" => array("int" => 1)
	, "instructions" => array("keyboard" => 1, "tags" => 1, "ucfirst" => 1)
	, "notes" => array("keyboard" => 1, "tags" => 1, "ucfirst" => 1)
	, "award" => array("keyboard" => 1, "tags" => 1)
);

if (!$template) {
	J_PrepFile("/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf");
	J_SetPerm($SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf");
	J_PrepFile("/stuff/establishments/" . $eid . "/ads/" . $id . "/files");
	J_SetPerm($SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/files");

	J_DirList("/stuff/establishments/" . $eid . "/ads/" . $id . "/files");
	$f = "";
	foreach ($J_filelist as $k => $v) {
		$n = "ff_" . str_replace(".", "dot", $v);
		if (!isset($_POST[$n])) {
			unlink($SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/files/" . $v);
			adHist($id, "Deleted File " . $v);
			J_act("Ad", 5, $v, $id, $eid);
		}
		unset($_POST[$n]);
	}

	foreach ($_FILES as $k => $v) {
		if ($_FILES[$k]["size"]) {
			if (strpos($k, "f_") !== false) {
				$n = $EPOCH . "." . str_replace(" ", "_", strtolower(substr($_FILES[$k]["name"], -3)));
				move_uploaded_file($_FILES[$k]["tmp_name"], $SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/files/" . $n);
				adHist($id, "Uploaded File " . $n);
				J_act("Ad", 6, $_FILES[$k]["name"], $id, $eid);
			} elseif ($k == "p_df") {
				$n = $EPOCH . "." . str_replace(" ", "_", strtolower(substr($_FILES[$k]["name"], -3)));
				move_uploaded_file($_FILES[$k]["tmp_name"], $SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf/" . $n);
				$_POST["last_pdf"] = $n;
				adHist($id, "Uploaded PDF " . $n);
				J_act("Ad", 6, "File: " . $n, $id, $eid);
			}
		}
	}
}

function update_Invoice_Cancel($c) {
	global $eid, $adb, $in, $EPOCH;
	// update pro-forma item with cancelled
	$q = mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=" . $adb["invoice_id"]); // get all items in invoice
	if (mysql_num_rows($q)) {
		$g = mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=" . $adb["invoice_id"] . " AND j_invit_reference='AD" . $adb["invoice_id"] . "'"));
		if (mysql_num_rows($q) == 1) { // if only one item - cancel invoice
			$inv = mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=" . $adb["invoice_id"] . " LIMIT 1"));
			mysql_query("UPDATE nse_invoice SET j_inv_cancel_date=" . ($c ? $EPOCH : 0) . ",j_inv_cancel_user=" . ($c ? $_SESSION["j_user"]["id"] : 0) . " WHERE j_inv_id=" . $adb["invoice_id"]);
			insertNote($eid, 2, "INV" . $adb["invoice_id"], ($c ? "Cancelled " : "Removed Cancelled Status from") . " Ad Invoice No " . $inv["j_inv_number"] . "): " . addslashes(preg_replace("~[^a-zA-Z0-9 &';,\.\-\(\)]~", "", $g["j_invit_name"])) . " (Invoice: " . $adb["invoice_id"] . " / Item: " . $g["j_invit_id"] . " / Ref: " . $g["j_invit_reference"] . ")", 0, 0, 1, 4);
		} else { // cancel item row
			// update item to cancelled
			mysql_query("UPDATE nse_invoice_item SET j_invit_code='" . ($c ? "Cancelled" : "") . "',j_invit_total=" . ($c ? 0 : ($g["j_invit_price"] - $g["j_invit_discount"]) + $g["j_invit_vat"]) . " WHERE j_invit_id=" . $g["j_invit_id"]);
			insertNote($eid, 2, "INV" . $adb["invoice_id"], ($c ? "Cancelled " : "Removed Cancelled Status from") . " Ad Invoice No " . $inv["j_inv_number"] . "): " . addslashes(preg_replace("~[^a-zA-Z0-9 &';,\.\-\(\)]~", "", $g["j_invit_name"])) . " (Invoice: " . $adb["invoice_id"] . " / Item: " . $g["j_invit_id"] . " / Ref: " . $g["j_invit_reference"] . ")", 0, 0, 1, 4);
		}
	}
}

$u = "";
$chng = 0;
$_POST["sharpen"] = $sharpen;
$_POST["material_outstanding"] = isset($_POST["material_outstanding"]) ? $suid : 0;
$_POST["request_proof_read"] = isset($_POST["request_proof_read"]) ? $suid : 0;
$_POST["proof_read"] = isset($_POST["proof_read"]) ? $suid : 0;
$_POST["approved"] = isset($_POST["approved"]) ? ($adb["approved"] == 0 ? $suid : $adb["approved"]) : 0;
$_POST["cancelled"] = isset($_POST["cancelled"]) ? $suid : 0;
$_POST["make_up_complete"] = (isset($_POST["make_up"]) && $_POST["make_up"] == 2 ? ($adb["make_up_complete"] ? $adb["make_up_complete"] : $suid) : 0);
$_POST["make_up"] = (isset($_POST["make_up"]) && $_POST["make_up"] == 1 ? ($adb["make_up"] ? $adb["make_up"] : $suid) : 0);
$_POST["supplied_material"] = isset($_POST["supplied_material"]) ? ($adb["supplied_material"] == 0 ? $suid : $adb["supplied_material"]) : 0;
foreach ($_POST as $k => $v) {
	$v = (isset($s[$k]) ? J_parseValue($v, $s[$k]) : addslashes($v));
	$u.="," . $k . "='" . $v . "'";

	if (!isset($insert)) {
		if ($k == "instructions" && $adb["instructions"] != $v)
			adHist($id, "Edited Staff Notes");
		elseif ($k == "notes" && $adb["notes"] != $v)
			adHist($id, "Edited Notes");
		elseif ($k == "cancelled") {
			if ((!$adb["cancelled"] && $v) || ($adb["cancelled"] && !$v)) {
				adHist($id, "Cancelled" . ($v ? "" : " turned off"));
				if ($adb["order_id"])
					mysql_query("UPDATE nse_invoice SET j_inv_cancel_date=" . ($v ? $EPOCH : 0) . ",j_inv_cancel_user=" . ($v ? $suid : 0) . " WHERE j_inv_id=" . $adb["order_id"]);
				if ($adb["invoice_id"])
					mysql_query("UPDATE nse_invoice SET j_inv_cancel_date=" . ($v ? $EPOCH : 0) . ",j_inv_cancel_user=" . ($v ? $suid : 0) . " WHERE j_inv_id=" . $adb["invoice_id"]);
				if (count($invoice_default_cancel_person)) {
					if ($v) {
						$sbjct = "AD CANCELLED - " . $esd["establishment_name"];
						$msg = "<b style='font-size:9pt'>Establishment: " . $esd["establishment_name"] . "</b><br>";
						$msg.="<span style='font-size:9pt'>Size " . $pub["j_inp_width"] . "&times;" . $pub["j_inp_height"] . "mm" . "</span>";
						$msg.="<hr>";
						$msg.="<b>Ad cancelled by: <a href='mailto:" . $per["email"] . "'>" . $per["firstname"] . " " . $per["lastname"] . "</a></b>";
						update_Invoice_Cancel(1);
						insertNote($eid, 7, "AD" . $id, "Cancelled Ad ", 0, 0, 1, 4);
					} else {
						$sbjct = "AD CANCEL REMOVED - " . $esd["establishment_name"];
						$msg = "<b style='font-size:9pt'>Establishment: " . $esd["establishment_name"] . "</b><br>";
						$msg.="<span style='font-size:9pt'>Size " . $pub["j_inp_width"] . "&times;" . $pub["j_inp_height"] . "mm" . "</span>";
						$msg.="<hr>";
						$msg.="<b>Ad cancel removed by: <a href='mailto:" . $per["email"] . "'>" . $per["firstname"] . " " . $per["lastname"] . "</a></b>";
						update_Invoice_Cancel(0);
						insertNote($eid, 7, "AD" . $id, "Cancel Status Removed Ad ", 0, 0, 0, 4);
					}
					foreach ($invoice_default_cancel_person as $k => $v)
						notify($v, $sbjct, $msg);
					if ($esd["assessor_id"])
						notify($esd["assessor_id"], $sbjct, $msg);
					if ($esd["rep_id1"])
						notify($esd["rep_id1"], $sbjct, $msg);
				}
			}
		}
		elseif ($k == "material_outstanding") {
			if ((!$adb["material_outstanding"] && $v) || ($adb["material_outstanding"] && !$v))
				adHist($id, "Material Outstanding turned " . ($v ? "on" : "off"));
		}
		elseif ($k == "request_proof_read") {
			if ((!$adb["request_proof_read"] && $v) || ($adb["request_proof_read"] && !$v)) {
				if ($_POST["proof_read"])
					$v = 0;
				adHist($id, "Request Proof-Read turned " . ($v ? "on" : "off"));
			}
		}
		elseif ($k == "make_up") {
			if ($adb["make_up"] != $v)
				adHist($id, "Make-up turned " . ($v ? "on" : "off"));
		}
		elseif ($k == "make_up_complete") {
			if ($adb["make_up_complete"] != $v)
				adHist($id, "Make-up Complete turned " . ($v ? "on" : "off"));
		}
		elseif ($k == "supplied_material") {
			if ($adb["supplied_material"] != $v)
				adHist($id, "Supplied Material turned " . ($v ? "on" : "off"));
		}
		elseif ($k == 'award') {
			$statement = "UPDATE nse_ads
							SET award = '$v'
							WHERE ad_id='$id'
							LIMIT 1";
			$sql_rate = $GLOBALS['db_pdo']->prepare($statement);
			$sql_rate->execute();

			$k = ucfirst(strtolower(str_replace("_", " ", $k)));
			adHist($id, "Changed " . $k);
			$chng = 1;
		}
		elseif (($k == "country_id" || $k == "province_id" || $k == "region_id" || $k == "town_id" || $k == "establishment_name" || $k == "qa_logo" || $k == "qa_text" || $k == "description" || $k == "address" || $k == "rates" || $k == "rooms" || $k == "qa_status" || $k == "star_grading" || $k == "image_src" || $k == "image_dimensions" || $k == "image_position" || $k == "award" || $k == "side" || $k == "icons") && $adb[$k] != $v) {
			$k = ucfirst(strtolower(str_replace("_", " ", $k)));
			adHist($id, "Changed " . $k);
			$chng = 1;

		}
	}
}


mysql_query("UPDATE nse_ads SET user=" . $suid . $u . " WHERE ad_id=" . $id);
if (mysql_error())
	die(mysql_error() . "<hr>");

if ($adb["sharpen"] != $sharpen)
	adHist($id, "Sharpen Images turned " . ($sharpen ? "on" : "off"));

if (!isset($insert) && $chng) {
	if ($adb["proof_read"]) {
		$_POST["proof_read"] = 0;
		mysql_query("UPDATE nse_ads SET proof_read=0 WHERE ad_id=" . $id);
		adHist($id, "Proof-Read removed because of changes", 1);
	}
	if ($adb["approved"]) {
		$_POST["approved"] = 0;
		adHist($id, "Approval removed because of changes", 1);
	}
}
if (!$adb["proof_read"] && $_POST["proof_read"])
	adHist($id, "Proof-Read");
elseif ($adb["proof_read"] && !$_POST["proof_read"])
	adHist($id, "Proof-Read turned off");

if (!$adb["approved"] && $_POST["approved"])
	adHist($id, "Approved");

if (!$_POST["cancelled"])
	$render_pdf = 1;
//else
//$no_render_pdf=1;

J_act("Ad", $pub["j_in_name"] . " (" . $adb["inventory_id"] . ")", $id, $eid);

?>