<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/apps/accounts/invoices/default.php";

//ini_set('display_errors', 0);

$var="ad";
$eid=$_GET["eid"];
$g_eid=0; // establishment group parent id
$choose=isset($_GET["choose"])?$_GET["choose"]:"";
$pid=$_GET["inventory_id"];
$i=mysql_fetch_array(mysql_query("SELECT * FROM nse_inventory WHERE j_in_id=".$pid." LIMIT 1"));
$invoice_default_to_ad_person=strpos($i["j_in_name"],"SA Best")!==false?329:$invoice_default_to_ad_person; // exception fro SA Best
$security_exception=1;

// if any, switch to group company parent
include_once $SDR."/apps/accounts/invoices/group_with.php";

// if group, ask user if this invoice must be places in group invoice or not
if(!isset($_GET["choose"]) && $g_eid)
{
	echo "<script>l='".$ROOT."/apps/ad_manager/f/order_set.php?set=ad&eid=".$eid."&inventory_id=".$pid."&choose=';if(confirm(\"ATTENTION\\nThis establishment belongs to a group.\\nGroup parent is ".preg_replace("~[^a-zA-Z0-9 ,'\-]~","",J_Value("","establishment","",$g_eid))."\\n\\nClick OK to add this ad to a group invoice.\\nClick CANCEL to create a separate invoice for this ad.\"))location=l+'grp';else location=l+'no'</script>";
	die();
}

if($choose=="grp" && $g_eid) // if group, find existing pro-forma w same items
{
	$_GET["geid"]=$g_eid;
	$q="SELECT j_inp_publication FROM nse_inventory JOIN nse_inventory_publications ON j_in_id=j_inp_inventory WHERE j_in_id=".$pid." LIMIT 1";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		$g=mysql_fetch_array($q);
		$q="SELECT j_invit_invoice FROM nse_invoice_item
		 JOIN nse_invoice ON j_invit_invoice=j_inv_id
		 JOIN nse_inventory_publications ON j_inp_inventory=j_invit_inventory
		 WHERE
		 j_inv_to_company='".$g_eid."'
		 AND j_inv_type=1
		 AND j_inp_publication=".$g["j_inp_publication"]."
		 LIMIT 1";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			$g=mysql_fetch_array($q);
			$q="SELECT j_inv_id FROM nse_invoice WHERE j_inv_invoice_id=".$g["j_invit_invoice"]." LIMIT 1";
			$q=mysql_query($q);
			if(mysql_num_rows($q))
			{
				$g=mysql_fetch_array($q);
				$id=$g["j_inv_id"];
				$add_items=$pid;
				$_GET["pid"]=$pid;
				include $SDR."/apps/accounts/invoices/edit.php";
				die();
			}
		}
	}
}

// new invoice
include_once $SDR."/apps/accounts/invoices/get.php"; // details getters

$co=$choose=="grp"&&$g_eid?$g_eid:$eid;
if(!isset($_SESSION["juno"]["accounts"]["company"][$co])) // get company details
	$fromCo=J_inv_getto($co);

// find establishment person
$from_id=0;
$no_js=1; // dont draw js stuff
include_once $SDR."/custom/lib/get_est_people.php";
$from_id=($manager?$manager:($owner?$owner:($accounts?$accounts:$anybody))); // find appropriate person
if($from_id) // get persons details
	$fromPer=J_inv_getpeo($from_id);
// get rep and assessors
$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$co."' LIMIT 1"));
$assessor_id=$g["assessor_id"];
if($assessor_id) // get persons details
	$fromPer=J_inv_getpeo($assessor_id);
$rep_id=$g["rep_id1"];
if($rep_id) // get persons details
	$fromRep=J_inv_getpeo($rep_id);
if($invoice_default_to_ad_person) // get to persons details
	$toPer=J_inv_getpeo($invoice_default_to_ad_person);

$total_sub=0;
$total_vat=0;
$total=0;

// item
$price=$i["j_in_price"];
$vat=($price*($invoice_default_vat/100));
$total_sub+=$price;
$total_vat+=$vat;
$total+=$price+$vat;
$_i="0`";
$_i.=$i["j_in_id"]."`";
$_i.=date("d/m/Y",$EPOCH)."`";
$_i.=$i["j_in_name"]." (".preg_replace("~[^a-zA-Z0-9 ,'@\-\(\)]~","",J_Value("","establishment","",$eid)).")`";
$_i.=substr($i["j_in_description"],0,256)."`";
$_i.=$i["j_in_code"]."`"; // 5
$_i.="1`";
$_i.=$price."`";
$_i.="0`";
$_i.=$vat."`";
$_i.=($price+$vat)."`"; // 10
$_i.="`";
$_i.=$invoice_default_vat."`";
$_i.="0`";
$_i.=$eid; // new company id for each item
$_i.="|";

// invoice
$_v="9~";
$_v.=date("d/m/Y H:i",$EPOCH)."~";
$_v.="~";
$_v.="~";
$_v.="~";
$_v.="0~"; // 5
$_v.=$co."~";
$_v.="0~";
$_v.=$from_id."~";
$_v.=$assessor_id."~";
$_v.=$rep_id."~"; // 10
$_v.="~";
$_v.=$invoice_default_to_ad_person."~";
$_v.="0~";
$_v.=$invoice_default_order_contact."~";
$_v.=$invoice_default_vat."~"; // 15
$_v.="~";
$_v.="~";
$_v.=$invoice_default_order_ads_condition."~";
$_v.="0~";
$_v.=$invoice_default_foot."~"; // 20
$_v.="~";
$_v.=$invoice_default_currency."~";
$_v.=$total_sub."~";
$_v.="0~";
$_v.=$total_vat."~"; // 25
$_v.=$total."~";
$_v.="9~";
$_v.="0~";
$_v.="0~";
$_v.="~";

$_v.=$_i;

$oid=1;
$_GET["pid"]=$pid;
include $SDR."/apps/accounts/invoices/edit.php";
?>