<?php
include_once $SDR."/custom/email_headers.php";

function notify($to=0,$subject="",$msg="",$attach=0)
{
	if($to)
	{
		global $SDR,$j_email_header,$j_email_footer;
		include_once $SDR."/custom/email_headers.php";

		$from=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1"));
		$to=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$to." LIMIT 1"));
	
		$msg=$j_email_header.$msg."<br><br><br>".$j_email_footer;
		require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
		$mail=new PHPMailer(true);
		try
		{
			$mail->From=$from["email"];
			$mail->FromName=$from["firstname"];
			$mail->AddReplyTo($from["email"],$from["email"]);
			$mail->SetFrom($from["email"],$from["email"]);
			$mail->AddAddress($to["email"],$to["email"]);
			$mail->Subject=$subject;
			$mail->MsgHTML($msg);
			if($attach)
			{
				$attach=explode("~",$attach);
				foreach($attach as $k => $v)
				{
					if($v)
					{
						$v=$SDR."/".trim($v,"/");
						if(file_exists($v))
							$mail->AddAttachment($v);
					}
				}
			}
			$mail->Send();
		}
		catch (phpmailerException $e){echo $e->errorMessage();}
		catch (Exception $e){echo $e->getMessage();}
	}
}
?>