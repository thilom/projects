<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

//Vars
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$pub="";
$siz="";
$aq="";
$ar="";
$reg="";
$pro="";
$tow="";
$sub="";

// for display
$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	if($v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
}
$t=array("X","pub","siz","code","name","reg","pro","tow","sub","qa","sta","res","roo","rat");
$d="";
$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}

$r="";

$q="SELECT ";
$q.="DISTINCT a.ad_id";
$q.=",a.establishment_code";
$q.=",i.j_in_id";
$q.=",i.j_in_name";
$q.=",ip.j_inp_publication";
$q.=",pb.publication_name";
$q.=",e.establishment_name";
$q.=",ac.aa_category_name";
$q.=",rt.subcategory_id";
$q.=",rt.star_grading";
$q.=",ed.room_count";
$q.=",ed.room_type";
$q.=",ep.category";
$q.=",ep.category_low";
$q.=",ep.category_prefix";
$q.=",ep.category_suffix";
$q.=",p.province_id";
$q.=",p.province_name";
$q.=",t.town_id as townid";
$q.=",t.town_name";
$q.=",s.suburb_id";
$q.=",s.suburb_name";
$q.=" FROM nse_ads AS a ";
$q.=" LEFT JOIN nse_inventory AS i ON a.inventory_id=i.j_in_id ";
$q.=" LEFT JOIN nse_inventory_publications AS ip ON a.inventory_id=ip.j_inp_inventory ";
$q.=" LEFT JOIN nse_publications AS pb ON ip.j_inp_publication=pb.publication_id ";
$q.=" JOIN nse_establishment AS e ON a.establishment_code=e.establishment_code";
$q.=" LEFT JOIN nse_aa_category AS ac ON e.aa_category_code=ac.aa_category_code";
$q.=" LEFT JOIN nse_establishment_restype AS rt ON e.establishment_code=rt.establishment_code";
$q.=" LEFT JOIN nse_establishment_data AS ed ON e.establishment_code=ed.establishment_code";
$q.=" LEFT JOIN nse_establishment_pricing AS ep ON e.establishment_code=ep.establishment_code";
$q.=" LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code";
$q.=" LEFT JOIN nse_nlocations_provinces AS p ON l.province_id=p.province_id";
$q.=" LEFT JOIN nse_nlocations_towns AS t ON l.town_id=t.town_id";
$q.=" LEFT JOIN nse_nlocations_suburbs AS s ON l.suburb_id=s.suburb_id";
$q.=" WHERE e.establishment_code!=''";
if(isset($_GET["pub"]))
	$q.=" AND ip.j_inp_publication=".$_GET["pub"]." ";
if(isset($_GET["siz"]))
	$q.=" AND a.inventory_id='".$_GET["siz"]."' ";
if(isset($_GET["estT"]))
	$q.=J_Search($_GET["estT"],"e.establishment_name")." ";
elseif(isset($_GET["est"]))
	$q.=" AND a.establishment_code='".$_GET["est"]."'";
$q.=" ORDER BY pb.publication_name,i.j_in_name,e.establishment_name";
mysql_query($q);
$jL0=mysql_num_rows(mysql_query($q));
if(!isset($_GET["estT"])&&isset($_GET["est"]))
	$q.=" LIMIT 1";
else
	$q.=" LIMIT ".$jL1.",".$jL2;
$q=mysql_query($q);
if(mysql_num_rows($q))
{

	function getRegion($t=0,$s=0)
	{
		global $tt,$ss;
		$r=array("","");
		if(($t && !isset($tt[$t])) || ($s && isset($ss[$s])))
		{
			global $rr;
			if(!$t) // find town id else-where
			{
				$q=mysql_query("SELECT town_id FROM nse_nlocations_suburbs WHERE suburb_id=".$s." LIMIT 1");
				if(mysql_num_rows($q))
				{
					$g=mysql_fetch_array($q);
					$t=$g["town_id"];
				}
			}

			if($t)
			{
				$q="SELECT * FROM nse_nlocations_cregion_locations
				INNER JOIN nse_nlocations_cregions ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
				AND parent_type LIKE 'province'
				WHERE location_id=".$t." AND location_type LIKE 'town'
				LIMIT 1";
				$q=mysql_query($q);
				if(mysql_num_rows($q))
				{
					$q=mysql_fetch_array($q);
					$r[0]=$q["region_id"];
					$r[1]=stripslashes($q["region_name"]);
					if(!isset($rr[0]))
						$rr[$r[0]]=$r[1];
				}
			}
		}
		return $r;
	}

	include $SDR."/custom/lib/est_price_codes.php";
	function rates($eid="")
	{
		$rts="";
		$r="SELECT price_in_high,price_in_low,category_prefix,category_suffix FROM nse_establishment_pricing WHERE establishment_code='".$eid."' LIMIT 1";
		$r=mysql_query($r);
		if(mysql_num_rows($r))
		{
			$g=mysql_fetch_array($r);
			if($g["price_in_low"]||$g["price_in_high"])
			{
				$rts=($g["price_in_low"]?get_price_code($g["price_in_low"]):"").($g["price_in_low"]&&$g["price_in_high"]?" - ":"").($g["price_in_high"]?get_price_code($g["price_in_high"]):"")." ".str_ireplace("  "," ",str_ireplace("b&b","B&B",str_ireplace("sharing","",$g["category_suffix"])));
				$rts=stripslashes($rts);
			}
		}
		return $rts;
	}

	$ppp=array();
	$ppt=array();
	$qa=array(""=>"");
	$rt=array(""=>"");
	$rr=array();
	$pp=array();
	$tt=array();
	$ss=array();
	$i=0;
	$hed="";

	while($g=mysql_fetch_array($q))
	{
		if(isset($_GET["csv"]))
		{
			$r.=",".$g["publication_name"];
			if(!$i)
				$hed.=",PUBLICATION";
			$r.=",".str_replace(",",";",trim($g["j_in_name"]?substr($g["j_in_name"],strrpos($g["j_in_name"]," - ")+3):""));
			if(!$i)
				$hed.=",SIZE";
			$r.=",".str_replace(",",";",trim($g["establishment_name"]));
			$r.=",".$g["establishment_code"];
			if(!$i)
				$hed.=",CODE";
			$r.=",".str_replace(",",";",trim($g["establishment_name"]));
			if(!$i)
				$hed.=",ESTABLISHMENT";
			if(isset($s["reg"]))
			{
				$v=getRegion($g["townid"],$g["suburb_id"]);
				$r.=",".str_replace(",",";",$v[1]);
				if(!$i)
					$hed.=",REGION";
			}
			if(isset($s["pro"]))
			{
				$r.=",".str_replace(",",";",$g["province_name"]);
				if(!$i)
					$hed.=",PROVINCE";
			}
			if(isset($s["tow"]))
			{
				$r.=",".str_replace(",",";",$g["town_name"]);
				if(!$i)
					$hed.=",TOWN";
			}
			if(isset($s["sub"]))
			{
				$r.=",".str_replace(",",";",$g["suburb_name"]);
				if(!$i)
					$hed.=",SUBURB";
			}
			if(isset($s["qa"]))
			{
				$r.=",".str_replace(",",";",$g["aa_category_name"]);
				if(!$i)
					$hed.=",QA STATUS";
			}
			if(isset($s["res"]))
			{
				if($g["subcategory_id"])
				{
					$qrt=mysql_query("SELECT subcategory_name FROM nse_restype_subcategory_lang WHERE subcategory_id=".$g["subcategory_id"]." LIMIT 1");
					if(mysql_num_rows($qrt))
					{
						$qrt=mysql_fetch_array($qrt);
						$r.=",".str_replace(",",";",$qrt["subcategory_name"]);
					}
					else
						$r.=",";
				}
				else
					$r.=",";
				if(!$i)
					$hed.=",RES TYPE";
			}
			if(isset($s["roo"]))
			{
				$r.=",".str_replace(",",";",($g["room_count"]?$g["room_count"]:""));
				if(!$i)
					$hed.=",ROOMS";
			}
			if(isset($s["rat"]))
			{
				$r.=",".str_replace(",",";",rates($g["establishment_code"]));
				if(!$i)
					$hed.=",RATES";
			}
			$r.="\n";
			$i++;
		}
		else
		{
			if(!isset($ppp[$g["j_inp_publication"]]))
				$ppp[$g["j_inp_publication"]]=trim($g["publication_name"]);
			if(!isset($ppt[$g["j_in_id"]]))
				$ppt[$g["j_in_id"]]=trim($g["j_in_name"]?substr($g["j_in_name"],strrpos($g["j_in_name"]," - ")+3):"");
			$r.="~";
			$r.=$g["j_inp_publication"]."~";
			$r.=$g["j_in_id"]."~";
			$r.=$g["establishment_code"]."~";
			$r.=trim($g["establishment_name"])."~";
			if(isset($s["reg"]))
			{
				$v=getRegion($g["townid"],$g["suburb_id"]);
				$r.=$v[0]."~";
			}
			if(isset($s["pro"]))
			{
				if(!isset($pp[$g["province_id"]]))
				  $pp[$g["province_id"]]=$g["province_name"];
				$r.=$g["province_id"]."~";
			}
			if(isset($s["tow"]))
			{
				if(!isset($tt[$g["townid"]]))
				  $tt[$g["townid"]]=$g["town_name"];
				$r.=$g["townid"]."~";
			}
			if(isset($s["sub"]))
			{
				if(!isset($ss[$g["suburb_id"]]))
				  $ss[$g["suburb_id"]]=$g["suburb_name"];
				$r.=$g["suburb_id"]."~";
			}
			if(isset($s["qa"]))
			{
				if(!isset($qa[$g["aa_category_name"]]))
				  $qa[$g["aa_category_name"]]=count($qa);
				$r.=$qa[$g["aa_category_name"]]."~";
			}
			if(isset($s["sta"]))
				$r.=$g["star_grading"]."~";
			if(isset($s["res"]))
			{
				if($g["subcategory_id"])
				{
					$qrt=mysql_query("SELECT subcategory_name FROM nse_restype_subcategory_lang WHERE subcategory_id=".$g["subcategory_id"]." LIMIT 1");
					if(mysql_num_rows($qrt))
					{
						$qrt=mysql_fetch_array($qrt);
						if(!isset($rt[$g["subcategory_id"]]))
						  $rt[$g["subcategory_id"]]=$qrt["subcategory_name"];
						$r.=$g["subcategory_id"]."~";
					}
					else
						$r.="~";
				}
				else
					$r.="~";
			}
			if(isset($s["roo"]))
				$r.=($g["room_count"]?$g["room_count"]:"")."~";
			if(isset($s["rat"]))
				$r.=rates($g["establishment_code"])."~";
			$r.="|";
		}
	}
	if(isset($_GET["csv"]))
		$csv=$hed."\n".$r;
	else
	{
	  $r=str_replace("\r","",$r);
	  $r=str_replace("\n","",$r);
	  $r=str_replace("\"","",$r);
	  $r=str_replace("~|","|",$r);
	  foreach($ppp as $k => $v)
			$pub.=$k."~".$v."|";
	  foreach($ppt as $k => $v)
			$siz.=$k."~".$v."|";
	  foreach($qa as $k => $v)
			$aq.=$v."~".$k."|";
	  foreach($rt as $k => $v)
			$ar.=$k."~".$v."|";
	  foreach($rr as $k => $v)
			$reg.=$k."~".$v."|";
	  foreach($pp as $k => $v)
			$pro.=$k."~".$v."|";
	  foreach($tt as $k => $v)
			$tow.=$k."~".$v."|";
	  foreach($ss as $k => $v)
			$sub.=$k."~".$v."|";
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"EST_DETAIL_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/ad_manager/f/est_detail_res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$d."\",\"".$pub."\",\"".$siz."\",\"".$reg."\",\"".$pro."\",\"".$tow."\",\"".$sub."\",\"".$aq."\",\"".$ar."\",".$jL0.",".$jL1.",".$jL2.")</script>";
}
?>