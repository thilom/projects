<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include $SDR."/system/parse.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"]) && $_GET["jL2"]?$_GET["jL2"]:10000);
$d="";
$csv="";
$pr="";

$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","est","per","post","town","prov","cou","ema","tel");
$d="";$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}
$d.=$n."~";$n++;
$d.=$n."~";$n++;
$d.=$n."~";$n++;
$d.=$n."~";$n++;


$q="SELECT ";
$q.="DISTINCT v.j_inv_to_company";
if(isset($s["cou"]))$q.=",c.country_name";
if(isset($s["prov"]))$q.=",cp.province_name";
if(isset($s["town"]))$q.=",t.town_name";
if(isset($s["est"]))$q.=",a.establishment_name";
if(isset($s["ema"]))$q.=",r.reservation_email";
if(isset($s["tel"]))$q.=",r.reservation_tel,r.reservation_cell";
if(isset($s["post"]))
{
	$q.=",e.postal_address_line1,e.postal_address_line2,e.postal_address_line3,e.postal_address_code";
	$q.=",r.reservation_postal1,r.reservation_postal2,r.reservation_postal3,r.reservation_postal_code";
}
$q.=",v.j_inv_to_contact";
$q.=",v.j_inv_to_person";
$q.=" FROM nse_ads AS a ";
$q.="LEFT JOIN nse_invoice AS v ON a.invoice_id=v.j_inv_id ";
$q.="LEFT JOIN nse_establishment AS e ON a.establishment_code=v.j_inv_to_company ";
$q.="LEFT JOIN nse_establishment_location AS l ON l.establishment_code=v.j_inv_to_company ";
$q.="LEFT JOIN nse_nlocations_countries AS c ON l.country_id=c.country_id ";
$q.="LEFT JOIN nse_nlocations_provinces AS cp ON l.province_id=cp.province_id ";
$q.="LEFT JOIN nse_nlocations_towns AS t ON l.town_id=t.town_id ";
$q.="LEFT JOIN nse_inventory AS i ON a.inventory_id=i.j_in_id ";
$q.="LEFT JOIN nse_inventory_publications AS ip ON a.inventory_id=ip.j_inp_inventory ";
$q.="LEFT JOIN nse_publications AS p ON ip.j_inp_publication=p.publication_id ";
if(isset($s["ema"]) || isset($s["tel"]) || isset($s["post"]))
	$q.="LEFT JOIN nse_establishment_reservation AS r ON a.establishment_code=r.establishment_code ";
$q.="WHERE ad_id>0 ";
if(isset($_GET["est"]))
	$q.="AND a.establishment_code='".$_GET["est"]."' ";
if(isset($_GET["pub"]))
	$q.="AND ip.j_inp_publication=".$_GET["pub"]." ";
if(isset($_GET["typ"]))
	$q.="AND a.inventory_id='".$_GET["typ"]."' ";
if(isset($_GET["cou"]))
	$q.="AND l.country_id='".$_GET["cou"]."' ";
if(isset($_GET["prov"]))
	$q.="AND l.province_id='".$_GET["prov"]."' ";
if(isset($_GET["appr"]))
	$q.="AND a.approved".($_GET["appr"]==1?"!=0":"=0")." ";
if(isset($_GET["can"]))
	$q.="AND a.cancelled".($_GET["can"]==1?"=0":"!=0")." ";
if(isset($_GET["pay"]))
	$q.="AND v.j_inv_paid".($_GET["pay"]==1?"=v.j_inv_total":"!=v.j_inv_total")." ";
$jL0=mysql_num_rows(mysql_query($q));
$q.="ORDER BY a.establishment_name ";
if(!isset($_GET["drw"]))
	$q.="LIMIT ".$jL1.",".$jL2;
$rs=mysql_query($q);
$r="";
if(mysql_num_rows($rs))
{
	$pp=array("");

	include $SDR."/custom/lib/est_items.php";

	$p=0;
	$chk=array();


	if(isset($_GET["drw"]))
	{
		$i=0;
		while($g=mysql_fetch_array($rs))
		{
			if(!isset($chk[$g["j_inv_to_company"]]))
			{
				$chk[$g["j_inv_to_company"]]=1;
				if($i%5==0)
					$r.="<tr>";
				$r.="<td>";
				$r.="<b>".str_replace("@"," @ ",strtoupper(preg_replace("~\(.*?\)~","",strip_tags($g["establishment_name"]))))."</b>";
				if(isset($s["per"]))
				{
					$p=people($g["j_inv_to_company"],$g["j_inv_to_contact"],$g["j_inv_to_person"]);
					$r.="<i>Att: ".(isset($pp[$p])?$pp[$p]:"")."</i>";
				}
				if(isset($s["post"]))
				{
					$r.="<u>";
					if($g["reservation_postal1"])
					{
						$r.=($g["reservation_postal1"]?trim(trim($g["reservation_postal1"],","),";"):"")."<br>";
						$r.=($g["reservation_postal2"]?trim(trim($g["reservation_postal2"],","),";"):"")."<br>";
						$r.=($g["reservation_postal3"]?trim(trim($g["reservation_postal3"],","),";")."<br>":"");
						$r.=($g["reservation_postal_code"]?trim(trim($g["reservation_postal_code"],","),"."):"")."<br>";
					}
					elseif($g["postal_address_line1"])
					{
						$r.=($g["postal_address_line1"]?trim(trim($g["postal_address_line1"],","),";"):"")."<br>";
						$r.=($g["postal_address_line2"]?trim(trim($g["postal_address_line2"],","),";"):"")."<br>";
						$r.=($g["postal_address_line3"]?trim(trim($g["postal_address_line3"],","),";")."<br>":"");
						$r.=($g["postal_address_code"]?trim(trim($g["postal_address_code"],","),"."):"")."<br>";
					}
					$r.="</u>";
				}
				if(isset($s["town"]) || isset($s["prov"]) || isset($s["cou"]) || isset($s["ema"]) || isset($s["tel"]));
				{
					$r.="<em>";
					$a="";
					if(isset($s["town"]))$a.=($g["town_name"]?", ".strtoupper($g["town_name"]):"");
					if(isset($s["prov"]))$a.=($g["province_name"]?", ".strtoupper($g["province_name"]):"");
					if(isset($s["cou"]))$a.=($g["country_name"]?", ".strtoupper($g["country_name"]):"");
					if(isset($s["ema"]))$a.=",".$g["reservation_email"];
					if(isset($s["tel"]))$a.=", ".numbers($g["j_inv_to_company"],$p,$g["reservation_tel"],$g["reservation_cell"]);
					$r.=trim(trim($a,"."),", ");
					$r.="</em>";
				}
				$r.="</td>";
				$i++;
				if($i%5==0)
					$r.="</tr>";
			}
		}
	}

	elseif(isset($_GET["csv"]))
	{
		$hed="";
		$c=0;
		$i=0;
		while($g=mysql_fetch_array($rs))
		{
			if(!isset($chk[$g["j_inv_to_company"]]))
			{
				$chk[$g["j_inv_to_company"]]=1;
				if(isset($s["est"]))
				{
					$r.=",".str_replace(","," ",$g["establishment_name"]);
					if(!$i)
					{
						$hed.=",ESTABLISHMENT";
						$c++;
					}
				}
				if(isset($s["per"]))
				{
					$p=people($g["j_inv_to_company"]);
					$r.=",".str_replace("<br>",". ",str_replace(",",";",$pp[$p]));
					if(!$i)
					{
						$hed.=",CONTACT";
						$c++;
					}
				}
				if(isset($s["post"]))
				{
					if(!$i)
					{
						$hed.=",POSTAL";
						$c++;
					}
					if(trim(trim($g["reservation_postal1"],","),";"))
						$r.=trim(trim($g["reservation_postal1"],","),";").($g["reservation_postal2"]?"; ".trim(trim($g["reservation_postal2"],","),";"):"").($g["reservation_postal3"]?"; ".trim(trim($g["reservation_postal3"],","),";"):"").($g["reservation_postal_code"]?"; ".trim(trim($g["reservation_postal_code"],","),";"):"");
					elseif(trim(trim($g["postal_address_line1"],","),";"))
						$r.=trim(trim($g["postal_address_line1"],","),";").($g["postal_address_line2"]?"; ".trim(trim($g["postal_address_line2"],","),";"):"").($g["postal_address_line3"]?"; ".trim(trim($g["postal_address_line3"],","),";"):"").($g["postal_address__code"]?"; ".trim(trim($g["postal_address_code"],","),";"):"");
					$r.="~";
				}
				if(isset($s["town"]))
				{
					$r.=",".($g["town_name"]?strtoupper(str_replace(","," ",$g["town_name"])):"");
					if(!$i)
					{
						$hed.=",TOWN";
						$c++;
					}
				}
				if(isset($s["prov"]))
				{
					$r.=",".($g["province_name"]?strtoupper(str_replace(","," ",$g["province_name"])):"");
					if(!$i)
					{
						$hed.=",PROVINCE";
						$c++;
					}
				}
				if(isset($s["cou"]))
				{
					$r.=",".($g["country_name"]?strtoupper(str_replace(","," ",$g["country_name"])):"");
					if(!$i)
					{
						$hed.=",COUNTRY";
						$c++;
					}
				}
				if(isset($s["ema"]))
				{
					$r.=",".str_replace(","," ",$g["reservation_email"]);
					if(!$i)
					{
						$hed.=",EMAIL";
						$c++;
					}
				}
				if(isset($s["tel"]))
				{
					$r.=",".numbers($g["j_inv_to_company"],$p,$g["reservation_tel"],$g["reservation_cell"]);
					if(!$i)
					{
						$hed.=",TEL";
						$c++;
					}
				}
			}
			$r.="\n";
			$i++;
		}
		$csv=$hed."\n".$r;
	}
	else
	{
		while($g=mysql_fetch_array($rs))
		{
			if(!isset($chk[$g["j_inv_to_company"]]))
			{
				$chk[$g["j_inv_to_company"]]=1;
				$r.="~";
				if(isset($s["est"]))$r.=strip_tags($g["establishment_name"])."~";
				if(isset($s["per"]))
				{
					$p=people($g["j_inv_to_company"]);
					$r.=$p."~";
				}
				if(isset($s["post"]))
				{
					if(trim(trim($g["reservation_postal1"],","),";"))
						$r.=trim(trim($g["reservation_postal1"],","),";").($g["reservation_postal2"]?", ".trim(trim($g["reservation_postal2"],","),";"):"").($g["reservation_postal3"]?", ".trim(trim($g["reservation_postal3"],","),";"):"").($g["reservation_postal_code"]?", ".trim(trim($g["reservation_postal_code"],","),";"):"");
					elseif(trim(trim($g["postal_address_line1"],","),";"))
						$r.=trim(trim($g["postal_address_line1"],","),";").($g["postal_address_line2"]?", ".trim(trim($g["postal_address_line2"],","),";"):"").($g["postal_address_line3"]?", ".trim(trim($g["postal_address_line3"],","),";"):"").($g["postal_address__code"]?", ".trim(trim($g["postal_address_code"],","),";"):"");
					$r.="~";
				}
				if(isset($s["town"]))$r.=($g["town_name"]?strtoupper($g["town_name"]):"")."~";
				if(isset($s["prov"]))$r.=($g["province_name"]?strtoupper($g["province_name"]):"")."~";
				if(isset($s["cou"]))$r.=($g["country_name"]?strtoupper($g["country_name"]):"")."~";
				if(isset($s["ema"]))$r.=$g["reservation_email"]."~";
				if(isset($s["tel"]))$r.=numbers($g["j_inv_to_company"],$p,$g["reservation_tel"],$g["reservation_cell"])."~";
				$r.="|";
			}
		}
		$r=str_replace("\r","",$r);
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($pp as $k => $v)
			$pr.=$k."~".$v."|";
	}
}

if(isset($_GET["drw"]))
{
	echo "<html>";
	echo "<style type=text/css>";
	echo "table td{width:20%;font-size:12pt;border:1px solid #DDD;font-family:arial}";
	echo "b{display:block;margin:0 0 8px 0}";
	echo "u{text-decoration:none;display:block;margin:8px 0 8px 0}";
	echo "em{font-size:8pt;display:block;border-top:1px black solid;margin:8px 0 0 0;padding:4px 0 0 0}";
	echo "</style>";
	echo "<body style=margin:0>";
	echo "<table cellspacing=0 cellpadding=10px width=100%>";
	echo $r;
	echo "</table>";
	echo "</body>";
	echo "</html>";
}
elseif(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"ADS_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/ad_manager/f/labels_res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$pr."\",\"".$d."\",".$jL0.",".$jL1.",".$jL2.",'".$_SESSION["j_user"]["role"]."')</script>";
}
?>