<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

try {
  $p = new PDFlib();
	$p->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");
   if($p->begin_document($SDR."/stuff/test.pdf","")==0)
     die("Error: " . $p->get_errmsg());

    $p->set_info("Creator", "AA TRAVEL GUIDES");
    $p->set_info("Author", "Unknown");
    $p->set_info("Title", "AA AD ");

    $p->begin_page_ext(595, 842, "");

		$p->set_parameter("textformat", "UTF8");
		$p->set_parameter("FontOutline", "HelveticaCondensedBold=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p->load_font("HelveticaCondensedBold", "unicode", "embedding");
    $p->setfont($font, 24.0);
		$p->set_parameter("FontOutline", "HelveticaCondensed=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p->load_font("HelveticaCondensed", "unicode", "embedding");
    $p->setfont($font, 24.0);

    $p->setcolor("fill", "cmyk", 1.0, 0.0, 0.0, 0.0);
    $p->rect(200, 200, 250, 150);
    $p->fill();
				echo $p->get_errmsg()."<hr>";

		$i=$p->load_image("auto", $SDR."/stuff/ad_templates/img/AA_cmyk.jpg", "");
		$p->fit_image($i, -7, 577, "boxsize={25 20} fitmethod=meet position=center matchbox={name=img margin=-3}");
		$p->close_image($i);

		$pre="Ut quam dolor, porttitor nec laoreet et, tempor nec tortor. Etiam vel neque eros. Sed tincidunt sollicitudin";
		$text="rus quis nulla ornare rutrum. Ut quam dolor, porttitor nec laoreet et, tempor nec tortor. Pellentesque feugiat lacus nec risus blandit pretium. Etiam vel neque eros. Sed tincidunt sollicitudin turpis consequat molestie. Nunc luctus luctus orci sed laoreet. Ut lacinia, massa at gravida facilisis, lacus nibh varius risus, quis congue nunc neque q Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut euismod erat ut nulla elementum sed posuere lectus lacinia. Pellentesque et justo non dui eleifend accumsan vitae non mauris. Nam egestas aliquet egestas. Fusce sed pu";
		$t=$p->add_textflow(0, $pre, "alignment=justify kerning=true charspacing=-0.5 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");
		$t=$p->add_textflow($t, $text, "adjustmethod=split kerning=true charspacing=-0.5 alignment=justify fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
		$p->fit_textflow($t, 0, 600, 200, 200, "verticalalign=justify linespreadlimit=120% wrap={usematchboxes={{img}}}");

    $p->set_text_pos(50, 700);
    $p->show("Hello world!");
    $p->continue_text("(says PHP)");
    $p->end_page_ext("");

    $p->end_document("");
}
catch (PDFlibException $e) {
    die("PDFlib exception occurred in hello sample:\n" .
    "[" . $e->get_errnum() . "] " . $e->get_apiname() . ": " .
    $e->get_errmsg() . "\n");
}
catch (Exception $e) {
    die($e);
}
$p = 0;
?>