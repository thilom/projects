{date}

ADVERTISING PROOF - AA TRAVEL GUIDES

Thank you for your valued support in advertising in the AA TRAVEL GUIDES AMERICAN EXPRESS ACCOMMODATION AWARDS - SOUTH AFRICA'S BEST {year}. Attached please find our proof for your insertion into the guide. We would be grateful if you would carefully check all aspects to ensure accuracy. Please sign off and fax or e-mail back to {user}.

Regret no telephonic alterations will be accepted.
Please fax back to (011) 483 2797 or 086 680 5914 or e-mail to <a href='mailto:{email}'>{email}</a>

Below are the 2 options for returning your proof:

OPTION 1 (FAX REPLY)
1.Print the page
2.Clearly indicate any changes required
3.Sign and fax back to {user} on (011) 483-2797

OPTION 2 (E-MAIL REPLY)
1.Reply via e-mail to <a href='mailto:{email}'>{email}</a>
2.Clearly indicate any changes required

Once the changes have been made a final proof will be e-mailed back to you for approval.

KINDLY NOTE THAT THIS PROOF NEEDS TO BE RETURNED TO OUR OFFICES WITHIN 48 HOURS TO
ENSURE DEADLINE DATES ARE MET.

RATE CATEGORIES CODES PER NIGHT:
A = 0 - 250
B = 250 - 499
C = 500 - 749
D = 750 - 999
E = 1000 - 1249
F = 1250 - 1499
G = 1500 - 1749
H = 1750 - 1999
I = 2000 - 2499
J = 2500 - 2999
K = 3000 - 3999
L = 4000 - 4999
M = 5000 - 5999
N = 6000 -

Kindly note that an invoice will be forwarded to you by my colleague, Kim Whittal.

Many thanks and kind regards
{user}
AA Travel Guides