{date}

ADVERTISING PROOF - AA TRAVEL GUIDES

Thank you for your valued support in advertising with the AA TRAVEL GUIDES QUALITY ASSURED {publication}. Attached please find our proof  for your insertion into the guide. We would be grateful if you would carefully check all aspects to ensure accuracy. Please sign off and fax or e-mail back to {user}.

Regret no telephonic alterations will be accepted.
Please fax back to (011) 728-3086 or e-mail to <a href='mailto:{email}'>{email}</a>

Listed below are the 2 options of returning your proof:

OPTION 1 (FAX REPLY)
1.Print the page
2.Clearly indicate any changes required
3.Sign and fax back to {user} on (011) 728-3086

OPTION 2 (E-MAIL REPLY)
1.Reply via e-mail to <a href='mailto:{email}'>{email}</a>
2.Clearly indicate any changes required

Once the changes have been made a final proof  will be e-mailed  back to you for approval.
KINDLY NOTE THAT THIS PROOF NEEDS TO BE RETURNED TO OUR OFFICES WITHIN 48 HOURS TO ENSURE DEADLINE DATES ARE MET.

Kindly note that payment of 50% is now due and an invoice will be forwarded to you by my colleague, Kim Whittal.

Many thanks and kind regards
{user}
AA Travel Guides


