<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include $SDR."/system/parse.php";
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"]) && $_GET["jL2"]?$_GET["jL2"]:10000);
$d="";
$csv="";
$pr="";

$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","year","date","pub","siz","cou","prov","town","sub","rep","est","per","ema","tel","post","appr","can","dis","vat","tot","pai","pad");
$d="";$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}
$d.=$n."~";$n++;
$d.=$n."~";$n++;
$d.=$n."~";$n++;
$d.=$n."~";$n++;


$q="SELECT ";
$q.="DISTINCT a.ad_id";
$q.=",a.invoice_id";
$q.=",a.order_id";
$q.=",a.establishment_code";
$q.=",a.last_pdf";
if(isset($s["year"]))$q.=",a.publication_year";
if(isset($s["date"]))$q.=",a.date";
if(isset($s["pub"]))$q.=",p.publication_name";
if(isset($s["cou"]))$q.=",c.country_name";
if(isset($s["prov"]))$q.=",cp.province_name";
if(isset($s["town"]))$q.=",a.town";
if(isset($s["sub"]))$q.=",a.suburb";
if(isset($s["rep"]))$q.=",e.rep_id1";
if(isset($s["est"]))$q.=",a.establishment_name";
if(isset($s["ema"]))$q.=",r.reservation_email";
if(isset($s["tel"]))$q.=",r.reservation_tel,r.reservation_cell";
if(isset($s["post"]))$q.=",r.reservation_postal1,r.reservation_postal2,r.reservation_postal3,r.reservation_postal_code";
if(isset($s["appr"]))$q.=",a.approved";
if(isset($s["can"]))$q.=",a.cancelled";
if(isset($s["siz"]))$q.=",i.j_in_name";
$q.=",v.j_inv_to_contact";
$q.=",v.j_inv_to_person";
$q.=",v.j_inv_from_rep";
$q.=",v.j_inv_total_discount";
$q.=",v.j_inv_total_vat";
$q.=",v.j_inv_total";
$q.=",v.j_inv_paid";
$q.=" FROM nse_ads AS a ";
$q.="LEFT JOIN nse_inventory AS i ON a.inventory_id=i.j_in_id ";
$q.="LEFT JOIN nse_inventory_publications AS ip ON a.inventory_id=ip.j_inp_inventory ";
$q.="LEFT JOIN nse_publications AS p ON ip.j_inp_publication=p.publication_id ";
$q.="LEFT JOIN nse_nlocations_countries AS c ON a.country_id=c.country_id ";
$q.="LEFT JOIN nse_nlocations_provinces AS cp ON a.province_id=cp.province_id ";
$q.="LEFT JOIN nse_establishment AS e ON a.establishment_code=e.establishment_code ";
if(isset($s["ema"]) || isset($s["tel"]) || isset($s["post"]))
	$q.="LEFT JOIN nse_establishment_reservation AS r ON a.establishment_code=r.establishment_code ";
$q.="LEFT JOIN nse_invoice AS v ON a.invoice_id=v.j_inv_id ";
$q.="WHERE ad_id>0 ";
if(isset($_GET["ds"]))
{
	$z=J_dateParse($_GET["ds"]);
	if($z)
		$q.="AND a.date>=".$z." ";
}
if(isset($_GET["de"]))
{
	$z=J_dateParse($_GET["de"],0,0,1);
	if($z)
		$q.="AND a.date<".$z." ";
}
if(isset($_GET["rep"]))
	$q.="AND e.rep_id1='".$_GET["rep"]."' ";
if(isset($_GET["est"]))
	$q.="AND a.establishment_code='".$_GET["est"]."' ";
if(isset($_GET["pub"]))
	$q.="AND ip.j_inp_publication=".$_GET["pub"]." ";
if(isset($_GET["typ"]))
	$q.="AND a.inventory_id='".$_GET["typ"]."' ";
if(isset($_GET["cou"]))
	$q.="AND a.country_id='".$_GET["cou"]."' ";
if(isset($_GET["prov"]))
	$q.="AND a.province_id='".$_GET["prov"]."' ";
if(isset($_GET["mou"]))
	$q.="AND a.material_outstanding".($_GET["mou"]==1?"!=0":"=0")." ";
if(isset($_GET["mup"]))
	$q.="AND a.make_up".($_GET["mup"]==1?"!=0":"=0")." ";
if(isset($_GET["mupc"]))
	$q.="AND a.make_up_complete".($_GET["mupc"]==1?"!=0":"=0")." ";
if(isset($_GET["rpro"]))
	$q.="AND a.request_proof_read".($_GET["rpro"]==1?"!=0":"=0")." ";
if(isset($_GET["proo"]))
	$q.="AND a.proof_read".($_GET["proo"]==1?"!=0":"=0")." ";
if(isset($_GET["appr"]))
	$q.="AND a.approved".($_GET["appr"]==1?"!=0":"=0")." ";
if(isset($_GET["can"]))
	$q.="AND a.cancelled".($_GET["can"]==1?"=0":"!=0")." ";
if(isset($_GET["pay"]))
	$q.="AND v.j_inv_paid".($_GET["pay"]==1?"=v.j_inv_total":"!=v.j_inv_total")." ";
$jL0=mysql_num_rows(mysql_query($q));
$q.="ORDER BY a.publication_year,p.publication_name,i.j_in_name,c.country_name,cp.province_name,a.town,a.suburb,a.establishment_name ";
$q.="LIMIT ".$jL1.",".$jL2;
$rs=mysql_query($q);
$r="";
if(mysql_num_rows($rs))
{
	$pp=array("");

	include $SDR."/custom/lib/est_items.php";

	$p=0;

	if(isset($_GET["csv"]))
	{
		$hed="";
		$c=0;
		$tot_disc=0;
		$tot_vat=0;
		$tot=0;
		$tot_pay=0;
		$i=0;
		while($g=mysql_fetch_array($rs))
		{
			if(isset($s["year"]))
			{
				$r.=",".$g["publication_year"];
				if(!$i)
				{
					$hed.=",YEAR";
					$c++;
				}
			}
			if(isset($s["date"]))
			{
				$r.=",".($g["date"]?date("d/m/Y",$g["date"]):"");
				if(!$i)
				{
					$hed.=",DATE";
					$c++;
				}
			}
			if(isset($s["pub"]))
			{
				$r.=",".str_replace(","," ",$g["publication_name"]);
				if(!$i)
				{
					$hed.=",PUBLICATION";
					$c++;
				}
			}
			if(isset($s["siz"]))
			{
				$r.=",".($g["j_in_name"]?str_replace(","," ",substr($g["j_in_name"],strrpos($g["j_in_name"]," - ")+3)):"");
				if(!$i)
				{
					$hed.=",SIZE";
					$c++;
				}
			}
			if(isset($s["cou"]))
			{
				$r.=",".($g["country_name"]?strtoupper(str_replace(","," ",$g["country_name"])):"");
				if(!$i)
				{
					$hed.=",COUNTRY";
					$c++;
				}
			}
			if(isset($s["prov"]))
			{
				$r.=",".($g["province_name"]?strtoupper(str_replace(","," ",$g["province_name"])):"");
				if(!$i)
				{
					$hed.=",PROVINCE";
					$c++;
				}
			}
			if(isset($s["town"]))
			{
				$r.=",".($g["town"]?($g["town"]!="?"?str_replace(","," ",$g["town"]):""):"");
				if(!$i)
				{
					$hed.=",TOWN";
					$c++;
				}
			}
			if(isset($s["sub"]))
			{
				$r.=",".($g["suburb"]?($g["suburb"]!="?"?trim(str_replace(","," ",$g["suburb"])," - "):""):"");
				if(!$i)
				{
					$hed.=",SUBURB";
					$c++;
				}
			}
			if(isset($s["rep"]))
			{
				$r.=",".str_replace(",",";",J_Value("","people","",($g["j_inv_from_rep"]?$g["j_inv_from_rep"]:$g["rep_id1"])));
				if(!$i)
				{
					$hed.=",REP";
					$c++;
				}
			}
			if(isset($s["est"]))
			{
				$r.=",".str_replace(","," ",$g["establishment_name"]);
				if(!$i)
				{
					$hed.=",ESTABLISHMENT";
					$c++;
				}
			}
			if(isset($s["per"]))
			{
				$p=people($g["establishment_code"],$g["j_inv_to_contact"],$g["j_inv_to_person"]);
				$r.=",".str_replace("<br>",". ",str_replace(",",";",$pp[$p]));
				if(!$i)
				{
					$hed.=",CONTACT";
					$c++;
				}
			}
			if(isset($s["ema"]))
			{
				$r.=",".str_replace(","," ",$g["reservation_email"]);
				if(!$i)
				{
					$hed.=",EMAIL";
					$c++;
				}
			}
			if(isset($s["tel"]))
			{
				$r.=",".numbers($g["establishment_code"],$p,$g["reservation_tel"],$g["reservation_cell"]);
				if(!$i)
				{
					$hed.=",TEL";
					$c++;
				}
			}
			if(isset($s["appr"]))
			{
				$r.=",".($g["approved"]?"Y":"N");
				if(!$i)
				{
					$hed.=",APPROVED";
					$c++;
				}
			}
			if(isset($s["can"]))
			{
				$r.=",".($g["cancelled"]?"Y":"N");
				if(!$i)
				{
					$hed.=",CANCELLED";
					$c++;
				}
			}
			$r.=",".($g["j_inv_total_discount"]?number_format($g["j_inv_total_discount"]):"");
			$tot_disc+=$g["j_inv_total_discount"];
			$r.=",".($g["j_inv_total_vat"]?number_format($g["j_inv_total_vat"],2,"."," "):"");
			$tot_vat+=$g["j_inv_total_vat"];
			$r.=",".($g["j_inv_total"]?number_format($g["j_inv_total"],2,"."," "):"");
			$tot+=$g["j_inv_total"];
			$r.=",".($g["j_inv_paid"]?number_format($g["j_inv_paid"],2,"."," "):"");
			$tot_pay+=$g["j_inv_paid"];
			$r.="\n";
			$i++;
		}
		$hed.=",DISCOUNT,VAT,TOTAL,PAID";
		$t="";
		$i=0;
		while($i<$c)
		{
			$t.=",";
			$i++;
		}
		$t.=",".number_format($tot_disc,2,"."," ");
		$t.=",".number_format($tot_vat,2,"."," ");
		$t.=",".number_format($tot,2,"."," ");
		$t.=",".number_format($tot_pay,2,"."," ");
		$t.="\n";
		$csv=$hed."\n".$r.$t;
	}
	else
	{
		while($g=mysql_fetch_array($rs))
		{
			$r.=$g["ad_id"]."~";
			if(isset($s["year"]))$r.=$g["publication_year"]."~";
			if(isset($s["date"]))$r.=($g["date"]?date("d/m/Y",$g["date"]):"")."~";
			if(isset($s["pub"]))$r.=$g["publication_name"]."~";
			if(isset($s["siz"]))$r.=($g["j_in_name"]?substr($g["j_in_name"],strrpos($g["j_in_name"]," - ")+3):"")."~";
			if(isset($s["cou"]))$r.=($g["country_name"]?strtoupper($g["country_name"]):"")."~";
			if(isset($s["prov"]))$r.=($g["province_name"]?strtoupper($g["province_name"]):"")."~";
			if(isset($s["town"]))$r.=($g["town"]?($g["town"]!="?"?$g["town"]:""):"")."~";
			if(isset($s["sub"]))$r.=($g["suburb"]?($g["suburb"]!="?"?trim($g["suburb"]," - "):""):"")."~";
			if(isset($s["rep"]))$r.=J_Value("","people","",($g["j_inv_from_rep"]?$g["j_inv_from_rep"]:$g["rep_id1"]))."~";
			if(isset($s["est"]))$r.=$g["establishment_name"]."~";
			if(isset($s["per"]))
			{
				$p=people($g["establishment_code"],$g["j_inv_to_contact"],$g["j_inv_to_person"]);
				$r.=$p."~";
			}
			if(isset($s["ema"]))$r.=$g["reservation_email"]."~";
			if(isset($s["tel"]))$r.=numbers($g["establishment_code"],$p,$g["reservation_tel"],$g["reservation_cell"])."~";
			if(isset($s["post"]))$r.=trim(trim($g["reservation_postal1"],","),";").($g["reservation_postal2"]?", ".trim(trim($g["reservation_postal2"],","),";"):"").($g["reservation_postal3"]?", ".trim(trim($g["reservation_postal3"],","),";"):"").($g["reservation_postal_code"]?", ".trim(trim($g["reservation_postal_code"],","),";"):"")."~";
			if(isset($s["appr"]))$r.=($g["approved"]?1:"")."~";
			if(isset($s["can"]))$r.=($g["cancelled"]?1:"")."~";
			if($g["invoice_id"])
			{
				$r.=($g["j_inv_total_discount"]?$g["j_inv_total_discount"]:"")."~";
				$r.=($g["j_inv_total_vat"]?$g["j_inv_total_vat"]:"")."~";
				$r.=($g["j_inv_total"]?$g["j_inv_total"]:"")."~";
				$r.=($g["j_inv_paid"]?$g["j_inv_paid"]:"")."~";
			}
			else
				$r.="~~~~";
			if(isset($s["pad"]))
			{
				$pd="SELECT j_invp_date FROM nse_invoice_item_paid WHERE j_invp_invoice=".$g["invoice_id"]." ORDER BY j_invp_date DESC LIMIT 1";
				$pd=mysql_query($pd);
				if(mysql_num_rows($pd))
				{
					$pd=mysql_fetch_array($pd);
					$r.=date("d/m/Y",$pd["j_invp_date"])."~";
				}
				else
					$r.="~";
			}
			else
				$r.="~";
			$r.=$g["establishment_code"]."~";
			$r.=$g["last_pdf"]."~";
			$r.=($g["order_id"]?$g["order_id"]:"")."~";
			$r.=($g["invoice_id"]?$g["invoice_id"]:"")."~";
			$r.="|";
		}
		$r=str_replace("\r","",$r);
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($pp as $k => $v)
			$pr.=$k."~".$v."|";
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"ADS_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/ad_manager/f/order_res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$pr."\",\"".$d."\",".$jL0.",".$jL1.",".$jL2.",'".$_SESSION["j_user"]["role"]."')</script>";
}
?>