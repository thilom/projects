<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include $SDR."/system/parse.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$d="";

$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","year","date","pub","siz","cou","prov","town","sub","est","qa","mou","mup","mupc","supp","appr","can","instr","con","tel","ema","pos1","pos2","pos3","posC","phys");
$d="";$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}
$d.=$n."~";$n++;
$d.=$n."~";$n++;
$d.=$n."~";$n++;
$d.=$n."~";$n++;


$q="SELECT ";
$q.="a.ad_id";
$q.=",a.invoice_id";
$q.=",a.order_id";
$q.=",a.establishment_code";
$q.=",a.last_pdf";
if(isset($s["year"]))$q.=",a.publication_year";
if(isset($s["date"]))$q.=",a.date";
if(isset($s["pub"]))$q.=",p.publication_name";
if(isset($s["cou"]))$q.=",c.country_name";
if(isset($s["prov"]))$q.=",cp.province_name";
if(isset($s["town"]))
{
	$q.=",a.town";
	$q.=",tw.town_name";
}
if(isset($s["sub"]))
{
	$q.=",a.suburb";
	$q.=",s.suburb_name";
}
if(isset($s["est"]))$q.=",a.establishment_name";
if(isset($s["qa"]))$q.=",a.qa_text";
if(isset($s["mou"]))$q.=",a.material_outstanding";
if(isset($s["mup"]))$q.=",a.make_up";
if(isset($s["mupc"]))$q.=",a.make_up_complete";
if(isset($s["supp"]))$q.=",a.supplied_material";
if(isset($s["appr"]))$q.=",a.approved";
if(isset($s["can"]))$q.=",a.cancelled";
if(isset($s["instr"]))$q.=",a.instructions";
if(isset($s["siz"]))$q.=",i.j_in_name";
if(isset($s["con"]))
{
	$q.=",v.j_inv_to_contact";
	$q.=",v.j_inv_to_person";
}
if(isset($s["ema"]))$q.=",r.reservation_email";
if(isset($s["tel"]))$q.=",r.reservation_tel,r.reservation_cell";
if(isset($s["pos1"]) || isset($s["pos2"]) || isset($s["pos3"]) || isset($s["posC"]))
{
	$q.=",e.postal_address_line1";
	$q.=",e.postal_address_line2";
	$q.=",e.postal_address_line3";
	$q.=",e.postal_address_code";
}
if(isset($s["phys"]))
{
	$q.=",e.street_address_line1";
	$q.=",e.street_address_line2";
	$q.=",e.street_address_line3";
}
$q.=" FROM nse_ads AS a ";
$q.="LEFT JOIN nse_inventory AS i ON a.inventory_id=i.j_in_id ";
$q.="LEFT JOIN nse_inventory_publications AS ip ON a.inventory_id=ip.j_inp_inventory ";
$q.="LEFT JOIN nse_publications AS p ON ip.j_inp_publication=p.publication_id ";
$q.="LEFT JOIN nse_nlocations_countries AS c ON a.country_id=c.country_id ";
$q.="LEFT JOIN nse_nlocations_provinces AS cp ON a.province_id=cp.province_id ";
$q.="LEFT JOIN nse_nlocations_towns AS tw ON a.town_id=tw.town_id ";
$q.="LEFT JOIN nse_nlocations_suburbs AS s ON a.suburb_id=s.suburb_id ";
$q.="LEFT JOIN nse_establishment AS e ON a.establishment_code=e.establishment_code ";
if(isset($s["ema"]) || isset($s["tel"]))
	$q.="LEFT JOIN nse_establishment_reservation AS r ON a.establishment_code=r.establishment_code ";
if(isset($s["con"]))
	$q.="LEFT JOIN nse_invoice AS v ON a.invoice_id=v.j_inv_id ";
$q.="WHERE ad_id>0 ";
if(isset($_GET["ds"]))
{
	$z=J_dateParse($_GET["ds"]);
	if($z)
		$q.="AND a.date>=".$z." ";
}
if(isset($_GET["de"]))
{
	$z=J_dateParse($_GET["de"],0,0,1);
	if($z)
		$q.="AND a.date<".$z." ";
}
if(isset($_GET["est"]))
	$q.="AND a.establishment_code='".$_GET["est"]."' ";
if(isset($_GET["pub"]))
	$q.="AND ip.j_inp_publication=".$_GET["pub"]." ";
if(isset($_GET["typ"]))
	$q.="AND a.inventory_id='".$_GET["typ"]."' ";
if(isset($_GET["cou"]))
	$q.="AND a.country_id='".$_GET["cou"]."' ";
if(isset($_GET["prov"]))
	$q.="AND a.province_id='".$_GET["prov"]."' ";
if(isset($_GET["mou"]))
	$q.="AND a.material_outstanding".($_GET["mou"]==1?"!=0":"=0")." ";
if(isset($_GET["mup"]))
	$q.="AND a.make_up".($_GET["mup"]==1?"!=0":"=0")." ";
if(isset($_GET["mupc"]))
	$q.="AND a.make_up_complete".($_GET["mupc"]==1?"!=0":"=0")." ";
if(isset($_GET["supp"]))
	$q.="AND a.supplied_material".($_GET["supp"]==1?"!=0":"=0")." ";
if(isset($_GET["appr"]))
	$q.="AND a.approved".($_GET["appr"]==1?"!=0":"=0")." ";
if(isset($_GET["can"]))
	$q.="AND a.cancelled".($_GET["can"]==1?"=0":"!=0")." ";
$jL0=mysql_num_rows(mysql_query($q));
$q.="ORDER BY a.publication_year,p.publication_name,c.country_name,cp.province_name,a.town,a.suburb,a.establishment_name ";
$q.="LIMIT ".$jL1.",".$jL2;
$rs=mysql_query($q);
if(isset($_GET["csv"]))
{
	$hed="";
	$tot="";
	$i=0;
	$c=0;
}
$r="";
$pr="";
if(mysql_num_rows($rs))
{

	$pp=array("");

	include $SDR."/custom/lib/est_items.php";

	$p=0;

	while($g=mysql_fetch_array($rs))
	{
		if(isset($_GET["csv"]))
		{
			if(isset($s["year"]))
			{
				$r.=",".$g["publication_year"];
				if(!$i)
				{
					$hed.=",YEAR";
					$c++;
				}
			}
			if(isset($s["date"]))
			{
				$r.=",".($g["date"]?date("d/m/Y",$g["date"]):"");
				if(!$i)
				{
					$hed.=",DATE";
					$c++;
				}
			}
			if(isset($s["pub"]))
			{
				$r.=",".str_replace(",",";",$g["publication_name"]);
				if(!$i)
				{
					$hed.=",PUBLICATION";
					$c++;
				}
			}
			if(isset($s["siz"]))
			{
				$r.=",".str_replace(",",";",($g["j_in_name"]?substr($g["j_in_name"],strrpos($g["j_in_name"]," - ")+3):""));
				if(!$i)
				{
					$hed.=",SIZE";
					$c++;
				}
			}
			if(isset($s["cou"]))
			{
				$r.=",".str_replace(",","",($g["country_name"]?strtoupper($g["country_name"]):""));
				if(!$i)
				{
					$hed.=",COUNTRY";
					$c++;
				}
			}
			if(isset($s["prov"]))
			{
				$r.=",".str_replace(",","",($g["province_name"]?strtoupper($g["province_name"]):""));
				if(!$i)
				{
					$hed.=",PROVINCE";
					$c++;
				}
			}
			if(isset($s["town"]))
			{
				$r.=",".str_replace(",","",strtoupper($g["town"]&&$g["town"]!="?"?$g["town"]:$g["town_name"]));
				if(!$i)
				{
					$hed.=",TOWN";
					$c++;
				}
			}
			if(isset($s["sub"]))
			{
				$r.=",".str_replace(",","",($g["suburb"]&&$g["suburb"]!="?"?trim($g["suburb"]," - "):$g["suburb_name"]));
				if(!$i)
				{
					$hed.=",SUBURB";
					$c++;
				}
			}
			if(isset($s["est"]))
			{
				$r.=",".str_replace(",","",$g["establishment_name"]);
				if(!$i)
				{
					$hed.=",ESTABLISHMENT";
					$c++;
				}
			}
			if(isset($s["qa"]))
			{
				$r.=",".str_replace("\n","   ",str_replace(",","",$g["qa_text"]));
				if(!$i)
				{
					$hed.=",QA/RES TYPE";
					$c++;
				}
			}
			if(isset($s["mou"]))
			{
				$r.=",".($g["material_outstanding"]?"Y":"");
				if(!$i)
				{
					$hed.=",MATERIAL OUTSTANDING";
					$c++;
				}
			}
			if(isset($s["mup"]))
			{
				$r.=",".($g["make_up"]?"Y":"");
				if(!$i)
				{
					$hed.=",MAKEUP";
					$c++;
				}
			}
			if(isset($s["mupc"]))
			{
				$r.=",".($g["make_up_complete"]?"Y":"");
				if(!$i)
				{
					$hed.=",MAKEUP COMPLETE";
					$c++;
				}
			}
			if(isset($s["supp"]))
			{
				$r.=",".($g["supplied_material"]?"Y":"");
				if(!$i)
				{
					$hed.=",SUPPLIED MATERIAL";
					$c++;
				}
			}
			if(isset($s["appr"]))
			{
				$r.=",".($g["approved"]?"Y":"");
				if(!$i)
				{
					$hed.=",APPROVED";
					$c++;
				}
			}
			if(isset($s["can"]))
			{
				$r.=",".($g["cancelled"]?"Y":"");
				if(!$i)
				{
					$hed.=",CANCELLED";
					$c++;
				}
			}
			if(isset($s["instr"]))
			{
				$r.=",".str_replace("<br>",". ",str_replace(",",";",($g["instructions"]?$g["instructions"]:"")));
				if(!$i)
				{
					$hed.=",INSTRUCTIONS";
					$c++;
				}
			}
			if(isset($s["con"]))
			{
				$p=people($g["establishment_code"],$g["j_inv_to_contact"],$g["j_inv_to_person"]);
				$r.=",".str_replace("<br>",". ",str_replace(",",";",$pp[$p]));
				if(!$i)
				{
					$hed.=",CONTACT";
					$c++;
				}
			}
			if(isset($s["tel"]))
			{
				$r.=",".numbers($g["establishment_code"],$p,$g["reservation_tel"],$g["reservation_cell"]);
				if(!$i)
				{
					$hed.=",TEL";
					$c++;
				}
			}
			if(isset($s["ema"]))
			{
				$r.=",".str_replace(","," ",$g["reservation_email"]);
				if(!$i)
				{
					$hed.=",EMAIL";
					$c++;
				}
			}
			if(isset($s["pos1"]))
			{
				$r.=",".str_replace(","," ",$g["postal_address_line1"]);
				if(!$i)
				{
					$hed.=",POST 1";
					$c++;
				}
			}
			if(isset($s["pos2"]))
			{
				$r.=",".str_replace(","," ",$g["postal_address_line2"]);
				if(!$i)
				{
					$hed.=",POST 2";
					$c++;
				}
			}
			if(isset($s["pos3"]))
			{
				$r.=",".str_replace(","," ",$g["postal_address_line3"]);
				if(!$i)
				{
					$hed.=",POST 3";
					$c++;
				}
			}
			if(isset($s["posC"]))
			{
				$r.=",".str_replace(","," ",$g["postal_address_code"]);
				if(!$i)
				{
					$hed.=",CODE";
					$c++;
				}
			}
			if(isset($s["phys"]))
			{
				$r.=",".str_replace(","," ",$g["street_address_line1"].($g["street_address_line2"]?" / ".$g["street_address_line2"]:"").($g["street_address_line3"]?" / ".$g["street_address_line3"]:""));
				if(!$i)
				{
					$hed.=",PHYSICAL";
					$c++;
				}
			}
			$r.="\n";
			$i++;
		}
		else
		{
			$r.=$g["ad_id"]."~";
			if(isset($s["year"]))$r.=$g["publication_year"]."~";
			if(isset($s["date"]))$r.=($g["date"]?date("d/m/Y",$g["date"]):"")."~";
			if(isset($s["pub"]))$r.=$g["publication_name"]."~";
			if(isset($s["siz"]))$r.=($g["j_in_name"]?substr($g["j_in_name"],strrpos($g["j_in_name"]," - ")+3):"")."~";
			if(isset($s["cou"]))$r.=($g["country_name"]?strtoupper($g["country_name"]):"")."~";
			if(isset($s["prov"]))$r.=($g["province_name"]?strtoupper($g["province_name"]):"")."~";
			if(isset($s["town"]))$r.=($g["town"]&&$g["town"]!="?"?$g["town"]:$g["town_name"])."~";
			if(isset($s["sub"]))$r.=($g["suburb"]&&$g["suburb"]!="?"?trim($g["suburb"]," - "):$g["suburb_name"])."~";
			if(isset($s["est"]))$r.=$g["establishment_name"]."~";
			if(isset($s["qa"]))$r.=str_replace("\n","<br>",str_replace("\"","&quot;",$g["qa_text"]))."~";
			if(isset($s["mou"]))$r.=($g["material_outstanding"]?1:"")."~";
			if(isset($s["mup"]))$r.=($g["make_up"]?1:"")."~";
			if(isset($s["mupc"]))$r.=($g["make_up_complete"]?1:"")."~";
			if(isset($s["supp"]))$r.=($g["supplied_material"]?1:"")."~";
			if(isset($s["appr"]))$r.=($g["approved"]?1:"")."~";
			if(isset($s["can"]))$r.=($g["cancelled"]?1:"")."~";
			if(isset($s["instr"]))$r.=($g["instructions"]?$g["instructions"]:"")."~";
			if(isset($s["con"]))
			{
				$p=people($g["establishment_code"],$g["j_inv_to_contact"],$g["j_inv_to_person"]);
				$r.=$p."~";
			}
			if(isset($s["tel"]))$r.=numbers($g["establishment_code"],$p,$g["reservation_tel"],$g["reservation_cell"])."~";
			if(isset($s["ema"]))$r.=$g["reservation_email"]."~";
			if(isset($s["pos1"]))$r.=$g["postal_address_line1"]."~";
			if(isset($s["pos2"]))$r.=$g["postal_address_line2"]."~";
			if(isset($s["pos3"]))$r.=$g["postal_address_line3"]."~";
			if(isset($s["posC"]))$r.=$g["postal_address_code"]."~";
			if(isset($s["phys"]))$r.=$g["street_address_line1"].($g["street_address_line2"]?"<br>".$g["street_address_line2"]:"").($g["street_address_line3"]?"<br>".$g["street_address_line3"]:"")."~";
			$r.=$g["establishment_code"]."~";
			$r.=$g["last_pdf"]."~";
			$r.=$g["order_id"]."~";
			$r.=$g["invoice_id"]."~";
			$r.="|";
		}
	}
	if(isset($_GET["csv"]))
		$csv=$hed."\n".$r;
	else
	{
		$r=str_replace("\r","",$r);
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($pp as $k => $v)
			$pr.=$k."~".$v."|";
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"ADS_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/ad_manager/f/res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$pr."\",\"".$d."\",".$jL0.",".$jL1.",".$jL2.",'".$_SESSION["j_user"]["role"]."')</script>";
}
?>