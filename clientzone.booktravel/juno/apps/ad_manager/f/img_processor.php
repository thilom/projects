<?php

if (!function_exists('img4Pdf')) {
	function img4Pdf($img="", $web="", $w, $h, $iw, $ih, $x, $y, $spn=0) {
	global $SDR;
	$s = $_SERVER["DOCUMENT_ROOT"];
	$file = $SDR . "/ico/0.gif";
	if (!empty($img) || !empty($web)) {
		$src = "";
		if ($img && file_exists($s . "/hires_images/" . $img)) {
			$src = $s . "/hires_images/" . $img;
		} elseif ($web) {
			$img = $web;
			if (file_exists("http://booktravel.travel/res_images/" . $img))	$src = "http://booktravel.travel/res_images/" . $img;
		}

		if ($src) {
			$x = $x < 0 ? 0 - $x : 0;
			$y = $y < 0 ? 0 - $y : 0;
			$d = getimagesize($src);
			$f = 1;
			while ($f) {
				if ($d[0] < $iw * $f || $d[1] < $ih * $f)
					break;
				$f+=0.05;
			}
			$file = $s . "/hires_temp/" . $img;
			$a = "/usr/bin/convert " . $src . " -crop " . ($w * $f) . "x" . ($h * $f) . "+" . ($x * $f) . "+" . ($y * $f) . " " . ($spn ? " -sharpen 0x1.0" : "") . " " . $file;
			exec($a);
		}
	}

	return $file;

}
}
?>