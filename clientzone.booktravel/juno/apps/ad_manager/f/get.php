<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

$t=$_GET["t"];
$tid=$_GET["town_id"];
$name=strtolower(str_replace(array("\"","'"),"",str_replace("+++","&",$_GET["name"])));
$o="";

if($t=="town")
{
	$tn=strtolower(str_replace(array("\"","'"),"",preg_replace('~[^a-zA-Z0-9 &\-\.]~',"",str_replace("+++","&",$_GET["town_name"]))));
	$q="SELECT DISTINCT t.town_id AS townid,t.town_name,s.suburb_id,s.suburb_name FROM nse_nlocations_towns AS t LEFT JOIN nse_nlocations_suburbs AS s ON t.town_id=s.town_id WHERE (lower(t.town_name) LIKE '%".$name."%' OR lower(s.suburb_name) LIKE '%".$name."%' OR t.town_id=".$tid.") ORDER BY t.town_name";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		$chk=array();
		while($g=mysql_fetch_array($q))
		{
			if(!isset($chk[$g["townid"]]))
			{
				$chk[$g["townid"]]=1;
				$o.="T".$g["townid"]."~".stripslashes(trim($g["town_name"]))."|";
				$s="SELECT suburb_id,suburb_name FROM nse_nlocations_suburbs WHERE town_id=".$g["townid"]." ORDER BY suburb_name";
				$s=mysql_query($s);
				if(mysql_num_rows($s))
				{
					while($gs=mysql_fetch_array($s))
						$o.="S".$gs["suburb_id"]."~&#9658; ".stripslashes(trim($gs["suburb_name"]))."|";
				}
			}
		}
	}
}

else
{
	$q="SELECT DISTINCT region_id,region_name FROM nse_nlocations_cregions WHERE lower(region_name) LIKE '%".$name."%' ORDER BY region_name";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
			$o.="R".$g["region_id"]."~".stripslashes(trim($g["region_name"]))."|";
	}
}

echo "<script>window.parent.J_ajax_R('".$_GET["d"]."'".($o?",\"".$o."\"":"").")</script>";
?>