<?php

include_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include_once $SDR . "/apps/ad_manager/f/default.php";
include_once $SDR . "/system/activity.php";
include_once $SDR . "/system/dir.php";
include_once $SDR . "/system/get.php";
include_once $SDR . "/system/parse.php";
include_once $SDR . "/apps/ad_manager/f/ads_history.php";
include_once $SDR . "/apps/ad_manager/f/notify.php";
include_once $SDR . "/apps/ad_manager/f/img_processor.php";
include_once $SDR . "/apps/notes/f/insert.php";
include_once $SDR . "/apps/accounts/invoices/types.php";
include_once $SDR . "/apps/accounts/invoices/types.php";

//Vars
$rates_sgl = '';

if (!isset($_SESSION["juno"]["est_img"]))
	$_SESSION["juno"]["est_img"] = array();
if (!isset($_SESSION["juno"]["ad_info"]))
	$_SESSION["juno"]["ad_info"] = array();

$suid = $_SESSION["j_user"]["id"];
$per = mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id='" . $suid . "' LIMIT 1"));

$id = isset($_POST["id"]) ? $_POST["id"] : (isset($_GET["id"]) ? $_GET["id"] : 0);

if ($id) {
	// put all ad contents into an array
	$q = mysql_query("SELECT * FROM nse_ads WHERE ad_id=" . $id . " LIMIT 1");
	if (mysql_num_rows($q))
		$adb = mysql_fetch_array($q);
	else {
		echo "<html>";
		echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
		echo "<script src=" . $ROOT . "/system/P.js></script>";
		echo "<body><tt><b style=color:#CC0000>ATTENTION!</b> This add could not be found!</tt></body></html>";
		die();
	}
}

$inventory_id = (isset($_GET["inventory_id"]) ? $_GET["inventory_id"] : (isset($adb["inventory_id"]) ? $adb["inventory_id"] : 0));

$template = 0;
if (file_exists($SDR . "/stuff/ad_templates/" . $inventory_id . ".php"))
	$template = $inventory_id;

$pub = mysql_fetch_array(mysql_query("SELECT * FROM nse_inventory LEFT JOIN nse_inventory_publications ON j_in_id=j_inp_inventory WHERE j_in_id=" . $inventory_id . " LIMIT 1"));

$eid = isset($_GET["eid"]) ? $_GET["eid"] : (isset($adb["establishment_code"]) ? $adb["establishment_code"] : 0);

$scale = isset($_POST["scale"]) ? $_POST["scale"] : (isset($_GET["scale"]) ? $_GET["scale"] : 6);
unset($_POST["scale"]);
$sharpen = isset($_POST["sharpen"]) ? 1 : 0;
$sabest = strpos($pub["j_in_name"], "SA Best") !== false ? 1 : 0;
$accdir = strpos($pub["j_in_name"], "Accommodation Directory") !== false ? 1 : 0;
$colors = array(
	"Gold" => array("#FFD700", "0,25,100,0")
);

if (!$id) {
	$adb = 0;
	// copy from specified previous ad - this bypasses all insert functions later at line 838
	if ($pub["j_inp_copy"])
		include $SDR . "/apps/ad_manager/f/edit_copy.php";
	if (!$adb) // put into blank array
		$adb = array
			(
			"order_number" => isset($_GET["order_number"]) ? $_GET["order_number"] : 0
			, "order_item_id" => isset($_GET["order_item_id"]) ? $_GET["order_item_id"] : 0
			, "inventory_id" => $inventory_id
			, "publication_year" => $pub["j_inp_year"]
			, "country_id" => 1
			, "province_id" => 0
			, "section_id" => 0
			, "region_id" => 0
			, "region" => ""
			, "town_id" => 0
			, "town" => ""
			, "suburb_id" => 0
			, "suburb" => ""
			, "establishment_name" => ""
			, "image_src" => ""
			, "image_dimensions" => $pub["j_inp_images"]
			, "image_position" => ""
			, "qa_logo" => 1
			, "qa_text" => "QA Status Pending"
			, "qa_status" => ""
			, "rooms" => ""
			, "description" => ""
			, "star_grading" => ""
			, "address" => ""
			, "rates" => "?"
			, "instructions" => ""
			, "notes" => ""
			, "cancelled" => ""
			, "files" => ""
			, "make_up" => 0
			, "proof_read" => 0
			, "request_proof_read" => 0
			, "material_outstanding" => 0
			, "approved" => 0
			, "sharpen" => 0
			, "award" => "?"
			, "icons" => "3~"
			, "color" => ""
			, "side" => 0
		);
}

$esd = mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='" . $eid . "' LIMIT 1"));
$inventory_id = $adb["inventory_id"];



// user saves and delete
if (isset($_GET["del"])) { // delete ads
	// check if this item is in a group invoice (by reference j_invit_reference) or if NOT paid - then delete
	$paid = 0;
	$items = 1;
	$item_deleted = 0;
	$proforma = 1;
	// check if invoice is pro-forma
	$inv = mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=" . $adb["invoice_id"]));
	if ($inv["j_inv_id"] && $inv["j_inv_type"] != 1)
		$proforma = 0;
	elseif ($inv["j_inv_id"]) { // check if invoice has paid elements
		$q = "SELECT * FROM nse_invoice_item LEFT JOIN nse_invoice_item_paid ON j_invit_id=j_invp_invoice_item WHERE j_invit_invoice=" . $adb["invoice_id"];
		$q = mysql_query($q);
		if (mysql_num_rows($q)) {
			$items = mysql_num_rows($q);
			while ($g = mysql_fetch_array($q)) {
				if ($g["j_invp_date"]) {
					$paid = 1;
					break;
				} elseif ($g["j_invit_reference"] == "AD" . $id) {
					if ($items > 1) {
						// invoice notes
						insertNote($eid, 2, "INV" . $g["j_invit_invoice"], "Deleted advert Item (from group Invoice No " . $inv["j_inv_number"] . "): " . addslashes(preg_replace("~[^a-zA-Z0-9 &';,\.\-\(\)]~", "", $g["j_invit_name"])) . " (Invoice: " . $g["j_invit_invoice"] . " / Item: " . $g["j_invit_id"] . " / Ref: " . $g["j_invit_reference"] . ")", 0, 0, 1, 4, 0, 0, "notify_ad_delete");
						mysql_query("DELETE FROM nse_invoice_item WHERE j_invit_id=" . $g["j_invit_id"]);
						mysql_query("DELETE FROM nse_invoice_item_paid WHERE j_invp_item=" . $g["j_invit_id"]);
						J_act($types[$inv["j_inv_type"]], 5, $inv["j_inv_number"] . " Row Item removed (AD" . $id . "/" . $inv["j_inv_id"] . "/" . $g["j_invit_id"] . ")", $id, $eid);
						$item_deleted = 1;
					}
				}
			}
		}
	}
	if ($items < 2 && !$paid && !$item_deleted && $proforma) {

		function delInv($id=0) {
			if ($id) {
				global $SDR, $EPOCH, $eid;
				$g = mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=" . $id . " LIMIT 1"));
				$it = mysql_fetch_array(mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=" . $id . " LIMIT 1"));
				$num = $g["j_inv_type"] == 9 ? $g["j_inv_order"] : $g["j_inv_number"];
				mysql_query("UPDATE nse_invoice SET j_inv_cancel_date=" . $EPOCH . ",j_inv_cancel_user=" . $_SESSION["j_user"]["id"] . " WHERE j_inv_id=" . $id);
				mysql_query("UPDATE nse_invoice SET j_inv_cancel_date=" . $EPOCH . ",j_inv_cancel_user=" . $_SESSION["j_user"]["id"] . " WHERE j_inv_invoice_id=" . $id);
				insertNote($eid, 2, "INV" . $id, "Deleted advert Item and cancelled Invoice No " . $g["j_inv_number"] . "): " . addslashes(preg_replace("~[^a-zA-Z0-9 &';,\.\-\(\)]~", "", $it["j_invit_name"])) . " (Invoice: " . $id . " / Item: " . $it["j_invit_id"] . " / Ref: " . $it["j_invit_reference"] . ")", 0, 0, 1, 4, 0, 0, "notify_ad_delete");
				J_act($types[$g["j_inv_type"]], 13, ($g["j_inv_type"] == 9 ? $g["j_inv_order"] : $g["j_inv_number"]), $id, $eid);
			}
		}

		delInv($adb["order_id"]);
		delInv($adb["invoice_id"]);
	}
	echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
	echo "<script src=" . $ROOT . "/system/P.js></script>";
	echo "<script>J_footer('');w=j_P.j_Wi[j_W]['F'][0]['W'];j_P.J_resize(j_W,'360,-1,0,0,0,0,0,0');w.J_opaq();w.location=ROOT+'/apps/ad_manager/manager.php?del=1&id=" . $eid . "'</script>";
	if ($item_deleted)
		echo "<tt><b><var>NOTE!</var> Although this ad has been deleted, the invoice to which this ad belonged is a grouped invoice containing references to other ads. This ad, component parts, associated files, directories, invoices and invoice components have been successfully deleted.<hr><a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_view.php?id=" . $adb["invoice_id"] . "');return false\" onfocus=blur()>View invoice " . $inv["j_inv_number"] . "</a></b></tt>";
	elseif (!$proforma)
		echo "<tt><b><var>WARNING!</var> This ad cannot be deleted! The invoice to which this ad belongs is a not a pro-forma invoice.<hr><a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_view.php?id=" . $adb["invoice_id"] . "');return false\" onfocus=blur()>View invoice " . $inv["j_inv_number"] . "</a></b></tt>";
	elseif ($paid)
		echo "<tt><b><var>WARNING!</var> This ad cannot be deleted! The invoice for this ad has items that have been paid.<hr><a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/invoice_view.php?id=" . $adb["invoice_id"] . "');return false\" onfocus=blur()>View invoice " . $inv["j_inv_number"] . "</a></b></tt>";
	else
		echo "<tt><b><var>DELETED!</var> This ad, component parts, associated files, directories, invoices and invoice components have been successfully deleted.</b></tt>";
	if (!$proforma || $paid) {

	} else {
		insertNote($eid, 7, "", "Deleted advert Item but no invoice to cancel", 0, 0, 0, 4, 0, 0, "notify_ad_delete");
		mysql_query("DELETE FROM nse_ads WHERE ad_id=" . $id . " AND establishment_code='" . $eid . "'");
		mysql_query("DELETE FROM nse_ads_history WHERE adhis_ad=" . $id);
		J_DeleteDir("/stuff/establishments/" . $eid . "/ads/" . $id);
		J_act("Ad", 5, $pub["j_in_name"], $id, $eid);
	}
	die();
} elseif (count($_POST))
	include $SDR . "/apps/ad_manager/f/edit_post.php";

//Get awards
$statuses = array('W'=>'Winner','F'=>'Finalist','HC'=>'Highly Commended','S'=>'Semi-Finalist','GA'=>'Gold Achiever', 'HF'=>'Hall of Fame');
if (!isset($award) || $award = '?') {
	$award_status = '';
	$award_year = '';
	$statement = "SELECT status, year
				FROM award
				WHERE code='$eid' AND year='2011'
				ORDER By year ASC
				LIMIT 1";
	$sql_award = $GLOBALS['dbCon']->prepare($statement);
	$sql_award->execute();
	$sql_award->bind_result($award_status, $award_year);
	$sql_award->fetch();
	$sql_award->close();

	if (!empty($award_year)) {
		$award = "{$statuses[$award_status]} $award_year AA Travel Guides Accommodation  Awards";
	}
}

if ($id) // put all ad contents into an array
	$adb = mysql_fetch_array(mysql_query("SELECT * FROM nse_ads WHERE ad_id=" . $id . " LIMIT 1"));

$order_item_id = stripslashes($adb["order_item_id"]);
$country_id = $adb["country_id"] ? $adb["country_id"] : 1;
$province_id = $adb["province_id"] ? $adb["province_id"] : 0;
$section_id = $adb["section_id"] ? $adb["section_id"] : 0;

$region_id = $adb["region_id"] ? $adb["region_id"] : 0;
$region = $adb["region"] ? stripslashes($adb["region"]) : "?";
$town_id = $adb["town_id"] ? $adb["town_id"] : 0;
$town = $adb["town"] ? stripslashes($adb["town"]) : "?";
$suburb_id = $adb["suburb_id"] ? $adb["suburb_id"] : 0;
$suburb = $adb["suburb"] ? stripslashes($adb["suburb"]) : "?";
$establishment_name = $adb["establishment_name"] ? stripslashes($adb["establishment_name"]) : "?";
$qa_logo = $adb["qa_logo"];
$qa_text = str_replace('B&b','B&B',stripslashes($adb["qa_text"]));
$qa_status = stripslashes($adb["qa_status"]);
$description = $adb["description"] ? stripslashes($adb["description"]) : "?";
$rates = trim($adb["rates"]) ? stripslashes($adb["rates"]) : "?";
$rates_sgl = isset($adb['rates_sgl'])&&trim($adb["rates_sgl"]) ? stripslashes($adb["rates_sgl"]) : "?";
$rooms = trim($adb["rooms"]) ? stripslashes($adb["rooms"]) : "?";
if ($adb["address"]) {
	$address = stripslashes($adb["address"]);
	// fix old addresses
//	$address = str_replace(array("%EF%BF%BD", "&#183;", "&#x00B7;"), "", $address);
	//$address=str_replace("T:","<img src=".$ROOT."/stuff/ad_templates/img/icons_rgb/tel.jpg> ",$address);
	//$address=str_replace("C:","<img src=".$ROOT."/stuff/ad_templates/img/icons_rgb/cell.jpg> ",$address);
	//$address=str_replace("F:","<img src=".$ROOT."/stuff/ad_templates/img/icons_rgb/fax.jpg> ",$address);
	//$address=str_replace("E:","<img src=".$ROOT."/stuff/ad_templates/img/icons_rgb/email.jpg> ",$address);
	//$address=str_replace("W:","<img src=".$ROOT."/stuff/ad_templates/img/icons_rgb/web.jpg> ",$address);
	$address = str_replace(array("  ", "  "), " ", $address);
}
$address = isset($address) ? $address : "?";
$qas = "";
$qal = 1;
$qat = "";
$make_up = $adb["make_up"];
$star_grading = $adb["star_grading"] * 1 > 0 ? $adb["star_grading"] : substr($adb["star_grading"], 0, 1) * 1;
$star_grading = $star_grading > 0 ? $star_grading : 0;
$award = !empty($award)? $award : "?";
$icons = $adb["icons"] ? $adb["icons"] : "?";
$color = $adb["color"] ? $adb["color"] : "";
$side = $adb["side"];

//Icons
if ($icons == '?') {
	$icons = '';
	$statement = "SELECT icon_id
				FROM nse_establishment_icon
				WHERE establishment_code='$eid'";
	$sql_icons = $GLOBALS['dbCon']->prepare($statement);
	$sql_icons->execute();
	$sql_icons->bind_result($icon_id);
	while ($sql_icons->fetch()) {
		$icons .= "$icon_id~";
	}
	$sql_icons->close();
}
if (empty($icons)) $icons = '?';

$adWidth = $pub["j_inp_width"];
$adHeight = $pub["j_inp_height"];

function trim_($s="") {
	if ($s) {
		$s = str_replace("  ", " ", $s);
		$s = str_replace("\n", " ", $s);
		$s = str_replace("\r", "", $s);
		$s = str_replace(",,", ",", $s);
	}
	return $s;
}

function tel_($v="") {
	$v = str_replace("(0) ", "(0)", $v);
	$v = str_replace(" 27 (", " +27 (", $v);
	$v = str_replace("++ 27 (", "+27 (", $v);
	return $v;
}

$r = "SELECT *,t.town_id as townid FROM nse_establishment AS a
LEFT JOIN nse_user_establishments AS ue ON a.establishment_code=ue.establishment_code
LEFT JOIN nse_user AS u ON ue.user_id=u.user_id
LEFT JOIN nse_establishment_billing AS b ON a.establishment_code=b.establishment_code
LEFT JOIN nse_establishment_location AS l ON a.establishment_code=l.establishment_code
LEFT JOIN nse_nlocations_towns AS t ON l.town_id=t.town_id
LEFT JOIN nse_nlocations_suburbs AS s ON l.suburb_id=s.suburb_id
LEFT JOIN nse_establishment_restype AS sg ON a.establishment_code=sg.establishment_code
WHERE a.establishment_code='" . $eid . "' LIMIT 1";
$r = mysql_query($r);
$est = mysql_fetch_array($r);
$est["town_id"] = $est["townid"]; // fix suburb/town_id conflict
if ($est["street_address_line1"])
	$physical = trim(str_replace("  ", " ", $est["street_address_line1"] . ($est["street_address_line2"] ? ", " . $est["street_address_line2"] : "") . ($est["street_address_line3"] ? ", " . $est["street_address_line3"] : "")));
elseif ($est["billing_line1"])
	$physical = trim(str_replace("  ", " ", $est["billing_line1"] . ($est["billing_line2"] ? ", " . $est["billing_line2"] : "") . ($est["billing_line3"] ? ", " . $est["billing_line3"] : "")));

$tel = "";
$cell = "";
$fax = "";
$email = "";
$gps = '';

$r = "SELECT * FROM nse_establishment_public_contact WHERE establishment_code='" . $eid . "'";
$r = mysql_query($r);
if (mysql_num_rows($r)) {
	while ($gc = mysql_fetch_array($r)) {
		if ($gc["contact_value"]) {
			if (!$cell) {
				if ($gc["contact_type"] == "cell1")
					$cell = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "cell2")
					$cell = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "cell3")
					$cell = tel_(stripslashes(trim_($gc["contact_value"])));
			}
			if (!$tel) {
				if ($gc["contact_type"] == "tel1")
					$tel = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "tel2")
					$tel = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "tel3")
					$tel = tel_(stripslashes(trim_($gc["contact_value"])));
			}
			if (!$fax) {
				if ($gc["contact_type"] == "fax1")
					$fax = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "fax2")
					$fax = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "fax3")
					$fax = tel_(stripslashes(trim_($gc["contact_value"])));
			}
			if (!$email) {
				if ($gc["contact_type"] == "email")
					$email = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "email1")
					$email = tel_(stripslashes(trim_($gc["contact_value"])));
				elseif ($gc["contact_type"] == "email2")
					$email = tel_(stripslashes(trim_($gc["contact_value"])));
			}
		}
	}
}

$est_name = $est["establishment_name"] ? strtoupper(stripslashes(trim_($est["establishment_name"]))) : "No Name";
$est_name = $est_name ? $est_name : "?";
$qas = $est["aa_category_code"];
$website = $est["website_url"] ? strtolower(trim_(stripslashes($est["website_url"]))) : "";

if ($sabest ||  $accdir) {
	$addr = ($tel ? "<img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/tel.jpg> " . $tel . "&nbsp;" : "");
	$addr.=($cell ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/cell.jpg> " . $cell . "&nbsp;" : "");
	$addr.=($fax ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/fax.jpg> " . $fax . "&nbsp;" : "");
	$addr.=($physical ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/physical.jpg> " . $physical . "&nbsp;" : "");
	$addr.=($email ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/email.jpg> " . $email . "&nbsp;" : "");
	$addr.=($gps ? " GPS: " . $gps . "&nbsp;" : "");
	$addr.=($website ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/web.jpg> " . str_replace(array("http://", "https://"), "", $website) : "");

} else {
	$addr = ($tel ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/tel.jpg> " . $tel . "&nbsp;" : "");
	$addr.=($cell ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/cell.jpg> " . $cell . "&nbsp;" : "");
	$addr.=($fax ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/fax.jpg> " . $fax . "&nbsp;" : "");
	$addr.="<br>";
	$addr.=($email ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/email.jpg> " . $email . "&nbsp;" : "");
	$addr.=($website ? " <img src=" . $ROOT . "/stuff/ad_templates/img/icons_rgb/web.jpg> " . str_replace(array("http://", "https://"), "", $website) : "");
	$addr = trim($addr, " &#x00B7;");
}
$addr = preg_replace('~[^a-zA-Z0-9 ,;:!+<>=@#_%"\'\/\&\*\$\.\-\(\)]~', "", $addr);
$addr = trim($addr);
$addr = str_replace("  ", " ", $addr);
$addr = $addr ? $addr : "?";

$est["address"] = $addr;
$est["region_id"] = 0;
$est["region"] = "";

if (!$id) {
	$country_id = $est["country_id"] ? $est["country_id"] : 1;
	$province_id = $est["province_id"] ? $est["province_id"] : 0;
	$town_id = $est["town_id"] ? $est["town_id"] : 0;
	$town = ($est["town_name"] ? stripslashes($est["town_name"]) : "?");
	$suburb_id = $est["suburb_id"] ? $est["suburb_id"] : 0;
	$suburb = ($est["suburb_name"] ? " " . stripslashes($est["suburb_name"]) : "?");
	$star_grading = ($est["star_grading"] > 0 ? $est["star_grading"] : 0);
	$establishment_name = $est_name;
	$address = $addr;
}

if ($est["town_id"] || $est["suburb_id"]) {
	$adb["region_id"] = 0;
	$adb["region"] = "?";

	if (!$est["town_id"]) { // find town id else-where
		$q = mysql_query("SELECT town_id FROM nse_nlocations_suburbs WHERE suburb_id=" . $est["suburb_id"] . " LIMIT 1");
		if (mysql_num_rows($q)) {
			$g = mysql_fetch_array($q);
			$est["town_id"] = $g["town_id"];
		}
	}

	if ($est["town_id"]) {
		$q = "SELECT * FROM nse_nlocations_cregion_locations
		INNER JOIN nse_nlocations_cregions ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
		AND parent_type LIKE 'province'
		WHERE location_id=" . $est["town_id"] . " AND location_type LIKE 'town'
		LIMIT 1";
		$q = mysql_query($q);
		if (mysql_num_rows($q)) {
			$q = mysql_fetch_array($q);
			$adb["region_id"] = $q["region_id"];
			$adb["region"] = stripslashes($q["region_name"]);
		}
	}

	if (!$id || isset($copy)) {
		$region_id = $adb["region_id"];
		$region = $adb["region"];
		$region = $region ? $region . " - " : "";
	}
}

$region = $region ? $region : (!$id && $est["province_name"] ? $est["province_name"] . " - " : "?");

$est["description"] = "";

$r = "SELECT establishment_description,description_type FROM nse_establishment_descriptions WHERE establishment_code='" . $eid . "' AND (description_type='short_description' OR description_type='long_description')";
$r = mysql_query($r);
if (mysql_num_rows($r)) {
	while ($g = mysql_fetch_array($r)) {
		if (($pub["j_inp_height"] > 60 || $sabest || $accdir) && $est["description"] && $g["description_type"] == "long_description")
			break;
		elseif ($g["description_type"] == "short_description" && $g["establishment_description"])
			$est["description"] = $g["establishment_description"];
		elseif (($sabest || !$est["description"]) && $g["establishment_description"])
			$est["description"] = $g["establishment_description"];
	}
	$est["description"] = $est["description"] ? trim_(stripslashes($est["description"])) : "?";
	if ($pub["j_inp_height"] < 40 && !$sabest)
		$est["description"] = substr($est["description"], 0, 90);
}

if (!$id)
	$description = $est["description"] ? $est["description"] . " " : "?";

$_qa_status = "";
if ($qas == "H/S" || $qas == "HR" || $qas == "R" || $qas == "R/H" || $qas == "S") {
	$r = "SELECT aa_category_name FROM nse_aa_category WHERE aa_category_code='" . $qas . "' LIMIT 1";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		$g = mysql_fetch_array($r);
		$_qa_status = stripslashes($g["aa_category_name"]);
	}
}

if ((!$id || isset($copy)) && !$_qa_status)
	$_qa_status = "QA Status Pending";

$res_type = "";
$_rooms = "";

$r = "SELECT subcategory_id FROM nse_establishment_restype WHERE establishment_code='" . $eid . "' LIMIT 1";
$r = mysql_query($r);
if (mysql_num_rows($r)) {
	$g = mysql_fetch_array($r);
	if ($g["subcategory_id"]) {
		$r = "SELECT subcategory_name FROM nse_restype_subcategory_lang WHERE subcategory_id=" . $g["subcategory_id"] . " LIMIT 1";
		$r = mysql_query($r);
		if (mysql_num_rows($r)) {
			$g = mysql_fetch_array($r);
			$res_type = $g["subcategory_name"];
			$res_type = str_ireplace("/", " / ", $res_type);
			$res_type = str_replace("  ", " ", $res_type);
			$res_type = str_ireplace("Bed & Breakfast", "B&B", $res_type);
			$res_type = str_ireplace(array("B&B /", "B&B-", "B&B -", "B&B"), "B&B", $res_type);
			$res_type = str_ireplace(array("self-catering", "self- catering", "self -catering", "self - catering"), "Self-Catering", $res_type);
			$res_type = str_ireplace(array("country-style", "country- style", "country -style", "country - style"), "Country-Style", $res_type);
			$res_type = str_ireplace("cottages / chalets", "Cottages/Chalets", $res_type);
			$res_type = str_ireplace("hotel / inn", "Hotel/Inn", $res_type);
			$res_type = str_ireplace("and / or", "/", $res_type);
			$res_type = ucwords(strtolower($res_type));
			$res_type = str_ireplace(" With ", " with ", $res_type);
			$res_type = str_ireplace(" By ", " by ", $res_type);
			$res_type = str_ireplace(" a ", " a ", $res_type);
			$res_type = str_ireplace(" The ", " the ", $res_type);
			$res_type = str_ireplace(" Of ", " of ", $res_type);
			$res_type = str_replace(" / ", "/", $res_type);
			$res_type = trim(str_replace("  ", " ", stripslashes($res_type)));
		}
	}

	$room_type = "";

	$r = mysql_query("SELECT room_count,room_type FROM nse_establishment_data WHERE establishment_code='" . $eid . "' LIMIT 1");
	if (mysql_num_rows($r)) {
		$g = mysql_fetch_array($r);
		$_rooms = $g["room_count"];
		$room_type = $g["room_type"];
	}

	if ($_qa_status)
		$qal = 1;

	$qat.=$_qa_status;
	$qat.=($res_type ? (($_qa_status ? " - " : "") . trim_(trim($res_type, "-"))) : "");
	$qat.=($_rooms ? ": " . $_rooms : "");
	$qat.=" " . ($room_type ? ucfirst(strtolower(trim(trim_($room_type), "s"))) . ($_rooms != 1 ? "s" : "") : "Rooms") . ".";
	$qat = stripslashes($qat);
}

$est["ql"] = $qal;
$est["qa"] = $qat;
if (!$id) {
	$qa_logo = $qal;
	$qa_text = $qat;
}

$rts = "";
$rts_low = '';
$r = "SELECT price_in_high,price_in_low,category_prefix,category_suffix, category_low FROM nse_establishment_pricing WHERE establishment_code='" . $eid . "' LIMIT 1";
$r = mysql_query($r);
if (mysql_num_rows($r)) {
	include $SDR . "/custom/lib/est_price_codes.php";
	$g = mysql_fetch_array($r);
	$low = $g["price_in_low"] ? get_price_code($g["price_in_low"]) : "";
	$hi = $g["price_in_high"] ? get_price_code($g["price_in_high"]) : "";
	$sgl_low = $g["category_low"] ? get_price_code($g["category_low"]) : "";
	if ($low || $hi) {
		$rts = $low . ($hi && $low != $hi ? ($low ? " - " : "") . $hi : "") . " " . str_ireplace("  ", " ", str_ireplace("b&b", "B&B", str_ireplace("sharing", "", $g["category_suffix"])));
		$rts = stripslashes($rts);
	}
	if ($low) {
		$rts_low = 'Sgl: ' . $sgl_low . " " .str_ireplace("  ", " ", str_ireplace("b&b", "B&B", str_ireplace("sharing", "", $g["category_suffix"])));
	}
}

$rts = !$rts ? "?" : $rts;
$est["rates"] = $rts;
$est["rooms"] = $_rooms;
$est["qa_status"] = $_qa_status;

if (!$id) {
	$rates = $rts;
	$rooms = $_rooms;
	$qa_status = $_qa_status;
}

// img array
$img = "";
if ($template) {
	$img = array();
	$imgContainer = explode("|", trim($pub["j_inp_images"], "|"));
	foreach ($imgContainer as $k => $v)
		$imgContainer[$k] = explode("~", $v);
	$images_req = count($imgContainer);
	$image_src = $adb["image_src"];
	$image_dimensions = explode("|", $adb["image_dimensions"]);
	$image_position = explode("|", $adb["image_position"]);
	$image_all = "";

	if (!$image_src || !isset($_SESSION["juno"]["est_img"][$eid])) {
		$r = "SELECT * FROM nse_establishment_images WHERE establishment_code='" . $eid . "' ORDER BY primary_image DESC";
		$r = mysql_query($r);
		if (mysql_num_rows($r)) {
			while ($g = mysql_fetch_array($r)) {
				$image_src.=$g["image_name"] . "~" . $g["hi_res"] . "|";
				if (!isset($_SESSION["juno"]["est_img"][$eid]))
					$image_all.=$g["thumb_name"] . "~" . $g["image_name"] . "~" . $g["original"] . "~" . ($g["date"] ? date("d/m/Y", $g["date"]) : "") . "~" . ($g["user"] ? J_Value("", "people", "", $g["user"]) : "") . "|";
			}
			if ($image_all)
				$_SESSION["juno"]["est_img"][$eid] = 1;
		}
	}

	if ($template && $image_src) {
		$image_src = explode("|", trim($image_src, "|"));
		$i = 0;
		while (isset($image_src[$i]) && $image_src[$i] && $i < $images_req) {
			$image_src[$i] = explode("~", $image_src[$i]);
			$s = "http://aatravel.co.za/res_images/" . $image_src[$i][0];
			$img[$i] = array("conWidth" => $imgContainer[$i][0],
								"conHeight" => $imgContainer[$i][1],
								"src" => $s,
								"web" => $image_src[$i][0],
								"hirez" => (isset($image_src[$i][1]) ? $image_src[$i][1] : ""));
			if ($id && $image_src[$i]) {
				$s = explode("~", $image_dimensions[$i]);
				$img[$i]["width"] = $s[0];
				$img[$i]["height"] = $s[1];
				$s = explode("~", $image_position[$i]);
				$img[$i]["left"] = $s[0];
				$img[$i]["top"] = $s[1];
			} else {
				$img[$i]["left"] = 0;
				$img[$i]["top"] = 0;
				$s = ($image_src[$i][0] && file_exists($s) ? getimagesize($s) : array(10, 10));
				$p = J_imgFit($s[0], $s[1], $img[$i]["conWidth"]);
				if ($p[1] < $img[$i]["conHeight"]) {
					$p = J_imgFit($s[0], $s[1], 0, $img[$i]["conHeight"]);
					$img[$i]["left"] = 0 - (($p[0] - $img[$i]["conWidth"]) / 2);
				}
				else
					$img[$i]["top"] = 0 - (($p[1] - $img[$i]["conHeight"]) / 2);
				$img[$i]["width"] = $p[0];
				$img[$i]["height"] = $p[1];
			}
			$i++;
		}
	}

	if (!count($img)) {
		$i = 0;
		while ($i < $images_req) {
			$img[$i] = array("conWidth" => $imgContainer[$i][0], "conHeight" => $imgContainer[$i][1], "src" => $ROOT . "/ico/0.gif", "web" => "", "hirez" => "", "width" => $imgContainer[$i][0], "height" => $imgContainer[$i][0], "left" => 0, "top" => 0);
			$i++;
		}
	}

	$stars = "";
	$i = 0;
	while ($i < $star_grading) {
		$stars.="<img src=" . $ROOT . "/stuff/ad_templates/img/star_rgb.jpg>";
		$i++;
	}
}

// color for top-strip
$cp = "";
$color_rgb = "";
$color_cmyk = "";

$q = "SELECT * FROM nse_nlocations_regions_content JOIN nse_nlocations_countries ON location_id=country_id WHERE region_id=61 ORDER BY country_name";
$q = mysql_query($q);
if (mysql_num_rows($q)) {
	while ($g = mysql_fetch_array($q)) {
		$c = "";
		$qc = mysql_query("SELECT * FROM nse_nlocations_colors WHERE country_id=" . $g["country_id"] . " LIMIT 1");
		if (mysql_num_rows($qc)) {
			$gc = mysql_fetch_array($qc);
			$c = $gc["color_rgb"];
			if ($country_id == $g["country_id"]) {
				$color_rgb = $gc["color_rgb"];
				$color_cmyk = $gc["color_cmyk"];
			}
		}
		$cp.=$g["country_id"] . "~" . $g["country_name"] . "~" . $c . "~";
		$qp = "SELECT * FROM nse_nlocations_provinces WHERE country_id=" . $g["country_id"] . " ORDER BY province_name";
		$qp = mysql_query($qp);
		if (mysql_num_rows($qp)) {
			while ($gp = mysql_fetch_array($qp)) {
				$c = "";
				$qc = mysql_query("SELECT * FROM nse_nlocations_colors WHERE province_id=" . $gp["province_id"] . " LIMIT 1");
				if (mysql_num_rows($qc)) {
					$gc = mysql_fetch_array($qc);
					$c = $gc["color_rgb"];
					if ($province_id == $gp["province_id"]) {
						$color_rgb = $gc["color_rgb"];
						$color_cmyk = $gc["color_cmyk"];
					}
				}
				$cp.=$gp["province_id"] . "`" . $gp["province_name"] . "`" . $c . "*";
			}
		}
		$cp.="|";
	}
}

if ($color && isset($colors[$color])) {
	$color_rgb = trim($colors[$color][0], ",");
	$color_cmyk = trim($colors[$color][1], ",");
}

$a = explode(",", $color_cmyk);
$color_cmyk = "";
$color_k = "";
$i = 0;
while ($i < 4) {
	$color_cmyk.=(!isset($a[$i]) || empty($a[$i]) ? 0 : round($a[$i] / 100, 2)) . ",";
	$color_k.=($i == 3 ? 1 : (!isset($a[$i]) || empty($a[$i]) ? 0 : round($a[$i] / 100, 2))) . ",";
	$i++;
}

$color_cmyk = trim($color_cmyk, ",");
$color_k = trim($color_k, ",");


// icons
$icons_db = "";
$icon_img = "";
if ($sabest || $accdir) {
	if (!$id) {
//		$icon_img = "<img src=http://www.aatravel.co.za/icons1/AMEX.gif>";
//		$icons = "3~";
		$q = "SELECT * FROM nse_establishment_icon WHERE establishment_code='" . $eid . "'";
		$q = mysql_query($q);
		if (mysql_num_rows($q)) {
			while ($g = mysql_fetch_array($q))
				$icons.=$g["icon_id"] . "~";
		}
		$icons = $icons ? $icons : "";
	}
	// db icons
	// first 1 is amex
//	$icons_db.="3~American Express~AMEX|";
//	if ($id && strpos("~" . $icons, "~3~") !== false)
//		$icon_img.="<img src=http://www.aatravel.co.za/icons1/AMEX.gif>";
	$q = "SELECT c.icon_category_id,l.icon_name FROM nse_icon_category AS c JOIN nse_icon_category_lang AS l ON c.icon_category_id=l.icon_category_id ORDER BY c.icon_priority";
	$q = mysql_query($q);
	if (mysql_num_rows($q)) {
		while ($g = mysql_fetch_array($q)) {
			$icons_db.="~" . $g["icon_name"] . "|";
			$qi = "SELECT DISTINCT i.icon_id,i.icon_url,l.icon_description FROM nse_icon AS i JOIN nse_icon_lang AS l ON i.icon_id=l.icon_id WHERE icon_category_id=" . $g["icon_category_id"] . " AND i.icon_id!=3 ORDER BY l.icon_description";
			$qi = mysql_query($qi);
			if (mysql_num_rows($qi)) {
				$i = "~" . $icons;
				while ($gi = mysql_fetch_array($qi)) {
					$icons_db.=$gi["icon_id"] . "~" . $gi["icon_description"] . "~" . str_replace(".gif", "", substr($gi["icon_url"], strrpos($gi["icon_url"], "/"))) . "|";
					if (strpos($i, "~" . $gi["icon_id"] . "~") !== false)
						$icon_img.="<img src=" . $gi["icon_url"] . ">";
					if ($gi['icon_id'] == 81) {
						$icons_db.="3~American Express~AMEX|";
						if ($id && strpos("~" . $icons, "~3~") !== false)
							$icon_img.="<img src=http://www.aatravel.co.za/icons1/AMEX.gif>";
					}
				}
			}
		}
	}
}

// insert from order
if (!count($_POST) && isset($_GET["new_order"]) && !isset($copy)) {
	$i_src = "";
	$i_dimensions = "";
	$i_position = "";
	if ($template) {
		foreach ($img as $k => $v) {
			$i_src.=$v["web"] . "~" . $v["hirez"] . "|";
			$i_position.=$v["left"] . "~" . $v["top"] . "|";
			$i_dimensions.=$v["width"] . "~" . $v["height"] . "|";
		}
	}

	$q = "INSERT INTO nse_ads (
	establishment_code
	,invoice_id
	,order_id
	,order_item_id
	,inventory_id
	,publication_year
	,date
	,country_id
	,province_id
	,region_id
	,region
	,town_id
	,town
	,suburb_id
	,suburb
	,establishment_name
	,rates";
	if ($template) {
		$q.=",image_src
		,image_dimensions
		,image_position
		,qa_logo
		,qa_text
		,description
		,star_grading
		,address";
	}
	$q.=",user
	) VALUES (
	'" . $_GET["eid"] . "'
	," . $_GET["invoice_id"] . "
	," . $_GET["order_id"] . "
	," . $_GET["order_item_id"] . "
	," . $inventory_id . "
	," . $pub["j_in_year"] . "
	," . $EPOCH . "
	," . $country_id . "
	," . $province_id . "
	," . $region_id . "
	,'" . addslashes($region) . "'
	," . $town_id . "
	,'" . addslashes($town) . "'
	," . $suburb_id . "
	,'" . addslashes($suburb) . "'
	,'" . J_parseValue($establishment_name, array("upper" => 1, "tagsall" => 1)) . "'
	,'" . J_parseValue($rates, array("tagsall" => 1)) . "'";
	if ($template) {
		$q.=",'" . $i_src . "'
		,'" . $i_dimensions . "'
		,'" . $i_position . "'
		," . $qa_logo . "
		,'" . J_parseValue($qa_text, array("tagsall" => 1, "ucfirst" => 1)) . "'
		,'" . J_parseValue($description, array("tagsall" => 1, "ucfirst" => 1)) . "'
		,'" . J_parseValue($star_grading, array("tagsall" => 1)) . "'
		,'" . J_parseValue($address, array("tagsall" => 1, "ucfirst" => 1)) . "'";
	}
	$q.="," . $suid . "
	)";
	mysql_query($q);
	if (mysql_error())
		echo mysql_error() . "<tt>" . $q . "</tt><hr>";
	// get ad id
	$id = J_ID("nse_ads", "ad_id");
	// update invoice item with reference to the ad-id in column j_invit_reference
	mysql_query("UPDATE nse_invoice_item SET j_invit_reference='AD" . $id . "' WHERE j_invit_id=" . $_GET["order_item_id"]);
	// reload general ad array values
	$adb = mysql_fetch_array(mysql_query("SELECT * FROM nse_ads WHERE ad_id=" . $id . " LIMIT 1"));
	// ad notes
	adHist($id, "Created");
	// activity
	J_act("Ad", 3, $pub["j_in_name"] . $id, $_GET["eid"]);
	$insert = 1;
}

// render pdf
elseif (isset($render_pdf)) {
	J_PrepFile("/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf");
	J_SetPerm($SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf");
	$pdf_title = "AA AD | " . $est["establishment_name"] . " | " . $pub["j_in_name"] . " (" . $pub["j_inp_width"] . "x" . $pub["j_inp_height"] . "mm) | " . date("d/m/Y", $EPOCH);

	$defile = $EPOCH . ($template ? "" : "_strip") . ".pdf";
	$pdf_filename = $SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf/" . $defile;

	$pdf_width = $pub["j_inp_width"];
	$pdf_height = ($template ? $pub["j_inp_height"] : 4);
	$pdf_author = J_Value("", "people", "", $_SESSION["j_user"]["id"]);
	$points = 0.3528;
	$s = isset($sharpen) ? 1 : 0;
	// get pdf creator template
	if ($template) {
		include $SDR . "/stuff/ad_templates/" . $template . ".php";
		mysql_query("UPDATE nse_ads SET last_pdf='" . $defile . "' WHERE ad_id=" . $id);
		$adb["last_pdf"] = $defile;
//		die();
	} else {
		include $SDR . "/stuff/ad_templates/strip.php";
		mysql_query("UPDATE nse_ads SET last_strip_pdf='" . $defile . "' WHERE ad_id=" . $id);
		$adb["last_strip_pdf"] = $defile;
	}

	J_SetPerm($pdf_filename);
	adHist($id, "PDF Created - " . $defile);
	if (isset($_GET["noti"])) {
		$n = explode("~", $_GET["noti"]);
		foreach ($n as $k => $v) {
			if ($v)
				adHist($id, "Notification sent to " . $v);
		}
	}

	header("Location: " . $ROOT . "/apps/ad_manager/f/edit.php?eid=" . $eid . "&id=" . $id . "&ret=1&rendpdf=1");
	die();
}


// html
echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/ad_manager/f/edit.js></script>";

if ($pub["j_inp_deadline"] > $EPOCH)
	echo "<script>Aded('" . date("d/m/Y", $pub["j_inp_deadline"]) . "')</script>";
if ($adb["cancelled"])
	echo "<script>Acan()</script>";

echo "<tt id=tt><h6>Publication: " . $pub["j_in_name"] . "</h6>Size " . $pub["j_inp_width"] . "&times;" . $pub["j_inp_height"] . "mm";
if ($adb["order_id"]) {
	$g = mysql_fetch_array(mysql_query("SELECT j_inv_order FROM nse_invoice WHERE j_inv_id=" . $adb["order_id"] . " LIMIT 1"));
	echo ($g["j_inv_order"] ? " / Order number: " . $g["j_inv_order"] : "");
	if (isset($insert) && $g["j_inv_order"])
		adHist($id, "Created Order " . $g["j_inv_order"]);
}
if ($adb["invoice_id"]) {
	$g = mysql_fetch_array(mysql_query("SELECT j_inv_number FROM nse_invoice WHERE j_inv_id=" . $adb["invoice_id"] . " LIMIT 1"));
	echo ($g["j_inv_number"] ? " / Invoice number: " . $g["j_inv_number"] : "");
	if (isset($insert) && $g["j_inv_number"])
		adHist($id, "Created Invoice " . $g["j_inv_number"]);
}
echo " / AD" . $id;
if ($template)
	echo " / Template " . $inventory_id;
echo "</tt>";
echo "<hr>";

if ($template && count($img) < $images_req)
	echo "<script>A_ad_x('" . $eid . "')</script>";

echo "<form method=post ";
if ($template)
	echo "onsubmit='return A_sub()'";
else
	echo "enctype=multipart/form-data onsubmit=J_opaq()";
echo">";

echo "<script>A_reg(\"" . $cp . "\"," . $country_id . "," . $province_id . ")</script>";

$edit = 1;
echo "<br><table style='width:100%; '><tr><td>";
if ($template) {
	include_once $SDR . "/stuff/ad_templates/" . $inventory_id . ".php";
	echo $ad;
} else {
	// top-strip template
	include_once $SDR . "/stuff/ad_templates/strip.php";
	echo $ad;
	J_DirList("/stuff/establishments/" . $eid . "/ads/" . $id . "/files");
	$f = "";
	foreach ($J_filelist as $k => $v)
		$f.=$v . "~" . J_Size(filesize($SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/files/" . $v)) . "|";
	echo "<script>A_file(";
	echo "'" . $_SESSION["j_user"]["role"] . "'";
	echo ",'" . $eid . "'";
	echo "," . $id;
	echo ",\"" . $f . "\"";
	echo "," . ($make_up ? "\"" . J_Value("", "people", "", $adb["make_up"]) . "\"" : 0);
	echo "," . ($adb["make_up_complete"] ? "\"" . J_Value("", "people", "", $adb["make_up_complete"]) . "\"" : 0);
	echo "," . ($adb["supplied_material"] ? "\"" . J_Value("", "people", "", $adb["supplied_material"]) . "\"" : 0);
	echo ")</script>";
}

echo "</div>";
echo "<u class=ji>";
echo "<input type=text name=id value=" . $id . ">";
echo "<input type=text name=section_id value=\"" . $section_id . "\">";
echo "<input type=text name=region_id value=\"" . $region_id . "\">";
echo "<input type=text name=region value=\"" . $region . "\">";
echo "<input type=text name=town_id value=\"" . $town_id . "\">";
echo "<input type=text name=town value=\"" . $town . "\">";
echo "<input type=text name=suburb_id value=\"" . $suburb_id . "\">";
echo "<input type=text name=suburb value=\"" . $suburb . "\">";
echo "<input type=text name=establishment_name value=\"" . $establishment_name . "\">";
echo "<textarea name=rates>" . $rates . "</textarea>";
echo "<textarea name=rates_sgl>" . $rates_sgl . "</textarea>";
echo "<textarea name=rooms>" . $rooms . "</textarea>";
echo "<textarea name=qa_status>" . $qa_status . "</textarea>";
if ($template) {
	echo "<input type=text name=qa_logo value=" . $qa_logo . ">";
	echo "<textarea name=qa_text>" . $qa_text . "</textarea>";
	echo "<textarea name=description>" . $description . "</textarea>";
	echo "<textarea name=address>" . $address . "</textarea>";
	echo "<input type=text name=star_grading value=" . $star_grading . ">";
	echo "<input type=text name=award value=\"" . $award . "\">";
	echo "<input type=text name=icons value=" . $icons . ">";
	echo "<input type=text name=image_src value=\"\">";
	echo "<input type=text name=image_dimensions value=\"\">";
	echo "<input type=text name=image_position value=\"\">";
	echo "<input type=text name=scale value=\"" . $scale . "\">";
}
echo "<input type=text name=color value=\"" . $color . "\">";
echo "</u>";

echo "</td><td style='text-align: right'>";

//Get the image color space
$colorspace = '';
$spaces = array(1=>'UNDEFINED',
    'RGB',
    'GRAY',
    'TRANSPARENT',
    'OHTA',
    'LAB',
    'XYZ',
    'YCBCR',
    'YCC',
    'YIQ',
    'YPBPR',
    'YUV',
    'CMYK',
    'SRGB',
    'HSB',
    'HSL',
    'HWB',
    'REC601LUMA',
    'REC709LUMA',
    'LOG ');

//var_dump($img[0]['src']);
//$src_image = "{$img[0]['src']}";
//
//
//$im = new Imagick($src_image);
//$profiles = $im->getImageProfiles(true);
//$attached_profiles = implode(',', $profiles);
//if (empty($attached_profiles)) $attached_profiles = 'none';
//$has_icc_profile = (array_search('icc', $profiles) !== false);
//
//
//$profile = $im->identifyImage();
//
//echo "<table style='border: 1px solid gray; width: 300px' align=right>";
//echo "<tr><th style='background-color: gray; color: black; text-align: center' colspan=2>Ad Information</th></tr>";
//echo "<tr><td style='font-size: .8em; font-weight: bold; background-color: silver'>Image Color Space</td><td style='font-size: .8em; '>{$profile['colorSpace']}</td></tr>";
//echo "<tr><td style='font-size: .8em; font-weight: bold; background-color: silver'>Image Resolution</td><td style='font-size: .8em; '>X:{$profile['resolution']['x']}, Y:{$profile['resolution']['y']}</td></tr>";
//echo "<tr><td style='font-size: .8em; font-weight: bold; background-color: silver'>Image Profiles</td><td style='font-size: .8em; '>$attached_profiles</td></tr>";
//
//echo "</table>";

echo "</td></tr></table>";
$oid = $id ? $adb["order_id"] : 0;
$iid = $id ? $adb["invoice_id"] : 0;

echo "<script>";
echo "instr=\"" . str_replace("\"", "", $adb["instructions"]) . "\"";
echo ";nota=\"" . str_replace("\"", "", $adb["notes"]) . "\"";
if ($template) {
	echo ";scl=" . $scale;
	echo ";img=[";
	foreach ($img as $k => $v) {
		echo ($k ? "," : "");
		echo "{";
		echo "'conWidth':" . ($v["conWidth"] * $scale);
		echo ",'conHeight':" . ($v["conHeight"] * $scale);
		echo ",'width':" . ($v["width"] * $scale);
		echo ",'height':" . ($v["height"] * $scale);
		echo ",'left':" . ($v["left"] * $scale);
		echo ",'top':" . ($v["top"] * $scale);
		echo ",'src':'" . $v["web"] . "'";
		echo ",'hirez':'" . $v["hirez"] . "'";
		echo "}";
	}
	echo "]";
}
echo ";db=1";
echo ";dbql=\"" . $est["ql"] . "\"";
echo ";dbqa=\"" . $est["qa"] . "\"";
echo ";dbra=\"" . $est["rates"] . "\"";
echo ";dbro=\"" . $est["rooms"] . "\"";
echo ";dbqastatus=\"" . $est["qa_status"] . "\"";
echo ";dbdc=\"" . addslashes($est["description"]) . "\"";
echo ";dbad=\"" . stripslashes(str_replace("  ", " ", str_replace(array("&nbsp;", ","), "<br>", preg_replace("~<img src=.*?>~", "", $est["address"])))) . "\"";
echo ";dbrid=" . ($est["region_id"] ? $est["region_id"] : 0);
echo ";dbrn=\"" . J_Value("region_name", "nse_nlocations_regions", "region_id", $est["region_id"]) . "\"";
echo ";dbtnid=" . ($est["town_id"] ? $est["town_id"] : 0);
echo ";dbtn=\"" . J_Value("town_name", "nse_nlocations_towns", "town_id", $est["town_id"]) . "\"";
echo ";dbsg=\"" . $star_grading . "\"";
echo ";sabest=" . $sabest;
echo ";accdir=" . $accdir;
echo ";extra_color=\"";
foreach ($colors as $k => $v)
	echo $k . "~" . $v[0] . "|";
echo "\"";
echo ";A_tail(";
echo "'" . $_SESSION["j_user"]["role"] . "'";
echo "," . $id;
echo ",'" . $eid . "'";
echo "," . (file_exists($SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf/" . $adb["last_pdf"]) ? "'" . $adb["last_pdf"] . "'" : 0);
echo "," . (file_exists($SDR . "/stuff/establishments/" . $eid . "/ads/" . $id . "/pdf/" . $adb["last_strip_pdf"]) ? "'" . $adb["last_strip_pdf"] . "'" : 0);
echo "," . (isset($_GET["rendpdf"]) ? 1 : 0);
echo "," . $oid;
echo "," . $iid;
echo "," . ($template && $image_all ? "\"" . str_replace(array("\n", "\r"), "", $image_all) . "\"" : 0);
echo "," . $inventory_id;
echo "," . ($adb["material_outstanding"] ? "\"" . J_Value("", "people", "", $adb["material_outstanding"]) . "\"" : 0);
echo "," . ($adb["request_proof_read"] ? "\"" . J_Value("", "people", "", $adb["request_proof_read"]) . "\"" : 0);
echo "," . ($adb["proof_read"] ? "\"" . J_Value("", "people", "", $adb["proof_read"]) . "\"" : 0);
echo "," . ($adb["approved"] ? "\"" . J_Value("", "people", "", $adb["approved"]) . "\"" : 0);
echo "," . ($adb["cancelled"] ? "\"" . J_Value("", "people", "", $adb["cancelled"]) . "\"" : 0);
echo "," . ($adb["sharpen"] ? 1 : 0);
echo "," . ($template ? 1 : 0);
echo "," . ($side ? 1 : 0);
echo "," . ($icons_db ? "\"" . $icons_db . "\"" : 0);
echo (isset($insert) ? ",2" : (isset($_GET["ret"]) ? ",1" : ""));
echo ")";
echo "</script>";
?>