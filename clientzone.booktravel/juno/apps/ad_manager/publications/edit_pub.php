<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
$id=isset($_GET["id"])?$_GET["id"]:0;

$err="";
if(count($_POST))
{
	include $SDR."/system/parse.php";

	$n=preg_replace("~[^a-zA-Z0-9 ]~","",$_POST["pn"]);
	if($id)
	{
		mysql_query("UPDATE nse_publications SET aa_entity='".$_POST["aa"]."',publication_name='".$n."',publication_year='".substr(preg_replace('~[^0-9]~',"",$_POST["yr"]),0,4)."',expiry_date='".J_dateParse($_POST["ex"],0,2)."',closed=".(isset($_POST["cl"])?1:0)." WHERE publication_id=".$id);
		echo mysql_error();
	}
	else
	{
		$q="SELECT * FROM nse_publications WHERE publication_name='".$n."' AND publication_year=".$_POST["yr"]." LIMIT 1";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
			$err="<var><tt><b>ATTENTION!</b> A publication with this name and year already exists</tt></var>";
		else
		{
			include $SDR."/system/get.php";
			mysql_query("INSERT INTO nse_publications (aa_entity,publication_name,publication_year,expiry_date) VALUES ('".$_POST["aa"]."','".$n."',".$_POST["yr"].",'".J_dateParse($_POST["ex"],0,2)."')");
			echo mysql_error();
			echo "<body><tt><b>Added</b></tt><script>J_frame(0,ROOT+'/apps/ad_manager/edit_publications.php?refco=".$_POST["aa"]."&ref=".J_ID("nse_publications","publication_id")."')</script>";
			die();
		}
	}
}

if($id)
	$pub=mysql_fetch_array(mysql_query("SELECT * FROM nse_publications WHERE publication_id=".$id." LIMIT 1"));

$p="";
$r=mysql_query("SELECT DISTINCT publication_name FROM nse_publications ORDER BY publication_name");
if(mysql_num_rows($r))
{
	while($g=mysql_fetch_array($r))
		$p.="<a href=go: onclick=\\\"j_G['self'].P_v(this);return false\\\" onfocus=blur()>".$g["publication_name"]."</a>";
}
echo "<script>pp=\"<b class=jA>".$p."</b>\";function P_v(t){j_E.value=t.innerHTML}</script>";

echo "<body onload=J_validate()>";
echo $err;

echo "<form method=post>";
echo "<table id=jTR class=jW100>";
echo "<tr><th>Company</th><td width=99%><select name=aa class=jW100>";

$q="SELECT establishment_code,establishment_name FROM nse_establishment WHERE aa_entity='Y' ORDER BY establishment_name LIMIT 5";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($ge=mysql_fetch_array($q))
		echo "<option value=".$ge["establishment_code"].($id && $pub["aa_entity"]==$ge["establishment_code"]?" selected":"").">".$ge["establishment_name"];
}
echo "</select></td></tr>";

echo "<tr><th>Publication</th><td><input type=text ".($id?"value=\"".$pub["publication_name"]."\" ":"")."name=pn class=jW100 onmouseover=\"J_TT(this,'Select from the list or add a new publication name')\" onclick=J_note(this,pp,240,3,0,-1) title='Requires value' style=font-weight:bold></td></tr>";

echo "<tr><th>Year</th><td width=99%><select name=yr>";
$i=2006;
$f=date("Y",$EPOCH)+1;
while($f>$i)
{
	echo "<option value=".$f.($id && $pub["publication_year"]==$f?" selected":"").">".$f;
	$f--;
}
echo "</select></td></tr>";

echo "<tr><th>Deadline</th><td><input type=text name=ex class=jW100 onclick=j_P.J_datePicker(self,3) onmouseover=\"J_TT(this,'<b>From Date</b><br>DD/MM/YYYY')\" value=".($id?($pub["expiry_date"]?substr($pub["expiry_date"],8,2)."/".substr($pub["expiry_date"],5,2)."/".substr($pub["expiry_date"],0,4):""):date("d/m/Y",mktime(date("H"),date("i"),date("s"),date("m")+3,date("d"),date("Y"))))."></td></tr>";

echo "<tr><th>Closed</th><td><input type=checkbox name=cl value=1".($id && $pub["closed"]?" checked":"")." onmouseover=\"J_TT(this,'This option will hide the publication from order selection')\"></td></tr>";

echo "</table>";
echo "<br><input type=submit value=OK>";
echo "</form>";

echo "<script>J_tr()</script>";
echo "</body></html>";
?>