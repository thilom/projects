<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

if(isset($_GET["ad"]))
{
	$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_inventory_publications WHERE j_inp_inventory=".$_GET["ad"]." LIMIT 1"));
	echo "<script>window.parent.T_ad('".$g["j_inp_width"]."','".$g["j_inp_height"]."','".$g["j_inp_images"]."')</script>";
	die();
}

elseif(isset($_GET["d"]))
{
	$o="";
	$qp=mysql_query("SELECT * FROM nse_publications ORDER BY publication_year DESC,publication_name");
	if(mysql_num_rows($qp))
	{
		while($gp=mysql_fetch_array($qp))
		{
			$i="";
			$r=mysql_query("SELECT * FROM nse_inventory JOIN nse_inventory_publications ON j_in_id=j_inp_inventory WHERE j_inp_publication=".$gp["publication_id"]." ORDER BY j_in_name");
			if(mysql_num_rows($r))
			{
				while($g=mysql_fetch_array($r))
				{
					if(isset($_GET["cpy"]) || file_exists($SDR."/stuff/ad_templates/".$g["j_in_id"].".php"))
						$i.=$g["j_in_id"]."~".substr($g["j_in_name"],strpos($g["j_in_name"]," - ")+3)."|";
				}
			}
			if($i)
				$o.="/~".$gp["publication_year"]." - ".$gp["publication_name"]."|".$i."/|";
		}
	}
	echo "<script>window.parent.J_ajax_R('".$_GET["d"]."',\"".$o."\")</script>";
	die();
}

$id=isset($_REQUEST["id"])?$_REQUEST["id"]:0;
$pub=mysql_fetch_array(mysql_query("SELECT * FROM nse_publications WHERE publication_id=".$_GET["pub"]));
include $SDR."/system/get.php";
include $SDR."/system/dir.php";

if(isset($_GET["delete"]))
{
	mysql_query("DELETE FROM nse_inventory WHERE j_in_id=".$id);
	mysql_query("DELETE FROM nse_inventory_publications WHERE j_inp_inventory=".$id);
	mysql_query("DELETE FROM nse_ads WHERE inventory_id=".$id);
	if(file_exists($SDR."/stuff/ad_templates/".$id.".php"))
	{
		J_SetPerm("/stuff/ad_templates");
		J_SetPerm("/stuff/ad_templates/".$id.".php");
		unlink($SDR."/stuff/ad_templates/".$id.".php");
	}
	if(is_dir($SDR."/stuff/ads/".$id))
	{
		J_SetPerm("/stuff");
		J_SetPerm("/stuff/ads");
		J_DeleteDir("/stuff/ads/".$id,1);
	}
	echo "<script>window.parent.J_frame(".$_GET["delete"].",0,'".$ROOT."/apps/ad_manager/edit_publications.php?refco=".$_GET["co"]."&ref=".$_GET["pub"]."')</script>";
	die();
}

elseif(count($_POST))
{
	include $SDR."/system/parse.php";

	$n=preg_replace("~[^a-zA-Z0-9 ]~","",trim(str_replace("  "," ",$_POST["pn"])))." - ".preg_replace("~[^a-zA-Z0-9 \-]~","",$_POST["sz"]);
	if($id)
	{
		if($n!=J_Value("j_in_name","nse_inventory","j_in_id",$id))
			$re=1;
		mysql_query("UPDATE nse_inventory SET aa_entity='".$_POST["aa"]."',j_in_name='".$n."',j_in_price='".$_POST["pr"]."',j_in_category=".(isset($_POST["mu"])?4:3).",j_in_commission_percent='".(preg_replace("~[^0-9\.]~","",$_POST["j_in_commission_percent"])*1)."' WHERE j_in_id=".$id);
		$q="UPDATE nse_inventory_publications SET j_inp_year=".$pub["publication_year"];
		$v=preg_replace("~[^0-9\.]~","",$_POST["aw"])*1;
		$q.=",j_inp_width=".($v>0?$v:0);
		$v=preg_replace("~[^0-9\.]~","",$_POST["ah"])*1;
		$q.=",j_inp_height=".($v>0?$v:0);
		$q.=" WHERE j_inp_inventory=".$id;
		mysql_query($q);
		if(mysql_error())echo mysql_error()."<hr>";

		$q="UPDATE nse_inventory_publications SET
		j_inp_year=".$pub["publication_year"];
		$v=preg_replace("~[^0-9\.]~","",$_POST["aw"])*1;
		$q.=",j_inp_width=".($v>0?$v:0);
		$v=preg_replace("~[^0-9\.]~","",$_POST["ah"])*1;
		$q.=",j_inp_height=".($v>0?$v:0);
		$q.=",j_inp_images='";
		if(isset($_POST["tmp"]))
		{
			foreach($_POST as $k => $v)
			{
				if(strpos($k,"iw_")!==false || strpos($k,"ih_")!==false)
				{
					$v=preg_replace("~[^0-9\.]~","",$v)*1;
					$q.=$v.(strpos($k,"iw_")!==false?"~":"|");
				}
			}
		}
		$q.="'";
		$q.=",j_inp_copy=".(isset($_POST["cpy"])?$_POST["cpy"]:0)."
		WHERE j_inp_inventory=".$id;
		mysql_query($q);
	}
	else
	{
		$q="INSERT INTO nse_inventory (
		j_in_name
		,j_in_created
		,j_in_price
		,j_in_category
		,j_in_year
		,aa_entity
		,j_in_commission_percent
		) VALUES (
		'".$n."'
		,".$EPOCH;
		$q.=",".$_POST["pr"];
		$q.=",4";
		$q.=",".$pub["publication_year"];
		$q.=",'".$_POST["aa"]."'";
		$q.=",'".(preg_replace("~[^0-9\.]~","",$_POST["j_in_commission_percent"])*1)."'";
		$q.=")";
		mysql_query($q);
		if(mysql_error())echo mysql_error()."<hr>";
		$id=J_ID("nse_inventory","j_in_id");

		$q="INSERT INTO nse_inventory_publications (
		j_inp_publication
		,j_inp_inventory
		,j_inp_year
		,j_inp_width
		,j_inp_height
		,j_inp_images
		,j_inp_copy
		) VALUES (
		".$_GET["pub"]."
		,'".$id."'
		,'".$pub["publication_year"]."'";
		$v=preg_replace("~[^0-9\.]~","",$_POST["aw"])*1;
		$q.=",".($v>0?$v:0);
		$v=preg_replace("~[^0-9\.]~","",$_POST["ah"])*1;
		$q.=",".($v>0?$v:0);
		$q.=",'";
		if(isset($_POST["tmp"]))
		{
			foreach($_POST as $k => $v)
			{
				if(strpos($k,"iw_")!==false || strpos($k,"ih_")!==false)
				{
					$v=preg_replace("~[^0-9\.]~","",$v)*1;
					$q.=$v.(strpos($k,"iw_")!==false?"~":"|");
				}
			}
		}
		$q.="'";
		$q.=",".(isset($_POST["cpy"])&&$_POST["cpy"]>0?$_POST["cpy"]:0);
		$q.=")";
		mysql_query($q);
		if(mysql_error())echo mysql_error()."<tt>".$q."</tt><hr>";
		$re=1;
	}

	J_SetPerm("/stuff");
	J_SetPerm("/stuff/ad_templates");
	if(isset($_POST["tmp"]) && $_POST["tmp"] && isset($_POST["cpy"]) && $_POST["cpy"] && file_exists($SDR."/stuff/ad_templates/".$_POST["cpy"].".php"))
		copy($SDR."/stuff/ad_templates/".$_POST["cpy"].".php",$SDR."/stuff/ad_templates/".$id.".php");
	elseif(!isset($_POST["tmp"]) && file_exists($SDR."/stuff/ad_templates/".$id.".php"))
	{
		J_SetPerm("/stuff/ad_templates/".$id.".php");
		@unlink($SDR."/stuff/ad_templates/".$id.".php");
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

$s=array();
$r=mysql_query("SELECT * FROM nse_inventory LEFT JOIN nse_inventory_publications ON j_in_id=j_inp_inventory WHERE (j_inp_publish=0 OR j_inp_publish>".$EPOCH.") AND (j_in_category=3 OR j_in_category=4) ORDER BY j_inp_year DESC,j_in_name");
if(mysql_num_rows($r))
{
	while($g=mysql_fetch_array($r))
		$s[substr($g["j_in_name"],strpos($g["j_in_name"]," - ")+3)]=1;
}

$ss="";
foreach($s as $k => $v)
	$ss.="<a href=go: onclick=\\\"j_G['self'].P_v(this);return false\\\" onfocus=blur()>".$k."</a>";
echo "<script>ss=\"<b class=jA>".$ss."</b>\";function P_v(t){j_E.value=t.innerHTML;J_validate()}</script>";

if($id)
	$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_inventory LEFT JOIN nse_inventory_publications ON j_in_id=j_inp_inventory WHERE j_in_id=".$id." LIMIT 1"));

echo "<body onload=J_validate()>";
echo "<h4>Publication Size: ".$pub["publication_name"]."</h4><hr>";
echo "<tt><b>NOTE!</b> Do not use special keyboard characters. When adding sizes, try to select from the list instead of typing in new ones. Item: ".$id;
$tmp=0;
if($id && file_exists($SDR."/stuff/ad_templates/".$id.".php"))
{
	$tmp=1;
	echo " <a href=go: onclick=\"J_W(ROOT+'/utility/media_viewer/f/basic.php?file=/stuff/ad_templates/".$id.".php');return false\" onfocus=blur()><b>This item uses /templates/".$id.".php</b></a>";
}
echo "</tt><br>";

echo "<form method=post>";
if($id)
	echo "<input type=hidden name=id value=".$id.">";
else // get last ads commission
{
	$q = "SELECT j_in_commission_percent,aa_entity FROM nse_inventory WHERE j_in_commission_percent!=0.00 AND j_in_commission_percent!='' ORDER BY j_in_id DESC LIMIT 1";
	$q = mysql_query($q);
	if (mysql_num_rows($q)) {
		$g = mysql_fetch_array($q);
			$invoice_default_commission=$g["j_in_commission_percent"];
	}
}
echo "<table id=jTR class=jW100>";
echo "<tr><th>Company</th><td width=99%><select name=aa class=jW100>";
$q="SELECT * FROM nse_establishment WHERE aa_entity='Y' ORDER BY establishment_name";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($ge=mysql_fetch_array($q))
		echo "<option value=".$ge["establishment_code"].($ge["establishment_code"]==$g["aa_entity"]?" selected":"").">".$ge["establishment_name"];
}
echo "</select></td></tr>";
echo "<tr><th>Name</th><td><input type=text name=pn value=\"".($id?substr($g["j_in_name"],0,strpos($g["j_in_name"]," - ")):$pub["publication_year"]." ".$pub["publication_name"])."\" class=jW100 onmouseover=\"J_TT(this,'Select from the list or add a new publication name')\" title='Requires value' style=font-weight:bold></td></tr>";
echo "<tr><th>Size</th><td><input type=text name=sz".($id?" value=\"".substr($g["j_in_name"],strpos($g["j_in_name"]," - ")+3)."\"":"")." class=jW100 onmouseover=\"J_TT(this,'Select from the list or add a new publication size')\" onclick=J_note(this,ss,240,3,0,-1,0,0,0,0,300) title='Requires value'></td></tr>";
echo "<tr><th>Price</th><td><input type=text name=pr value='".($id&&$g["j_in_price"]?$g["j_in_price"]:"0.00")."' title='Requires value'> <span class=jO50>Excl VAT</tt></td></tr>";
echo "<tr><th>Dimensions</th><td><input type=text name=aw value='".($id?$g["j_inp_width"]:0)."' title='Requires value' size=4 onmouseover=\"J_TT(this,'Width MM')\"> &times; <input type=text name=ah value='".($id?$g["j_inp_height"]:0)."' title='Requires value' size=4 onmouseover=\"J_TT(this,'Height MM')\"></td></tr>";
echo "<tbody><tr><th>Copy Ad</th><td><input type=text onclick=\"pmode='cpy';J_ajax_K(this,'/apps/ad_manager/publications/edit_size.php?cpy=1&w='+j_W+'&f='+j_Wn,300,3)\" value=\"".($id?J_Value("j_in_name","nse_inventory","j_in_id",$g["j_inp_copy"]):"")."\" onmouseover=\"J_TT(this,'Copy ad content from previous ads')\" class=jW100><input type=hidden name=cpy value=".($id?$g["j_inp_copy"]:0)."></td></tr></tbody>";
echo "<tbody id=mu".($id&&$tmp&&$g["j_in_category"]==3?" class=ji":"")."><tr><th>Make-Up</th><td><input type=checkbox name=mu value=1".($id&&$g["j_in_category"]==4?" checked":"")." onmouseover=\"J_TT(this,'These are ads that are created by AA Travel Guides')\"></td></tr></tbody>";
echo "<tr><th>Template</th><td><input type=checkbox name=tmp value=1".($tmp?" checked":"")." onclick=tc_(this)></td></tr>";
echo "<tr id=imt".($tmp?"":" class=ji")."><th colspan=9 onmouseover=\"J_TT(this,'Please define the dimensions of each image within the ad.')\"><input type=button value='Add image' onclick=img_('~|',1) style=float:right>Images</th></tr>";
echo "<tbody id=img".($tmp?"":" class=ji").">";
echo "</tbody>";
echo "<tr><th>Commission</th><td><input type=text name=j_in_commission_percent value='".($id&&$g["j_in_commission_percent"]?$g["j_in_commission_percent"]:(isset($invoice_default_commission)?$invoice_default_commission:""))."'> %</td></tr>";
echo "</table>";
echo "<br><input type=submit value=OK>";
if($id)
	echo " <input type=button value=Delete onclick=\"if(confirm('WARNING!\\nAny ads created with this size will be lost.\\nProceed?'))location=location.href+'&delete='+j_W\">";
echo "</form>";
echo "<iframe id=j_IF></iframe>";

echo "<script>	J_tr()";
echo ";pmode=0";
echo ";function img_(v)
{
	var i,tr,td
	tr=\$('img').childNodes
	\$('img').innerHTML=''
	ic=1
	if(v)
	{
		v=v.split('|'),i=0
		while(v[i])
		{
			u=v[i].split('~')
			tr=document.createElement('tr')
			td=document.createElement('td')
			td.colSpan=9
			td.innerHTML=\"<input type=button value=&#10006; onclick=\\\"var t=this.parentNode.parentNode;t.parentNode.removeChild(t)\\\" style=float:right>\"+ic+\". <input type=text value='\"+u[0]+\"' name=iw_\"+ic+\" title='Requires value' size=4 onmouseover=\\\"J_TT(this,'Width MM')\\\"> &times; <input type=text name=ih_\"+ic+\" value='\"+u[1]+\"' title='Requires value' size=4 onmouseover=\\\"J_TT(this,'Height MM')\\\"> mm\"
			tr.appendChild(td)
			\$('img').appendChild(tr)
			ic++
			i++
		}
	}
	J_tr()
	J_validate()
	j_P.J_note0(2)
}";
echo ";function tc_(t)
{
	pmode='tmp'
	var c=t.checked==true?1:0
	\$('mu').className=c?'ji':''
	\$('imt').className=c?'':'ji'
	\$('img').className=c?'':'ji'
	if(c)
		J_ajax_K(t,'/apps/ad_manager/publications/edit_size.php?w='+j_W+'&f='+j_Wn,300,3)
}";
if($id)
	echo ";img_(\"".$g["j_inp_images"]."\")";
echo ";function J_ajax_Z(o,v,t){J_opaq();if(pmode=='tmp'){o.value=t.rel;J_note(o,'<b>Retrieving template</b>');\$('j_IF').src=ROOT+'/apps/ad_manager/publications/edit_size.php?ad='+\$N('tmp')[0].value}else{J_note(o,'<b>Retrieving Ad details</b>');\$('j_IF').src=ROOT+'/apps/ad_manager/publications/edit_size.php?ad='+\$N('cpy')[0].value}}";
echo ";function T_ad(w,h,i){\$N('aw')[0].value=w;\$N('ah')[0].value=h;if(pmode=='tmp')img_(i);J_opaq(1)}";
if(isset($re))
	echo ";J_frame(0,ROOT+'/apps/ad_manager/edit_publications.php?refco=".$_GET["co"]."&ref=".$_GET["pub"]."')";
echo "</script>";
echo "</body></html>";
?>