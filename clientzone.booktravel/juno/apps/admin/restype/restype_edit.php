<?php

//Includes
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$template = file_get_contents('restype_edit.html');
$restype_name = '';
$restype_description = '';
$not_awards = '';

//Save restype
if (count($_POST) > 0) {
	$not_awards = isset($_POST['not_awards'])?'1':'';
	
	if (isset($_GET['restype_id'])) {
		$statement = "UPDATE nse_restype
						SET restype_name=?, restype_description=?, not_awards=?
						WHERE restype_id=?
						LIMIT 1";
		$sql_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_update->bind_param('sssi', $_POST['restype_name'], $_POST['restype_description'], $not_awards, $_GET['restype_id']);
		$sql_update->execute();
		$sql_update->close();
		
		$message = 'Restype Updated Successfully';
	} else {
		$statement = "INSERT INTO nse_restype
							(restype_name, restype_description, not_awards)
						VALUES
							(?,?,?)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_insert->bind_param('sss', $_POST['restype_name'], $_POST['restype_description'], $not_awards);
		$sql_insert->execute();
		$sql_insert->close();
		
		$message = "Restype Created Successfully";
	}
	
	echo "<div style='text-align: center; margin: auto auto; width: 500; padding: 10px; border: 1px dotted black; background-color: gray; color: black'>$message</div>";
	
	die();
}

//Get restype data
if (isset($_GET['restype_id'])) {
	$statement = "SELECT restype_name, restype_description, not_awards 
					FROM nse_restype
					WHERE restype_id=?
					LIMIT 1";
	$sql_restype = $GLOBALS['dbCon']->prepare($statement);
	$sql_restype->bind_param('i', $_GET['restype_id']);
	$sql_restype->execute();
	$sql_restype->bind_result($restype_name, $restype_description, $not_awards);
	$sql_restype->fetch();
	$sql_restype->close();
}

//Not awards
$not_awards = $not_awards=='1'?'checked':'';

//Replace Tags
$template = str_replace('<!-- restype_name -->', $restype_name, $template);
$template = str_replace('<!-- restype_description -->', $restype_description, $template);
$template = str_replace('<!-- not_awards -->', $not_awards, $template);

echo $template;

?>
