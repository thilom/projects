<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

if(isset($_GET["del"]))
{
	mysql_query("DELETE FROM nse_establishment_group WHERE group_name='".$_GET["del"]."'");
	mysql_query("UPDATE nse_user SET establishment_group='' WHERE establishment_group='".$_GET["del"]."'");
	include_once $SDR."/system/activity.php";
	J_act("EST GROUP",5,$_GET["del"]);
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script>w=j_P.j_Wi[j_W];w['h1'].innerHTML='Establishment Groups';w['h2'].style.display='';j_P.J_reload(self,0,'2')
</script>";
}

elseif(count($_POST))
{
	include_once $SDR."/system/activity.php";

	if(isset($_GET["users"]))
	{
		$name=$_GET["users"];
		foreach($_POST as $k => $v)
		{
			if(substr($k,0,2)=="u_" && !isset($_POST["g_".$v]))
			{
				mysql_query("UPDATE nse_user SET establishment_group='' WHERE user_id=".$v);
				J_act("EST GROUP",5,"User: ".$name." (".J_Value("","people","",$v).")",$v);
			}
			elseif(substr($k,0,3)=="nu_" && $v>0)
			{
				mysql_query("UPDATE nse_user SET establishment_group='".$name."' WHERE user_id=".$v);
				J_act("EST GROUP",3,"User: ".$name." (".J_Value("","people","",$v).")",$v);
			}
		}
	}
	else
	{
		$name="";
		$new_name=0;
		$parent=(isset($_POST["p_"])?$_POST["p_"]:0);
		foreach($_POST as $k => $v)
		{
			if($k=="name")
			{
				$name=strtoupper(trim(substr(preg_replace("[^a-zA-Z0-9 \-]","",$v),0,32)));
				if($_POST["old"] && $_POST["old"]!=$name)
				{
					mysql_query("UPDATE nse_establishment_group SET group_name='".$name."',group_parent=".($parent==substr($k,2)?1:0)." WHERE group_name='".$_POST["old"]."'");
					mysql_query("UPDATE nse_user SET establishment_group='".$name."' WHERE establishment_group='".$_POST["old"]."'");
					if($_POST["old"] && $_POST["old"]!=$name)
						$new_name=1;
				}
			}
			elseif(substr($k,0,2)=="e_" && !isset($_POST["g_".$v]))
			{
				mysql_query("DELETE FROM nse_establishment_group WHERE group_id=".$v);
				J_act("EST GROUP",5,$name." (".J_Value("","establishment","",$_POST["c_".$v]).")",$_POST["c_".$v]);
			}
			elseif(substr($k,0,2)=="e_")
				mysql_query("UPDATE nse_establishment_group SET group_parent=".($parent==substr($k,2)?1:0)." WHERE group_id=".substr($k,2));
			elseif(substr($k,0,3)=="ne_" && $v)
			{
				mysql_query("INSERT INTO nse_establishment_group (group_name,establishment_code,group_parent) VALUES ('".$name."','".$v."',".($parent==substr($k,2)?1:0).")");
				J_act("EST GROUP",3,$name." (".J_Value("","establishment","",$v).")",$v);
			}
		}
		if($new_name)
			J_act("EST GROUP",4,$_POST["old"]." to ".$name);
		// check if parent
		if(!mysql_num_rows(mysql_query("SELECT group_id FROM nse_establishment_group WHERE group_name='".$name."' AND group_parent=1 LIMIT 1")))
		{
			$q=mysql_query("SELECT group_id FROM nse_establishment_group ORDER BY group_id DESC LIMIT 1");
			if(mysql_num_rows($q))
			{
				$g=mysql_fetch_array($q);
				mysql_query("UPDATE nse_establishment_group SET group_parent=1 WHERE group_id=".$g["group_id"]);
			}
		}
		echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<script src=".$ROOT."/system/P.js></script>";
		echo "<script>w=j_P.j_Wi[j_W]['F'][0]['W'];w.location=w.location.href</script>";
	}
	unset($_GET["name"]);
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/admin/f/group.js></script>";

// group users

if(isset($_GET["users"]))
{
	$u="";
	$q="SELECT user_id,firstname,lastname FROM nse_user WHERE establishment_group='".$_GET["users"]."' ORDER BY firstname";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
			$u.=$g["user_id"]."~".ucwords(strtolower(trim(preg_replace("[^a-zA-Z0-9 \-]","",$g["firstname"]))))." ".ucwords(strtolower(trim(preg_replace("[^a-zA-Z0-9 \-]","",$g["lastname"]))))."|";
	}
	echo "<script>G_u('".$_GET["users"]."',\"".$u."\")</script>";
	die();
}

if(isset($_GET["user"]))
{
	echo "<script>J_start('".$_GET["user"]."')</script>";
	die();
}

// group establishments

elseif(isset($_GET["name"]) || isset($name))
{
	$name=isset($_GET["name"])?$_GET["name"]:$name;
	$j="";
	$q="SELECT * FROM nse_establishment_group AS g JOIN nse_establishment AS e ON g.establishment_code=e.establishment_code WHERE g.group_name='".$name."' ORDER BY g.group_parent DESC,e.establishment_name";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
			$j.=$g["group_id"]."~".$g["establishment_code"]."~".ucwords(strtolower(preg_replace("[^a-zA-Z0-9 \-]","",$g["establishment_name"])))."~".($g["group_parent"]?1:"")."|";
	}
	echo "<script>G_e(\"".$name."\",\"".$j."\")</script>";
	die();
}

// start

elseif((isset($_GET["j_IF"])&&$_GET["j_IF"]==1) || count($_POST) || isset($_GET["del"]))
{
	echo "<body><script>G_s()</script></body></html>";
	die();
}

// start

$j="";
$c=array();

$q="SELECT group_name,establishment_code FROM nse_establishment_group ORDER BY group_parent DESC,group_name";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if(!isset($c[$g["group_name"]]))
		{
			$c[$g["group_name"]]=1;
			$j.=$g["group_name"]."~";
			$j.=J_Value("","establishment","",$g["establishment_code"]);
			$j.="|";
		}
	}
}
echo "<script>G_m(\"".$j."\")</script>";

$J_home=0;
$J_title1="Establishment Groups";
$J_icon="<img src=".$ROOT."/ico/set/gohome_group.png>";
$J_label=21;
$J_width=800;
$J_framesize=260;
$J_frame1=$ROOT."/apps/admin/establishment_groups.php";
$J_tooltip="Set up groups for establishments that fall under a holding company";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>