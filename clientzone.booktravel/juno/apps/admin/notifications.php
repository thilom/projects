<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

if(count($_POST))
{
	$del=array();
	foreach($_POST as $k => $v)
	{
		if(strpos($k,"notify_")!==false)
		{
			$k=substr($k,0,strrpos($k,"_"));
			if(!isset($del[$k]))
			{
				$del[$k]=1;
				mysql_query("DELETE FROM nse_variables WHERE variable_name='".$k."'");
			}
			if($v)
				mysql_query("INSERT INTO nse_variables (variable_name,variable_value) VALUES ('".$k."','".$v."')");
		}
	}
}


echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/admin/f/notifications.js></script>";
echo "<body>";

echo "<form method=post>";
echo "<table id=jTR class=jW100>";

$i=0;

echo "<tr><th><h4>Invoices</h4></th></tr>";
echo "<tr><th>New</th><td>";
$q="SELECT * FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='notify_invoice_new' ORDER BY firstname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		echo "<tt>";
		echo "<input type=text value=\"".$g["firstname"]." ".$g["lastname"]."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)>";
		echo "<input type=hidden name=notify_invoice_new_".$i." value=".$g["user_id"].">";
		echo "</tt>";
		$i++;
	}
}
echo "<script>N_but('notify_invoice_new')</script>";
echo "</td></tr>";

echo "<tr><th>Cancelled</th><td>";
$q="SELECT * FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='notify_invoice_cancel' ORDER BY firstname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		echo "<tt>";
		echo "<input type=text value=\"".$g["firstname"]." ".$g["lastname"]."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)>";
		echo "<input type=hidden name=notify_invoice_cancel_".$i." value=".$g["user_id"].">";
		echo "</tt>";
		$i++;
	}
}
echo "<script>N_but('notify_invoice_cancel')</script>";
echo "</td></tr>";

echo "<tr><th>Deleted</th><td>";
$q="SELECT * FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='notify_invoice_delete' ORDER BY firstname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		echo "<tt>";
		echo "<input type=text value=\"".$g["firstname"]." ".$g["lastname"]."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)>";
		echo "<input type=hidden name=notify_invoice_delete_".$i." value=".$g["user_id"].">";
		echo "</tt>";
		$i++;
	}
}
echo "<script>N_but('notify_invoice_delete')</script>";
echo "</td></tr>";

echo "<tr><th><br><h4>Assessments</h4></th></tr>";
echo "<tr><th>New</th><td>";
$q="SELECT * FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='notify_assessment_new' ORDER BY firstname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		echo "<tt>";
		echo "<input type=text value=\"".$g["firstname"]." ".$g["lastname"]."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)>";
		echo "<input type=hidden name=notify_assessment_new_".$i." value=".$g["user_id"].">";
		echo "</tt>";
		$i++;
	}
}
echo "<script>N_but('notify_assessment_new')</script>";
echo "</td></tr>";
echo "<tr><th>Cancelled</th><td>";
$q="SELECT * FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='notify_assessment_cancel' ORDER BY firstname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		echo "<tt>";
		echo "<input type=text value=\"".$g["firstname"]." ".$g["lastname"]."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)>";
		echo "<input type=hidden name=notify_assessment_cancel_".$i." value=".$g["user_id"].">";
		echo "</tt>";
		$i++;
	}
}
echo "<script>N_but('notify_assessment_cancel')</script>";
echo "</td></tr>";

echo "<tr><th><br><h4>Establishments</h4></th></tr>";
echo "<tr><th>Cancelled</th><td>";
$q="SELECT * FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='notify_establishment_cancel' ORDER BY firstname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		echo "<tt>";
		echo "<input type=text value=\"".$g["firstname"]." ".$g["lastname"]."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)>";
		echo "<input type=hidden name=notify_establishment_cancel_".$i." value=".$g["user_id"].">";
		echo "</tt>";
		$i++;
	}
}
echo "<script>N_but('notify_establishment_cancel')</script>";
echo "</td></tr>";

echo "<tr><th><br><h4>Ads</h4></th></tr>";
echo "<tr><th>Cancelled</th><td>";
$q="SELECT * FROM nse_variables JOIN nse_user ON variable_value=user_id WHERE variable_name='notify_ad_cancel' ORDER BY firstname";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		echo "<tt>";
		echo "<input type=text value=\"".$g["firstname"]." ".$g["lastname"]."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=user&email&role=1',320,4) onclick=J_ajax_C(this);N_del(this)>";
		echo "<input type=hidden name=notify_ad_cancel_".$i." value=".$g["user_id"].">";
		echo "</tt>";
		$i++;
	}
}
echo "<script>N_but('notify_ad_cancel')</script>";
echo "</td></tr>";


echo "</table><br>";
echo "<input type=submit value=OK>";
echo "</form>";

$J_home=0;
$J_title1="Notifications";
$J_icon="<img src=".$ROOT."/ico/set/email.png>";
$J_label=21;
$J_width=560;
$J_nostart=1;
$J_framesize=480;
include $SDR."/system/deploy.php";
echo "<script>J_tr();id=".$i."</script>";
echo "</body></html>";
?>