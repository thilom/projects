<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";



echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/admin/restype/restype.js></script>";

// Vars
$restype_list = '';


//Get restype list
$statement = "SELECT restype_id, restype_name 
				FROM nse_restype
				ORDER BY restype_name";
$sql_restype = $GLOBALS['dbCon']->prepare($statement);
$sql_restype->execute();
$sql_restype->bind_result($restype_id, $restype_name);
while ($sql_restype->fetch()) {
	$restype_list .= "$restype_id~$restype_name|";
}
$sql_restype->close();


echo "<script>G_m(\"".$restype_list."\")</script>";

$J_home=0;
$J_title1="Restype Manager";
$J_icon="<img src=".$ROOT."/ico/set/folder_editor.png>";
$J_label=21;
$J_width=800;
$J_framesize=260;
$J_frame1=$ROOT."/apps/admin/restype/restype.php";
$J_tooltip="Manage establishment restypes";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>