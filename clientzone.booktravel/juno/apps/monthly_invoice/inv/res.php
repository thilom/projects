<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";
$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 20);
$test = isset($_GET['test']) ? TRUE : FALSE;
$test_status = '';
$formula = $_GET['formula'];

$age_limit = strtotime("now - 6 months");

if (isset($_POST['mails'])) {
	include 'create_invoice.php';
}

if (isset($_GET['month'])) {
	$month = $_GET['month'];
} else {
	echo "No Month Selected";
}

if (isset($_GET['year'])) {
	$year = $_GET['year'];
} else {
	echo "No Year Selected";
}

$r = "";
$counter = 0;
$code_list = array();
$review_dates = array();
$line_counter = 0;
$establishment_count = 0;
$current_month = date('n');

//$list = explode("\n", file_get_contents('qaaug.csv'));
//foreach ($list as $key => $value) {
//    $list[$key] = str_replace('"', '', $value);
//}

$renewal_date_start = mktime(0, 0, 0, $month, 1, $year);
$renewal_date_end = mktime(0, 0, 0, $month, date('t', $renewal_date_start), $year);

/* NEW METHOD - START */
$check_date = "{$_GET['month']}-{$_GET['year']}";
$statement = "SELECT a.establishment_code, b.cancelled_date, a.assessment_date
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_establishment_qa_cancelled AS b ON a.establishment_code=b.establishment_code
				WHERE DATE_FORMAT(renewal_date,'%c-%Y')=?";
$sql_renewal = $GLOBALS['dbCon']->prepare($statement);
$sql_renewal->bind_param('s', $check_date);
$sql_renewal->execute();
$sql_renewal->store_result();
$sql_renewal->bind_result($eCode, $cancelled_date, $assessment_date);
while ($sql_renewal->fetch()) {
	if (!empty($cancelled_date)) {
		if ($assessment_date < $cancelled_date) {
			continue;
		}
	}
//	if ($cancelled_date!='0000-00-00') continue;
	$eCode_list[] = $eCode;
}
$sql_renewal->close();
//print_r($eCode_list);
//echo count($eCode_list);
/* NEW METHOD - END */

//Get products
$statement = "SELECT j_in_id, j_in_name, j_in_description, j_in_price, j_in_max_room FROM nse_inventory
				WHERE j_in_max_room > 0 AND j_in_category=5
				ORDER BY j_in_max_room DESC";
$sql_products = $GLOBALS['dbCon']->prepare($statement);
$sql_products->execute();
$sql_products->store_result();
$sql_products->bind_result($product_id, $product_name, $product_description, $product_price, $max_room);
while ($sql_products->fetch()) {
	$products[$product_id]['product_name'] = $product_name;
	$products[$product_id]['product_description'] = $product_description;
	$products[$product_id]['product_price'] = $product_price;
	$products[$product_id]['max_rooms'] = $max_room;
}
$sql_products->free_result();
$sql_products->close();

//Prepare statement - Next review date from assessments
$statement = "SELECT renewal_date, DATE_FORMAT(assessment_date, '%Y-%m-%d') FROM nse_establishment_assessment WHERE establishment_code=? ORDER BY assessment_date DESC LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - update reveiw date
$statement = "UPDATE nse_establishment SET review_date_depricated=? WHERE establishment_code=?";
$sql_review_update = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - QA Out
$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=?";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);

//Get list of establishments
$codes = isset($_GET['code']) ? explode(',', $_GET['code']) : array();
$statement = "SELECT establishment_code, establishment_name,review_date_depricated, enter_date
				FROM nse_establishment
				WHERE aa_estab='Y' ";
foreach ($codes as $k => $eCode) {
	$statement .= $k == 0 ? " AND (" : " OR ";
	$statement .= "establishment_code='$eCode'";
}
$statement .= isset($_GET['code']) ? ")" : '';
$statement .= "ORDER BY enter_date";
$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
$sql_estabs->execute();
$sql_estabs->store_result();
$sql_estabs->bind_result($establishment_code, $establishment_name, $review_date, $entered_date);
while ($sql_estabs->fetch()) {
	$assessment_review = '';
	$qa_out = '';
	$test_status = '';
	$sql_data->bind_param('s', $establishment_code);
	$sql_data->execute();
	$sql_data->bind_result($assessment_review, $assessment_date);
	$sql_data->fetch();
	$sql_data->free_result();

//	echo "--<br>$establishment_code :: $assessment_review<br>";

	if (empty($assessment_review))
		$assessment_review = $review_date;


	if (empty($assessment_review)) {
		if (!empty($entered_date) && $entered_date != '0000-00-00') {

			list($y, $m, $d) = explode('-', $entered_date);
			$this_year = date('y');
			$renew_year = (int) $m < $current_month ? $this_year + 1 : $this_year;
			$renewal_date = date('Y-m-d', strtotime("20$renew_year-$m-1"));

			$sql_review_update->bind_param('ss', $renewal_date, $establishment_code);
			$sql_review_update->execute();

			$assessment_review = $renewal_date;
		}
	}


	if (empty($assessment_review)) {
		if ($test === TRUE) {
			$test_status = "No Review Date";
		} else {
			continue;
		}
	}


	//Check QA Out
	$qa_out = '';
	$sql_qa_out->bind_param('s', $establishment_code);
	$sql_qa_out->execute();
	$sql_qa_out->bind_result($qa_out);
	$sql_qa_out->fetch();
	$sql_qa_out->free_result();


	//Calculate date diff
	$enter_timestamp = 0;
	$qa_out_timestamp = 0;
	$diff = 0;
	if (!empty($entered_date) && !empty($qa_out)) {
		list($y1, $m1, $d1) = explode('-', $entered_date);
		$enter_timestamp = mktime(0, 0, 0, $m1, $d1, $y1);
		list($y2, $m2, $d2) = explode('-', $qa_out);
		$qa_out_timestamp = mktime(0, 0, 0, $m2, $d2, $y2);
		$diff = $enter_timestamp - $qa_out_timestamp;
	} else {
		$diff = 0;
	}
	if (!empty($qa_out) && $qa_out != '0000-00-00' && $diff < 0) {
		if ($test === TRUE) {
			$test_status = "QA Out: $qa_out";
		} else {
			continue;
		}
	}

	if (!empty($assessment_review)) {
		list($y, $m, $d) = explode('-', $assessment_review);
		$review_timestamp = mktime(0, 0, 0, $m, 1, $y);

		if ($review_timestamp != $renewal_date_start) {
			if ($test === TRUE) {
				$test_status = 'Out of range';
			} else {
				continue;
			}
		}
	}
//echo "$establishment_code :: $assessment_review<br>";

		$code_list[] = $establishment_code;

		$review_dates[$establishment_code] = !empty($assessment_review) ? date('Y-m-d', $review_timestamp) : '-';
		$test_statuses[$establishment_code] = $test_status;
		$assessments[$establishment_code] = $assessment_date;
		$outs[$establishment_code] = $qa_out;


//	$line_counter++;
//	$establishment_count++;
}
$sql_estabs->free_result();
$sql_estabs->close();

//Prepare statement - Establishment Data
$statement = "SELECT establishment_name, enter_date FROM nse_establishment WHERE establishment_code=?";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - location
$statement = "SELECT b.town_name, c.province_name
                FROM nse_establishment_location AS a
                LEFT JOIN nse_nlocations_towns AS b ON a.town_id=b.town_id
                LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
                WHERE a.establishment_code=?";
$sql_location = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Find invoice
$statement = "SELECT j_inv_id, j_inv_date
                FROM nse_invoice AS a
                LEFT JOIN nse_invoice_item AS b ON a.j_inv_id=b.j_invit_invoice
                WHERE a.j_inv_to_company=? && b.j_invit_inventory=? && $age_limit < a.j_inv_date";
$sql_inv_check = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - Email list
$statement = "SELECT u.firstname, u.lastname, u.email, e.designation
		FROM nse_user_establishments AS e
               LEFT JOIN nse_user AS u  ON u.user_id=e.user_id
                WHERE e.establishment_code=?
		ORDER BY qa_contact DESC";
$sql_email = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - get room count
$statement = "SELECT room_count FROM nse_establishment_data WHERE establishment_code=?";
$sql_room = $GLOBALS['dbCon']->prepare($statement);


//Prepare statement - assessments
$statement = "SELECT DATE_FORMAT(assessment_date, '%y-%m-%d') FROM nse_establishment_assessment WHERE establishment_code=? ORDER BY assessment_date LIMIT 1";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);

$line_counter = 0;
foreach ($code_list as $establishment_code) {
	$invoice_id = 0;
	$status = '';
	$assessment_date = '';
	$room_count = 0;
	$town_name = '';
	$province_name = '';
	$contact = '';
	$sql_data->bind_param('s', $establishment_code);
	$sql_data->execute();
	$sql_data->bind_result($establishment_name, $enter_date);
	$sql_data->fetch();
	$sql_data->free_result();
	$test_status = $test_statuses[$establishment_code];

	$sql_email->bind_param('s', $establishment_code);
	$sql_email->execute();
	$sql_email->store_result();
	$sql_email->bind_result($firstname, $lastname, $email, $designation);
	while ($sql_email->fetch()) {
		if (!empty($email)) {
			$contact .= preg_replace('~[^a-zA-Z \-\(\)]~', "", $firstname . " " . $lastname) . "=" . preg_replace('~[^a-zA-Z \-\(\)]~', "", $designation) . "=" . preg_replace('~[^a-zA-Z0-9@_#+&\.\-]~', "", strtolower($email)) . "^";
		} else {
			$contact .= '';
		}
		//$contact .= !empty($email)?str_replace("'",' ', $firstname." ".$lastname) . "=$designation=$email^":'';
	}
	$sql_email->free_result();
//	echo "$establishment_code: $contact<br>";
	if (empty($contact)) $status = 'No Email';

	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->bind_result($town_name, $province_name);
	$sql_location->fetch();
	$sql_location->free_result();

	$establishment_name .= " ($town_name, $province_name)";
	$establishment_name = str_replace(array("'", '"'), '', $establishment_name);

	$sql_assessments->bind_param('s', $establishment_code);
	$sql_assessments->execute();
	$sql_assessments->bind_result($assessment_date);
	$sql_assessments->fetch();
	$sql_assessments->free_result();

	//Get room count
	$sql_room->bind_param('s', $establishment_code);
	$sql_room->execute();
	$sql_room->bind_result($room_count);
	$sql_room->fetch();
	$sql_room->free_result();

	//Get product
	foreach ($products as $product_id => $prod) {
		if ($room_count <= $prod['max_rooms'])
			$this_product = $product_id;
	}

	//Check for invoice
	$sql_inv_check->bind_param('ss', $establishment_code, $this_product);
	$sql_inv_check->execute();
	$sql_inv_check->bind_result($invoice_id, $invoice_date);
	$sql_inv_check->fetch();
	$sql_inv_check->free_result();
	if ($invoice_id != 0) {
		$invoice_date = date('d-m-Y', $invoice_date);
		$status = 'Invoiced' . " ($invoice_date)";
	}

	if (empty($room_count) || $room_count == 0)
		$status = "No Rooms";

	if (!empty($assessment_date)) {
		list($y, $m, $d) = explode('-', $assessment_date);
		$assessment_date = mktime(0, 0, 0, $m, $d, $y);
		$now_date = strtotime('today - 3 months');
		if ($assessment_date > $now_date)
			$status = 'Recently Assessed';
	}

	//Get DBase data
	$QAOUT = '';
	$QVISITDATE = '';
	$statement = "SELECT QAOUT, QVISITDATE FROM nse_establishment_dbase WHERE establishment_code=? LIMIT 1";
	$sql_dbase = $GLOBALS['dbCon']->prepare($statement);
	$sql_dbase->bind_param('s', $establishment_code);
	$sql_dbase->execute();
	$sql_dbase->bind_result($QAOUT, $QVISITDATE);
	$sql_dbase->fetch();
	$sql_dbase->close();

	$highlight = 0;
	if (!empty($status) && $status != 'Invoiced')
//		$contact = '';
		$highlight = 1;
	if (!empty($status) && $status != 'Recently Assessed') {
//		$contact = '';
		$highlight = 1;
	}


	$line_max = $jL1 + $jL2;
	if ($formula == 0) {
		if (in_array($establishment_code, $eCode_list)) {
//			echo "MAX: $line_max; COUNT: $line_counter<br>";
			if ($line_counter >= $jL1 && $line_counter < $line_max) {
				$r .= "$counter~$establishment_code~$establishment_name~$enter_date~{$review_dates[$establishment_code]}~{$outs[$establishment_code]}~{$assessments[$establishment_code]}~$contact~$room_count~{$products[$this_product]['product_name']}~$status~$test_status~$highlight~";
				$r .= "1|";
			}
			$line_counter++;
			$establishment_count++;
		}
	} else {
		if (!in_array($establishment_code, $eCode_list)) {
//			echo "MAX: $line_max; COUNT: $line_counter; jL1: $jL1<br>";
			if ($line_counter >= $jL1 && $line_counter < $line_max) {
				$r .= "$counter~$establishment_code~$establishment_name~$enter_date~{$review_dates[$establishment_code]}~{$outs[$establishment_code]}~{$assessments[$establishment_code]}~$contact~$room_count~{$products[$this_product]['product_name']}~$status~$test_status~$highlight~";
				$r .= "0|";
			}
			$line_counter++;
			$establishment_count++;
		}
	}


	$counter++;
}
$r = str_replace("\r", "", $r);
$r = str_replace("\n", "", $r);
$r = str_replace("\"", "", $r);
$r = str_replace("~|", "|", $r);

//print_r(implode(',',$list));

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/monthly_invoice/inv/res.js></script>";
echo "<script>J_in_r(\"" . $r . "\"," . $establishment_count . "," . $jL1 . "," . $jL2 . "," . ((isset($_GET['test'])) ? '1' : '0') . ")</script>";
?>