<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/apps/accounts/invoices/default.php";

$id=isset($id)?$id:(isset($_GET["id"])?$_GET["id"]:0);
$v="";
$head="";
$fromCo="";
$toCo="";
$fromPer="";
$fromRep="";
$toPer="";
$fromCnt="";
$toCnt="";
$condition="";
$bank="";
$foot="";
$lock="";

$rs=mysql_query("SELECT * FROM nse_invoice WHERE j_inv_id=".$id);
if(mysql_num_rows($rs))
{
	$g=mysql_fetch_array($rs);

	include_once $SDR."/system/get.php";
	include_once $SDR."/apps/accounts/invoices/get.php";

	$type=$g["j_inv_type"];
	$head=J_inv_getele($g["j_inv_head"]);
	$date=date("d/m/Y H:i",$g["j_inv_date"]);
	$orderNo=$g["j_inv_order"];
	$reference=$g["j_inv_reference"];
	$invoiceNumber=$g["j_inv_number"];
	$fromCompany=explode("~",$invoice_default_company_info);
	$toCompany=explode("~",J_inv_getto($g["j_inv_to_company"]));
	$fromPerson=explode("~",J_inv_getpeo($g["j_inv_from_person"]));
	$fromRep=explode("~",J_inv_getpeo($g["j_inv_from_rep"]));
	$fromContact=explode("~",J_inv_getpeo($g["j_inv_from_contact"]));
	$toPerson=explode("~",J_inv_getpeo($g["j_inv_to_person"]));
	$toContact=explode("~",J_inv_getpeo($g["j_inv_to_contact"]));
	$info=$g["j_inv_info"];
	$comments=$g["j_inv_comment"];
	$conditions=J_inv_getele($g["j_inv_condition"]);
	$bank=J_inv_getele($g["j_inv_bank"]);
	$foot=J_inv_getele($g["j_inv_foot"]);
	$vat=$g["j_inv_vat"];
	$currency=$g["j_inv_currency"];
	$totalPrice=$g["j_inv_total_price"];
	$totalDiscount=$g["j_inv_total_discount"];
	$totalVat=$g["j_inv_total_vat"];
	$total=$g["j_inv_total"];
	$totalUnitPrice=0;

	$items=array();
	$r=mysql_query("SELECT * FROM nse_invoice_item WHERE j_invit_invoice=".$id." ORDER BY j_invit_date,j_invit_name");
	if(mysql_num_rows($r))
	{
		while($i=mysql_fetch_array($r))
		{
			$items[]=array();
			$items[]["id"]=$i["j_invit_id"];
			$items[]["id"]=$i["j_invit_inventory"];
			$items[]["date"]=($i["j_invit_date"]?date("d/m/Y",$i["j_invit_date"]):$date);
			$items[]["name"]=$i["j_invit_name"];
			$items[]["description"]=$i["j_invit_description"];
			$items[]["code"]=$i["j_invit_code"];
			$items[]["quantity"]=$i["j_invit_quantity"];
			$items[]["price"]=$i["j_invit_price"];
			$totalUnitPrice+=$i["j_invit_quantity"]*$i["j_invit_price"];
			$items[]["discount"]=$i["j_invit_discount"];
			$items[]["vat"]=$i["j_invit_vat"];
			$items[]["total"]=$i["j_invit_total"];
			$items[]["serial"]=$i["j_invit_serial"];
			$items[]["vatPercent"]=$i["j_invit_vat_percent"];
			$items[]["discountPercent"]=$i["j_invit_disc_percent"];
		}
	}
}

?>