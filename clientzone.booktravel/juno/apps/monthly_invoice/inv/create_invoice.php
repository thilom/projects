<?php
/**
 * Create and send monthly QA invoices
 *
 * @author Thilo Muller(2010-2012)
 * @category RVBUS
 * @param invoice
 */

include_once $SDR . '/apps/accounts/invoices/increment.php';
include_once $SDR . '/apps/accounts/invoices/default.php';
require_once $SDR . '/utility/PHPMailer/class.phpmailer.php';


//Vars
$email_list = explode('|', $_POST['mails']);

$products = array();
$age_limit = strtotime("now - 6 months");
$designation_priority = array('owner', 'owner/manager', 'manager');
$mail = new PHPMailer();

//Get products
$statement = "SELECT j_in_id, j_in_name, j_in_description, j_in_price, j_in_max_room
				FROM nse_inventory
				WHERE j_in_max_room > 0 && j_in_category=5
				ORDER BY j_in_max_room DESC";
$sql_products = $GLOBALS['dbCon']->prepare($statement);
$sql_products->execute();
$sql_products->store_result();
$sql_products->bind_result($product_id, $product_name, $product_description, $product_price, $max_room);
while ($sql_products->fetch()) {
    $products[$product_id]['product_name'] = $product_name;
    $products[$product_id]['product_description'] = $product_description;
    $products[$product_id]['product_price'] = $product_price;
    $products[$product_id]['max_rooms'] = $max_room;
}
$sql_products->free_result();
$sql_products->close();


//Prepare statement - establishment details
$statement = "SELECT a.establishment_name, a.assessor_id, b.email
				FROM nse_establishment AS a
				LEFT JOIN nse_user AS b ON b.user_id=a.assessor_id
				WHERE a.establishment_code=?";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Insert invoice base
$statement = "INSERT INTO nse_invoice
                (j_inv_number, j_inv_increment_id, j_inv_date, j_inv_from_company, j_inv_from_person,
                j_inv_from_contact, j_inv_to_company, j_inv_to_person, j_inv_to_contact, j_inv_closed,
                j_inv_vat, j_inv_currency, j_inv_head, j_inv_info, j_inv_comment,
                j_inv_condition,j_inv_bank, j_inv_foot, j_inv_total_price, j_inv_total_discount,
                j_inv_total_vat, j_inv_total, j_inv_temp, j_inv_user, j_inv_creator,
                 j_inv_from_rep, j_inv_type, j_inv_paid, j_inv_created)
               VALUES (?,1,?,0,?,
                           ?,?,?,0,0,
                           14,'ZAR',1,'','',
                           ?,?,?,?,0,
                           ?,?,'',?,?,
                           0,1,0, ".$EPOCH.")";
$sql_inv = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - invoice items
$statement = "INSERT INTO nse_invoice_item (j_invit_invoice, j_invit_inventory, j_invit_code, j_invit_serial, j_invit_date,
                j_invit_name, j_invit_description, j_invit_quantity, j_invit_discount, j_invit_vat_percent,
                j_invit_vat, j_invit_total, j_invit_type, j_invit_user, j_invit_deleted,
                j_invit_disc_percent, j_invit_paid, j_invit_price, j_invit_company, j_invit_created)
                    VALUES
                (?, ?, '', '', ?,
                ?, ?, 1, 0, 14,
                ?, ?, 0, ?, 0,
                0, 0, ?, '', ".$EPOCH.")";
$sql_inv_item = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Find invoice
$statement = "SELECT j_inv_id
                FROM nse_invoice AS a
                LEFT JOIN nse_invoice_item AS b ON a.j_inv_id=b.j_invit_invoice
                WHERE a.j_inv_to_company=? && b.j_invit_inventory=? && $age_limit < a.j_inv_date";
$sql_inv_check = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - get room count
$statement = "SELECT room_count FROM nse_establishment_data WHERE establishment_code=?";
$sql_room = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Find owner, manager etc
$statement = "SELECT user_id, designation FROM nse_user_establishments WHERE establishment_code=?";
$sql_contacts = $GLOBALS['dbCon']->prepare($statement);

//prepare statement - Check assessment exists
$statement = "SELECT assessment_id, invoice_id FROM nse_establishment_assessment WHERE establishment_code=? AND (assessment_status='draft' OR assessment_status='auto') LIMIT 1";
$sql_assessment_check = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Insert assessment
$statement = "INSERT INTO nse_establishment_assessment
				(establishment_code, assessment_status, assessment_type, invoice_id, active, assessment_type_2)
					VALUES (?,'auto','renewal',?,'Y','fa')";
$sql_assessment_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Update assessment
$statement = "UPDATE nse_establishment_assessment SET invoice_id=? WHERE assessment_id=?";
$sql_assessment_update = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - get invoice number
$statement = "SELECT j_inv_number FROM nse_invoice WHERE j_inv_id=? LIMIT 1";
$sql_inv_number = $GLOBALS['dbCon']->prepare($statement);

foreach ($email_list as $line) {
    //Vars
    $assessor_id = 0;
    $invoice_id = 0;
    $con_id = 0;
    $establishment_code = 0;
    $user_name = '';

    if (empty($line) || strlen($line) < 10) continue;

    list($email, $establishment_code) = explode('~', $line);

    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->bind_result($establishment_name, $assessor_id, $assessor_email);
    $sql_data->fetch();
    $sql_data->free_result();

    $sql_room->bind_param('s', $establishment_code);
    $sql_room->execute();
    $sql_room->bind_result($room_count);
    $sql_room->fetch();
    $sql_room->free_result();

    $inv_no = J_invoice_increment(1);
    if (empty($assessor_id)) $assessor_id = 0;

    foreach ($products as $product_id=>$prod) {
        if ($room_count <= $prod['max_rooms']) $this_product = $product_id;
    }

    //Check if invoice already exists
    $sql_inv_check->bind_param('ss', $establishment_code, $this_product);
    $sql_inv_check->execute();
    $sql_inv_check->bind_result($invoice_id);
    $sql_inv_check->fetch();
    $sql_inv_check->free_result();

    //Get owner ie. to person
    $sql_contacts->bind_param('s', $establishment_code);
    $sql_contacts->execute();
    $sql_contacts->store_result();
    $sql_contacts->bind_result($user_id, $designation);
    while ($sql_contacts->fetch()) {
        if (empty($con_id)) {
            $con_id = $user_id;
            if (in_array(strtolower($designation), $designation_priority)) {
                $last_designation = array_search(strtolower($designation), $designation_priority);
            } else {
                $last_designation = 0;
            }
        } else {
             if (in_array(strtolower($designation), $designation_priority)) {
                if (array_search(strtolower($designation), $designation_priority) < $last_designation) $last_designation = array_search(strtolower($designation), $designation_priority);
                $con_id = $user_id;
            }
        }
    }
    $sql_contacts->free_result();

    //Calculate VAT
    $vat = $products[$this_product]['product_price']*($invoice_default_vat/100);
    $total = $products[$this_product]['product_price'] + $vat;

    if ($invoice_id == 0) {
         //Create Invoice
        $sql_inv->bind_param('ssssssssssssss', $inv_no, $EPOCH, $assessor_id, $invoice_default_contact, $establishment_code, $con_id, $invoice_default_condition,$invoice_default_bank, $invoice_default_foot, $products[$this_product]['product_price'], $vat, $total, $_SESSION['j_user']['id'], $_SESSION['j_user']['id']);
        $sql_inv->execute();
        $invoice_id = $sql_inv->insert_id;

        $sql_inv_item->bind_param('sssssssss', $invoice_id, $this_product, $EPOCH, $products[$this_product]['product_name'], $products[$this_product]['product_description'], $vat, $total, $_SESSION['j_user']['id'], $products[$this_product]['product_price']);
        $sql_inv_item->execute();
    } else {
		//Get invoice no

		$sql_inv_number->bind_param('i', $invoice_id);
		$sql_inv_number->execute();
		$sql_inv_number->bind_result($inv_no);
		$sql_inv_number->fetch();
		$sql_inv_number->free_result();
	}


	//Check assessment exists
	$assessment_id = 0;
	$sql_assessment_check->bind_param('s', $establishment_code);
	$sql_assessment_check->execute();
	$sql_assessment_check->bind_result($assessment_id, $assessment_invoice);
	$sql_assessment_check->fetch();
	$sql_assessment_check->free_result();

	if ($assessment_id == 0) { //Insert assessment
		$sql_assessment_insert->bind_param('si', $establishment_code, $invoice_id);
		$sql_assessment_insert->execute();
	} else {
		if (empty($assessment_invoice)) {
			$sql_assessment_update->bind_param('ii', $invoice_id, $assessment_id);
			$sql_assessment_update->execute();
		}
	}


    /* ASSEMBLE EMAIL - START */

    //Get template
    $email_template = file_get_contents($SDR . '/apps/monthly_invoice/inv/email_template.html');

    //Get contact details
    $statement = "SELECT CONCAT(b.firstname, ' ', b.lastname)
                    FROM nse_user_establishments AS a
                    JOIN nse_user AS b ON a.user_id=b.user_id
                    WHERE b.email=? LIMIT 1";
    $sql_name = $GLOBALS['dbCon']->prepare($statement);
    $sql_name->bind_param('s', $email);
    $sql_name->execute();
    $sql_name->bind_result($user_name);
    $sql_name->fetch();
    $sql_name->free_result();


    //Get assessor name
    $statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
    $sql_assessor_name = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor_name->bind_param('i', $assessor_id);
    $sql_assessor_name->execute();
    $sql_assessor_name->bind_result($assessor_name);
    $sql_assessor_name->fetch();
    $sql_assessor_name->free_result();

    //Get link
    $name =  stripslashes($establishment_name);
	$name = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $name);
	$name = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $name );
	$name = htmlentities($name);
	$name = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $name);
	$name = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $name);
	$name = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $name);
	$name = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $name);
	$name = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $name);
	$name = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $name);
	$file_name = "http://booktravel.travel/accommodation/". strtolower(str_replace(' ', '_', $establishment_code.'_'.$name . ".html"));

    //Get renewal date
    $statement = "SELECT a.review_date_depricated, b.renewal_date
                    FROM nse_establishment AS a
                    LEFT JOIN nse_establishment_assessment AS b ON a.establishment_code=b.establishment_code
                    WHERE a.establishment_code=? AND b.assessment_status='complete'
                    ORDER BY b.renewal_date
                    LIMIT 1";
    $sql_renew = $GLOBALS['dbCon']->prepare($statement);
    $sql_renew->bind_param('s', $establishment_code);
    $sql_renew->execute();
    $sql_renew->bind_result($dep_renew, $renewal_date);
    $sql_renew->fetch();
    $sql_renew->free_result();
    if (empty($renewal_date)) $renewal_date = $dep_renew;
    list($y, $m, $d) = explode('-', $renewal_date);
    $renewal_date = date('d F Y', mktime(0,0,0, $m, $d, $y ));


    //Replace Tags
    $email_content = str_replace('<!-- month_year -->', date('F Y'), $email_template);
    $email_content = str_replace('<!-- establishment_name -->', $establishment_name, $email_content);
    $email_content = str_replace('<!-- contact -->', $user_name, $email_content);
    $email_content = str_replace("<!-- category_price -->", number_format($products[$this_product]['product_price'],2,'.',''), $email_content);
    $email_content = str_replace("<!-- category_vat -->", number_format($vat,2,'.',''), $email_content);
    $email_content = str_replace("<!-- category_total -->", number_format($total,2,'.',''), $email_content);
    $email_content = str_replace('<!-- assessor_name -->', $assessor_name, $email_content);
    $email_content = str_replace('<!-- establishment_link -->', $file_name, $email_content);
    $email_content = str_replace('<!-- renewal_date -->', $renewal_date, $email_content);

    $email_content = str_replace('<!--', "<span style='color: red; font-weight: bold'>", $email_content);
    $email_content = str_replace('-->', "</span>", $email_content);

    //Get Invoice
    $ch = curl_init("http://{$_SERVER['HTTP_HOST']}/juno/apps/accounts/invoices/pdf_invoice.php?xid=$invoice_id");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $res = curl_exec($ch);
    curl_close($ch);

    $pdf_filename=$SDR."/stuff/accounts/pdf_invoice/".$inv_no."_".$invoice_id.".pdf";


    /* ASSEMBLE EMAIL - END */

    /* SEND EMAIL - START */
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
    if (CUR_DOMAIN == 'clientzone.essentialtravelinfo.com') $mail->AddAddress($email, $user_name);
    if (CUR_DOMAIN == 'clientzone.essentialtravelinfo.com') $mail->AddBCC($assessor_email, $assessor_name);
    $mail->AddBCC('rory@rmfweb.com', $user_name);
    if (CUR_DOMAIN == 'clientzone.essentialtravelinfo.com') $mail->AddBCC('info@essentialtravelinfo.com', 'Quality Assurance');
    $mail->SetFrom('info@essentialtravelinfo.com', 'Quality Assurance');
    $mail->AddReplyTo('info@essentialtravelinfo.com', 'Quality Assurance');
    $mail->Subject = "AA Quality Assurance for $establishment_name";
    $mail->MsgHTML($email_content);

    $mail->AddAttachment($pdf_filename, $inv_no."_".$invoice_id.".pdf");
    $mail->Send();

    /* SEND EMAIL - END */


    //echo $products[$product_id]['product_name'];



}


$_SESSION['juno']['accounts'] = array();


?>
