<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$d="";

$s="SELECT * FROM nse_user WHERE user_id>0 ";
if(isset($_GET["t"]))
	$s.=J_Search($_GET["t"],trim($_GET["f"],"~"));
if(isset($_GET["a"]))
	$s.="AND roles LIKE '%".$_GET["a"]."%' ";
$jL0=mysql_num_rows(mysql_query($s));
$s.="ORDER BY firstname,lastname ";
$s.="LIMIT ".$jL1.",".$jL2;
$rs=mysql_query($s);
$r="";
if(mysql_num_rows($rs))
{
	$dd=explode("~",trim($_GET["d"],"~"));
	$ddd=array();
	$s=array();
	foreach($dd as $k => $v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
	$t=array("X","name","surname","email","tel","cell","est","roles","estab");
	$d="";$n=1;
	foreach($t as $k => $v)
	{
		if(isset($ddd[$v]))
		{
			$d.=$n;
			$n++;
		}
		$d.="~";
	}
	while($g=mysql_fetch_array($rs))
	{
		$estab_list = '';
		
		//Get establishments
		$statement = "SELECT a.establishment_code, b.establishment_name 
						FROM nse_user_establishments AS a
						LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
						WHERE a.user_id=?";
		$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
		$sql_estabs->bind_param('s', $g['user_id']);
		$sql_estabs->execute();
		$sql_estabs->store_result();
		$sql_estabs->bind_result($estab_code, $estab_name);
		while ($sql_estabs->fetch()) {
			$estab_list .= !empty($estab_list)?"<br>":""; 
			$estab_list .= "$estab_code - $estab_name";
		}
		$sql_estabs->close();
		
		$r.=$g["user_id"]."~";
		$r.=ucfirst($g["firstname"])."~";
		$r.=ucfirst($g["lastname"])."~";
		if(isset($s["email"]))
			$r.=$g["email"]."~";
		if(isset($s["tel"]))
			$r.=$g["phone"]."~";
		if(isset($s["cell"]))
			$r.=$g["cell"]."~";
		if(isset($s["est"]))
			$r.=$estab_list."~";
		if(isset($s["roles"]))
			$r.=$g["roles"]."~";
		
		$r.="|";
	}
	$r=str_replace("\r","",$r);
	$r=str_replace("\n","",$r);
	$r=str_replace("\"","",$r);
	$r=str_replace("~|","|",$r);
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/people/edit/res.js></script>";
echo "<script>J_in_r(\"".$r."\",\"".$d."\",".$jL0.",".$jL1.",".$jL2.",".J_getSecure("/apps/people/edit_person.php").",".J_getSecure("/apps/people/send_login.php").")</script>";
?>