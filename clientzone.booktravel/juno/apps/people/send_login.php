<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
include $SDR."/system/get.php";

$id=$_GET["id"];
$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$id." LIMIT 1"));

if(count($_POST))
{
	$sender=array("email"=>"","name"=>"");
	$target=array("email"=>$g["email"],"name"=>ucwords(strtolower($g["firstname"]." ".$g["lastname"])));
	$s="SELECT firstname,lastname,email FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1";
	$s=mysql_query($s);
	if(mysql_num_rows($s))
	{
		$s=mysql_fetch_array($s);
		if($s["email"])
			$sender=array("email"=>$s["email"],"name"=>ucwords(strtolower($s["firstname"]." ".$s["lastname"])));
	}
	if($sender["email"] && $target["email"])
	{
		include $SDR."/custom/email_headers.php";
		$subject="ESSENTIAL TRAVEL INFO SYSTEM LOGIN";
		$message=substr(preg_replace('~[^a-zA-Z0-9 ,;:"@#_<>+=%&_\*\-\$\'\(\)\[\]\?\.]~',"",strip_tags(str_replace("\n","<br>",str_replace("\r","",$_POST["m"])),"<br>")),0,100000);
		$msg=$j_email_header;
		$msg.="<b>ESSENTIAL TRAVEL INFO SYSTEM LOGIN</b><hr>";
		$msg.=($message?$message."<br><br><hr>":"");
		$msg.="User-Name: <b>".$g["login_name"]."</b><br>";
		require $SDR."/custom/login/class.login.php";
		$msg.="Password: <b>".$GLOBALS['tc']->decrypt($g["password"], $g['iv'])."</b><br>";
		$msg.="<hr><div style='font-size:9pt'><b style='color:#DD0000'>IMPORTANT!</b> For security reasons, we strongly recommend you delete this email as soon as you have used your details. We can always send details when you require.</div>";
		$msg.="<br><br><br>";
		$msg.=$j_email_footer;

		require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
		$mail=new PHPMailer(true);
		try
		{
			$mail->AddReplyTo($sender["email"],$sender["name"]);
			$mail->AddAddress($target["email"],$target["name"]);
			$mail->SetFrom($sender["email"],$sender["name"]);
			$mail->Subject =$subject;
			$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
			$mail->MsgHTML($msg);
			$mail->Send();
			include $SDR."/system/activity.php";
			J_act("LOGIN",7);
			echo "Message Sent OK";
			echo "<script>setTimeout(\"window.parent.J_WX(self)\",2000)</script>";
		}
		catch (phpmailerException $e){echo $e->errorMessage();}
		catch (Exception $e){echo $e->getMessage();}
		die();
	}
	else
		echo "<script>alert('Target email address failure!')</script>";
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<body style='margin:2px 16px'>";

echo "<form method=post>";
echo "<table width=100% height=100% cellspacing=0 cellpadding=0>";
echo "<tr><td height=1><tt>The user will be emailed their login-name and password - you may also send a message with this email.</tt></td></tr>";
echo "<tr><td><textarea name=m class=jW100 style=height:100%></textarea></td></tr>";
echo "<tr><td height=1><input type=submit value=OK></td></tr>";
echo "</table>";
echo "</form>";

$J_title1="Send Login";
$J_title3="To: <b>".$g["firstname"]." ".$g["lastname"]."</b> (".$g["email"].")";
$J_icon="<img src=".$ROOT."/ico/set/password.png>";
$J_label=5;
$J_width=360;
$J_height=260;
$J_nostart=1;
$J_nomax=1;
$J_foot="";
if(J_getCurator())
	$J_foot.="<a href=go: onclick=\"J_W(ROOT+'/apps/people/edit_person.php?id=".$id."');return false\" onmouseover=\"J_TT(this,'Access control')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/lock.png></a>";
if(J_getSecure("/apps/people/edit_person.php"))
	$J_foot.="<a href=go: onclick=\"J_W(ROOT+'/apps/people/edit_person.php?id=".$id."');return false\" onmouseover=\"J_TT(this,'Edit person')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/edit.png></a>";
else
	$J_foot.="<a href=go: onclick=\"J_W(ROOT+'/apps/people/view.php?id=".$id."');return false\" onmouseover=\"J_TT(this,'View person')\" onfocus=blur() class=jW3a><img src=".$ROOT."/ico/set/user_male.png></a>";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>