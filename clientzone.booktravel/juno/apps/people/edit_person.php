<?php

/**
 * Add and edit establishment contacts
 *
 * @version $Id: edit_person.php 255 2011-02-14 08:41:14Z web37 $
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";
include $SDR . "/set/roles.php";
require_once $SDR . "/custom/login/class.login.php";

if (isset($_GET["chk"])) {
	echo "<script>window.parent.P_c(0,1," . (mysql_num_rows(mysql_query("SELECT user_id FROM nse_user WHERE login_name='" . str_replace("```", "&", str_replace("~~~", "?", $_GET["chk"])) . "' LIMIT 1")) ? 1 : 0) . ")</script>";
	die();
}

$id = (isset($_GET["my"]) || isset($my_details) ? $_SESSION["j_user"]["id"] : (isset($id) ? $id : (isset($_GET["id"]) ? $_GET["id"] : 0)));
$establishment_code = isset($_GET['est'])?$_GET['est']:"";

$company_role = '';

$SUB_ACCESS = array("roles" => 0);
if (!isset($_GET["excp"]))
	include $SDR . "/system/secure.php";

if (count($_POST)) {

	function J_strip($v="", $l=0) {
		$v = stripslashes($v);
		$v = strip_tags($v, "<br>");
		$v = str_replace("{", "(", $v);
		$v = str_replace("}", ")", $v);
		$v = str_replace("  ", " ", $v);
		$v = str_replace("  ", " ", $v);
		$v = str_replace("\"", "'", $v);
		$v = str_replace("\r", "", $v);
		$v = str_replace("\v", "", $v);
		$v = str_replace("\t", "", $v);
		$v = str_replace("\n", "<br>", $v);
		$v = preg_replace('~[^a-zA-Z0-9 _,:;+<>!@#$%&\'\(\)\-\?\.\*]~s', "", $v);
		$v = trim($v, ";");
		$v = trim($v, ",");
		$v = trim($v, ".");
		$v = trim($v);
		$v = addslashes($v);
		return $v;
	}

	include_once $SDR . "/system/activity.php";
	$err = 1;
	$new_roles = "";
	$q = "";
	$c = "";
	if ($_POST["password"]) {
		include_once $SDR . "/custom/login/tcrypt.class.php";
		$tc = new tcrypt();
		$pass = $tc->encrypt($_POST["password"]);
		$password = $pass['data'];
		$iv = $pass['iv'];
		unset($_POST["password"]);

		//Save new password
		$statement = "UPDATE nse_user SET iv=?, password=? WHERE user_id=?";
		$sql_password = $GLOBALS['dbCon']->prepare($statement);
		$sql_password->bind_param('ssi', $iv, $password, $id);
		$sql_password->execute();

	} else {
		unset($_POST["password"]);
	}
	if (isset($_POST["oR_"])) {
		$old_roles = str_split($_POST["oR_"]);
		unset($_POST["oR_"]);
	}

	foreach ($_POST as $k => $v) {
		if (strpos($k, "r_") !== false)
			$new_roles.=$v;
		else {
			if (in_array($k, array('awards_contact','ads_contact','accounts_contact','qa_contact','materials_contact','proofing_contact'))) continue;
			$v = J_strip($v);
			if ($id)
				$q.= ( $q ? "," : "") . $k . "='" . $v . "'";
			else {
				$c.= ( $c ? "," : "") . $k;
				$q.= ( $q ? "," : "") . "'" . $v . "'";
			}
		}
	}

$new_roles = $new_roles ? $new_roles : "c";
if ($id) {
	$s = "UPDATE nse_user SET " . $q;
	if($SUB_ACCESS["roles"])
		$s.=",roles='" . $new_roles . "'";
	$s.=" WHERE user_id=" . $id;
	mysql_query($s);
	if (mysql_error ()) {
		$err = 1;
		echo mysql_error() . "<br>" . $s . "<hr>";
	}
	else
		J_act("Person", 4, $_POST["firstname"] . " " . $_POST["lastname"],$id);
}
else {
	$s = "INSERT INTO nse_user (" . $c;
	if($SUB_ACCESS["roles"])
		$s.=",roles";
	$s.=") VALUES (" . $q;
	if($SUB_ACCESS["roles"])
		$s.=",'".$new_roles."'";
	$s.=")";
	mysql_query($s);
	if (mysql_error ()) {
		$err = 1;
		echo mysql_error() . "<br>" . $s . "<hr>";
	} else {
		$id = J_ID("nse_user", "user_id");
		J_act("Person", 3, $_POST["firstname"] . " " . $_POST["lastname"],$id);
	}
}

	// update security roles
	if ($SUB_ACCESS["roles"]) {
		$x = mktime(0, 0, 0, date("m"), date("d"), date("Y") + 3);
		$new_roles = str_split($new_roles);
		$chk_role = array();
		foreach ($new_roles as $k => $v)
			$chk_role[$v] = 1;
		foreach ($old_roles as $k => $r) {
			if (isset($chk_role[$r]) && isset($roles[$r])) {
				$q = "SELECT * FROM nse_user_pages_set WHERE j_uas_name='" . $roles[$r] . "'";
				$q = mysql_query($q);
				if (mysql_num_rows($q)) {
					while ($g = mysql_fetch_array($q)) {
						$q1 = "SELECT * FROM nse_user_pages WHERE j_uac_user=" . $id . " AND j_uac_page='" . $g["j_uas_page"] . "' AND j_uac_role='" . $r . "' LIMIT 1";
						$q1 = mysql_query($q1);
						if (!mysql_num_rows($q1)) {
							$s = "INSERT INTO nse_user_pages (
							j_uac_date
							,j_uac_expiry
							,j_uac_curator
							,j_uac_user
							,j_uac_role
							,j_uac_page
							,j_uac_set
							) VALUES (
							" . $EPOCH . "
							," . $x . "
							," . $_SESSION["j_user"]["id"] . "
							," . $id . "
							,'" . $r . "'
							,'" . $g["j_uas_page"] . "'
							,'" . $roles[$r] . "'
							)";
							mysql_query($s);
						}
					}
				}
			}
			elseif(isset($roles[$r]))
				mysql_query("DELETE FROM nse_user_pages WHERE j_uac_user=" . $id . " AND j_uac_page!='SuperUser' AND (j_uac_role='" . $r . "' OR j_uac_set='" . $roles[$r] . "')");
		}
	}

	//Does role exist
	$current_designation = '';
	$statement = "SELECT designation FROM nse_user_establishments WHERE establishment_code=? AND user_id=?";
	$sql_user_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_user_check->bind_param('si', $establishment_code, $id);
	$sql_user_check->execute();
	$sql_user_check->store_result();
	$c = $sql_user_check->num_rows;
	$sql_user_check->bind_result($current_designation);
	$sql_user_check->fetch();
	$sql_user_check->close();

	if (isset($_POST['designation']) && $current_designation != $_POST['designation']) {
		if ($c == 0) {
			$statement = "INSERT INTO nse_user_establishments (user_id, establishment_code, designation) VALUES (?,?,?)";
			$sql_designation_add = $GLOBALS['dbCon']->prepare($statement);
			$sql_designation_add->bind_param('iss', $id, $establishment_code, $_POST['designation']);
			$sql_designation_add->execute();
			$sql_designation_add->close();
		} else {
			$statement = "UPDATE nse_user_establishments SET designation=? WHERE establishment_code=? AND user_id=?";
			$sql_designation_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_designation_update->bind_param('ssi', $_POST['designation'], $establishment_code, $id);
			$sql_designation_update->execute();
			$sql_designation_update->close();
		}
	}

	//Save Awards Type
	$awards_contact = isset($_POST['awards_contact'])?'Y':'';
	$statement = "UPDATE nse_user_establishments
					SET awards_contact=?
					WHERE user_id=? AND establishment_code=?
					LIMIT 1";
	$sql_type = $GLOBALS['dbCon']->prepare($statement);
	$sql_type->bind_param('sis', $awards_contact, $id, $establishment_code);
	$sql_type->execute();
	$sql_type->close();
	
	//Save Ads Type
	$ads_contact = isset($_POST['ads_contact'])?'Y':'';
	$statement = "UPDATE nse_user_establishments
					SET ads_contact=?
					WHERE user_id=? AND establishment_code=?
					LIMIT 1";
	$sql_type = $GLOBALS['dbCon']->prepare($statement);
	$sql_type->bind_param('sis', $ads_contact, $id, $establishment_code);
	$sql_type->execute();
	$sql_type->close();
	
	//Save Accounts Type
	$accounts_contact = isset($_POST['accounts_contact'])?'Y':'';
	$statement = "UPDATE nse_user_establishments
					SET accounts_contact=?
					WHERE user_id=? AND establishment_code=?
					LIMIT 1";
	$sql_type = $GLOBALS['dbCon']->prepare($statement);
	$sql_type->bind_param('sis', $accounts_contact, $id, $establishment_code);
	$sql_type->execute();
	$sql_type->close();
	
	//Save QA Type
	$qa_contact = isset($_POST['qa_contact'])?'Y':'';
	$statement = "UPDATE nse_user_establishments
					SET qa_contact=?
					WHERE user_id=? AND establishment_code=?
					LIMIT 1";
	$sql_type = $GLOBALS['dbCon']->prepare($statement);
	$sql_type->bind_param('sis', $qa_contact, $id, $establishment_code);
	$sql_type->execute();
	$sql_type->close();
	
	//Save Materials Type
	$materials_contact = isset($_POST['materials_contact'])?'Y':'';
	$statement = "UPDATE nse_user_establishments
					SET materials_contact=?
					WHERE user_id=? AND establishment_code=?
					LIMIT 1";
	$sql_type = $GLOBALS['dbCon']->prepare($statement);
	$sql_type->bind_param('sis', $materials_contact, $id, $establishment_code);
	$sql_type->execute();
	$sql_type->close();
	
	
	//Save Proofing Type
	$proofing_contact = isset($_POST['proofing_contact'])?'Y':'';
	$statement = "UPDATE nse_user_establishments
					SET proofing_contact=?
					WHERE user_id=? AND establishment_code=?
					LIMIT 1";
	$sql_type = $GLOBALS['dbCon']->prepare($statement);
	$sql_type->bind_param('sis', $proofing_contact, $id, $establishment_code);
	$sql_type->execute();
	$sql_type->close();
	


	if (!$err) {
		unset($_SESSION["juno"]["accounts"]["user"][$id]);
		echo "<script>window.parent.J_WX(self)</script>";
		die();
	}
}

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/people/edit/edit.js></script>";
echo "<body>";

if ($id) {
	$g = mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=" . $id . " LIMIT 1"));

	//Get company role for user
	$statement = "SELECT designation FROM nse_user_establishments WHERE user_id=? AND establishment_code=?";
	$sql_role = $GLOBALS['dbCon']->prepare($statement);
	$sql_role->bind_param('si', $id, $establishment_code);
	$sql_role->execute();
	$sql_role->bind_result($company_role);
	$sql_role->fetch();
	$sql_role->close();

} else {
	$g = array(
		"firstname" => ""
		, "lastname" => ""
		, "email" => ""
		, "phone" => ""
		, "cell" => ""
		, "roles" => ""
		, "establishment_group" => ""
		, "login_name" => ""
	);
}



$y = 0;

echo "<form method=post>";
echo "<table id=jTR class=jW100>";
echo "<tr><th>Name</th><td><input type=text name=firstname value=\"" . $g["firstname"] . "\" title='Requires value' class=jW100></td></tr>";
echo "<tr><th>Surname</th><td><input type=text name=lastname value=\"" . $g["lastname"] . "\" title='Requires value' class=jW100></td></tr>";

//Roles
if (!$SUB_ACCESS["roles"]) {
	$company_roles = array('','CEO','Owner','Owner/Manager','Manager','General Manager','Marketing Manager','Accounts','Reception','Tourism','Operations','Proofing');
	echo "<tr><th>Designation</th><td><select name=designation class=jW100>";
	foreach ($company_roles as $v) {
		echo $company_role==$v?"<option value='$v' selected>$v":"<option value='$v'>$v";
	}
	echo "</select></td></tr>";
}


echo "<tr><th>Email</th><td><input type=text name=email value=\"" . $g["email"] . "\" title='Requires email' class=jW100></td></tr>";
echo "<tr><th>Tel</th><td><input type=text name=phone value=\"" . $g["phone"] . "\" onclick=j_P.J_telFormat(this) class=jW100></td></tr>";
echo "<tr><th>Cell</th><td><input type=text name=cell value=\"" . $g["cell"] . "\" onclick=j_P.J_telFormat(this) class=jW100></td></tr>";
$ln = $g["login_name"] ? $g["login_name"] : (mysql_num_rows(mysql_query("SELECT user_id FROM nse_user WHERE login_name='" . $g["email"] . "' LIMIT 1")) ? "" : $g["email"]);
echo "<tr><th>User-Name</th><td><input type=text name=login_name value='" . $ln . "' onkeyup=P_0(this) class=jW100 title='Requires value' onmouseover=J_TT(this,jun)>";
echo "</td></tr>";
$password = "";
if ($id && !empty($g["password"])) {
	$password = $GLOBALS["tc"]->decrypt($g["password"], $g['iv']);
}
if (!$password) {
		include_once $SDR . "/custom/security.class.php";
		$gp = new security();
	$password = $gp->generate_password();
}
echo "<tr><th>Password</th><td><input type=password name=password value=\"" . $password . "\" class=jW100" . ($id ? "" : " title='Requires password'") . " onmouseover=J_TT(this,jpw)></td></tr>";
if ($SUB_ACCESS["roles"]) {
	if ($id)
		$y = 1;
	echo "<tr><th>Roles</th><td>";
	foreach ($roles as $k => $v)
		echo "<tt onclick=j_P.J_checkbox(this)><input type=checkbox name=r_" . $k . " value=" . $k . (strpos($g["roles"], $k) !== false ? " checked" : "") . "> " . $v . "</tt>";
	echo "<input type=hidden name=oR_ value=" . $g["roles"] . ">";
	echo "</td></tr>";
	echo "<tr><th>Group</th><td><select name=establishment_group onmouseover=J_TT(this,jeg) class=jW100><option value=''>";
	$q = "SELECT DISTINCT group_name FROM nse_establishment_group ORDER BY group_name";
	$q = mysql_query($q);
	if (mysql_num_rows($q)) {
		while ($eg = mysql_fetch_array($q))
			echo "<option value='" . $eg["group_name"] . "'" . ($eg["group_name"] == $g["establishment_group"] ? " selected" : "") . ">" . $eg["group_name"];
	}
	echo "</select></td></tr>";
}

//Contact Types
$awards_contact = '';
$ads_contact = '';
$accounts_contact = '';
$qa_contact = '';
$materials_contact = '';
$proofing_contact = '';
$est_code = str_pad($establishment_code, 6, 0, STR_PAD_LEFT);
$statement = "SELECT awards_contact, ads_contact, accounts_contact, qa_contact, materials_contact, proofing_contact
				FROM nse_user_establishments
				WHERE establishment_code=? AND user_id=?
				LIMIT 1";
$sql_type = $GLOBALS['dbCon']->prepare($statement);
$sql_type->bind_param('si', $est_code, $id);
$sql_type->execute();
$sql_type->bind_result($awards_contact, $ads_contact, $accounts_contact, $qa_contact, $materials_contact, $proofing_contact);
$sql_type->fetch();
$sql_type->close();

$awards_contact = $awards_contact=='Y'?'checked':'';
$ads_contact = $ads_contact=='Y'?'checked':'';
$accounts_contact = $accounts_contact=='Y'?'checked':'';
$qa_contact = $qa_contact=='Y'?'checked':'';
$materials_contact = $materials_contact=='Y'?'checked':'';
$proofing_contact = $proofing_contact=='Y'?'checked':'';

echo "<tr><th>Type</th><td>";
echo "<input type=checkbox name='ads_contact' value='Y' $ads_contact >Advertising contact<br>";
echo "<input type=checkbox name='awards_contact' value='Y' $awards_contact >Awards contact<br>";
echo "<input type=checkbox name='accounts_contact' value='Y' $accounts_contact >Accounts contact<br>";
echo "<input type=checkbox name='qa_contact' value='Y' $qa_contact >QA contact<br>";
echo "<input type=checkbox name='materials_contact' value='Y' $materials_contact >Materials contact<br>";
echo "<input type=checkbox name='proofing_contact' value='Y' $proofing_contact >Proofing contact<br>";
echo "</td></tr>";

echo "<script>P_(" . ($y ? $id : 0) . "," . J_getCurator() . "," . J_getSecure("/apps/people/send_login.php") . ")</script>";

$J_title1 = "Edit Person";
$J_title1 = ($id ? $g["firstname"] . " " . $g["lastname"] : "Add Person");
$J_title2 = ($id ? "Edit Person" : "");
if ($id && $_SESSION["j_user"]["role"] == "d")
	$J_title3 = "User id: " . $id;
$J_icon = "<img src=" . $ROOT . "/ico/set/user_both.png><var><img src=" . $ROOT . "/ico/set/" . ($id ? "edit" : "add") . ".png></var>";
$J_label = 5;
$J_width = 400;
$J_height = $SUB_ACCESS["roles"] ? 360 : 300;
$J_nomax = 1;
$J_nostart = 1;
include $SDR . "/system/deploy.php";
echo "</body></html>";
?>