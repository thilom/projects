<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

//Vars
$qa_in_new_total = 0;
$qa_in_renew_total = 0;
$qa_out_total = 0;
$qa_lapsed_total = 0;
$net_total = 0;
$g_in_new = '';
$g_in_renew = '';
$g_out = '';
$xaxis = '';
$y_start = 0;
$y_end = 0;
$year = isset($_GET['y'])?$_GET['y']:date('Y');
$month = isset($_GET['m'])?$_GET['m']:date('m');


if (isset($_GET['ass_snp'])) {
    $date = date('Y-m-d');

    $html = "<h4>Waiting for approval (<!-- waiting --> in total)</h4>";
    $html .= "<table width=100% id=jTR style='border: solid 1px black'>";
    $html .= "<tr style='background-color: gray'><th>Code</th><th>Name</th><th>Assessor</th>";
    $statement = "SELECT a.establishment_code, b.establishment_name, CONCAT(c.firstname, ' ', c.lastname)
                    FROM nse_establishment_assessment AS a
                    JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                    JOIN nse_user AS c ON b.assessor_id=c.user_id
                    WHERE assessment_status='waiting'";
    if (isset($_GET['a'])) $statement .= " && b.assessor_id='{$_GET['a']}'";
    $sql_waiting = $GLOBALS['dbCon']->prepare($statement);
    $sql_waiting->execute();
    $sql_waiting->store_result();
    $waiting_num = $sql_waiting->num_rows;
    $sql_waiting->bind_result($code, $name, $assessor);
    while($sql_waiting->fetch()) {
        $html .= "<tr><td class=jdbY>$code</td><td  class=jdbO>$name</td><td  class=jdbR>$assessor</td></tr>";
    }
    $html = str_replace('<!-- waiting -->', $waiting_num, $html);
    $html .= "</table><br><br>";


    $html .= "<h4>Approved Today (<!-- today --> in total)</h4>";
    $html .= "<table width=100% id=jTR style='border: solid 1px black'>";
    $html .= "<tr style='background-color: gray'><th>Code</th><th>Name</th><th>Assessor</th>";
    $statement = "SELECT a.establishment_code, b.establishment_name, CONCAT(c.firstname, ' ', c.lastname)
                    FROM nse_establishment_assessment AS a
                    JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                    JOIN nse_user AS c ON b.assessor_id=c.user_id
                    WHERE assessment_status='complete' && approval_date=?";
    if (isset($_GET['a'])) $statement .= " && b.assessor_id='{$_GET['a']}'";
    $sql_waiting = $GLOBALS['dbCon']->prepare($statement);
    $sql_waiting->bind_param('s', $date);
    $sql_waiting->execute();
    $sql_waiting->store_result();
    $waiting_num = $sql_waiting->num_rows;
    $sql_waiting->bind_result($code, $name, $assessor);
    while($sql_waiting->fetch()) {
        $html .= "<tr><td class=jdbY>$code</td><td  class=jdbO>$name</td><td  class=jdbR>$assessor</td></tr>";
    }
    $html = str_replace('<!-- today -->', $waiting_num, $html);
    $html .= "</table><br><br>";



    $date = date('Y-m-');
    $day = date('d') - 1 ;
    $date  .= $day;
    $html .= "<h4>Approved Yesterday (<!-- yesterday --> in total)</h4>";
    $html .= "<table width=100% id=jTR style='border: solid 1px black'>";
    $html .= "<tr style='background-color: gray'><th>Code</th><th>Name</th><th>Assessor</th>";
    $statement = "SELECT a.establishment_code, b.establishment_name, CONCAT(c.firstname, ' ', c.lastname)
                    FROM nse_establishment_assessment AS a
                    JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                    JOIN nse_user AS c ON b.assessor_id=c.user_id
                    WHERE assessment_status='complete' && approval_date=?";
    if (isset($_GET['a'])) $statement .= " && b.assessor_id='{$_GET['a']}'";
    $sql_waiting = $GLOBALS['dbCon']->prepare($statement);
    $sql_waiting->bind_param('s', $date);
    $sql_waiting->execute();
    $sql_waiting->store_result();
    $waiting_num = $sql_waiting->num_rows;
    $sql_waiting->bind_result($code, $name, $assessor);
    while($sql_waiting->fetch()) {
        $html .= "<tr><td class=jdbY>$code</td><td  class=jdbO>$name</td><td  class=jdbR>$assessor</td></tr>";
    }
    $html = str_replace('<!-- yesterday -->', $waiting_num, $html);
    $html .= "</table><br><br>";




    $html .= "<h4>Returned to assessor (<!-- returned --> in total)</h4>";
    $html .= "<table width=100% id=jTR style='border: solid 1px black'>";
    $html .= "<tr style='background-color: gray'><th>Code</th><th>Name</th><th>Assessor</th>";
    $statement = "SELECT a.establishment_code, b.establishment_name, CONCAT(c.firstname, ' ', c.lastname)
                    FROM nse_establishment_assessment AS a
                    JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                    JOIN nse_user AS c ON b.assessor_id=c.user_id
                    WHERE assessment_status='returned'";
    if (isset($_GET['a'])) $statement .= " && b.assessor_id='{$_GET['a']}'";
    $sql_waiting = $GLOBALS['dbCon']->prepare($statement);
    $sql_waiting->execute();
    $sql_waiting->store_result();
    $waiting_num = $sql_waiting->num_rows;
    $sql_waiting->bind_result($code, $name, $assessor);
    while($sql_waiting->fetch()) {
        $html .= "<tr><td class=jdbY>$code</td><td  class=jdbO>$name</td><td  class=jdbR>$assessor</td></tr>";
    }
    $html = str_replace('<!-- returned -->', $waiting_num, $html);
    $html .= "</table><br><br>";


    $html .= "<h4>Saved as Draft by Assessor (<!-- draft --> in total)</h4>";
    $html .= "<table width=100% id=jTR style='border: solid 1px black'>";
    $html .= "<tr style='background-color: gray'><th>Code</th><th>Name</th><th>Assessor</th>";
    $statement = "SELECT a.establishment_code, b.establishment_name, CONCAT(c.firstname, ' ', c.lastname)
                    FROM nse_establishment_assessment AS a
                    JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                    JOIN nse_user AS c ON b.assessor_id=c.user_id
                    WHERE assessment_status='draft'";
    if (isset($_GET['a'])) $statement .= " && b.assessor_id='{$_GET['a']}'";
    $sql_waiting = $GLOBALS['dbCon']->prepare($statement);
    $sql_waiting->execute();
    $sql_waiting->store_result();
    $waiting_num = $sql_waiting->num_rows;
    $sql_waiting->bind_result($code, $name, $assessor);
    while($sql_waiting->fetch()) {
        $html .= "<tr><td class=jdbY>$code</td><td  class=jdbO>$name</td><td  class=jdbR>$assessor</td></tr>";
    }
    $html = str_replace('<!-- draft -->', $waiting_num, $html);
    $html .= "</table><br><br>";


} else if (isset($_GET['ass_prod'])) {
    $days_in_month = date('t', mktime(0, 0, 0, $month));
    $qa_visit_total = 0;
    $qa_submit_total = 0;
    $qa_return_total = 0;
    $qa_approve_total = 0;
    $g_visit = '';
    $g_submit = '';
    $g_return = '';
    $g_approve = '';

    $html = "<h2>Productivity Report for " . date('F', mktime(0, 0, 0, $month)) . " $year </h2>";
    $html .= "<div style='border: 1px dotted gray; background-color: #F2F2F2; width: 810px; padding: 5px ' >";
    $html .= "<img style='border: 5px solid black' src=http://chart.apis.google.com/chart?chs=800x150&chd=t:!data!&cht=bvg&chxt=x,y&chxr=0,1,!days!,1|1,!ystart!,!yend!&chbh=a,10,5&chco=FFCD08,EBDA04,AD0808,2316F3&chf=c,lg,90,B3B3B3,0,DCDCDC,0.5|bg,s,000000 />";

    $html .= "<table border=1 width=810px id=jTR>";
    $html .= "<th style=' width: 20px'>Day</th>";
    $html .= "<th style='background-color: #FFCD08'>Visited</th>";
    $html .= "<th style='background-color: #EBDA04'>Submitted</th>";
    $html .= "<th style='background-color: #AD0808'>Returned</th>";
    $html .= "<th style='background-color: #2316F3'>Approved</th>";

    //Prepare statement - Visit count
    $statement = "SELECT COUNT(*)
                            FROM nse_establishment_assessment AS a
                            LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                            WHERE a.assessment_date=? ";
    $statement .= isset($_GET['a'])?"&& b.assessor_id={$_GET['a']}":"&& b.assessor_id!=''";
    $sql_visit = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Submit count
    $statement = "SELECT COUNT(*)
                            FROM nse_establishment_assessment AS a
                            LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                            WHERE a.submitted_date=? ";
    $statement .= isset($_GET['a'])?"&& b.assessor_id={$_GET['a']}":"&& b.assessor_id!=''";
    $sql_submit = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Return count
    $statement = "SELECT COUNT(*)
                            FROM nse_establishment_assessment AS a
                            LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                            WHERE a.return_date=? ";
    $statement .= isset($_GET['a'])?"&& b.assessor_id={$_GET['a']}":"&& b.assessor_id!=''";
    $sql_return = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Approve count
    $statement = "SELECT COUNT(*)
                            FROM nse_establishment_assessment AS a
                            LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
                            WHERE a.approval_date=? ";
    $statement .= isset($_GET['a'])?"&& b.assessor_id={$_GET['a']}":"&& b.assessor_id!=''";
    $sql_approve = $GLOBALS['dbCon']->prepare($statement);

    //Find max Y values
    for ($i=1; $i<=$days_in_month; $i++) {
        $date = "$year-$month-$i";

        if (mktime(0, 0, 0, $month, $i, $year) > mktime()) {

        } else {
            //Visit count
            $sql_visit->bind_param('s', $date);
            $sql_visit->execute();
            $sql_visit->bind_result($qa_visit);
            $sql_visit->fetch();
            $sql_visit->free_result();

            if ($qa_visit < $y_start) $y_start = $qa_visit;
            if ($qa_visit > $y_end) $y_end = $qa_visit;

             //Submit count
            $sql_submit->bind_param('s', $date);
            $sql_submit->execute();
            $sql_submit->bind_result($qa_submit);
            $sql_submit->fetch();
            $sql_submit->free_result();

            if ($qa_submit < $y_start) $y_start = $qa_submit;
            if ($qa_submit > $y_end) $y_end = $qa_submit;

            //Return count
            $sql_return->bind_param('s', $date);
            $sql_return->execute();
            $sql_return->bind_result($qa_return);
            $sql_return->fetch();
            $sql_return->free_result();

            if ($qa_return < $y_start) $y_start = $qa_return;
            if ($qa_return > $y_end) $y_end = $qa_return;

            //Approve count
            $sql_approve->bind_param('s', $date);
            $sql_approve->execute();
            $sql_approve->bind_result($qa_approve);
            $sql_approve->fetch();
            $sql_approve->free_result();

            if ($qa_approve < $y_start) $y_start = $qa_approve;
            if ($qa_approve > $y_end) $y_end = $qa_approve;
        }
    }

    $y_end = $y_end + 1;
    $height = $y_end - $y_start;

    for ($i=1; $i<=$days_in_month; $i++) {
        $xaxis .= "$i,";
        $qa_in = '';
        $date = "$year-$month-$i";


        if (mktime(0, 0, 0, $month, $i, $year) > mktime()) {
            $qa_visit = '-';
            $qa_in_renew = '-';
            $qa_out = '-';
            $g_in_new .= "0,";
            $g_in_renew .= "0,";
            $g_out .= "0,";
            $net = '-';
        } else {
            $qa_visit = '0';
            $qa_in_renew = '-';
            $qa_out = '-';
            $g_in_new .= "0,";
            $g_in_renew .= "0,";
            $g_out .= "0,";
            $net = '-';

            //Visit count
            $sql_visit->bind_param('s', $date);
            $sql_visit->execute();
            $sql_visit->bind_result($qa_visit);
            $sql_visit->fetch();
            $sql_visit->free_result();
            $g_visit .= $qa_visit==0?'0,':($qa_visit/$height) * 100 . ",";

            //Submit count
            $sql_submit->bind_param('s', $date);
            $sql_submit->execute();
            $sql_submit->bind_result($qa_submit);
            $sql_submit->fetch();
            $sql_submit->free_result();
            $g_submit .= $qa_submit==0?'0,':($qa_submit/$height) * 100 . ",";

            //Return count
            $sql_return->bind_param('s', $date);
            $sql_return->execute();
            $sql_return->bind_result($qa_return);
            $sql_return->fetch();
            $sql_return->free_result();
            $g_return .= $qa_return==0?'0,':($qa_return/$height) * 100 . ",";

            //Approve count
            $sql_approve->bind_param('s', $date);
            $sql_approve->execute();
            $sql_approve->bind_result($qa_approve);
            $sql_approve->fetch();
            $sql_approve->free_result();
            $g_approve .= $qa_approve==0?'0,':($qa_approve/$height) * 100 . ",";

            $qa_visit_total += $qa_visit;
            $qa_submit_total += $qa_submit;
            $qa_return_total += $qa_return;
            $qa_approve_total += $qa_approve;
        }

        $html .= "<tr>";
        $html .= "<td  class=jdbK>$i</td>";
        $html .= "<td  class=jdbY>$qa_visit</td>";
        $html .= "<td  class=jdbO>$qa_submit</td>";
        $html .= "<td  class=jdbR>$qa_return</td>";
        $html .= "<td  class=jdbB>$qa_approve</td>";
        $html .= "</tr>";
    }

    $html .= "<tr style='border-top: double 4px gray; font-weight: bold'>";
    $html .= "<td>Totals</td>";
    $html .= "<td style='background-color: #F9E38C'>$qa_visit_total</td>";
    $html .= "<td style='background-color: #F4ED97'>$qa_submit_total</td>";
    $html .= "<td style='background-color: #FAB6B6'>$qa_return_total</td>";
    $html .= "<td style='background-color: #C4C1FB'>$qa_approve_total</td>";
    $html .= "</tr>";
    $html .= "</table></div>";

    $html .= "<html>";

    //Replace Graph values
    $data = substr($g_visit, 0, -1) . '|' . substr($g_submit, 0, -1) . '|' . substr($g_return, 0, -1) . '|' . substr($g_approve, 0, -1);
    $xaxis = substr($xaxis, 0,-1);
    $html = str_replace('!data!', $data, $html);
    $html = str_replace('!ystart!', $y_start, $html);
    $html = str_replace('!yend!', $y_end, $html);
    $html = str_replace('!days!', $days_in_month, $html);
    $html = str_replace('!xaxis!', $xaxis, $html);

} else {

    $html = "<h2>Daily Report for " . date('F', mktime(0, 0, 0, $month)) . " $year</h2>";

    $html .= "<br><h4>Quality Assurance - In/Out</h4>";
    $html .= "<div style='border: 1px dotted gray; background-color: #F2F2F2; width: 810px; padding: 5px ' >";
    $html .= "<img style='border: 5px solid black' src=http://chart.apis.google.com/chart?chs=800x150&chd=t:!data!&cht=bvg&chxt=x,y&chxr=0,1,!days!,1|1,!ystart!,!yend!&chbh=a,10,5&chco=FFCD08,EBDA04,AD0808&chf=c,lg,90,B3B3B3,0,DCDCDC,0.5|bg,s,000000 />";

    $html .= "<table border=1 width=810px id=jTR>";
    $html .= "<th style=' width: 20px'>Day</th>";
    $html .= "<th style='background-color: #FFCD08'>QA New</th>";
    $html .= "<th style='background-color: #EBDA04'>QA Renewal</th>";
    $html .= "<th style='background-color: #AD0808'>QA Out</th>";
    $html .= "<th style='background-color: #2316F3'>NET</th>";

    //Prepare statement - Count QA new
    $statement = "SELECT COUNT(*) FROM nse_establishment WHERE enter_date=?";
    $sql_in_new = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Count QA renewal
    $statement = "SELECT COUNT(*) FROM nse_establishment_assessment WHERE assessment_date=? && assessment_status='complete' && assessment_type='renewal'";
    $sql_in_renew = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Count QA Out
    $statement = "SELECT COUNT(*) FROM nse_establishment_qa_cancelled WHERE cancelled_date=?";
    $sql_qa_out = $GLOBALS['dbCon']->prepare($statement);


    $days_in_month = date('t', mktime(0, 0, 0, $month));

    //Find max Y values
    for ($i=1; $i<=$days_in_month; $i++) {
        $date = "$year-$month-$i";

        if (mktime(0, 0, 0, $month, $i, $year) > mktime()) {

        } else {
            $sql_in_new->bind_param('s', $date);
            $sql_in_new->execute();
            $sql_in_new->bind_result($qa_in_new);
            $sql_in_new->fetch();
            $sql_in_new->free_result();

            if ($qa_in_new < $y_start) $y_start = $qa_in_new;
            if ($qa_in_new > $y_end) $y_end = $qa_in_new;

            $sql_in_renew->bind_param('s', $date);
            $sql_in_renew->execute();
            $sql_in_renew->bind_result($qa_in_renew);
            $sql_in_renew->fetch();
            $sql_in_renew->free_result();

            if ($qa_in_renew < $y_start) $y_start = $qa_in_renew;
            if ($qa_in_renew > $y_end) $y_end = $qa_in_renew;

            $sql_qa_out->bind_param('s', $date);
            $sql_qa_out->execute();
            $sql_qa_out->bind_result($qa_out);
            $sql_qa_out->fetch();
            $sql_qa_out->free_result();

            if ($qa_out < $y_start) $y_start = $qa_out;
            if ($qa_out > $y_end) $y_end = $qa_out;
        }
    }

    $y_end = $y_end + 1;
    $height = $y_end - $y_start;

    for ($i=1; $i<=$days_in_month; $i++) {
        $xaxis .= "$i,";
        $qa_in = '';
        $date = "$year-$month-$i";


        if (mktime(0, 0, 0, $month, $i, $year) > mktime()) {
            $qa_in_new = '-';
            $qa_in_renew = '-';
            $qa_out = '-';
            $g_in_new .= "0,";
            $g_in_renew .= "0,";
            $g_out .= "0,";
            $net = '-';
        } else {
            $sql_in_new->bind_param('s', $date);
            $sql_in_new->execute();
            $sql_in_new->bind_result($qa_in_new);
            $sql_in_new->fetch();
            $sql_in_new->free_result();
            $g_in_new .= $qa_in_new==0?'0,':($qa_in_new/$height) * 100 . ",";

            $sql_in_renew->bind_param('s', $date);
            $sql_in_renew->execute();
            $sql_in_renew->bind_result($qa_in_renew);
            $sql_in_renew->fetch();
            $sql_in_renew->free_result();
            $g_in_renew .= $qa_in_renew==0?'0,':($qa_in_renew/$height) * 100 . ",";

            $sql_qa_out->bind_param('s', $date);
            $sql_qa_out->execute();
            $sql_qa_out->bind_result($qa_out);
            $sql_qa_out->fetch();
            $sql_qa_out->free_result();
            $g_out .= $qa_out==0?'0,':($qa_out/$height) * 100 . ",";

            $net = $qa_in_new + $qa_in_renew - $qa_out;

            $qa_in_new_total += $qa_in_new;
            $qa_in_renew_total += $qa_in_renew;
            $qa_out_total += $qa_out;
            $net_total += $net;
        }

        $html .= "<tr>";
        $html .= "<td  class=jdbK>$i</td>";
        $html .= "<td  class=jdbY>$qa_in_new</td>";
        $html .= "<td  class=jdbO>$qa_in_renew</td>";
        $html .= "<td  class=jdbR>$qa_out</td>";
        $html .= "<td  class=jdbB>$net</td>";
        $html .= "</tr>";
    }

    $html .= "<tr style='border-top: double 4px gray; font-weight: bold'>";
    $html .= "<td>Totals</td>";
    $html .= "<td style='background-color: #F9E38C'>$qa_in_new_total</td>";
    $html .= "<td style='background-color: #F4ED97'>$qa_in_renew_total</td>";
    $html .= "<td style='background-color: #FAB6B6'>$qa_out_total</td>";
    $html .= "<td style='background-color: #C4C1FB'>$net_total</td>";
    $html .= "</tr>";
    $html .= "</table></div>";





    $html .= "<html>";

    //Replace Graph values
    $data = substr($g_in_new, 0, -1) . '|' . substr($g_in_renew, 0, -1) . '|' . substr($g_out, 0, -1);
    $xaxis = substr($xaxis, 0,-1);
    $html = str_replace('!data!', $data, $html);
    $html = str_replace('!ystart!', $y_start, $html);
    $html = str_replace('!yend!', $y_end, $html);
    $html = str_replace('!days!', $days_in_month, $html);
    $html = str_replace('!xaxis!', $xaxis, $html);
}


echo $html;
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/reports/daily/res.js></script>";
echo "<script>J_tr()</script>";
?>