<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include $SDR."/system/parse.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"]) && $_GET["jL2"]?$_GET["jL2"]:20000);
$d="";
$csv="";
$pr="";

$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","per","town","prov","cou","ema","tel","res","aasa","qcat","qact","qent","qren","qout","ass","rep");
$d="";$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}


$q="SELECT ";
$q.="DISTINCT e.establishment_code,e.establishment_name";
$q.=",e.postal_address_line1,e.postal_address_line2,e.postal_address_line3,e.postal_address_code";
$q.=",e.street_address_line1,e.street_address_line2,e.street_address_line3";
$q.=",e.assessor_id,e.rep_id1";
$q.=",e.aa_estab,e.active,e.aa_category_code";
$q.=",b.billing_line1,b.billing_line2,b.billing_line3,b.billing_code";
if(isset($s["cou"]))$q.=",c.country_name";
if(isset($s["prov"]))$q.=",p.province_name";
if(isset($s["town"]))$q.=",t.town_name";
$q.=" FROM nse_establishment AS e ";
$q.="LEFT JOIN nse_establishment_billing AS b ON e.establishment_code=b.establishment_code ";
if(isset($s["cou"]) || isset($s["prov"]) || isset($s["town"]))
{
	$q.="LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code ";
	if(isset($s["cou"]))
		$q.="LEFT JOIN nse_nlocations_countries AS c ON l.country_id=c.country_id ";
	if(isset($s["prov"]))
		$q.="LEFT JOIN nse_nlocations_provinces AS p ON l.province_id=p.province_id ";
	if(isset($s["town"]))
		$q.="LEFT JOIN nse_nlocations_towns AS t ON l.town_id=t.town_id ";
}
$q.="WHERE e.establishment_code!='' ";
if(isset($_GET["est"]))
	$q.="AND e.establishment_code='".$_GET["est"]."' ";
if(isset($_GET["cou"]))
	$q.="AND l.country_id='".$_GET["cou"]."' ";
if(isset($_GET["prov"]))
	$q.="AND l.province_id='".$_GET["prov"]."' ";
$jL0=mysql_num_rows(mysql_query($q));
$q.="ORDER BY e.establishment_name ";
$rs=mysql_query($q);
//echo $q."<hr>".mysql_error();
$csv_content="";
if(mysql_num_rows($rs))
{
	include $SDR."/custom/lib/est_items.php";
	$pp=array();

	while($g=mysql_fetch_array($rs))
	{
		if(isset($s["per"]) || isset($s["tel"]))
			$p=people($g["establishment_code"]);

		$csv_content.=",".str_replace(","," ",$g["establishment_name"]);
		if(isset($s["per"]))
			$csv_content.=",".(isset($pp[$p])?str_replace("<br>",". ",str_replace(",",";",$pp[$p])):"");
		if($g["postal_address_line1"])
		{
			$csv_content.=",".str_replace(",",";",trim($g["postal_address_line1"]));
			$csv_content.=",".str_replace(",",";",trim($g["postal_address_line2"]));
			$csv_content.=",".str_replace(",",";",trim($g["postal_address_line3"]));
			$csv_content.=",".str_replace(",",";",trim($g["postal_address_code"]));
		}
		else
		{
			$csv_content.=",".str_replace(",",";",trim($g["billing_line1"]));
			$csv_content.=",".str_replace(",",";",trim($g["billing_line2"]));
			$csv_content.=",".str_replace(",",";",trim($g["billing_line3"]));
			$csv_content.=",".str_replace(",",";",trim($g["billing_code"]));
		}
		$csv_content.=",".str_replace(",",";",trim($g["street_address_line1"]));
		$csv_content.=",".str_replace(",",";",trim($g["street_address_line2"]));
		$csv_content.=",".str_replace(",",";",trim($g["street_address_line3"]));
		if(isset($s["town"]))
			$csv_content.=",".($g["town_name"]?strtoupper(str_replace(","," ",$g["town_name"])):"");
		if(isset($s["prov"]))
			$csv_content.=",".($g["province_name"]?strtoupper(str_replace(","," ",$g["province_name"])):"");
		if(isset($s["cou"]))
			$csv_content.=",".($g["country_name"]?strtoupper(str_replace(","," ",$g["country_name"])):"");

		if(isset($s["ema"]) || isset($s["tel"]))
		{
			$c=reservation_details($g["establishment_code"]);
			if(isset($s["ema"]))
				$csv_content.=",".(isset($c["email"])?str_replace(","," ",$c["email"]):"");
			if(isset($s["tel"]))
				$csv_content.=",".str_replace(",",";",numbers($g["establishment_code"],$p,isset($c["tel1"])?$c["tel1"]:"",isset($c["cell1"])?$c["cell1"]:""));
		}

		if(isset($s["res"]))
			$csv_content.=",".str_replace(",",";",res_type($g["establishment_code"]));
		if(isset($s["aasa"]))
			$csv_content.=",".($g["aa_estab"]?"Y":"");
		if(isset($s["qact"]))
			$csv_content.=",".($g["active"]?"Y":"");
		if(isset($s["qcat"]))
			$csv_content.=",".J_Value("aa_category_name","nse_aa_category","aa_category_code",$g["aa_category_code"]);
		if(isset($s["qent"]))
			$csv_content.=",".str_replace(",",";",entered($g["establishment_code"]));
		if(isset($s["qren"]))
		{
			$v=renew($g["establishment_code"]);
			$csv_content.=",".str_replace(",",";",$v[0]);
		}
		if(isset($s["qout"]))
			$csv_content.=",".str_replace(",",";",qa_out($g["establishment_code"]));


		if(isset($s["ass"]))
			$csv_content.=",".str_replace(",",";",J_Value("","people","",$g["assessor_id"]));
		if(isset($s["rep"]))
			$csv_content.=",".str_replace(",",";",J_Value("","people","",$g["rep_id1"]));

		$csv_content.="\n";
	}
}

$hed=",ESTABLISHMENT";
if(isset($s["per"]))
	$hed.=",CONTACT";
$hed.=",POSTAL 1";
$hed.=",POSTAL 2";
$hed.=",POSTAL 3";
$hed.=",CODE";
$hed.=",STREET 1";
$hed.=",STREET 2";
$hed.=",STREET 3";
if(isset($s["town"]))
	$hed.=",TOWN";
if(isset($s["prov"]))
	$hed.=",PROVINCE";
if(isset($s["cou"]))
	$hed.=",COUNTRY";
if(isset($s["ema"]))
	$hed.=",EMAIL";
if(isset($s["tel"]))
	$hed.=",TEL";
if(isset($s["res"]))
	$hed.=",RES TYPE";
if(isset($s["aasa"]))
	$hed.=",AA SA";
if(isset($s["qact"]))
	$hed.=",QA ACTIVE";
if(isset($s["qcat"]))
	$hed.=",QA CATERGORY";
if(isset($s["qent"]))
	$hed.=",QA ENTERED";
if(isset($s["qren"]))
	$hed.=",QA RENEW";
if(isset($s["qout"]))
	$hed.=",QA OUT";
if(isset($s["ass"]))
	$hed.=",ASSESSOR";
if(isset($s["rep"]))
	$hed.=",REP";
$hed.="\n";

$csv_content=$hed.$csv_content;

$csv_name="Labels_".date("Y_m_d",$EPOCH).".csv";
include $SDR."/utility/CSV/create.php";
?>