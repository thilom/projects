<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

if(isset($_GET["csv"]))
{
	include $SDR."/utility/force_download.php";
	die();
}

include $SDR."/system/get.php";
include $SDR."/system/parse.php";

//$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
//$jL2=(isset($_GET["jL2"])?($_GET["jL2"]>1000&&!isset($_GET["csv"])?2000:$_GET["jL2"]):100);

set_time_limit(0);

$q="SELECT DISTINCT e.establishment_code";
$q.=",e.establishment_name";
$q.=",e.rep_id1";
$q.=",e.rep1_date";
$q.=",datediff(NOW(),e.rep1_date) AS rep1date";
$q.=",e.rep_id2";
$q.=",e.assessor_id";
$q.=",e.aa_category_code";
$q.=",e.website_url";
$q.=",e.nightsbridge_bbid";
$q.=",e.aa_estab";
$q.=",e.aa_zim";
$q.=",e.etravel";
$q.=",e.accommodation_directory";
$q.=",e.last_updated";
$q.=",e.active";
$q.=",c.country_id";
$q.=",c.country_name";
$q.=",p.province_id";
$q.=",p.province_name";
$q.=",t.town_id";
$q.=",t.town_name";
$q.=",s.suburb_id";
$q.=",s.suburb_name";
$q.=",d.room_count";
$q.=" FROM nse_establishment AS e ";
$q.="LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code ";
$q.="LEFT JOIN nse_nlocations_countries AS c ON c.country_id=l.country_id ";
$q.="LEFT JOIN nse_nlocations_provinces AS p ON p.province_id=l.province_id ";
$q.="LEFT JOIN nse_nlocations_towns AS t ON t.town_id=l.town_id ";
$q.="LEFT JOIN nse_nlocations_suburbs AS s ON s.suburb_id=l.suburb_id ";
$q.="LEFT JOIN nse_establishment_data AS d ON e.establishment_code=d.establishment_code ";
$q.="WHERE ";
if(isset($_GET["est"]))
	$q.="e.establishment_code='".$_GET["est"]."' ";
else
	$q.="e.establishment_code!='' ";
if(isset($_GET["cou"]))
{
	if($_GET["cou"]==-1)
		$q.="AND (c.country_id=1 OR c.country_id=2 OR c.country_id=3 OR c.country_id=4 OR c.country_id=5 OR c.country_id=6 OR c.country_id=7 OR c.country_id=8 OR c.country_id=9 OR c.country_id=10 OR c.country_id=11 OR c.country_id=12 OR c.country_id=13 OR c.country_id=14 OR c.country_id=16 OR c.country_id=17) ";
	else
		$q.="AND c.country_id='".$_GET["cou"]."' ";
}
if(isset($_GET["pro"]))
	$q.="AND p.province_id='".$_GET["pro"]."' ";
if(isset($_GET["twn"]))
	$q.="AND t.town_id='".$_GET["twn"]."' ";
if(isset($_GET["rep"]))
	$q.="AND (e.rep_id1='".$_GET["rep"]."' OR e.rep_id2='".$_GET["rep"]."') ";
$jL0=mysql_num_rows(mysql_query($q));
$q.="ORDER BY e.establishment_name ";
$rs=mysql_query($q);
if(isset($_GET["csv"]))
{
	$hed="";
	$tot="";
	$i=0;
	$c=0;
}
$r="";
$pr="";
$c="";
$pc="";
$t="";
$s="";
$rg="";
$pubs=array();
$pub_names="";
$pub_string="";

if(mysql_num_rows($rs))
{
	$star=0;
	$pp=array("");
	$cc=array("");
	$pv=array("");
	$tt=array("");
	$ss=array("");
	$rrg=array("");

	$q="SELECT publication_id,publication_name,publication_year FROM nse_publications WHERE expiry_date>'2010-10-30' ORDER BY publication_year,expiry_date";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
		{
			$pub[]=$g["publication_id"];
			$pub_names[$g["publication_id"]]=$g["publication_year"]." - ".$g["publication_name"];
			$pub_string.=$g["publication_year"]." - ".$g["publication_name"]."~";
		}
	}

	include $SDR."/custom/lib/est_items.php";

	function curr_ads($e,$p=0)
	{
		$v="";
		$q="SELECT j_in_name FROM nse_ads
		JOIN nse_inventory ON inventory_id=j_in_id
		JOIN nse_inventory_publications ON j_inp_inventory=inventory_id
		WHERE
		establishment_code='".$e."'
		AND j_inp_publication=".$p."
		LIMIT 1";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			$g=mysql_fetch_array($q);
			$v=substr($g["j_in_name"],strrpos($g["j_in_name"],"-")+2);
		}
		return $v;
	}

	function curr_adprice($e,$p=0)
	{
		$v=0;
		$q="SELECT j_invit_total FROM nse_invoice_item
		JOIN nse_invoice ON j_invit_invoice=j_inv_id
		JOIN nse_inventory_publications ON j_inp_inventory=j_invit_inventory
		WHERE
		j_inv_to_company='".$e."'
		AND j_inv_type=2
		AND j_inp_publication=".$p;
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
				$v+=$g["j_invit_total"];
		}
		return $v?number_format($v,2,"."," "):"";
	}

	function landmarks($e)
	{
		return mysql_num_rows(mysql_query("SELECT establishment_code FROM nse_establishment_landmarks WHERE establishment_code='".$e."'"));
	}

	$p=0;
	$i=0;


	$file="REP_REPORT_".date("d-m-Y",$EPOCH).".csv";
	@unlink($SDR."/stuff/temp/".$file);
	include_once $SDR."/system/dir.php";

	$i=0;
	while(!$i && $g=mysql_fetch_array($rs))
	{
		$f=",COUNTRY";
		$f.=",PROVINCE";
		$f.=",REGION";
		$f.=",SUB REGION";
		$f.=",TOWN";
		$f.=",SUBURB";
		$f.=",CODE";
		$f.=",ESTABLISHMENT";
		$f.=",AA SA";
		$f.=",AA ZIM";
		$f.=",E-TRAVEL";
		$f.=",REP";
		$f.=",REP 1";
		$f.=",REP DAYS";
		$f.=",REP 2";
		$f.=",ASSESSOR";
		$f.=",NIGHTSBRIDGE";
		$f.=",ACCOMMODATION DIRECTORY";
		$f.=",SC25HL26,SC26HL27,SC27HL28,SC28HL29,SC29HL10,SC10HL11";
		foreach($pub as $k => $v)
			$f.=",".strtoupper($pub_names[$v]);
		$f.=",PDINT_PRIC,PDLOC_PRIC";
		foreach($pub as $k => $v)
			$f.=",".strtoupper($pub_names[$v])." - PRICE";
		$f.=",ROOMS";
		$f.=",RES TYPE";
		$f.=",STARS";
		$f.=",QA-GRADE SA FIXED";
		$f.=",QACAT";
		$f.=",QA-CATEGORY";
		$f.=",QA-ENTERED";
		$f.=",QA-RENEW";
		$f.=",QA-OUT";
		$f.=",ACTIVE";
		$f.=",QINSPCODE";
		$f.=",LAST VISIT";
		$f.=",LAST INVOICE";
		$f.=",LAST PAID";
		$f.=",AWARDS";
		$f.=",CONTACT";
		$f.=",TEL";
		$f.=",CELL";
		$f.=",EMAIL";
		$f.=",WEB";
		$f.=",UPDATED";
		$f.="\n";
		$i++;
	}

	J_PrepFile("/stuff/temp",$file,$f);
	$i=0;

	while($g=mysql_fetch_array($rs))
	{

		$db=array();
		$qdb=mysql_query("SELECT * FROM nse_establishment_dbase WHERE establishment_code='".$g["establishment_code"]."' LIMIT 1");
		if(mysql_num_rows($qdb))
			$db=mysql_fetch_array($qdb);
		$db["PDINT_PRIC"]=(isset($db["PDINT_PRIC"])?(strpos($db["PDINT_PRIC"],"/")!==false?"":($db["PDINT_PRIC"]*1>0?number_format($db["PDINT_PRIC"]*1,2,"."," "):"")):"");
		$db["PDLOC_PRIC"]=(isset($db["PDLOC_PRIC"])?(strpos($db["PDLOC_PRIC"],"/")!==false?"":($db["PDLOC_PRIC"]*1>0?number_format($db["PDLOC_PRIC"]*1,2,"."," "):"")):"");

		$regs=region($g["town_id"],1);
		$qaout=qa_out($g["establishment_code"]);
		$status=qa_status($g["establishment_code"]);
		$entered=entered($g["establishment_code"]);
		$rnw=renew($g["establishment_code"]);
		$restype=res_type($g["establishment_code"]);
		$visited=visit($g["establishment_code"]);
		$award=awards($g["establishment_code"]);
		$person=people($g["establishment_code"]);
		$pubNum=reservation_details($g["establishment_code"]);
		$number1=numbers($g["establishment_code"],$person,$pubNum["tel"],$pubNum["cell"]);
		$number2=numbers($g["establishment_code"],$person,$pubNum["cell"],$pubNum["cell"]);

		$f=",".str_replace(",",";",strtoupper($g["country_name"]));
		$f.=",".str_replace(",",";",strtoupper($g["province_name"]));
		$f.=",".str_replace(",",";",strtoupper($regs[0]));
		$f.=",".str_replace(",",";",strtoupper($regs[1]));
		$f.=",".str_replace(",",";",strtoupper($g["town_name"]));
		$f.=",".str_replace(",",";",strtoupper($g["suburb_name"]));
		$f.=",".str_replace(",",";",strtoupper($g["establishment_code"]));
		$f.=",".str_replace(",",";",strtoupper($g["establishment_name"]));
		$f.=",".($g["aa_estab"]?"Y":"");
		$f.=",".($g["aa_zim"]?"Y":"");
		$f.=",".$g["etravel"];
		$f.=",".(isset($db["REP"])?$db["REP"]:"");
		$f.=",".str_replace(",",";",J_Value("","people","",$g["rep_id1"]));
		$f.=",".$g["rep1date"];
		$f.=",".str_replace(",",";",J_Value("","people","",$g["rep_id2"]));
		$f.=",".str_replace(",",";",J_Value("","people","",$g["assessor_id"]));
		$f.=",".str_replace(",",";",$g["nightsbridge_bbid"]);
		$f.=",".$g["accommodation_directory"];
		$f.=",".(isset($db["SC25HL26"])?$db["SC25HL26"]:"");
		$f.=",".(isset($db["SC26HL27"])?$db["SC26HL27"]:"");
		$f.=",".(isset($db["SC27HL28"])?$db["SC27HL28"]:"");
		$f.=",".(isset($db["SC28HL29"])?$db["SC28HL29"]:"");
		$f.=",".(isset($db["SC29HL10"])?$db["SC29HL10"]:"");
		$f.=",".(isset($db["SC10HL11"])?$db["SC10HL11"]:"");
		foreach($pub as $k => $v)
			$f.=",".curr_ads($g["establishment_code"],$v);
		$f.=",".$db["PDINT_PRIC"];
		$f.=",".$db["PDLOC_PRIC"];
		foreach($pub as $k => $v)
			$f.=",".curr_adprice($g["establishment_code"],$v);
		$f.=",".($g["room_count"]>0?$g["room_count"]:"");
		$f.=",".str_replace(",",";",$restype);
		$f.=",".($star?$star:"");
		$f.=",".$status;
		$f.=",".(isset($db["QACAT"])?$db["QACAT"]:"");
		$f.=",".($g["aa_category_code"]?$g["aa_category_code"]:"");
		$f.=",".str_replace(",",";",$entered);
		$f.=",".str_replace(",",";",$rnw[0]);
		$f.=",".str_replace(",",";",$qaout);
		$f.=",".($g["active"]?"Y":"N");
		$f.=",".(isset($db["QINSPCODE"])?$db["QINSPCODE"]:"");
		$f.=",".$visited[0];
		$f.=",".$visited[1];
		$f.=",".$visited[2];
		$f.=",".$award;
		$f.=",".(isset($pp[$person])?str_replace(",",";",$pp[$person]):"");
		$f.=",".str_replace(",",";",$number1);
		$f.=",".str_replace(",",";",$number2);
		$f.=",".str_replace(",",";",$pubNum["email"]);
		$f.=",".str_replace(",",";",$g["website_url"]);
		$f.=",".($g["last_updated"]?str_replace("-","/",substr($g["last_updated"],0,10)):"");
		$f.="\n";

		$a=fopen($SDR."/stuff/temp/".$file,"a+");
		fwrite($a,$f);
		fclose($a);
		$f="";

		if($i<500)
		{
			$r.=$i."~";
			$r.=$g["country_id"]."~";
			if(!isset($cc[$g["country_id"]]) && $g["country_name"])
				$cc[$g["country_id"]]=strtoupper($g["country_name"]);
			$r.=$g["province_id"]."~";
			if(!isset($pv[$g["province_id"]]) && $g["province_name"])
				$pv[$g["province_id"]]=strtoupper($g["province_name"]);
			$r.=$regs[0]."~";
			$r.=$regs[1]."~";
			$r.=$g["town_id"]."~";
			if(!isset($tt[$g["town_id"]]) && $g["town_name"])
				$tt[$g["town_id"]]=strtoupper($g["town_name"]);
			$r.=$g["suburb_id"]."~";
			if(!isset($ss[$g["suburb_id"]]) && $g["suburb_name"])
				$ss[$g["suburb_id"]]=strtoupper($g["suburb_name"]);
			$r.=$g["establishment_code"]."~";
			$r.=trim($g["establishment_name"])."~";
			$r.=($g["aa_estab"]?"Y":"")."~";
			$r.=($g["aa_zim"]?"Y":"")."~";
			$r.=$g["etravel"]."~";
			$r.=(isset($db["REP"])?$db["REP"]:"")."~";
			$r.=($g["rep_id1"]?$g["rep_id1"]:"")."~";
			$r.=($g["rep1date"]?$g["rep1date"]:"")."~";
			$r.=($g["rep_id2"]?$g["rep_id2"]:"")."~";
			$r.=($g["assessor_id"]?$g["assessor_id"]:"")."~";
			$r.=($g["nightsbridge_bbid"]?$g["nightsbridge_bbid"]:"")."~";
			$r.=$g["accommodation_directory"]."~";
			$r.=(isset($db["SC25HL26"])?$db["SC25HL26"]:"")."~";
			$r.=(isset($db["SC26HL27"])?$db["SC26HL27"]:"")."~";
			$r.=(isset($db["SC27HL28"])?$db["SC27HL28"]:"")."~";
			$r.=(isset($db["SC28HL29"])?$db["SC28HL29"]:"")."~";
			$r.=(isset($db["SC29HL10"])?$db["SC29HL10"]:"")."~";
			$r.=(isset($db["SC10HL11"])?$db["SC10HL11"]:"")."~";
			foreach($pub as $k => $v)
				$r.=curr_ads($g["establishment_code"],$v)."~";
			$r.=$db["PDINT_PRIC"]."~";
			$r.=$db["PDLOC_PRIC"]."~";
			foreach($pub as $k => $v)
				$r.=curr_adprice($g["establishment_code"],$v)."~";
			$r.=($g["room_count"]>0?$g["room_count"]:"")."~";
			$r.=$restype."~";
			$r.=($star?$star:"")."~";
			$r.=$status."~";
			$r.=(isset($db["QACAT"])?$db["QACAT"]:"")."~";
			$r.=($g["aa_category_code"]?$g["aa_category_code"]:"")."~";
			$r.=$entered."~";
			$r.=$rnw[0]."~";
			$r.=$qaout."~";
			$r.=($g["active"]?"Y":"N")."~";
			$r.=(isset($db["QINSPCODE"])?$db["QINSPCODE"]:"")."~";
			$r.=$visited[0]."~";
			$r.=$visited[1]."~";
			$r.=$visited[2]."~";
			$r.=$award."~";
			$r.=$person."~";
			$r.=$number1."~";
			$r.=$number2."~";
			$r.=$pubNum["email"]."~";
			$r.=$g["website_url"]."~";
			$r.=($g["last_updated"]?str_replace("-","/",substr($g["last_updated"],0,10)):"")."~";
			$r.="|";
			if(!isset($pp[$g["rep_id1"]]))
				$pp[$g["rep_id1"]]=J_Value("","people","",$g["rep_id1"]);
			if(!isset($pp[$g["rep_id2"]]))
				$pp[$g["rep_id2"]]=J_Value("","people","",$g["rep_id2"]);
			if(!isset($pp[$g["assessor_id"]]))
				$pp[$g["assessor_id"]]=J_Value("","people","",$g["assessor_id"]);
			$i++;
		}
	}

	$r=str_replace("\r","",$r);
	$r=str_replace("\n","",$r);
	$r=str_replace("\"","",$r);
	$r=str_replace("~|","|",$r);
	foreach($cc as $k => $v)
		$c.=$k."~".$v."|";
	foreach($pv as $k => $v)
		$pc.=$k."~".$v."|";
	foreach($rrg as $k => $v)
		$rg.=$k."~".$v."|";
	foreach($tt as $k => $v)
		$t.=$k."~".$v."|";
	foreach($ss as $k => $v)
		$s.=$k."~".$v."|";
	foreach($pp as $k => $v)
		$pr.=$k."~".$v."|";
}


echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/reports/reps/rep_res.js></script>";
echo "<script>J_in_r(\"".$r."\",\"".$c."\",\"".$pc."\",\"".$rg."\",\"".$t."\",\"".$s."\",\"".$pr."\",\"".trim($pub_string,"~")."\",".$i.",'".$ROOT."/stuff/temp/".$file."')</script>";
?>