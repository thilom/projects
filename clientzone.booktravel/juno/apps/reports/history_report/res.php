<?php

//Includes
require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";

//Vars
$d = "X~Code~Establishment~Assessor~Rep~Country~Province~Town~Suburb~";
$establishments = array();
$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 100);
$r = '';
$award_order = array();

//Get list of publications for headers
$d .= "C25HL26~SC26HL27~SC27HL28~SC28HL29~SC29HL10~SC10HL11~";
$statement = "SELECT publication_id, publication_name, publication_year
			FROM nse_publications
			ORDER BY publication_year";
$sql_publications = $GLOBALS['dbCon']->prepare($statement);
$sql_publications->execute();
$sql_publications->bind_result($publication_id, $publication_name, $publication_year);
while ($sql_publications->fetch()) {
	$d .= str_replace(',','',"$publication_name($publication_year)~");
	$publication_order[] = $publication_id;
}

//Get list of awards for headers
$statement = "SELECT DISTINCT year
			FROM award
			ORDER BY year ";
$sql_awards = $GLOBALS['dbCon']->prepare($statement);
$sql_awards->execute();
$sql_awards->bind_result($award_year);
while ($sql_awards->fetch()) {
	$d .= "Awards($award_year)~";
	$award_order[] = $award_year;
}
$sql_awards->close();

//Get QA Awards years
$d .= "QA Enter Date~";
$statement = "SELECT DISTINCT DATE_FORMAT(assessment_date, '%Y') AS d
			FROM nse_establishment_assessment
			ORDER BY d";
$sql_qa = $GLOBALS['dbCon']->prepare($statement);
$sql_qa->execute();
$sql_qa->bind_result($qa_year);
while ($sql_qa->fetch()) {
	if (empty($qa_year) || $qa_year == '0000') continue;
	$d .= "QA($qa_year) - In~";
	$d .= "QA($qa_year) - Status~";
	$d .= "QA($qa_year) - Out~";
	$d .= "QA($qa_year) - Endorsement~";
	$qa_order[] = $qa_year;
}
$sql_qa->close();

$d .=  "Nightsbridge~Contact Name~Contact Tel~Contact Cell~Contact Email~";
$d .= "Reservation Tel~Reservation Cell~Reservation Email~";


// Display stuff
$dd = explode("~", trim($d, "~"));
$t = $dd;
$ddd = array();
$s = array();
foreach ($dd as $k => $v) {
	if (empty($v))
		continue;
	$s[$v] = 1;
	$ddd[$v] = 1;
}
$da = "";
$n = 0;
foreach ($t as $k => $v) {
	if (isset($ddd[$v])) {
		$da.=$n;
		$n++;
	}
	$da.="~";
}

//Collect Data
$statement = "SELECT a.establishment_code
			FROM nse_establishment AS a
			LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code";
if (isset($_GET['est']) && $_GET['est'] != '0') {
	$statement .= " WHERE a.establishment_code='{$_GET['est']}'";
} else {
	if (isset($_GET['suburb']) && !empty($_GET['suburb'])) {
		$statement .= strpos($statement, 'WHERE') === false?' WHERE':' AND';
		$statement .= " b.suburb_id={$_GET['suburb']}";
	} else if (isset($_GET['town']) && !empty($_GET['town'])) {
		$statement .= strpos($statement, 'WHERE') === false?' WHERE':' AND';
		$statement .= " b.town_id={$_GET['town']}";
	} else if (isset($_GET['province']) && !empty($_GET['province'])) {
		$statement .= strpos($statement, 'WHERE') === false?' WHERE':' AND';
		$statement .= " b.province_id={$_GET['province']}";
	} else if (isset($_GET['country']) && $_GET['country'] != 0) {
		$statement .= strpos($statement, 'WHERE') === false?' WHERE':' AND';
		$statement .= " b.country_id={$_GET['country']}";
	}
}
$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
$sql_establishments->execute();
$sql_establishments->bind_result($establishment_code);
while ($sql_establishments->fetch()) {
	$establishments[] = $establishment_code;
}
$sql_establishments->close();

foreach ($establishments as $key=>$establishment_code) {
	$last_assessment_data = get_last_assessment($establishment_code);
	$qa_out = get_qa_out($establishment_code);

	//Check if estalishment was assessed
	if (!isset($_GET['est']) || $_GET['est'] == 0) {
		if (isset($_GET['status']) && $_GET['status'] == 1) {
			if (!empty($last_assessment_data['last_assessment']) && empty($qa_out)) unset($establishments[$key]);
		} else if (isset($_GET['status']) && $_GET['status'] == 0) {
			if (empty($last_assessment_data['last_assessment'])) unset($establishments[$key]);
			if (!empty($qa_out)) unset($establishments[$key]);
		} else {

		}
	}

}

//Prepare statement - Establishment name
$statement = "SELECT a.establishment_name, c.country_name, d.province_name, e.town_name, f.suburb_name,
			IF (a.enter_date='0000-00-00','',DATE_FORMAT(a.enter_date,'%d-%m-%Y')), a.nightsbridge_bbid,
			CONCAT(g.firstname, ' ', g.lastname), CONCAT(h.firstname, ' ', h.lastname)
			FROM nse_establishment AS a
			LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
			LEFT JOIN nse_nlocations_countries AS c ON b.country_id=c.country_id
			LEFT JOIN nse_nlocations_provinces AS d ON b.province_id=d.province_id
			LEFT JOIN nse_nlocations_towns AS e ON b.town_id=e.town_id
			LEFT JOIN nse_nlocations_suburbs AS f ON b.suburb_id=f.suburb_id
			LEFT JOIN nse_user AS g ON a.assessor_id=g.user_id
			LEFT JOIN nse_user AS h ON a.rep_id1=h.user_id
			WHERE a.establishment_code=?
			LIMIT 1";
$sql_establishment = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - DB Publications
$statement = "SELECT SC25HL26,SC26HL27,SC27HL28,SC28HL29,SC29HL10,SC10HL11
			FROM nse_establishment_dbase
			WHERE establishment_code=?
			LIMIT 1";
$sql_dbase = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - publications
$statement = "SELECT b.j_in_name
			FROM nse_invoice_item AS a
			LEFT JOIN nse_inventory AS b ON a.j_invit_inventory = b.j_in_id
			LEFT JOIN nse_inventory_publications AS c ON b.j_in_id = c.j_inp_inventory
			LEFT JOIN nse_invoice AS d ON d.j_inv_id = a.j_invit_invoice
			WHERE c.j_inp_publication=? AND d.j_inv_to_company=?";
$sql_publications2 = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Awards
$statement = "SELECT a.status, a.category
			FROM award AS a
			WHERE a.code=? AND a.year=?
			LIMIT 1";
$sql_awards2 = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - QA new/renewals
$statement = "SELECT DATE_FORMAT(assessment_date,'%d-%m-%Y'), qa_status, assessment_status
			FROM nse_establishment_assessment
			WHERE establishment_code=? AND DATE_FORMAT(assessment_date,'%Y')=?
			LIMIT 1";
$sql_qa2 = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - QA Out
$statement = "SELECT cancelled_date
			FROM nse_establishment_qa_cancelled
			WHERE establishment_code=? AND DATE_FORMAT(cancelled_date,'%Y')=?
			ORDER BY cancelled_date DESC LIMIT 1";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement = Contact Information
$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), b.phone, b.cell, b.email
				FROM nse_user_establishments AS a
				LEFT JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code=?
				LIMIT 1";
$sql_contact = $GLOBALS['dbCon']->prepare($statement);


//Prepare statement - Reservation
$statement = "SELECT contact_type, contact_value, contact_description
			FROM nse_establishment_public_contact
			WHERE establishment_code=?";
$sql_reservation = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Alternate contact
$statement = "SELECT contact_tel, contact_cell, contact_email
			FROM nse_establishment_contact
			WHERE establishment_code=?
			LIMIT 1";
$sql_contact2 = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

if ($jL1 == 0) {
	 $fh = fopen($SDR."/apps/reports/files/history_report_{$_SESSION['j_user']['id']}.csv", 'w');
	fputs($fh, str_replace('~',',',$d) . PHP_EOL);
}

$counter = 0;
foreach ($establishments as $establishment_code) {
	$rr = '';
	$establishment_name = '';
	$country = '';
	$province = '';
	$town = '';
	$suburb = '';
	$SC25HL26 = '';
	$SC26HL27 = '';
	$SC27HL28 = '';
	$SC28HL29 = '';
	$SC29HL10 = '';
	$SC10HL11 = '';
	$fullname = '';
	$phone = '';
	$cell = '';
	$email = '';
	$qa_enter = '';
	$nightsbridge = '';
	$contact_type ='';
	$contact_value = '';
	$contact_description ='';
	$reservation_tel = '';
	$reservation_cell = '';
	$reservation_email = '';
	$contact_tel = '';
	$contact_cell = '';
	$contact_email = '';


	$sql_establishment->bind_param('s', $establishment_code);
	$sql_establishment->execute();
	$sql_establishment->bind_result($establishment_name, $country, $province, $town, $suburb, $qa_enter, $nightsbridge, $assessor, $rep);
	$sql_establishment->fetch();
	$sql_establishment->free_result();

	$sql_dbase->bind_param('s', $establishment_code);
	$sql_dbase->execute();
	$sql_dbase->bind_result($SC25HL26,$SC26HL27,$SC27HL28,$SC28HL29,$SC29HL10,$SC10HL11);
	$sql_dbase->fetch();
	$sql_dbase->free_result();

	$rr .= "$counter~";
	$rr .= "$establishment_code~";
	$rr .= str_replace(array(',',"'",'"',"\r\n","\r","\n"),'',$establishment_name) ."~";
	$rr .= "$assessor~";
	$rr .= "$rep~";
	$rr .= "$country~";
	$rr .= "$province~";
	$rr .= str_replace(array(',',"'",'"',"\r\n","\r","\n"),'',$town) ."~";
	$rr .= "$suburb~";
	$rr .= "$SC25HL26~";
	$rr .= "$SC26HL27~";
	$rr .= "$SC27HL28~";
	$rr .= "$SC28HL29~";
	$rr .= "$SC29HL10~";
	$rr .= "$SC10HL11~";

	foreach ($publication_order as $key=>$publication_id) {
		$ad = '';
		$sql_publications2->bind_param('ss', $publication_id, $establishment_code);
		$sql_publications2->execute();
		$sql_publications2->bind_result($ad);
		$sql_publications2->fetch();
		$sql_publications2->free_result();
		$ad = substr($ad, strpos($ad, '-')+1);
		$rr .= "$ad~";
	}

	foreach ($award_order as $key=>$award_year) {
		$award_status = '';
		$award_category = '';
		$sql_awards2->bind_param('ss', $establishment_code, $award_year);
		$sql_awards2->execute();
		$sql_awards2->bind_result($award_status, $award_category);
		$sql_awards2->fetch();
		$sql_awards2->free_result();
		$award_data = trim("$award_status-$award_category~");
		$rr .= $award_data;
	}

	$rr .= "$qa_enter~";
	foreach ($qa_order as $key=>$qa_year) {
		$assessment_date = '';
		$assessment_endorsement = '';
		$assessment_status = '';
		$qa_out = '';

		$sql_qa2->bind_param('ss', $establishment_code, $qa_year);
		$sql_qa2->execute();
		$sql_qa2->bind_result($assessment_date, $assessment_endorsement, $assessment_status);
		$sql_qa2->fetch();
		$sql_qa2->free_result();

		$sql_qa_out->bind_param('ss', $establishment_code, $qa_year);
		$sql_qa_out->execute();
		$sql_qa_out->bind_result($qa_out);
		$sql_qa_out->fetch();
		$sql_qa_out->free_result();

		$rr .= "$assessment_date~$assessment_status~$qa_out~$assessment_endorsement~";
	}

	$sql_contact->bind_param('s', $establishment_code);
	$sql_contact->execute();
	$sql_contact->store_result();
	$sql_contact->bind_result( $fullname, $phone, $cell, $email);
	$sql_contact->fetch();
	$sql_contact->free_result();
	$rr .= "$nightsbridge~$fullname~$phone~$cell~$email~";

	//Reservation details
	$sql_reservation->bind_param('s', $establishment_code);
	$sql_reservation->execute();
	$sql_reservation->bind_result($contact_type, $contact_value, $contact_description);
	while ($sql_reservation->fetch()) {
		switch ($contact_type) {
			case 'tel1':
				$reservation_tel = $contact_value;
				break;
			case 'cell1':
				$reservation_cell = $contact_value;
				if (!empty($contact_description)) $reservation_tel .=  "($contact_description)";
				break;
			case 'email':
				$reservation_email = $contact_value;
				break;
		}
	}
	$sql_reservation->free_result();

	//Alternat Contact
	if (empty($reservation_tel) && empty($reservation_email)) {
		$sql_contact2->bind_param('s', $establishment_code);
		$sql_contact2->execute();
		$sql_contact2->bind_result($contact_tel, $contact_cell, $contact_email);
		$sql_contact2->fetch();
		$sql_contact2->free_result();
		$reservation_tel = $contact_tel;
		$reservation_cell = $contact_cell;
		$reservation_email = $contact_email;
	}

	$rr .= "$reservation_tel~$reservation_cell~$reservation_email~";

	$rr = substr($rr, 0 ,-1);
	if ($counter >= $jL1 && $counter <= $jL1+$jL2) {
		$r .= $rr . "|";
	}

	$rr = str_replace('~-~','~~', $rr);
	$rr = str_replace('~-~','~~', $rr);
	$rr = str_replace(array(',',"'",'"',"\r\n","\r","\n"),'',$rr) ."~";
	if ($jL1 == 0) fputs($fh, str_replace('~',',',$rr) . PHP_EOL);

	if ($jL1 > 0 && $counter > $jL1+$jL2)  break;
	$counter++;

}



//Vars
$estab_count = count($establishments);
//$r = $d
$d = substr($d, 0, -1);
$da = substr($da, 0, -1);
$r = str_replace(array(',',"'",'"',"\r\n","\r","\n"),'',$r) ."~";
$r = substr($r, 0 ,-1);

$file = "/juno/apps/reports/files/history_report_{$_SESSION['j_user']['id']}.csv";

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/jsscripts/list_item_remover.js></script>";
echo "<script src=" . $ROOT . "/apps/reports/history_report/res.js></script>";
echo "<script>J_in_r(\"$r\",\"$da\",$estab_count,$jL1,$jL2,\"$d\",'$file')</script>";
if ($jL1 == 0)  fclose($fh);

/*
 * Get the next assessment date for an establishment
 * Looks for the latest complete assessment and then fetches the next renewal date
 *
 */
function get_last_assessment($establishment_code) {
	$data['next_assessment'] = '';
	$data['last_assessment'] = '';

	$statement = "SELECT DATE_FORMAT(renewal_date, '%Y-%m-%d'), DATE_FORMAT(assessment_date, '%Y-%m-%d')
					FROM nse_establishment_assessment
					WHERE establishment_code=? AND assessment_status='complete'
					ORDER BY assessment_date DESC
					LIMIT 1";
	$sql_next = $GLOBALS['dbCon']->prepare($statement);
	$sql_next->bind_param('s', $establishment_code);
	$sql_next->execute();
	$sql_next->bind_result($next_assessment, $last_assessment);
	$sql_next->fetch();
	$sql_next->close();

	$data['next_assessment'] = $next_assessment;
	$data['last_assessment'] = $last_assessment;
	return $data;
}

/**
 * Get last QA Out date if available
 * @param string $establishment_code
 */
function get_qa_out($establishment_code) {
	$out_date = '';
	$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=? ORDER BY cancelled_date DESC LIMIT 1";
	$sql_qa = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa->bind_param('s', $establishment_code);
	$sql_qa->execute();
	$sql_qa->bind_result($out_date);
	$sql_qa->fetch();
	$sql_qa->free_result();
	$sql_qa->close();

	if ($out_date == '0000-00-00') $out_date = '';

	return $out_date;
}



?>
