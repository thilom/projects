<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$d="";
$line_counter = -1;
$file_counter = 1;
$files = '';

// for display
$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
    if(empty($v))continue;
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","code","name","sta","cou","pro","twn","sub","str","pos","ema","cel","tel","con");
$d="";
$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}

//Vars
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);

$r="";
$q="SELECT * FROM nse_establishment AS e";
$q.=" LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code";
if(isset($_GET["sta"]) || isset($s["sta"]))
	$q.=" LEFT JOIN nse_aa_category AS ac ON e.aa_category_code=ac.aa_category_code";
if(isset($_GET["cou"]) || isset($s["cou"]))
	$q.=" LEFT JOIN nse_nlocations_countries AS c ON l.country_id=c.country_id";
if(isset($_GET["pro"]) || isset($s["pro"]))
	$q.=" LEFT JOIN nse_nlocations_provinces AS p ON l.province_id=p.province_id";
if(isset($_GET["twn"]) || isset($s["twn"]))
	$q.=" LEFT JOIN nse_nlocations_towns AS t ON l.town_id=t.town_id";
if(isset($_GET["sub"]) || isset($s["sub"]))
	$q.=" LEFT JOIN nse_nlocations_suburbs AS s ON l.suburb_id=s.suburb_id";
if(isset($s["ema"]) || isset($s["cel"]) || isset($s["tel"]))
{
	$q.=" LEFT JOIN nse_establishment_reservation AS r ON e.establishment_code=r.establishment_code";
	$q.=" LEFT JOIN nse_user_establishments AS ue ON e.establishment_code=ue.establishment_code";
	$q.=" LEFT JOIN nse_user AS u ON ue.user_id=u.user_id";
}
$q.=" WHERE e.establishment_code!=''";
if(isset($_GET["est"]))
	$q.=" AND e.establishment_code='".$_GET["est"]."'";
if(isset($_GET["sta"]))
	$q.=" AND e.aa_estab='Y'";
if(isset($_GET["cou"]))
	$q.=" AND l.country_id=".$_GET["cou"];
if(isset($_GET["pro"]))
	$q.=" AND l.province_id=".$_GET["pro"];
if(isset($_GET["twn"]))
	$q.=" AND l.town_id=".$_GET["twn"];
if(isset($_GET["sub"]))
	$q.=" AND l.suburb_id=".$_GET["sub"];
$q.=" ORDER BY e.establishment_name";
$jL0=mysql_num_rows(mysql_query($q));
if ($jL2 != 0) $q.=" LIMIT ".$jL1.",".$jL2;
//echo $q;
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$sta=array(""=>"");
	$cou=array(""=>"");
	$pro=array(""=>"");
	$twn=array(""=>"");
	$sub=array(""=>"");

	while($g=mysql_fetch_array($q))
	{
        if ($jL2 == 0) {
            
            $line_counter++;
            $headers[0] = 'Establishment_code';
            $headers[1] = 'Establishment_name';
            $data[0] =$g["establishment_code"];
            $data[1] =$g["establishment_name"];
            if(isset($s["sta"]))
            {
                $headers[2] = 'QA Status';
                $data[2] = $g["aa_category_name"];
            }
            if(isset($s["cou"]))
            {
                $headers[3] = 'Country';
                $data[3] = $g["country_name"];
            }
            if(isset($s["pro"]))
            {
                $headers[4] = 'Province';
                $data[4] =$g["province_name"];
            }
            if(isset($s["twn"]))
            {
                $headers[5] = 'Town';
                $data[5] =$g["town_name"];
            }
            if(isset($s["sub"]))
            {
                $headers[6] = 'Suburb';
                $data[6]=$g["suburb_name"];
            }
            if(isset($s["str"]))$headers[7] = 'Street_address';
            if(isset($s["pos"]))$headers[8] = 'Postal Address';
            if(isset($s["ema"]))$headers[9] = 'Email';
            if(isset($s["cel"]))$headers[10] = 'Cell';
            if(isset($s["tel"]))$headers[11] ='Tel';
            if(isset($s["con"]))$headers[12] ='Contact';

            if(isset($s["str"]))$headers[7] =trim(trim(trim($g["street_address_line1"],"."),","));
            if(isset($s["pos"]))
				{
					if($g["reservation_postal1"])
						$headers[8] =trim(trim(trim($g["reservation_postal1"],"."),",")).($g["postal_address_line2"]?", ".trim(trim(trim($g["reservation_postal2"],"."),",")):"");
					else
						$headers[8] =trim(trim(trim($g["postal_address_line1"],"."),",")).($g["postal_address_line2"]?", ".trim(trim(trim($g["postal_address_line2"],"."),",")):"");
				}
            if(isset($s["ema"]))$headers[9] =($g["reservation_email"]?$g["reservation_email"]:$g["email"]);
            if(isset($s["cel"]))$headers[10] =($g["reservation_cell"]?$g["reservation_cell"]:$g["cell"]);
            if(isset($s["tel"]))$headers[11] =($g["reservation_tel"]?$g["reservation_tel"]:$g["tel"]);
            if(isset($s["con"]))$headers[12] =($g["firstname"]?"{$g["firstname"]} {$g["lastname"]}":"");

            if ($line_counter == 0) {
                $fh = fopen($SDR."/apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv", 'w');
                fputcsv($fh, $headers);
                $files .= "<li><a href='/juno/apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv'>Report Part $file_counter.csv</a></li>";
            }
            fputcsv($fh, $data);
            if ($line_counter == 10000) {
                fclose($fh);
                $line_counter=-1;
                $file_counter++;
            }

        } else {
            $r.="~";
            $r.=$g["establishment_code"]."~";
            $r.=$g["establishment_name"]."~";
            if(isset($s["sta"]))
            {
                if(!isset($sta[$g["aa_category_name"]]))
                    $sta[$g["aa_category_name"]]=count($sta);
                $r.=$sta[$g["aa_category_name"]]."~";
            }
            if(isset($s["cou"]))
            {
                if(!isset($cou[$g["country_name"]]))
                    $cou[$g["country_name"]]=count($cou);
                $r.=$cou[$g["country_name"]]."~";
            }
            if(isset($s["pro"]))
            {
                if(!isset($pro[$g["province_name"]]))
                    $pro[$g["province_name"]]=count($pro);
                $r.=$pro[$g["province_name"]]."~";
            }
            if(isset($s["twn"]))
            {
                if(!isset($twn[$g["town_name"]]))
                    $twn[$g["town_name"]]=count($twn);
                $r.=$twn[$g["town_name"]]."~";
            }
            if(isset($s["sub"]))
            {
                if(!isset($sub[$g["suburb_name"]]))
                    $sub[$g["suburb_name"]]=count($sub);
                $r.=$sub[$g["suburb_name"]]."~";
            }
            if(isset($s["str"]))$r.=trim(trim(trim($g["street_address_line1"],"."),","))."~";
            if(isset($s["pos"]))$r.=trim(trim(trim($g["postal_address_line1"],"."),",")).($g["postal_address_line2"]?", ".trim(trim(trim($g["postal_address_line2"],"."),",")):"")."~";
            if(isset($s["ema"]))$r.=($g["reservation_email"]?$g["reservation_email"]:$g["email"])."~";
            if(isset($s["cel"]))$r.=($g["reservation_cell"]?$g["reservation_cell"]:$g["cell"])."~";
            if(isset($s["tel"]))$r.=($g["reservation_tel"]?$g["reservation_tel"]:$g["phone"])."~";
            if(isset($s["con"]))$r.=($g["firstname"]?"{$g["firstname"]} {$g["lastname"]}":'')."~";
            $r.="|";
        }
	}
    if ($jL2 != '0') {
        $r=str_replace("\r","",$r);
        $r=str_replace("\n","",$r);
        $r=str_replace("\"","",$r);
        $r=str_replace("~|","|",$r);
        $aq="";
        foreach($sta as $k => $v)
            $aq.=$v."~".$k."|";
        $ac="";
        foreach($cou as $k => $v)
            $ac.=$v."~".$k."|";
        $ap="";
        foreach($pro as $k => $v)
            $ap.=$v."~".$k."|";
        $at="";
        foreach($twn as $k => $v)
            $at.=$v."~".$k."|";
        $as="";
        foreach($sub as $k => $v)
            $ac.=$v."~".$k."|";
    }
}

if ($jL2 != '0') {
    echo "<html>";
    echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<script src=".$ROOT."/system/P.js></script>";
    echo "<script src=".$ROOT."/apps/reports/contact/res.js></script>";
    echo "<script>J_in_r(\"".$r."\",\"".$d."\",\"".$aq."\",\"".$ac."\",\"".$ap."\",\"".$at."\",\"".$as."\",".$jL0.",".$jL1.",".$jL2.")</script>";
} else {

    $page = "<body style='color:#999;padding:30 40 40 160;background:url(/juno/ico/set/spreadsheet.png) no-repeat 40px 40px'><tt style=width:260><hr>CSV Files Created. Right-click to save.<ul>$files</ul></tt></body></html>";
    echo $page;

}

?>