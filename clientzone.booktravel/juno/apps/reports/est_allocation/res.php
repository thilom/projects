<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$aq="";
$ar="";

// for display
$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	if($v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
}
$t=array("X","code","name","cou","pro","twn","sub","ass","rep");
$d="";
$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}


$r="";

$q="SELECT ";
$q.="DISTINCT e.establishment_code";
$q.=",e.establishment_name";
$q.=",e.assessor_id";
$q.=",e.rep_id1";
$q.=",e.rep_id2";
$q.=",l.country_id";
$q.=",l.province_id";
$q.=",l.town_id";
$q.=",l.suburb_id";
$q.=" FROM nse_establishment AS e";
$q.=" LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code";
$q.=" WHERE e.establishment_code!=''";
if(isset($_GET["est"]))
	$q.=" AND e.establishment_code='".$_GET["est"]."'";
if(isset($_GET["cou"]))
	$q.=" AND l.country_id=".$_GET["cou"];
if(isset($_GET["pro"]))
	$q.=" AND l.province_id=".$_GET["pro"];
if(isset($_GET["twn"]))
	$q.=" AND l.town_id=".$_GET["twn"];
if(isset($_GET["sub"]))
	$q.=" AND l.suburb_id=".$_GET["sub"];
if(isset($_GET["sta"]))
	$q.=" AND e.aa_estab='Y'";
if(isset($_GET["ass"]))
	$q.=" AND e.assessor_id=".$_GET["ass"];
if(isset($_GET["rep"]))
	$q.=" AND (e.rep_id1=".$_GET["rep"]." OR e.rep_id2=".$_GET["rep"].")";
$q.=" ORDER BY e.establishment_name";
$jL0=mysql_num_rows(mysql_query($q));
echo mysql_error();
if(isset($_GET["est"]))
	$q.=" LIMIT 1";
else
	$q.=" LIMIT ".$jL1.",".$jL2;
//echo $q;
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	include $SDR."/system/get.php";
	if(!isset($_GET["csv"]))
	{
		$cou=array();
		$pro=array();
		$twn=array();
		$sub=array();
		$peo=array();
		$co="";
		$pr="";
		$tw="";
		$su="";
		$pe="";
	}

	while($g=mysql_fetch_array($q))
	{
		if(isset($_GET["csv"]))
		{
			$r.=$g["establishment_code"];
			if(!$i)
			{
				$hed.="CODE";
				$c++;
			}
			$r.=",".str_replace(",",";",$g["establishment_name"]);
			if(!$i)
			{
				$hed.=",ESTABLISHMENT";
				$c++;
			}
			if(isset($s["cou"]))
			{
				$r.=",".str_replace(",",";",J_Value("country_name","nse_nlocations_countries","country_id",$g["country_id"]));
				if(!$i)
				{
					$hed.=",COUNTRY";
					$c++;
				}
			}
			if(isset($s["pro"]))
			{
				$r.=",".str_replace(",",";",J_Value("province_name","nse_nlocations_provinces","province_id",$g["province_id"]));
				if(!$i)
				{
					$hed.=",PROVINCE";
					$c++;
				}
			}
			if(isset($s["twn"]))
			{
				$r.=",".str_replace(",",";",J_Value("town_name","nse_nlocations_towns","town_id",$g["town_id"]));
				if(!$i)
				{
					$hed.=",TOWN";
					$c++;
				}
			}
			if(isset($s["sub"]))
			{
				$r.=",".str_replace(",",";",J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$g["suburb_id"]));
				if(!$i)
				{
					$hed.=",SUBURB";
					$c++;
				}
			}
			if(isset($s["ass"]))
			{
				$r.=",".str_replace(",",";",J_Value("","people","",$g["assessor_id"]));
				if(!$i)
				{
					$hed.=",ASSESSOR";
					$c++;
				}
			}
			if(isset($s["rep"]))
			{
				$r.=",".str_replace(",",";",J_Value("","people","",$g["rep_id1"]));
				$r.=",".str_replace(",",";",J_Value("","people","",$g["rep_id2"]));
				if(!$i)
				{
					$hed.=",REP 1";
					$hed.=",REP 2";
					$c++;
				}
			}
		}
		else
		{
			$r.="~";
			$r.=$g["establishment_code"]."~";
			$r.=$g["establishment_name"]."~";
			if(isset($s["cou"]))
			{
				$r.=$g["country_id"]."~";
				if(!isset($cou[$g["country_id"]]))
				  $cou[$g["country_id"]]=J_Value("country_name","nse_nlocations_countries","country_id",$g["country_id"]);
			}
			if(isset($s["pro"]))
			{
				$r.=$g["province_id"]."~";
				if(!isset($cou[$g["province_id"]]))
				  $pro[$g["province_id"]]=J_Value("province_name","nse_nlocations_provinces","province_id",$g["province_id"]);
			}
			if(isset($s["twn"]))
			{
				$r.=$g["town_id"]."~";
				if(!isset($cou[$g["town_id"]]))
				  $twn[$g["town_id"]]=J_Value("town_name","nse_nlocations_towns","town_id",$g["town_id"]);
			}
			if(isset($s["sub"]))
			{
				$r.=$g["suburb_id"]."~";
				if(!isset($cou[$g["suburb_id"]]))
				  $sub[$g["suburb_id"]]=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$g["suburb_id"]);
			}
			if(isset($s["ass"]))
			{
				$r.=$g["assessor_id"]."~";
				if(!isset($cou[$g["assessor_id"]]))
				  $peo[$g["assessor_id"]]=J_Value("","people","",$g["assessor_id"]);
			}
			if(isset($s["rep"]))
			{
				$r.=($g["rep_id1"]>0?$g["rep_id1"]:"")."~";
				if($g["rep_id1"]>0 && !isset($cou[$g["rep_id1"]]))
				  $peo[$g["rep_id1"]]=J_Value("","people","",$g["rep_id1"]);
				$r.=($g["rep_id2"]>0?$g["rep_id2"]:"")."~";
				if($g["rep_id2"]>0 && !isset($cou[$g["rep_id2"]]))
				  $peo[$g["rep_id2"]]=J_Value("","people","",$g["rep_id2"]);
			}
			$r.="|";
		}
	}
	if(isset($_GET["csv"]))
		$csv=$hed."\n".$r;
	else
	{
	  $r=str_replace("\r","",$r);
	  $r=str_replace("\n","",$r);
	  $r=str_replace("\"","",$r);
	  $r=str_replace("~|","|",$r);
	  foreach($cou as $k => $v)
			$co.=$k."~".$v."|";
	  foreach($pro as $k => $v)
			$pr.=$k."~".$v."|";
	  foreach($twn as $k => $v)
			$tw.=$k."~".$v."|";
	  foreach($sub as $k => $v)
			$su.=$k."~".$v."|";
	  foreach($peo as $k => $v)
			$pe.=$k."~".$v."|";
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"EST_DETAIL_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/reports/est_allocation/res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$d."\",\"".$co."\",\"".$pr."\",\"".$tw."\",\"".$su."\",\"".$pe."\",".$jL0.",".$jL1.",".$jL2.")</script>";
}
?>