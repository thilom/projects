<?php

/**
 * Draw a report for Quality Assurance 12 month projection on renewals
 * 
 * @author Thilo Muller(2011)
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$table = '';
$cost = 46; // %of cost to assessors
$total_estab = 0;
$total_turnover = 0;
$total_cost = 0;
$total_income = 0;
$dates = array();
$estab_counter = array();
$revenue = array();

//Assemble table info
$table .= "<h1>Quality Assurance projection report</h1>";


//Assemble table header
$table .= "<table style='width: 800px; border-collapse: collapse; border: 1px solid black'>";
$table .= "<tr style='background-color: gray; border-bottom: 1px solid black;'>";
$table .= "<th style='width: 500px'> </th>";
$table .= "<th>Establishments</th>";
$table .= "<th>Turnover (R)</th>";
//$table .= "<th>Cost - $cost% (R)</th>";
//$table .= "<th>Income (R)</th>";
$table .= "</tr>";

//Assemble data
for ($i=1; $i<13; $i++) {
	
	$display_date = date('M Y', strtotime( "NOW + $i months"));
	$current_date = date('mY', strtotime( "NOW + $i months"));
	$estab_count = 0;
	$turnover = 0;
	
	$dates[] = $display_date;
	
	
	
	//Get establihments for month
	$statement = "SELECT a.establishment_code, b.room_count
					FROM nse_establishment_assessment AS a
					LEFT JOIN nse_establishment_data AS b ON a.establishment_code=b.establishment_code
					WHERE DATE_FORMAT(a.renewal_date, '%m%Y') = $current_date AND a.assessment_status='complete'";
	$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_estabs->execute();
	$sql_estabs->bind_result($establishment_code, $room_count);
	while ($sql_estabs->fetch()) {
		$estab_count++;
		$turnover += get_value($room_count);
	}
	$sql_estabs->close();
	
	//Calculations
	$total_estab += $estab_count;
	$total_turnover += $turnover;
	$cost_month = ($turnover * (($cost/100)+1)) - $turnover;
	$total_cost += $cost_month;
	$income = $turnover - $cost_month;
	$total_income += $income;
	
	$estab_counter[] = $estab_count;
	$revenue[] = $turnover;
	
	$table .= "<tr style='border-bottom: 1px solid silver'>";
	$table .= "<td style='background-color: silver'>$display_date</td>";
	$table .= "<td>$estab_count</td>";
	$table .= "<td style='text-align: right'>" . number_format($turnover,0) . "</td>";
//	$table .= "<td style='text-align: right'>" . number_format($cost_month) . "</td>";
//	$table .= "<td style='text-align: right'>" . number_format($income) . "</td>";
	$table .= "</tr>";
}

//Totals
$table .= "<tr>";
$table .= "<td style='font-weight: bold; border-top: 3px double black'> Totals</td>";
$table .= "<td style='border-top: 3px double black;font-weight: bold;' >$total_estab</td>";
$table .= "<td style='border-top: 3px double black; text-align: right;font-weight: bold;'>" . number_format($total_turnover) . "</td>";
//$table .= "<td style='border-top: 3px double black; text-align: right;font-weight: bold;'>" . number_format($total_cost) . "</td>";
//$table .= "<td style='border-top: 3px double black; text-align: right;font-weight: bold;'>" . number_format($total_income) . "</td>";
$table .= "</tr>";

//Close table
$table .= "</table>";
$table .= "<tt>Note: Does not take cancellations or new clients into account</tt><br>";
$table .= "<tt>Note: Last 3 months does not reflect Quality assurance that is in the process of bieng renewed.</tt>";


$graph1 = "<html>
  <head>
    <!--Load the AJAX API-->
    <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script type=\"text/javascript\">
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Establishments');
      data.addRows([
        ['{$dates[0]}', {$estab_counter[0]}],
        ['{$dates[1]}', {$estab_counter[1]}],
        ['{$dates[2]}', {$estab_counter[2]}], 
        ['{$dates[3]}', {$estab_counter[3]}],
        ['{$dates[4]}', {$estab_counter[4]}],
        ['{$dates[5]}', {$estab_counter[5]}],
        ['{$dates[6]}', {$estab_counter[6]}],
        ['{$dates[7]}', {$estab_counter[7]}],
        ['{$dates[8]}', {$estab_counter[8]}],
        ['{$dates[9]}', {$estab_counter[9]}],
        ['{$dates[10]}', {$estab_counter[10]}],
        ['{$dates[11]}', {$estab_counter[11]}]
      ]);

      // Set chart options
      var options = {'title':'12 Month Quality Assurance Projection by Establishment Count',
                     'width':800,
                     'height':200,
					 'legend':'none',
					 'backgroundColor':'#6D7B8D'};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id=\"chart_div\" ></div>
  </body>
</html>";

$graph2 = "<html>
  <head>
    <!--Load the AJAX API-->
    <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script type=\"text/javascript\">
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Revenue');
      data.addRows([
        ['{$dates[0]}', {$revenue[0]}],
        ['{$dates[1]}', {$revenue[1]}],
        ['{$dates[2]}', {$revenue[2]}], 
        ['{$dates[3]}', {$revenue[3]}],
        ['{$dates[4]}', {$revenue[4]}],
        ['{$dates[5]}', {$revenue[5]}],
        ['{$dates[6]}', {$revenue[6]}],
        ['{$dates[7]}', {$revenue[7]}],
        ['{$dates[8]}', {$revenue[8]}],
        ['{$dates[9]}', {$revenue[9]}],
        ['{$dates[10]}', {$revenue[10]}],
        ['{$dates[11]}', {$revenue[11]}]
      ]);

      // Set chart options
      var options = {'title':'12 Month Quality Assurance Projection by Revenue (R)',
                     'width':800,  
                     'height':200,
					 'legend':'none',
					 'backgroundColor':'#6D7B8D'};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
      chart.draw(data, options);
    }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id=\"chart_div2\" style=\"margin-top: 20px\"></div>
  </body>
</html>";
		
$table = "<html>
  <head>
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Establishments');
        data.addColumn('number', 'Revenue');
        data.addRows(12);
        data.setCell(0, 0, '{$dates[0]}');
        data.setCell(0, 1, {$estab_counter[0]},' {$estab_counter[0]}');
        data.setCell(0, 2, {$revenue[0]}, '{$revenue[0]}');
        data.setCell(1, 0, '{$dates[1]}');
        data.setCell(1, 1, {$estab_counter[1]},' {$estab_counter[1]}');
        data.setCell(1, 2, {$revenue[1]}, '{$revenue[1]}');
        data.setCell(2, 0, '{$dates[2]}');
        data.setCell(2, 1, {$estab_counter[2]},' {$estab_counter[2]}');
        data.setCell(2, 2, {$revenue[2]}, '{$revenue[2]}');
        data.setCell(3, 0, '{$dates[3]}');
        data.setCell(3, 1, {$estab_counter[3]},' {$estab_counter[3]}');
        data.setCell(3, 2, {$revenue[3]}, '{$revenue[3]}');
        data.setCell(4, 0, '{$dates[4]}');
        data.setCell(4, 1, {$estab_counter[4]},' {$estab_counter[4]}');
        data.setCell(4, 2, {$revenue[4]}, '{$revenue[4]}');
        data.setCell(5, 0, '{$dates[5]}');
        data.setCell(5, 1, {$estab_counter[5]},' {$estab_counter[5]}');
        data.setCell(5, 2, {$revenue[5]}, '{$revenue[5]}');
        data.setCell(6, 0, '{$dates[6]}');
        data.setCell(6, 1, {$estab_counter[6]},' {$estab_counter[6]}');
        data.setCell(6, 2, {$revenue[6]}, '{$revenue[6]}');
        data.setCell(7, 0, '{$dates[7]}');
        data.setCell(7, 1, {$estab_counter[7]},' {$estab_counter[7]}');
        data.setCell(7, 2, {$revenue[7]}, '{$revenue[7]}');
        data.setCell(8, 0, '{$dates[8]}');
        data.setCell(8, 1, {$estab_counter[8]},' {$estab_counter[8]}');
        data.setCell(8, 2, {$revenue[8]}, '{$revenue[8]}');
        data.setCell(9, 0, '{$dates[9]}');
        data.setCell(9, 1, {$estab_counter[9]},' {$estab_counter[9]}');
        data.setCell(9, 2, {$revenue[9]}, '{$revenue[9]}');
        data.setCell(10, 0, '{$dates[10]}');
        data.setCell(10, 1, {$estab_counter[10]},' {$estab_counter[10]}');
        data.setCell(10, 2, {$revenue[10]}, '{$revenue[10]}');
        data.setCell(11, 0, '{$dates[11]}');
        data.setCell(11, 1, {$estab_counter[11]},' {$estab_counter[11]}');
        data.setCell(11, 2, {$revenue[11]}, '{$revenue[11]}');
        

        var table = new google.visualization.Table(document.getElementById('table_div'));
        table.draw(data, {width: 800});
      }
    </script>
  </head>

  <body>
    <div id='table_div' style='margin-top: 20px;'></div>
  </body>
</html>";

echo "<h1>12 Month Quality Assurance Projection Report</h1>";
echo "<tt>Note: This report does not take QA cancellations or new QA's into account</tt><br>";
echo "<tt>Note: The last 3 months of the report may not be 100% correct due to renewals that still need to be finalized in the past 3 months</tt><hr>";
echo $graph1;
echo $graph2;
echo $table;


function get_value($room_count) {
	$value = 0;
	if ($room_count < 4) {
		$value = 2100;
	} else if($room_count < 7) {
		$value = 2400;
	} else if($room_count < 13) {
		$value = 2850;
	} else if($room_count < 20) {
		$value = 3100;
	} else if($room_count < 50) {
		$value = 4400;
	} else if($room_count < 76) {
		$value = 4800;
	} else if($room_count < 101) {
		$value = 6200;
	} else {
		$value = 7500;
	}
	
	
	return $value;
}

?>
