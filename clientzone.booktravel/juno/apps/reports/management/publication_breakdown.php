<?php

/**
 * Publication breakdown by month
 * 
 * @author Thilo Muller (2011)
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

/* PUBLICATIONS LIST */
echo "<form action='' method='post'>";
echo "<div style='background-color: silver; border: 1px dotted gray'><span style='font-weight: bold; font-size: .8em'>Select Publications:</span>";

//Get list
$year_iteration = '';
$counter = 0;
$statement = "SELECT publication_id, publication_name, publication_year
			FROM nse_publications
			ORDER BY publication_year DESC";
$sql_publications = $GLOBALS['dbCon']->prepare($statement);
$sql_publications->execute();
$sql_publications->bind_result($publication_id, $publication_name, $publication_year);
while ($sql_publications->fetch()) {
	if ($publication_year != $year_iteration) {
		if (!empty($year_iteration)) {
			echo "</table>";
			echo "</fieldset>";
		}
		echo "<fieldset><legend>$publication_year</legend>";
		echo "<table style='width: 100%; '>";
		$counter = 0;
	}
	if ($counter == 0) {
		echo "<tr>";
	}
	
	echo "<td style='width: 33%'><input type='checkbox' name='publicationSearch[]' value='$publication_id' >$publication_name</td>";
	
	if ($counter == 2) {
		echo "</tr>";
		$counter = 0;
	} else {
		$counter++;
	}
	if ($publication_year != $year_iteration) {
		
		$year_iteration = $publication_year;
	}
}
$sql_publications->close();

if ($counter < 3) {
	echo "</table>";
	echo "</fieldset>";
}

echo "<div style='text-align: right'><Input type='submit' value='Create Report' ></div>";
echo "</form>";
echo "</div>";

if (!isset($_POST['publicationSearch'])) {
	die();
}


/* DRAW REPORTS */

//Prepare statement - Publication info
$statement = "SELECT publication_name, publication_year
			FROM nse_publications 
			WHERE publication_id=?
			LIMIT 1";
$sql_info = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Invoices
$statement = "SELECT b.j_inv_id, b.j_inv_number,  b.j_inv_date, c.j_in_name, a.j_invit_total, b.j_inv_type, a.j_invit_id
			FROM nse_invoice_item AS a
			LEFT JOIN nse_invoice AS b ON a.j_invit_invoice=b.j_inv_id
			LEFT JOIN nse_inventory AS c ON a.j_invit_inventory=c.j_in_id
			LEFT JOIN nse_inventory_publications AS d ON d.j_inp_inventory=c.j_in_id
			WHERE d.j_inp_publication=? AND b.j_inv_type=2";
$sql_items = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Payments
$statement = "SELECT j_invp_paid, j_invp_date
			FROM nse_invoice_item_paid
			WHERE j_invp_invoice_item=?
			LIMIT 1";
$sql_payments = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Prepare statement - Credit Notes
$statement = "SELECT b.j_inv_reference, a.j_invit_total
			FROM nse_invoice_item AS a
			LEFT JOIN nse_invoice AS b ON a.j_invit_invoice=b.j_inv_id
			LEFT JOIN nse_inventory AS c ON a.j_invit_inventory=c.j_in_id
			LEFT JOIN nse_inventory_publications AS d ON d.j_inp_inventory=c.j_in_id
			WHERE d.j_inp_publication=? AND b.j_inv_type=3";
$sql_credits = $GLOBALS['dbCon']->prepare($statement);


foreach ($_POST['publicationSearch'] as $publication_id) {
	
	//Vars
	$data_dates = array();
	$credit_notes = array();
	$turnover = array();
	$payments = array();
	
	//Publication info
	$sql_info->bind_param('i', $publication_id);
	$sql_info->execute();
	$sql_info->bind_result($publication_name, $publication_year);
	$sql_info->fetch();
	$sql_info->free_result();
	
	//Credit Notes
	$sql_credits->bind_param('s', $publication_id);
	$sql_credits->execute();
	$sql_credits->bind_result($credit_note_reference, $credit_note_total);
	while ($sql_credits->fetch()) {
		$credit_notes[$credit_note_reference] = $credit_note_total;
	}
	$sql_credits->free_result();

	echo "<hr>";
	echo "<tt style='font-size: 1.2em; font-weight: bold'>Publication Breakdown for $publication_year-$publication_name</tt><br>";
	
	//Payments
//	$sql_payments->bind_param('s', $publication_id);
//	$sql_payments->execute();
//	$sql_payments->bind_result($item_date, $item_name, $item_paid, $estab);
//	while ($sql_payments->fetch()) {
//		$display_date = date('M Y', $item_date);
//		$key =  date('Ym', $item_date);
//		$data_dates[$key] = $display_date;
		
//		if (isset($payments[$key])) {
//			$payments[$key] += $item_paid;
//		} else {
//			$payments[$key] = $item_paid;
//		}
		
//		echo "$estab - $display_date - $item_name - $item_paid<br>";
//	}
//	$sql_payments->free_result();
	
	//Invoices
	$invoice_items = array();
	$sql_items->bind_param('s', $publication_id);
	$sql_items->execute();
	$sql_items->bind_result($invoice_id, $invoice_number, $invoice_date, $invoice_item, $invoice_total, $invoice_type, $item_id);
	while ($sql_items->fetch()) {
		
		if (in_array($item_id, $invoice_items)) {
			echo "DUPLICATE";
		}
		$invoice_items[] = $item_id;
		
		$display_date = date('M Y', $invoice_date);
		$key =  date('Ym', $invoice_date);
		$data_dates[$key] = $display_date;
		
		$invoice_value = $invoice_total;
		if (isset($credit_notes[$invoice_number])) {
			$invoice_value += $credit_notes[$invoice_number];
		}
		if (isset($turnover[$key])) {
			$turnover[$key] += $invoice_value;
		} else {
			$turnover[$key] = $invoice_value;
		}
		
//		if (isset($payments[$key])) {
//			$payments[$key] += $item_paid;
//		} else {
//			$payments[$key] = $item_paid;
//		}
		
		
		
//		echo "$display_date - $key - $invoice_item - $invoice_total - $invoice_type - $invoice_value <br>";
	}
	$sql_items->free_result();
	
	foreach ($invoice_items as $item_id) {
		$item_paid = 0;
		$sql_payments->bind_param('i', $item_id);
		$sql_payments->execute();
		$sql_payments->bind_result($item_paid, $pay_date);
		$sql_payments->fetch();
		$sql_payments->free_result();
		
		$display_date = date('M Y', $pay_date);
		$key =  date('Ym', $pay_date);
		$data_dates[$key] = $display_date;
		
		if (isset($payments[$key])) {
			$payments[$key] += $item_paid;
		} else {
			$payments[$key] = $item_paid;
		}
		
	}
	
	foreach ($payments as $key=>$value) {
		$payments[$key] = number_format($value,2,'.','');
	}
	foreach ($turnover as $key=>$value) {
		$turnover[$key] = number_format($value,2,'.','');
	}
	
	ksort($data_dates);
//	var_dump($data_dates);
//	var_dump($turnover);
//	var_dump($payments);
	
	//Data Table
$table = "
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Tax Invoices');
        data.addColumn('number', 'Payments');
	data.addRows(" . (count($data_dates)+1) . ");";

$row_count = 0;
$total_turnover = 0;
$total_payments = 0;
foreach ($data_dates as $key=>$date) {
	if (!isset($turnover[$key])) $turnover[$key] = 0;
	if (!isset($payments[$key])) $payments[$key] = 0;
	$table .= "
        data.setCell($row_count, 0, '$date');
        data.setCell($row_count, 1, {$turnover[$key]},' {$turnover[$key]}');
        data.setCell($row_count, 2, {$payments[$key]}, '{$payments[$key]}')";

	$total_turnover += $turnover[$key];
	$total_payments += $payments[$key];
	$row_count++;
}
        
$total_turnover = number_format($total_turnover,2,'.','');
$total_payments = number_format($total_payments,2,'.','');
$table .= "
        data.setCell($row_count, 0, 'Total');
        data.setCell($row_count, 1, $total_turnover,' $total_turnover');
        data.setCell($row_count, 2, $total_payments, '$total_payments')";
		
$table .= "				
        var table = new google.visualization.Table(document.getElementById('table_div$publication_id'));
        table.draw(data, {width: 800, allowHtml: true,});
      }
    </script>
    <div id='table_div$publication_id' style='margin-top: 20px;'></div>";

echo $table;
	
}


?>
