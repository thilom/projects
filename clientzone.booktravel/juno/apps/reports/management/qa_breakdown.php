<?php


/**
 * Awards breakdown by month
 * 
 * @author Thilo Muller (2011)
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";


/* AWARDS LIST */
echo "<script src=/juno/system/P.js></script>";
echo "<form action='' method='post'>";
echo "<div style='background-color: silver; border: 1px dotted gray'><span style='font-weight: bold; font-size: .8em'>Display Report for</span>";

//Get list
echo "<table>";
echo "<tr>";
echo "<td style='width: 10%'><td>From:</td><td><input type='text' name='startDate' onclick=j_P.J_datePicker(self,3) ></td><td><tt>This date will be rounded to the FIRST day of the selected month</tt></td></tr>";
echo "<td style='width: 10%'><td>To:</td><td><input type='text' name='endDate' onclick=j_P.J_datePicker(self,3)></td><td><tt>This date will be rounded to the LAST day of the selected month</tt></td></tr>";
echo "</tr>";
echo "</table>";

echo "<div style='text-align: right'><Input type='submit' value='Create Report' ></div>";
echo "</form>";
echo "</div>";

if (!isset($_POST['startDate'])) {
	die();
}


/* DRAW REPORTS */

//Vars
list($day, $month, $year) = explode('/', $_POST['startDate']);
$start_date = mktime(0,0,0,$month, 1, $year);
$display_start = date('d F Y', $start_date);

list($day, $month, $year) = explode('/', $_POST['endDate']);
$end_date = mktime(23,59,59,$month, $day, $year);
$last = date('t', $end_date);
$end_date = mktime(23,59,59,$month, $last, $year);
$display_end = date('d F Y', $end_date);

//Prepare statement - Invoices
$statement = "SELECT b.j_inv_id, b.j_inv_number,  b.j_inv_date, c.j_in_name, a.j_invit_total, b.j_inv_type, a.j_invit_id
			FROM nse_invoice_item AS a
			LEFT JOIN nse_invoice AS b ON a.j_invit_invoice=b.j_inv_id
			LEFT JOIN nse_inventory AS c ON a.j_invit_inventory=c.j_in_id
			WHERE (c.j_in_category=5 || c.j_in_category=6 || c.j_in_category=15 || c.j_in_category=16) AND b.j_inv_type=2 AND b.j_inv_date>? AND j_inv_date<?";
$sql_items = $GLOBALS['dbCon']->prepare($statement);

////Prepare statement - Payments
$statement = "SELECT a.j_invp_paid, a.j_invp_date
			FROM nse_invoice_item_paid AS a
			LEFT JOIN nse_invoice_item AS b ON a.j_invp_invoice_item=b.j_invit_id
			LEFT JOIN nse_inventory AS c ON c.j_in_id=b.j_invit_inventory
			WHERE j_invp_date>? AND j_invp_date<? AND (c.j_in_category=5 || c.j_in_category=6 || c.j_in_category=15 || c.j_in_category=16)";
$sql_payments = $GLOBALS['dbCon']->prepare($statement);

////Prepare statement - Credit Notes
$statement = "SELECT b.j_inv_reference, a.j_invit_total
			FROM nse_invoice_item AS a
			LEFT JOIN nse_invoice AS b ON a.j_invit_invoice=b.j_inv_id
			LEFT JOIN nse_inventory AS c ON a.j_invit_inventory=c.j_in_id
			WHERE (c.j_in_category=5 || c.j_in_category=6 || c.j_in_category=15 || c.j_in_category=16 ) AND b.j_inv_type=3 AND b.j_inv_date>?";
$sql_credits = $GLOBALS['dbCon']->prepare($statement);
	
//	//Vars
	$data_dates = array();
	$credit_notes = array();
	$turnover = array();
	$payments = array();
	
	
//	//Credit Notes
	$sql_credits->bind_param('s', $start_date);
	$sql_credits->execute();
	$sql_credits->bind_result($credit_note_reference, $credit_note_total);
	while ($sql_credits->fetch()) {
		$credit_notes[$credit_note_reference] = $credit_note_total;
	}
	$sql_credits->free_result();
	
//	echo "<hr>";
	echo "<tt style='font-size: 1.2em; font-weight: bold'>Quality Assurance Breakdown from $display_start to $display_end</tt><br>";
	
	$check_date = $start_date;
	while ($check_date < $end_date) {
		$display_date = date('M Y', $check_date);
		$key =  date('Ym', $check_date);
		$data_dates[$key] = $display_date;
		$check_date = strtotime(date('d M Y', $check_date) ." +1 month");
	}
	
	//Invoices
	$invoice_items = array();
	$sql_items->bind_param('ss', $start_date, $end_date);
	$sql_items->execute();
	$sql_items->bind_result($invoice_id, $invoice_number, $invoice_date, $invoice_item, $invoice_total, $invoice_type, $item_id);
	while ($sql_items->fetch()) {
		
		$invoice_items[] = $item_id;
		
		$display_date = date('M Y', $invoice_date);
		$key =  date('Ym', $invoice_date);
		$data_dates[$key] = $display_date;
		
		$invoice_value = $invoice_total;
		if (isset($credit_notes[$invoice_number])) {
			$invoice_value += $credit_notes[$invoice_number];
		}
		if (isset($turnover[$key])) {
			$turnover[$key] += $invoice_value;
		} else {
			$turnover[$key] = $invoice_value;
		}
		
//		echo "$display_date - $key - $invoice_item - $invoice_total - $invoice_type - $invoice_value <br>";
	}
	$sql_items->free_result();
	
//	var_dump($turnover);
	
	
//	foreach ($invoice_items as $item_id) {
		$sql_payments->bind_param('ss', $start_date, $end_date);
		$sql_payments->execute();
		$sql_payments->bind_result($item_paid, $pay_date);
		while ($sql_payments->fetch()) {
			$display_date = date('M Y', $pay_date);
			$key =  date('Ym', $pay_date);
			$data_dates[$key] = $display_date;
			
//			echo "$item_id ~ $display_date<br>";
		
			if (isset($payments[$key])) {
				$payments[$key] += $item_paid;
			} else {
				$payments[$key] = $item_paid;
			}
			
		}
		$sql_payments->free_result();
//		
//		$display_date = date('M Y', $pay_date);
//		$key =  date('Ym', $pay_date);
//		$data_dates[$key] = $display_date;
//		
//		echo "$item_id ~ $display_date<br>";
//		
//		if (isset($payments[$key])) {
//			$payments[$key] += $item_paid;
//		} else {
//			$payments[$key] = $item_paid;
//		}
//		
//	}
	
	foreach ($payments as $key=>$value) {
		$payments[$key] = number_format($value,2,'.','');
	}
	foreach ($turnover as $key=>$value) {
		$turnover[$key] = number_format($value,2,'.','');
	}
	
	ksort($data_dates);
//	var_dump($data_dates);
//	var_dump($turnover);
//	var_dump($payments);
	
	//Data Table
$table = "
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Tax Invoices');
        data.addColumn('number', 'Payments');
	data.addRows(" . (count($data_dates)+1) . ");";

$row_count = 0;
$total_turnover = 0;
$total_payments = 0;
foreach ($data_dates as $key=>$date) {
	if (!isset($turnover[$key])) $turnover[$key] = 0;
	if (!isset($payments[$key])) $payments[$key] = 0;
	$table .= "
        data.setCell($row_count, 0, '$date');
        data.setCell($row_count, 1, {$turnover[$key]},' {$turnover[$key]}');
        data.setCell($row_count, 2, {$payments[$key]}, '{$payments[$key]}')";

	$total_turnover += $turnover[$key];
	$total_payments += $payments[$key];
	$row_count++;
}
        
$total_turnover = number_format($total_turnover,2,'.','');
$total_payments = number_format($total_payments,2,'.','');
$table .= "
        data.setCell($row_count, 0, 'Total');
        data.setCell($row_count, 1, $total_turnover,' $total_turnover');
        data.setCell($row_count, 2, $total_payments, '$total_payments')";
		
$table .= "				
        var table = new google.visualization.Table(document.getElementById('table_div'));
        table.draw(data, {width: 800, allowHtml: true,});
      }
    </script>
    <div id='table_div' style='margin-top: 20px;'></div>";


$graph1 = "
    <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script type=\"text/javascript\">
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Tax Invoices');
      data.addColumn('number', 'Payments');
      data.addRows([";

      foreach ($data_dates as $key=>$date) {
	  $graph1 .= "['{$date}', {$turnover[$key]}, {$payments[$key]}],";
      }
      $graph1 .= "]);

      // Set chart options
      var options = {
                     'width':800,  
                     'height':200,
					 'legend':'none',
					 'backgroundColor':'#6D7B8D'};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
      chart.draw(data, options);
    }
    </script>
    <div id=\"chart_div2\" style=\"margin-top: 20px\"></div>";

echo $graph1;
echo $table;
	
?>
