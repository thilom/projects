<?php

/**
 * New Quality assurance vs cancelled QA report for past 12 months
 * 
 * @author Thilo Muller
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$cancelled_qa_count = array();
$cancelled_qa_value = array();
$new_qa_count = array();
$new_qa_value = array();
$dates = array();
$products = array();
$nett = array();

//Get products
$statement = "SELECT j_in_id, j_in_name, j_in_description, j_in_price, j_in_max_room FROM nse_inventory
				WHERE j_in_max_room > 0 AND j_in_category=5
				ORDER BY j_in_max_room DESC";
$sql_products = $GLOBALS['dbCon']->prepare($statement);
$sql_products->execute();
$sql_products->store_result();
$sql_products->bind_result($product_id, $product_name, $product_description, $product_price, $max_room);
while ($sql_products->fetch()) {
	$products[$product_id]['product_name'] = $product_name;
	$products[$product_id]['product_description'] = $product_description;
	$products[$product_id]['product_price'] = $product_price;
	$products[$product_id]['max_rooms'] = $max_room;
}
$sql_products->free_result();
$sql_products->close();

//Prepare statement - Get assessments
$statement = "SELECT a.establishment_code, b.room_count
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_establishment_data AS b ON a.establishment_code=b.establishment_code
				WHERE a.assessment_type='new establishment' 
					AND a.assessment_status = 'complete'
					AND DATE_FORMAT(a.assessment_date, '%m%Y')=?";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - get cancelations
$statement = "SELECT a.establishment_code, b.room_count
				FROM nse_establishment_qa_cancelled AS a
				LEFT JOIN nse_establishment_data AS b ON a.establishment_code=b.establishment_code
				WHERE DATE_FORMAT(a.cancelled_date, '%m%Y')=?";
$sql_cancel = $GLOBALS['dbCon']->prepare($statement);

//Get new QA count & value
for ($x=12; $x>0; $x--) {
	if (!isset($new_qa_count[$x])) $new_qa_count[$x] = 0;
	if (!isset($new_qa_value[$x])) $new_qa_value[$x] = 0;
	if (!isset($cancelled_qa_value[$x])) $cancelled_qa_value[$x] = 0;
	if (!isset($cancelled_qa_count[$x])) $cancelled_qa_count[$x] = 0;
	
	//Dates
	$display_date = date('M Y', strtotime( "NOW - $x months"));
	$current_date = date('mY', strtotime( "NOW - $x months"));
	$dates[$x] = $display_date;	
	
	//New QA's
	$sql_assessments->bind_param('s', $current_date);
	$sql_assessments->execute();
	$sql_assessments->store_result();
	$sql_assessments->bind_result($establishment_code, $room_count);
	while ($sql_assessments->fetch()) {
		$new_qa_value[$x] += get_value($room_count);
		$new_qa_count[$x]++;
	}
	
	//Cancelled QA's
	$sql_cancel->bind_param('s', $current_date);;
	$sql_cancel->execute();
	$sql_cancel->store_result();
	$sql_cancel->bind_result($establishment_code, $room_count);
	while ($sql_cancel->fetch()) {
		$cancelled_qa_value[$x] += get_value($room_count);
		$cancelled_qa_count[$x]++;
	}
}


//Net values
foreach ($new_qa_value as $key=>$new_value) {
	$nett[$key] = $new_value - $cancelled_qa_value[$key];
}

//var_dump($dates);
//var_dump($new_qa_count);
//var_dump($new_qa_value);
//var_dump($cancelled_qa_count);
//var_dump($cancelled_qa_value);


//Data Table
$table = "
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'New Establishments');
        data.addColumn('number', 'Revenue Gain');
        data.addColumn('number', 'Cancelled Establishments');
        data.addColumn('number', 'Revenue Loss');
        data.addColumn('number', 'Nett');

        data.addRows(12);
        data.setCell(0, 0, '{$dates[12]}');
        data.setCell(0, 1, {$new_qa_count[12]},' {$new_qa_count[12]}');
        data.setCell(0, 2, {$new_qa_value[12]}, '{$new_qa_value[12]}');
        data.setCell(0, 3, {$cancelled_qa_count[12]},' {$cancelled_qa_count[12]}');
        data.setCell(0, 4, {$cancelled_qa_value[12]}, '{$cancelled_qa_value[12]}');
        data.setCell(0, 5, {$nett[12]}, '{$nett[12]}');
		
        data.setCell(1, 0, '{$dates[11]}');
        data.setCell(1, 1, {$new_qa_count[11]},' {$new_qa_count[11]}');
        data.setCell(1, 2, {$new_qa_value[11]}, '{$new_qa_value[11]}');
        data.setCell(1, 3, {$cancelled_qa_count[11]},' {$cancelled_qa_count[11]}');
        data.setCell(1, 4, {$cancelled_qa_value[11]}, '{$cancelled_qa_value[11]}');
		data.setCell(1, 5, {$nett[11]}, '{$nett[11]}');
		
        data.setCell(2, 0, '{$dates[10]}');
        data.setCell(2, 1, {$new_qa_count[10]},' {$new_qa_count[10]}');
        data.setCell(2, 2, {$new_qa_value[10]}, '{$new_qa_value[10]}');
        data.setCell(2, 3, {$cancelled_qa_count[10]},' {$cancelled_qa_count[10]}');
        data.setCell(2, 4, {$cancelled_qa_value[10]}, '{$cancelled_qa_value[10]}');
		data.setCell(2, 5, {$nett[10]}, '{$nett[10]}');
		
		data.setCell(3, 0, '{$dates[9]}');
        data.setCell(3, 1, {$new_qa_count[9]},' {$new_qa_count[9]}');
        data.setCell(3, 2, {$new_qa_value[9]}, '{$new_qa_value[9]}');
        data.setCell(3, 3, {$cancelled_qa_count[9]},' {$cancelled_qa_count[9]}');
        data.setCell(3, 4, {$cancelled_qa_value[9]}, '{$cancelled_qa_value[9]}');
		data.setCell(3, 5, {$nett[9]}, '{$nett[9]}');

		data.setCell(4, 0, '{$dates[8]}');
        data.setCell(4, 1, {$new_qa_count[8]},' {$new_qa_count[8]}');
        data.setCell(4, 2, {$new_qa_value[8]}, '{$new_qa_value[8]}');
        data.setCell(4, 3, {$cancelled_qa_count[8]},' {$cancelled_qa_count[8]}');
        data.setCell(4, 4, {$cancelled_qa_value[8]}, '{$cancelled_qa_value[8]}');
		data.setCell(4, 5, {$nett[8]}, '{$nett[8]}');
		
		data.setCell(5, 0, '{$dates[7]}');
        data.setCell(5, 1, {$new_qa_count[7]},' {$new_qa_count[7]}');
        data.setCell(5, 2, {$new_qa_value[7]}, '{$new_qa_value[7]}');
        data.setCell(5, 3, {$cancelled_qa_count[7]},' {$cancelled_qa_count[7]}');
        data.setCell(5, 4, {$cancelled_qa_value[7]}, '{$cancelled_qa_value[7]}');
		data.setCell(5, 5, {$nett[7]}, '{$nett[7]}');
		
		data.setCell(6, 0, '{$dates[6]}');
        data.setCell(6, 1, {$new_qa_count[6]},' {$new_qa_count[6]}');
        data.setCell(6, 2, {$new_qa_value[6]}, '{$new_qa_value[6]}');
        data.setCell(6, 3, {$cancelled_qa_count[6]},' {$cancelled_qa_count[6]}');
        data.setCell(6, 4, {$cancelled_qa_value[6]}, '{$cancelled_qa_value[6]}');
		data.setCell(6, 5, {$nett[6]}, '{$nett[6]}');
		
		data.setCell(7, 0, '{$dates[5]}');
        data.setCell(7, 1, {$new_qa_count[5]},' {$new_qa_count[5]}');
        data.setCell(7, 2, {$new_qa_value[5]}, '{$new_qa_value[5]}');
        data.setCell(7, 3, {$cancelled_qa_count[5]},' {$cancelled_qa_count[5]}');
        data.setCell(7, 4, {$cancelled_qa_value[5]}, '{$cancelled_qa_value[5]}');
		data.setCell(7, 5, {$nett[5]}, '{$nett[5]}');
		
		data.setCell(8, 0, '{$dates[4]}');
        data.setCell(8, 1, {$new_qa_count[4]},' {$new_qa_count[4]}');
        data.setCell(8, 2, {$new_qa_value[4]}, '{$new_qa_value[4]}');
        data.setCell(8, 3, {$cancelled_qa_count[4]},' {$cancelled_qa_count[4]}');
        data.setCell(8, 4, {$cancelled_qa_value[4]}, '{$cancelled_qa_value[4]}');
		data.setCell(8, 5, {$nett[4]}, '{$nett[4]}');
		
		data.setCell(9, 0, '{$dates[3]}');
        data.setCell(9, 1, {$new_qa_count[3]},' {$new_qa_count[3]}');
        data.setCell(9, 2, {$new_qa_value[3]}, '{$new_qa_value[3]}');
        data.setCell(9, 3, {$cancelled_qa_count[3]},' {$cancelled_qa_count[3]}');
        data.setCell(9, 4, {$cancelled_qa_value[3]}, '{$cancelled_qa_value[3]}');
		data.setCell(9, 5, {$nett[3]}, '{$nett[3]}');
		
		data.setCell(10, 0, '{$dates[2]}');
        data.setCell(10, 1, {$new_qa_count[2]},' {$new_qa_count[2]}');
        data.setCell(10, 2, {$new_qa_value[2]}, '{$new_qa_value[2]}');
        data.setCell(10, 3, {$cancelled_qa_count[2]},' {$cancelled_qa_count[2]}');
        data.setCell(10, 4, {$cancelled_qa_value[2]}, '{$cancelled_qa_value[2]}');
		data.setCell(10, 5, {$nett[2]}, '{$nett[2]}');
		
		data.setCell(11, 0, '{$dates[1]}');
        data.setCell(11, 1, {$new_qa_count[1]},' {$new_qa_count[1]}');
        data.setCell(11, 2, {$new_qa_value[1]}, '{$new_qa_value[1]}');
        data.setCell(11, 3, {$cancelled_qa_count[1]},' {$cancelled_qa_count[1]}');
        data.setCell(11, 4, {$cancelled_qa_value[1]}, '{$cancelled_qa_value[1]}');
		data.setCell(11, 5, {$nett[1]}, '{$nett[1]}');
		
		var table = new google.visualization.Table(document.getElementById('arrowformat_div'));
  
		var formatter = new google.visualization.ArrowFormat();
		formatter.format(data, 5); // Apply formatter to second column
  

		
        var table = new google.visualization.Table(document.getElementById('table_div'));
        table.draw(data, {width: 800, allowHtml: true,});
      }
    </script>
    <div id='table_div' style='margin-top: 20px;'></div>";

//Graph: revenue
$graph1 = "
    <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script type=\"text/javascript\">
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Cancelled QA');
      data.addColumn('number', 'New QA');
      data.addRows([
        ['{$dates[12]}', {$cancelled_qa_value[12]}, {$new_qa_value[12]}],
        ['{$dates[11]}', {$cancelled_qa_value[11]},{$new_qa_value[11]}],
        ['{$dates[10]}', {$cancelled_qa_value[10]}, {$new_qa_value[10]}],
        ['{$dates[9]}', {$cancelled_qa_value[9]},{$new_qa_value[9]}],
        ['{$dates[8]}', {$cancelled_qa_value[8]},{$new_qa_value[8]}],
        ['{$dates[7]}', {$cancelled_qa_value[7]},{$new_qa_value[7]}],
        ['{$dates[6]}', {$cancelled_qa_value[6]},{$new_qa_value[6]}],
        ['{$dates[5]}', {$cancelled_qa_value[5]},{$new_qa_value[5]}],
        ['{$dates[4]}', {$cancelled_qa_value[4]},{$new_qa_value[4]}],
        ['{$dates[3]}', {$cancelled_qa_value[3]},{$new_qa_value[3]}],
        ['{$dates[2]}', {$cancelled_qa_value[2]},{$new_qa_value[2]}],
        ['{$dates[1]}', {$cancelled_qa_value[1]}, {$new_qa_value[1]}]
      ]);

      // Set chart options
      var options = {'title':'12 Month New and Cancelled Quality assurance by Revenue (R)',
                     'width':800,  
                     'height':200,
					 'legend':'none',
					 'backgroundColor':'#6D7B8D'};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
      chart.draw(data, options);
    }
    </script>
    <div id=\"chart_div2\" style=\"margin-top: 20px\"></div>";
		
//Graph: establishment count
$graph2 = "
    <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script type=\"text/javascript\">
    
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);
      
      // Callback that creates and populates a data table, 
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Cancelled QA');
      data.addColumn('number', 'New QA');
      data.addRows([
        ['{$dates[12]}', {$cancelled_qa_count[12]}, {$new_qa_count[12]}],
        ['{$dates[11]}', {$cancelled_qa_count[11]},{$new_qa_count[11]}],
        ['{$dates[10]}', {$cancelled_qa_count[10]}, {$new_qa_count[10]}],
        ['{$dates[9]}', {$cancelled_qa_count[9]},{$new_qa_count[9]}],
        ['{$dates[8]}', {$cancelled_qa_count[8]},{$new_qa_count[8]}],
        ['{$dates[7]}', {$cancelled_qa_count[7]},{$new_qa_count[7]}],
        ['{$dates[6]}', {$cancelled_qa_count[6]},{$new_qa_count[6]}],
        ['{$dates[5]}', {$cancelled_qa_count[5]},{$new_qa_count[5]}],
        ['{$dates[4]}', {$cancelled_qa_count[4]},{$new_qa_count[4]}],
        ['{$dates[3]}', {$cancelled_qa_count[3]},{$new_qa_count[3]}],
        ['{$dates[2]}', {$cancelled_qa_count[2]},{$new_qa_count[2]}],
        ['{$dates[1]}', {$cancelled_qa_count[1]}, {$new_qa_count[1]}]
      ]);

      // Set chart options
      var options = {'title':'12 Month Nem and Cancelled Quality assurance by Establishment',
                     'width':800,  
                     'height':200,
					 'legend':'none',
					 'backgroundColor':'#6D7B8D'};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div1'));
      chart.draw(data, options);
    }
    </script>
    <div id=\"chart_div1\" style=\"margin-top: 20px\"></div>";
		
echo "<h1>12 Month New and Cancelled Quality Assurance Report</h1>";		
echo $graph1;
echo $graph2;
echo $table;
		
function get_value($room_count) {
	
	//Get product
    foreach ($GLOBALS['products'] as $product_id=>$prod) {
        if ($room_count <= $prod['max_rooms']) $value = $prod['product_price'];
    }
	
	return $value;
}



?>
