<?php
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include $SDR."/system/parse.php";

$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?($_GET["jL2"]>2000&&!isset($_GET["csv"])?2000:$_GET["jL2"]):100);

$q="SELECT DISTINCT e.establishment_code";
$q.=",e.establishment_name";
$q.=",e.aa_category_code";
$q.=",e.nightsbridge_bbid";
$q.=",e.aa_estab";
$q.=",e.active";
$q.=",c.country_id";
$q.=",c.country_name";
$q.=",p.province_id";
$q.=",p.province_name";
$q.=",t.town_id";
$q.=",t.town_name";
$q.=",s.suburb_id";
$q.=",s.suburb_name";
$q.=",r.reservation_email";
$q.=",r.reservation_tel";
$q.=",r.reservation_cell";
$q.=",d.room_count";
$q.=" FROM nse_establishment AS e ";
$q.="LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code ";
$q.="LEFT JOIN nse_nlocations_countries AS c ON c.country_id=l.country_id ";
$q.="LEFT JOIN nse_nlocations_provinces AS p ON p.province_id=l.province_id ";
$q.="LEFT JOIN nse_nlocations_towns AS t ON t.town_id=l.town_id ";
$q.="LEFT JOIN nse_nlocations_suburbs AS s ON s.suburb_id=l.suburb_id ";
$q.="LEFT JOIN nse_establishment_reservation AS r ON r.establishment_code=e.establishment_code ";
$q.="LEFT JOIN nse_establishment_data AS d ON e.establishment_code=d.establishment_code ";
$q.="WHERE ";
if(isset($_GET["est"]))
	$q.="e.establishment_code='".$_GET["est"]."' ";
else
	$q.="e.establishment_code!='' ";
if(isset($_GET["cou"]))
{
	if($_GET["cou"]==-1)
		$q.="AND (c.country_id=1 OR c.country_id=2 OR c.country_id=3 OR c.country_id=4 OR c.country_id=5 OR c.country_id=6 OR c.country_id=7 OR c.country_id=8 OR c.country_id=9 OR c.country_id=10 OR c.country_id=11 OR c.country_id=12 OR c.country_id=13 OR c.country_id=14 OR c.country_id=16 OR c.country_id=17) ";
	else
		$q.="AND c.country_id='".$_GET["cou"]."' ";
}
if(isset($_GET["pro"]))
	$q.="AND p.province_id='".$_GET["pro"]."' ";
if(isset($_GET["twn"]))
	$q.="AND t.town_id='".$_GET["twn"]."' ";
$jL0=mysql_num_rows(mysql_query($q));
$q.="ORDER BY e.establishment_name ";
$q.="LIMIT ".$jL1.",".$jL2;
//echo $q."<hr>";
//echo mysql_error();
$rs=mysql_query($q);
if(isset($_GET["csv"]))
{
	$hed="";
	$tot="";
	$i=0;
	$c=0;
}
$r="";
$pr="";
$c="";
$pc="";
$t="";
$s="";
$rg="";

if(mysql_num_rows($rs))
{
	$star=0;
	$pp=array("");
	$cc=array("");
	$pv=array("");
	$tt=array("");
	$ss=array("");
	$rrg=array("");

	include $SDR."/custom/lib/est_items.php";

	function search($e=0)
	{
		$v=0;
		if($e)
		{
			$v=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_searchlog WHERE establishment_code='".$e."'"));
			$v=$v[0];
		}
		return $v;
	}

	function views($e=0)
	{
		$v=0;
		if($e)
		{
			$v=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_view_logs WHERE establishment_code='".$e."'"));
			$v=$v[0];
		}
		return $v;
	}

	function clickthru($e=0)
	{
		$v=0;
		if($e)
		{
			$v=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_clickthru_log WHERE establishment_code='".$e."'"));
			$v=$v[0];
		}
		return $v;
	}


	$p=0;
	$i=0;

	while($g=mysql_fetch_array($rs))
	{
		$db=array();
		$qdb=mysql_query("SELECT * FROM nse_establishment_dbase WHERE establishment_code='".$g["establishment_code"]."' LIMIT 1");
		if(mysql_num_rows($qdb))
			$db=mysql_fetch_array($qdb);

		if(isset($_GET["csv"]))
		{
			if(!$i)
				$hed.=",COUNTRY";
			$r.=",".str_replace(",",";",strtoupper($g["country_name"]));
			if(!$i)
				$hed.=",PROVINCE";
			$r.=",".str_replace(",",";",strtoupper($g["province_name"]));
			if(!$i)
				$hed.=",REGION";
			$regs=region($g["town_id"],1);
			$r.=",".str_replace(",",";",strtoupper($regs[0]));
			if(!$i)
				$hed.=",SUB REGION";
			$r.=",".str_replace(",",";",strtoupper($regs[1]));
			if(!$i)
				$hed.=",TOWN";
			$r.=",".str_replace(",",";",strtoupper($g["town_name"]));
			if(!$i)
				$hed.=",SUBURB";
			$r.=",".str_replace(",",";",strtoupper($g["suburb_name"]));
			if(!$i)
				$hed.=",CODE";
			$r.=",".str_replace(",",";",strtoupper($g["establishment_code"]));
			if(!$i)
				$hed.=",ESTABLISHMENT";
			$r.=",".str_replace(",",";",strtoupper($g["establishment_name"]));
			if(!$i)
				$hed.=",RES TYPE";
			$r.=",".str_replace(",",";",res_type($g["establishment_code"]));
			if(!$i)
				$hed.=",ROOMS";
			$r.=",".($g["room_count"]>0?$g["room_count"]:"");
			if(!$i)
				$hed.=",RES-TYPE";
			$r.=",".str_replace(",",";",res_type($g["establishment_code"]));
			if(!$i)
				$hed.=",STARS";
			$r.=",".($star?$star:"");
			if(!$i)
				$hed.=",ACTIVE";
			$r.=",".($g["active"]?"Y":"N");
			if(!$i)
				$hed.=",SEARCHES";
			$r.=",".search($g["establishment_code"]);
			if(!$i)
				$hed.=",VIEWS";
			$r.=",".views($g["establishment_code"]);
			if(!$i)
				$hed.=",CLICK";
			$r.=",".clickthru($g["establishment_code"]);
			if(!$i)
				$hed.=",NIGHTSBRIDGE";
			$r.=",-";
			$p=people($g["establishment_code"]);
			if(!$i)
				$hed.=",CONTACT";
			$r.=",".(isset($pp[$p])?str_replace(",",";",$pp[$p]):"");
			if(!$i)
				$hed.=",TEL";
			$r.=",".str_replace(",",";",numbers($g["establishment_code"],$p,$g["reservation_tel"],$g["reservation_cell"]));
			if(!$i)
				$hed.=",CELL";
			$r.=",".str_replace(",",";",numbers($g["establishment_code"],$p,$g["reservation_cell"],$g["reservation_cell"]));
			if(!$i)
				$hed.=",EMAIL";
			$r.=",".str_replace(",",";",$g["reservation_email"]);
			$r.="\n";
			$i++;
		}
		else
		{
			$r.=$i."~";
			$r.=$g["country_id"]."~";
			if(!isset($cc[$g["country_id"]]) && $g["country_name"])
				$cc[$g["country_id"]]=strtoupper($g["country_name"]);
			$r.=$g["province_id"]."~";
			if(!isset($pv[$g["province_id"]]) && $g["province_name"])
				$pv[$g["province_id"]]=strtoupper($g["province_name"]);
			$regs=region($g["town_id"]);
			$r.=$regs[0]."~";
			$r.=$regs[1]."~";
			$r.=$g["town_id"]."~";
			if(!isset($tt[$g["town_id"]]) && $g["town_name"])
				$tt[$g["town_id"]]=strtoupper($g["town_name"]);
			$r.=$g["suburb_id"]."~";
			if(!isset($ss[$g["suburb_id"]]) && $g["suburb_name"])
				$ss[$g["suburb_id"]]=strtoupper($g["suburb_name"]);
			$r.=$g["establishment_code"]."~";
			$r.=trim($g["establishment_name"])."~";
			$r.=res_type($g["establishment_code"])."~";
			$r.=($g["room_count"]>0?$g["room_count"]:"")."~";
			$r.=($g["active"]?"Y":"N")."~";
			$r.=search($g["establishment_code"])."~";
			$r.=views($g["establishment_code"])."~";
			$r.=clickthru($g["establishment_code"])."~";
			$r.="-~";
			$p=people($g["establishment_code"]);
			$r.=$p."~";
			$r.=numbers($g["establishment_code"],$p,$g["reservation_tel"],$g["reservation_cell"])."~";
			$r.=numbers($g["establishment_code"],$p,$g["reservation_cell"],$g["reservation_cell"])."~";
			$r.=$g["reservation_email"]."~";
			$r.="|";
			$i++;
		}
	}

	if(isset($_GET["csv"]))
		$csv=$hed."\n".$r;
	else
	{
		$r=str_replace("\r","",$r);
		$r=str_replace("\n","",$r);
		$r=str_replace("\"","",$r);
		$r=str_replace("~|","|",$r);
		foreach($cc as $k => $v)
			$c.=$k."~".$v."|";
		foreach($pv as $k => $v)
			$pc.=$k."~".$v."|";
		foreach($rrg as $k => $v)
			$rg.=$k."~".$v."|";
		foreach($tt as $k => $v)
			$t.=$k."~".$v."|";
		foreach($ss as $k => $v)
			$s.=$k."~".$v."|";
		foreach($pp as $k => $v)
			$pr.=$k."~".$v."|";
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"ADS_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/reports/clicks/res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$c."\",\"".$pc."\",\"".$rg."\",\"".$t."\",\"".$s."\",\"".$pr."\",".$jL0.",".$jL1.",".$jL2.",'".$_SESSION["j_user"]["role"]."')</script>";
}
?>