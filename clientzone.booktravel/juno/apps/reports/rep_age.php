<?php
/**
 * Rep age analysis. Displays a list of establishment with the
 * respective time in days that they have been responsible for the establishment.
 *
 * @version $Id$
 * @author Thilo Muller(2011)
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/reports/rep_age/rep_search.js></script>";
if(isset($_GET["j_IF"]))
{
	echo "<body><script>J_in_s()</script></body></html>";
	die();
}
echo "<body class=j_List>";
$s=$SDR."/stuff/people/".$_SESSION["j_user"]["id"]."/set.php";
if(is_file($s))include $s;
$s=(isset($j_repGen_report)?$j_repGen_report:"");
echo "<script>J_in_N('".$s."')</script>";
$J_home=0;
$J_title1="Rep Age Analysis";
$J_icon="<img src=".$ROOT."/ico/set/user_both.png>";
$J_label=21;
$J_width=1440;
$J_framesize=260;
$J_frame1=$ROOT."/apps/reports/rep_age.php";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>