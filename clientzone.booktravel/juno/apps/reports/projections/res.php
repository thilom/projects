<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:20);

$age_limit = strtotime("now - 6 months");

if (isset($_POST['mails'])) {
    include 'create_invoice.php';
}

if (isset($_GET['month'])) {
    $month = $_GET['month'];
} else {
    echo "No Month Selected";
}

if (isset($_GET['year'])) {
    $year = $_GET['year'];
} else {
    echo "No Year Selected";
}

$r = "";
$counter = 0;
$code_list = array();
$review_dates = array();
$line_counter = 0;
$establishment_count = 0;
$current_month = date('n');

$renewal_date_start = mktime(0,0,0,$month, 1, $year);
$renewal_date_end = mktime(0,0,0, $month, date('t', $renewal_date_start), $year);


//Get products
$statement = "SELECT j_in_id, j_in_name, j_in_description, j_in_price, j_in_max_room FROM nse_inventory WHERE j_in_max_room > 0 ORDER BY j_in_max_room DESC";
$sql_products = $GLOBALS['dbCon']->prepare($statement);
$sql_products->execute();
$sql_products->store_result();
$sql_products->bind_result($product_id, $product_name, $product_description, $product_price, $max_room);
while ($sql_products->fetch()) {
    $products[$product_id]['product_name'] = $product_name;
    $products[$product_id]['product_description'] = $product_description;
    $products[$product_id]['product_price'] = $product_price;
    $products[$product_id]['max_rooms'] = $max_room;
}
$sql_products->free_result();
$sql_products->close();

//Prepare statement - Next review date from assessments
$statement = "SELECT renewal_date FROM nse_establishment_assessment WHERE establishment_code=? LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - update reveiw date
$statement = "UPDATE nse_establishment SET review_date_depricated=? WHERE establishment_code=?";
$sql_review_update = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Assessor & rep
$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=? LIMIT 1";
$sql_assessor = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Get QA out
$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=?";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);

//Get list of establishments
$statement = "SELECT establishment_code, establishment_name,review_date_depricated, enter_date, assessor_id, rep_id1, rep_id2 FROM nse_establishment WHERE aa_estab='Y' ORDER BY enter_date";
$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
$sql_estabs->execute();
$sql_estabs->store_result();
$sql_estabs->bind_result($establishment_code, $establishment_name, $review_date, $entered_date, $assessor_id, $rep_id1, $rep_id2);
while ($sql_estabs->fetch()) {

    $assessment_review = '';
    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->bind_result($assessment_review);
    $sql_data->fetch();
    $sql_data->free_result();

    //Get Qa Out Date
    $qa_out = '';
    $sql_qa_out->bind_param('s', $establishment_code);
    $sql_qa_out->execute();
    $sql_qa_out->bind_result($qa_out);
    $sql_qa_out->fetch();
    $sql_qa_out->free_result();

    //Check QA out date
    if (!empty($qa_out) && $qa_out != '0000-00-00') continue;

    if (empty($assessment_review)) $assessment_review = $review_date;
    if (empty($assessment_review)) continue;

    if (empty($ssessment_review)) {
        if (!empty($entered_date) && $entered_date != '0000-00-00') {

            list($y, $m, $d) = explode('-', $entered_date);
            $this_year = date('y');
            $renew_year = (int) $m < $current_month?$this_year+1:$this_year;
            $renewal_date = date('Y-m-d', strtotime("20$renew_year-$m-1"));

            $sql_review_update->bind_param('ss', $renewal_date, $establishment_code);
            $sql_review_update->execute();

            $assessment_review = $renewal_date;
        }
    }

    list($y, $m, $d) = explode('-', $assessment_review);
    $review_timestamp = mktime(0,0,0, $m, $d, $y );


    if ($review_timestamp != $renewal_date_start) continue;

    if ($line_counter >= $jL1 && $line_counter < $jL1+$jL2) {

        $code_list[] = $establishment_code;

        $review_dates[$establishment_code] = !empty($review_timestamp)?date('Y-m-d', $review_timestamp):'-';

    }
    $line_counter++;
    $establishment_count++;

}


$sql_estabs->free_result();
$sql_estabs->close();

//Prepare statment - Establishment Data
$statement = "SELECT establishment_name, enter_date, assessor_id FROM nse_establishment WHERE establishment_code=?";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - location
$statement = "SELECT b.town_name, c.province_name, d.country_name
                FROM nse_establishment_location AS a
                LEFT JOIN nse_nlocations_towns AS b ON a.town_id=b.town_id
                LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
                LEFT JOIN nse_nlocations_countries AS d ON a.country_id=d.country_id
                WHERE a.establishment_code=?";
$sql_location = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Find invoice
$statement = "SELECT j_inv_id
                FROM nse_invoice AS a
                LEFT JOIN nse_invoice_item AS b ON a.j_inv_id=b.j_invit_invoice
                WHERE a.j_inv_to_company=? && b.j_invit_inventory=? && $age_limit < a.j_inv_date";
$sql_inv_check = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - Email list
$statement = "SELECT u.firstname, u.lastname, u.email, e.designation
                FROM nse_user AS u
                JOIN nse_user_establishments AS e ON u.user_id=e.user_id
                WHERE e.establishment_code=?";
$sql_email = $GLOBALS['dbCon']->prepare($statement);

//Prepare Statement - get room count
$statement = "SELECT room_count FROM nse_establishment_data WHERE establishment_code=?";
$sql_room = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Get

//Prepare statement - assessments
$statement = "SELECT DATE_FORMAT(assessment_date, '%y-%m-%d') FROM nse_establishment_assessment WHERE establishment_code=? ORDER BY assessment_date LIMIT 1";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);

foreach ($code_list as $establishment_code) {

    $invoice_id  = 0;
    $status = '';
    $assessment_date = '';
    $room_count = 0;
    $town_name = '';
    $province_name = '';
    $contact = '';
    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->bind_result($establishment_name, $enter_date, $assessor_id);
    $sql_data->fetch();
    $sql_data->free_result();

    $sql_email->bind_param('s', $establishment_code);
    $sql_email->execute();
    $sql_email->store_result();
    $sql_email->bind_result($firstname, $lastname, $email, $designation);
    while ($sql_email->fetch()) {
        $contact .= !empty($email)?preg_replace('~[^a-zA-Z \-\(\)]~',"",$firstname." ".$lastname)."=".preg_replace('~[^a-zA-Z \-\(\)]~',"",$designation)."=".preg_replace('~[^a-zA-Z0-9@_#+&\.\-]~',"",strtolower($email))."^":'';
        //$contact .= !empty($email)?str_replace("'",' ', $firstname." ".$lastname) . "=$designation=$email^":'';
    }
    $sql_email->free_result();
    if (empty($contact)) $status = 'No Email';

    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->bind_result($town_name, $province_name, $country_name);
    $sql_location->fetch();
    $sql_location->free_result();

//    $establishment_name .= " ($town_name, $province_name)";
    $establishment_name = str_replace(array("'", '"'), '', $establishment_name);

    $sql_assessments->bind_param('s', $establishment_code);
    $sql_assessments->execute();
    $sql_assessments->bind_result($assessment_date);
    $sql_assessments->fetch();
    $sql_assessments->free_result();

    //Get room count
    $sql_room->bind_param('s', $establishment_code);
    $sql_room->execute();
    $sql_room->bind_result($room_count);
    $sql_room->fetch();
    $sql_room->free_result();

    //Get product
    foreach ($products as $product_id=>$prod) {
        if ($room_count <= $prod['max_rooms']) $this_product = $product_id;
    }

    //Get assessor

    $assessor_name = '';
    if (!empty($assessor_id)) {
        $sql_assessor->bind_param('i', $assessor_id);
        $sql_assessor->execute();
        $sql_assessor->bind_result($assessor_name);
        $sql_assessor->fetch();
        $sql_assessor->free_result();
    }

    //Get Rep 1
    $rep_name1 = '';
    if (!empty($rep_id1)) {
        $sql_assessor->bind_param('i', $rep_id1);
        $sql_assessor->execute();
        $sql_assessor->bind_result($rep_name1);
        $sql_assessor->fetch();
        $sql_assessor->free_result();
    }

    //Get Rep 2
    $rep_name2 = '';
    if (!empty($rep_id2)) {
        $sql_assessor->bind_param('i', $rep_id2);
        $sql_assessor->execute();
        $sql_assessor->bind_result($rep_name2);
        $sql_assessor->fetch();
        $sql_assessor->free_result();
    }

    $r .= "$counter~$establishment_code~$establishment_name~$country_name~$province_name~$town_name~$assessor_name~$rep_id1~$rep_id2~$enter_date~{$review_dates[$establishment_code]}~$room_count~{$products[$this_product]['product_price']}|";

    $counter++;
}
$r=str_replace("\r","",$r);
$r=str_replace("\n","",$r);
$r=str_replace("\"","",$r);
$r=str_replace("~|","|",$r);

//print_r(implode(',',$list));

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/reports/projections/res.js></script>";
echo "<script>J_in_r(\"".$r."\",".$establishment_count.",".$jL1.",".$jL2.")</script>";
?>