<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$d="";
$establishments = array();
//$_GET["d"] = "x~code~name~rep~age";

// for display
$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
    if (empty($v)) continue;
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","code","name","rep","date","age","inv");
$da="";
$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$da.=$n;
		$n++;
	}
	$da.="~";
}


//Vars
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$r = '';


//Get data
$statement = "SELECT a.establishment_code, a.establishment_name, DATEDIFF(NOW(),a.rep1_date), CONCAT(b.firstname, ' ', b.lastname), a.rep1_date
	FROM nse_establishment AS a
	JOIN nse_user AS b ON a.rep_id1=b.user_id
	WHERE a.rep1_date != '' ";

if (isset($_GET['rep'])) {
 $statement .= " AND rep_id1={$_GET['rep']}";
} else {
	$statement .= " AND (rep_id1 != '' || rep_id1 IS NOT NULL)";
}

if (isset($_GET['age'])) {
	$statement .= " AND DATEDIFF(NOW(),a.rep1_date) > {$_GET['age']}";
}

$statement .= " ORDER BY DATEDIFF(NOW(),a.rep1_date) ASC";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);
$sql_data->execute();
$sql_data->store_result();
$count = $sql_data->num_rows;
$sql_data->bind_result($establishment_code, $establishment_name, $age, $rep_name, $rep_date);
while ($sql_data->fetch()) {
	$establishments[$establishment_code]['name'] = $establishment_name;
	$establishments[$establishment_code]['rep'] = $rep_name;
	$establishments[$establishment_code]['date'] = $rep_date;
	$establishments[$establishment_code]['age'] = $age;
}
$sql_data->close();

//Prepare statement - invoices
$statement = "SELECT COUNT(*) FROM nse_invoice WHERE j_inv_date > ? AND j_inv_to_company=?";
$sql_invoices = $GLOBALS['dbCon']->prepare($statement);

foreach ($establishments as $establishment_code=>$data) {
	$invoice_count = 0;
	list($year, $month, $day) = explode('-', $data['date']);
	$timestamp = mktime(0,0,0,$month, $day, $year);

	$sql_invoices->bind_param('ss', $timestamp, $establishment_code);
	$sql_invoices->execute();
	$sql_invoices->bind_result($invoice_count);
	$sql_invoices->fetch();

	if ($invoice_count > 0 && $_GET['hide'] == 0) {
		unset($establishments[$establishment_code]);
	} else {
		$establishments[$establishment_code]['invoice'] = $invoice_count;
	}
}

foreach ($establishments as $establishment_code=>$data) {
	$r .= "$establishment_code~$establishment_code~{$data['name']}~{$data['rep']}~{$data['date']}~{$data['age']}~{$data['invoice']}|";
}

$r=str_replace("\r","",$r);
$r=str_replace("\n","",$r);
$r=str_replace("\"","",$r);
$r=str_replace("~|","|",$r);

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/jsscripts/list_item_remover.js></script>";
echo "<script src=".$ROOT."/apps/reports/rep_age/rep_res.js></script>";
echo "<script>J_in_r(\"".$r."\",\"".$da."\",".$count.",".$jL1.",".$jL2.")</script>";
?>