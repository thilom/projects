<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

//Vars
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);

// for display
$d="";
$dd=explode("~",trim($_GET["d"],"~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
    if(empty($v))continue;
	$s[$v]=1;
	$ddd[$v]=1;
}
$t=array("X","cod","nam","pro","twn","sub","str","rtel","rcel","rfax","rpos","pos","gps","ema","web","pix","con","roo","rat1","rat2","des_shrt","des_long");
$d="";
$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}

$prv="";
$ppv=array();
$tow="";
$ttv=array();
$sub="";
$ssv=array();

$r="";
$q="SELECT";
$q.=" DISTINCT e.establishment_code";
$q.=" ,e.establishment_name";
$q.=" ,e.street_address_line1";
$q.=" ,e.postal_address_line1";
$q.=" ,e.website_url";
$q.=" ,r.reservation_postal1";
$q.=" ,pc.price_description";
$q.=" ,pc.price_in_low";
$q.=" ,pc.price_in_high";
$q.=" ,l.province_id";
$q.=" ,l.town_id";
$q.=" ,l.suburb_id";
$q.=" ,l.gps_latitude";
$q.=" ,l.gps_longitude";
$q.=" ,p.province_name";
$q.=" ,t.town_name";
$q.=" ,s.suburb_name";
$q.=" ,d.room_count";
$q.=" FROM nse_establishment AS e";
$q.=" LEFT JOIN nse_establishment_reservation AS r ON e.establishment_code=r.establishment_code";
$q.=" LEFT JOIN nse_establishment_public_contact AS c ON e.establishment_code=c.establishment_code";
$q.=" LEFT JOIN nse_establishment_data AS d ON e.establishment_code=d.establishment_code";
$q.=" LEFT JOIN nse_establishment_pricing AS pc ON e.establishment_code=pc.establishment_code";
$q.=" LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code";
$q.=" LEFT JOIN nse_nlocations_provinces AS p ON l.province_id=p.province_id";
$q.=" LEFT JOIN nse_nlocations_towns AS t ON l.town_id=t.town_id";
$q.=" LEFT JOIN nse_nlocations_suburbs AS s ON l.suburb_id=s.suburb_id";
$q.=" WHERE e.establishment_code!=''";
if(isset($_GET["est"]))
	$q.=" AND e.establishment_code='".$_GET["est"]."'";
if(isset($_GET["cou"]))
	$q.=" AND l.country_id=".$_GET["cou"];
if(isset($_GET["prv"]))
	$q.=" AND l.province_id=".$_GET["prv"];
	$qq="";
	if(isset($_GET["pro"]))
		$qq.=" OR l.province_id=0";
	if(isset($_GET["twn"]))
		$qq.=" OR l.town_id=0";
	if(isset($_GET["sub"]))
		$qq.=" OR l.suburb_id=0";
	if(isset($_GET["str"]))
		$qq.=" OR e.street_address_line1=''";
	if(isset($_GET["pos"]))
		$qq.=" OR e.postal_address_line1=''";
	if(isset($_GET["gps"]))
			$qq.=" OR l.gps_latitude='' OR l.gps_longitude=''";
	if(isset($_GET["web"]))
		$qq.=" OR e.website_url=''";
	if(isset($_GET["roo"]))
		$qq.=" OR d.room_count=0";
	if(isset($_GET["rat1"]))
		$qq.=" OR pc.price_in_low='' OR pc.price_in_low='0.00' OR pc.price_in_high='' OR pc.price_in_high='0.00'";
	if(isset($_GET["rat2"]))
		$qq.=" OR pc.price_description=''";
	if(isset($_GET["rtel"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_public_contact WHERE establishment_code=e.establishment_code AND contact_type LIKE 'tel%' AND contact_value='')";
	if(isset($_GET["rcel"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_public_contact WHERE establishment_code=e.establishment_code AND contact_type LIKE 'cell%' AND contact_value='')";
	if(isset($_GET["rfax"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_public_contact WHERE establishment_code=e.establishment_code AND contact_type LIKE 'fax%' AND contact_value='')";
	if(isset($_GET["ema"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_public_contact WHERE establishment_code=e.establishment_code AND contact_type LIKE 'email%' AND contact_value='')";
	if(isset($_GET["dss"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_descriptions WHERE establishment_code=e.establishment_code AND description_type='short_description' AND establishment_description='')";
	if(isset($_GET["dsl"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_descriptions WHERE establishment_code=e.establishment_code AND description_type='long_description' AND establishment_description='')";
	if(isset($_GET["pix"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_images WHERE establishment_code=e.establishment_code)";
	if(isset($_GET["con"]))
		$qq.=" OR e.establishment_code NOT IN (SELECT establishment_code FROM nse_establishment_contact WHERE establishment_code=e.establishment_code)";
if($qq)
		$q.=" AND (".trim($qq," OR ").")";
$q.=" ORDER BY e.establishment_name";
$jL0=mysql_num_rows(mysql_query($q));
if(isset($_GET["est"]))
	$q.=" LIMIT 1";
else
	$q.=" LIMIT ".$jL1.",".$jL2;
$q=mysql_query($q);
if(mysql_num_rows($q))
{

	function getDescription($e,$t=0)
	{
		$v="";
		$q = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code='".$e."' AND description_type='".($t?"long_description":"short_description")."' LIMIT 1";
		$q = mysql_query($q);
		if (mysql_num_rows($q))
		{
			$g = mysql_fetch_array($q);
				$v=substr($g["establishment_description"],0,64)."...";
		}
		return $v;
	}

	function getPix($e)
	{
		$v=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_images WHERE establishment_code='".$e."'"));
		return $v[0]?$v[0]:"";
	}

	function getContacts($e)
	{
		$v=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_contacts WHERE establishment_code='".$e."'"));
		return $v[0]?$v[0]:"";
	}

	function getPubCont($t,$e="")
	{
		$v="";
		if($e)
		{
			$i=0;
		 $q = "SELECT contact_value FROM nse_establishment_public_contact WHERE establishment_code='".$e."' AND contact_type LIKE '".$t."%' ORDER BY contact_type LIMIT 5";
		 $q = mysql_query($q);
		 if (mysql_num_rows($q)) {
			while ($g = mysql_fetch_array($q)) 	{
			 if($g["contact_value"])
			 {
				 $v=$g["contact_value"];
				 break;
				}
			}
		 }
		}
		return $v;
	}

	while($g=mysql_fetch_array($q))
	{
		$r.="~";
		$r.=$g["establishment_code"]."~";
		$r.=$g["establishment_name"]."~";
		if(isset($s["pro"]))$r.=$g["province_id"]."~";
		if(isset($s["twn"]))$r.=$g["town_id"]."~";
		if(isset($s["sub"]))$r.=$g["suburb_id"]."~";
		if(isset($s["str"]))$r.=$g["street_address_line1"]."~";
		if(isset($s["rtel"]))$r.=getPubCont("tel",$g["establishment_code"])."~";
		if(isset($s["rcel"]))$r.=getPubCont("cell",$g["establishment_code"])."~";
		if(isset($s["rfax"]))$r.=getPubCont("fax",$g["establishment_code"])."~";
		if(isset($s["rpos"]))$r.=$g["reservation_postal1"]."~";
		if(isset($s["pos"]))$r.=$g["postal_address_line1"]."~";
		if(isset($s["gps"]))$r.=($g["gps_latitude"]&&$g["gps_longitude"]?"Y":"")."~";
		if(isset($s["ema"]))$r.=getPubCont("email",$g["establishment_code"])."~";
		if(isset($s["web"]))$r.=$g["website_url"]."~";
		if(isset($s["pix"]))$r.=getPix($g["establishment_code"])."~";
		if(isset($s["con"]))$r.=getContacts($g["establishment_code"])."~";
		if(isset($s["roo"]))$r.=($g["room_count"]?$g["room_count"]:"")."~";
		if(isset($s["rat1"]))$r.=($g["price_description"]?substr(str_replace("\n"," ",$g["price_description"]),0,64):"")."~";
		if(isset($s["rat2"]))$r.=($g["price_in_low"]&&$g["price_in_low"]!="0.00"?"Low: ".$g["price_in_low"]:"").($g["price_in_high"]&&$g["price_in_high"]!="0.00"?($g["price_in_low"]&&$g["price_in_low"]!="0.00"?" - ":"")."High: ".$g["price_in_high"]:"")."~";
		if(isset($s["des_shrt"]))$r.=getDescription($g["establishment_code"])."~";
		if(isset($s["des_long"]))$r.=getDescription($g["establishment_code"],1)."~";
		$r.="|";
		if(!isset($ppv[$g["province_id"]]))
			$ppv[$g["province_id"]]=$g["province_name"];
		if(!isset($ttv[$g["town_id"]]))
			$ttv[$g["town_id"]]=$g["town_name"];
		if(!isset($ttv[$g["suburb_id"]]))
			$ssv[$g["suburb_id"]]=$g["suburb_name"];
	}
  $r=str_replace("\r","",$r);
  $r=str_replace("\n","",$r);
  $r=str_replace("\"","",$r);
  $r=str_replace("~|","|",$r);
	foreach($ppv as $k => $v)
		$prv.=$k."~".$v."|";
	foreach($ttv as $k => $v)
		$tow.=$k."~".$v."|";
	foreach($ssv as $k => $v)
		$sub.=$k."~".$v."|";
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/reports/integrity/res.js></script>";
echo "<script>J_in_r(\"".$r."\",\"".$d."\",\"".$prv."\",\"".$tow."\",\"".$sub."\",".$jL0.",".$jL1.",".$jL2.")</script>";
?>