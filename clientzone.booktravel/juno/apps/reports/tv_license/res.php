<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";

//Vars
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$pro="";
$tow="";

// for display
$dd=explode("~",trim($_GET["d"]."code~name","~"));
$ddd=array();
$s=array();
foreach($dd as $k => $v)
{
	if($v)
	{
		$s[$v]=1;
		$ddd[$v]=1;
	}
}
$t=array("X","code","name","pro","tow","qa","qao","qap","qac","ass","rep","tvs","lic","con","tel","ema");
$d="";
$n=1;
foreach($t as $k => $v)
{
	if(isset($ddd[$v]))
	{
		$d.=$n;
		$n++;
	}
	$d.="~";
}


$r="";

$q="SELECT ";
$q.=" e.establishment_code";
$q.=",e.establishment_name";
if(isset($s["ass"]))
	$q.=",e.assessor_id";
if(isset($s["rep"]))
	$q.=",e.rep_id1";
if(isset($s["pro"]))
{
	$q.=",p.province_id";
	$q.=",p.province_name";
}
if(isset($s["tow"]))
{
	$q.=",t.town_id as townid";
	$q.=",t.town_name";
}
$q.=",d.tv_count";
$q.=",d.tv_licence";
if(isset($s["tel"]))
{
	$q.=",r.reservation_tel";
	$q.=",r.reservation_cell";
}
if(isset($s["ema"]))
	$q.=",r.reservation_email";
if(isset($s["qac"]))
	$q.=",ec.aa_category_name";
$q.=" FROM nse_establishment AS e";
$q.=" LEFT JOIN nse_establishment_data AS d ON e.establishment_code=d.establishment_code";
$q.=" LEFT JOIN nse_establishment_reservation AS r ON e.establishment_code=r.establishment_code";
$q.=" LEFT JOIN nse_establishment_location AS l ON e.establishment_code=l.establishment_code";
$q.=" LEFT JOIN nse_nlocations_countries AS c ON l.country_id=c.country_id";
if(isset($s["pro"]))
	$q.=" LEFT JOIN nse_nlocations_provinces AS p ON l.province_id=p.province_id";
if(isset($s["tow"]))
	$q.=" LEFT JOIN nse_nlocations_towns AS t ON l.town_id=t.town_id";
if(isset($s["qac"]))
	$q.=" LEFT JOIN nse_aa_category AS ec ON e.aa_category_code=ec.aa_category_code";
$q.=" WHERE c.country_id=1";
if(isset($_GET["est"]))
	$q.=" AND e.establishment_code='".$_GET["est"]."'";
if(isset($_GET["lic"]))
	$q.=" AND d.tv_licence > 0 ";
$q.=" ORDER BY e.establishment_name";

$jL0=mysql_num_rows(mysql_query($q));
if(isset($_GET["est"]))
	$q.=" LIMIT 1";
else	if(isset($_GET["qa"]))
	$q.=" LIMIT ".$jL1.",15000";
else
	$q.=" LIMIT ".$jL1.",".$jL2;
$q=mysql_query($q);
if(mysql_num_rows($q))
{

	$rt=array(""=>"");
	$pv=array();
	$pp=array();
	$tt=array();

	include $SDR."/custom/lib/est_items.php";

	$i=0;

	while($g=mysql_fetch_array($q))
	{

		if(isset($s["qa"]))
			$qa=qa_status($g["establishment_code"]);

		if(isset($_GET["qa"]) && (!$qa || $qa=="X"))
		{}
		else
		{
			if($i>=$jL2)
				break;

			if(isset($s["tel"]) || isset($s["con"]))
			{
				if(isset($s["con"]))
					$person=people($g["establishment_code"]);
				if(isset($s["tel"]))
				{
					$number1=numbers($g["establishment_code"],$person,$g["reservation_tel"],$g["reservation_cell"]);
					$number2=numbers($g["establishment_code"],$person,$g["reservation_cell"],$g["reservation_cell"]);
				}
			}

			if(isset($_GET["csv"]))
			{
				$r.=",".$g["establishment_code"];
				$r.=",".str_replace(",",";",trim($g["establishment_name"]));
				if(isset($s["pro"]))
					$r.=",".str_replace(",",";",$g["province_name"]);
				if(isset($s["tow"]))
					$r.=",".str_replace(",",";",$g["town_name"]);
				if(isset($s["qa"]))
					$r.=",".$qa;
				if(isset($s["qao"]))
					$r.=",".qa_out($g["establishment_code"]);
				if(isset($s["qap"]))
					$r.=",".qa_paid_up($g["establishment_code"]);
				if(isset($s["qac"]))
					$r.=",".$g["aa_category_name"];
				if(isset($s["ass"]))
					$r.=",".($g["assessor_id"]?J_Value("","people","",$g["assessor_id"]):"");
				if(isset($s["rep"]))
					$r.=",".($g["rep_id1"]?J_Value("","people","",$g["rep_id1"]):"");
				if(isset($s["tvs"]))
					$r.=",".($g["tv_count"]?$g["tv_count"]:0);
				if(isset($s["lic"]))
					$r.=",".str_replace(",","",($g["tv_licence"]));
				if(isset($s["con"]))
					$r.=",".(isset($pp[$person])?str_replace(",",";",$pp[$person]):"");
				if(isset($s["tel"]))
					$r.=",".str_replace(",",";",$number1?$number1:$number2);
				if(isset($s["ema"]))
					$r.=",".str_replace(",","",($g["reservation_email"]));
				$r.="\n";
			}
			else
			{
				$r.="~";
				$r.=$g["establishment_code"]."~";
				$r.=trim($g["establishment_name"])."~";
				if(isset($s["pro"]))
				{
					if(!isset($pv[$g["province_id"]]))
							$pv[$g["province_id"]]=$g["province_name"];
					$r.=$g["province_id"]."~";
				}
				if(isset($s["tow"]))
				{
					if(!isset($tt[$g["townid"]]))
							$tt[$g["townid"]]=$g["town_name"];
					$r.=$g["townid"]."~";
				}
				if(isset($s["qa"]))
					$r.=$qa."~";
				if(isset($s["qao"]))
					$r.=qa_out($g["establishment_code"])."~";
				if(isset($s["qap"]))
					$r.=qa_paid_up($g["establishment_code"])."~";
				if(isset($s["qac"]))
					$r.=$g["aa_category_name"]."~";
				if(isset($s["ass"]))
					$r.=($g["assessor_id"]?J_Value("","people","",$g["assessor_id"]):"")."~";
				if(isset($s["rep"]))
					$r.=($g["rep_id1"]?J_Value("","people","",$g["rep_id1"]):"")."~";
				if(isset($s["tvs"]))
					$r.=($g["tv_count"]?$g["tv_count"]:"")."~";
				if(isset($s["lic"]))
					$r.=($g["tv_licence"]?$g["tv_licence"]:"")."~";
				if(isset($s["con"]))
					$r.=(isset($pp[$person])?$pp[$person]:"")."~";
				if(isset($s["tel"]))
					$r.=($number1?$number1:$number2)."~";
				if(isset($s["ema"]))
					$r.=$g["reservation_email"]."~";
				$r.="|";
			}
			$i++;
		}
	}
	if(isset($_GET["csv"]))
	{
		$hed=",CODE";
		$hed.=",ESTABLISHMENT";
		if(isset($s["pro"]))
			$hed.=",PROVINCE";
		if(isset($s["tow"]))
			$hed.=",TOWN";
		if(isset($s["qa"]))
			$hed.=",QA STATUS";
		if(isset($s["qao"]))
			$hed.=",QA OUT";
		if(isset($s["qap"]))
			$hed.=",QA PAID";
		if(isset($s["qac"]))
			$hed.=",QA CATEGORY";
		if(isset($s["ass"]))
			$hed.=",ASSESSOR";
		if(isset($s["rep"]))
			$hed.=",REP";
		if(isset($s["tvs"]))
			$hed.=",TVS";
		if(isset($s["lic"]))
			$hed.=",LICENSE";
		if(isset($s["con"]))
			$hed.=",CONTACT";
		if(isset($s["tel"]))
			$hed.=",TEL";
		if(isset($s["ema"]))
			$hed.=",EMAIL";
		$csv=$hed."\n".$r;
	}
	else
	{
	  $r=str_replace("\r","",$r);
	  $r=str_replace("\n","",$r);
	  $r=str_replace("\"","",$r);
	  $r=str_replace("~|","|",$r);
	  foreach($pv as $k => $v)
			$pro.=$k."~".$v."|";
	  foreach($tt as $k => $v)
			$tow.=$k."~".$v."|";
	}
}

if(isset($_GET["csv"]))
{
	$csv_content=$csv;
	$csv_name=$_GET["csv_name"]?$_GET["csv_name"]:"TV_LICENSES_".$jL1."-".$jL2."_".date("d-m-Y",$EPOCH).".csv";
	include $SDR."/utility/CSV/create.php";
}
else
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/reports/tv_license/res.js></script>";
	echo "<script>J_in_r(\"".$r."\",\"".$d."\",\"".$pro."\",\"".$tow."\",".$jL0.",".$jL1.",".$jL2.")</script>";
}
?>