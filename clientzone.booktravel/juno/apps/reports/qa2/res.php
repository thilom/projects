<?php

/**
 * Draw a report for Quality Assurance
 *
 * @author Thilo Muller(2011,2012)
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";
$d = "";
$files = '';

// Display stuff
if (strpos($_GET['d'], 'conP')) {
	$_GET['d'] .= '~conP2~conP3~conP4';
}
$dd = explode("~", trim($_GET["d"], "~"));
$ddd = array();
$s = array();
foreach ($dd as $k => $v) {
	if (empty($v))
		continue;
	$s[$v] = 1;
	$ddd[$v] = 1;
}
$t = array("X", "code", "name", "cou", "pro", "city", "sub", "reg", "reg2", "sta", "ass", "rep", "date", "out", "current",
			"last", "next",'inv', 'type', 'pay', 'pdate', 'grading', "room", "HL", "SC", "non",
			"HL2012","HL2013","HL2014","HL2015","SC2012","SC2013","SC2014","SC2015","Awards2012",
			"Awards2013","Awards2014","Awards2015","SABest2012","SABest2013","SABest2014","SABest2015","Webonly",
			"Webexpiry", "resTel", "resCell", "resEmail", "conN", "conT", "conC", "conE", "conP", "conP2", "conP3", "conP4");
$da = "";
$n = 1;
foreach ($t as $k => $v) {
	if (isset($ddd[$v])) {
		$da.=$n;
		$n++;
	}
	$da.="~";
}

//Vars
$jL1 = (isset($_GET["jL1"]) ? $_GET["jL1"] : 0);
$jL2 = (isset($_GET["jL2"]) ? $_GET["jL2"] : 100);
$r = '';
$search_data['status']= $_GET['status'];
$search_data['assessor']= $_GET['assessor'];
$search_data['rep']= $_GET['rep'];
$search_data['country']= $_GET['country'];
$search_data['province']= $_GET['province'];
$search_data['town']= $_GET['town'];
$search_data['suburb']= $_GET['suburb'];

//var_dump($search_data);
/* PUT STUFF HERE */

//Get list of initial establishments
$statement = "SELECT a.establishment_code, a.establishment_name, c.country_name, d.province_name, e.town_name,
						f.suburb_name, g.aa_category_name, CONCAT(h.firstname, ' ' , h.lastname),
						CONCAT(i.firstname, ' ' , i.lastname), DATE_FORMAT(a.enter_date, '%Y-%m-%d'),
						j.room_count
				FROM nse_establishment AS a
				LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
				LEFT JOIN nse_nlocations_countries AS c ON b.country_id=c.country_id
				LEFT JOIN nse_nlocations_provinces AS d ON b.province_id=d.province_id
				LEFT JOIN nse_nlocations_towns AS e ON b.town_id=e.town_id
				LEFT JOIN nse_nlocations_suburbs AS f ON b.suburb_id=f.suburb_id
				LEFT JOIN nse_aa_category AS g ON a.aa_category_code=g.aa_category_code
				LEFT JOIN nse_user AS h ON a.assessor_id=h.user_id
				LEFT JOIN nse_user AS i ON a.rep_id1=i.user_id
				LEFT JOIN nse_establishment_data AS j ON a.establishment_code=j.establishment_code ";

if (isset($_GET['est']) && !empty($_GET['est'])) {
	$statement .= "WHERE a.establishment_code={$_GET['est']}";
} else {
	if (!empty($search_data['status'])) { //Establishment QA status
		switch ($search_data['status']) {
			case '1': //All QA
				$statement .= "WHERE TRIM(a.aa_category_code) != ''";
				break;
			case '2': //Superior Only
				$statement .= "WHERE a.aa_category_code = 'S' ";
				break;
			case '3': //Highly Recommended only
				$statement .= "WHERE a.aa_category_code = 'HR' ";
				break;
			case '4': //Recomended Only
				$statement .= "WHERE a.aa_category_code = 'R' ";
				break;
			default: //All establishments
				$statement .= '';
		}
	}

	if (isset($_GET['assessor']) && !empty($_GET['assessor'])) {
		$statement .= strpos($statement, 'WHERE')!==false?' AND':' WHERE';
		$statement .= " h.user_id={$_GET['assessor']}";
	}
	if (isset($_GET['rep']) && !empty($_GET['rep'])) {
		$statement .= strpos($statement, 'WHERE')!==false?' AND':' WHERE';
		$statement .= " i.user_id={$_GET['rep']}";
	}
	if (isset($_GET['country']) && $_GET['country'] != '0' && !empty($_GET['country'])) {
		$statement .= strpos($statement, 'WHERE')>0?' AND':' WHERE';
		$statement .= " b.country_id = {$_GET['country']} ";
	}
	if (isset($_GET['province']) && $_GET['province'] != '0' && !empty($_GET['province'])) {
		$statement .= strpos($statement, 'WHERE')>0?' AND':' WHERE';
		$statement .= " b.province_id = {$_GET['province']} ";
	}
	if (isset($_GET['town']) && $_GET['town'] != '0' && !empty($_GET['town'])) {
		$statement .= strpos($statement, 'WHERE')>0?' AND':' WHERE';
		$statement .= " b.town_id = {$_GET['town']} ";
	}
	if (isset($_GET['suburb']) && $_GET['suburb'] != '0' && !empty($_GET['suburb'])) {
		$statement .= strpos($statement, 'WHERE')>0?' AND':' WHERE';
		$statement .= " b.suburb_id = {$_GET['suburb']} ";
	}
}

$statement .= " ORDER BY establishment_name";
//echo($statement);
//die();

$sql_establishments = $GLOBALS['dbCon']->prepare($statement);
//echo mysqli_error($GLOBALS['dbCon']);
$sql_establishments->execute();
$sql_establishments->bind_result($establishment_code, $establishment_name, $country, $province, $town, $suburb,
								$qa_category, $fa_assessor, $fa_rep, $qa_enter_date, $room_count);
while ($sql_establishments->fetch()) {
	$establishments[$establishment_code]['name'] = $establishment_name;
	$establishments[$establishment_code]['country'] = $country;
	$establishments[$establishment_code]['province'] = $province;
	$establishments[$establishment_code]['town'] = $town;
	$establishments[$establishment_code]['suburb'] = $suburb;
	$establishments[$establishment_code]['qa_category'] = $qa_category;
	$establishments[$establishment_code]['fa_assessor'] = $fa_assessor;
	$establishments[$establishment_code]['fa_rep'] = $fa_rep;
	$establishments[$establishment_code]['qa_enter_date'] = $qa_enter_date;
	$establishments[$establishment_code]['room_count'] = $room_count;
}
$sql_establishments->close();

//Assemble Data
$line_counter = 0;
$estab_count = 0;
foreach ($establishments as $establishment_code => $establishment_data) {


		//Vars
		$last_assessment_data = get_last_assessment($establishment_code);
		$contact_data = get_contact($establishment_code);
		$qa_out = get_qa_out($establishment_code);
		$star_grading = get_restype($establishment_code);
		$latest_invoice = get_invoice($establishment_code);
		$regions = get_regions($establishment_code);

		//Check if estalishment was assessed
		if (empty($last_assessment_data['last_assessment'])) {
//			unset($establishments[$establishment_code]);
//			continue;
		}
		if (!empty($qa_out) && $search_data['status'] > 0 && $search_data['status'] != 5) {
//			unset($establishments[$establishment_code]);
//			continue;
		}
		if ($search_data['status'] == 5 && empty($qa_out)) {
//			unset($establishments[$establishment_code]);
//			continue;
		}


		//Check if it's within the count parameter
		$ns = $jL1 + $jL2;


		$line_counter++;
		if ($line_counter < $jL1 && $jL2 != 0) continue;
		if ($line_counter >= $ns && $jL2 != 0) continue;

		//Invoice Type
		switch($latest_invoice['type']) {
			case '1':
				$invoice_type = 'Pro-Forma';
				break;
			case '2':
				$invoice_type = 'Tax Invoice';
				break;
			default:
				$invoice_type = '';
				break;
		}

		$statement = "SELECT HL, SC, nonaccomm, HL2012,HL2013,HL2014, HL2015,SC2012,SC2013,SC2014,SC2015,Awards2012,
							Awards2013,Awards2014,Awards2015,SABest2012,SABest2013,SABest2014,SABest2015,webonly,Webexpirydate
						FROM nse_establishment_more_crap
						WHERE establishment_code = :establishment_code
						LIMIT 1";
		$sql_mc = $GLOBALS['db_pdo']->prepare($statement);
		$sql_mc->bindParam(':establishment_code', $establishment_code);
		$sql_mc->execute();
		$sql_mc_data = $sql_mc->fetch();
		$sql_mc->closeCursor();

                //Get Reservation Details
                $res_cell = '';
                $res_tel = '';
                $res_email = '';
                $statement = "SELECT contact_type, contact_value
                                FROM nse_establishment_public_contact
                                WHERE establishment_code = :establishment_code";
                $sql_res = $GLOBALS['db_pdo']->prepare($statement);
                $sql_res->bindParam(':establishment_code', $establishment_code);
                $sql_res->execute();
                $sql_res_data = $sql_res->fetchAll();
                $sql_res->closeCursor();
                foreach ($sql_res_data as $res_data) {
                    switch ($res_data['contact_type']) {
                        case 'tel1':
                            $res_tel = $res_data['contact_value'];
                            break;
                        case 'cell1':
                            $res_cell = $res_data['contact_value'];
                            break;
                        case 'email':
                            $res_email = $res_data['contact_value'];
                            break;
                    }
                }
                
                
		$establishment_final[$establishment_code][0] = $line_counter;
		if (isset($s['code'])) $establishment_final[$establishment_code][1] = $establishment_code;
		if (isset($s['name'])) $establishment_final[$establishment_code][2] = $establishment_data['name'];
		if (isset($s['cou'])) $establishment_final[$establishment_code][3] = $establishment_data['country'];
		if (isset($s['pro'])) $establishment_final[$establishment_code][4] = $establishment_data['province'];
		if (isset($s['city'])) $establishment_final[$establishment_code][5] = $establishment_data['town'];
		if (isset($s['sub'])) $establishment_final[$establishment_code][6] = $establishment_data['suburb'];
		if (isset($s['reg'])) $establishment_final[$establishment_code][7] = isset($regions[0])?$regions[0]:'';
		if (isset($s['reg2'])) $establishment_final[$establishment_code][8] = isset($regions[1])?$regions[1]:'';
		if (isset($s['sta'])) $establishment_final[$establishment_code][9] = $establishment_data['qa_category'];
		if (isset($s['ass'])) $establishment_final[$establishment_code][10] = $establishment_data['fa_assessor'];
		if (isset($s['rep'])) $establishment_final[$establishment_code][11] = $establishment_data['fa_rep'];
		if (isset($s['date'])) $establishment_final[$establishment_code][12] = $establishment_data['qa_enter_date'];
		if (isset($s['out'])) $establishment_final[$establishment_code][13] = $qa_out;
		if (isset($s['current'])) $establishment_final[$establishment_code][14] = get_current_assessment($establishment_code);
		if (isset($s['last'])) $establishment_final[$establishment_code][15] = $last_assessment_data['last_assessment'];
		if (isset($s['next'])) $establishment_final[$establishment_code][16] = $last_assessment_data['next_assessment'];
		if (isset($s['inv'])) $establishment_final[$establishment_code][17] = $latest_invoice['inv'];
		if (isset($s['type'])) $establishment_final[$establishment_code][18] = $invoice_type;
		if (isset($s['pay'])) $establishment_final[$establishment_code][19] = $latest_invoice['pay'];
		if (isset($s['pdate'])) $establishment_final[$establishment_code][20] = $latest_invoice['pdate'];
		if (isset($s['grading'])) $establishment_final[$establishment_code][21] = $star_grading;
		if (isset($s['room'])) $establishment_final[$establishment_code][22] = $establishment_data['room_count'];
		if (isset($s['HL'])) $establishment_final[$establishment_code][23] = $sql_mc_data['HL'];
		if (isset($s['SC'])) $establishment_final[$establishment_code][24] = $sql_mc_data['SC'];
		if (isset($s['non'])) $establishment_final[$establishment_code][25] = $sql_mc_data['nonaccomm'];
		if (isset($s['HL2012'])) $establishment_final[$establishment_code][26] = $sql_mc_data['HL2012'];
		if (isset($s['HL2013'])) $establishment_final[$establishment_code][27] = $sql_mc_data['HL2013'];
		if (isset($s['HL2014'])) $establishment_final[$establishment_code][28] = $sql_mc_data['HL2014'];
		if (isset($s['HL2015'])) $establishment_final[$establishment_code][29] = $sql_mc_data['HL2015'];
		if (isset($s['SC2012'])) $establishment_final[$establishment_code][30] = $sql_mc_data['SC2012'];
		if (isset($s['SC2013'])) $establishment_final[$establishment_code][31] = $sql_mc_data['SC2013'];
		if (isset($s['SC2014'])) $establishment_final[$establishment_code][32] = $sql_mc_data['SC2014'];
		if (isset($s['SC2015'])) $establishment_final[$establishment_code][33] = $sql_mc_data['SC2015'];
		if (isset($s['Awards2012'])) $establishment_final[$establishment_code][34] = $sql_mc_data['Awards2012'];
		if (isset($s['Awards2013'])) $establishment_final[$establishment_code][35] = $sql_mc_data['Awards2013'];
		if (isset($s['Awards2014'])) $establishment_final[$establishment_code][36] = $sql_mc_data['Awards2014'];
		if (isset($s['Awards2015'])) $establishment_final[$establishment_code][37] = $sql_mc_data['Awards2015'];
		if (isset($s['SABest2012'])) $establishment_final[$establishment_code][38] = $sql_mc_data['SABest2012'];
		if (isset($s['SABest2013'])) $establishment_final[$establishment_code][39] = $sql_mc_data['SABest2013'];
		if (isset($s['SABest2014'])) $establishment_final[$establishment_code][40] = $sql_mc_data['SABest2014'];
		if (isset($s['SABest2015'])) $establishment_final[$establishment_code][41] = $sql_mc_data['SABest2015'];
		if (isset($s['Webonly'])) $establishment_final[$establishment_code][42] = $sql_mc_data['webonly'];
		if (isset($s['Webexpiry'])) $establishment_final[$establishment_code][43] = $sql_mc_data['Webexpirydate'];
		if (isset($s['resTel'])) $establishment_final[$establishment_code][44] = $res_tel;
		if (isset($s['resCell'])) $establishment_final[$establishment_code][45] = $res_cell;
		if (isset($s['resEmail'])) $establishment_final[$establishment_code][46] = $res_email;
		if (isset($s['conN'])) $establishment_final[$establishment_code][47] = $contact_data['name'];
		if (isset($s['conT'])) $establishment_final[$establishment_code][48] = $contact_data['tel'];
		if (isset($s['conC'])) $establishment_final[$establishment_code][49] = $contact_data['cell'];
		if (isset($s['conE'])) $establishment_final[$establishment_code][50] = $contact_data['email'];
		if (isset($s['conP'])) $establishment_final[$establishment_code][51] = $contact_data['postal_1'];
		if (isset($s['conP'])) $establishment_final[$establishment_code][52] = $contact_data['postal_2'];
		if (isset($s['conP'])) $establishment_final[$establishment_code][53] = $contact_data['postal_3'];
		if (isset($s['conP'])) $establishment_final[$establishment_code][54] = $contact_data['postal_code'];

		$estab_count++;
}

$estab_count = count($establishments);

$line_counter = 0;
$file_counter = 1;
foreach ($establishment_final as $establishment_code => $data) {
	if ($jL2 == 0) {
        if ($line_counter == 0) {
            $headers[0] = '';
            if (isset($s['code'])) $headers[1] = 'Code';
			if (isset($s['name'])) $headers[2] = 'Name';
			if (isset($s['cou'])) $headers[3] = 'Country';
			if (isset($s['pro'])) $headers[4] = 'Province';
			if (isset($s['city'])) $headers[5] = 'City';
			if (isset($s['sub'])) $headers[6] = 'Suburb';
			if (isset($s['reg'])) $headers[7] = 'Region 1';
			if (isset($s['reg'])) $headers[8] = 'Region 2';
			if (isset($s['sta'])) $headers[9] = 'QA Category';
			if (isset($s['ass'])) $headers[10] = 'Assessor';
			if (isset($s['rep'])) $headers[11] = 'Sales Rep';
			if (isset($s['date'])) $headers[12] = 'QA Enter Date';
			if (isset($s['out'])) $headers[13] = 'QA Out';
			if (isset($s['current'])) $headers[14] = 'Current Assessment Status';
			if (isset($s['last'])) $headers[15] = 'Last Completed Assessment';
			if (isset($s['next'])) $headers[16] = 'Next Assessment';
			if (isset($s['inv'])) $headers[17] = 'Last QA Invoice';
			if (isset($s['type'])) $headers[18] = 'Invoice Type';
			if (isset($s['pay'])) $headers[19] = 'Last QA Payment';
			if (isset($s['pdate'])) $headers[20] = 'Payment Date';
			if (isset($s['grading'])) $headers[21] = 'Star Grading';
			if (isset($s['room'])) $headers[22] = 'Room Count';
			if (isset($s['HL'])) $headers[23] = 'HL';
			if (isset($s['SC'])) $headers[24] = 'SC';
			if (isset($s['non'])) $headers[25] = 'Non-Accommodation';
			if (isset($s['HL2012'])) $headers[26] = 'HL2012';
			if (isset($s['HL2013'])) $headers[27] = 'HL2013';
			if (isset($s['HL2014'])) $headers[28] = 'HL2014';
			if (isset($s['HL2015'])) $headers[29] = 'HL2015';
			if (isset($s['SC2012'])) $headers[30] = 'SC2012';
			if (isset($s['SC2013'])) $headers[31] = 'SC2013';
			if (isset($s['SC2014'])) $headers[32] = 'SC2014';
			if (isset($s['SC2015'])) $headers[33] = 'SC2015';
			if (isset($s['Awards2012'])) $headers[34] = 'Awards2012';
			if (isset($s['Awards2013'])) $headers[35] = 'Awards2013';
			if (isset($s['Awards2014'])) $headers[36] = 'Awards2014';
			if (isset($s['Awards2015'])) $headers[37] = 'Awards2015';
			if (isset($s['SABest2012'])) $headers[38] = 'SABest2012';
			if (isset($s['SABest2013'])) $headers[39] = 'SABest2013';
			if (isset($s['SABest2014'])) $headers[40] = 'SABest2014';
			if (isset($s['SABest2015'])) $headers[41] = 'SABest2015';
			if (isset($s['Webonly'])) $headers[42] = 'webonly';
			if (isset($s['Webexpiry'])) $headers[43] = 'Webexpirydate';
			if (isset($s['resTel'])) $headers[44] = 'Reservation Tel';
			if (isset($s['resCell'])) $headers[45] = 'Reservation Cell';
			if (isset($s['resEmail'])) $headers[46] = 'Reservation Email';
			if (isset($s['conN'])) $headers[47] = 'Contact Name';
			if (isset($s['conT'])) $headers[48] = 'Contact Tel';
			if (isset($s['conC'])) $headers[49] = 'Contact Cell';
			if (isset($s['conE'])) $headers[50] = 'Contact Email';
			if (isset($s['conP'])) $headers[51] = 'Contact Postal #1';
			if (isset($s['conP'])) $headers[52] = 'Contact Postal #2';
			if (isset($s['conP'])) $headers[53] = 'Contact Postal #3';
			if (isset($s['conP'])) $headers[54] = 'Contact Postal Code';

            $fh = fopen($SDR."/apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv", 'w');
            fputcsv($fh, $headers);
            $files .= "<li><a href='/juno//apps/reports/files/{$_SESSION['j_user']['id']}_$file_counter.csv'>Report Part $file_counter.csv</a></li>";
        }



        fputcsv($fh, $data);

        if ($line_counter == 10000) {
            fclose($fh);
            $line_counter=-1;
            $file_counter++;
        }

		$line_counter++;
    } else {
		$r .= implode('~', $data);
		$r .= '|';
	}
}

$r = str_replace("\r", "", $r);
$r = str_replace("\n", "", $r);
$r = str_replace("\"", "", $r);
$r = str_replace("~|", "|", $r);
//echo $r;
//print_r(implode(',',$list));

echo "<html>";
echo "<link rel=stylesheet href=" . $ROOT . "/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/jsscripts/list_item_remover.js></script>";
echo "<script src=" . $ROOT . "/apps/reports/qa2/res.js></script>";

if ($jL2 == 0) {
    $page = "<body style='color:#999;padding:30 40 40 160;background:url(/juno/ico/set/spreadsheet.png) no-repeat 40px 40px'><tt style=width:260><hr>CSV Files Created. Right click to save.<ul>$files</ul></tt></body></html>";
    echo $page;
} else {
	echo "<script>J_in_r(\"" . $r . "\",\"" . $da . "\",$estab_count," . $jL1 . "," . $jL2 . ")</script>";
}






/*
 * Get the next assessment date for an establishment
 * Looks for the latest complete assessment and then fetches the next renewal date
 *
 */
function get_last_assessment($establishment_code) {
	$data['next_assessment'] = '';
	$data['last_assessment'] = '';

	$statement = "SELECT DATE_FORMAT(renewal_date, '%Y-%m-%d'), DATE_FORMAT(assessment_date, '%Y-%m-%d')
					FROM nse_establishment_assessment
					WHERE establishment_code=? AND assessment_status='complete'
					ORDER BY assessment_date DESC
					LIMIT 1";
	$sql_next = $GLOBALS['dbCon']->prepare($statement);
	$sql_next->bind_param('s', $establishment_code);
	$sql_next->execute();
	$sql_next->bind_result($next_assessment, $last_assessment);
	$sql_next->fetch();
	$sql_next->close();

	$data['next_assessment'] = $next_assessment;
	$data['last_assessment'] = $last_assessment;
	return $data;
}

/*
 * Get the current assessment status
 * Looks for the latest complete assessment and then fetches the next renewal date
 *
 */
function get_current_assessment($establishment_code) {
	$assessment_status = '';

	$statement = "SELECT assessment_status
					FROM nse_establishment_assessment
					WHERE establishment_code=? AND assessment_status!='complete'
					LIMIT 1";
	$sql_next = $GLOBALS['dbCon']->prepare($statement);
	$sql_next->bind_param('s', $establishment_code);
	$sql_next->execute();
	$sql_next->bind_result($assessment_status);
	$sql_next->fetch();
	$sql_next->close();

	return $assessment_status;
}

/**
 * Get last QA Out date if available
 * @param string $establishment_code
 */
function get_qa_out($establishment_code) {
	$out_date = '';
	$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=? ORDER BY cancelled_date DESC LIMIT 1";
	$sql_qa = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa->bind_param('s', $establishment_code);
	$sql_qa->execute();
	$sql_qa->bind_result($out_date);
	$sql_qa->fetch();
	$sql_qa->free_result();
	$sql_qa->close();

	if ($out_date == '0000-00-00') $out_date = '';

	return $out_date;
}

function get_contact($establishment_code) {
	$contact_data['name'] = '';
	$contact_data['tel'] = '';
	$contact_data['cell'] = '';
	$contact_data['email'] = '';
	$contact_data['postal'] = '';

	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), phone, cell, email
					FROM nse_user_establishments AS a
					LEFT JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=?
					ORDER BY a.qa_contact
					LIMIT 1";
	$sql_contact = $GLOBALS['dbCon']->prepare($statement);
	$sql_contact->bind_param('s', $establishment_code);
	$sql_contact->execute();
	$sql_contact->bind_result($contact_name, $tel, $cell, $email);
	$sql_contact->fetch();
	$sql_contact->close();

	//Postal Address
	$statement = "SELECT b.reservation_postal1, b.reservation_postal2, b.reservation_postal3, b.reservation_postal_code,
						a.postal_address_line1, a.postal_address_line2, a.postal_address_line3,	a.postal_address_code
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_reservation AS b ON a.establishment_code=b.establishment_code
					WHERE a.establishment_code=?
					LIMIT 1";
	$sql_postal = $GLOBALS['dbCon']->prepare($statement);
	$sql_postal->bind_param('s', $establishment_code);
	$sql_postal->execute();
	$sql_postal->bind_result($reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code,
						$postal_address_line1, $postal_address_line2, $postal_address_line3, $postal_address_code);
	$sql_postal->fetch();
	$sql_postal->close();
	$reservation_postal1 = trim($reservation_postal1);
	$reservation_postal2 = trim($reservation_postal2);
	$reservation_postal3 = trim($reservation_postal3);
	$reservation_postal_code = trim($reservation_postal_code);
	$postal_address_line1 = trim($postal_address_line1);
	$postal_address_line2 = trim($postal_address_line2);
	$postal_address_line3 = trim($postal_address_line3);
	$postal_address_code = trim($postal_address_code);

	//Remove trailing comma
	if (strpos($postal_address_line1, ',') > 0) $postal_address_line1 = substr($postal_address_line1, 0, -1);
	if (strpos($postal_address_line2, ',') > 0) $postal_address_line2 = substr($postal_address_line2, 0, -1);
	if (strpos($postal_address_line3, ',') > 0) $postal_address_line3 = substr($postal_address_line3, 0, -1);
	if (strpos($reservation_postal1, ',') > 0) $reservation_postal1 = substr($reservation_postal1, 0, -1);
	if (strpos($reservation_postal2, ',') > 0) $reservation_postal2 = substr($reservation_postal2, 0, -1);
	if (strpos($reservation_postal3, ',') > 0) $reservation_postal3 = substr($reservation_postal3, 0, -1);

	//Remove trailing dot
	if (strpos($reservation_postal_code, '.') > 0) $reservation_postal_code = substr($reservation_postal_code, 0, -1);
	if (strpos($postal_address_code, '.') > 0) $postal_address_code = substr($postal_address_code, 0, -1);

	//Assemble array
	$contact_data['name'] = $contact_name;
	$contact_data['tel'] = $tel;
	$contact_data['cell'] = $cell;
	$contact_data['email'] = $email;
	if (empty($reservation_postal1)) {
		if (!empty($postal_address_line1)) $contact_data['postal'] = "$postal_address_line1";
		if (!empty($postal_address_line2)) $contact_data['postal'] .= ", $postal_address_line2";
		if (!empty($postal_address_line3)) $contact_data['postal'] .= ", $postal_address_line3";
		if (!empty($postal_address_code)) $contact_data['postal'] .= ", $postal_address_code";

		$contact_data['postal_1'] = $postal_address_line1;
		$contact_data['postal_2'] = $postal_address_line2;
		$contact_data['postal_3'] = $postal_address_line3;
		$contact_data['postal_code'] = $postal_address_code;
	} else {
		if (!empty($reservation_postal1)) $contact_data['postal'] = "$reservation_postal1";
		if (!empty($reservation_postal2)) $contact_data['postal'] .= ", $reservation_postal2";
		if (!empty($reservation_postal3)) $contact_data['postal'] .= ", $reservation_postal3";
		if (!empty($reservation_postal_code)) $contact_data['postal'] .= ", $reservation_postal_code";

		$contact_data['postal_1'] = $reservation_postal1;
		$contact_data['postal_2'] = $reservation_postal2;
		$contact_data['postal_3'] = $reservation_postal3;
		$contact_data['postal_code'] = $reservation_postal_code;

	}

	return $contact_data;
}

function get_restype($establishment_code) {
	$star_grading = '';

	//Prepare statement - restype/star grading
	$statement = "SELECT IF(star_grading=0,'',star_grading)
				FROM nse_establishment_restype
				WHERE establishment_code=?
				ORDER BY star_grading
				LIMIT 1";
	$sql_restype = $GLOBALS['dbCon']->prepare($statement);
	$sql_restype->bind_param('s', $establishment_code);
	$sql_restype->execute();
	$sql_restype->bind_result($star_grading);
	$sql_restype->fetch();
	$sql_restype->close();

	return $star_grading;
}

function get_invoice($establishment_code) {
	$latest_invoice = array('inv'=>'', 'pay'=>'', 'type'=>'','pdate'=>'');

	$statement = "SELECT a.j_inv_total, a.j_inv_paid, a.j_inv_type, d.j_invp_date
				FROM nse_invoice AS a
				LEFT JOIN nse_invoice_item AS b ON a.j_inv_id=b.j_invit_invoice
				LEFT JOIN nse_inventory AS c ON b.j_invit_inventory=c.j_in_id
				LEFT JOIN nse_invoice_item_paid AS d ON b.j_invit_id=d.j_invp_invoice_item
				WHERE a.j_inv_to_company=?
					AND (c.j_in_category=5 OR c.j_in_category=6 OR c.j_in_category=13 OR c.j_in_category=15 OR c.j_in_category=16)
					AND (a.j_inv_type = 1 OR j_inv_type=2)
				ORDER BY a.j_inv_date DESC
				LIMIT 1";
	$sql_invoices = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_invoices->bind_param('s', $establishment_code);
	$sql_invoices->execute();
	$sql_invoices->bind_result($invoice_total, $invoice_paid, $invoice_type, $payment_date);
	$sql_invoices->fetch();
	$sql_invoices->close();

	if (!empty($invoice_total)) {
		$latest_invoice['inv'] = number_format($invoice_total,2,'.','');
		$latest_invoice['pay'] = number_format($invoice_paid,2,'.','');
		$latest_invoice['type'] = $invoice_type;
		$latest_invoice['pdate'] = date('Y-m-d', $payment_date);
	}


	return $latest_invoice;
}


/*
 * Get region list for an establishment
 */
function get_regions($establishment_code) {
	$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
	$sql_region_locations = $GLOBALS['dbCon']->prepare($statement);
    $sql_region_locations->bind_param('s', $establishment_code);
    $sql_region_locations->execute();
    $sql_region_locations->bind_result($country_id, $province_id, $town_id, $suburb_id);
    $sql_region_locations->fetch();
    $sql_region_locations->free_result();

	$statement = "SELECT a.region_id, a.region_name
					FROM nse_nlocations_regions AS a
					LEFT JOIN nse_nlocations_regions_content AS b ON a.region_id=b.region_id
					WHERE b.location_type=? && location_id=?";
	$sql_region = $GLOBALS['dbCon']->prepare($statement);

    $region_list = array();

     if (!empty($province_id) || $province_id != 0) {
        $location_type = 'province';
        $sql_region->bind_param('si', $location_type, $province_id);
        $sql_region->execute();
        $sql_region->store_result();
        $sql_region->bind_result($region_id, $region_name);
        while ($sql_region->fetch()) {
            $region_list[] = $region_name;
        }
        $sql_region->free_result();
    }

	if (!empty($town_id) || $town_id != 0) {
        $location_type = 'town';
        $sql_region->bind_param('si', $location_type, $town_id);
        $sql_region->execute();
        $sql_region->store_result();
        $sql_region->bind_result($region_id, $region_name);
        while ($sql_region->fetch()) {
            $region_list[] = $region_name;
        }
        $sql_region->free_result();
    }

	if (!empty($suburb_id) || $suburb_id != 0) {
        $location_type = 'suburb';
        $sql_region->bind_param('si', $location_type, $suburb_id);
        $sql_region->execute();
        $sql_region->store_result();
        $sql_region->bind_result($region_id, $region_name);
        while ($sql_region->fetch()) {
            $region_list[] = $region_name;
        }
        $sql_region->free_result();
    }

    return $region_list;
}


?>