<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";

//Get list
$statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) AS assessor 
				FROM nse_user
				WHERE roles LIKE '%a%'
				ORDER BY assessor";
$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
$sql_assessor->execute();
$sql_assessor->bind_result($assessor_id, $assessor);
while ($sql_assessor->fetch()) {
	echo "$assessor_id~$assessor|";
}
$sql_assessor->close();

?>
