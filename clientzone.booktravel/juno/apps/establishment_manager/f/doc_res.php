<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/dir.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

$id=$_GET["id"];
$get=$_GET["get"];

if($get=="acc")
{
	J_PrepFile("/stuff/establishments/".$id."/files/accounts");
	$browse_dir="/stuff/establishments/".$id."/files/accounts";
	$r=$_SESSION["j_user"]["role"];
	$browse_edit=1;
	if($r=="a" || $r=="b" || $r=="d" || $r=="r" || $r=="s")
		$all_acts=1;
	elseif($r=="c")
		$upload=1;
	include $SDR."/utility/browse/list.php";
}

elseif($get=="certif")
{
	echo "<h5>Assessment Certificates</h5>";
	$q="SELECT * FROM nse_establishment_assessment WHERE establishment_code='".$id."' AND assessment_status='complete' ORDER BY assessment_date DESC";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		echo "<br><blockquote>";
		include $SDR."/apps/assessment_manager/f/create_functions.php";
		while($g=mysql_fetch_array($q))
		{
			$f=create_certificate($g["assessment_id"]);
			echo "<a href=http://".$_SERVER["SERVER_NAME"].$f." target=_blank onfocus=blur()><b>".substr($g["assessment_date"],0,10)."</b> (".J_size(filesize(DOC_ROOT.$f)).")</a>";
		}
		echo "</blockquote>";
	}
	else
		echo "<hr><tt><var><b>ATTENTION!</b> There are no certificates for this establishment.</var><br>Contact AA Travel for queries.</tt>";
}

elseif($get=="report")
{
	echo "<h5>Assessment Report</h5>";
	$q="SELECT * FROM nse_establishment_assessment WHERE establishment_code='".$id."' AND assessment_status='complete' ORDER BY assessment_date DESC";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		echo "<br><blockquote>";
		include $SDR."/apps/assessment_manager/f/create_functions.php";
		while($g=mysql_fetch_array($q))
		{
			$f=create_report($g["assessment_id"]);
			echo "<a href=http://".$_SERVER["SERVER_NAME"].$f." target=_blank onfocus=blur()><b>".substr($g["assessment_date"],0,10)."</b> (".J_size(filesize(DOC_ROOT.$f)).")</a>";
		}
		echo "</blockquote>";
	}
	else
		echo "<hr><tt><var><b>ATTENTION!</b> There are no reports for this establishment.</var><br>Contact AA Travel for queries.</tt>";
}

elseif($get=="logo")
{
	echo "<h5>AA Logos</h5>";
	$q=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment AS a LEFT JOIN nse_establishment_restype AS r ON a.establishment_code=r.establishment_code WHERE a.establishment_code='".$id."' LIMIT 1"));
	$q=$q["aa_category_code"];
	if($q=="H/S" || $q=="HR" || $q=="R" || $q=="R/H" || $q=="S")
	{
		echo "<br><blockquote>";
		$q=mysql_fetch_array(mysql_query("SELECT aa_category_name FROM nse_aa_category WHERE aa_category_code='".$q."' LIMIT 1"));
		$q=$q["aa_category_name"];
		$i=0;
		$l=1;
		$f="/stuff/qa_logos/".str_replace(array(" ","/"),"_",$q).".jpg";
		if(file_exists($SDR.$f))
		{
			$i=$ROOT.$f;
			echo "<a href='".$ROOT."/utility/force_download.php?file=".$ROOT.$f."' onmouseover=\"J_TT(this,'<img src=".$i.">')\" onfocus=blur()><b>".$q."</b> - Low Resolution (".J_size(filesize($SDR.$f)).")</a>";
			$l=0;
		}
		$f="/stuff/qa_logos/".str_replace(array(" ","/"),"_",$q)."_1.jpg";
		if(file_exists($SDR.$f))
		{
			echo "<a href='".$ROOT."/utility/force_download.php?file=".$ROOT.$f."'".($i?" onmouseover=\"J_TT(this,'<img src=".$i.">')\"":"")." onfocus=blur()><b>".$q."</b> - High Resolution (".J_size(filesize($SDR.$f)).")</a>";
			$l=0;
		}
		if($l)
			echo "<hr><tt><var><b>WARNING!</b> No logos found for this category name. (Code: ".($q?$q:"Unspecified").").</var><br>Contact AA Travel for queries.</tt>";
		echo "</blockquote>";
	}
	else
		echo "<hr><tt><var><b>ATTENTION!</b> This establishment does not qualify for use of the AA Quality Assurance logo. (Code: ".($q?$q:"Unspecified").").</var><br>Contact AA Travel for queries.</tt>";
}

elseif($get=="misc")
{
	J_PrepFile("/stuff/establishments/".$id."/files");
	J_PrepFile("/stuff/establishments/".$id."/files/accounts");
	J_PrepFile("/stuff/establishments/".$id."/files/documents");
	J_PrepFile("/stuff/establishments/".$id."/files/legal");
	J_PrepFile("/stuff/establishments/".$id."/files/images");
	$browse_dir="/stuff/establishments/".$id."/files";
	$r=$_SESSION["j_user"]["role"];
	$browse_edit=1;
	if($r=="a" || $r=="b" || $r=="d" || $r=="r" || $r=="s")
		$all_acts=1;
	elseif($r=="c")
		$upload=1;
	include $SDR."/utility/browse/list.php";
}

echo "</body></html>";
?>