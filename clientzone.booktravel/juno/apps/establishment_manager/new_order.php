<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/f/order.js></script>";
echo "<body>";
echo "<form method=post>";

echo "<table id=jTR class=detail>";
echo "<tr><th>Date</th><td><input type=text name=od value=".date("d/m/Y",$EPOCH)." onclick=j_P.J_datePicker(self,3)></td></tr>";
echo "<tr><th>Consultant</th><td><input type=text><input type=hidden name=cs></td></tr>";
echo "<tr><th>Accounts person</th><td><input type=text><input type=hidden name=ap></td></tr>";
echo "<tr><th>Contact for proofs</th><td><input type=text><input type=hidden name=cp></td></tr>";
echo "<tr><th>Special Requests</th><td><textarea name=sp rows=6 class=jW100></textarea></td></tr>";
echo "</table>";
echo "<br>";

echo "<table id=jTR class=prod>";
echo "<tr class=hed><th class=jdbO>Qty</th><th width=99%>Item</th><th class=jdbK></th><th>Price</th><th class=jdbC>Sub</th><th class=jdbR>VAT</th><th class=jdbG>Total</th></tr>";
echo "<tbody id=bod>";
echo "<tr><th class=jdbO><input type=text name=ofp value=0 onkeyup=Calc()></th><td><b>Full Page</b></td><td class=jdbK>R</td><th>14 500.00</th><th class=jdbC></th><th class=jdbR></th><th class=jdbG></th></tr>";
echo "<tr><th class=jdbO><input type=text name=otqp value=0 onkeyup=Calc()></th><td><b>Three Quarters</b></td><td class=jdbK>R</td><th>11 600.00</th><th class=jdbC></th><th class=jdbR></th><th class=jdbG></th></tr>";
echo "<tr><td class=jdbO><input type=text name=ohp value=0 onkeyup=Calc()></th><td><b>Half page</b></td><td class=jdbK>R</td><th>8 700.00</th><th class=jdbC></th><th class=jdbR></th><th class=jdbG></th></tr>";
echo "<tr><th class=jdbO><input type=text name=oqp value=0 onkeyup=Calc()></th><td><b>Quarter page</b></td><td class=jdbK>R</td><th>4 900.00</th><th class=jdbC></th><th class=jdbR></th><th class=jdbG></th></tr>";
echo "<tr><th class=jdbO><input type=text name=oofp value=0 onkeyup=Calc()></th><td><b>One/fifth page</b></td><td class=jdbK>R</td><th>3 900.00</th><th class=jdbC></th><th class=jdbR></th><th class=jdbG></th></tr>";
echo "</tbody>";
echo "<tbody>";
echo "<tr id=tot><td class=jdbO>0 &times;</td><td>Items</td><td class=jdbK>R</td><th></th><th class=jdbC>00.00</th><th class=jdbR>00.00</th><th class=jdbG>00.00</th></tr>";
echo "</tbody>";
echo "</table>";
echo "<br><br>";
echo "<input type=hidden name=vat value=14>";

echo "<tt><b>Terms and Conditions</b></tt>";
echo "<ol class=foot>
<li>Material to be supplied in digital format as PDF or JPEG at 300dpi
<li>A maximum of two black and white proofs will be supplied to the client for approval. Changes to specifications after the proof is supplied will be charged according to complexity and cost. After approval of the supplied proof, or if no response is received by the due date of the proof, the client will have no recourse against AA TRAVEL GUIDES for errors or omissions.
<li>RATES QUOTED ON THIS APPLICATION FORM ARE IN SOUTH AFRICAN RAND AND SHOW 14% VAT (NO VAT APPLICABLE FOR APPLICANTS OUTSIDE SOUTH AFRICA). INVOICES ARE PAYABLE ON PRESENTATION.
<li>RATES QUOTED EXCLUDE ADVERTISING AGENCY COMMISSION PAYABLE TO YOUR AGENCY WHICH MUST BE ADDED TO THE TOTALS ABOVE. THE TOTAL VALUE OF THE ORDER ABOVE WILL BECOME THE �CONTRACT SUM� WHEN THIS ORDER IS ACCEPTED BY AA TRAVEL GUIDES.
<li>A non-refundable prepayment of 50% (fifty percent) of the total value of the order applied for is required by the due date. The balance of the final order is payable after the publication of the guide. Money must be paid directly into the bank account of AA Travel Guides, Nedbank Norwood, Branch Code: 191905, Account Number: 1919554998
<li>Please fax a copy of the deposit slip or notification of transfer to (011) 728-3086 clearly indicating your establishment name and telephone numbers.
<li>Copy and artwork must be supplied by the client/establishment/company. If copy and artwork is not received within 21 (twenty-one) days after
<li>The Publisher receive this application, then The Publisher reserve the right to compile information from any available source, in which case a further 5% penalty will be levied on the contract sum.
<li>The Publisher cannot be held responsible for the quality of printed material supplied to the client and reserves the right not to publish any material considered unsuitable. The Publisher will not be liable if the publication is delayed nor shall its liability for whatever reason exceed the value of the contract sum. In addition the validity of this contract will not be effected by a delayed or late publication.
<li>The client expressly admits that this document will become a valid contract, whether hand delivered, posted, faxed or e-mailed, when it is received and accepted by The Publisher and that the contract sum above will be due and payable. The photo and listing proof will not affect the validity of this contract and it is expressly accepted that the photo and listing proof are not a corollary to this contract. The content of this paragraph is limited by the provision that no charge will be levied for a cancellation within 7 (seven) days of receipt of this application by The Publisher. Thereafter the full contract sum will be due and payable.
<li>The client accepts: (1) The above address as its domicilium citandi et executandi for the service of all process; (2) Interest on all overdue amounts at 18% p.a. compounded; (3) Legal charges on an attorney-and-client scale and the necessary cost of tracing; the person completing and signing this form will be personally liable in solidum if the client cannot or will not pay the contract sum.
<li>Accommodation clients acknowledges having read the AA Quality Assured Terms and Conditions as published on booktravel.travel and agrees to be bound by them.
</ol><hr>";
echo "<tt><b>I/We, the client, hereby apply to have my/our establishments included in the AA Quality Assured Pocket Directory (2010 International Edition)</b></tt><hr>";
echo "<input type=submit value=OK>";
echo "</form>";
echo "<script>J_tr()</script>";

$J_title2="Establishment Manager";
$J_title1="New Order";
$J_width=640;
$J_height=680;
$J_icon="<img src=".$ROOT."/ico/set/spreadsheet_yellow_edit.png>";
$J_tooltip="Place an ad in our publication or internet services";
$J_label=22;
$J_home=3;
$J_nostart=1;
$J_group=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>