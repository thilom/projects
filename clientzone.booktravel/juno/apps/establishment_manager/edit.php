<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
$URL="/apps/establishment_manager/sections/overview.php";
$view=(isset($_GET["view"])?1:0);

if(isset($_GET["id"]) && (!$view || ((count($_SESSION["j_user"]["estabs"])<2 && ($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="c" || $_SESSION["j_user"]["role"]=="a" || $_SESSION["j_user"]["role"]=="r")))))
{
	$j_IF=1;
	include $SDR."/apps/establishment_manager/sections/overview.php";
//	die();
}
else
{
	if($_SESSION["j_user"]["role"]=="b" || $_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s")
		echo "<script>staff=1</script>";

	if($_SESSION["j_user"]["role"]=="a")
		echo "<script>ass=1</script>";
	
	if($_SESSION["j_user"]["role"]=="c")
		echo "<script>client=1</script>";

	include $SDR."/apps/establishment_manager/sections/menu.php";
}

$J_title1="Establishment Editor";
$J_width=1000;
$J_height=800;
$J_framesize=300;
$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
$J_tooltip="Update your establishment details";
$J_label=22;
$J_home=0;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>
