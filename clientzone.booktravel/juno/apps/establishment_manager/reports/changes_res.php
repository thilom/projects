<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$jL1=(isset($_GET["jL1"])?$_GET["jL1"]:0);
$jL2=(isset($_GET["jL2"])?$_GET["jL2"]:100);
$d="";

//Prepare statement - change dates
$statement = "SELECT a.update_date, CONCAT(b.firstname, ' ', b.lastname)
				FROM nse_establishment_updates AS a
				LEFT JOIN nse_user AS b ON a.changed_by=b.user_id
				WHERE a.establishment_code=?
				ORDER BY a.update_date DESC
				LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

$s = "SELECT DISTINCT a.establishment_code, b.establishment_name
		FROM nse_establishment_updates AS a
		JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code";


$rs=mysql_query($s);
$jL0 = mysql_num_rows($rs);
$r="";
$e="";
$p="";
if(mysql_num_rows($rs))
{
	
	$pp=array();
	$ee=array();
	while($g=mysql_fetch_array($rs))
	{
		if(!isset($ee[$g["establishment_code"]]))
		{
			$r.=$g["establishment_code"]."~{$g['establishment_name']}~";
		} else {
			$r.="~~";
		}
		
		//Get last update
		$update_date = '';
		$changed_by = '';
		$sql_data->bind_param('s', $g['establishment_code']);
		$sql_data->execute();
		$sql_data->bind_result($update_date, $changed_by);
		$sql_data->fetch();
				
		$r.=substr($update_date,0,16)."~";
//		$r.=ucwords(strtolower(str_replace("_"," ",$g["field_name"])))."~";
//		$r.="~";
		$r.=$changed_by."~";
//		if(!isset($pp[$changed_by]))
//			$pp[$g["changed_by"]]=J_Value("","people","",$changed_by);
		$r.="|";
	}
	$r=str_replace("\r","",$r);
	$r=str_replace("\n","",$r);
	$r=str_replace("\"","",$r);
	$r=str_replace("~|","|",$r);
	foreach($ee as $k => $v)
		$e.=$k."~".preg_replace("~[^a-zA-Z0-9 '\-]~","",$v)."|";
	foreach($pp as $k => $v)
		$p.=$k."~".preg_replace("~[^a-zA-Z0-9 '\-]~","",$v)."|";
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/reports/changes_res.js></script>";
echo "<script>J_in_r(\"".$r."\",\"".$e."\",\"".$p."\",".$jL0.",".$jL1.",".$jL2.")</script>";
?>