<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/parse.php";
include $SDR."/system/get.php";

$co=$_GET["co"];
$mn=!isset($_GET["ds"])?$_GET["ds"]:date("d/m/Y",mktime(0,0,0,date("m")-3,date("d"),date("Y")));
$mx=!isset($_GET["de"])?$_GET["de"]:date("d/m/Y",mktime(0,0,0,date("m"),date("d")+1,date("Y")));

if(preg_replace("~[0-9]~","",$mn)<200001010000 || preg_replace("~[0-9]~","",$mn)=="")
	$mn=date("d/m/Y",mktime(0,0,0,date("m")-2,date("d"),date("Y")));
if(preg_replace("~[0-9]~","",$mx)<200001010000 || preg_replace("~[0-9]~","",$mx)=="")
	$mx=date("d/m/Y",mktime(0,0,0,date("m"),date("d")+1,date("Y")));

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=http://www.google.com/jsapi></script>";
echo "<script>j_P.J_resize(j_W,'20,-1')</script><body>";
$c="<tt>";
$c.="Establishment: <b>".J_Value("establishment_name","nse_establishment","establishment_code",$co)."</b> (".$co.")";
$c.=" | ".$mn;
$c.=" | ".$mx;
$c.=" | Drawn by:".J_Value("","people","",$_SESSION["j_user"]["id"]);
$c.="</tt>";
echo $c;

$mn=J_dateParse($mn);
$mx=J_dateParse($mx);

$d=date("d",$mn);
$m=date("m",$mn);
$y=date("Y",$mn);


echo "<hr><br><h4>User Search Results</h4>";
$q="";
$i=0;
$yc=0;
$mc=0;
$t=0;
$de=0;
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m),date($d)+$i,date($y));
	$de=mktime(0,0,0,date($m),date($d)+$i+1,date($y));
	if($yc!=date("Y",$ds))
	{
		$yc=date("Y",$ds);
		$q.="d.setValue(".$i.",0,'".date("Y M d",$ds)."');";
	}
	elseif($mc!=date("M",$ds))
	{
		$mc=date("M",$ds);
		$q.="d.setValue(".$i.",0,'".date("M d",$ds)."');";
	}
	else
		$q.="d.setValue(".$i.",0,'".date("d",$ds)."');";
	$s=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_searchlog WHERE search_time>'".date("Y-m-d H:i:s",$ds)."' AND search_time<'".date("Y-m-d H:i:s",$de)."' AND establishment_code='".$co."'"));
	$q.="d.setValue(".$i.",1,".$s[0].");";
	$t+=$s[0];
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
echo "d.addColumn('number','Total Search Results ".$t."');";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c3'));c.draw(d,{width:".($i*30).",height:320,legend:'right',title:'',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";
echo "<span id=c3></span>";



echo "<hr><br><h4>User Views</h4>";
$q="";
$i=0;
$yc=0;
$mc=0;
$t=0;
$de=0;
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m),date($d)+$i,date($y));
	$de=mktime(0,0,0,date($m),date($d)+$i+1,date($y));
	if($yc!=date("Y",$ds))
	{
		$yc=date("Y",$ds);
		$q.="d.setValue(".$i.",0,'".date("Y M d",$ds)."');";
	}
	elseif($mc!=date("M",$ds))
	{
		$mc=date("M",$ds);
		$q.="d.setValue(".$i.",0,'".date("M d",$ds)."');";
	}
	else
		$q.="d.setValue(".$i.",0,'".date("d",$ds)."');";
	$s=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_view_logs WHERE view_time>'".date("Y-m-d H:i:s",$ds)."' AND view_time<'".date("Y-m-d H:i:s",$de)."' AND establishment_code='".$co."'"));
	$q.="d.setValue(".$i.",1,".$s[0].");";
	$t+=$s[0];
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
echo "d.addColumn('number','Total Views ".$t."');";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c2'));c.draw(d,{width:".($i*30).",height:320,legend:'right',title:'',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";
echo "<span id=c2></span>";



echo "<hr><br><h4>User Click-Through Frequency</h4>";
$q="";
$i=0;
$yc=0;
$mc=0;
$t=0;
$de=0;
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m),date($d)+$i,date($y));
	$de=mktime(0,0,0,date($m),date($d)+$i+1,date($y));
	if($yc!=date("Y",$ds))
	{
		$yc=date("Y",$ds);
		$q.="d.setValue(".$i.",0,'".date("Y M d",$ds)."');";
	}
	elseif($mc!=date("M",$ds))
	{
		$mc=date("M",$ds);
		$q.="d.setValue(".$i.",0,'".date("M d",$ds)."');";
	}
	else
		$q.="d.setValue(".$i.",0,'".date("d",$ds)."');";
	$s=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_clickthru_log WHERE clickthru_date>'".date("Y-m-d H:i:s",$ds)."' AND clickthru_date<'".date("Y-m-d H:i:s",$de)."' AND establishment_code='".$co."'"));
	$q.="d.setValue(".$i.",1,".$s[0].");";
	$t+=$s[0];
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
echo "d.addColumn('number','Total Click-throughs ".$t."');";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c0'));c.draw(d,{width:".($i*30).",height:320,legend:'right',title:'',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";
echo "<span id=c0></span>";



echo "<hr><br><h4>User Enquiries</h4>";
$q="";
$i=0;
$yc=0;
$mc=0;
$t=0;
$de=0;
while($de<=$mx)
{
	$ds=mktime(0,0,0,date($m),date($d)+$i,date($y));
	$de=mktime(0,0,0,date($m),date($d)+$i+1,date($y));
	if($yc!=date("Y",$ds))
	{
		$yc=date("Y",$ds);
		$q.="d.setValue(".$i.",0,'".date("Y M d",$ds)."');";
	}
	elseif($mc!=date("M",$ds))
	{
		$mc=date("M",$ds);
		$q.="d.setValue(".$i.",0,'".date("M d",$ds)."');";
	}
	else
		$q.="d.setValue(".$i.",0,'".date("d",$ds)."');";
	$s=mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM nse_establishment_enquiry WHERE enquiry_date>'".date("Y-m-d H:i:s",$ds)."' AND enquiry_date<'".date("Y-m-d H:i:s",$de)."' AND establishment_code='".$co."'"));
	$q.="d.setValue(".$i.",1,".$s[0].");";
	$t+=$s[0];
	$i++;
}
echo "<script>google.load('visualization','1',{packages:['areachart']});google.setOnLoadCallback(drawChart);function drawChart(){var d=new google.visualization.DataTable();";
echo "d.addColumn('string','Months');";
echo "d.addColumn('number','Total Enquiries ".$t."');";
echo "d.addRows(".$i.");";
echo $q;
echo "var c=new google.visualization.AreaChart($('c1'));c.draw(d,{width:".($i*30).",height:320,legend:'right',title:'',axisFontSize:'11pt',tooltipWidth:'120',pointSize:4,legendFontSize:'11pt'});}</script>";
echo "<span id=c1></span>";



echo "</body></html>";
?>