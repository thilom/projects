<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/reports/changes.js></script>";
if(!isset($_GET["id"]) && isset($_GET["j_IF"]))
{
	echo "<script>J_in_s()</script>";
	die();
}
echo "<body class=j_List>";
echo "<script>J_in_N(".($_SESSION["j_user"]["role"]=="b" || $_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s"?1:0);
if(isset($_GET["id"]))
{
	include $SDR."/system/get.php";
	echo ",'".$_GET["id"]."',\"".J_Value("","establishment","",$_GET["id"])."\"";
	echo ",'".date("d/m/Y",mktime(0,0,0,date("m")-3,date("d"),date("Y")))."'";
	echo ",'".date("d/m/Y",mktime(0,0,0,date("m"),date("d")+1,date("Y")))."'";
}
echo ")</script>";
$J_home=0;
$J_title1="Pending Changes";
$J_title2="Establishment Manager";
$J_icon="<img src=".$ROOT."/ico/set/spreadsheet.png><var><img src=".$ROOT."/ico/set/chart.png></var>";
$J_label=22;
$J_width=1440;
$J_framesize=260;
$J_frame1=$ROOT."/apps/establishment_manager/reports/user_activity.php";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>