<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

if (count($_POST) > 0) {
	
	$statement = "DELETE FROM nse_establishment_specials WHERE specials_id=? LIMIT 1";
	$sql_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete->bind_param('i', $_GET['id']);
	$sql_delete->execute();

	echo "<script>parent.J_WX({$_GET['j_W']})</script>";
	
	die();
}


	$title2 = 'Delete Promotion';
	//Get specials Data
	$statement = "SELECT a.establishment_code, a.specials_start, a.specials_end, a.specials_name, a.specials_description, a.aa_members_only, b.establishment_name 
					FROM nse_establishment_specials AS a
					LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE a.specials_id=?
					LIMIT 1";
	$sql_special = $GLOBALS['dbCon']->prepare($statement);
	$sql_special->bind_param('i', $_GET['id']);
	$sql_special->execute();
	$sql_special->store_result();
	$sql_special->bind_result($establishment_code, $specials_start, $specials_end, $specials_name, $specials_description, $aa_members_only, $establishment_name);
	$sql_special->fetch();
	$sql_special->close();
	$aa_members_only = $aa_members_only=='1'?'checked':'';


$template = file_get_contents('delete_specials.html');

//Replace Tags
$template = str_replace('<!-- special_start -->', $specials_start, $template);
$template = str_replace('<!-- special_end -->', $specials_end, $template);
$template = str_replace('<!-- special_name -->', $specials_name, $template);
$template = str_replace('<!-- special_description -->', $specials_description, $template);
$template = str_replace('<!-- aa_members_only -->', $aa_members_only, $template);

echo $template;

$J_home=7;
$J_title1="$establishment_name";
$J_title2=$title2;
$J_icon="<img src=".$ROOT."/ico/set/file_drawer.png>";
$J_label=22;
$J_width=800;
$J_height=400;
$J_framesize=260;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>