<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

if (count($_POST) > 0) {
	$aa_members_only = isset($_POST['aa_members_only'])?'1':'';
	
	if ($_GET['id'] == 'n') {
		$statement = "INSERT INTO nse_establishment_specials 
						(specials_start, specials_end, specials_name, specials_description, aa_members_only, establishment_code)
						VALUES 
						(?,?,?,?,?,?)";
		$sql_save = $GLOBALS['dbCon']->prepare($statement);
		$sql_save->bind_param('ssssss', $_POST['special_start'], $_POST['special_end'], $_POST['special_name'], $_POST['special_description'], $aa_members_only, $_GET['eCode']);
		$sql_save->execute();
		$sql_save->close();
	} else {
		$statement = "UPDATE nse_establishment_specials 
						SET specials_start=?, specials_end=?, specials_name=?, specials_description=?, aa_members_only=?
						WHERE specials_id=?";
		$sql_save = $GLOBALS['dbCon']->prepare($statement);
		$sql_save->bind_param('ssssss', $_POST['special_start'], $_POST['special_end'], $_POST['special_name'], $_POST['special_description'], $aa_members_only, $_GET['id']);
		$sql_save->execute();
	}
	
	echo "<script>parent.J_WX({$_GET['j_W']})</script>";
	
	die();
}

if ($_GET['id'] == 'n') {
	$specials_start='';
	$specials_end='';
	$specials_name='';
	$specials_description='';
	$aa_members_only='';
	$title2 = 'Add Promotion';
	
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
	$sql_establishment = $GLOBALS['dbCon']->prepare($statement);
	$sql_establishment->bind_param('s', $_GET['eCode']);
	$sql_establishment->execute();
	$sql_establishment->bind_result($establishment_name);
	$sql_establishment->fetch();
	$sql_establishment->close();
	
} else {
	$title2 = 'Edit Promotion';
	//Get specials Data
	$statement = "SELECT a.establishment_code, a.specials_start, a.specials_end, a.specials_name, a.specials_description, a.aa_members_only, b.establishment_name 
					FROM nse_establishment_specials AS a
					LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
					WHERE a.specials_id=?
					LIMIT 1";
	$sql_special = $GLOBALS['dbCon']->prepare($statement);
	$sql_special->bind_param('i', $_GET['id']);
	$sql_special->execute();
	$sql_special->store_result();
	$sql_special->bind_result($establishment_code, $specials_start, $specials_end, $specials_name, $specials_description, $aa_members_only, $establishment_name);
	$sql_special->fetch();
	$sql_special->close();
	$aa_members_only = $aa_members_only=='1'?'checked':'';
}

$template = file_get_contents('edit_specials.html');

//Replace Tags
$template = str_replace('<!-- special_start -->', $specials_start, $template);
$template = str_replace('<!-- special_end -->', $specials_end, $template);
$template = str_replace('<!-- special_name -->', $specials_name, $template);
$template = str_replace('<!-- special_description -->', $specials_description, $template);
$template = str_replace('<!-- aa_members_only -->', $aa_members_only, $template);

echo $template;

$J_home=7;
$J_title1="$establishment_name";
$J_title2=$title2;
$J_icon="<img src=".$ROOT."/ico/set/file_drawer.png>";
$J_label=22;
$J_width=800;
$J_height=400;
$J_framesize=260;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>