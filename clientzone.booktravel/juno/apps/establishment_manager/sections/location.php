<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

if(isset($_GET["check_prov"])) {
	echo "<script>window.parent.j_chk(".mysql_num_rows(mysql_query("SELECT province_id FROM nse_nlocations_provinces WHERE country_id=".$_GET["check_prov"]." LIMIT 1")).")</script>";
	die();
}

$id=$_GET["id"];
$establishment_code = $_GET['id'];
$location_fields = array('dd_lat','dd_lon','town','province','country', 'suburb');
$real_fields = array('dd_lat'=>'gps_latitude','dd_lon'=>'gps_longitude','town'=>'town_id','province'=>'province_id','country'=>'country_id', 'suburb'=>'suburb_id');
$directions_fields = array('directions','map_type','image_name');
$image_whitelist = array(IMAGETYPE_JPEG=>'jpg',IMAGETYPE_GIF=>'gif',IMAGETYPE_PNG=>'png');
$dd_lat = '';
$dd_lon = '';



if(isset($_GET["j_hide"])) {
	//Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (in_array($field_name, $location_fields) || in_array($field_name,$directions_fields)) $changes .= "$field_name,";
    }
    $sql_updates->free_result();
    $sql_updates->close();

    echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/location.js></script>";
	echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
    echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
	
	die();
}
echo "<script src='http://maps.google.com/maps/api/js?sensor=false' ></script> ";
$SUB_ACCESS=array("edit-data"=>0);
include $SDR."/system/secure.php";
if(isset($_GET["view"]))
	$SUB_ACCESS["edit-data"]=0;

if(count($_POST)) { //Save

    //Get current Data
    $statement = "SELECT gps_latitude, gps_longitude, country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
    $sql_location = $GLOBALS['dbCon']->prepare($statement);
    $sql_location->bind_param('s', $establishment_code);
    $sql_location->execute();
    $sql_location->bind_result($dd_lat, $dd_lon, $country, $province, $town, $suburb);
    $sql_location->fetch();
    $sql_location->free_result();
    $sql_location->close();
  
    //Get direction data
    $statement = "SELECT directions, map_type, image_name FROM nse_establishment_directions WHERE establishment_code=? LIMIT 1";
    $sql_directions = $GLOBALS['dbCon']->prepare($statement);
    $sql_directions->bind_param('s', $establishment_code);
    $sql_directions->execute();
    $sql_directions->bind_result($directions, $map_type, $image_name);
    $sql_directions->fetch();
    $sql_directions->free_result();
    $sql_directions->close();

    if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only
        
        if (isset($_FILES['image_name']['name']) && !empty($_FILES['image_name']['name'])) {
            $image_data = getimagesize($_FILES['image_name']['tmp_name']);
            if (isset($image_whitelist[$image_data[2]])) {
                $file_name = "{$establishment_code}_map_{$EPOCH}.{$image_whitelist[$image_data[2]]}";
                move_uploaded_file($_FILES['image_name']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/temp_images/". $file_name);
            }
        }

        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
            if (isset($$field_name) && $field_value != $$field_name) {
                $no_change = FALSE;
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        //Delete upload
        if (isset($_POST['delete_new'])) {
            $statement = "SELECT field_value FROM nse_establishment_updates WHERE establishment_code=? AND field_name='image_name'";
            $sql_new_data = $GLOBALS['dbCon']->prepare($statement);
            $sql_new_data->bind_param('s', $establishment_code);
            $sql_new_data->execute();
            $sql_new_data->bind_result($image_file);
            $sql_new_data->fetch();
            $sql_new_data->close();

            unlink($_SERVER['DOCUMENT_ROOT'] . "/temp_images/$image_file");

            $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? AND field_value='image_name' LIMIT 1";
            $sql_delete = $GLOBALS['dbCon']->prepare($statement);
            $sql_delete->bind_param('s', $establishment_code);
            $sql_delete->execute();
            $sql_delete->close();
        }

        if (isset($_POST['delete_current'])) {
            $statement = "SELECT image_name FROM nse_establishment_directions WHERE establishment_code=? LIMIT 1";
            $sql_data = $GLOBALS['dbCon']->prepare($statement);
            $sql_data->bind_param('s', $establishment_code);
            $sql_data->execute();
            $sql_data->bind_result($image_file);
            $sql_data->fetch();
            $sql_data->close();

            unlink($_SERVER['DOCUMENT_ROOT'] . "/res_maps/$image_file");

            $statement = "UPDATE nse_establishment_directions SET image_name='' WHERE establishment_code=?";
            $sql_remove = $GLOBALS['dbCon']->prepare($statement);
            $sql_remove->bind_param('s', $establishment_code);
            $sql_remove->execute();
            $sql_remove->close();
        }

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated by client", '', $establishment_code);
			timestamp_establishment($establishment_code);
        }

    } else {

        //Delete upload
        if (isset($_POST['delete_new'])) {
            $statement = "SELECT field_value FROM nse_establishment_updates WHERE establishment_code=? AND field_name='image_name'";
            $sql_new_data = $GLOBALS['dbCon']->prepare($statement);
            $sql_new_data->bind_param('s', $establishment_code);
            $sql_new_data->execute();
            $sql_new_data->bind_result($image_file);
            $sql_new_data->fetch();
            $sql_new_data->close();

            unlink($_SERVER['DOCUMENT_ROOT'] . "/temp_images/$image_file");

            $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? AND field_value='image_name' LIMIT 1";
            $sql_delete = $GLOBALS['dbCon']->prepare($statement);
            $sql_delete->bind_param('s', $establishment_code);
            $sql_delete->execute();
            $sql_delete->close();
        }

        if (isset($_POST['delete_current'])) {
            $statement = "SELECT image_name FROM nse_establishment_directions WHERE establishment_code=? LIMIT 1";
            $sql_data = $GLOBALS['dbCon']->prepare($statement);
            $sql_data->bind_param('s', $establishment_code);
            $sql_data->execute();
            $sql_data->bind_result($image_file);
            $sql_data->fetch();
            $sql_data->close();

            unlink($_SERVER['DOCUMENT_ROOT'] . "/res_maps/$image_file");

            $statement = "UPDATE nse_establishment_directions SET image_name='' WHERE establishment_code=?";
            $sql_remove = $GLOBALS['dbCon']->prepare($statement);
            $sql_remove->bind_param('s', $establishment_code);
            $sql_remove->execute();
            $sql_remove->close();
        }

        //Prepare statement - get submitted changes
        $statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert permanent log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
        $sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statment - clear temp log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {

            if (!isset($$field_name)) $$field_name = '';
            if ($field_value != $$field_name) {

                $submitted_by = '';
                $submitted_date = '';
                $submitted_field_value = '';

                //Get client submitted changes
                $sql_submitted_data->bind_param('ss', $establishment_code, $field_name);
                $sql_submitted_data->execute();
                $sql_submitted_data->store_result();
                if ($sql_submitted_data->num_rows == 1) {
                    $sql_submitted_data->bind_result($submitted_by, $submitted_field_value, $submitted_date);
                    $sql_submitted_data->fetch();
                    $sql_submitted_data->free_result();
                }

                //Write to permanent log
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();

                //Clear client log
                $sql_clear_log->bind_param('ss', $establishment_code, $field_name);
                $sql_clear_log->execute();

                $changed_fields[$field_name] = $field_value;
            }
        }
        $sql_clear_log->close();
        $sql_log_insert->close();
        $sql_submitted_data->close();

        //Check if entry exists in location table
        $count = 0;
        $statement = "SELECT COUNT(*) FROM nse_establishment_location WHERE establishment_code=?";
        $sql_check = $GLOBALS['dbCon']->prepare($statement);
        $sql_check->bind_param('s', $establishment_code);
        $sql_check->execute();
        $sql_check->bind_result($count);
        $sql_check->fetch();
        $sql_check->close();
        if ($count == 0) {
            $statement = "INSERT INTO nse_establishment_location (establishment_code) VALUE (?)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bind_param('s', $establishment_code);
            $sql_insert->execute();
            $sql_insert->close();
        }

        //Check if entry exists in directions table
        $count = 0;
        $statement = "SELECT COUNT(*) FROM nse_establishment_directions WHERE establishment_code=?";
        $sql_check = $GLOBALS['dbCon']->prepare($statement);
        $sql_check->bind_param('s', $establishment_code);
        $sql_check->execute();
        $sql_check->bind_result($count);
        $sql_check->fetch();
        $sql_check->close();
        if ($count == 0) {
            $statement = "INSERT INTO nse_establishment_directions (establishment_code) VALUES (?)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bind_param('s', $establishment_code);
            $sql_insert->execute();
            $sql_insert->close();
        }

        if (isset($_FILES['image_name']['name']) && !empty($_FILES['image_name']['name'])) {
            $image_data = getimagesize($_FILES['image_name']['tmp_name']);
            if (isset($image_whitelist[$image_data[2]])) {
                $file_name = "{$establishment_code}_map_{$EPOCH}.{$image_whitelist[$image_data[2]]}";
				$file_name2 = strtolower($establishment_code);
				move_uploaded_file($_FILES['image_name']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/temp_images/". $file_name);

				//Create thumbnail
				list($src_width, $src_height, $image_type, $html_markup) = getimagesize(SITE_ROOT . "/temp_images/$file_name");
				switch ($image_type) {
					case IMG_GIF:
						$src_image = imagecreatefromgif(SITE_ROOT . "/temp_images/$file_name");
						break;
					case IMG_JPG:
						$src_image = imagecreatefromjpeg(SITE_ROOT . "/temp_images/$file_name");
						break;
					case IMG_PNG:
						$src_image = imagecreatefrompng(SITE_ROOT . "/temp_images/$file_name");
						break;
					case IMG_WBMP:
						$src_image = imagecreatefromwbmp(SITE_ROOT . "/temp_images/$file_name");
						break;
				}

				if ($src_width > $src_height) {
					$new_height = 200*($src_height/$src_width);
					$new_image = imagecreatetruecolor(200, $new_height);
					imagecopyresampled($new_image, $src_image, 0, 0, 0, 0, 200, $new_height, $src_width, $src_height);
				} else {
					$new_width = 200*($src_width/$src_height);
					$new_image = imagecreatetruecolor(200, $new_width);
					imagecopyresampled($new_image, $src_image, 0, 0, 0, 0, $new_width, 200, $src_width, $src_height);
				}

				imagejpeg($new_image, SITE_ROOT . "/res_maps/TN_$file_name2.jpg");

				
				rename($_SERVER['DOCUMENT_ROOT'] . "/res_maps/TN_$file_name2.jpg", "/var/www/booktravel.travel/web/res_maps/TN_$file_name2.jpg");
                $changed_fields['image_name'] = $file_name;
            }
        }

        if (!empty($changed_fields)) {

            //Prepare statement - check for res field
            $statement = "SELECT COUNT(*) FROM nse_establishment_public_contact WHERE establishment_code=? && contact_type=?";
            $sql_check = $GLOBALS['dbCon']->prepare($statement); 

            foreach ($changed_fields as $field_name => $field_value) {
                $no_change = FALSE;

                if (in_array($field_name, $directions_fields)) {
                    if ($field_name == 'image_name') {
                        rename($_SERVER['DOCUMENT_ROOT'] . "/temp_images/". $field_value, "/var/www/booktravel.travel/web/res_maps/". $field_value);
                    }

                    $statement = "UPDATE nse_establishment_directions SET $field_name=? WHERE establishment_code=?";
                    $sql_directions = $GLOBALS['dbCon']->prepare($statement);
                    $sql_directions->bind_param('ss',  $field_value, $establishment_code);
                    $sql_directions->execute();
                    $sql_directions->close();
                }

                if (in_array($field_name, $location_fields)) {
                    $field_name = $real_fields[$field_name];
                    $statement = "UPDATE nse_establishment_location SET $field_name=? WHERE establishment_code=?";
                    $sql_directions = $GLOBALS['dbCon']->prepare($statement);
                    $sql_directions->bind_param('ss',  $field_value, $establishment_code);
                    $sql_directions->execute();
                    $sql_directions->close();
                }
                
            }

        }

    

        if (!$no_change) {
            $note_content = "Establishment Data updated (Locations Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
        }
    }
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/location.js></script>";
include_once $SDR."/system/get.php";
echo "<script>E_s()</script>";

$gps_latitude="";
$gps_longitude="";
$country=0;
$country_name="";
$province=0;
$province_name="";
$town=0;
$town_name="";
$suburb=0;
$suburb_name="";
$l=mysql_query("SELECT * FROM nse_establishment_location WHERE establishment_code='".$id."' LIMIT 1");
if(mysql_num_rows($l)) {
	$l=mysql_fetch_array($l);
	if($l["gps_latitude"])
		$dd_lat=$l["gps_latitude"];
	if($l["gps_longitude"])
		$dd_lon=$l["gps_longitude"];
	if($l["country_id"]) {
		$country=$l["country_id"];
		$country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
	}
	if($l["province_id"]) {
		$province=$l["province_id"];
		$province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
	}
	if($l["town_id"]) {
		$town=$l["town_id"];
		$town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
	}
	if($l["suburb_id"]) {
		$suburb=$l["suburb_id"];
		$suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
	}
}

$directions="";
$map_type="";
$q="SELECT * FROM nse_establishment_directions WHERE establishment_code='".$id."' LIMIT 1";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$g=mysql_fetch_array($q);
	$directions=$g["directions"];
	$map_type=$g["map_type"];
    $image_name = $g['image_name'];
}

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    $$field_name = $field_value;
    if ($field_name == 'suburb') $suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
    if ($field_name == 'town') $town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
    if ($field_name == 'province') $province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
    if ($field_name == 'country') $country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
    switch ($field_name) {
        case 'image_name':
            $new_image = $$field_name;
        default:
            if (in_array($field_name, $location_fields) || in_array($field_name,$directions_fields)) $changes .= "$field_name,";
    }
}
$sql_updates->free_result();
$sql_updates->close();


echo "<script>changes='$changes'</script>";
if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th width=2 nowrap>Decimal Degrees<tt>eg. -26.14338, 28.06883</tt></th><td><input type=text name=dd_lat id=dd_lat value='$dd_lat' onChange='calc_gps(this.name,this.value)' class=jW100></td><td><input type=text name=dd_lon id=dd_lon value='$dd_lon' onChange='calc_gps(this.name,this.value)' class=jW100></td></tr>";
	echo "<tr><th nowrap>Degrees, Minutes & Seconds<tt>eg. S26&deg; 8' 36\" , E28&deg;  4' 7\"</tt></th><td><input type=text name=dms_lat id=dms_lat  onChange='calc_gps(this.name,this.value)' value=\"\" class=jW100></td><td><input type=text name=dms_lon id=dms_lon value=\"\"  onChange='calc_gps(this.name,this.value)' class=jW100></td></tr>";
	echo "<tr><th>GPS<tt>eg. S 26 8.603, E 28 4.130</tt></th><td><input type=text name=gps_lat id=gps_lat  onChange='calc_gps(this.name,this.value)' value='' class=jW100></td><td><input type=text name=gps_lon id=gps_lon value='' onChange='calc_gps(this.name,this.value)' class=jW100></td></tr>";
	echo "<tr><th><input type=button value='Find Coordinates' class=jW100 onmouseover=\"J_TT(this,'Click here to find co-ordinates based on your address')\" onclick=J_V('fc')></th><td><input type=button value='Clear Coordinates' class=jW100 onClick=\"clr_coords()\"></td><td><input type=button value='Check Coordinates' onclick=\"J_W(ROOT+'/apps/establishment_manager/sections/location_map.php?lat='+ document.getElementById('dd_lat').value +'&lon='+ document.getElementById('dd_lon').value +'&w='+j_W+'&f='+j_Wn)\" class=jW100></td></tr>";
	echo "<tbody id=fc class=ji>";
	echo "<tr><th>Street No.</th><td><input type=text id=gps_number value='' class=jW100></td><td rowspan=5  style='width: 380px'><tt>Enter the street address of the establishment on the left and click on the 'Find' button. An attempt will be made to find the establishments GPS co-ordinates.<p><b>Note: </b>This system works best with adresses located in large towns and metropolitan areas. You can check and refine the GPS position by clicking on 'Check Coordinates' button above. </tt></td></tr>";
	echo "<tr><th>Street Name</th><td><input type=text id=gps_street value='' class=jW100></td></tr>";
	echo "<tr><th>Suburb</td><th><input type=text id=gps_suburb value='' class=jW100></td></tr>";
	echo "<tr><th>Town/City</th><td><input type=text id=gps_town value='' class=jW100></td></tr>";
	echo "<tr><th>&nbsp;</th><td><input type=button value='Find GPS Co-ordinates' class=jW100 onClick=get_coords()></td></tr>";
	echo "<tr><th colspan=9><hr></th></tr>";
	echo "</tbody>";
} else {
	echo "<tr><th width=2 nowrap>Decimal Degrees</th><td>$dd_lat</td><td>$dd_lon</td></tr>";
//	echo "<tr><th nowrap>Degrees, Minutes & Seconds</th><td><input type=text name=dms_lat id=dms_lat  onChange='calc_gps(this.name,this.value)' value=\"\" class=jW100></td><td><input type=text name=dms_lon id=dms_lon value=\"\"  onChange='calc_gps(this.name,this.value)' class=jW100></td></tr>";
//	echo "<tr><th>GPS</th><td><input type=text name=gps_lat id=gps_lat  onChange='calc_gps(this.name,this.value)' value='' class=jW100></td><td><input type=text name=gps_lon id=gps_lon value='' onChange='calc_gps(this.name,this.value)' class=jW100></td></tr>";
}


echo "<tr><th>Directions</th><td colspan=9><textarea name=directions rows=8 class=jW100>".$directions."</textarea></td></tr>";

if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th>Map Type</th><td><select name=map_type class=jW100>".str_replace("=".$map_type.">","=".$map_type." selected>","<option value=none><option value=google>Google<option value=upload>Uploaded")."</select></td></tr>";
} else {
	echo "<tr><th>Map Type</th><td colspan=2>$map_type</td></tr>";
}

if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th>Own image</th><td colspan=2><input type=file name=image_name class=jW100>";
	if (isset($new_image)) {
		echo "<br><tt><input type='checkbox' name='delete_new' value='d' title='Delete Map'>New Map: $new_image </tt>";
	} else if (!empty($image_name)) {
		echo "<br><tt><input type='checkbox' name='delete_current' value='d' title='Delete Map'>Current Map: $image_name </tt>";
	}
	echo "</td></tr>";
}


if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th>Country</th><td><input type=text name=cou value=\"".$country_name."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=country',200,4,9) onclick=J_ajax_C(this) onmouseover=\"J_TT(this,'Type in a few letters of the country')\" class=jW100><input type=hidden name=country value=".$country."></td></tr>";

	$y=mysql_num_rows(mysql_query("SELECT province_id FROM nse_nlocations_provinces WHERE country_id=".$country." LIMIT 1"));

	echo "<tr><th>Province</th><td><input type=text name=pro ".($y?"":" disabled")." value=\"".($y?$province_name:"None available")."\" onkeyup=\"if(this.disabled==false)J_ajax_K(this,'/system/ajax.php?t=province'+(\$N('country')[0].value!='0'?'&country='+\$N('country')[0].value:''),200,4,9)\" onclick=\"if(this.disabled==false)J_ajax_C(this)\" onmouseover=\"if(this.disabled==false)J_TT(this,'Type in a few letters of the province')\" class=jW100><input type=hidden name=province value=".($y?$province:0)."></td></tr>";

	echo "<tr><th>Town</th><td><input type=text value=\"".$town_name."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=town'+(\$N('country')[0].value!='0'?'&country='+\$N('country')[0].value:'')+(\$N('province')[0].value!='0'?'&province='+\$N('province')[0].value:''),200,4,9) onclick=J_ajax_C(this) onmouseover=\"J_TT(this,'Type in a few letters of the town')\" class=jW100><input type=hidden name=town value=".$town."></td></tr>";

	echo "<tr><th>Suburb</th><td><input type=text value=\"".$suburb_name."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=suburb'+(\$N('town')[0].value!='0'?'&town='+\$N('town')[0].value:''),200,4,9) onclick=J_ajax_C(this) onmouseover=\"J_TT(this,'Type in a few letters of the suburb')\" class=jW100><input type=hidden name=suburb value=".$suburb."></td></tr>";
} else {
	echo "<tr><th>Country</th><td colspan=2>$country_name</td></tr>";
	echo "<tr><th>Province</th><td colspan=2>$province_name</td></tr>";
	echo "<tr><th>Town</th><td colspan=2>$town_name</td></tr>";
	echo "<tr><th>Suburb</th><td colspan=2>$suburb_name</td></tr>";

}
echo "<script>E_d()</script>";

if($SUB_ACCESS["edit-data"])
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if(!isset($_GET["j_IF"]))
{
	$J_title2="Establishment Manager";
	$J_title1="Location";
	$J_width=480;
	$J_height=480;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}
echo "<script>calc_gps('dd_lat', document.getElementById('dd_lat').value);calc_gps('dd_lon', document.getElementById('dd_lon').value);</script>";
echo "</body></html>";
?>