<?php

/**
 * History page of establishment manager. Displays name changes and advertising history
 *
 * @version $Id: history.php 1184 2011-09-19 10:53:44Z web37 $
 */


require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Security
include $SDR."/system/get.php";

//Vars
$establishment_code=isset($_GET["id"])?$_GET["id"]:0;

if(isset($_GET["j_hide"])) {
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/history.js></script>";
	echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
	die();
}



echo "<html>";
echo "<link rel=stylesheet href=/juno/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/history.js></script>";
echo "<script>E_s(1)</script>";

//Get current establishment details
$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
$sql_current = $GLOBALS['dbCon']->prepare($statement);
$sql_current->bind_param('s', $establishment_code);
$sql_current->execute();
$sql_current->bind_result($establishment_name);
$sql_current->fetch();
$sql_current->close();


/* Publications History */
echo "<table id=jTR class=jW100>";
echo "<tr><th colspan=10>Publication History</th></tr>";

//From invoices
$statement = "SELECT DISTINCT j_invit_name
				FROM nse_invoice_item AS a 
				LEFT JOIN nse_invoice AS b ON a.j_invit_invoice=b.j_inv_id
				LEFT JOIN nse_inventory AS c ON a.j_invit_inventory=c.j_in_id
				WHERE c.j_in_category=3 AND (b.j_inv_to_company=? OR a.j_invit_company=?)";
$sql_invoice_publications = $GLOBALS['dbCon']->prepare($statement);
$sql_invoice_publications->bind_param('ss', $establishment_code,$establishment_code);
$sql_invoice_publications->execute();
$sql_invoice_publications->store_result();
$sql_invoice_publications->bind_result($publication_name);
while ($sql_invoice_publications->fetch()) {
	echo "<tr><td>$publication_name</td><td>From Invoice</td></tr>";
}
$sql_invoice_publications->close();

//From old publication tables
$statement = "SELECT b.publication_name, b.publication_year
				FROM nse_establishment_publications AS a
				LEFT JOIN nse_publications AS b ON a.publication_id=b.publication_id
				WHERE establishment_code=?
				ORDER BY a.add_date";
$sql_old_publications = $GLOBALS['dbCon']->prepare($statement);
$sql_old_publications->bind_param('s', $establishment_code);
$sql_old_publications->execute();
$sql_old_publications->bind_result($publication_name, $publication_date);
while ($sql_old_publications->fetch()) {
	echo "<tr><td>$publication_date - $publication_name</td><td>Manual Entry</td></tr>";
}
$sql_old_publications->close();
		
//From DBase
$publication_codes = array('A'=>'Display Advert (A)', 'MS'=>'MS', 'C'=>'Cut (C)', 'P'=>'Photograph (P)', 'L'=>'Listing only (L)', 'HL'=>'HL', 'D'=>'D', 'S'=>'Short Listing (S)', 'MA'=>'MA', 'SC'=>'SC', 'U'=>'U', 'SA'=>'SA');
$statement = "SELECT SC25HL26, SC26HL27, SC27HL28, SC28HL29, SC29HL10, SC10HL11 FROM nse_establishment_dbase WHERE establishment_code=? LIMIT 1";
$sql_dbase = $GLOBALS['dbCon']->prepare($statement);
$sql_dbase->bind_param('s', $establishment_code);
$sql_dbase->execute();
$sql_dbase->bind_result($SC25HL26, $SC26HL27, $SC27HL28, $SC28HL29, $SC29HL10, $SC10HL11);
$sql_dbase->fetch();
$sql_dbase->close();

if (!empty($SC25HL26)) echo "<tr><td>Self Catering 2005/ Hotels Lodges 2006 (SC25HL26) - {$publication_codes[$SC25HL26]}</td><td>From DBase</td></tr>";
if (!empty($SC26HL27)) echo "<tr><td>Self Catering 2006/ Hotels Lodges 2007 (SC26HL27) - {$publication_codes[$SC26HL27]}</td><td>From DBase</td></tr>";
if (!empty($SC27HL28)) echo "<tr><td>Self Catering 2007/ Hotels Lodges 2008 (SC27HL28) - {$publication_codes[$SC27HL28]}</td><td>From DBase</td></tr>";
if (!empty($SC28HL29) && isset($publication_codes[$SC28HL29])) echo "<tr><td>Self Catering 2008/ Hotels Lodges 2009 (SC28HL29) - {$publication_codes[$SC28HL29]}</td><td>From DBase</td></tr>";
if (!empty($SC29HL10)) echo "<tr><td>Self Catering 2009/ Hotels Lodges 2010 (SC29HL10) - {$publication_codes[$SC29HL10]}</td><td>From DBase</td></tr>";
if (!empty($SC10HL11)) echo "<tr><td>Self Catering 2010/ Hotels Lodges 2011 (SC10HL11) - {$publication_codes[$SC10HL11]}</td><td>From DBase</td></tr>";

echo "</table><br>";

//Awards History
echo "<table id=jTR class=jW100>";
echo "<tr><th colspan=10>Awards History</th></tr>";

$statement = "SELECT d.result_name, a.year, b.category_description, c.restype_name
				FROM award AS a
				LEFT JOIN nse_award_category AS b ON a.category=b.category_code
				LEFT JOIN nse_restype AS c ON a.category=c.restype_id
				LEFT JOIN nse_award_result AS d ON a.status=d.result_code
				WHERE a.code=?
				ORDER BY a.year";
$sql_awards = $GLOBALS['dbCon']->prepare($statement);
$sql_awards->bind_param('s', $establishment_code);
$sql_awards->execute();
$sql_awards->bind_result($result, $year, $old_category, $new_category);
while ($sql_awards->fetch()) {
	$category = empty($old_category)?$new_category:$old_category;
	echo "<tr><td width=200px>$year</td><td>$result</td><td>$category</td></tr>";
}
$sql_awards->close();

echo "</table><br>";

/* Assessor History */
echo "<table id=jTR class=jW100>";
echo "<tr><th colspan=10>Assessor History</th></tr>";

$statement = "SELECT DATE_FORMAT(a.user_date, '%d-%m-%Y'), CONCAT(b.firstname, ' ', b.lastname)
				FROM nse_history_assessor_rep AS a
				LEFT JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code=? && a.user_type='assessor'";
$sql_history = $GLOBALS['dbCon']->prepare($statement);
$sql_history->bind_param('s', $establishment_code);
$sql_history->execute();
$sql_history->bind_result($user_date, $user_name);
while ($sql_history->fetch()) {
	echo "<tr><td width=200px>$user_date</td><td>$user_name</td></tr>";
}

echo "</table><br>";
echo "<script>J_tr()</script>";


/* Assessor History */
echo "<table id=jTR class=jW100>";
echo "<tr><th colspan=10>Rep History</th></tr>";

$statement = "SELECT DATE_FORMAT(a.user_date, '%d-%m-%Y'), CONCAT(b.firstname, ' ', b.lastname)
				FROM nse_history_assessor_rep AS a
				LEFT JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code=? && a.user_type='rep'";
$sql_history = $GLOBALS['dbCon']->prepare($statement);
$sql_history->bind_param('s', $establishment_code);
$sql_history->execute();
$sql_history->bind_result($user_date, $user_name);
while ($sql_history->fetch()) {
	echo "<tr><td width=200px>$user_date</td><td>$user_name</td></tr>";
}

echo "</table><br>";
echo "<script>J_tr()</script>";

/* Name Change History */
echo "<table id=jTR class=jW100>";

echo "<tr><th colspan=10>Name Change History</th></tr>";
$rc = 0;
$statement = "SELECT DATE_FORMAT(change_date,'%d-%m-%Y'), old_establishment_name FROM nse_establishment_name_history WHERE establishment_code=? ORDER BY change_date ASC";
$sql_names = $GLOBALS['dbCon']->prepare($statement);
$sql_names->bind_param('s', $establishment_code);
$sql_names->execute();
$sql_names->store_result();
$sql_names->bind_result($change_date, $old_establishment_name);
if ($sql_names->num_rows > 0) {
	while ($sql_names->fetch()) {
		if ($rc > 0) echo "$old_establishment_name</td></tr>";
		echo "<tr><td>$change_date</td><td>$old_establishment_name</td><td>--></td><td>";
		$rc++;
	}
	echo "$establishment_name</td></tr>";
} else {
	echo "No Data";
}
$sql_names->free_result();
$sql_names->close();
echo "</table><br>";

echo "<script>J_tr()</script>";


/* QA History */
echo "<table id=jTR class=jW100>";
echo "<tr><th colspan=10>Quality Assurance History</th></tr>";

$statement = "SELECT DATE_FORMAT(a.assessment_date,'%d-%m-%Y'), a.assessment_type, b.aa_category_name, c.subcategory_name, a.assessor_name
				FROM nse_establishment_assessment AS a
				LEFT JOIN nse_aa_category AS b ON a.qa_status=b.aa_category_code
				LEFT JOIN nse_restype_subcategory_lang AS c ON a.proposed_category=c.subcategory_id
				WHERE establishment_code=? AND a.assessment_status='complete'
				ORDER BY a.assessment_date DESC";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
$sql_assessments->bind_param('s', $establishment_code);
$sql_assessments->execute();
$sql_assessments->bind_result($assessment_date, $assessment_type, $qa_status, $proposed_category, $assessor_name);
while ($sql_assessments->fetch()) {
	echo "<tr><td>$assessment_date</td><td>".(ucfirst($assessment_type))."</td><td>$qa_status</td><td>$proposed_category</td><td>$assessor_name</td></tr>";
}
$sql_assessments->close();

echo "</table><br>";
echo "<script>J_tr()</script>";

if(!isset($_GET["view"]) && (isset($j_IF) || isset($_GET["j_IF"])))
{}
else
{
	$J_title2="Establishment Manager";
	$J_title1=str_replace("\"","",$establishment_name);
	$J_title3="Establishment Code: ".$establishment_code;
	$J_width=1000;
	$J_height=640;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}

echo "</body></html>";
?>