<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$id=$_GET["id"];

if(isset($_GET["j_hide"]))
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/notes.js></script>";
	echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
	die();
}

$SUB_ACCESS=array("edit-data"=>0,"send-notes"=>0);
include $SDR."/system/secure.php";
if(isset($_GET["view"]))
	$SUB_ACCESS["edit-data"]=0;

$edit=$SUB_ACCESS["edit-data"];

$notes="";
$q="SELECT * FROM nse_notes WHERE establishment_code='".$id."' ORDER BY note_date DESC";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	include $SDR."/system/get.php";
	while($g=mysql_fetch_array($q))
	{
		$notes.=$g["note_id"]."~";
		$notes.=date("d/m/Y",$g["note_date"])."~";
		$notes.=$g["note_user"]."~";
		$notes.=J_Value("","people","",$g["note_user"])."~";
		$notes.=stripslashes($g["note_content"])."~";
		if($g["note_reminder"]&&$g["note_user"]==$_SESSION["j_user"]["id"])
			$notes.=date("d/m/Y",$g["note_reminder"]);
		$notes.="|";
	}
}

// required
$app=4; // accounts // see custom/extra.js for list of ids
$eid=$id; // establishment code

// feed this to note reader
include $SDR."/apps/notes/view.php";
?>