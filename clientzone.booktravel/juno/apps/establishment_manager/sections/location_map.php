<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/location_map.js></script>";
echo "<script>ww=".$_GET["w"].";wf=".$_GET["f"]."</script>";

echo <<<MAP
            <!DOCTYPE html>
            <html>
            <head>
            <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
            <style type="text/css">
              html { height: 100% }
              body { height: 100%; margin: 0px; padding: 0px }
              #map_canvas { height: 100% }
            </style>
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false" ></script>
            <script>
                function initialize() {
                    var latlng = new google.maps.LatLng({$_GET["lat"]}, {$_GET["lon"]});
                    var myOptions = {
                              zoom: 15,
                              center: latlng,
                              mapTypeId: google.maps.MapTypeId.ROADMAP };
                    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

                    var image = '/juno/apps/establishment_manager/accom.png';
                    marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            draggable:true,
                            title: "Drag to fine tune position",
                            icon: image});


                    google.maps.event.addListener(marker, "drag", function(){
                        //alert(marker.getPosition());
                     E_vals(marker.getPosition().lat(),marker.getPosition().lng())
                    });
                }
            </script>
            </head>
            <body onload="initialize()">
              <div id="map_canvas" style="width:100%; height:100%"></div>
            </body>
            </html>
MAP;

$J_title1="Location Map";
$J_title2="Establishment Manager";
$J_icon="<img src=".$ROOT."/ico/set/map.png>";
$J_width=520;
$J_height=520;
$J_label=22;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>