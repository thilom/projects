function E_stab(g,o)
{
	var p=window.parent,i,u,y,z,a="",b=""
	if(g)
	{
		y=1
		a+="<optgroup label=Group>"
		b+="<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf1>Group</a><div style=display:block>"
		g=g.split("|")
		i=0
		while(g[i])
		{
			u=g[i].split("~")
			a+="<option value="+u[0]+">"+u[1]+"</option>"
			b+="<a href=go: onclick=\"E_go('"+u[0]+"');return false\" onfocus=blur()>"+u[1]+"</a>"
			i++
		}
		a+="</optgroup>"
		b+="</div>"
	}

	if(o)
	{
		z=1
		if(y)
		{
			a+="<optgroup label='"+(y?"Other ":"")+"Establishments'>"
			b+="<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf1>Group</a><div style=display:block>"
		}
		o=o.split("|")
		i=0
		while(o[i])
		{
			u=o[i].split("~")
			a+="<option value="+u[0]+">"+u[1]+"</option>"
			b+="<a href=go: onclick=\"E_go('"+u[0]+"');return false\" onfocus=blur()>"+u[1]+"</a>"
			i++
		}
		if(y)
		{
			a+="</optgroup>"
			b+="</div>"
		}
	}
	if(y||z)
		b="<blockquote>"+b+"</blockquote>"
	p.j_G["estabs"]=[]
	p.j_G["estabs"]["option"]=a
	p.j_G["estabs"]["link"]=b
}
