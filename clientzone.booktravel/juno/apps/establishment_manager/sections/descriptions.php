<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$id=$_GET["id"];
$establishment_code = $_GET['id'];
$no_change = TRUE;
$description_fields = array('short_description','long_description');

if(isset($_GET["j_hide"])) {
    //Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (in_array($field_name, $description_fields)) $changes .= "$field_name,";
    }
    $sql_updates->free_result();
    $sql_updates->close();

	echo "<html>";
	echo "<link rel=stylesheet href=/juno/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/descriptions.js></script>";
	echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
    echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
	die();
}

$SUB_ACCESS=array("edit-data"=>0);
include $SDR."/system/secure.php";
if(isset($_GET["view"]))
	$SUB_ACCESS["edit-data"]=0;

if(count($_POST)) {
	// clean up values
	$err="";
	function clip($p,$n)
	{
		global $err;
		$o=$_POST[$p];
		$o=trim($o);
		$o=str_replace(array("\r","\t","\v"),"",$o);
		$o=str_replace(array("  ","  ")," ",$o);
		$o=str_replace(array("\n "," \n"),"\n",$o);
		$o=str_replace("\n\n\n\n","\n\n\n",$o);
		$o=str_replace(array(" .",".. "),".",$o);
		$_POST[$p]=$o;
		$o=explode(" ",$o);
		if(count($o)>$n)
		{
			$a="";
			foreach($o as $k => $v)
			{
				if($k<$n)
					$a.=" ".$v;
				else
				{
					$a.="...";
					break;
				}
			}
			$v=trim($a);
			$_POST[$p]=$a;
			$err.=ucfirst(str_replace("_"," ",$p))." was clipped to ".$n." words.<br>";
		}
	}

	clip("short_description",48);
	clip("long_description",12000);

    //Get current content
    $statement = "SELECT description_type, establishment_description FROM nse_establishment_descriptions WHERE establishment_code=?";
    $sql_current = $GLOBALS['dbCon']->prepare($statement);
    $sql_current->bind_param('s', $establishment_code);
    $sql_current->execute();
    $sql_current->store_result();
    $sql_current->bind_result($type, $content);
    while ($sql_current->fetch()) {
        if ($type == 'short_description') $short_description = $content;
        if ($type == 'long_description') $long_description = $content;
    }
    $sql_current->free_result();
    $sql_current->close();

    //Insert missing entries
    if (!isset($short_description)) {
        $statement = "INSERT INTO nse_establishment_descriptions (establishment_code, description_type) VALUES (?,'short_description')";
        $sql_short_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_short_insert->bind_param('s', $establishment_code);
        $sql_short_insert->execute();
        $sql_short_insert->close();
        $short_description = '';
    }
    if (!isset($long_description)) {
        $statement = "INSERT INTO nse_establishment_descriptions (establishment_code, description_type) VALUES (?,'long_description')";
        $sql_long_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_long_insert->bind_param('s', $establishment_code);
        $sql_long_insert->execute();
        $sql_long_insert->close();
        $long_description = '';
    }


    if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only
        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
            if (isset($$field_name) && $field_value != $$field_name) {
                $no_change = FALSE;
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated by client", '', $establishment_code);
        }

    } else {
        //Prepare statement - get submitted changes
        $statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert permanent log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
        $sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statment - clear temp log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {

            if (!isset($$field_name)) $$field_name = '';
            if ($field_value != $$field_name) {

                $submitted_by = '';
                $submitted_date = '';
                $submitted_field_value = '';

                //Get client submitted changes
                $sql_submitted_data->bind_param('ss', $establishment_code, $field_name);
                $sql_submitted_data->execute();
                $sql_submitted_data->store_result();
                if ($sql_submitted_data->num_rows == 1) {
                    $sql_submitted_data->bind_result($submitted_by, $submitted_field_value, $submitted_date);
                    $sql_submitted_data->fetch();
                    $sql_submitted_data->free_result();
                }

                //Write to permanent log
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();

                //Clear client log
                $sql_clear_log->bind_param('ss', $establishment_code, $field_name);
                $sql_clear_log->execute();

                $changed_fields[$field_name] = $field_value;
            }
        }
        $sql_clear_log->close();
        $sql_log_insert->close();
        $sql_submitted_data->close();


        //Save Data
        $statement = "UPDATE nse_establishment_descriptions SET establishment_description=? WHERE establishment_code=? AND description_type=?";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        foreach ($_POST as $key=>$value) {
            if ($$key != $value) {
                $no_change = FALSE;
                $sql_update->bind_param('sss', $value, $establishment_code, $key);
                $sql_update->execute();
            }
        }
        $sql_update->close();

        if (!$no_change) {
			$note_content = "Establishment Data updated (Descriptions Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
			timestamp_establishment($establishment_code);
		}

    }

}


$short_description="";
$long_description = "";
$q="SELECT * FROM nse_establishment_descriptions WHERE establishment_code='".$id."'";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if($g["description_type"]=="short_description")
			$short_description=$g["establishment_description"];
		if($g["description_type"]=="long_description")
			$long_description=$g["establishment_description"];
	}
}

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    $$field_name = $field_value;
    if (in_array($field_name, $description_fields)) $changes .= "$field_name,";
}
$sql_updates->free_result();
$sql_updates->close();

echo "<html>";
echo "<link rel=stylesheet href=/juno/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/descriptions.js></script>";
echo "<script>E_s(".($SUB_ACCESS["edit-data"]?1:"").")</script>";
echo "<script>changes='$changes'</script>";
if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if(isset($err) && $err)
	echo "<tt><var><b>WARNING!</b> ".$err."</var></tt><hr>";

echo "<tt><b>Short description</b> is used for search and browse location results. The short description in limited to 48 words.<br><b>Long Description</b> is used for the detailed listing. There is no limit on length but AA Travel Guides reserves the right to edit excessively long descriptions.</tt><hr>";
echo "<tr><th>Short</th><td width=99%>".($SUB_ACCESS["edit-data"]?"<textarea name=short_description rows=5 class=jW100 onkeyup=E_c(this,1) onclick=E_c(this,1)>".$short_description."</textarea>":$short_description)."</td></tr>";
echo "<tr><th>Long</th><td>".($SUB_ACCESS["edit-data"]?"<textarea name=long_description rows=10 class=jW100 onkeyup=E_c(this) onclick=E_c(this)>".$long_description."</textarea>":$long_description)."</td></tr>";

echo "<script>E_d(".($SUB_ACCESS["edit-data"]?1:"").")</script>";

if($SUB_ACCESS["edit-data"])
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if(!isset($_GET["j_IF"]))
{
	$J_title2="Establishment Manager";
	$J_title1="Descriptions";
	$J_width=480;
	$J_height=480;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}

echo "</body></html>";
?>