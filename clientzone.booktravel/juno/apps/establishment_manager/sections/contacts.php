<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$id=isset($_GET["id"])?$_GET["id"]:0;
$establishment_code = $id;

if(isset($_GET["j_hide"]))
{
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/contacts.js></script>";
	unset($_GET["j_hide"]);
	echo "<script>J_start('".$id."'".(isset($_GET["view"])?",1":"").")</script>";
	die();
}

$SUB_ACCESS=array("edit-data"=>0);
include $SDR."/system/secure.php";
if(isset($_GET["view"]))
	$SUB_ACCESS["edit-data"]=0;

if(count($_POST))
{
	include $SDR."/system/activity.php";
	function client()
	{
		global $EPOCH,$d;
		$q="SELECT j_uas_page FROM nse_user_pages_set WHERE j_uas_name='client'";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			$exp=mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")+3);
			while($s=mysql_fetch_array($q))
			{
				mysql_query("INSERT INTO nse_user_pages (j_uac_expiry,j_uac_user,j_uac_page,j_uac_date,j_uac_curator,j_uac_set,j_uac_role) VALUES (".$exp.",".$d.",'".$s["j_uas_page"]."',".$EPOCH.",".$_SESSION["j_user"]["id"].",'client','c')");
			}
		}
	}

	foreach($_POST as $k => $v)
	{
		if(substr($k,0,2)=="c_")
		{
			$d=substr($k,2);
			$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE user_id=".$d));
			if(isset($_POST["a_".$d]))
			{
				$x=mysql_num_rows(mysql_query("SELECT * FROM nse_user_pages WHERE j_uac_user=".$d." AND j_uac_role='c' LIMIT 1"));
				if($x && !isset($_POST["x_".$d]))
				{
					mysql_query("DELETE FROM nse_user_pages WHERE j_uac_user=".$d." AND j_uac_role='c'");
//					J_act("EST MANAGER",4,"Contact: ".$g["firstname"]." ".$g["lastname"]." - Edit Status Revoked",$id);
					$note_content = "Contact: ".$g["firstname"]." ".$g["lastname"]." - Edit Status Revoked";
					insertNote($establishment_code, 4, 0, $note_content);
				}
				elseif(!$x && isset($_POST["x_".$d]))
				{
						client();
//						J_act("EST MANAGER",4,"Contact: ".$g["firstname"]." ".$g["lastname"]." - Edit Status Granted",$id);
						$note_content = "Contact: ".$g["firstname"]." ".$g["lastname"]." - Edit Status Granted";
						insertNote($establishment_code, 4, 0, $note_content);
				}
			}
			else
			{
				mysql_query("DELETE FROM nse_user_establishments WHERE user_id=".$d." AND establishment_code='".$id."'");
//				J_act("EST MANAGER",5,"Contact: ".$g["firstname"]." ".$g["lastname"],$id);
				$note_content = "Contact: ".$g["firstname"]." ".$g["lastname"] . "- Removed as contact for establishment";
				insertNote($establishment_code, 4, 0, $note_content);
			}
		}
		elseif(substr($k,0,3)=="nf_") // New users
		{
			
			if (empty($v)) continue;
			$d=substr($k,3);
			$q="SELECT * FROM nse_user WHERE user_id=".$v;
			$q=mysql_query($q);
			if(mysql_num_rows($q))
			{
				$g=mysql_fetch_array($q);
				mysql_query("INSERT INTO nse_user_establishments (establishment_code,user_id) VALUES ('".$id."',".$v.")");
				client();
//				J_act("EST MANAGER",3,"Contact: ".$g["firstname"]." ".$g["lastname"],$id);
				$note_content = "Contact: ".$g["firstname"]." ".$g["lastname"] . "- Added as contact for establishment";
				insertNote($establishment_code, 4, 0, $note_content);
				timestamp_establishment($establishment_code);
			}
		}
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/contacts.js></script>";

$u="";
$q="SELECT u.user_id,u.firstname,u.lastname,u.phone,u.cell,u.email,e.designation FROM nse_user AS u JOIN nse_user_establishments AS e ON u.user_id=e.user_id WHERE e.establishment_code='".$id."'";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		$u.=$g["user_id"]."~";
		$u.=preg_replace("[^a-zA_Z0-9 \-]","",$g["firstname"])."~";
		$u.=preg_replace("[^a-zA_Z0-9 \-]","",$g["lastname"])."~";
		$u.=preg_replace("[^a-zA_Z0-9 \-]","",$g["designation"])."~";
		$u.=$g["phone"]."~";
		$u.=$g["cell"]."~";
		$u.=$g["email"]."~";
		$u.=(mysql_num_rows(mysql_query("SELECT * FROM nse_user_pages WHERE j_uac_user=".$g["user_id"]." AND j_uac_role='c' LIMIT 1"))?1:"");
		$u.="|";
	}
}
echo "<script>E_d(".($SUB_ACCESS["edit-data"]?1:0).",\"".$u."\",".J_getSecure("/apps/people/send_login.php").",'$establishment_code')</script>";

if($SUB_ACCESS["edit-data"])
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if(!isset($_GET["j_IF"]))
{
	$J_title2="Establishment Manager";
	$J_title1="Contact Details";
	$J_width=480;
	$J_height=480;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}

echo "</body></html>";
?>