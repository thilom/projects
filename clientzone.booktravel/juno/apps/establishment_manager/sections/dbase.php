<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";

$id = $_GET["id"];
$establishment_code = $_GET['id'];
$c = array();
$message = '';

if (isset($_GET["j_hide"])) {
    echo "<html>";
    echo "<script src=" . $ROOT . "/system/P.js></script>";
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/dbase.js></script>";
    echo "<script>J_start('" . $_GET["id"] . "'" . (isset($_GET["view"]) ? ",1" : "") . ")</script>";
    die();
}


echo "<html>";
echo "<link rel=stylesheet href=/juno/style/set/page.css>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/dbase.js></script>";
echo "<script>E_s(1)</script>";

//** Extra Fields - STart *//
$template = file_get_contents('dbase.html');
$fields = array('HL', 'SC', 'nonaccomm', 'HL2012', 'HL2013', 'HL2014', 'HL2015', 'SC2012', 'SC2013', 'SC2014', 'SC2015', 'Awards2012',
					'Awards2013', 'Awards2014', 'Awards2015', 'SABest2012', 'SABest2013', 'SABest2014', 'SABest2015', 'webonly', 'Webexpirydate');

//Save
if (count($_POST) > 0) {
	//Convert to saveable data
	foreach ($fields as $field_name) {
		if ($field_name == 'Webexpirydate') {
			if (!empty($_POST['Webexpirydate'])) {
				list($day, $month, $year) = explode('/', $_POST['Webexpirydate']);
				$_POST['Webexpirydate'] = "$year-$month-$day";
			}
			continue;
		}
		$_POST[$field_name] = isset($_POST[$field_name])?'Y':'';
	}

	//Does an entry exist
	$statement = "SELECT COUNT(*) AS count
					FROM nse_establishment_more_crap
					WHERE establishment_code = :establishment_code";
	$sql_count = $GLOBALS['db_pdo']->prepare($statement);
	$sql_count->bindParam(':establishment_code', $establishment_code);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();

	//Save data
	if ($sql_count_data['count'] == 0) {
		$statement = "INSERT INTO nse_establishment_more_crap (";
		foreach ($fields as $field_name) {
			$statement .= "$field_name,";
		}
		$statement .= "establishment_code) VALUES (";
		foreach ($fields as $field_name) {
			$statement .= "'{$_POST[$field_name]}',";
		}
		$statement .= "'$establishment_code')";
	} else {
		$statement = "UPDATE nse_establishment_more_crap SET ";
		foreach ($fields as $field_name) {
			$statement .= "$field_name='{$_POST[$field_name]}',";
		}
		$statement = substr($statement, 0, -1);
		$statement .= " WHERE establishment_code='$establishment_code' LIMIT 1";
	}

	$sql_update = $GLOBALS['db_pdo']->prepare($statement);
	$sql_update->execute();
	$message = "<div style='padding: 5px; width: 100%; border: 1px dotted gray; background-color: silver; text-align: center'>Data Saved</div>";
}

//Get current data
$statement = "SELECT ";
foreach ($fields as $field_name) {
	$statement .= "$field_name,";
}
$statement = substr($statement, 0, -1);
$statement .= " FROM nse_establishment_more_crap WHERE establishment_code = :establishment_code LIMIT 1";
$sql_fields = $GLOBALS['db_pdo']->prepare($statement);
$sql_fields->bindParam(':establishment_code', $establishment_code);
$sql_fields->execute();
$sql_fields_data = $sql_fields->fetch();
$sql_fields->closeCursor();

//Convert to checked/unchecked
foreach ($fields as $field_name) {
	if ($field_name == 'Webexpirydate') {
		$sql_fields_data[$field_name] = isset($sql_fields_data[$field_name])?$sql_fields_data[$field_name]:'';
		if (!empty($sql_fields_data[$field_name])) {
			list($year, $month, $day) = explode('-', $sql_fields_data[$field_name]);
			$sql_fields_data[$field_name] = "$day/$month/$year";
		}
		continue;
	}
	$sql_fields_data[$field_name] = isset($sql_fields_data[$field_name])&&$sql_fields_data[$field_name]=='Y'?'checked':'';
}

//Replace tags
foreach ($fields as $field_name) {
	$template = str_replace("<!-- $field_name -->", $sql_fields_data[$field_name], $template);
}
$template = str_replace("<!-- message -->", $message, $template);

echo $template;

//** Extra Fields - End *//


$counter = 0;
$statement = "SELECT * FROM nse_establishment_dbase WHERE establishment_code=?";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
$sql_data->bind_param('s', $establishment_code);
$sql_data->execute();
$meta = $sql_data->result_metadata();
while ($field = $meta->fetch_field()) {
	$params[] = &$row[$field->name];
}
call_user_func_array(array($sql_data, 'bind_result'), $params);

while ($sql_data->fetch()) {
	foreach($row as $key => $val) {
		$c[$counter][$key] = $val;
	}
	$counter++;
}
$sql_data->free_result();
$sql_data->close();



echo "<table id=jTR >";

if (count($c) == 0) {
	echo "<tr><td colspan=100 align=center>No DBase entries for this establishment</td></tr>";
} else {
	foreach ($c as $k1 => $v1) {
		foreach ($v1 as $key=>$value) {
			if ($key == 'line_id' || $key == 'code') continue;
			echo "<tr valign=top ><th>$key</th><td width=90%>$value</td></tr>";
		}
	}
}

echo "<table>";
echo "<script>E_d('')</script>";


if (!isset($_GET["j_IF"])) {
    $J_title2 = "Establishment Manager";
    $J_title1 = "DBase Values";
    $J_width = 480;
    $J_height = 480;
    $J_icon = "<img src=" . $ROOT . "/ico/set/gohome-edit.png>";
    $J_tooltip = "";
    $J_label = 22;
    $J_nostart = 1;
    $J_nomax = 1;
    include $SDR . "/system/deploy.php";
}

echo "</body></html>";
?>