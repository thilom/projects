

jc_custom="<img src="+ROOT+"/ico/set/location.gif id=jtab onmouseover=J_OP(this,100) onmouseout=J_OP(this,70) class=jO70 onclick=\"J_resize("+j_W+",'"+j_P.J_reSet(j_Wn,-1)+"')"

function J_start(d)
{
	j_P.j_Wi[j_W]["F"][j_Wn]["sup"].innerHTML=jc_custom+";var w=j_Wi["+j_W+"]['F']["+j_Wn+"]['W'];w.J_opaq();w.location=ROOT+'/apps/establishment_manager/sections/location.php?j_IF&id="+d+"'\">"
	document.write("<body></body></html>")
	J_resize()
}

function E_s()
{
	j_P.j_Wi[j_W]["F"][j_Wn]["sup"].innerHTML=jc_custom+"\">"
	var a=""
	a+="<style type=text/css>th{white-space:nowrap;font-size:14px;color:#888}.hed th{font-size:8pt;padding:1 4;color:#888;background:#DDD}th tt{color:#888;font-weight:normal}</style>"
	a+="<body onclick=\"j_P.J_resize("+j_W+",'"+j_P.J_reSet(j_Wn,-1)+"')\">"
	a+="<form method=post enctype='multipart/form-data'>"
	a+="<table id=jTR class=jW100 cellspacing=0 cellpadding=0>"
	a+="<tr class=hed><th></th><th>Latitude</th><th>Longitude</th></tr>"
	document.write(a)
}

function E_d(d)
{
	var a=""
	a+="</table><br><input type=submit value=OK></form>"
	a+="<iframe id=j_IF></iframe>"
	a+="<img id=chk src="+ROOT+"/ico/set/load.gif class=ji style=position:absolute>"
	document.write(a)
	J_tr()
}

function clr_coords() {
    $('dd_lat').value = '';
	$('dd_lon').value = '';
	$('gps_lat').value = '';
	$('gps_lon').value = '';
	$('dms_lat').value = '';
	$('dms_lon').value = '';
}

function calc_gps(n,v) {

	switch(n) {
		case 'dd_lat':
			if (v == '') break;
			nv = dd_to_dms('lat',v);
			$('dms_lat').value = nv;
			nv = dd_to_gps('lat',v);
			$('gps_lat').value = nv;
			break;
		case 'dd_lon':
			if (v == '') break;
			nv = dd_to_dms('lon',v);
			$('dms_lon').value = nv;
			nv = dd_to_gps('lon',v);
			$('gps_lon').value = nv;
			break;
		case 'dms_lat':
			nv = dms_to_dd('lat',v);
			$('dd_lat').value = nv;
			nv = dms_to_gps('lat',v);
			$('gps_lat').value = nv;
			break;
		case 'dms_lon':
			nv = dms_to_dd('lon',v);
			$('dd_lon').value = nv;
			nv = dms_to_gps('lon',v);
			$('gps_lon').value = nv;
			break;
		case 'gps_lat':
			nv = gps_to_dd('lat',v);
			$('dd_lat').value = nv;
			nv = gps_to_dms('lat',v);
			$('dms_lat').value = nv;
			break;
		case 'gps_lon':
			nv = gps_to_dd('lon',v);
			$('dd_lon').value = nv;
			nv = gps_to_dms('lon',v);
			$('dms_lon').value = nv;
			break;
	}
}

// Convert decimal degrees TO degrees,minutes, seconds
function dd_to_dms(t,v) {
	if (t == 'lat') {
		if (v.substr(0,1) == '-') {
			nv = 'S';
			v = v.substr(1);
		} else {
			nv = 'N';
		}
	} else {
		if (v.substr(0,1) == '-') {
			nv = 'W';
			v = v.substr(1);
		} else {
			nv = 'E';
		}
	}

	nv += parseInt(v); //Degrees
	//nv += chr(167) + " ";
	nv += String.fromCharCode(176) + " ";

	v = (v - parseInt(v)) * 60;
	nv += parseInt(v); //Minutes
	nv += "' ";

	v = (v - parseInt(v)) * 60;
	nv += parseInt(v); //Seconds
	nv += '" ';

	return nv;
}

// Convert decimal degrees TO GPS
function dd_to_gps(t,v) {
	if (t == 'lat') {
		if (v.substr(0,1) == '-') {
			nv = 'S';
			v = v.substr(1);
		} else {
			nv = 'N';
		}
	} else {
		if (v.substr(0,1) == '-') {
			nv = 'W';
			v = v.substr(1);
		} else {
			nv = 'E';
		}
	}
	nv += ' ';

	nv += parseInt(v); //Degrees
	nv += " ";

	v = (v - parseInt(v)) * 60;
	nv += v.toFixed(3); //Decimal

	return nv;
}

//Convert degrees, minutes, seconds TO decimal degrees
function dms_to_dd(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = '-';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = '';
		} else {
			nv = '';
		}

	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = '-';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = '';
		} else {
			nv = '';
		}
	}

	if (v.indexOf('�') != -1) {
		d = v.substr(0, v.indexOf('�'));
		v = v.substr(v.indexOf('�')+1);
	} else {
		d = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}
	v = trim(v);

	if (v.indexOf("'") != -1) {
		m = v.substr(0, v.indexOf("'"));
		v = v.substr(v.indexOf("'")+1);
	} else {
		m = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}

	if (v.indexOf('"') != -1) {
		s = v.substr(0, v.indexOf('"'));
	} else {
		s = v.substr(v.indexOf(' '));
	}

	r = (((parseInt(m)*60)+parseInt(s))/3600) + parseInt(d);
	nv += r.toFixed(6);

//	set_change('gps','locations','dd_lat');
//	set_change('gps','locations','dd_lon');

	return nv;
}

//Convert degrees, minutes, seconds TO GPS
function dms_to_gps(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = 'S';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = 'N';
		} else {
			nv = 'S';
		}
	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = 'W';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = 'E';
		} else {
			nv = 'E';
		}
	}

	if (v.indexOf('�') != -1) {
		d = v.substr(0, v.indexOf('�'));
		v = v.substr(v.indexOf('�')+1);
	} else {
		d = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}
	v = trim(v);

	if (v.indexOf("'") != -1) {
		m = v.substr(0, v.indexOf("'"));
		v = v.substr(v.indexOf("'")+1);
	} else {
		m = v.substr(0, v.indexOf(' '));
		v = v.substr(v.indexOf(' '));
	}

	if (v.indexOf('"') != -1) {
		s = v.substr(0, v.indexOf('"'));
	} else {
		s = v.substr(v.indexOf(' '));
	}

	nv += ' ' + d;
	r = (parseInt(s)/60) + parseInt(m);
	nv += ' ' + r.toFixed(3);

	return nv;
}


//Convert GPS to decimal degrees
function gps_to_dd(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = '-';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = '';
		} else {
			nv = '';
		}
	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = '-';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = '';
		} else {
			nv = '';
		}
	}

	v = trim(v);


	d = v.substr(0, v.indexOf(' '));
	v = v.substr(v.indexOf(' '));

	r = (parseFloat(v)/60) + parseInt(d);

	nv += '' + r.toFixed(6);

//	set_change('gps','locations','dd_lat');
//	set_change('gps','locations','dd_lon');

	return nv;
}

// Convert GPS to Degrees, Minutes, Seconds
function gps_to_dms(t,v) {
	if (t == 'lat') {
		s = v.indexOf('S');
		if (s != -1) {
			nv = 'S';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		}else if (v.indexOf('N') != -1) {
			if (v.indexOf('N') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('N'));
			}
			nv = 'N';
		} else {
			nv = 'N';
		}
	} else {
		s = v.indexOf('W');
		if (s != -1) {
			nv = 'W';
			if (s == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,s);
			}
		} else if (v.indexOf('E') != -1) {
			if (v.indexOf('E') == 0) {
				v = v.substr(1);
			} else {
				v = v.substr(0,v.indexOf('E'));
			}
			nv = 'E';
		} else {
			nv = 'E';
		}
	}

	v = trim(v);

	d = v.substr(0, v.indexOf(' '));
	v = v.substr(v.indexOf(' '));
	m = parseInt(v);
	s = (parseFloat(v) - parseInt(v)) * 60;
	s = parseInt(s);

	nv +=  d + ' ' + m + " " + s + '';


	return nv;
}

function get_coords() {
    address = '';
    if ($('gps_number').value != '') address += $('gps_number').value;
    if ($('gps_street').value != '') address += ' ' + $('gps_street').value;
    if ($('gps_suburb').value != '') address += ',' + $('gps_suburb').value;
    if ($('gps_town').value != '') address += ',' + $('gps_town').value;

   var geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({ address: address, region: 'ZA' }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          $('dd_lat').value = results[0].geometry.location.lat();
		  $('dd_lon').value = results[0].geometry.location.lng();
          calc_gps('dd_lat',$('dd_lat').value);
          calc_gps('dd_lon',$('dd_lon').value);
          J_V('fc');
          alert("GPS Coordinates successfully retrieved "+results[0].geometry.location+". Please check and refine these results by clicking on the 'Check Coordinates' button");
        } else {
          msg = "GPS Coordinates could not be retrieved";
          if (status == google.maps.GeocoderStatus.ZERO_RESULTS) msg = "No GPS coordinates could be found for the supplied address.";
          alert(msg);
        }});
    }
    
  }

function J_ajax_Z(o,d)
{
	if(o.name=="cou")
	{
		var a,b
		$("j_IF").src=ROOT+"/apps/establishment_manager/sections/location.php?check_prov="+o.value
		o=$N("pro")[0]
		o.value="Checking for provinces"
		o.disabled=true
		o.nextSibling.value=0
		d=j_P.J_pos(o)
		a=$("chk")
		a.style.height=o.clientHeight+2
		a.style.left=d[0]+(o.clientWidth-o.clientHeight)+1
		a.style.top=d[1]+1
		a.className="jO50"
	}
}

function j_chk(v)
{
	var a,o=$N("pro")[0]
	o.value=v?"":"None available"
	o.disabled=v?false:true
	$("chk").className="ji"
}

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}