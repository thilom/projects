j_P.j_Wi[j_W]["F"][j_Wn]["sup"].innerHTML="<img src="+ROOT+"/ico/set/details.gif id=jtab onmouseover=J_OP(this,100) onmouseout=J_OP(this,70) class=jO70 onclick=\"J_resize("+j_W+",'"+j_P.J_reSet(j_Wn,-1)+"')\">"

document.write("<style type=text/css>th{white-space:nowrap;width:50;color:#888}</style><body><table id=jTR width=100% cellspacing=0 cellpadding=0>")

function E_d(e,v,p,n)
{
	if(n=="new")
		j_P.J_resize(j_W,"-2,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
	var a=""
	a+="</table><br>"
	
		a+="<input type=submit value=Ok></form>"
	document.write(a)
	J_tr()
}

function activate(sta,ova) {
	if (sta) {
		$('deactivate_reason').style.display = 'none';
		$('astatus').innerHTML = 'Active';
	} else {
		if (ova == '1') {
			$('deactivate_reason').style.display = 'block';
			$('astatus').innerHTML = 'De-activated';
		}
	}
}

function reason_field(rva) {
	if (rva == 'Reason for de-activation') {
		$('deactivate_reason').value = ''
	} else if (rva == '') {
		$('deactivate_reason').value = 'Reason for de-activation'
	}
}

/**
 * Uses AJAX to check for similar establishment names
 */
function chkName(nm,cd) {

    if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			line = al.split('|');
            c = 0;
			tbl = '<table width=100%>';
            for (i=0; i<line.length; i++) {
				if (line[i] == '') continue
				items = line[i].split('~')
				tbl += '<tr><td width=100px >'
				tbl += c==0?"<tt>Similar Names: </tt>":''
                tbl += "<td><tt>"
				if (items[1].toLowerCase() == nm.toLowerCase()) tbl += "<b style='color: red'>"
				tbl += items[0] + " - "+items[1]
				
				tbl += " ("
				if (items[2]) tbl += items[2]
				if (items[3]) tbl += "," + items[3]
				if (items[4]) tbl += "," + items[4]
				tbl += ")"
				if (items[1] == 'nm') tbl += "</b>"
				tbl += "</tt></td></tr>";
				c++;
            }
			tbl += "</table>"
			document.getElementById('simName').innerHTML = tbl
		}
	}
	loc = "/juno/custom/lib/ajax_est_check.php?est="+nm
	if (cd) loc += "&code=" + cd;
	xmlhttp.open("GET",loc,true)
	xmlhttp.send();
}