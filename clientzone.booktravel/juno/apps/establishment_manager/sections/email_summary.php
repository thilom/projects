<?php
/** 
 * Establishment overview for emails
 *
 * @version $Id$
 * @author Thilo Muller(2011)
 * 
 */



//Get Establishment Data
$statement = "SELECT a.establishment_name, a.website_url, b.aa_category_name, a.assessor_id , a.rep_id1, 
						 a.active, DATE_FORMAT(a.enter_date,'%d %M %Y'), DATE_FORMAT(a.creation_date,'%d %M %Y')
                        FROM nse_establishment AS a
						LEFT JOIN nse_aa_category AS b ON a.aa_category_code=b.aa_category_code
                        WHERE establishment_code=? LIMIT 1";
$sql_estab = $GLOBALS['dbCon']->prepare($statement);
$sql_estab->bind_param('s', $establishment_code);
$sql_estab->execute();
$sql_estab->bind_result($establishment_name, $website_url, $aa_endorsement, $assessor_id, $rep_id1, $active, $entered_date, $creation_date);
$sql_estab->fetch();
$sql_estab->free_result();
$sql_estab->close();

//Get location data
$country=0;
$country_name="";
$province=0;
$province_name="";
$town=0;
$town_name="";
$suburb=0;
$suburb_name="";
$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
$sql_location = $GLOBALS['dbCon']->prepare($statement);
$sql_location->bind_param('s', $establishment_code);
$sql_location->execute();
$sql_location->bind_result($country_id, $province_id, $town_id, $suburb_id);
$sql_location->fetch();
$sql_location->free_result();
$sql_location->close();
if (!empty($country_id)) {
    $country=$country_id;
    $country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
}
if (!empty($province_id)) {
    $province=$province_id;
    $province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
}
if(!empty($town_id)) {
    $town=$town_id;
    $town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
}
if(!empty($suburb_id)) {
    $suburb=$suburb_id;
    $suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
}


//Get QA Out date
$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=?";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);
$sql_qa_out->bind_param('s', $establishment_code);
$sql_qa_out->execute();
$sql_qa_out->bind_result($qa_out);
$sql_qa_out->fetch();
$sql_qa_out->close();
if (!isset($qa_out) || $qa_out == '0000-00-00') $qa_out = '-';

//Assemble locations
$location_list = '';
$location_list .= "$country_name";
if (!empty($province_name)) $location_list .= " &#9658; $province_name";
$location_list .= " &#9658; $town_name";
if (isset($suburb_name) && !empty($suburb_name)) $location_list .= " &#9658; $suburb_name";

$regions_list = '';
$statement = "SELECT region_name FROM nse_nlocations_cregion_locations
INNER JOIN nse_nlocations_cregions ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
AND parent_type LIKE 'province'
WHERE location_id=? AND location_type LIKE 'town'";
$sql_regions = $GLOBALS['dbCon']->prepare($statement);
$sql_regions->bind_param('i', $town_id);
$sql_regions->execute();
$sql_regions->store_result();
$sql_regions->bind_result($region_name);
while ($sql_regions->fetch()) {
	$regions_list .= "$region_name<br>";
}
$sql_regions->close();


//Get assessment data
$statement = "SELECT DATE_FORMAT(assessment_date, '%d %M %Y'), assessment_status 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='fa' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->store_result();
$sql_assessment->bind_result($assessment_date, $assessment_status);
if ($sql_assessment->num_rows > 0) {
	$sql_assessment->fetch();
	if (isset($assessment_date) && ($assessment_date == '0000-00-00') || empty($assessment_date)) $assessment_status = '-';
	switch ($assessment_status) {
		case 'waiting':
			$status = "Waiting for approval";
			break;
		case 'draft':
		case 'returned':
			$status = empty($assessment_date)?'':'In Draft';
			break;
		case 'complete':
			$status = 'Complete';
			break;
		default:
			$status = 'Unknown';
	}

	$assessment_data = "$assessment_date ($status)";
} else {
	$assessment_data = 'None';
}

$statement = "SELECT DATE_FORMAT(assessment_date, '%d %M %Y'), assessment_status 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='cc' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->store_result();
$sql_assessment->bind_result($assessment_date_cc, $assessment_status_cc);
if ($sql_assessment->num_rows > 0) {
	$sql_assessment->fetch();
	if (isset($assessment_date) && ($assessment_date == '0000-00-00') || empty($assessment_date)) $assessment_status = '-';
	switch ($assessment_status) {
		case 'waiting':
			$status_cc = "Waiting for approval";
			break;
		case 'draft':
		case 'returned':
			$status_cc = empty($assessment_date)?'':'In Draft';
			break;
		case 'complete':
			$status_cc = 'Complete';
			break;
		default:
			$status_cc = 'Unknown';
	}

	$assessment_data_cc = "$assessment_date_cc ($status_cc)";
} else {
	$assessment_data_cc = '-';
}


//Creation Date
if (empty($creation_date)) $creation_date = 'Unknown';

//Last visit date - fa
if (!empty($assessment_date)) {
	$vDate = '';
	$statement = "SELECT QVISITDATE FROM nse_establishment_dbase WHERE establishment_code=?";
	$sql_visit = $GLOBALS['dbCon']->prepare($statement);
	$sql_visit->bind_param('s', $establishment_code);
	$sql_visit->execute();
	$sql_visit->bind_result($vDate);
	$sql_visit->fetch();
	$sql_visit->close();
	if (!empty($vDate) && strpos($vDate, '/') !== FALSE) {
		list($month,$year) = explode('/', $vDate);
		$last_visit = "01-$month-" . ($year<40?"20":"19") . $year;
		$last_visit = strtotime($last_visit);
		$last_visit = date("F Y", $last_visit);
		$last_visit .= " (from DBase)";
	} else {
		$last_visit = 'Unknown';
	}
} else {
	$last_visit = $assessment_date;
}

//Next renewal date - fa
$renewal_fa_date = '';
$statement = "SELECT DATE_FORMAT(renewal_date, '%d %M %Y') 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='fa' AND assessment_status='complete' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_fa_renewal = $GLOBALS['dbCon']->prepare($statement);
$sql_fa_renewal->bind_param('s', $establishment_code);
$sql_fa_renewal->execute();
$sql_fa_renewal->bind_result($renewal_fa_date);
$sql_fa_renewal->fetch();
$sql_fa_renewal->close();

//Next renewal date - Caravan & Camping
$renewal_cc_date = '-';
$statement = "SELECT DATE_FORMAT(renewal_date, '%d %M %Y') 
				FROM nse_establishment_assessment 
				WHERE establishment_code=? AND assessment_type_2='cc' AND assessment_status='complete' 
				ORDER BY assessment_date DESC 
				LIMIT 1";
$sql_fa_renewal = $GLOBALS['dbCon']->prepare($statement);
$sql_fa_renewal->bind_param('s', $establishment_code);
$sql_fa_renewal->execute();
$sql_fa_renewal->bind_result($renewal_cc_date);
$sql_fa_renewal->fetch();
$sql_fa_renewal->close();


//Get assessor Name
$assessor_name = '';
if (!empty($assessor_id)) {
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) 
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessor->bind_param('i', $assessor_id);
	$sql_assessor->execute();
	$sql_assessor->bind_result($assessor_name);
	$sql_assessor->fetch();
	$sql_assessor->close();
}

//Get rep1 Name
$rep_name1 = '';
if (!empty($rep_id1)) {
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) 
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_rep1 = $GLOBALS['dbCon']->prepare($statement);
	$sql_rep1->bind_param('i', $rep_id1);
	$sql_rep1->execute();
	$sql_rep1->bind_result($rep_name1);
	$sql_rep1->fetch();
	$sql_rep1->close();
}

//Get rep2 Name
$rep_name2 = '';
if (!empty($rep_id2)) {
	$statement = "SELECT CONCAT(firstname, ' ' , lastname) 
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_rep2 = $GLOBALS['dbCon']->prepare($statement);
	$sql_rep2->bind_param('i', $rep_id2);
	$sql_rep2->execute();
	$sql_rep2->bind_result($rep_name2);
	$sql_rep2->fetch();
	$sql_rep2->close();
	$rep_name2 = " ,$rep_name2";
}

//Latest award
$result = '';
$statement = "SELECT d.result_name, a.year, b.category_description, c.restype_name
				FROM award AS a
				LEFT JOIN nse_award_category AS b ON a.category=b.category_code
				LEFT JOIN nse_restype AS c ON a.category=c.restype_id
				LEFT JOIN nse_award_result AS d ON a.status=d.result_code
				WHERE a.code=?
				ORDER BY a.year DESC
				LIMIT 1";
$sql_award = $GLOBALS['dbCon']->prepare($statement);
$sql_award->bind_param('s', $establishment_code);
$sql_award->execute();
$sql_award->bind_result($result, $year, $old_category, $new_category);
$sql_award->fetch();
$sql_award->close();
if (empty($result)) {
	$award = 'none';
} else {
	$award = "$result ($year) - ";
	$award .= empty($old_category)?$new_category:$old_category;
}

//QA Category
$qa_category = '';
$statement = "SELECT b.subcategory_name, d.category_name, f.toplevel_name
				FROM nse_establishment_restype AS a
				LEFT JOIN nse_restype_subcategory_lang AS b ON a.subcategory_id=b.subcategory_id
				LEFT JOIN nse_restype_subcategory_parent AS c ON b.subcategory_id=c.subcategory_id
				LEFT JOIN nse_restype_category_lang AS d ON c.category_id=d.category_id
				LEFT JOIN nse_restype_category AS e ON d.category_id=e.category_id
				LEFT JOIN nse_restype_toplevel AS f ON e.toplevel_id=f.toplevel_id
				WHERE a.establishment_code = ?
				LIMIT 1";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bind_param('s', $establishment_code);
$sql_category->execute();
$sql_category->bind_result($subcategory, $category, $top);
$sql_category->fetch();
$sql_category->close();
$qa_category = "$top &#9658; $subcategory";


//Top Block
echo "<style>";
echo "TABLE TD {color: black; text-align: left}";
echo "</style>";
echo "The following establishment has been added to your portfolio. <a href='http://clientzone.essentialtravelinfo.com/juno/index.php?page=/apps/establishment_manager/edit.php?id=$establishment_code'>[Direct Link]</a><hr>";
echo "<table style='width:100%; border: 1px dotted gray'>";
echo "<tr><th style='color: gray; text-align: left'>Establishment Name</th><td>$establishment_name</td><th  style='color: gray; text-align: left'; width: 150px>Assessor</th><td>$assessor_name</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>Establishment Code</th><td>$establishment_code</td><th style='color: gray; text-align: left'>Rep</th><td>$rep_name1 $rep_name2</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>System Enter Date</th><td colspan=4>$creation_date</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>Latest Award</th><td colspan=4>$award</td></tr>";
echo "<tr><td colspan=4>&nbsp;</td></tr>";
echo "<tr><td colspan=5 style='font-weight: bold'>Fixed Accommodation</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>QA Out</th><td>$qa_out</td><th style='color: gray; text-align: left'>Endorsement</th><td>$aa_endorsement</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>Last Assessment</th><td >$assessment_data</td><th style='color: gray; text-align: left'>Next Assessment</th><td>$renewal_fa_date</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>QA Enter Date</th><td colspan=4>$entered_date</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>QA Category</th><td colspan=4>$qa_category</td></tr>";
echo "<tr><td colspan=6>&nbsp;</td></tr>";

echo "<tr><td colspan=6 style='font-weight: bold'>Caravan & Camping</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>Last Assessment</th><td>$assessment_data_cc</td><th style='color: gray; text-align: left'>Next Assessment</th><td>$renewal_cc_date</td></tr>";

echo "<tr><td colspan=6>&nbsp;</td></tr>";
echo "<tr><th style='color: gray; text-align: left'>Location</th><td colspan=4>$location_list</td></tr>";
echo "<tr valign=top><th style='color: gray; text-align: left'>Regions</th><td colspan=4>$regions_list</td></tr>";



echo "</body></html>";

?>
