<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";
include_once $SDR."/system/activity.php";

$id = $_GET["id"];
$establishment_code = $_GET['id'];
$current_icons = array();
$no_change = TRUE;
$max_cols = 3;
$col_count = 0;
$themes_aa = array();
$themes_et = array();

if (isset($_GET["j_hide"])) {
    echo "<html>";
    echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<script src=".$ROOT."/system/P.js></script>";
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/themes.js></script>";
    echo "<script>J_start('" . $id . "'" . (isset($_GET["view"]) ? ",1" : "") . ")</script>";
    die();
}

$SUB_ACCESS = array("edit-data" => 0);
include $SDR . "/system/secure.php";
if (isset($_GET["view"]))
    $SUB_ACCESS["edit-data"]=0;

if (count($_POST)) {

	//Delete All
	$statement = "DELETE FROM nse_establishment_themes
					WHERE establishment_code=? AND theme_id!=48 AND theme_id!=49";
	$sql_delete_themes = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete_themes->bind_param('s', $establishment_code);
	$sql_delete_themes->execute();
	$sql_delete_themes->close();
	$statement = "DELETE FROM nse_establishment_themes_backup
					WHERE establishment_code=? AND theme_id!=48 AND theme_id!=49";
	$sql_delete_themes = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete_themes->bind_param('s', $establishment_code);
	$sql_delete_themes->execute();
	$sql_delete_themes->close();
	
	//Add AA Themes
	$statement = "INSERT INTO nse_establishment_themes
						(establishment_code, theme_id, type)
					VALUES
						(?,?,'aa')";
    $sql_theme_insert = $GLOBALS['dbCon']->prepare($statement);
	if (isset($_POST['themesAA'])) {
		foreach ($_POST['themesAA'] as $theme_id) {
			$sql_theme_insert->bind_param('si', $establishment_code, $theme_id);
			$sql_theme_insert->execute();
		}
		$sql_theme_insert->close();
		$statement = "INSERT INTO nse_establishment_themes_backup
							(establishment_code, theme_id, type)
						VALUES
							(?,?,'aa')";
		$sql_theme_insert = $GLOBALS['dbCon']->prepare($statement);
		foreach ($_POST['themesAA'] as $theme_id) {
			$sql_theme_insert->bind_param('si', $establishment_code, $theme_id);
			$sql_theme_insert->execute();
		}
		$sql_theme_insert->close();
	}
	
	//Add ETI Themes
	$statement = "INSERT INTO nse_establishment_themes
						(establishment_code, theme_id, type)
					VALUES
						(?,?,'et')";
    $sql_theme_insert = $GLOBALS['dbCon']->prepare($statement);
	if (isset($_POST['themesETI'])) {
		foreach ($_POST['themesETI'] as $theme_id) {
			$sql_theme_insert->bind_param('si', $establishment_code, $theme_id);
			$sql_theme_insert->execute();
		}
		$sql_theme_insert->close();
		$statement = "INSERT INTO nse_establishment_themes_backup
							(establishment_code, theme_id, type)
						VALUES
							(?,?,'et')";
		$sql_theme_insert = $GLOBALS['dbCon']->prepare($statement);
		foreach ($_POST['themesETI'] as $theme_id) {
			$sql_theme_insert->bind_param('si', $establishment_code, $theme_id);
			$sql_theme_insert->execute();
		}
		$sql_theme_insert->close();
	}

	$note_content = "Establishment Data updated (Themes Tab)";
	insertNote($establishment_code, 4, 0, $note_content);
	
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/themes.js></script>";
echo "<form action='' method='post'>";

//Current Themes
$statement = "SELECT theme_id, type
				FROM nse_establishment_themes
				WHERE establishment_code=?";
$sql_current = $GLOBALS['dbCon']->prepare($statement);
$sql_current->bind_param('s', $establishment_code);
$sql_current->execute();
$sql_current->bind_result($theme_id, $type);
while ($sql_current->fetch()) {
	switch ($type) {
		case 'aa':
			$themes_aa[] = $theme_id;
			break;
		case 'et':
			$themes_et[] = $theme_id;
			break;
	}
}
$sql_current->close();


//AA Travel
echo "<h1>AA Travel / Zim Website</h1>";
echo "<table width=100%>";
$statement = "SELECT theme_id, theme_name 
				FROM nse_themes 
				WHERE fk_theme_id != 0 AND theme_name!='Specials' AND theme_name!='Bonsela Discounts'
				ORDER BY theme_name";
$sql_themes = $GLOBALS['dbCon']->prepare($statement);
$sql_themes->execute();
$sql_themes->bind_result($theme_id, $theme_name);
while ($sql_themes->fetch()) {
	if ($col_count == 0) {
		echo "<tr>";
	}
	echo "<td><input type='checkbox' name='themesAA[]' value='$theme_id'";
	if (in_array($theme_id, $themes_aa)) echo "checked";
	echo " >$theme_name</td>";
	$col_count++;
	if ($col_count == $max_cols) {
		echo "</tr>";
		$col_count = 0;
	}
}
$sql_themes->close();
echo "</table>";

//Esential Travel
$col_count = 0;
echo "<p><h1>Essential Travel</h1>";
echo "<table width=100%>";
$statement = "SELECT theme_id, theme_name 
				FROM nse_themes 
				WHERE fk_theme_id != 0 AND theme_name!='Specials' AND theme_name!='Bonsela Discounts'
				ORDER BY theme_name";
$sql_themes = $GLOBALS['dbCon']->prepare($statement);
$sql_themes->execute();
$sql_themes->bind_result($theme_id, $theme_name);
while ($sql_themes->fetch()) {
	if ($col_count == 0) {
		echo "<tr>";
	}
	echo "<td><input type='checkbox' name='themesETI[]' value='$theme_id'";
	if (in_array($theme_id, $themes_et)) echo "checked";
	echo ">$theme_name</td>";
	$col_count++;
	if ($col_count == $max_cols) {
		echo "</tr>";
		$col_count = 0;
	}
}
$sql_themes->close();
echo "</table>";

//Save Button
echo "<p><input type='submit' value='Save' ></form>";

if ($SUB_ACCESS["edit-data"])
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/saver.js></script>";

if (!isset($_GET["j_IF"])) {
    $J_title2 = "Establishment Manager";
    $J_title1 = "Themes";
    $J_width = 480;
    $J_height = 480;
    $J_icon = "<img src=" . $ROOT . "/ico/set/gohome-edit.png>";
    $J_tooltip = "";
    $J_label = 22;
    $J_nostart = 1;
    $J_nomax = 1;
    include $SDR . "/system/deploy.php";
}

echo "</body></html>";
?>