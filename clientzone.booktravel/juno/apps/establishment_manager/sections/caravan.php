<?php
/** 
 * Caravan & camping section
 *
 * @version $Id$
 * @author Thilo Muller(2011)
 */

require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$cc_fields = array('camp_sites','caravan_sites','caravan_camping_sites');
$endorsement_fields = array('cc_endorsement');
$changed_fields = array();
$cc_endorsement = '';
$camp_sites = '';
$caravan_sites = '';
$caravan_camping_sites = '';

if(isset($_GET["j_hide"])) {
    //Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (in_array($field_name, $cc_fields)) $changes .= "$field_name,";
    }
    $sql_updates->free_result();
    $sql_updates->close();
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/caravan.js></script>";
	echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
	echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
    die();
}

//Security
$SUB_ACCESS=array("edit-data"=>0,"edit-endorsement"=>0,"edit-assessor"=>0,"edit-sales-rep"=>0);
include $SDR."/system/secure.php";
if(isset($_GET["view"])) $SUB_ACCESS["edit-data"]=0;

include $SDR."/system/get.php";

//Vars
$establishment_code=isset($_GET["id"])?$_GET["id"]:0;
$no_change = TRUE;
$aa_category_options = '';

if(count($_POST) && $SUB_ACCESS["edit-data"]) {
	//Get current caravan & camping data
	$statement = "SELECT caravan_sites, camp_sites, caravan_camping_sites FROM nse_establishment_data WHERE establishment_code=? LIMIT 1";
	$sql_cc = $GLOBALS['dbCon']->prepare($statement);
	$sql_cc->bind_param('s', $establishment_code);
	$sql_cc->execute();
	$sql_cc->bind_result($caravan_sites, $camp_sites, $caravan_camping_sites);
	$sql_cc->fetch();
	$sql_cc->close();

	//Get current caravan & camping endorsement
	$statement = "SELECT cc_endorsement, cc_assessor, establishment_name, cc_rep FROM nse_establishment WHERE establishment_code=?";
	$sql_endorsement = $GLOBALS['dbCon']->prepare($statement);
	$sql_endorsement->bind_param('s', $establishment_code);
	$sql_endorsement->execute();
	$sql_endorsement->bind_result($cc_endorsement, $cc_assessor, $establishment_name, $cc_rep);
	$sql_endorsement->fetch();
	$sql_endorsement->close();
	if (!isset($caravan_sites)) $caravan_sites = '';
	
	if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only
        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
            $no_change = FALSE;
            if (isset($$field_name) && $field_value != $$field_name) {
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated by client", '', $establishment_code);
        }
    } else {
        //Prepare statement - get submitted changes
        $statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert permanent log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
        $sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statment - clear temp log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

        //Check endorsement
		if ($cc_endorsement != $_POST['cc_endorsement']) $changed_fields['cc_endorsement'] = $_POST['cc_endorsement'];
		
		//Check data fields
		if ($camp_sites != $_POST['camp_sites']) $changed_fields['camp_sites'] = $_POST['camp_sites'];
		if ($caravan_sites != $_POST['caravan_sites']) $changed_fields['caravan_sites'] = $_POST['caravan_sites'];
		if ($caravan_camping_sites != $_POST['caravan_camping_sites']) $changed_fields['caravan_camping_sites'] = $_POST['caravan_camping_sites'];
		
        //Check if data entry exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_data WHERE establishment_code=?";
        $sql_check = $GLOBALS['dbCon']->prepare($statement);
        $sql_check->bind_param('s', $establishment_code);
        $sql_check->execute();
        $sql_check->bind_result($count);
        $sql_check->fetch();
        $sql_check->free_result();
        $sql_check->close();

        //Insert if it doesn't exist
        if ($count == 0) {
            $statement = "INSERT INTO nse_establishment_data (establishment_code) VALUES (?)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bind_param('s', $establishment_code);
            $sql_insert->execute();
            $sql_insert->close();
        }        

		//Change of Assessor
		if (isset($_POST['cc_assessor']) && $_POST['cc_assessor'] != $cc_assessor && !empty($_POST['cc_assessor'])) {
			$note_date = mktime();

			//Get assessor name
			$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
			$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
			$sql_assessor->bind_param('i', $_POST['cc_assessor']);
			$sql_assessor->execute();
			$sql_assessor->bind_result($assessor_name);
			$sql_assessor->fetch();
			$sql_assessor->close();

			$statement = "UPDATE nse_establishment SET cc_assessor_date=NOW(), cc_assessor=? WHERE establishment_code=?";
			$sql_rep_date = $GLOBALS['dbCon']->prepare($statement);
			$sql_rep_date->bind_param('is', $_POST['cc_assessor'], $establishment_code);
			$sql_rep_date->execute();
			$sql_rep_date->close();

			$statement = "INSERT INTO nse_history_assessor_rep (establishment_code, user_id, user_date, user_type) 
							VALUES (?,?,NOW(),'cc_assessor')";
			$sql_history = $GLOBALS['dbCon']->prepare($statement);
			$sql_history->bind_param('si', $establishment_code, $_POST['cc_assessor']);
			$sql_history->execute();
			$sql_history->close();

			$note_content = "$assessor_name added as caravan & camping assessor to $establishment_name";
			insertNote($establishment_code, 4, 0, $note_content);
		}
		
		//Change of Rep 1
		if (isset($_POST['cc_rep']) && $_POST['cc_rep'] != $cc_rep && !empty($_POST['cc_rep'])) {
			$note_date = mktime();

			//Get Rep1 name
			$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
			$sql_rep1 = $GLOBALS['dbCon']->prepare($statement);
			$sql_rep1->bind_param('i', $_POST['cc_rep']);
			$sql_rep1->execute();
			$sql_rep1->bind_result($rep1_name);
			$sql_rep1->fetch();
			$sql_rep1->close();

			$statement = "UPDATE nse_establishment SET cc_rep_date=NOW(), cc_rep=? WHERE establishment_code=?";
			$sql_rep_date = $GLOBALS['dbCon']->prepare($statement);
			$sql_rep_date->bind_param('is', $_POST['cc_rep'], $establishment_code);
			$sql_rep_date->execute();
			$sql_rep_date->close();

			$statement = "INSERT INTO nse_history_assessor_rep (establishment_code, user_id, user_date, user_type) VALUES (?,?,NOW(),'cc_rep')";
			$sql_history = $GLOBALS['dbCon']->prepare($statement);
			$sql_history->bind_param('si', $establishment_code, $_POST['cc_rep']);
			$sql_history->execute();
			$sql_history->close();

			$note_content = "$rep1_name added as caravan & camping sales rep to $establishment_name";
			insertNote($establishment_code, 4, 0, $note_content);
		}
		
        if (!empty($changed_fields)) {
            $no_change = FALSE;
            foreach ($changed_fields as $field_name => $field_value) {
                if (in_array($field_name, $endorsement_fields)) {
                    $statement = "UPDATE nse_establishment SET $field_name=? WHERE establishment_code=?";
                    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
                    $sql_estab->bind_param('ss',  $field_value, $establishment_code);
                    $sql_estab->execute();
                    $sql_estab->close();
                }

				if (in_array($field_name, $cc_fields)) {
                    $statement = "UPDATE nse_establishment_data SET $field_name=? WHERE establishment_code=?";
                    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
                    $sql_estab->bind_param('ss',  $field_value, $establishment_code);
                    $sql_estab->execute();
                    $sql_estab->close();
                }
				
                $submitted_field_value = '';
                $submitted_by = '';
                $submitted_date = '';
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();
            }
        }

        if (!$no_change) {
            $note_content = "Establishment Data updated (Caravan and Camping Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
			timestamp_establishment($establishment_code);
        }
    }
}

//Get and assemble QA list
$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category WHERE active='Y' ORDER BY aa_category_name";
$sql_aa_cats = $GLOBALS['dbCon']->prepare($statement);
$sql_aa_cats->execute();
$sql_aa_cats->store_result();
$sql_aa_cats->bind_result($aa_category_code, $aa_category_name);
while ($sql_aa_cats->fetch()) {
    $aa_categories[$aa_category_code] = $aa_category_name;
}
$sql_aa_cats->free_result();
$sql_aa_cats->close();


//Get caravan & camping data
$statement = "SELECT caravan_sites, camp_sites, caravan_camping_sites FROM nse_establishment_data WHERE establishment_code=? LIMIT 1";
$sql_cc = $GLOBALS['dbCon']->prepare($statement);
$sql_cc->bind_param('s', $establishment_code);
$sql_cc->execute();
$sql_cc->bind_result($caravan_sites, $camp_sites, $caravan_camping_sites);
$sql_cc->fetch();
$sql_cc->close();

//Get endorsement
$statement = "SELECT cc_endorsement, cc_assessor, cc_rep FROM nse_establishment WHERE establishment_code=?";
$sql_endorsement = $GLOBALS['dbCon']->prepare($statement);
$sql_endorsement->bind_param('s', $establishment_code);
$sql_endorsement->execute();
$sql_endorsement->bind_result($aa_endorsement, $cc_assessor, $cc_rep);
$sql_endorsement->fetch();
$sql_endorsement->close();

//HTML start
echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/caravan.js></script>";
echo "<form action='' method=post>";
echo "<script>E_s()</script>";
echo "<table style='width: 100%' >";
echo "<tr><th width=250px>Caravan Only Sites</th><td>".($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c'?"<input type=text name=caravan_sites value='$caravan_sites' class=jW100>":$caravan_sites)."</td></tr>" . PHP_EOL;
echo "<tr><th>Camping Only Sites</th><td>".($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c'?"<input type=text name=camp_sites value='$camp_sites' class=jW100>":$camp_sites)."</td></tr>" . PHP_EOL;
echo "<tr><th>Caravan & Camping Sites</th><td>".($SUB_ACCESS["edit-data"]&&$_SESSION['j_user']['role']!='c'?"<input type=text name=caravan_camping_sites value='$caravan_camping_sites' class=jW100>":$caravan_camping_sites)."</td></tr>" . PHP_EOL;
echo "<tr><th>Endorsement</th><td>" . PHP_EOL;

if($SUB_ACCESS["edit-data"] && $_SESSION['j_user']['role'] != 'c' && $SUB_ACCESS["edit-endorsement"]) {
    foreach ($aa_categories as $cat_code=>$cat_name) {
        if ($cat_code == $aa_endorsement) {
            $aa_category_options .= "<option value='$cat_code' selected>$cat_name";
        } else {
            $aa_category_options .= "<option value='$cat_code' >$cat_name";
        }
    }
    echo "<select name=cc_endorsement class=jW100><option value=''>$aa_category_options</select>" . PHP_EOL;
} else {
    echo isset($aa_categories[$aa_endorsement])?"{$aa_categories[$aa_endorsement]}</td></tr>" . PHP_EOL:"</td></tr>" . PHP_EOL;
}

//Get and assemble rep list
$rep_list = array(''=>'',0=>' ');
$statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) FROM nse_user WHERE roles LIKE '%r%' ORDER BY firstname";
$sql_reps = $GLOBALS['dbCon']->prepare($statement);
$sql_reps->execute();
$sql_reps->store_result();
$sql_reps->bind_result($rep_id, $rep_name);
while ($sql_reps->fetch()) {
    $rep_list[$rep_id] = $rep_name;
}
$sql_reps->free_result();
$sql_reps->close();

//Get and assemble assessors list
$assessor_list = array(0=>' ',''=>'');
$statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) FROM nse_user WHERE roles LIKE '%a%' ORDER BY firstname";
$sql_assessors = $GLOBALS['dbCon']->prepare($statement);
$sql_assessors->execute();
$sql_assessors->store_result();
$sql_assessors->bind_result($ass_id, $ass_name);
while ($sql_assessors->fetch()) {
    $assessor_list[$ass_id] = $ass_name;
}
$sql_assessors->free_result();
$sql_assessors->close();

//Assessors 
echo "<tr><th>Assessor</th><td>" . PHP_EOL;
if ($SUB_ACCESS['edit-data'] && $SUB_ACCESS['edit-assessor'] && ($_SESSION['j_user']['role'] == 's' || $_SESSION['j_user']['role'] == 'd' )) {
    $ass_options = '';
    foreach ($assessor_list as $ass_id=>$ass_name) {
        if ($ass_id == $cc_assessor) {
            $ass_options .= "<option value='$ass_id' selected >$ass_name";
        } else {
            $ass_options .= "<option value='$ass_id'>$ass_name";
        }
    }
    echo "<select name=cc_assessor class=jW100><option value=''>$ass_options</select>" . PHP_EOL;
} else {
     echo isset($assessor_list[$cc_assessor])?"{$assessor_list[$cc_assessor]}</td></tr>":"</td></tr>";
}

//Sales Rep
echo "<tr><th>Sales Rep</th><td>";
if ($SUB_ACCESS['edit-data'] && $SUB_ACCESS['edit-sales-rep'] && ($_SESSION['j_user']['role'] == 's' || $_SESSION['j_user']['role'] == 'd')) {
    $rep_options = '';
    foreach ($rep_list as $rep_id=>$rep_name) {
        if ($rep_id == $cc_rep) {
            $rep_options .= "<option value='$rep_id' selected >$rep_name";
        } else {
            $rep_options .= "<option value='$rep_id'>$rep_name";
        }
    }
    echo "<select name=cc_rep class=jW100><option value=''>$rep_options</select>";
} else {
     echo "{$rep_list[$cc_rep]}</td></tr>";
}


echo "</table>";

if($SUB_ACCESS["edit-data"])
	echo "<script>E_r()</script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if(!isset($_GET["view"]) && (isset($j_IF) || isset($_GET["j_IF"])))
{}
else
{
	$J_title2="Establishment Manager";
	$J_title1=str_replace("\"","",$establishment_name);
	$J_title3="Establishment Code: ".$establishment_code;
	$J_width=1000;
	$J_height=640;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}

echo "</body></html>";




?>
