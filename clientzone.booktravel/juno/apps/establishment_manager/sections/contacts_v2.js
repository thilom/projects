jc_custom="<img src="+ROOT+"/ico/set/contacts.gif id=jtab onmouseover=J_OP(this,100) onmouseout=J_OP(this,70) class=jO70 onclick=\"J_resize("+j_W+",'"+j_P.J_reSet(j_Wn,-1)+"')"

function J_start(d,v)
{
	j_P.j_Wi[j_W]["F"][j_Wn]["sup"].innerHTML=jc_custom+";var w=j_Wi["+j_W+"]['F']["+j_Wn+"]['W'];w.J_opaq();w.location=ROOT+'/apps/establishment_manager/sections/contacts_v2.php?j_IF&id="+d+(v?"&view=1":"")+"'\">"
	document.write("<body></body></html>")
	J_resize()
}

function editContact(e,d) {
	startReloadCheck()
	
	url = "/juno/apps/establishment_manager/edit_contact.php?est=" + e;
	if (d) {
		url += "&id=" + d;
	}
	
	J_W(url);
}

/**
 * Reload Check
 */
function checkReload() {
    if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			if (al == '1') document.location = document.location;
		}
	}

	Url = "/juno/apps/establishment_manager/ajax_contact_v2_reload.php";

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Start the reload check timer
 */
function startReloadCheck() {
    setInterval('checkReload()', 10000);
}

/**
 * Remove a user
 */
function removeContact(est,id) {
	
    if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var al = xmlhttp.responseText;
			document.location = document.location;
		}
	}

	Url = "/juno/apps/establishment_manager/ajax_contact_v2_userremove.php";
	Url += "?user_id=" + id;
	Url += "&establishment=" + est;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}
