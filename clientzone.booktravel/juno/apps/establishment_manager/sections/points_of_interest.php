<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$id = $_GET["id"];
$establishment_code = $_GET['id'];
$no_change = TRUE;
$fields = array('landmark_name','landmark_distance','landmark_measurement','landmark_group','landmark_delete');

if (isset($_GET["j_hide"])) {
    //Get updated data
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (substr($field_name, 0, 9) == 'landmark_') {
            list($field_type, $field_name, $landmark_id) = explode('_', $field_name);
			$changes .= "$field_name,";
//            switch ($field_type) {
//                case 'nt':
//                    $changes .= "$field_name,";
//                    break;
//                case 'nd':
//                    $changes .= "$field_name,";
//                    break;
//                case 'nm';
//                    $changes .= "$field_name,";
//                    break;
//                case 'ng':
//                    $changes .= "$field_name,";
//                    break;
//            }
        }
    }
    $sql_updates->free_result();
    $sql_updates->close();

    echo "<html>";
    echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<script src=".$ROOT."/system/P.js></script>";
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/points_of_interest.js></script>";
    echo "<script>J_start('" . $id . "'" . (isset($_GET["view"]) ? ",1" : "") . ")</script>";
    echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
    die();
}

$SUB_ACCESS = array("edit-data"=>0);
include $SDR . "/system/secure.php";
if (isset($_GET["view"])) $SUB_ACCESS["edit-data"] = 0;

if (count($_POST)) {

    $new_landmarks = array();

    //Get Current Data
    $statement = "SELECT l.landmark_type_id, l.landmark_name, e.landmark_id, e.landmark_distance, e.landmark_measurement FROM nse_establishment_landmarks AS e JOIN nse_landmark AS l ON e.landmark_id=l.landmark_id WHERE e.establishment_code=?";
    $sql_data = $GLOBALS['dbCon']->prepare($statement);
    $sql_data->bind_param('s', $establishment_code);
    $sql_data->execute();
    $sql_data->store_result();
    $sql_data->bind_result($landmark_type_id,$landmark_name,$landmark_id,$landmark_distance,$landmark_measurement);
    while ($sql_data->fetch()) {
        $current[$landmark_id]['landmark_name'] = $landmark_name;
        $current[$landmark_id]['landmark_distance'] = $landmark_distance;
        $current[$landmark_id]['landmark_measurement'] = $landmark_measurement;
    }
    $sql_data->free_result();
    $sql_data->close();

    if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only

        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
            $landmark_id = substr($field_name,strrpos($field_name,'_')+1);
            $db_field = substr($field_name,0,strrpos($field_name,'_'));

            if ($landmark_id < 100 || (isset($current[$landmark_id][$db_field]) && $current[$landmark_id][$db_field] != $field_value) || !isset($_POST["landmark_delete_$landmark_id"])) {
                $no_change = FALSE;
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }

            if (!isset($_POST["landmark_delete_$landmark_id"])) {
                $field_name = "landmark_delete_$landmark_id";
                $field_value = "off";
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated by client", '', $establishment_code);
			timestamp_establishment($establishment_code);
        }
    } else {

        //Prepare statement - update existing landmark name
        $statement = "UPDATE nse_landmark SET landmark_name=? WHERE landmark_id=?";
        $sql_landmark_update = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - landmark check
        $statement = "SELECT landmark_id, landmark_name FROM nse_landmark WHERE landmark_name=? LIMIT 1";
        $sql_landmark_select = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert new landmark
        $statement = "INSERT INTO nse_landmark (landmark_name, landmark_type_id) VALUES (?,?)";
        $sql_landmark_insert = $GLOBALS['dbCon']->Prepare($statement);

        //Prepare statement - Insert establishment link
        $statement = "INSERT INTO nse_establishment_landmarks (establishment_code, landmark_id, landmark_distance, landmark_measurement) VALUES (?,?,?,?)";
        $sql_establishment_landmarks_insert = $GLOBALS['dbCon']->Prepare($statement);

        //Prepare statement - Get update data
        $statement = "SELECT field_value, update_date, changed_by FROM nse_establishment_updates WHERE establishment_code=? AND field_name=?";
        $sql_establishment_updates_select = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert now log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code, field_name, field_value, submitted_by, approved_by, submitted_field_value, submitted_date) VALUES (?,?,?,?,?,?,?)";
        $sql_establishment_updates_logs_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Delete client log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? AND field_name=? LIMIT 1";
        $sql_establishment_updates_delete = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Delete link
        $statement = "DELETE FROM nse_establishment_landmarks WHERE establishment_code=? AND landmark_id=?";
        $sql_establishment_landmarks_delete = $GLOBALS['dbCon']->prepare($statement);

        $deleted_links = array();
        foreach ($_POST as $field_name=>$field_value) {
			
            $landmark_id = substr($field_name,strrpos($field_name,'_')+1);
            $db_field = substr($field_name,0,strrpos($field_name,'_'));

            if (!isset($_POST["landmark_delete_$landmark_id"])) {
                $sql_establishment_landmarks_delete->bind_param('ss', $establishment_code, $landmark_id);
                $sql_establishment_landmarks_delete->execute();
                $deleted_links[] = $landmark_id;
                unset($new_landmarks[$landmark_id]);

				//Update logs
                foreach ($fields as $field) {
                    $field_name = "{$field}_{$landmark_id}";
                    $field_value = '';
                    $update_date = '';
                    $changed_by = '';

                    //Get client submitted changes (if available)
                    $sql_establishment_updates_select->bind_param('ss', $establishment_code, $field_name);
                    $sql_establishment_updates_select->execute();
                    $sql_establishment_updates_select->store_result();
                    $sql_establishment_updates_select->bind_result($field_value, $update_date, $changed_by);
                    $sql_establishment_updates_select->fetch();
                    $sql_establishment_updates_select->free_result();

                    //Write to log
                    $sql_establishment_updates_logs_insert->bind_param('ssssiss', $establishment_code, $field_name, $landmarks_data[$landmark_id][$field], $changed_by, $_SESSION['j_user']['id'], $field_value, $submitted_date);
                    $sql_establishment_updates_logs_insert->execute();

                    //Clear client submitted log
                    $sql_establishment_updates_delete->bind_param('ss', $establishment_code, $field_name);
                    $sql_establishment_updates_delete->execute();

                }
            }

            if (!in_array($landmark_id, $deleted_links)) {
                if ($landmark_id < 100) {
                    $no_change = FALSE;

                    //Add to new landmarks array
                    if (!isset($new_landmarks[$landmark_id])) $new_landmarks[$landmark_id] = array();
                    $new_landmarks[$landmark_id][$db_field] = $field_value;

                } else if ((isset($current[$landmark_id][$db_field]) && $current[$landmark_id][$db_field] != $field_value)) {
                    if ($db_field == 'landmark_name') {
                        $changes = "$field_name,";
                        $sql_landmark_update->bind_param('ss', $field_value, $landmark_id);
                        $sql_landmark_update->execute();
                    }
                    if ($db_field == 'landmark_distance' || $db_field == 'landmark_measurement') {
                        $changes = "$field_name,";
                        $statement = "UPDATE nse_establishment_landmarks SET $db_field=? WHERE establishment_code=? AND landmark_id=?";
                        $sql_establishment_landmarks = $GLOBALS['dbCon']->prepare($statement);
                        $sql_establishment_landmarks->bind_param('ssi', $field_value, $establishment_code, $landmark_id);
                        $sql_establishment_landmarks->execute();
                        $sql_establishment_landmarks->close();
                    }
                }
				
				//Clear client submitted log
				$sql_establishment_updates_delete->bind_param('ss', $establishment_code, $field_name);
				$sql_establishment_updates_delete->execute();
            }
        }

        //New landmarks
        foreach ($new_landmarks as $landmark_post_id=>$landmark_data) {
            //Check if landmark already exists
            $landmark_id2 = 0;
            $sql_landmark_select->bind_param('s', $landmark_data['landmark_name']);
            $sql_landmark_select->execute();
            $sql_landmark_select->bind_result($landmark_id2, $landmark_name2);
            $sql_landmark_select->fetch();
            $sql_landmark_select->free_result();

            if ($landmark_id2 == 0) { //Save landmark id
                $sql_landmark_insert->bind_param('ss', $landmark_data['landmark_name'], $landmark_data['landmark_group']);
                $sql_landmark_insert->execute();
                $landmark_id2 = $sql_landmark_insert->insert_id;
            }

            //Attach to establishment
            $sql_establishment_landmarks_insert->bind_param('siss', $establishment_code, $landmark_id2, $landmark_data['landmark_distance'], $landmark_data['landmark_measurement']);
            $sql_establishment_landmarks_insert->execute();

            //Update logs
            foreach ($fields as $field) {
                $field_name = "{$field}_{$landmark_post_id}";
                $field_value = '';
                $update_date = '';
                $changed_by = '';

                //Get client submitted changes (if available)
                $sql_establishment_updates_select->bind_param('ss', $establishment_code, $field_name);
                $sql_establishment_updates_select->execute();
                $sql_establishment_updates_select->store_result();
                $sql_establishment_updates_select->bind_result($field_value, $update_date, $changed_by);
                $sql_establishment_updates_select->fetch();
                $sql_establishment_updates_select->free_result();

                //Write to log
                $sql_establishment_updates_logs_insert->bind_param('ssssiss', $establishment_code, $field_name, $landmarks_data[$landmark_id][$field], $changed_by, $_SESSION['j_user']['id'], $field_value, $submitted_date);
                $sql_establishment_updates_logs_insert->execute();

                //Clear client submitted log
                $sql_establishment_updates_delete->bind_param('ss', $establishment_code, $field_name);
                $sql_establishment_updates_delete->execute();

            }
        }

        if (!$no_change) {
            $note_content = "Establishment Data updated (Points of Interest Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
        }
    }
}

//Get current landmarks
$landmark_data = array();
$landmark_types = array();
$landmark_data = array();
$statement = "SELECT l.landmark_type_id,l.landmark_name,e.landmark_id,e.landmark_distance,e.landmark_measurement
                FROM nse_establishment_landmarks AS e
                LEFT JOIN nse_landmark AS l ON e.landmark_id=l.landmark_id
                WHERE e.establishment_code=?";
$sql_landmarks = $GLOBALS['dbCon']->prepare($statement);
$sql_landmarks->bind_param('s', $establishment_code);
$sql_landmarks->execute();
$sql_landmarks->store_result();
$sql_landmarks->bind_result($landmark_type_id,$landmark_name,$landmark_id,$landmark_distance,$landmark_measurement);
while ($sql_landmarks->fetch()) {
    if (!isset($landmark_types[$landmark_type_id])) $landmark_types[$landmark_type_id] = array();
    if (!in_array($landmark_id, $landmark_types[$landmark_type_id])) $landmark_types[$landmark_type_id][] = $landmark_id;
    $landmark_data1[$landmark_id]['landmark_name'] = $landmark_name;
    $landmark_data1[$landmark_id]['landmark_distance'] = $landmark_distance;
    $landmark_data1[$landmark_id]['landmark_measurement'] = $landmark_measurement;
}
$sql_landmarks->free_result();
$sql_landmarks->close();

//Get updated data
$new_landmarks = array();
$changes = '';
$nid = 0;
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    $landmark_id = substr($field_name,strrpos($field_name,'_')+1);
    $db_field = substr($field_name,0,strrpos($field_name,'_'));
    if ($nid < $landmark_id && $landmark_id < 100) $nid = $landmark_id;
    switch ($db_field) {
        default:
            $new_landmarks[$landmark_id][$db_field] = $field_value;
            $changes .= "$field_name,";
    }
}
$sql_updates->free_result();
$sql_updates->close();
$nid++;

foreach ($new_landmarks as $landmark_id=>$data) {
//        if (!isset($landmark_types[$data['landmark_group']])) $landmark_types[$data['landmark_group']] = array();
        if (@!in_array($landmark_id, $landmark_types[$landmark_type_id])) @$landmark_types[$data['landmark_group']][] = $landmark_id;
        if (isset($data['landmark_name'])) $landmark_data1[$landmark_id]['landmark_name'] = $data['landmark_name'];
        if (isset($data['landmark_distance'])) $landmark_data1[$landmark_id]['landmark_distance'] = $data['landmark_distance'];
        if (isset($data['landmark_measurement'])) $landmark_data1[$landmark_id]['landmark_measurement'] = $data['landmark_measurement'];
        if (isset($data['landmark_group'])) $landmark_data1[$landmark_id]['landmark_group'] = $data['landmark_group'];
        if (isset($data['landmark_delete'])) $landmark_data1[$landmark_id]['landmark_delete'] = $data['landmark_delete'];
}

echo "<html>";
echo "<script >var nid=$nid;</script>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/points_of_interest.js></script>";
echo "<script>E_s(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ")</script>";
echo "<script>changes='$changes'</script>";
if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if ($SUB_ACCESS["edit-data"]) {
    $o = "<option value=min >Min<option value=km >Km";
} else {
    $o=array("" => "", "min" => "Min", "km" => "Km");
}

$set = array("Restaurants" => "restaurants", "Shopping Centers / Entertainment" => "entertainment", "Monuments, Museums & Other Historical places" => "historical", "Airports and Train Stations" => "transport", "Hospitals" => "hospitals", "Sports Stadiums" => "sports", "Other places of interest eg. Government buildings" => "other");

foreach ($set as $landmark_type_name => $landmark_type_key) {
    echo "<tr><th colspan=9>" . $landmark_type_name . "</td></tr>";
    echo "<script>E_h()</script>";
    if (isset($landmark_types[$landmark_type_key])) {
        foreach ($landmark_types[$landmark_type_key] as $landmark_id) {
            echo "<tr>";
            if ($SUB_ACCESS['edit-data']) {
                echo "<td width=1><input type=checkbox name=landmark_delete_$landmark_id value=1 ";
                echo isset($landmark_data1[$landmark_id]['landmark_delete'])&&$landmark_data1[$landmark_id]['landmark_delete']=='off'?'':'checked';
                echo " onmouseover=J_TT(this,E_tt) onclick=P_di(this)></td>";
                echo "<th><input type=text name=landmark_name_$landmark_id value='{$landmark_data1[$landmark_id]['landmark_name']}'><input type=hidden name=landmark_group_$landmark_id value='$landmark_type_key' ></th>";
                echo "<td><input type=text name=landmark_distance_$landmark_id value='{$landmark_data1[$landmark_id]['landmark_distance']}'></td>";
                echo "<td><select name=landmark_measurement_$landmark_id><option value=0>" . str_replace("={$landmark_data1[$landmark_id]['landmark_measurement']} >", "={$landmark_data1[$landmark_id]['landmark_measurement']} selected >", $o) . "</select></td>";
            } else {
                echo "<td><b>{$landmark_data1[$landmark_id]['landmark_name']}</b></td>";
                echo "<td style=text-align:right>{$landmark_data1[$landmark_id]['landmark_distance']}</td>";
                echo "<td>{$o[$landmark_data1[$landmark_id]['landmark_measurement']]}</td>";
            }
            echo "</tr>";
        }
    }
    echo "<script>E_t(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ",'$landmark_type_key')</script>";
}

echo "<script>E_d(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ")</script>";

if (!isset($_GET["j_IF"])) {
    $J_title2 = "Establishment Manager";
    $J_title1 = "Contact Details";
    $J_width = 480;
    $J_height = 480;
    $J_icon = "<img src=" . $ROOT . "/ico/set/gohome-edit.png>";
    $J_tooltip = "";
    $J_label = 22;
    $J_nostart = 1;
    $J_nomax = 1;
    include $SDR . "/system/deploy.php";
}

echo "</body></html>";

?>
