<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$id = $_GET["id"];
$establishment_code = $_GET['id'];
$current_icons = array();
$no_change = TRUE;

if (isset($_GET["j_hide"])) {
	//Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (substr($field_name, 0, 2) == 'f_') {
			$changes .= "$field_name,";
		}
    }
    $sql_updates->free_result();
    $sql_updates->close();
	
    echo "<html>";
    echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
		echo "<script src=".$ROOT."/system/P.js></script>";
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/facilities.js></script>";
    echo "<script>J_start('" . $id . "'" . (isset($_GET["view"]) ? ",1" : "") . ")</script>";
	echo "<script>changes='$changes'</script>";
	if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
    die();
}

$SUB_ACCESS = array("edit-data" => 0);
include $SDR . "/system/secure.php";
if (isset($_GET["view"]))
    $SUB_ACCESS["edit-data"]=0;

if (count($_POST)) {

    //Get current facilities
    $statement = "SELECT icon_id FROM nse_establishment_icon WHERE establishment_code=?";
    $sql_establishment_icon_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_establishment_icon_select->bind_param('s', $establishment_code);
    $sql_establishment_icon_select->execute();
    $sql_establishment_icon_select->store_result();
    $sql_establishment_icon_select->bind_result($icon_id);
    while ($sql_establishment_icon_select->fetch()) {
        $current_icons[] = $icon_id;
    }
    $sql_establishment_icon_select->free_result();
    $sql_establishment_icon_select->close();

    //Prepare statement - Check if field exists
    $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
    $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - insert change
    $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
    $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - update field
    $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
    $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Get update data
    $statement = "SELECT field_value, update_date, changed_by FROM nse_establishment_updates WHERE establishment_code=? AND field_name=?";
    $sql_establishment_updates_select = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Insert now log
    $statement = "INSERT INTO nse_establishment_updates_log (establishment_code, field_name, field_value, submitted_by, approved_by, submitted_field_value, submitted_date) VALUES (?,?,?,?,?,?,?)";
    $sql_establishment_updates_logs_insert = $GLOBALS['dbCon']->prepare($statement);

    //Prepare statement - Delete client log
    $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? AND field_name=? LIMIT 1";
    $sql_establishment_updates_delete = $GLOBALS['dbCon']->prepare($statement);

    if ($_SESSION['j_user']['role'] == 'c') {

        //Check if any icons have been removed
        foreach ($current_icons as $c_icon) {
            if (!isset($_POST["f_$c_icon"])) { //Icon has been removed
                $no_change = FALSE;
                $field_name = "f_$c_icon";
                $field_value = '0';
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();

                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }

        //Check for newly added icons
        foreach($_POST as $post_key=>$post_value) {
            list($post_type, $post_icon) = explode('_',$post_key);
            if ($post_type == 'f') {
                if (!in_array($post_icon, $current_icons)) {
                    $no_change = FALSE;
                    $field_name = $post_key;
                    $field_value = '1';
                    $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                    $sql_check_change->execute();
                    $sql_check_change->bind_result($count);
                    $sql_check_change->fetch();
                    $sql_check_change->free_result();

                    if ($count == 0) {
                        $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                        $sql_insert_change->execute();
                    } else {
                        $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                        $sql_update_change->execute();
                    }
                }
            }
        }

        if (!$no_change) {
           $note_content = "Establishment Data updated (Facilities Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
        }

    } else {

        //Prepare statement - delete from current DB
        $statement = "DELETE FROM nse_establishment_icon WHERE establishment_code=? AND icon_id=?";
        $sql_establishment_icon_delete = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Add icon
        $statement = "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUE (?,?)";
        $sql_establishment_icon_insert = $GLOBALS['dbCon']->prepare($statement);

        foreach ($current_icons as $c_icon) {

            if (!isset($_POST["f_$c_icon"])) { //Icon has been removed
                $no_change = FALSE;

                //Delete From current icons in DB
                $sql_establishment_icon_delete->bind_param('si', $establishment_code, $c_icon);
                $sql_establishment_icon_delete->execute();

                $field_name = "f_$c_icon";
                $field_value = '';
                $update_date = '';
                $changed_by = '';

                //Get client submitted changes (if available)
                $sql_establishment_updates_select->bind_param('ss', $establishment_code, $field_name);
                $sql_establishment_updates_select->execute();
                $sql_establishment_updates_select->store_result();
                $sql_establishment_updates_select->bind_result($field_value, $update_date, $changed_by);
                $sql_establishment_updates_select->fetch();
                $sql_establishment_updates_select->free_result();

                //Write to log
                $sql_establishment_updates_logs_insert->bind_param('ssssiss', $establishment_code, $field_name, $field_value, $changed_by, $_SESSION['j_user']['id'], $field_value, $submitted_date);
                $sql_establishment_updates_logs_insert->execute();

                //Clear client submitted log
                $sql_establishment_updates_delete->bind_param('ss', $establishment_code, $field_name);
                $sql_establishment_updates_delete->execute();
            }
        }

        //Check for newly added icons
        foreach($_POST as $post_key=>$post_value) {
            list($post_type, $post_icon) = explode('_',$post_key);
            if ($post_type == 'f') {
                if (!in_array($post_icon, $current_icons)) {

					//Add Icon
                    $sql_establishment_icon_insert->bind_param('si', $establishment_code, $post_icon);
                    $sql_establishment_icon_insert->execute();

                    $no_change = FALSE;
                    $field_name = $post_key;
                    $field_value = '1';
                    $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                    $sql_check_change->execute();
                    $sql_check_change->bind_result($count);
                    $sql_check_change->fetch();
                    $sql_check_change->free_result();

                    //Get client submitted changes (if available)
                    $sql_establishment_updates_select->bind_param('ss', $establishment_code, $field_name);
                    $sql_establishment_updates_select->execute();
                    $sql_establishment_updates_select->store_result();
                    $sql_establishment_updates_select->bind_result($field_value, $update_date, $changed_by);
                    $sql_establishment_updates_select->fetch();
                    $sql_establishment_updates_select->free_result();

                    //Write to log
                    $sql_establishment_updates_logs_insert->bind_param('ssssiss', $establishment_code, $field_name, $field_value, $changed_by, $_SESSION['j_user']['id'], $field_value, $submitted_date);
                    $sql_establishment_updates_logs_insert->execute();

                    //Clear client submitted log
                    $sql_establishment_updates_delete->bind_param('ss', $establishment_code, $field_name);
                    $sql_establishment_updates_delete->execute();
                }
            }
        }

        if (!$no_change) {
            $note_content = "Establishment Data updated (Facilities Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
			timestamp_establishment($establishment_code);
        }

    }

}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/facilities.js></script>";
echo "<script>E_s(" . ($SUB_ACCESS["edit-data"] ? 1 : "") . ")</script>";

$l = "";
$q = "SELECT c.icon_category_id,l.icon_name FROM nse_icon_category AS c JOIN nse_icon_category_lang AS l ON c.icon_category_id=l.icon_category_id ORDER BY c.icon_priority";
$q = mysql_query($q);
if (mysql_num_rows($q)) {
    while ($g = mysql_fetch_array($q)) {
        $l.="~" . $g["icon_name"] . "|";
        $qi = "SELECT DISTINCT i.icon_id,i.icon_url,l.icon_description FROM nse_icon AS i JOIN nse_icon_lang AS l ON i.icon_id=l.icon_id WHERE icon_category_id=" . $g["icon_category_id"] . " ORDER BY l.icon_description";
        $qi = mysql_query($qi);
        if (mysql_num_rows($qi)) {
            while ($gi = mysql_fetch_array($qi))
                $l.=$gi["icon_id"] . "~" . $gi["icon_description"] . "~" . str_replace(".gif", "", substr($gi["icon_url"], strrpos($gi["icon_url"], "/"))) . "|";
        }
    }
}

$a = "";
$q = "SELECT * FROM nse_establishment_icon WHERE establishment_code='" . $id . "'";
$q = mysql_query($q);
if (mysql_num_rows($q)) {
    while ($g = mysql_fetch_array($q))
        $a.=$g["icon_id"] . "~";
}

//Get updated fields
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    if (substr($field_name,0,2) == 'f_') {
        $icon_id = substr($field_name, 2);
        if ($field_value == '0') {
          $a = str_replace("~$icon_id~",'~', $a);
        }
        if ($field_value == '1') {
            $a .= "$icon_id~";
        }
    }
}
$sql_updates->free_result();
$sql_updates->close();

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
	if (substr($field_name, 0, 2) == 'f_') {
		$changes .= "$field_name,";
	}
}
$sql_updates->free_result();
$sql_updates->close();

echo "<script>E_d(" . $SUB_ACCESS["edit-data"] . ",\"" . $l . "\",\"" . $a . "\")</script>";

if ($SUB_ACCESS["edit-data"])
	echo "<script>changes='$changes'</script>";
    echo "<script src=" . $ROOT . "/apps/establishment_manager/sections/saver.js></script>";

if (!isset($_GET["j_IF"])) {
    $J_title2 = "Establishment Manager";
    $J_title1 = "Facilities";
    $J_width = 480;
    $J_height = 480;
    $J_icon = "<img src=" . $ROOT . "/ico/set/gohome-edit.png>";
    $J_tooltip = "";
    $J_label = 22;
    $J_nostart = 1;
    $J_nomax = 1;
    include $SDR . "/system/deploy.php";
}

echo "</body></html>";
?>