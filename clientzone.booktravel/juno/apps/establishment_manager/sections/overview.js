/**
 * Overview JS file
 *
 * @version $Id$
 * @author Thilo Muller(2011)
 */

j_P.j_Wi[j_W]["F"][j_Wn]["sup"].innerHTML="<img src="+ROOT+"/ico/set/summary.gif id=jtab onmouseover=J_OP(this,100) onmouseout=J_OP(this,70) class=jO70 onclick=\"J_resize("+j_W+",'"+j_P.J_reSet(j_Wn,-1)+"')\">"

document.write("<style type=text/css>th{white-space:nowrap;width:50;color:#888}</style><body onclick=\"j_P.J_resize("+j_W+",'"+j_P.J_reSet(j_Wn,-1)+"')\"><table id=jTR width=100% cellspacing=0 cellpadding=0>")

function E_d(no,v,d,e,yh,ya,yr,yd,ye,z)
{
	document.write("</table>")
	J_tr()
//	var f=j_Wn
	var f = 1
	J_frame(f+1,ROOT+"/apps/establishment_manager/sections/detail.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+2,ROOT+"/apps/establishment_manager/sections/reservation_details.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+3,ROOT+"/apps/establishment_manager/sections/caravan.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+4,ROOT+"/apps/establishment_manager/sections/type.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+5,ROOT+"/apps/establishment_manager/sections/descriptions.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+6,ROOT+"/apps/establishment_manager/sections/facilities.php?j_hide&id="+d+(v?"&view=1":""))
	if (no) J_frame(f+7,ROOT+"/apps/establishment_manager/sections/themes.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+8,ROOT+"/apps/establishment_manager/sections/pricing.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+9,ROOT+"/apps/establishment_manager/sections/policies.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+10,ROOT+"/apps/establishment_manager/sections/location.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+11,ROOT+"/apps/establishment_manager/sections/points_of_interest.php?j_hide&id="+d+(v?"&view=1":""))
	J_frame(f+12,ROOT+"/apps/establishment_manager/sections/images.php?j_hide&id="+d+(v?"&view=1":""))
	if (no) J_frame(f+13,ROOT+"/apps/establishment_manager/sections/contacts_v2.php?j_hide&id="+d+(v?"&view=1":""))
	if (no) J_frame(f+14,ROOT+"/apps/establishment_manager/sections/history.php?j_hide&id="+d+(v?"&view=1":""))
	if (no) J_frame(f+15,ROOT+"/apps/establishment_manager/sections/dbase.php?j_hide&id="+d+(v?"&view=1":""))
	if (no) J_frame(f+16,ROOT+"/apps/establishment_manager/sections/notes.php?j_hide&id="+d+(v?"&view=1":""))
	a=j_P.j_Wi[j_W]
	a["h1"].innerHTML=e?e:'Untitled Establishment';
	a["h2"].innerHTML=(z?"<img src="+ROOT+"/ico/set/no_entry.png style='height:48;width:48;float:left;margin:0 4 0 0'> <b style=color:#EE0000>This Establishment is In-active</b>":"Establishment Editor")
	a["h2"].style.display="block"
	a["h3"].innerHTML="Establishment Code: "+d
	a["h3"].style.display="block"
	a["tool"]["tip"]="<center><img src="+ROOT+"/ico/set/gohome-edit.png>"+(z?"<var><img src="+ROOT+"/ico/set/no_entry.png></var>":"<br>")+"Establishment Editor<br><b>"+e+"</b></center>"
	a["icon_div"].innerHTML="<img src="+ROOT+"/ico/set/gohome-edit.png>"+(z?"<var><img src="+ROOT+"/ico/set/no_entry.png></var>":"")
	a=""
	if(f)
		a+="<a href=go: onclick=\"J_W('"+ROOT+"/apps/establishment_manager/contact.php');return false\" onmouseover=\"J_TT(this,'<b>Office: 011 713-2000</b> or click for more contact details')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/phone2.png></a>"
	a+="<a href='http://www.aatravel.co.za/accommodation/"+ d +"' target='_blank' onfocus=blur() onmouseover=\"J_TT(this,'View Listing on AA Travel Website')\" class=jW3a><img src="+ROOT+"/ico/set/net_set.png></a>"
	if (no)a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/create_orders/create_order.php?eid="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Create an Order')\" class=jW3a><img src="+ROOT+"/ico/set/spreadsheet_yellow.png></a>"
	if (no)a+="<a href=go: onclick=\"J_W(ROOT+'/apps/accounts/invoices/age_view.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Establishment Accounts')\" class=jW3a><img src="+ROOT+"/ico/set/accounts_statements.png></a>"
	if (no)a+="<a href=go: onclick=\"J_W(ROOT+'/apps/image_manager/manager.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Image Manager')\" class=jW3a><img src="+ROOT+"/ico/set/img.png></a>"
	if(yd)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/documents.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Documents')\" class=jW3a><img src="+ROOT+"/ico/set/file_drawer.png></a>"
	if(yh)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/admin/history.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Activity History')\" class=jW3a><img src="+ROOT+"/ico/set/activity.png></a>"
	if(yr)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/reports/user_activity.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'User reports')\" class=jW3a><img src="+ROOT+"/ico/set/chart.png></a>"
	if(ya)
		a+="<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/admin/admin.php?id="+d+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'Admin Options')\" class=jW3a><img src="+ROOT+"/ico/set/set.png></a>"
	J_footer(a)
}
