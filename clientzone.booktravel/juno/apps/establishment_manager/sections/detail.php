<?php

/**
 * Details page of establishment manager.
 *
 * @version $Id: detail.php 1321 2011-11-17 12:00:16Z web37 $
 */


require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/shared/PHPMailer/class.phpmailer.php';

//Security
$SUB_ACCESS=array("edit-data"=>0,"edit-assessor"=>0,"edit-sales-rep"=>0,"edit-qa-enter-date"=>0);
include $SDR."/system/secure.php";

if(isset($_GET["view"]))
{
	$SUB_ACCESS["edit-data"]=0;
	$SUB_ACCESS["edit-assessor"]=0;
	$SUB_ACCESS["edit-sales-rep"]=0;
	$SUB_ACCESS["edit-qa-enter-date"]=0;
}

include $SDR."/system/get.php";

//Vars
$establishment_code=isset($_GET["id"])?$_GET["id"]:0;
$mail = new PHPMailer(true);
$vat_number = '';
$company_name = '';
$company_number = '';
$aa_categories = array();
$no_change = TRUE;
$aa_category_options = '';
$estab_fields = array('establishment_name', 'website_url', 'aa_category_code', 'advertiser', 'assessor_id' , 'rep_id1', 'rep_id2',
		'nightsbridge_bbid', 'staysys_code', 'id_360', 'server_360', 'aa_estab', 'aa_zim',
		'etravel','enter_date', 'premium_member');
$billing_fields = array('vat_number', 'company_name', 'company_number', 'billing_line1', 'billing_line2', 'billing_line3', 'billing_code');
$data_fields = array('tv_licence', 'tv_count');
$insert=isset($_GET['insert'])?1:0;
$billing_line1 = '';
$billing_line2 = '';
$billing_line3 = '';
$billing_code = '';
$n1_code = '';
$tv_licence = '';
$tv_count = '';


if(count($_POST) && $SUB_ACCESS["edit-data"]) {

	if ($_GET['id'] == 'new') {
		$establishment_code = next_establishment_code();

		//Insert Establishment Data
		$statement = "INSERT INTO nse_establishment (establishment_code, establishment_name, creation_date) VALUES (?,?,NOW())";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_insert->bind_param('ss', $establishment_code, $_POST['establishment_name']);
		$sql_insert->execute();
		$insert=1;
	}

	//Get Establishment Data
	$statement = "SELECT establishment_name, website_url, postal_address_line1, postal_address_line2, postal_address_line3,
					postal_address_code, aa_category_code, advertiser, assessor_id , rep_id1, rep_id2, nightsbridge_bbid,
					staysys_code, pastel_code, 360_id, 360_server, active, aa_estab, aa_zim, etravel, enter_date, n1_code,
					no_contact, no_contact_reason, premium_member
					FROM nse_establishment
					WHERE establishment_code=? LIMIT 1";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $establishment_code);
	$sql_estab->execute();
	$sql_estab->bind_result($establishment_name, $website_url, $postal_address_line1, $postal_address_line2, $postal_address_line3, $postal_address_code, $aa_category_code, $advertiser, $assessor_id, $rep_id1, $rep_id2, $nightsbridge_bbid, $staysys_code, $pastel_code, $id_360, $server_360, $active, $aa_estab, $aa_zim, $etravel, $enter_date, $n1_code, $no_contact, $no_contact_reason, $premium_member);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	if (!isset($assessor_id)) $assessor_id = '';
	if (!isset($nightsbridge_bbid)) $nightsbridge_bbid = '';
	if (!isset($n1_code)) $n1_code = '';
	if (!isset($server_360)) $server_360 = '';
	if (!isset($id_360)) $id_360 = '';
	if (!isset($aa_category_code)) $aa_category_code = '';
	if (!isset($enter_date)) $enter_date = '';

	//Get billing data
	$statement = "SELECT vat_number, company_name, company_number, billing_line1, billing_line2, billing_line3, billing_code FROM nse_establishment_billing WHERE establishment_code=? LIMIT 1";
	$sql_billing = $GLOBALS['dbCon']->prepare($statement);
	$sql_billing->bind_param('s', $establishment_code);
	$sql_billing->execute();
	$sql_billing->bind_result($vat_number, $company_name, $company_number, $billing_line1, $billing_line2, $billing_line3, $billing_code);
	$sql_billing->fetch();
	$sql_billing->free_result();
	$sql_billing->close();
	if (!isset($billing_line1)) $billing_line1 = '';
	if (!isset($billing_line2)) $billing_line2 = '';
	if (!isset($billing_line3)) $billing_line3 = '';
	if (!isset($billing_code)) $billing_code = '';
	if (!isset($vat_number)) $vat_number = '';
	if (!isset($company_name)) $company_name = '';
	if (!isset($company_number)) $company_number = '';

	//Get TV Licence
	$statement = "SELECT tv_licence, tv_count FROM nse_establishment_data WHERE establishment_code=? LIMIT 1";
	$sql_tv = $GLOBALS['dbCon']->prepare($statement);
	$sql_tv->bind_param('s', $establishment_code);
	$sql_tv->execute();
	$sql_tv->bind_result($tv_licence, $tv_count);
	$sql_tv->fetch();
	$sql_tv->free_result();
	$sql_tv->close();
	if (!isset($tv_count)) $tv_count = '';
	if (!isset($tv_licence)) $tv_licence = '';

	//Get location data
	$country=0;
	$country_name="";
	$province=0;
	$province_name="";
	$town=0;
	$town_name="";
	$suburb=0;
	$suburb_name="";
	$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $establishment_code);
	$sql_location->execute();
	$sql_location->bind_result($country_id, $province_id, $town_id, $suburb_id);
	$sql_location->fetch();
	$sql_location->free_result();
	$sql_location->close();
	if (!empty($country_id)) {
		$country=$country_id;
		$country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
	}
	if (!empty($province_id)) {
		$province=$province_id;
		$province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
	}
	if(!empty($town_id)) {
		$town=$town_id;
		$town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
	}
	if(!empty($suburb_id)) {
		$suburb=$suburb_id;
		$suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
	}

	if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only

		//Prepare statement - Check if field exists
		$statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
		$sql_check_change = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - insert change
		$statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
		$sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - update field
		$statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
		$sql_update_change = $GLOBALS['dbCon']->prepare($statement);

		foreach ($_POST as $field_name=>$field_value) {
			if (isset($$field_name) && $field_value != $$field_name) {
				$no_change = FALSE;
				$sql_check_change->bind_param('ss', $establishment_code, $field_name);
				$sql_check_change->execute();
				$sql_check_change->bind_result($count);
				$sql_check_change->fetch();
				$sql_check_change->free_result();

				if ($count == 0) {
					$sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
					$sql_insert_change->execute();
				} else {
					$sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
					$sql_update_change->execute();
				}
			}
		}
		$sql_insert_change->close();
		$sql_update_change->close();

		if (!$no_change) {
			//Check if log entry exists for today
			$log_count = 0;
			$day = date('d');
			$month = date('m');
			$year = date('y');
			$start_date = mktime(0, 0, 0, $month, $day, $year);
			$end_date = mktime(23, 59, 59, $month, $day, $year);
			$statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data update by client'";
			$sql_check = $GLOBALS['dbCon']->prepare($statement);
			$sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
			$sql_check->execute();
			$sql_check->bind_result($log_count);
			$sql_check->fetch();
			$sql_check->free_result();
			$sql_check->close();

			if ($log_count == 0 && isset($_SESSION['client_save'])) insertNote($establishment_code, 4, 0, "Establishment data update by client");
			$_SESSION['client_save'] = 'Y';
		}
	} else {

		//Prepare statement - get submitted changes
		$statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
		$sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - Insert permanent log
		$statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
		$sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statment - clear temp log
		$statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
		$sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

		if (!isset($rep_id1)) $rep_id1 = 0;
		if (!isset($rep_id2)) $rep_id2 = 0;
		if (!isset($_POST['premium_member'])) $_POST['premium_member'] = 'N';
		foreach ($_POST as $field_name=>$field_value) {
			if (isset($$field_name) && $field_value != $$field_name) {
				$submitted_by = '';
				$submitted_date = '';
				$submitted_field_value = '';

				//Get client submitted changes
				$sql_submitted_data->bind_param('ss', $establishment_code, $field_name);
				$sql_submitted_data->execute();
				$sql_submitted_data->store_result();
				if ($sql_submitted_data->num_rows == 1) {
					$sql_submitted_data->bind_result($submitted_by, $submitted_field_value, $submitted_date);
					$sql_submitted_data->fetch();
					$sql_submitted_data->free_result();
				}

				//Write to permanent log
				$sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
				$sql_log_insert->execute();

				//Clear client log
				$sql_clear_log->bind_param('ss', $establishment_code, $field_name);
				$sql_clear_log->execute();

				$changed_fields[$field_name] = $field_value;
			}
		}
		$sql_clear_log->close();
		$sql_log_insert->close();
		$sql_submitted_data->close();

		//Check if estab exists in billing table
		$statement = "SELECT count(*) FROM nse_establishment_billing WHERE establishment_code=?";
		$sql_billing_check = $GLOBALS['dbCon']->prepare($statement);
		$sql_billing_check->bind_param('s', $establishment_code);
		$sql_billing_check->execute();
		$sql_billing_check->bind_result($count);
		$sql_billing_check->fetch();
		$sql_billing_check->close();

		//Insert billing entry if not exist
		if ($count == 0) {
			$statement = "INSERT INTO nse_establishment_billing (establishment_code) VALUES (?)";
			$sql_insert_billing = $GLOBALS['dbCon']->prepare($statement);
			$sql_insert_billing->bind_param('s', $establishment_code);
			$sql_insert_billing->execute();
			$sql_insert_billing->close();
		}

		//Check if estab exists in data table
		$statement = "SELECT count(*) FROM nse_establishment_data WHERE establishment_code=?";
		$sql_data_check = $GLOBALS['dbCon']->prepare($statement);
		$sql_data_check->bind_param('s', $establishment_code);
		$sql_data_check->execute();
		$sql_data_check->bind_result($count_data);
		$sql_data_check->fetch();
		$sql_data_check->close();

		//Insert data entry if not exist
		if ($count_data == 0) {
			$statement = "INSERT INTO nse_establishment_data (establishment_code) VALUES (?)";
			$sql_insert_data = $GLOBALS['dbCon']->prepare($statement);
			$sql_insert_data->bind_param('s', $establishment_code);
			$sql_insert_data->execute();
			$sql_insert_data->close();
		}

		//Get TV Licence
		$statement = "SELECT tv_licence FROM nse_establishment_data WHERE establishment_code=? LIMIT 1";
		$sql_tv = $GLOBALS['dbCon']->prepare($statement);
		$sql_tv->bind_param('s', $establishment_code);
		$sql_tv->execute();
		$sql_tv->bind_result($tv_licence);
		$sql_tv->fetch();
		$sql_tv->free_result();
		$sql_tv->close();
		if ($tv_licence != $_POST['tv_licence']) {
			$statement = "UPDATE nse_establishment_data SET tv_licence=? WHERE establishment_code=? LIMIT 1";
			$sql_tv = $GLOBALS['dbCon']->prepare($statement);
			$sql_tv->bind_param('ss', $_POST['tv_licence'], $establishment_code);
			$sql_tv->execute();
			$sql_tv->close();
		}

		//Logo on site
		$logo_on_site = isset($_POST['logo_on_site'])?'Y':'';
		$statement = "SELECT logo_on_site
						FROM nse_establishment_data
						WHERE establishment_code=? LIMIT 1";
		$sql_logo = $GLOBALS['dbCon']->prepare($statement);
		$sql_logo->bind_param('s', $establishment_code);
		$sql_logo->execute();
		$sql_logo->bind_result($logo_on_site_curr);
		$sql_logo->fetch();
		$sql_logo->free_result();
		$sql_logo->close();
		if ($logo_on_site_curr != $logo_on_site) {
			$statement = "UPDATE nse_establishment_data
								SET logo_on_site=?
								WHERE establishment_code=?
								LIMIT 1";
			$sql_logo = $GLOBALS['dbCon']->prepare($statement);
			$sql_logo->bind_param('ss', $logo_on_site, $establishment_code);
			$sql_logo->execute();
			$sql_logo->close();
		}

		//Nedbank Member
		$nedbank_member = isset($_POST['nedbank_member'])?'Y':'';
		$statement = "SELECT nedbank_member
						FROM nse_establishment_data
						WHERE establishment_code=? LIMIT 1";
		$sql_nedbank = $GLOBALS['dbCon']->prepare($statement);
		$sql_nedbank->bind_param('s', $establishment_code);
		$sql_nedbank->execute();
		$sql_nedbank->bind_result($nedbank_member_curr);
		$sql_nedbank->fetch();
		$sql_nedbank->free_result();
		$sql_nedbank->close();
		if ($nedbank_member_curr != $nedbank_member) {
			$statement = "UPDATE nse_establishment_data
								SET nedbank_member=?
								WHERE establishment_code=?
								LIMIT 1";
			$sql_nedbank = $GLOBALS['dbCon']->prepare($statement);
			$sql_nedbank->bind_param('ss', $nedbank_member, $establishment_code);
			$sql_nedbank->execute();
			$sql_nedbank->close();
		}

		//Active status change
		$cActive = isset($_POST['active'])?'1':'';
		if ($cActive != $active) {
			$statement = "UPDATE nse_establishment SET active=? WHERE establishment_code=?";
			$sql_active = $GLOBALS['dbCon']->prepare($statement);
			$sql_active->bind_param('ss', $cActive, $establishment_code);
			$sql_active->execute();
			$sql_active->close();

			if ($cActive == '') {
				insertNote($establishment_code, 4, 0, "Establishment De-activated: {$_POST['deactivate_reason']}");
			} else {
				insertNote($establishment_code, 4, 0, "Establishment Activated");
			}

		}

		//Name change
		if ($establishment_name != $_POST['establishment_name']) {
			insertNote($establishment_code, 4, 0, "Establishment name change from $establishment_name to {$_POST['establishment_name']}");
		}

		//Check and update aa_estab
		$naa_estab = isset($_POST['aa_estab'])?'Y':'';
		if ($naa_estab != $aa_estab) {
			$statement = "UPDATE nse_establishment SET aa_estab=? WHERE establishment_code=?";
			$sql_aa_estab = $GLOBALS['dbCon']->prepare($statement);
			$sql_aa_estab->bind_param('ss',$naa_estab, $establishment_code);
			$sql_aa_estab->execute();
			$sql_aa_estab->close();

			$no_change = FALSE;
		}

		//Check and update aa_zim
		$naa_zim = isset($_POST['aa_zim'])?'Y':'';
		if ($naa_zim != $aa_zim) {
			$statement = "UPDATE nse_establishment SET aa_zim=? WHERE establishment_code=?";
			$sql_aa_zim = $GLOBALS['dbCon']->prepare($statement);
			$sql_aa_zim->bind_param('ss',$naa_zim, $establishment_code);
			$sql_aa_zim->execute();
			$sql_aa_zim->close();

			$no_change = FALSE;
		}

		//Check and update etravel
		$netravel = isset($_POST['etravel'])?'Y':'';
		if ($netravel != $etravel) {
			$statement = "UPDATE nse_establishment SET etravel=? WHERE establishment_code=?";
			$sql_etravel = $GLOBALS['dbCon']->prepare($statement);
			$sql_etravel->bind_param('ss',$netravel, $establishment_code);
			$sql_etravel->execute();
			$sql_etravel->close();

			$no_change = FALSE;
		}

		//Change of Assessor
		if (isset($_POST['assessor_id']) && $_POST['assessor_id'] != $assessor_id && !empty($_POST['assessor_id'])) {
			$note_date = mktime();

			//Get assessor name
			$statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
			$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
			$sql_assessor->bind_param('i', $_POST['assessor_id']);
			$sql_assessor->execute();
			$sql_assessor->bind_result($assessor_name, $assessor_email);
			$sql_assessor->fetch();
			$sql_assessor->close();

			$statement = "UPDATE nse_establishment SET assessor_date=NOW() WHERE establishment_code=?";
			$sql_rep_date = $GLOBALS['dbCon']->prepare($statement);
			$sql_rep_date->bind_param('s', $establishment_code);
			$sql_rep_date->execute();
			$sql_rep_date->close();

			$statement = "INSERT INTO nse_history_assessor_rep (establishment_code, user_id, user_date, user_type) VALUES (?,?,NOW(),'assessor')";
			$sql_history = $GLOBALS['dbCon']->prepare($statement);
			$sql_history->bind_param('si', $establishment_code, $_POST['assessor_id']);
			$sql_history->execute();
			$sql_history->close();

			//Email assessor
			ob_start();
			include 'email_summary.php';
			$email_body = ob_get_clean();

			$subject = "New establishment added to your portfolio: $establishment_name";
			//Assemble email
			try {
				$mail->ClearAllRecipients();
				$mail->AddAddress($assessor_email, $assessor_name);
				$mail->SetFrom('webmaster@aa-travel.co.za', 'Admin');
				$mail->Subject = $subject;
				$mail->MsgHTML($email_body);
				$mail->Send();
			} catch (phpmailerException $e) {
				echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
			}

			$note_content = "$assessor_name added as assessor to $establishment_name";
			insertNote($establishment_code, 4, 0, $note_content);
		}

		//Change of Rep 1
		if (isset($_POST['rep_id1']) && $_POST['rep_id1'] != $rep_id1 && !empty($_POST['rep_id1'])) {
			$note_date = mktime();

			//Get Rep1 name
			$statement = "SELECT CONCAT(firstname, ' ', lastname), email FROM nse_user WHERE user_id=?";
			$sql_rep1 = $GLOBALS['dbCon']->prepare($statement);
			$sql_rep1->bind_param('i', $_POST['rep_id1']);
			$sql_rep1->execute();
			$sql_rep1->bind_result($rep1_name, $rep_email);
			$sql_rep1->fetch();
			$sql_rep1->close();

			$statement = "UPDATE nse_establishment SET rep1_date=NOW() WHERE establishment_code=?";
			$sql_rep_date = $GLOBALS['dbCon']->prepare($statement);
			$sql_rep_date->bind_param('s', $establishment_code);
			$sql_rep_date->execute();
			$sql_rep_date->close();

			$statement = "INSERT INTO nse_history_assessor_rep (establishment_code, user_id, user_date, user_type) VALUES (?,?,NOW(),'rep')";
			$sql_history = $GLOBALS['dbCon']->prepare($statement);
			$sql_history->bind_param('si', $establishment_code, $_POST['rep_id1']);
			$sql_history->execute();
			$sql_history->close();

			//Email assessor
			ob_start();
			include 'email_summary.php';
			$email_body = ob_get_clean();

			$subject = "New establishment added to your portfolio: $establishment_name";
			//Assemble email
			try {
				$mail->ClearAllRecipients();
				$mail->AddAddress($rep_email, $rep1_name);
				$mail->SetFrom('webmaster@aa-travel.co.za', 'Admin');
				$mail->Subject = $subject;
				$mail->MsgHTML($email_body);
				$mail->Send();
			} catch (phpmailerException $e) {
				echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
			}

			$note_content = "$rep1_name added as sales rep to $establishment_name";
			insertNote($establishment_code, 4, 0, $note_content);
		}

		//Change of Rep 2
		if (isset($_POST['rep_id2']) && $_POST['rep_id2'] != $rep_id2 && !empty($_POST['rep_id2'])) {
			$note_date = mktime();

			//Get Rep2 name
			$statement = "SELECT CONCAT(firstname, ' ', lastname) FROM nse_user WHERE user_id=?";
			$sql_rep2 = $GLOBALS['dbCon']->prepare($statement);
			$sql_rep2->bind_param('i', $_POST['rep_id2']);
			$sql_rep2->execute();
			$sql_rep2->bind_result($rep2_name);
			$sql_rep2->fetch();
			$sql_rep2->close();

			$note_content = "$rep2_name added as co-sales rep to $establishment_name";
			insertNote($establishment_code, 4, 0, $note_content);
		}

		//Update contact status
		$_POST['noContact'] = isset($_POST['noContact'])?'Y':'';
		if ($no_contact != $_POST['noContact']) {
			$no_contact_reason_post = $_POST['noContact']=='Y'?$_POST['noContactReason']:'';
			$statement = "UPDATE nse_establishment
						SET no_contact=:no_contact,
							no_contact_reason=:no_contact_reason,
							no_contact_date = NOW()
						WHERE establishment_code = :establishment_code
						LIMIT 1";
			$sql_no = $GLOBALS['db_pdo']->prepare($statement);
			$sql_no->bindParam(':no_contact', $_POST['noContact']);
			$sql_no->bindParam(':no_contact_reason', $no_contact_reason_post);
			$sql_no->bindParam(':establishment_code', $establishment_code);
			$sql_no->execute();

		}


		if (!$no_change) {
			//Check if log entry exists for today
			$log_count = 0;
			$day = date('d');
			$month = date('m');
			$year = date('y');
			$start_date = mktime(0, 0, 0, $month, $day, $year);
			$end_date = mktime(23, 59, 59, $month, $day, $year);
			$statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated or approved'";
			$sql_check = $GLOBALS['dbCon']->prepare($statement);
			$sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
			$sql_check->execute();
			$sql_check->bind_result($log_count);
			$sql_check->fetch();
			$sql_check->free_result();
			$sql_check->close();

			if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated or approved", '', $establishment_code);
		}

		if (!empty($changed_fields)) {

			foreach ($changed_fields as $field_name=>$field_value) {
				$no_change = FALSE;
				if (in_array($field_name, $estab_fields)) {
					if ($field_name == 'id_360') $field_name = '360_id';
					if ($field_name == 'server_360') $field_name = '360_server';
					if ($field_name == 'nightsbridge_bbid') $field_value = trim($field_value);

					$statement = "UPDATE nse_establishment SET $field_name=? WHERE establishment_code=?";
					$sql_estab = $GLOBALS['dbCon']->prepare($statement);
					$sql_estab->bind_param('ss',  $field_value, $establishment_code);
					$sql_estab->execute();
					$sql_estab->close();

				} else if (in_array($field_name, $billing_fields)) {
					$statement = "UPDATE nse_establishment_billing SET $field_name=? WHERE establishment_code=?";
					$sql_billing = $GLOBALS['dbCon']->prepare($statement);
					$sql_billing->bind_param('ss',  $field_value, $establishment_code);
					$sql_billing->execute();
					$sql_billing->close();

				} else if (in_array($field_name, $data_fields)) {
					$statement = "UPDATE nse_establishment_data SET $field_name=? WHERE establishment_code=?";
					$sql_data = $GLOBALS['dbCon']->prepare($statement);
					$sql_data->bind_param('ss',  $field_value, $establishment_code);
					$sql_data->execute();
					$sql_data->close();
				}


			}
			$note_content = "Establishment Data updated (Details Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
			timestamp_establishment($establishment_code);
		}
		unset($_SESSION["juno"]["accounts"]["company"][$establishment_code]);
	}

	//Get TV Licence
	$statement = "SELECT tv_licence, tv_count FROM nse_establishment_data WHERE establishment_code=? LIMIT 1";
	$sql_tv = $GLOBALS['dbCon']->prepare($statement);
	$sql_tv->bind_param('s', $establishment_code);
	$sql_tv->execute();
	$sql_tv->bind_result($tv_licence, $tv_count);
	$sql_tv->fetch();
	$sql_tv->free_result();
	$sql_tv->close();



	if ($_GET['id'] == 'new') {
		echo "<script>document.location='/juno/apps/establishment_manager/sections/overview.php?id=$establishment_code'</script>";
		die();
	}

}

//Get Establishment Data
$statement = "SELECT establishment_name, website_url, postal_address_line1, postal_address_line2, postal_address_line3,
						postal_address_code, aa_category_code, advertiser, assessor_id , rep_id1, rep_id2, nightsbridge_bbid,
						staysys_code, pastel_code, 360_id, 360_server, active, enter_date, aa_estab, aa_zim, etravel,
						n1_code, no_contact, no_contact_date, no_contact_reason, premium_member
                        FROM nse_establishment
                        WHERE establishment_code=? LIMIT 1";
$sql_estab = $GLOBALS['dbCon']->prepare($statement);
$sql_estab->bind_param('s', $establishment_code);
$sql_estab->execute();
$sql_estab->bind_result($establishment_name, $website_url, $postal_address_line1, $postal_address_line2, $postal_address_line3, $postal_address_code, $aa_endorsement, $advertiser, $assessor_id, $rep_id1, $rep_id2, $nightsbridge_bbid, $staysys_code, $pastel_code, $id_360, $server_360, $active, $entered_date, $aa_estab, $aa_zim, $etravel, $n1_code, $no_contact, $no_contact_date, $no_contact_reason, $premium_member);
$sql_estab->fetch();
$sql_estab->free_result();
$sql_estab->close();


//Get billing data
$statement = "SELECT vat_number, company_name, company_number, billing_line1, billing_line2, billing_line3, billing_code FROM nse_establishment_billing WHERE establishment_code=? LIMIT 1";
$sql_billing = $GLOBALS['dbCon']->prepare($statement);
$sql_billing->bind_param('s', $establishment_code);
$sql_billing->execute();
$sql_billing->bind_result($vat_number, $company_name, $company_number, $billing_line1, $billing_line2, $billing_line3, $billing_code);
$sql_billing->fetch();
$sql_billing->free_result();
$sql_billing->close();

//Get TV Licence
$statement = "SELECT tv_licence, tv_count, logo_on_site, nedbank_member
				FROM nse_establishment_data
				WHERE establishment_code=? LIMIT 1";
$sql_tv = $GLOBALS['dbCon']->prepare($statement);
$sql_tv->bind_param('s', $establishment_code);
$sql_tv->execute();
$sql_tv->bind_result($tv_licence, $tv_count, $logo_on_site, $nedbank_member);
$sql_tv->fetch();
$sql_tv->free_result();
$sql_tv->close();

//Get location data
$country=0;
$country_name="";
$province=0;
$province_name="";
$town=0;
$town_name="";
$suburb=0;
$suburb_name="";
$statement = "SELECT country_id, province_id, town_id, suburb_id
				FROM nse_establishment_location
				WHERE establishment_code=? LIMIT 1";
$sql_location = $GLOBALS['dbCon']->prepare($statement);
$sql_location->bind_param('s', $establishment_code);
$sql_location->execute();
$sql_location->bind_result($country_id, $province_id, $town_id, $suburb_id);
$sql_location->fetch();
$sql_location->free_result();
$sql_location->close();
if (!empty($country_id)) {
	$country=$country_id;
	$country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
}
if (!empty($province_id)) {
	$province=$province_id;
	$province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
}
if(!empty($town_id)) {
	$town=$town_id;
	$town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
}
if(!empty($suburb_id)) {
	$suburb=$suburb_id;
	$suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
}

//Assemble billing address
if (empty($billing_code) && empty($billing_line1) && empty($billing_line2) && empty($billing_line3)) {
	$billing_code = $postal_address_code;
	$billing_line1 = $postal_address_line1;
	$billing_line2 = $postal_address_line2;
	$billing_line3 = $postal_address_line3;
}

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
	$$field_name = $field_value;
	if (in_array($field_name, $estab_fields) || in_array($field_name, $billing_fields)) $changes .= "$field_name,";
}
$sql_updates->free_result();
$sql_updates->close();

//Get and assemble QA list
$statement = "SELECT aa_category_code, aa_category_name FROM nse_aa_category ORDER BY aa_category_name";
$sql_aa_cats = $GLOBALS['dbCon']->prepare($statement);
$sql_aa_cats->execute();
$sql_aa_cats->store_result();
$sql_aa_cats->bind_result($aa_category_code, $aa_category_name);
while ($sql_aa_cats->fetch()) {
	$aa_categories[$aa_category_code] = $aa_category_name;
}
$sql_aa_cats->free_result();
$sql_aa_cats->close();

//Get and assemble assessors list
$assessor_list = array(0=>' ',''=>'');
$statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) FROM nse_user WHERE roles LIKE '%a%' ORDER BY firstname";
$sql_assessors = $GLOBALS['dbCon']->prepare($statement);
$sql_assessors->execute();
$sql_assessors->store_result();
$sql_assessors->bind_result($ass_id, $ass_name);
while ($sql_assessors->fetch()) {
	$assessor_list[$ass_id] = $ass_name;
}
$sql_assessors->free_result();
$sql_assessors->close();

//Get and assemble rep list
$rep_list = array(''=>'',0=>' ');
$statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) FROM nse_user WHERE roles LIKE '%r%' ORDER BY firstname";
$sql_reps = $GLOBALS['dbCon']->prepare($statement);
$sql_reps->execute();
$sql_reps->store_result();
$sql_reps->bind_result($rep_id, $rep_name);
while ($sql_reps->fetch()) {
	$rep_list[$rep_id] = $rep_name;
}
$sql_reps->free_result();
$sql_reps->close();

//HTML start
echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/detail.js></script>";
echo "<script>changes='$changes'</script>";
if (!empty($changes)) "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

echo "<form action='' method=post>";
//Get QA Out date
$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=?";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);
$sql_qa_out->bind_param('s', $establishment_code);
$sql_qa_out->execute();
$sql_qa_out->bind_result($qa_out);
$sql_qa_out->fetch();
$sql_qa_out->close();
if (!isset($qa_out) || $qa_out == '0000-00-00') $qa_out = '-';

//Assemble locations
$location_list = '';
$location_list .= "$country_name";
if (!empty($province_name)) $location_list .= " >> $province_name";
$location_list .= " >> $town_name";
if (isset($suburb_name) && !empty($suburb_name)) $location_list .= " >> $suburb_name";

$regions_list = '';
$statement = "SELECT region_name FROM nse_nlocations_cregion_locations
INNER JOIN nse_nlocations_cregions ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
AND parent_type LIKE 'province'
WHERE location_id=? AND location_type LIKE 'town'";
$sql_regions = $GLOBALS['dbCon']->prepare($statement);
$sql_regions->bind_param('i', $town_id);
$sql_regions->execute();
$sql_regions->store_result();
$sql_regions->bind_result($region_name);
while ($sql_regions->fetch()) {
	$regions_list .= "$region_name<br>";
}
$sql_regions->close();


//Get assessment data
$statement = "SELECT DATE_FORMAT(assessment_date, '%d %M %Y'), assessment_status FROM nse_establishment_assessment WHERE establishment_code=? ORDER BY assessment_date DESC LIMIT 1";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->store_result();
$sql_assessment->bind_result($assessment_date, $assessment_status);
if ($sql_assessment->num_rows > 0) {
	$sql_assessment->fetch();
	if (isset($assessment_date) && ($assessment_date == '0000-00-00') || empty($assessment_date)) $assessment_status = '-';
	switch ($assessment_status) {
		case 'waiting':
			$status = "Waiting for approval";
			break;
		case 'draft':
		case 'returned':
			$status = empty($assessment_date)?'':'In Draft';
			break;
		case 'complete':
			$status = 'Complete';
			break;
		default:
			$status = 'Unknown';
	}

	$assessment_data = "$assessment_date ($status)";
} else {
	$assessment_data = 'None';
}

if ($_SESSION['j_user']['role'] != 'c') {
	if ($premium_member == 'Y') {
		echo "<tr><td>Premium Member</td></td><td><input type='checkbox' name='premium_member' value='Y' checked ></td></tr>";
	} else {
		echo "<tr><th>Premium Member</th><td><input type='checkbox' name='premium_member' value='Y' ></td></tr>";
	}
}
//Website display
if ($_SESSION['j_user']['role'] != 'c') {
	if ($SUB_ACCESS['edit-data']) {
		$active_status = $active=='1'?'checked':'';
		$active_message = $active=='1'?'Active':'De-activated';

		if ($active != 1) {
			$statement = "SELECT note_content, note_date
							FROM nse_notes
							WHERE establishment_code = :establishment_code AND note_content LIKE 'Establishment De-activated:%'
							ORDER BY note_date DESC
							LIMIT 1";
			$sql_message = $GLOBALS['db_pdo']->prepare($statement);
			$sql_message->bindParam(':establishment_code', $establishment_code);
			$sql_message->execute();
			$sql_message_data = $sql_message->fetch();
			$sql_message->closeCursor();

			$message_date = date('d/m/Y', $sql_message_data['note_date']);
			$active_message .= ": <span style='font-size: 0.8em; color: red'>$message_date - ";

			$active_message .= str_replace('Establishment De-activated:', '', $sql_message_data['note_content']) . '</span>';


		}

		echo "<tr><th>Active Status</th><td>" . PHP_EOL;
		echo "<input type=checkbox name=active id=active value=1 onClick=activate(this.checked,'$active') $active_status><span id=astatus>$active_message</span>";
		echo "<textarea style='width: 100%; height: 30px; display: none' id='deactivate_reason' name='deactivate_reason' onFocus=reason_field(this.value) onBlur=reason_field(this.value)>Reason for de-activation</textarea>";
		echo "<tt>This is a global setting to activate or de-activate an establishment. If de-activated the establishment will NOT be displayed on any website irrespective of other settings. </tt>" . PHP_EOL;
		echo "</td></tr>" . PHP_EOL;
		echo "<tr><th>Display On</th><td>" . PHP_EOL;
		echo "<input type=checkbox name=aa_estab id=aa_estab value=Y ". ($aa_estab=='Y'?'checked':'') ."> <label for=aa_estab style='padding-right: 30px'>Essential Travel Info(SA)</label>" . PHP_EOL;
		echo "<input type=checkbox name=aa_zim id=aa_zim value=Y ". ($aa_zim=='Y'?'checked':'') ."> <label for=aa_zim style='padding-right: 30px'>AA Travel(Zim)</label>" . PHP_EOL;
		echo "<input type=checkbox name=etravel id=etravel value=Y ". ($etravel=='Y'?'checked':'') ." > <label for=etravel>E-Travel Info</label>" . PHP_EOL;
		echo "</td></tr>" . PHP_EOL;
	} else {
		$active_message = $active=='1'?'Active':'De-activated';
		echo "<tr><th>Active Status</th><td>" . PHP_EOL;
		echo "$active_message<tt>This is a global setting to activate or de-activate an establishment. If de-activated the establishment will NOT be displayed on any website irrespective of other settings. </tt>" . PHP_EOL;
		echo "</td></tr>" . PHP_EOL;
		echo "<tr><th>Display On</th><td>" . PHP_EOL;
		if ($aa_estab=='Y') echo "AA Travel(SA)" . PHP_EOL;
		if ($aa_zim=='Y') echo "AA Travel(Zim)" . PHP_EOL;
		if ($etravel=='Y') echo "E-Travel Info" . PHP_EOL;
		echo "</td></tr>" . PHP_EOL;
	}
}

//Contact display
if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th colspan=2><br><b> </b></th></tr>" . PHP_EOL;
	$no_contact_date = $no_contact == 'Y'?"<span style='font-size: 0.8em'>($no_contact_date)</span>":'';
	echo "<tr><th>Contact Status</th><td>" . PHP_EOL;
	echo "<input type=checkbox name='noContact' id='noContact' value=Y ". ($no_contact=='Y'?'checked':'') ."> <label for='noContact' style='padding-right: 30px'>Do Not Contact</label>$no_contact_date" . PHP_EOL;
	echo "<br><input type='text' name='noContactReason' value='$no_contact_reason' style='width: 100%' >";
	echo "</td></tr>" . PHP_EOL;
	echo "<tr><th colspan=2><br><b> </b></th></tr>" . PHP_EOL;
}


//estabilshment name
echo "<tr><th>Name</th><td>";
if ($SUB_ACCESS["edit-data"] && $_SESSION['j_user']['role'] != 'c') {
	echo "<input type=text name=establishment_name value=\"".$establishment_name."\" class=jW100 title='Requires value' ";
	if ($_SESSION['j_user']['role'] != 'c') {
		echo "onChange='chkName(this.value";
		if (isset($_GET['id']) && $_GET['id']!= 'new') echo ",\"{$_GET['id']}\"";
		echo ")' ";
	}
	echo " >". PHP_EOL;
	echo "<span id='simName'></div>";
} else {
	echo "<b>".$establishment_name."</b>";
}
echo "</td></tr>";

//Establishment data
echo "<tr><th>Company</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=company_name value='$company_name' class=jW100>":$company_name)."</td></tr>" . PHP_EOL;
echo "<tr><th>Reg No.</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=company_number value='$company_number' class=jW100>":$company_number)."</td></tr>" . PHP_EOL;
echo "<tr><th>Vat No.</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=vat_number value='$vat_number' class=jW100>":$vat_number)."</td></tr>" . PHP_EOL;
echo "<tr><th>Website</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=website_url value='$website_url' class=jW100>":($website_url?"<a href=".$website_url." target=_blank onfocus=blur()>".$website_url."</a>":""))."</td></tr>" . PHP_EOL;


if (!isset($_GET['id'])) {
	echo "<tr><th>Country</th><td><input type=text name=cou value=\"".$country_name."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=country',200,4,9) onclick=J_ajax_C(this) onmouseover=\"J_TT(this,'Type in a few letters of the country')\" class=jW100><input type=hidden name=country value=".$country."></td></tr>" . PHP_EOL;
	$y=mysql_num_rows(mysql_query("SELECT province_id FROM nse_nlocations_provinces WHERE country_id=".$country." LIMIT 1"));
	echo "<tr><th>Province</th><td><input type=text name=pro ".($y?"":" disabled")." value=\"".($y?$province_name:"None available")."\" onkeyup=\"if(this.disabled==false)J_ajax_K(this,'/system/ajax.php?t=province'+(\$N('country')[0].value!='0'?'&country='+\$N('country')[0].value:''),200,4,9)\" onclick=\"if(this.disabled==false)J_ajax_C(this)\" onmouseover=\"if(this.disabled==false)J_TT(this,'Type in a few letters of the province')\" class=jW100><input type=hidden name=province value=".($y?$province:0)."></td></tr>" . PHP_EOL;
	echo "<tr><th>Town</th><td><input type=text value=\"".$town_name."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=town'+(\$N('country')[0].value!='0'?'&country='+\$N('country')[0].value:'')+(\$N('province')[0].value!='0'?'&province='+\$N('province')[0].value:''),200,4,9) onclick=J_ajax_C(this) onmouseover=\"J_TT(this,'Type in a few letters of the town')\" class=jW100><input type=hidden name=town value=".$town."></td></tr>" . PHP_EOL;
	echo "<tr><th>Suburb</th><td><input type=text value=\"".$suburb_name."\" onkeyup=J_ajax_K(this,'/system/ajax.php?t=suburb'+(\$N('town')[0].value!='0'?'&town='+\$N('town')[0].value:''),200,4,9) onclick=J_ajax_C(this) onmouseover=\"J_TT(this,'Type in a few letters of the suburb')\" class=jW100><input type=hidden name=suburb value=".$suburb."></td></tr>" . PHP_EOL;
}

//Billing Address
echo "<tr><th colspan=2><br><b>Billing Address</b></th></tr>" . PHP_EOL;
echo "<tr><th>Line 1</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=billing_line1 value='$billing_line1' class=jW100>":$billing_line1)."</td></tr>" . PHP_EOL;
echo "<tr><th>Line 2</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=billing_line2 value='$billing_line2' class=jW100>":$billing_line2)."</td></tr>" . PHP_EOL;
echo "<tr><th>Line 3</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=billing_line3 value='$billing_line3' class=jW100>":$billing_line3)."</td></tr>" . PHP_EOL;
echo "<tr><th>Code</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=billing_code value='$billing_code' class=jW100>":$billing_code)."</td></tr>" . PHP_EOL;

//Quality Assurance
echo "<tr><th colspan=2><br><b>Quality Assurance</b></th></tr>" . PHP_EOL;
echo "<tr><th>Endorsement</th><td>" . PHP_EOL;
if($SUB_ACCESS["edit-data"] && $_SESSION['j_user']['role'] != 'c' ) {
	foreach ($aa_categories as $cat_code=>$cat_name) {
		if ($cat_code == $aa_endorsement) {
			$aa_category_options .= "<option value='$cat_code' selected>$cat_name";
		} else {
			$aa_category_options .= "<option value='$cat_code' >$cat_name";
		}
	}
	echo "<select name=aa_category_code class=jW100><option value=''>$aa_category_options</select>" . PHP_EOL;
} else {
	echo "{$aa_categories[$aa_endorsement]}</td></tr>" . PHP_EOL;
}

//Advertiser Field
if(!$SUB_ACCESS["edit-data"]){
	$a=array(""=>"","B"=>"Corporate Advertiser","A"=>"Display Advert","L"=>"Listing Only","P"=>"Photo & Listing","F"=>"Web Advertiser (AA)","E"=>"Web Advertiser (ETI)");
	echo "<tr><th>Advertiser</th><td>".($SUB_ACCESS["edit-data"]?"<select name=advertiser class=jW100><option value=''>N/A".str_replace("=$advertiser>","=$advertiser selected>","<option value=B>Corporate Advertiser<option value=A>Display Advert<option value=L>Listing Only<option value=P>Photo & Listing<option value=F>Web Advertiser (AA)<option value=E>Web Advertiser (ETI)")."</select>":$a[$advertiser])."</td></tr>" . PHP_EOL;
}

//Assessors
echo "<tr><th colspan=2><br><b>AA Personnel</b></th></tr>" . PHP_EOL;
echo "<tr><th>Assessor</th><td>" . PHP_EOL;
if ($SUB_ACCESS['edit-data'] && $SUB_ACCESS['edit-assessor'] && ($_SESSION['j_user']['role'] == 's')) {
	$ass_options = '';
	foreach ($assessor_list as $ass_id=>$ass_name) {
		if ($ass_id == $assessor_id) {
			$ass_options .= "<option value='$ass_id' selected >$ass_name";
		} else {
			$ass_options .= "<option value='$ass_id'>$ass_name";
		}
	}
	echo "<select name=assessor_id class=jW100><option value=''>$ass_options</select>" . PHP_EOL;
} else {
	echo isset($assessor_list[$assessor_id])?"{$assessor_list[$assessor_id]}</td></tr>":"</td></tr>";
}

//Rep 1
echo "<tr><th>Sales Rep</th><td>";
if ($SUB_ACCESS['edit-data'] && $SUB_ACCESS['edit-sales-rep'] && ($_SESSION['j_user']['role'] == 's')) {
	$rep_options = '';
	foreach ($rep_list as $rep_id=>$rep_name) {
		if ($rep_id == $rep_id1) {
			$rep_options .= "<option value='$rep_id' selected >$rep_name";
		} else {
			$rep_options .= "<option value='$rep_id'>$rep_name";
		}
	}
	echo "<select name=rep_id1 class=jW100><option value=''>$rep_options</select>";
} else {
	echo isset($rep_list[$rep_id1])?$rep_list[$rep_id1]:'';
	echo "</td></tr>";
}

//Rep 1
if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th>Sales Rep (B)</th><td>";
	if ($SUB_ACCESS['edit-data'] && $SUB_ACCESS['edit-sales-rep'] && ($_SESSION['j_user']['role'] == 's')) {
		$rep_options = '';
		foreach ($rep_list as $rep_id=>$rep_name) {
			if ($rep_id == $rep_id2) {
				$rep_options .= "<option value='$rep_id' selected >$rep_name";
			} else {
				$rep_options .= "<option value='$rep_id'>$rep_name";
			}
		}
		echo "<select name=rep_id2 class=jW100><option value=''>$rep_options</select>";
	} else {
		echo "{$rep_list[$rep_id2]}</td></tr>";
	}
}

//Codes
echo "<tr><th colspan=2><br><b>Miscellaneous</b></th></tr>";
echo "<tr><th>Nightsbridge BBID</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=nightsbridge_bbid value='$nightsbridge_bbid' class=jW100>":$nightsbridge_bbid)."</td></tr>";

if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th>StaySys Code</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=staysys_code value='$staysys_code' class=jW100>":$staysys_code)."</td></tr>";
	//echo "<tr><th>N1 Code</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=n1_code value='$n1_code' class=jW100>":$n1_code)."</td></tr>";
}
echo "<tr><th>TV Licence</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=tv_licence value='$tv_licence' class=jW100>":$tv_licence)."</td></tr>";
echo "<tr><th>No of TV Sets</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=tv_count value='$tv_count' class=jW100>":$tv_count)."</td></tr>";

if ($_SESSION['j_user']['role'] != 'c') {
	//echo "<tr><th>Pastel Code</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=pastel_code value='$pastel_code' class=jW100>":$pastel_code)."</td></tr>";
	if ($SUB_ACCESS['edit-qa-enter-date']) {
		echo "<tr><th>QA Entered</th><td><input type=text name=enter_date value='$entered_date'>(YY-mm-dd)</td></tr>";
	} else {
		echo "<tr><th>QA Entered</th><td>$entered_date</td></tr>";
	}
}

//Does the client use the AATG logo on thier site
if ($_SESSION['j_user']['role'] != 'c') {
	if ($logo_on_site == 'Y') {
		$logo_on_site = 'checked';
		$logo_on_site_txt = 'Yes';
	} else {
		$laga_on_site = '';
		$logo_on_site_txt = 'No';
	}
	echo "<tr><th>Logo on Site</th><td>".($SUB_ACCESS["edit-data"]?"<input type=checkbox name=logo_on_site value='Y' $logo_on_site >":$logo_on_site_txt)."</td></tr>";
}

//Is the establishment a Nedbank member
if ($_SESSION['j_user']['role'] != 'c') {
	if ($nedbank_member == 'Y') {
		$nedbank_member = 'checked';
		$nedbank_member_txt = 'Yes';
	} else {
		$nedbank_member = '';
		$nedbank_member_txt = 'No';
	}
	echo "<tr><th>Nedbank Member</th><td>".($SUB_ACCESS["edit-data"]?"<input type=checkbox name=nedbank_member value='Y' $nedbank_member >":$nedbank_member_txt)."</td></tr>";
}

//360 degrees
if ($_SESSION['j_user']['role'] != 'c') {
	echo "<tr><th colspan=2><br><b>360 Panorama</b></th></tr>";
	if ($id_360 == '0') $id_360 = '';
	echo "<tr><th>Id</th><td>".($SUB_ACCESS["edit-data"]?"<input type=text name=id_360 value='$id_360' class=jW100>":$id_360)."</td></tr>";

	$servers=array(1=>"GoVisit",2=>"360 SA");
	echo "<tr><th>Server</th><td>";
	if($SUB_ACCESS["edit-data"]) {
		echo "<select name=server_360 class=jW100><option value=0>";
		foreach ($servers as $serv_id=>$serv) {
			if ($serv_id == $server_360) {
				echo "<option value='$serv_id' selected>$serv";
			} else {
				echo "<option value='$serv_id' >$serv";
			}
		}
		echo "</select>";
	} else {
		echo "$servers[$server_360]";
	}
	echo "</td></tr>";
}


echo "<script>E_d(";
echo $_SESSION['j_user']['role']=='c'?"0":"1";
echo "," . (isset($_GET["view"])?1:0);
echo ",".($insert?1:((isset($j_IF) || isset($_GET["j_IF"]))?1:0));
echo ",'".$establishment_code."'";
echo ",\"".str_replace("\"","",$establishment_name)."\"";
echo ",".J_getSecure("/apps/establishment_manager/admin/history.php");
echo ",".J_getSecure("/apps/establishment_manager/admin/admin.php");
echo ",".J_getSecure("/apps/establishment_manager/reports/home.php");
echo ",".J_getSecure("/apps/establishment_manager/documents.php");
echo ($active?0:",1");

echo ")</script>";

if($SUB_ACCESS["edit-data"])
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if(!isset($_GET["view"]) && (isset($j_IF) || isset($_GET["j_IF"])))
{}
else
{
	$J_title2="Establishment Manager";
	$J_title1=str_replace("\"","",$establishment_name);
	$J_title3="Establishment Code: ".$establishment_code;
	$J_width=1000;
	$J_height=640;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}

echo "</body></html>";

/**
 * Returns the next available establishment code
 *
 * The establishment code consists of a letter followed by 5 digits (A00000) to create a 6 character establishment code.
 * The last used letter and number is stored in a table (nse_settings). The next available number is checked against
 * the current establishment_table (nse_establishment) for availability. This is done to ensure that new codes are not already in use
 * due to a legacy (deprecated) code system.
 *
 * @return string
 */
function next_establishment_code() {
	//Vars
	$alphabet = str_split('ABCDEFGHIJKLMNPQRSTUVWXYZ123456789');
	$code_letter = '';
	$code_number = '';
	$code_ok = FALSE;
	$check_result = '';

	//Get current letter & number
	$statement = "SELECT variable_value FROM nse_variables WHERE variable_name=?";
	$sql_settings = $GLOBALS['dbCon']->prepare($statement);

	$tag_name = 'code_letter';
	$sql_settings->bind_param('s', $tag_name);
	$sql_settings->execute();
	$sql_settings->store_result();
	$sql_settings->bind_result($code_letter);
	$sql_settings->fetch();
	$sql_settings->free_result();

	$tag_name = 'code_number';
	$sql_settings->bind_param('s', $tag_name);
	$sql_settings->execute();
	$sql_settings->store_result();
	$sql_settings->bind_result($code_number);
	$sql_settings->fetch();
	$sql_settings->free_result();
	$sql_settings->close();


	//Prepare statement for code check
	$statement = "SELECT COUNT(*) FROM nse_establishment WHERE establishment_code=?";
	$sql_code_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement for code update
	$statement = "UPDATE nse_variables SET variable_value=? WHERE variable_name=?";
	$sql_code_update = $GLOBALS['dbCon']->prepare($statement);

	while (!$code_ok) {
		$code_number++;
		if ($code_number > 99999) {
			$code_number = 0;
			foreach ($alphabet as $k=>$v) {
				if ($v == $code_letter) {
					$code_letter = $alphabet[$k+1];

					//Update Settings
					$code_tag = 'code_letter';
					$sql_code_update->bind_param('ss', $code_letter, $code_tag);
					$sql_code_update->execute();

					break;
				}
			}
		}

		//Check if code exists
		$new_code = $code_letter . str_pad($code_number, 5, 0, STR_PAD_LEFT);
		$sql_code_check->bind_param('s', $new_code);
		$sql_code_check->execute();
		$sql_code_check->store_result();
		$sql_code_check->bind_result($check_result);
		$sql_code_check->fetch();
		$sql_code_check->free_result();

		if ($check_result == 0) {
			$code_ok = TRUE;
		}
	}

	//Update code number
	$code_tag = 'code_number';
	$sql_code_update->bind_param('ss', $code_number, $code_tag);
	$sql_code_update->execute();

	return $new_code;
}

?>