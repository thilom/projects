<?php
echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";

switch ($_SESSION['j_user']['role']) {
	case 'c':
		echo "<script>var ests_code = new Array();" . PHP_EOL;
		echo "var ests_name = Array();" . PHP_EOL;
		echo "rep=0;";
		$cnt = 0;
		$statement = "SELECT a.establishment_code, b.establishment_name 
						FROM nse_user_establishments AS a
						LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
						WHERE a.user_id=?";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->bind_param('i', $_SESSION['j_user']['id']);
		$sql_assessor->execute();
		$sql_assessor->bind_result($est_code, $est_name);
		while ($sql_assessor->fetch()) {
			if (empty($est_code) || $est_code=='undefined')continue;
			echo "ests_code[$cnt]='$est_code';" . PHP_EOL;
			echo "ests_name[$cnt]='" . addslashes($est_name) . "';" . PHP_EOL;
			$cnt++;
		}
		$sql_assessor->close();
		echo "</script>";
		break;
	case 'a':
		echo "<script>var ests_code = Array();" . PHP_EOL;
		echo "var ests_name = Array();" . PHP_EOL;
		echo "rep=0;";
		$cnt = 0;
		$statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE assessor_id=? ORDER BY establishment_name";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->bind_param('i', $_SESSION['j_user']['id']);
		$sql_assessor->execute();
		$sql_assessor->bind_result($est_code, $est_name);
		while ($sql_assessor->fetch()) {
			if (empty($est_code))continue;
			echo "ests_code[$cnt]='$est_code';" . PHP_EOL;
			echo "ests_name[$cnt]='" . addslashes($est_name) . "';" . PHP_EOL;
			$cnt++;
		}
		$sql_assessor->close();
		echo "</script>";
		break;
	case 'r':
		echo "<script>var ests_code = Array();" . PHP_EOL;
		echo "var ests_name = Array();" . PHP_EOL;
		echo "rep=1;";
		$cnt = 0;
		$statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE rep_id1=? ORDER BY establishment_name";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->bind_param('i', $_SESSION['j_user']['id']);
		$sql_assessor->execute();
		$sql_assessor->bind_result($est_code, $est_name);
		while ($sql_assessor->fetch()) {
			if (empty($est_code))continue;
			echo "ests_code[$cnt]='$est_code';" . PHP_EOL;
			echo "ests_name[$cnt]='" . addslashes($est_name) . "';" . PHP_EOL;
			$cnt++;
		}
		$sql_assessor->close();
		echo "</script>";
		break;
	default:
		echo "<script>rep=0;</script>";	
			
}

echo "<script src=".$ROOT."/apps/establishment_manager/sections/menu.js></script>";
$J_frame1=$ROOT."/apps/establishment_manager/sections/splash.php";
?>