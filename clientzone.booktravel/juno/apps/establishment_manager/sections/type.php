<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/activity.php";
include_once $SDR."/apps/notes/f/insert.php";
include_once $SDR . "/custom/lib/general_functions.php";

$no_change = TRUE;
$establishment_code=$_GET["id"];
for ($i=1; $i<4; $i++) {
    $current_subcategory[$i] = '';
    $current_star[$i] = '';
}
$room_fields = array('room_count','room_type');
$restype_fields = array('restype_1','stargrading_1','restype_2','stargrading_2','restype_3','stargrading_3','data_id_1','data_id_2','data_id_3');
$room_count = 0;
$room_type = '';

if(isset($_GET["j_hide"])) {
    //Check for changes
    $changes = '';
    $statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
    $sql_updates = $GLOBALS['dbCon']->prepare($statement);
    $sql_updates->bind_param('s', $establishment_code);
    $sql_updates->execute();
    $sql_updates->store_result();
    $sql_updates->bind_result($field_name, $field_value);
    while ($sql_updates->fetch()) {
        if (in_array($field_name, $room_fields)) $changes .= "$field_name,";
        if (in_array($field_name, $restype_fields)) $changes .= "$field_name,";
    }
    $sql_updates->free_result();
    $sql_updates->close();
	echo "<html>";
	echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
	echo "<script src=".$ROOT."/system/P.js></script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/type.js></script>";
	echo "<script>J_start('".$_GET["id"]."'".(isset($_GET["view"])?",1":"").")</script>";
	echo "<script>changes='$changes'</script>";
    if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
    die();
}

$SUB_ACCESS=array("edit-data"=>0);
include $SDR."/system/secure.php";
if(isset($_GET["view"]))
	$SUB_ACCESS["edit-data"]=0;

if(count($_POST) > 0 && $SUB_ACCESS["edit-data"]) {
    //Get room count and type
    $statement = "SELECT room_count, room_type FROM nse_establishment_data WHERE establishment_code=? LIMIT 1";
    $sql_room = $GLOBALS['dbCon']->prepare($statement);
    $sql_room->bind_param('s', $establishment_code);
    $sql_room->execute();
    $sql_room->bind_result($room_count, $room_type);
    $sql_room->fetch();
    $sql_room->free_result();
    $sql_room->close();

	//Get restype
	$statement = "SELECT star_grading
					FROM nse_establishment_restype
					WHERE establishment_code = ?
					LIMIT 1";
	$sql_grading = $GLOBALS['dbCon']->prepare($statement);
	$sql_grading->bind_param('s',$establishment_code);
	$sql_grading->execute();
	$sql_grading->bind_result($stargrading_1);
	$sql_grading->fetch();
	$sql_grading->close();


    //Upload new certificate
    if (!empty($_FILES['certificate_upload']['name'])) {

		//Check and create directory
		$d = $_SERVER['DOCUMENT_ROOT'] . '/juno/stuff/';
		$dirs = array('establishments', $establishment_code,'files','documents');
		foreach ($dirs as $dv) {
			$d .= "$dv/";
			if (!is_dir($d)) {
				mkdir($d);
			}
		}

        if ($_FILES['certificate_upload']['type'] == 'application/pdf') move_uploaded_file($_FILES['certificate_upload']['tmp_name'], $SDR . "/stuff/establishments/$establishment_code/files/documents/" . str_replace(' ', '_', $_FILES['certificate_upload']['name']));
    }

    if ($_SESSION['j_user']['role'] == 'c') { //Save to update table only
        //Prepare statement - Check if field exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_check_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - insert change
        $statement = "INSERT INTO nse_establishment_updates (establishment_code, field_name, field_value, changed_by) VALUES (?,?,?,?)";
        $sql_insert_change = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - update field
        $statement = "UPDATE nse_establishment_updates SET field_value=?, changed_by=? WHERE establishment_code=? && field_name=?";
        $sql_update_change = $GLOBALS['dbCon']->prepare($statement);

        foreach ($_POST as $field_name=>$field_value) {
			$count = '';
            $no_change = FALSE;
//			echo "$field_name :: {$$field_name} --> $field_value<br>";
            if ($field_value != $$field_name) {
                $sql_check_change->bind_param('ss', $establishment_code, $field_name);
                $sql_check_change->execute();
                $sql_check_change->bind_result($count);
                $sql_check_change->fetch();
                $sql_check_change->free_result();
//				var_dump($count);
                if ($count == 0) {
                    $sql_insert_change->bind_param('sssi', $establishment_code, $field_name, $field_value, $_SESSION['j_user']['id']);
                    $sql_insert_change->execute();
                } else {
                    $sql_update_change->bind_param('siss', $field_value, $_SESSION['j_user']['id'], $establishment_code, $field_name);
                    $sql_update_change->execute();
                }
            }
        }
        $sql_insert_change->close();
        $sql_update_change->close();

        if (!$no_change) {
            //Check if log entry exists for today
            $log_count = 0;
            $day = date('d');
            $month = date('m');
            $year = date('y');
            $start_date = mktime(0, 0, 0, $month, $day, $year);
            $end_date = mktime(23, 59, 59, $month, $day, $year);
            $statement = "SELECT COUNT(*) FROM nse_activity WHERE j_act_user=? && establishment_code=? && (j_act_date > ? || j_act_date < ?) && j_act_description='Establishment data updated by client'";
            $sql_check = $GLOBALS['dbCon']->prepare($statement);
            $sql_check->bind_param('isii', $_SESSION['j_user']['id'], $establishment_code, $start_date, $end_date);
            $sql_check->execute();
            $sql_check->bind_result($log_count);
            $sql_check->fetch();
            $sql_check->free_result();
            $sql_check->close();

            if ($log_count == 0) J_act('EST MANAGER', 4, "Establishment data updated by client", '', $establishment_code);
        }
    } else {
        //Prepare statement - get submitted changes
        $statement = "SELECT changed_by, field_value, update_date FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_submitted_data = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - Insert permanent log
        $statement = "INSERT INTO nse_establishment_updates_log (establishment_code,field_name, field_value, submitted_field_value, submitted_by, submitted_date, approved_by) VALUES (?,?,?,?,?,?,?)";
        $sql_log_insert = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statment - clear temp log
        $statement = "DELETE FROM nse_establishment_updates WHERE establishment_code=? && field_name=?";
        $sql_clear_log = $GLOBALS['dbCon']->prepare($statement);

        //Prepare statement - current restype data
        $statement = "SELECT subcategory_id, star_grading FROM nse_establishment_restype WHERE data_id=?";
        $sql_restype_current = $GLOBALS['dbCon']->prepare($statement);
        foreach ($_POST as $field_name=>$field_value) {
            if (!isset($$field_name)) $$field_name = '';
            if (substr($field_name, 0, 4) == 'data') continue;
            if (in_array($field_name, $restype_fields)) {
                list($field_name_db, $field_id) = explode('_', $field_name);

                //Get current data
                if ($_POST['data_id_'.$field_id] != '0') {
                    $sql_restype_current->bind_param('i', $_POST['data_'.$field_id]);
                    $sql_restype_current->execute();
                    $sql_restype_current->bind_result($current_subcategory_id, $current_star_grading);
                    $sql_restype_current->fetch();
                    $sql_restype_current->free_result();

                    if ($field_name_db == 'restype' && ($field_value == '0' || empty($field_value))) { //Delete
                        $no_change = FALSE;
                        $statement = "DELETE FROM nse_establishment_restype WHERE data_id=?";
                        $sql_restype_update = $GLOBALS['dbCon']->prepare($statement);
                        $sql_restype_update->bind_param('i',$_POST['data_id_'.$field_id]);
                        $sql_restype_update->execute();
                        $sql_restype_update->close();

                        $submitted_field_value = '';
                        $submitted_by = '';
                        $submitted_date = '';
                        $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                        $sql_log_insert->execute();

                    } else { //Update
                        if ($field_name_db == 'restype') {
                            if ($current_subcategory_id != $field_value) {
                                $no_change = FALSE;
                                $statement = "UPDATE nse_establishment_restype SET subcategory_id=? WHERE data_id=?";
                                $sql_restype_update = $GLOBALS['dbCon']->prepare($statement);
                                $sql_restype_update->bind_param('si', $field_value, $_POST['data_id_'.$field_id]);
                                $sql_restype_update->execute();
                                $sql_restype_update->close();

                                $submitted_field_value = '';
                                $submitted_by = '';
                                $submitted_date = '';
                                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                                $sql_log_insert->execute();
                            }
                        } else if ($field_name_db == 'stargrading') {
                            if ($current_star_grading !== $field_value) {
                                $no_change = FALSE;
                                $statement = "UPDATE nse_establishment_restype SET star_grading=? WHERE data_id=?";
                                $sql_restype_update = $GLOBALS['dbCon']->prepare($statement);
                                $sql_restype_update->bind_param('si', $field_value, $_POST['data_id_'.$field_id]);
                                $sql_restype_update->execute();
                                $sql_restype_update->close();

								//Clear client log
								$sql_clear_log->bind_param('ss', $establishment_code, $field_name);
								$sql_clear_log->execute();

                                $submitted_field_value = '';
                                $submitted_by = '';
                                $submitted_date = '';
                                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                                $sql_log_insert->execute();
                            }
                        }
                    }
                } else { //insert
                    if ($field_value != 0 && !empty($field_value)) {
                        $no_change = FALSE;
                        if ($field_name_db == 'restype') {
                            $statement = "INSERT INTO nse_establishment_restype (establishment_code, subcategory_id) VALUE (?,?)";
                            $sql_restype_update = $GLOBALS['dbCon']->prepare($statement);
                            $sql_restype_update->bind_param('si',$establishment_code, $field_value);
                            $sql_restype_update->execute();
                            $sql_restype_update->close();
                        } else if ($field_name_db == 'stargrading') {
                            $statement = "INSERT INTO nse_establishment_restype (establishment_code, star_grading) VALUE (?,?)";
                            $sql_restype_update = $GLOBALS['dbCon']->prepare($statement);
                            $sql_restype_update->bind_param('si',$establishment_code, $field_value);
                            $sql_restype_update->execute();
                            $sql_restype_update->close();
                        }
                    }

                    $submitted_field_value = '';
                    $submitted_by = '';
                    $submitted_date = '';
                    $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                    $sql_log_insert->execute();


                }

            } else if ($field_value != $$field_name) {

                $submitted_by = '';
                $submitted_date = '';
                $submitted_field_value = '';

                //Get client submitted changes
                $sql_submitted_data->bind_param('ss', $establishment_code, $field_name);
                $sql_submitted_data->execute();
                $sql_submitted_data->store_result();
                if ($sql_submitted_data->num_rows == 1) {
                    $sql_submitted_data->bind_result($submitted_by, $submitted_field_value, $submitted_date);
                    $sql_submitted_data->fetch();
                    $sql_submitted_data->free_result();
                }

                //Write to permanent log
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();

                //Clear client log
                $sql_clear_log->bind_param('ss', $establishment_code, $field_name);
                $sql_clear_log->execute();

                $changed_fields[$field_name] = $field_value;
            }
        }
        $sql_clear_log->close();
        $sql_submitted_data->close();

        //Check if data entry exists
        $statement = "SELECT COUNT(*) FROM nse_establishment_data WHERE establishment_code=?";
        $sql_check = $GLOBALS['dbCon']->prepare($statement);
        $sql_check->bind_param('s', $establishment_code);
        $sql_check->execute();
        $sql_check->bind_result($count);
        $sql_check->fetch();
        $sql_check->free_result();
        $sql_check->close();

        //Insert if it doesn't exist
        if ($count == 0) {
            $statement = "INSERT INTO nse_establishment_data (establishment_code) VALUES (?)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bind_param('s', $establishment_code);
            $sql_insert->execute();
            $sql_insert->close();
        }

        if (!empty($changed_fields)) {
            $no_change = FALSE;
            foreach ($changed_fields as $field_name => $field_value) {
                if (in_array($field_name, $room_fields)) {
                    $statement = "UPDATE nse_establishment_data SET $field_name=? WHERE establishment_code=?";
                    $sql_estab = $GLOBALS['dbCon']->prepare($statement);
                    $sql_estab->bind_param('ss',  $field_value, $establishment_code);
                    $sql_estab->execute();
                    $sql_estab->close();
                }

                $submitted_field_value = '';
                $submitted_by = '';
                $submitted_date = '';
                $sql_log_insert->bind_param('ssssisi', $establishment_code, $field_name, $field_value, $submitted_field_value, $submitted_by, $submitted_date, $_SESSION['j_user']['id']);
                $sql_log_insert->execute();
            }
        }

        if (!$no_change) {
            $note_content = "Establishment Data updated (Fixed Accommodation Tab)";
			insertNote($establishment_code, 4, 0, $note_content);
			timestamp_establishment($establishment_code);
        }

    }
}

//Prepare statement - restype category
$statement = "SELECT a.category_id, b.category_name
                FROM nse_restype_category AS a
                LEFT JOIN nse_restype_category_lang AS b ON a.category_id=b.category_id
                WHERE a.toplevel_id=?
                ORDER BY a.category_id";
$sql_category = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - restype sub_categories
$statement = "SELECT b.subcategory_id, b.subcategory_name
                FROM nse_restype_subcategory_parent AS a
                LEFT JOIN nse_restype_subcategory_lang AS b ON a.subcategory_id=b.subcategory_id
                WHERE a.category_id=?";
$sql_subcategory = $GLOBALS['dbCon']->prepare($statement);

//Get top_levels
$statement = "SELECT toplevel_id, toplevel_name FROM nse_restype_toplevel ORDER BY toplevel_id";
$sql_toplevel = $GLOBALS['dbCon']->prepare($statement);
$sql_toplevel->execute();
$sql_toplevel->store_result();
$sql_toplevel->bind_result($top_level_id, $top_level_name);
while ($sql_toplevel->fetch()) {
    $restypes[$top_level_id]['name'] = $top_level_name;

    //Get categories
    $sql_category->bind_param('i', $top_level_id);
    $sql_category->execute();
    $sql_category->store_result();
    $sql_category->bind_result($category_id, $category_name);
    while($sql_category->fetch()) {
        $restypes[$top_level_id]['category'][$category_id]['name'] = $category_name;
        $restypes[$top_level_id]['category'][$category_id]['subcategory'] = array();

        //Get subcategories
        $sql_subcategory->bind_param('i', $category_id);
        $sql_subcategory->execute();
        $sql_subcategory->store_result();
        $sql_subcategory->bind_result($subcategory_id, $subcategory_name);
        while ($sql_subcategory->fetch()) {
            $restypes[$top_level_id]['category'][$category_id]['subcategory'][$subcategory_id] = $subcategory_name;
        }
        $sql_subcategory->free_result();
    }
    $sql_category->free_result();

}
$sql_toplevel->free_result();
$sql_toplevel->close();
$sql_category->close();
$sql_subcategory->close();

//Get current restype values
$count = 1;
$statement = "SELECT a.data_id, a.subcategory_id, a.star_grading, b.subcategory_name
                FROM nse_establishment_restype AS a
                LEFT JOIN nse_restype_subcategory_lang AS b ON a.subcategory_id=b.subcategory_id
                WHERE establishment_code=?";
$sql_current = $GLOBALS['dbCon']->prepare($statement);
$sql_current->bind_param('s', $establishment_code);
$sql_current->execute();
$sql_current->store_result();
$sql_current->bind_result($data_id, $subcategory_id, $star_grading, $subcategory_name);
while ($sql_current->fetch()) {
    $current_data_id[$count] = $data_id;
    $current_subcat_name[$count] = $subcategory_name;
    $current_subcategory[$count] = $subcategory_id;
    $current_star[$count] = $star_grading;
    $count++;
}
$sql_current->free_result();
$sql_current->close();

//Get room count and type
$statement = "SELECT room_count, room_type FROM nse_establishment_data WHERE establishment_code=? LIMIT 1";
$sql_room = $GLOBALS['dbCon']->prepare($statement);
$sql_room->bind_param('s', $establishment_code);
$sql_room->execute();
$sql_room->bind_result($room_count, $room_type);
$sql_room->fetch();
$sql_room->free_result();
$sql_room->close();

//Check for changes
$changes = '';
$statement = "SELECT field_name, field_value FROM nse_establishment_updates WHERE establishment_code=?";
$sql_updates = $GLOBALS['dbCon']->prepare($statement);
$sql_updates->bind_param('s', $establishment_code);
$sql_updates->execute();
$sql_updates->store_result();
$sql_updates->bind_result($field_name, $field_value);
while ($sql_updates->fetch()) {
    $$field_name = $field_value;
    if (in_array($field_name, $room_fields)) $changes .= "$field_name,";
    if (in_array($field_name, $restype_fields)) $changes .= "$field_name,";
	if ($field_name == 'stargrading_1') $current_star[1] = $$field_name;
}
$sql_updates->free_result();
$sql_updates->close();

if (empty($room_type)) $room_type = 'Rooms';
$room_options = '';
$room_types = array('Rooms', 'Units', 'Apartments','Chalets', 'Cottages', 'Suites');
foreach ($room_types AS $type) {
    if ($type == $room_type) {
        $room_options .= "<option value='$type' selected >$type</options>";
    } else {
        $room_options .= "<option value='$type'>$type</options>";
    }
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script>changes='$changes'</script>";
if (!empty($changes)) echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";

if($SUB_ACCESS["edit-data"])
{
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/type_ed.js></script>";
	echo "<script>E_s()</script>";

    for ($i=1; $i<4; $i++) {
        $options[$i] = '<option value=""> </option>';
        if (!isset($current_data_id[$i])) $current_data_id[$i] = 0;
    }

    foreach($restypes as $toplevel_id=>$toplevel_data) {
        for ($i=1; $i<4; $i++) {$options[$i] .= "<optgroup label={$toplevel_data['name']} >";}
        foreach ($toplevel_data['category'] as $category_id=>$category_data) {
            for ($i=1; $i<4; $i++) {$options[$i] .= "<optgroup label='{$category_data['name']}' >";}

            foreach ($category_data['subcategory'] as $subcategory_id=>$subcategory_name) {
                for ($i=1; $i<4; $i++) {
                    if ($subcategory_id == $current_subcategory[$i]) {
                        $options[$i] .= "<option value='$subcategory_id' selected> $subcategory_name</option>";
                    } else {
                        $options[$i] .= "<option value='$subcategory_id'> $subcategory_name</option>";
                    }

                }
            }

            for ($i=1; $i<4; $i++) {$options[$i] .= "</optgroup>";}
        }

        for ($i=1; $i<4; $i++) {$options[$i] .= "</optgroup>";}
    }

    for ($i=1; $i<4; $i++) {$stars[$i] = '<option value=""> </option>';}
    for ($y=1; $y<6; $y++) {
        for ($i=1; $i<4; $i++) {
            $stars[$i] .= $current_star[$i]==$y?"<option value='$y' selected>$y</option>":"<option value='$y'>$y</option>";

        }
    }

    if ($_SESSION['j_user']['role'] == 'c') {
        echo "<tr><td>{$current_subcat_name[1]}</td><td><select name=stargrading_1 class=jW100>{$stars[1]}</select></td></tr>";
        if (!empty($current_subcat_name[2])) echo "<tr><td><select name=stargrading_2 class=jW100>{$stars[2]}</select></td></tr>";
        if (!empty($current_subcat_name[3])) echo "<tr><td><select name=stargrading_3 class=jW100>{$stars[3]}</select></td></tr>";
    } else {
        echo "<tr><td><select name=restype_1 class=jW100>{$options[1]}</select><input type=hidden name=data_id_1 value='{$current_data_id[1]}' > </td><td><select name=stargrading_1 class=jW100>{$stars[1]}</select></td></tr>";
        echo "<tr><td><select name=restype_2 class=jW100>{$options[2]}</select><input type=hidden name=data_id_2 value='{$current_data_id[2]}' ></td><td><select name=stargrading_2 class=jW100>{$stars[2]}</select></td></tr>";
        echo "<tr><td><select name=restype_3 class=jW100>{$options[3]}</select><input type=hidden name=data_id_3 value='{$current_data_id[3]}' ></td><td><select name=stargrading_3 class=jW100>{$stars[3]}</select></td></tr>";
    }

	if ($_SESSION['j_user']['role'] != 'dht') {
		echo "<tr><td colspan=2><input type=text name=room_count value='$room_count' size=4><select name=room_type >$room_options</select></td></tr>";
	} else {
		echo "<tr><td colspan=2>$room_count $room_type</td></tr>";
	}


    //Star Grading certificate upload
    echo "<tr><th colspan=2><hr>Attach grading council certificate as proof of current star grading (PDF Only)</th></tr>";
    echo "<tr><td colspan=2><input type=file name=certificate_upload ></td></tr>";

	echo "<script>E_r()</script>";
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/saver.js></script>";
} else {
	echo "<script src=".$ROOT."/apps/establishment_manager/sections/type.js></script>";
	echo "<script>E_s()</script>";

	echo "<tr><td>{$current_subcat_name[1]}</td><td>{$current_star[1]}</td></tr>";
	if (isset($current_subcat_name[2])) echo "<tr><td>{$current_subcat_name[2]}</td><td>{$current_star[2]}</td></tr>";
	if (isset($current_subcat_name[3]))echo "<tr><td>{$current_subcat_name[3]}</td><td>{$current_star[3]}</td></tr>";

	echo "<tr><td colspan=2>$room_count $room_type</td></tr>";

	echo "<script>E_r()</script>";
}

if(!isset($_GET["j_IF"]))
{
	$J_title2="Establishment Manager";
	$J_title1="Contact Details";
	$J_width=480;
	$J_height=480;
	$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
	$J_tooltip="";
	$J_label=22;
	$J_nostart=1;
	$J_nomax=1;
	include $SDR."/system/deploy.php";
}

echo "</body></html>";

?>