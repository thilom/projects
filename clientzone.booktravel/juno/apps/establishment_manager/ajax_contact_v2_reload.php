<?php

/**
 * Checks if reload of the contact form is required
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";

if (isset($_SESSION['reload']) && $_SESSION['reload'] == '1') {
	echo '1';
} else {
	echo '0';
}
?>
