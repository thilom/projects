<?php

/**
 * Get a users details from server for AJAX call
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
require_once $SDR . "/custom/login/class.login.php";

//Vars
$user_id = $_GET['user_id'];

//Get details
$statement = "SELECT firstname, lastname, email, phone, cell, login_name, password, salutation, iv
				FROM nse_user
				WHERE user_id=?
				LIMIT 1";
$sql_user = $GLOBALS['dbCon']->prepare($statement);
$sql_user->bind_param('i', $user_id);
$sql_user->execute();
$sql_user->bind_result($firstname, $lastname, $email, $tel, $cell, $login_name, $password, $salutation, $iv);
$sql_user->fetch();
$sql_user->close();

if (!empty($password)) {
	$password = $GLOBALS["tc"]->decrypt($password, $iv);
}
if (!$password) {
	include_once $SDR . "/custom/security.class.php";
	$gp = new security();
	$password = $gp->generate_password();
}

echo "$user_id~$firstname~$lastname~$email~$salutation~$tel~$cell~$login_name~$password";
?>
