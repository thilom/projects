
/**
 * Check user
 */
function checkUser(mode,id) {
	
	//Is checking enabled
	if (document.getElementById('checkButton').disabled) {
		return;
	}
	
	//Get Data
	firstname = document.getElementById('firstname').value;
	surname = document.getElementById('lastname').value;
	email = document.getElementById('email').value;
	
	if (firstname && surname && email) mode = 1;
	
	//Check user
	if (mode == 1) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				var al = xmlhttp.responseText;
				var msg = '';
				if (al == '0') {
					msg = "<div class='noMatch'>No matches found<div>";
				} else {
					data = al.split('|');
					for (i = 0; i < data.length; i++) {
						dataItems = data[i].split('~');
						if (dataItems[0] == '-2') {
							msg = "<div class='exactMatch'>Exact match found<br>Contact details updated</div>";
							getUser(dataItems[1]);
						} else {
							if (dataItems[0]=='') continue;
							if (msg=='') msg = "<div class='partialMatch'>Similar contacts found. Select from the list below to import user details.<hr></div>";
							msg += "<div class='matchData'><input type='button' value='&#9664;' onClick='getUser(" + dataItems[0] + ")' >" + dataItems[1] + " " + dataItems[2] + "(" + dataItems[3] + ")</div>";
						}
					}
				}
				document.getElementById('check').innerHTML = msg;
			}
		}

		Url = "/juno/apps/establishment_manager/ajax_contact_v2_usercheck.php";
		Url += "?firstname=" + firstname;
		Url += "&lastname=" + surname;
		Url += "&email=" + email;
		Url += id?"&user_id=" + id:'';

		xmlhttp.open("GET",Url,true);
		xmlhttp.send();
	}
}

/**
 * Gets user details from the server
 */
function getUser(userID) {
    if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var al = xmlhttp.responseText;
			userData = al.split('~');
			
			document.getElementById('userID').value = userData[0];
			document.getElementById('firstname').value = userData[1];
			document.getElementById('lastname').value = userData[2];
			document.getElementById('email').value = userData[3];
			document.getElementById('salutation').value = userData[4];
			document.getElementById('tel').value = userData[5];
			document.getElementById('cell').value = userData[6];
			document.getElementById('username').value = userData[7];
			document.getElementById('userPassword').value = userData[8];
			document.getElementById('designation').value = '';
			document.getElementById('ads_contact').checked = false;
			document.getElementById('awards_contact').checked = false;
			document.getElementById('accounts_contact').checked = false;
			document.getElementById('proofing_contact').checked = false;
			document.getElementById('materials_contact').checked = false;
			document.getElementById('qa_contact').checked = false;
			
		}
	}

	Url = "/juno/apps/establishment_manager/ajax_contact_v2_userget.php";
	Url += "?user_id=" + userID;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Calculates warnings
 */
function calculateWarnings() {
	
	adminAccess = document.getElementById('adminAccess').value;
	
	//Admin Access
	if (adminAccess == 0) {
		warningMsg = "<span class='warningL1'>&#187; No admin access</span>";
	} else {
		warningMsg = "<span class='warningOK'>&#187; Admin access enabled</span>";
		
		if (document.getElementById('username').value == '') {
			warningMsg += "<br><span class='warningL2'>&#187; Admin access is enabled but contact does not have a username.</span>";
		}
		if (document.getElementById('userPassword').value == '') {
			warningMsg += "<br><span class='warningL2'>&#187; Admin access is enabled but contact does not have a password.</span>";
		}
	}
	
	
	
	document.getElementById('warnings').innerHTML = warningMsg;
}

/**
 * Toggles the admin access
 */
function toggleAdmin() {
	var accessField = document.getElementById('adminAccess');
	var accessButton = document.getElementById('adminButton');
	
	
	if (accessField.value == 0) {
		accessField.value = 1;
		accessButton.value = 'Disable Admin Access';
	} else {
		accessField.value = 0;
		accessButton.value = 'Enable Admin Access';
	}
	
	calculateWarnings();
}

/**
 * Get a username
 */
function getUsername(id) {
	//Get Data
	firstname = document.getElementById('firstname').value;
	surname = document.getElementById('lastname').value;
	email = document.getElementById('email').value;
	
    if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var al = xmlhttp.responseText;
			
			document.getElementById('username').value = al;
			
		}
	}

	Url = "/juno/apps/establishment_manager/ajax_contact_v2_username.php";
	Url += "?firstname=" + firstname;
	Url += "&lastname=" + surname;
	Url += "&email=" + email;
	Url += id?"&user_id=" + id:'';

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}
