<?php

/**
 * Remove a user from establishment contacts list
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";

//Vars
$user_id = $_GET['user_id'];
$establishment_code = $_GET['establishment'];

//Remove
$statement = "DELETE FROM nse_user_establishments
				WHERE user_id=? AND establishment_code=?
				LIMIT 1";
$sql_remove = $GLOBALS['dbCon']->prepare($statement);
$sql_remove->bind_param('ss', $user_id, $establishment_code);
$sql_remove->execute();
$sql_remove->close();


?>
