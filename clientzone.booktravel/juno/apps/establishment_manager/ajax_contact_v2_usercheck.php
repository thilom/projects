<?php

/**
 * Check for exact and similar user match
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";

//Vars
$firstname = $_GET['firstname'];
$lastname = $_GET['lastname'];
$email = $_GET['email'];
$ex_user_id = isset($_GET['user_id'])?$_GET['user_id']:'';

//Check for exact Match
$statement = "SELECT user_id 
				FROM nse_user
				WHERE firstname=? AND lastname=? AND email=? AND user_id!=?
				LIMIT 1";
$sql_exact = $GLOBALS['dbCon']->prepare($statement);
$sql_exact->bind_param('ssss', $firstname, $lastname, $email, $ex_user_id);
$sql_exact->execute();
$sql_exact->bind_result($user_id);
$sql_exact->fetch();
$sql_exact->close();
if (!empty($user_id)) {
	echo "-2~$user_id|";
	die();
} 


//Check for partial matches
$firstname = "%$firstname%";
$lastname = "%$lastname%";
$email = "%$email%";
$statement = "SELECT user_id, firstname, lastname, email
				FROM nse_user
				WHERE (firstname LIKE ? OR lastname LIKE ?) AND email LIKE ? AND user_id != ?";
$sql_partial = $GLOBALS['dbCon']->prepare($statement);
$sql_partial->bind_param('ssss', $firstname, $lastname, $email, $ex_user_id);
$sql_partial->execute();
$sql_partial->store_result();
$rows = $sql_partial->num_rows;
$sql_partial->bind_result($user_id, $firstname, $lastname, $email);
while ($sql_partial->fetch()) {
	echo "$user_id~$firstname~$lastname~$email|";
}
$sql_partial->close();
if ($rows > 0) die();

//If nothing is found
echo "0";

?>
