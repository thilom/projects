<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/f/documents.js></script>";
if(!isset($_GET["id"]) && isset($_GET["j_IF"]))
{
	echo "<script>J_in_s()</script>";
	die();
}
echo "<body class=j_List>";

switch ($_SESSION['j_user']['role']) {
	case 'c':
		echo "<script>var ests_code = new Array();" . PHP_EOL;
		echo "var ests_name = new Array();" . PHP_EOL;
		$cnt = 0;
		$statement = "SELECT a.establishment_code, b.establishment_name 
						FROM nse_user_establishments AS a
						LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
						WHERE a.user_id=?";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->bind_param('i', $_SESSION['j_user']['id']);
		$sql_assessor->execute();
		$sql_assessor->bind_result($est_code, $est_name);
		while ($sql_assessor->fetch()) {
			if (empty($est_code) || $est_code=='undefined')continue;
			echo "ests_code[$cnt]='$est_code';" . PHP_EOL;
			echo "ests_name[$cnt]='" . addslashes($est_name) . "';" . PHP_EOL;
			$cnt++;
		}
		$sql_assessor->close();
		echo "</script>";
		break;
	case 'a':
		echo "<script>var ests_code = Array();" . PHP_EOL;
		echo "var ests_name = Array();" . PHP_EOL;
		$cnt = 0;
		$statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE assessor_id=?";
		$sql_assessor = $GLOBALS['dbCon']->prepare($statement);
		$sql_assessor->bind_param('i', $_SESSION['j_user']['id']);
		$sql_assessor->execute();
		$sql_assessor->bind_result($est_code, $est_name);
		while ($sql_assessor->fetch()) {
			if (empty($est_code))continue;
			echo "ests_code[$cnt]='$est_code';" . PHP_EOL;
			echo "ests_name[$cnt]='" . addslashes($est_name) . "';" . PHP_EOL;
			$cnt++;
		}
		$sql_assessor->close();
		echo "</script>";
		break;
}

echo "<script>J_in_N('{$_SESSION["j_user"]["role"]}'";
if(isset($_GET["id"]))
{
	include $SDR."/system/get.php";
	echo ",'".$_GET["id"]."',\"".J_Value("","establishment","",$_GET["id"])."\"";
}
echo ")</script>";
$J_home=7;
$J_title1="Documents and Files";
$J_title2="Establishment Manager";
$J_icon="<img src=".$ROOT."/ico/set/file_drawer.png>";
$J_label=22;
$J_width=640;
$J_framesize=260;
$J_frame1=$ROOT."/apps/establishment_manager/documents.php";
include $SDR."/system/deploy.php";
echo "</body></html>";
?>