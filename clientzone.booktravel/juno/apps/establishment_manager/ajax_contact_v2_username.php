<?php

/**
 * Create username and check against the DB
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";

//Vars
$firstname = $_GET['firstname'];
$lastname = $_GET['lastname'];
$email = $_GET['email'];
$user_id = isset($_GET['user_id'])?$_GET['user_id']:'';
$possibilities = array();

//Calculate possibilities
$possibilities[] = $email;
$possibilities[] = "$firstname{$lastname}";

//Prepare statement 
$statement = "SELECT COUNT(*) 
				FROM nse_user
				WHERE login_name = ? ";
if (!empty($user_id)) $statement .= "AND user_id!=?";
$sql_check = $GLOBALS['dbCon']->prepare($statement);


foreach ($possibilities as $login_name) {
	if (!empty($user_id)) {
		$sql_check->bind_param('si', $login_name, $user_id);
	} else {
		$sql_check->bind_param('s', $login_name);
	}
	$sql_check->execute();
	$sql_check->bind_result($result);
	$sql_check->fetch();
	$sql_check->free_result();
	
	if ($result == 0) {
		echo "$login_name";
		die();
	}
}

?>
