<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
include $SDR . "/system/get.php";
require_once $SDR . "/custom/login/class.login.php";
include_once $SDR."/apps/notes/f/insert.php";

//Vars
$user_id = isset($_GET['id'])?$_GET['id']:'';
$establishment_code = $_GET['est'];
$template = file_get_contents('edit_contact.html');
$enable_check = '';
$access = 0;
$admin_access = 0;
$admin_button = 'Enable Adimn Access';
$firstname = '';
$surname = '';
$email = '';
$designation = '';
$tel = '';
$cell = '';
$username = '';
$password = '';
$awards_contact = '';
$ads_contact = '';
$qa_contact = '';
$accounts_contact = '';
$materials_contact = '';
$proofing_contact = '';
$designation_options = '';
$salutation = '';
$salutation_options = '';
$company_roles = array('',
						'CEO',
						'Owner',
						'Owner/Manager',
						'Manager',
						'General Manager',
						'Marketing Manager',
						'Accounts',
						'Reception',
						'Tourism',
						'Operations',
						'Proofing');
$salutations = array('',
		'Mr',
		'Mrs',
		'Ms',
		'Miss',
		'Dr',
		'Prof');


if (!empty($user_id)) {
	
	//Get user Data
	$statement = "SELECT firstname, lastname, email, phone, cell, login_name, password, iv, salutation
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bind_param('i', $user_id);
	$sql_user->execute();
	$sql_user->bind_result($firstname, $surname, $email, $tel, $cell, $username, $password, $iv, $salutation);
	$sql_user->fetch();
	$sql_user->close();
	
	//Passwords
	if (!empty($password)) {
		$password = $GLOBALS["tc"]->decrypt($password, $iv);
	}
	if (!$password) {
		include_once $SDR . "/custom/security.class.php";
		$gp = new security();
		$password = $gp->generate_password();
	}
	
	//Contact types and designation
	$statement = "SELECT designation, awards_contact, ads_contact, qa_contact, materials_contact, proofing_contact, accounts_contact
					FROM nse_user_establishments
					WHERE establishment_code=? AND user_id=?
					LIMIT 1";
	$sql_types = $GLOBALS['dbCon']->prepare($statement);
	$sql_types->bind_param('si', $establishment_code, $user_id);
	$sql_types->execute();
	$sql_types->bind_result($designation, $awards_contact, $ads_contact, $qa_contact, $materials_contact, $proofing_contact, $accounts_contact);
	$sql_types->fetch();
	$sql_types->close();
	
	//Contact Types
	if ($awards_contact == 'Y') $awards_contact = 'checked';
	if ($ads_contact == 'Y') $ads_contact = 'checked';
	if ($qa_contact == 'Y') $qa_contact = 'checked';
	if ($accounts_contact == 'Y') $accounts_contact = 'checked';
	if ($materials_contact == 'Y') $materials_contact = 'checked';
	if ($proofing_contact == 'Y') $proofing_contact = 'checked';
	
	//Admin Access
	$statement = "SELECT COUNT(*) 
					FROM nse_user_pages 
					WHERE j_uac_user=? AND j_uac_role='c'";
	$sql_access = $GLOBALS['dbCon']->prepare($statement);
	$sql_access->bind_param('s', $user_id);
	$sql_access->execute();
	$sql_access->bind_result($access);
	$sql_access->fetch();
	$sql_access->free_result();
	$admin_access = $access<1?0:1;
	$admin_button = $access<1?"Enable Admin Access":"Disable Admin Access";
}

//Save Data
if (count($_POST) > 0) {
	
	$firstname = $_POST['firstname'];
	$surname = $_POST['lastname'];
	$email = $_POST['email'];
	$salutation = $_POST['salutation'];
	$designation = $_POST['designation'];
	$tel = $_POST['tel'];
	$cell = $_POST['cell'];
	$login_name = $_POST['username'];
	$password = $_POST['userPassword'];
	$awards_contact = isset($_POST['awards_contact'])?'Y':'';
	$ads_contact = isset($_POST['ads_contact'])?'Y':'';
	$qa_contact = isset($_POST['qa_contact'])?'Y':'';
	$accounts_contact = isset($_POST['accounts_contact'])?'Y':'';
	$materials_contact = isset($_POST['materials_contact'])?'Y':'';
	$proofing_contact = isset($_POST['proofing_contact'])?'Y':'';
	$admin_access = $_POST['adminAccess'];
	if (isset($_POST['userID']) && !empty($_POST['userID'])) $user_id=$_POST['userID'];

	if (!empty($password)) {
		$pass = $tc->encrypt($password);
		$password = $pass['data'];
		$iv = $pass['iv'];
	} 

	if (!empty($user_id)) {

		//Update User
		$statement = "UPDATE nse_user 
						SET firstname=?, lastname=?, email=?, phone=?, cell=?, login_name=?, password=?, iv=?, salutation=?
						WHERE user_id=?";
		$sql_user_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_user_update->bind_param('sssssssssi', $firstname, $surname, $email, $tel, $cell, $login_name, $password, $iv, $salutation, $user_id);
		$sql_user_update->execute();
		$sql_user_update->close();

		//Check if designation and user types exist in table
		$type_count = 0;
		$statement = "SELECT COUNT(*) 
						FROM nse_user_establishments
						WHERE establishment_code=? AND user_id=?";
		$sql_count = $GLOBALS['dbCon']->prepare($statement);
		$sql_count->bind_param('ss', $establishment_code, $user_id);
		$sql_count->execute();
		$sql_count->bind_result($type_count);
		$sql_count->fetch();
		$sql_count->close();
		
		//Update designation and user types
		if ($type_count == 0) {
			$statement = "INSERT INTO nse_user_establishments
								(designation, ads_contact, awards_contact, qa_contact, 
								accounts_contact, materials_contact, proofing_contact,
								establishment_code, user_id)
							VALUES
								(?,?,?,?,?,?,?,?,?)";
			$sql_types_insert = $GLOBALS['dbCon']->prepare($statement);
			$sql_types_insert->bind_param('ssssssssi', $designation, $ads_contact, $awards_contact, $qa_contact, 
														$accounts_contact, $materials_contact, $proofing_contact, $establishment_code, $user_id);
			$sql_types_insert->execute();
			$sql_types_insert->close();
		} else {
			$statement = "UPDATE nse_user_establishments
							SET designation=?, ads_contact=?, awards_contact=?, qa_contact=?, 
								accounts_contact=?, materials_contact=?, proofing_contact=?
							WHERE establishment_code=? AND user_id=?";
			$sql_types_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_types_update->bind_param('ssssssssi', $designation, $ads_contact, $awards_contact, $qa_contact, 
														$accounts_contact, $materials_contact, $proofing_contact, $establishment_code, $user_id);
			$sql_types_update->execute();
			$sql_types_update->close();
		}

		//Update Access
		if ($admin_access == 1 && $access < 1) {
			$x = mktime(0, 0, 0, date("m"), date("d"), date("Y") + 3);
			
			//Prepare statement 
			$statement = "INSERT INTO nse_user_pages 
								(j_uac_date,j_uac_expiry,j_uac_curator,j_uac_user,j_uac_role,j_uac_page,j_uac_set)
							VALUES 
								($EPOCH,$x,?,?,'c',?,'Client')";
			$sql_add = $GLOBALS['dbCon']->prepare($statement);
			
			$statement = "SELECT j_uas_page 
							FROM nse_user_pages_set 
							WHERE j_uas_name='Client'";
			$sql_setup = $GLOBALS['dbCon']->prepare($statement);
			$sql_setup->execute();
			$sql_setup->bind_result($j_uas_page);
			while ($sql_setup->fetch()) {
				$pages[] = $j_uas_page; 
			}
			$sql_setup->close();
			
			foreach ($pages as $j_uas_page) {
				$sql_add->bind_param('sis', $_SESSION["j_user"]["id"], $user_id, $j_uas_page);
				$sql_add->execute();
			}
		
		} else { //Remove Access
			if ($admin_access == 0) {
				$statement = "DELETE FROM nse_user_pages
								WHERE j_uac_user = ? AND j_uac_role='c'";
				$sql_access_delete = $GLOBALS['dbCon']->prepare($statement);
				$sql_access_delete->bind_param('i', $user_id);
				$sql_access_delete->execute();
				$sql_access_delete->close();
			}
		}
		
		//Log & display result
		$note_content = "Edited contact - $firstname $surname";
		insertNote($establishment_code, 4, 0, $note_content);
		$message = "&#8226; Contact Details for $firstname $surname updated.";

	} else {

		//Insert User
		$statement = "INSERT INTO nse_user 
							(firstname, lastname, email, phone, cell, login_name, password, iv, roles, salutation)
						VALUES	
							(?,?,?,?,?,?,?,?,'c',?)";
		$sql_user_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_user_insert->bind_param('sssssssss', $firstname, $surname, $email, $tel, $cell, 
													$login_name, $password, $iv, $salutation);
		$sql_user_insert->execute();
		$user_id = $sql_user_insert->insert_id;
		$sql_user_insert->close();
		
		//Add designation and types
		$statement = "INSERT INTO nse_user_establishments
							(establishment_code, user_id, designation, ads_contact, awards_contact, 
								qa_contact, accounts_contact, materials_contact, proofing_contact)
						VALUES 
							(?,?,?,?,?,?,?,?,?)";
		$sql_type_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_type_insert->bind_param('sssssssss', $establishment_code, $user_id, $designation, $ads_contact, 
								$awards_contact, $qa_contact, $accounts_contact, $materials_contact, 
								$proofing_contact);
		$sql_type_insert->execute();

		$message = "&#8226; New contact created - $firstname $surname. $user_id";
	}

	echo "<div style='background-color: silver; color: black; margin: auto auto; border: 1px dotted gray;
				padding: 10px; text-align: center; '>$message</div>";
	echo "<div style='width: 100%; text-align: right'><input type='button' value='Continue' onClick='parent.J_WX({$_GET['j_W']})' ></div>";
		
	$_SESSION['reload'] = '1';
	
	die();
}

//Designations
foreach ($company_roles as $company_designation) {
	if ($company_designation == $designation) {
		$designation_options .= "<option value='$company_designation' selected >$company_designation</option>";
	} else {
		$designation_options .= "<option value='$company_designation'>$company_designation</option>";
	}
}

//Salutations
foreach ($salutations as $salutation_item) {
	if ($salutation_item == $salutation) {
		$salutation_options .= "<option value='$salutation_item' selected >$salutation_item</option>";
	} else {
		$salutation_options .= "<option value='$salutation_item'>$salutation_item</option>";
	}
}

//Enabale/disable user checking
if (!empty($user_id)) $enable_check = 'disabled';

//Replace Vars
$template = str_replace('<!-- firstname -->', $firstname , $template);
$template = str_replace('<!-- surname -->', $surname , $template);
$template = str_replace('<!-- email -->', $email , $template);
$template = str_replace('<!-- tel -->', $tel , $template);
$template = str_replace('<!-- cell -->', $cell , $template);
$template = str_replace('<!-- username -->', $username , $template);
$template = str_replace('<!-- password -->', $password , $template);
$template = str_replace('<!-- ads_contact -->', $ads_contact , $template);
$template = str_replace('<!-- awards_contact -->', $awards_contact , $template);
$template = str_replace('<!-- qa_contact -->', $qa_contact , $template);
$template = str_replace('<!-- materials_contact -->', $materials_contact , $template);
$template = str_replace('<!-- proofing_contact -->', $proofing_contact , $template);
$template = str_replace('<!-- accounts_contact -->', $accounts_contact , $template);
$template = str_replace('<!-- designation_options -->', $designation_options , $template);
$template = str_replace('<!-- salutation_options -->', $salutation_options , $template);
$template = str_replace('<!-- user_id -->', $user_id , $template);
$template = str_replace('<!-- enable_check -->', $enable_check , $template);
$template = str_replace('<!-- admin_access -->', $admin_access , $template);
$template = str_replace('<!-- admin_button -->', $admin_button , $template);

echo $template;

$J_title1 = "Edit Contact";
$J_title2 = !empty($user_id)?"$firstname $surname":'';
if ($user_id && $_SESSION["j_user"]["role"] == "d") $J_title3 = "User id: " . $user_id;
$J_icon = "<img src=" . $ROOT . "/ico/set/user_both.png><var><img src=" . $ROOT . "/ico/set/" . ($user_id ? "edit" : "add") . ".png></var>";
$J_label = 5;
$J_width = 700;
$J_height = 450;
$J_nomax = 1;
$J_nostart = 1;
include $SDR . "/system/deploy.php";
echo "</body></html>";
?>
