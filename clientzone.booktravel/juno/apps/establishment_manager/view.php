<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
$id=isset($_GET["id"])?$_GET["id"]:0;

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/sections/view.js></script>";

include $SDR."/system/get.php";
$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));

$reg="";
$vat="";
$co="";
$three="";

$l=mysql_query("SELECT vat_number,company_name,company_number FROM nse_establishment_billing WHERE establishment_code='".$id."' LIMIT 1");
if(mysql_num_rows($l))
{
	$l=mysql_fetch_array($l);
	if($l["company_name"])
		$co=$l["company_name"];
	if($l["company_number"])
		$reg=$l["company_number"];
	if($l["vat_number"])
		$vat=$l["vat_number"];
}
if($co)
	echo "<tr><th>Company</th><td>".$co."</td></tr>";
if($reg)
	echo "<tr><th>Reg No.</th><td>".$reg."</td></tr>";
if($vat)
	echo "<tr><th>Vat No.</th><td>".$vat."</td></tr>";
if($g["website_url"])
	echo "<tr><th>Website</th><td><a href=".$g["website_url"]." target=_blank onfocus=blur()>".$g["website_url"]."</a></td></tr>";

$country=0;
$country_name="";
$province=0;
$province_name="";
$town=0;
$town_name="";
$suburb=0;
$suburb_name="";
$l=mysql_query("SELECT country_id,province_id,town_id,suburb_id FROM nse_establishment_location WHERE establishment_code='".$id."' LIMIT 1");
if(mysql_num_rows($l))
{
	$l=mysql_fetch_array($l);
	if($l["country_id"])
	{
		$country=$l["country_id"];
		$country_name=J_Value("country_name","nse_nlocations_countries","country_id",$country);
	}
	if($l["province_id"])
	{
		$province=$l["province_id"];
		$province_name=J_Value("province_name","nse_nlocations_provinces","province_id",$province);
	}
	if($l["town_id"])
	{
		$town=$l["town_id"];
		$town_name=J_Value("town_name","nse_nlocations_towns","town_id",$town);
	}
	if($l["suburb_id"])
	{
		$suburb=$l["suburb_id"];
		$suburb_name=J_Value("suburb_name","nse_nlocations_suburbs","suburb_id",$suburb);
	}
}

if($country_name||$province_name||$town_name||$suburb_name)
	echo "<tr><th colspan=2><b>Regional Address</b></th></tr>";
if($country_name)
{
	echo "<tr><th>Country</th><td>".$country_name."</td></tr>";
	$three.=$country_name;
}
if($province_name)
{
	echo "<tr><th>Province</th><td>".$province_name."</td></tr>";
	$three.=" | ".$province_name;
}
if($town_name)
{
	echo "<tr><th>Town</th><td>".$town_name."</td></tr>";
	$three.=" | ".$town_name;
}
if($suburb_name)
{
	echo "<tr><th>Suburb</th><td>".$suburb_name."</td></tr>";
	$three.=" | ".$suburb_name;
}

if($g["street_address_line1"]||$g["street_address_line2"]||$g["street_address_line3"])
	echo "<tr><th colspan=2><b>Street Address</b></th></tr>";
if($g["street_address_line1"])
	echo "<tr><th>Line 1</th><td>".$g["street_address_line1"]."</td></tr>";
if($g["street_address_line1"])
	echo "<tr><th>Line 2</th><td>".$g["street_address_line2"]."</td></tr>";
if($g["street_address_line2"])
	echo "<tr><th>Line 3</th><td>".$g["street_address_line3"]."</td></tr>";
if($g["street_address_line3"])


if($g["postal_address_line1"]||$g["postal_address_line2"]||$g["postal_address_line3"]||$g["postal_address_code"])
	echo "<tr><th colspan=2><b>Postal Address</b></th></tr>";
if($g["postal_address_line1"])
	echo "<tr><th>Line 1</th><td>".$g["postal_address_line1"]."</td></tr>";
if($g["postal_address_line2"])
	echo "<tr><th>Line 2</th><td>".$g["postal_address_line2"]."</td></tr>";
if($g["postal_address_line3"])
	echo "<tr><th>Line 3</th><td>".$g["postal_address_line3"]."</td></tr>";
if($g["postal_address_code"])
	echo "<tr><th>Code</th><td>".$g["postal_address_code"]."</td></tr>";

echo "<tr><th colspan=2><b>Quality Assurance</b></th></tr>";

$a=array(""=>"N/A","A"=>"Associate Member","E"=>"Eco","EHR"=>"Eco Highly Recommended","ER"=>"Eco Recommended","ES"=>"Eco Superior","H/S"=>"Highly Recommended/Superior","HR"=>"Highly Recommended","R"=>"Recommended","R/H"=>"Recommended/Highly Recommended","S"=>"Superior");
echo "<tr><th>Endorsement</th><td>".$a[$g["aa_category_code"]]."</td></tr>";

$a=array(""=>"","B"=>"Corporate Advertiser","A"=>"Display Advert","L"=>"Listing Only","P"=>"Photo & Listing","F"=>"Web Advertiser (AA)","E"=>"Web Advertiser (ETI)");
if(isset($a[$g["advertiser"]]))
	echo "<tr><th>Advertiser</th><td>".$a[$g["advertiser"]]."</td></tr>";

echo "<tr><th colspan=2><b>AA Personel</b></th></tr>";

$t=array("","");
if($g["assessor_id"])
	$t=mysql_fetch_array(mysql_query("SELECT firstname,lastname FROM nse_user WHERE user_id=".$g["assessor_id"]." LIMIT 1"));
echo "<tr><th>Assessor</th><td>";
$a=J_Value("email","nse_user","user_id",$g["assessor_id"]);
echo "<a href=mailto:".$a." onfocus=blur() onmouseover=\"J_TT(this,'".$a."')\">".$t[0].($t[1]?" ".$t[1]:"")."</a>";
echo "</td></tr>";

$t=array("","");
if($g["rep_id1"]>0)
	$t=mysql_fetch_array(mysql_query("SELECT firstname,lastname FROM nse_user WHERE user_id=".$g["rep_id1"]." LIMIT 1"));
echo "<tr><th>Rep</th><td>";
$a=J_Value("email","nse_user","user_id",$g["rep_id1"]);
echo "<a href=mailto:".$a." onfocus=blur() onmouseover=\"J_TT(this,'".$a."')\">".$t[0].($t[1]?" ".$t[1]:"")."</a>";
echo "</td></tr>";

echo "<script>E_z('".$id."',".J_getSecure("/apps/establishment_manager/sections/detail.php").")</script>";

$J_title2="Establishment Details";
$J_title1=($g["establishment_name"]?$g["establishment_name"]:"Untitled Establishment");
$J_title3=$three;
$J_width=480;
$J_height=480;
$J_icon="<img src=".$ROOT."/ico/set/gohome-edit.png>";
$J_label=22;
$J_nomax=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>