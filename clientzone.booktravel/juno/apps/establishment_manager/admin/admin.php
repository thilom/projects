<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
$SUB_ACCESS=array("QA-out"=>0);
include $SDR."/system/secure.php";
include $SDR."/system/get.php";
include_once $SDR."/apps/notes/f/insert.php";

$establishment_code=$_GET["id"];
$template = file_get_contents('html/qa_out.html');

$message = '';
$assessment_message = "This will remove all assessments that have not been completed for this establishment";
$invoice_id = '';
$invoice_message = "Could not find any invoices associated with this establishments assessments. This process may need to be done manually";
$cancel_date = mktime();
$finish_message = '<ul>';

if (count($_POST) > 0) {

	//Remove from AA sites
	if (isset($_POST['deactivate_sa']) || isset($_POST['deactivate_zim'])) {
		$statement = "UPDATE nse_establishment SET ";
		if (isset($_POST['deactivate_sa'])) {
			$statement .= "aa_estab='',";
			$finish_message .= "<li>Establishment Deactivated on AA Travel - South Africa</li>";
		}
		if (isset($_POST['deactivate_zim'])) {
			$statement .= "aa_zim='',";
			$finish_message .= "<li>Establishment Deactivated on AA Travel - Zimbabwe</li>";
		}
		$statement = substr($statement, 0, -1);
		$statement .= " WHERE establishment_code=?";
		$sql_deactivate = $GLOBALS['dbCon']->prepare($statement);
		$sql_deactivate->bind_param('s', $establishment_code);
		$sql_deactivate->execute();
		$sql_deactivate->close();
	}

	//Remove endorsement
	if (isset($_POST['remove_endorsement'])) {
		if (isset($_POST['cancel_type_cc'])) { //Caravan & Camping
			$statement = "UPDATE nse_establishment SET cc_endorsement='' WHERE establishment_code=?";
			$sql_endorsement = $GLOBALS['dbCon']->prepare($statement);
			$sql_endorsement->bind_param('s', $establishment_code);
			$sql_endorsement->execute();
			$sql_endorsement->close();
			$finish_message .= "<li>Caravan & Camping Endorsement Removed</li>";
		}
		
		if (isset($_POST['cancel_type_fa'])) { //Fixed Accommodation
			$statement = "UPDATE nse_establishment SET aa_category_code='' WHERE establishment_code=?";
			$sql_endorsement = $GLOBALS['dbCon']->prepare($statement);
			$sql_endorsement->bind_param('s', $establishment_code);
			$sql_endorsement->execute();
			$sql_endorsement->close();
			$finish_message .= "<li>Establishment Endorsement Removed</li>";
		}
		
	}

	//Cancel Invoices
	$invoices = array();
	if (isset($_POST['cancel_invoice']) && (isset($_POST['cancel_type_cc']) || isset($_POST['cancell_type_fa']))) {
		//Get invoice number
		if (isset($_POST['cancel_type_cc'])) {
			$statement = "SELECT invoice_id FROM nse_establishment_assessment WHERE establishment_code=? AND assessment_status!='complete' AND assessment_type_2='cc'";
			$sql_invoice_id = $GLOBALS['dbCon']->prepare($statement);
			$sql_invoice_id->bind_param('s', $establishment_code);
			$sql_invoice_id->execute();
			$sql_invoice_id->store_result();
			$sql_invoice_id->bind_result($invoice_id);
			while ($sql_invoice_id->fetch()) {
				$invoices[] = $invoice_id;
			}
			$sql_invoice_id->close();
		}
		
		if (isset($_POST['cancel_type_fa'])) {
			$statement = "SELECT invoice_id FROM nse_establishment_assessment WHERE establishment_code=? AND assessment_status!='complete' AND assessment_type_2='fa'";
			$sql_invoice_id = $GLOBALS['dbCon']->prepare($statement);
			$sql_invoice_id->bind_param('s', $establishment_code);
			$sql_invoice_id->execute();
			$sql_invoice_id->store_result();
			$sql_invoice_id->bind_result($invoice_id);
			while ($sql_invoice_id->fetch()) {
				$invoices[] = $invoice_id;
			}
			$sql_invoice_id->close();
		}
		

		//Prepare statement - proforma check
		$statement = "SELECT j_inv_type, j_inv_id, j_inv_order FROM nse_invoice WHERE j_inv_number=? LIMIT 1";
		$sql_invoice_check = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - cancel order/invoice
		$statement = "UPDATE nse_invoice SET j_inv_cancel_date=?, j_inv_cancel_user=? WHERE j_inv_id=?";
		$sql_invoice_update = $GLOBALS['dbCon']->prepare($statement);

		$c = 0;
		foreach ($invoices as $invoice_number) {
			$invoice_type = '';
			$invoice_id = '';
			$order_id = '';
			$invoice_number = str_pad($invoice_number, 8, 0, STR_PAD_LEFT);
			$sql_invoice_check->bind_param('s', $invoice_number);
			$sql_invoice_check->execute();
			$sql_invoice_check->bind_result($invoice_type, $invoice_id, $order_id);
			$sql_invoice_check->fetch();
			$sql_invoice_check->free_result();

			if ($invoice_type == '1') {
				$sql_invoice_update->bind_param('sii', $cancel_date, $_SESSION['j_user']['id'], $invoice_id);
				$sql_invoice_update->execute();
				if ($c==0) {
					$finish_message .= "<li>Pro-forma invoices cancelled</li>";
					$c++;
				}
			}

			if (!empty($order_id)) {
				$sql_invoice_update->bind_param('sii', $cancel_date, $_SESSION['j_user']['id'], $invoice_id);
				$sql_invoice_update->execute();
			}
		}

		$sql_invoice_update->close();
		$sql_invoice_check->close();
	}

	//Remove assessments
	if (isset($_POST['cancel_assessment']) && (isset($_POST['cancel_type_cc']) || isset($_POST['cancell_type_fa']))) {
		$assessments = array();
		
		if (isset($_POST['cancel_type_cc'])) {
			$statement = "SELECT assessment_id FROM nse_establishment_assessment WHERE establishment_code=? AND assessment_status!='complete'  AND assessment_type_2='cc'";
			$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
			$sql_assessments->bind_param('s', $establishment_code);
			$sql_assessments->execute();
			$sql_assessments->store_result();
			$sql_assessments->bind_result($assessment_id);
			while ($sql_assessments->fetch()) {
				$assessments[] = $assessment_id;
			}
			$sql_assessments->close();
		}
		
		if (isset($_POST['cancell_type_fa'])) {
			$statement = "SELECT assessment_id FROM nse_establishment_assessment WHERE establishment_code=? AND assessment_status!='complete' AND assessment_type_2='fa'";
			$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
			$sql_assessments->bind_param('s', $establishment_code);
			$sql_assessments->execute();
			$sql_assessments->store_result();
			$sql_assessments->bind_result($assessment_id);
			while ($sql_assessments->fetch()) {
				$assessments[] = $assessment_id;
			}
			$sql_assessments->close();
		}
		

		//Prepare statement - Delete assessment data
		$statement = "DELETE FROM nse_establishment_assessment_data WHERE assessment_id=?";
		$sql_data_delete = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - Delete Data
		$statement = "DELETE FROM nse_establishment_assessment WHERE assessment_id=?";
		$sql_delete = $GLOBALS['dbCon']->prepare($statement);

		$c = 0;
		foreach ($assessments as $assessment_id) {
			$sql_data_delete->bind_param('i', $assessment_id);
			$sql_data_delete->execute();
			
			$sql_delete->bind_param('i', $assessment_id);
			$sql_delete->execute();
			if ($c == 0) {
				$finish_message .= "<li>Current assessments deleted</li>";
			}
		}
	}

	//Clear QA Out
//	$statement = "DELETE FROM nse_establishment_qa_cancelled where establishment_code=?";
//	$sql_qa_clear = $GLOBALS['dbCon']->prepare($statement);
//	$sql_qa_clear->bind_param('s', $establishment_code);
//	$sql_qa_clear->execute();
//	$sql_qa_clear->close();

	/* Update QA Out */
	//Does an entry exist
	$qa_count = 0;
	$statement = 'SELECT COUNT(*) FROM nse_establishment_qa_cancelled WHERE establishment_code=?';
	$sql_qa_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa_check->bind_param('s', $establishment_code);
	$sql_qa_check->execute();
	$sql_qa_check->bind_result($qa_count);
	$sql_qa_check->fetch();
	$sql_qa_check->close();
	if ($qa_count == 0) {
		$statement = "INSERT INTO nse_establishment_qa_cancelled (establishment_code) VALUES (?)";
		$sql_qa_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_qa_insert->bind_param('s', $establishment_code);
		$sql_qa_insert->execute();
		$sql_qa_insert->close();
	}	
	if (isset($_POST['cancel_type_cc'])) {
		$statement = "UPDATE nse_establishment_qa_cancelled SET cc_cancelled_date=NOW() WHERE establishment_code=?";
		$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);
		$sql_qa_out->bind_param('s', $establishment_code);
		$sql_qa_out->execute();
	}	
	if (isset($_POST['cancel_type_fa'])) {
		$statement = "UPDATE nse_establishment_qa_cancelled SET cancelled_date=NOW() WHERE establishment_code=?";
		$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);
		$sql_qa_out->bind_param('s', $establishment_code);
		$sql_qa_out->execute();
	}
	
	//Add Note
	$note_content = "QA OUT: {$_POST['reason']}";
	insertNote($establishment_code, 4, 0, $note_content,0,0,1);


	
	echo "<div style='border: 1px solid gray; background-color: silver; color: black; font-size: .9em;'>
			$finish_message
		</div>
		<div style='text-align: right'>
			<input type='button' value='Continue' onClick='document.location=\"admin.php?id=$establishment_code\"' >
		</div>";

	die();
}

//Get establishment data
$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
$sql_estab = $GLOBALS['dbCon']->prepare($statement);
$sql_estab->bind_param('s', $establishment_code);
$sql_estab->execute();
$sql_estab->bind_result($establishment_name);
$sql_estab->fetch();
$sql_estab->close();

//Get current QA Out status
$cancel_date = '';
$statement = "SELECT DATE_FORMAT(cancelled_date, '%d-%m-%Y'), DATE_FORMAT(cc_cancelled_date, '%d-%m-%Y') FROM nse_establishment_qa_cancelled WHERE establishment_code=? ORDER BY cancelled_date DESC LIMIT 1";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);
$sql_qa_out->bind_param('s', $establishment_code);
$sql_qa_out->execute();
$sql_qa_out->bind_result($cancel_date, $cc_cancelled_date);
$sql_qa_out->fetch();
$sql_qa_out->close();
if ((!empty($cancel_date) && $cancel_date != '00-00-0000') || (!empty($cc_cancelled_date) && $cc_cancelled_date != '00-00-0000')) {
	$message = "<div style='color: red; background-color: silver'>";
	if (!empty($cancel_date) && $cancel_date != '00-00-0000') $message .= "Fixed Accommodation Cancelled on $cancel_date <input type=button value='Reinstate' onclick='reinstateQa(\"$establishment_code\",\"fa\")' >";
	if (!empty($cc_cancelled_date) && $cc_cancelled_date != '00-00-0000' ) $message .= "<br>Caravan & camping Cancelled on $cc_cancelled_date <input type=button value='Reinstate' onclick='reinstateQa(\"$establishment_code\",\"cc\")' >";
	$message .= "</div>";
}

//Check for current assessments
$statement = "SELECT count(*) FROM nse_establishment_assessment WHERE assessment_status!='complete' AND establishment_code=?";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
$sql_assessment->bind_param('s', $establishment_code);
$sql_assessment->execute();
$sql_assessment->bind_result($assessment_count);
$sql_assessment->fetch();
$sql_assessment->close();
if ($assessment_count == 0) $assessment_message = "There are no open assessments for this establishment";

//Find current QA invoices
$statement = "SELECT invoice_id FROM nse_establishment_assessment WHERE assessment_status!='complete' AND establishment_code=?";
$sql_invoice = $GLOBALS['dbCon']->prepare($statement);
$sql_invoice->bind_param('s', $establishment_code);
$sql_invoice->execute();
$sql_invoice->store_result();
$sql_invoice->bind_result($invoice_id);
while ($sql_invoice->fetch()) {
	if (!empty($invoice_id) && $invoice_id!='0') $invoice_message = "All QA invoices associated with an assessment will be cancelled";
}
$sql_invoice->close();

//Get current endorsement
$statement = "SELECT b.aa_category_name, c.aa_category_name
				FROM nse_establishment AS a
				LEFT JOIN nse_aa_category AS b ON a.aa_category_code=b.aa_category_code
				LEFT JOIN nse_aa_category AS c ON a.cc_endorsement=c.aa_category_code
				WHERE a.establishment_code=? 
				LIMIT 1";
$sql_endorsement = $GLOBALS['dbCon']->prepare($statement);
$sql_endorsement->bind_param('s', $establishment_code);
$sql_endorsement->execute();
$sql_endorsement->bind_result($fa_endorsement, $cc_endorsement);
$sql_endorsement->fetch();
$sql_endorsement->close();
if (empty($fa_endorsement)) $fa_endorsement = '-';
if (empty($cc_endorsement)) $cc_endorsement = '-';

//Replace Tags
$template = str_replace('<!-- message -->', $message, $template);
$template = str_replace('<!-- assessment_message -->', $assessment_message, $template);
$template = str_replace('<!-- invoice_message -->', $invoice_message, $template);
$template = str_replace('<!-- fa_endorsement -->', $fa_endorsement, $template);
$template = str_replace('<!-- cc_endorsement -->', $cc_endorsement, $template);

echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/admin/reinstate_qa.js></script>";
echo $template;

$J_title1="Establishment - QA Out";
$J_title3="<b>".ucwords(strtolower(trim(preg_replace("~[^a-zA-Z0-9 '\(\)\-]~","",$establishment_name))))."</b> | Code: ".$establishment_code;
$J_icon="<img src=".$ROOT."/ico/set/set.png>";
$J_label=22;
$J_width=450;
$J_height=400;
$J_nostart=1;
$J_nomax=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>