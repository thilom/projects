<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/secure.php";
$id=$_GET["id"];

if(count($_POST))
{
	include_once $SDR."/system/activity.php";

	$g=mysql_fetch_array(mysql_query("SELECT firstname,lastname,email FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." LIMIT 1"));
	$from=array($g["email"],ucwords(strtolower($g["firstname"]." ".$g["lastname"])));
	$to=array();
	$i=0;
	foreach($_POST as $k => $v)
	{
		if(strpos($k,"a_")!==false)
		{
			if($v)
			{
				if(is_numeric($v))
					$to[$i]=array(J_Value("email","nse_user","user_id",$v),J_Value("","people","",$v));
				else
				{
					$n=$v;
					$q="SELECT firstname,lastname FROM nse_user WHERE email='".$v."'";
					$q=mysql_query($q);
					if(mysql_num_rows($q))
					{
						$e=mysql_fetch_array($q);
						$n=$e["firstname"]." ".$e["lastname"];
					}
					$to[$i]=array($v,$n);
				}
				$i++;
			}
		}
	}
	$ad_pdf=(file_exists($SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_pdf"])?$SDR."/stuff/establishments/".$eid."/ads/".$id."/pdf/".$adb["last_pdf"]:0);
	$order_number="";
	$order_date="";
	$order_pdf=0;
	if(isset($_POST["order"]))
	{
		$s="SELECT * FROM nse_invoice WHERE j_inv_id=".$adb["order_id"]." LIMIT 1";
		$s=mysql_query($s);
		if(mysql_num_rows($s))
		{
			$g=mysql_fetch_array($s);
			$order_number=$g["j_inv_order"];
			$order_date=($g["j_inv_date"]?date("d/m/Y",$g["j_inv_date"]):"");
			$f=$SDR."/stuff/accounts/pdf_invoice/".$order_number."_".$g["j_inv_id"].".pdf";
			$order_pdf=file_exists($f)?$f:0;
			if(!$order_pdf)
			{
				$xid=$g["j_inv_id"];
				include_once $SDR."/apps/accounts/invoices/pdf_invoice.php";
				$order_pdf=file_exists($f)?$f:0;
			}
		}
	}
	include $SDR."/custom/email_headers.php";
	$subject=$pub["j_in_name"]." (".$est["establishment_name"].")";
	$m=substr(preg_replace('~[^a-zA-Z0-9 ,;:"@#_<>+=%&\*\-\$\'\(\)\[\]\?\.]~',"",strip_tags(str_replace("\n","<br>",str_replace("\r","",$_POST["m"])),"<br>")),0,100000);
	
	$msg=$j_email_header;
	$msg.="<b style='font-size:9pt'>".$est["establishment_name"]."</b><br>";
	$msg.="<b>".$pub["j_in_name"]."</b><hr>";
	$msg.=($m?$m."<br><br><hr>":"");
	if(isset($_POST["letter1"]))
	{
		include $SDR."/apps/ad_manager/f/attach/letter1.php";
		$msg.=$letter1;
	}
	$msg.="<div style='font-size:9pt'>";
	if($ad_pdf)
		$msg.="<b>See Attached AD PDF (".$adb["last_pdf"].")</b><br>";
	if($order_pdf)
		$msg.="<b>See Attached AD ORDER PDF (".$order_number.")</b><br>";
	if(isset($_POST["rates1"]))
		$msg.="<b>See Attached Rate-Sheet</b><br>";
	if($ad_pdf||$order_pdf||isset($_POST["rates1"]))
		$msg.="<hr>";
	$msg.="<b>Sent by:</b><br>";
	$msg.="<a href='mailto:".$from[0]."' onfocus=blur()>".$from[1]."</a><br>";
	if(count($to)>1)
	{
		$msg.="<b>Also sent to:</b><br>";
		foreach($to as $k => $v)
			$msg.="<a href='mailto:".$v[0]."' onfocus=blur()>".$v[1]."</a><br>";
	}
	$msg.="</div>";
	$msg.="<hr>";
	$msg.="<br><br><br>";
	$msg.=$j_email_footer;

	require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
	$mail=new PHPMailer(true);
	try
	{
		$mail->AddReplyTo($from[0],$from[1]);
		$mail->SetFrom($from[0],$from[1]);
		foreach($to as $k => $v)
		{
			$mail->AddAddress($v[0],$v[1]);
			adHist($id,"Sent to: ".$v[1]." (".$v[0].")");
			J_act("Ad",7,$pub["j_in_name"]." (".$id.")");
		}
		$mail->Subject=$subject;
		$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
		$mail->MsgHTML($msg);
		if($ad_pdf)
			$mail->AddAttachment($ad_pdf);
		if($order_pdf)
			$mail->AddAttachment($order_pdf);
		if(isset($_POST["rates1"]))
			$mail->AddAttachment($SDR."/apps/ad_manager/f/attach/icons_and_rates.pdf");
		$mail->Send();
		echo "Message Sent OK";
		echo "<script>setTimeout(\"window.parent.J_WX(self)\",1000)</script>";
		die();
	}
	catch (phpmailerException $e){echo $e->errorMessage();}
	catch (Exception $e){echo $e->getMessage();}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/admin/admin.js></script>";
echo "<style type=text/css>td,td big{font-size:9pt}big{font-weight:bold;color:#808080}</style>";
echo "<body style=margin:12>";

$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_establishment WHERE establishment_code='".$id."' LIMIT 1"));

echo "<form method=post>";
echo "<table id=jTR width=100%>";
echo "<tr><td><big>To</big>";

$q="SELECT u.user_id,u.firstname,u.lastname,u.phone,u.cell,u.email,e.designation FROM nse_user AS u JOIN nse_user_establishments AS e ON u.user_id=e.user_id WHERE e.establishment_code='".$id."'";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$n=0;
	while($p=mysql_fetch_array($q))
	{
		echo "<tt onclick=j_P.J_checkbox(this) onmouseover=\"J_TT(this,'".$p["email"]."')\"><input type=checkbox name=a_".$n." value=".$p["user_id"]."> <b>".preg_replace("[^a-zA_Z0-9 \-]","",$p["firstname"]." ".$p["lastname"])."</b>".($p["designation"]?" (".$p["designation"].")":"")."</tt>";
		$n++;
	}
}

echo "<tr><td><big>Message</big><textarea name=m class=jW100></textarea></td></tr>";
echo "</td></tr>";
echo "<tr><td><big>Documents</big>";

echo "</td></tr>";
echo "</table>";
echo "<br><input type=submit value=OK>";
echo "</form>";
echo "<script>J_tr()</script>";

$J_title1="Send Documents";
$J_title3="<b>To: ".ucwords(strtolower(trim(preg_replace("~[^a-zA-Z0-9 '\(\)\-]~","",$g["establishment_name"]))))."</b> | Code: ".$id;
$J_icon="<img src=".$ROOT."/ico/set/email.png>";
$J_label=22;
$J_width=360;
$J_height=260;
$J_nostart=1;
$J_nomax=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>