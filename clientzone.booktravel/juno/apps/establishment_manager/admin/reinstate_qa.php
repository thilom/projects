<?php

/**
 * Reinstate a QA that has previously been cancelled
 * 
 * @author Thilo Muller(2011)
 */

//Includes
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $SDR."/apps/notes/f/insert.php";

//Vars
$reason = urldecode($_GET['reason']);
$establishment_code = $_GET['code'];
$qa_type = $_GET['type'];
$message = '';

//Log reinstatement
$note_content = "QA Reinstated: $reason";
insertNote($establishment_code, 4, 0, $note_content,0,0,1);

//Remove QA Out
if ($qa_type == 'fa') {
	$statement = "UPDATE nse_establishment_qa_cancelled 
				SET cancelled_date = NULL
				WHERE establishment_code=?
				LIMIT 1";
	$message = "Fixed accommodation QA reinstated";
} else {
	$statement = "UPDATE nse_establishment_qa_cancelled 
				SET cc_cancelled_date = NULL
				WHERE establishment_code=?
				LIMIT 1";
	$message = "Caravan and camping QA reinstated";
}
$sql_clear = $GLOBALS['dbCon']->prepare($statement);
$sql_clear->bind_param('s', $establishment_code);
$sql_clear->execute();
$sql_clear->close();

echo "	<div style='border: 1px solid gray; background-color: silver; color: black; font-size: .9em;'>
			$message
		</div>
		<div style='text-align: right'>
			<input type='button' value='Continue' onClick='document.location=\"admin.php?id=$establishment_code\"' >
		</div>";

?>
