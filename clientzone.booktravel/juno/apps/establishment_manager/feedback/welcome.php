<?php

//Includes
include_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/shared/PHPMailer/class.phpmailer.php';

if (!isset($_GET['eID'])) {
	die();
}

$establishment_code = $_GET['eID'];
$award_year = $_GET['year'];
$participation_options = '';

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/awards/awards_welcome/welcome.js></script>";
echo "<body>";

//Get establishment name
$statement = "SELECT establishment_name
				FROM nse_establishment 
				WHERE establishment_code=?
				LIMIT 1";
$sql_name = $GLOBALS['dbCon']->prepare($statement);
$sql_name->bind_param('s', $establishment_code);
$sql_name->execute();
$sql_name->bind_result($establishment_name);
$sql_name->fetch();
$sql_name->close();

if (count($_POST) > 0) {
	
	$email_template = file_get_contents('email_template.html');
	
	//Get participation options
	$counter = 1;
	$forms_append = '';
	$statement = "SELECT a.input_id, b.input_name 
					FROM nse_establishment_awards_input AS a
					LEFT JOIN nse_awards_input_methods AS b ON a.input_id=b.input_id
					WHERE establishment_code=? AND award_year=?";
	$sql_inputs = $GLOBALS['dbCon']->prepare($statement);
	$sql_inputs->bind_param('ss', $establishment_code, $award_year);
	$sql_inputs->execute();
	$sql_inputs->store_result();
	$input_count = $sql_inputs->num_rows;
	$sql_inputs->bind_result($input_id, $input_name);
	while ($sql_inputs->fetch()) {
		if ($counter > 1) {
			$participation_options .= $counter==$input_count?" and":",";
		}
		$participation_options .= " $input_name";
		if ($input_id == 3) $forms_append = " Our AA Guest Review forms will be posted to you shortly.";
		$counter++;
	}
	$sql_inputs->close();
	$participation_options .= $input_count>1?" options.":" option.";
	$participation_options .= $forms_append;
	
	//Replace Vars
	$email_template = str_replace('<!-- participation_options -->', $participation_options, $email_template);
	$email_template = str_replace('<!-- award_year -->', $award_year, $email_template);
	
	//Prepare statement - Get user email
	$statement = "SELECT email, CONCAT(firstname, ' ', lastname)
					FROM nse_user
					WHERE user_id=?
					LIMIT 1";
	$sql_email = $GLOBALS['dbCon']->prepare($statement);

	//Send emails
	foreach ($_POST['emails'] as $user_id) {
		$sql_email->bind_param('i', $user_id);
		$sql_email->execute();
		$sql_email->bind_result($email_address, $contact_name);
		$sql_email->fetch();
		
		//Assemble email
		$mail = new PHPMailer(true);
		try {
			$mail->ClearAllRecipients();
			$mail->AddAddress($email_address, $contact_name);
			$mail->SetFrom('awards@aatraveladmin.co.za', 'Vanessa Weltman');
			$mail->AddReplyTo('awards@aatraveladmin.co.za', 'Vanessa Weltman');
			$mail->Subject = "Thank you for participating in the AAQAA Awards";
			$mail->MsgHTML($email_template);
			$mail->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'] . '/juno/apps/assessment_manager/i/qa_logo.jpg', 'logoimg', 'qa_logo.jpg');
			$mail->Send();
		} catch (phpmailerException $e) {
			echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
		}
	}
	
	echo "<div style='width: 200px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Email Sent</div>";
	echo "<div style='width: 200px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='parent.J_WX({$_GET['j_W']});return false' ></div>";
	
	die();
}

$counter = 0;
$email_list = "<TABLE width=100% id=jTR style='border: solid 1px black'><tr style='background-color: gray'><td>&nbsp;</td><td>Contact</td><td>Email</td><td>Designation</td></tr>";
$req = '';

$statement =  "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
				FROM nse_user_establishments AS a
				JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code=?";
$sql_email = $GLOBALS['dbCon']->prepare($statement);
$sql_email->bind_param('s', $establishment_code);
$sql_email->execute();
$sql_email->store_result();
$sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
while ($sql_email->fetch()) {
	$email_list .= "<tr><td class=jdbO><input type=checkbox name=emails[] id=email_$counter value='$uID' >";
	$email_list .= "</td><td class=jdbR><label for=email_$counter >$contact_name</label></td><td class=jdbY>$contact_email</td><td class=jdbB>$contact_designation</td></tr>" . PHP_EOL;
	$counter++;
}
$sql_email->free_result();
$sql_email->close();


$email_list .= '</TABLE >';
//$email_list = substr($email_list, 6);

//Get template
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/juno/apps/awards/awards_welcome/welcome_form.html");

//Replace Tags
$template = str_replace('<!-- email_list -->', $email_list, $template);
$template = str_replace('<!-- req -->', $req, $template);

echo $template;

$J_title1=$establishment_name;
$J_title2="Send Awards Welcome";
$J_title3="Code: $establishment_code";
$J_icon="<img src=".$ROOT."/ico/set/email.png>";
$J_nostart=1;
$J_label=23;
$J_height=640;
$J_width=820;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>