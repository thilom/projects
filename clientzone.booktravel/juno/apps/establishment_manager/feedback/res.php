<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include $SDR."/system/get.php";
$d="";

$t=array("X","date","code","establishment","visitor","status","score","comment");
$da="";
$n=1;

foreach($t as $k => $v)
{
	$da.=$n;
	$n++;
	$da.="~";
}

//Vars
$r = '';
$establishment_code = $_GET['est'];
$jL2 = isset($_GET['jL2'])?$_GET['jL2']:10;
$jL1 = isset($_GET['jL1'])?$_GET['jL1']:0;
$status_codes = array(0=>'Dissaproved', 'Waiting','Approved','On Hold');


/* PUT STUFF HERE */

//Get list of 
$line_counter = 0;
$filter = -1;
$statement = "SELECT a.feedback_id, a.feedback_date, a.feedback_status, b.establishment_code, b.establishment_name, CONCAT(c.firstname, ' ', c.surname), a.visitor_comment
				FROM nse_establishment_feedback AS a
				LEFT JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
				LEFT JOIN nse_visitor AS c ON c.visitor_id=a.visitor_id ";
if (isset($_GET['filter']) && $_GET['filter'] != -1) {
	$filter = $_GET['filter'];
	$statement .= "WHERE a.feedback_status={$_GET['filter']} ";
}

if (isset($_GET['noEstab'])) {
	$statement .= strpos($statement, 'WHERE')>0?' AND':' WHERE';
	$statement .= " a.establishment_code='' || a.establishment_code IS NULL ";
} else if (isset($_GET['est']) && !empty($_GET['est'])) {
	$statement .= strpos($statement, 'WHERE')>0?' AND':' WHERE';
	$statement .= " a.establishment_code='{$_GET['est']}' ";
}




$statement .= "ORDER BY a.feedback_date DESC";
if ($filter == 1) {
	$statement .= " LIMIT 0,50";
}
$sql_feedback = $GLOBALS['dbCon']->prepare($statement);
$sql_feedback->execute();
$sql_feedback->store_result();
$result_count = $sql_feedback->num_rows;
$sql_feedback->bind_result($feedback_id, $feedback_date, $feedback_status, $establishment_code, $establishment_name, $visitor_name, $comment);
while ($sql_feedback->fetch()) {
	$statement = "SELECT feedback_value FROM nse_feedback_results WHERE feedback_id = ".$feedback_id;
	$sql_feedbackres = $GLOBALS['dbCon']->prepare($statement);
	$sql_feedbackres->execute();
	$sql_feedbackres->store_result();
	$sql_feedbackres->bind_result($feedback_value);
	
	$resultAvg = "NA";
	
	if ($filter == 1) {
		$resultCount = 0;
		$resultTotal = 0;
		while ($sql_feedbackres->fetch()) {
			if ($feedback_value != 0) {
				$resultCount++;
				$resultTotal += $feedback_value;
			}
		}
		$resultAvg = round(($resultTotal/$resultCount),1);
	}

	$c = $jL1+$jL2;	
	if ($line_counter >= $jL1 && $line_counter <= $c) {
		$r .= "$feedback_id~$feedback_id~$feedback_date~$establishment_code~$establishment_name~$visitor_name~{$status_codes[$feedback_status]}~$resultAvg~$comment|";
	} 
	$line_counter++;
}
$sql_feedback->close();

$r=str_replace("\r","",$r);
$r=str_replace("\n","",$r);
$r=str_replace("\"","",$r);
$r=str_replace("~|","|",$r);

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/apps/jsscripts/list_item_remover.js></script>";
echo "<script src=".$ROOT."/apps/establishment_manager/feedback/res.js></script>";
echo "<script>J_in_r(\"$r\",\"$da\",\"$result_count\",0,$jL2,'$establishment_code')</script>";

?>