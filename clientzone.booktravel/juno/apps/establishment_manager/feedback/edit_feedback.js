/**
 * Closes the current window
 */
function closeWindow() {
	wVal = getUrlValue('J_W');
	parent.J_WX(wVal);
}

/**
 * Get a value from URL
 */
function getUrlValue(sVal) {
	var searchResult;
    docLocation = document.location.href;
	searchLength = sVal.length + 1;
	WLoc = docLocation.indexOf(sVal+'=');
	if (WLoc > 0) {
		WEnd = docLocation.indexOf('&', WLoc);
		searchResult = WEnd>0?docLocation.substring(WLoc+searchLength,WEnd):docLocation.substring(WLoc+searchLength);
	} else {
		searchResult=0;
	}
	return searchResult;
}

/**
 * Delete Feedback
 */
function deleteFeedback(id) {
    if(confirm("WARNING!\nThis will permanently delete the selected feedback from the database.\nThis action cannot be undone.\n\nContinue")) {
		document.location="/juno/apps/establishment_manager/feedback/edit_feedback.php?f=delete&id=" + id + "&J_W=" + getUrlValue('j_W');
	}
}


