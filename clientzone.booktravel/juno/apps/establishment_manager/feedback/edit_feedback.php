<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/juno/set/init.php";
require_once $_SERVER["DOCUMENT_ROOT"] . '/shared/PHPMailer/class.phpmailer.php';
$SUB_ACCESS = array("approve_feedback" => 0, "delete_feedback" => 0, "disapprove_feedback" => 0, "hold_feedback" => 0, "email_feedback" => 0);
include_once $SDR . "/system/secure.php";

echo "<html>";
echo "<script src=" . $ROOT . "/system/P.js></script>";
echo "<body>";

$content = file_get_contents('edit_feedback.html');

//Put on hold
if (isset($_GET['f']) && $_GET['f'] == 'hold') {
	$feedback_id = ctype_digit($_GET['id']) ? $_GET['id'] : '';
	$establishment_code = '';
	$establishment_name = '';

	$statement = "UPDATE nse_establishment_feedback SET feedback_status='3' WHERE feedback_id=?";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('s', $feedback_id);
	$sql_status->execute();

	//Get establishment Code
	$statement = "SELECT a.establishment_code, b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $feedback_id);
	$sql_estab->execute();
	$sql_estab->bind_result($establishment_code, $establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();

	echo "<script src='edit_feedback.js'></script>";
	echo "<div style='width: 300px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Feedback placed on Hold</div>";
	echo "<div style='width: 300px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='closeWindow()' ></div>";

	update_cache($feedback_id);

	die();
}

//Approve
if (isset($_GET['f']) && $_GET['f'] == 'approve') {

	//Vars
	$feedback_id = ctype_digit($_GET['id']) ? $_GET['id'] : '';
	$establishment_code = '';
	$establishment_name = '';

	$statement = "UPDATE nse_establishment_feedback SET feedback_status='2' WHERE feedback_id=?";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('s', $feedback_id);
	$sql_status->execute();

	//Get establishment Code
	$statement = "SELECT a.establishment_code, b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $feedback_id);
	$sql_estab->execute();
	$sql_estab->bind_result($establishment_code, $establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	if (isset($_GET['r'])) {
		echo "<div style='width: 300px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Feedback approved</div>";
		echo "<script>parent.jF".$_GET['r'].".J_in_srch();</script>";
	} else {
		echo "<script src='edit_feedback.js'></script>";
		echo "<div style='width: 300px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Feedback approved</div>";
		echo "<div style='width: 300px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='closeWindow()' ></div>";
	}

	update_cache($feedback_id);
	die();
}

//Disapprove
if (isset($_GET['f']) && $_GET['f'] == 'disapprove') {

	//Vars
	$feedback_id = ctype_digit($_GET['id']) ? $_GET['id'] : '';
	$establishment_code = '';
	$establishment_name = '';

	$statement = "UPDATE nse_establishment_feedback SET feedback_status='0' WHERE feedback_id=?";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('s', $feedback_id);
	$sql_status->execute();

	//Get establishment Code
	$statement = "SELECT a.establishment_code, b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $feedback_id);
	$sql_estab->execute();
	$sql_estab->bind_result($establishment_code, $establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();
	
	if (isset($_GET['r'])) {
		echo "<div style='width: 300px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Feedback disapproved</div>";
		echo "<script>parent.jF".$_GET['r'].".J_in_srch();</script>";
	} else {
		echo "<script src='edit_feedback.js'></script>";
		echo "<div style='width: 300px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Feedback disapproved</div>";
		echo "<div style='width: 300px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='closeWindow()' ></div>";
	}

	update_cache($feedback_id);

	die();
}

//Delete
if (isset($_GET['f']) && $_GET['f'] == 'delete') {

	$feedback_id = ctype_digit($_GET['id']) ? $_GET['id'] : '';
	$establishment_name = '';


	//Get establishment Code
	$statement = "SELECT b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $feedback_id);
	$sql_estab->execute();
	$sql_estab->bind_result($establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();

	$statement = "DELETE FROM nse_establishment_feedback WHERE feedback_id=?";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('s', $feedback_id);
	$sql_status->execute();

	$statement = "DELETE FROM nse_feedback_results WHERE feedback_id=?";
	$sql_status = $GLOBALS['dbCon']->prepare($statement);
	$sql_status->bind_param('s', $feedback_id);
	$sql_status->execute();

	echo "<script src='edit_feedback.js'></script>";
	echo "<div style='width: 300px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Feedback deleted</div>";
	echo "<div style='width: 300px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='closeWindow()' ></div>";

	update_cache($feedback_id);

	die();
}

//Email
if (isset($_GET['f']) && $_GET['f'] == 'email') {

	$feedback_id = ctype_digit($_GET['id']) ? $_GET['id'] : '';
	$counter = 0;
	$email_list = "<TABLE width=100% id=jTR style='border: solid 1px black; border-collapse: collapse'><tr style='background-color: gray'><td>&nbsp;</td><td>Contact</td><td>Email</td><td>Designation</td></tr>";
	$req = '';

	if (isset($_GET['hold'])) {
		$statement = "UPDATE nse_establishment_feedback SET feedback_status='3' WHERE feedback_id=?";
		$sql_status = $GLOBALS['dbCon']->prepare($statement);
		$sql_status->bind_param('s', $feedback_id);
		$sql_status->execute();
	}

	//Get establishment Code
	$statement = "SELECT a.establishment_code, b.establishment_name
    				FROM nse_establishment_feedback AS a
    				JOIN nse_establishment AS b ON a.establishment_code=b.establishment_code
    				WHERE feedback_id=?";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('s', $feedback_id);
	$sql_estab->execute();
	$sql_estab->bind_result($establishment_code, $establishment_name);
	$sql_estab->fetch();
	$sql_estab->free_result();
	$sql_estab->close();

	if (count($_POST) > 0) {

		//Vars
		$email_content = file_get_contents('email_content.html');
		$email_content = str_replace(array("\r\n", "\r", "\n"), '$%^', $email_content);
		preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@', $email_content, $matches);
		$lContent = $matches[0][0];
		$color = '';
		$nLine = '';
		$reply = '';
		$message = $_POST['email_message'];

		$mail = new PHPMailer();

		//Get Data
		$statement = "SELECT DATE_FORMAT(a.feedback_date, '%d %M, %Y'), a.visitor_comment, a.feedback_status, a.establishment_code, c.firstname, c.surname
						FROM nse_establishment_feedback AS a
						LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
						WHERE feedback_id=? ";
		$sql_feedback = $GLOBALS['dbCon']->prepare($statement);
		$sql_feedback->bind_param('s', $feedback_id);
		$sql_feedback->execute();
		$sql_feedback->bind_result($feedback_date, $feedback_message, $feedback_status, $establishment_code, $firstname, $surname);
		$sql_feedback->fetch();
		$sql_feedback->free_result();
		$sql_feedback->close();
		if (empty($feedback_message))
			$feedback_message = 'No Comment';

		//Get feedback results
		$statement = "SELECT a.feedback_value, b.category_description
						FROM nse_feedback_results AS a
						JOIN nse_feedback_category AS b ON a.category_code=b.category_code
						WHERE a.feedback_id=?";
		$sql_results = $GLOBALS['dbCon']->prepare($statement);
		$sql_results->bind_param('s', $feedback_id);
		$sql_results->execute();
		$sql_results->bind_result($feedback_value, $feedback_description);
		$sql_results->store_result();
		while ($sql_results->fetch()) {
			$color = ($color == '#DDDDDD') ? '#E1E1E1' : '#DDDDDD';
			$line = str_replace('<!-- feedback_item -->', $feedback_description, $lContent);
			$line = str_replace('<!-- line_color -->', $color, $line);
			for ($x = 5; $x >= -1; $x--) {
				if ($x == $feedback_value) {
					$line = str_replace("<!-- feedback_value$x -->", "*", $line);
				} else {
					$line = str_replace("<!-- feedback_value$x -->", '&nbsp;', $line);
				}
			}

			$nLine .= $line;
		}

		//Get Establishment Location
		$statement = "SELECT a.establishment_name, c.town_name, d.suburb_name
						FROM nse_establishment AS a
						JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
						LEFT JOIN nse_location_town AS c ON b.town_id=c.town_id
						LEFT JOIN nse_location_suburb AS d ON b.suburb_id=d.suburb_id
						WHERE a.establishment_code=?";
		$sql_location = $GLOBALS['dbCon']->prepare($statement);
		$sql_location->bind_param('s', $establishment_code);
		$sql_location->execute();
		$sql_location->bind_result($establishment_name, $town_name, $suburb_name);
		$sql_location->fetch();
		$sql_location->free_result();
		$sql_location->close();

		//Assemble Location
		if (!empty($suburb_name)) {
			$location = "$suburb_name, $town_name";
		} else {
			$location = $town_name;
		}

		//Replace Tags
		$email_content = str_replace('<!-- establishment_name -->', $establishment_name, $email_content);
		$email_content = str_replace('<!-- message -->', $message, $email_content);
		$email_content = str_replace('<!-- feedback_name -->', "$firstname $surname", $email_content);
		$email_content = str_replace('<!-- feedback_date -->', $feedback_date, $email_content);
		$email_content = str_replace('<!-- feedback_message -->', $feedback_message, $email_content);
		$email_content = str_replace('<!-- location -->', $location, $email_content);
		$email_content = str_replace('<!-- reply -->', $reply, $email_content);
		$email_content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@', $nLine, $email_content);
		$email_content = str_replace('$%^', "\n", $email_content);

		//Prepare statement - Get email address
		$statement = "SELECT email, CONCAT(firstname, ' ', lastname)
						FROM nse_user
						WHERE user_id=?
						LIMIT 1";
		$sql_email = $GLOBALS['dbCon']->prepare($statement);

		//Send email
		if (isset($_POST['emails'])) {
			foreach ($_POST['emails'] as $user_id) {
				$email = '';
				$email_name = '';

				//Get email address
				$sql_email->bind_param('i', $user_id);
				$sql_email->execute();
				$sql_email->bind_result($email, $email_name);
				$sql_email->fetch();
				$sql_email->free_result();

				$mail->AddAddress($email, $email_name);
				$email_list .= " $email,";
			}
		}

		if (isset($_POST['other_email']) && !empty($_POST['other_email'])) {
			$mail->AddAddress($_POST['other_email']);
			$email_list .= " {$_POST['other_email']},";
		}

		$email_list = substr($email_list, 0, -1);
		try {
			$mail->AddReplyTo('feedback@booktravel.travel', 'booktravel.travel');
			$mail->SetFrom('feedback@booktravel.travel', 'booktravel.travel');
			$mail->Subject = "Feedback for $establishment_name ($location) from $firstname, $surname ";
			$mail->MsgHTML($email_content);
			$mail->Send();
		} catch (phpmailerException $e) {
			echo "<script>alert('ERROR!\nFeedback email to $establishment_name ($email) failed');</script>";
		} catch (Exception $e) {
			echo "<script>alert('ERROR!\nFeedback(id: $feedback_id) email to $establishment_name($email) failed');</script>";
		}

		echo "<script src='edit_feedback.js'></script>";
		echo "<div style='width: 300px; text-align: center; margin: auto auto; color: black; background-color: silver; border: 1px dotted black'>Email Sent</div>";
		echo "<div style='width: 300px; text-align: center; margin: auto auto;'><input type=button value='Continue' onClick='closeWindow()' ></div>";

		die();
	}

	//Get email list
	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), a.designation, b.email, a.user_id
					FROM nse_user_establishments AS a
					JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=?";
	$sql_email = $GLOBALS['dbCon']->prepare($statement);
	$sql_email->bind_param('s', $establishment_code);
	$sql_email->execute();
	$sql_email->store_result();
	$sql_email->bind_result($contact_name, $contact_designation, $contact_email, $uID);
	while ($sql_email->fetch()) {
		$email_list .= "<tr><td class=jdbO><input type=checkbox name='emails[]' id='email_$counter' value='$uID' >";
		$email_list .= "</td><td class=jdbR><label for='email_$counter' >$contact_name</label></td><td class=jdbY>$contact_email</td><td class=jdbB>$contact_designation</td></tr>" . PHP_EOL;
		$counter++;
	}
	$sql_email->free_result();
	$sql_email->close();


	$email_list .= "<tr><td class=jdbO>";
	$email_list .= "</td><td class=jdbR>Additional Email</td><td class=jdbB><input type='text' name='other_email'></td></tr>" . PHP_EOL;
	$email_list .= '</TABLE >';
	//$email_list = substr($email_list, 6);
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/juno/apps/establishment_manager/feedback/welcome_form.html");

	//Replace Tags
	$template = str_replace('<!-- email_list -->', $email_list, $template);
	$template = str_replace('<!-- req -->', $req, $template);

	echo $template;

	die();
}

//Vars
$feedback_id = ctype_digit($_GET['id']) ? $_GET['id'] : '';
$feedback_date = '';
$establishment_name = '';
$firstname = '';
$surname = '';
$feedback_message = '';
$feedback_status = '';
$matches = array();
$nLine = '';
$feedback_value = '';
$feedback_description = '';
$buttons = '';
$town_name = '';
$suburb_name = '';
$establishment_code = '';
$location = '';
$reply = 'No Reply';

//Get template
$content = str_replace(array("\r\n", "\r", "\n"), '$%^', $content);
preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@', $content, $matches);
$lContent = $matches[0][0];

//Get Data
$statement = "SELECT DATE_FORMAT(a.feedback_date, '%d %M, %Y'), a.visitor_comment, a.feedback_status, a.establishment_code, c.firstname, c.surname, c.email
    				FROM nse_establishment_feedback AS a
    				LEFT JOIN nse_visitor AS c ON a.visitor_id=c.visitor_id
    				WHERE feedback_id=? ";
$sql_feedback = $GLOBALS['dbCon']->prepare($statement);
$sql_feedback->bind_param('s', $feedback_id);
$sql_feedback->execute();
$sql_feedback->bind_result($feedback_date, $feedback_message, $feedback_status, $establishment_code, $firstname, $surname, $feedback_email);
$sql_feedback->fetch();
$sql_feedback->free_result();
$sql_feedback->close();
if (empty($feedback_message))
	$feedback_message = 'No Comment';

//Get feedback results
$statement = "SELECT a.feedback_value, b.category_description
    				FROM nse_feedback_results AS a
    				JOIN nse_feedback_category AS b ON a.category_code=b.category_code
    				WHERE a.feedback_id=?";
$sql_results = $GLOBALS['dbCon']->prepare($statement);
$sql_results->bind_param('s', $feedback_id);
$sql_results->execute();
$sql_results->bind_result($feedback_value, $feedback_description);
$sql_results->store_result();
while ($sql_results->fetch()) {
	$line = str_replace('<!-- feedback_item -->', $feedback_description, $lContent);

	for ($x = 5; $x >= -1; $x--) {
		if ($x == $feedback_value) {
			$line = str_replace("<!-- feedback_value$x -->", "<img src='/juno/ico/set/material_16.png'", $line);
		} else {
			$line = str_replace("<!-- feedback_value$x -->", '', $line);
		}
	}

	$nLine .= $line;
}

//Get Establishment Location
$statement = "SELECT a.establishment_name, c.town_name, d.suburb_name
    				FROM nse_establishment AS a
    				JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
    				LEFT JOIN nse_location_town AS c ON b.town_id=c.town_id
    				LEFT JOIN nse_location_suburb AS d ON b.suburb_id=d.suburb_id
    				WHERE a.establishment_code=?";
$sql_location = $GLOBALS['dbCon']->prepare($statement);
$sql_location->bind_param('s', $establishment_code);
$sql_location->execute();
$sql_location->bind_result($establishment_name, $town_name, $suburb_name);
$sql_location->fetch();
$sql_location->free_result();
$sql_location->close();

//Assemble Location
if (!empty($suburb_name)) {
	$location = "$suburb_name, $town_name";
} else {
	$location = $town_name;
}

//Calculate status
switch ($feedback_status) {
	case 0:
		$status = 'Disapproved';
		break;
	case 1:
		$status = 'Awaiting Action';
		break;
	case 2:
		$status = 'Approved (Live)';
		break;
	case 3:
		$status = 'On Hold';
		break;
	default:
		$status = '';
}

//Generate Buttons
if ($SUB_ACCESS['delete_feedback'])
	$buttons .= "<input type=button class=pause_button value='Hold' onClick='document.location=\"/juno/apps/establishment_manager/feedback/edit_feedback.php?f=hold&id=$feedback_id&J_W={$_GET['j_W']}\"'>";
if ($SUB_ACCESS['delete_feedback'])
	$buttons .= "<input type=button class=email_button value='Email' onClick='document.location=\"/juno/apps/establishment_manager/feedback/edit_feedback.php?f=email&id=$feedback_id&J_W={$_GET['j_W']}\"'>";
if ($SUB_ACCESS['delete_feedback'])
	$buttons .= "<input type=button class=email_button value='Email & Hold' onClick='document.location=\"/juno/apps/establishment_manager/feedback/edit_feedback.php?f=email&id=$feedback_id&hold\"'>";
if ($SUB_ACCESS['approve_feedback'])
	$buttons .= "<input type=button class=ok_button value='Approve' onClick='document.location=\"/juno/apps/establishment_manager/feedback/edit_feedback.php?f=approve&id=$feedback_id&J_W={$_GET['j_W']}\"'>";
//    if ($SUB_ACCESS['approve_feedback']) $buttons .= "<input type=button class=edit_button value='Edit & Approve' onClick='document.location=\"/juno/apps/establishment_manager/feedback/edit_feedback.php?f=edit_feedback&id=$feedback_id\"'>";
if ($SUB_ACCESS['disapprove_feedback'])
	$buttons .= "<input type=button class=cross_button value='Disapprove' onClick='document.location=\"/juno/apps/establishment_manager/feedback/edit_feedback.php?f=disapprove&id=$feedback_id&J_W={$_GET['j_W']}\"'>";
if ($SUB_ACCESS['delete_feedback'])
	$buttons .= "<input type=button class=delete_button value='Delete' onClick=deleteFeedback('$feedback_id')>";
//
//Replace Tags
$content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
$content = str_replace('<!-- feedback_name -->', "$firstname $surname", $content);
$content = str_replace('<!-- feedback_email -->', $feedback_email, $content);
$content = str_replace('<!-- feedback_date -->', $feedback_date, $content);
$content = str_replace('<!-- feedback_message -->', $feedback_message, $content);
$content = str_replace('<!-- feedback_status -->', $status, $content);
$content = str_replace('<!-- buttons -->', $buttons, $content);
$content = str_replace('<!-- location -->', $location, $content);
$content = str_replace('<!-- reply -->', $reply, $content);
$content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@', $nLine, $content);
$content = str_replace('$%^', "\n", $content);

echo $content;


$J_title1 = "Edit Feedback";
$J_title2 = "$establishment_name, $location";
$J_title3 = "$establishment_code";
$J_label = 22;
$J_width = 800;
$J_nostart = 1;
include $SDR . "/system/deploy.php";
echo "</body></html>";

/**
 * Recalculate average feedback results and saves it to a table for use on the website.
 *
 * @param $feedback_id
 */
function update_cache($feedback_id) {

    //Vars
    $results = array();
	$totals = array();
	$establishment_code = '';
	$feedback_id_bulk = '';
	$code = '';
	$value = '';

	//Get Establishment Code
	$statement = "SELECT establishment_code FROM nse_establishment_feedback WHERE feedback_id=?";
	$sql_code = $GLOBALS['dbCon']->prepare($statement);
	$sql_code->bind_param('s', $feedback_id);
	$sql_code->execute();
	$sql_code->bind_result($establishment_code);
	$sql_code->fetch();
	$sql_code->free_result();
	$sql_code->close();


	//Clear cache
	$statement = "DELETE FROM nse_feedback_cache WHERE establishment_code=?";
	$sql_cache_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_cache_delete->bind_param('s', $establishment_code);
	$sql_cache_delete->execute();

	$statement = "SELECT category_code, feedback_value FROM nse_feedback_results WHERE feedback_id=?";
	$sql_cache_results = $GLOBALS['dbCon']->prepare($statement);

	$statement = "SELECT feedback_id FROM nse_establishment_feedback WHERE establishment_code=? && feedback_status='2'";
	$sql_cache_ids = $GLOBALS['dbCon']->prepare($statement);
	$sql_cache_ids->bind_param('s', $establishment_code);
	$sql_cache_ids->execute();
	$sql_cache_ids->bind_result($feedback_id_bulk);
	$sql_cache_ids->store_result();
	while ($sql_cache_ids->fetch()) {
		$sql_cache_results->bind_param('s', $feedback_id_bulk);
		$sql_cache_results->execute();
		$sql_cache_results->bind_result($code, $value);
		$sql_cache_results->store_result();
		while ($sql_cache_results->fetch()) {
			if (0 == $value) continue;
			if (!isset($results[$code])) $results[$code] = array();
			$results[$code][] = $value;
		}
	}

	$statement = "INSERT INTO nse_feedback_cache (establishment_code, category_code, smilies) VALUES (?,?,?)";
	$sql_cache_insert = $GLOBALS['dbCon']->prepare($statement);
	foreach ($results as $c=>$code) {
		$totals[$c] = round(array_sum($code)/count($code));
		$sql_cache_insert->bind_param('sss', $establishment_code, $c, $totals[$c]);
		$sql_cache_insert->execute();
	}

	$tot = (count($totals)!= 0)?round(array_sum($totals)/count($totals)):'';
	$c = "total";
	$sql_cache_insert->bind_param('sss', $establishment_code, $c, $tot);
	$sql_cache_insert->execute();
}
?>


