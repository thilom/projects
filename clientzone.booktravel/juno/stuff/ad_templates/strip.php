<?php
$ad="";

if(isset($edit))
{
	$scale=(isset($_GET["scale"])?$_GET["scale"]:6);
	$adW=$adWidth*$scale;
	$ad.="<div class=box style=width:".$adW.";height:20;position:relative>";

	$ad.="<div id=tab style='position:absolute;left:0;top:0;width:".$adW.";font-size:12pt;padding:1 4 1 4;background:".$color_rgb."'>";
	$ad.="<div id=par style=height:20>";
	$ad.="<div class=rates style='float:right;margin:0 8 0 0'><span onclick=A_ed(this.nextSibling,'rates') onmouseover=\"J_TT(this,'Rates')\">Rates: </span><span onclick=A_ed(this,'rates')><span>".$rates."</span></span></div>";
	$ad.="<b id=region onclick=A_ed(this,'region')><span onmouseover=\"J_TT(this,'Region')\">".$region."</span></b> ";
	$ad.="<b id=town onclick=A_ed(this,'town') onmouseover=\"J_TT(this,'Town or suburb')\"><span>".$town."</span></b>";
	$ad.="</div>";
	$ad.="</div>";

	$ad.="</div>";
}
else
{
	$adW=89/$points;
	$adH=3.5/$points;

	try
	{
		$p = new PDFlib();
		$p->set_parameter("logging", "filename=".$SDR."/stuff/ad_templates/logging/".preg_replace("~[^a-zA-Z0-9\-]~","",str_replace(array(" ","--"),"-",$pdf_title)).".log");
		$p->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");
		$p->set_parameter("textformat", "bytes");
		if($p->begin_document($pdf_filename,"")==0)
			die("Error: " . $p->get_errmsg());

		$p->set_info("Creator", "AA TRAVEL GUIDES");
		$p->set_info("Author", $pdf_author);
		$p->set_info("Title", $pdf_title);

		$p->begin_page_ext($adW, $adH, "");

		$p->set_parameter("FontOutline", "HelveticaCondensedBold=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p->load_font("HelveticaCondensedBold", "unicode", "embedding");
		$p->setfont($font, 24.0);
		$p->set_parameter("FontOutline", "HelveticaCondensed=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p->load_font("HelveticaCondensed", "unicode", "embedding");
		$p->setfont($font, 24.0);

		$c=explode(",",$color_cmyk);
		$p->setcolor("fill", "cmyk", $c[0], $c[1], $c[2], $c[3]);
		$p->rect(0, $adH-(3.5/$points), $adW, 3.5/$points);
		$p->fill();

		$region=($region&&$region!="?"?$region:"");
		$region.=($town&&$town!="?"?trim($town):"");
		if($region)
			$p->fit_textline($region, 1/$points, $adH-(1.8/$points), "position={left center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=7 fillcolor={cmyk ".$c[0]." ".$c[1]." ".$c[2]." 1} fontname=HelveticaCondensedBold encoding=unicode");

		$rates=($rates&&$rates!="?"?"Rates: ".$rates:"");
		if($rates)
			$p->fit_textline($rates, $adW-(1/$points), $adH-(1.8/$points), "position={right center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=7 fillcolor={cmyk ".$c[0]." ".$c[1]." ".$c[2]." 1}");

		$p->end_page_ext("");
		$p->end_document("");
	}
	catch (PDFlibException $e)
	{
		die("PDFlib exception occurred in hello sample:\n" .
		"[" . $e->get_errnum() . "] " . $e->get_apiname() . ": " .
		$e->get_errmsg() . "\n");
	}
	catch (Exception $e)
	{
		die($e);
	}
	$p = 0;

}

?>