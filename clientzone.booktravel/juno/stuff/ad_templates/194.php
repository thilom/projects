<?php
$ad="";

if(isset($edit))
{
	$scale=(isset($_GET["scale"])?$_GET["scale"]:6);
	$adW=$adWidth*$scale;
	$adH=($adHeight-3)*$scale;
	$ad.="<div class=box style=width:".$adW.";height:".$adH.";position:relative>";

	$ad.="<div id=tab style='position:absolute;left:0;top:0;width:".$adW.";font-size:12pt;padding:1 4 1 4;background:".$color_rgb."'>";
	$ad.="<div id=par style=height:20>";
	$ad.="<div class=rates style='float:right;margin:0 8 0 0'><span onclick=A_ed(this.nextSibling,'rates') onmouseover=\"J_TT(this,'Rates')\">Rates: </span><span onclick=A_ed(this,'rates')><span>".$rates."</span></span></div>";
	$ad.="<b id=region onclick=A_ed(this,'region')><span onmouseover=\"J_TT(this,'Region')\">".$region."</span></b> ";
	$ad.="<b id=town onclick=A_ed(this,'town') onmouseover=\"J_TT(this,'Town or suburb')\"><span>".$town."</span></b>";
	$ad.="</div>";
	$ad.="</div>";

	$ad.="<div id=par style='position:absolute;left:0;top:27;width:264;height:214;font-size:17px;line-height:100%;overflow:hidden'>";

	$ad.="<table width=100% cellspacing=0 cellpadding=0>";

	$ad.="<tr><td onclick=A_ed(this,'establishment_name') style='font-size:14pt;height:24;font-weight:bold;line-height:100%;position:static'><span class=est_name onmouseover=\"J_TT(this,'Establishment name')\">".$establishment_name."</span></td></tr>";

	$ad.="<tr><td style=font-size:12pt;line-height:110%;word-spacing:2 onclick=A_ql(this)>";

	$ad.="<img src=".$ROOT."/stuff/ad_templates/img/AA_rgb.png onclick=A_qal() onmouseover=\"J_TT(this,'Hide QA Logo')\" style='".($qa_logo?"":"display:none;")."height:50;float:left;margin:2 3 0 0;cursor:pointer' id=qalogo>";
	$ad.="<div".($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s" || $_SESSION["j_user"]["role"]=="b"?" class=con onclick=A_ed(this,'qa_text')":"").">";
	$ad.="<b class=qatext style=word-spacing:-2 onmouseover=\"J_TT(this,'QA Text')\">".($qa_text&&$qa_text!=" "?$qa_text:"?")."</b>";
	$ad.="</div> ";
	$ad.="<div style=text-align:justify>";
	$ad.="<span id=descr class=con onclick=A_ed(this,'description')><span onmouseover=\"J_TT(this,'Description')\">".($description&&$description!=" "?$description:"?")."</span></span>";
	$ad.=" <span id=star class=con".($stars?"":" style=display:none")."><span>".$stars."</span></span>";
	$ad.="</div>";
	$ad.="</td></tr></table>";
	$ad.="</div>";

	$ad.="<div id=img0 onclick=A_img(this) onmousedown=A_MV(this) style=position:absolute;top:30;right:0;width:".($img[0]["conWidth"]*$scale).";height:".($img[0]["conHeight"]*$scale).";overflow:hidden>";
	$ad.="<img src=".$img[0]["src"]." style=position:absolute;left:".($img[0]["left"]*$scale).";top:".($img[0]["top"]*$scale).";height:".($img[0]["height"]*$scale).">";
	$ad.="</div>";

	$ad.="<div class=con id=par onclick=A_ed(this,'address') style=position:absolute;left:0;bottom:0;height:40;width:".$adW.";text-align:center;font-size:13pt;font-weight:bold;line-height:110%;overflow:hidden onmouseover=\"J_TT(this,'Address line')\">";
	$ad.="<span class=addr>".$address."</span>";
	$ad.="</div>";

	$ad.="</div>";
}
else
{
	$adW=89/$points;
	$adH=50/$points;

	try
	{
		$p = new PDFlib();
		$p->set_parameter("logging", "filename=".$SDR."/stuff/ad_templates/logging/".preg_replace("~[^a-zA-Z0-9\-]~","",str_replace(array(" ","--"),"-",$pdf_title)).".log");
		$p->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");
		$p->set_parameter("textformat", "bytes");
		if($p->begin_document($pdf_filename,"")==0)
			die("Error: " . $p->get_errmsg());

		$p->set_info("Creator", "booktravel.travel");
		$p->set_info("Author", $pdf_author);
		$p->set_info("Title", $pdf_title);

		$p->begin_page_ext($adW, $adH, "");

		$p->set_parameter("FontOutline", "HelveticaCondensedBold=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p->load_font("HelveticaCondensedBold", "unicode", "embedding");
		$p->setfont($font, 24.0);
		$p->set_parameter("FontOutline", "HelveticaCondensed=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p->load_font("HelveticaCondensed", "unicode", "embedding");
		$p->setfont($font, 24.0);

		$c=explode(",",$color_cmyk);
		$p->setcolor("fill", "cmyk", $c[0], $c[1], $c[2], $c[3]);
		$p->rect(0, $adH-(3.5/$points), $adW, 3.5/$points);
		$p->fill();

		$region=($region&&$region!="?"?$region:"");
		$region.=($town&&$town!="?"?trim($town):"");
		if($region)
			$p->fit_textline($region, 1/$points, $adH-(1.8/$points), "position={left center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=7 fillcolor={cmyk ".$c[0]." ".$c[1]." ".$c[2]." 1} fontname=HelveticaCondensedBold encoding=unicode");

		$rates=($rates&&$rates!="?"?"Rates: ".$rates:"");
		if($rates)
			$p->fit_textline($rates, $adW-(1/$points), $adH-(1.8/$points), "position={right center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=7 fillcolor={cmyk ".$c[0]." ".$c[1]." ".$c[2]." 1}");

		$establishment_name=($establishment_name&&$establishment_name!="?"?$establishment_name:"");
		if($establishment_name)
			$p->fit_textline($establishment_name, 0, $adH-(5.8/$points), "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");

		$i=$p->load_image("auto", $SDR."/stuff/ad_templates/img/AA_cmyk.jpg", "");
		$p->fit_image($i, 0, $adH-(16.96/$points), "boxsize={16.5 23.6} fitmethod=entire position=center matchbox={name=img margin=-2}");
		$p->close_image($i);

		$t=$p->create_textflow(($qa_text?trim($qa_text)."\n":""), "alignment=justify kerning=true charspacing=-0.3 wordspacing=-0.3 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");
		$t=$p->add_textflow($t, $description, "adjustmethod=split alignment=justify kerning=true fontsize=8 kerning=true charspacing=-0.1 wordspacing=-0.3 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
		$p->fit_textflow($t, 0, $adH-(8/$points), 43/$points, 10/$points, "verticalalign=justify linespreadlimit=115% wrap={usematchboxes={{img}}}");

		$i=img4Pdf($img[0]["hirez"],$img[0]["web"],$img[0]["conWidth"],$img[0]["conHeight"],$img[0]["width"],$img[0]["height"],$img[0]["left"],$img[0]["top"],$sharpen);
		$i=$p->load_image("auto", $i, "");
		$p->fit_image($i, 44.5/$points, $adH-(($img[0]["conHeight"]+4.75+1.6)/$points), "boxsize={".(($img[0]["conWidth"]+1.6)/$points)." ".(($img[0]["conHeight"]+1.6)/$points)."} position={center} fitmethod=entire");
		$p->close_image($i);

		$address=($address&&$address!="?"?str_replace($ROOT."/stuff/ad_templates/img/icons_rgb/",$SDR."/stuff/ad_templates/img/icons_cmyk/",$address):"");
		if($address)
		{
			$opt="";
			$ico=array();
			preg_match_all("~<img src=(.*?)>~", $address, $m);
			if(isset($m[1]) && count($m[1]))
			{
				$opt.="macro {
					";
				$m=$m[1];
				foreach($m as $k => $v)
				{
					$kk=$k+1;
					$address=preg_replace("~<img src=".$v.">~", "<&icon".$kk."><&end>", $address, 1);
					$ico[]=$v;
					$opt.="icon".$kk." {matchbox={name=icon".$kk." boxwidth=5 boxheight={ascender descender} offsettop=1}}
					";
				}
				$opt.="end {matchbox={end}}
					";
				$opt.="}
					";
			}
			$opt.="alignment=center kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7.5 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode";
			$address=preg_replace("<img src=.*?>", "", $address);
			$t=$p->create_textflow($address, $opt);
			$p->fit_textflow($t, 0, 2/$points, $adW, 8.2/$points, "verticalalign=justify linespreadlimit=110%");
			foreach($ico as $k => $v)
			{
				$kk=$k+1;
				$n="icon".$kk;
				$v = $p->load_image("auto", $v, "");
				$x1 = $p->info_matchbox($n, $kk, "x1");
				$y1 = $p->info_matchbox($n, $kk, "y1");
				$width = $p->info_matchbox($n, $kk, "width");
				$height = $p->info_matchbox($n, $kk, "height");
				$p->fit_image($v, $x1, $y1, "boxsize={".$width." ".$height."} fitmethod=entire position=center");
			}
		}

		$p->end_page_ext("");
		$p->end_document("");
	}
	catch (PDFlibException $e)
	{
		die("PDFlib exception occurred in hello sample:\n" .
		"[" . $e->get_errnum() . "] " . $e->get_apiname() . ": " .
		$e->get_errmsg() . "\n");
	}
	catch (Exception $e)
	{
		die($e);
	}
	$p = 0;

}

?>