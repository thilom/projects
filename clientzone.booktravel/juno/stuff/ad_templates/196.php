<?php
$ad="";

if(isset($edit))
{
	$scale=(isset($_GET["scale"])?$_GET["scale"]:6);
	$adW=$adWidth*$scale;
	$adH=($adHeight-3)*$scale;
	$ad.="<div class=box style=width:".$adW.";height:".$adH.";position:relative>";

	$ad.="<div onclick=A_ed(this,'establishment_name') style='font-size:14pt;height:24;font-weight:bold;line-height:100%;position:static'><span class=est_name onmouseover=\"J_TT(this,'Establishment name')\">".$establishment_name."</span></div>";

	$ad.="<span class=rooms style='position:absolute;left:right;top:0;padding:0 8 0 0'><span onclick=A_ed(this,'rooms')><span>".$rooms."</span></span>";

	$ad.="<span class=rates style='position:absolute;left:right;top:0;padding:2 8 2 8;display:block;background:#777;font-style:italic'><span onclick=A_ed(this,'rates')><span>".$rates."</span></span>";

	$ad.="<div id=img0 onclick=A_img(this) onmousedown=A_MV(this) style='margin:8 0 0 0;width:".($img[0]["conWidth"]*$scale).";height:".($img[0]["conHeight"]*$scale).";overflow:hidden'>";
	$ad.="<img src=".$img[0]["src"]." style=position:absolute;left:".($img[0]["left"]*$scale).";top:".($img[0]["top"]*$scale).";height:".($img[0]["height"]*$scale).">";
	$ad.="</div>";

	$ad.="<div id=par style='position:absolute;left:0;top:27;width:264;height:214;font-size:17px;line-height:100%;overflow:hidden'>";

	$ad.="<table width=100% cellspacing=0 cellpadding=0>";

	$ad.="<tr><td style=font-size:12pt;line-height:110%;word-spacing:2 onclick=A_ql(this)>";

	$ad.="<img src=".$ROOT."/stuff/ad_templates/img/AA_rgb.png onclick=A_qal() onmouseover=\"J_TT(this,'Hide QA Logo')\" style='".($qa_logo?"":"display:none;")."height:50;float:left;margin:2 3 0 0;cursor:pointer' id=qalogo>";
	$ad.="<div".($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s" || $_SESSION["j_user"]["role"]=="b"?" class=con onclick=A_ed(this,'qa_text')":"").">";
	$ad.="<b class=qatext style=word-spacing:-2 onmouseover=\"J_TT(this,'QA Text')\">".($qa_text&&$qa_text!=" "?$qa_text:"?")."</b>";
	$ad.="</div> ";
	$ad.="<div style=text-align:justify>";
	$ad.="<span id=descr class=con onclick=A_ed(this,'description')><span onmouseover=\"J_TT(this,'Description')\">".($description&&$description!=" "?$description:"?")."</span></span>";
	$ad.=" <span id=star class=con".($stars?"":" style=display:none")."><span>".$stars."</span></span>";
	$ad.="</div>";
	$ad.="</td></tr></table>";
	$ad.="</div>";

	$ad.="<div class=con id=par onclick=A_ed(this,'address') style=position:absolute;left:0;bottom:0;height:36;width:".$adW.";text-align:center;font-size:11pt;word-spacing:-1;font-weight:bold;line-height:110%;overflow:hidden onmouseover=\"J_TT(this,'Address line')\">";
	$ad.="<span id=addr>".$address."</span>";
	$ad.="</div>";

	$ad.="</div>";
}
else
{
	$mm=0.352777778;
	$adW=89/$mm;
	$adH=50/$mm;
	include $SDR."/apps/ad_manager/f/img_processor.php";
	$s=isset($sharpen)?1:0;

	$ad.="<html>";
	$ad.="<body style='margin:0;font-family:helvetica-condensed;color:cmyk(0,0,0,1)'>";

	$ad.="<table style='width:".$adW.";border-collapse:collapse'>";
	$ad.="<tr>";
	$ad.="<td style='padding:0.3 0 -1.6 2;font-size:7pt;background-color:cmyk(".$color_cmyk.");color:cmyk(".$color_k.")'><b>";
	$ad.=($region&&$region!="?"?$region:"");
	$ad.=($town&&$town!="?"?trim($town):"");
	$ad.="</b></td>";
	$ad.="<td style='padding:0.3 2 -1.6 0;text-align:right;font-size:7pt;background-color:cmyk(".$color_cmyk.");color:cmyk(".$color_k.")'>";
	$ad.=($rates&&$rates!="?"?"Rates: ".$rates:"&nbsp;");
	$ad.="</td>";
	$ad.="</tr>";
	$ad.="</table>";

	$ad.="<table style='width:".$adW.";border-collapse:collapse'>";
	$ad.="<tr>";

	$ad.="<td style='width:50%;padding:0;font-size:8pt;vertical-align:top;line-height:86%'>";
	$ad.="<div style='width:100%;padding:3 0 3 0;font-size:9pt'><b>".($establishment_name&&$establishment_name!="?"?$establishment_name:"")."</b></div>";
	$ad.="<div style='text-align:justify'>";
	$ad.=($qa_logo?"<img src=".$SDR."/stuff/ad_templates/img/AA_cmyk.jpg style='height:32;float:left;margin:1 1.5 0 0'>":"");
	$ad.=($qa_text&&$qa_text!="?"?"<div style='font-weight:bold;text-align:left'>".$qa_text."</div>":"");
	$ad.=($description&&$description!="?"?$description:"");
	$ad.=($stars?" ".str_replace($ROOT."/stuff/ad_templates/img/star_rgb.jpg",$SDR."/stuff/ad_templates/img/star_cmyk.jpg style='height:10;margin:1 0 0 0'",$stars):"");
	$ad.="</div>";
	$ad.="</td>";

	$ad.="<td style='padding:5.4 0 0 0;text-align:right'><img src='".img4Pdf($img[0]["hirez"],$img[0]["web"],$img[0]["conWidth"],$img[0]["conHeight"],$img[0]["width"],$img[0]["height"],$img[0]["left"],$img[0]["top"],$sharpen)."' style='width:".($img[0]["conWidth"]*3.76).";height:".($img[0]["conHeight"]*3.76)."'></td>";

	$ad.="</tr></table>";

	$ad.="<div style='width:".$adW.";text-align:center;word-spacing:-0.5;font-size:8pt;line-height:86%;color:cmyk(0,0,0,1);font-weight:bold;padding:3.5 0 0 0'>".($address&&$address!="?"?str_replace(">"," style='height:8;margin:2 0 0 0'>",str_replace($ROOT."/stuff/ad_templates/img/icons_rgb/",$SDR."/stuff/ad_templates/img/icons_cmyk/",$address)):"")."</div>";

	//$ad.="<div style='width:".$adW.";height:".(3/$mm).";background-color:#DDDDDD'></div>";

	$ad.="</body></html>";
}

?>