<?php
// SA BEST
// quarter page ad
$ad="";
if(isset($edit))
{
	if (empty($qa_text)) $qa_text = '?';
	$scale=(isset($_GET["scale"])?$_GET["scale"]:6);
	$adW=$adWidth*$scale;
	$adH=$adHeight*$scale;
	$ad.="<div class=box style=width:".$adW.";height:".$adH.";position:relative>";

	$ad.="<div id=tab style='position:absolute;left:0;top:0;width:".$adW.";font-size:12pt;padding:1 4 1 4;background:".$color_rgb."'>";
	$ad.="<div id=par style=height:20>";
	$ad.="<b onclick=A_ed(this,'establishment_name') style='font-size:14pt;height:24;font-weight:bold;line-height:100%;position:static'><span class=est_name onmouseover=\"J_TT(this,'Establishment name')\">".$establishment_name."</span></b>";
	$ad.="<span style=float:right>";
	$ad.="<span id=region onclick=A_ed(this,'region')><span onmouseover=\"J_TT(this,'Region')\">".$region."</span></span> ";
	$ad.="<span id=town onclick=A_ed(this,'town') onmouseover=\"J_TT(this,'Town or suburb')\"><span style='margin:0 8 0 0'>".$town."</span></span>";
	$ad.="</span>";
	$ad.="</div>";
	$ad.="</div>";

	if($side==0) // left
	{
		$ad.="<span onclick=A_ql(this.previousSibling)>";
		$ad.="<div id=rates style='position:absolute;left:0;top:30;padding:1 4;width:247;font-size:11pt;font-style:italic;background:".$color_rgb."'><span onclick=A_ed(this,'rates')><b>".($rates&&$rates!=" "?$rates:"?")."</b></span></div>";
		$ad.="<div class=award style='position:absolute;left:0;top:54;width:200;font-size:11pt'><span onclick=A_ed(this,'award')><b>".($award?$award:"?")."</b></span></div>";
		$ad.="<div id=star style=position:absolute;left:0;top:74;width:200;".($stars?"":";display:none")."><span>".$stars."</span></div>";
		$ad.="<div id=img0 onclick=A_img(this) onmousedown=A_MV(this) style=position:absolute;bottom:0;left:0;width:".($img[0]["conWidth"]*$scale).";height:".($img[0]["conHeight"]*$scale).";overflow:hidden>";
		$ad.="<img src=".$img[0]["src"]." style=position:absolute;left:".($img[0]["left"]*$scale).";top:".($img[0]["top"]*$scale).";height:".($img[0]["height"]*$scale).">";
		$ad.="</div>";
		$ad.="</span>";

		$ad.="<div id=par onclick=A_ql(this) style='position:absolute;right:0;top:28;width:412;height:266;font-size:17px;line-height:115%;overflow:hidden;font-size:12pt;word-spacing:2'>";
		$ad.="<img src=".$ROOT."/stuff/ad_templates/img/AA_rgb.png onclick=A_qal() onmouseover=\"J_TT(this,'Hide QA Logo')\" style='".($qa_logo?"":"display:none;")."height:50;float:left;margin:2 3 0 0;cursor:pointer' id=qalogo>";
		$ad.="<div style=text-align:justify>";
		$ad.="<span"." class=con onclick=A_ed(this,'qa_text')"."><b class=qatext style=word-spacing:-2 onmouseover=\"J_TT(this,'QA Text')\">".$qa_text."</b></span> ";
		$ad.="<span id=descr class=con onclick=A_ed(this,'description')><span onmouseover=\"J_TT(this,'Description')\">".($description&&$description!=" "?$description:"?")."</span></span>";
		$ad.="<div id=addr class=con onclick=A_ed(this,'address') style=width:100%;font-size:11pt;word-spacing:-1;text-align:left><span onmouseover=\"J_TT(this,'Address')\" style=text-align:left>".($address?$address:"?")."</span></div>";
		$ad.="<div id=icons onclick=A_ed(this,'icons') onmouseover=\"J_TT(this,'Icons')\"><span>".($icon_img?$icon_img:"?")."</span></div>";
		$ad.="</div>";
		$ad.="</td></tr></table>";
		$ad.="</div>";
	}
	else
	{
		$ad.="<div id=par onclick=A_ql(this) style='position:absolute;left:0;top:28;width:412;height:266;font-size:17px;line-height:115%;overflow:hidden;font-size:12pt;word-spacing:2'>";
		$ad.="<img src=".$ROOT."/stuff/ad_templates/img/AA_rgb.png onclick=A_qal() onmouseover=\"J_TT(this,'Hide QA Logo')\" style='".($qa_logo?"":"display:none;")."height:50;float:left;margin:2 3 0 0;cursor:pointer' id=qalogo>";
		$ad.="<div style=text-align:justify>";
		$ad.="<span".($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s" || $_SESSION["j_user"]["role"]=="b"?" class=con onclick=A_ed(this,'qa_text')":"")."><b class=qatext style=word-spacing:-2 onmouseover=\"J_TT(this,'QA Text')\">".$qa_text."</b></span> ";
		$ad.="<span id=descr class=con onclick=A_ed(this,'description')><span onmouseover=\"J_TT(this,'Description')\">".($description&&$description!=" "?$description:"?")."</span></span>";
		$ad.="<div id=addr class=con onclick=A_ed(this,'address') style=width:100%;font-size:11pt;word-spacing:-1;text-align:left><span onmouseover=\"J_TT(this,'Address')\" style=text-align:left>".($address?$address:"?")."</span></div>";
		$ad.="<div id=icons onclick=A_ed(this,'icons') onmouseover=\"J_TT(this,'Icons')\"><span onmouseover=\"J_TT(this,'Icons')\">".($icon_img?$icon_img:"?")."</span></div>";
		$ad.="</div>";
		$ad.="</td></tr></table>";
		$ad.="</div>";

		$ad.="<div onclick=A_ql(this.previousSibling)>";
		$ad.="<div id=rates style='position:absolute;right:0;top:30;padding:1 4;width:247;text-align:right;font-size:11pt;font-style:italic;background:".$color_rgb."'><span onclick=A_ed(this,'rates')><b>".($rates&&$rates!=" "?$rates:"?")."</b></span></div>";
		$ad.="<div class=award style='position:absolute;right:0;top:54;font-size:11pt;text-align:right'><span onclick=A_ed(this,'award')><b>".($award?$award:"?")."</b></span></div>";
		$ad.="<div id=star style=position:absolute;right:0;top:74;text-align:right;".($stars?"":";display:none")."><span>".$stars."</span></div>";
		$ad.="<div id=img0 onclick=A_img(this) onmousedown=A_MV(this) style=position:absolute;bottom:0;right:0;width:".($img[0]["conWidth"]*$scale).";height:".($img[0]["conHeight"]*$scale).";overflow:hidden>";
		$ad.="<img src=".$img[0]["src"]." style=position:absolute;left:".($img[0]["left"]*$scale).";top:".($img[0]["top"]*$scale).";height:".($img[0]["height"]*$scale).">";
		$ad.="</div>";
		$ad.="</div>";
	}

	$ad.="</div>";
}
else
{

	list($k1, $k2,$k3, $k4) = explode(',', $color_cmyk);
	$adW = 114 / $points;
	$adH = 49 / $points;
	try {
		$p = new PDFlib();
		$p->set_parameter("logging", "filename=" . $SDR . "/stuff/ad_templates/logging/" . preg_replace("~[^a-zA-Z0-9\-]~", "", str_replace(array(" ", "--"), "-", $pdf_title)) . ".log");

		$p->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");

		$p->set_parameter("textformat", "bytes");
		$p->set_parameter("SearchPath", $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/color_profiles');
		if ($p->begin_document($pdf_filename, "") == 0)
			die("Error: " . $p->get_errmsg());

		$p->set_info("Creator", "booktravel.travel");
		$p->set_info("Author", $pdf_author);
		$p->set_info("Title", $pdf_title);

		$p->begin_page_ext($adW, $adH, "");

		$p->set_parameter("FontOutline", "HelveticaCondensedBold=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p->load_font("HelveticaCondensedBold", "unicode", "embedding");
		$p->setfont($font, 24.0);
		$p->set_parameter("FontOutline", "HelveticaCondensed=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p->load_font("HelveticaCondensed", "unicode", "embedding");
		$p->setfont($font, 24.0);

		$c = explode(",", $color_cmyk);
		$p->setcolor("fill", "cmyk", $c[0], $c[1], $c[2], $c[3]);

		//Establishment Name
		$establishment_name = ($establishment_name && $establishment_name != "?" ? $establishment_name : "");
		$h = 0;
		if ($establishment_name) {
			$p->setcolor("fill", "cmyk", $k1, $k2, $k3, $k4);
			$p->rect(0,$adH -10,$adW,10);
			$p->fill();

			$establishment_line = explode('<br>', $establishment_name);
			foreach ($establishment_line as $line) {
				$yPos = ($adH - (1.8 / $points)) - $h;
				$p->fit_textline($line, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");
				$h += 8;
			}
		}
		$h -= 8;

		//Region
		$region1 = '';
		$region1.=($region&&$region!="?"?$region:"");
		$region1.=($town&&$town!="?"?$town:"");
		$yPos = ($adH - (1.8 / $points));
		$p->fit_textline($region1, $adW, $yPos, "position={right center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");


		//Rates
		if (isset($rates) && $rates == '?') $rates = '';
		if ($rates) {
			$p->setcolor("fill", "cmyk", $k1, $k2, $k3, $k4);
			$p->rect(0,$adH-$h-24.5,125,12);
			$p->fill();

			$p->fit_textline($rates, 0, $adH-$h-18, "position={left center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=9 fillcolor={cmyk 1 1 1 0}  fontname=HelveticaCondensedBold encoding=unicode");
		}

		//Awards
		if (isset($award) && $award == '?') $award = '';
		if (!empty($award)) {
			$yPos = ($adH - (7.4 / $points)) - $h -8;
//			$h += 8;
			$p->fit_textline($award, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
		}
		$h -= 8;


		//Stars
		$star_description = '';
		for ($x=0; $x<$_POST['star_grading']; $x++) {
			$i = $p->load_image("auto", $SDR . "/stuff/ad_templates/img/star_cmyk.jpg", "");
			$p->fit_image($i, $x*8.2, ($adH - (5.5 / $points)) - $h-35, "boxsize={8 8} fitmethod=entire position=center matchbox={name=img margin=-2}");
			$p->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		//Image
//		$i = 'Bastion_Guest_House.tif';
		$i = img4Pdf($img[0]["hirez"], $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);
//		$icchandle = $p->load_iccprofile("USWebCoatedSWOP", "usage=iccbased");
//		if ($icchandle == 0) throw new Exception("Error: " . $p->get_errmsg());
//		$i = $p->load_image("auto", $i, "iccprofile=" . $icchandle .  " honoriccprofile=false");
//		if ($i == 0) throw new Exception("Error: " . $p->get_errmsg());
		$i = $p->load_image("auto", $i, "");
		$p->fit_image($i, 0, ($adH-$h-30 - (($img[0]["conHeight"] + 4.75 + 5.5) / $points)), "boxsize={" . (($img[0]["conWidth"] + 1.6) / $points) . " " . ((($img[0]["conHeight"] + 1.6) / $points)-10). "} position={center} fitmethod=entire");
		$p->close_image($i);

		//Prepare statement - get Icon data
		$statement = "SELECT icon_url
					FROM nse_icon
					WHERE icon_id=?
					LIMIT 1";
		$sql_icon = $GLOBALS['dbCon']->prepare($statement);

		$icon_list = explode('~', $icons);

		foreach ($icon_list as $k=>$v) {
			if (empty($v)) unset($icon_list[$k]);
		}

		$icon_counter = 0;
		if (count($icon_list) >= 23) {
			$address_bottom = 16;
			$icon_line_pos = 46;
			$footer_space = 11;
		} else {
			$address_bottom = 9;
			$icon_line_pos = 49;
			$footer_space = 6;
		}
		foreach ($icon_list as $icon_id) {
			if (empty($icon_id)) continue;
			$sql_icon->bind_param('i', $icon_id);
			$sql_icon->execute();
			$sql_icon->bind_result($icon_url);
			$sql_icon->fetch();
			$sql_icon->free_result();

			if (!empty($icon_url)) {
				$icon_file = substr($icon_url, strrpos($icon_url, '/')+1);
				$icon_file = substr($icon_file, 0, strrpos($icon_file, '.'));
				$icon_file = "/stuff/ad_templates/img/icons_cmyk/$icon_file.jpg";

				$i = $p->load_image("auto", $SDR . $icon_file, "");
				$p->fit_image($i, $icon_counter*(3.0/$points)+130, ($adH - ($icon_line_pos / $points)) , "boxsize={". 2.7/$points . " ". 2.7/$points . "} fitmethod=entire position=center matchbox={name=img2 margin=-2}");
				$p->close_image($i);
				$img_wrap = 'wrap={usematchboxes={{img2}}}';
			}

			if ($icon_counter == 21) {
				$icon_counter = 0;
				$icon_line_pos = 49;
			} else {
				$icon_counter++;
			}
		}

		//Address
		$address = ($address && $address != "?" ? str_replace($ROOT . "/stuff/ad_templates/img/icons_rgb/", $SDR . "/stuff/ad_templates/img/icons_cmyk/", $address) : "");
		$address = str_replace('&#x00B7;', chr(183), $address);
		$address = str_replace('&nbsp;', ' ', $address);

		if ($address) {

			$address_line = explode('<br>',$address);
			$line_total = count($address_line);
			$footer_space += $line_total*$line_total;
			$counter2 = 1;
			foreach ($address_line as $counter=>$line) {

				$opt = "";
				$ico = array();

				preg_match_all("~<img src=(.*?)>~", $line, $m);

				if (isset($m[1]) && count($m[1])) {
					$opt.="macro {
						";
					$m = $m[1];

					foreach ($m as $k => $v) {
						if (empty($v)) continue;
						$kk = $counter2;
						$line = preg_replace("~<img src=" . $v . ">~", "<&icon" . $kk . "><&end>", $line, 1);
						$ico[$kk] = $v;
						$opt.="icon" . $kk . " {matchbox={name=icon" . $kk . " boxwidth=6 boxheight={4 0} offsettop=2}}
						";
						$counter2++;
					}
					$opt.="end {matchbox={end}}
						";
					$opt.="}
						";
				}

				$opt.=" kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode";
				$line = preg_replace("<img src=.*?/>", "", $line);
				$t = $p->create_textflow($line, $opt);
				$p->fit_textflow($t, 130, $address_bottom, $adW, ($address_bottom + (($line_total-$counter)*8)), "linespreadlimit=110%");
				foreach ($ico as $k => $v) {
					$kk = $k ;
					$n = "icon" . $kk;
					$v1 = $p->load_image("auto", $v, "");
					$x1 = $p->info_matchbox($n, 1,"x1");
					$y1 = $p->info_matchbox($n, 1, "y1");
					$width = $p->info_matchbox($n, 1, "width");
					$height = $p->info_matchbox($n, 1, "height");
					$p->fit_image($v1, $x1, $y1, "boxsize={".$width." ".$height."} fitmethod=entire position=center");
				}

			}
		}

		//Description
		$img_wrap = '';
		if ($_POST['qa_logo']!=0) {
			$i = $p->load_image("auto", $SDR . "/stuff/ad_templates/img/AA_cmyk.jpg", "");
			$p->fit_image($i, 130, ($adH - (12 / $points)) - $h -8, "boxsize={14.5 21.6} fitmethod=entire  matchbox={name=img margin=-2 boxheight={descender ascender} createwrapbox}");
			$p->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		$description = "<adjustmethod=split alignment=justify kerning=true fontsize=8 charspacing=-0.1 wordspacing=-0.3 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode>$description";

		$body_height =  $footer_space;
		$description = str_replace('<br>', "\n", $description);
		$qa_text = str_replace('<br>', "\n", $qa_text);
		$qa_text = str_replace('&nbsp;', " ", $qa_text);
		$t = $p->create_textflow(($qa_text ? trim($qa_text) . " $description" : " $description"), " adjustmethod=split alignment=justify kerning=true fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode ");
		$p->fit_textflow($t, 130, $adH - 10, $adW, $body_height + 22, "  $img_wrap verticalalign=justify");
		$i = img4Pdf($img[0]["hirez"], $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);

		$p->end_page_ext("");
		$p->end_document("");
	} catch (PDFlibException $e) {
		if ($e->get_errnum() == 1118) {
			echo "<div style='font-weight: none; color: red; background-color: silver; border: 1px dotted gray; width: 90%; margin: auto auto; padding: 10px'>
					<b>The uploaded image appears to be damaged.</b><br>
					Please open in an external application such as photoshop, fix and re-upload.<br>
					Error: {$e->get_errmsg()}
					</div>";
			die();
		} else {
			die("PDFlib exception occurred in 223.php:\n" .
					"[" . $e->get_errnum() . "] " . $e->get_apiname() . ":: {$e->get_errmsg()}");
		}
	} catch (Exception $e) {
		var_dump($e);
	}


	//PDF Low Res
	try {
		$p_low = new PDFlib();
		$p_low->set_parameter("logging", "filename=" . $SDR . "/stuff/ad_templates/logging/" . preg_replace("~[^a-zA-Z0-9\-]~", "", str_replace(array(" ", "--"), "-", $pdf_title)) . ".log");
		$p_low->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");
		$p_low->set_parameter("textformat", "bytes");
		$p_low->set_parameter("SearchPath", $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/color_profiles');
		$pdf_low_filename = str_replace('.pdf', '_low.pdf', $pdf_filename);
		if ($p_low->begin_document($pdf_low_filename, "") == 0)
			die("Error: " . $p_low->get_errmsg());

		$p_low->set_info("Creator", "booktravel.travel");
		$p_low->set_info("Author", $pdf_author);
		$p_low->set_info("Title", $pdf_title);

		$p_low->begin_page_ext($adW, $adH, "");

		$p_low->set_parameter("FontOutline", "HelveticaCondensedBold=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p_low->load_font("HelveticaCondensedBold", "unicode", "embedding");
		$p_low->setfont($font, 24.0);
		$p_low->set_parameter("FontOutline", "HelveticaCondensed=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p_low->load_font("HelveticaCondensed", "unicode", "embedding");
		$p_low->setfont($font, 24.0);

		$c = explode(",", $color_cmyk);
		$p_low->setcolor("fill", "cmyk", $c[0], $c[1], $c[2], $c[3]);

		//Establishment Name
		$establishment_name = ($establishment_name && $establishment_name != "?" ? $establishment_name : "");
		$h = 0;
		if ($establishment_name) {
			$p_low->setcolor("fill", "cmyk", $k1, $k2, $k3, $k4);
			$p_low->rect(0,$adH -10,$adW,10);
			$p_low->fill();

			$establishment_line = explode('<br>', $establishment_name);
			foreach ($establishment_line as $line) {
				$yPos = ($adH - (1.8 / $points)) - $h;
				$p_low->fit_textline($line, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");
				$h += 8;
			}
		}
		$h -= 8;

		//Region
		$region1 = '';
		$region1.=($region&&$region!="?"?$region:"");
		$region1.=($town&&$town!="?"?$town:"");
		$yPos = ($adH - (1.8 / $points));
		$p_low->fit_textline($region1, $adW, $yPos, "position={right center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");


		//Rates
		if (isset($rates) && $rates == '?') $rates = '';
		if ($rates) {
			$p_low->setcolor("fill", "cmyk", $k1, $k2, $k3, $k4);
			$p_low->rect(0,$adH-$h-24.5,125,12);
			$p_low->fill();

			$p_low->fit_textline($rates, 0, $adH-$h-18, "position={left center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=9 fillcolor={cmyk 1 1 1 0}  fontname=HelveticaCondensedBold encoding=unicode");
		}

		//Awards
		if (isset($award) && $award == '?') $award = '';
		if (!empty($award)) {
			$yPos = ($adH - (7.4 / $points)) - $h -8;
//			$h += 8;
			$p_low->fit_textline($award, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
		}
		$h -= 8;


		//Stars
		$star_description = '';
		for ($x=0; $x<$_POST['star_grading']; $x++) {
			$i = $p_low->load_image("auto", $SDR . "/stuff/ad_templates/img/star_cmyk.jpg", "");
			$p_low->fit_image($i, $x*8.2, ($adH - (5.5 / $points)) - $h-35, "boxsize={8 8} fitmethod=entire position=center matchbox={name=img margin=-2}");
			$p_low->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		//Image
		//Resize image
		$low_file_name = substr($img[0]["hirez"], 0, strrpos($img[0]["hirez"],'.')) . '_low.jpg';
		$low_file = $_SERVER['DOCUMENT_ROOT'] . "/hires_images/{$low_file_name}";
		$high_file = $_SERVER['DOCUMENT_ROOT'] . '/hires_images/' . $img[0]["hirez"];
		list($current_width, $current_height, $image_format) = getimagesize($high_file);
		if ($current_width > $current_height) {
			$ratio = 600/$current_width;
		} else {
			$ratio = 600/$current_height;
		}
		$low_width = (int) ($current_width*$ratio);
		$low_height = (int) ($current_height*$ratio);
		$low_image = imagecreatetruecolor($low_width, $low_height);
		$current_image = '';
		switch($image_format) {
			case IMAGETYPE_TIFF_II:
			case IMAGETYPE_TIFF_MM:
				$current_image = imagecreatefromtiff($high_file);
				break;
			case IMAGETYPE_JPEG:
				$current_image = imagecreatefromjpeg($high_file);
				break;
			case IMAGETYPE_GIF:
				$current_image = imagecreatefromgif($high_file);
				break;
			case IMAGETYPE_PNG:
				$current_image = imagecreatefrompng($high_file);
				break;
			case IMAGETYPE_WBMP:
				$current_image = imagecreatefromwbmp($high_file);
				break;
		}
		imagecopyresized($low_image, $current_image, 0, 0, 0, 0, $low_width, $low_height, $current_width, $current_height);
		imagejpeg($low_image, $low_file,80);
		imagedestroy($low_image);
		imagedestroy($current_image);
		$i = img4Pdf($low_file_name, $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);
//		$icchandle = $p_low->load_iccprofile("USWebCoatedSWOP", "usage=iccbased");
//		if ($icchandle == 0)
//			 throw new Exception("Error: " . $p_low->get_errmsg());
//		$i = $p_low->load_image("auto", $i, "iccprofile=" . $icchandle .  " honoriccprofile=false");
//		if ($i == 0)
//			throw new Exception("Error: " . $p_low->get_errmsg());
		$i = $p_low->load_image("auto", $i, "");
		$p_low->fit_image($i, 0, ($adH-$h-30 - (($img[0]["conHeight"] + 4.75 + 5.5) / $points)), "boxsize={" . (($img[0]["conWidth"] + 1.6) / $points) . " " . ((($img[0]["conHeight"] + 1.6) / $points)-10). "} position={center} fitmethod=entire");
		$p_low->close_image($i);

		//Prepare statement - get Icon data
		$statement = "SELECT icon_url
					FROM nse_icon
					WHERE icon_id=?
					LIMIT 1";
		$sql_icon = $GLOBALS['dbCon']->prepare($statement);

		$icon_list = explode('~', $icons);

		foreach ($icon_list as $k=>$v) {
			if (empty($v)) unset($icon_list[$k]);
		}

		$icon_counter = 0;
		if (count($icon_list) >= 23) {
			$address_bottom = 16;
			$icon_line_pos = 46;
			$footer_space = 11;
		} else {
			$address_bottom = 9;
			$icon_line_pos = 49;
			$footer_space = 6;
		}
		foreach ($icon_list as $icon_id) {
			if (empty($icon_id)) continue;
			$sql_icon->bind_param('i', $icon_id);
			$sql_icon->execute();
			$sql_icon->bind_result($icon_url);
			$sql_icon->fetch();
			$sql_icon->free_result();

			if (!empty($icon_url)) {
				$icon_file = substr($icon_url, strrpos($icon_url, '/')+1);
				$icon_file = substr($icon_file, 0, strrpos($icon_file, '.'));
				$icon_file = "/stuff/ad_templates/img/icons_rgb/$icon_file.jpg";

				$i = $p_low->load_image("auto", $SDR . $icon_file, "");
				$p_low->fit_image($i, $icon_counter*(3.0/$points)+130, ($adH - ($icon_line_pos / $points)) , "boxsize={". 2.7/$points . " ". 2.7/$points . "} fitmethod=entire position=center matchbox={name=img2 margin=-2}");
				$p_low->close_image($i);
				$img_wrap = 'wrap={usematchboxes={{img2}}}';
			}

			if ($icon_counter == 21) {
				$icon_counter = 0;
				$icon_line_pos = 49;
			} else {
				$icon_counter++;
			}
		}

		//Address
		$address = ($address && $address != "?" ? str_replace($ROOT . "/stuff/ad_templates/img/icons_rgb/", $SDR . "/stuff/ad_templates/img/icons_cmyk/", $address) : "");
		$address = str_replace('&#x00B7;', chr(183), $address);
		$address = str_replace('&nbsp;', ' ', $address);

		if ($address) {

			$address_line = explode('<br>',$address);
			$line_total = count($address_line);
			$footer_space += $line_total*$line_total;
			$counter2 = 1;
			$counter3 = 0;
			foreach ($address_line as $counter=>$line) {

				$opt = "";
				$ico = array();

				preg_match_all("~<img src=(.*?)>~", $line, $m);

				if (isset($m[1]) && count($m[1])) {
					$opt.="macro {";
					$m = $m[1];

					foreach ($m as $k => $v) {
						if (empty($v)) continue;
						$kk = $counter2;
						$line = preg_replace("~<img src=" . $v . ">~", "<&icon" . $kk . "><&end>", $line, 1);
						$ico[$kk] = $v;
						$opt.="icon" . $kk . " {matchbox={name=icon" . $kk . " boxwidth=6 boxheight={4 0} offsettop=2}}
";
						$counter2++;
					}
					$opt.="end {matchbox={end}}
";
					$opt.="}
";
				}

				$opt.=" kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode";
				$line = preg_replace("<img src=.*?/>", "", $line);
				$t = $p_low->create_textflow($line, $opt);
				$p_low->fit_textflow($t, 130, $address_bottom, $adW, ($address_bottom + (($line_total-$counter3)*8)), "linespreadlimit=110%");
				foreach ($ico as $k => $v) {
					$kk = $k ;
					$n = "icon" . $kk;
					$v1 = $p_low->load_image("auto", $v, "");
					$x1 = $p_low->info_matchbox($n, 1,"x1");
					$y1 = $p_low->info_matchbox($n, 1, "y1");
					$width = $p_low->info_matchbox($n, 1, "width");
					$height = $p_low->info_matchbox($n, 1, "height");
					$p_low->fit_image($v1, $x1, $y1, "boxsize={".$width." ".$height."} fitmethod=entire position=center");
				}
				$counter3++;
			}
		}

		//Description
		$img_wrap = '';
		if ($_POST['qa_logo']!=0) {
			$i = $p_low->load_image("auto", $SDR . "/stuff/ad_templates/img/AA_cmyk.jpg", "");
			$p_low->fit_image($i, 130, ($adH - (12 / $points)) - $h -8, "boxsize={14.5 21.6} fitmethod=entire  matchbox={name=img margin=-2 boxheight={descender ascender} createwrapbox}");
			$p_low->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		$description = "<adjustmethod=split alignment=justify kerning=true fontsize=8 charspacing=-0.1 wordspacing=-0.3 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode>$description";

		$body_height =  $footer_space;
		$description = str_replace('<br>', "\n", $description);
		$qa_text = str_replace('<br>', "\n", $qa_text);
		$qa_text = str_replace('&nbsp;', " ", $qa_text);
		$t = $p_low->create_textflow(($qa_text ? trim($qa_text) . " $description" : " $description"), " adjustmethod=split alignment=justify kerning=true fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode ");
		$p_low->fit_textflow($t, 130, $adH - 10, $adW, $body_height + 22, "  $img_wrap verticalalign=justify");
		$i = img4Pdf($img[0]["hirez"], $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);

		$p_low->end_page_ext("");
		$p_low->end_document("");
	} catch (PDFlibException $e) {
		if ($e->get_errnum() == 1118) {
			echo "<div style='font-weight: none; color: red; background-color: silver; border: 1px dotted gray; width: 90%; margin: auto auto; padding: 10px'>
					<b>The uploaded image appears to be damaged.</b><br>
					Please open in an external application such as photoshop, fix and re-upload.<br>
					Error: {$e->get_errmsg()}
					</div>";
			die();
		} else {
			die("PDFlib exception occurred in 223.php:\n" .
					"[" . $e->get_errnum() . "] " . $e->get_apiname() . ":: {$e->get_errmsg()}");
		}
	} catch (Exception $e) {
		var_dump($e);
	}
}

?>