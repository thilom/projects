<?php

$ad = "";

if (isset($edit)) {

	$scale = (isset($_GET["scale"]) ? $_GET["scale"] : 7);
	$adW = $adWidth * $scale;
	$adH = ($adHeight - 3) * $scale;
	$ad.="<div class=box style=width:" . $adW . ";height:" . $adH . ";position:relative; >";

//	$ad.="<div id=tab style='position:absolute;left:0;top:0;width:" . $adW . ";font-size:12pt;padding:1 4 1 4;background:" . $color_rgb . "'>";
	$ad.="<div id=par style=height:47;>";
	$ad .= "<table style=width:100%;border-collapse:collapse >";
	$ad .= "<tr>";
	$ad .= "<td onclick=A_ed(this,'establishment_name') style='font-size:14pt;height:24;font-weight:bold;line-height:100%;position:static'><span class=est_name onmouseover=\"J_TT(this,'Establishment name')\">" . $establishment_name . "</span><br><span id=star class=con" . ($stars ? "" : " style=display:none") . "><span>" . $stars . "</span></span></td>";
	$ad .= "<td class=rates style='width:100px;float:right;margin:0 0 0 0;text-align:right;'><div style='background-color:gray;width:100%; font-size:9pt' onclick=A_ed(this,'rates')><span>" . $rates . "</span></div><div onclick=A_ed(this,'rates_sgl') style='background-color:silver;width:100%;margin-top:2px;font-size:9pt'><span>" . $rates_sgl . "</span></div></td>";
	$ad .= "</tr>";
	$ad .= "</table>";
	$ad.="<div class=award style='position:absolute;left:0;top:52;width:100%;font-size:11pt'><span onclick=A_ed(this,'award')><b>".($award?$award:"?")."</b></span></div>";
//	$ad.="<div class=rates style='float:right;margin:0 8 0 0'><span onclick=A_ed(this.nextSibling,'rates') onmouseover=\"J_TT(this,'Rates')\">Rates: </span><span onclick=A_ed(this,'rates')><span>" . $rates . "</span></span></div>";
//	$ad.="<b id=region onclick=A_ed(this,'region')><span onmouseover=\"J_TT(this,'Region')\">" . $region . "</span></b> ";
//	$ad.="<b id=town onclick=A_ed(this,'town') onmouseover=\"J_TT(this,'Town or suburb')\"><span>" . $town . "</span></b>";
	$ad.="</div>";
//	$ad.="</div>";

	$ad.="<div id=par style='position:absolute;left:0;top:350;width:100%;height:214;font-size:17px;line-height:100%;overflow:hidden'>";

	$ad.="<table width=100% cellspacing=0 cellpadding=0>";

//	$ad.="<tr><td onclick=A_ed(this,'establishment_name') style='font-size:14pt;height:24;font-weight:bold;line-height:100%;position:static'><span class=est_name onmouseover=\"J_TT(this,'Establishment name')\">" . $establishment_name . "</span></td></tr>";

	$ad.="<tr><td style=font-size:12pt;line-height:110%;word-spacing:2 onclick=A_ql(this)>";

	$ad.="<img src=" . $ROOT . "/stuff/ad_templates/img/AA_rgb.png onclick=A_qal() onmouseover=\"J_TT(this,'Hide QA Logo')\" style='" . ($qa_logo ? "" : "display:none;") . "height:48;float:left;margin:2 3 0 0;cursor:pointer' id=qalogo>";
	$ad.="<div style=text-align:justify>";
	$ad.="<span" . ($_SESSION["j_user"]["role"] == "d" || $_SESSION["j_user"]["role"] == "s" || $_SESSION["j_user"]["role"] == "b" ? " class=con onclick=A_ed(this,'qa_text')" : "") . " >";
	$ad.="<b class=qatext style=word-spacing:-2 onmouseover=\"J_TT(this,'QA Text')\">" . ($qa_text && $qa_text != " " ? $qa_text : "?") . "</b>";
	$ad.="</span> ";
//	$ad.="</div> ";
//	$ad.="<div style=text-align:justify>";
	$ad.=" <span id=descr class=con onclick=A_ed(this,'description')><span onmouseover=\"J_TT(this,'Description')\">" . ($description && $description != " " ? $description : "?") . "</span></span>";
//	$ad.=" <span id=star class=con" . ($stars ? "" : " style=display:none") . "><span>" . $stars . "</span></span>";
	$ad.="</div>";
	$ad.="</td></tr></table>";
	$ad.="</div>";

	$ad.="<div id=img0 onclick=A_img(this) onmousedown=A_MV(this) style=position:absolute;top:70;right:0;width:" . ($img[0]["conWidth"] * $scale) . ";height:" . ($img[0]["conHeight"] * $scale) . ";overflow:hidden>";
	$ad.="<img src=" . $img[0]["src"] . " style=position:absolute;left:" . ($img[0]["left"] * $scale) . ";top:" . ($img[0]["top"] * $scale) . ";height:" . ($img[0]["height"] * $scale) . ">";
	$ad.="</div>";

	$ad.="<div class=con id=par onclick=A_ed(this,'address') style=position:absolute;left:0;bottom:35;height:60;width:" . $adW . ";text-align:left;font-size:11pt;font-weight:normal;line-height:110%;overflow:hidden onmouseover=\"J_TT(this,'Address line')\">";
	$ad.="<span class=addr>" . $address . "</span>";
	$ad.="</div>";

	$ad.="<div class=con id=icons onclick=A_ed(this,'icons') style=position:absolute;left:0;bottom:0;height:38;width:" . $adW . ";text-align:left;font-size:13pt;font-weight:bold;line-height:110%;overflow:hidden onmouseover=\"J_TT(this,'Icons')\">";
	$ad.="<span >".($icon_img?$icon_img:"?")."</span>";
	$ad.="</div>";

	$ad.="</div>";
} else {
	$adW = 60 / $points;
	$adH = 93 / $points;
	try {
		$p = new PDFlib();
		$p->set_parameter("logging", "filename=" . $SDR . "/stuff/ad_templates/logging/" . preg_replace("~[^a-zA-Z0-9\-]~", "", str_replace(array(" ", "--"), "-", $pdf_title)) . ".log");
		$p->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");
		$p->set_parameter("textformat", "bytes");
		$p->set_parameter("SearchPath", $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/color_profiles');
		if ($p->begin_document($pdf_filename, "") == 0)
			die("Error: " . $p->get_errmsg());

		$p->set_info("Creator", "booktravel.travel");
		$p->set_info("Author", $pdf_author);
		$p->set_info("Title", $pdf_title);

		$p->begin_page_ext($adW, $adH, "");

		$p->set_parameter("FontOutline", "HelveticaCondensedBold=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p->load_font("HelveticaCondensedBold", "unicode", "embedding");
		$p->setfont($font, 24.0);
		$p->set_parameter("FontOutline", "HelveticaCondensed=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p->load_font("HelveticaCondensed", "unicode", "embedding");
		$p->setfont($font, 24.0);

		$c = explode(",", $color_cmyk);
		$p->setcolor("fill", "cmyk", $c[0], $c[1], $c[2], $c[3]);

		//Rates
		if (isset($rates_sgl) && $rates_sgl == '?') $rates_sgl = '';
		if ($rates) {
			$p->setcolor("fill", "cmyk", 0, 0, 0, 0.60);
			$p->rect($adW-44,$adH-8.8,44,7);
			$p->fill();

			$p->fit_textline($rates, $adW, $adH - (1.8 / $points), "position={right center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=6 fillcolor={cmyk 0 0 0 0}");
		}
		if ($rates_sgl) {
			$p->setcolor("fill", "cmyk", 0, 0, 0, 0.20);
			$p->rect($adW-44,$adH-17,44,7);
			$p->fill();
			$p->fit_textline($rates_sgl, $adW, $adH - (1.8 / $points)-8, "position={right center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=6 fillcolor={cmyk 0 0 0 1}");
		}

		//Establishment Name
		$establishment_name = ($establishment_name && $establishment_name != "?" ? $establishment_name : "");
		$h = 0;
		if ($establishment_name) {
			$establishment_line = explode('<br>', $establishment_name);
			foreach ($establishment_line as $line) {
				$yPos = ($adH - (1.8 / $points)) - $h;
				$p->fit_textline($line, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");
				$h += 8;
			}
		}
		$h -= 8;

		//Stars
		$star_description = '';
		for ($x=0; $x<$_POST['star_grading']; $x++) {
			$i = $p->load_image("auto", $SDR . "/stuff/ad_templates/img/star_cmyk.jpg", "");
			$p->fit_image($i, $x*5.2, ($adH - (5.5 / $points)) - $h, "boxsize={6 6} fitmethod=entire position=center matchbox={name=img margin=-2}");
			$p->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		//Awards
		if (isset($award) && $award == '?') $award = '';
		if (!empty($award)) {
			$yPos = ($adH - (7.4 / $points)) - $h;
			$h += 8;
			$p->fit_textline($award, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
		}
		$h -= 8;


		//Image
//		$im = 'Bastion_Guest_House.tif';
		$i = img4Pdf($img[0]["hirez"], $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);


		$icchandle = $p->load_iccprofile("USWebCoatedSWOP", "usage=iccbased");
		if ($icchandle == 0)
			 throw new Exception("Error: " . $p->get_errmsg());


		$i = $p->load_image("auto", $i, "iccprofile=" . $icchandle .  " honoriccprofile=false");
		if ($i == 0)
			throw new Exception("Error: " . $p->get_errmsg());

//		$i = $p->load_image("auto", $i, "");
		$p->fit_image($i, 0, ($adH-$h - (($img[0]["conHeight"] + 4.75 + 2.6) / $points)), "boxsize={" . (($img[0]["conWidth"] + 1.6) / $points) . " " . ((($img[0]["conHeight"] + 1.6) / $points)-10). "} position={center} fitmethod=entire");
		$p->close_image($i);

		//Prepare statement - get Icon data
		$statement = "SELECT icon_url
					FROM nse_icon
					WHERE icon_id=?
					LIMIT 1";
		$sql_icon = $GLOBALS['dbCon']->prepare($statement);

		$icon_list = explode('~', $icons);

		foreach ($icon_list as $k=>$v) {
			if (empty($v)) unset($icon_list[$k]);
		}

		$icon_counter = 0;
		if (count($icon_list) >= 21) {
			$address_bottom = 6;
			$icon_line_pos = 90;
			$footer_space = 9;
		} else {
			$address_bottom = 3;
			$icon_line_pos = 93;
			$footer_space = 6;
		}
		foreach ($icon_list as $icon_id) {
			if (empty($icon_id)) continue;
			$sql_icon->bind_param('i', $icon_id);
			$sql_icon->execute();
			$sql_icon->bind_result($icon_url);
			$sql_icon->fetch();
			$sql_icon->free_result();

			if (!empty($icon_url)) {
				$icon_file = substr($icon_url, strrpos($icon_url, '/')+1);
				$icon_file = substr($icon_file, 0, strrpos($icon_file, '.'));
				$icon_file = "/stuff/ad_templates/img/icons_cmyk/$icon_file.jpg";

				$i = $p->load_image("auto", $SDR . $icon_file, "");
				$p->fit_image($i, $icon_counter*(3.0/$points), ($adH - ($icon_line_pos / $points)) , "boxsize={". 2.7/$points . " ". 2.7/$points . "} fitmethod=entire position=center matchbox={name=img2 margin=-2}");
				$p->close_image($i);
				$img_wrap = 'wrap={usematchboxes={{img2}}}';
			}

			if ($icon_counter == 19) {
				$icon_counter = 0;
				$icon_line_pos = 93;
			} else {
				$icon_counter++;
			}
		}

		//Address
		$address = ($address && $address != "?" ? str_replace($ROOT . "/stuff/ad_templates/img/icons_rgb/", $SDR . "/stuff/ad_templates/img/icons_cmyk/", $address) : "");
		$address = str_replace('&#x00B7;', chr(183), $address);
		$address = str_replace('&nbsp;', ' ', $address);

		if ($address) {


			$address_line = explode('<br>',$address);
			$line_total = count($address_line);
			$footer_space += $line_total*2.5;
			$counter2 = 1;
			foreach ($address_line as $counter=>$line) {

				$opt = "";
				$ico = array();

				preg_match_all("~<img src=(.*?)>~", $line, $m);

				if (isset($m[1]) && count($m[1])) {
					$opt.="macro {
						";
					$m = $m[1];


					foreach ($m as $k => $v) {
						if (empty($v)) continue;
						$kk = $counter2;
						$line = preg_replace("~<img src=" . $v . ">~", "<&icon" . $kk . "><&end>", $line, 1);
						$ico[$kk] = $v;
						$opt.="icon" . $kk . " {matchbox={name=icon" . $kk . " boxwidth=6 boxheight={4 0} offsettop=2}}
						";
						$counter2++;
					}
					$opt.="end {matchbox={end}}
						";
					$opt.="}
						";
				}

				$opt.=" kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode";
				$line = preg_replace("<img src=.*?/>", "", $line);
				$t = $p->create_textflow($line, $opt);
				$p->fit_textflow($t, 0, 2 / $points, $adW, ($address_bottom + (($line_total-$counter)*3))/$points, "linespreadlimit=110%");
				foreach ($ico as $k => $v) {
					$kk = $k ;
					$n = "icon" . $kk;
					$v = $p->load_image("auto", $v, "");
					$x1 = $p->info_matchbox($n, 1,"x1");
					$y1 = $p->info_matchbox($n, 1, "y1");
					$width = $p->info_matchbox($n, 1, "width");
					$height = $p->info_matchbox($n, 1, "height");
					$p->fit_image($v, $x1, $y1, "boxsize={".$width." ".$height."} fitmethod=entire position=center");
				}

			}
		}

		//Description
		$img_wrap = '';
		if ($_POST['qa_logo']!=0) {
			$i = $p->load_image("auto", $SDR . "/stuff/ad_templates/img/AA_cmyk.jpg", "");
			$p->fit_image($i, 0, ($adH - (55.4 / $points)) - $h, "boxsize={14.5 21.6} fitmethod=entire  matchbox={name=img margin=-2 boxheight={descender ascender} createwrapbox}");
			$p->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		$description = "<adjustmethod=split alignment=justify kerning=true fontsize=8 charspacing=-0.1 wordspacing=-0.3 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode>$description";

		$body_height =  $footer_space;
		$description = str_replace('<br>', "\n", $description);
		$qa_text = str_replace('<br>', "\n", $qa_text);
		$t = $p->create_textflow(($qa_text ? trim($qa_text) . " $description" : " $description"), " adjustmethod=split alignment=justify kerning=true fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode ");
		$p->fit_textflow($t, 0, ($adH - (47 / $points))  - $h, 60 / $points, $body_height / $points, "  $img_wrap verticalalign=justify");
//		$i = img4Pdf($img[0]["hirez"], $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);

		$p->end_page_ext("");
		$p->end_document("");
	} catch (PDFlibException $e) {
		if ($e->get_errnum() == 1118) {
			echo "<div style='font-weight: none; color: red; background-color: silver; border: 1px dotted gray; width: 90%; margin: auto auto; padding: 10px'>
					<b>The uploaded image appears to be damaged.</b><br>
					Please open in an external application such as photoshop, fix and re-upload.<br>
					Error: {$e->get_errmsg()}
					</div>";
			die();
		} else {
			die("PDFlib exception occurred in 201.php:\n" .
					"[" . $e->get_errnum() . "] " . $e->get_apiname() . ": " .
					$e->get_errmsg() . "\n");
		}
	} catch (Exception $e) {
		die("Error:" . $e->get_errmsg());
	}


	//Lowres PDF
	try {
		$footer_space = 0;
		$line_total = 0;
		$icon_counter = 0;
		$p_low = new PDFlib();
		$p_low->set_parameter("logging", "filename=" . $SDR . "/stuff/ad_templates/logging/" . preg_replace("~[^a-zA-Z0-9\-]~", "", str_replace(array(" ", "--"), "-", $pdf_title)) . ".log");
		$p_low->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");
		$p_low->set_parameter("textformat", "bytes");
		$p_low->set_parameter("SearchPath", $_SERVER['DOCUMENT_ROOT'] . '/juno/utility/color_profiles');
		$pdf_low_filename = str_replace('.pdf', '_low.pdf', $pdf_filename);
		if ($p_low->begin_document($pdf_low_filename, "") == 0)
			die("Error: " . $p_low->get_errmsg());

		$p_low->set_info("Creator", "booktravel.travel");
		$p_low->set_info("Author", $pdf_author);
		$p_low->set_info("Title", $pdf_title);

		$p_low->begin_page_ext($adW, $adH, "");

		$p_low->set_parameter("FontOutline", "HelveticaCondensedBold=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p_low->load_font("HelveticaCondensedBold", "unicode", "embedding");
		$p_low->setfont($font, 24.0);
		$p_low->set_parameter("FontOutline", "HelveticaCondensed=" . $SDR . "/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p_low->load_font("HelveticaCondensed", "unicode", "embedding");
		$p_low->setfont($font, 24.0);

		$c = explode(",", $color_cmyk);
		$p_low->setcolor("fill", "cmyk", $c[0], $c[1], $c[2], $c[3]);

		//Rates
		if (isset($rates_sgl) && $rates_sgl == '?') $rates_sgl = '';
		if ($rates) {
			$p_low->setcolor("fill", "cmyk", 0, 0, 0, 0.60);
			$p_low->rect($adW-44,$adH-8.8,44,7);
			$p_low->fill();

			$p_low->fit_textline($rates, $adW, $adH - (1.8 / $points), "position={right center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=6 fillcolor={cmyk 0 0 0 0}");
		}
		if ($rates_sgl) {
			$p_low->setcolor("fill", "cmyk", 0, 0, 0, 0.20);
			$p_low->rect($adW-44,$adH-17,44,7);
			$p_low->fill();
			$p_low->fit_textline($rates_sgl, $adW, $adH - (1.8 / $points)-8, "position={right center} kerning=true charspacing=-0.3 wordspacing=-0.2 fontsize=6 fillcolor={cmyk 0 0 0 1}");
		}

		//Establishment Name
		$establishment_name = ($establishment_name && $establishment_name != "?" ? $establishment_name : "");
		$h = 0;
		if ($establishment_name) {
			$establishment_line = explode('<br>', $establishment_name);
			foreach ($establishment_line as $line) {
				$yPos = ($adH - (1.8 / $points)) - $h;
				$p_low->fit_textline($line, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");
				$h += 8;
			}
		}
		$h -= 8;

		//Stars
		$star_description = '';
		for ($x=0; $x<$_POST['star_grading']; $x++) {
			$i = $p_low->load_image("auto", $SDR . "/stuff/ad_templates/img/star_rgb.jpg", "");
			$p_low->fit_image($i, $x*5.2, ($adH - (5.5 / $points)) - $h, "boxsize={6 6} fitmethod=entire position=center matchbox={name=img margin=-2}");
			$p_low->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		//Awards
		if (isset($award) && $award == '?') $award = '';
		if (!empty($award)) {
			$yPos = ($adH - (7.4 / $points)) - $h;
			$h += 8;
			$p_low->fit_textline($award, 0, $yPos, "position={left center} kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
		}
		$h -= 8;

		//Resize image
		$low_file_name = substr($img[0]["hirez"], 0, strrpos($img[0]["hirez"],'.')) . '_low.jpg';
		$low_file = $_SERVER['DOCUMENT_ROOT'] . "/hires_images/{$low_file_name}";
		$high_file = $_SERVER['DOCUMENT_ROOT'] . '/hires_images/' . $img[0]["hirez"];
		list($current_width, $current_height, $image_format) = getimagesize($high_file);
		if ($current_width > $current_height) {
			$ratio = 600/$current_width;
		} else {
			$ratio = 600/$current_height;
		}
		$low_width = (int) ($current_width*$ratio);
		$low_height = (int) ($current_height*$ratio);
		$low_image = imagecreatetruecolor($low_width, $low_height);
		$current_image = '';
		switch($image_format) {
			case IMAGETYPE_TIFF_II:
			case IMAGETYPE_TIFF_MM:
				$current_image = imagecreatefromtiff($high_file);
				break;
			case IMAGETYPE_JPEG:
				$current_image = imagecreatefromjpeg($high_file);
				break;
			case IMAGETYPE_GIF:
				$current_image = imagecreatefromgif($high_file);
				break;
			case IMAGETYPE_PNG:
				$current_image = imagecreatefrompng($high_file);
				break;
			case IMAGETYPE_WBMP:
				$current_image = imagecreatefromwbmp($high_file);
				break;
		}
		imagecopyresized($low_image, $current_image, 0, 0, 0, 0, $low_width, $low_height, $current_width, $current_height);
		imagejpeg($low_image, $low_file,80);
		imagedestroy($low_image);
		imagedestroy($current_image);

		//Image
		$i = img4Pdf($low_file_name, $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);

		$i = $p_low->load_image("auto", $i, "");
		$p_low->fit_image($i, 0, ($adH-$h - (($img[0]["conHeight"] + 4.75 + 2.6) / $points)), "boxsize={" . (($img[0]["conWidth"] + 1.6) / $points) . " " . ((($img[0]["conHeight"] + 1.6) / $points)-10). "} position={center} fitmethod=entire");
		$p_low->close_image($i);

		//Prepare statement - get Icon data
		$line_count = 0;
		$statement = "SELECT icon_url
					FROM nse_icon
					WHERE icon_id=?
					LIMIT 1";
		$sql_icon = $GLOBALS['dbCon']->prepare($statement);

		$icon_list = explode('~', $icons);

		foreach ($icon_list as $k=>$v) {
			if (empty($v)) unset($icon_list[$k]);
		}

		$icon_counter = 0;
		if (count($icon_list) >= 41) {
			$address_bottom = 9;
			$icon_line_pos = 87;
			$footer_space = 12;
		} else if (count($icon_list) >= 21) {
			$address_bottom = 6;
			$icon_line_pos = 90;
			$footer_space = 9;
		} else {
			$address_bottom = 3;
			$icon_line_pos = 93;
			$footer_space = 6;
		}
		foreach ($icon_list as $icon_id) {
			if (empty($icon_id)) continue;
			$sql_icon->bind_param('i', $icon_id);
			$sql_icon->execute();
			$sql_icon->bind_result($icon_url);
			$sql_icon->fetch();
			$sql_icon->free_result();

			if (!empty($icon_url)) {
				$icon_file = substr($icon_url, strrpos($icon_url, '/')+1);
				$icon_file = substr($icon_file, 0, strrpos($icon_file, '.'));
				$icon_file = "/stuff/ad_templates/img/icons_rgb/$icon_file.jpg";

				$i = $p_low->load_image("auto", $SDR . $icon_file, "");
				$p_low->fit_image($i, $icon_counter*(3.0/$points), ($adH - ($icon_line_pos / $points)) , "boxsize={". 2.7/$points . " ". 2.7/$points . "} fitmethod=entire position=center matchbox={name=img2 margin=-2}");
				$p_low->close_image($i);
				$img_wrap = 'wrap={usematchboxes={{img2}}}';
			}

			if ($icon_counter == 19) {
				$line_count++;
				$icon_counter = 0;
				$icon_line_pos += 3;
			} else {
				$icon_counter++;
			}
		}

		//Address
		$address = ($address && $address != "?" ? str_replace($ROOT . "/stuff/ad_templates/img/icons_rgb/", $SDR . "/stuff/ad_templates/img/icons_rgb/", $address) : "");
		$address = str_replace('&#x00B7;', chr(183), $address);
		$address = str_replace('&nbsp;', ' ', $address);

		if ($address) {
			$address_line = explode('<br>',$address);
			$line_total = count($address_line);
			$footer_space += $line_total*2.5;
			$counter2 = 1;
			foreach ($address_line as $counter=>$line) {

				$opt = "";
				$ico = array();

				preg_match_all("~<img src=(.*?)>~", $line, $m);

				if (isset($m[1]) && count($m[1])) {
					$opt.="macro {
						";
					$m = $m[1];

					foreach ($m as $k => $v) {
						if (empty($v)) continue;
						$kk = $counter2;
						$line = preg_replace("~<img src=" . $v . ">~", "<&icon" . $kk . "><&end>", $line, 1);
						$ico[$kk] = $v;
						$opt.="icon" . $kk . " {matchbox={name=icon" . $kk . " boxwidth=6 boxheight={4 0} offsettop=2}}
						";
						$counter2++;
					}
					$opt.="end {matchbox={end}}
						";
					$opt.="}
						";
				}

				$opt.=" kerning=true charspacing=-0.4 wordspacing=-0.2 fontsize=7 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode";
				$line = preg_replace("<img src=.*?/>", "", $line);
				$t = $p_low->create_textflow($line, $opt);
				$p_low->fit_textflow($t, 0, 2 / $points, $adW, ($address_bottom + (($line_total-$counter)*3))/$points, "linespreadlimit=110%");
				foreach ($ico as $k => $v) {
					$kk = $k ;
					$n = "icon" . $kk;
					$v = $p_low->load_image("auto", $v, "");
					$x1 = $p_low->info_matchbox($n, 1,"x1");
					$y1 = $p_low->info_matchbox($n, 1, "y1");
					$width = $p_low->info_matchbox($n, 1, "width");
					$height = $p_low->info_matchbox($n, 1, "height");
					$p_low->fit_image($v, $x1, $y1, "boxsize={".$width." ".$height."} fitmethod=entire position=center");
				}
			}
		}

		//Description
		$img_wrap = '';
		if ($_POST['qa_logo']!=0) {
			$i = $p_low->load_image("auto", $SDR . "/stuff/ad_templates/img/AA_rgb.png", "");
			$p_low->fit_image($i, 0, ($adH - (55.4 / $points)) - $h, "boxsize={14.5 21.6} fitmethod=entire  matchbox={name=img margin=-2 boxheight={descender ascender} createwrapbox}");
			$p_low->close_image($i);
			$img_wrap = 'wrap={usematchboxes={{img}}}';
		}

		$description = "<adjustmethod=split alignment=justify kerning=true fontsize=8 charspacing=-0.1 wordspacing=-0.3 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode>$description";

		$body_height =  $footer_space;
		$description = str_replace('<br>', "\n", $description);
		$qa_text = str_replace('<br>', "\n", $qa_text);
		$t = $p_low->create_textflow(($qa_text ? trim($qa_text) . " $description" : " $description"), " adjustmethod=split alignment=justify kerning=true fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode ");
		$p_low->fit_textflow($t, 0, ($adH - (47 / $points))  - $h, 60 / $points, $body_height / $points, "  $img_wrap verticalalign=justify");
//		$i = img4Pdf($img[0]["hirez"], $img[0]["web"], $img[0]["conWidth"], $img[0]["conHeight"], $img[0]["width"], $img[0]["height"], $img[0]["left"], $img[0]["top"], $sharpen);

		$p_low->end_page_ext("");
		$p_low->end_document("");
	} catch (PDFlibException $e) {
		if ($e->get_errnum() == 1118) {
			echo "<div style='font-weight: none; color: red; background-color: silver; border: 1px dotted gray; width: 90%; margin: auto auto; padding: 10px'>
					<b>The uploaded image appears to be damaged.</b><br>
					Please open in an external application such as photoshop, fix and re-upload.<br>
					Error: {$e->get_errmsg()}
					</div>";
			die();
		} else {
			die("PDFlib exception occurred in 201.php:\n" .
					"[" . $e->get_errnum() . "] " . $e->get_apiname() . ": " .
					$e->get_errmsg() . "\n");
		}
	} catch (Exception $e) {
		var_dump($e);
		die("Error:" . $e->get_errmsg());
	}
	$p = 0;
	$p_low = 0;
}
?>