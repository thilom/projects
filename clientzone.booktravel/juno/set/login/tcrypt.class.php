<?php
/**
 * Encryption/Decryption Class
 *
 * Warning! Changing values in this class may break parts of the application such as esur login.
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */

if (realpath(__FILE__) == realpath($_SERVER['SCRIPT_FILENAME'])) exit('This file can not be accessed directly...');

class tcrypt {
    /**
     * @var string Mcrypt made such as ecb, efb, cfb
     */
    private $mode = 'cfb';
    
    /**
     * @var string The key used for encryption and decryption
     */
    private $key = 'RVBus encrypt Key!';
    
    /**
     * @var string The cypher to use for encryption and decryption
     */
    private $cipher = 'rijndael-256';
    
    /**
     * @var object Holds the loaded cypher
     */
    private $lCipher = '';
    
    /**
     * @var int Initialization Vector Size
     */
    private $ivSize = '';
    
    function __construct() {
        $this->lCipher = mcrypt_module_open($this->cipher, '', $this->mode, '');
        $keySize = mcrypt_enc_get_key_size($this->lCipher);
        $this->key = substr(sha1($this->key), 0, $keySize);
        $this->ivSize = mcrypt_enc_get_iv_size($this->lCipher);
    }
    
    public function encrypt($data) {
        $iv = mcrypt_create_iv($this->ivSize, MCRYPT_RAND);
        if (mcrypt_generic_init($this->lCipher, $this->key, $iv) !== 0 ) return false;
        $data = mcrypt_generic($this->lCipher, $data);
        $data = $iv . $data;
        return $data;
    }
    
    public function decrypt($data) {
        $iv = substr($data, 0, $this->ivSize);
        $data = substr($data, $this->ivSize);
        if (mcrypt_generic_init($this->lCipher, $this->key, $iv) !== 0 ) return false;
        $data = mdecrypt_generic($this->lCipher, $data);
        return $data;
    }
    
    function __destruct() {
        //mcrypt_generic_deinit($this->lCipher);
        mcrypt_module_close($this->lCipher);
    }
}

?>