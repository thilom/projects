<?php

//Constants
define('PROFORMA',1);
define('TAX_INVOICE',2);
define('EFT',1);
define('CASH',2);
define('CHEQUE',3);
define('DEBIT_ORDER',4);
define('CREDIT_CARD',5);
define('DISCOUNT_NONE',0);
define('DISCOUNT_PERCENT',1);
define('DISCOUNT_FIXED',2);

session_name("aatravel");
ini_set("session.gc_maxlifetime",21600);
if(!session_id() && session_id()!="aatravel")session_start();
if(version_compare("5.1.0",PHP_VERSION)<0)date_default_timezone_set("Africa/Johannesburg");

$ROOT="/juno";
$_SESSION["ROOT"]=$ROOT;
$SDR=$_SERVER["DOCUMENT_ROOT"].$ROOT;

//Contants
define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('CUR_DOMAIN', $_SERVER['SERVER_NAME']);
define ("LOGIN_TIMEOUT",30);

require_once $SDR . '/custom/variables.php';
require_once $SDR . '/custom/database.php';
require_once $SDR . '/custom/google.php';
require_once $SDR . '/custom/exception.class.php';
require_once $SDR . '/custom/sql.class.php';
require_once $SDR . '/custom/security.class.php';

//Instantiate Security
$security = new security();

//Connect to Database - Traditional
if (($db_con = @mysql_connect($db['aa_travel']['db_server'], $db['aa_travel']['db_username'], $db['aa_travel']['db_password'])) === FALSE) die("<span style='font-weight: bold; color: red'>Failed to connect to Database (Basic)</span>");
$db_con;
mysql_select_db($db['aa_travel']['db_name']);

//Connect to AA Travel Database - Mysqli
try {
	$dbCon = @new aa_sql($db['aa_travel']['db_server'], $db['aa_travel']['db_username'], $db['aa_travel']['db_password'], $db['aa_travel']['db_name']);
} catch (SQLConnectException   $e) {
	die("<span style='font-weight: bold; color: red'>Failed to connect to AA Travel Database (Advanced) </span>");
}


//Connect to Database - PDO
try {
	$connect = "mysql:host={$db['aa_travel']['db_server']};dbname={$db['aa_travel']['db_name']}";
	$db_pdo = new PDO($connect, $db['aa_travel']['db_username'], $db['aa_travel']['db_password']);
	$db_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo 'Failed to connect: PDO';
}

error_reporting(E_ALL);
ini_set("display_errors",1);

$EPOCH=mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
$URL=str_replace($ROOT,"",$_SERVER["SCRIPT_NAME"]);

include_once $SDR."/custom/init_extra.php";
?>