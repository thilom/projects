<?php
/**
 * Encryption/Decryption Class
 *
 * Warning! Changing values in this class may break parts of the application such as user login.
 *
 * @author Thilo Muller(2009-2011)
 * @package RVBus
 * @category security
 * @version $Id: tcrypt.class.php 255 2011-02-14 08:41:14Z web37 $
 */

if (realpath(__FILE__) == realpath($_SERVER['SCRIPT_FILENAME'])) exit('This file can not be accessed directly...');

class tcrypt {
    /**
     * @var string Mcrypt made such as ecb, efb, cfb
     */
    private $mode = 'cfb';
    
    /**
     * @var string The key used for encryption and decryption
     */
    private $key = 'RVBus encrypt Key!';
    
    /**
     * @var string The cypher to use for encryption and decryption
     */
    private $cipher = 'rijndael-256';
    
    /**
     * @var object Holds the loaded cypher
     */
    private $lCipher = '';
    
    /**
     * @var int Initialization Vector Size
     */
    private $ivSize = '';
    
    function __construct() {
        $this->lCipher = mcrypt_module_open($this->cipher, '', $this->mode, '');
        $keySize = mcrypt_enc_get_key_size($this->lCipher);
        $this->key = substr(sha1($this->key), 0, $keySize);
        $this->ivSize = mcrypt_enc_get_iv_size($this->lCipher);
    }
    
    public function encrypt($data) {
        $enc['iv'] = mcrypt_create_iv($this->ivSize, MCRYPT_RAND);
        if (mcrypt_generic_init($this->lCipher, $this->key, $enc['iv']) !== 0 ) return false;
        $data = mcrypt_generic($this->lCipher, $data);
		$enc['data'] = $data;
        return $enc;
    }
    
    public function decrypt($data, $iv) {
        if (mcrypt_generic_init($this->lCipher, $this->key, $iv) !== 0 ) return false;
        $data = mdecrypt_generic($this->lCipher, $data);
        return $data;
    }
    
    function __destruct() {
        //mcrypt_generic_deinit($this->lCipher);
        mcrypt_module_close($this->lCipher);
    }
}

?>