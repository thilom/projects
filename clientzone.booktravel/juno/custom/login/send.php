<?php
require_once $SDR."/custom/email_headers.php";
$subject="AA TRAVEL GUIDES LOGIN";
$msg=$j_email_header;
$msg.="<b>AA TRAVEL GUIDES LOGIN</b><hr>";
$msg.="User-Name: <b>".$g["login_name"]."</b><br>";
require_once $SDR."/custom/login/class.login.php";
$msg.="Password: <b>".$GLOBALS["tc"]->decrypt($g["password"], $g['iv'])."</b><br>";
$msg.="<hr><div style='font-size:9pt'><b style='color:#DD0000'>IMPORTANT!</b> For security reasons, we strongly recommend you delete this email as soon as you have used your details. We can always send details when you require.</div><br><br><br>";
$msg.=$j_email_footer;
require_once $SDR."/utility/PHPMailer/class.phpmailer.php";
$mail=new PHPMailer(true);
try
{
	$send=1;
	$mail->AddReplyTo($g["email"],$g["firstname"]." ".$g["lastname"]);
	$mail->AddAddress($g["email"],$g["firstname"]." ".$g["lastname"]);
	$mail->SetFrom($g["email"],$g["firstname"]." ".$g["lastname"]);
	$mail->Subject =$subject;
	$mail->AltBody="To view the message, please use an HTML compatible email viewer!";
	$mail->MsgHTML($msg);
	$mail->Send();
}
catch (phpmailerException $e){$send=0;}
catch (Exception $e){$send=0;}
?>
