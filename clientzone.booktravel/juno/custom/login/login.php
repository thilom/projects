<?php

/**
 *
 *
 * @version $Id: login.php 1312 2011-11-04 06:14:23Z web37 $
 */

require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

if(count($_POST) && (!isset($_POST["r"]) || (isset($_POST["r"]) && isset($_SESSION["j_user"]["role"]) && strpos($_SESSION["j_user"]["role"],$_POST["r"])!==false)))
{
	if(isset($_POST["e"]) && $_POST["e"] && !$_POST["p"])
	{
		$g=mysql_fetch_array(mysql_query("SELECT * FROM nse_user WHERE login_name='".str_replace(array("\"","'"),"",$_POST["e"])."' LIMIT 1"));
		$send=0;
		if($g["user_id"])
			include $SDR."/custom/login/send.php";
	}
	else
	{
		include $SDR."/set/roles.php";
		function LoIn()
		{
			global $SDR,$ROOT,$EPOCH,$roles;
			echo "<script>p=parent";
			echo ";p.j_user_id=".$_SESSION["j_user"]["id"];
			$login="p.";
			include $SDR."/system/prep.php";
			if($roles=="s"||$roles=="d")
				echo ";p.J_W('".$ROOT."/apps/dashboard/start.php')";
			echo ";p.J_WX(self,1)"; // close
			echo "</script>";
			include_once $SDR."/system/activity.php";
			J_act("",1);
			die();
		}
		if(isset($_POST["r"]))
		{
			$_SESSION["j_user"]["role"]=$_POST["r"];
			if(isset($_SESSION["j_prev"])) // previously logged in
			{
				unset($_SESSION["j_prev"]);
				echo "<script>parent.location='".$ROOT."/index.php'</script>";
				die();
			}
			LoIn();
		}
		if(isset($_SESSION["j_user"]["role"]) && strlen($_SESSION["j_user"]["role"])==1)
			$_SESSION["j_prev"]=$_SESSION["j_user"]["role"];
		require_once $SDR."/custom/login/class.login.php";
		$lg_obj=new login();
		$lg_obj->do_login($_POST["e"],$_POST["p"]);
		if($lg_obj->check_login())
		{
			$id=$_SESSION["j_user"]["id"];
			$db_roles="c";
			$statement="SELECT roles FROM nse_user WHERE user_id=?";
			$sql=$GLOBALS['dbCon']->prepare($statement);
			$sql->bind_param('s',$id);
			$sql->execute();
			$sql->store_result();
			$sql->bind_result($db_roles);
			$sql->fetch();
			$sql->free_result();
			$sql->close();
			if (empty($db_roles)) $db_roles = 'c';
			$_SESSION["j_user"]["role"]=$db_roles;
			if(strlen($_SESSION["j_user"]["role"])>1)
			{
				echo "<html>";
				echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
				echo "<script src=".$ROOT."/system/P.js></script>";
				echo "<script src=".$ROOT."/custom/login/login.js></script>";
				$a="";
				$r=str_split($_SESSION["j_user"]["role"]);
				foreach($r as $k => $v)
					$a.=$v."~".$roles[$v]."|";
				echo "<script>J_role(\"".$a."\"".(isset($_SESSION["j_prev"])?",'".$_SESSION["j_prev"]."'":"").")</script>";
				die();
			}
			LoIn();
		}
	}
}

echo "<html>";
echo "<link rel=stylesheet href=".$ROOT."/style/set/page.css>";
echo "<script src=".$ROOT."/system/P.js></script>";
echo "<script src=".$ROOT."/custom/login/login.js></script>";
echo "<body style=overflow-x:hidden>";
echo "<script>J_log(".(isset($_POST["e"])?"\"".$_POST["e"]."\"":0).(isset($send)?",".($send?1:2):"").")</script>";
$J_title1="Log In";
$J_width=300;
$J_height=140;
$J_label=8;
$J_icon="<img src=".$ROOT."/ico/set/password.png>";
$J_nomax=1;
$J_nostart=1;
include $SDR."/system/deploy.php";
echo "</body></html>";
?>