<?php
/** 
 * Check a given name againts current establishments
 *
 * @author Thilo Muller (2011)
 */

require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$max_distance = strlen($_GET['est'])<10?3:5;
$establishment_list = array();

if (!isset($_GET['est'])) {
	echo "";
	die();
}

$statement = "SELECT establishment_code, establishment_name FROM nse_establishment";
$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
$sql_estabs->execute();
$sql_estabs->store_result();
$sql_estabs->bind_result($establishment_code, $check_name);
while ($sql_estabs->fetch()) {
	if (isset($_GET['code']) && $_GET['code']==$establishment_code) continue;

	//Check Levenstein distance
	$distance = levenshtein($_GET['est'], $check_name);
	if ($distance < $max_distance) {
		$establishment_list[$establishment_code] = $distance;
	}
}
$sql_estabs->free_result();
$sql_estabs->close();


//Sort by distance
ksort($establishment_list);

//Prepare statement - establishment data
$statement = "SELECT a.establishment_name, c.province_name, d.town_name, e.suburb_name
				FROM nse_establishment AS a
				LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
				LEFT JOIN nse_nlocations_provinces AS c ON b.province_id=c.province_id
				LEFT JOIN nse_nlocations_towns AS d ON b.town_id=d.town_id
				LEFT JOIN nse_nlocations_suburbs AS e ON b.suburb_id=e.suburb_id
				WHERE a.establishment_code=?
				LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Get top 5 results
$counter = 0;
foreach ($establishment_list as $establishment_code=>$distance) {
	if ($counter > 6) break;

	$sql_data->bind_param('s', $establishment_code);
	$sql_data->execute();
	$sql_data->bind_result($establishment_name, $province_name, $town_name, $suburb_name);
	$sql_data->fetch();

	echo "$establishment_code~$establishment_name~$province_name~$town_name~$suburb_name|";
	$counter++;
}



?>
