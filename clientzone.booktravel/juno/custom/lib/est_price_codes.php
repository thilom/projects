<?php

$price_codes=array(
"A"=>array(0,249)
,"B"=>array(250,499)
,"C"=>array(500,749)
,"D"=>array(750,999)
,"E"=>array(1000,1249)
,"F"=>array(1250,1499)
,"G"=>array(1500,1749)
,"H"=>array(1750,1999)
,"I"=>array(2000,2499)
,"J"=>array(2500,2999)
,"K"=>array(3000,3999)
,"L"=>array(4000,4999)
,"M"=>array(5000,5999)
,"N"=>array(6000,1000000)
);

function get_price_code($p=0)
{
	$r="";
	if($p)
	{
		$p=$p*1;
		if(is_numeric($p))
		{
			global $price_codes;
			foreach($price_codes as $k => $v)
			{
				if($p<=$v[1])
				{
					$r=$k;
					break;
				}
			}
		}
	}
	return $r;
}

?>