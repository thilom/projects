<?php
/**
 * Manages establishment notes
 *
 * @author Thilo Muller(2010)
 * @package RVBus
 * @category notes
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

/**
 * Return an array of notes for an establishment
 * @param string $establishment_code 
 */
function get_notes($establishment_code) {
   $notes = array();
   $note_counter = 0;
   $statement = "SELECT user_name, note_timestamp, note_content FROM nse_establishment_notes WHERE establishment_code=? ORDER BY note_timestamp DESC";
   $sql_notes = $GLOBALS['dbCon']->prepare($statement);
   $sql_notes->bind_param('s', $establishment_code);
   $sql_notes->execute();
   $sql_notes->store_result();
   $sql_notes->bind_result($note_user, $note_date, $note_content);
   while ($sql_notes->fetch()) {
       $notes[$note_counter]['user'] = $note_user;
       $notes[$note_counter]['date'] = $note_date;
       $notes[$note_counter]['note'] = $note_content;
       $note_counter++;
   }
   $sql_notes->free_result();
   $sql_notes->close();
   
   return $notes;

}

?>
