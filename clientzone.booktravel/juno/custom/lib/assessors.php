<?php
/**
 * Functions to handle AA Assessors
 *
 * @author Thilo Muller(2010)
 * @package RVBus
 * @category libs
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";


if (isset($_GET['listall'])) {
    echo get_assessors();
}

/**
 * Retrieves a tilde(~) delimited list of assessors and thier user ID's
 *
 * @return string
 */
function get_assessors() {
    $assessors = '';
    $statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) FROM nse_user WHERE roles LIKE '%a%' ORDER BY firstname";
    $sql_assessor = $GLOBALS['dbCon']->prepare($statement);
    $sql_assessor->execute();
    $sql_assessor->store_result();
    $sql_assessor->bind_result($user_id, $user_name);
    while ($sql_assessor->fetch()) {
        $assessors .= "$user_id|$user_name~";
    }
    $sql_assessor->free_result();
    $sql_assessor->close();
    $assessors = substr($assessors, 0, -1);
    return $assessors;

}

?>
