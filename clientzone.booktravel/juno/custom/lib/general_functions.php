<?php
/** 
 * Shared functions
 *
 * @author Thilo Muller
 * @version $Id$
 */

function timestamp_establishment($code) {
	$statement = "UPDATE nse_establishment SET last_updated=NOW() WHERE establishment_code=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bind_param('s', $code);
	$sql_update->execute();
	$sql_update->close();
}

?>
