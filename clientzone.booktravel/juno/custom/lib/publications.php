<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

function publications($id=0)
{
	$v="";
	$statement="SELECT publication_id,publication_name,publication_year FROM nse_publications";
	if($id != 0) {
		$statement.=" WHERE publication_id=".$id." LIMIT 1";
    } else {
		$statement.=" ORDER BY publication_year DESC,publication_name";
    }
	$sql_publications=$GLOBALS['dbCon']->prepare($statement);
	$sql_publications->execute();
	$sql_publications->store_result();
	$sql_publications->bind_result($pub_id,$name,$year);
	while($sql_publications->fetch())
	{
		 $v .= $id!=0?"$year - $name":"$pub_id~$name~$year|";
	}
	$sql_publications->free_result();
	$sql_publications->close();
	return $v;
}

?>