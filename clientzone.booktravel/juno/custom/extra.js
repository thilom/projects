contact="<a href=go: onclick=\"J_W(ROOT+'/apps/establishment_manager/contact.php');return false\" onmouseover=\"J_TT(this,'<b>Office: 011 713-2000</b> or click for more contact details')\" onfocus=blur() class=jW3a><img src="+ROOT+"/ico/set/phone2.png></a>"

j_G["sec"]="This facility is for use by the owners only. The data and lists found here are confidential and may not be duplicated for any reason without expressed permission from senior personel."
j_G["non_staff_search"]="<tt style='margin:6px 0 4px 0'><b style=color:#CC0000>WARNING!</b> Your establishment search criteria is limited to those companies to which you currently have access.</tt>"

j_G['default_establishment']={"id":"","name":""}

// every app has an id
j_G['app_ids']="<option value=0>None<option value=1>Access<option value=2>Accounts<option value=3>Assessment Manager<option value=4>Establishment Manager<option value=5>Image Manager<option value=6>People"
apps=["","Access","Accounts","Assessment Manager","Establishment Manager","Image Manager","People","Awards Manager"]

function E_stab(g,o)
{
	var i,u,y,z,a="",b=""
	if(g)
	{
		y=1
		a+="<optgroup label=Group>"
		b+="<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf1>Group</a><div style=display:block>"
		g=g.split("|")
		i=0
		while(g[i])
		{
			u=g[i].split("~")
			a+="<option value="+u[0]+">"+u[1]+"</option>"
			b+="<a href=go: onclick=\"E_go('"+u[0]+"');return false\" onfocus=blur()>"+u[1]+"</a>"
			i++
		}
		a+="</optgroup>"
		b+="</div>"
	}
	if(o)
	{
		z=1
		if(y)
		{
			a+="<optgroup label='"+(y?"Other ":"")+"Establishments'>"
			b+="<a href=go: onclick=\"J_U(this);return false\" onfocus=blur() class=jf1>Group</a><div style=display:block>"
		}
		o=o.split("|")
		i=0
		while(o[i])
		{
			u=o[i].split("~")
			a+="<option value="+u[0]+">"+u[1]+"</option>";
			b+="<a href=go: onclick=\"E_go('"+u[0]+"');return false\" onfocus=blur()>"+u[1]+"</a>"
			i++
		}
		if(y)
		{
			a+="</optgroup>"
			b+="</div>"
		}
	}
	if(y||z)
		b="<blockquote>"+b+"</blockquote>"
	j_G["estabs"]=[]
	j_G["estabs"]["option"]=a
	j_G["estabs"]["link"]=b
}

j_G["telcou"]=""

function J_telFormat(t,r,g,c)
{
	var a="",p=["","","",""],v,i
	if(c)
	{
		j_G["self"].j_E.value=""
		J_closer()
	}
	else
	{
		if(g)
		{
			t=$("jccv")
			t.value=g
		}
		if(r)
		{
			function k(t,s)
			{
				return (t.value+"").replace(/[^0-9]/g,"").substr(0,s)
			}
			t=$T(t.parentNode,"input")
			p[0]=k(t[0],4)
			p[1]=k(t[1],4)
			p[2]=k(t[2],3)
			p[3]=k(t[3],4)
			p[0]=p[0].length>1?("+"+(p[0].substr(0,1)=="0"?p[0].substr(1):p[0])):""
			j_G["self"].j_E.value=J_trim((p[0]?p[0]:"")+(p[1].length>1?(p[0]?" ":"")+"("+p[1].substr(0,1)+")"+p[1].substr(1):"")+(p[2]?" "+p[2]:"")+(p[3]?" "+p[3]:""))
		}
		else if(t.value.indexOf("0860 ")>-1)
		{}
		else
		{
			v=J_trim((t.value+"").replace(/ -/g," ").replace(/- /g," ").replace(/-/g," ").replace(/  /g," "))
			v=v.replace(/\+/g,"0").replace(/[^0-9 \(\)]/g,"")
			if(v)
			{
				if(v.indexOf("(")>0&&v.indexOf(")")>0)
				{
					v=v.replace(/ \(/,"(").replace(/\( /,"(").replace(/ \)/,")").replace(/\) /,")")
					if(v.indexOf("(0)")>0&&v.match(/ /g).length>0)
					{
						a=v.substr(0,v.indexOf("("))
						i=v.substr(v.indexOf(")")+1)
						i=i.substr(0,i.indexOf(" "))
						v=v.substr(v.indexOf(" ")+1)
						v=a+"("+i+")"+v
					}
					v=v.replace(/ /g,"")
					p[0]=v
					p[0].replace(/(.*?)\(/,function(a,b){p[0]=b?b:"?"})
					p[1]=v
					p[1].replace(/\((.*?)\)/,function(a,b){p[1]=b?b:"?"})
					v=v.substr(v.indexOf(")")+1)
					p[2]=v.substr(0,3)
					p[3]=v.substr(3,4)
				}
				else if(v.length)
				{
					function rr(v,s)
					{
						return (v+"").split("").reverse().join("")
					}
					v=rr("0000000000000"+v.replace(/[^0-9]/g,""))
					p[3]=rr(v.substr(0,4))
					p[2]=rr(v.substr(4,3))
					p[1]=rr(v.substr(7,2))
					if(!p[1].indexOf("0"))
					{
						p[1]=p[1].substr(1)
						p[0]=rr(v.substr(10,2))
					}
					else
						p[0]=rr(v.substr(10,2))
					if(!p[0].indexOf("0"))
					{}
					else
						p[0]="0"+p[0]
				}
			}
			a="<table cellpadding=0><tr>"
			a+="<td><a href=http://www.countrycallingcodes.com/countrylist.php target=_blank onfocus=blur()><img src="+ROOT+"/ico/set/phone1.png style='margin:0 3px 0 0;height:22px;cursor:pointer' onmouseover=\"J_TT(this,'<b>Country and Area Codes</b><br>Please try and fill in both the Country and Area code. Click here for list of both codes')\"></a></td>"
			a+="<td nowrap>"
			a+="<input value='"+p[0]+"' type=text onkeyup=J_telFormat(this,1) onmouseover=\"J_TT(this,'<b>Country code</b><br>(2-3 digits)')\" style=font-weight:bold;width:38 id=jccv onclick=\"J_note(this,j_G['telcou'],160,3,4,-1,1,3,0,0,160)\">"
			a+=" <input value='0"+p[1]+"' type=text onkeyup=J_telFormat(this,1) onmouseover=\"J_TT(this,'<b>Area code</b><br>(3-4 digits)')\" style=font-weight:bold;width:30>"
			a+=" <input value='"+p[2]+"' type=text onkeyup=\"if(this.value.length>3){var a=this.value.substr(0,3),b=this.value.substr(3);this.value=a;var c=$('tf_4');c.value=b;c.focus()}J_telFormat(this,1)\" onmouseover=\"J_TT(this,'<b>Number</b><br>(3 digits)')\" style=font-weight:bold;width:30>"
			a+="-<input value='"+p[3]+"' id=tf_4 type=text onkeyup=J_telFormat(this,1) onmouseover=\"J_TT(this,'<b>Number</b><br>(4 digits)')\" style=font-weight:bold;width:38>"
			a+="</td>"
			a+="<td><img src="+ROOT+"/ico/set/cancel.png onclick=J_telFormat(0,0,0,this) style='margin:0 0 0 3px;height:22px;cursor:pointer' onmouseover=\"J_TT(this,'Clear')\"></td>"
			a+="</tr></table>"
			J_note(t,a,0,3,0,4,1,2)
			if(!j_G["telcou"])
			{
				a="<b class=jA>"
				a+="<a href=http://www.countrycallingcodes.com/countrylist.php target=_blank onfocus=blur() onmouseover=\"J_TT(this,'Click here for country and area codes')\">List of Codes</a>"
				c=[]
				c["Angola"]="+244"
				c["Botswana"]="+267"
				c["Kenya"]="+254"
				c["Madagascar"]="+261"
				c["Malawi"]="+265"
				c["Mauritius"]="+230"
				c["Mozambique"]="+258"
				c["Lesotho"]="+266"
				c["Namibia"]="+264"
				c["Reunion"]="+262"
				c["South Africa"]="+27"
				c["Swaziland"]="+268"
				c["Tanzania"]="+258"
				c["Zambia"]="+260"
				c["Zimbabwe"]="+263"
				for(i in c)
				{
					a+="<a href=go: onclick=\"J_telFormat(1,1,'"+c[i]+"');return false\" onfocus=blur() onmouseover=\"J_TT(this,'"+c[i]+"')\">"+i+"</a>"
					i++
				}
				a+="</b>"
				j_G["telcou"]=a
			}
		}
	}
}