<?php
$_SESSION["j_user"]["estabs"]=array();
$ga=array();
$gp="";
$na="";

// find if user has access to a whole group
$q="SELECT establishment_group FROM nse_user WHERE user_id=".$_SESSION["j_user"]["id"]." AND establishment_group!=0 LIMIT 1";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	$g=mysql_fetch_array($q);
	{
		$q="SELECT DISTINCT e.establishment_code,e.establishment_name FROM nse_establishment AS e JOIN nse_establishment_group AS g ON e.establishment_code=g.establishment_code WHERE g.group_name='".$g["establishment_group"]."' ORDER BY e.establishment_name";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
			{
				$_SESSION["j_user"]["estabs"][$g["establishment_code"]]=1;
				$ga[ucwords(strtolower(trim(preg_replace("~[^a-zA-Z0-9 ,:&@\(\)\-\.]~","",$g["establishment_name"]))))]=$g["establishment_code"];
			}
			ksort($ga);
			foreach($ga as $k => $v)
				$gp.=$v."~".$k."|";
		}
	}
}

// find normal access
$q="SELECT DISTINCT e.establishment_code,e.establishment_name FROM nse_establishment AS e JOIN nse_user_establishments AS u ON e.establishment_code=u.establishment_code WHERE (e.assessor_id=".$_SESSION["j_user"]["id"]." OR e.rep_id1=".$_SESSION["j_user"]["id"]." OR e.rep_id2=".$_SESSION["j_user"]["id"]." OR u.user_id=".$_SESSION["j_user"]["id"].") ORDER BY e.establishment_name";
$q=mysql_query($q);
if(mysql_num_rows($q))
{
	while($g=mysql_fetch_array($q))
	{
		if(!isset($_SESSION["j_user"]["estabs"][$g["establishment_code"]]))
		{
			$_SESSION["j_user"]["estabs"][$g["establishment_code"]]=1;
			$na.=$g["establishment_code"]."~".ucwords(strtolower(trim(preg_replace("~[^a-zA-Z0-9 ,:&@\(\)\-\.]~","",$g["establishment_name"]))))."|";
		}
	}
}

// default establishment - used in search.js panels
// if only one establishment for non staff
if(count($_SESSION["j_user"]["estabs"])==1 && ($_SESSION["j_user"]["role"]=="a" || $_SESSION["j_user"]["role"]=="c" || $_SESSION["j_user"]["role"]=="r"))
{
	foreach($_SESSION["j_user"]["estabs"] as $k => $v)
		echo ";".$login."j_G['default_establishment']={'id':'".$k."','name':\"".preg_replace("~[^0-9a-zA-Z !@&;:',\(\)\-\.]~","",J_Value("","establishment","",$k))."\"}";
}
?>