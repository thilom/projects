<?php

// EXAMPLE
// part of sql query
// $eid=check_user_establishment($eid);
// if($eid) // if valid establishment code
//	$sql.="AND establishment_code='".$eid."' ";
// else // constrain user establishments
//	$sql.=constrain_user_establishments(1);

// both functions only work on non-staff roles

function check_user_establishment($id)
{
	if($_SESSION["j_user"]["role"]=="a" || $_SESSION["j_user"]["role"]=="c" || $_SESSION["j_user"]["role"]=="r")
	{
		 if(!isset($_SESSION["j_user"]["estabs"][$id]))
			$id=0;
	}
	return $id;
}

// ARGUMENTS
// $on - by default is off
// $columns - what columns the output sql must check. If multiple columns (separated by ~) then all those will be used

function user_establishments_constrain($on=0,$columns="establishment_code")
{
	$sql="";
	if($on && ($_SESSION["j_user"]["role"]=="a" || $_SESSION["j_user"]["role"]=="c" || $_SESSION["j_user"]["role"]=="r"))
	{
		// separate columns
		$columns=explode("~",$columns);
		// run thru available establishment_codes
		foreach($_SESSION["j_user"]["estabs"] as $establishment_code => $v)
		{
		 // apply columns to each value
 			foreach($columns as $k => $column)
			{
				if($column) // avoid blanks
					$sql.=(!$sql?"":" OR ").$column."='".$establishment_code."'";
			}
		}
		if($sql)
			$sql=" AND (".$sql.") "; // prep for insertion into main sql string
	}
	return $sql;
}

?>