<?php
function qa_status($e)
{
	$status="";
	//Check current complete assessments
	$statement="SELECT a.assessment_date, b.cancelled_date
	FROM nse_establishment_assessment AS a
	LEFT JOIN nse_establishment_qa_cancelled AS b ON a.establishment_code=b.establishment_code
	WHERE a.establishment_code=? AND a.assessment_status='complete'
	LIMIT 1";
	$sql_assessment = $GLOBALS['dbCon']->prepare($statement);
	$sql_assessment->bind_param('s',$e);
	$sql_assessment->execute();
	$sql_assessment->bind_result($assessment_date,$cancel_date);
	$sql_assessment->fetch();
	$sql_assessment->close();
	if(!empty($assessment_date))
	{
		if(!empty($cancel_date) && $cancel_date!="0000-00-00")
			$status="X";
		else
			$status="Y";
	}
	if($status!="X")
	{
		//Check for incomplete assessments
		$statement = "SELECT assessment_status
		FROM nse_establishment_assessment
		WHERE establishment_code=? AND assessment_status!='complete'
		LIMIT 1";
		$sql_current = $GLOBALS['dbCon']->prepare($statement);
		$sql_current->bind_param('s',$e);
		$sql_current->execute();
		$sql_current->bind_result($current_status);
		$sql_current->fetch();
		$sql_current->close();
		if(isset($current_status) && !empty($current_status))
			$status="P";
	}
	return $status;
}

function qa_out($e)
{
	$o="";
	$q=mysql_query("SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code='".$e."' ORDER BY cancelled_date DESC LIMIT 1");
	if(mysql_num_rows($q))
	{
		$g=mysql_fetch_array($q);
		$o=substr($g["cancelled_date"],5,2)."/".substr($g["cancelled_date"],2,2);
		if($o=="00/00")
			$o="";
	}
	return $o;
}

function qa_paid_up($e)
{
	$v="Y";
	$q="SELECT j_invit_total,j_invit_paid FROM nse_invoice_item JOIN nse_invoice ON j_invit_invoice=j_inv_id JOIN nse_inventory ON j_invit_inventory=j_in_id WHERE j_inv_to_company='".$e."' AND j_inv_type<6 AND j_invit_total>0 AND j_invit_total!=j_invit_paid AND (j_in_category=5 OR j_in_category=6 OR j_in_category=15 OR j_in_category=16)";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		$t=0;
		$p=0;
		while($g=mysql_fetch_array($q))
		{
			$t+=$g["j_invit_total"];
			$p+=$g["j_invit_paid"];
		}
		$t=$t-$p;
		if($t>99.99)
			$v="N";
	}
	return $v;
}

function entered($e)
{
	$v="";
	$q=mysql_query("SELECT enter_date FROM nse_establishment WHERE establishment_code='".$e."' AND enter_date!='' LIMIT 1");
	if(mysql_num_rows($q))
	{
		$g=mysql_fetch_array($q);
		$g=$g["enter_date"];
		$v=substr($g,5,2)."/".substr($g,2,2);
		if($v=="00/00")
			$v="";
	}
	return $v;
}

function renew($e)
{
	global $EPOCH;
	$v=array("","");
	if(mysql_num_rows(mysql_query("SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code='".$e."' LIMIT 1")))
			$v=array("","X");
	else
	{
		$q=mysql_query("SELECT renewal_date FROM nse_establishment_assessment WHERE establishment_code='".$e."' AND assessment_status='complete' ORDER BY assessment_date DESC LIMIT 1");
		if(mysql_num_rows($q))
		{
			$g=mysql_fetch_array($q);
			if($g["renewal_date"])
			{
				$g=$g["renewal_date"];
				$v[0]=substr($g,5,2)."/".substr($g,2,2);
				$v[1]=mktime(0,0,0,date(substr($g,5,2)),date(substr($g,8,2)),date(substr($g,0,4)));
				$v[1]=$EPOCH>$v[1]?"X":"Y";
				if($v[0]=="00/00")
					$v[0]="";
			}
		}
		else
		{
			$q=mysql_query("SELECT enter_date FROM nse_establishment WHERE establishment_code='".$e."' AND enter_date!='' LIMIT 1");
			if(mysql_num_rows($q))
			{
				$g=mysql_fetch_array($q);
				if($g["enter_date"])
				{
					$g=$g["enter_date"];
					$v[0]=substr($g,5,2)."/".substr($g,2,2);
					$v[1]=mktime(0,0,0,date(substr($g,5,2)),date(1),date("Y"));
					$v[1]=$EPOCH>$v[1]?"X":"Y";
					if($v[0]=="00/00")
						$v[0]="";
				}
			}
		}
	}
	return $v;
}

function visit($e)
{
	$v=array("","","");
	$q=mysql_query("SELECT assessment_date,invoice_id FROM nse_establishment_assessment WHERE establishment_code='".$e."' LIMIT 1");
	if(mysql_num_rows($q))
	{
		$g=mysql_fetch_array($q);
		if($g["assessment_date"])
		{
			$m=substr($g["assessment_date"],5,2);
			$d=substr($g["assessment_date"],2,2);
			if($m && $m*1>0 && $d && $d*1>0)
				$v[0]=$m."/".$d;
			if($g["invoice_id"])
			{
				$q=mysql_query("SELECT j_inv_total,j_inv_paid FROM nse_invoice WHERE j_inv_id=".$g["invoice_id"]." LIMIT 1");
				if(mysql_num_rows($q))
				{
					$g=mysql_fetch_array($q);
					if($g["j_inv_total"]>0)
						$v[1]=number_format($g["j_inv_total"],2,"."," ");
					if($g["j_inv_paid"]>0)
						$v[2]=number_format($g["j_inv_paid"],2,"."," ");
				}
			}
		}
	}
	return $v;
}

function res_type($e)
{
	global $star;
	$star=0;
	$t="";
	$r="SELECT subcategory_id,star_grading FROM nse_establishment_restype WHERE establishment_code='".$e."' LIMIT 1";
	$r=mysql_query($r);
	if(mysql_num_rows($r))
	{
		$g=mysql_fetch_array($r);
		$star=$g["star_grading"];
		if($g["subcategory_id"])
		{
			$r="SELECT subcategory_name FROM nse_restype_subcategory_lang WHERE subcategory_id=".$g["subcategory_id"]." LIMIT 1";
			$r=mysql_query($r);
			if(mysql_num_rows($r))
			{
				$g=mysql_fetch_array($r);
				$t=$g["subcategory_name"];
				$t=str_ireplace("Bed & Breakfast","B&B",$t);
				$t=str_ireplace("B&B/","B&B",$t);
				$t=str_ireplace("B&B-","B&B",$t);
				$t=str_ireplace("B&B /","B&B",$t);
				$t=str_ireplace("B&B -","B&B",$t);
				$t=str_ireplace("self-catering","Self-Catering",$t);
				$t=str_ireplace("self- catering","Self-Catering",$t);
				$t=str_ireplace("self -catering","Self-Catering",$t);
				$t=str_ireplace("self - catering","Self-Catering",$t);
				$t=str_ireplace("cottages/ chalets","Cottages/Chalets",$t);
				$t=str_ireplace("cottages /chalets","Cottages/Chalets",$t);
				$t=str_ireplace("cottages / chalets","Cottages/Chalets",$t);
				$t=str_ireplace("cottages/chalets","Cottages/Chalets",$t);
				$t=str_ireplace("and/or","/",$t);
				$t=str_ireplace(" With "," with ",$t);
				$t=str_ireplace("/"," / ",$t);
				$t=str_ireplace("  /"," /",$t);
				$t=str_ireplace("/  ","/ ",$t);
				$t=trim(str_replace("  "," ",stripslashes($t)));
			}
		}
	}
	return $t;
}

function people($e)
{
	global $pp;
	$p="";
	$q="SELECT user_id,designation FROM nse_user_establishments WHERE establishment_code='".$e."' LIMIT 20";
	$q=mysql_query($q);
	if(mysql_num_rows($q))
	{
		$a0=0;
		$a1=0;
		$a2=0;
		while($g=mysql_fetch_array($q))
		{
			$d=strtolower($g["designation"]);
			if(strpos($d,"director")!==false || strpos($d,"owner")!==false || strpos($d,"manager")!==false)
				$a1=$g["user_id"];
			elseif(strpos($d,"account")!==false || strpos($d,"books")!==false || strpos($d,"admin")!==false || strpos($d,"marketing")!==false)
				$a0=$g["user_id"];
			else
				$a2=$g["user_id"];
		}
		if($a0)$p=$a0;
		elseif($a1)$p=$a1;
		elseif($a2)$p=$a2;
	}
	if($p && !isset($pp[$p]))
		$pp[$p]=preg_replace("~[^a-zA-Z0-9 \-]~","",trim(J_Value("","people","",$p)));
	return $p;
}

function numbers($e,$p=0,$t=0,$c=0)
{
	global $ema;
	$n="";
	if($p)
	{
		$q="SELECT phone,cell,email FROM nse_user WHERE user_id=".$p." AND (phone!='' OR cell!='') LIMIT 1";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			$g=mysql_fetch_array($q);
			if($g["phone"])
				$n=$g["phone"];
			elseif($g["cell"])
				$n=$g["cell"];
			if($g["email"])
				$ema=$g["email"];
		}
	}
	if(!$n)
	{
		if($t)
			$n=$t;
		elseif($c)
			$n=$c;
		else
		{
			if(!$ema && $e)
			{
				$q=mysql_query("SELECT reservation_tel,reservation_cell,reservation_email FROM nse_establishment_reservation WHERE establishment_code='".$e."' LIMIT 1");
				if(mysql_num_rows($q))
				{
					$g=mysql_fetch_array($q);
					if($g["reservation_tel"])
						$n=$g["reservation_tel"];
					elseif($g["reservation_cell"])
						$n=$g["reservation_cell"];
					if($g["reservation_email"])
						$ema=$g["reservation_email"];
				}
			}
		}
	}
	return preg_replace("~[^a-zA-Z0-9 +\(\)\-]~","",$n);
}

function reservation_details($e)
{
	$values=array("email"=>"","fax"=>"","tel"=>"","cell"=>"");
	$q=mysql_query("SELECT * FROM establishment_public_contact WHERE establishment_code='".$e."' ORDER BY contact_type");
	if(mysql_num_rows($q))
	{
		while($g=mysql_fetch_array($q))
		{
			if($g["contact_value"])
			{
				foreach($values as $k => $v)
				{
					if(!$v && strpos($g["contact_type"],$k)!==false)
						$values[$k]=$g["contact_value"];
				}
			}
		}
	}
	return $values;
}

function awards($e)
{
	return mysql_num_rows(mysql_query("SELECT sfkEstablishmentCode FROM nse_award_establishments WHERE sfkEstablishmentCode='".$e."'"))?"Y":"N";
}

function region($t=0,$c=0)
{
	$v=array("","");
	if($t)
	{
		global $rrg;
		$r=0;
		$q="SELECT * FROM nse_nlocations_cregion_locations
		INNER JOIN nse_nlocations_cregions ON
		nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
		WHERE location_id=".$t."
		AND (parent_type LIKE 'province' OR parent_type LIKE 'region')
		AND location_type LIKE 'town'
		LIMIT 2";
		$q=mysql_query($q);
		if(mysql_num_rows($q))
		{
			while($g=mysql_fetch_array($q))
			{
				if($v[0] && $v[1])
					break;
				elseif($c)
					$v[$r]=strtoupper($g["region_name"]);
				else
				{
					$v[$r]=$g["region_id"];
					if($v[$r]&&!isset($rrg[$v[$r]]))
						$rrg[$v[$r]]=strtoupper($g["region_name"]);
				}
				if(strpos($g["parent_type"],"region")!==false)
					$r=1;
				else
					$r++;
			}
		}
	}
	return $v;
}

?>