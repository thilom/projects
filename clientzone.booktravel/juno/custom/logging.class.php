<?php
/**
 * Activity Logging
 *
 * Logs user activities in the admin panel.
 *
 */
class logging {
    
    function __construct() {
        
    }
    
    /**
     * Write a log entry
     *
     * @param string $message
     * @param int $user_id
     */
    function write_entry($message, $user_id) {
        $statement = "INSERT INTO nse_logs (user_id, log_message, ip_address) VALUES (?, ?, ?)";
        $sql_log = $GLOBALS['dbCon']->prepare($statement);
        $sql_log->bind_param('sss', $user_id, $message, $_SERVER['REMOTE_ADDR']);
        $sql_log->execute();
        $sql_log->close();
        
        return TRUE;
    }
    
}


?>