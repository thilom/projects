<?php

$default = 0;

if ($t == "country") {
	$r = "SELECT * FROM nse_nlocations_countries WHERE country_id>0" . J_Search($v, "country_name") . " ORDER BY country_name LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		while ($g = mysql_fetch_array($r))
			$o.=$g["country_id"] . "~" . J_stripit($g["country_name"]) . "|";
	}
} elseif ($t == "province") {
	$r = "SELECT * FROM nse_nlocations_provinces WHERE province_id>0" . J_Search($v, "province_name") . (isset($_GET["country"]) && $_GET["country"] ? " AND country_id=" . $_GET["country"] : "") . " ORDER BY province_name LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		while ($g = mysql_fetch_array($r))
			$o.=$g["province_id"] . "~" . J_stripit($g["province_name"]) . "|";
	}
} elseif ($t == "town") {
	$r = "SELECT * FROM nse_nlocations_towns WHERE town_id>0" . J_Search($v, "town_name") . (isset($_GET["country"]) && $_GET["country"] ? " AND country_id=" . $_GET["country"] : "") . (isset($_GET["province"]) && $_GET["province"] ? " AND province_id=" . $_GET["province"] : "") . " ORDER BY town_name LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		while ($g = mysql_fetch_array($r))
			$o.=$g["town_id"] . "~" . J_stripit($g["town_name"]) . "|";
	}
} elseif ($t == "suburb") {
	$r = "SELECT * FROM nse_nlocations_suburbs WHERE suburb_id>0" . J_Search($v, "suburb_name") . (isset($_GET["town"]) ? " AND town_id=" . $_GET["town"] : "") . " ORDER BY suburb_name LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		while ($g = mysql_fetch_array($r))
			$o.=$g["suburb_id"] . "~" . J_stripit($g["suburb_name"]) . "|";
	}
} elseif ($t == "region") {
	$r = "SELECT * FROM nse_nlocations_regions WHERE region_id>0" . J_Search($v, "region_name") . " ORDER BY region_name LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		while ($g = mysql_fetch_array($r))
			$o.=$g["region_id"] . "~" . J_stripit($g["region_name"]) . "|";
	}
} elseif ($t == "publications") {
	$r = "SELECT * FROM nse_publications WHERE publication_id>0" . J_Search($v, "publication_name") . " ORDER BY publication_year,publication_name LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		while ($g = mysql_fetch_array($r))
			$o.=$g["publication_id"] . "~" . ($g["publication_year"] ? $g["publication_year"] . " - " : "") . J_stripit($g["publication_name"]) . "|";
	}
} elseif ($t == "publication_item") {
	if (isset($_GET["publication"]))
		$r = "SELECT * FROM nse_inventory JOIN nse_inventory_publications ON j_in_id=j_inp_inventory WHERE j_inp_publication=" . $_GET["publication"] . J_Search($v, "j_in_name");
	else
		$r = "SELECT * FROM nse_inventory WHERE j_in_category>0 AND j_in_category<5" . J_Search($v, "j_in_name");
	$r.=" ORDER BY j_in_name LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		while ($g = mysql_fetch_array($r)) {
			$o.=$g["j_in_id"] . "~";
			if (isset($_GET["size"]) && strpos($g["j_in_name"], " - ") !== false)
				$o.=J_stripit(substr($g["j_in_name"], strpos($g["j_in_name"], " - ") + 3));
			else
				$o.=J_stripit($g["j_in_name"]);
			$o.="|";
		}
	}
}

elseif ($t == "inventory") {
	$r = "SELECT j_in_id,j_in_name,j_in_category,j_inp_publication ";
	$r.="FROM nse_inventory ";
	$r.="LEFT JOIN nse_inventory_publications ON j_in_id=j_inp_inventory ";
	$r.="WHERE j_in_category!=2 ";
	if (($_SESSION["j_user"]["role"] == "a" || $_SESSION["j_user"]["role"] == "r" || $_SESSION["j_user"]["role"] == "c") && isset($_GET["inv"]))
		$r.=" AND j_in_category!=5 AND j_in_category!=6 AND j_in_category!=13 ";
	$r.=J_Search($v, "j_in_name") . " ";
	$r.="ORDER BY j_in_name ";
	$r.="LIMIT 100";
	$r = mysql_query($r);
	if (mysql_num_rows($r)) {
		$p = -1;
		$z = "";
		while ($g = mysql_fetch_array($r)) {
			// compile publication headers
			if ($g["j_in_category"] > 2 && $g["j_in_category"] < 5 && false) {
				if ($g["j_inp_publication"] && $p != $g["j_inp_publication"]) {
					$p = $g["j_inp_publication"];
					$o.="pub" . $g["j_inp_publication"] . "~" . J_stripit(substr($g["j_in_name"], 0, strpos($g["j_in_name"], " - ")));
					$o.="|";
				}
			} else {
				$o.=$g["j_in_id"] . "~";
				$o.=str_replace(" - ", "<br>", J_stripit($g["j_in_name"]));
				$o.="|";
			}
		}
	}
} elseif ($t == "establishment") {
	$found = array();

	$statement = "SELECT a.establishment_code, a.establishment_name, c.town_name
	FROM nse_establishment AS a
	LEFT JOIN nse_search_names AS s ON a.establishment_code=s.establishment_code
	LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
	LEFT JOIN nse_nlocations_towns AS c ON b.town_id=c.town_id ";
	if ($v)
		$statement.="WHERE a.establishment_code!='' AND (a.establishment_name LIKE '!v!' OR s.search_name LIKE '!v!' OR a.establishment_code LIKE '!v!') ";
	if (isset($_GET["constrain"]) && $_GET["constrain"]) { // constrain_user_establishments
		include $SDR . "/custom/constrain_user_establishments.php";
		$statement.=user_establishments_constrain(1, "a.establishment_code");
	}
	$statement.="ORDER BY a.establishment_name LIMIT 100";

	//First iteration
	$statement_first = str_replace('!v!', "$v%", $statement);
	$sql_location = $GLOBALS['dbCon']->prepare($statement_first);
	$sql_location->execute();
	$sql_location->bind_result($establishment_code, $establishment_name, $town_name);
	while ($sql_location->fetch()) {
		$found[] = $establishment_code;
		$o.= $establishment_code . "~" . J_stripit($establishment_name) . " (" . (J_stripit($town_name)) . ")|";
	}
	$sql_location->free_result();
	$sql_location->close();

	//Second Iteration
	$statement_second = str_replace('!v!', "%$v%", $statement);
	$sql_location = $GLOBALS['dbCon']->prepare($statement_second);
	$sql_location->execute();
	$sql_location->bind_result($establishment_code, $establishment_name, $town_name);
	while ($sql_location->fetch()) {
		if (in_array($establishment_code, $found))
			continue;
		$o.= $establishment_code . "~" . J_stripit($establishment_name) . " (" . (J_stripit($town_name)) . ")|";
	}
	$sql_location->free_result();
	$sql_location->close();
}

elseif ($t == "user") {
	$statement = "SELECT DISTINCT u.user_id,u.firstname,u.lastname,e.designation,u.email";
	if (isset($_GET["sho"]) && $_GET["sho"] == "co")
		$statement.=",a.establishment_name";
	$statement.=" FROM nse_user AS u ";
	$statement.="LEFT JOIN nse_user_establishments AS e ON u.user_id=e.user_id ";
	if (isset($_GET["sho"]) && $_GET["sho"] == "co")
		$statement.="LEFT JOIN nse_establishment AS a ON a.establishment_code=e.establishment_code ";
	$statement.="WHERE (u.firstname!='' OR u.lastname!='') ";
	if ($v) {
		$e = $_GET["v"];
		preg_match("~\(.*?\)~", $v, $m); // if there is an email address, isolate it
		if (count($m)) {
			foreach ($m as $k => $p) {
				if ($p && strpos($p, "@") !== false && strpos($p, ".") !== false) {
					$e = preg_replace('~[^a-z0-9 @_\-\.]~', "", strtolower($p));
					$v = trim(str_replace("  ", " ", preg_replace("~\(.*?\)~", "", $v)));
					break;
				}
			}
		}
		$statement.=J_Search($v, "u.firstname~u.lastname~u.email~u.user_id");
	}
	if (isset($_GET["role"])) {
		$statement.="AND ";
		if ($_GET["role"] == 1)
			$statement.="(u.roles LIKE '%s%' OR u.roles LIKE '%d%' OR u.roles LIKE '%b%' OR u.roles LIKE '%a%') ";
		elseif ($_GET["role"] == "s")
			$statement.="(u.roles LIKE '%s%' OR u.roles LIKE '%b%') ";
		elseif ($_GET["role"] == "ar" || $_GET["role"] == "ra")
			$statement.="(u.roles LIKE '%a%' OR u.roles LIKE '%r%') ";
		elseif ($_GET["role"] == "r")
			$statement.="(u.roles LIKE '%a%' OR u.roles LIKE '%r%') ";
		elseif ($_GET["role"] == "a")
			$statement.="u.roles LIKE '%a%' ";
		else
			$statement.="u.roles LIKE '%" . $_GET["role"] . "%' ";
	}
	if (isset($_GET['co']))
		$statement .= "AND e.establishment_code='" . $_GET['co'] . "' ";
	$statement.="ORDER BY u.firstname,u.lastname,e.designation ";
	$statement.="LIMIT 100";
	echo $statement;
	$sql_users = $GLOBALS['dbCon']->prepare($statement);
	$sql_users->execute();
	$sql_users->store_result();
	if (isset($_GET["sho"]) && $_GET["sho"] == "co") {
		$sql_users->bind_result($user_id, $firstname, $lastname, $desig, $establishment);
		while ($sql_users->fetch())
			$o.=$user_id . "~" . J_stripit($firstname . " " . $lastname) . ($desig ? " (" . J_stripit($desig) . ($establishment ? " - " . J_stripit($establishment) : "") . ")" : "") . "|";
	} elseif (isset($_GET["email"])) {
		$sql_users->bind_result($user_id, $firstname, $lastname, $desig, $email);
		while ($sql_users->fetch())
			$o.=$user_id . "~" . J_stripit($firstname . " " . $lastname) . ($email ? " (" . J_stripit($email) . ")" : "") . "|";
	} else {
		$sql_users->bind_result($user_id, $firstname, $lastname, $desig, $email);
		while ($sql_users->fetch())
			$o.=$user_id . "~" . J_stripit($firstname . " " . $lastname) . ($desig ? " (" . J_stripit($desig) . ")" : "") . "|";
	}
	$sql_users->free_result();
	$sql_users->close();
}

else
	$default = 1;
?>