<?php

/**
 * Prints an array in html format
 *
 * @author Thilo Muller(2010)
 * @param array $var
 */
function pretty_print_r($var, $heading='') {
    if (!empty($heading))
			echo "<h2>$heading</h2>";
    if (empty($var)) {
        $data = "Array()";
    } else {
        $data = print_r($var, true);
        $data = str_replace(' ', '&nbsp;', $data);
        $data = nl2br($data);
    }
	echo $data;
    if (!empty($heading)) echo "<hr>";
}

?>