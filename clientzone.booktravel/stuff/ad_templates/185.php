<?php
$ad="";

if(isset($edit))
{
	$scale=(isset($_GET["scale"])?$_GET["scale"]:6);
	$adW=$adWidth*$scale;
	$adH=($adHeight-3)*$scale;
	$ad.="<div class=box style=width:".$adW.";height:".$adH.";position:relative>";

	$ad.="<div id=tab style='position:absolute;left:0;top:0;width:".$adW.";font-size:12pt;padding:1 4 1 4;background:".$color_rgb."'>";
	$ad.="<div id=par style=height:20>";
	$ad.="<div class=rates style='float:right;margin:0 8 0 0'><span onclick=A_ed(this.nextSibling,'rates') onmouseover=\"J_TT(this,'Rates')\">Rates: </span><span onclick=A_ed(this,'rates')><span>".$rates."</span></span></div>";
	$ad.="<b id=region onclick=A_ed(this,'region')><span onmouseover=\"J_TT(this,'Region')\">".$region."</span></b> ";
	$ad.="<b id=town onclick=A_ed(this,'town') onmouseover=\"J_TT(this,'Town or suburb')\"><span>".$town."</span></b>";
	$ad.="</div>";
	$ad.="</div>";

	$ad.="<div id=par style='position:absolute;left:0;top:27;width:264;height:214;font-size:17px;line-height:100%;overflow:hidden'>";

	$ad.="<table width=100% cellspacing=0 cellpadding=0>";

	$ad.="<tr><td onclick=A_ed(this,'establishment_name') style='font-size:14pt;height:24;font-weight:bold;line-height:100%;position:static'><span class=est_name onmouseover=\"J_TT(this,'Establishment name')\">".$establishment_name."</span></td></tr>";

	$ad.="<tr><td style=font-size:12pt;line-height:110%;word-spacing:2 onclick=A_ql(this)>";

	$ad.="<img src=".$ROOT."/stuff/ad_templates/img/AA_rgb.png onclick=A_qal() onmouseover=\"J_TT(this,'Hide QA Logo')\" style='".($qa_logo?"":"display:none;")."height:50;float:left;margin:2 3 0 0;cursor:pointer' id=qalogo>";
	$ad.="<div".($_SESSION["j_user"]["role"]=="d" || $_SESSION["j_user"]["role"]=="s" || $_SESSION["j_user"]["role"]=="b"?" class=con onclick=A_ed(this,'qa_text')":"").">";
	$ad.="<b class=qatext style=word-spacing:-2 onmouseover=\"J_TT(this,'QA Text')\">".($qa_text&&$qa_text!=" "?$qa_text:"?")."</b>";
	$ad.="</div> ";
	$ad.="<div style=text-align:justify>";
	$ad.="<span id=descr class=con onclick=A_ed(this,'description')><span onmouseover=\"J_TT(this,'Description')\">".($description&&$description!=" "?$description:"?")."</span></span>";
	$ad.=" <span id=star class=con".($stars?"":" style=display:none")."><span>".$stars."</span></span>";
	$ad.="</div>";
	$ad.="</td></tr></table>";
	$ad.="</div>";

	$ad.="<div id=img0 onclick=A_img(this) onmousedown=A_MV(this) style=position:absolute;top:30;right:0;width:".($img[0]["conWidth"]*$scale).";height:".($img[0]["conHeight"]*$scale).";overflow:hidden>";
	$ad.="<img src=".$img[0]["src"]." style=position:absolute;left:".($img[0]["left"]*$scale).";top:".($img[0]["top"]*$scale).";height:".($img[0]["height"]*$scale).">";
	$ad.="</div>";

	$ad.="<div class=con id=par onclick=A_ed(this,'address') style=position:absolute;left:0;bottom:0;height:36;width:".$adW.";text-align:center;font-size:11pt;word-spacing:-1;font-weight:bold;line-height:110%;overflow:hidden onmouseover=\"J_TT(this,'Address line')\">";
	$ad.="<span class=addr>".$address."</span>";
	$ad.="</div>";

	$ad.="</div>";
}
else
{
	$adW=89/$mm;
	$adH=50/$mm;

	try {
	$p = new PDFlib();
	$p->set_parameter("licensefile", "/etc/php5/apache2/pdflib_license.txt");
	$p->set_parameter("SearchPath", $SDR."/stuff/ad_templates/fonts");
	$p->set_parameter("SearchPath", $SDR."/stuff/ad_templates/img");
	$p->set_parameter("textformat", "UTF8");
  $p->set_parameter("topdown", "true");
	if($p->begin_document($pdf_filename,"")==0)
		die("Error: " . $p->get_errmsg());

		$p->set_info("Creator", "AA TRAVEL GUIDES");
		$p->set_info("Author", $pdf_author);
		$p->set_info("Title", $pdf_title);

		$p->begin_page_ext($adH, $adW, "");

		$p->set_parameter("textformat", "UTF8");
		$p->set_parameter("FontOutline", "HelveticaCondensedBold=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed-bold.ttf");
		$font = $p->load_font("HelveticaCondensedBold", "unicode", "embedding");
    $p->setfont($font, 24.0);
		$p->set_parameter("FontOutline", "HelveticaCondensed=".$SDR."/stuff/ad_templates/fonts/helvetica-condensed.ttf");
		$font = $p->load_font("HelveticaCondensed", "unicode", "embedding");
    $p->setfont($font, 24.0);

		$c=explode($color_cmyk);
    $p->setcolor("fill", "cmyk", $c[0], $c[1], $c[2], $c[3]);
    $p->rect(0, 0, $adW, 3/$mm);
    $p->fill();

		$region=($region&&$region!="?"?$region:"");
		$region.=($town&&$town!="?"?trim($town):"");
		if($region)
			$p->fit_textline($region, 5/$mm, 5/$mm, "position={left center} kerning=true fontsize=7 fillcolor={cmyk ".$c[0]." ".$c[1]." ".$c[2]." 1}");

		$rates=($rates&&$rates!="?"?"Rates: ".$rates:"&nbsp;");
		if($rates)
			$p->fit_textline($rates, 5/$mm, $adW-(5/$mm), "position={right center} kerning=true fontsize=7 fillcolor={cmyk ".$c[0]." ".$c[1]." ".$c[2]." 1}");

		$i=$p->load_image("auto", $SDR."/stuff/ad_templates/img/AA_cmyk.jpg", "");
		$p->fit_image($i, -1, 5/$mm, "boxsize={25 20} fitmethod=meet position=center matchbox={name=img margin=-3}");
		$p->close_image($i);

		$description=($description&&$description!="?"?$description:"");
		$qa_text=($qa_text&&$qa_text!="?"?$qa_text:"");

		$t=$p->add_textflow(0, $qa_text, "alignment=justify kerning=true fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensedBold encoding=unicode");
		$t=$p->add_textflow($t, $description, "adjustmethod=split alignment=justify kerning=true fontsize=8 kerning=true fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
		$p->fit_textflow($t, 0, 50/$mm, 100/$mm, 100/$mm, "verticalalign=justify linespreadlimit=120% wrap={usematchboxes={{img}}}");

		$i=img4Pdf($img[0]["hirez"],$img[0]["web"],$img[0]["conWidth"],$img[0]["conHeight"],$img[0]["width"],$img[0]["height"],$img[0]["left"],$img[0]["top"],$sharpen);
    $p->fit_image($i, 80/$mm, 80/$mm, "boxsize={".($img[0]["conWidth"]*3.76)." ".($img[0]["conHeight"]*3.76)."} position={center} fitmethod=meet");
    $p->close_image($i);

		$address=($address&&$address!="?"?str_replace(">"," style='height:8;margin:2 0 0 0'>",str_replace($ROOT."/stuff/ad_templates/img/icons_rgb/",$SDR."/stuff/ad_templates/img/icons_cmyk/",$address)):"");
		if($address)
		{
			$t=$p->add_textflow(0, $address, "alignment=center kerning=true fontsize=8 fillcolor={cmyk 0 0 0 1} fontname=HelveticaCondensed encoding=unicode");
			$p->fit_textflow($t, 0, 40/$mm, $adW, 5/$mm, "verticalalign=justify linespreadlimit=120%");
		}

		$p->end_page_ext("");
		$p->end_document("");
	}
	catch (PDFlibException $e) {
					die("PDFlib exception occurred in hello sample:\n" .
					"[" . $e->get_errnum() . "] " . $e->get_apiname() . ": " .
					$e->get_errmsg() . "\n");
	}
	catch (Exception $e) {
					die($e);
	}
	$p = 0;
}

?>
