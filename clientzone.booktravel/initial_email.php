<?php
//Vars
$establishment_code = '';

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/tcrypt.class.php';
$tc = new tcrypt();

//prepare statement - check contacts
$statement = "SELECT COUNT(*) FROM nse_establishment_contacts WHERE establishment_code=? && contact_name=?";
$sql_check = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Prepare statements - add contact
$statement = "INSERT INTO nse_establishment_contacts (establishment_code,contact_name,contact_designation,contact_email,contact_tel,contact_cell) VALUE (?,?,?,?,?,?)";
$sql_insert_contact = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - check users
$statement = "SELECT user_id FROM nse_user WHERE email=?";
$sql_user_check = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - add user
$statement = "INSERT INTO nse_user (email,firstname,lastname,phone,cell,new_password,user_type_id,top_level_type) VALUES (?,?,?,?,?,?,'2','c')";
$sql_user_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - user establishment
$statement = "INSERT INTO nse_user_establishments (user_id,establishment_code) VALUES (?,?)";
$sql_estab_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - estab check
$statement = "SELECT COUNT(*) FROM nse_user_establishments WHERE user_id=? && establishment_code=?";
$sql_estab_check = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - inetstru
$statement = "SELECT CONTACT,DESIGNATN,EMAILADDR,TELNO1,CELLNO FROM inetstru_copy WHERE CODE=?";
$sql_inetstru = $GLOBALS['dbCon']->prepare($statement);


//Start update
$statement = "SELECT establishment_code FROM nse_establishment WHERE aa_estab='Y'";
$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
$sql_estabs->execute();
$sql_estabs->store_result();
$sql_estabs->bind_result($establishment_code);
while ($sql_estabs->fetch()) {
	$counter = 0;
	$user_id = '';
	$contact = '';
	$designation = '';
	$email = '';
	$tel = '';
	$cell = '';
	
	echo $establishment_code . "<br>";
	
	$sql_inetstru->bind_param('s', $establishment_code);
	$sql_inetstru->execute();
	$sql_inetstru->store_result();
	$sql_inetstru->bind_result($contact,$designation,$email,$tel,$cell);
	$sql_inetstru->fetch();
	$sql_inetstru->free_result();
	
	echo "Contact: $contact<br>";
	
	$sql_check->bind_param('ss', $establishment_code, $contact);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($counter);
	$sql_check->fetch();
	$sql_check->free_result();
	
	echo "Counter 1: $counter<br>";
	
	if ($counter == 0) {
		$sql_insert_contact->bind_param('ssssss', $establishment_code, $contact, $designation, $email, $tel, $cell);
		$sql_insert_contact->execute();
	}
	
	$sql_user_check->bind_param('s',$email);
	$sql_user_check->execute();
	$sql_user_check->store_result();
	if ($sql_user_check->num_rows == 0) {
		$space_pos = strpos($contact,' ');
		$firstname = substr($contact, 0, $space_pos);
		$lastname = substr($contact, $space_pos);
		
		//Generate Password
		$password = $GLOBALS['security']->generate_password(6);
		$new_password = $tc->encrypt($password);
		
		$sql_user_insert->bind_param('ssssss', $email, $firstname, $lastname, $tel, $cell, $new_password);
		$sql_user_insert->execute();
		$user_id = $sql_user_insert->insert_id;
		
	} else {
		$sql_user_check->bind_result($user_id);
		$sql_user_check->fetch();
		$sql_user_check->free_Result();
	}

	$counter = 0;
	$sql_estab_check->bind_param('ss', $user_id, $establishment_code);
	$sql_estab_check->execute();
	$sql_estab_check->store_result();
	$sql_estab_check->bind_result($counter);
	$sql_estab_check->fetch();
	$sql_estab_check->free_result();
	
	if ($counter == 0) {
		$sql_estab_insert->bind_param('ss', $user_id, $establishment_code);
		$sql_estab_insert->execute();
	}
}
$sql_estabs->free_result();
$sql_estabs->close();
$sql_inetstru->close();
$sql_check->close();
$sql_user_check->close();
$sql_estab_check->close();
$sql_estab_insert->close();
?>