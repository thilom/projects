<?php



  class security {
	
	function __construct() {
	
	}
	
	/**
	 * Generate a random password.
	 * @param $length Length of the generated password
	 * @return string
	 */
	function generate_password ($length = 8){
		//Vars
		$password = "";
	  	$chars = "0123456789bcdfghjkmnpqrstvwxyz";
	  	$i = 0;
		while ($i < $length) {
	    	$char = substr($chars, mt_rand(0, strlen($chars)-1), 1);
	    	if (!strstr($password, $char)) {
	    		$password .= $char;
	    		$i++;
	   		}
		}
		return $password;
	}
	
	/**
	 * Checks a string and returns TRUE if it is in a valid email address format.
	 * @param $email
	 * @return bool
	 */
	function is_valid_email($email = '') {
		if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $_POST['email'])) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
}

?>