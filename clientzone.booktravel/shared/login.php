<?php
define('SETTINGS',$_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Includes
require_once SETTINGS;

//Constants
define('DESKTOP_GENERATOR', SITE_ROOT . '/admin/desktop.php');

require_once SITE_ROOT.'/modules/login/class.login.php';
$lg_obj = new login();

if (!empty($_POST['submit']) && $_POST['submit'] == 'login') {
	$lg_obj->do_login($_POST['login_username_txt'], $_POST['login_pass_txt']);
}

if (empty($_REQUEST['W1'])) {
	$W1 = "";
} else {
	$W1 = $_REQUEST['W1'];
}

if ($lg_obj->check_login()) {
    
	//Vars
	$roles = '';
	$awebber = '';
	
	//Get last login status
	$statement = "SELECT top_level_type, juno_msg FROM nse_user WHERE user_id=?";
	$sql_awebber = $GLOBALS['dbCon']->prepare($statement);
	$sql_awebber->bind_param('s', $_SESSION['dbweb_user_id']);
	$sql_awebber->execute();
	$sql_awebber->store_result();
	$sql_awebber->bind_result($roles, $awebber);
	$sql_awebber->fetch();
	$sql_awebber->free_result();
	$sql_awebber->close();
		
	echo "<script>P=window.parent;"; // collect the parent
	if ($roles == 'c' && $awebber != 1) {
		echo "P.W('New AA Travel Administration System','/shared/awebber.php',500,600,0,0,2);";
	}
	
	if ($roles == 's') {
		echo "P.W('Dashboard','/shared/dashboard.php',500,500,0,0,0);";
	}
	//echo ";P.WN(1,'Content Manager','/modules/text/i/content.png','/modules/text/start.php',0,560,100,0,2)";
	require DESKTOP_GENERATOR;
	//echo ";P.WN(0,'Settings','F/set.png','/shared/settings.php',0,560)";
	echo ";P.WD(1)"; // WD close window - the id of each window is passed to every window when created
	echo ";P.WD(0)";
	echo "</script>";
	
} else {
	$body = file_get_contents(SITE_ROOT . '/modules/login/html/login_form.html');
	$body = str_ireplace('<!-- $W1 -->', $W1, $body);
	echo $body;
}

?>