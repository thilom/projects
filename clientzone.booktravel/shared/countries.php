<?php

// 203=South Africa

$c0untries=array();
$c0untries[]="International";
$c0untries[]="Afghanistan";
$c0untries[]="Aland Is";
$c0untries[]="Albania";
$c0untries[]="Algeria";
$c0untries[]="American Samoa";
$c0untries[]="Andorra";
$c0untries[]="Angola";
$c0untries[]="Anguilla";
$c0untries[]="Antarctica";
$c0untries[]="Antigua & Barbuda";
$c0untries[]="Argentina";
$c0untries[]="Armenia";
$c0untries[]="Aruba";
$c0untries[]="Australia";
$c0untries[]="Austria";
$c0untries[]="Azerbaijan";
$c0untries[]="Bahamas";
$c0untries[]="Bahrain";
$c0untries[]="Bangladesh";
$c0untries[]="Barbados";
$c0untries[]="Belarus";
$c0untries[]="Belgium";
$c0untries[]="Belize";
$c0untries[]="Benin";
$c0untries[]="Bermuda";
$c0untries[]="Bhutan";
$c0untries[]="Bolivia";
$c0untries[]="Bosnia & Herzegovina";
$c0untries[]="Botswana";
$c0untries[]="Bouvet Is";
$c0untries[]="Brazil";
$c0untries[]="British Indian Ocean Territory";
$c0untries[]="Brunei Darussalam";
$c0untries[]="Bulgaria";
$c0untries[]="Burkina Faso";
$c0untries[]="Burundi";
$c0untries[]="Cambodia";
$c0untries[]="Cameroon";
$c0untries[]="Canada";
$c0untries[]="Cape Verde";
$c0untries[]="Cayman Is";
$c0untries[]="Central African Rep";
$c0untries[]="Chad";
$c0untries[]="Chile";
$c0untries[]="China";
$c0untries[]="Christmas Is";
$c0untries[]="Cocos Is";
$c0untries[]="Colombia";
$c0untries[]="Comoros";
$c0untries[]="Congo";
$c0untries[]="Congo, the DRC";
$c0untries[]="Cook Is";
$c0untries[]="Costa Rica";
$c0untries[]="Cote D`ivoire";
$c0untries[]="Croatia";
$c0untries[]="Cuba";
$c0untries[]="Cyprus";
$c0untries[]="Czech Rep";
$c0untries[]="Denmark";
$c0untries[]="Djibouti";
$c0untries[]="Dominica";
$c0untries[]="Dominican Rep";
$c0untries[]="Ecuador";
$c0untries[]="Egypt";
$c0untries[]="El Salvador";
$c0untries[]="Equatorial Guinea";
$c0untries[]="Eritrea";
$c0untries[]="Estonia";
$c0untries[]="Ethiopia";
$c0untries[]="Falkland Is";
$c0untries[]="Faroe Is";
$c0untries[]="Fiji";
$c0untries[]="Finland";
$c0untries[]="France";
$c0untries[]="French Guiana";
$c0untries[]="French Polynesia";
$c0untries[]="French S. Territories";
$c0untries[]="Gabon";
$c0untries[]="Gambia";
$c0untries[]="Georgia";
$c0untries[]="Germany";
$c0untries[]="Ghana";
$c0untries[]="Gibraltar";
$c0untries[]="Greece";
$c0untries[]="Greenland";
$c0untries[]="Grenada";
$c0untries[]="Guadeloupe";
$c0untries[]="Guam";
$c0untries[]="Guatemala";
$c0untries[]="Guernsey";
$c0untries[]="Guinea";
$c0untries[]="Guinea - Bissau";
$c0untries[]="Guyana";
$c0untries[]="Haiti";
$c0untries[]="Heard & Mcdonald Is";
$c0untries[]="Honduras";
$c0untries[]="Hong Kong";
$c0untries[]="Hungary";
$c0untries[]="Iceland";
$c0untries[]="India";
$c0untries[]="Indonesia";
$c0untries[]="Iran, Islamic Rep of";
$c0untries[]="Iraq";
$c0untries[]="Ireland";
$c0untries[]="Isle of Man";
$c0untries[]="Israel";
$c0untries[]="Italy";
$c0untries[]="Jamaica";
$c0untries[]="Japan";
$c0untries[]="Jersey";
$c0untries[]="Jordan";
$c0untries[]="Kazakhstan";
$c0untries[]="Kenya";
$c0untries[]="Kiribati";
$c0untries[]="Korea, North";
$c0untries[]="Korea, South";
$c0untries[]="Kuwait";
$c0untries[]="Kyrgyzstan";
$c0untries[]="Laos";
$c0untries[]="Latvia";
$c0untries[]="Lebanon";
$c0untries[]="Lesotho";
$c0untries[]="Liberia";
$c0untries[]="Libyan Arab Jamahiriya";
$c0untries[]="Liechtenstein";
$c0untries[]="Lithuania";
$c0untries[]="Luxembourg";
$c0untries[]="Macao";
$c0untries[]="Macedonia";
$c0untries[]="Madagascar";
$c0untries[]="Malawi";
$c0untries[]="Malaysia";
$c0untries[]="Maldives";
$c0untries[]="Mali";
$c0untries[]="Malta";
$c0untries[]="Marshall Is";
$c0untries[]="Martinique";
$c0untries[]="Mauritania";
$c0untries[]="Mauritius";
$c0untries[]="Mayotte";
$c0untries[]="Mexico";
$c0untries[]="Micronesia";
$c0untries[]="Moldova";
$c0untries[]="Monaco";
$c0untries[]="Mongolia";
$c0untries[]="Montenegro";
$c0untries[]="Montserrat";
$c0untries[]="Morocco";
$c0untries[]="Mozambique";
$c0untries[]="Myanmar";
$c0untries[]="Namibia";
$c0untries[]="Nauru";
$c0untries[]="Nepal";
$c0untries[]="Netherlands";
$c0untries[]="Netherlands Antilles";
$c0untries[]="New Caledonia";
$c0untries[]="New Zealand";
$c0untries[]="Nicaragua";
$c0untries[]="Niger";
$c0untries[]="Nigeria";
$c0untries[]="Niue";
$c0untries[]="Norfolk Is";
$c0untries[]="Northern Mariana Is";
$c0untries[]="Norway";
$c0untries[]="Oman";
$c0untries[]="Pakistan";
$c0untries[]="Palau";
$c0untries[]="Palestinian Territory";
$c0untries[]="Panama";
$c0untries[]="Papua New Guinea";
$c0untries[]="Paraguay";
$c0untries[]="Peru";
$c0untries[]="Philippines";
$c0untries[]="Pitcairn";
$c0untries[]="Poland";
$c0untries[]="Portugal";
$c0untries[]="Puerto Rico";
$c0untries[]="Qatar";
$c0untries[]="R�union";
$c0untries[]="Romania";
$c0untries[]="Russian Federation";
$c0untries[]="Rwanda";
$c0untries[]="St Barth�lemy";
$c0untries[]="St Helena";
$c0untries[]="St Kitts & Nevis";
$c0untries[]="St Lucia";
$c0untries[]="St Martin";
$c0untries[]="St Pierre & Miquelon";
$c0untries[]="St Vincent & Grenadines";
$c0untries[]="Samoa";
$c0untries[]="San Marino";
$c0untries[]="Sao Tome & Principe";
$c0untries[]="Saudi Arabia";
$c0untries[]="Senegal";
$c0untries[]="Serbia";
$c0untries[]="Seychelles";
$c0untries[]="Sierra Leone";
$c0untries[]="Singapore";
$c0untries[]="Slovakia";
$c0untries[]="Slovenia";
$c0untries[]="Solomon Is";
$c0untries[]="Somalia";
$c0untries[]="South Africa";
$c0untries[]="South Georgia & Sandwich Is";
$c0untries[]="Spain";
$c0untries[]="Sri Lanka";
$c0untries[]="Sudan";
$c0untries[]="Suriname";
$c0untries[]="Svalbard & Jan Mayen";
$c0untries[]="Swaziland";
$c0untries[]="Sweden";
$c0untries[]="Switzerland";
$c0untries[]="Syrian Arab Rep";
$c0untries[]="Taiwan, Province of China";
$c0untries[]="Tajikistan";
$c0untries[]="Tanzania";
$c0untries[]="Thailand";
$c0untries[]="Timor - Leste";
$c0untries[]="Togo";
$c0untries[]="Tokelau";
$c0untries[]="Tonga";
$c0untries[]="Trinidad & Tobago";
$c0untries[]="Tunisia";
$c0untries[]="Turkey";
$c0untries[]="Turkmenistan";
$c0untries[]="Turks & Caicos Is";
$c0untries[]="Tuvalu";
$c0untries[]="Uganda";
$c0untries[]="Ukraine";
$c0untries[]="United Arab Emirates";
$c0untries[]="United Kingdom";
$c0untries[]="United States";
$c0untries[]="United States Minor Is";
$c0untries[]="Uruguay";
$c0untries[]="Uzbekistan";
$c0untries[]="Vanuatu";
$c0untries[]="Vatican City State";
$c0untries[]="Venezuela";
$c0untries[]="Viet Nam";
$c0untries[]="Virgin Is. British";
$c0untries[]="Virgin Is. U.S.";
$c0untries[]="Wallis & Futuna";
$c0untries[]="Western Sahara";
$c0untries[]="Yemen";
$c0untries[]="Zambia";
$c0untries[]="Zimbabwe";

?>