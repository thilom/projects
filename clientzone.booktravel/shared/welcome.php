<?php

echo "<html>";
echo "<script src=/shared/G.js></script>";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/admin_style.php";
echo $css;
?>

<body>
<b>AA Travel Guides Administration</b><br />&nbsp;<br />
<table align=left><tr><td><img src='/i/icons-help.png' /></td></tr></table>
You can move windows around, resize, maximise or minimize them. <p><span style='color: brown' >As pseudo-windows are used, the use of browser back and forward buttons is discouraged</span>.</p>

<hr>
<table><tr><td><b>Tip</b><br />For more space, hit F11 for full screen mode. This will maximise the working area inside the browser. Hit F11 again to return to normal working mode.</td></tr></table>

<br>
</body>
</html>