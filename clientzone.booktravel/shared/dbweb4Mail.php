<?php

/**
 * class dbweb4Mail
 * 
 */
class dbweb4Mail
{
	private $version = '';	
	private $boundry = array();	
	private $mailObject = array();

	function dbweb4Mail() {
		$this->version = '20070410';
		$this->mailObject = array();
		$this->boundry = array();
		$this->boundry[0] = MD5(time() . 'A1');
		$this->boundry[1] = MD5(time() . 'B2');
		$this->mailObject['header']['X-Mailer'] = 'dbweb4Mail v.' . $this->version;
		$this->mailObject['header']['MIME-Version'] = '1.0';	
	}
	

	/**
	 * 
	 *
	 * @return bool
	 * @access public
	 */
	function clearMail( )
	{
		$this->mailObject = array();
	} // end of member function clearMail

	/**
	 * 
	 *
	 * @param string message 
	 * @param string templateFile 
	 * @param array templateValues 
	 * @return bool
	 * @access public
	 */
	function addText( $message,  $templateFile = '',  $templateValues = '' )
	{
		if (empty($message)) {
			$this->mailObject['body']['text'] = dbweb4Mail::populateTemplate($templateFile, $templateValues);
		} else {
			$this->mailObject['body']['text'] = $message;
		}
	} // end of member function addText

	/**
	 * 
	 *
	 * @param string message 
	 * @param string templateFile 
	 * @param string templateValues 
	 * @return bool
	 * @access public
	 */
	function addHTML( $message,  $templateFile = '',  $templateValues = '' )
	{
		if (empty($message)) {
			$this->mailObject['body']['html'] = dbweb4Mail::populateTemplate($templateFile, $templateValues);
		} else {
			$this->mailObject['body']['html'] = $message;
		}
	} // end of member function addHTML

	/**
	 * 
	 *
	 * @param string attachmentFile 
	 * @return bool
	 * @access public
	 */
	function addAttachment( $attachmentFile = '', $fileName='')
	{
		$pathinfo = pathinfo($attachmentFile);
		if (empty($fileName)) $fileName = $pathinfo['basename'];
		$nextKey = count($this->mailObject['attachment']);
		$this->mailObject['attachment'][$nextKey]['mimeType'] = dbweb4Mail::setMIME($pathinfo['extension']);
		$file = fopen($attachmentFile,'rb');
		 $data = fread($file,filesize($attachmentFile));
		 fclose($file);
		 $this->mailObject['attachment'][$nextKey]['data'] = chunk_split(base64_encode($data));
		 $this->mailObject['attachment'][$nextKey]['fileName'] = $fileName;
	} // end of member function addAttachments

	/**
	 * 
	 *
	 * @return string
	 * @access public
	 */
	function getVersion( )
	{
		return $this->version;
	} // end of member function getVersion

	/**
	 * 
	 *
	 * @param int maxSize 
	 * @return bool
	 * @access public
	 */
	function setMaxAttachmentSize(  )
	{
		
	} // end of member function setMaxAttachmentSize

	/**
	 * 
	 *
	 * @param string templateFile 
	 * @param string templateValues 
	 * @return string
	 * @access private
	 */
	function populateTemplate( $templateFile = '',  $templateValues )
	{
		$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] .  $templateFile);
		foreach ($templateValues as $tag=>$value) {
			$data = str_replace('<%' . $tag . '%>', $value, $data);	
		}
		return $data;
	} // end of member function populateTemplate

	
	
	
	/**
	 * Set the To Address
	 */
	function setTo($setAddress, $realName='')
	{
		$this->mailObject['to'] = "$realName <$setAddress>";
	}

	
	/**
	 * Set the From Address
	 */
	function setFrom($setAddress, $realName = '')
	{
		$this->mailObject['header']['From'] = "$realName <$setAddress>";
		$this->mailObject['header']['Return-Path'] = $setAddress;
	}

	
	/**
	 * Set the Subject
	 */
	function setSubject( $subject)
	{
		$this->mailObject['subject'] = $subject;
	}
	
	
	/**
	 * Set the Mime Type
	 */
	function setMIME($suffix)
	{
		$mimetypes = array(
		    'ez'        => 'application/andrew-inset',
		    'hqx'        => 'application/mac-binhex40',
		    'cpt'        => 'application/mac-compactpro',
		    'doc'        => 'application/msword',
		    'bin'        => 'application/octet-stream',
		    'dms'        => 'application/octet-stream',
		    'lha'        => 'application/octet-stream',
		    'lzh'        => 'application/octet-stream',
		    'exe'        => 'application/octet-stream',
		    'class'        => 'application/octet-stream',
		    'so'        => 'application/octet-stream',
		    'dll'        => 'application/octet-stream',
		    'oda'        => 'application/oda',
		    'pdf'        => 'application/pdf',
		    'ai'        => 'application/postscript',
		    'eps'        => 'application/postscript',
		    'ps'        => 'application/postscript',
		    'smi'        => 'application/smil',
		    'smil'        => 'application/smil',
		    'mif'        => 'application/vnd.mif',
		    'xls'        => 'application/vnd.ms-excel',
		    'ppt'        => 'application/vnd.ms-powerpoint',
		    'wbxml'        => 'application/vnd.wap.wbxml',
		    'wmlc'        => 'application/vnd.wap.wmlc',
		    'wmlsc'        => 'application/vnd.wap.wmlscriptc',
		    'bcpio'        => 'application/x-bcpio',
		    'vcd'        => 'application/x-cdlink',
		    'pgn'        => 'application/x-chess-pgn',
		    'cpio'        => 'application/x-cpio',
		    'csh'        => 'application/x-csh',
		    'dcr'        => 'application/x-director',
		    'dir'        => 'application/x-director',
		    'dxr'        => 'application/x-director',
		    'dvi'        => 'application/x-dvi',
		    'spl'        => 'application/x-futuresplash',
		    'gtar'        => 'application/x-gtar',
		    'hdf'        => 'application/x-hdf',
		    'js'        => 'application/x-javascript',
		    'skp'        => 'application/x-koan',
		    'skd'        => 'application/x-koan',
		    'skt'        => 'application/x-koan',
		    'skm'        => 'application/x-koan',
		    'latex'        => 'application/x-latex',
		    'nc'        => 'application/x-netcdf',
		    'cdf'        => 'application/x-netcdf',
		    'sh'        => 'application/x-sh',
		    'shar'        => 'application/x-shar',
		    'swf'        => 'application/x-shockwave-flash',
		    'sit'        => 'application/x-stuffit',
		    'sv4cpio'    => 'application/x-sv4cpio',
		    'sv4crc'    => 'application/x-sv4crc',
		    'tar'        => 'application/x-tar',
		    'tcl'        => 'application/x-tcl',
		    'tex'        => 'application/x-tex',
		    'texinfo'    => 'application/x-texinfo',
		    'texi'        => 'application/x-texinfo',
		    't'            => 'application/x-troff',
		    'tr'        => 'application/x-troff',
		    'roff'        => 'application/x-troff',
		    'man'        => 'application/x-troff-man',
		    'me'        => 'application/x-troff-me',
		    'ms'        => 'application/x-troff-ms',
		    'ustar'        => 'application/x-ustar',
		    'src'        => 'application/x-wais-source',
		    'xhtml'        => 'application/xhtml+xml',
		    'xht'        => 'application/xhtml+xml',
		    'zip'        => 'application/zip',
		    'au'        => 'audio/basic',
		    'snd'        => 'audio/basic',
		    'mid'        => 'audio/midi',
		    'midi'        => 'audio/midi',
		    'kar'        => 'audio/midi',
		    'mpga'        => 'audio/mpeg',
		    'mp2'        => 'audio/mpeg',
		    'mp3'        => 'audio/mpeg',
		    'aif'        => 'audio/x-aiff',
		    'aiff'        => 'audio/x-aiff',
		    'aifc'        => 'audio/x-aiff',
		    'm3u'        => 'audio/x-mpegurl',
		    'ram'        => 'audio/x-pn-realaudio',
		    'rm'        => 'audio/x-pn-realaudio',
		    'rpm'        => 'audio/x-pn-realaudio-plugin',
		    'ra'        => 'audio/x-realaudio',
		    'wav'        => 'audio/x-wav',
		    'pdb'        => 'chemical/x-pdb',
		    'xyz'        => 'chemical/x-xyz',
		    'bmp'        => 'image/bmp',
		    'gif'        => 'image/gif',
		    'ief'        => 'image/ief',
		    'jpeg'        => 'image/jpeg',
		    'jpg'        => 'image/jpeg',
		    'jpe'        => 'image/jpeg',
		    'png'        => 'image/png',
		    'tiff'        => 'image/tiff',
		    'tif'        => 'image/tiff',
		    'djvu'        => 'image/vnd.djvu',
		    'djv'        => 'image/vnd.djvu',
		    'wbmp'        => 'image/vnd.wap.wbmp',
		    'ras'        => 'image/x-cmu-raster',
		    'pnm'        => 'image/x-portable-anymap',
		    'pbm'        => 'image/x-portable-bitmap',
		    'pgm'        => 'image/x-portable-graymap',
		    'ppm'        => 'image/x-portable-pixmap',
		    'rgb'        => 'image/x-rgb',
		    'xbm'        => 'image/x-xbitmap',
		    'xpm'        => 'image/x-xpixmap',
		    'xwd'        => 'image/x-xwindowdump',
		    'igs'        => 'model/iges',
		    'iges'        => 'model/iges',
		    'msh'        => 'model/mesh',
		    'mesh'        => 'model/mesh',
		    'silo'        => 'model/mesh',
		    'wrl'        => 'model/vrml',
		    'vrml'        => 'model/vrml',
		    'css'        => 'text/css',
		    'html'        => 'text/html',
		    'htm'        => 'text/html',
		    'asc'        => 'text/plain',
		    'txt'        => 'text/plain',
		    'rtx'        => 'text/richtext',
		    'rtf'        => 'text/rtf',
		    'sgml'        => 'text/sgml',
		    'sgm'        => 'text/sgml',
		    'tsv'        => 'text/tab-separated-values',
		    'wml'        => 'text/vnd.wap.wml',
		    'wmls'        => 'text/vnd.wap.wmlscript',
		    'etx'        => 'text/x-setext',
		    'xsl'        => 'text/xml',
		    'xml'        => 'text/xml',
		    'mpeg'        => 'video/mpeg',
		    'mpg'        => 'video/mpeg',
		    'mpe'        => 'video/mpeg',
		    'qt'        => 'video/quicktime',
		    'mov'        => 'video/quicktime',
		    'mxu'        => 'video/vnd.mpegurl',
		    'avi'        => 'video/x-msvideo',
		    'movie'        => 'video/x-sgi-movie',
		    'ice'        => 'x-conference/x-cooltalk',
		);
		return $mimetypes[$suffix];
	}

	
	/**
	 * Send the email
	 */
	function sendMail($logMember='',  $sendCron=false)
	{
		$matches = '';
		preg_match_all("/<[^>]*\n?.*=(\"|\')?(.*\.gif|.jpg)(\"|\')?.*\n?[^<]*>/", $this->mailObject['body']['html'], $matches);
		$inlineImages = $matches[2];
		
		if (isset($this->mailObject['body']['text']) && !isset($this->mailObject['body']['html']) && !isset($this->mailObject['attachment'])) {
			// -= TEXT ONLY =- //
			$header = '';
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			
			$message .= PHP_EOL . $this->mailObject['body']['text'];
		} else if (isset($this->mailObject['body']['text']) && !isset($this->mailObject['body']['html']) && isset($this->mailObject['attachment'])) {
			// -=  TEXT, ATTACHMENT =- //
			$header = '';
			$this->mailObject['header']['Content-Type'] = "multipart/mixed; boundary=\"" . $this->boundry[0] . "\"";
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			$message = PHP_EOL . "--" . $this->boundry[0];
			$message .= PHP_EOL . "Content-Type: text/plain; charset=\"iso-8859-1\"";
			$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL; 
			$message .= PHP_EOL . $this->mailObject['body']['text'];
			foreach ($this->mailObject['attachment'] as $value) {
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type:" . $value['mimeType'] . "; name=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Disposition: attachment;  filename=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: base64" . PHP_EOL;
				$message .= PHP_EOL . $value['data'];
			}
			$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
		} else if (isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && isset($this->mailObject['attachment'])) {
			// -= TEXT, HTML, ATTACHMENT =- //
			$header = '';
			$this->mailObject['header']['Content-Type'] = "multipart/mixed; boundary=\"" . $this->boundry[0] . "\"";
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			$message = "--" . $this->boundry[0];
			$message .= PHP_EOL . "Content-Type: multipart/alternative; boundary=\"".$this->boundry[1]."\"" . PHP_EOL;
			$message .= PHP_EOL . "--" . $this->boundry[1];
			$message .= PHP_EOL . "Content-Type: text/plain; charset=ISO-8859-1";
			$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
			$message .= PHP_EOL . $this->mailObject['body']['text'];
			$message .= PHP_EOL . "--". $this->boundry[1];
			$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
			$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
			$message .= PHP_EOL . $this->mailObject['body']['html'];
			$message .= PHP_EOL . "--" . $this->boundry[1] . "--";
			foreach ($this->mailObject['attachment'] as $value) {
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type:" . $value['mimeType'] . "; name=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Disposition: attachment;  filename=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: base64" . PHP_EOL;
				$message .= PHP_EOL . $value['data'];
			}
			$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
		} else if (!isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && !isset($this->mailObject['attachment'])) {
			// -= HTML ONLY =- //
			if (count($inlineImages) != 0) {
				foreach($inlineImages as $key=>$value) {
					$fileinfo = pathinfo($value);
					$mimeType = setMIME($fileinfo['extension']);
					$imageCID = time() . $key . '@chrome.co.za';
					$this->mailObject['body']['html'] = str_replace($value, 'cid:' . $imageCID, $this->mailObject['body']['html']);
					$base64[$key] = PHP_EOL . "--" . $this->boundry[0];
					$base64[$key] .= PHP_EOL . "Content-Type: ". $mimeType ."; name=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL . "Content-Transfer-Encoding: base64";
					$base64[$key] .= PHP_EOL . "Content-ID: <$imageCID>";
					$base64[$key] .= PHP_EOL . "Content-Disposition: inline;  filename=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL;
					$base64[$key] .= PHP_EOL;
					$file = fopen($_SERVER['DOCUMENT_ROOT'] . $value,'rb');
					$data = fread($file,filesize($_SERVER['DOCUMENT_ROOT'] . $value));
					fclose($file);
					$base64[$key] .= chunk_split(base64_encode($data));
					$base64[$key] .= PHP_EOL; 
				}
				$this->mailObject['header']['Content-Type'] = 'multipart/related; boundary="'. $this->boundry[0] .'"';
				$message = PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				foreach ($base64 as $value) {
					$message .= $value;	
				}
				$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
			} else {
				$this->mailObject['header']['Content-Type'] = 'text/html';
				$message .= $this->mailObject['body']['html'];
			}
			$header = '';
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
		} else if (!isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && isset($this->mailObject['attachment'])) {
			// -= HTML, ATTACHMENT=- //
			$header = '';
			$this->mailObject['header']['Content-Type'] = "multipart/mixed; boundary=\"" . $this->boundry[0] . "\"";
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			$message = PHP_EOL . "--" . $this->boundry[0];
			if (count($inlineImages) == 0) {
				$message .= PHP_EOL . "Content-Type: text/html; charset=\"iso-8859-1\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL; 
				$message .= PHP_EOL . $this->mailObject['body']['html'];
			} else {
				$message .= PHP_EOL . "Content-Type: multipart/related; boundary=\"". $this->boundry[1] ."\"";
				$message .= PHP_EOL;
				$message .= PHP_EOL . "--" . $this->boundry[1];
				foreach($inlineImages as $key=>$value) {
					$fileinfo = pathinfo($value);
					$mimeType = setMIME($fileinfo['extension']);
					$imageCID = time() . $key . '@imcsa.org.za';
					$this->mailObject['body']['html'] = str_replace($value, 'cid:' . $imageCID, $this->mailObject['body']['html']);
					$base64[$key] = PHP_EOL . "--" . $this->boundry[1];
					$base64[$key] .= PHP_EOL . "Content-Type: ". $mimeType ."; name=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL . "Content-Transfer-Encoding: base64";
					$base64[$key] .= PHP_EOL . "Content-ID: <$imageCID>";
					$base64[$key] .= PHP_EOL . "Content-Disposition: inline;  filename=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL;
					$base64[$key] .= PHP_EOL;
					$file = fopen($_SERVER['DOCUMENT_ROOT'] . $value,'rb');
					$data = fread($file,filesize($_SERVER['DOCUMENT_ROOT'] . $value));
					fclose($file);
					$base64[$key] .= chunk_split(base64_encode($data));
					$base64[$key] .= PHP_EOL; 
				}
				//$message .= "\r\n--" . $this->boundry[1];
				$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				foreach ($base64 as $value) {
					$message .= $value;	
				}
				$message .= PHP_EOL . "--" . $this->boundry[1] . "--";
			}
			foreach ($this->mailObject['attachment'] as $value) {
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type:" . $value['mimeType'] . "; name=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Disposition: attachment;  filename=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: base64" . PHP_EOL;
				$message .= PHP_EOL . $value['data'];
			}
			$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
		} else if  (isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && !isset($this->mailObject['attachment'])) {
			// -=TEXT, HTML=- //
			if (count($inlineImages) == 0) {
				$this->mailObject['header']['Content-Type'] = 'multipart/alternative; boundary=' . $this->boundry[0];
				$header = '';
				foreach ($this->mailObject['header'] as $key=>$value) {
					$header .= "$key: $value" . PHP_EOL;
				}
				$message = PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/plain; charset='iso-8859-1'";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
				$message .= PHP_EOL . $this->mailObject['body']['text'];
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/html; charset='iso-8859-1'";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
			} else {
				$this->mailObject['header']['Content-Type'] = 'multipart/alternative; boundary=' . $this->boundry[0];
				$header = '';
				foreach ($this->mailObject['header'] as $key=>$value) {
					$header .= "$key: $value" . PHP_EOL;
				}
				$message = PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/plain; charset='iso-8859-1'; format=flowed";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['text'];
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: multipart/related; boundary=" . $this->boundry[1] ;
				$message .= PHP_EOL;
				$message .= PHP_EOL . "--" . $this->boundry[1];
				foreach($inlineImages as $key=>$value) {
					$fileinfo = pathinfo($value);
					$mimeType = setMIME($fileinfo['extension']);
					$imageCID = time() . $key . '@imcsa.org.za';
					$this->mailObject['body']['html'] = str_replace($value, 'cid:' . $imageCID, $this->mailObject['body']['html']);
					$base64[$key] = PHP_EOL . "--" . $this->boundry[1];
					$base64[$key] .= PHP_EOL . "Content-Type: ". $mimeType ."; name=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL . "Content-Transfer-Encoding: base64";
					$base64[$key] .= PHP_EOL . "Content-ID: <$imageCID>";
					$base64[$key] .= PHP_EOL . "Content-Disposition: inline;  filename=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL;
					$base64[$key] .= PHP_EOL;
					$file = fopen($_SERVER['DOCUMENT_ROOT'] . $value,'rb');
					$data = fread($file,filesize($_SERVER['DOCUMENT_ROOT'] . $value));
					fclose($file);
					$base64[$key] .= chunk_split(base64_encode($data));
					$base64[$key] .= PHP_EOL; 
				}
				//$message .= "\r\n";
				$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				foreach ($base64 as $value) {
					$message .= $value;	
				}
				$message .= PHP_EOL . "--" . $this->boundry[1] . "--" . PHP_EOL;
				$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
				
				//echo $message;
			}
		} else {}
		
		if ($sendCron) {
			$attachments = '';
			foreach ($this->mailObject['attachment'] as $value) {
				$attachments .= $_SERVER['DOCUMENT_ROOT'] .'/'. $value['fileName']  .',';
			}
			$attachments = substr($attachments, 0, -1);
			if (mysql_query("INSERT INTO chromeMail_cron (memberID, emailTo, headers, subject, bodyText, bodyHTML, attachments, message) 
			VALUES ('$logMember', '{$this->mailObject['to']}', '$header', '{$this->mailObject['subject']}', '{$this->mailObject['body']['text']}', '{$this->mailObject['body']['html']}', '$attachments', '$message')")) {
				return true;
			} else {
				return false;	
			}
		} else {
			if (mail($this->mailObject['to'], $this->mailObject['subject'], $message, $header)) {
				$attachments = '';
				foreach ($this->mailObject['attachment'] as $value) {
					$attachments .= $_SERVER['DOCUMENT_ROOT'] .'/'. $value['fileName']  .',';
				}
				$attachments = substr($attachments, 0, -1);
				//logMail($this->mailObject['to'], $this->mailObject['subject'], $this->mailObject['body']['text'], $this->mailObject['body']['html'],  $attachments, $header, $logMember);
				
			return true;
			} else {
				return false;
			}
		}
	}


	
} // end of chromeMail
?>
