<?php
/**
 * File manipulation class. Currently changes the file permissions using several different method. First tries using the
 * PHP built-in chmod function. If it fails it will try to use the FTP method.
 *
 * @package DBWeb4
 */
class file_man {
	private $error = '';
	private $settings = array();
	
	public function __construct() {
		$SQL_statement  = "SELECT tag, value FROM dbweb_settings";
		$SQL_settings = mysql_query($SQL_statement);
		while(list($tag, $value) = mysql_fetch_array($SQL_settings)) {
			$this->settings[$tag] = stripslashes($value);
		}
	}
	
	/**
	 *Attempt to change the mode of a given file.
	 *
	 * @access public
	 * @param string $file
	 * @param int $mode
	 * @return bool
	 */
	public function dbweb_chmod($file, $mode) {
		//Initialize Variables
		$full_name = $_SERVER['DOCUMENT_ROOT'] . "/$file";
		
		//Get current permisions
		$permission = @fileperms($full_name);
		$permission = substr(sprintf('%o',$permission), -4);
		
		//Check if permission needs to be changed
		if ($permission == $mode) {
			$this->error = "Already in requested mode.";
			return FALSE;
		}
		
		//Try with chmod
		if (function_exists('chmod')) {
			if (@chmod($full_name, $mode))  {
				$permission = fileperms($full_name);
				$permission = substr(sprintf('%o',$permission), -4);
				if ($permission == $mode) {
					return TRUE;
				}
			}
		}

		
		//Try FTP
		if ($ftp_handle = ftp_connect($this->settings['ftp_host'])) {
			ftp_login($ftp_handle, $this->settings['ftp_username'], $this->settings['ftp_password']);
			@ftp_chmod($ftp_handle, $mode, $this->settings['ftp_dir'] . $file);
			$permission = @fileperms($full_name);
			$permission = substr(sprintf('%o',$permission), -4);
			if (base_convert($permission,8,8) == base_convert($mode, 10, 8)) {
				return true;
			}
		}
	}
	
	/**
	 * Create an empty file. First atempts to create it via fopen and then via FTP.
	 *
	 * @param string $dir
	 * @param string $file
	 * @return bool
	 */
	public function create_file($dir, $file) {
		//Initialize Variables
		$remote_location = '';
		$local_tmp_location = '';
		$local_location = '';

		//Assemble Local Temp. Locations
		$local_tmp_location .= SITE_ROOT;
		$local_tmp_location .= substr($local_tmp_location, -1) == '/'?"tmp/":"/tmp/" ;
		$local_tmp_location .= $file;
		
		//Assemble Remote Location
		$remote_location .= $this->settings['ftp_dir'];
		if (substr($remote_location, -1)=='/' && substr($dir, 0, 1)=='/') {
			$remote_location .= substr($dir, 1);
		} else if (substr($remote_location, -1)!='/' && substr($dir, 0, 1)!='/') {
			$remote_location .= "/".$dir;
		} else {
			$remote_location .= $dir;
		}
		$remote_location .= substr($dir, -1)=='/'?"$file":"/$file";
		
		//Assemble Local Location
		$local_location .= SITE_ROOT;
		if (substr($dir, 0, 1)=='/' && substr($local_location, -1)=='/') {
			$local_location .= substr($dir, 1);
		} else if (substr($dir, 0, 1)!='/' && substr($local_location, -1)!='/') {
			$local_location .= '/'.$dir;
		} else {
			$local_location .= $dir;
		}
		$local_location .= substr($local_location, -1)=='/'?"$file":"/$file";
		
		//Check if file already exists
		if (is_file($local_location)) return FALSE;
		
		//Try to create it locally
		if (@fopen($local_location, 'a')) return TRUE;
		
		//Create a temp instance of the file
		$fh = fopen($local_tmp_location, 'a');
		 
		if ($ftp_handle = ftp_connect($this->settings['ftp_host'])) {
			ftp_login($ftp_handle, $this->settings['ftp_username'], $this->settings['ftp_password']);
			ftp_fput($ftp_handle, $remote_location, $fh, FTP_ASCII);
		}
		
		//Remove the file instance
		fclose($fh);
		unlink($local_tmp_location);
		
		//Check if file exists now
		if (is_file($local_location)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function create_dir($base_dir, $new_dir) {
	//Initialize Variables
		$remote_location = '';
		$local_tmp_location = '';
		$local_location = '';
		
		//Assemble Remote Location
		$remote_location .= $this->settings['ftp_dir'];
		if (substr($remote_location, -1)=='/' && substr($base_dir, 0, 1)=='/') {
			$remote_location .= substr($base_dir, 1);
		} else if (substr($remote_location, -1)!='/' && substr($base_dir, 0, 1)!='/') {
			$remote_location .= "/".$base_dir;
		} else {
			$remote_location .= $base_dir;
		}
		$remote_location .= substr($base_dir, -1)=='/'?"$new_dir":"/$new_dir";
		
		
		//Check if file already exists
		if (is_file($local_location)) return FALSE;
		
		//Try to create it locally
		if (@fopen($local_location, 'a')) return TRUE;
		
		//Create a temp instance of the file
		$fh = fopen($local_tmp_location, 'a');
		 
		if ($ftp_handle = ftp_connect($this->settings['ftp_host'])) {
			ftp_login($ftp_handle, $this->settings['ftp_username'], $this->settings['ftp_password']);
			@ftp_mkdir($ftp_handle,$remote_location);
		}
	}
	
	/**
	 * Return the last generated error.
	 *
	 * @return unknown
	 */
	public function get_error() {
		return $this->error;
	}
}



?>