<?php
/**
 * Modularus Dashboard
 *
 * Iterates through the '/modules' directory looking for 'dashboard.php'. If found
 * the result of the file is inserted into the darhboard.
 *
 * @author Thilo Muller(2009)
 * @package modularus
 * @category feedback
 */

//Vars
$module_name = '';
$template = "<table class=dBo cellspacing=0 cellpadding=2><tr><th><!-- module_name --></th></tr><tr><td><!-- module_content --></td></tr></table><br />";
$nLine = '';

//Settings
define('SETTINGS',$_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');
require_once SETTINGS;
echo "<link rel=stylesheet href=/shared/dashboard.css>";

$dir = SITE_ROOT . '/modules/';

if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if (is_dir($dir . $file)) {
                $dashboard = '';
            	if (!file_exists($dir.$file.'/module.ini.php') || !file_exists($dir.$file.'/dashboard.php')) continue; //Check if dashboard.php exists
            	include($dir.$file.'/module.ini.php');
            	ob_start();
            	include $dir.$file.'/dashboard.php';
            	$dashboard = ob_get_clean();
            	
            	if (empty($dashboard)) continue;
            	
            	$line = str_replace('<!-- module_name -->', $module_name, $template);
            	$line = str_replace('<!-- module_content -->', $dashboard, $line);
            	$nLine .= $line;
            }
        }
        closedir($dh);
    }
}

echo $nLine;
?>