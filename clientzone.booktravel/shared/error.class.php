<?php
/**
 * DBWeb4 Error Class
 * 
 * This is an abstract class which is used for error handling.
 * 
 * @author Thilo Muller
 * @copyright DBWeb4 - 2008
 * @version 1.0.0
 * 
 */

abstract class error {
	private $error_buffer = array ( );
	private $error_count = 0;
	private $error_file = '';
	private $max_error = '';
	
	function __construct() {
		echo "REMOVE ERROR REPORTING";
	}
	
	/**
	 * Remove all errors from the error buffer
	 *
	 * @return boolean
	 */
	public function clear_error_buffer() {
		$this->error_buffer = array();
		return true;
	}
	
	/**
	 * Returns the latest error in the error buffer
	 *
	 */
	public function get_last_error() {
		
	}
	
	/**
	 * Return all errors in the error buffer
	 *
	 * @param string $style 'js', 'inline', 'db',''ile'or 'raw'
	 */
	public function get_all_errors($style = 'js') {
		switch ($style) {
			case 'js':
				return $this->assemble_js_errors();
				break;
			case 'html':
				return $this->assemble_inline_errors();
				break;
			case 'raw':
				return $this->error_buffer;
				break;
			case 'file':
				return $this->write_error_to_file();
				break;
			case 'db':
				return $this->write_error_to_db();
				break;
		}
		return false;
	}
	
	/**
	 * Report an error and add it to the error buffer
	 *
	 */
	public function report_error($error_level, $error_message, $file, $line) {
		$this->error_buffer[$this->error_count]['error_level'] = $error_level;
		$this->error_buffer[$this->error_count]['error_message'] = $error_message;
		$this->error_buffer[$this->error_count]['file'] = $file;
		$this->error_buffer[$this->error_count]['line'] = $line;
		$this->error_count++;
	}
	
	/**
	 * Set the file to be used for error reporting to a file
	 *
	 * @param string $file
	 * @return boolean
	 */
	public function set_error_file($file) {
		$this->error_file = $file;
		return true;
	}
	
	/**
	 * Sets the maximum number of errors that can be saved in the file or DB
	 *
	 * @param int $max
	 * @return boolean
	 */
	public function set_max_error($max) {
		$this->max_error = $max;
		return true;
	}
	
	/**
	 * Returns a list of all errors from the error buffer in a Javascript format
	 *
	 */
	private function assemble_js_errors() {
		$js_errors = file_get_contents(SITE_ROOT.'/html/js_error.html');
		$matches = array();
		$errorList = '';
		
		//Get error row that will be used to put each error on a line
		preg_match_all("/<!-- ErrorLine Start -->(.*)<!-- ErrorLine End -->/is", $js_errors, $matches);
		$rowTemplate = $matches[1][0];
		
		foreach($this->error_buffer as $error) {
			$newRow = str_replace('<!-- errorMessage -->', $error['error_message'], $rowTemplate);
			$newRow = str_replace('<!-- errorType -->', $error['error_level'], $newRow);
			$newRow = str_replace('<!-- errorName -->', $this->get_error_types($error['error_level']), $newRow);
			$newRow = str_replace('<!-- file -->', $error['file'], $newRow);
			$newRow = str_replace('<!-- line -->', $error['line'], $newRow);
			$errorList .= $newRow;
		}
		
		if ($_COOKIE['hideErrors'] == 1) {
			preg_match_all("/<!-- HideErrors Start -->(.*)<!-- HideErrors End -->/is", $js_errors, $matches);
			$hideErrors = $matches[1][0];
		} else {
			$hideErrors = '';
		}		
		
		//Replace Tags
		$js_errors = str_replace('<!-- errorCount -->', count($this->error_buffer), $js_errors);
		$js_errors = preg_replace("/<!-- ErrorLine Start -->(.*)<!-- ErrorLine End -->/is", $errorList, $js_errors);
		$js_errors = preg_replace("/<!-- HideErrors Start -->(.*)<!-- HideErrors End -->/is", $hideErrors, $js_errors);
		
		return $js_errors;
	}
	
	/**
	 * Returns a list of all error from the error buffer in standard HTML format.
	 *
	 */
	private function assemble_inline_errors() {
		$error_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/html/error_reporting_html.html');
		$html_error = '';
		foreach ($this->error_buffer as $error) {
			$format_error = str_replace('<!-- error_level -->', $this->get_error_types($error['error_level']), $error_template);
			$format_error = str_replace('<!-- error_message -->', $error['error_message'], $format_error);
			$format_error = str_replace('<!-- error_file -->', $error['file'], $format_error);
			$format_error = str_replace('<!-- error_line -->', $error['line'], $format_error);
			$html_error .= $format_error;
		}
		return $html_error;
	}
	
	/**
	 * Writes all error from the error buffer to a text file.
	 *
	 * @return string
	 */
	private function write_error_to_file() {
		
		//Create file if it doesn't exist
		if (!file_exists(SITE_ROOT . $this->error_file)) {
			$fh = fopen(SITE_ROOT . $this->error_file, 'w+');
			fclose($fh);
		}
		
		//read file contents and explode
		$file_content = file_get_contents(SITE_ROOT . $this->error_file);
		$file_errors = explode('-----' . PHP_EOL, $file_content);
		
		//add new errors 
		foreach ($this->error_buffer as $error) {
			$timestamp = date('Y-m-d h:m:s');
			$formatted_error = $this->get_error_types($error['error_level']) . " ($timestamp)" . PHP_EOL;
			$formatted_error .= $error['error_message'] . PHP_EOL;
			$formatted_error .= "{$error['file']} (Line: {$error['line']})" . PHP_EOL;
			$file_errors[] = $formatted_error;
		}
		
		//count array & trim
		if (count($file_errors) > $this->max_error) {
			$file_errors = array_slice($file_errors, -$this->max_error);
		} 
				
		//save to file
		$file_content = implode('-----' . PHP_EOL, $file_errors);
		file_put_contents(SITE_ROOT . $this->error_file, $file_content);
		
		return '';
	}
	
	/**
	 * Write all errors from the error buffer to a database table.
	 *
	 * @return string
	 */
	private function write_error_to_db() {
		//Check if DB exists and create
		$SQL_statement = "SHOW TABLES LIKE 'error_log' ";
		if (!$SQL_result = mysql_query($SQL_statement)) echo mysql_error();
		if (mysql_num_rows($SQL_result) == 0) {
			$SQL_statement = "CREATE TABLE error_log (error_date timestamp , INDEX (error_date), error_type varchar(20), error_message text, error_file varchar(200), error_line integer(5));";
			if (!mysql_query($SQL_statement)) echo mysql_error();
		}
		
		//write to db
		foreach ($this->error_buffer as $error) {
			$error_type = $this->get_error_types($error['error_level']);
			$error_message = mysql_real_escape_string($error['error_message']);
			$error_file = mysql_real_escape_string($error['file']);
			$error_line = mysql_real_escape_string($error['line']);
			$SQL_statement = "INSERT INTO error_log (error_date, error_type, error_message, error_file, error_line) 
												VALUES (NOW(), '$error_type', '$error_message', '$error_file', '$error_line')";
			if (!mysql_query($SQL_statement)) echo mysql_error();
		}
		
		//check db length and resize to max_size
		$SQL_statement = "SELECT count(*) FROM error_log";
		if (!$SQL_result = mysql_query($SQL_statement)) echo mysql_error();
		list($error_count) = mysql_fetch_array($SQL_result);
		if ($error_count > $this->max_error) {
			$limit = $error_count - $this->max_error;
			$SQL_statement = "DELETE FROM error_log ORDER BY error_date LIMIT $limit";
			if (!mysql_query($SQL_statement)) echo mysql_error();
		}		
		return '';
	}

	/**
	 * Returns a name for a supplied error code
	 *
	 * @param int $error_code
	 * @return string
	 */
	private function get_error_types($error_code) {
		$errors = array(E_NOTICE => 'NOTICE',
									E_WARNING => 'WARNING',
									E_NOTICE => 'NOTICE', 
									E_USER_ERROR => 'ERROR',
									E_USER_WARNING => 'WARNING',
									E_USER_NOTICE => 'NOTICE');
		return  $errors[$error_code];
	}
}

?>