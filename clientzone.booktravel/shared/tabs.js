// add links
// tabs("<a href=0.php>Naught</a><a href=1.php>One</a>",1)
// the W variable will also be attached to these links.
// you can add tooltips and attributes to these links.
// v argument will mark a link as class=active
// the tab bar will be positioned at top:0;left:0;width:100%.

function tabs(l,v)
{
	G_wd()
	v=(v>-1?v:-1)
	var a=""
	if(!FF)
		a+="<style type=text/css>.TabS a:link,.TabS a:visited,.TabS a:hover,.TabS a.active:link,.TabS a.active:visited,.TabS a.active:hover{height:23;font-weight:bold}</style>"
	a+="<table class=TabS id=TabS style=width:"+(FF||window.miE?"100%":ww-17)+" ondblclick=\"this.style.visibility='hidden'\">"
	a+="<tr><th>"
	if(l)
	{
		l=l.split("</a>")
		var i=0
		while(l[i])
		{
			if(window.W)
			{
				p1=l[i].replace(/= /g,"=").replace(/ =/g,"=").replace(/>/g," >")
				p2=p1.substr(p1.indexOf("href=")+5)
				p1=p1.substr(0,p1.indexOf("href=")+5)
				p3=p2.substr(p2.indexOf(" "))
				p2=p2.substr(0,p2.indexOf(" "))
				p2=p2.replace(/"/g,"").replace(/'/g,"").replace(/ /g,"")
				if(p2=="#"||p2=="javascript:void(0)")
					p2="javascript:void(0)"
				else
				{
					if(p2.indexOf("?")>0)
						p2=p2+"&W="+W
					else
						p2=p2+"?W="+W
					if(window.WR)
						p2=p2+"&WR="+WR
				}
				l[i]=(p1+p2+p3.replace(/ >/g,">"))
			}
			if(l[i].toLowerCase().indexOf("onfocus=blur()")<0)
				l[i]=l[i].replace(">"," onfocus=blur()>")
			if(v==i)
				l[i]=l[i].replace(">"," class=active>")
			a+=l[i]+"</a>"
			i++
		}
	}
	a+="</tr>"
	a+="<tr><td><br></td></tr>"
	a+="</table>"
	document.write(a)
}

function TabScroll(h)
{
	clearTimeout(MENU_t["tabS"])
	G_wd()
	var t=document.getElementById("TabS")
	if(h)
	{
		if(h==99)
		{
			t.style.top=sy-t.offsetHeight
			MENU_t["tabS"]=setTimeout("TabScroll(1)",16)
		}
		else
		{
			t.style.visibility=""
			var y=parseInt(t.style.top)
			if(y<sy)
			{
				t.style.top=y+1
				MENU_t["tabS"]=setTimeout("TabScroll(1)",16)
			}
		}
	}
	else
	{
		var y=parseInt(t.style.top)
		if(y!=sy)
		{
			var h=y+t.offsetHeight
			if(sy>y&&sy<h)
				MENU_t["tabS"]=setTimeout("TabScroll(1)",800)
			else
			{
				t.style.visibility="hidden"
				MENU_t["tabS"]=setTimeout("TabScroll(99)",800)
			}
		}
	}
}