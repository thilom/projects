<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] ."/shared/admin_style.php";
echo $css;

if (isset($_REQUEST['save_data'])) {
	subscribe_success();
} else {
	subscribe_form();
}

function subscribe_form() {
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/html/awebber_signup.html');
	
	//Get user Data
	$statement = "SELECT firstname,lastname,email FROM nse_user WHERE user_id=?";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bind_param('s', $_SESSION['dbweb_user_id']);
	$sql_user->execute();
	$sql_user->store_result();
	$sql_user->bind_result($firstname, $lastname, $email);
	$sql_user->fetch();
	$sql_user->free_result();
	$sql_user->close();
	
	//Repalec Tags
	$template = str_replace('<!-- name -->', "$firstname $lastname", $template);
	$template = str_replace('<!-- from -->', $email, $template);
	
	echo $template;
}

function subscribe_success() {
	$statement = "UPDATE nse_user SET juno_msg='1' WHERE user_id=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_update->bind_param('s',$_SESSION['dbweb_user_id']);
	$sql_update->execute();

	$signup = isset($_POST['signup'])?'Y':'';
	$tester = isset($_POST['beta'])?'Y':'';

	$statement = "INSERT INTO nse_user_poll (user_id, juno_msg, tester) VALUES (?,?,?)";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bind_param('sss',$_SESSION['dbweb_user_id'], $signup, $tester);
	$sql_update->execute();

	echo "<script>parent.location='/W/index.php';</script>";
}

?>
