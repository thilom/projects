// ARGUMENTS
// Win(n,s,h,w,m)
// n= title of window
// s= address for iframe
//  (optional)
// h= height
// w= width
// m= positions at mouseclick

Win_i=0
Win_z=99999
Win_n=1

function Win(n,s,h,w,m)
{
	n=n.replace(/_/g," ")

	a=document.getElementsByTagName('TH')
	for(var i=0;i<a.length;i++)
	{
		if(a[i].className=="Win_th")
		{
			v=a[i].getElementsByTagName('B')[0].innerHTML
			if(n==v)
			{
				n=0
				break
			}
		}
	}


	if(n)
	{
		G_wd()
		h=(h>240?h:240)
		w=(w>260?w:260)
		var a=document.createElement('TABLE')
		a.id="Win_"+Win_i
		a.className="Win_tb"
		a.style.zIndex=Win_z+2
		a.style.top=(m&&y>0?y:(wh-h+20)/2)+sy
		a.style.left=(m&&x+10>0?x+10:(ww-w+20)/2)+sx
		a.style.height=(h>wh-40?wh-40:h)
		a.style.width=(w>ww-40?ww-40:w)
		var b=document.createElement('TBODY')
		var c=document.createElement('TR')
		var d=document.createElement('TD')
		d.style.width=8
		d.style.height=8
		d.innerHTML="<img src=/W/S1.png>"
		c.appendChild(d)
		d=document.createElement('TD')
		d.style.background="url(/W/S2.png) repeat-x bottom left"
		c.appendChild(d)
		d=document.createElement('TD')
		d.style.width=16
		d.style.height=8
		d.innerHTML="<img src=/W/S3.png>"
		c.appendChild(d)
		b.appendChild(c)
		c=document.createElement('TR')
		d=document.createElement('TD')
		d.style.background="url(/W/S8.png) repeat-y top right"
		c.appendChild(d)
		d=document.createElement('TD')
		var e="<table class=W_itb>"
		e+="<tr>"
		e+="<th class=W_th>"
		e+="<a href=javascript:void(WD("+Win_i+")) onfocus=blur() onmouseover=\"T_S(event,'Close',1)\">&#215;</a>"
		e+="<b class=Win_b lang=m00vW>"+n+"</b>"
		e+="</th>"
		e+="</tr>"
		e+="<tr>"
		e+="<td>"
		e+="<iframe src="+s+(s.indexOf(".php?")>0?"&":"?")+"W="+Win_i+" marginwidth=0 marginheight=0 frameborder=0 class=W_if></iframe>"
		e+="</td>"
		e+="</tr>"
		e+="</table>"
		d.innerHTML=e
		c.appendChild(d)
		d=document.createElement('TD')
		d.style.background="url(/W/S4.png) repeat-y top left"
		c.appendChild(d)
		b.appendChild(c)
		c=document.createElement('TR')
		d=document.createElement('TD')
		d.style.width=8
		d.style.height=16
		d.innerHTML="<img src=/W/S7.png>"
		c.appendChild(d)
		d=document.createElement('TD')
		d.style.background="url(/W/S6.png) repeat-x top left"
		c.appendChild(d)
		d=document.createElement('TD')
		d.style.width=16
		d.style.height=16
		d.innerHTML="<img src=/W/S5.png>"
		c.appendChild(d)
		b.appendChild(c)
		a.appendChild(b)
		document.getElementsByTagName('body')[0].appendChild(a)
		Win_z++
		Win_i++
	}
}

function WD(v)
{
	document.getElementsByTagName('BODY')[0].removeChild(document.getElementById("Win_"+v))
}

document.write("<link rel=stylesheet href=/shared/windows.css>")