<?php
/**
 * Utilities te keep the html pages up to date.
 *
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category pageUpdate
 */
class pageUpdate {
	
	/**
	 * Last error message
	 * @var string
	 */
	private $error = '';
	
	function __construct() {
	
	}
	
	/**
	 * Update an establishment html page
	 *
	 * @param $establishment_code
	 * @return bool
	 */
	public function update_establishment($establishment_code) {
		if (empty($establishment_code)) {
			$this->error = "Establishment code not supplied";
			return false;
		}
		
		
		
		return true;
	}
	
	/**
	 * Returns the last error that occured
	 *
	 * @return string
	 */
	public function last_error() {
		return $this->error;
	}
	
}

?>