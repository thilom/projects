function get(v)
{
	var c=document.cookie
	v=v+"="
	l=c.length
	var a=0
	while(a<l)
	{
		var b=a+v.length
		if(c.substring(a,b)==v)
		{
			var n=c.indexOf(";",b)
			if(n==-1)
				n=l
			return unescape(c.substring(b,n))
		}
		a=c.indexOf("",a)+1
		if(a==0)
			break
	}
	return null
}

function set(n,v,e)
{
	ex=new Date()
	ex.setTime(ex.getTime()+(24*60*60*1000*(e==1?1000:e?e:-1)))
	document.cookie=n+"="+escape(v?v:0)+";expires="+ex.toGMTString()+";path=/"
}

function createCookie(name,value) {
	var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}