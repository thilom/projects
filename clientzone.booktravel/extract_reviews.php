<?php

//Vars
$data1 = "feedback ID,establishment Code,Establishment Name,Feedback Date,Visit Date,Recommended,Experience,Value for Money,Room Comfort,Room Cleanliness,Food Quality,Staff Friendliness,Security,Grounds & Gardens,Building Maintenance\r\n";
$data2 = "feedback ID,establishment Code,Establishment Name,Feedback Date,Visit Date,Recommended,Quality,Staff FriendlIness,Staff Dependability,Value for Money,Room Cleanliness,Room Comfort,Condition of Appliances,Bathrooms,Self Catering Kitchen,Quality of Food,Helpfulness of Management and Staff,Safety and Security,Check-in,Activities\r\n";
$line = '';
$fields = array('recommends_as','experience','value','comfort','cleanliness','food','friendliness','security','grounds','maintenance','2_recommends_as','2_quality','2_staff','2_staff_dependability','2_value','2_room_cleanliness','2_room_comfort','2_appliances','2_bathrooms','2_kitchen','2_food','2_management','2_security','2_checkin');

//init
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Line Template
$lTemplate1 = "feedback_id,establishment_code,\"establishment_name\",feedback_date,visit_date,'recommends_as','experience','value','comfort','cleanliness','food','friendliness','security','grounds','maintenance'\r\n";
$lTemplate2 = "feedback_id,establishment_code,\"establishment_name\",feedback_date,visit_date,'2_recommends_as','2_quality','2_staff','2_staff_dependability','2_value','2_room_cleanliness','2_room_comfort','2_appliances','2_bathrooms','2_kitchen','2_food','2_management','2_security','2_checkin','2_activities'\r\n";

//Prepare statement
$statement = "SELECT b.category_name, a.feedback_value, a.category_code
				FROM nse_feedback_results AS a
				JOIN nse_feedback_category AS b ON a.category_code=b.category_code
				WHERE a.feedback_id=?";
$sql_result = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Get list of reviews
$statement = "SELECT a.feedback_id, a.establishment_code, a.feedback_date, a.visit_date, b.establishment_name
				FROM nse_establishment_feedback AS a
				JOIN nse_establishment as b ON b.establishment_code=a.establishment_code
				WHERE a.feedback_date IS NOT NULL";
$sql_feedback = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);
$sql_feedback->execute();
$sql_feedback->store_result();
$sql_feedback->bind_result($feedback_id, $establishment_code, $feedback_date, $visit_date, $establishment_name);
while ($sql_feedback->fetch()) {
	$line = '';
	$type = '';
	$sql_result->bind_param('s',$feedback_id);
	$sql_result->execute();
	$sql_result->store_result();
	$sql_result->bind_result($category, $result, $category_code);
	while($sql_result->fetch()) {
		if (substr($category_code,0,2) == '2_') {
			$type = 2;
			if (empty($line)) $line = $lTemplate2;
			$line = str_replace("'$category_code'",$result,$line);
		} else {
			$type = 1;
			if (empty($line)) $line = $lTemplate1;
			$line = str_replace("'$category_code'",$result,$line);
		}
	}
	$line = str_replace('feedback_id', $feedback_id, $line);
	$line = str_replace('establishment_code', $establishment_code, $line);
	$line = str_replace('establishment_name', str_replace(',',' ',$establishment_name), $line);
	$line = str_replace('feedback_date', str_replace(',',' ', $feedback_date), $line);
	$line = str_replace('visit_date', str_replace(',',' ',$visit_date), $line);
	foreach ($fields as $f) {
		$line = str_replace("'$f'", ' ', $line);
	}
	//echo "$feedback_id :: $establishment_code, $feedback_date, $visit_date<br>";
	if ($type == 2) {
		$data2 .= $line;
	} else {
		$data1 .= $line;
	}
}
$sql_feedback->close();

file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/temp/reviews1.csv', $data1);
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/temp/reviews2.csv', $data2);
echo nl2br("$data1<hr>$data2")
?>