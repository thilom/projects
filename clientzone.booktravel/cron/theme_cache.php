<?php
/**
 * Update the theme cache table via cron job
 *
 * @author Thilo Muller(2010)
 * @package RVBUS
 * @category themes
 */

set_time_limit(0);


   $mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $starttime = $mtime;


//echo "START: " . memory_get_usage() . "\n";

//Vars
$aa_countries = array();

//Includes
require_once '/var/www/clients/client1/web4/web/settings/init.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Clear theme cache temp table
$statement = "DELETE FROM nse_themes_locations_temp";
$sql_clear = $GLOBALS['dbCon']->prepare($statement);
$sql_clear->execute();
$sql_clear->close();

//Get countries for southern africa
$statement = "SELECT location_id
                FROM nse_nlocations_regions_content  AS a
                JOIN nse_nlocations_regions AS b ON a.Region_id	= b.region_id
                WHERE b.region_name = 'southern africa'";
$sql_aa_countries = $GLOBALS['dbCon']->prepare($statement);
$sql_aa_countries->execute();
$sql_aa_countries->store_result();
$sql_aa_countries->bind_result($country_id);
while ($sql_aa_countries->fetch()) {
    $aa_countries[] = $country_id;
}
$sql_aa_countries->free_result();
$sql_aa_countries->close();

//Prepare statement - Check for entry
$statement = "SELECT COUNT(*) FROM nse_themes_locations_temp WHERE theme_id=? && location_type=? && location_id=?";
$sql_location_count = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Insert location
$statement = "INSERT INTO nse_themes_locations_temp (theme_id, location_type, location_id, aa_establishment_count, eti_establishment_count, zw_establishment_count) VALUES (?,?,?,0,0,0)";
$sql_location_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Add to counter
$statement = "UPDATE nse_themes_locations_temp SET aa_establishment_count=aa_establishment_count+?, eti_establishment_count=eti_establishment_count+?, zw_establishment_count=zw_establishment_count+? WHERE theme_id=? && location_type=? && location_id=?";
$sql_estab_update = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - get location details
$statement = "SELECT country_id, province_id, town_id, suburb_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
$sql_location = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - aa status
$statement = "SELECT aa_category_code, active, advertiser, aa_estab FROM nse_establishment WHERE establishment_code=?";
$sql_aa_estab1 = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Description
$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=? && description_type='short_description'";
$sql_description = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - email/Tel
$statement = "SELECT COUNT(*) FROM nse_establishment_public_contact WHERE establishment_code=? && (contact_type='email' || contact_type='tel1' || contact_type='tel2')";
$sql_contact_count = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - aa status publications
$statement = "SELECT COUNT(*)
                FROM nse_establishment_publications AS a
                JOIN nse_publications AS b ON a.publication_id=b.publication_id
                WHERE a.establishment_code=? && b.expiry_date>NOW()";
$sql_aa_estab2 = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - List of regions
$statement = "SELECT region_id FROM nse_nlocations_regions_content WHERE location_type=? && location_id=?";
$sql_regions = $GLOBALS['dbCon']->prepare($statement);

//Get theme list
$themes = array();
$statement = "SELECT theme_id FROM nse_themes ";
$sql_theme_list = $GLOBALS['dbCon']->prepare($statement);
$sql_theme_list->execute();
$sql_theme_list->store_result();
$sql_theme_list->bind_result($theme_id);
while ($sql_theme_list->fetch()) {
    $themes[] = $theme_id;
}
$sql_theme_list->free_result();
$sql_theme_list->close();

foreach ($themes as $theme_id) {
    //Vars
    $restype = array();
    $icons = array();
    
//    echo "$theme_id: " . memory_get_usage() . "\n";
//    echo "THEME COUNT: " . count($restype) . "\n\n";

    //Get theme constraints
    $statement = "SELECT restype_ids, icon_ids, star_grad FROM nse_themes WHERE theme_id=? LIMIT 1";
    $sql_theme = $GLOBALS['dbCon']->prepare($statement);
    $sql_theme->bind_param('i', $theme_id);
    $sql_theme->execute();
    $sql_theme->bind_result($restype_list, $icon_list, $star_list);
    $sql_theme->fetch();
    $sql_theme->free_result();
    $sql_theme->close();

    //Get list of establishments with restype & star grading
    if (empty($restype_list) && empty($star_list)) {
        $statement = "SELECT establishment_code FROM nse_establishment";
        $sql_all = $GLOBALS['dbCon']->prepare($statement);
        $sql_all->execute();
        $sql_all->store_result();
        $sql_all->bind_result($establishment_code);
        while ($sql_all->fetch()) {
            $restype[] = $establishment_code;
        }
        $sql_all->free_result();
        $sql_all->close();
    } else {
        $statement = "SELECT establishment_code FROM nse_establishment_restype WHERE (";
        if (!empty($restype_list)) {
            foreach(explode(',',$restype_list) as $restype_id) {
                $statement .= "subcategory_id=$restype_id || ";
            }
            $statement = substr($statement, 0, -3) . ')';
            if (!empty($star_list)) $statement .= " && (";
        }
        if (!empty($star_list)) {

            foreach(explode(',',$star_list) as $star_id) {
                $statement .= "star_grading=$star_id || ";
            }
            $statement = substr($statement, 0, -3) . ')';
        }

        $sql_restypes = $GLOBALS['dbCon']->prepare($statement);
        $sql_restypes->execute();
        $sql_restypes->store_result();
        $sql_restypes->bind_result($establishment_code);
        while($sql_restypes->fetch()) {
            $restype[] = $establishment_code;
        }
        $sql_restypes->free_result();
        $sql_restypes->close();
    }


    //Get list of establishment for icons
    if (empty($icon_list)) {
        $statement = "SELECT establishment_code FROM nse_establishment";
        $sql_all = $GLOBALS['dbCon']->prepare($statement);
        $sql_all->execute();
        $sql_all->store_result();
        $sql_all->bind_result($establishment_code);
        while ($sql_all->fetch()) {
            $icons[] = $establishment_code;
        }
        $sql_all->free_result();
        $sql_all->close();
    } else {
        $statement = "SELECT establishment_code FROM nse_establishment_icon WHERE ";
        foreach(explode(',', $icon_list) as $icon_id) {
            $statement .= "icon_id=$icon_id ||";
        }
        $statement = substr($statement, 0, -3);
        $sql_icons = $GLOBALS['dbCon']->prepare($statement);
        echo mysqli_error($GLOBALS['dbCon']);
        $sql_icons->execute();
        $sql_icons->store_result();
        $sql_icons->bind_result($establishment_code);
        while($sql_icons->fetch()) {
            $icons[] = $establishment_code;
        }
        $sql_icons->free_result();
        $sql_icons->close();
    }


    $establishment_list = array_intersect($restype, $icons);

    foreach ($establishment_list as $establishment_code) {
	    $sql_location->bind_param('s', $establishment_code);
	    $sql_location->execute();
	    $sql_location->bind_result($country_id, $province_id, $town_id, $suburb_id);
	    $sql_location->fetch();
	    $sql_location->free_result();
	
	    //Check estab aa_status
	    $aa_estab = 0;
	    $eti_estab = 1;
	    $zw_estab = 1;
	    $sql_aa_estab1->bind_param('s', $establishment_code);
	    $sql_aa_estab1->execute();
	    $sql_aa_estab1->bind_result($grade, $active, $advertiser, $aa_estab);
	    $sql_aa_estab1->fetch();
	    $sql_aa_estab1->free_result();
	    $aa_estab = empty($aa_estab)?0:1;
	    if ($active != '1') {
	        $aa_estab = 0;
	        $eti_estab = 0;
	        $zw_estab = 0;
	    }
		//    } else if (!empty($grade) || in_array($advertiser, array('P', 'L', 'A', 'B', 'S'))) {
		//        $aa_estab = 1;
		//    } else {
		//        $sql_aa_estab2->bind_param('s', $establishment_code);
		//        $sql_aa_estab2->execute();
		//        $sql_aa_estab2->store_result();
		//        $sql_aa_estab2->bind_result($aa_estab_count);
		//        $sql_aa_estab2->fetch();
		//        $sql_aa_estab2->free_result();
		//
		//        if ($aa_estab_count > 0) {
		//                $aa_estab = 1;
		//        }
		//    }
	
	
	    //Check for empty description
	    $description = '';
	    $sql_description->bind_param('s', $establishment_code);
	    $sql_description->execute();
	    $sql_description->bind_result($description);
	    $sql_description->fetch();
	    $sql_description->free_result();
	
	    //Check for empty contact info
	    $contact_count = 0;
	    $sql_contact_count->bind_param('s', $establishment_code);
	    $sql_contact_count->execute();
	    $sql_contact_count->bind_result($contact_count);
	    $sql_contact_count->fetch();
	    $sql_contact_count->free_result();
	
	    //Update eti_estab variable
	    //if ($contact_count == 0 && empty($description)) $eti_estab = 0;
	
	    //Countries
	    $location_type = 'country';
	    $sql_location_count->bind_param('isi', $theme_id, $location_type, $country_id);
	    $sql_location_count->execute();
	    $sql_location_count->bind_result($result_count);
	    $sql_location_count->fetch();
	    $sql_location_count->free_result();
	    if ($result_count == 0) {
	        $sql_location_insert->bind_param('isi', $theme_id, $location_type, $country_id);
	        $sql_location_insert->execute();
	    }
	    if (!in_array($country_id, $aa_countries)) {
	        $aa_estab = 0;
	    }
	    if (!empty($country_id) && $country_id != 0) {
	    	if ($country_id == 1) {
	    		$zw_estab = 0;
	    	}
	        $sql_estab_update->bind_param('iiiisi', $aa_estab, $eti_estab, $zw_estab, $theme_id, $location_type, $country_id);
	        $sql_estab_update->execute();
	        //$zw_estab = 1;
	    }
	
	    //Provinces
	    $location_type = 'province';
	    $sql_location_count->bind_param('isi', $theme_id, $location_type, $province_id);
	    $sql_location_count->execute();
	    $sql_location_count->bind_result($result_count);
	    $sql_location_count->fetch();
	    $sql_location_count->free_result();
	    if ($result_count == 0) {
	        $sql_location_insert->bind_param('isi', $theme_id, $location_type, $province_id);
	        $sql_location_insert->execute();
	    }
	
	    if (!empty($province_id) && $province_id != 0) {
	        $sql_estab_update->bind_param('iiiisi', $aa_estab, $eti_estab, $zw_estab, $theme_id, $location_type, $province_id);
	        $sql_estab_update->execute();
	    }
	
	    //Towns
	    $location_type = 'town';
	    $sql_location_count->bind_param('isi', $theme_id, $location_type, $town_id);
	    $sql_location_count->execute();
	    $sql_location_count->bind_result($result_count);
	    $sql_location_count->fetch();
	    $sql_location_count->free_result();
	    if ($result_count == 0) {
	        $sql_location_insert->bind_param('isi', $theme_id, $location_type, $town_id);
	        $sql_location_insert->execute();
	    }
	
	    if (!empty($town_id) && $town_id != 0) {
	        $sql_estab_update->bind_param('iiiisi', $aa_estab, $eti_estab, $zw_estab, $theme_id, $location_type, $town_id);
	        $sql_estab_update->execute();
	    }
	
	
	    //Suburbs
	    $location_type = 'suburb';
	    $sql_location_count->bind_param('isi', $theme_id, $location_type, $suburb_id);
	    $sql_location_count->execute();
	    $sql_location_count->bind_result($result_count);
	    $sql_location_count->fetch();
	    $sql_location_count->free_result();
	    if ($result_count == 0) {
	        $sql_location_insert->bind_param('isi', $theme_id, $location_type, $suburb_id);
	        $sql_location_insert->execute();
	    }
	
	    if (!empty($suburb_id) && $suburb_id != 0) {
	        $sql_estab_update->bind_param('iiiisi', $aa_estab, $eti_estab, $zw_estab, $theme_id, $location_type, $suburb_id);
	        $sql_estab_update->execute();
	    }
	
	    //Get regions - For country ID
	    $regions = array();
	    $location_type = 'country';
	    $sql_regions->bind_param('si', $location_type, $country_id);
	    $sql_regions->execute();
	    $sql_regions->store_result();
	    $sql_regions->bind_result($region_id);
	    while ($sql_regions->fetch()) {
	        $regions[] = $region_id;
	    }
	    $sql_regions->free_result();
	
	    //Get regions - For Province ID
	    $location_type = 'province';
	    $sql_regions->bind_param('si', $location_type, $province_id);
	    $sql_regions->execute();
	    $sql_regions->store_result();
	    $sql_regions->bind_result($region_id);
	    while ($sql_regions->fetch()) {
	        $regions[] = $region_id;
	    }
	    $sql_regions->free_result();
	
	    //Get regions - For town ID
	    $location_type = 'town';
	    $sql_regions->bind_param('si', $location_type, $town_id);
	    $sql_regions->execute();
	    $sql_regions->store_result();
	    $sql_regions->bind_result($region_id);
	    while ($sql_regions->fetch()) {
	        $regions[] = $region_id;
	    }
	    $sql_regions->free_result();
	
	    //Get regions - For suburb ID
	    $location_type = 'suburb';
	    $sql_regions->bind_param('si', $location_type, $suburb_id);
	    $sql_regions->execute();
	    $sql_regions->store_result();
	    $sql_regions->bind_result($region_id);
	    while ($sql_regions->fetch()) {
	        $regions[] = $region_id;
	    }
	    $sql_regions->free_result();
	
	    $location_type = 'region';
	    foreach ($regions as $region_id) {
	        $sql_location_count->bind_param('isi', $theme_id, $location_type, $region_id);
	        $sql_location_count->execute();
	        $sql_location_count->bind_result($result_count);
	        $sql_location_count->fetch();
	        $sql_location_count->free_result();
	        if ($result_count == 0) {
	            $sql_location_insert->bind_param('isi', $theme_id, $location_type, $region_id);
	            $sql_location_insert->execute();
	        }
	
	
	        if (!empty($region_id) && $region_id != 0) {
	        	if ($region_id != "61") {
	        		$zw_estab = 0;
	        	} 
	            $sql_estab_update->bind_param('iiiisi', $aa_estab, $eti_estab, $zw_estab, $theme_id, $location_type, $region_id);
	            $sql_estab_update->execute();
	        }
	    }
	}
}

//Clear cache table
$statement = "DELETE FROM nse_themes_locations";
$sql_clear = $GLOBALS['dbCon']->prepare($statement);
$sql_clear->execute();
$sql_clear->close();

//Copy temp table
$statement = "INSERT INTO nse_themes_locations SELECT * FROM nse_themes_locations_temp";
$sql_copy = $GLOBALS['dbCon']->prepare($statement);
$sql_copy->execute();
$sql_copy->close();

$mtime = microtime();
   $mtime = explode(" ",$mtime);
   $mtime = $mtime[1] + $mtime[0];
   $endtime = $mtime;
   $totaltime = ($endtime - $starttime);
   echo "This page was created in ".$totaltime." seconds"; 
?>

