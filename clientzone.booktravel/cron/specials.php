<?php
/**
 * Generate specials pages on aatravel.co.za
 */

//Includes and Setup
require_once '/var/www/admin.aatravel.co.za/web/settings/init.php';


update_table();
run_update();

mail('thilo@aatravel.co.za', 'Specials Cron', 'Cron job success');

/**
 * Update the export table that will be used to locate updatable locations
 */
function update_table() {
	//Vars
	$suburb_id = 0;
	$town_id = 0;
	$province_id = 0;
	$country_id = 0;
	$no_provinces = '';
	
	//Prepare Statement -> No Provinces
	$statement = "SELECT no_provinces FROM nse_location_country WHERE country_id=?";
	$sql_no_province = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare statement -> Provinces
	$statement = "SELECT province_id FROM nse_location_province WHERE country_id=?";
	$sql_provinces = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement -> Town, no province
	$statement = "SELECT town_id FROM nse_location_town WHERE country_id=?";
	$sql_town_no_province = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement -> Towns
	$statement = "SELECT town_id FROM nse_location_town WHERE province_id=?";
	$sql_towns = $GLOBALS['dbCon']->prepare($statement);
	
	//Prepare Statement -> Suburbs
	$statement = "SELECT suburb_id FROM nse_location_suburb WHERE town_id=?";
	$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
	
	//Iterate and find locations
	$statement = "SELECT country_id
					FROM nse_location_region_towns AS a
					JOIN nse_location_region_lang AS b ON a.region_id=b.region_id
					WHERE b.region_name='Southern Africa'";
	$sql_countries = $GLOBALS['dbCon']->prepare($statement);
	$sql_countries->execute();
	$sql_countries->store_result();
	$sql_countries->bind_result($country_id);
	while ($sql_countries->fetch()) {
		//Check for provinces
		$sql_no_province->bind_param('i', $country_id);
		$sql_no_province->execute();
		$sql_no_province->store_result();
		$sql_no_province->bind_result($no_provinces);
		$sql_no_province->fetch();
		$sql_no_province->free_result();
		
		$estab_count = establishment_count('country', $country_id);
		insert_location($country_id, 0, 0, 0, $estab_count);
		
		//echo "Country :: $country_id ($estab_count)<br>";
		
		if ($no_provinces == 1) {
			
		} else {
			$sql_provinces->bind_param('i', $country_id);
			$sql_provinces->execute();
			$sql_provinces->store_result();
			$sql_provinces->bind_result($province_id);
			while ($sql_provinces->fetch()) {
				$estab_count = establishment_count('province', $province_id);
				insert_location($country_id, $province_id, 0, 0, $estab_count);
				//echo "&nbsp;&nbsp;Province :: $province_id ($estab_count)<br>";
				
				$sql_towns->bind_param('i', $province_id);
				$sql_towns->execute();
				$sql_towns->store_result();
				$sql_towns->bind_result($town_id);
				while ($sql_towns->fetch()) {
					$estab_count = establishment_count('town', $town_id);
					insert_location($country_id, $province_id, $town_id, 0, $estab_count);
					//echo "&nbsp;&nbsp;&nbsp;&nbsp;Town :: $town_id ($estab_count)<br>";
					
					$sql_suburb->bind_param('i', $town_id);
					$sql_suburb->execute();
					$sql_suburb->store_result();
					$sql_suburb->bind_result($suburb_id);
					while ($sql_suburb->fetch()) {
						$estab_count = establishment_count('suburb', $suburb_id);
						insert_location($country_id, $province_id, $town_id, $suburb_id, $estab_count);
						//echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suburb :: $suburb_id ($estab_count)<br>";
					}
					$sql_suburb->free_result();
				}
				$sql_towns->free_result();
			}
			$sql_provinces->free_result();
		}
		
	}
	$sql_countries->free_result();
	$sql_countries->close();
}



function establishment_count($type, $location_id) {
	//Vars
	$establishment_count = 0;
	
	//Get number of establishments for country
	
	switch ($type) {
		case 'country':
			$statement = "SELECT COUNT(*)
							FROM nse_establishment_location As a
							JOIN nse_establishment AS b ON a.establishment_code = b.establishment_code
							WHERE a.country_id=? && b.aa_estab='Y'";
			break;
		case 'province':
			$statement = "SELECT COUNT(*) FROM nse_establishment_location WHERE province_id=?";
			break;
		case 'town':
			$statement = "SELECT COUNT(*) FROM nse_establishment_location WHERE town_id=?";
			break;
		case 'suburb':
			$statement = "SELECT COUNT(*) FROM nse_establishment_location WHERE suburb_id=?";
			break;
	}
	
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bind_param('i', $location_id);
	$sql_count->execute();
	$sql_count->store_result();
	$sql_count->bind_result($establishment_count);
	$sql_count->fetch();
	$sql_count->free_result();
	$sql_count->close();
	
	return $establishment_count;
}

function insert_location($country_id, $province_id, $town_id, $suburb_id, $estab_count) {
	//Vars
	$old_count = 0;
	$export_id = 0;
	
	//Check if country_exists
	$statement = "SELECT export_id, establishment_count FROM nse_export_specials WHERE country_id=? && province_id=? && town_id=? && suburb_id=? LIMIT 1";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_check->bind_param('iiii', $country_id, $province_id, $town_id, $suburb_id);
	$sql_check->execute();
	$sql_check->store_result();
	$sql_check->bind_result($export_id, $old_count);
	$sql_check->fetch();
	$sql_check->free_result();
	$sql_check->close();
	
	//Update table
	if ($export_id == 0 && $estab_count > 0) {
		$statement = "INSERT INTO nse_export_specials (country_id, province_id, town_id, suburb_id, establishment_count) VALUES (?,?,?,?,?)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_insert->bind_param('iiiii', $country_id, $province_id, $town_id, $suburb_id, $estab_count);
		$sql_insert->execute();
		$sql_insert->close();
	} else {
		if ($old_count > 0 && $estab_count == 0) {
			$statement = "UPDATE nse_export_specials SET complete = null, establishment_count=? WHERE export_id=?";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bind_param('ss', $estab_count, $export_id);
			$sql_update->execute();
			$sql_update->close();
		} else if ($estab_count > 0) {
			$statement = "UPDATE nse_export_specials SET complete = null, establishment_count=? WHERE export_id=?";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bind_param('ss', $estab_count, $export_id);
			$sql_update->execute();
			$sql_update->close();
		}
	}
}

function run_update() {
	
	//Vars
	$country_id = '';
	$province_id = '';
	$town_id = '';
	$suburb_id = '';
	$export_id = '';
	
	//Prepare Statement - Set Complete
	$statement = "UPDATE nse_export_specials SET complete='Y' WHERE export_id=?";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	
	//Get Export list
	$statement = "SELECT export_id, country_id, province_id, town_id, suburb_id FROM nse_export_specials WHERE complete IS NULL";
	$sql_list = $GLOBALS['dbCon']->prepare($statement);
	$sql_list->execute();
	$sql_list->store_result();
	$sql_list->bind_result($export_id, $country_id, $province_id, $town_id, $suburb_id);
	while ($sql_list->fetch()) {
		$ch = curl_init("http://aatravel.co.za/export/specials_pages.php?c=$country_id&p=$province_id&t=$town_id&s=$suburb_id");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$res = curl_exec($ch);
		if ($res == '1') {
			$sql_update->bind_param('i', $export_id);
			$sql_update->execute();
		}
		//echo "<b>$res</b><hr>";
		curl_close($ch);
	}
	$sql_list->free_result();
	$sql_list->close();
}















?>