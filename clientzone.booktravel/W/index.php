<?php
echo "<html>";
echo "<title>AA Travel Guides - Administration System</title>";

// get fluid css
include $_SERVER['DOCUMENT_ROOT']."/shared/directory_item.php";
getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","W.css");
if($itemname)
	echo "<link rel=stylesheet href=/i/fluid/".$itemname.">";
else
	echo "<link rel=stylesheet href=/W/W.css>";

echo "<script src=W.js></script>";
echo "<script src=/shared/T.js></script>";
echo "<script src=/shared/C.js></script>";
echo "<body";
if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.gif"))
{
	echo " style=background:url(/i/fluid/DB.";
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.png"))
		echo "png";
	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.jpg"))
		echo "jpg";
	else
		echo "gif";
	echo ")";
}
echo ">";

if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.gif"))
{
	echo "<table id=Wf><tr><td style='background:url(/i/fluid/DF.";
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.png"))
		echo "png";
	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.jpg"))
		echo "jpg";
	else
		echo "gif";
	echo ") no-repeat center center'></td></tr></table>";
}

if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.gif"))
{
	echo "<table id=Wb><tr><td><img src=/i/fluid/L.";
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.png"))
		echo "png";
	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.jpg"))
		echo "jpg";
	else
		echo "gif";
	echo "></td></tr></table>";
}
elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/W/F/L.png"))
	echo "<table id=Wb><tr><td><!-- <img src=F/L.png><br /> --><span style='color: grey; font-weight: bold; font-size: 10pt'>Beta 1.01<br />AA Travel Guides Administration System</span></td></tr></table>";

echo "<table id=WnB class=i><tr><td></td><th></th></tr></table>";
echo "<table id=Wn onmouseover=Wn0(1) onmouseout=Wn0()><tr><th>";
if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.gif"))
{
	echo "<img src=/i/fluid/DL.";
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.png"))
		echo "png";
	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.jpg"))
		echo "jpg";
	else
		echo "gif";
	echo " class=WL>";
}
echo "</th></tr></table>";

echo "<i id=Wm><a href=javascript:void(Wn0(1)) onfocus=blur() class=m>mOdules</a></i>";

echo "</body>";
echo "</html>";
echo "<script>WS()</script>";

?>