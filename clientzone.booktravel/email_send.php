<?php
/**
 * Send emails from the db
 */

//init
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';


require_once SITE_ROOT . '/shared/PHPMailer/class.phpmailer.php';


//Prepare Statement - Update sent date
$statement = "UPDATE nse_bulk_emails SET send_date=NOW() WHERE mail_id=?";
$sql_update = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);


$statement = "SELECT mail_id, email_address, email_subject, email_content FROM nse_bulk_emails WHERE send_date IS NULL LIMIT 1";
$sql_emails = $GLOBALS['dbCon']->prepare($statement);
$sql_emails->execute();
$sql_emails->store_result();
$sql_emails->bind_result($id, $address, $subject, $content);
while ($sql_emails->fetch()) {
	
	echo "$address ";
	try {
	  $mail = new PHPMailer(true);
      $mail->AddReplyTo('admin@aatravel.co.za', 'AA Travel Guides');
      $mail->AddAddress($address);
      //$mail->AddAddress('meloth14@gmail.com');
      $mail->SetFrom('admin@aatravel.co.za', 'AA Travel Guides');
      $mail->Subject = $subject;
      $mail->MsgHTML($content);
      $mail->Send();
      $mail->ClearAddresses();
      
      echo " - Sent";
    } catch (phpmailerException $e) {
      //Log
      echo $e->errorMessage();
    } catch (Exception $e) {
      //Log
      echo $e->errorMessage();
    }
    echo "<br>";
    echo $content;
    $sql_update->bind_param('i', $id);
    $sql_update->execute();
}
$sql_emails->free_result();
$sql_emails->close();

?>