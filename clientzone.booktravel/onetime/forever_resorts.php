<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$forever_list = array();
$hq_list = array();
$invoice_id = '';

//Get list of establishments
$statement = "SELECT establishment_code, establishment_name
			FROM nse_establishment
			WHERE establishment_name LIKE '%Forever Resorts%'";
$sql_list = $GLOBALS['dbCon']->prepare($statement);
$sql_list->execute();
$sql_list->bind_result($code, $name);
while ($sql_list->fetch()) {
	if ($code == 'A00993' || $code == 'A00994') continue;
//	echo "$code-$name<br>";
	$forever_list[] = $code;
}

//Get list ogf head office assessments
$statement = "SELECT assessment_id
			FROM nse_establishment_assessment
			WHERE establishment_code = 'A00993' AND assessment_status='draft'";
$sql_hq = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);
$sql_hq->execute();
$sql_hq->bind_result($assessment_id);
while ($sql_hq->fetch()) {
	$hq_list[] = $assessment_id;
}
$sql_hq->close();

//Prepare statement - insert assessment
$statement = "INSERT INTO nse_establishment_assessments
				(establishment_code, assessment_status, assessment_type_2,active,invoice_id)
			VALUES
				(?,'draft','fa','Y',?)";
$sql_insert = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - update
$statement = "UPDATE nse_establishment_assessment
			SET establishment_code=?
			WHERE assessment_id=?
			LIMIT 1";
$sql_update = $GLOBALS['dbCon']->prepare($statement);

foreach ($forever_list as $key=>$establishment_code) {
//	$sql_insert->bind_param('ss', $establishment_code, $invoice_id);
//	$sql_insert->execute();
//	$sql_insert->free_result();

	if (isset($hq_list[$key])) {
		$sql_update->bind_param('ss', $establishment_code, $hq_list[$key]);
		$sql_update->execute();
		$sql_update->free_result();
	}

	echo "$establishment_code - {$hq_list[$key]}<br>";
}


?>
