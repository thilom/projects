<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/utility/PHPMailer/class.phpmailer.php";

$sent = array('Dan3 Brown', 'Dan Brown','Johan Meiring','Juliuso, Madelene Ges','Ursula Mouton','Marina Jordaan','SABC','TV Licence','Roger morris', 'Jason dieeudi');
$mail = new PHPMailer();
$file = 'mailer_sent.txt';
//$sent_data = array();

$data_file = file_get_contents($file);
$sent_data = explode(',', $data_file);

//Get list of email
$statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) AS contact_name, email
			FROM nse_user
			WHERE roles = 'c' OR roles IS NULL";
$sql_contacts = $GLOBALS['db_pdo']->prepare($statement);
$sql_contacts->execute();
$sql_contacts_data = $sql_contacts->fetchAll();

$template = file_get_contents('mailer_aaqa.html');
echo "<button onclick='resetTimeout()'>stop</button>";
$counter = 0;
$mail->AddReplyTo('vanessa@aatravelmail.co.za', 'Vanessa Sand');
foreach ($sql_contacts_data as $data) {
	if (in_array($data['contact_name'], $sent)) continue;
	if (empty($data['email'])) continue;
	if (in_array($data['user_id'], $sent_data)) {
		echo "{$data['contact_name']} {$data['email']} <span style='color: red'><========</span><br>";
		continue;
	}
//	if ($data['email'] == 'meloth14@gmail.com' || $data['email'] == 'thilo@aatravel.co.za') {

	if ($counter >= 10) break;
	$sent[] = $data['contact_name'];
	echo "{$data['contact_name']} {$data['email']}<br>";

	$email_template = str_replace("<!-- contact_name -->", $data['contact_name'], $template);

	try {
                $mail->ClearAllRecipients();
                $mail->AddAddress($data['email'], $data['contact_name']);
                $mail->SetFrom('vanessa@aatravelmail.co.za', 'Vanessa Sand');
                $mail->Subject = "Important and exciting changes regarding AA Travel Guides";
                $mail->MsgHTML($email_template);
                $mail->Send();
            } catch (phpmailerException $e) {
                echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo __LINE__ . ' :: ' . $e->getMessage(); //Boring error messages from anything else!
            }

	$sent_data[] = $data['user_id'];

	$counter++;
//	}
}

$file_sent = implode(',', $sent_data);
file_put_contents($file, $file_sent);

echo count($sent);
echo $email_template;

if ($counter > 0) {

	echo '<script type="text/javascript">
	  var timeout = setTimeout("location.reload(true);",10000);
	  function resetTimeout() {
	  alert("Timer Stopped");
		clearTimeout(timeout);
	  }
	</script>';
	}
?>
