<?php


include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/utility/PHPMailer/class.phpmailer.php";
$mail = new PHPMailer();

$statement = "SELECT a.establishment_code, c.country_name, a.establishment_name
                FROM nse_establishment AS a
                LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
                LEFT JOIN nse_nlocations_countries AS c ON b.country_id=c.country_id";
//$statement .= " WHERE a.establishment_code = '012414'";

$statement .= " WHERE b.country_id=2 OR b.country_id=3 OR b.country_id=4 OR b.country_id=5 OR b.country_id=6 OR b.country_id=7 OR b.country_id=8 OR b.country_id=9";
$sql_data = $GLOBALS['db_pdo']->prepare($statement);
$sql_data->execute();
$sql_data_data = $sql_data->fetchAll();
$sql_data->closeCursor();

foreach ($sql_data_data as $establishment_data) {
    $res_email = '';
    $statement = "SELECT contact_type, contact_value
                    FROM nse_establishment_public_contact
                    WHERE establishment_code = :establishment_code";
    $sql_res = $GLOBALS['db_pdo']->prepare($statement);
    $sql_res->bindParam(':establishment_code', $establishment_data['establishment_code']);
    $sql_res->execute();
    $sql_res_data = $sql_res->fetchAll();
    $sql_res->closeCursor();
    foreach ($sql_res_data as $res_data) {
        switch ($res_data['contact_type']) {
            case 'email':
                $res_email = $res_data['contact_value'];
                break;
        }
    }
    
    if (empty($res_email)) {
        $contact_data = get_contact($establishment_data['establishment_code']);
        if (!empty($contact_data['email'])) {
            $res_email = $contact_data['email'];
        }
    }
    
    if (empty($res_email)) continue;
    
//    $res_email = 'thilo@palladianbytes.co.za';
    $send_data[$establishment_data['establishment_code']]['email'] = $res_email;
    $send_data[$establishment_data['establishment_code']]['name'] = $establishment_data['establishment_name'];
    $send_data[$establishment_data['establishment_code']]['country'] = $establishment_data['country_name'];
    
    echo "{$establishment_data['establishment_code']},\"".(str_replace("'", "", $establishment_data['establishment_name']))."\",{$establishment_data['country_name']}, '$res_email'<br>";
    
}
foreach ($send_data as $data) {
    $email_content = file_get_contents('mailshot_sadc.html');
		try {
			$mail->ClearAllRecipients();
			$mail->AddAddress($data['email']);
			$mail->SetFrom('vweltman@accommodationawards.co.za', 'Vanessa Weltman');
			$mail->Subject = "Awards are good for business - especially when you are the Achiever";
			$mail->MsgHTML($email_content);
                        $mail->Send();
		} catch (phpmailerException $e) {
			echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
			$sent_data[] = $data[0];
		} catch (Exception $e) {
			echo __LINE__ . ' ::: ' . $e->getMessage(); //Boring error messages from anything else!
			$sent_data[] = $data[0];
		}
                
}

//var_dump($sql_data_data);

function get_contact($establishment_code) {
	$contact_data['name'] = '';
	$contact_data['tel'] = '';
	$contact_data['cell'] = '';
	$contact_data['email'] = '';
	$contact_data['postal'] = '';

	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), phone, cell, email
					FROM nse_user_establishments AS a
					LEFT JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=?
					ORDER BY a.qa_contact
					LIMIT 1";
	$sql_contact = $GLOBALS['dbCon']->prepare($statement);
	$sql_contact->bind_param('s', $establishment_code);
	$sql_contact->execute();
	$sql_contact->bind_result($contact_name, $tel, $cell, $email);
	$sql_contact->fetch();
	$sql_contact->close();

	//Postal Address
	$statement = "SELECT b.reservation_postal1, b.reservation_postal2, b.reservation_postal3, b.reservation_postal_code,
						a.postal_address_line1, a.postal_address_line2, a.postal_address_line3,	a.postal_address_code
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_reservation AS b ON a.establishment_code=b.establishment_code
					WHERE a.establishment_code=?
					LIMIT 1";
	$sql_postal = $GLOBALS['dbCon']->prepare($statement);
	$sql_postal->bind_param('s', $establishment_code);
	$sql_postal->execute();
	$sql_postal->bind_result($reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code,
						$postal_address_line1, $postal_address_line2, $postal_address_line3, $postal_address_code);
	$sql_postal->fetch();
	$sql_postal->close();
	$reservation_postal1 = trim($reservation_postal1);
	$reservation_postal2 = trim($reservation_postal2);
	$reservation_postal3 = trim($reservation_postal3);
	$reservation_postal_code = trim($reservation_postal_code);
	$postal_address_line1 = trim($postal_address_line1);
	$postal_address_line2 = trim($postal_address_line2);
	$postal_address_line3 = trim($postal_address_line3);
	$postal_address_code = trim($postal_address_code);

	//Remove trailing comma
	if (strpos($postal_address_line1, ',') > 0) $postal_address_line1 = substr($postal_address_line1, 0, -1);
	if (strpos($postal_address_line2, ',') > 0) $postal_address_line2 = substr($postal_address_line2, 0, -1);
	if (strpos($postal_address_line3, ',') > 0) $postal_address_line3 = substr($postal_address_line3, 0, -1);
	if (strpos($reservation_postal1, ',') > 0) $reservation_postal1 = substr($reservation_postal1, 0, -1);
	if (strpos($reservation_postal2, ',') > 0) $reservation_postal2 = substr($reservation_postal2, 0, -1);
	if (strpos($reservation_postal3, ',') > 0) $reservation_postal3 = substr($reservation_postal3, 0, -1);

	//Remove trailing dot
	if (strpos($reservation_postal_code, '.') > 0) $reservation_postal_code = substr($reservation_postal_code, 0, -1);
	if (strpos($postal_address_code, '.') > 0) $postal_address_code = substr($postal_address_code, 0, -1);

	//Assemble array
	$contact_data['name'] = $contact_name;
	$contact_data['tel'] = $tel;
	$contact_data['cell'] = $cell;
	$contact_data['email'] = $email;
	if (empty($reservation_postal1)) {
		if (!empty($postal_address_line1)) $contact_data['postal'] = "$postal_address_line1";
		if (!empty($postal_address_line2)) $contact_data['postal'] .= ", $postal_address_line2";
		if (!empty($postal_address_line3)) $contact_data['postal'] .= ", $postal_address_line3";
		if (!empty($postal_address_code)) $contact_data['postal'] .= ", $postal_address_code";

		$contact_data['postal_1'] = $postal_address_line1;
		$contact_data['postal_2'] = $postal_address_line2;
		$contact_data['postal_3'] = $postal_address_line3;
		$contact_data['postal_code'] = $postal_address_code;
	} else {
		if (!empty($reservation_postal1)) $contact_data['postal'] = "$reservation_postal1";
		if (!empty($reservation_postal2)) $contact_data['postal'] .= ", $reservation_postal2";
		if (!empty($reservation_postal3)) $contact_data['postal'] .= ", $reservation_postal3";
		if (!empty($reservation_postal_code)) $contact_data['postal'] .= ", $reservation_postal_code";

		$contact_data['postal_1'] = $reservation_postal1;
		$contact_data['postal_2'] = $reservation_postal2;
		$contact_data['postal_3'] = $reservation_postal3;
		$contact_data['postal_code'] = $reservation_postal_code;

	}

	return $contact_data;
}
?>
