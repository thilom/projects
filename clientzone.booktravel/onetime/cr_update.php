<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Get list of invoices to update
$row = 1;
if (($handle = fopen("pro-formas.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        $row++;
		if ($data[3] == 'Tax Invoice Number') {
			continue;
		}
            $invoices[] = $data[4];
    }
    fclose($handle);
}

//Prepare statement - Get reference
$statement = "SELECT j_inv_number
			FROM nse_invoice
			WHERE j_inv_reference=? AND j_inv_type=3
			LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);



//Prepare statement - Update Credit Note
$update_date = date('d-m-Y');
$statement = "UPDATE nse_invoice
			SET j_inv_total_price='0',
				j_inv_total_vat=0,
				j_inv_total=0,
				j_inv_info = CONCAT(COALESCE(j_inv_info, ''), ', Credit Note Correction: Updated via script on $update_date by Thilo Muller')
			WHERE j_inv_number = ?
			LIMIT 1";
$sql_cr_update = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

//Prepare statement - Update invoice
$statement = "UPDATE nse_invoice
			SET j_inv_type=1,
				j_inv_date=j_inv_created
			WHERE j_inv_number=?
			LIMIT 1";
$sql_inv_update = $GLOBALS['dbCon']->prepare($statement);

foreach ($invoices as $invoice_number) {
//	if ($invoice_number != '00003616') {
//		continue;
//	}
	 $sql_data->bind_param('s', $invoice_number);
	 $sql_data->execute();
	 $sql_data->bind_result($j_inv_number);
	 $sql_data->fetch();
	 $sql_data->free_result();

	 $sql_cr_update->bind_param('s', $j_inv_number);
	 $sql_cr_update->execute();

	 $sql_inv_update->bind_param('s', $invoice_number);
	 $sql_inv_update->execute();

	 echo "$invoice_number :: Ref - $j_inv_number<br>";
}

?>
