<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";



//Vars
$credit_notes = array();
$date_format = 'd-m-Y';

//CSV
$fh = fopen('pro-formas.csv', 'w');
$header = array(	'Establishment (code)',
				'Credit Note Number',
				'Credit Note Date',
				'Credit Note Total',
				'Tax Invoice Number',
				'Tax Invoice Date',
				'Tax Invoice Total',
				'Original Creation Date');
fputcsv($fh, $header);

//Draw Table
echo "<a href='pro-formas.csv'>Download</span>";
echo "<table width='100%'>";
echo "<tr><td>Establishment (code)</td><td>Credit Note Number</td><td>Credit Note Date</td><td>Credit Note Total</td><td>Tax Invoice Number</td><td>Tax Invoice Date</td><td>Tax Invoice Total</td><td>Original Creation Date</td></tr>";



//Get list of credit notes
$statement = "SELECT j_inv_id, j_inv_number, j_inv_reference, j_inv_date, j_inv_total
			FROM nse_invoice
			WHERE j_inv_type = 3
			ORDER BY j_inv_date DESC";
$sql_cr = $GLOBALS['dbCon']->prepare($statement);
$sql_cr->execute();
$sql_cr->bind_result($j_inv_id, $j_inv_number, $j_inv_reference, $j_inv_date, $j_inv_total);

$counter = 0;
while ($sql_cr->fetch()) {
	if (empty($j_inv_reference)) continue;

	$credit_notes[$counter] = array();
	$credit_notes[$counter]['cr_j_inv_id'] = $j_inv_id;
	$credit_notes[$counter]['cr_j_inv_number'] = $j_inv_number;
	$credit_notes[$counter]['cr_j_inv_reference'] = $j_inv_reference;
	$credit_notes[$counter]['cr_j_inv_date'] = date($date_format,$j_inv_date);
	$credit_notes[$counter]['cr_j_inv_total'] = $j_inv_total;

	$counter++;
}
$sql_cr->close();

//Got associated invoice
$statement = "SELECT j_inv_id, j_inv_number, j_inv_paid, j_inv_type, j_inv_date, j_inv_created, j_inv_total, b.establishment_code, b.establishment_name
			FROM nse_invoice AS a
			LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code
			WHERE j_inv_number=?
			LIMIT 1";
$sql_tax = $GLOBALS['dbCon']->prepare($statement);

foreach ($credit_notes as $key=>$value) {
	$j_inv_id = '';
	$j_inv_number = '';
	$j_inv_paid = '';
	$j_inv_date = '';
	$code = '';
	$name = '';

	$sql_tax->bind_param('s', $value['cr_j_inv_reference']);
	$sql_tax->execute();
	$sql_tax->bind_result($j_inv_id, $j_inv_number, $j_inv_paid, $j_inv_type, $j_inv_date, $j_inv_created, $j_inv_total, $code, $name);
	$sql_tax->fetch();

	$credit_notes[$key]['tax_j_inv_id'] = $j_inv_id;
	$credit_notes[$key]['tax_j_inv_number'] = $j_inv_number;
	$credit_notes[$key]['tax_j_inv_paid'] = $j_inv_paid;
	$credit_notes[$key]['tax_j_inv_type'] = $j_inv_type;
	$credit_notes[$key]['tax_j_inv_date'] = date($date_format, $j_inv_date);
	$credit_notes[$key]['tax_j_inv_created'] = date($date_format, $j_inv_created);
	$credit_notes[$key]['tax_j_inv_total'] = $j_inv_total;
	$credit_notes[$key]['code'] = $code;
	$credit_notes[$key]['name'] = $name;

	if (empty($j_inv_id) || $j_inv_paid > 0 || $j_inv_type != 2) {
		unset($credit_notes[$key]);
	}

	$sql_tax->free_result();
}

$counter = 0;
foreach ($credit_notes as $key=>$value) {

	$data = array("{$value['name']} ({$value['code']})",
				$value['cr_j_inv_number'],
				$value['cr_j_inv_date'],
				$value['cr_j_inv_total'],
				$value['tax_j_inv_number'],
				$value['tax_j_inv_date'],
				$value['tax_j_inv_total'],
				$value['tax_j_inv_created']);
	fputcsv($fh, $data);

	echo "<tr><td>{$value['name']} ({$value['code']})</td><td> {$value['cr_j_inv_number']}</td><td>{$value['cr_j_inv_date']}</td><td>{$value['cr_j_inv_total']}</td><td>{$value['tax_j_inv_number']}</td><td>{$value['tax_j_inv_date']}</td><td>{$value['tax_j_inv_total']}</td><td>{$value['tax_j_inv_created']}</td></tr>";
//	echo "$counter: {$value['j_inv_id']}, {$value['j_inv_number']}, {$value['j_inv_reference']}, {$value['tax_j_inv_id']}, {$value['tax_j_inv_number']},{$value['tax_j_inv_paid']},{$value['tax_j_inv_type']}<br>";
	$counter++;
}

echo "</table>";

fclose($fh);
?>
