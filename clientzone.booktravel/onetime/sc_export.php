<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$theme_id = 8;
$counter = 0;
$export_file = 'sc_export.csv';
$file_data = '';


//Open file
$fl = fopen($export_file, 'a');

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Get initial list of establishments
	$statement = "SELECT DISTINCT a.establishment_code
					FROM nse_establishment_themes AS a
					LEFT JOIN nse_establishment_restype AS c ON a.establishment_code=c.establishment_code
					WHERE theme_id=:theme_id OR subcategory_id=5 OR subcategory_id=32 OR subcategory_id=33 OR subcategory_id=34
					ORDER BY a.establishment_code DESC";
	$sql_theme = $GLOBALS['db_pdo']->prepare($statement);
	$sql_theme->bindParam(':theme_id', $theme_id);
	$sql_theme->execute();
	$sql_theme_data = $sql_theme->fetchAll();
	$sql_theme->execute();

//Get list of facilities
$statement = "SELECT b.icon_id, b.icon_description
				FROM nse_icon_lang AS b
				LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
				WHERE c.icon_category_id=3
				ORDER BY b.icon_description";
$sql_facilities = $GLOBALS['db_pdo']->prepare($statement);
$sql_facilities->execute();
$sql_facilities_data = $sql_facilities->fetchAll();
$sql_facilities->closeCursor();

//Get list of activities
$statement = "SELECT b.icon_id, b.icon_description
				FROM nse_icon_lang AS b
				LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
				WHERE c.icon_category_id=2
				ORDER BY b.icon_description";
$sql_activities = $GLOBALS['db_pdo']->prepare($statement);
$sql_activities->execute();
$sql_activities_data = $sql_activities->fetchAll();
$sql_activities->closeCursor();

//Get list of affiliations
$statement = "SELECT b.icon_id, b.icon_description
				FROM nse_icon_lang AS b
				LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
				WHERE c.icon_category_id=1
				ORDER BY b.icon_description";
$sql_affiliations = $GLOBALS['db_pdo']->prepare($statement);
$sql_affiliations->execute();
$sql_affiliations_data = $sql_affiliations->fetchAll();
$sql_affiliations->closeCursor();

//Get list of services
$statement = "SELECT b.icon_id, b.icon_description
				FROM nse_icon_lang AS b
				LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
				WHERE c.icon_category_id=4
				ORDER BY b.icon_description";
$sql_services = $GLOBALS['db_pdo']->prepare($statement);
$sql_services->execute();
$sql_services_data = $sql_services->fetchAll();
$sql_services->closeCursor();

//Get list of camping
$statement = "SELECT b.icon_id, b.icon_description
				FROM nse_icon_lang AS b
				LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
				WHERE c.icon_category_id=5
				ORDER BY b.icon_description";
$sql_camping = $GLOBALS['db_pdo']->prepare($statement);
$sql_camping->execute();
$sql_camping_data = $sql_camping->fetchAll();
$sql_camping->closeCursor();

//Assemble Header
$header[] = 'Establishment Code';
$header[] = 'Establishhment Name';
$header[] = 'Last Updated';
$header[] = 'Reservation Tel';
$header[] = '';
$header[] = 'Reservation Email';
$header[] = '';
$header[] = 'Contact Name';
$header[] = 'Contact Number';
$header[] = 'Contact Email';
$header[] = 'Skype';
$header[] = 'Twitter';
$header[] = 'Website';
$header[] = 'Postal Adress';
$header[] = 'Street Adress';
$header[] = 'Country';
$header[] = 'Province';
$header[] = 'Town';
$header[] = 'Suburb';
$header[] = 'Region';
$header[] = 'Sub-region';
$header[] = 'Star Grading';
$header[] = 'Res. Type';
$header[] = 'Quality Assurance';
$header[] = 'Room Count';
$header[] = 'Room Type';
$header[] = 'Price Low (new data)';
$header[] = 'Price High (new data)';
$header[] = 'Price Low (category)';
$header[] = 'Price High (category)';

foreach ($sql_facilities_data as $facility_value) {
	$header[] = $facility_value['icon_description'];
}
foreach ($sql_activities_data as $activities_value) {
	$header[] = $activities_value['icon_description'];
}
foreach ($sql_affiliations_data as $affiliations_value) {
	$header[] = $affiliations_value['icon_description'];
}
foreach ($sql_services_data as $services_value) {
	$header[] = $services_value['icon_description'];
}
foreach ($sql_camping_data as $cc_value) {
	$header[] = $cc_value['icon_description'];
}

//Write headers
$file_data = implode(',', $header);
fwrite($fl, $file_data . PHP_EOL);


//Get estab data and add to file
foreach ($sql_theme_data as $estab_data) {
	$data = array();
	$affiliations = array();
	$services = array();
	$camping = array();
	$facilities = array();
	$activities = array();

	$data[] = $estab_data['establishment_code'];

	//Establishment name
	$statement = "SELECT a.establishment_name, b.aa_category_name, a.website_url, DATE_FORMAT(a.last_updated,'%d-%m-%Y') AS last_updated,
							CONCAT(a.postal_address_line1, ', ', a.postal_address_line2, ', ', a.postal_address_line3, ', ', a.postal_address_code) AS postal,
							CONCAT(a.street_address_line1, ', ', a.street_address_line2, ', ', a.street_address_line3 ) AS physical
					FROM nse_establishment AS a
					LEFT JOIN nse_aa_category AS b ON a.aa_category_code=b.aa_category_code
					WHERE a.establishment_code = :establishment_code
					LIMIT 1";
	$sql_name = $GLOBALS['db_pdo']->prepare($statement);
	$sql_name->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_name->execute();
	$sql_name_data = $sql_name->fetch();
	$sql_name->closeCursor();
	$data[] = '"' . str_replace(array(',,', ', ,', '"', '"'), ',', trim($sql_name_data['establishment_name'])) . '"';
	$data[] = $sql_name_data['last_updated'];

	//Get old res data
	$statement = "SELECT reservation_tel, reservation_email
					FROM nse_establishment_reservation
					WHERE establishment_code = :establishment_code
					LIMIT 1";
	$sql_oldres = $GLOBALS['db_pdo']->prepare($statement);
	$sql_oldres->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_oldres->execute();
	$sql_oldres_data = $sql_oldres->fetch();
	$sql_oldres->closeCursor();

	//Reservation Tel / Email
	$tel = '';
	$email = '';
	$skype = '';
	$twitter = '';
	$statement = "SELECT contact_type, contact_value
				FROM nse_establishment_public_contact
				WHERE establishment_code = :establishment_code";
	$sql_res = $GLOBALS['db_pdo']->prepare($statement);
	$sql_res->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_res->execute();
	$sql_res_data = $sql_res->fetchAll();
	$sql_res->closeCursor();
	foreach ($sql_res_data as $res_data) {
		switch ($res_data['contact_type']) {
			case 'tel1':
				$tel = $res_data['contact_value'];
				break;
			case 'email':
				$email = $res_data['contact_value'];
				break;
			case 'skype':
				$skype = $res_data['contact_value'];
				break;
			case 'twitter':
				$twitter = $res_data['contact_value'];
				break;
		}
	}
	if (empty($tel)) {
		$data[] = $sql_oldres_data['reservation_tel'];
		$data[] = '*';
	} else {
		$data[] = $tel;
		$data[] = '';
	}

	if (empty($email)) {
		$data[] = $sql_oldres_data['reservation_email'];
		$data[] = '*';
	} else {
		$data[] = $email;
		$data[] = '';
	}

	//Get contact data
	$statement = "SELECT phone, email, CONCAT(firstname, ' ', lastname) AS contact_name
					FROM nse_user_establishments As a
					LEFT JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=:establishment_code
					LIMIT 1";
	$sql_contact = $GLOBALS['db_pdo']->prepare($statement);
	$sql_contact->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_contact->execute();
	$sql_contact_data = $sql_contact->fetch();
	$sql_contact->closeCursor();
	$data[] = $sql_contact_data['contact_name'];
	$data[] = $sql_contact_data['phone'];
	$data[] = $sql_contact_data['email'];

	$data[] = $skype;
	$data[] = $twitter;

	//Web address
	$data[] = $sql_name_data['website_url'];

	//Postal & Physical Address
	$data[] = '"' . str_replace(array(',,', ', ,', '"', '"'), ',', trim($sql_name_data['postal'])) . '"';
	$data[] = '"' . str_replace(array(',,', ', ,', '"', '"'), ',', trim($sql_name_data['physical'])) . '"';

	//Location
	$statement = "SELECT b.province_name, c.town_name, d.suburb_name, a.province_id, a.town_id, a.suburb_id, a.country_id, e.country_name
					FROM nse_establishment_location AS a
					LEFT JOIN nse_nlocations_provinces AS b ON a.province_id=b.province_id
					LEFT JOIN nse_nlocations_towns AS c ON a.town_id=c.town_id
					LEFT JOIN nse_nlocations_suburbs AS d ON a.suburb_id=d.suburb_id
					LEFT JOIN nse_nlocations_countries AS e ON a.country_id=e.country_id
					WHERE a.establishment_code=:establishment_code
					LIMIT 1";
	$sql_location = $GLOBALS['db_pdo']->prepare($statement);
	$sql_location->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_location->execute();
	$sql_location_data = $sql_location->fetch();
	$sql_location->closeCursor();
	$data[] = $sql_location_data['country_name'];
	$data[] = $sql_location_data['province_name'];
	$data[] = $sql_location_data['town_name'];
	$data[] = $sql_location_data['suburb_name'];

	//Regions
	$statement = "SELECT b.region_name
					FROM nse_nlocations_regions_content AS a
					LEFT JOIN nse_nlocations_regions AS b ON a.region_id=b.region_id
					WHERE a.location_type='town' AND a.location_id = :town_id
					LIMIT 1";
	$sql_region = $GLOBALS['db_pdo']->prepare($statement);
	$sql_region->bindParam(':town_id', $sql_location_data['town_id']);
	$sql_region->execute();
	$sql_region_data = $sql_region->fetch();
	$sql_region->closeCursor();
	$data[] = $sql_region_data['region_name'];

	//sub-Regions
	$statement = "SELECT b.region_name
					FROM nse_nlocations_regions_content AS a
					LEFT JOIN nse_nlocations_regions AS b ON a.region_id=b.region_id
					WHERE a.location_type='suburb' AND a.location_id = :suburb_id
					LIMIT 1";
	$sql_region = $GLOBALS['db_pdo']->prepare($statement);
	$sql_region->bindParam(':suburb_id', $sql_location_data['suburb_id']);
	$sql_region->execute();
	$sql_region_data = $sql_region->fetch();
	$sql_region->closeCursor();
	$data[] = $sql_region_data['region_name'];

	if ($sql_location_data['country_name'] == 'South Africa' || $sql_location_data['country_name'] == 'Australia') continue;

	//Star Grading & resType
	$statement  = "SELECT a.star_grading, b.subcategory_name
				FROM nse_establishment_restype AS a
				LEFT JOIN nse_restype_subcategory_lang AS b On a.subcategory_id=b.subcategory_id
				WHERE a.establishment_code=:establishment_code
				ORDER BY a.star_grading
				LIMIT 1";
	$sql_star = $GLOBALS['db_pdo']->prepare($statement);
	$sql_star->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_star->execute();
	$sql_star_data = $sql_star->fetch();
	$sql_star->closeCursor();
	$data[] = $sql_star_data['star_grading'];
	$data[] = $sql_star_data['subcategory_name'];

	//Quality Assurance
	$data[] = $sql_name_data['aa_category_name'];

	//Room Count
	$statement = "SELECT room_count, room_type
					FROM nse_establishment_data
					WHERE establishment_code = :establishment_code
					LIMIT 1";
	$sql_room = $GLOBALS['db_pdo']->prepare($statement);
	$sql_room->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_room->execute();
	$sql_room_data = $sql_room->fetch();
	$sql_room->closeCursor();
	$data[] = $sql_room_data['room_count'];
	$data[] = $sql_room_data['room_type'];

	//Price New
	$statement = "SELECT price_1, price_2
					FROM nse_temp_pricing
					WHERE establishment_code = :establishment_code
					LIMIT 1";
	$sql_price = $GLOBALS['db_pdo']->prepare($statement);
	$sql_price->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_price->execute();
	$sql_price_data = $sql_price->fetch();
	$sql_price->closeCursor();
	$data[] = $sql_price_data['price_1'];
	$data[] = $sql_price_data['price_2'];

	//Price Old
	$statement = "SELECT category_low, category
					FROM nse_establishment_pricing
					WHERE establishment_code = :establishment_code
					LIMIT 1";
	$sql_price2 = $GLOBALS['db_pdo']->prepare($statement);
	$sql_price2->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_price2->execute();
	$sql_price2_data = $sql_price2->fetch();
	$sql_price2->closeCursor();
	$data[] = $sql_price2_data['category_low'];
	$data[] = $sql_price2_data['category'];


	//Get facilities
	$statement = "SELECT a.icon_id, b.icon_description
				FROM nse_establishment_icon AS a
				LEFT JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
				LEFT JOIN nse_icon As c ON a.icon_id=c.icon_id
				WHERE establishment_code=:establishment_code AND c.icon_category_id=3";
	$sql_fac = $GLOBALS['db_pdo']->prepare($statement);
	$sql_fac->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_fac->execute();
	$sql_fac_data = $sql_fac->fetchAll();
	$sql_fac->closeCursor();
	foreach ($sql_fac_data as $fac_data) {
		$facilities[] = $fac_data['icon_description'];
	}
	foreach ($sql_facilities_data as $fac_data) {
		if (in_array($fac_data['icon_description'], $facilities)) {
			$data[] = 'Y';
		} else {
			$data[] = '';
		}
	}

	//Get activities
	$statement = "SELECT a.icon_id, b.icon_description
				FROM nse_establishment_icon AS a
				LEFT JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
				LEFT JOIN nse_icon As c ON a.icon_id=c.icon_id
				WHERE establishment_code=:establishment_code AND c.icon_category_id=2";
	$sql_act = $GLOBALS['db_pdo']->prepare($statement);
	$sql_act->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_act->execute();
	$sql_act_data = $sql_act->fetchAll();
	$sql_act->closeCursor();
	foreach ($sql_act_data as $act_data) {
		$activities[] = $act_data['icon_description'];
	}
	foreach ($sql_activities_data as $act_data) {
		if (in_array($act_data['icon_description'], $activities)) {
			$data[] = 'Y';
		} else {
			$data[] = '';
		}
	}

	//Get affiliations
	$statement = "SELECT a.icon_id, b.icon_description
				FROM nse_establishment_icon AS a
				LEFT JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
				LEFT JOIN nse_icon As c ON a.icon_id=c.icon_id
				WHERE establishment_code=:establishment_code AND c.icon_category_id=1";
	$sql_aff = $GLOBALS['db_pdo']->prepare($statement);
	$sql_aff->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_aff->execute();
	$sql_aff_data = $sql_aff->fetchAll();
	$sql_aff->closeCursor();
	foreach ($sql_aff_data as $aff_data) {
		$affiliations[] = $act_data['icon_description'];
	}
	foreach ($sql_affiliations_data as $aff_data) {
		if (!empty($affiliations) && in_array($aff_data['icon_description'], $affiliations)) {
			$data[] = 'Y';
		} else {
			$data[] = '';
		}
	}

	//Get services
	$statement = "SELECT a.icon_id, b.icon_description
				FROM nse_establishment_icon AS a
				LEFT JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
				LEFT JOIN nse_icon As c ON a.icon_id=c.icon_id
				WHERE establishment_code=:establishment_code AND c.icon_category_id=4";
	$sql_ser = $GLOBALS['db_pdo']->prepare($statement);
	$sql_ser->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_ser->execute();
	$sql_ser_data = $sql_ser->fetchAll();
	$sql_ser->closeCursor();
	foreach ($sql_ser_data as $ser_data) {
		$services[] = $act_data['icon_description'];
	}
	foreach ($sql_services_data as $ser_data) {
		if (!empty($services) && in_array($ser_data['icon_description'], $services)) {
			$data[] = 'Y';
		} else {
			$data[] = '';
		}
	}

	//Get camping
	$statement = "SELECT a.icon_id, b.icon_description
				FROM nse_establishment_icon AS a
				LEFT JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
				LEFT JOIN nse_icon As c ON a.icon_id=c.icon_id
				WHERE establishment_code=:establishment_code AND c.icon_category_id=5";
	$sql_cc = $GLOBALS['db_pdo']->prepare($statement);
	$sql_cc->bindParam(':establishment_code', $estab_data['establishment_code']);
	$sql_cc->execute();
	$sql_cc_data = $sql_cc->fetchAll();
	$sql_cc->closeCursor();
	foreach ($sql_cc_data as $cc_data) {
		$camping[] = $cc_data['icon_description'];
	}
	foreach ($sql_camping_data as $cc_data) {
		if (!empty($camping) && in_array($cc_data['icon_description'], $camping)) {
			$data[] = 'Y';
		} else {
			$data[] = '';
		}
	}



//	var_dump($facilities);
//	var_dump($activities);
//	var_dump($affiliations);
//	var_dump($data);

	//Write data
	$file_data = implode(',', $data);
	fwrite($fl, $file_data . PHP_EOL);


}



?>
