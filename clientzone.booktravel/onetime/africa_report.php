<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//vars
$country_name = $_GET['country'];
$file = '/onetime/africa_report.csv';
$headers = array('Code','Establishment Name','Country','Town','Reservation_email','Reservation Tel', 'Reservation Cell', 'Contact_name',
				'Contact Email','Contact Tel','Contact Cell', 'DBase Contact','DBase Tel','DBase Email');

//Get country ID
$statement = "SELECT country_id
			FROM nse_nlocations_countries
			WHERE country_name = :country_name
			LIMIT 1";
$sql_country = $GLOBALS['db_pdo']->prepare($statement);
$sql_country->bindParam(':country_name', $country_name);
$sql_country->execute();
$sql_country_data = $sql_country->fetch();
$sql_country->closeCursor();
$country_id = $sql_country_data['country_id'];

//Get establishments
$statement = "SELECT a.establishment_code, a.establishment_name, c.country_name, d.town_name
			FROM nse_establishment AS a
			LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
			LEFT JOIN nse_nlocations_countries AS c ON b.country_id=c.country_id
			LEFT JOIN nse_nlocations_towns AS d ON b.town_id=d.town_id
			WHERE b.country_id=:country_id";
$sql_establishment = $GLOBALS['db_pdo']->prepare($statement);
$sql_establishment->bindParam(':country_id', $country_id);
$sql_establishment->execute();
$sql_establishment_data = $sql_establishment->fetchAll();
$sql_establishment->closeCursor();

if (count($sql_establishment_data) == 0) {
	echo "No results in DB for $country_name";
	die();
}

//Prepare statement - contact
$statement = "SELECT CONCAT(firstname, ' ', lastname) as contact_name, email, phone, cell
			FROM nse_user_establishments AS a
			LEFT JOIN nse_user AS b ON a.user_id=b.user_id
			WHERE a.establishment_code=:establishment_code
			LIMIT 1";
$sql_contact = $GLOBALS['db_pdo']->prepare($statement);

//Prepare statement - public contact
$statement = "SELECT contact_type, contact_value
			FROM nse_establishment_public_contact
			WHERE establishment_code = :establishment_code";
$sql_public = $GLOBALS['db_pdo']->prepare($statement);


//Prepare statemen - dbase
$statement = 'SELECT OWNERMGR, TELNO1,EMAILADDR
			FROM nse_establishment_dbase
			WHERE establishment_code = :establishment_code
			LIMIT 1';
$sql_dbase = $GLOBALS['db_pdo']->prepare($statement);


$counter = 0;
foreach ($sql_establishment_data as $establishment_data) {
	$reservation_email = '';
	$reservation_tel = '';
	$reservation_cell = '';

	$csv_data[$counter][] = $establishment_data['establishment_code'];
	$csv_data[$counter][] = $establishment_data['establishment_name'];
	$csv_data[$counter][] = $establishment_data['country_name'];
	$csv_data[$counter][] = $establishment_data['town_name'];

	//Get public contact
	$sql_public->bindParam(':establishment_code', $establishment_data['establishment_code']);
	$sql_public->execute();
	$sql_public_data = $sql_public->fetchAll();
	foreach ($sql_public_data as $public_data) {
		switch ($public_data['contact_type']) {
			case 'email':
				$reservation_email = $public_data['contact_value'];
				break;
			case 'tel1':
				$reservation_tel = $public_data['contact_value'];
				break;
			case 'cell1':
				$reservation_cell = $public_data['contact_value'];
				break;
		}
	}

	$csv_data[$counter][] = $reservation_email;
	$csv_data[$counter][] = $reservation_tel;
	$csv_data[$counter][] = $reservation_cell;

	//Get contact
	$sql_contact->bindParam(':establishment_code', $establishment_data['establishment_code']);
	$sql_contact->execute();
	$sql_contact_data = $sql_contact->fetch();
	$csv_data[$counter][] = $sql_contact_data['contact_name'];
	$csv_data[$counter][] = $sql_contact_data['email'];
	$csv_data[$counter][] = $sql_contact_data['phone'];
	$csv_data[$counter][] = $sql_contact_data['cell'];

	//Get Dbase
	$sql_dbase->bindParam(':establishment_code', $establishment_data['establishment_code']);
	$sql_dbase->execute();
	$sql_dbase_data = $sql_dbase->fetch();
	$csv_data[$counter][] = $sql_dbase_data['OWNERMGR'];
	$csv_data[$counter][] = $sql_dbase_data['TELNO1'];
	$csv_data[$counter][] = $sql_dbase_data['EMAILADDR'];

	$counter++;
}

create_csv($headers, $csv_data, $file);

echo "<a href='$file'>Get CSV File</a> ";

function create_csv($headers, $csv_data, $file) {
	$fp = fopen($_SERVER['DOCUMENT_ROOT'] . $file, 'w');

	fputcsv($fp, $headers);

	foreach ($csv_data as $data) {
		fputcsv($fp, $data);
	}

	fclose($fp);
}

?>
