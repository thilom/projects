<?php

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

$for_month = 'Oct 2011';

//Vars
$invoice_list = array();
$credit_list = array();

//Get list of invoices
$counter = 0;
$statement = "SELECT j_inv_id, j_inv_date, j_inv_vat, j_inv_total_price, j_inv_total_vat, j_inv_total, j_inv_paid, b.establishment_code, b.establishment_name, j_inv_number
			FROM nse_invoice AS a
			LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code
			WHERE j_inv_type=2
			ORDER BY j_inv_id DESC";
$sql_invoices = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);
$sql_invoices->execute();
$sql_invoices->bind_result($invoice_id, $invoice_date, $vat_percentage, $total_excl, $vat_rand, $total_vat, $paid, $establishment_code, $establishment_name, $invoice_number);
while ($sql_invoices->fetch()) {
	$invoice_list[$counter]['id'] = $invoice_id;
	$invoice_list[$counter]['invoice_number'] = "'$invoice_number";
	$invoice_list[$counter]['code'] = "'$establishment_code";
	$invoice_list[$counter]['name'] = $establishment_name;
	$invoice_list[$counter]['current_date'] = $invoice_date;
	$invoice_list[$counter]['vat_percentage'] = $vat_percentage;
	$invoice_list[$counter]['total_excl'] = number_format($total_excl,2,'.','');
	$invoice_list[$counter]['vat_rand'] = number_format($vat_rand,2,'.','');
	$invoice_list[$counter]['total_vat'] = number_format($total_vat,2,'.','');
	$invoice_list[$counter]['calc_total'] = number_format($total_excl + $vat_rand,2,'.','');
	$invoice_list[$counter]['paid'] = number_format($paid,2,'.','');
	$counter++;
}
$sql_invoices->close();

//Prepare statement - First payment
$statement = "SELECT j_invp_date
			FROM nse_invoice_item_paid
			WHERE j_invp_invoice=?
			ORDER BY j_invp_date DESC
			LIMIT 1";
$sql_payments = $GLOBALS['dbCon']->prepare($statement);



foreach ($invoice_list as $key=>$data) {
	$payment_date = '';
	$sql_payments->bind_param('i', $data['id']);
	$sql_payments->execute();
	$sql_payments->bind_result($payment_date);
	$sql_payments->fetch();
	
	if (empty($payment_date)) {
		$invoice_list[$key]['payment_date'] = date('Y-m-d', $data['current_date']);
		$check_date = date('M Y', $data['current_date']);
	} else {
		$invoice_list[$key]['payment_date'] = date('Y-m-d', $payment_date);
		$check_date = date('M Y', $payment_date);
	}

	if ($check_date != $for_month) {
		unset($invoice_list[$key]);
		continue;
	}
	
	$invoice_list[$key]['current_date'] = date('Y-m-d', $data['current_date']);
}
$sql_payments->close();

$fp = fopen('invoices.csv', 'w');
$c = 0;
foreach ($invoice_list as $key=>$data) {
	if ($c == 0) {
		fputcsv($fp, array('ID','Invoice Number', 'Establishment Code', 'Establishment Name','Current Date','VAT Percentage','Total Excl','VAT Rand','Total Incl','Calculated Total','paid','New Date'));
	}
	fputcsv($fp, $data);
	$c++;
}
echo "<a href='invoices.csv'>Download File Here</a>";

fclose($fp);

//Get all credit notes 
$counter = 0;
$statement = "SELECT j_inv_id, j_inv_date,  j_inv_total_price, j_inv_total_vat, j_inv_total, b.establishment_code, b.establishment_name, j_inv_number, j_inv_reference
			FROM nse_invoice AS a
			LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code
			WHERE j_inv_type=3
			ORDER BY j_inv_id DESC";
$sql_credits = $GLOBALS['dbCon']->prepare($statement);
$sql_credits->execute();
$sql_credits->bind_result($invoice_id, $invoice_date, $total_excl, $vat_rand, $total_vat, $establishment_code, $establishment_name, $invoice_number, $invoice_reference);
while ($sql_credits->fetch()) {
	$credit_list[$counter]['invoice_number'] = $invoice_number;
	$credit_list[$counter]['reference'] = $invoice_reference;
	$credit_list[$counter]['code'] = "'$establishment_code";
	$credit_list[$counter]['name'] = $establishment_name;
	$credit_list[$counter]['current_date'] = $invoice_date;
	$credit_list[$counter]['total_excl'] = number_format($total_excl,2,'.','');
	$credit_list[$counter]['vat_rand'] = number_format($vat_rand,2,'.','');
	$credit_list[$counter]['total_vat'] = number_format($total_vat,2,'.','');
	
	$counter++;
}
$sql_credits->close();

//Prepare statement - invoice type
$statement = "SELECT j_inv_type
			FROM nse_invoice
			WHERE j_inv_number = ?
			LIMIT 1";
$sql_type = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

foreach ($credit_list as $key=>$data) {
	$invoice_type = '';
	
	$sql_type->bind_param('s', $data['reference']);
	$sql_type->execute();
	$sql_type->bind_result($invoice_type);
	$sql_type->fetch();
	
	$check_date = date('M Y', $data['current_date']);
	
	if ($invoice_type != 2 || $check_date != $for_month) {
		unset($credit_list[$key]);
		continue;
	}
	
	$credit_list[$key]['invoice_type'] = $invoice_type;
	$credit_list[$key]['current_date'] = date('Y-m-d', $data['current_date']);
}

$fp = fopen('credits.csv', 'w');
$c = 0;
foreach ($credit_list as $key=>$data) {
	if ($c == 0) {
		fputcsv($fp, array('Invoice Number', 'Reference', 'Establishment Code', 'Establishment Name','Current Date','Total Excl','VAT Rand','Total Incl'));
	}
	fputcsv($fp, $data);
	$c++;
}
echo "<p><a href='credits.csv'>Credit Notes</a>";
?>
