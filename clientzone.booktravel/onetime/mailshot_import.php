<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$read_file = 'mailshot_write.csv';
$done_file = 'mailshot_done.csv';
$template = file_get_contents('mailshot_import.html');



//Open files
$rf = fopen($read_file, 'r');

//Done Data
$contents = file_get_contents($done_file);
$done = explode(',', $contents);
$done_counter = count($done);


//Save Data
if (count($_POST) > 0) {

	$establishment_code = $_POST['establishment_code'];

	//Save Tel Data
	$statement = "SELECT COUNT(*) AS count
					FROM nse_establishment_public_contact
					WHERE establishment_code=:establishment_code AND contact_type='tel1'";
	$sql_count = $GLOBALS['db_pdo']->prepare($statement);
	$sql_count->bindParam(':establishment_code', $establishment_code);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();

	if ($sql_count_data['count'] == 0) {
		$statement = "INSERT INTO nse_establishment_public_contact
						(establishment_code, contact_type, contact_value)
						VALUES
						(:establishment_code, 'tel1', :contact_value)";
		$sql_tel = $GLOBALS['db_pdo']->prepare($statement);
		$sql_tel->bindParam(':contact_value', $_POST['reservation_tel']);
		$sql_tel->bindParam(':establishment_code', $establishment_code);
		$sql_tel->execute();
	} else {
		$statement = "UPDATE nse_establishment_public_contact
					SET contact_value=:contact_value
					WHERE establishment_code=:establishment_code AND contact_type='tel1'
					LIMIT 1";
		$sql_tel = $GLOBALS['db_pdo']->prepare($statement);
		$sql_tel->bindParam(':contact_value', $_POST['reservation_tel']);
		$sql_tel->bindParam(':establishment_code', $establishment_code);
		$sql_tel->execute();
	}

	//Save Email Data
	$statement = "SELECT COUNT(*) AS count
					FROM nse_establishment_public_contact
					WHERE establishment_code=:establishment_code AND contact_type='email'";
	$sql_count = $GLOBALS['db_pdo']->prepare($statement);
	$sql_count->bindParam(':establishment_code', $establishment_code);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();

	if ($sql_count_data['count'] == 0) {
		$statement = "INSERT INTO nse_establishment_public_contact
						(establishment_code, contact_type, contact_value)
						VALUES
						(:establishment_code, 'email', :contact_value)";
		$sql_tel = $GLOBALS['db_pdo']->prepare($statement);
		$sql_tel->bindParam(':contact_value', $_POST['reservation_email']);
		$sql_tel->bindParam(':establishment_code', $establishment_code);
		$sql_tel->execute();
	} else {
		$statement = "UPDATE nse_establishment_public_contact
					SET contact_value=:contact_value
					WHERE establishment_code=:establishment_code AND contact_type='email'
					LIMIT 1";
		$sql_tel = $GLOBALS['db_pdo']->prepare($statement);
		$sql_tel->bindParam(':contact_value', $_POST['reservation_email']);
		$sql_tel->bindParam(':establishment_code', $establishment_code);
		$sql_tel->execute();
	}


	//Save Location
	$statement = "UPDATE nse_establishment_location
					SET province_id=:province_id,
						country_id=:country_id,
						town_id=:town_id,
						suburb_id=:suburb_id
					WHERE establishment_code=:establishment_code
					LIMIT 1";
	$sql_loc = $GLOBALS['db_pdo']->prepare($statement);
	$sql_loc->bindParam(':country_id', $_POST['cou']);
	$sql_loc->bindParam(':province_id', $_POST['pro']);
	$sql_loc->bindParam(':town_id', $_POST['tow']);
	$sql_loc->bindParam(':suburb_id', $_POST['sub']);
	$sql_loc->bindParam(':establishment_code', $establishment_code);
	$sql_loc->execute();

	//Save Skype address
	$statement = "INSERT INTO nse_establishment_public_contact
						(establishment_code, contact_type, contact_value)
					VALUES
						(:establishment_code, 'skype', :contact_value)";
	$sql_tel = $GLOBALS['db_pdo']->prepare($statement);
	$sql_tel->bindParam(':contact_value', $_POST['skypeAddress']);
	$sql_tel->bindParam(':establishment_code', $establishment_code);
	$sql_tel->execute();

	//Save Twitter address
	$statement = "INSERT INTO nse_establishment_public_contact
						(establishment_code, contact_type, contact_value)
					VALUES
						(:establishment_code, 'twitter', :contact_value)";
	$sql_tel = $GLOBALS['db_pdo']->prepare($statement);
	$sql_tel->bindParam(':contact_value', $_POST['twitterAddress']);
	$sql_tel->bindParam(':establishment_code', $establishment_code);
	$sql_tel->execute();

	//Update star grading`
	$star_grading = (int) $_POST['starGrading'];
	$statement = "UPDATE nse_establishment_restype
					SET star_grading=:star_grading
					WHERE establishment_code = :establishment_code";
	$sql_star = $GLOBALS['db_pdo']->prepare($statement);
	$sql_star->bindParam(':star_grading', $star_grading);
	$sql_star->bindParam(':establishment_code', $establishment_code);
	$sql_star->execute();

	//Postal Address
	$statement = "UPDATE nse_establishment
					SET postal_address_line1=:postal1,
						postal_address_line2=:postal2,
						postal_address_line3=:postal3,
						postal_address_code=:postal4
					WHERE establishment_code=:establishment_code
					LIMIT 1";
	$sql_postal = $GLOBALS['db_pdo']->prepare($statement);
	$sql_postal->bindParam(':postal1', $_POST['postal1']);
	$sql_postal->bindParam(':postal2', $_POST['postal2']);
	$sql_postal->bindParam(':postal3', $_POST['postal3']);
	$sql_postal->bindParam(':postal4', $_POST['postal4']);
	$sql_postal->bindParam(':establishment_code', $establishment_code);
	$sql_postal->execute();

	//Street Address
	$statement = "UPDATE nse_establishment
					SET street_address_line1=:street1,
						street_address_line2=:street2,
						street_address_line3=:street3
					WHERE establishment_code=:establishment_code
					LIMIT 1";
	$sql_postal = $GLOBALS['db_pdo']->prepare($statement);
	$sql_postal->bindParam(':street1', $_POST['physical1']);
	$sql_postal->bindParam(':street2', $_POST['physical2']);
	$sql_postal->bindParam(':street3', $_POST['physical3']);
	$sql_postal->bindParam(':establishment_code', $establishment_code);
	$sql_postal->execute();

	//Get list of facilities
	$statement = "SELECT b.icon_id, b.icon_description
					FROM nse_icon_lang AS b
					LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
					WHERE c.icon_category_id=3";
	$sql_icons = $GLOBALS['db_pdo']->prepare($statement);
	$sql_icons->execute();
	$sql_icons_data = $sql_icons->fetchAll();
	$sql_icons->closeCursor();
	$facility_ids = array();
	foreach ($sql_icons_data as $icons_data) {
		$facility_ids[] = $icons_data['icon_id'];
	}
	$sql_icons->closeCursor();

	//Delete Current icons
	$statement = "DELETE FROM
					nse_establishment_icon
					WHERE establishment_code = :establishment_code
						AND icon_id=:icon_id";
	$sql_fac_delete = $GLOBALS['db_pdo']->prepare($statement);
	foreach ($facility_ids as $icon_id) {
		$sql_fac_delete->bindParam(':establishment_code', $establishment_code);
		$sql_fac_delete->bindParam(':icon_id', $icon_id);
		$sql_fac_delete->execute();
	}

	//Insert Facilities
	$statement = "INSERT INTO nse_establishment_icon
						(icon_id, establishment_code)
					VALUES
						(:icon_id, :establishment_code)";
	$sql_fac_insert = $GLOBALS['db_pdo']->prepare($statement);
	foreach ($_POST['facilities'] as $icon_id) {
		$sql_fac_insert->bindParam(':establishment_code', $establishment_code);
		$sql_fac_insert->bindParam(':icon_id', $icon_id);
		$sql_fac_insert->execute();
	}

	//Get list of activities
	$statement = "SELECT b.icon_id, b.icon_description
					FROM nse_icon_lang AS b
					LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
					WHERE c.icon_category_id=2";
	$sql_icons = $GLOBALS['db_pdo']->prepare($statement);
	$sql_icons->execute();
	$sql_icons_data = $sql_icons->fetchAll();
	$sql_icons->closeCursor();
	$activity_ids = array();
	foreach ($sql_icons_data as $icons_data) {
		$activity_ids[] = $icons_data['icon_id'];
	}
	$sql_icons->closeCursor();


	//Delete Current icons
	$statement = "DELETE FROM
					nse_establishment_icon
					WHERE establishment_code = :establishment_code
						AND icon_id=:icon_id";
	$sql_fac_delete = $GLOBALS['db_pdo']->prepare($statement);
	foreach ($activity_ids as $icon_id) {
		$sql_fac_delete->bindParam(':establishment_code', $establishment_code);
		$sql_fac_delete->bindParam(':icon_id', $icon_id);
		$sql_fac_delete->execute();
	}

	//Insert Facilities
	$statement = "INSERT INTO nse_establishment_icon
						(icon_id, establishment_code)
					VALUES
						(:icon_id, :establishment_code)";
	$sql_fac_insert = $GLOBALS['db_pdo']->prepare($statement);
	foreach ($_POST['activities'] as $icon_id) {
		$sql_fac_insert->bindParam(':establishment_code', $establishment_code);
		$sql_fac_insert->bindParam(':icon_id', $icon_id);
		$sql_fac_insert->execute();
	}

	//Update restype
	$statement = "UPDATE nse_establishment_restype
					SET subcategory_id=:subcategory_id
					WHERE establishment_code=:establishment_code
					LIMIT 1";
	$sql_res = $GLOBALS['db_pdo']->prepare($statement);
	$sql_res->bindParam(':establishment_code', $establishment_code);
	$sql_res->bindParam(':subcategory_id', $_POST['resType']);
	$sql_res->execute();

	//Pricing
	$statement = "INSERt INTO nse_temp_pricing
						(establishment_code, price_1, price_2)
					VALUES
						(:establishment_code, :price_1, :price_2)";
	$sql_price = $GLOBALS['db_pdo']->prepare($statement);
	$sql_price->bindParam(':establishment_code', $establishment_code);
	$sql_price->bindParam(':price_1', $_POST['price_1']);
	$sql_price->bindParam(':price_2', $_POST['price_2']);
	$sql_price->execute();

}


$todo_counter = 0;
while ($data = fgetcsv($rf)) {
	if (!in_array($data[22], $done)) {
		if (empty($data[22])) continue;
		if (!isset($this_data)) $this_data = $data;
		$todo_counter++;
//		break;
	}
}

$est_code = str_pad($this_data[22], 6, 0, STR_PAD_LEFT);

//Get current establishment data
$statement = "SELECT a.establishment_name, b.aa_category_name, b.aa_category_code, CONCAT(postal_address_line1, '<br>', postal_address_line2, '<br>', postal_address_line3, '<br>', postal_address_code) AS postal,
					CONCAT(street_address_line1, '<br>', street_address_line2, '<br>', street_address_line3 ) AS physical
				FROM nse_establishment AS a
				LEFT JOIN nse_aa_category AS b ON a.aa_category_code=b.aa_category_code
				WHERE a.establishment_code = :establishment_code
				LIMIT 1";
$sql_estab = $GLOBALS['db_pdo']->prepare($statement);
$sql_estab->bindParam(':establishment_code', $est_code);
$sql_estab->execute();
$sql_estab_data = $sql_estab->fetch();
$sql_estab->closeCursor();

//Get current reservation data
$statement = "SELECT contact_type, contact_value
				FROM nse_establishment_public_contact
				WHERE establishment_code = :establishment_code";
$sql_res = $GLOBALS['db_pdo']->prepare($statement);
$sql_res->bindParam(':establishment_code', $est_code);
$sql_res->execute();
$sql_res_data = $sql_res->fetchAll();
$sql_res->closeCursor();
$cur_tel = '';
$cur_email = '';
foreach ($sql_res_data as $res_data) {
	switch($res_data['contact_type']) {
		case 'tel1':
			$cur_tel = $res_data['contact_value'];
			break;
		case 'email':
			$cur_email = $res_data['contact_value'];
			break;
	}
}

//Get current location
$statement = "SELECT b.province_name, c.town_name, d.suburb_name, a.province_id, a.town_id, a.suburb_id, a.country_id
				FROM nse_establishment_location AS a
				LEFT JOIN nse_nlocations_provinces AS b ON a.province_id=b.province_id
				LEFT JOIN nse_nlocations_towns AS c ON a.town_id=c.town_id
				LEFT JOIN nse_nlocations_suburbs AS d ON a.suburb_id=d.suburb_id
				WHERE a.establishment_code=:establishment_code
				LIMIT 1";
$sql_location = $GLOBALS['db_pdo']->prepare($statement);
$sql_location->bindParam(':establishment_code', $est_code);
$sql_location->execute();
$sql_location_data = $sql_location->fetch();
$sql_location->closeCursor();
$cur_location = "{$sql_location_data['province_name']} ==> {$sql_location_data['town_name']} ==> {$sql_location_data['suburb_name']}";

//Get star grading
$statement  = "SELECT star_grading
				FROM nse_establishment_restype
				WHERE establishment_code=:establishment_code
				ORDER BY star_grading
				LIMIT 1";
$sql_star = $GLOBALS['db_pdo']->prepare($statement);
$sql_star->bindParam(':establishment_code', $est_code);
$sql_star->execute();
$sql_star_data = $sql_star->fetch();
$sql_star->closeCursor();

//Get QA List
$qa_list = "<option value=''> </option>";
$statement = "SELECT aa_category_code, aa_category_name
				FROM nse_aa_category
				ORDER BY aa_category_name";
$sql_qa = $GLOBALS['db_pdo']->prepare($statement);
$sql_qa->execute();
$sql_qa_data = $sql_qa->fetchAll();
$sql_qa->closeCursor();
foreach ($sql_qa_data as $qa_data) {
	if ($qa_data['aa_category_code'] == $sql_estab_data['aa_category_code']) {
		$qa_list .= "<option value='{$qa_data['aa_category_code']}' selected>{$qa_data['aa_category_name']}</option>";
	} else {
		$qa_list .= "<option value='{$qa_data['aa_category_code']}'>{$qa_data['aa_category_name']}</option>";
	}

}

//Expand postal address
$postal[1] = '';
$postal[2] = '';
$postal[3] = '';
$postal[4] = '';
if (!empty($this_data[13])) {
	$nPostal = explode("\n", $this_data[13]);
	$counter = 1;
	foreach ($nPostal as $value) {
		if (!empty($value)) {
			$postal[$counter] = $value;
			$counter++;
		}
	}
}

//Expand physical address
$physical[1] = '';
$physical[2] = '';
$physical[3] = '';
$physical[4] = '';
if (!empty($this_data[11])) {
	$nPostal = explode("\n", $this_data[11]);
	$counter = 1;
	foreach ($nPostal as $value) {
		if (!empty($value)) {
			$physical[$counter] = $value;
			$counter++;
		}
	}
}

//Facility list
$list = array('Washing machine'=>95,
				'Linen Supplied'=>96,
				'Towels supplied'=>97,
				'Toiletries Supplied'=>67,
				'Hair dryer'=>66,
				'Fridge / minibar fridge'=>73,
				'Microwave Oven'=>62,
				'Stove / hob'=>98,
				'Normal Oven'=>99,
				'TV in unit - SABC only'=>104,
				'TV in unit - DSTV channels'=>105,
				'FREE wireless internet'=>106,
				'Fans'=>69,
				'DVD Player'=>100,
				'Built in Safe'=>71,
				'Heating'=>70,
				'Fans'=>101,
				'Bathroom with bath'=>102,
				'Bathroom with shower'=>103,
				'Iron / ironing board in unit'=>74,
				'Air conditioning'=>68);

$fac_list = explode(',', $this_data[17]);
foreach ($fac_list as $key=>$value) {
	$fac_list[$key] = trim($value);
}

//Get facilities
$statement = "SELECT a.icon_id, b.icon_description
				FROM nse_establishment_icon AS a
				LEFT JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
				LEFT JOIN nse_icon As c ON a.icon_id=c.icon_id
				WHERE establishment_code=:establishment_code AND c.icon_category_id=3";
$sql_icon = $GLOBALS['db_pdo']->prepare($statement);
$sql_icon->bindParam(':establishment_code', $est_code);
$sql_icon->execute();
$sql_icon_data = $sql_icon->fetchAll();
$sql_icon->closeCursor();
$cu_facilities = '';
foreach ($sql_icon_data as $icon_data) {
	$cu_facilities .= "{$icon_data['icon_description']}<br>";
}

//Get list of all facilities
$statement = "SELECT b.icon_id, b.icon_description
				FROM nse_icon_lang AS b
				LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
				WHERE c.icon_category_id=3
				ORDER BY b.icon_description";
$sql_icons = $GLOBALS['db_pdo']->prepare($statement);
$sql_icons->execute();
$sql_icons_data = $sql_icons->fetchAll();
$sql_icons->closeCursor();
$cu_facilities_list = '';
$counter = 0;
foreach ($sql_icons_data as $icons_data) {
	$in_list = false;
	foreach ($fac_list as $facility) {
		if (isset($list[$facility])) {
			if ($icons_data['icon_id'] == $list[$facility]) {
				$in_list = true;
			}
		}
	}

	$cu_facilities_list .= "<input type='checkbox' name='facilities[]' value='{$icons_data['icon_id']}' id='f$counter'";
	if ($in_list) {
		$cu_facilities_list .= " checked ";
	}
	$cu_facilities_list .= "> <label for='f$counter'>{$icons_data['icon_description']}</label><br>";
	$counter++;
}

//Get activities
$statement = "SELECT a.icon_id, b.icon_description
				FROM nse_establishment_icon AS a
				LEFT JOIN nse_icon_lang AS b ON a.icon_id=b.icon_id
				LEFT JOIN nse_icon As c ON a.icon_id=c.icon_id
				WHERE establishment_code=:establishment_code AND c.icon_category_id=2";
$sql_icon = $GLOBALS['db_pdo']->prepare($statement);
$sql_icon->bindParam(':establishment_code', $est_code);
$sql_icon->execute();
$sql_icon_data = $sql_icon->fetchAll();
$sql_icon->closeCursor();
$cu_activities = '';
foreach ($sql_icon_data as $icon_data) {
	$cu_activities .= "{$icon_data['icon_description']}<br>";
}

//Get list of all activities
$statement = "SELECT b.icon_id, b.icon_description
				FROM nse_icon_lang AS b
				LEFT JOIN nse_icon As c ON b.icon_id=c.icon_id
				WHERE c.icon_category_id=2
				ORDER BY b.icon_description";
$sql_icons = $GLOBALS['db_pdo']->prepare($statement);
$sql_icons->execute();
$sql_icons_data = $sql_icons->fetchAll();
$sql_icons->closeCursor();
$cu_activities_list = '';
$counter = 0;
foreach ($sql_icons_data as $icons_data) {
	$cu_activities_list .= "<input type='checkbox' name='activities[]' value='{$icons_data['icon_id']}' id='a$counter'> <label for='a$counter'>{$icons_data['icon_description']}</label><br>";
	$counter++;
}

//Get current restype
$statement = "SELECT a.subcategory_id, b.subcategory_name
				FROM nse_establishment_restype AS a
				LEFT JOIN nse_restype_subcategory_lang AS b ON a.subcategory_id=b.subcategory_id
				WHERE a.establishment_code=:establishment_code";
$sql_res = $GLOBALS['db_pdo']->prepare($statement);
$sql_res->bindParam(':establishment_code', $est_code);
$sql_res->execute();
$sql_res_data = $sql_res->fetchAll();
$sql_res->closeCursor();
$cu_accommodation = '';
foreach ($sql_res_data as $res_data) {
	$cu_accommodation .= "{$res_data['subcategory_name']}<br>";
}

//Get country list
$statement = "SELECT country_id, country_name
				FROM nse_nlocations_countries";
$sql_countries = $GLOBALS['db_pdo']->prepare($statement);
$sql_countries->execute();
$sql_countries_data = $sql_countries->fetchAll();
$sql_countries->closeCursor();
$country_list = "<option value=''></option>";
foreach ($sql_countries_data as $country_data) {
	if ($country_data['country_id'] == $sql_location_data['country_id']) {
		$country_list .= "<option value='{$country_data['country_id']}' selected>{$country_data['country_name']}</option>";
	} else {
		$country_list .= "<option value='{$country_data['country_id']}'>{$country_data['country_name']}</option>";
	}

}

//Get province list
$province_list = "<option value=''></option>";
if (!empty($sql_location_data['country_id'])) {
	$statement = "SELECT province_id, province_name
					FROM nse_nlocations_provinces
					WHERE country_id=:country_id
					ORDER BY province_name";
	$sql_provinces = $GLOBALS['db_pdo']->prepare($statement);
	$sql_provinces->bindParam(':country_id', $sql_location_data['country_id']);
	$sql_provinces->execute();
	$sql_provinces_data = $sql_provinces->fetchAll();
	$sql_provinces->closeCursor();
	foreach ($sql_provinces_data as $province_data) {
		if ($province_data['province_id'] == $sql_location_data['province_id']) {
			$province_list .= "<option value='{$province_data['province_id']}' selected>{$province_data['province_name']}</option>";
		} else {
			$province_list .= "<option value='{$province_data['province_id']}'>{$province_data['province_name']}</option>";
		}

	}
}

//Get town list
$town_list = "<option value=''></option>";
if (!empty($sql_location_data['province_id'])) {
	$statement = "SELECT town_id, town_name
					FROM nse_nlocations_towns
					WHERE province_id=:province_id
					ORDER BY town_name";
	$sql_towns = $GLOBALS['db_pdo']->prepare($statement);
	$sql_towns->bindParam(":province_id", $sql_location_data['province_id']);
	$sql_towns->execute();
	$sql_towns_data = $sql_towns->fetchAll();
	$sql_towns->closeCursor();
	foreach ($sql_towns_data as $town_data) {
		if ($town_data['town_id'] == $sql_location_data['town_id']) {
			$town_list .= "<option value='{$town_data['town_id']}' selected>{$town_data['town_name']}</option>";
		} else {
			$town_list .= "<option value='{$town_data['town_id']}'>{$town_data['town_name']}</option>";
		}

	}
}

//Get suburb list
$suburb_list = "<option value=''></option>";
if (!empty($sql_location_data['town_id'])) {
	$statement = "SELECT suburb_id, suburb_name
					FROM nse_nlocations_suburbs
					WHERE town_id=:town_id
					ORDER BY suburb_name";
	$sql_suburbs = $GLOBALS['db_pdo']->prepare($statement);
	$sql_suburbs->bindParam(":town_id", $sql_location_data['town_id']);
	$sql_suburbs->execute();
	$sql_suburbs_data = $sql_suburbs->fetchAll();
	$sql_suburbs->closeCursor();
	foreach ($sql_suburbs_data as $suburb_data) {
		if ($suburb_data['suburb_id'] == $sql_location_data['suburb_id']) {
			$suburb_list .= "<option value='{$suburb_data['suburb_id']}' selected>{$suburb_data['suburb_name']}</option>";
		} else {
			$suburb_list .= "<option value='{$suburb_data['suburb_id']}'>{$suburb_data['suburb_name']}</option>";
		}

	}
}

//Get list of all accommodation
$statement = "SELECT a.category_id, b.category_name
				FROM nse_restype_category AS a
				LEFT JOIN nse_restype_category_lang AS b ON a.category_id=b.category_id
				WHERE a.toplevel_id=1";
$sql_cat = $GLOBALS['db_pdo']->prepare($statement);
$sql_cat->execute();
$sql_cat_data = $sql_cat->fetchAll();
$sql_cat->closeCursor();

$res_list = "<option value=''> </option>";
$statement = "SELECT a.subcategory_id, b.subcategory_name
				FROM nse_restype_subcategory_parent AS a
				LEFT JOIN nse_restype_subcategory_lang AS b ON a.subcategory_id=b.subcategory_id
				WHERE a.category_id=:category_id
				ORDER BY b.subcategory_name";
$sql_res = $GLOBALS['db_pdo']->prepare($statement);
foreach ($sql_cat_data as $cat_data) {
	$res_list .= "<optgroup label='{$cat_data['category_name']}'> ";
	$sql_res->bindParam(":category_id", $cat_data['category_id']);
	$sql_res->execute();
	$sql_res_data = $sql_res->fetchAll();

	foreach ($sql_res_data as $res_data) {
		$res_list .= "<option value='{$res_data['subcategory_id']}'>{$res_data['subcategory_name']}</option>";
	}

	$res_list .= "</optgroup>";
}

//Replace tags
$template = str_replace('<!-- cl_establishment_name -->', $this_data[1], $template);
$template = str_replace('<!-- upd_establishment_name -->', $this_data[1], $template);
$template = str_replace('<!-- cur_establishment_name -->', $sql_estab_data['establishment_name'], $template);
$template = str_replace('<!-- upd_establishment_code -->', $est_code, $template);

$template = str_replace('<!-- cl_reservation_tel -->', str_replace(',', '',$this_data[4]), $template);
$template = str_replace('<!-- upd_reservation_tel -->', str_replace(',', '',$this_data[4]), $template);
$template = str_replace('<!-- cur_reservation_tel -->', $cur_tel, $template);

$template = str_replace('<!-- cl_reservation_email -->', $this_data[5], $template);
$template = str_replace('<!-- upd_reservation_email -->', $this_data[5], $template);
$template = str_replace('<!-- cur_reservation_email -->', $cur_email, $template);

$template = str_replace('<!-- cl_location -->', $this_data[2], $template);
$template = str_replace('<!-- province_name -->', '', $template);
$template = str_replace('<!-- province_id -->', '', $template);
$template = str_replace('<!-- town_name -->', '', $template);
$template = str_replace('<!-- town_id -->', '', $template);
$template = str_replace('<!-- suburb_name -->', '', $template);
$template = str_replace('<!-- suburb_id -->', '', $template);
$template = str_replace('<!-- cur_location -->', $cur_location, $template);
$template = str_replace('<!-- country_list -->', $country_list, $template);
$template = str_replace('<!-- province_list -->', $province_list, $template);
$template = str_replace('<!-- town_list -->', $town_list, $template);
$template = str_replace('<!-- suburb_list -->', $suburb_list, $template);

$template = str_replace('<!-- cl_skype_address -->', $this_data[9], $template);
$template = str_replace('<!-- upd_skype_address -->', $this_data[9], $template);

$template = str_replace('<!-- cl_twitter_address -->', $this_data[10], $template);
$template = str_replace('<!-- upd_twitter_address -->', $this_data[10], $template);

$template = str_replace('<!-- cl_star_grading -->', $this_data[14], $template);
$template = str_replace('<!-- upd_star_grading -->', $this_data[14], $template);
$template = str_replace('<!-- cu_star_grading -->', $sql_star_data['star_grading'], $template);

$template = str_replace('<!-- cl_qa -->', $this_data[15], $template);
$template = str_replace('<!-- upd_qa_list -->', $qa_list, $template);
$template = str_replace('<!-- cu_qa -->', $sql_estab_data['aa_category_name'], $template);

$template = str_replace('<!-- cl_postal -->', str_replace("\n", "<br>", $this_data[13]), $template);
$template = str_replace('<!-- upd_postal1 -->', $postal[1], $template);
$template = str_replace('<!-- upd_postal2 -->', $postal[2], $template);
$template = str_replace('<!-- upd_postal3 -->', $postal[3], $template);
$template = str_replace('<!-- upd_postal4 -->', $postal[4], $template);
$template = str_replace('<!-- cu_postal -->', $sql_estab_data['postal'], $template);

$template = str_replace('<!-- cl_physical -->', str_replace("\n", "<br>", $this_data[11]), $template);
$template = str_replace('<!-- upd_physical1 -->', $physical[1], $template);
$template = str_replace('<!-- upd_physical2 -->', $physical[2], $template);
$template = str_replace('<!-- upd_physical3 -->', $physical[3], $template);
$template = str_replace('<!-- cu_physical -->', $sql_estab_data['physical'], $template);

$template = str_replace('<!-- cl_facilities -->', str_replace(',', '<br>', $this_data[17]), $template);
$template = str_replace('<!-- cu_facilities_list -->', $cu_facilities_list, $template);
$template = str_replace('<!-- cu_facilities -->', $cu_facilities, $template);

$template = str_replace('<!-- cl_activities -->', str_replace(',', '<br>', $this_data[18]), $template);
$template = str_replace('<!-- cu_activities_list -->', $cu_activities_list, $template);
$template = str_replace('<!-- cu_activities -->', $cu_activities, $template);

$template = str_replace('<!-- cl_accomodation -->', str_replace(',', '<br>', $this_data[16]) . '<hr>' . str_replace(',', '<br>', $this_data[21]), $template);
$template = str_replace('<!-- cu_accomodation_list -->', $res_list, $template);
$template = str_replace('<!-- cu_accomodation -->', $cu_accommodation, $template);

$template = str_replace('<!-- cl_pricing_1 -->', $this_data[19], $template);
$template = str_replace('<!-- cl_pricing_2 -->', $this_data[20], $template);
$template = str_replace('<!-- price_1 -->', $this_data[19], $template);
$template = str_replace('<!-- price_2 -->', $this_data[20], $template);

$template = str_replace('<!-- done_counter -->', $done_counter, $template);
$template = str_replace('<!-- todo_counter -->', $todo_counter, $template);


echo $template;

$done[] = $est_code;
$line = implode(',', $done);
file_put_contents($done_file, $line);

?>
