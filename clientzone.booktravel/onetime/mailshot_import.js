/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 */
function getLocations(id) {
	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {popLocations(id)};

	Url = "/onetime/location_list.ajax.php?id=" + document.getElementById(id).value;
	switch (id) {
		case 'pro':
			Url += "&type=town";
			break;
		case 'tow':
			Url += "&type=suburb";
			break;
		case 'cou':
			Url += "&type=province";
			break;
		default:
			break;
	}


	xmlhttp.open("GET", Url, true);
	xmlhttp.send();
}


function popLocations(id) {
	if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		switch (id) {
			case 'cou':
				townSelect = document.getElementById('pro');
				break;
			case 'pro':
				townSelect = document.getElementById('tow');
				break;
			case 'tow':
				townSelect = document.getElementById('sub');
				break;
			default:
				break;
		}


		townSelect.options.length=0;

		var townList = xmlhttp.responseText.split('|');
		for (i = 0; i < townList.length; i++) {
			town = townList[i].split('~');
			if (town[1] == undefined) continue;
			townSelect.options[townSelect.length] = new Option(town[1], town[0]);
		}

	}
}
