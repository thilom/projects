<?php

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$table = '';
$cost = 46; // %of cost to assessors
$total_estab = 0;
$total_turnover = 0;
$total_cost = 0;
$total_income = 0;

//Assemble table info
$table .= "<h1>Quality Assurance projection report</h1>";


//Assemble table header
$table .= "<table style='width: 800px; border-collapse: collapse; border: 1px solid black'>";
$table .= "<tr style='background-color: gray; border-bottom: 1px solid black'>";
$table .= "<th> </th>";
$table .= "<th>Establishments</th>";
$table .= "<th>Turnover (R)</th>";
$table .= "<th>Cost - $cost% (R)</th>";
$table .= "<th>Income (R)</th>";
$table .= "</tr>";

//Assemble data
for ($i=1; $i<13; $i++) {
	
	$display_date = date('M Y', strtotime( "NOW + $i months"));
	$current_date = date('mY', strtotime( "NOW + $i months"));
	$estab_count = 0;
	$turnover = 0;
	
	
	//Get establihments for month
	$statement = "SELECT a.establishment_code, b.room_count
					FROM nse_establishment_assessment AS a
					LEFT JOIN nse_establishment_data AS b ON a.establishment_code=b.establishment_code
					WHERE DATE_FORMAT(a.renewal_date, '%m%Y') = $current_date AND a.assessment_status='complete'";
	$sql_estabs = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_estabs->execute();
	$sql_estabs->bind_result($establishment_code, $room_count);
	while ($sql_estabs->fetch()) {
		$estab_count++;
		$turnover += get_value($room_count);
	}
	$sql_estabs->close();
	
	//Calculations
	$total_estab += $estab_count;
	$total_turnover += $turnover;
	$cost_month = ($turnover * (($cost/100)+1)) - $turnover;
	$total_cost += $cost_month;
	$income = $turnover - $cost_month;
	$total_income += $income;
	
	$table .= "<tr style='border-bottom: 1px solid silver'>";
	$table .= "<td style='background-color: silver'>$display_date</td>";
	$table .= "<td>$estab_count</td>";
	$table .= "<td style='text-align: right'>" . number_format($turnover,0) . "</td>";
	$table .= "<td style='text-align: right'>" . number_format($cost_month) . "</td>";
	$table .= "<td style='text-align: right'>" . number_format($income) . "</td>";
	$table .= "</tr>";
}

//Totals
$table .= "<tr>";
$table .= "<td style='font-weight: bold; border-top: 3px double black'> Totals</td>";
$table .= "<td style='border-top: 3px double black;font-weight: bold;' >$total_estab</td>";
$table .= "<td style='border-top: 3px double black; text-align: right;font-weight: bold;'>" . number_format($total_turnover) . "</td>";
$table .= "<td style='border-top: 3px double black; text-align: right;font-weight: bold;'>" . number_format($total_cost) . "</td>";
$table .= "<td style='border-top: 3px double black; text-align: right;font-weight: bold;'>" . number_format($total_income) . "</td>";
$table .= "</tr>";

//Close table
$table .= "</table>";
$table .= "<tt>Note: Does not take cancellations or new clients into account</tt><br>";
$table .= "<tt>Note: September 2012 onwards does not reflect Quality assurance that is in the process of bieng renewed.</tt>";


echo $table;


function get_value($room_count) {
	$value = 0;
	if ($room_count < 4) {
		$value = 2100;
	} else if($room_count < 7) {
		$value = 2400;
	} else if($room_count < 13) {
		$value = 2850;
	} else if($room_count < 20) {
		$value = 3100;
	} else if($room_count < 50) {
		$value = 4400;
	} else if($room_count < 76) {
		$value = 4800;
	} else if($room_count < 101) {
		$value = 6200;
	} else {
		$value = 7500;
	}
	
	
	return $value;
}
?>
