<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Open File


//echo "<table width='100%'>";
//echo "<tr>";
//echo "<th>Code</th>";
//echo "<th>Name</th>";
//echo "<th>Assessor</th>";
//echo "<th>Rep</th>";
//echo "<th>Country</th>";
//echo "<th>Province</th>";
//echo "<th>Town</th>";
//echo "<th>Suburb</th>";
//echo "<th>AA-SA</th>";
//echo "<th>AA-Zim</th>";
//echo "<th>eTravel</th>";
//echo "<th>Nightsbridge</th>";
//echo "<th>QA Enter</th>";
//echo "<th>QA Out</th>";
//echo "<th>Active</th>";
//echo "<th>QA Category</th>";
//echo "<th>Restype</th>";
//echo "<th>Restype</th>";
//echo "<th>Restype</th>";
//echo "<th>Stars</th>";
//echo "<th>Stars</th>";
//echo "<th>Stars</th>";
//echo "<th>Rooms</th>";
//echo "<th>Last Assessment</th>";
//echo "<th>Next Renewal</th>";
//echo "<th>Contact Name</th>";
//echo "<th>Contact Email</th>";
//echo "<th>Contact Tel</th>";
//echo "<th>Contact Cell</th>";
//echo "<th>Postal1</th>";
//echo "<th>Postal2</th>";
//echo "<th>Postal3</th>";
//echo "<th>Postal4</th>";
//echo "</tr>";

$keys = array('Code',
'Name',
'Assessor',
'Rep',
'Country',
'Province',
'Town',
'Suburb',
'AA-SA',
'AA-Zim',
'eTravel',
'Nightsbridge',
'QA Enter',
'QA Out',
'Active',
'QA Category',
'Restype(1)',
'Restype(2)',
'Restype(3)',
'Stars(1)',
'Stars(2)',
'Stars(3)',
'Rooms',
	'Postal 1',
'Postal 2',
'Postal 3',
'Postal Code',
'Last Assessment',
'Next Renewal',
'Salutation',
'Contact Name',
'Contact Email',
'Contact Tel',
'Contact Cell');

//Get initial Data
$counter = 0;
$statement = "SELECT a.establishment_code, a.establishment_name, CONCAT(b.firstname, ' ', b.lastname),
				CONCAT(c.firstname, ' ', c.lastname), e.country_name, f.province_name, g.town_name, h.suburb_name,
				aa_estab, aa_zim, etravel, nightsbridge_bbid, IF(enter_date='0000-00-00','',enter_date),
				IF(cancelled_date='0000-00-00','',cancelled_date), a.active, j.aa_category_name,
				m.room_count, n.reservation_postal1, n.reservation_postal2, n.reservation_postal3,
				n.reservation_postal_code, a.postal_address_line1, a.postal_address_line2, a.postal_address_line3,
				a.postal_address_code
				FROM nse_establishment AS a
				LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
				LEFT JOIN nse_user AS c ON a.rep_id1=c.user_id
				LEFT JOIN nse_establishment_location AS d ON a.establishment_code=d.establishment_code
				LEFT JOIN nse_nlocations_countries AS e ON d.country_id=e.country_id
				LEFT JOIN nse_nlocations_provinces AS f ON d.province_id=f.province_id
				LEFT JOIN nse_nlocations_towns AS g ON d.town_id=g.town_id
				LEFT JOIN nse_nlocations_suburbs AS h ON d.suburb_id=h.suburb_id
				LEFT JOIN nse_establishment_qa_cancelled AS i ON a.establishment_code=i.establishment_code
				LEFT JOIN nse_aa_category AS j ON a.aa_category_code=j.aa_category_code
				LEFT JOIN nse_establishment_data AS m ON a.establishment_code=m.establishment_code
				LEFT JOIN nse_establishment_reservation AS n ON a.establishment_code=n.establishment_code
				LEFT JOIN nse_establishment_icon AS o ON a.establishment_code=o.establishment_code
				WHERE (o.icon_id=21 OR o.icon_id=22) AND d.country_id=1
				ORDER BY e.country_name, f.province_name, g.town_name, h.suburb_name";
$sql_est = $GLOBALS['dbCon']->prepare($statement);
$sql_est->execute();
$sql_est->bind_result($establishment_code, $establishment_name, $assessor, $rep, $country, $province, 
						$town, $suburb, $aa_estab, $aa_zim, $etravel, $nightsbridge, $enter_date, $qa_out,
						$active, $qa_category, $rooms, $postal1, $postal2, $postal3, $postal4,
						$postal1a, $postal2a, $postal3a, $postal4a);
while ($sql_est->fetch()) {
	$sql_est_data[$counter]['establishment_code'] = $establishment_code;
	$sql_est_data[$counter]['establishment_name'] = $establishment_name;
	$sql_est_data[$counter]['assessor'] = $assessor;
	$sql_est_data[$counter]['rep'] = $rep;
	$sql_est_data[$counter]['country'] = $country;
	$sql_est_data[$counter]['province'] = $province;
	$sql_est_data[$counter]['town'] = $town;
	$sql_est_data[$counter]['suburb'] = $suburb;
	$sql_est_data[$counter]['aa_estab'] = $aa_estab;
	$sql_est_data[$counter]['aa_zim'] = $aa_zim;
	$sql_est_data[$counter]['etravel'] = $etravel;
	$sql_est_data[$counter]['nightsbridge'] = $nightsbridge;
	$sql_est_data[$counter]['enter_date'] = $enter_date;
	$sql_est_data[$counter]['qa_out'] = $qa_out;
	$sql_est_data[$counter]['active'] = $active;
	$sql_est_data[$counter]['qa_category'] = $qa_category;
	$sql_est_data[$counter]['restype1'] = '';
	$sql_est_data[$counter]['restype2'] = '';
	$sql_est_data[$counter]['restype3'] = '';
	$sql_est_data[$counter]['stars1'] = '';
	$sql_est_data[$counter]['stars2'] = '';
	$sql_est_data[$counter]['stars3'] = '';
	$sql_est_data[$counter]['rooms'] = $rooms;
	
	if (empty($postal1)) {
		$sql_est_data[$counter]['postal1'] = $postal1a;
		$sql_est_data[$counter]['postal2'] = $postal2a;
		$sql_est_data[$counter]['postal3'] = $postal3a;
		$sql_est_data[$counter]['postal4'] = "'" . $postal4a;
	} else {
		$sql_est_data[$counter]['postal1'] = $postal1;
		$sql_est_data[$counter]['postal2'] = $postal2;
		$sql_est_data[$counter]['postal3'] = $postal3;
		$sql_est_data[$counter]['postal4'] = "'" . $postal4;
	}
	$counter++;
}
$sql_est->free_result();
$sql_est->close();

//Prepare statement - Last Assessment
$statement = "SELECT DATE_FORMAT(assessment_date,'%Y-%m-%d'), renewal_date
				FROM nse_establishment_assessment
				WHERE establishment_code = ?
				ORDER BY assessment_date DESC
				LIMIT 1";
$sql_ass = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement = Contact
$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), b.email, b.phone, b.cell, b.salutation
				FROM nse_user_establishments AS a
				LEFT JOIN nse_user AS b ON a.user_id=b.user_id
				WHERE a.establishment_code=?
				ORDER BY a.user_id DESC
				LIMIT 1";
$sql_contact = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Star grading
$statement = "SELECT IF(a.star_grading=0,'',a.star_grading), l.subcategory_name
				FROM nse_establishment_restype AS a
				LEFT JOIN nse_restype_subcategory_lang AS l ON a.subcategory_id=l.subcategory_id
				WHERE a.establishment_code=?";
$sql_star_grading = $GLOBALS['dbCon']->prepare($statement);

$counter = 0;
foreach ($sql_est_data as $key=>$est_data) {
	$assessment_date = '';
	$next_renewal = '';
	$contact_name = '';
	$email = '';
	$tel = '';
	$cell = '';
	$star_grading = '';
	
	
	$sql_ass->bind_param('s', $est_data['establishment_code']);
	$sql_ass->execute();
	$sql_ass->bind_result($assessment_date, $next_renewal);
	$sql_ass->fetch();
	$sql_est_data[$key]['assessment_date'] = $assessment_date;
	$sql_est_data[$key]['next_renewal'] = $next_renewal;
	$sql_ass->free_result();

	$sql_contact->bind_param('s', $est_data['establishment_code']);
	$sql_contact->execute();
	$sql_contact->bind_result($contact_name, $email, $tel, $cell, $salutation);
	$sql_contact->fetch();
	$sql_est_data[$key]['salutation'] = $salutation;
	$sql_est_data[$key]['contact_name'] = $contact_name;
	$sql_est_data[$key]['email'] = $email;
	$sql_est_data[$key]['tel'] = $tel;
	$sql_est_data[$key]['cell'] = $cell;
	$sql_contact->free_result();
	
	$star_count = 1;
	$sql_star_grading->bind_param('s', $est_data['establishment_code']);
	$sql_star_grading->execute();
	$sql_star_grading->bind_result($star_grading, $restype);
	while ($sql_star_grading->fetch()) {
		$sql_est_data[$key]['stars'.$star_count] = $star_grading;
		$sql_est_data[$key]['restype'.$star_count] = $restype;
		$star_count++;
	}
	$sql_star_grading->free_result();
}

$fp = fopen('estab_birds.csv', 'w');
foreach ($sql_est_data as $est_data) {
	$est_data['establishment_code'] = "'".$est_data['establishment_code'];
//	echo "<tr>";
//	echo "<td>{$est_data['establishment_code']}</td>";
//	echo "<td>{$est_data['establishment_name']}</td>";
//	echo "<td>{$est_data['assessor']}</td>";
//	echo "<td>{$est_data['rep']}</td>";
//	echo "<td>{$est_data['country']}</td>";
//	echo "<td>{$est_data['province']}</td>";
//	echo "<td>{$est_data['town']}</td>";
//	echo "<td>{$est_data['suburb']}</td>";
//	echo "<td>{$est_data['aa_estab']}</td>";
//	echo "<td>{$est_data['aa_zim']}</td>";
//	echo "<td>{$est_data['etravel']}</td>";
//	echo "<td>{$est_data['nightsbridge']}</td>";
//	echo "<td>{$est_data['enter_date']}</td>";
//	echo "<td>{$est_data['qa_out']}</td>";
//	echo "<td>{$est_data['active']}</td>";
//	echo "<td>{$est_data['qa_category']}</td>";
//	echo "<td>{$est_data['restype1']}</td>";
//	echo "<td>{$est_data['restype2']}</td>";
//	echo "<td>{$est_data['restype3']}</td>";
//	echo "<td>{$est_data['stars1']}</td>";
//	echo "<td>{$est_data['stars2']}</td>";
//	echo "<td>{$est_data['stars3']}</td>";
//	echo "<td>{$est_data['rooms']}</td>";
//	echo "<td>{$est_data['assessment_date']}</td>";
//	echo "<td>{$est_data['next_renewal']}</td>";
//	echo "<td>{$est_data['contact_name']}</td>";
//	echo "<td>{$est_data['email']}</td>";
//	echo "<td>{$est_data['tel']}</td>";
//	echo "<td>{$est_data['cell']}</td>";
//	echo "<td>{$est_data['postal1']}</td>";
//	echo "<td>{$est_data['postal2']}</td>";
//	echo "<td>{$est_data['postal3']}</td>";
//	echo "<td>{$est_data['postal4']}</td>";
//	echo "</tr>";

	
		if ($counter == 0) {
			fputcsv($fp, $keys);
		}
		fputcsv($fp, $est_data);
		$counter++;
}

echo "<a href='estab_birds.csv'>Download File Here</a>";

fclose($fp);

echo "</table>";

?>
