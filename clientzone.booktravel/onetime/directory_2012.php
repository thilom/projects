<?php

/**
 * List establishments for 2012 directory
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$counter = 1;

echo "<a href='directory_2012.csv'>Download File Here</a>";

echo "<table width=100% >";
echo "<tr>";
echo "<th></th>";
echo "<th>Code</th>";
echo "<th>Name</th>";
echo "<th>Country</th>";
echo "<th>Province</th>";
echo "<th>Town</th>";
echo "<th>Suburb</th>";
echo "<th>Regions 1</th>";
echo "<th>Regions 2</th>";
echo "<th>Telephone 1</th>";
echo "<th>Telephone 2</th>";
echo "<th>QA Status</th>";
echo "<th>Restype</th>";
echo "<th>Room Type</th>";
echo "<th>Room Count</th>";
echo "<th>Star Grading</th>";
echo "<th>Price</th>";
echo "<th>Assessor</th>";

//echo "<th>Current Assessment</th>";
//echo "<th>Last Assessment</th>";
echo "</tr>";

$headers = array('Code', 'Name','Country','Province','Town','Suburb','Regions 1', 'Region 2', 'Telephone 1','Telephone 2','QA Status','Restype','Room Type','Room Count','Star Grading','Price','Assessor');
$fp = fopen('directory_2012.csv', 'w');
fputcsv($fp, $headers);

//Get initial list
$statement = "SELECT establishment_code, establishment_name
			FROM nse_establishment
			WHERE aa_category_code != '' && aa_category_code IS NOT NULL";

$sql_establishment = $GLOBALS['db_pdo']->prepare($statement);
$sql_establishment->execute();
$sql_establishment_data = $sql_establishment->fetchAll();
$sql_establishment->closeCursor();

foreach ($sql_establishment_data as $establishment_data) {

	//Get QA Out
	$qa_out = get_qa_out($establishment_data['establishment_code']);
	if (!empty($qa_out)) continue;

	//Get current assessment
	$current_assessment = get_current_assessment($establishment_data['establishment_code']);

	//Last assessment
	$last_assessment = get_last_assessment($establishment_data['establishment_code']);
	if (empty($last_assessment['last_assessment'])) continue;

	//Location
	$location = get_location($establishment_data['establishment_code']);

	//Telephane
	$tel = get_tel($establishment_data['establishment_code']);

	//QA Status
	$qa_status = '';
	$qa_status = get_qa_status($establishment_data['establishment_code']);

	//Restype
	$restype = get_restype($establishment_data['establishment_code']);

	//Room count
	$room_data = get_roomcount($establishment_data['establishment_code']);

	//Star grading
	$star = get_stargrading($establishment_data['establishment_code']);

	//Get assessor
	$assessor = get_assessor($establishment_data['establishment_code']);

	$est_data[0] = $establishment_data['establishment_code'];
	$est_data[1] = $establishment_data['establishment_name'];
	$est_data[2] = $location['country'];
	$est_data[3] = $location['province'];
	$est_data[4] = $location['town'];
	$est_data[5] = $location['suburb'];
	$est_data[6] = '';
	$est_data[7] = '';

	foreach ($location['regions'] as $key=>$region) {
		if ($key == 0) {
			$est_data[6] = $region;
		}
		if ($key == 1) {
			$est_data[7] = $region;
		}
	}
	$est_data[8] = $tel['tel1'];
	$est_data[9] = $tel['tel2'];
	$est_data[10] = $qa_status;
	$est_data[11] = $restype;
	$est_data[12] = $room_data['room_type'];
	$est_data[13] = $room_data['room_count'];
	$est_data[14] = $star;
	$est_data[15] = $room_data['description'];
	$est_data[16] = $assessor;

	echo "<tr><td>$counter</td><td>{$establishment_data['establishment_code']}</td><td>{$establishment_data['establishment_name']}</td>";

	echo "<td>{$location['country']}</td>";
	echo "<td>{$location['province']}</td>";
	echo "<td>{$location['town']}</td>";
	echo "<td>{$location['suburb']}</td>";

	echo "<td>";
	foreach ($location['regions'] as $region) {
		echo "$region<br>";
	}
	echo "</td>";

	echo "<td>{$tel['tel1']}</td>";
	echo "<td>{$tel['tel2']}</td>";
	echo "<td>$qa_status</td>";
	echo "<td>$restype</td>";
	echo "<td>{$room_data['room_type']}</td>";
	echo "<td>{$room_data['room_count']}</td>";
	echo "<td>$star</td>";
	echo "<td>{$room_data['description']}</td>";
	echo "<td>$assessor</td>";
	//echo "<td>$current_assessment</td>";
//	echo "<td>{$last_assessment['last_assessment']}</td>";
	echo "</tr>";

	fputcsv($fp, $est_data);
	$counter++;
}

fclose($fp);

echo "</table>";

/*
 * Get the next assessment date for an establishment
 * Looks for the latest complete assessment and then fetches the next renewal date
 *
 */
function get_last_assessment($establishment_code) {
	$data['next_assessment'] = '';
	$data['last_assessment'] = '';

	$statement = "SELECT DATE_FORMAT(renewal_date, '%Y-%m-%d'), DATE_FORMAT(assessment_date, '%Y-%m-%d')
					FROM nse_establishment_assessment
					WHERE establishment_code=? AND assessment_status='complete'
					ORDER BY assessment_date DESC
					LIMIT 1";
	$sql_next = $GLOBALS['dbCon']->prepare($statement);
	$sql_next->bind_param('s', $establishment_code);
	$sql_next->execute();
	$sql_next->bind_result($next_assessment, $last_assessment);
	$sql_next->fetch();
	$sql_next->close();

	$data['next_assessment'] = $next_assessment;
	$data['last_assessment'] = $last_assessment;
	return $data;
}

/*
 * Get the current assessment status
 * Looks for the latest complete assessment and then fetches the next renewal date
 *
 */
function get_current_assessment($establishment_code) {
	$assessment_status = '';

	$statement = "SELECT assessment_status
					FROM nse_establishment_assessment
					WHERE establishment_code=? AND assessment_status!='complete'
					LIMIT 1";
	$sql_next = $GLOBALS['dbCon']->prepare($statement);
	$sql_next->bind_param('s', $establishment_code);
	$sql_next->execute();
	$sql_next->bind_result($assessment_status);
	$sql_next->fetch();
	$sql_next->close();

	return $assessment_status;
}

/**
 * Get last QA Out date if available
 * @param string $establishment_code
 */
function get_qa_out($establishment_code) {
	$out_date = '';
	$statement = "SELECT cancelled_date FROM nse_establishment_qa_cancelled WHERE establishment_code=? ORDER BY cancelled_date DESC LIMIT 1";
	$sql_qa = $GLOBALS['dbCon']->prepare($statement);
	$sql_qa->bind_param('s', $establishment_code);
	$sql_qa->execute();
	$sql_qa->bind_result($out_date);
	$sql_qa->fetch();
	$sql_qa->free_result();
	$sql_qa->close();

	if ($out_date == '0000-00-00') $out_date = '';

	return $out_date;
}

function get_contact($establishment_code) {
	$contact_data['name'] = '';
	$contact_data['tel'] = '';
	$contact_data['cell'] = '';
	$contact_data['email'] = '';
	$contact_data['postal'] = '';

	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname), phone, cell, email
					FROM nse_user_establishments AS a
					LEFT JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=?
					ORDER BY a.qa_contact
					LIMIT 1";
	$sql_contact = $GLOBALS['dbCon']->prepare($statement);
	$sql_contact->bind_param('s', $establishment_code);
	$sql_contact->execute();
	$sql_contact->bind_result($contact_name, $tel, $cell, $email);
	$sql_contact->fetch();
	$sql_contact->close();

	//Postal Address
	$statement = "SELECT b.reservation_postal1, b.reservation_postal2, b.reservation_postal3, b.reservation_postal_code,
						a.postal_address_line1, a.postal_address_line2, a.postal_address_line3,	a.postal_address_code
					FROM nse_establishment AS a
					LEFT JOIN nse_establishment_reservation AS b ON a.establishment_code=b.establishment_code
					WHERE a.establishment_code=?
					LIMIT 1";
	$sql_postal = $GLOBALS['dbCon']->prepare($statement);
	$sql_postal->bind_param('s', $establishment_code);
	$sql_postal->execute();
	$sql_postal->bind_result($reservation_postal1, $reservation_postal2, $reservation_postal3, $reservation_postal_code,
						$postal_address_line1, $postal_address_line2, $postal_address_line3, $postal_address_code);
	$sql_postal->fetch();
	$sql_postal->close();
	$reservation_postal1 = trim($reservation_postal1);
	$reservation_postal2 = trim($reservation_postal2);
	$reservation_postal3 = trim($reservation_postal3);
	$reservation_postal_code = trim($reservation_postal_code);
	$postal_address_line1 = trim($postal_address_line1);
	$postal_address_line2 = trim($postal_address_line2);
	$postal_address_line3 = trim($postal_address_line3);
	$postal_address_code = trim($postal_address_code);

	//Remove trailing comma
	if (strpos($postal_address_line1, ',') > 0) $postal_address_line1 = substr($postal_address_line1, 0, -1);
	if (strpos($postal_address_line2, ',') > 0) $postal_address_line2 = substr($postal_address_line2, 0, -1);
	if (strpos($postal_address_line3, ',') > 0) $postal_address_line3 = substr($postal_address_line3, 0, -1);
	if (strpos($reservation_postal1, ',') > 0) $reservation_postal1 = substr($reservation_postal1, 0, -1);
	if (strpos($reservation_postal2, ',') > 0) $reservation_postal2 = substr($reservation_postal2, 0, -1);
	if (strpos($reservation_postal3, ',') > 0) $reservation_postal3 = substr($reservation_postal3, 0, -1);

	//Remove trailing dot
	if (strpos($reservation_postal_code, '.') > 0) $reservation_postal_code = substr($reservation_postal_code, 0, -1);
	if (strpos($postal_address_code, '.') > 0) $postal_address_code = substr($postal_address_code, 0, -1);

	//Assemble array
	$contact_data['name'] = $contact_name;
	$contact_data['tel'] = $tel;
	$contact_data['cell'] = $cell;
	$contact_data['email'] = $email;
	if (empty($reservation_postal1)) {
		if (!empty($postal_address_line1)) $contact_data['postal'] = "$postal_address_line1";
		if (!empty($postal_address_line2)) $contact_data['postal'] .= ", $postal_address_line2";
		if (!empty($postal_address_line3)) $contact_data['postal'] .= ", $postal_address_line3";
		if (!empty($postal_address_code)) $contact_data['postal'] .= ", $postal_address_code";

		$contact_data['postal_1'] = $postal_address_line1;
		$contact_data['postal_2'] = $postal_address_line2;
		$contact_data['postal_3'] = $postal_address_line3;
		$contact_data['postal_code'] = $postal_address_code;
	} else {
		if (!empty($reservation_postal1)) $contact_data['postal'] = "$reservation_postal1";
		if (!empty($reservation_postal2)) $contact_data['postal'] .= ", $reservation_postal2";
		if (!empty($reservation_postal3)) $contact_data['postal'] .= ", $reservation_postal3";
		if (!empty($reservation_postal_code)) $contact_data['postal'] .= ", $reservation_postal_code";

		$contact_data['postal_1'] = $reservation_postal1;
		$contact_data['postal_2'] = $reservation_postal2;
		$contact_data['postal_3'] = $reservation_postal3;
		$contact_data['postal_code'] = $reservation_postal_code;

	}

	return $contact_data;
}

function get_stargrading($establishment_code) {
	$star_grading = '';

	//Prepare statement - restype/star grading
	$statement = "SELECT IF(star_grading=0,'',star_grading)
				FROM nse_establishment_restype
				WHERE establishment_code=?
				ORDER BY star_grading
				LIMIT 1";
	$sql_restype = $GLOBALS['dbCon']->prepare($statement);
	$sql_restype->bind_param('s', $establishment_code);
	$sql_restype->execute();
	$sql_restype->bind_result($star_grading);
	$sql_restype->fetch();
	$sql_restype->close();

	return $star_grading;
}

function get_invoice($establishment_code) {
	$latest_invoice = array('inv'=>'', 'pay'=>'', 'type'=>'','pdate'=>'');

	$statement = "SELECT a.j_inv_total, a.j_inv_paid, a.j_inv_type, d.j_invp_date
				FROM nse_invoice AS a
				LEFT JOIN nse_invoice_item AS b ON a.j_inv_id=b.j_invit_invoice
				LEFT JOIN nse_inventory AS c ON b.j_invit_inventory=c.j_in_id
				LEFT JOIN nse_invoice_item_paid AS d ON b.j_invit_id=d.j_invp_invoice_item
				WHERE a.j_inv_to_company=?
					AND (c.j_in_category=5 OR c.j_in_category=6 OR c.j_in_category=13 OR c.j_in_category=15 OR c.j_in_category=16)
					AND (a.j_inv_type = 1 OR j_inv_type=2)
				ORDER BY a.j_inv_date DESC
				LIMIT 1";
	$sql_invoices = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_invoices->bind_param('s', $establishment_code);
	$sql_invoices->execute();
	$sql_invoices->bind_result($invoice_total, $invoice_paid, $invoice_type, $payment_date);
	$sql_invoices->fetch();
	$sql_invoices->close();

	if (!empty($invoice_total)) {
		$latest_invoice['inv'] = number_format($invoice_total,2,'.','');
		$latest_invoice['pay'] = number_format($invoice_paid,2,'.','');
		$latest_invoice['type'] = $invoice_type;
		$latest_invoice['pdate'] = date('Y-m-d', $payment_date);
	}


	return $latest_invoice;
}

function get_location($establishment_code) {
	$statement = "SELECT b.country_name, c.province_name, d.town_name, e.suburb_name, d.town_id, e.suburb_id
				FROM nse_establishment_location AS a
				LEFT JOIN nse_nlocations_countries AS b ON a.country_id=b.country_id
				LEFT JOIN nse_nlocations_provinces AS c ON a.province_id=c.province_id
				LEFT JOIN nse_nlocations_towns AS d ON a.town_id=d.town_id
				LEFT JOIN nse_nlocations_suburbs AS e ON a.suburb_id=e.suburb_id
				WHERE a.establishment_code=:establishment_code
				LIMIT 1";
	$sql_location = $GLOBALS['db_pdo']->prepare($statement);
	$sql_location->bindParam(':establishment_code', $establishment_code);
	$sql_location->execute();
	$sql_location_data = $sql_location->fetch();
	$sql_location->closeCursor();

	$data['country'] = $sql_location_data['country_name'];
	$data['province'] = $sql_location_data['province_name'];
	$data['town'] = $sql_location_data['town_name'];
	$data['suburb'] = $sql_location_data['suburb_name'];

	//Get Regions
	$statement = "SELECT b.region_name
				FROM nse_nlocations_regions_content AS a
				LEFT JOIN nse_nlocations_regions AS b ON a.region_id=b.region_id
				WHERE (a.location_id=:town_id AND location_type='town') OR (a.location_id=:suburb_id AND location_type='suburb')";
	$sql_regions = $GLOBALS['db_pdo']->prepare($statement);
	$sql_regions->bindParam(':town_id', $sql_location_data['town_id']);
	$sql_regions->bindParam(':suburb_id', $sql_location_data['suburb_id']);
	$sql_regions->execute();
	$sql_regions_data = $sql_regions->fetchAll();
	$sql_regions->closeCursor();

	$data['regions'] = array();
	foreach ($sql_regions_data as $regions_data) {
		$data['regions'][] = $regions_data['region_name'];
	}

	return $data;
}

function get_tel($establishment_code) {
	$statement = "SELECT contact_type, contact_value
				FROM nse_establishment_public_contact
				WHERE establishment_code = :establishment_code";
	$sql_contact = $GLOBALS['db_pdo']->prepare($statement);
	$sql_contact->bindParam(':establishment_code', $establishment_code);
	$sql_contact->execute();
	$sql_contact_data = $sql_contact->fetchAll();
	$sql_contact->closeCursor();

	$data['tel1'] = '';
	$data['tel2'] = '';

	foreach ($sql_contact_data as $contact_data) {
		if ($contact_data['contact_type'] == 'tel1') {
			$data['tel1'] = $contact_data['contact_value'];
		}
		if ($contact_data['contact_type'] == 'tel2') {
			$data['tel2'] = $contact_data['contact_value'];
		}
	}

	return $data;
}

function get_qa_status($establishment_code) {
	$statement = "SELECT aa_category_name
				FROM nse_establishment AS a
				LEFT JOIN nse_aa_category AS b ON a.aa_category_code=b.aa_category_code
				WHERE establishment_code = :establishment_code
				LIMIT 1";
	$sql_status = $GLOBALS['db_pdo']->prepare($statement);
	$sql_status->bindParam(':establishment_code', $establishment_code);
	$sql_status->execute();
	$sql_status_data = $sql_status->fetch();
	$sql_status->closeCursor();

	return $sql_status_data['aa_category_name'];
}

function get_restype($establishment_code) {
	$statement = "SELECT b.subcategory_name
				FROM nse_establishment_restype AS a
				LEFT JOIN nse_restype_subcategory_lang AS b ON a.subcategory_id=b.subcategory_id
				WHERE a.establishment_code=?
				LIMIT 1";
	$sql_restype = $GLOBALS['dbCon']->prepare($statement);
	$sql_restype->bind_param('s', $establishment_code);
	$sql_restype->execute();
	$sql_restype->bind_result($restype);
	$sql_restype->fetch();
	$sql_restype->close();

	return $restype;
}

function get_roomcount($establishment_code) {
	$data = array();

	//Room Count
	$statement = "SELECT room_count, room_type
				FROM nse_establishment_data
				WHERE establishment_code=:establishment_code
				LIMIT 1";
	$sql_rooms = $GLOBALS['db_pdo']->prepare($statement);
	$sql_rooms->bindParam(':establishment_code', $establishment_code);
	$sql_rooms->execute();
	$sql_rooms_data = $sql_rooms->fetch();
	$sql_rooms->closeCursor();
	$data['room_count'] = $sql_rooms_data['room_count'];
	$data['room_type'] = $sql_rooms_data['room_type'];

	//Room type
	$statement = "SELECT price_description
				FROM nse_establishment_pricing
				WHERE establishment_code = :establishment_code
				LIMIT 1";
	$sql_type = $GLOBALS['db_pdo']->prepare($statement);
	$sql_type->bindParam(':establishment_code', $establishment_code);
	$sql_type->execute();
	$sql_type_data = $sql_type->fetch();
	$sql_type->closeCursor();

	$data['description'] = $sql_type_data['price_description'];

	return $data;
}

function get_assessor($establishment_code) {
	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS assessor
				FROM nse_establishment AS a
				LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
				WHERE a.establishment_code=:establishment_code
				LIMIT 1";
	$sql_assessor = $GLOBALS['db_pdo']->prepare($statement);
	$sql_assessor->bindParam(':establishment_code', $establishment_code);
	$sql_assessor->execute();
	$sql_assessor_data = $sql_assessor->fetch();
	$sql_assessor->closeCursor();

	return $sql_assessor_data['assessor'];
}


?>
