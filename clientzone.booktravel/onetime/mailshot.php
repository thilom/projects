<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$theme_id = 8;
$counter = 0;
$file = 'mailshot.csv';
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/utility/PHPMailer/class.phpmailer.php";
$mail = new PHPMailer();
$file_data = '';
$run = 1;

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

if ($run == 0) {
	$data_file = file_get_contents($file);
	$sent_data = explode(',', $data_file);

	$statement = "SELECT a.establishment_code
					FROM nse_establishment_themes AS a
					LEFT JOIN nse_establishment_restype AS c ON a.establishment_code=c.establishment_code
					WHERE theme_id=:theme_id OR subcategory_id=5 OR subcategory_id=32 OR subcategory_id=33 OR subcategory_id=34";
	$sql_theme = $GLOBALS['db_pdo']->prepare($statement);
	$sql_theme->bindParam(':theme_id', $theme_id);
	$sql_theme->execute();
	$sql_theme_data = $sql_theme->fetchAll();
	$sql_theme->execute();
	$done = array();
	$done2 = array();
	//$sql_theme_data = array(0=>array('establishment_code'=>'012414'));

	file_put_contents('mailshot_list.csv', '');
	$fp = fopen('mailshot_list.csv', 'a');

	$statement = "SELECT establishment_name
					FROM nse_establishment
					WHERE establishment_code=:establishment_code
					LIMIT 1";
	$sql_estab = $GLOBALS['db_pdo']->prepare($statement);

	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS contact_name, email, a.user_id
					FROM nse_user_establishments AS a
					LEFT JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=:establishment_code ";
	$sql_contacts = $GLOBALS['db_pdo']->prepare($statement);

	$statement = "SELECT contact_type, contact_value
					FROM nse_establishment_public_contact
					WHERE establishment_code = :establishment_code AND contact_type = 'email'
					LIMIT 1";
	$sql_res = $GLOBALS['db_pdo']->prepare($statement);

	foreach ($sql_theme_data as $theme_data) {

			$sql_estab->bindParam(':establishment_code', $theme_data['establishment_code']);
			$sql_estab->execute();
			$sql_estab_data = $sql_estab->fetch();

			$sql_contacts->bindParam(':establishment_code', $theme_data['establishment_code']);
			$sql_contacts->execute();
			$sql_contacts_data = $sql_contacts->fetchAll();

			foreach ($sql_contacts_data as $contact_data) {
				if (!in_array($contact_data['user_id'], $done)) {
	//				$data[$counter]['code'] = $theme_data['establishment_code'];
	//				$data[$counter]['name'] = $sql_estab_data['establishment_name'];
	//				$data[$counter]['contact_name'] = $contact_data['contact_name'];
	//				$data[$counter]['email'] = $contact_data['email'];
	//				$data[$counter]['user_id'] = $contact_data['user_id'];

					$file_data = "{$theme_data['establishment_code']},{$sql_estab_data['establishment_name']},{$contact_data['contact_name']},{$contact_data['email']}\n";
					fwrite($fp, $file_data);
					$done[] = $contact_data['user_id'];
				}
				$counter++;
			}

			//Get res data

			if (!in_array($theme_data['establishment_code'], $done2)) {
				$sql_res->bindParam(':establishment_code', $theme_data['establishment_code']);
				$sql_res->execute();
				$sql_res_data = $sql_res->fetch();
		//		$data[$counter]['code'] = $theme_data['establishment_code'];
		//		$data[$counter]['name'] = $sql_estab_data['establishment_name'];
		//		$data[$counter]['contact_name'] = 'Reservations';
		//		$data[$counter]['email'] = $sql_res_data['contact_value'];
		//		$data[$counter]['user_id'] = '';
				$file_data = "{$theme_data['establishment_code']},{$sql_estab_data['establishment_name']},Reservations,{$sql_res_data['contact_value']}\n";
				fwrite($fp, $file_data);
				$done2[] = $theme_data['establishment_code'];
				$counter++;
			}

	}

	fclose($fp);
	die();

}


if ($run == 1) {

	//Get data from file
	$fp = fopen('mailshot_nlist.csv', 'r');

	//Get sent data
	$data_file = file_get_contents($file);
	$sent_data = explode(',', $data_file);

	$counter = 0;
	while ($data = fgetcsv($fp)) {
		if (in_array($data[0], $sent_data)) continue;
		if ($counter >= 10) break;


		//Email client
		$email_content = file_get_contents('mailshot.html');
		try {
			$contact_name = $data[3]=='Reservations'?'':$data[3];
			$mail->ClearAllRecipients();
			$mail->AddAddress($data[4], $contact_name);
			$mail->SetFrom('vanessa@aa-travel.com', 'Vanessa Sand');
			$mail->Subject = "Please tell us about your establishment";
			$mail->MsgHTML($email_content);
//			if ($data[4] == 'meloth14@gmail.com' || $data[4] == '1vanessa@aatravel.co.za') {
				$mail->Send();
				$sent_data[] = $data[0];
				echo "{$data[0]}~{$data[1]}~{$data[2]}<br>";
				echo "<span style='font-size: 0.8em; padding-left: 20px'> ->{$contact_name}~{$data[4]}</span><br>";
				$counter++;
//			}
		} catch (phpmailerException $e) {
			echo __LINE__ . ' :: ' . $e->errorMessage(); //Pretty error messages from PHPMailer
			$sent_data[] = $data[0];
		} catch (Exception $e) {
			echo __LINE__ . ' ::: ' . $e->getMessage(); //Boring error messages from anything else!
			$sent_data[] = $data[0];
		}
	}

	$file_sent = implode(',', $sent_data);
	file_put_contents($file, $file_sent);

	if ($counter > 0) {
		echo '<script type="text/javascript">
		  var timeout = setTimeout("location.reload(true);",10000);
		  function resetTimeout() {
		  alert("Timer Stopped");
			clearTimeout(timeout);
		  }
		</script>';
	}

}


//echo $counter;
?>
