<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$user_id = '9496';
$estabs = array('010788','011065','021529','021530','021531','022115','023513','023520','029613','033514',
				'040790','057629','057630','072116','079614','118068','119612','121532','123517','130787',
				'131107','133518','133519','133521','139538','141108','151533','153523','161534','163524',
				'181060','183525','190789','191535','191536','191537','193526','193527','201106','203528',
				'221142','237383','AG9755','KN8357','MA7611','NA8354','TA7109','TS7629');

$statement = "SELECT count(*) as count
				FROM nse_user_establishments
				WHERE establishment_code = :establishment_code AND user_id=:user_id";
$sql_check = $GLOBALS['db_pdo']->prepare($statement);


$statement = "INSERT INTO nse_user_establishments
				(establishment_code, user_id)
				VALUES
				(:establishment_code, :user_id)";
$sql_insert = $GLOBALS['db_pdo']->prepare($statement);

//Check and insert
foreach ($estabs as $establishment_code) {
	$sql_check->bindParam(':establishment_code', $establishment_code);
	$sql_check->bindParam(':user_id', $user_id);
	$sql_check->execute();
	$sql_check_data = $sql_check->fetch();

	if ($sql_check_data['count'] == 0) {
		$sql_insert->bindParam(':establishment_code', $establishment_code);
		$sql_insert->bindParam(':user_id', $user_id);
		$sql_insert->execute();
	}

}
?>
