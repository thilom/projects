<?php

/**
 * Add assessor name to assessments where the assessor name is missing
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$assessments = array();

//Get list of assessments without names
$statement = "SELECT assessment_id, establishment_code
			FROM nse_establishment_assessment
			WHERE assessor_name = '' OR assessor_name IS NULL";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);
$sql_assessments->execute();
$sql_assessments->bind_result($assessment_id, $establishment_code);
while ($sql_assessments->fetch()) {
	$assessments[$assessment_id] = $establishment_code;
}
$sql_assessments->close();

//Prepare statement - Get assessor_name
$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname)
			FROM nse_establishment AS a
			LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
			WHERE a.establishment_code=?
			LIMIT 1";
$sql_name = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Update assessment
$statement = "UPDATE nse_establishment_assessment
			SET assessor_name=?
			WHERE assessment_id=?
			LIMIT 1";
$sql_update = $GLOBALS['dbCon']->prepare($statement);

//Update empty assessors
foreach ($assessments as $assessment_id=>$establishment_code) {
	$assessor_name = '';
	$sql_name->bind_param('s', $establishment_code);
	$sql_name->execute();
	$sql_name->bind_result($assessor_name);
	$sql_name->fetch();
	$sql_name->free_result();

	if (empty($assessor_name)) continue;

	$sql_update->bind_param('ss', $assessor_name, $assessment_id);
	$sql_update->execute();
	$sql_update->free_result();

	echo "$assessment_id, $establishment_code, $assessor_name<br>";
}
?>
