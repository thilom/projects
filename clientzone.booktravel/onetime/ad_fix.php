<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$file = "ad_fix.csv";
$past_sabest_id = 120; //TODO: Get correct id from live site
$new_sabest_id = 223;
$invoice_numbers = array();
$date = strtotime('now');
$not_done = '';

//Read CSV
//if (($handle = fopen($file, "r")) !== FALSE) {
//    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//		if ($data[2] != '' && $data[2] != 'NUMBER') {
//			$invoice_numbers[] = str_pad($data[2], 8, 0, STR_PAD_LEFT);
//		}
//    }
//    fclose($handle);
//}
$invoice_numbers[] = '00006453';
$invoice_numbers[] = '00006455';
$invoice_numbers[] = '00006456';
$invoice_numbers[] = '00006457';
$invoice_numbers[] = '00006458';
$invoice_numbers[] = '00006459';
$invoice_numbers[] = '00006462';
$invoice_numbers[] = '00006461';

foreach ($invoice_numbers as $inv_number) {
	//Get establishment cade
	$statement = "SELECT j_inv_to_company, j_inv_id, j_inv_order
					FROM nse_invoice
					WHERE j_inv_number = :j_inv_number
					LIMIT 1";
	$sql_est = $GLOBALS['db_pdo']->prepare($statement);
	$sql_est->bindParam(':j_inv_number', $inv_number);
	$sql_est->execute();
	$sql_est_data = $sql_est->fetch();
	$sql_est->closeCursor();

	switch ($sql_est_data['j_inv_to_company']) {
		case 'A01806':
			$sql_est_data['j_inv_to_company'] = '083251';
			break;
		case 'A01709':
			$sql_est_data['j_inv_to_company'] = '203288';
			break;
	}

	//Look for previous entry
	$statement = "SELECT country_id, province_id, section_id, region_id, region, town_id,
							town, suburb_id, suburb, rates, establishment_name, image_src,
							image_dimensions, image_position, qa_logo, qa_text, description,
							star_grading, address, icons, side, rooms, rates_sgl
					FROM nse_ads
					WHERE establishment_code=:establishment_code
						AND inventory_id=:inventory_id";
	$sql_check = $GLOBALS['db_pdo']->prepare($statement);
	$sql_check->bindParam(':establishment_code', $sql_est_data[0]);
	$sql_check->bindParam(':inventory_id', $past_sabest_id);
	$sql_check->execute();
	$sql_check_data = $sql_check->fetch();
	$sql_check->closeCursor();

	//Get generec data
	if ($sql_check_data === false) {
		$statement = "SELECT a.establishment_name, b.province_id, b.town_id, c.town_name, b.suburb_id, d.suburb_name
						FROM nse_establishment AS a
						LEFT JOIN nse_establishment_location AS b ON a.establishment_code=b.establishment_code
						LEFT JOIN nse_nlocations_towns AS c ON b.town_id=c.town_id
						LEFT JOIN nse_nlocations_suburbs AS d ON b.suburb_id=d.suburb_id
						WHERE a.establishment_code=:establishment_code";
		$sql_estab = $GLOBALS['db_pdo']->prepare($statement);
		$sql_estab->bindParam(':establishment_code', $sql_est_data['j_inv_to_company']);
		$sql_estab->execute();
		$sql_estab_data = $sql_estab->fetch();
		$sql_estab->closeCursor();

		//Description
		$statement = "SELECT establishment_description
						FROM nse_establishment_descriptions
						WHERE establishment_code=:establishment_code AND description_type='short_description'
						LIMIT 1";
		$sql_desc = $GLOBALS['db_pdo']->prepare($statement);
		$sql_desc->bindParam(':establishment_code', $sql_est_data['j_inv_to_company']);
		$sql_desc->execute();
		$sql_desc_data = $sql_desc->fetch();
		$sql_desc->closeCursor();

		$statement = "INSERT INTO nse_ads
						 (`invoice_id`,
						 `date`,
						 `publication_year`,
						 `inventory_id`,
						 `country_id`,
						 `province_id`,
						 `section_id`,
						 `town_id`,
						 `town`,
						 `suburb_id`,
						 `suburb`,
						 `establishment_code`,
						 `establishment_name`,
						 `description`,
						 image_dimensions,
						 image_position)
					VALUES (:invoice_id,
							:date,
							'2012',
							'223',
							'1',
							:province_id,
							'0',
							:town_id,
							:town,
							:suburb_id,
							:suburb,
							:establishment_code,
							:establishment_name,
							:description,
							'47.000~31.000|',
							'-3.000~0.000|');";
		$sql_insert = $GLOBALS['db_pdo']->prepare($statement);
		$sql_insert->bindParam(':invoice_id', $sql_est_data['j_inv_id']); //
		$sql_insert->bindParam(':date', $date); //
		$sql_insert->bindParam(':province_id', $sql_estab_data['province_id']); //
//		$sql_insert->bindParam(':section_id', $sql_check_data['section_id']);
//		$sql_insert->bindParam(':region_id', $sql_check_data['region_id']);
//		$sql_insert->bindParam(':region', $sql_check_data['region']);
		$sql_insert->bindParam(':town_id', $sql_estab_data['town_id']); //
		$sql_insert->bindParam(':town', $sql_estab_data['town_name']); //
		$sql_insert->bindParam(':suburb_id', $sql_estab_data['suburb_id']); //
		$sql_insert->bindParam(':suburb', $sql_estab_data['suburb_name']); //
//		$sql_insert->bindParam(':rates', $sql_check_data['rates']);
		$sql_insert->bindParam(':establishment_code', $sql_est_data['j_inv_to_company']); //
		$sql_insert->bindParam(':establishment_name', $sql_estab_data['establishment_name']); //
//		$sql_insert->bindParam(':image_src', $sql_check_data['image_src']);
//		$sql_insert->bindParam(':image_dimensions', '47.000~31.000|');
//		$sql_insert->bindParam(':image_position', $sql_check_data['image_position']);
//		$sql_insert->bindParam(':qa_logo', $sql_check_data['qa_logo']);
//		$sql_insert->bindParam(':qa_text', $sql_check_data['qa_text']);
		$sql_insert->bindParam(':description', $sql_desc_data['establishment_description']); //
//		$sql_insert->bindParam(':star_grading', $sql_check_data['star_grading']);
//		$sql_insert->bindParam(':address', $sql_check_data['address']);
//		$sql_insert->bindParam(':award', $sql_check_data['award']);
//		$sql_insert->bindParam(':rooms', $sql_check_data['rooms']);
//		$sql_insert->bindParam(':qa_status', $sql_check_data['qa_status']);
//		$sql_insert->bindParam(':rates_sgl', $sql_check_data['rates_sgl']);
		$sql_insert->execute();


		$not_done .= "{$sql_est_data['j_inv_to_company']} - {$sql_estab_data['establishment_name']}<br>";
	} else {
		$country_id = $sql_check_data['country_id'];
		$province_id = $sql_check_data['province_id'];
		$section_id = $sql_check_data['section_id'];
		$region_id = $sql_check_data['region_id'];
		$region = $sql_check_data['region'];
		$town_id = $sql_check_data['town_id'];
		$town = $sql_check_data['town'];
		$suburb_id = $sql_check_data['suburb_id'];
		$suburb = $sql_check_data['suburb'];
		$rates = $sql_check_data['rates'];
		$establishment_name = $sql_check_data['establishment_name'];
		$image_src = $sql_check_data['image_src'];
		$image_dimensions = $sql_check_data['image_dimensions'];
		$image_position = $sql_check_data['image_position'];
		$qa_logo = $sql_check_data['qa_logo'];
		$qa_text = $sql_check_data['qa_text'];
		$description = $sql_check_data['description'];
		$star_grading = $sql_check_data['star_grading'];
		$address = $sql_check_data['address'];
		$icons = $sql_check_data['icons'];
		$side = $sql_check_data['side'];
		$rooms = $sql_check_data['rooms'];
		$rates_sgl = $sql_check_data['rates_sgl'];

//		var_dump($sql_est_data);
//		var_dump($inv_number);

		$statement = "INSERT INTO nse_ads
						 (`invoice_id`,
						 `date`,
						 `publication_year`,
						 `inventory_id`,
						 `country_id`,
						 `province_id`,
						 `section_id`,
						 `region_id`,
						 `region`,
						 `town_id`,
						 `town`,
						 `suburb_id`,
						 `suburb`,
						 `rates`,
						 `establishment_code`,
						 `establishment_name`,
						 `image_src`,
						 `image_dimensions`,
						 `image_position`,
						 `qa_logo`,
						 `qa_text`,
						 `description`,
						 `star_grading`,
						 `address`,
						 `award`,
						 `icons`,
						 `side`,
						 `color`,
						 `rooms`,
						 `qa_status`,
						 `rates_sgl`)
					VALUES (:invoice_id,
							:date,
							'2012',
							'223',
							'1',
							:province_id,
							:section_id,
							:region_id,
							:region,
							:town_id,
							:town,
							:suburb_id,
							:suburb,
							:rates,
							:establishment_code,
							:establishment_name,
							:image_src,
							:image_dimensions,
							:image_position,
							:qa_logo,
							:qa_text,
							:description,
							:star_grading,
							:address,
							:award,
							:icons,
							:side,
							:color,
							:rooms,
							:qa_status,
							:rates_sgl);";
		$sql_insert = $GLOBALS['db_pdo']->prepare($statement);
		$sql_insert->bindParam(':invoice_id', $sql_est_data['j_inv_id']);
		$sql_insert->bindParam(':date', $date);
		$sql_insert->bindParam(':province_id', $sql_check_data['province_id']);
		$sql_insert->bindParam(':section_id', $sql_check_data['section_id']);
		$sql_insert->bindParam(':region_id', $sql_check_data['region_id']);
		$sql_insert->bindParam(':region', $sql_check_data['region']);
		$sql_insert->bindParam(':town_id', $sql_check_data['town_id']);
		$sql_insert->bindParam(':town', $sql_check_data['town']);
		$sql_insert->bindParam(':suburb_id', $sql_check_data['suburb_id']);
		$sql_insert->bindParam(':suburb', $sql_check_data['suburb']);
		$sql_insert->bindParam(':rates', $sql_check_data['rates']);
		$sql_insert->bindParam(':establishment_code', $sql_est_data['j_inv_to_company']);
		$sql_insert->bindParam(':establishment_name', $sql_check_data['establishment_name']);
		$sql_insert->bindParam(':image_src', $sql_check_data['image_src']);
		$sql_insert->bindParam(':image_dimensions', $sql_check_data['image_dimensions']);
		$sql_insert->bindParam(':image_position', $sql_check_data['image_position']);
		$sql_insert->bindParam(':qa_logo', $sql_check_data['qa_logo']);
		$sql_insert->bindParam(':qa_text', $sql_check_data['qa_text']);
		$sql_insert->bindParam(':description', $sql_check_data['description']);
		$sql_insert->bindParam(':star_grading', $sql_check_data['star_grading']);
		$sql_insert->bindParam(':address', $sql_check_data['address']);
		$sql_insert->bindParam(':award', $sql_check_data['award']);
		$sql_insert->bindParam(':address', $sql_check_data['address']);
		$sql_insert->bindParam(':icons', $sql_check_data['icons']);
		$sql_insert->bindParam(':side', $sql_check_data['side']);
		$sql_insert->bindParam(':color', $sql_check_data['color']);
		$sql_insert->bindParam(':rooms', $sql_check_data['rooms']);
		$sql_insert->bindParam(':qa_status', $sql_check_data['qa_status']);
		$sql_insert->bindParam(':rates_sgl', $sql_check_data['rates_sgl']);
		$sql_insert->execute();

		echo "{$sql_est_data['j_inv_to_company']} - {$sql_check_data['establishment_name']}<br>";
	}



}

echo "<p>NOT DONE</p>$not_done";

//Check for past entry
$statement = "SELECT ";

?>
