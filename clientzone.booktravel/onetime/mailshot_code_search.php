<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$read_file = 'mailshot_read.csv';
$write_file = 'mailshot_write.csv';

//Open file
$rf = fopen($read_file, 'r');
$wf = fopen($write_file, 'a');

//Prepare statement - find name
$statement = "SELECT establishment_code
				FROM nse_establishment
				WHERE establishment_name=:establishment_name
				LIMIT 1";
$sql_estab = $GLOBALS['db_pdo']->prepare($statement);


//Find estabs
$counter = 0;
while ($data = fgetcsv($rf)) {

	$sql_estab->bindParam(':establishment_name', $data[1]);
	$sql_estab->execute();
	$sql_estab_data = $sql_estab->fetch();

	if ($sql_estab_data === false) {
		echo "{$data[1]}<br>";
		$data[] = '';
		$counter++;

	} else {
		$data[] = $sql_estab_data['establishment_code'];
	}

	fputcsv($wf, $data, ',', '"');
}

echo "NOT FOUND: $counter";

?>
