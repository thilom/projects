<?php

/**
 * Get the 2012 Accommodation directory advertisers with postal addresses
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$estabs = array('Legacy Hotel Management Services',
	'Zebra Country Lodge  Conference and Wedding Venue  Leeuwkloof',
	"Jemima's Hospitality  Bunker On Bailie Guest Lodge  East London",
	'Afro-Uluwatu (Ballito  Zimbali)  Ballito');
$estabs2 = array('Legacy Hotel Management Services'=>'A01168',
				'Zebra Country Lodge  Conference and Wedding Venue  Leeuwkloof'=>'262568',
				"Jemima's Hospitality  Bunker On Bailie Guest Lodge  East London"=>'JE7494',
	'Afro-Uluwatu (Ballito  Zimbali)  Ballito'=>'A01266');

//Prepare statement - get postal address
$statement = "SELECT postal_address_line1, establishment_code, postal_address_line2, postal_address_line3, postal_address_code,
				street_address_line1, street_address_line2, street_address_line3
			FROM nse_establishment
			WHERE establishment_code=:establishment_code OR establishment_name=:establishment_name
			LIMIT 1";
$sql_postal = $GLOBALS['db_pdo']->prepare($statement);

//Read CSV
$csv_input = fopen('2012_accomm_dir.csv', 'r');

//New CSV
$csv_output = fopen('2012_accomm_dir_new.csv', 'w');

$row = 0;
while (($data = fgetcsv($csv_input, 1000, ",")) !== FALSE) {
	$establishment_name = '';
	$sql_postal_data = array();

	if ($row == 0) {
		var_dump($data);

		$header = array();
		$header[] = 'Code';
		$header[] = 'Name';
		$header[] = 'Postal Address(1)';
		$header[] = 'Postal Address(2)';
		$header[] = 'Postal Address(3)';
		$header[] = 'Postal Address(4)';
		$header[] = 'Street Address(1)';
		$header[] = 'Street Address(2)';
		$header[] = 'Street Address(3)';
		$header[] = 'Invoice Date';
		$header[] = 'Invoice Number';
		$header[] = 'Invoice Amount';
		$header[] = 'Payment';

		fputcsv($csv_output, $header);

		$row++;
		continue;
	}

	$strpos =  strrpos($data[5], '  ');
	$establishment_name = substr($data[5], 0, $strpos);

	$establishment_code2 = in_array($data[5], $estabs)?$estabs2[$data[5]]:'rrrcrcg';

	$sql_postal->bindParam(':establishment_name', $establishment_name);
	$sql_postal->bindParam(':establishment_code', $establishment_code2);
	$sql_postal->execute();
	$sql_postal_data = $sql_postal->fetch();
//	var_dump($sql_postal_data);

	$new_data = array();
	$new_data[] = $sql_postal_data['establishment_code'];
	$new_data[] = $establishment_name;
	$new_data[] = $sql_postal_data['postal_address_line1'];
	$new_data[] = $sql_postal_data['postal_address_line2'];
	$new_data[] = $sql_postal_data['postal_address_line3'];
	$new_data[] = $sql_postal_data['postal_address_code'];
	$new_data[] = $sql_postal_data['street_address_line1'];
	$new_data[] = $sql_postal_data['street_address_line2'];
	$new_data[] = $sql_postal_data['street_address_line3'];
	$new_data[] = $data[1];
	$new_data[] = str_pad($data[2], 6, '0') ;
	$new_data[] = number_format((float)$data[11],2, '.', '');
	$new_data[] = number_format((float) str_replace(' ', '', $data[12]),2, '.', '');

	fputcsv($csv_output, $new_data);

	if (in_array($data[5], $estabs)) {
		echo "<span style='color: red'>{$data[5]} ($establishment_code2 | {$sql_postal_data['establishment_code']}) </span><br>";
	} else {
		echo "{$data[5]} ($establishment_code2 | {$sql_postal_data['establishment_code']}) <br>";
	}


	$row++;
}

echo "<a href='2012_accomm_dir_new.csv'>Get File Here</a>";

fclose($csv_input);
fclose($csv_output);
?>
