<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Var
$type = $_GET['type'];
$id = $_GET['id'];

echo " ~ |";
switch ($type) {
	case 'province':
		$statement = "SELECT province_id, province_name
						FROM nse_nlocations_provinces
						WHERE country_id=:country_id
						ORDER BY province_name";
		$sql_provinces = $GLOBALS['db_pdo']->prepare($statement);
		$sql_provinces->bindParam(':country_id', $id);
		$sql_provinces->execute();
		$sql_provinces_data = $sql_provinces->fetchAll();
		$sql_provinces->closeCursor();
		foreach ($sql_provinces_data as $province_data) {
			echo "{$province_data['province_id']}~{$province_data['province_name']}|";
		}
		break;
	case 'town':
		$statement = "SELECT town_id, town_name
						FROM nse_nlocations_towns
						WHERE province_id=:province_id
						ORDER BY town_name";
		$sql_towns = $GLOBALS['db_pdo']->prepare($statement);
		$sql_towns->bindParam(":province_id", $id);
		$sql_towns->execute();
		$sql_towns_data = $sql_towns->fetchAll();
		$sql_towns->closeCursor();
		foreach ($sql_towns_data as $town_data) {
			echo "{$town_data['town_id']}~{$town_data['town_name']}|";
		}
		break;
	case 'suburb':
		$statement = "SELECT suburb_id, suburb_name
						FROM nse_nlocations_suburbs
						WHERE town_id=:town_id
						ORDER BY suburb_name";
		$sql_suburbs = $GLOBALS['db_pdo']->prepare($statement);
		$sql_suburbs->bindParam(":town_id", $id);
		$sql_suburbs->execute();
		$sql_suburbs_data = $sql_suburbs->fetchAll();
		$sql_suburbs->closeCursor();
		foreach ($sql_suburbs_data as $suburb_data) {
			echo "{$suburb_data['suburb_id']}~{$suburb_data['suburb_name']}|";
		}
		break;
}
?>
