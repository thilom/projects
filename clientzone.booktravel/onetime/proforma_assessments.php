<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Invoice date
$start_date = mktime(0, 0, 0, 2, 15, 2012);
$end_date = mktime(23, 59, 59, 2, 15, 2012);



//Get invoices
$statement = "SELECT a.j_inv_number, a.j_inv_to_company, b.establishment_name, c.j_invit_name, a.j_inv_date, a.j_inv_id
			FROM nse_invoice AS a
			LEFT JOIN nse_invoice_item AS c ON a.j_inv_id=c.j_invit_invoice
			LEFT JOIN nse_establishment AS b ON a.j_inv_to_company=b.establishment_code
			WHERE (a.j_inv_date > $start_date AND a.j_inv_date < $end_date)
			ORDER BY a.j_inv_number";
$sql_inv = $GLOBALS['dbCon']->prepare($statement);
$sql_inv->execute();
$sql_inv->bind_result($j_inv_number, $j_inv_to_company, $establishment_name, $j_invit_name, $j_inv_date, $j_inv_id);
while ($sql_inv->fetch()) {
	$establishments[$j_inv_to_company]['name'] = str_replace(',','',$establishment_name);
	$establishments[$j_inv_to_company]['number'] = $j_inv_number;
	$establishments[$j_inv_to_company]['date'] = date('d-m-Y', $j_inv_date);
	$establishments[$j_inv_to_company]['item_name'] = str_replace(',','',$j_invit_name);
	$establishments[$j_inv_to_company]['inv_id'] = $j_inv_id;
//	echo "$j_inv_number - $j_inv_to_company - $establishment_name<br>";
}
$sql_inv->close();

//Prepare statement - find assessment
$statement = "SELECT assessment_id, assessment_status
			FROM nse_establishment_assessment
			WHERE establishment_code=? AND (assessment_status='auto' OR assessment_status='draft')
			ORDER BY assessment_id DESC
			LIMIT 1";
$sql_assessments = $GLOBALS['dbCon']->prepare($statement);

foreach ($establishments as $key=>$data) {
	$assessment_status = '';
	$assessment_id = '';
	$sql_assessments->bind_param('s', $key);
	$sql_assessments->execute();
	$sql_assessments->bind_result($assessment_id, $assessment_status);
	$sql_assessments->fetch();
	$sql_assessments->free_result();
	$establishments[$key]['status'] = $assessment_status;
	$establishments[$key]['id'] = $assessment_id;
}

$fh = fopen('proforma_assessments.csv','w');
fwrite($fh,"Date,Est. Code,Est. Name,Inv. Number,Inv. ID,Assessment Status,Assessment ID\n");

foreach ($establishments as $key=>$data) {

	fwrite($fh,"{$data['date']},$key,{$data['name']},{$data['number']},{$data['inv_id']},{$data['status']},{$data['id']},{$data['item_name']}\n");
}

fclose($fh);

//Prepare statement - insert assessment
$statement = "INSERT INTO nse_establishment_assessment
				(establishment_code, assessment_status, assessment_type_2,active,invoice_id)
			VALUES
				(?,'auto','fa','Y',?)";
$sql_insert = $GLOBALS['dbCon']->prepare($statement);



foreach ($establishments as $key=>$data) {
	if (empty($data['id'])) {
//		$sql_insert->bind_param('ss', $key, $data['inv_id']);
//		$sql_insert->execute();
//		$sql_insert->free_result();
		
		echo "{$data['date']},$key,{$data['name']},{$data['number']},{$data['inv_id']},{$data['status']},{$data['id']},{$data['item_name']}<br>";
	}
}

echo "<a href='proforma_assessments.csv'>Download Here!</a>";
?>
