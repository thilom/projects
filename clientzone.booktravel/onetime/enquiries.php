<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";

//Vars
$fields = array('establishment_code','establishment_name','arrive_date','depart_date','adults','children','rooms','comment','travel_method','enquiry_date',
			'title_id','firstname','surname','email','country','town','phone','cell','fax','contact_method','contact_secondary',
			'age_group','occupation','gender','member_number','receive_updates');


//Get list of enquiries
$statement = "SELECT DATE_FORMAT(a.enquiry_date, '%Y'), a.establishment_code, c.establishment_name, a.arrive_date, a.depart_date, a.adults, a.children, a.rooms, a.comment, a.travel_method, a.enquiry_date,
					d.title_name,firstname,surname,email,country,town,phone,cell,fax,contact_method,contact_secondary,age_group,occupation,gender,member_number,receive_updates
			FROM nse_establishment_enquiry AS a
			LEFT JOIN nse_visitor AS b ON a.visitor_id=b.visitor_id
			LEFT JOIN nse_establishment AS c ON a.establishment_code=c.establishment_code
			LEFT JOIN nse_title AS d ON b.title_id=d.title_id";
if (isset($_GET['year'])) {
	$statement .= " WHERE DATE_FORMAT(a.enquiry_date, '%Y')={$_GET['year']} ";
}
$statement .= " ORDER BY a.enquiry_date DESC";
$sql_enquiries = $GLOBALS['dbCon']->prepare($statement);
$sql_enquiries->execute();
$sql_enquiries->bind_result($file_year, $establishment_code,$establishment_name, $arrive_date,$depart_date,$adults,$children,$rooms,$comment,$travel_method,$enquiry_date,
						$title_id,$firstname,$surname,$email,$country,$town,$phone,$cell,$fax,$contact_method,$contact_secondary,$age_group,
						$occupation,$gender,$member_number,$receive_updates);
while ($sql_enquiries->fetch()) {

	if (!isset($fh[$file_year])) {
		$fh[$file_year] = fopen("enquiries_$file_year.csv", 'w');
		$headers = "establishment_code,establishment_name, arrive_date,depart_date,adults,children,rooms,comment,travel_method,enquiry_date,title,firstname,surname,email,country,town,phone,cell,fax,contact_method,contact_secondary,age_group,occupation,gender,member_number,receive_updates" . PHP_EOL;

		fwrite($fh[$file_year], $headers);
	}

	foreach ($fields as $field) {
		$$field = str_replace(',','',$$field);
		$$field = str_replace(array("\r\n","\r","\n"),' ',$$field);
	}

	$entry = "$establishment_code, $establishment_name, $arrive_date,$depart_date,$adults,$children,$rooms,$comment,$travel_method,$enquiry_date,$title_id,$firstname,$surname,$email,$country,$town,$phone,$cell,$fax,$contact_method,$contact_secondary,$age_group,$occupation,$gender,$member_number,$receive_updates" . PHP_EOL;

	fwrite($fh[$file_year], $entry);

//	echo "$file_year, $establishment_code,$arrive_date,$depart_date,$adults,$children,$rooms,$comment,$travel_method,$enquiry_date<br>";
}
$sql_enquiries->close();

foreach ($fh as $key=>$file) {
	fclose($fh[$key]);
	echo "Download File: <a href='enquiries_$key.csv'>Enquiries $key</a>";
}

?>
