<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once $_SERVER["DOCUMENT_ROOT"]."/juno/set/init.php";
include_once $_SERVER['DOCUMENT_ROOT']."/juno/apps/notes/f/insert.php";


//Vars
$read_file = 'mailshot_date.csv';

//Get data
$file_data = file_get_contents($read_file);
$estab_code = explode(',', str_replace(array("\r\n", "\r", "\n"), ',', $file_data));

var_dump($estab_code);

//Update dat
foreach ($estab_code as $establishment_code) {
	$establishment_code = str_pad($establishment_code, 6, 0, STR_PAD_LEFT);

	$statement = "UPDATE nse_establishment
					SET last_updated=NOW()
					WHERE establishment_code=:establishment_code
					LIMIT 1";
	$sql_date = $GLOBALS['db_pdo']->prepare($statement);
	$sql_date->bindParam(':establishment_code', $establishment_code);
	$sql_date->execute();
	$sql_date->closeCursor();

	//Add a note
	$note_content = 'Updated via mailer to all self-catering establishments';
	$statement = "INSERT INTO nse_notes
						(establishment_code,note_user,note_date,note_app,note_item,note_content,note_reminder,
							note_staff,note_cancellation,note_share,note_action)
					VALUES
						(:establishment_code,0,$EPOCH,0,0,:note_content,0,
							0,0,0,0)";
	$sql_note = $GLOBALS['db_pdo']->prepare($statement);
	$sql_note->bindParam(':establishment_code', $establishment_code);
	$sql_note->bindParam(':note_content', $note_content);
	$sql_note->execute();


}
?>
