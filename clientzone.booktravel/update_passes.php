<?php

//includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/modules/login/tcrypt.class.php';

//Intantiate CLasses
$enc = new tcrypt();

//Prepare Statement
$statement = "UPDATE nse_user SET new_password=? WHERE user_id=?";
$sql_update = $dbCon->prepare($statement);

$statement = "SELECT new_password FROM nse_user WHERE user_id=?";
$sql_fetch = $dbCon->prepare($statement);

//Get Data
$statement = "SELECT user_id, string_id FROM nse_user";
$sql_data = $dbCon->prepare($statement);
$sql_data->execute();
$sql_data->bind_result($user_id, $password);
$sql_data->store_result();
while ($sql_data->fetch()) {
    if (empty($password)) $password = 'def';
    $nPass = $enc->encrypt($password);
    $sql_update->bind_param('ss', $nPass, $user_id);
    echo mysqli_error($dbCon);
    $sql_update->execute();
  
    $sql_fetch->bind_param('s', $user_id);
    $sql_fetch->execute();
    $sql_fetch->bind_result($retPass);
    $sql_fetch->store_result();
    $sql_fetch->fetch();
    
    $dPass = $enc->decrypt($retPass);
    
    echo "$user_id: $password: $dPass<br>";
}
 

?>