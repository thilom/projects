<?php
/**
 * Display Desktop Icons
 *
 * Checks user privileges and displays icons on the desktop accordingly
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category security
 */

//Vars
$dir = SITE_ROOT . '/modules/';
$icons = '';
$top_level = '';
$module_id = '';

//Prepare Statements
$statement = "SELECT access FROM nse_user_access WHERE module_id=? && parent_id=? && function_id=? && user_id=?";
$sql_privilege = $dbCon->prepare($statement);

$statement = "SELECT top_level_type FROM nse_user WHERE user_id=?";
$sql_top = $dbCon->prepare($statement);

//Get top level privilege
$sql_top->bind_param('s', $_SESSION['dbweb_user_id']);
$sql_top->execute();
$sql_top->bind_result($top_level);
$sql_top->store_result();
$sql_top->fetch();
$sql_top->close();

//Get last login status
$statement = "SELECT top_level_type, awebber, assessor, sales_rep FROM nse_user WHERE user_id=?";
$sql_awebber = $GLOBALS['dbCon']->prepare($statement);
$sql_awebber->bind_param('s', $_SESSION['dbweb_user_id']);
$sql_awebber->execute();
$sql_awebber->store_result();
$sql_awebber->bind_result($top_level_type, $awebber, $assessor, $sales_rep);
$sql_awebber->fetch();
$sql_awebber->free_result();
$sql_awebber->close();

if ($assessor == '1') $top_level = 'as';



//Iterate thru directories looking for module.ini.php
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if (is_dir($dir . $file)) {
            	if ($file == '.' || $file == '..') continue;
            		if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
            		if (!empty($module_icon)) {
            			$window_width = isset($window_width)&&!empty($window_width)?$window_width:'0';
            			$window_height = isset($window_height)&&!empty($window_height)?$window_height:'480';
            			$window_xposition = isset($window_xposition)&&!empty($window_xposition)?$window_xposition:'0';
            			$window_yposition =isset($window_yposition)&& !empty($window_yposition)?$window_yposition:'0';
            			$window_attribute = isset($window_attribute)&&!empty($window_attribute)?$window_attribute:'0';
            			$noaccess_icon = isset($noaccess_icon)&&!empty($noaccess_icon)?$noaccess_icon:'';
            			$access_allowed = FALSE;
            			
            			//Create Icon
            			if ($top_level_allowed == 'a' || $top_level == $top_level_allowed) {
            			    if ($top_level_allowed == 'a') {
            			        $access_allowed = TRUE;
            			    } else if ($top_level_allowed == 'c' && $top_level == 'c') {
            			    	 $access_allowed = TRUE;
            			    } else if ($top_level_allowed == 'as' && $top_level == 'as') {
            			    	$access_allowed = TRUE;
            			    } else {
                			    foreach ($sub_levels as $k=>$v) {
                			        if (isset($v['subs'])) {
                			            foreach ($v['subs'] as $sk=>$sv) {
                    			            $access = '0';
                    			            $sql_privilege->bind_param('ssss', $module_id, $k, $sk, $_SESSION['dbweb_user_id']);
                    			            $sql_privilege->execute();
                    			            $sql_privilege->bind_result($access);
                    			            $sql_privilege->fetch();
                    			            if ('1' == $access) {
                    			                $access_allowed = TRUE;
                    			                break 2;
                    			            }
                			            }
                			        } else {
                			            $access = '0';
                			            $parent_id = '';
                			            $sql_privilege->bind_param('ssss', $module_id, $parent_id, $k, $_SESSION['dbweb_user_id']);
                			            $sql_privilege->execute();
                			            $sql_privilege->bind_result($access);
                			            $sql_privilege->fetch();
                			            if ('1' == $access) {
                			                $access_allowed = TRUE;
                			                break;
                			            }
                			        }
                			    }
                			}
            			}
            			if ($access_allowed) {
//            				if ($top_level_type == 'c' && $awebber != 1) {
//            					$icons .= ";P.WN(1,'$module_name','/modules/$file/i/$noaccess_icon','/modules/$file/no_access.php',300,600, 0, 0, $window_attribute)";
//            				} else {
            					$icons .= ";P.WN(1,'$module_name','/modules/$file/i/$module_icon','/modules/$file/$module_link',$window_height,$window_width, $window_xposition, $window_yposition, $window_attribute)";
//            				}
            				
            			}
            		}
            }
        }
        closedir($dh);
    }
}
$icons .= ";P.WN(0,'Logout','/i/log_out.png','/modules/login/logout.php',50,300)";

echo $icons;


?>