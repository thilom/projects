/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Global Vars
var price = new Array();
var code = new Array();
var buttonPadCorrection1 = 0;
var buttonPadCorrection2 = 0;
var buttonPadCorrection3 = 3;
var buttonPadCorrection4 = 1;
var buttonPadCorrection5 = 0;
var cardTemplate = 1;
var cardPos = new Array();
cardPos[1] = 0;
cardPos[2] = 0;
cardPos[3] = 0;
var cardCounter1, cardCounter2, cardCounter3;

/**
 * Initialize
 */
function init() {
	if ($.browser.mozilla) {
		buttonPadCorrection1 = 1;
		buttonPadCorrection2 = 1;
		buttonPadCorrection3 = 3;
		buttonPadCorrection4 = 2;
		buttonPadCorrection5 = 0;
	}

	drawButtonStack('F');
	getProducts();
//	setDefault();

    displayCards(1);
    displayCards(2);
    displayCards(3);
    cardCounter1 = setInterval(function() {displayCards(1)}, 6000);
    setTimeout(function(){startTimer2()}, 2000);
    setTimeout(function(){startTimer3()}, 4000);
}

/**
 * Draws the button stack for card front
 */
function drawButtonStack(cardPos) {
	var content = '';
	var maxLength = cardPos=='F'?35:46;

	for (i = 0; i < 10; i++) {
		content += '<span id="'+cardPos+'block'+ i +'"><input type="text" name="'+cardPos+'line'+ i +'" id="'+cardPos+'line'+ i +'" maxlength="'+maxLength+'" onkeyup="setText('+ i +',\'\', \''+cardPos+'\')">';
		content += "<button type='button' title='Bold Text' class='boldButton buttonOff' style='padding-top:"+ buttonPadCorrection1 +"px !important' id='"+cardPos+"boldButton"+i+"' onclick='setText("+ i +",\"bold\",\""+cardPos+"\")'>B</button>";
		content += "<button type='button' title='Italic Text' class='italicButton buttonOff' style='padding-top:"+ buttonPadCorrection2 +"px !important' id='"+cardPos+"italicButton"+i+"' onclick='setText("+ i +",\"italic\",\""+cardPos+"\")'>I</button>";
		content += "<button type='button' title='Small Text' class='smallTextButton buttonOff' style='padding-top:"+ buttonPadCorrection3 +"px !important' id='"+cardPos+"smallButton"+i+"' onclick='setText("+ i +",\"small\",\""+cardPos+"\")'>A</button>";
		content += "<button type='button' title='Medium Text' class='mediumTextButton buttonOn' style='padding-top:"+ buttonPadCorrection4 +"px !important' id='"+cardPos+"mediumButton"+i+"' onclick='setText("+ i +",\"medium\",\""+cardPos+"\")'>A</button>";
		content += "<button type='button' title='Large Text' class='largeTextButton buttonOff' style='padding-top:"+ buttonPadCorrection5 +"px !important' id='"+cardPos+"largeButton"+i+"' onclick='setText("+ i +",\"large\",\""+cardPos+"\")'>A</button>";
		content += "<input type='hidden' value='00010' name='"+cardPos+"lineStyle"+ i +"' id='"+cardPos+"lineStyle"+ i +"'>";

		content += "<br></span>";
	}

	if (cardPos == 'B') {
		document.getElementById('buttonStack2').innerHTML = content;
	} else {
		content += "<iframe src='/html/upload_logo_form.html' id='uploadFrame'></iframe>";
		document.getElementById('buttonStack').innerHTML = content;
	}

	drawCard(cardPos);
}

function resetButtonStack(cardPos) {
	if (cardTemplate == 1) {
		$('#buttonStack').css('padding-top', '11px');
		for (i = 0; i < 10; i++) {
			$('#'+cardPos+'block'+ i).show();
		}
	} else {
		$('#buttonStack').css('padding-top', '95px');
		for (i = 0; i < 10; i++) {
			if (i > 5) {
				$('#'+cardPos+'block'+ i).hide();
			} else {
				$('#'+cardPos+'block'+ i).show();
			}
		}

	}
}


/**
 * Draws the front card
 */
function drawCard(cardPos) {
	content = '';
	var logo = $('#cardLogoSrc').val()==''?"/templates/images/sample_logo.png":$('#cardLogoSrc').val();

	content += "<table class='busCardTable'>";
	content += "<tr>"
	if (cardPos == 'F')	{
		if (cardTemplate == 1) {

			content += "<td><img src='"+logo+"' id='cardLogo'></td>"
		} else {
			content += "<td></td><div style='text-align: center'><img src='"+logo+"' id='cardLogo' style='max-height: 60px; margin-bottom: 15px; margin-top: 20px;'></div>"
		}
	}
	content += "";


	if (cardPos == 'B') {
			center2 = 'center';
	} else {
		center2 = '';
	}

	if (cardTemplate == 1) {
		content += "<td>";
		for (i = 0; i < 10; i++) {
			content += "<div id='"+cardPos+"cardLine"+ i +"' class='cardLine medium "+ center2 +"'></div>";
		}
		content += "</td></tr>";
	} else {
		for (i = 0; i < 6; i++) {
			content += "<div id='"+cardPos+"cardLine"+ i +"' class='cardLine medium "+ center2 +"' style='text-align: center; margin-left: 0px;'></div>";
		}
		content += "</td></tr>";
	}

	content += "</table>";

	if (cardPos == 'B') {
		document.getElementById('cardBack').innerHTML = content;
	} else {
		document.getElementById('cardFront').innerHTML = content;
	}
}

/**
 * Sets the text on the bus card
 */
function setText(lineID, style, cardPos) {
	var cardLine = document.getElementById(cardPos+'cardLine'+lineID);
	var lineText = document.getElementById(cardPos+'line'+lineID).value;
	var classes = cardLine.className;
	var lineStyle = document.getElementById(cardPos+"lineStyle"+ lineID).value;
	var toggleValue;
	var maxLength;

	if (style == 'small' || style == 'medium' || style == 'large') {
		document.getElementById(cardPos+'smallButton'+lineID).className = document.getElementById(cardPos+'smallButton'+lineID).className.replace('buttonOn', 'buttonOff');
		document.getElementById(cardPos+'mediumButton'+lineID).className = document.getElementById(cardPos+'mediumButton'+lineID).className.replace('buttonOn', 'buttonOff');
		document.getElementById(cardPos+'largeButton'+lineID).className = document.getElementById(cardPos+'largeButton'+lineID).className.replace('buttonOn', 'buttonOff');
		classes = classes.replace('small','');
		classes = classes.replace('medium','');
		classes = classes.replace('large','');
		lineStyle = setCharAt(lineStyle, 2, '0');
		lineStyle = setCharAt(lineStyle, 3, '0');
		lineStyle = setCharAt(lineStyle, 4, '0');
	}

	if (style) {
		if (classes.indexOf(style) > 0) {
		   classes = classes.replace(style,'');
		   document.getElementById(cardPos+style+'Button'+lineID).className = document.getElementById(cardPos+style+'Button'+lineID).className.replace('buttonOn', 'buttonOff');
		   toggleValue = '0';
		} else {
			classes += ' ' + style;
		   document.getElementById(cardPos+style+'Button'+lineID).className = document.getElementById(cardPos+style+'Button'+lineID).className.replace('buttonOff', 'buttonOn');
		   toggleValue = '1';
		}
	}

	//Style Field
	switch (style) {
		case 'bold':
			lineStyle = setCharAt(lineStyle, 0, toggleValue);
			break;
		case 'italic':
			lineStyle = setCharAt(lineStyle, 1, toggleValue);
			break;
		case 'small':
			lineStyle = setCharAt(lineStyle, 2, toggleValue);
			if (cardPos == 'B') {
				maxLength = 58;
			} else {
				maxLength = cardTemplate==1?'35':'54';
			}
			$('#'+cardPos+'line'+lineID).attr('maxLength',maxLength);
			break;
		case 'medium':
			lineStyle = setCharAt(lineStyle, 3, toggleValue);
			if (cardPos == 'B') {
				maxLength = 48;
			} else {
				maxLength = cardTemplate==1?'29':'46'; //90:42
			}
			$('#'+cardPos+'line'+lineID).attr('maxLength',maxLength);
			break;
		case 'large':
			lineStyle = setCharAt(lineStyle, 4, toggleValue);
			if (cardPos == 'B') {
				maxLength = 42;
			} else {
				maxLength = cardTemplate==1?'23':'40';
			}
			$('#'+cardPos+'line'+lineID).attr('maxLength',maxLength);
			break;
		default:
			break;
	}

	document.getElementById(cardPos+"lineStyle"+ lineID).value = lineStyle;

	cardLine.className = classes;

	lineText = lineText.substr(0, maxLength);
	cardLine.innerHTML = lineText;
}


function resetStyle(lineID, cardPos) {
		var styleCode = document.getElementById(cardPos+"lineStyle"+ lineID).value;

		//Bold
		if (styleCode[0] == 1) setText(lineID, 'bold', cardPos);
		if (styleCode[1] == 1) setText(lineID, 'italic', cardPos);
		if (styleCode[2] == 1) setText(lineID, 'small', cardPos);
		if (styleCode[3] == 1) setText(lineID, 'medium', cardPos);
		if (styleCode[4] == 1) setText(lineID, 'large', cardPos);
}

/**
 * hilight the selected fields on the card
 */
function hilight(lineID) {
	for (i = 0; i < 10; i++) {
		document.getElementById('cardLine'+i).className = document.getElementById('cardLine'+i).className.replace(' hilight', '');
	}

	document.getElementById('cardLine'+lineID).className = document.getElementById('cardLine'+lineID).className + ' hilight';
}

/**
 * Draws the card back
 */
function drawCardBack(backType) {
	switch (backType) {
		case 'logo':
			document.getElementById('buttonStack2').innerHTML = '';
			document.getElementById('cardBack').innerHTML = '';

			content = "<div class='center'></div>";
			content += "<div class='center cardMakerLink'>www.CardMaker.co.za</div>";
			document.getElementById('cardBack').innerHTML = content;

			break;
		case 'text':
			drawButtonStack('B');
			break;
		default:
			document.getElementById('buttonStack2').innerHTML = '';
			document.getElementById('cardBack').innerHTML = '';
			break;
	}

	calculatePrice();
}


/**
 * Update the logo after upload
 */
function updateLogo(image) {
	document.getElementById('cardLogo').src = image;
	document.getElementById('cardLogoSrc').value = image;
}

/**
 * Get products and pricing
 */
function getProducts() {
	var url = '/ajax/products.ajax.php';

	$.get(url,
		function(data) {
			insertProducts(data);
			setDefault();
		}, 'json');
}

/**
 * Insert products
 */
function insertProducts(data) {
	var counter = 0;

	//Packages
	for (i = 0; i < data['packages'].length; i++) {
		$('#cardQuantity')
			.append($("<option></option>")
			.attr("value",data['packages'][i].code)
			.text(data['packages'][i].name));
		price[counter] = data['packages'][i].price;
		code[counter] = data['packages'][i].code;
		counter++;
	}

	//Card Back
	for (i = 0; i < data['back'].length; i++) {
//		$('#backCardSelect')
//			.append($("<option></option>")
//			.attr("value",data['back'][i].code)
//			.text(data['back'][i].name));
		price[counter] = data['back'][i].price;
		code[counter] = data['back'][i].code;
		counter++;
	}

	$('#backCardSelect')
		.append($("<option></option>")
		.attr("value",'logo')
		.text('CardMaker Logo'));
	$('#backCardSelect')
		.append($("<option></option>")
		.attr("value",'empty')
		.text('Blank Back'));
	$('#backCardSelect')
		.append($("<option></option>")
		.attr("value",'text')
		.text('Your Black Text'));

}


/**
 * Calculates the price
 */
function calculatePrice() {
	var cardBack = document.getElementById('backCardSelect').value;
	var cardQuantity = document.getElementById('cardQuantity').value;
	var priceBack = 0;
	var priceQuantity = 0;
	var backCode = cardBack + cardQuantity;

	//Get price for card back
	for (i = 0; i < code.length; i++) {
		if (code[i] == backCode) {
			priceBack = parseInt(price[i]);
			break;
		}
	}

	//Get price for quantity
	for (i = 0; i < code.length; i++) {
		if (code[i] == cardQuantity) {
			priceQuantity = parseInt(price[i]);
			break;
		}
	}

	//Total Price
	var totalValue = priceBack + priceQuantity;

	//Save image
	var saveImage = 'save_000.png';
	if (cardBack == 'logo') {
		switch (cardQuantity) {
			case '500':
				saveImage = 'save_100.jpg';
				break;
			case '1000':
				saveImage = 'save_200.jpg';
				break;
			case '2000':
				saveImage = 'save_300.jpg';
				break;
			default:
				break;
		}
	}
	$("#saveImage").fadeOut(function() {
		$(this).load(function() { $(this).fadeIn(); });
		$(this).attr("src", '/i/'+saveImage);
	  });


	if (totalValue > 999) {
		document.getElementById('orderValue').className = 'orderValueBig';
	} else {
		document.getElementById('orderValue').className = 'orderValue';
	}

//	if (totalValue == 199) totalValue = '000';
	document.getElementById('orderValue').innerHTML = 'R' + totalValue;
}

/**
 * Subit the business card
 */
function submitCard() {
	if (!document.getElementById('tac').checked) {
		alert('You need to accept our Terms and Conditions to continue');
		return false;
	} else {
		document.getElementById('busCardForm').submit();
	}


}

/**
 * Set the PDF URL's
 */
function setPDF(front, back) {
	document.onload = document.getElementById('cardFront').data = front;

}

function setCharAt(str,index,chr) {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}

/**
 * Set default buscard deails
 */
function setDefault() {
	var url = '/ajax/default.ajax.php';

	$.get(url,
		function(data) {
			drawDefault(data)
		}, 'json');
}

/**
 * Draw default
 */
function drawDefault(data) {
	var frontData = JSON.parse(data.front);
	var backData = JSON.parse(data.back);
	var styles = new Array('bold', 'italic', 'small', 'medium', 'large');
	var stylePos;

	/* fill front card */
	for (i = 0; i < 10; i++) {
		var cardLine = document.getElementById('FcardLine'+i);
		var lineText = document.getElementById('Fline'+i);
		var classes = cardLine.className;

		lineText.value = frontData[i].text;

		for (y = 0; y < styles.length; y++) {
			stylePos = frontData[i].style.substr(y,1);
			if (stylePos == '0') {
				classes = classes.replace(styles[y],'');
				document.getElementById('F'+styles[y]+'Button'+i).className = document.getElementById('F'+styles[y]+'Button'+i).className.replace('buttonOn', 'buttonOff');
				toggleValue = '0';
			} else {
				classes += ' ' + styles[y];
				document.getElementById('F'+styles[y]+'Button'+i).className = document.getElementById('F'+styles[y]+'Button'+i).className.replace('buttonOff', 'buttonOn');
				toggleValue = '1';
				switch (styles[y]) {
					case 'small':
						maxLength = '35'; //42
						$('#Fline'+i).attr('maxLength',maxLength);
						break;
					case 'medium':
						maxLength = '29'; //35
						$('#Fline'+i).attr('maxLength',maxLength);
						break;
					case 'large':
						maxLength = '23'; //27
						$('#Fline'+i).attr('maxLength',maxLength);
						break;

				}

			}
		}

		document.getElementById('F'+"lineStyle"+ i).value = frontData[i].style;

		cardLine.className = classes;

		cardLine.innerHTML = lineText.value;

	}

	//Logo
	document.getElementById('cardLogoSrc').value = frontData.logo;
	if (frontData.logo == '') {
		document.getElementById('cardLogo').src = '/templates/images/sample_logo.png';
	} else {
		document.getElementById('cardLogo').src = frontData.logo;
	}
	document.getElementById('cardTemplate').value = frontData.cardTemplate;
	changeTemplate(frontData.cardTemplate);

	/* Fill card back */
	document.getElementById('backCardSelect').value = backData.type;
	drawCardBack(backData.type);

	switch (backData.type) {
		case 'empty':

			break;
		case 'text':
			for (i = 0; i < 10; i++) {
			cardLine = document.getElementById('BcardLine'+i);
			lineText = document.getElementById('Bline'+i);
			classes = cardLine.className;

			lineText.value = backData[i].text;

			for (y = 0; y < styles.length; y++) {
				stylePos = backData[i].style.substr(y,1);
				if (stylePos == '0') {
					classes = classes.replace(styles[y],'');
					document.getElementById('B'+styles[y]+'Button'+i).className = document.getElementById('B'+styles[y]+'Button'+i).className.replace('buttonOn', 'buttonOff');
					toggleValue = '0';

				} else {
					classes += ' ' + styles[y];
					document.getElementById('B'+styles[y]+'Button'+i).className = document.getElementById('B'+styles[y]+'Button'+i).className.replace('buttonOff', 'buttonOn');
					toggleValue = '1';
					switch (styles[y]) {
						case 'small':
							maxLength = '58';//60
							$('#Bline'+i).attr('maxLength',maxLength);
							break;
						case 'medium':
							maxLength = '48'; //50
							$('#Bline'+i).attr('maxLength',maxLength);
							break;
						case 'large':
							maxLength = '42'; //40
							$('#Bline'+i).attr('maxLength',maxLength);
							break;
					}
				}
			}

			document.getElementById('B'+"lineStyle"+ i).value = backData[i].style;

			cardLine.className = classes;

			cardLine.innerHTML = lineText.value;
		}
			break;
		default:

			break;
	}


	//Quantity
	document.getElementById('cardQuantity').value = data.quantity;

	calculatePrice();
}

/**
 * Toggle Terms and Conditions highlighting
 */
function toggleHighlight(state) {
		$('#TandC').toggleClass('tcHighlight',  '3000');
}

function changeTemplate(templateID) {
	cardTemplate = templateID;
	$('#cardTemplate').val(templateID);
	drawCard('F');
	resetButtonStack('F');
	if (templateID == 1) {
		for (i=0; i<10;i++) {
			resetStyle(i, 'F');
		}
	} else {
		for (i=0; i<6;i++) {
			setText(i,'', 'F');
			resetStyle(i, 'F');
		}
	}
}

function displayCards(pos) {
    var cardData = $('#card' + pos + '_data').val().split(',');
    $('#cardFront'+pos).attr('data', cardData[cardPos[pos]]);
    cardPos[pos]++;
    if (cardPos[pos] == cardData.length) cardPos[pos] = 0;
}

function startTimer2() {
    cardCounter2 = setInterval(function() {displayCards(2)}, 6000);
}

function startTimer3() {
    cardCounter3 = setInterval(function() {displayCards(3)}, 6000);
}