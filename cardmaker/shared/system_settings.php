<?php
//Includes 
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';


//Map
if (isset($_POST['updSysSetting'])) {
	update_settings();
} else {
	show_settings();
}

function update_settings() {
	//Initialize Variables
	$posts = array('ftp_host', 'ftp_username', 'ftp_dir', 'error_reporting');
	if (!empty($_POST['ftp_password'])) $posts[] = 'ftp_password';
	
	//FTP Class
	//include_once 'file_man.class.php';
	//$fle = new file_man();
	
	//Get current Google ID
	//$SQL_statement = "SELECT value FROM dbweb_settings WHERE tag='{$_POST['google_verification_id']}'";
	//if (($SQL_oldID =  mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	//list($old_id) = mysql_fetch_array($SQL_oldID);
	
	//Update Settings
	foreach ($posts as $tag) {
		$val = mysql_real_escape_string($_POST[$tag]);
		$SQL_statement = "SELECT count(*) FROM dbweb_settings WHERE tag='$tag'";
		if (($SQL_count = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
		list($count) = mysql_fetch_array($SQL_count);
		if ($count == 0) {
			$SQL_statement = "INSERT INTO dbweb_settings (tag, value) VALUES ('$tag','$val')";
		} else {
			$SQL_statement = "UPDATE dbweb_settings SET value='$val' WHERE tag='$tag'";
		}
		if ((mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	}
	
	//Check if google file already exists
	//if ($old_id != $_POST['google_verification_id']) {
	//	if (is_file(SITE_ROOT . "/$old_id")) unlink(SITE_ROOT . "/$old_id");
	//	$fle->create_file('/', $_POST['google_verification_id'] . ".html" );
	//}
	
	show_settings();
}

function show_settings() {
	//Get template
	$template = file_get_contents('../html/system_settings.html');
	
	//Get data
	$settings_tags = array('ftp_host', 'ftp_username', 'ftp_dir', 'error_reporting');
	foreach ($settings_tags as $tag) {
		$SQL_statement = "SELECT value FROM dbweb_settings WHERE tag='$tag'";
		if (($SQL_setting = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_errno(), E_USER_NOTICE);
		if (mysql_num_rows($SQL_setting) == 0) {
			$settings[$tag] = '';
		} else {
			list($settings[$tag]) = mysql_fetch_array($SQL_setting);
		}
	}
	
	
	//Error reporting tags
	if ($settings['error_reporting'] == 'on') {
		$settings['error_reporting_on'] = 'checked';
		$settings['error_reporting_off'] = '';
	} else {
		$settings['error_reporting_on'] = '';
		$settings['error_reporting_off'] = 'checked';
	}
	
	//Replace Tags
	foreach ($settings as $tag=>$val) {
		$template = str_replace("<!-- $tag -->", $val, $template );
	}
	
	//Serve
	echo $template;
}

?>