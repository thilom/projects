Vo=0

function validate(f,k)
{
	if(Vo||f)
	{
		var e=0
		var p=""
		var cp=""
		for(var i=0;i<Vo.length;i++)
		{
			var c=0
			var o=Vo[i]
			if(o.title=="Requires value" && (!o.value || o.value==0))
				c=1
			if(o.title.indexOf("Requires password")==0)
			{
				if(o.value)
				{
					if(o.title.indexOf("characters"))
					{
						p=o.title.substr(18)
						p=parseInt(p.substr(0,p.indexOf(" ")))
						if(o.value.length<p)
						{
							c=1
							if(k==1)
							{
								alert("Password must be over "+(p-1)+" characters long")
								cp=1
							}
						}
					}
					p=o.value
				}
				else
					c=1
			}
			if(o.title=="Requires confirmation")
			{
				if(!o.value || (o.value && p!=o.value))
					c=1
				if(o.value && p!=o.value)
				{
					if(k==1 && !cp)
						alert("Confirmation does not match password")
					cp="\nConfirmation does not match password"
				}
			}
			else if(o.title=="Requires email" && (!o.value || o.value.indexOf("@")<1 || o.value.indexOf(".")<1))
				c=1
			else if(o.lang=="L")
			{
				if(o.value==0)
					c=1
				else if(o.value=="A"||o.className=="w0")
				{
					o.style.background="white"
					var a=o.parentNode.getElementsByTagName('INPUT')[0]
					if(a)
					{
						if(a.value)
							a.style.background="white"
						else
						{
							o=a
							c=1
						}
					}
				}
			}
			o.style.background=(c?"yellow":"white")
			e=e+c
		}
		if(f&&e)
		{
			alert(e+" Field"+(e!=1?"s":"")+" require proper values!"+cp)
			return false
		}
		else if(f)
		{
			f=document.getElementsByTagName('FORM')[0].getElementsByTagName('INPUT')
			for(var i=0;i<f.length;i++)
			{
				if(f[i].type.toLowerCase()=="submit")
					f[i].disabled=true
			}
		}
	}
	else // setup
	{
		Vo=new Array()
		var a=document.getElementsByTagName('FORM')[0].getElementsByTagName('*')
		var n=0
		for(var i=0;i<a.length;i++)
		{
			if(a[i].title || a[i].lang)
			{
				if(a[i].title=="Requires value" || a[i].title=="Requires email" || a[i].title.indexOf("Requires password")==0 || a[i].title=="Requires confirmation")
				{
					if(a[i].tagName.toLowerCase()=="select")
						a[i].onchange=function(){validate()}
					else
						a[i].onkeyup=function(){validate()}
					if(a[i].title.indexOf("Requires password")==0 || a[i].title=="Requires confirmation")
						a[i].onblur=function(){validate(0,1)}
					else
						a[i].onblur=function(){validate()}
					if(!a[i].value)
						a[i].style.background="yellow"
					Vo[n]=a[i]
					n++
				}
				else if(a[i].lang && a[i].tagName.toLowerCase()=="select")
				{
					if(a[i].lang=="L")
					{
						a[i].onchange=function(){SL(this);validate()}
						a[i].onblur=function(){validate()}
						var b=a[i].parentNode.getElementsByTagName('INPUT')[0]
						if(b)
						{
							b.style.background="yellow"
							b.onkeyup=function(){validate()}
						}
					}
					else
						a[i].onchange=function(){validate()}
					if(a[i].value==0)
						a[i].style.background="yellow"
					Vo[n]=a[i]
					n++
				}
			}
		}
	}
}