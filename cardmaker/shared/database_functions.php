<?php
/**
 * Various function to manipulate DB
 *
 * @version $id$
 * @author Thilo Muller(2011)
 */

/**
 * Get the next value in an auto-increment field
 *
 * @param string $table
 * @param string $field
 * @return int
 *
 * @todo Check for race condition. Maybe save the next increment in a table.
 */
function next_id($table, $field) {
	$statement = "SELECT MAX($field)+1 AS max FROM $table";
	$sql_next = $GLOBALS['dbCon']->prepare($statement);
	$sql_next->execute();
	$sql_data = $sql_next->fetch();
	$sql_next->closeCursor();

	if (empty($sql_data['max'])) $sql_data['max'] = 1;

	return $sql_data['max'];
}

?>
