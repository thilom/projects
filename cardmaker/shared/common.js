/*
 * Shared Js functions
 *
 * @author Thilo Muller(2011)
 * @version $Id: common.js 139 2012-03-02 19:45:57Z thilo $
 */


/**
 * Get a value from URL
 */
function getUrlValue(sVal) {
	var searchResult;
	docLocation = document.location.href;
	searchLength = sVal.length + 1;
	WLoc = docLocation.indexOf(sVal+'=');
	if (WLoc > 0) {
		WEnd = docLocation.indexOf('&', WLoc);
		searchResult = WEnd>0?docLocation.substring(WLoc+searchLength,WEnd):docLocation.substring(WLoc+searchLength);
	} else {
		searchResult=0;
	}
	return searchResult;
}

/**
 * Set a browser cookie
 */
function setCookie(cookieName,value,exdays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var cookieValue=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=cookieName + "=" + cookieValue;
}

/**
 * Get a cookie Value
 */
function getCookie(cookieName) {
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++) {
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==cookieName) {
			return unescape(y);
		}
	}
	return false;
}

/**
 * Toggle field values between indicator values and real values.
 */
function toggleFieldValue(field, state, value) {
	if (state == 0) {
		if (field.value == '') {
			field.value = value;
			field.className = 'searchOff';
		}
	} else {
		if (field.value == value) {
			field.value = '';
			field.className = 'searchOn';
		}
	}
}

/**
 * Checks that all required fields are completed
 */
function checkRequired() {
	//Vars
	fields = document.getElementsByTagName('INPUT');
	msg = "Please complete all required fields";
	msg2 = "You need to accept our terms and conditions to continue";
	

	for (i = 0; i < fields.length; i++) {
		if (fields[i].title.substr(0,9) == 'Required:') {
			if (fields[i].type == 'checkbox') {
				if (fields[i].checked == false) {
					alert(msg2);
					return false;
				}
			} else {
				if (fields[i].value.length == 0) {
					alert(msg);
					return false;
				}
			}
			
		}
	}

	document.getElementById('orderForm').submit();

}

/**
 * Returns the X:Y position of the passed element.
 */
function findPos(fieldID) {
	var curleft = 0;
	var curtop = 0;
	if (fieldID.offsetParent) {
		do {
			curleft += fieldID.offsetLeft;
			curtop += fieldID.offsetTop;
		} while (fieldID = fieldID.offsetParent);
		return [curleft,curtop];
	}
}