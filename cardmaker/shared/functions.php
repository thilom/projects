<?php

/**
 *  Set of shared functions
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: functions.php 87 2011-09-14 06:45:49Z thilo $
 */

/**
 * Returns a file list for a particular hook name. Traverses the modules directory 
 * looking for a 'hooks' directory and then looks inside for a file called $hook_name.
 * 
 * @param type $hook_name
 * @return type 
 */
function get_hooks($hook_name) {
	$hooks = array();
	
	$dir = SITE_ROOT . "/modules";
	$files = scandir($dir);
	
	foreach ($files as $file) {
		if (is_file(SITE_ROOT . "/modules/$file/hooks/$hook_name.php")) {
			$hooks[] = "/modules/$file/hooks/$hook_name.php";
		}
	}
	
	return $hooks;
}
?>
