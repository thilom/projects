<?php

//includes
require_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";

//Vars
$image_location = '';

//Is the file empty
if (empty($_FILES['logo']['name'])) {
	echo "<div style='font-size: 0.8em; color: red; text-align: center'>You need to choose a file first</div>";
	echo "<meta http-equiv='refresh' content='2; URL=/html/upload_logo_form.html'>";
	die();
}

//Save Logo
if (is_uploaded_file($_FILES['logo']['tmp_name']) === true) {
	$file_id = session_id();
	$image_location = "/i/client_logos/".$file_id."_{$_FILES['logo']['name']}";
	move_uploaded_file($_FILES['logo']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $image_location);
} else {
	echo "<meta http-equiv='refresh' content='4; URL=/html/upload_logo_form.html'>";
	die();
}

//Check if the uploaded file is an image
if(function_exists('finfo_open')) {
	$file_info = new finfo(FILEINFO_MIME);
	$mime_type = $file_info->file($_SERVER['DOCUMENT_ROOT'] .$image_location);
} else if (function_exists(mime_content_type)) {
	 $mime_type = mime_content_type($_SERVER['DOCUMENT_ROOT'] .$image_location);
} else if (function_exists( 'exif_imagetype' )) {
	$mime_type = exif_imagetype($_SERVER['DOCUMENT_ROOT'] .$image_location);
	switch($mime_type) {
		case IMAGETYPE_GIF:
			$mime_type = 'image/gif';
			break;
		case IMAGETYPE_JPEG:
			$mime_type = 'image/jpeg';
			break;
		case IMAGETYPE_PNG:
			$mime_type = 'image/png';
			break;
		default:
	}
} else  {
	$mime_type = getimagesize($_SERVER['DOCUMENT_ROOT'] .$image_location);
	
	switch($mime_type[2]) {
		case IMAGETYPE_GIF:
			$mime_type = 'image/gif';
			break;
		case IMAGETYPE_JPEG:
			$mime_type = 'image/jpeg';
			break;
		case IMAGETYPE_PNG:
			$mime_type = 'image/png';
			break;
		default:
	}
}
if (substr($mime_type, 0, 10) != 'image/jpeg' &&  substr($mime_type, 0, 9) != 'image/png' && substr($mime_type, 0, 9) != 'image/gif') {
	echo "<div style='font-size: 0.8em; color: red; text-align: center'>Only JPEG, PNG and GIF files allowed</div>";
	unlink($_SERVER['DOCUMENT_ROOT'] .$image_location);
	echo "<meta http-equiv='refresh' content='4; URL=/html/upload_logo_form.html'>";
	die();
}

echo "<script>parent.updateLogo('$image_location')</script>";
echo "<meta http-equiv='refresh' content='1; URL=/html/upload_logo_form.html'>";
?>
