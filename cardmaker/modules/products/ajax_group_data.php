<?php

/**
 * Collect group information
 *
 * @author Thilo Muller(2012)
 * @version $Id$
 */
ob_start();
//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$product_id = $_GET['product_id'];
$groups = explode(',',$_GET['groups']);
$group_list = '';

//Prepare statement - Group name
$statement = "SELECT group_name
			FROM {$GLOBALS['db_prefix']}_products_variations_groups
			WHERE product_id=:product_id AND group_id=:group_id
			LIMIT 1";
$sql_name = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Group items
$statement = "SELECT item_code, item_name
			FROM {$GLOBALS['db_prefix']}_products_variations_variable
			WHERE group_id=:group_id AND product_id=:product_id";
$sql_items = $GLOBALS['dbCon']->prepare($statement);

//Get group data
foreach ($groups as $group_id) {
	if (empty($group_id)) continue;

	//Vars
	$group_name = '';
	$group_list = '';

	$sql_name->bindParam(':product_id', $product_id);
	$sql_name->bindParam(':group_id', $group_id);
	$sql_name->execute();
	$sql_name_data = $sql_name->fetch();
	$group_name = $sql_name_data['group_name'];

	$group_list .= "$group_id///$group_name~~~";

	$sql_items->bindParam(':product_id', $product_id);
	$sql_items->bindParam(':group_id', $group_id);
	$sql_items->execute();
	$sql_items_data = $sql_items->fetchAll();
	foreach ($sql_items_data as $item_data) {
		$group_list .= "{$item_data['item_code']}///{$item_data['item_name']}~~~";
	}

	$group_list .= "|||";

}

echo $group_list;
$clean_data = ob_get_clean();
ob_end_clean();

echo trim($clean_data);

?>