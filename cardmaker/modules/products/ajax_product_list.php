<?php

/**
 * Returns a list of products
 *
 * @author Thilo Muller (2011)
 * @version $Id$
 */

ob_start(); //Added this to get rid of an unfound include which added whitespace to the beginning of the reselt

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$product_list = '';
$startAt = isset($_GET['start'])&&!empty($_GET['start'])?$_GET['start']:0;
$length = isset($_GET['length'])?$_GET['length']:10;
$filter_string = isset($_GET['filter'])?"%{$_GET['filter']}%":'';

//Get total product count
$statement = "SELECT COUNT(*) AS product_count
				FROM {$GLOBALS['db_prefix']}_products";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$product_list .= "|||data~~~{$sql_count_data['product_count']}";

//Get result product count
$statement = "SELECT COUNT(*) AS product_count
				FROM {$GLOBALS['db_prefix']}_products";
if (!empty($filter_string)) {
	$statement .= " WHERE product_name LIKE :filter_string";
}
$sql_count = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) {
	$sql_count->bindParam(':filter_string', $filter_string);
}
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$product_list .= "~~~{$sql_count_data['product_count']}";

//Get products
$statement = "SELECT product_id,  product_name, product_code, product_price
				FROM {$GLOBALS['db_prefix']}_products";
if (!empty($filter_string)) {
	$statement .= " WHERE product_name LIKE :filter_string";
}
$statement .= " ORDER BY product_name ";
$statement .= "	LIMIT $startAt, $length";
$sql_products = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) {
	$sql_products->bindParam(':filter_string', $filter_string);
}
$sql_products->execute();
$sql_products_data = $sql_products->fetchAll();
$sql_products->closeCursor();

//Prepare statement - Variations
$statement = "SELECT COUNT(*) AS variation_count
				FROM {$GLOBALS['db_prefix']}_products_variations
				WHERE product_id=:product_id";
$sql_variations = $GLOBALS['dbCon']->prepare($statement);

//Assemble Matches
foreach ($sql_products_data as $product_data) {

	$sql_variations->bindParam(':product_id', $product_data['product_id']);
	$sql_variations->execute();
	$sql_variations_data = $sql_variations->fetch();
	$variations = $sql_variations_data['variation_count']>0?'Y':'';

	//Price
	$price = $variations!='Y'?$product_data['product_price']:'';

	$product_list .= "|||{$product_data['product_id']}~~~{$product_data['product_code']}~~~{$product_data['product_name']}~~~$variations~~~$price";
}


echo $product_list;

$content = ob_get_clean();

echo trim($content);

?>