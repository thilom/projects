<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Products Manager";
$module_id = "products"; //needs to be the same as the module directory
$module_icon = 'off_products_64.png';
$module_link = 'products.php';
$window_width = '800';
$window_height = '';
$window_xposition = '';
$window_yposition = '';

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Products Manager";
$security_id = "mod_products";

//Content Types
$content_type[0]['name'] = 'Products DB: Text product list';
$content_type[0]['id'] = 'text_product_list';
$content_type[0]['content_file'] = 'edit/prd_text_list.php';
$content_type[0]['display_file'] = 'view/prd_text_list.php';
$content_type[0]['public_display_file'] = 'view/prd_text_list.php';
$content_type[0]['preferences_file'] = 'edit/preferences_prd_text_list.php';
$content_type[0]['css_file'] = 'edit/css_prd_text_list.php';
$content_type[0]['explanation'] = 'Dislay a list of products in text format';

//$content_type[1]['name'] = 'Products DB: Image product list';
//$content_type[1]['id'] = 'image_product_list';
//$content_type[1]['content_file'] = 'edit/quote_ticker.php';
//$content_type[1]['display_file'] = 'view/quote_ticker.php';
//$content_type[1]['public_display_file'] = 'view/public_quote_ticker.php';
//$content_type[1]['preferences_file'] = 'edit/preferences_quote_ticker.php';
//$content_type[1]['css_file'] = 'edit/css_quote_ticker.php';
//$content_type[1]['explanation'] = 'Dislay a list af products as a set of images';

$content_type[2]['name'] = 'Products DB: Full Description';
$content_type[2]['id'] = 'product_full';
$content_type[2]['content_file'] = 'edit/prd_full_list.php';
$content_type[2]['display_file'] = 'view/prd_full_list.php';
$content_type[2]['public_display_file'] = 'view/public_prd_full_list.php';
$content_type[2]['preferences_file'] = 'edit/preferences_prd_full_list.php';
$content_type[2]['css_file'] = 'edit/css_prd_full_list.php';
$content_type[2]['explanation'] = 'Displays the full description of a product';

$content_type[3]['name'] = 'Products DB: Product variations list';
$content_type[3]['id'] = 'product_variations_text';
$content_type[3]['content_file'] = 'edit/prd_variations_list.php';
$content_type[3]['display_file'] = 'view/prd_variations_list.php';
$content_type[3]['public_display_file'] = 'view/public_prd_variations_list.php';
$content_type[3]['preferences_file'] = 'edit/preferences_prd_variations_list.php';
$content_type[3]['css_file'] = 'edit/css_prd_variations_list.php';
$content_type[3]['explanation'] = 'Displays a list of variation associated with a product';

$content_type[4]['name'] = 'Products DB: Related products';
$content_type[4]['id'] = 'product_related_text';
$content_type[4]['content_file'] = 'edit/prd_related_list.php';
$content_type[4]['display_file'] = 'view/prd_related_list.php';
$content_type[4]['public_display_file'] = 'view/public_prd_related_list.php';
$content_type[4]['preferences_file'] = 'edit/preferences_prd_related_list.php';
$content_type[4]['css_file'] = 'edit/css_prd_related_list.php';
$content_type[4]['explanation'] = 'Displays a list of products related to the active product';

$content_type[5]['name'] = 'Products DB: Manufacturer list';
$content_type[5]['id'] = 'product_manufacturer_text';
$content_type[5]['content_file'] = 'edit/prd_manufac_list.php';
$content_type[5]['display_file'] = 'view/prd_manufac_list.php';
$content_type[5]['public_display_file'] = 'view/prd_manufac_list.php';
$content_type[5]['preferences_file'] = 'edit/preferences_prd_manufac_list.php';
$content_type[5]['css_file'] = 'edit/css_prd_manufac_list.php';
$content_type[5]['explanation'] = 'Displays a list of manufacturers';

$content_type[6]['name'] = 'Products DB: Category list (Plain Text)';
$content_type[6]['id'] = 'product_category_text';
$content_type[6]['content_file'] = 'edit/prd_category_list.php';
$content_type[6]['display_file'] = 'view/prd_category_list.php';
$content_type[6]['public_display_file'] = 'view/prd_public_category_list.php';
$content_type[6]['preferences_file'] = 'edit/preferences_prd_category_list.php';
$content_type[6]['css_file'] = 'edit/css_prd_category_list.php';
$content_type[6]['explanation'] = 'Displays a list of product categories as a plain text list';

$content_type[7]['name'] = 'Products DB: Category list (Dropdown Menu Style)';
$content_type[7]['id'] = 'product_category_text_dd';
$content_type[7]['content_file'] = 'edit/prd_category_list_dd.php';
$content_type[7]['display_file'] = 'view/prd_category_list_dd.php';
$content_type[7]['public_display_file'] = 'view/prd_public_category_list_dd.php';
$content_type[7]['preferences_file'] = 'edit/preferences_prd_category_list_dd.php';
$content_type[7]['css_file'] = 'edit/css_prd_category_list_dd.php';
$content_type[7]['explanation'] = 'Displays a list of product categories as a dropdown menu';

//$content_type[8]['name'] = 'Category List: ';
//$content_type[8]['id'] = 'product_application_text';
//$content_type[8]['content_file'] = 'edit/quote_ticker.php';
//$content_type[8]['display_file'] = 'view/quote_ticker.php';
//$content_type[8]['public_display_file'] = 'view/public_quote_ticker.php';
//$content_type[8]['preferences_file'] = 'edit/preferences_quote_ticker.php';
//$content_type[8]['css_file'] = 'edit/css_quote_ticker.php';
//$content_type[8]['explanation'] = 'Displays a list of product applications';

//Settings
$settings[0]['name'] = "Products";
$settings[0]['file'] = "$module_id/settings/product_settings.php";
$settings[0]['order'] = "20";
?>