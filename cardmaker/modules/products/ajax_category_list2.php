<?php

/**
 * Return a list of product categories for javascript
 *
 * @author Thilo Muller (2012)
 * @version $Id$
 */

ob_start(); //Required to remove white space

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$category_list = '';
$parent_id = $_GET['parent'];

//Get data
$statement = "SELECT category_id, category_name
			FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_parent = :category_parent
			ORDER BY category_name";
$sql_categories = $GLOBALS['dbCon']->prepare($statement);
$sql_categories->bindParam(':category_parent', $parent_id);
$sql_categories->execute();
$sql_categories_data = $sql_categories->fetchAll();
$sql_categories->closeCursor();

//Assemble Data
foreach ($sql_categories_data as $category_data) {
	$category_list .= "{$category_data['category_id']}~~~{$category_data['category_name']}|||";
}

echo $category_list;

$list = ob_get_clean();

echo trim($list);


?>
