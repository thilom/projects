<?php

/**
 * The ini file for product category list layouts
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

$layout[0] = array();
$layout[0]['id'] = 'cl_tpl_A';
$layout[0]['name'] = 'Category name Only';
$layout[0]['file_html'] = 'product_category_list_A.html';
$layout[0]['file_css'] = 'product_category_list_A.css';
$layout[0]['description'] = 'List of categories with only the category name';
$layout[0]['fields'] = 'category name';

$layout[1] = array();
$layout[1]['id'] = 'cl_tpl_B';
$layout[1]['name'] = 'Category name with product count';
$layout[1]['file_html'] = 'product_category_list_B.html';
$layout[1]['file_css'] = 'product_category_list_B.css';
$layout[1]['description'] = 'List of product categories with product count for each';
$layout[1]['fields'] = 'category name, product count';

?>
