<?php

/**
 * The ini file for product manufacturer list layouts
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

$layout[0] = array();
$layout[0]['id'] = 'ml_tpl_A';
$layout[0]['name'] = 'Manufacturer Only';
$layout[0]['file_html'] = 'product_manufac_list_A.html';
$layout[0]['file_css'] = 'product_manufac_list_A.css';
$layout[0]['description'] = 'List of manufacturers with only the manufacturer name';
$layout[0]['fields'] = 'Manufacturer name';

$layout[1] = array();
$layout[1]['id'] = 'ml_tpl_B';
$layout[1]['name'] = 'Manufacturer with product count';
$layout[1]['file_html'] = 'product_manufac_list_B.html';
$layout[1]['file_css'] = 'product_manufac_list_B.css';
$layout[1]['description'] = 'List of product manufacturers with product count for each';
$layout[1]['fields'] = 'manufacturer name, product count';

?>
