<?php

/**
 * The ini file for full product information layouts
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

$layout[0] = array();
$layout[0]['id'] = 'fp_tpl_A';
$layout[0]['name'] = 'Verbose Information';
$layout[0]['file_html'] = 'product_full_A.html';
$layout[0]['file_css'] = 'product_full_A.css';
$layout[0]['description'] = 'Complete product information';
$layout[0]['fields'] = 'product_name, manufacturer, price';

$layout[1] = array();
$layout[1]['id'] = 'fp_tpl_B';
$layout[1]['name'] = 'Minimal information';
$layout[1]['file_html'] = 'product_full_B.html';
$layout[1]['file_css'] = 'product_full_B.css';
$layout[1]['description'] = 'Product information with minimal data';
$layout[1]['fields'] = 'product name';

$layout[2] = array();
$layout[2]['id'] = 'fp_tpl_C';
$layout[2]['name'] = 'Split images';
$layout[2]['file_html'] = 'product_full_C.html';
$layout[2]['file_css'] = 'product_full_C.css';
$layout[2]['description'] = 'Product information showing with split primary image and the rest';
$layout[2]['fields'] = 'product name';


?>
