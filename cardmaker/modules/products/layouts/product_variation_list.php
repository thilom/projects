<?php

/**
 * The ini file for product variations list layouts
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

$layout[0] = array();
$layout[0]['id'] = 'vl_tpl_A';
$layout[0]['name'] = 'Text List - Verbose';
$layout[0]['file_html'] = 'product_variation_list_A.html';
$layout[0]['file_css'] = 'product_variation_list_A.css';
$layout[0]['description'] = 'Full variation layout without shopping cart link';
$layout[0]['fields'] = 'Product name, code, manufacturer, price, model, short description';

$layout[1] = array();
$layout[1]['id'] = 'vl_tpl_B';
$layout[1]['name'] = 'Text List - Miminmal';
$layout[1]['file_html'] = 'product_variation_list_B.html';
$layout[1]['file_css'] = 'product_variation_list_B.css';
$layout[1]['description'] = 'Full variation layout with shopping cart link';
$layout[1]['fields'] = 'Product name, code, manufacturer, price, model, short description, shopping cart link';

?>
