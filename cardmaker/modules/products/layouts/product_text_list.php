<?php

/**
 * The ini file for product text list layouts
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

$layout[0] = array();
$layout[0]['id'] = 'tl_tpl_A';
$layout[0]['name'] = 'Text List - Verbose';
$layout[0]['file_html'] = 'product_text_list_A.html';
$layout[0]['file_css'] = 'product_text_list_A.css';
$layout[0]['description'] = 'Full product layout without shopping cart link';
$layout[0]['fields'] = 'Product name, code, manufacturer, price, model, short description';

$layout[1] = array();
$layout[1]['id'] = 'tl_tpl_B';
$layout[1]['name'] = 'Text List - Miminmal';
$layout[1]['file_html'] = 'product_text_list_B.html';
$layout[1]['file_css'] = 'product_text_list_B.css';
$layout[1]['description'] = 'Full product layout with shopping cart link';
$layout[1]['fields'] = 'Product name, code, manufacturer, price, model, short description, shopping cart link';

$layout[2] = array();
$layout[2]['id'] = 'tl_tpl_C';
$layout[2]['name'] = 'Text List - Column Format';
$layout[2]['file_html'] = 'product_text_list_C.html';
$layout[2]['file_css'] = 'product_text_list_C.css';
$layout[2]['description'] = 'List with name, primary image and description. Suitable for column format';
$layout[2]['fields'] = 'Product name, code, manufacturer, price, model, short description, shopping cart link';

?>
