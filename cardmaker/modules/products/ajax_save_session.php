<?php

/**
 * Saves the given data in $_SESSION
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;



if (isset($_GET['type']) && $_GET['type'] == 'color') {
	//Vars
	$color_id = $_GET['id'];
	$color_code = urldecode($_GET['code']);
	$color_name = urldecode($_GET['name']);

	//Save Session
	$_SESSION['colors'][$color_id]['code'] = $color_code;
	$_SESSION['colors'][$color_id]['name'] = $color_name;
} else if (isset($_GET['type']) && $_GET['type'] == 'size') {
	//Vars
	$size_id = $_GET['id'];
	$size_code = urldecode($_GET['code']);
	$size_name = urldecode($_GET['name']);

	//Save Session
	$_SESSION['sizes'][$size_id]['code'] = $size_code;
	$_SESSION['sizes'][$size_id]['name'] = $size_name;
} else {
	//Vars
	$variation_id = $_GET['id'];
	$variation_code = urldecode($_GET['code']);
	$variation_name = urldecode($_GET['name']);
	$variation_description = urldecode($_GET['description']);
	$variation_price = urldecode($_GET['price']);
	$price_from_product = urldecode($_GET['pricefromproduct']);

	//Save Session
	$_SESSION['variations'][$variation_id]['code'] = $variation_code;
	$_SESSION['variations'][$variation_id]['name'] = $variation_name;
	$_SESSION['variations'][$variation_id]['description'] = $variation_description;
	$_SESSION['variations'][$variation_id]['price'] = $variation_price;
	$_SESSION['variations'][$variation_id]['price_from_product'] = $price_from_product;
}

?>
