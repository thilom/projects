<?php

/**
 * Add or Edit a product color
 *
 * @author Thilo Muller (2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/variations_group_edit.html');
$color_code = '';
$color_name = '';


if (isset($_GET['color_id'])) {
	if ($_GET['color_id'] >= 800000) {

		//Find data in supplied string
		$color_code = $_SESSION['colors'][$_GET['color_id']]['code'];
		$color_name = $_SESSION['colors'][$_GET['color_id']]['name'];


	} else {

		//Does it exist in session
		if (isset($_SESSION['colors'][$_GET['color_id']])) {
			$color_code = $_SESSION['colors'][$_GET['color_id']]['code'];
			$color_name = $_SESSION['colors'][$_GET['color_id']]['name'];
		} else {
			$statement = "SELECT color_id, color_code, color_name
							FROM {$GLOBALS['db_prefix']}_products_colors
							WHERE color_id=:color_id
							LIMIT 1";
			$sql_color = $GLOBALS['dbCon']->prepare($statement);
			$sql_color->bindParam(':color_id', $_GET['color_id']);
			$sql_color->execute();
			$sql_color_data = $sql_color->fetch();
			$sql_color->closeCursor();

			$color_code = $sql_color_data['color_code'];
			$color_name = $sql_color_data['color_name'];
		}
	}
}

//Replace Tags
$template = str_replace('<!-- color_code -->', $color_code, $template);
$template = str_replace('<!-- color_name -->', $color_name, $template);

echo $template;
?>
