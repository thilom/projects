<?php

/**
 * Return a list of manufacturers for javascrip
 *
 * @author Thilo Muller (2012)
 * @version $Id$
 */

ob_start();

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$manufacturer_list = '';
$filter_string = isset($_GET['filter'])?"%{$_GET['filter']}%":'';
$start = (int) $_GET['start'];
$limit = (int) $_GET['length'];
$filter_count = '0';

//Get total manufacturer count
$statement = "SELECT COUNT(*) AS manufacturer_count
			FROM {$GLOBALS['db_prefix']}_products_manufacturers";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$manufacturer_count = $sql_count_data['manufacturer_count'];

//Get filter count
if (!empty($filter_string)) {
	$statement = "SELECT COUNT(*) AS filter_count
				FROM {$GLOBALS['db_prefix']}_products_manufacturers";
	$statement .= " WHERE manufacturer_name LIKE :filter_string";
	$statement .= "	ORDER BY manufacturer_name";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bindParam(':filter_string', $filter_string);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();
	$filter_count = $sql_count_data['filter_count'];
} else {
	$filter_count = $manufacturer_count;
}

//Get data
$statement = "SELECT manufacturer_id, manufacturer_name
			FROM {$GLOBALS['db_prefix']}_products_manufacturers";
if (!empty($filter_string)) $statement .= " WHERE manufacturer_name LIKE :filter_string";
$statement .= "	ORDER BY manufacturer_name
				LIMIT $start, $limit";
$sql_manufacturers = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) $sql_manufacturers->bindParam(':filter_string', $filter_string);
$sql_manufacturers->execute();
$sql_manufacturers_data = $sql_manufacturers->fetchAll();
$sql_manufacturers->closeCursor();

//Prepare statement - product_count
$statement = "SELECT count(*) AS count
			FROM {$GLOBALS['db_prefix']}_products
			WHERE product_manufacturer = :manufacturer_id";
$sql_product = $GLOBALS['dbCon']->prepare($statement);

//Assemble Data
foreach ($sql_manufacturers_data as $manufacturer_data) {

	//Vars
	$product_count = 0;

	//Product Count
	$sql_product->bindParam(':manufacturer_id', $manufacturer_data['manufacturer_id']);
	$sql_product->execute();
	$sql_product_data = $sql_product->fetch();
	$product_count = $sql_product_data['count'];

	$manufacturer_list .= "{$manufacturer_data['manufacturer_id']}~~~{$manufacturer_data['manufacturer_name']}~~~$product_count|||";
}

//Assemble data
$manufacturer_list = "data~~~$manufacturer_count~~~$filter_count|||" . $manufacturer_list;

echo $manufacturer_list;

$list = ob_get_clean();

echo trim($list);


?>
