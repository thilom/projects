<?php

/**
 * Delete a product category from the database and remove product references to it
 *
 * @author Thilo Muller(2012)
 * @version $Id$
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$category_id = isset($_GET['category_id'])?(int)$_GET['category_id']:0;

//Remove from DB
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_id=:category_id
			LIMIT 1";
$sql_delete = $GLOBALS['dbCon']->prepare($statement);
$sql_delete->bindParam('category_id', $category_id);
$sql_delete->execute();
$sql_delete->closeCursor();

//Remove references
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_category_bridge
			WHERE category_id=:category_id";
$sql_ref = $GLOBALS['dbCon']->prepare($statement);
$sql_ref->bindParam('category_id', $category_id);
$sql_ref->execute();
$sql_ref->closeCursor();

?>
