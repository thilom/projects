<?php

/**
 *  Toolbar for tour editing
 *
 *  @author Thilo Muller(2011)
 *  @version $Id: toolbar.php 131 2012-01-02 06:53:41Z thilo $
 */



//Menu Bar
$menu_bar = "<table id=menu_bar><tr>";

//Vars
$product_id = isset($_GET['product_id'])?$_GET['product_id']:'';
$manufacturers_enabled = 'N';
$applications_enabled = 'N';

//Get current Settings
$statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
$sql_ftp = $GLOBALS['dbCon']->prepare($statement);
$sql_ftp->execute();
$ftp_data = $sql_ftp->fetchAll();
$sql_ftp->closeCursor();
foreach ($ftp_data as $data) {
	switch ($data['tag']) {
		case 'manufacturers_enabled':
			$manufacturers_enabled = $data['value'];
			break;
		case 'applications_enabled':
			$applications_enabled = $data['value'];
			break;
	}
}

$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='products.php?W={$_GET['W']}'\">Products</td>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='categories.php?W={$_GET['W']}'\">Categories</td>";
//$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='color_editor.php?W={$_GET['W']}'\">Color Editor</td>";
if ($manufacturers_enabled == 'Y') $menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='manufacturers.php?W={$_GET['W']}'\">Manufacturers</td>";
if ($applications_enabled == 'Y')$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='applications.php?W={$_GET['W']}'\">Applications</td>";

//$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='product_edit.php?W={$_GET['W']}&f=Media&product_id=$product_id'\">Product Media</td>";
//$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='product_edit.php?W={$_GET['W']}&f=full&product_id=$product_id'\">Full Description</td>";
//if ($variation_enabled == 'Y') $menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='product_edit.php?W={$_GET['W']}&f=variation&product_id=$product_id'\">Variations</td>";
//$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='product_edit.php?W={$_GET['W']}&f=associations&product_id=$product_id'\">Related Products</td>";


$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td>";
if ($manufacturers_enabled == 'Y')  $menu_bar .= "<td class='menu_drop'></td>";
if ($applications_enabled == 'Y')  $menu_bar .= "<td class='menu_drop'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "</table>";
echo "<div id=top_menu >$menu_bar</div>";


?>
