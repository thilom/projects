<?php

/**
 *  Places image editor
 *
 *  @author Thilo Muller(2011)
 *  @version $Id: edit_product_medias.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';


//Toolbar
include 'toolbar.php';

//Vars
$image_list = "";
$image_template = file_get_contents('html/edit_product_media.html');
$doc_list = '';

//Delete Image
if (isset($_GET['delete_image']) ) {

	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_media
					WHERE product_media_id=:product_media_id
					LIMIT 1";
	$sql_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete->bindParam(':product_media_id', $_GET['delete_image']);
	$sql_delete->execute();
	$sql_delete->closeCursor();

	$message = '&#187; Image Deleted';
	write_log("Image Deleted",'tour', $_GET['product_id']);
	echo "<script type='text/javascript' src='/modules/tour/js/product_medias.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button'  onClick='document.location=\"/modules/products/edit_product_media.php?W={$_GET['W']}&product_id={$_GET['product_id']}\"'  ><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}

//Delete Image
if (isset($_GET['delete_doc']) ) {

	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_media
					WHERE product_media_id=:product_media_id
					LIMIT 1";
	$sql_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete->bindParam(':product_media_id', $_GET['delete_doc']);
	$sql_delete->execute();
	$sql_delete->closeCursor();

	$message = '&#187; Document Deleted';
	write_log("Documont Deleted",'products', $_GET['product_id']);
	echo "<script type='text/javascript' src='/modules/tour/js/product_medias.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button'  onClick='document.location=\"/modules/products/edit_product_media.php?W={$_GET['W']}&product_id={$_GET['product_id']}\"'  ><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}

//Get image list
$statement = "SELECT product_media_id, product_media_file, product_media_title, product_media_description, product_media_primary
				FROM {$GLOBALS['db_prefix']}_products_media
				WHERE product_id=:product_id AND (product_media_type = 'image' OR product_media_type IS NULL)
				ORDER BY product_media_primary DESC, product_media_title";
$sql_images = $GLOBALS['dbCon']->prepare($statement);
$sql_images->bindParam(':product_id', $_GET['product_id']);
$sql_images->execute();
$sql_images_data = $sql_images->fetchAll();
$sql_images->closeCursor();

//Assemble Image list
foreach ($sql_images_data as $image_data) {
	//Set empty values
	$image_title = empty($image_data['product_media_title'])?"-No Title-":$image_data['product_media_title'];
	$image_description = empty($image_data['product_media_description'])?"-No Description-":str_replace(array("\r\n","\n","\r"),'\n',$image_data['product_media_description']);

	$image_list .= "<tr valign='top'>";
	$image_list .= "<td width='180px'><img src='/i/products/{$image_data['product_media_file']}' class='place_thumbnail' ></td>";

	$image_list .= "<td><div class='place_title'>$image_title</div>";
	$image_list .= "<div class='place_description'>$image_description</div>";
	$image_list .= $image_data['product_media_primary']=='1'?"<span class='primary_image'>Primary Image</span>":"";
	$image_list .= "</td>";
	$image_list .= "<td class='button_td'>";
	$image_list .= "<button name='edit_image_{$image_data['product_media_id']}' >Edit Image</button><br>";
	$image_list .= "<button name='delete_image_{$image_data['product_media_id']}' >Remove Image</button>";
	$image_list .= "</td>";
	$image_list .= "</tr>";

//	$image_list .= "{}~~~{}~~~$image_title";
//	$image_list .= "~~~$image_description~~~{$image_data['product_media_primary']}";
//	$image_list .= "|||";
}

//Get doc list
$statement = "SELECT product_media_id, product_media_file, product_media_title, product_media_description
				FROM {$GLOBALS['db_prefix']}_products_media
				WHERE product_id=:product_id AND (product_media_type = 'doc')";
$sql_doc = $GLOBALS['dbCon']->prepare($statement);
$sql_doc->bindParam(':product_id', $_GET['product_id']);
$sql_doc->execute();
$sql_doc_data = $sql_doc->fetchAll();
$sql_doc->closeCursor();

//Assemble Image list
foreach ($sql_doc_data as $doc_data) {
	//Set empty values
	$doc_title = empty($doc_data['product_media_title'])?"-No Title-":$doc_data['product_media_title'];
	$doc_description = empty($doc_data['product_media_description'])?"-No Description-":str_replace(array("\r\n","\n","\r"),'\n',$doc_data['product_media_description']);

	$doc_list .= "<tr valign='top'>";

	$doc_list .= "<td><div class='place_title'>$doc_title</div>";
	$doc_list .= "<div class='place_description'>$doc_description</div>";
	$doc_list .= "<a href='/i/products/{$doc_data['product_media_file']}'>{$doc_data['product_media_file']}</a></td>";
	$doc_list .= "<td class='button_td'>";
	$doc_list .= "<button name='edit_doc_{$doc_data['product_media_id']}' >Edit Document</button><br>";
	$doc_list .= "<button name='delete_doc_{$doc_data['product_media_id']}' >Remove Document</button>";
	$doc_list .= "</td>";
	$doc_list .= "</tr>";
}


//Replace Tags
$image_template = str_replace('<!-- image_list -->', $image_list, $image_template);
$image_template = str_replace('<!-- doc_list -->', $doc_list, $image_template);

echo $image_template;


?>
