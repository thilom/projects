<?php

/**
 * Add or Edit a product size
 *
 * @author Thilo Muller (2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/variation_item_edit.html');
$item_code = $_GET['item_id']=='0'?'':$_GET['item_id'];
$item_name = $_GET['item_name'];
$group_id = $_GET['group_id'];
$group_name = $_GET['group_name'];


//Replace Tags
$template = str_replace('<!-- item_code -->', $item_code, $template);
$template = str_replace('<!-- item_name -->', $item_name, $template);
$template = str_replace('<!-- group_name -->', $group_name, $template);

echo $template;
?>
