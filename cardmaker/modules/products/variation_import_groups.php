<?php

/**
 * Import groups from other products
 *
 * @author Thilo Muller (2012)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/variation_import_groups.html');
$product_select = '';


//Get list of products
$statement = "SELECT product_id, product_name
			FROM {$GLOBALS['db_prefix']}_products
			ORDER BY product_name";
$sql_products = $GLOBALS['dbCon']->prepare($statement);
$sql_products->execute();
$sql_products_data = $sql_products->fetchAll();
$sql_products->closeCursor();

//Prepare statement - group list
$statement = "SELECT group_name, group_id
			FROM {$GLOBALS['db_prefix']}_products_variations_groups
			WHERE product_id=:product_id";
$sql_groups = $GLOBALS['dbCon']->prepare($statement);


//Assemble product list
foreach ($sql_products_data as $product_data) {

	//Vars
	$sql_groups_data = '';
	$value_list = '';

	//Get group list
	$sql_groups->bindParam(':product_id', $product_data['product_id']);
	$sql_groups->execute();
	$sql_groups_data = $sql_groups->fetchAll();

	//Assemble value list
	if (count($sql_groups_data) > 0) {
		$value_list = "{$product_data['product_id']}~~~";
		foreach ($sql_groups_data as $group_data) {
			$value_list .= "{$group_data['group_id']}///{$group_data['group_name']}~~~";
		}
		$product_select .= "<option value='$value_list'>{$product_data['product_name']}</option>";
	}

}

//Replace Tags
$template = str_replace('<!-- product_select -->', $product_select, $template);

echo $template;
?>
