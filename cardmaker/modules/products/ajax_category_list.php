<?php

/**
 * Return a list of product categories for javascript
 *
 * @author Thilo Muller (2012)
 * @version $Id$
 */

ob_start(); //Required to remove white space

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$category_list = '';
$filter_string = isset($_GET['filter'])?"%{$_GET['filter']}%":'';
$start = (int) $_GET['start'];
$limit = (int) $_GET['length'];
$filter_count = '0';

//Get total category count
$statement = "SELECT COUNT(*) AS category_count
			FROM {$GLOBALS['db_prefix']}_products_categories";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$category_count = $sql_count_data['category_count'];

//Get filter count
if (!empty($filter_string)) {
	$statement = "SELECT COUNT(*) AS filter_count
				FROM {$GLOBALS['db_prefix']}_products_categories";
	$statement .= " WHERE category_name LIKE :filter_string";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bindParam(':filter_string', $filter_string);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();
	$filter_count = $sql_count_data['filter_count'];
} else {
	$filter_count = $category_count;
}

//Get data
$statement = "SELECT category_id, category_name
			FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_parent = '0' OR category_parent IS NULL
			ORDER BY category_name";
$sql_categories = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) $sql_categories->bindParam(':filter_string', $filter_string);
$sql_categories->execute();
$sql_categories_data = $sql_categories->fetchAll();
$sql_categories->closeCursor();

//Prepare statement - product_count
$statement = "SELECT count(*) AS count
			FROM {$GLOBALS['db_prefix']}_products_category_bridge
			WHERE category_id = :category_id";
$sql_product = $GLOBALS['dbCon']->prepare($statement);

//Assemble Data
foreach ($sql_categories_data as $category_data) {

	//Vars
	$product_count = 0;

	//Product Count
	$sql_product->bindParam(':category_id', $category_data['category_id']);
	$sql_product->execute();
	$sql_product_data = $sql_product->fetch();
	$product_count = $sql_product_data['count'];

	$category_list .= "{$category_data['category_id']}~~~{$category_data['category_name']}~~~$product_count~~~0|||";

	//Get sub categories
	$statement = "SELECT category_id, category_name
			FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_parent = :category_parent
			ORDER BY category_name";
	$sql_subcat = $GLOBALS['dbCon']->prepare($statement);
	$sql_subcat->bindParam(':category_parent', $category_data['category_id']);
	$sql_subcat->execute();
	$sql_subcat_data = $sql_subcat->fetchAll();
	$sql_subcat->closeCursor();

	if (!empty($sql_subcat_data)) {
		foreach ($sql_subcat_data as $subcat_data) {

			//Product Count
			$sql_product->bindParam(':category_id', $subcat_data['category_id']);
			$sql_product->execute();
			$sql_product_data = $sql_product->fetch();
			$product_count = $sql_product_data['count'];

			$category_list .= "{$subcat_data['category_id']}~~~{$subcat_data['category_name']}~~~$product_count~~~1|||";

			//Get sub categories
			$statement = "SELECT category_id, category_name
					FROM {$GLOBALS['db_prefix']}_products_categories
					WHERE category_parent = :category_parent
					ORDER BY category_name";
			$sql_subcat2 = $GLOBALS['dbCon']->prepare($statement);
			$sql_subcat2->bindParam(':category_parent', $subcat_data['category_id']);
			$sql_subcat2->execute();
			$sql_subcat2_data = $sql_subcat2->fetchAll();
			$sql_subcat2->closeCursor();

			if (!empty($sql_subcat2_data)) {
				foreach ($sql_subcat2_data as $subcat2_data) {

					//Product Count
					$sql_product->bindParam(':category_id', $subcat2_data['category_id']);
					$sql_product->execute();
					$sql_product_data = $sql_product->fetch();
					$product_count = $sql_product_data['count'];

					$category_list .= "{$subcat2_data['category_id']}~~~{$subcat2_data['category_name']}~~~$product_count~~~2|||";


				}
			}

		}
	}
}

//Assemble data
$category_list = "data~~~$category_count~~~$filter_count|||" . $category_list;

echo $category_list;

$list = ob_get_clean();

echo trim($list);


?>
