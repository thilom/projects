<?php
/**
 *  Products - List available products
 *
 *  @author Thilo Muller(2011)
 *  @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/shared/database_functions.php';
include_once SITE_ROOT . "/shared/terms.php";

// Toolbar
include "toolbar_start.php";

//Vars
$template = file_get_contents('html/products.html');


//Replace Tags
$template = str_replace('<!-- terms -->', $terms, $template);

echo $template;

?>
