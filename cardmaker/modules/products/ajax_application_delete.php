<?php

/**
 * Delete a product application from the database and remove product references to it
 *
 * @author Thilo Muller(2012)
 * @version $Id$
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$application_id = isset($_GET['application_id'])?(int)$_GET['application_id']:0;

//Remove from DB
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_applications
			WHERE application_id=:application_id
			LIMIT 1";
$sql_delete = $GLOBALS['dbCon']->prepare($statement);
$sql_delete->bindParam('application_id', $application_id);
$sql_delete->execute();
$sql_delete->closeCursor();

//Remove references
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_applictaion_bridge
			WHERE application_id=:application_id";
$sql_ref = $GLOBALS['dbCon']->prepare($statement);
$sql_ref->bindParam('application_id', $application_id);
$sql_ref->execute();
$sql_ref->closeCursor();

?>
