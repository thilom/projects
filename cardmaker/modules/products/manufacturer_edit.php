<?php

/**
 * Edit/Add product manufacturer
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */
if (!isset($_GET['page'])) {
	echo "<script>document.location='edit_manufacturer_data.php?W={$_GET['W']}";
	echo isset($_GET['new']) ? "&new" : '';
	echo isset($_GET['parent']) ? "&parent={$_GET['parent']}" : '';
	echo isset($_GET['manufacturer_id']) ? "&manufacturer_id={$_GET['manufacturer_id']}" : '';
	echo isset($_GET['ver']) ? "&ver={$_GET['ver']}" : '';
	echo "'</script>";
} else {
	echo "<script>document.location='edit_manufacturer_data.php?W={$_GET['W']}";
	echo isset($_GET['manufacturer_id']) ? "&manufacturer_id={$_GET['manufacturer_id']}" : '';
	echo "'</script>";
}
?>

