<?php

/**
 * Edit/Add product categories
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

if (!isset($_GET['page'])) {
	echo "<script>document.location='edit_category_data.php?W={$_GET['W']}";
	echo isset($_GET['new'])?"&new":'';
	echo isset($_GET['parent'])?"&parent={$_GET['parent']}":'';
	echo isset($_GET['category_id'])?"&category_id={$_GET['category_id']}":'';
	echo isset($_GET['version'])?"&version={$_GET['version']}":'';
	echo "'</script>";
} else {
	echo "<script>document.location='edit_category_data.php?W={$_GET['W']}";
	echo isset($_GET['category_id'])?"&category_id={$_GET['category_id']}":'';
	echo "'</script>";
}

?>
