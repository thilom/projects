<?php

/**
 * Product variations editor
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar.php';

//Vars
$product_id = $_GET['product_id'];
$variation_list = '';
$message = '';
$color_list = '';
$size_list = '';
$variation_type = 'fixed';

//Get current Settings
$statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
$sql_ftp = $GLOBALS['dbCon']->prepare($statement);
$sql_ftp->execute();
$ftp_data = $sql_ftp->fetchAll();
$sql_ftp->closeCursor();
foreach ($ftp_data as $data) {
	switch ($data['tag']) {
		case 'variation_type':
			$variation_type = $data['value'];
			break;
	}
}

//Save Data
if (count($_POST) > 0) {

	//Get product name
	$statement = "SELECT product_name
					FROM {$GLOBALS['db_prefix']}_products
					WHERE product_id=:product_id
					LIMIT 1";
	$sql_product = $GLOBALS['dbCon']->prepare($statement);
	$sql_product->bindParam(':product_id', $product_id);
	$sql_product->execute();
	$sql_product_data = $sql_product->fetch();
	$sql_product->closeCursor();
	$product_name = $sql_product_data['product_name'];

	switch ($variation_type) {
		case 'fixed':

			//Prepare statement - New variation
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_variations
								(variation_id, product_id, variation_code, variation_name, variation_description,
									variation_price, price_from_product)
							VALUES
								(:variation_id, :product_id, :variation_code, :variation_name, :variation_description,
									:variation_price, :price_from_product)";
			$sql_variation_insert = $GLOBALS['dbCon']->prepare($statement);

			//Get and expand data
			$data_row = explode('|||', $_POST['variations']);
			foreach ($data_row as $data) {
				$data_items = explode('~~~', $data);
				if (empty($data_items[0])) continue;
				if ($data_items[0] >= 800000) {
					$variation_id = next_id("{$GLOBALS['db_prefix']}_products_variations", 'variation_id');

					$sql_variation_insert->bindParam(':variation_id', $variation_id);
					$sql_variation_insert->bindParam(':product_id', $product_id);
					$sql_variation_insert->bindParam(':variation_code', $data_items[1]);
					$sql_variation_insert->bindParam(':variation_name', $data_items[2]);
					$sql_variation_insert->bindParam(':variation_description', $data_items[3]);
					$sql_variation_insert->bindParam(':variation_price', $data_items[4]);
					$sql_variation_insert->bindParam(':price_from_product', $data_items[5]);
					$sql_variation_insert->execute();

					$message .= "&#187; Product variation ({$data_items[2]}) added to $product_name.<br>";
					write_log("Product variation ({$data_items[2]}) added to $product_name",'products', $product_id);

				} else {
					if ($data_items[1] == 'removed') {

						//Get current variation
						$statement = "SELECT variation_code
										FROM {$GLOBALS['db_prefix']}_products_variations
										WHERE variation_id=:variation_id
										LIMIT 1";
						$sql_variation_current = $GLOBALS['dbCon']->prepare($statement);
						$sql_variation_current->bindParam(':variation_id', $data_items[0]);
						$sql_variation_current->execute();
						$sql_variation_current_data = $sql_variation_current->fetch();
						$sql_variation_current->closeCursor();
						$current_variation = $sql_variation_current_data['variation_code'];

						$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_variations
										WHERE variation_id=:variation_id
										LIMIT 1";
						$sql_delete = $GLOBALS['dbCon']->prepare($statement);
						$sql_delete->bindParam(':variation_id', $data_items[0]);
						$sql_delete->execute();
						$sql_delete->closeCursor();

						$message .= "&#187; Variation '$current_variation' removed from product.<br>";

					} else {

						//Get current variation data
						$statement = "SELECT variation_code, variation_name, variation_description, variation_price, price_from_product
										FROM {$GLOBALS['db_prefix']}_products_variations
										WHERE variation_id=:variation_id
										LIMIT 1";
						$sql_variation_current = $GLOBALS['dbCon']->prepare($statement);
						$sql_variation_current->bindParam(':variation_id', $data_items[0]);
						$sql_variation_current->execute();
						$sql_variation_current_data = $sql_variation_current->fetch();
						$sql_variation_current->closeCursor();
						$current_code = $sql_variation_current_data['variation_code'];
						$current_name = $sql_variation_current_data['variation_name'];
						$current_description = $sql_variation_current_data['variation_description'];
						$current_price = $sql_variation_current_data['variation_price'];
						$current_price_from_product = $sql_variation_current_data['price_from_product'];

						//Variation Code
						if ($current_code != $data_items[1]) {
							$statement = "UPDATE {$GLOBALS['db_prefix']}_products_variations
											SET variation_code=:variation_code
											WHERE variation_id=:variation_id
											LIMIT 1";
							$sql_update = $GLOBALS['dbCon']->prepare($statement);
							$sql_update->bindParam(':variation_code', $data_items[1]);
							$sql_update->bindParam(':variation_id', $data_items[0]);
							$sql_update->execute();
							$sql_update->closeCursor();

							$message .= "&#187; Variation code changed from '$current_code' to '{$data_items[1]}'.<br>";
						}

						//Variation Name
						if ($current_name != $data_items[2]) {
							$statement = "UPDATE {$GLOBALS['db_prefix']}_products_variations
											SET variation_name=:variation_name
											WHERE variation_id=:variation_id
											LIMIT 1";
							$sql_update = $GLOBALS['dbCon']->prepare($statement);
							$sql_update->bindParam(':variation_name', $data_items[2]);
							$sql_update->bindParam(':variation_id', $data_items[0]);
							$sql_update->execute();
							$sql_update->closeCursor();

							$message .= "&#187; Variation name changed from '$current_name' to '{$data_items[2]} for '{$data_items[1]}'.<br>";
						}

						//Variation Description
						if ($current_description != $data_items[3]) {
							$statement = "UPDATE {$GLOBALS['db_prefix']}_products_variations
											SET variation_description=:variation_description
											WHERE variation_id=:variation_id
											LIMIT 1";
							$sql_update = $GLOBALS['dbCon']->prepare($statement);
							$sql_update->bindParam(':variation_description', $data_items[3]);
							$sql_update->bindParam(':variation_id', $data_items[0]);
							$sql_update->execute();
							$sql_update->closeCursor();

							$message .= "&#187; Variation description updated' for '{$data_items[1]}.<br>";
						}

						//Variation Price from product
						if ($current_price_from_product != $data_items[5]) {
							$statement = "UPDATE {$GLOBALS['db_prefix']}_products_variations
											SET price_from_product=:price_from_product
											WHERE variation_id=:variation_id
											LIMIT 1";
							$sql_update = $GLOBALS['dbCon']->prepare($statement);
							$sql_update->bindParam(':price_from_product', $data_items[5]);
							$sql_update->bindParam(':variation_id', $data_items[0]);
							$sql_update->execute();
							$sql_update->closeCursor();

							if ($data_items[5] =='Y') {
								$message .= "&#187; Variation price associated with product for '{$data_items[1]}'.<br>";
								$data_items[4] = 0;
							} else {
								$message .= "&#187; Independent variation price activated for '{$data_items[1]}'.<br>";
							}
						}

						//Variation Price
						if ($current_price != $data_items[4]) {
							$statement = "UPDATE {$GLOBALS['db_prefix']}_products_variations
											SET variation_price=:variation_price
											WHERE variation_id=:variation_id
											LIMIT 1";
							$sql_update = $GLOBALS['dbCon']->prepare($statement);
							$sql_update->bindParam(':variation_price', $data_items[4]);
							$sql_update->bindParam(':variation_id', $data_items[0]);
							$sql_update->execute();
							$sql_update->closeCursor();

							if ($data_items[4] != 'Y') $message .= "&#187; Variation price changed from '$current_price' to '{$data_items[4]}' for '{$data_items[1]}'.<br>";
						}
					}
				}
			}
			break;
		case 'variable':

			//Vars
			$groups = $_POST['groups'];
			$variations = $_POST['variations'];

			//Clear groups
			$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_variations_groups
						WHERE product_id=:product_id";
			$sql_clear_groups = $GLOBALS['dbCon']->prepare($statement);
			$sql_clear_groups->bindParam(':product_id', $product_id);
			$sql_clear_groups->execute();
			$sql_clear_groups->closeCursor();

			//Prepare statement - Group Insert
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_variations_groups
							(product_id, group_id, group_name)
						VALUES
							(:product_id, :group_id,:group_name)";
			$sql_group_insert = $GLOBALS['dbCon']->prepare($statement);

			//Insert Group
			$group_lines = explode('|||', $groups);
			foreach ($group_lines as $group_line) {
				if (empty($group_line)) continue;
				list($group_id, $group_name) = explode('~~~', $group_line);

				$sql_group_insert->bindParam(':product_id', $product_id);
				$sql_group_insert->bindParam(':group_id', $group_id);
				$sql_group_insert->bindParam(':group_name', $group_name);
				$sql_group_insert->execute();
			}

			//Clear Variation Items
			$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_variations_variable
						WHERE product_id=:product_id";
			$sql_clear_items = $GLOBALS['dbCon']->prepare($statement);
			$sql_clear_items->bindParam(':product_id', $product_id);
			$sql_clear_items->execute();
			$sql_clear_items->closeCursor();

			//Prepare statement - Insert Item
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_variations_variable
							(product_id, group_id, item_code, item_name)
						VALUES
							(:product_id, :group_id, :item_code, :item_name)";
			$sql_item_insert = $GLOBALS['dbCon']->prepare($statement);

			$item_lines = explode('|||', $variations);
			foreach ($item_lines as $item) {
				if (empty($item)) continue;
				list($item_code, $item_name, $group_id) = explode('~~~', $item);

				$sql_item_insert->bindParam(':product_id', $product_id);
				$sql_item_insert->bindParam(':item_code', $item_code);
				$sql_item_insert->bindParam(':item_name', $item_name);
				$sql_item_insert->bindParam(':group_id', $group_id);
				$sql_item_insert->execute();

			}

			$message .= "&#187; Variations updated ";

			break;
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button'   id='variationSaved' ><img src='/i/button/next_16.png'>Continue</button></div>";
	echo "<script type='text/javascript'>init();productID=$product_id</script>";

	die();
}

//Vars
switch ($variation_type) {
	case 'fixed':

		$template = file_get_contents('html/product_edit_variation_fixed.html');

		//Get current variations
		$statement = "SELECT variation_id, variation_code, variation_name, variation_description,
						variation_price, price_from_product
						FROM {$GLOBALS['db_prefix']}_products_variations
						WHERE product_id=:product_id";
		$sql_variations = $GLOBALS['dbCon']->prepare($statement);
		$sql_variations->bindParam(':product_id', $product_id);
		$sql_variations->execute();
		$sql_variations_data = $sql_variations->fetchAll();
		$sql_variations->closeCursor();

		if (empty($sql_variations_data)) {
			$variation_list .= "<tr id='trempty'><td colspan=5 style='text-align: center'>~ No Variations To Display ~</td></tr>";
		} else {
			foreach ($sql_variations_data as $variation_data) {
				$variation_list .= "<tr id='tr{$variation_data['variation_id']}'>";
				$variation_list .= "<td>{$variation_data['variation_code']}</td>";
				$variation_list .= "<td>{$variation_data['variation_name']}<br><span class='variationDesc'>{$variation_data['variation_description']}</span></td>";
				$variation_list .= $variation_data['price_from_product']=='Y'?'<td>FP</td>':"<td>{$variation_data['variation_price']}</td>";
				$variation_list .= "<td class='rightAlign'>";
				$variation_list .= "<input type='button' value='Remove' id='vr{$variation_data['variation_id']}' name='removeVariation' class='delete_button' >";
				$variation_list .= "<input type='button' value='Edit' id='ve{$variation_data['variation_id']}' name='editVariation' class='edit_button' >";
				$variation_list .= "</td>";
			}
		}

		//Replace Tags
		$template = str_replace('<!-- product_id -->', $_GET['product_id'], $template);
		$template = str_replace('<!-- variation_list -->', $variation_list, $template);

		break;
	case 'variable':

		//Vars
		$template = file_get_contents('html/product_edit_variation_variable.html');
		$group_list = '';
		$item_list = '';

		//Get Group List
		$statement = "SELECT group_id, group_name
						FROM {$GLOBALS['db_prefix']}_products_variations_groups
						WHERE product_id=:product_id";
		$sql_groups = $GLOBALS['dbCon']->prepare($statement);
		$sql_groups->bindParam(':product_id', $product_id);
		$sql_groups->execute();
		$sql_groups_data = $sql_groups->fetchAll();
		$sql_groups->closeCursor();

		//Assemble Groups
		foreach ($sql_groups_data as $group_data) {
			$group_list .= "{$group_data['group_id']}~~~{$group_data['group_name']}|||";
		}

		//Get variations list
		$statement = "SELECT group_id, item_code, item_name
					FROM {$GLOBALS['db_prefix']}_products_variations_variable
					WHERE product_id=:product_id";
		$sql_items = $GLOBALS['dbCon']->prepare($statement);
		$sql_items->bindParam(':product_id', $product_id);
		$sql_items->execute();
		$sql_items_data = $sql_items->fetchAll();
		$sql_items->closeCursor();

		//Assemble Items
		foreach ($sql_items_data as $item_data) {
			$item_list .= "{$item_data['item_code']}~~~{$item_data['item_name']}~~~{$item_data['group_id']}|||";
		}

		$template = str_replace('<!-- group_list -->', $group_list, $template);
		$template = str_replace('<!-- item_list -->', $item_list, $template);

		break;
}

echo $template;

?>
