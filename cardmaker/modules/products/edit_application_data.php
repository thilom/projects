<?php

/**
 * Edit/add product categories
 *
 * @author Thilo Muller(2011,2012)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/edit_application_data.html');
$application_name = '';
$message = '';
$application_id = isset($_GET['application_id'])?(int)$_GET['application_id']:0;
$version = isset($_GET['version'])?(int)$_GET['version']:0;

if (count($_POST) > 0) {

	//Vars
	$application_name = $_POST['applicationName'];
	$application_count = 0;

	//Check if category exists
	$statement = "SELECT COUNT(*) AS count
					FROM {$GLOBALS['db_prefix']}_products_applications
					WHERE application_name=:application_name";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bindParam(':application_name', $application_name);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();
	$application_count = $sql_count_data['count'];

	if ($application_count > 0) {
		$message = '&#187; Product application already exists';
	} else {
		if ($application_id > 0) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products_applications
						SET application_name = :application_name
						WHERE application_id=:application_id
						LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':application_name', $application_name);
			$sql_update->bindParam(':application_id', $application_id);
			$sql_update->execute();
			$sql_update->closeCursor();

			$message = '&#187; Product application updated';
		} else {

			$application_id = next_id("{$GLOBALS['db_prefix']}_products_applications", 'application_id');

			//Save Category
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_applications
								(application_id, application_name)
							VALUES
								(:application_id, :application_name)";
			$sql_insert = $GLOBALS['dbCon']->prepare($statement);
			$sql_insert->bindParam(':application_id', $application_id);
			$sql_insert->bindParam(':application_name', $application_name);
			$sql_insert->execute();
			$sql_insert->closeCursor();
			$message = "Application Created";

			$application_name = addslashes($application_name);

			$message = '&#187; Product application added';
		}
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	if ($version == 2) {
		echo "<div class='bMsg'><button type='button' id='applicationSavedMain'><img src='/i/button/next_16.png'>Continue</button></div>";
	} else {
		echo "<div class='bMsg'><button type='button' id='applicationSaved'><img src='/i/button/next_16.png'>Continue</button></div>";
	}

	echo "<script type='text/javascript'>init(); applicationID='$application_id';applicationName='$application_name';</script>";

	die();
}

if ($application_id > 0) {
	$statement = "SELECT application_name
				FROM {$GLOBALS['db_prefix']}_products_applications
				WHERE application_id=:application_id
				LIMIT 1";
	$sql_application = $GLOBALS['dbCon']->prepare($statement);
	$sql_application->bindParam(':application_id', $application_id);
	$sql_application->execute();
	$sql_application_data = $sql_application->fetch();
	$sql_application->closeCursor();
	$application_name = $sql_application_data['application_name'];
}

//Replace Tags
$template = str_replace('<!-- application_name -->', $application_name, $template);

echo $template;
?>
