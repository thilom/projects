<?php

/**
 * Edit/add products
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar.php';

//Vars
$template = file_get_contents('html/product_edit_data.html');
$product_name = '';
$product_code = '';
$product_model = '';
$product_price = '';
$product_description = '';
$product_manufacturer = '';
$manufacturer_list = '';
$category_list = '';
$application_list = '';
$product_categories = '';
$product_applications = '';
$product_weight = '';
$current_categories = array();
$current_applications = array();
$message = '';
$manufacturer_enabled = 'hiddenRow';
$applications_enabled = 'hiddenRow';
$nett_weight_enabled = 'hiddenRow';
$voucher = '';
$product_voucher = '';
$category1_list = '<option value=""> </option>';
$category2_list = '<option value=""> </option>';
$category3_list = '<option value=""> </option>';
$category_temp = array(0=>'', 1=>'', 2=>'');
$new_product = '';
$featured_product = '';
$archived_product = '';
$new_category = '';

//Show / Hide Manufacturer
$statement = "SELECT value
			FROM {$GLOBALS['db_prefix']}_settings
			WHERE tag='manufacturers_enabled'
			LIMIT 1";
$sql_manufacturer_enabled = $GLOBALS['dbCon']->prepare($statement);
$sql_manufacturer_enabled ->execute();
$sql_manufacturer_enabled_data = $sql_manufacturer_enabled->fetch();
$sql_manufacturer_enabled->closeCursor();
$manufacturer_enabled = $sql_manufacturer_enabled_data['value']=='Y'?'':'hiddenRow';

//Show / Hide Applications
$statement = "SELECT value
			FROM {$GLOBALS['db_prefix']}_settings
			WHERE tag='applications_enabled'
			LIMIT 1";
$sql_applications_enabled = $GLOBALS['dbCon']->prepare($statement);
$sql_applications_enabled ->execute();
$sql_applications_enabled_data = $sql_applications_enabled->fetch();
$sql_applications_enabled->closeCursor();
$applications_enabled = $sql_applications_enabled_data['value']=='Y'?'':'hiddenRow';

//Show / Hide Nett Weight
$statement = "SELECT tag, value
			FROM {$GLOBALS['db_prefix']}_settings
			WHERE tag='shipping_mode' OR tag='shipping_enabled'
			LIMIT 2";
$sql_shipping = $GLOBALS['dbCon']->prepare($statement);
$sql_shipping ->execute();
$sql_shipping_data = $sql_shipping->fetchAll();
$sql_shipping->closeCursor();
foreach ($sql_shipping_data as $shipping_data) {
	switch ($shipping_data['tag']) {
		case 'shipping_mode':
			$shipping_mode = $shipping_data['value'];
			break;
		case 'shipping_enabled':
			$shipping_enabled = $shipping_data['value'];
			break;
	}
}
if ($shipping_enabled == 'Y' && $shipping_mode == 'by_weight') {
	$nett_weight_enabled = '';
}


if (isset($_GET['product_id'])) {
	$product_id = $_GET['product_id'];

	$statement = "SELECT product_name, product_code, product_model, product_description, product_price,
							product_manufacturer, product_weight, product_voucher, archived_product, new_product, featured_product
					FROM {$GLOBALS['db_prefix']}_products
					WHERE product_id=:product_id
					LIMIT 1";
	$sql_product = $GLOBALS['dbCon']->prepare($statement);
	$sql_product->bindParam(':product_id', $product_id);
	$sql_product->execute();
	$sql_product_data = $sql_product->fetch();
	$sql_product->closeCursor();
	$product_name = $sql_product_data['product_name'];
	$product_code = $sql_product_data['product_code'];
	$product_model = $sql_product_data['product_model'];
	$product_price = $sql_product_data['product_price'];
	$product_description = $sql_product_data['product_description'];
	$product_manufacturer = $sql_product_data['product_manufacturer'];
	$product_weight = $sql_product_data['product_weight'];
	$product_voucher = $sql_product_data['product_voucher'];
	if ($sql_product_data['new_product'] == 'Y') $new_product = 'checked';
	if ($sql_product_data['featured_product'] == 'Y') $featured_product = 'checked';
	if ($sql_product_data['archived_product'] == 'Y') $archived_product = 'checked';

	//Get current category list
	$statement = "SELECT a.category_id, b.category_parent
					FROM {$GLOBALS['db_prefix']}_products_category_bridge AS a
					LEFT JOIN {$GLOBALS['db_prefix']}_products_categories AS b ON a.category_id=b.category_id
					WHERE a.product_id=:product_id
					LIMIT 1";
	$sql_product_categories = $GLOBALS['dbCon']->prepare($statement);
	$sql_product_categories->bindParam(':product_id', $product_id);
	$sql_product_categories->execute();
	$sql_product_categories_data = $sql_product_categories->fetch();
	$sql_product_categories->closeCursor();
	$category_temp[2] = $sql_product_categories_data['category_id'];
	$current_cat = $sql_product_categories_data['category_id'];
	if ($sql_product_categories_data['category_parent'] != 0) {
		$statement = "SELECT category_id, category_parent
						FROM {$GLOBALS['db_prefix']}_products_categories
						WHERE category_id=:category_id
						LIMIT 1";
		$sql_product_categories = $GLOBALS['dbCon']->prepare($statement);
		$sql_product_categories->bindParam(':category_id', $sql_product_categories_data['category_parent']);
		$sql_product_categories->execute();
		$sql_product_categories_data = $sql_product_categories->fetch();
		$sql_product_categories->closeCursor();
		$category_temp[1] = $sql_product_categories_data['category_id'];
			if ($sql_product_categories_data['category_parent'] != 0) {
			$statement = "SELECT category_id, category_parent
							FROM {$GLOBALS['db_prefix']}_products_categories
							WHERE category_id=:category_id
							LIMIT 1";
			$sql_product_categories = $GLOBALS['dbCon']->prepare($statement);
			$sql_product_categories->bindParam(':category_id', $sql_product_categories_data['category_parent']);
			$sql_product_categories->execute();
			$sql_product_categories_data = $sql_product_categories->fetch();
			$sql_product_categories->closeCursor();
			$category_temp[0] = $sql_product_categories_data['category_id'];
		}
	}

	//Arrange categories
	foreach ($category_temp as $key=>$value) {
		if (empty($value)) continue;
		$current_categories[] = $value;
	}


	//Get current application list
	$statement = "SELECT application_id
					FROM {$GLOBALS['db_prefix']}_products_application_bridge
					WHERE product_id=:product_id";
	$sql_product_applications = $GLOBALS['dbCon']->prepare($statement);
	$sql_product_applications->bindParam(':product_id', $product_id);
	$sql_product_applications->execute();
	$sql_product_applications_data = $sql_product_applications->fetchAll();
	$sql_product_applications->closeCursor();
	foreach ($sql_product_applications_data as $product_application) {
		$product_applications .= "{$product_application['application_id']},";
		$current_applications[] = $product_application['application_id'];
	}
}

if (count($_POST) > 0) {

	$post_archived = isset($_POST['archiveProduct'])?'Y':'';
	$post_new = isset($_POST['newProduct'])?'Y':'';
	$post_featured = isset($_POST['featuredProduct'])?'Y':'';

	if (isset($_GET['product_id'])) {
		$product_id = $_GET['product_id'];

		//Save product Name
		if ($_POST['productName'] != $product_name) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_name=:product_name
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_name', $_POST['productName']);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			$message .= "&#187; Product name changed from '$product_name' to '{$_POST['productName']}'.<br>";
			write_log("Product name changed from '$product_name' to {$_POST['productName']}",'products', $product_id);
		}

		//Save product Voucher
		$post_voucher = isset($_POST['productVoucher'])?'Y':'';
		if ($post_voucher != $product_voucher) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_voucher=:product_voucher
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_voucher', $post_voucher);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			$voucher_state = $post_voucher=='Y'?'enabled':'disabled';
			$message .= "&#187; Product voucher $voucher_state.<br>";
			write_log("Product voucher $voucher_state",'products', $product_id);
		}

		//Product Code
		if ($_POST['productCode'] != $product_code) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_code=:product_code
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_code', $_POST['productCode']);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			$message .= "&#187; Product code changed from '$product_code' to '{$_POST['productCode']}'.<br>";
			write_log("Product code changed from '$product_code' to {$_POST['productCode']}",'products', $product_id);
		}

		//Featured Product
		if ($post_featured != $sql_product_data['featured_product']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET featured_product=:featured_product
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':featured_product', $post_featured);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			if ($post_featured == 'Y') {
				$message .= "&#187; This is now a featured product.<br>";
				write_log("Product set as featured product",'products', $product_id);
			} else {
				$message .= "&#187; This is no longer a featured product.<br>";
				write_log("Product unset as featured product",'products', $product_id);
			}

		}

		//New Product
		if ($post_new != $sql_product_data['new_product']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET new_product=:new_product
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':new_product', $post_new);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			if ($post_new == 'Y') {
				$message .= "&#187; This is now a new product.<br>";
				write_log("Product set as new product",'products', $product_id);
			} else {
				$message .= "&#187; This is no longer a new product.<br>";
				write_log("Product unset as new product",'products', $product_id);
			}

		}

		//Archived Product
		if ($post_archived != $sql_product_data['archived_product']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET archived_product=:archived_product
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':archived_product', $post_archived);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			if ($post_archived == 'Y') {
				$message .= "&#187; Product archived.<br>";
				write_log("Product archived product",'products', $product_id);
			} else {
				$message .= "&#187; Product unarchived.<br>";
				write_log("Product unarchived",'products', $product_id);
			}

		}

		//Product Description
		if ($_POST['productDescription'] != $product_description) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_description=:product_description
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_description', $_POST['productDescription']);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			$message .= "&#187; Product description updated.<br>";
			write_log("Product description changed updated",'products', $product_id);
		}

		//Product Model
		if ($_POST['productModel'] != $product_model) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_model=:product_model
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_model', $_POST['productModel']);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			$message .= "&#187; Product model changed from '$product_model' to '{$_POST['productModel']}'.<br>";
			write_log("Product model changed from '$product_model' to {$_POST['productModel']}",'products', $product_id);
		}

		//Product Price
		if ($_POST['productPrice'] != $product_price) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_price=:product_price
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_price', $_POST['productPrice']);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			$message .= "&#187; Product price changed from '$product_price' to '{$_POST['productPrice']}'.<br>";
			write_log("Product price changed from '$product_price' to {$_POST['productPrice']}",'products', $product_id);
		}

		//Product Weight
		if ($_POST['productWeight'] != $product_weight) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_weight=:product_weight
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_weight', $_POST['productWeight']);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			$message .= "&#187; Product weight changed from '$product_weight grams' to '{$_POST['productWeight']} grams'.<br>";
			write_log("Product weight changed from '$product_weight grams' to {$_POST['productWeight']} grams",'products', $product_id);
		}

		//Product Manufacturer
		if (isset($_POST['manufacturer']) && $_POST['manufacturer'] != $product_manufacturer) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products
							SET product_manufacturer=:product_manufacturer
							WHERE product_id=:product_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':product_manufacturer', $_POST['manufacturer']);
			$sql_name_update->bindParam(':product_id', $product_id);
			$sql_name_update->execute();
			$sql_name_update->closeCursor();

			//Get old manufacturer name
			$statement = "SELECT manufacturer_name
							FROM {$GLOBALS['db_prefix']}_products_manufacturers
							WHERE manufacturer_id=:manufacturer_id
							LIMIT 1";
			$sql_manufacturer = $GLOBALS['dbCon']->prepare($statement);
			$sql_manufacturer->bindParam(':manufacturer_id', $product_manufacturer);
			$sql_manufacturer->execute();
			$sql_manufacturer_data = $sql_manufacturer->fetch();
			$sql_manufacturer->closeCursor();
			$old_name = $sql_manufacturer_data['manufacturer_name'];

			//Get new manufacturer name
			$statement = "SELECT manufacturer_name
							FROM {$GLOBALS['db_prefix']}_products_manufacturers
							WHERE manufacturer_id=:manufacturer_id
							LIMIT 1";
			$sql_manufacturer = $GLOBALS['dbCon']->prepare($statement);
			$sql_manufacturer->bindParam(':manufacturer_id', $_POST['manufacturer']);
			$sql_manufacturer->execute();
			$sql_manufacturer_data = $sql_manufacturer->fetch();
			$sql_manufacturer->closeCursor();
			$new_name = $sql_manufacturer_data['manufacturer_name'];

			$message .= "&#187; Product manufacturer changed from '$old_name' to '$new_name'.<br>";
			write_log("Product manufacturer changed from '$old_name' to $new_name",'products', $product_id);
		}

		//Prepare statement - New Category
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_category_bridge
							(product_id, category_id)
						VALUES
							(:product_id, :category_id)";
		$sql_category_insert = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - Remove Category
		$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_category_bridge
						WHERE category_id=:category_id AND product_id=:product_id
						LIMIT 1";
		$sql_category_delete = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - Category Name
		$statement = "SELECT category_name
						FROM {$GLOBALS['db_prefix']}_products_categories
						WHERE category_id=:category_id
						LIMIT 1";
		$sql_category_name = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - New Application
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_application_bridge
							(product_id, application_id)
						VALUES
							(:product_id, :application_id)";
		$sql_application_insert = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - Remove Application
		$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_application_bridge
						WHERE application_id=:application_id AND product_id=:product_id
						LIMIT 1";
		$sql_application_delete = $GLOBALS['dbCon']->prepare($statement);

		//Prepare statement - Application Name
		$statement = "SELECT application_name
						FROM {$GLOBALS['db_prefix']}_products_applications
						WHERE application_id=:application_id
						LIMIT 1";
		$sql_application_name = $GLOBALS['dbCon']->prepare($statement);

		//Applications
		$new_applications = explode(',', $_POST['application']);
		foreach ($new_applications as $new_application_id) {
			if (empty($new_application_id)) continue;
			if (!in_array($new_application_id, $current_applications)) { //New category
				$sql_application_insert->bindParam(':product_id', $product_id);
				$sql_application_insert->bindParam(':application_id', $new_application_id);
				$sql_application_insert->execute();

				//Old Category Name
				$sql_application_name->bindParam(':application_id', $new_application_id);
				$sql_application_name->execute();
				$sql_application_name_data = $sql_application_name->fetch();
				$application_name = $sql_application_name_data['application_name'];

				$message .= "&#187; Product added to '$application_name' application.<br>";
				write_log("Product added to '$application_name' application.",'products', $product_id);
			}
		}

		foreach ($current_applications AS $old_application_id) {
			if (!in_array($old_application_id, $new_applications)) { //Deleted category
				$sql_application_delete->bindParam(':product_id', $product_id);
				$sql_application_delete->bindParam(':application_id', $old_application_id);
				$sql_application_delete->execute();

				//Old Category Name
				$sql_application_name->bindParam(':application_id', $old_application_id);
				$sql_application_name->execute();
				$sql_application_name_data = $sql_application_name->fetch();
				$application_name = $sql_application_name_data['application_name'];

				$message .= "&#187; Product removed from '$application_name' application.<br>";
				write_log("Product removed from '$application_name' application.",'products', $product_id);
			}
		}

		//Insert Categories
		if (isset($_POST['category1']) && !empty($_POST['category1'])) $new_category = $_POST['category1'];
		if (isset($_POST['category2']) && !empty($_POST['category2'])) $new_category = $_POST['category2'];
		if (isset($_POST['category3']) && !empty($_POST['category3'])) $new_category = $_POST['category3'];
		if ($new_category != $current_cat) {
			if (empty($current_cat)) { //Insert New
				$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_category_bridge
									(product_id, category_id)
								VALUES
									(:product_id, :category_id)";
				$sql_insert = $GLOBALS['dbCon']->prepare($statement);
				$sql_insert->bindParam(':category_id', $new_category);
				$sql_insert->bindParam(':product_id', $product_id);
				$sql_insert->execute();
			} else { //UPDATE
				$statement = "UPDATE {$GLOBALS['db_prefix']}_products_category_bridge
								SET category_id=:category_id
								WHERE product_id=:product_id
								LIMIT 1";
				$sql_update = $GLOBALS['dbCon']->prepare($statement);
				$sql_update->bindParam(':category_id', $new_category);
				$sql_update->bindParam(':product_id', $product_id);
				$sql_update->execute();
			}

			$message .= "&#187; Product category updated.<br>";
			write_log("Product category updated",'products', $product_id);
		}

	} else {
		//Vars
		$product_name = $_POST['productName'];
		$product_code = $_POST['productCode'];
		$product_model = $_POST['productModel'];
		$product_manufacturer = isset($_POST['manufacturer'])?$_POST['manufacturer']:'';
		$product_price = $_POST['productPrice'];
		$product_description = $_POST['productDescription'];
		$product_categories = $_POST['category'];
		$product_applications = $_POST['application'];
		$product_voucher = isset($_POST['productVoucher'])?'Y':'';
                $post_archived = isset($_POST['archiveProduct'])?'Y':'';                
                $post_new = isset($_POST['newProduct'])?'Y':'';                
                $post_featured = isset($_POST['featuredProduct'])?'Y':'';                

		$product_id = next_id("{$GLOBALS['db_prefix']}_products", 'product_id');

		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products
							(product_id, product_name, product_model, product_description, product_price, product_code,
								product_manufacturer, product_voucher, archived_product, new_product, featured_product)
						VALUES
							(:product_id, :product_name, :product_model, :product_description, :product_price, :product_code,
								:product_manufacturer, :product_voucher, :archived_product, :new_product, :featured_product)";
		$sql_product_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_product_insert->bindParam(':product_id', $product_id);
		$sql_product_insert->bindParam(':product_name', $product_name);
		$sql_product_insert->bindParam(':product_model', $product_model);
		$sql_product_insert->bindParam(':product_description', $product_description);
		$sql_product_insert->bindParam(':product_price', $product_price);
		$sql_product_insert->bindParam(':product_code', $product_code);
		$sql_product_insert->bindParam(':product_manufacturer', $product_manufacturer);
		$sql_product_insert->bindParam(':product_voucher', $product_voucher);
		$sql_product_insert->bindParam(':archived_product', $post_archived);
		$sql_product_insert->bindParam(':new_product', $post_new);
		$sql_product_insert->bindParam(':featured_product', $post_featured);
		$sql_product_insert->execute();
		$sql_product_insert->closeCursor();

		//Save Categories
		$category_list = explode(',', $product_categories);
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_category_bridge
							(product_id, category_id)
						VALUES
							(:product_id, :category_id)";
		$sql_category_insert = $GLOBALS['dbCon']->prepare($statement);
		foreach ($category_list as $category_id) {
			if (empty($category_id)) continue;
			$sql_category_insert->bindParam(':product_id', $product_id);
			$sql_category_insert->bindParam(':category_id', $category_id);
			$sql_category_insert->execute();
		}
		$sql_category_insert->closeCursor();

		//Save Applications
		$application_list = explode(',', $product_applications);
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_application_bridge
							(product_id, application_id)
						VALUES
							(:product_id, :application_id)";
		$sql_application_insert = $GLOBALS['dbCon']->prepare($statement);
		foreach ($application_list as $application_id) {
			if (empty($application_id)) continue;
			$sql_application_insert->bindParam(':product_id', $product_id);
			$sql_application_insert->bindParam(':application_id', $application_id);
			$sql_application_insert->execute();
		}
		$sql_application_insert->closeCursor();

		$message = "&#187; Product Created";
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
		echo "<script src='/shared/common.js' type='text/javascript'></script>";
		echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
		echo "<br><div class='dMsg' width=200px>$message</div>";
		echo "<div class='bMsg'><button type='button' id='productSaved'><img src='/i/button/next_16.png'>Continue</button></div>";
		echo "<script type='text/javascript'>init();productID=$product_id</script>";


	die();
}

//Get list of manufacturers
$statement = "SELECT manufacturer_id, manufacturer_name
				FROM {$GLOBALS['db_prefix']}_products_manufacturers
				ORDER BY manufacturer_name";
$sql_manufacturer = $GLOBALS['dbCon']->prepare($statement);
$sql_manufacturer->execute();
$sql_manufacturer_data = $sql_manufacturer->fetchAll();
$sql_manufacturer->closeCursor();
foreach ($sql_manufacturer_data as $manufacturer_data) {
	if ($manufacturer_data['manufacturer_id'] == $product_manufacturer) {
		$manufacturer_list .= "<option value={$manufacturer_data['manufacturer_id']} selected >{$manufacturer_data['manufacturer_name']}</option>";
	} else {
		$manufacturer_list .= "<option value={$manufacturer_data['manufacturer_id']}>{$manufacturer_data['manufacturer_name']}</option>";
	}
}

//Get category 1 list
$statement = "SELECT category_id, category_name
				FROM {$GLOBALS['db_prefix']}_products_categories
				WHERE category_parent = 0 OR category_parent IS NULL
				ORDER BY category_name";
$sql_categories = $GLOBALS['dbCon']->prepare($statement);
$sql_categories->execute();
$sql_categories_data = $sql_categories->fetchAll();
$sql_categories->closeCursor();
foreach ($sql_categories_data as $categories_data) {
	if (isset($current_categories[0]) && $current_categories[0] == $categories_data['category_id']) {
		$category1_list .= "<option value={$categories_data['category_id']} selected >{$categories_data['category_name']}</option>";
	} else {
		$category1_list .= "<option value={$categories_data['category_id']}>{$categories_data['category_name']}</option>";
	}
}

//Get category 2 list
if (!empty($current_categories[0])) {
	$statement = "SELECT category_id, category_name
					FROM {$GLOBALS['db_prefix']}_products_categories
					WHERE category_parent = :category_parent
					ORDER BY category_name";
	$sql_categories = $GLOBALS['dbCon']->prepare($statement);
	$sql_categories->bindParam(':category_parent', $current_categories[0]);
	$sql_categories->execute();
	$sql_categories_data = $sql_categories->fetchAll();
	$sql_categories->closeCursor();
	foreach ($sql_categories_data as $categories_data) {
		if ($current_categories[1] == $categories_data['category_id']) {
			$category2_list .= "<option value={$categories_data['category_id']} selected >{$categories_data['category_name']}</option>";
		} else {
			$category2_list .= "<option value={$categories_data['category_id']}>{$categories_data['category_name']}</option>";
		}
	}
}

//Get category 3 list
if (!empty($current_categories[1])) {
	$statement = "SELECT category_id, category_name
					FROM {$GLOBALS['db_prefix']}_products_categories
					WHERE category_parent = :category_parent
					ORDER BY category_name";
	$sql_categories = $GLOBALS['dbCon']->prepare($statement);
	$sql_categories->bindParam(':category_parent', $current_categories[1]);
	$sql_categories->execute();
	$sql_categories_data = $sql_categories->fetchAll();
	$sql_categories->closeCursor();
	foreach ($sql_categories_data as $categories_data) {
		if ($current_categories[2] == $categories_data['category_id']) {
			$category3_list .= "<option value={$categories_data['category_id']} selected >{$categories_data['category_name']}</option>";
		} else {
			$category3_list .= "<option value={$categories_data['category_id']}>{$categories_data['category_name']}</option>";
		}
	}
}


//Get applications list
$statement = "SELECT application_id, application_name
				FROM {$GLOBALS['db_prefix']}_products_applications
				ORDER BY application_name";
$sql_applications = $GLOBALS['dbCon']->prepare($statement);
$sql_applications->execute();
$sql_applications_data = $sql_applications->fetchAll();
$sql_applications->closeCursor();
foreach ($sql_applications_data as $application_data) {
	$application_list .= "<option value={$application_data['application_id']}>{$application_data['application_name']}</option>";
}

if (is_module('vouchers')) {
	$product_voucher = $product_voucher=='Y'?'checked':'';
	$voucher = "<tr class=''>
				<td>Voucher</td>
				<td >
					<input type='checkbox' name='productVoucher' value='Y' $product_voucher > <span style='font-size:.7em'>Checking this box will generate a voucher to the value below</span>
				</td>
			   </tr>";
}

//Replace Tags
$template = str_replace('<!-- product_name -->', $product_name, $template);
$template = str_replace('<!-- product_code -->', $product_code, $template);
$template = str_replace('<!-- product_model -->', $product_model, $template);
$template = str_replace('<!-- product_price -->', $product_price, $template);
$template = str_replace('<!-- product_description -->', $product_description, $template);
$template = str_replace('<!-- manufacturer_list -->', $manufacturer_list, $template);
$template = str_replace('<!-- category_list -->', $category_list, $template);
$template = str_replace('<!-- application_list -->', $application_list, $template);
$template = str_replace('<!-- product_categories -->', $product_categories, $template);
$template = str_replace('<!-- product_applications -->', $product_applications, $template);
$template = str_replace('<!-- product_weight -->', $product_weight, $template);
$template = str_replace('<!-- manufacturer_enabled -->', $manufacturer_enabled, $template);
$template = str_replace('<!-- applications_enabled -->', $applications_enabled, $template);
$template = str_replace('<!-- nett_weight_enabled -->', $nett_weight_enabled, $template);
$template = str_replace('<!-- voucher -->', $voucher, $template);
$template = str_replace('<!-- category_list_1 -->', $category1_list, $template);
$template = str_replace('<!-- category_list_2 -->', $category2_list, $template);
$template = str_replace('<!-- category_list_3 -->', $category3_list, $template);
$template = str_replace('<!-- new_product -->', $new_product, $template);
$template = str_replace('<!-- archived_product -->', $archived_product, $template);
$template = str_replace('<!-- featured_product -->', $featured_product, $template);

echo $template;
?>
