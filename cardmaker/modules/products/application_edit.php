<?php

/**
 * Edit/Add product applications
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

if (!isset($_GET['page'])) {
	echo "<script>document.location='edit_application_data.php?W={$_GET['W']}";
	echo isset($_GET['new'])?"&new":'';
	echo isset($_GET['parent'])?"&parent={$_GET['parent']}":'';
	echo isset($_GET['application_id'])?"&application_id={$_GET['application_id']}":'';
	echo isset($_GET['version'])?"&version={$_GET['version']}":'';
	echo "'</script>";
} else {
	echo "<script>document.location='edit_application_data.php?W={$_GET['W']}";
	echo isset($_GET['application_id'])?"&application_id={$_GET['application_id']}":'';
	echo "'</script>";
}

?>
