<?php
/**
 *  Edit or add a product document
 *
 *  @author Thilo Muller(2012)
 *  @version $Id$
 */

//Includes
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';

//Vars
$doc_template = file_get_contents('html/edit_product_doc.html');
$doc_title = '';
$doc_description = '';
$doc_file = '';
$message = '';

//Get image data
if ($_GET['doc_id'] != 0) {
	$statement = "SELECT product_media_title, product_media_description, product_media_file
					FROM {$GLOBALS['db_prefix']}_products_media
					WHERE product_media_id=:product_media_id
					LIMIT 1";
	$sql_doc = $GLOBALS['dbCon']->prepare($statement);
	$sql_doc->bindParam("product_media_id", $_GET['doc_id']);
	$sql_doc->execute();
	$sql_doc_data = $sql_doc->fetch();
	$sql_doc->closeCursor();

	$doc_file = $sql_doc_data['product_media_file'];
	$doc_title = $sql_doc_data['product_media_title'];
	$doc_description = $sql_doc_data['product_media_description'];
}

//Save Image
if (COUNT($_POST) > 0) {

	//Vars
	$uploaded_doc_file = '';

	//Upload Document
	if (!empty($_FILES['docFile']['name']) && is_uploaded_file($_FILES['docFile']['tmp_name'])) {
		move_uploaded_file($_FILES['docFile']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/i/products/{$_FILES['docFile']['name']}");
		$uploaded_doc_file = $_FILES['docFile']['name'];
	}

	//Get current image Data
	if ($_GET['doc_id'] != 0) {

		if ($doc_file != $uploaded_doc_file && !empty($uploaded_doc_file)) {
			update_image_table("product_media_file", $uploaded_doc_file, $_GET['doc_id']);
			$message .= '&#187; New document uploaded<br>';
			write_log("New document uploaded",'products', $_GET['product_id']);
		}

		if ($doc_title != $_POST['doc_title']) {
			update_image_table("product_media_title", $_POST['doc_title'], $_GET['doc_id']);
			$message .= '&#187; Document title updated<br>';
			write_log("Document title updated",'products', $_GET['product_id']);
		}

		if ($doc_description != $_POST['doc_description']) {
			update_image_table("product_media_description", $_POST['doc_description'], $_GET['doc_id']);
			$message .= '&#187; Document description updated<br>';
			write_log("Document description updated",'products', $_GET['product_id']);
		}

	} else {
		$doc_id = next_id("{$GLOBALS['db_prefix']}_products_media", 'product_media_id');



		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_media
						 (product_id, product_media_id, product_media_title, product_media_description, product_media_file, product_media_type)
						VALUES
						 (:product_id, :product_media_id, :product_media_title, :product_media_description, :product_media_file, 'doc')";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_insert->bindParam(':product_id', $_GET['product_id']);
		$sql_insert->bindParam(':product_media_id', $doc_id);
		$sql_insert->bindParam(':product_media_title', $_POST['doc_title']);
		$sql_insert->bindParam(':product_media_description', $_POST['doc_description']);
		$sql_insert->bindParam(':product_media_file', $uploaded_doc_file);
		$sql_insert->execute();
		$sql_insert->closeCursor();

		$message = '&#187; New Document Uploaded';
		write_log("New Document Uploaded",'products', $_GET['product_id']);
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';

	echo "<script type='text/javascript' src='/shared/common.js'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/product_images.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button' onClick='openListWindow()'  ><img src='/i/button/next_16.png'>Continue</button></div>";


	die();
}




//Replace Tags
$doc_template = str_replace('<!-- doc_title -->', $doc_title, $doc_template);
$doc_template = str_replace('<!-- doc_description -->', $doc_description, $doc_template);
$doc_template = str_replace('<!-- doc_file -->', $doc_file, $doc_template);

echo $doc_template;

/**
 * Updates the ?_tour_images table
 *
 * @param string $field_name The name of the field to update
 * @param string $field_value The field value
 * @param int $image_id The ID of the image to update
 * @return bool Always TRUE
 */
function update_image_table($field_name, $field_value, $image_id) {
	$statement = "UPDATE {$GLOBALS['db_prefix']}_products_media
					SET $field_name=:field_value
					WHERE product_media_id=:product_media_id
					LIMIT 1";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bindParam(':field_value', $field_value);
	$sql_update->bindParam(':product_media_id', $image_id);
	$sql_update->execute();
	$sql_update->closeCursor();

	return true;
}
?>
