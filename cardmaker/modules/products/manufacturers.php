<?php

/**
 * Manage product manafacturers
 *
 * @author Thilo Muller(2012)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar_start.php';

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/manufacturers.html');

echo $template;

?>
