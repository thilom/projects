<?php

/**
 * List product to be associated with another product
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Vars
$template = file_get_contents('html/association_picker.html');

//Get initial list

echo $template;

?>
