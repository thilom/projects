<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar_start.php';

if (count($_POST)  > 0) {

	//Vars
	$color_name = $_POST['colorName'];
	$file_name = $_FILES['colorFile']['name'];

	//Upload file
	move_uploaded_file($_FILES['colorFile']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/i/image_variations/{$_FILES['colorFile']['name']}");

	//Save Color
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_image_variations
						(imgVariation_name, imgVariation_file)
					VALUES
						(:imgVariation_name, :imgVariation_file)";
	$sql_save = $GLOBALS['dbCon']->prepare($statement);
	$sql_save->bindParam(':imgVariation_name', $color_name);
	$sql_save->bindParam(':imgVariation_file', $file_name);
	$sql_save->execute();


	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>Color Saved</div>";
		echo "<div class='bMsg'><button type='button' onclick='document.location=\"/modules/products/color_editor.php?W={$_GET['W']}\"'><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/color_editor_new.html');

//Repalec tags
$template = str_replace('<!-- W -->', $_GET['W'], $template);

echo $template;

?>
