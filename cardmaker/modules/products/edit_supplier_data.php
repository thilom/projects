<?php

/**
 * Edit/add product categories
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/edit_supplier_data.html');
$supplier_name = '';
$message = '';
$category_id = isset($_GET['category_id'])?(int)$_GET['category_id']:0;

if (count($_POST) > 0) {

	//Vars
	$supplier_name = $_POST['supplierName'];
	$supplier_count = 0;

	//Check if category exists
	$statement = "SELECT COUNT(*) AS count
					FROM {$GLOBALS['db_prefix']}_products_suppliers
					WHERE supplier_name=:supplier_name";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bindParam(':supplier_name', $supplier_name);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();
	$supplier_count = $sql_count_data['count'];

	if ($supplier_count > 0) {
		$message = "&#187; Supplier already exists, cannot be created or updated.";
	} else {

		if (isset($category_id) && $category_id > 0)  {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products_categories
						SET category_name=:category_name
						WHERE category_id=:category_id
						LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':category_name', $category_name);
			$sql_update->bindParam(':category_id',$category_id);
			$sql_update->execute();
			$sql_update->closeCursor();

			$message = "&#187; Category updated";

		} else {

			$supplier_id = next_id("{$GLOBALS['db_prefix']}_products_suppliers", 'supplier_id');

			//Save Category
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_suppliers
								(supplier_id, supplier_name)
							VALUES
								(:supplier_id, :supplier_name)";
			$sql_insert = $GLOBALS['dbCon']->prepare($statement);
			$sql_insert->bindParam(':supplier_id', $supplier_id);
			$sql_insert->bindParam(':supplier_name', $supplier_name);
			$sql_insert->execute();
			$sql_insert->closeCursor();
			$message = "&#187; Supplier Created";

			$supplier_name = addslashes($supplier_name);
		}
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	if (isset($_GET['version']) && $_GET['version'] == 2) {
		echo "<div class='bMsg'><button type='button' id='supplierSavedMain'><img src='/i/button/next_16.png'>Continue</button></div>";
	} else {
		echo "<div class='bMsg'><button type='button' id='supplierSaved'><img src='/i/button/next_16.png'>Continue</button></div>";
	}

	echo "<script type='text/javascript'>init(); supplierID='$supplier_id';categoryName='$supplier_name';</script>";

	die();
}

if (isset($category_id) && $category_id > 0) {
	$statement = "SELECT category_name
				FROM {$GLOBALS['db_prefix']}_products_categories
				WHERE category_id=:category_id
				LIMIT 1";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);
	$sql_category->bindParam(':category_id', $category_id);
	$sql_category->execute();
	$sql_category_data = $sql_category->fetch();
	$sql_category->closeCursor();
	$category_name = $sql_category_data['category_name'];
}

//Replace Tags
$template = str_replace('<!-- supplier_name -->', $supplier_name, $template);

echo $template;
?>
