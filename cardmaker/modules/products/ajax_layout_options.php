<?php

/**
 * Get a list of layouts for use in the option list
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */


//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php';

//Vars
$layout_list = '';
$layout_type = $_GET['type'];
$product_template = '';

//Prepare statement - current layout
//Get current Prefs
$pref_data = get_area_data($_GET['area_id'], 'latest', array('preferences'));

//Expand Prefs
if (!empty($pref_data['data']['preferences'])) {
	$prefs = explode('|', $pref_data['data']['preferences']);

	foreach ($prefs as $pref) {
		if (empty($pref)) continue;
		list($var, $value) = explode('=', $pref);
		switch ($var) {
			case 'template':
				$product_template = $value;
				break;
		}
	}
}

//Image block
$image_block = "<img src='/modules/products/i/extension_cord_128.png' ><img src='/modules/products/i/toaster_128.png'>";

//Get list of templates
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$layout_type.php");
$layout_list .= "<layouts>";
foreach ($layout as $key=>$layout_values) {
	
	$current = $layout_values['id']==$product_template?1:0;
	
	//Get layout
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/{$layout_values['file_html']}");
	$template = str_replace('<!-- product_name -->', 'Product Name', $template);
	$template = str_replace('<!-- product_code -->', 'Code', $template);
	$template = str_replace('<!-- product_price -->', 'R0.00', $template);
	$template = str_replace('<!-- product_description -->', 'Description', $template);
	$template = str_replace('<!-- product_manufacturer -->', 'Manufacturer', $template);
	$template = str_replace('<!-- product_model -->', 'Model', $template);
	$template = str_replace('<!-- product_count -->', '00', $template);
	$template = str_replace('<!-- manufacturer_name -->', 'Manufacturer Name', $template);
	$template = str_replace('<!-- category_name -->', 'Category Name', $template);
	$template = str_replace('<!-- product_categories -->', 'Product, Categories', $template);
	$template = str_replace('<!-- product_information -->', 'Product Information', $template);
	$template = str_replace('<!-- variation_code -->', 'Variation Code', $template);
	$template = str_replace('<!-- variation_name -->', 'Variation Name', $template);
	$template = str_replace('<!-- cart_link -->', '', $template);
	$template = str_replace('<!-- product_image -->', '/modules/products/i/extension_cord_128.png', $template);
	$template = str_replace('<!-- primary_image -->', '/modules/products/i/dishwasher_128.png', $template);
	$template = str_replace('<!-- product_image_block -->', $image_block, $template);
	$template = str_replace(array("\n\r","\n","\r"), '', $template);
	$template = urlencode($template);
	
	$layout_list .= "<layout>";
	$layout_list .= "<id current='$current'>{$layout_values['id']}</id>";
	$layout_list .= "<name>{$layout_values['name']}</name>";
	$layout_list .= "<description>{$layout_values['description']}</description>";
	$layout_list .= "<fields>{$layout_values['fields']}</fields>";
	$layout_list .= "<template>$template</template>";
	$layout_list .= "<css>{$layout_values['file_css']}</css>";
	$layout_list .= "</layout>";
}
$layout_list .= "</layouts>";

echo $layout_list;

?>
