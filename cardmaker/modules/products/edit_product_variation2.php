<?php

/**
 * Product variations editor
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar.php';

//Vars
$product_id = $_GET['product_id'];
$message = '';
$color_list = '<table class="colorVariations">';
$current_color = array();
$template = file_get_contents('html/edit_product_variations2.html');

//Save Data
if (count($_POST) > 0) {

	//Clear current data
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_image_variations_bridge
					WHERE product_id=:product_id";
	$sql_clear = $GLOBALS['dbCon']->prepare($statement);
	$sql_clear->bindParam(':product_id', $product_id);
	$sql_clear->execute();

	//Save variations
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_image_variations_bridge
						(product_id, imgVariation_id)
					VALUES
						(:product_id, :imgVariation_id)";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);
	foreach ($_POST['image'] as $image_data) {
		$sql_insert->bindParam(':product_id', $product_id);
		$sql_insert->bindParam(':imgVariation_id', $image_data);
		$sql_insert->execute();
		$message = '&#187; Variations Added';
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button' onclick='document.location=\"/modules/products/edit_product_variation2.php?W={$_GET['W']}&product_id=$product_id\"' ><img src='/i/button/next_16.png'>Continue</button></div>";
	echo "<script type='text/javascript'>init();productID=$product_id</script>";

	die();
}

//Get current product color variations
$statement = "SELECT imgVariation_id
				FROM {$GLOBALS['db_prefix']}_products_image_variations_bridge
				WHERE product_id = :product_id";
$sql_current = $GLOBALS['dbCon']->prepare($statement);
$sql_current->bindParam(':product_id', $product_id);
$sql_current->execute();
$sql_current_data = $sql_current->fetchAll();
$sql_current->closeCursor();
foreach ($sql_current_data as $current_data) {
	$current_color[] = $current_data['imgVariation_id'];
}

//Get color variation list
$statement = "SELECT imgVariation_id, imgVariation_file, imgVariation_name
				FROM {$GLOBALS['db_prefix']}_products_image_variations
				ORDER BY imgVariation_name";
$sql_color = $GLOBALS['dbCon']->prepare($statement);
$sql_color->execute();
$sql_color_data = $sql_color->fetchAll();
$sql_color->closeCursor();

//Assemble color variations
$col_count = 0;
foreach ($sql_color_data as $color_data) {
	if ($col_count == 0) $color_list .= "<tr>";

	$checked = in_array($color_data['imgVariation_id'], $current_color)?'checked':'';
	$color_list .= "<td><img src='/i/image_variations/{$color_data['imgVariation_file']}' ><br>{$color_data['imgVariation_name']}<br><input type='checkbox' name='image[]' value='{$color_data['imgVariation_id']}' $checked ></td>";

	if ($col_count == 3) {
		$color_list .= "</tr>";
		$col_count = 0;
	} else {
		$col_count++;
	}
}
while ($col_count != 3) {
	if ($col_count == 0) break;
	$color_list .= "<td></td>";
	$col_count++;
	if ($col_count == 3) {
		$color_list .= "</tr>";
	}
}
$color_list .= "</table>";

//Replace tags
$template = str_replace('<!-- color_list -->', $color_list, $template);

echo $template;

?>
