<?php

/**
 * Get and display product results for Content manager
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Vars
$manufacturer_list = '';

//Get Preferences
$pref_data = get_area_data($id, 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);


//Get list of templates
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/product_manufac_list.php");
foreach ($layout as $key=>$layout_values) {
	if (isset($layout_values['id']) && $layout_values['id'] == $preferences['template']) {
		$file_html = $layout_values['file_html'];
		$file_css = $layout_values['file_css'];
	}
}
$product_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_html");

//Get CSS file
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_$id.css";
if (is_file($file)) {
	echo "<link rel=stylesheet href='/styles/css_$id.css'>";
} else {
	$css_data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_css");
	$css_data = str_replace('_suffix', "_$id", $css_data);
	
	echo "<style >
			$css_data;
			</style>";
}

//Get manufacturer list
$statement = "SELECT manufacturer_id, manufacturer_name
				FROM {$GLOBALS['db_prefix']}_products_manufacturers
				ORDER BY manufacturer_name";
$sql_manufacturer = $GLOBALS['dbCon']->prepare($statement);
$sql_manufacturer->execute();
$sql_manufacturer_data = $sql_manufacturer->fetchAll();
$sql_manufacturer->closeCursor();

//Prepare statement - Product Count
$statement = "SELECT COUNT(*) AS product_count
				FROM {$GLOBALS['db_prefix']}_products
				WHERE product_manufacturer=:product_manufacturer";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
				
//Draw Data
foreach ($sql_manufacturer_data as $manufacturer_data) {
	//Assemble link
	$link = "/index.php?page={$preferences['link_id']}&manID={$manufacturer_data['manufacturer_id']}";
	
	//Product count
	$sql_count->bindParam(':product_manufacturer', $manufacturer_data['manufacturer_id']);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	
	if (isset($preferences['hide_manufacturers']) && $preferences['hide_manufacturers'] == 1 && $sql_count_data['product_count'] == 0) {
		continue;
	}
	
	$new_template = str_replace('<!-- manufacturer_name -->', $manufacturer_data['manufacturer_name'], $product_template);
	$new_template = str_replace('<!-- product_count -->', $sql_count_data['product_count'], $new_template);
	$new_template = str_replace('<!-- product_link -->', $link, $new_template);
	$new_template = str_replace('_suffix', "_$id", $new_template);
	
	$manufacturer_list .= $new_template;
}

echo $manufacturer_list;

?>

