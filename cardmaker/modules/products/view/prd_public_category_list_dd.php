<?php

/**
 * Display a list of categories in menu dropdown format
 */

echo "<link href='/shared/drop-down-menu_v1.3/css/dropdown/dropdown.vertical.css' media='screen' rel='stylesheet' type='text/css' />";
echo "<link href='/styles/css_$id.css' media='screen' rel='stylesheet' type='text/css' />";
//echo "<link href='/shared/drop-down-menu_v1.3/dropdown/themes/_template/default.css' media='screen' rel='stylesheet' type='text/css' />";

//Vars
$category_list = '';

//Get Preferences
$pref_data = get_area_data($id, 'current', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);

//Get data
$statement = "SELECT category_id, category_name
			FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_parent = '0' OR category_parent IS NULL
			ORDER BY category_name";
$sql_categories = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) $sql_categories->bindParam(':filter_string', $filter_string);
$sql_categories->execute();
$sql_categories_data = $sql_categories->fetchAll();
$sql_categories->closeCursor();


//Prepare statement - product_count
$statement = "SELECT count(*) AS count
			FROM {$GLOBALS['db_prefix']}_products_category_bridge
			WHERE category_id = :category_id";
$sql_product = $GLOBALS['dbCon']->prepare($statement);

//Assemble Data
foreach ($sql_categories_data as $category_data) {

	//Vars
	$product_count = 0;

	//Product Count
	$sql_product->bindParam(':category_id', $category_data['category_id']);
	$sql_product->execute();
	$sql_product_data = $sql_product->fetch();
	$product_count = $sql_product_data['count'];

	$category_list .= "<ul id='nav' class='dropdown dropdown-vertical'>";
	$category_list .= "<li class='level1_suffix'><a href='/index.php?page={$preferences['link_id']}&catID={$category_data['category_id']}'>{$category_data['category_name']}</a>";

	//Get sub categories
	$statement = "SELECT category_id, category_name
			FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_parent = :category_parent
			ORDER BY category_name";
	$sql_subcat = $GLOBALS['dbCon']->prepare($statement);
	$sql_subcat->bindParam(':category_parent', $category_data['category_id']);
	$sql_subcat->execute();
	$sql_subcat_data = $sql_subcat->fetchAll();
	$sql_subcat->closeCursor();

	if (!empty($sql_subcat_data)) {
		$category_list .= "<ul class='dir' >";
		foreach ($sql_subcat_data as $subcat_data) {

			//Product Count
			$sql_product->bindParam(':category_id', $subcat_data['category_id']);
			$sql_product->execute();
			$sql_product_data = $sql_product->fetch();
			$product_count = $sql_product_data['count'];


			$category_list .= "<li><a href='/index.php?page={$preferences['link_id']}&catID={$subcat_data['category_id']}'>{$subcat_data['category_name']}</a>";

			//Get sub categories
			$statement = "SELECT b.product_name, a.product_id
					FROM {$GLOBALS['db_prefix']}_products_category_bridge AS a
					LEFT JOIN {$GLOBALS['db_prefix']}_products AS b ON a.product_id=b.product_id
					WHERE category_id=:category_id
					ORDER BY b.product_name";
			$sql_subcat2 = $GLOBALS['dbCon']->prepare($statement);
			$sql_subcat2->bindParam(':category_id', $subcat_data['category_id']);
			$sql_subcat2->execute();
			$sql_subcat2_data = $sql_subcat2->fetchAll();
			$sql_subcat2->closeCursor();

			if (!empty($sql_subcat2_data)) {
				$category_list .= "<ul >";
				foreach ($sql_subcat2_data as $subcat2_data) {

					//Product Count
					$sql_product->bindParam(':category_id', $subcat2_data['category_id']);
					$sql_product->execute();
					$sql_product_data = $sql_product->fetch();
					$product_count = $sql_product_data['count'];

					$category_list .= "<li><a href='/index.php?page=product_detail&product_id={$subcat2_data['product_id']}'>{$subcat2_data['product_name']}</a>";
					$category_list .= "</li>";

				}
				$category_list .= "</ul>";
			}

			$category_list .= "</li>";

		}
		$category_list .= "</ul>";
	}

	$category_list .= "</li></ul>";
}



echo $category_list;
?>
