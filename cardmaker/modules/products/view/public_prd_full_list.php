<?php

/**
 * Get and display product categories for Content manager
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Vars
$product_id = isset($_GET['product_id'])?$_GET['product_id']:'';
$category_list = '';
$variation_list = '';
$image_block = '';

//Get Preferences
$pref_data = get_area_data($id, 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);

//Get list of templates
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/product_full_list.php");
foreach ($layout as $key=>$layout_values) {
	if (isset($layout_values['id']) && $layout_values['id'] == $preferences['template']) {
		$file_html = $layout_values['file_html'];
		$file_css = $layout_values['file_css'];
	}
}
$product_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_html");

//Get CSS file
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_$id.css";
if (is_file($file)) {
	echo "<link rel=stylesheet href='/styles/css_$id.css'>";
} else {
	$css_data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_css");
	$css_data = str_replace('_suffix', "_$id", $css_data);

	echo "<style >
			$css_data;
			</style>";
}

//Get random product if the product ID does not exist
if (empty($product_id)) {
	$statement = "SELECT product_id
					FROM {$GLOBALS['db_prefix']}_products
					ORDER BY RAND()
					LIMIT 1";
	$sql_id = $GLOBALS['dbCon']->prepare($statement);
	$sql_id->execute();
	$sql_id_data = $sql_id->fetch();
	$sql_id->closeCursor();
	$product_id = $sql_id_data['product_id'];
}

//Get product information
$statement = "SELECT a.product_name, a.product_full, a.product_price, b.manufacturer_name
				FROM {$GLOBALS['db_prefix']}_products AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_products_manufacturers AS b ON a.product_manufacturer=b.manufacturer_id
				WHERE a.product_id=:product_id
				LIMIT 1";
$sql_product = $GLOBALS['dbCon']->prepare($statement);
$sql_product->bindParam(':product_id', $product_id);
$sql_product->execute();
$sql_product_data = $sql_product->fetch();
$sql_product->closeCursor();

//Assemble Categories
$statement = "SELECT b.category_id, b.category_name
				FROM {$GLOBALS['db_prefix']}_products_category_bridge AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_products_categories AS b ON a.category_id=b.category_id
				WHERE a.product_id=:product_id
				ORDER BY b.category_name";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->bindParam(':product_id', $product_id);
$sql_category->execute();
$sql_category_data = $sql_category->fetchAll();
$sql_category->closeCursor();
foreach ($sql_category_data as $category_data) {
	if (isset($preferences['category_link']) && !empty($preferences['category_link'])) {
		$link = "/index.php?page={$preferences['category_link']}&catID={$category_data['category_id']}";
		$category_list .= "<A href='$link'>";
	}
	$category_list .= $category_data['category_name'];
	if (isset($preferences['category_link']) && !empty($preferences['category_link'])) {
		$category_list .= "</A>";
	}
	$category_list .= ",";
}
if (!empty($category_list)) $category_list = substr($category_list, 0, -1);

/* Variable variations */

//Settings
$statement = "SELECT tag, value
			FROM {$GLOBALS['db_prefix']}_settings
			WHERE tag='variation_enabled' OR tag='variation_type'";
$sql_settings = $GLOBALS['dbCon']->prepare($statement);
$sql_settings->execute();
$sql_settings_data = $sql_settings->fetchAll();
$sql_settings->closeCursor();
foreach ($sql_settings_data as $setting_data) {
	switch ($setting_data['tag']) {
		case 'variation_enabled':
			$variation_enabled = $setting_data['value'];
			break;
		case 'variation_type':
			$variation_type = $setting_data['value'];
			break;
	}
}

if ($variation_enabled == 'Y' && $variation_type == 'variable') {

	//Get list of varation groups
	$statement = "SELECT group_id, group_name
				FROM {$GLOBALS['db_prefix']}_products_variations_groups
				WHERE product_id=:product_id";
	$sql_groups = $GLOBALS['dbCon']->prepare($statement);
	$sql_groups->bindParam('product_id', $product_id);
	$sql_groups->execute();
	$sql_groups_data = $sql_groups->fetchAll();
	$sql_groups->closeCursor();

	//Prepare statement - group items
	$statement = "SELECT item_name, item_code
				FROM {$GLOBALS['db_prefix']}_products_variations_variable
				WHERE product_id=:product_id AND group_id=:group_id";
	$sql_items = $GLOBALS['dbCon']->prepare($statement);

	//Assemble variations
	foreach ($sql_groups_data as $group_data) {
		$variation_name = str_replace(' ', '_', $group_data['group_name']);
		$variation_list .= "<span class='variation_block_suffix'><span class='variation_name_suffix'>{$group_data['group_name']} </span><SELECT class='variation_select_suffix' name='sci_$variation_name'>";

		$sql_items->bindParam(':product_id', $product_id);
		$sql_items->bindParam(':group_id', $group_data['group_id']);
		$sql_items->execute();
		$sql_items_data = $sql_items->fetchAll();
		foreach ($sql_items_data as $item_data) {
			$variation_list .= "<option value='{$item_data['item_code']}'>{$item_data['item_name']}</option>";
		}

		$variation_list .= "</SELECT></span>";
	}

}

if (isset($preferences['display_type']) && $preferences['display_type']=='lightbox_1') {
	$image_block .= "<script type='text/javascript' src='/shared/lightbox/js/prototype.js'></script>";
	$image_block .= "<script type='text/javascript' src='/shared/lightbox/js/scriptaculous.js?load=effects,builder'></script>";
	$image_block .= "<script type='text/javascript' src='/shared/lightbox/js/lightbox.js'></script>";
	$image_block .= "<link rel='stylesheet' href='/shared/lightbox/css/lightbox.css' >";
}

//Get primary image
$statement = "SELECT product_media_file
				FROM {$GLOBALS['db_prefix']}_products_media
				WHERE product_id=:product_id
				ORDER BY product_media_primary DESC
				LIMIT 1";
$sql_media = $GLOBALS['dbCon']->prepare($statement);
$sql_media->bindParam(':product_id', $product_id);
$sql_media->execute();
$sql_media_data = $sql_media->fetch();
$primary_image = "/i/products/{$sql_media_data['product_media_file']}";
$used_file = $sql_media_data['product_media_file'];
$sql_media->closeCursor();

//Image block
$statement = "SELECT product_media_file
				FROM {$GLOBALS['db_prefix']}_products_media
				WHERE product_id=:product_id
				ORDER BY product_media_primary DESC";
$sql_media = $GLOBALS['dbCon']->prepare($statement);
$sql_media->bindParam(':product_id', $product_id);
$sql_media->execute();
$sql_media_data = $sql_media->fetchAll();
$sql_media->closeCursor();
foreach ($sql_media_data as $media_data) {
//	if ($media_data['product_media_file'] != $used_file) {
		if (isset($preferences['display_type']) && $preferences['display_type']=='lightbox_1') {
			$image_block .= "<a href='/i/products/{$media_data['product_media_file']}' rel='lightbox[pil_img]'><img src='/i/products/{$media_data['product_media_file']}'></a>";
		} else {
			$image_block .= "<img src='/i/products/{$media_data['product_media_file']}'>";
		}

//	}

}

//Links
$products_list_link = "/index.php?page={$preferences['products_link']}";

$new_template = str_replace('<!-- product_name -->', $sql_product_data['product_name'], $product_template);
$new_template = str_replace('<!-- product_categories -->', $category_list, $new_template);
$new_template = str_replace('<!-- product_information -->', $sql_product_data['product_full'], $new_template);
$new_template = str_replace('<!-- product_price -->', $sql_product_data['product_price'], $new_template);
$new_template = str_replace('<!-- product_manufacturer -->', $sql_product_data['manufacturer_name'], $new_template);
$new_template = str_replace('<!-- cart_link -->', "addCart('$product_id',1,{$_SESSION['cart_position']},1)", $new_template);
$new_template = str_replace('<!-- variation_list -->', $variation_list, $new_template);
$new_template = str_replace('<!-- primary_image -->', $primary_image, $new_template);
$new_template = str_replace('<!-- product_image_block -->', $image_block, $new_template);
$new_template = str_replace('<!-- products_list_link -->', $products_list_link, $new_template);
$new_template = str_replace('_suffix', "_$id", $new_template);


echo $new_template;

?>

