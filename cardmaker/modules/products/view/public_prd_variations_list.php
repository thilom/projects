<?php

/**
 * Get and display product variations for Content manager (live)
 * 
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Vars
$variation_list = '';
$category_id = '';
$manufacturer_id = '';
$application_id = '';

//Get Preferences
$pref_data = get_area_data($id, 'current', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);


//Get list of templates
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/product_variation_list.php");
foreach ($layout as $key=>$layout_values) {
	if (isset($layout_values['id']) && $layout_values['id'] == $preferences['template']) {
		$file_html = $layout_values['file_html'];
		$file_css = $layout_values['file_css'];
	}
}
$product_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_html");

//Get CSS file
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_$id.css";
if (is_file($file)) {
	echo "<link rel=stylesheet href='/styles/css_$id.css'>";
} else {
	$css_data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_css");
	$css_data = str_replace('_suffix', "_$id", $css_data);
	
	echo "<style >
			$css_data;
			</style>";
}

//Get Data
$statement = "SELECT variation_code, variation_name, variation_description, variation_price, price_from_product
				FROM {$GLOBALS['db_prefix']}_products_variations
				WHERE product_id=:product_id";
$sql_variations = $GLOBALS['dbCon']->prepare($statement);
$sql_variations->bindParam(':product_id', $product_id);
$sql_variations->execute();
$sql_variations_data = $sql_variations->fetchAll();
$sql_variations->closeCursor();

//Prepare statement - Product price
$statement = "SELECT product_price
				FROM {$GLOBALS['db_prefix']}_products
				WHERE product_id=:product_id
				LIMIT 1";
$sql_price = $GLOBALS['dbCon']->prepare($statement);
				
//Draw Data
foreach ($sql_variations_data as $variation_data) {
	//Assemble Price
	$variation_price = '';
	if ($variation_data['price_from_product'] == 'Y') {
		$sql_price->bindParam(':product_id', $product_id);
		$sql_price->execute();
		$sql_price_data = $sql_price->fetch();
		$variation_price = $sql_price_data['product_price'];
	} else {
		$variation_price = $variation_data['variation_price'];
	}
	
		
	$new_template = str_replace('<!-- variation_name -->', $variation_data['variation_name'], $product_template);
	$new_template = str_replace('<!-- variation_code -->', $variation_data['variation_code'], $new_template);
	$new_template = str_replace('<!-- variation_description -->', $variation_data['variation_description'], $new_template);
	$new_template = str_replace('<!-- variation_price -->', $variation_price, $new_template);
	$new_template = str_replace('_suffix', "_$id", $new_template);
	
	$variation_list .= $new_template;
}

echo $variation_list;

?>

