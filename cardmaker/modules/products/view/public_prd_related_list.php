<?php

/**
 * Get and display related products results for Content manager
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Vars
$product_list = '';
$category_id = '';
$manufacturer_id = '';
$application_id = '';
$product_counter = 0;

//Get Preferences
$pref_data = get_area_data($id, 'current', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);


//Get list of templates
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/product_related_list.php");
foreach ($layout as $key=>$layout_values) {
	if (isset($layout_values['id']) && $layout_values['id'] == $preferences['template']) {
		$file_html = $layout_values['file_html'];
		$file_css = $layout_values['file_css'];
	}
}
$product_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_html");

//Get CSS file
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_$id.css";
if (is_file($file)) {
	echo "<link rel=stylesheet href='/styles/css_$id.css'>";
} else {
	$css_data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/$file_css");
	$css_data = str_replace('_suffix', "_$id", $css_data);

	echo "<style >
			$css_data;
			</style>";
}

//Get category ID from preferences
if (isset($preferences['product_category']) && !empty($preferences['product_category'])) {
	$category_id = $preferences['product_category'];
}

//Get application ID
if (isset($preferences['product_application']) && !empty($preferences['product_application'])) {
	$application_id = $preferences['product_application'];
}

//Get manufacturer ID
if (isset($preferences['product_manufacturer']) && !empty($preferences['product_manufacturer'])) {
	$manufacturer_id = $preferences['product_manufacturer'];
}

//Get Data
$statement = "SELECT DISTINCT a.product_name, a.product_code, a.product_description, a.product_price, b.manufacturer_name,
						a.product_model, a.product_id
				FROM {$GLOBALS['db_prefix']}_products_associations AS c
				LEFT JOIN {$GLOBALS['db_prefix']}_products AS a ON a.product_id=c.associated_product_id
				LEFT JOIN {$GLOBALS['db_prefix']}_products_manufacturers AS b ON a.product_manufacturer=b.manufacturer_id
				WHERE c.product_id=:product_id";
if (isset($preferences['max_products']) && !empty($preferences['max_products']) && $preferences['max_products'] > 0) {
	$statement .= " LIMIT {$preferences['max_products']}";
}
$sql_products = $GLOBALS['dbCon']->prepare($statement);
$sql_products->bindParam(':product_id', $product_id);
$sql_products->execute();
$sql_products_data = $sql_products->fetchAll();
$sql_products->closeCursor();

//Prepare statement - Product Image
$statement = "SELECT product_media_file
				FROM {$GLOBALS['db_prefix']}_products_media
				WHERE product_id=:product_id
				ORDER BY product_media_primary DESC
				LIMIT 1";
$sql_media = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Categories
$statement = "SELECT b.category_id, b.category_name
				FROM {$GLOBALS['db_prefix']}_products_category_bridge AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_products_categories AS b ON a.category_id=b.category_id
				WHERE product_id=:product_id
				ORDER BY b.category_name";
$sql_category = $GLOBALS['dbCon']->prepare($statement);

//Draw Data
if (isset($preferences['max_cols']) && $preferences['max_cols'] > 0) {
	$product_list .= "<table class='list_template_suffix' >";
}
foreach ($sql_products_data as $product_data) {
	//Assemble link
	$link = "/index.php?page={$preferences['link_id']}&product_id={$product_data['product_id']}";

	//Get & assemble image
	$sql_media->bindParam(':product_id', $product_data['product_id']);
	$sql_media->execute();
	$sql_media_data = $sql_media->fetch();
	$image_file = $sql_media_data['product_media_file'];
	$image = "<img src='/i/products/{$sql_media_data['product_media_file']}'>";

	//Get and assemble categories
	$category_list = '';
	$sql_category->bindParam(':product_id', $product_data['product_id']);
	$sql_category->execute();
	$sql_category_data = $sql_category->fetchAll();
	foreach ($sql_category_data as $category_data) {
		$category_list .= "<a href='/index.php?page=$page&catID={$category_data['category_id']}'>{$category_data['category_name']}</a>, ";
	}
	$category_list = substr($category_list, 0, -2);

	$new_template = str_replace('<!-- product_name -->', $product_data['product_name'], $product_template);
	$new_template = str_replace('<!-- product_code -->', $product_data['product_code'], $new_template);
	$new_template = str_replace('<!-- product_description -->', $product_data['product_description'], $new_template);
	$new_template = str_replace('<!-- product_manufacturer -->', $product_data['manufacturer_name'], $new_template);
	$new_template = str_replace('<!-- product_model -->', $product_data['product_model'], $new_template);
	$new_template = str_replace('<!-- product_price -->', $product_data['product_price'], $new_template);
	$new_template = str_replace('<!-- product_link -->', $link, $new_template);
	$new_template = str_replace('<!-- product_image -->', $image, $new_template);
	$new_template = str_replace('<!-- primary_image -->', "/i/products/$image_file", $new_template);
	$new_template = str_replace('<!-- product_categories -->', $category_list, $new_template);
	$new_template = str_replace('_suffix', "_$id", $new_template);

	if (isset($preferences['max_cols']) && $preferences['max_cols'] > 0) {
		if ($product_counter == 0) $product_list .= "<tr>";
		$product_list .= "<td>";
	}

	$product_list .= $new_template;

	$product_counter++;

	if (isset($preferences['max_cols']) && $preferences['max_cols'] > 0) {
		$product_list .= "</td>";
		if ($product_counter == $preferences['max_cols']) {
			$product_list .= "</tr>";
			$product_counter = 0;
		}
	}


}

if (isset($preferences['max_cols']) && $preferences['max_cols'] > 0) {
	$product_list .= "</table>";
}

echo $product_list;

?>

