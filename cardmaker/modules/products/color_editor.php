<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar_start.php';

if (isset($_GET['delete'])) {

	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_image_variations
					WHERE imgVariation_id = :imgVariation_id
					LIMIT 1";
	$sql_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete->bindParam(':imgVariation_id', $_GET['colorID']);
	$sql_delete->execute();
}

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/color_editor.html');
$color_list = '';

//Get list af colors
$statement = "SELECT imgVariation_id, imgVariation_file, imgVariation_name
				FROM {$GLOBALS['db_prefix']}_products_image_variations
				ORDER BY imgVariation_name";
$sql_color = $GLOBALS['dbCon']->prepare($statement);
$sql_color->execute();
$sql_color_data = $sql_color->fetchAll();
$sql_color->closeCursor();

//Assemble Colors
foreach ($sql_color_data as $color_data) {
	$color_list .= "<tr><td><img src='/i/image_variations/{$color_data['imgVariation_file']}' style='height: 50px;' ></td><td>{$color_data['imgVariation_name']}</td>";
	$color_list .= "<td style='text-align: right'>";
	$color_list .= "<button onclick='deleteColor({$color_data['imgVariation_id']})'>Delete</button>";
	$color_list .= "<button onclick='document.location=\"/modules/products/color_editor_edit.php?W={$_GET['W']}&colorID={$color_data['imgVariation_id']}\"'>Edit</button></td>";
	$color_list .= "</tr>";
}

//Replace Tags
$template = str_replace('<!-- color_list -->', $color_list, $template);
$template = str_replace('<!-- W -->', $_GET['W'], $template);

echo $template;

?>
