<?php

/**
 * Add or Edit a product variation
 * 
 * @author Thilo Muller (2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/variation_edit.html');
$variation_code = '';
$variation_name = '';
$variation_description = '';
$variation_price = '';
$price_from_product = '';


if (isset($_GET['variation_id'])) {
	if ($_GET['variation_id'] >= 800000) {
		
		//Find data in supplied string
		$variation_code = $_SESSION['variations'][$_GET['variation_id']]['code'];
		$variation_name = $_SESSION['variations'][$_GET['variation_id']]['name'];
		$variation_description = $_SESSION['variations'][$_GET['variation_id']]['description'];
		$variation_price = $_SESSION['variations'][$_GET['variation_id']]['price'];
		$price_from_product = $_SESSION['variations'][$_GET['variation_id']]['price_from_product']=='Y'?'checked':'';
		
		
	} else {
		
		//Does it exist in session
		if (isset($_SESSION['variations'][$_GET['variation_id']])) {
			$variation_code = $_SESSION['variations'][$_GET['variation_id']]['code'];
			$variation_name = $_SESSION['variations'][$_GET['variation_id']]['name'];
			$variation_description = $_SESSION['variations'][$_GET['variation_id']]['description'];
			$variation_price = $_SESSION['variations'][$_GET['variation_id']]['price'];
			$price_from_product = $_SESSION['variations'][$_GET['variation_id']]['price_from_product']=='Y'?'checked':'';
		} else {
			$statement = "SELECT variation_id, variation_code, variation_name, variation_description, 
							variation_price, price_from_product
							FROM {$GLOBALS['db_prefix']}_products_variations
							WHERE variation_id=:variation_id";
			$sql_variation = $GLOBALS['dbCon']->prepare($statement);
			$sql_variation->bindParam(':variation_id', $_GET['variation_id']);
			$sql_variation->execute();
			$sql_variation_data = $sql_variation->fetch();
			$sql_variation->closeCursor();
			
			$variation_code = $sql_variation_data['variation_code'];
			$variation_name = $sql_variation_data['variation_name'];
			$variation_description = $sql_variation_data['variation_description'];
			$variation_price = $sql_variation_data['variation_price'];
			$price_from_product = $sql_variation_data['price_from_product']=='Y'?'checked':'';
		}
	}
}

//Replace Tags
$template = str_replace('<!-- variation_code -->', $variation_code, $template);
$template = str_replace('<!-- variation_name -->', $variation_name, $template);
$template = str_replace('<!-- variation_description -->', $variation_description, $template);
$template = str_replace('<!-- variation_price -->', $variation_price, $template);
$template = str_replace('<!-- price_from_product -->', $price_from_product, $template);

echo $template;
?>
