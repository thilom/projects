<?php

/**
 * Delete a product
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once SITE_ROOT . '/modules/logs/log_functions.php';

//Vars
$product_id = intval($_GET['product_id']);


//Application Bridge
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_application_bridge
			WHERE product_id=:product_id";
$sql_application_bridge = $GLOBALS['dbCon']->prepare($statement);
$sql_application_bridge->bindParam(':product_id', $product_id);
$sql_application_bridge->execute();
$sql_application_bridge->closeCursor();

//Associations
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_associations
			WHERE product_id=:product_id OR associated_product_id=:product_id";
$sql_associations = $GLOBALS['dbCon']->prepare($statement);
$sql_associations->bindParam(':product_id', $product_id);
$sql_associations->execute();
$sql_associations->closeCursor();

//Category bridge
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_category_bridge
			WHERE product_id=:product_id";
$sql_category_bridge = $GLOBALS['dbCon']->prepare($statement);
$sql_category_bridge->bindParam(':product_id', $product_id);
$sql_category_bridge->execute();
$sql_category_bridge->closeCursor();

//Colors
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_colors
			WHERE product_id=:product_id";
$sql_colors = $GLOBALS['dbCon']->prepare($statement);
$sql_colors->bindParam(':product_id', $product_id);
$sql_colors->execute();
$sql_colors->closeCursor();

//Media
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_media
			WHERE product_id=:product_id";
$sql_media = $GLOBALS['dbCon']->prepare($statement);
$sql_media->bindParam(':product_id', $product_id);
$sql_media->execute();
$sql_media->closeCursor();

//Sizes
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_sizes
			WHERE product_id=:product_id";
$sql_sizes = $GLOBALS['dbCon']->prepare($statement);
$sql_sizes->bindParam(':product_id', $product_id);
$sql_sizes->execute();
$sql_sizes->closeCursor();

//Variations
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_variations
			WHERE product_id=:product_id";
$sql_variations = $GLOBALS['dbCon']->prepare($statement);
$sql_variations->bindParam(':product_id', $product_id);
$sql_variations->execute();
$sql_variations->closeCursor();

//Delete the product
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products
			WHERE product_id=:product_id
			LIMIT 1";
$sql_product = $GLOBALS['dbCon']->prepare($statement);
$sql_product->bindParam(':product_id', $product_id);
$sql_product->execute();
$sql_product->closeCursor();

write_log("Product deleted",'products', $product_id);

?>
