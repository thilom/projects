<?php


/**
 * Edit product yull description
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar.php';
//Vars
$article_template = file_get_contents('html/edit_product_full.html');
$product_content = '';
$message = '';

//Get WYSIWYG Settings
$statement = "SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='content_manager_wysiwyg'";
$sql_wysiwyg = $GLOBALS['dbCon']->prepare($statement);
$sql_wysiwyg->execute();
$wysiwyg_data = $sql_wysiwyg->fetch();
$sql_wysiwyg->closeCursor();

//Get current product data
$statement = "SELECT product_full
				FROM {$GLOBALS['db_prefix']}_products
				WHERE product_id=:product_id
				LIMIT 1";
$sql_full = $GLOBALS['dbCon']->prepare($statement);
$sql_full->bindParam(':product_id', $product_id);
$sql_full->execute();
$content_count = $sql_full->rowCount();
$sql_full_data = $sql_full->fetch();
$product_content = $sql_full_data['product_full'];

if (count($_POST) > 0) {

	if ($_POST['product_content'] != $product_content) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_products
						SET product_full=:product_full
						WHERE product_id=:product_id
						LIMIT 1";
		$sql_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_update->bindParam(':product_id', $product_id);
		$sql_update->bindParam(':product_full', $_POST['product_content']);
		$sql_update->execute();

		$message = "&#187; Product full description updated";
		write_log("Product full description updated",'products', $product_id);
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';

	echo "<script type='text/javascript' src='/modules/quote/js/quote.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button'  onClick='document.location=\"edit_product_full.php?W={$_GET['W']}&product_id={$_GET['product_id']}\"'><img src='/i/button/next_16.png'>Continue</button></div>";


	die();
}



//Replace Tags
$article_template = str_replace('!editor_features!', $wysiwyg_data['value'], $article_template);
$article_template = str_replace('<!-- product_content -->', $product_content, $article_template);
$article_template = str_replace('!W!', $_GET['W'], $article_template);

echo $article_template;

?>
