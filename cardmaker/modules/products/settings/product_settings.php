<?php
/**
 * General scrum settings
 */

//Includes
include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php');

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/settings/html/product_settings.html');
$type_select = '';
$variation_type = '';
$variation_enabled = 'Y';
$applications_enabled = '';
$manufacturers_enabled = '';
$variation_types = array('fixed'=>'Fixed Variation','variable'=>'Variable Variations');

//Get current Settings
$statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
$sql_ftp = $GLOBALS['dbCon']->prepare($statement);
$sql_ftp->execute();
$ftp_data = $sql_ftp->fetchAll();
$sql_ftp->closeCursor();
foreach ($ftp_data as $data) {
	switch ($data['tag']) {
		case 'variation_type':
			$variation_type = $data['value'];
			break;
		case 'variation_enabled':
			$variation_enabled = $data['value'];
			break;
		case 'applications_enabled':
			$applications_enabled = $data['value'];
			break;
		case 'manufacturers_enabled':
			$manufacturers_enabled = $data['value'];
			break;
	}
}

if (count($_POST) > 0) {
	$message = '';

	//Prepare statement - Update
	$statement = "UPDATE {$GLOBALS['db_prefix']}_settings SET value=:value WHERE tag=:tag";
	$sql_ftp_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Check
	$statement = "SELECT COUNT(*) AS c FROM {$GLOBALS['db_prefix']}_settings WHERE tag=:tag";
	$sql_ftp_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Insert
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings (tag, value) VALUES (:tag,:value)";
	$sql_ftp_insert = $GLOBALS['dbCon']->prepare($statement);

	//Save Variation Enabled
	if (!isset($_POST['variation_enabled'])) $_POST['variation_enabled'] = '';
	if ($_POST['variation_enabled'] != $variation_enabled) {
		$tag = 'variation_enabled';
		if ($_POST['variation_enabled'] == 'Y') {
			$indicator = 'enabled';
		} else {
			$indicator = 'disabled';
		}

		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['variation_enabled']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['variation_enabled']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}



		$message .= "&#187; Variations $indicator for products<br>";
		$log_message = "Variations $indicator for products'";
		write_log($log_message, 'products');

	}

	//Save variation type
	if ($_POST['variation_type'] != $variation_type) {
		$tag = 'variation_type';
		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['variation_type']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['variation_type']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$message .= "&#187; Variation type set to '{$variation_types[$_POST['variation_type']]}'<br>";
		$log_message = "Variation type set to {$variation_types[$_POST['variation_type']]}'";
		if (!empty($variation_type)) $log_message .= "(Previously: '{$variation_types[$_POST['variation_type']]}')";
		write_log($log_message, 'settings');

	}

	//Save Applications Enabled
	if (!isset($_POST['applications_enabled'])) $_POST['applications_enabled'] = '';
	if ($_POST['applications_enabled'] != $applications_enabled) {
		$tag = 'applications_enabled';
		if ($_POST['applications_enabled'] == 'Y') {
			$indicator = 'enabled';
		} else {
			$indicator = 'disabled';
		}

		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['applications_enabled']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['applications_enabled']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$message .= "&#187; Applications $indicator for products<br>";
		$log_message = "Applications $indicator for products'";
		write_log($log_message, 'products');
	}

	//Save Manufacturers Enabled
	if (!isset($_POST['manufacturers_enabled'])) $_POST['manufacturers_enabled'] = '';
	if ($_POST['manufacturers_enabled'] != $manufacturers_enabled) {
		$tag = 'manufacturers_enabled';
		if ($_POST['manufacturers_enabled'] == 'Y') {
			$indicator = 'enabled';
		} else {
			$indicator = 'disabled';
		}

		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['manufacturers_enabled']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['manufacturers_enabled']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$message .= "&#187; Manufacturers $indicator for products<br>";
		$log_message = "Manufacturers $indicator for products'";
		write_log($log_message, 'products');
	}

	if (empty($message)) $message = "&#187; No Changes, Nothing to update";
	echo "<br><br><div class='dMsg'>$message</div>";
	echo "<div class='bMsg'><button type='button' onClick='document.location=\"/modules/settings/settings.php?d=products/settings/product_settings.php\"' ><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}


//Assemble variations list
foreach ($variation_types as $type_tag=>$type_name) {
	$type_select .= "<option value='$type_tag'";
	$type_select .= $variation_type==$type_tag?' selected ':'';
	$type_select .= ">$type_name</option>";
}

//Enabled variations
$variation_enabled = $variation_enabled=='Y'?'checked':'';
$applications_enabled = $applications_enabled=='Y'?'checked':'';
$manufacturers_enabled = $manufacturers_enabled=='Y'?'checked':'';

//Replace Tags
$template = str_replace('<!-- type_select -->', $type_select, $template);
$template = str_replace('<!-- variation_enabled -->', $variation_enabled, $template);
$template = str_replace('<!-- applications_enabled -->', $applications_enabled, $template);
$template = str_replace('<!-- manufacturers_enabled -->', $manufacturers_enabled, $template);


echo $template;
?>

