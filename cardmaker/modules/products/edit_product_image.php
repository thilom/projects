<?php
/**
 *  Edit or add a product image
 *
 *  @author Thilo Muller(2011)
 *  @version $Id$
 */

//Includes
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';

//Vars
$image_template = file_get_contents('html/edit_product_image.html');
$image_title = '';
$image_description = '';
$image_file = '';
$primary_image = '';
$message = '';

//Save Image
if (COUNT($_POST) > 0) {

	//Vars
	$uploaded_image_file = substr($_POST['uploaded_image'], strrpos($_POST['uploaded_image'], '/')+1);
	$new_primary_image = $_POST['primary_image'];

	//Get current image Data
	if ($_GET['image_id'] != 0) {
		$statement = "SELECT product_media_title, product_media_description, product_media_file, product_media_primary
						FROM {$GLOBALS['db_prefix']}_products_media
						WHERE product_media_id=:product_media_id
						LIMIT 1";
		$sql_image = $GLOBALS['dbCon']->prepare($statement);
		$sql_image->bindParam("product_media_id", $_GET['image_id']);
		$sql_image->execute();
		$sql_image_data = $sql_image->fetch();
		$sql_image->closeCursor();

		$image_file = $sql_image_data['product_media_file'];
		$image_title = $sql_image_data['product_media_title'];
		$image_description = $sql_image_data['product_media_description'];
		$primary_image = $sql_image_data['product_media_primary'];

		if ($image_file != $uploaded_image_file) {
			update_image_table("product_media_file", $uploaded_image_file, $_GET['image_id']);
			$message .= '&#187; New image uploaded<br>';
			write_log("New image uploaded",'places', $_GET['product_id']);
		}

		if ($image_title != $_POST['image_title']) {
			update_image_table("product_media_title", $_POST['image_title'], $_GET['image_id']);
			$message .= '&#187; Image title updated<br>';
			write_log("Image title updated",'places', $_GET['product_id']);
		}

		if ($image_description != $_POST['image_description']) {
			update_image_table("product_media_description", $_POST['image_description'], $_GET['image_id']);
			$message .= '&#187; Image description updated<br>';
			write_log("Image description updated",'places', $_GET['product_id']);
		}

		if ($primary_image != $new_primary_image) {

			//Remove current primary image
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products_media
							SET product_media_primary=''
							WHERE product_id=:product_id AND product_media_primary='1'";
			$sql_clear_primary = $GLOBALS['dbCon']->prepare($statement);
			$sql_clear_primary->bindParam(':product_id', $_GET['product_id']);
			$sql_clear_primary->execute();
			$sql_clear_primary->closeCursor();

			//Update primary
			update_image_table("product_media_primary", $new_primary_image, $_GET['image_id']);

			//Check for a primary image
			$statement = "SELECT COUNT(*) as primary_count
							FROM {$GLOBALS['db_prefix']}_products_media
							WHERE product_id=:product_id AND product_media_primary='1'";
			$sql_primary = $GLOBALS['dbCon']->prepare($statement);
			$sql_primary->bindParam(':product_id', $_GET['product_id']);
			$sql_primary->execute();
			$sql_primary_data = $sql_primary->fetch();
			$sql_primary->closeCursor();

			if ($sql_primary_data['primary_count'] == 0) {
				$message .= '&#187; Primary image changed (NOTE: No primary image, Random mode set)<br>';
			} else {
				$message .= '&#187; Primary image changed<br>';
			}

			write_log("Primary image changed",'places', $_GET['product_id']);
		}

	} else {
		$image_id = next_id("{$GLOBALS['db_prefix']}_products_media", 'product_media_id');

		//Remove current primary image if the new image is set to primary
		if ($new_primary_image == '1') {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products_media
							SET product_media_primary=''
							WHERE product_id=:product_id AND product_media_primary='1'";
			$sql_clear_primary = $GLOBALS['dbCon']->prepare($statement);
			$sql_clear_primary->bindParam(':product_id', $_GET['product_id']);
			$sql_clear_primary->execute();
			$sql_clear_primary->closeCursor();
		}

		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_media
						 (product_id, product_media_id, product_media_title, product_media_description, product_media_file, product_media_primary)
						VALUES
						 (:product_id, :product_media_id, :product_media_title, :product_media_description, :product_media_file, :product_media_primary)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_insert->bindParam(':product_id', $_GET['product_id']);
		$sql_insert->bindParam(':product_media_id', $image_id);
		$sql_insert->bindParam(':product_media_title', $_POST['image_title']);
		$sql_insert->bindParam(':product_media_description', $_POST['image_description']);
		$sql_insert->bindParam(':product_media_file', $uploaded_image_file);
		$sql_insert->bindParam(':product_media_primary', $new_primary_image);
		$sql_insert->execute();
		$sql_insert->closeCursor();

		$message = '&#187; New Image Uploaded';
		write_log("New Image Uploaded",'products', $_GET['product_id']);
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';

	echo "<script type='text/javascript' src='/shared/common.js'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/product_images.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button' onClick='openListWindow()'  ><img src='/i/button/next_16.png'>Continue</button></div>";


	die();
}

//Get image data
if ($_GET['image_id'] != 0) {
	$statement = "SELECT product_media_title, product_media_description, product_media_file, product_media_primary
					FROM {$GLOBALS['db_prefix']}_products_media
					WHERE product_media_id=:product_media_id
					LIMIT 1";
	$sql_image = $GLOBALS['dbCon']->prepare($statement);
	$sql_image->bindParam("product_media_id", $_GET['image_id']);
	$sql_image->execute();
	$sql_image_data = $sql_image->fetch();
	$sql_image->closeCursor();

	$image_file = $sql_image_data['product_media_file'];
	$image_title = $sql_image_data['product_media_title'];
	$image_description = $sql_image_data['product_media_description'];
	$primary_image = $sql_image_data['product_media_primary']=='1'?'checked':'';
}

//Image File
if (!empty($image_file)) {
	$image_file = "/i/products/{$image_file}";
}



//Replace Tags
$image_template = str_replace('!server!', $_SERVER['HTTP_HOST'], $image_template);
$image_template = str_replace('!doc_root!', $_SERVER['DOCUMENT_ROOT'], $image_template);
$image_template = str_replace('<!-- image_title -->', $image_title, $image_template);
$image_template = str_replace('<!-- image_description -->', $image_description, $image_template);
$image_template = str_replace('<!-- image_file -->', $image_file, $image_template);
$image_template = str_replace('<!-- primary_image -->', $primary_image, $image_template);

echo $image_template;

/**
 * Updates the ?_tour_images table
 *
 * @param string $field_name The name of the field to update
 * @param string $field_value The field value
 * @param int $image_id The ID of the image to update
 * @return bool Always TRUE
 */
function update_image_table($field_name, $field_value, $image_id) {
	$statement = "UPDATE {$GLOBALS['db_prefix']}_products_media
					SET $field_name=:field_value
					WHERE product_media_id=:product_media_id
					LIMIT 1";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bindParam(':field_value', $field_value);
	$sql_update->bindParam(':product_media_id', $image_id);
	$sql_update->execute();
	$sql_update->closeCursor();

	return true;
}
?>
