<?php

/**
 * Product associations editor
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar.php';

//Vars
$product_id = $_GET['product_id'];
$template = file_get_contents('html/edit_product_association.html');
$association_list = '';
$id_list = '';
$current_associations = array();
$message = '';

//Get list af associations
$statement = "SELECT a.association_id, b.product_id, b.product_code, b.product_name
				FROM {$GLOBALS['db_prefix']}_products_associations AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_products AS b ON a.associated_product_id = b.product_id
				WHERE a.product_id=:product_id";
$sql_associations = $GLOBALS['dbCon']->prepare($statement);
$sql_associations->bindParam(':product_id', $product_id);
$sql_associations->execute();
$sql_associations_data = $sql_associations->fetchAll();

//Assemble html
foreach ($sql_associations_data as $association_data) {
	$association_list .= "<tr id='ar{$association_data['product_id']}'><td>{$association_data['product_code']}</td>";
	$association_list .= "<td>{$association_data['product_name']}</td>";
	$association_list .= "<td class='rightAlign'><button type='button' name='removeAssociation' id='sp{$association_data['product_id']}'><img src='/i/button/button_cancel_16'>Remove</button></td>";
	$association_list .= "</tr>";
	$id_list .= ",{$association_data['product_id']}";
	$current_associations[] = $association_data['product_id'];
}

//Save Associations
if (count($_POST) > 0) {

	//Vars
	$new_associations = explode(',', $_POST['assocciations']);

	//Prepare statement - Add association
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_associations
						(association_id, product_id, associated_product_id)
					VALUES
						(:association_id, :product_id, :associated_product_id)";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Remove Association
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_associations
					WHERE product_id=:product_id AND associated_product_id=:associated_product_id
					LIMIT 1";
	$sql_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Product Name
	$statement = "SELECT product_name
					FROM {$GLOBALS['db_prefix']}_products
					WHERE product_id=:product_id
					LIMIT 1";
	$sql_product = $GLOBALS['dbCon']->prepare($statement);

	foreach ($current_associations as $assoc_product_id) {
		if (!in_array($assoc_product_id, $new_associations)) {

			$sql_delete->bindParam(':product_id', $product_id);
			$sql_delete->bindParam(':associated_product_id', $assoc_product_id);
			$sql_delete->execute();

			$sql_product->bindParam(':product_id', $assoc_product_id);
			$sql_product->execute();
			$sql_product_data = $sql_product->fetch();
			$sql_product->closeCursor();
			$product_name = $sql_product_data['product_name'];

			$message .= "&#187; $product_name association removed from this product.<br>";
			write_log("$product_name association removed from this product",'products', $product_id);
		}
	}

	foreach ($new_associations as $assoc_product_id) {
		if (!in_array($assoc_product_id, $current_associations)) {
			if (empty($assoc_product_id)) continue;
			$product_name = '';
			$association_id = next_id("{$GLOBALS['db_prefix']}_products_associations", 'association_id');

			$sql_insert->bindParam(':association_id', $association_id);
			$sql_insert->bindParam(':product_id', $product_id);
			$sql_insert->bindParam(':associated_product_id', $assoc_product_id);
			$sql_insert->execute();

			$sql_product->bindParam(':product_id', $assoc_product_id);
			$sql_product->execute();
			$sql_product_data = $sql_product->fetch();
			$sql_product->closeCursor();
			$product_name = $sql_product_data['product_name'];

			$message .= "&#187; $product_name association added for this product.<br>";
			write_log("$product_name association added for this product",'products', $product_id);
		}
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button' id='associationSaved'><img src='/i/button/next_16.png'>Continue</button></div>";
	echo "<script type='text/javascript'>init();productID=$product_id</script>";

	die();

}

//Replace Tags
$template = str_replace('<!-- association_list -->', $association_list, $template);
$template = str_replace('<!-- id_list -->', $id_list, $template);

echo $template;
?>
