/*
 *  Functions for image management for products
 *
 *  @author Thilo Muller(2011)
 *  @version $Id$
 */


/**
 * event handle for document
 */
function eventHandle(e) {
    var eSrc;

	//Cross Browser
	if (window.event) {
		e = window.event;
		eSrc = e.srcElement;
	} else {
		eSrc = e.target;
	}

	//onClick
	if (e.type == 'click') {
		if (eSrc.name && eSrc.name.substr(0,10) == 'edit_image') {
			openEditWindow(eSrc.name);
		}
		if (eSrc.name && eSrc.name.substr(0,8) == 'edit_doc') {
			openEditDocWindow(eSrc.name);
		}
		if (eSrc.name && eSrc.name.substr(0,12) == 'delete_image') {
			deleteImage(eSrc.name);
		}
		if (eSrc.name && eSrc.name.substr(0,10) == 'delete_doc') {
			deleteDoc(eSrc.name);
		}
	}
}

/**
 * Opens the image edit window for new and current images
 */
function openEditWindow(bName) {
	W = getUrlValue('W');
	productID = getUrlValue('product_id');
    imageID = bName.substr(11);
	windowTitle = imageID=='0'?'Upload New Image':'Edit Image';
	newLocation = '/modules/products/edit_product_image.php?image_id=' + imageID;
	if (productID > 0) newLocation += "&product_id=" + productID;
	window.parent.W(windowTitle,newLocation,300,900);
	window.parent.WD(W);
}

/**
 * Opens the image list window and close current window
 */
function openListWindow() {
	W = getUrlValue('W');
	productID = getUrlValue('product_id');
	windowTitle = 'Edit Product';
	newLocation = '/modules/products/edit_product_media.php?product_id=' + productID;
	window.parent.W(windowTitle,newLocation,500,900);
	window.parent.WD(W);
}

/**
 * Delete an image
 */
function deleteImage(bName) {
    if (confirm("WARNING!\nThis will action will permanently delete the image and cannot be undone.\n\nContinue?")) {
		var W = getUrlValue('W');
		productID = getUrlValue('product_id');
		imageID = bName.substr(13);

		document.location = "/modules/products/edit_product_media.php?product_id="+ productID +"&W="+ W +"&delete_image="+imageID;
	}
}

/**
 * Copies form data to a sendable form
 */
function copy_data() {
	document.getElementById('uploaded_image').value = document.getElementById('upImage').src;
    document.getElementById('image_title').value = document.getElementById('image_title_tmp').value;
    document.getElementById('image_description').value = document.getElementById('image_description_tmp').value;
    document.getElementById('primary_image').value = document.getElementById('primary_image_tmp').checked?'1':'';

}


/**
 * Opens the doc edit window for new and current images
 */
function openEditDocWindow(bName) {
	W = getUrlValue('W');
	productID = getUrlValue('product_id');
    docID = bName.substr(9);
	windowTitle = docID=='0'?'Upload New Document':'Edit Document';
	newLocation = '/modules/products/edit_product_doc.php?doc_id=' + docID;
	if (productID > 0) newLocation += "&product_id=" + productID;
	window.parent.W(windowTitle,newLocation,300,900);
	window.parent.WD(W);
}

/**
 * Delete an image
 */
function deleteDoc(bName) {
    if (confirm("WARNING!\nThis will action will permanently delete the document and cannot be undone.\n\nContinue?")) {
		var W = getUrlValue('W');
		productID = getUrlValue('product_id');
		docID = bName.substr(11);

		document.location = "/modules/products/edit_product_media.php?product_id="+ productID +"&W="+ W +"&delete_doc="+docID;
	}
}


document.onclick = eventHandle;
