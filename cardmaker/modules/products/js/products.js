var c;
var productListB = new Array();

/**
 * Initialize page
 */
function init() {
	document.onclick = handleEvent;
}

/**
 * Event Handler
 */
function handleEvent(e) {
	var eSrc;
	if (window.event) {
		e = window.event;
		eSrc = e.srcElement;
	} else {
		eSrc = e.target;
	}


	if (e.type == 'click') {
		switch (eSrc.id) {
			case 'addProduct':
				openProductWindow();
				break;
			case 'addManufacturer':
				openEditManufacturerWindow(0);
				break;
			case 'addCategory':
				addCategory();
				break;
			case 'addApplication':
				addApplication();
				break;
			case 'newCategory':
				openCategoryWindow(0);
				break;
			case 'newCategoryMain':
				openCategoryWindow(0,2);
				break;
			case 'newApplication':
				openApplicationWindow();
				break;
			case 'newApplicationMain':
				openApplicationWindow(0,2);
				break;
			case 'newManufacturer':
				openManufacturerWindow();
				break;
			case 'categorySaved':
				closeCategoryWindow();
				break;
			case 'categorySavedMain':
				closeCategoryWindowMain();
				break;
			case 'applicationSaved':
				closeApplicationWindow();
				break;
			case 'applicationSavedMain':
				closeApplicationWindowMain();
				break;
			case 'applicationMain':
				closeApplicationWindowMain();
				break;
			case 'productSaved':
				productSaved();
				break;
			case 'variationSaved':
				variationSaved();
				break;
			case 'manufacturerSaved':
				closeManufacturerWindow(1);
				break;
			case 'manufacturerSavedMain':
				closeManufacturerWindow(2);
				break;
			case 'closeWindow':
				closeWindow();
				break;
			case 'addVariation':
				openVariationWindow();
				break;
			case 'addGroup':
				openGroupWindow();
				break;
			case 'insertVariationB':
				insertVariationB();
				break;
			case 'insertVariation':
				insertVariation();
				break;
			case 'insertGroup':
				insertGroup();
				break;
			case 'importGroup':
				importGroupWindow();
				break;
			case 'addProductAssociation':
				openAssociationWindow();
				break;
			case 'filterProducts':
				getAssociatedProducts(0);
				break;
			case 'associationSaved':
				associationSaved();
				break;
			case 'applyGroups':
				applyGroups();
				break;
		}

		switch (eSrc.name) {
			case 'removeCategory':
				removeCategory(eSrc.id);
				break;
			case 'removeApplication':
				removeApplication(eSrc.id);
				break;
			case 'editmanufacturer':
				manufacturerID = eSrc.id.substr(1);
				openEditManufacturerWindow(manufacturerID);
				break;
			case 'editcategory':
				categoryID = eSrc.id.substr(1);
				openCategoryWindow(categoryID,2);
				break;
			case 'editApplication':
				applicationID = eSrc.id.substr(1);
				openApplicationWindow(applicationID,2);
				break;
			case 'deletemanufacturer':
				manufacturerID = eSrc.id.substr(1);
				deleteManufacturer(manufacturerID);
				break;
			case 'deletecategory':
				manufacturerID = eSrc.id.substr(1);
				deletecategory(manufacturerID);
				break;
			case 'deleteApplication':
				applicationID = eSrc.id.substr(1);
				deleteApplication(applicationID);
				break;
			case 'addItem':
				groupID = eSrc.id.substr(0, eSrc.id.indexOf('_'));
				itemID = eSrc.id.substr(eSrc.id.indexOf('_')+1);
				openItemWindow(groupID,itemID);
				break;
			case 'deleteItem':
				groupID = eSrc.id.substr(0, eSrc.id.indexOf('_'));
				itemID = eSrc.id.substr(eSrc.id.indexOf('_')+1);
				removeVariationB(groupID,itemID);
				break;
			case 'firstPage':
				nextID = eSrc.id.substr(0,1);
				if (nextID == 'n') {
					getProducts(0);
				} else if (nextID == 'm') {
					getManufacturers(0);
				} else if (nextID == 'c') {
					getCategories(0);
				} else if (nextID == 'a') {
					getApplications(0);
				} else {
					getAssociatedProducts(0);
				}
				break;
			case'previousPage':
				nextID = eSrc.id.substr(0,1);
				next = eSrc.id.substr(1);
				next = (next*1);
				if (nextID == 'n') {
					getProducts(next);
				} else if (nextID == 'm') {
					getManufacturers(next);
				} else if (nextID == 'c') {
					getCategories(next);
				} else if (nextID == 'a') {
					getApplications(next);
				} else {
					getAssociatedProducts(next);
				}
				break;
			case'nextPage':
				nextID = eSrc.id.substr(0,1);
				next = eSrc.id.substr(1);
				next = (next*1);
				if (nextID == 'n') {
					getProducts(next);
				} else if (nextID == 'm') {
					getManufacturers(next);
				} else if (nextID == 'c') {
					getCategories(next);
				} else if (nextID == 'a') {
					getApplications(next);
				} else {
					getAssociatedProducts(next);
				}
				break;
			case 'lastPage':
				nextID = eSrc.id.substr(0,1);
				next = eSrc.id.substr(1);
				next = next*10;
				if (nextID == 'n') {
					getProducts(next);
				} else if (nextID == 'm') {
					getManufacturers(next);
				} else if (nextID == 'c') {
					getCategories(next);
				} else if (nextID == 'a') {
					getApplications(next);
				} else {
					getAssociatedProducts(next);
				}
				break;
			case 'filterMatch':
				getProducts();
				break;
			case 'filterManufacturer':
				getManufacturers();
				break;
			case 'filterCategory':
				getCategories();
				break;
			case 'filterApplication':
				getApplications();
				break;
			case 'editProduct':
				openProductWindow(eSrc.id.substr(1));
				break;
			case 'deleteProduct':
				deleteProduct(eSrc.id.substr(1));
				break;
			case 'editVariation':
				variationID = eSrc.id.substr(2)
				openVariationWindow(variationID);
				break;
			case 'removeVariation':
				variationID = eSrc.id.substr(2);
				removeVariation(variationID);
				break;
			case 'removeGroup':
				groupID = eSrc.id;
				removeGroup(groupID);
				break;
			case 'selectProduct':
				id = eSrc.id.substr(2);
				addAssociatedProduct(id);
				break;
			case 'removeAssociation':
				id = eSrc.id.substr(2);
				removeAssociatedProduct(id);
				break;
		}

	}
}

/**
 * Opens the new/edit product window
 */
function openProductWindow(id) {
	title = "New Product";
	W = getUrlValue('W');
	url = '/modules/products/product_edit.php';
	if (id) {
		url += "?product_id=" + id;
		url += "&parent=" + W;
		title = 'Edit Product';
	} else {
		url += "?parent=" + W;
	}

	window.parent.W(title,url,500,900);
}

/**
 * Add a category to the list
 */
function addCategory(catVal) {
	var dup = false;
	var cat = document.getElementById('productCategory');
	var tbl = document.getElementById('categoryTable');
	var currentCategories = document.getElementById('category');
	if (!catVal) catVal = cat.value;

	for (i = 0; i < cat.options.length; i++) {
		if (cat.options[i].value == catVal) {
			catName = cat.options[i].text;
		}
	}

	//Check for duplicates
	var catList = currentCategories.value.split(',');
	for (i in catList) {
		if (catVal == catList[i]) {
			dup = true;
		}
	}

	if (!dup) {
		TR = document.createElement('TR');
		TR.setAttribute('id', 'r'+catVal);

		TD1 = document.createElement('TD');
		TD2 = document.createElement('TD');
		TD2.setAttribute('class', 'rightAlign');

		Text = document.createTextNode(catName);
		TD1.appendChild(Text);

		button = document.createElement('BUTTON');
		button.setAttribute('type', 'button');
//		button.setAttribute('value', 'Remove');
//		button.setAttribute('class', 'delete_button');
		button.setAttribute('name', 'removeCategory');
		button.setAttribute('id', 'p'+catVal);
		buttonText = document.createTextNode('Remove');
		buttonImage = document.createElement('IMG');
		buttonImage.setAttribute('src', '/i/button/button_cancel_16.png');
		button.appendChild(buttonImage);
		button.appendChild(buttonText);
		TD2.appendChild(button);

		TR.appendChild(TD1);
		TR.appendChild(TD2);
		tbl.appendChild(TR);

		currentCategories.value = currentCategories.value + ',' + catVal;
	}


}

/**
 * Initialize Category List
 */
function initCategory() {
	categoryList = document.getElementById('categoryA');

	catList1 = categoryList.value.split(',');
	for (j=0; j<catList1.length; j++) {
		if (catList1[j] == '') continue;
		addCategory(catList1[j]);
	}
}

/**
 * Initialize Application List
 */
function initApplication() {
	applicationList = document.getElementById('applicationA');

	appList1 = applicationList.value.split(',');
	for (z=0; z<appList1.length; z++) {
		if (appList1[z] == '') continue;
		addApplication(appList1[z]);
	}
}

/**
 * Add an Application to the list
 */
function addApplication(appVal) {
	var dup = false;
	app = document.getElementById('productApplications');
	var tbl = document.getElementById('applicationTable');
	currentApplications = document.getElementById('application');
	if (!appVal) appVal = app.value;

	for (i = 0; i < app.options.length; i++) {
		if (app.options[i].value == appVal) {
			appName = app.options[i].text;
		}
	}

	//Check for duplicates
	appList = currentApplications.value.split(',');
	for (i in appList) {
		if (appVal == appList[i]) {
			dup = true;
		}
	}

	if (!dup) {
		TR = document.createElement('TR');
		TR.setAttribute('id', 'ra'+appVal);

		TD1 = document.createElement('TD');
		TD2 = document.createElement('TD');
		TD2.setAttribute('class', 'rightAlign');

		Text = document.createTextNode(appName);
		TD1.appendChild(Text);

		button = document.createElement('BUTTON');
		button.setAttribute('type', 'button');
//		button.setAttribute('value', 'Remove');
//		button.setAttribute('class', 'delete_button');
		button.setAttribute('name', 'removeApplication');
		button.setAttribute('id', 'pa'+appVal);
		buttonText = document.createTextNode('Remove');
		buttonImage = document.createElement('IMG');
		buttonImage.setAttribute('src', '/i/button/button_cancel_16.png');
		button.appendChild(buttonImage);
		button.appendChild(buttonText);
		TD2.appendChild(button);

		TR.appendChild(TD1);
		TR.appendChild(TD2);
		tbl.appendChild(TR);

		currentApplications.value = currentApplications.value + ',' + appVal;
	}
}

/**
 * Remove a category
 */
function removeCategory(id) {
	id = id.substr(1);
	var newList = '';
	var tbl = document.getElementById('categoryTable');
	var currentCategory = document.getElementById('category');
	var rmCat = document.getElementById('r'+id);

	//Remove from category field
	cats = currentCategory.value.split(',');
	for (i in cats) {
		if (cats[i] == '') continue;
		if (cats[i] != id) {
			newList += newList==''?cats[i]:','+cats[i];
		}
	}

	tbl.removeChild(rmCat);

	currentCategory.value = newList;

}

/**
 * Remove a Application
 */
function removeApplication(id) {
	id = id.substr(2);
	var newList = '';
	var tbl = document.getElementById('applicationTable');
	var currentApplication = document.getElementById('application');
	var rmApp = document.getElementById('ra'+id);

	//Remove from category field
	apps = currentApplication.value.split(',');
	for (i in apps) {
		if (apps[i] == '') continue;
		if (apps[i] != id) {
			newList += newList==''?apps[i]:','+apps[i];
		}
	}

	tbl.removeChild(rmApp);

	currentApplication.value = newList;

}

/**
 * Opens a category window to add a new category
 */
function openCategoryWindow(id,version) {
	title = id?"Edit Category":"New Category";
	W = getUrlValue('W');
	url = '/modules/products/category_edit.php?parent=' + W + '&category_id=' + id;
	if (version) url += '&version=' + version;
	window.parent.W(title,url,150,400);
}

/**
 * Opens a Application window to add a new application
 */
function openApplicationWindow(applicationID, version) {
	title = applicationID?'Edit Application':"New Application";
	W = getUrlValue('W');
	url = '/modules/products/application_edit.php?new&parent=' + W + '&application_id=' + applicationID + '&version=' + version;
	window.parent.W(title,url,150,400);
}

/**
 * Opens a manufacturer window to add a new manufacturer
 */
function openManufacturerWindow() {
	title = "New Manufacturer";
	W = getUrlValue('W');
	url = '/modules/products/manufacturer_edit.php?new&parent=' + W;
	window.parent.W(title,url,150,400);
}

/**
 * Close new category window, add category to list
 */
function closeCategoryWindow() {
	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'addCategoryFromNew', new Array(categoryID,categoryName));
	window.parent.WD(getUrlValue('W')); //Close Window
}

/**
 * Close new category window
 */
function closeCategoryWindowMain() {
	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'getCategories', new Array());
	window.parent.WD(getUrlValue('W')); //Close Window
}

/**
 * Updated the category list after adding a new category
 */
function addCategoryFromNew(data) {
	categoryID = data[0];
	categoryName = data[1];

	slt = document.getElementById('productCategory');
	var opt = document.createElement('option');
	opt.text = categoryName;
	opt.value = categoryID;

	try {
		slt.add(opt, null); // standards compliant;
	} catch(ex) {
		slt.add(opt); // IE only
	}

	addCategory(categoryID);
}

/**
 * Close new application window, add application to list
 */
function closeApplicationWindow() {
	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'addApplicationFromNew', new Array(applicationID,applicationName));
	window.parent.WD(getUrlValue('W')); //Close Window
}

/**
 * Updated the application list after adding a new application
 */
function addApplicationFromNew(data) {
	applicationID = data[0];
	applicationName = data[1];

	slt = document.getElementById('productApplications');
	var opt = document.createElement('option');
	opt.text = applicationName;
	opt.value = applicationID;

	try {
		slt.add(opt, null); // standards compliant;
	} catch(ex) {
		slt.add(opt); // IE only
	}

	addApplication(applicationID);
}

/**
 * Close new manufacturer window, add manufacturer to list
 */
function closeManufacturerWindow(version) {
	parentID = getUrlValue('parent');
	if (version == 2) {
		parent.runWindowFunction(parentID, 'getManufacturers', new Array());
	} else {
		parent.runWindowFunction(parentID, 'addManufacturerFromNew', new Array(manufacturerID,manufacturerName));
	}

	window.parent.WD(getUrlValue('W')); //Close Window
}

/**
 * Updated the manufacturer list after adding a new manufacturer
 */
function addManufacturerFromNew(data) {
	manufacturerID = data[0];
	manufacturerName = data[1];

	slt = document.getElementById('manufacturer');
	var opt = document.createElement('option');
	opt.text = manufacturerName;
	opt.value = manufacturerID;

	try {
		slt.add(opt, null); // standards compliant;
	} catch(ex) {
		slt.add(opt); // IE only
	}

	slt.value = manufacturerID;

}

/**
 * Closes the current window
 */
function closeWindow() {
	window.parent.WD(getUrlValue('W'));
}

/**
 * Redirect after product save
 */
function productSaved() {
	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'getProducts', new Array());
	document.location = 'edit_product_data.php?product_id=' + productID + "&W=" + getUrlValue('W');
//	window.parent.WD(getUrlValue('W')); //Close Window
}

/**
 * Redirect after variation save
 */
function variationSaved() {
	document.location = 'edit_product_variation.php?product_id=' + productID + "&W=" + getUrlValue('W');
}

/**
 * Get a list of products
 */
function getProducts(startAt,filter) {
	var productCount, row;
	if (!startAt) startAt=0;
	var productCanvas = document.getElementById('productList');
	var productContent = "<table id='productList' class='tbl_list'><tr><th>Code</th><th>Product Name</th><th>Variations</th><th>Price</th><th>&nbsp;</th></tr>";

	//Check if there is anything to filter
	filter = document.getElementById('filterString').value;

	c=0;

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			productLine = al.split('|||');
			if (productLine.length > 2) {
				for (i=0; i<productLine.length; i++) {
					if (productLine[i] == '') continue;
					if (productLine[i].substr(0,5) == '<link') continue;

					productItem = productLine[i].split('~~~');

					if (productItem[0] == 'data') {

						productCount = productItem[1];
						document.getElementById('productCount').innerHTML = productCount + ' Product' + (productCount==1?"":'s') + ' in total';
						var filterCount = productItem[2];
						if (filter == '') {
							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}


					productContent += "<tr id='row"+ c + "'>";
					productContent += "<td>"+ productItem[1] + "</td>";
					productContent += "<td>"+ productItem[2] + "</td>";
					productContent += "<td>"+ productItem[3] + "</td>";
					productContent += "<td>"+ productItem[4] + "</td>";
					productContent += "<td class='lastCol' align='right'>";
					productContent += "<button type='button' name='deleteProduct' id='d" + productItem[0] + "'><img src='/i/button/delete_16.png'>Delete</button>";
					productContent += "<button type='button' name='editProduct' id='e" + productItem[0] + "'><img src='/i/button/edit_16.png'>Edit</button>";
					productContent += "</td>";
					productContent += "</tr>";


					c++;
				}
			} else {
				for (i=0; i<productLine.length; i++) {
					if (productLine[i] == '') continue;
					if (productLine[i].substr(0,5) == '<link') continue;

					productItem = productLine[i].split('~~~');

					if (productItem[0] == 'data') {

						productCount = productItem[1];
						document.getElementById('productCount').innerHTML = productCount + ' Product' + (productCount==1?"":'s') + ' in total';
						var filterCount = productItem[2];
						if (filter == '') {
							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}
				}

				if (filter == '') {
					productContent += "<tr><td colspan=5  style='text-align: center; color: red'>~No Products to Display~</td>";
				} else {
					productContent += "<tr><td colspan=5 style='text-align: center; color: red'>~Filter returned 0 results~</td>";
				}

			}

			//No of pages
			pages = Math.floor(filterCount/10);
			last = Math.floor(startAt/10);
			lastPage = pages==last?'-1':pages;
			nextPage = lastPage==-1?'-1':startAt+10;
			previousPage = (last-1)==-1?'-1':startAt-10;

			productContent += getNavigation(startAt,previousPage,nextPage,lastPage)

			productContent += "</table>";
			productCanvas.innerHTML = productContent;

		}
	}

	Url = "/modules/products/ajax_product_list.php?start="+startAt+"&length=10";
	if (filter != '') Url += "&filter="+filter;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Creates a navigation bar
 */
function getNavigation(firstPage, previousPage, nextPage, lastPage, link) {
	if (!link) link = 'n';
	var navContent = "<tr><td colspan=6>";
	navContent += "<table class='navigation'>";

	if (firstPage == '0') {
		navContent += "<td class='navFirstTD'><span class='navLinkLeftOff'>First</span></td>";
	} else {
		navContent += "<td class='navFirstTD'><a name='firstPage' class='navLinkLeft' id='" + link + "0'>First</a></td>";
	}

	if (previousPage < 0) {
		navContent += "<td class='navPreviousTD'><span class='navLinkLeftOff'>Previous</span></td>";
	} else {
		navContent += "<td class='navPreviousTD'><a name='previousPage' class='navLinkLeft' id='" + link + previousPage+ "'>Previous</a></td>";
	}

	if (nextPage < 0) {
		navContent += "<td class='navNextTD'><span class='navLinkRightOff'>Next</span></td>";
	} else {
		navContent += "<td class='navNextTD'><a name='nextPage' class='navLinkRight' id='" + link + nextPage+ "'>Next</a></td>";
	}

	if (lastPage < 0) {
		navContent += "<td class='navLastTD'><span class='navLinkRightOff'>Last</span></td>";
	} else {
		navContent += "<td class='navLastTD'><a name='lastPage' class='navLinkRight' id='" + link + lastPage + "'>Last</a></td>";
	}

	navContent += "</table></td></tr>";
	return navContent;

	c++;
}

/**
 * Opens the edit/add variations window
 */
function openVariationWindow(variationID) {
	title = variationID?"Edit Variation":"New Variation";
	W = getUrlValue('W');
	url = '/modules/products/variation_edit.php?parent=' + W;
	if (variationID) {
		url += "&variation_id=" + variationID;
	}
	window.parent.W(title,url,250,600);
}

/**
 * Passes data to the variation insert script
 */
function insertVariation() {

	var data = new Array();

	data[0] = getUrlValue('variation_id');
	data[1] = document.getElementById('variationCode').value;
	data[2] = document.getElementById('variationName').value;
	data[3] = document.getElementById('variationDescription').value;
	data[4] = document.getElementById('variationPrice').value;
	data[5] = document.getElementById('priceFromProduct').checked?'Y':'N';

	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'addVariation', data);
	window.parent.WD(getUrlValue('W')); //Close Window

}

/**
 * Add a variation to the list
 */
function addVariation(data) {

	//Remove 'no variation' line
	if (document.getElementById('trempty')) {
		delRow = document.getElementById('trempty').rowIndex;
		document.getElementById('variationTable').deleteRow(delRow);
	}

	var tbl = document.getElementById('variationTable');
	if (data[0] == '') {

		var vCount = getCookie('vCount');
		if (vCount === false) vCount = 800000;

		TRTop = document.createElement('TR');
		TRTop.setAttribute('id', 'tr' + vCount);

		//Variation Code
		TDCode = document.createElement('TD');
		TDText = document.createTextNode(data[1]);
		TDCode.appendChild(TDText);
		TRTop.appendChild(TDCode);

		//Variation Name & Descrition
		TDName = document.createElement('TD');
		TDText = document.createTextNode(data[2]);
		TDName.appendChild(TDText);
		BR = document.createElement('BR');
		TDName.appendChild(BR);
		SPAN = document.createElement('SPAN');
		SPAN.setAttribute('class', 'variationDesc');
		TDName.appendChild(SPAN);
		TDText = document.createTextNode(data[3]);
		SPAN.appendChild(TDText)
		TDName.appendChild(SPAN);
		TRTop.appendChild(TDName);

		//Variation Price
		TDPrice = document.createElement('TD');
		if (data[5] == 'Y') {
			TDText = document.createTextNode('FP');
		} else {
			TDText = document.createTextNode(data[4]);
		}
		TDPrice.appendChild(TDText);
		TRTop.appendChild(TDPrice);

		//Remove Button
		TD2 = document.createElement('TD');
		TD2.setAttribute('class', 'rightAlign');
		button = document.createElement('INPUT');
		button.setAttribute('type', 'button');
		button.setAttribute('value', 'Remove');
		button.setAttribute('class', 'delete_button');
		button.setAttribute('name', 'removeVariation');
		button.setAttribute('id', 'vr' + vCount);
		TD2.appendChild(button);

		//Edit Button
		button = document.createElement('INPUT');
		button.setAttribute('type', 'button');
		button.setAttribute('value', 'Edit');
		button.setAttribute('class', 'edit_button');
		button.setAttribute('name', 'editVariation');
		button.setAttribute('id', 've' + vCount);
		TD2.appendChild(button);
		TRTop.appendChild(TD2);

		tbl.appendChild(TRTop);

		//Update variations field
		newVariation = vCount + '~~~' + data[1] + '~~~' + data[2] + '~~~' + data[3] + '~~~' + data[4] + '~~~' + data[5] + '|||';
		document.getElementById('variations').value += newVariation;

		//Set session
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				al = xmlhttp.responseText;
			}
		}

		Url = "/modules/products/ajax_save_session.php?";
		Url += "id=" + vCount;
		Url += "&code=" + escape(data[1]);
		Url += "&name=" + escape(data[2]);
		Url += "&description=" + escape(data[3]);
		Url += "&price=" + escape(data[4]);
		Url += "&pricefromproduct=" + escape(data[5]);

		xmlhttp.open("GET",Url,true);
		xmlhttp.send();

		//update Cookie
		vCount++;
		setCookie('vCount', vCount, 10);

	} else {

		//Remove existing row
		delRow = document.getElementById('tr'+ data[0]).rowIndex;
		document.getElementById('variationTable').deleteRow(delRow);

		TRTop = document.createElement('TR');
		TRTop.setAttribute('id', 'tr' + data[0]);

		//Variation Code
		TDCode = document.createElement('TD');
		TDText = document.createTextNode(data[1]);
		TDCode.appendChild(TDText);
		TRTop.appendChild(TDCode);

		//Variation Name & Descrition
		TDName = document.createElement('TD');
		TDText = document.createTextNode(data[2]);
		TDName.appendChild(TDText);
		BR = document.createElement('BR');
		TDName.appendChild(BR);
		SPAN = document.createElement('SPAN');
		SPAN.setAttribute('class', 'variationDesc');
		TDName.appendChild(SPAN);
		TDText = document.createTextNode(data[3]);
		SPAN.appendChild(TDText)
		TDName.appendChild(SPAN);
		TRTop.appendChild(TDName);

		//Variation Price
		TDPrice = document.createElement('TD');
		if (data[5] == 'Y') {
			TDText = document.createTextNode('FP');
		} else {
			TDText = document.createTextNode(data[4]);
		}
		TDPrice.appendChild(TDText);
		TRTop.appendChild(TDPrice);

		//Remove Button
		TD2 = document.createElement('TD');
		TD2.setAttribute('class', 'rightAlign');
		button = document.createElement('INPUT');
		button.setAttribute('type', 'button');
		button.setAttribute('value', 'Remove');
		button.setAttribute('class', 'delete_button');
		button.setAttribute('name', 'removeVariation');
		button.setAttribute('id', 'vr' + data[0]);
		TD2.appendChild(button);

		//Edit Button
		button = document.createElement('INPUT');
		button.setAttribute('type', 'button');
		button.setAttribute('value', 'Edit');
		button.setAttribute('class', 'edit_button');
		button.setAttribute('name', 'editVariation');
		button.setAttribute('id', 've' + data[0]);
		TD2.appendChild(button);
		TRTop.appendChild(TD2);

		tbl.appendChild(TRTop);

		//Update variations field
		newVariation = '';
		dataInserted = false;
		currentVariations = document.getElementById('variations').value.split('|||');
		for (i = 0; i < currentVariations.length; i++) {
			if (currentVariations[i] == '') continue;
			varData = currentVariations[i].split('~~~');
			if (varData[0] == data[0]) {
				newVariation += data[0] + '~~~' + data[1] + '~~~' + data[2] + '~~~' + data[3] + '~~~' + data[4] + '~~~' + data[5] + '|||';
				dataInserted = true;
			} else {
				newVariation += currentVariations[i] + '|||';
			}
		}
		if (!dataInserted) {
			newVariation += data[0] + '~~~' + data[1] + '~~~' + data[2] + '~~~' + data[3] + '~~~' + data[4] + '~~~' + data[5] + '|||';
		}
		document.getElementById('variations').value = newVariation;

		//Set session
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				al = xmlhttp.responseText;
			}
		}

		Url = "/modules/products/ajax_save_session.php?";
		Url += "id=" + data[0];
		Url += "&code=" + escape(data[1]);
		Url += "&name=" + escape(data[2]);
		Url += "&description=" + escape(data[3]);
		Url += "&price=" + escape(data[4]);
		Url += "&pricefromproduct=" + escape(data[5]);

		xmlhttp.open("GET",Url,true);
		xmlhttp.send();
	}
}

/**
 * Remove a variation
 */
function removeVariation(id) {
	//Remove existing row
	delRow = document.getElementById('tr'+ id).rowIndex;
	document.getElementById('variationTable').deleteRow(delRow);

	//Update variations field
	newVariation = '';
	dataInserted = false;
	currentVariations = document.getElementById('variations').value.split('|||');
	for (i = 0; i < currentVariations.length; i++) {
		if (currentVariations[i] == '') continue;
		varData = currentVariations[i].split('~~~');
		if (varData[0] == id) {
			newVariation += id + '~~~removed|||'
			dataInserted = true;
		} else {
			newVariation += currentVariations[i] + '|||';
		}
	}
	if (!dataInserted) {
		newVariation += id + '~~~removed|||';
	}
	document.getElementById('variations').value = newVariation;

}

/**
 * Opens the edit/add variations window
 */
function openAssociationWindow() {
	title = "Add Associated Product";
	W = getUrlValue('W');
	url = '/modules/products/association_picker.php?parent=' + W;
	window.parent.W(title,url,400,600);
}

/**
 * Get a list of products for association selection
 */
function getAssociatedProducts(startAt) {
	if (!startAt) startAt=0;
	var tbl = document.getElementById('productList');
	filter = document.getElementById('filterString').value;
	//	productList = new Array();

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			var insertPoint = document.getElementById('productList');
			var productList = "<table id='productList' class='tbl_list'><tr><th>Product Code</th><th>Product Name</th><th>&nbsp;</th></tr>";

			//Clear previous results
//			for (j = 0; j < 40; j++) {
//				nodeList = document.getElementsByTagName('TR');
//				for (i = 0; i < nodeList.length; i++) {
//					if (nodeList[i].id.substr(0,2) == 'tp' || nodeList[i].id.substr(0,3) == 'row') {
//						delRow = document.getElementById(nodeList[i].id).rowIndex;
//						document.getElementById('productList').deleteRow(delRow);
//					}
//				}
//			}

			line = al.split('|||');
			for (i = 0; i < line.length; i++) {
				if (line[i] == '') continue;
				lineData = line[i].split('~~~');

				if (lineData[0] == 'data') {
					productCount = lineData[1];
					filterCount = lineData[2];
					continue;
				}

				productList += "<tr>";
				productList += "<td>"+ lineData[1] +"</td>";
				productList += "<td>"+ lineData[2] +"</td>";
				productList += "<td class='rightAlign'>";
				productList += "<button type='button' id='sp" + lineData[0] +"' name='selectProduct'><img src='/i/button/down_16.png'>Select</button>";
				productList += "</td></tr>";

				productListB[i] = new Array();
				productListB[i][0] = lineData[0];
				productListB[i][1] = lineData[1];
				productListB[i][2] = lineData[2];
			}

			//No of pages
			pages = Math.floor(filterCount/10);
			last = Math.floor(startAt/10);
			lastPage = pages==last?'-1':pages;
			nextPage = lastPage==-1?'-1':startAt+10;
			previousPage = (last-1)==-1?'-1':startAt-10;

			productList += getNavigation(startAt,previousPage,nextPage,lastPage,'r');

			productList += "</table>";

			insertPoint.innerHTML = productList;
		}
	}

	Url = "/modules/products/ajax_product_list.php?start="+startAt+"&length=10";
	if (filter != '') Url += "&filter="+filter;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Add a selected associated product to the list
 */
function addAssociatedProduct(id) {
	data = new Array();
	for (i = 0; i < productListB.length; i++) {
		if (productListB[i] == undefined) continue;
		if (productListB[i] == '') continue;

		if (productListB[i][0] == id) {
			data = productListB[i];
			break;
		}
	}

	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'insertAssociatedProduct', data);
	window.parent.WD(getUrlValue('W')); //Close Window

}

/**
 * Insert the associated product into the table
 */
function insertAssociatedProduct(data) {

	var tbl = document.getElementById('associationTable');

	TR = document.createElement('TR');
	TR.setAttribute('id', 'ar'+data[0])

	TDCode = document.createElement('TD');
	TDText = document.createTextNode(data[1]);
	TDCode.appendChild(TDText);

	TDName = document.createElement('TD');
	TDText = document.createTextNode(data[2]);
	TDName.appendChild(TDText);

	TDButton = document.createElement('TD');
	TDButton.setAttribute('class', 'rightAlign');
	button = document.createElement('BUTTON');
	button.setAttribute('type', 'button');
//	button.setAttribute('value', 'Remove');
	button.setAttribute('name', 'removeAssociation');
//	button.setAttribute('class', 'delete_button');
	button.setAttribute('id', 'sp' + data[0]);
	buttonText = document.createTextNode('Remove');
	buttonImage = document.createElement('IMG');
	buttonImage.setAttribute('src', '/i/button/button_cancel_16.png');
	button.appendChild(buttonImage);
	button.appendChild(buttonText);
	TDButton.appendChild(button);

	TR.appendChild(TDCode);
	TR.appendChild(TDName);
	TR.appendChild(TDButton);

	tbl.appendChild(TR);

	//Add to hidden field
	document.getElementById('associations').value += "," + data[0];

}

/**
 * Remove an associated product from the list
 */
function removeAssociatedProduct(id) {
	newList = '';
	delRow = document.getElementById('ar'+id).rowIndex;
	document.getElementById('associationTable').deleteRow(delRow);

	//Remove from hidden field
	hiddenData = document.getElementById('associations').value.split(',');
	for (i = 0; i < hiddenData.length; i++) {
		if (hiddenData[i] == '') continue;
		if (hiddenData[i] != id) {
			newList += hiddenData[i] + "," ;
		}
	}

	document.getElementById('associations').value = newList;

}

/**
 * Redirect after association save
 */
function associationSaved() {
	document.location = 'edit_product_association.php?product_id=' + productID + "&W=" + getUrlValue('W');
}


/**
 * get a list af layout via Ajax
 */
function getLayouts(layoutType, areaID) {
	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=popLayout;

	Url = "/modules/products/ajax_layout_options.php?type=" + layoutType + "&area_id=" + areaID;

	xmlhttp.open("GET", Url, true);
	xmlhttp.send();
}

/**
 * Populate the layout list in HTML
 */
function popLayout() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		if (window.DOMParser) {
			parser=new DOMParser();
			xmlDoc=parser.parseFromString(xmlhttp.responseText,"text/xml");
		} else { // Internet Explorer
			xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async="false";
			xmlDoc.loadXML(xmlhttp.responseText);
		}

		var css = new Array();
		var insertBlock = document.getElementById('layoutList');
		var al = xmlDoc.getElementsByTagName('layout');

		for (i = 0; i < al.length; i++) {
			layoutID = al[i].childNodes[0].textContent?al[i].childNodes[0].textContent:al[i].childNodes[0].text;
			selectedLayout = al[i]. childNodes[0].getAttribute('current');
			TDname = al[i].childNodes[1].textContent?al[i].childNodes[1].textContent:al[i].childNodes[1].text;
			TDdescription = al[i].childNodes[2].textContent?al[i].childNodes[2].textContent:al[i].childNodes[2].text;
			css[i] = al[i].childNodes[5].textContent?al[i].childNodes[5].textContent:al[i].childNodes[5].text;
			TDTemplate = decodeURIComponent(al[i].childNodes[4].textContent?al[i].childNodes[4].textContent:al[i].childNodes[4].text);
			TDTemplate = TDTemplate.replace(/\+/g, ' ');

			//Outer TR & TD
			var outerTR = document.createElement('TR');
			var outerTD = document.createElement('TD');

			//Inner Table
			table1 = document.createElement('TABLE');
			table1.setAttribute('class', 'templateTable');
			innerTRname = document.createElement('TR');
			innerTDname = document.createElement('TD');
			innerTDnameText = document.createTextNode(TDname);
			BR = document.createElement('BR');
			BR2 = document.createElement('BR');

			//Description
			descriptionDiv = document.createElement('Span');
			descriptionDiv.setAttribute('class', 'templateDescription');
			innerTDnameDescription = document.createTextNode(TDdescription);
			descriptionDiv.appendChild(innerTDnameDescription);

			//Radio Button
			radioButton = document.createElement('INPUT');
			radioButton.setAttribute('type', 'radio');
			radioButton.setAttribute('name', 'layoutID');
			radioButton.setAttribute('value', layoutID);
			if (selectedLayout == 1) radioButton.setAttribute('checked', true);


			//Template
			templateDiv = document.createElement('DIV');
			templateDiv.setAttribute('id', 'div'+layoutID);
			templateDiv.setAttribute('class', 'templateDiv');

			//Put it all together
			innerTDname.appendChild(radioButton);
			innerTDname.appendChild(innerTDnameText);
			innerTDname.appendChild(BR2);
			innerTDname.appendChild(descriptionDiv);
			innerTDname.appendChild(BR);
			innerTDname.appendChild(templateDiv);

			innerTRname.appendChild(innerTDname)
			table1.appendChild(innerTRname);
			outerTD.appendChild(table1);
			outerTR.appendChild(outerTD);

			insertBlock.appendChild(outerTR);

			//Empty Row for formatting
			emptyTR = document.createElement('TR');
			emptyTD = document.createElement('TD');
			emptyTD.setAttribute('colspan', '3');
			emptyTD.setAttribute('class', 'trDivider');
			emptyTR.appendChild(emptyTD);
			insertBlock.appendChild(emptyTR);



			document.getElementById('div'+layoutID).innerHTML = TDTemplate;


		}

		insertSpot = document.getElementById('formCanvas');
		for (i = 0; i < css.length; i++) {
			cssItem = document.createElement('LINK');
			cssItem.setAttribute('type', 'text/css');
			cssItem.setAttribute('href', '/modules/products/layouts/' + css[i]);
			cssItem.setAttribute('rel', 'stylesheet');

			insertSpot.appendChild(cssItem);
		}

	}
}

/**
 * Get and replace the current CSS data with the defaults
 */
function get_css_default(areaID, file) {
	if (confirm('WARNING!\n\nThis process will replace the current data\n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById('css_data').value = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","/modules/products/edit/css_reload.php?area_id=" + areaID + "&file=" + file,false);
		xmlhttp.send();
	}
}
/**
 * Get and replace the current CSS data with the defaults
 */
function get_css_default_old(file) {
	if (confirm('WARNING!\n\nThis process will replace the current data\n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById('css_data').value = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","/modules/products/css/default/" + file + '.css',false);
		xmlhttp.send();
	}
}

/**
 * Opens the edit/add product variation group window
 */
function openGroupWindow(productID,groupID) {
	title = groupID?"Edit Variations Group":"New Variations Group";
	W = getUrlValue('W');
	url = '/modules/products/variations_group_edit.php?parent=' + W;
	if (groupID) {
		url += "&group_id=" + groupID;
	}
	window.parent.W(title,url,100,400);


}

/**
 * Passes data to the variation group insert script
 */
function insertGroup() {

	var data = new Array();

	data[0] = document.getElementById('groupName').value;

	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'addGroup', data);
	window.parent.WD(getUrlValue('W')); //Close Window

}

/**
 * Add/update a group
 */
function addGroup(data) {
	var groupField = document.getElementById('groups');

	groupID = data[0].replace(' ','').toLowerCase();

	groupField.value += groupID +'~~~' + data[0] + '|||';

	document.getElementById('closeButton').innerHTML = "<img src='/i/button/close_16.png'>Close Without Saving";
	drawVariations();
}

/**
 * Draws the variations
 */
function drawVariations() {
	var groupData = document.getElementById('groups').value;
	var variationsData = document.getElementById('variations').value;
	var groupLocation = document.getElementById('variationsList');
	var table = '';

	groupLine = groupData.split('|||');
	for (i = 0; i < groupLine.length; i++) {
		if (groupLine[i] == '') continue;
		groupItem = groupLine[i].split('~~~');

		table += "<table class='tbl_list'>";
		table += "<tr><th colspan=2 class='groupName'>" + groupItem[1] + "</th>";
		table += "<th class='thirdCol'><button type='button' id='"+ groupItem[0] +"_0' name='addItem'><img src='/i/button/add_16.png'>Add "+ groupItem[1] +"</button>";
		table += "<button type='button' id='"+ groupItem[0] +"' name='removeGroup' ><img src='/i/button/delete_16.png'>Remove Group</button></th></tr>";
		table += "<tr><td class='secondRow firstCol'>Variation Code</td><td class='secondRow secondCol'>Variation Name</td><td class='secondRow thirdCol'></td></tr>";

		variationLine = variationsData.split('|||');
		for (j = 0; j < variationLine.length; j++) {
			variationItem = variationLine[j].split('~~~');
			if (variationItem[2] == groupItem[0]) {
				table += "<tr>";
				table += "<td>"+ variationItem[0] +"</td>";
				table += "<td>"+ variationItem[1] +"</td>";
				table += "<td class='thirdCol'><button type='button'  id='"+ groupItem[0] +"_"+ variationItem[0] +"' name='deleteItem'><img src='/i/button/delete_16.png'>Remove</button>";
				table += "<button type='button' id='"+ groupItem[0] +"_"+ variationItem[0] +"' name='addItem'><img src='/i/button/edit_16.png'>Edit</button></td>";
			}
		}
		table += '</table><br>';
	}

	groupLocation.innerHTML = table;
}



/**
 * Remove a variation group
 */
function removeGroup(groupID) {
	var groupData = document.getElementById('groups');
	var itemData = document.getElementById('variations');
	var newItemList = '';
	var newGroupList = '';
	var groupName = '';
	var item;

	//Get the group Name
	groupLine = groupData.value.split('|||');
	for (i = 0; i < groupLine.length; i++) {
		if (groupLine[i] == '') continue;
		groupItem = groupLine[i].split('~~~');
		if (groupItem[0] == groupID) groupName = groupItem[1];
	}

	if (confirm('Remove '+ groupName +' and all it\'s contents?')) {

		//Remove Items
		itemLine = itemData.value.split('|||');
		for (i = 0; i < itemLine.length; i++) {
			if (itemLine[i] == '') continue;
			item = itemLine[i].split('~~~');
			if (item[2]!=groupID) {
				newItemList += itemLine[i] + '|||';
			}
		}

		//Remove Group
		groupLine = groupData.value.split('|||');
		for (i = 0; i < groupLine.length; i++) {
			if (groupLine[i] == '') continue;
			groupItem = groupLine[i].split('~~~');
			if (groupItem[0] != groupID) {
				newGroupList += groupLine[i] + '|||';
			}
		}

		itemData.value = newItemList;
		groupData.value = newGroupList;

		document.getElementById('closeButton').innerHTML = "<img src='/i/button/close_16.png'>Close Without Saving";
		drawVariations();
	}
}


/**
 * Opens the edit/add variation group item window
 */
function openItemWindow(groupID, itemID) {

	//Vars
	var groupName = '';
	var itemName = '';

	//Get the group Name
	var groupData = document.getElementById('groups').value;
	groupLine = groupData.split('|||');
	for (i = 0; i < groupLine.length; i++) {
		if (groupLine[i] == '') continue;
		groupItem = groupLine[i].split('~~~');
		if (groupItem[0] == groupID) groupName = groupItem[1];
	}

	//Get Item name
	var itemData = document.getElementById('variations').value;
	var itemLine = itemData.split('|||');
	for (i = 0; i < itemLine.length; i++) {
		if (itemLine[i] == '') continue;
		var item = itemLine[i].split('~~~');
		if (item[0] == itemID && item[2]==groupID) itemName = item[1];
	}


	title = itemID!=0?"Edit " + groupName:"New "+ groupName;
	W = getUrlValue('W');
	url = '/modules/products/variation_item_edit.php?parent=' + W;
	url += "&item_id=" + itemID;
	url += "&item_name=" + escape(itemName);
	url += "&group_id=" + groupID;
	url += "&group_name=" + escape(groupName);
	window.parent.W(title,url,200,450);
}


/**
 * Add a variation to the list
 */
function addVariationB(data) {
	var itemData = document.getElementById('variations');
	var append = true;
	var update = false;
	var newList = '';
	var item;

	itemLine = itemData.value.split('|||')
	for (i = 0; i < itemLine.length; i++) {
		if (itemLine[i] == '') continue;
		item = itemLine[i].split('~~~');
		if (data[1] == 0) { //Insert Item
			if (data[2] == item[0] && data[0] == item[2]) append = false;
		} else { //Update Item
			update = true;
			if (data[1] == item[0]) {
				newList += data[2] + "~~~" + data[3] + "~~~" + data[0] + "|||";
			} else {
				newList += itemLine[i] + '|||';
			}
		}
	}

	if (data[1] == 0 && append) {
		itemData.value += data[2] + "~~~" + data[3] + "~~~" + data[0] + "|||";
	}

	if (update) {
		itemData.value = newList;
	}

	document.getElementById('closeButton').innerHTML = "<img src='/i/button/close_16.png'>Close Without Saving";
	drawVariations();

}

/**
 * Passes data to the product variation insert script
 */
function insertVariationB() {

	var data = new Array();

	data[0] = getUrlValue('group_id');
	data[1] = getUrlValue('item_id');
	data[2] = document.getElementById('itemCode').value;
	data[3] = document.getElementById('itemName').value;

	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'addVariationB', data);
	window.parent.WD(getUrlValue('W')); //Close Window

}

/**
 * Remove a variation
 */
function removeVariationB(groupID, itemID) {

	//Vars
	var itemData = document.getElementById('variations');
	var groupData = document.getElementById('groups').value;
	var newList = '';
	var itemName = '';
	var groupName = '';
	var item, groupItem;

	//Get the group Name
	groupLine = groupData.split('|||');
	for (i = 0; i < groupLine.length; i++) {
		if (groupLine[i] == '') continue;
		groupItem = groupLine[i].split('~~~');
		if (groupItem[0] == groupID) groupName = groupItem[1];
	}

	//Get Item name
	itemLine = itemData.value.split('|||');
	for (i = 0; i < itemLine.length; i++) {
		if (itemLine[i] == '') continue;
		item = itemLine[i].split('~~~');
		if (item[0] == itemID && item[2]==groupID) itemName = item[1];
	}

	if (confirm('Delete '+ itemName +' from '+ groupName +'?')) {
		itemLine = itemData.value.split('|||');
		for (i = 0; i < itemLine.length; i++) {
			if (itemLine[i] == '') continue;
			item = itemLine[i].split('~~~');
			if (itemID == item[0] && item[2]==groupID) {

			} else {
				newList += itemLine[i] + '|||';
			}
		}

		itemData.value = newList;

		document.getElementById('closeButton').innerHTML = "<img src='/i/button/close_16.png'>Close Without Saving";
		drawVariations();
	}


}

/**
 * Delete a product
 */
function deleteProduct(productID) {
	if (confirm('WARNING! This will permanently delete the product. This action cannot be undone. \n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				getProducts();
			}
		}

		Url = "/modules/products/ajax_product_delete.php?product_id=" + productID;

		xmlhttp.open("GET",Url,false);
		xmlhttp.send();
	}


}

/**
 * Import groups from anather product
 */
function importGroupWindow() {
	title = 'Import Groups';
	W = getUrlValue('W');
	url = '/modules/products/variation_import_groups.php?parent=' + W;
	window.parent.W(title,url,200,450);
}

/**
 * Draw a list of groups to select from
 */
function drawGroups(groupList) {
	var boxes = '';

	groupLine = groupList.split('~~~');
	for (i = 0; i < groupLine.length; i++) {
		if (groupLine[i] == '') continue;
		if (i==0) {

		} else {
			groupItem = groupLine[i].split('///');
			boxes += "<input type='checkbox' id='"+ groupItem[0] +"' value='"+ groupItem[0] +"'>";
			boxes += "<label for='"+ groupItem[0] +"'>" + groupItem[1] + "</label><br>";
		}
	}

	document.getElementById('groupList').innerHTML = boxes;
}

/**
 * Pass the selected groups to the product window
 */
function applyGroups() {

	//Vars
	var cnt = 0;
	var allInputs = document.getElementsByTagName('input');
	var productData = document.getElementById('product').value;
	data = new Array();

	//Product code
	data[0] = productData.substr(0, productData.indexOf('~~~'));

	//Get all checkboxes
	data[1] = new Array();
	for (i = 0; i < allInputs.length; i++) {

		if (allInputs[i].type == 'checkbox') {
			if (allInputs[i].checked) {
				data[1][cnt] = allInputs[i].id;
				cnt++;
			}
		}
	}

	//Open/CLose windows
	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'getGroups', data);
	window.parent.WD(getUrlValue('W')); //Close Window

}

/**
 * Uses import data to collect group information
 */
function getGroups(data) {
	 if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=updateGroups;

	Url = "/modules/products/ajax_group_data.php?product_id=" + data[0] + "&groups="

	for (i = 0; i < data[1].length; i++) {
		Url += data[1][i] + ","
	}

	xmlhttp.open("GET", Url, true);
	xmlhttp.send();

}

/**
 * updates group information with fetch group data
 */
function updateGroups() {
	if (xmlhttp.readyState==4 && xmlhttp.status==200) {

		//Vars
		var newList = '';
		var groupData = document.getElementById('groups');
		var itemData = document.getElementById('variations');
		var item;

		groupLine = xmlhttp.responseText.split('|||');
		for (i = 0; i < groupLine.length; i++) {
			if (groupLine[i] == '') continue;
			groupItems = groupLine[i].split('~~~');
			for (j = 0; j < groupItems.length; j++) {
				if (groupItems[j] == '') continue;
				if (j == 0) { //Update Groups
					groups = groupItems[j].split('///');
					if (groupData.value.indexOf(groups[0]) < 0) {
						groupData.value += groups[0] + '~~~' + groups[1] + '|||';
					}
					//Remove Current group data
					oldItemLine = itemData.value.split('|||');
					for (k = 0; k < oldItemLine.length; k++) {
						if (oldItemLine[k] == '') continue;
						oldItem = oldItemLine[k].split('~~~');
						if (oldItem[2] != groups[0]) {
							newList += oldItemLine[k] + '|||';
						}
					}
					itemData.value = newList;

				} else { //Update Items
					item = groupItems[j].split('///');

					//Append imported item data
					itemData.value += item[0] + "~~~" + item[1] + "~~~" + groups[0] + '|||';

				}
			}
		}

//		alert(itemData.value);
		drawVariations()

	}
}

/**
 * AJAX call to get a list af manufactures
 */
function getManufacturers(startAt,filter) {
	if (!startAt) startAt=0;
	var manufacturerCanvas = document.getElementById('manufacturerList');
	var manufacturerContent = "<table id='manufacturerTable' class='tbl_list'><tr><th width=500px >Manufacturer Name</th><th>Product Count</th><th>&nbsp;</th></tr>";

	//Check if there is anything to filter
	filter = document.getElementById('filterString').value;

	c=0;

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			manufacturerLine = al.split('|||');
			if (manufacturerLine.length > 2) {
				for (i=0; i<manufacturerLine.length; i++) {
					if (manufacturerLine[i] == '') continue;
					if (manufacturerLine[i].substr(0,5) == '<link') continue;

					manufacturerItem = manufacturerLine[i].split('~~~');

					if (manufacturerItem[0] == 'data') {

						manufacturerCount = manufacturerItem[1];
						document.getElementById('manufacturerCount').innerHTML = manufacturerCount + ' manufacturer' + (manufacturerCount==1?"":'s') + ' in total';
						var filterCount = manufacturerItem[2];
						if (filter == '') {
							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}


					manufacturerContent += "<tr id='row"+ c + "'>";
					manufacturerContent += "<td>"+ manufacturerItem[1] + "</td>";
					manufacturerContent += "<td style='text-align: center'>"+ manufacturerItem[2] + "</td>";
					manufacturerContent += "<td class='lastCol' align='right'>";
					manufacturerContent += "<button type='button' name='deletemanufacturer' id='d" + manufacturerItem[0] + "'><img src='/i/button/delete_16.png'>Delete</button>";
					manufacturerContent += "<button type='button' name='editmanufacturer' id='e" + manufacturerItem[0] + "'><img src='/i/button/edit_16.png'>Edit</button>";
					manufacturerContent += "</td>";
					manufacturerContent += "</tr>";


					c++;
				}
			} else {
				for (i=0; i<manufacturerLine.length; i++) {
					if (manufacturerLine[i] == '') continue;
					if (manufacturerLine[i].substr(0,5) == '<link') continue;

					manufacturerItem = manufacturerLine[i].split('~~~');

					if (manufacturerItem[0] == 'data') {

						manufacturerCount = manufacturerItem[1];
						document.getElementById('manufacturerCount').innerHTML = manufacturerCount + ' manufacturer' + (manufacturerCount==1?"":'s') + ' in total';
						var filterCount = manufacturerItem[2];
						if (filter == '') {
							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}
				}

				if (filter == '') {
					manufacturerContent += "<tr><td colspan=5  style='text-align: center; color: red'>~No manufacturers to Display~</td>";
				} else {
					manufacturerContent += "<tr><td colspan=5 style='text-align: center; color: red'>~Filter returned 0 results~</td>";
				}

			}

			//No of pages
			pages = Math.floor(filterCount/10);
			last = Math.floor(startAt/10);
			lastPage = pages==last?'-1':pages;
			nextPage = lastPage==-1?'-1':startAt+10;
			previousPage = (last-1)==-1?'-1':startAt-10;

			manufacturerContent += getNavigation(startAt,previousPage,nextPage,lastPage,'m')

			manufacturerContent += "</table>";
			manufacturerCanvas.innerHTML = manufacturerContent;

		}
	}

	Url = "/modules/products/ajax_manufacturer_list.php?start="+startAt+"&length=10";
	if (filter != '') Url += "&filter="+filter;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Opens a windom to edit/add a manufacturer
 */
function openEditManufacturerWindow(id) {
	title = id?"Edit Manufacturer":"New Manufacturer";
	W = getUrlValue('W');
	url = '/modules/products/manufacturer_edit.php?parent=' + W + '&manufacturer_id=' + id + '&ver=2';
	window.parent.W(title,url,150,400);
}

/**
 * Deletes a manufacturer
 */
function deleteManufacturer(id) {

	if (confirm('WARNING!\nDeleting a manufacturer cannot be undone\n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				getManufacturers();
			}
		}

		Url = "/modules/products/ajax_manufacturer_delete.php?manufacturer_id=" + id;

		xmlhttp.open("GET",Url,true);
		xmlhttp.send();
	}
}

/**
 * AJAX call to get a list of product categories
 */
function getCategories(startAt,filter) {
	if (!startAt) startAt=0;
	var categoryCanvas = document.getElementById('categoryList');
	var categoryContent = "<table id='categoryTable' class='tbl_list'><tr><th width=500px >category Name</th><th>Product Count</th><th>&nbsp;</th></tr>";

	//Check if there is anything to filter
//	filter = document.getElementById('filterString').value;

	c=0;

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			categoryLine = al.split('|||');
			if (categoryLine.length > 2) {
				for (i=0; i<categoryLine.length; i++) {
					if (categoryLine[i] == '') continue;
					if (categoryLine[i].substr(0,5) == '<link') continue;

					categoryItem = categoryLine[i].split('~~~');

					if (categoryItem[0] == 'data') {

						categoryCount = categoryItem[1];
						document.getElementById('categoryCount').innerHTML = categoryCount + ' categor' + (categoryCount==1?"y":'ies') + ' in total';
						var filterCount = categoryItem[2];
						if (filter == '') {
//							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
//							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}


					categoryContent += "<tr id='row"+ c + "'";
					if (categoryItem[3] == 0) categoryContent += " style='border-top: 2px solid black' ";
					categoryContent += ">";
					categoryContent += "<td"
					if (categoryItem[3] == 1) categoryContent += " style='padding-left: 10px' ";
					if (categoryItem[3] == 2) categoryContent += " style='padding-left: 30px' ";
					categoryContent += ">"
					if (categoryItem[3] == 1) categoryContent += "";
					if (categoryItem[3] == 2) categoryContent += "";
					categoryContent += categoryItem[1] + "</td>";
					categoryContent += "<td style='text-align: center'>"+ categoryItem[2] + "</td>";
					categoryContent += "<td class='lastCol' align='right'>";
					categoryContent += "<button type='button' name='deletecategory' id='d" + categoryItem[0] + "'><img src='/i/button/delete_16.png'>Delete</button>";
					categoryContent += "<button type='button' name='editcategory' id='e" + categoryItem[0] + "'><img src='/i/button/edit_16.png'>Edit</button>";
					categoryContent += "</td>";
					categoryContent += "</tr>";


					c++;
				}
			} else {
				for (i=0; i<categoryLine.length; i++) {
					if (categoryLine[i] == '') continue;
					if (categoryLine[i].substr(0,5) == '<link') continue;

					categoryItem = categoryLine[i].split('~~~');

					if (categoryItem[0] == 'data') {

						categoryCount = categoryItem[1];
						document.getElementById('categoryCount').innerHTML = categoryCount + ' category' + (categoryCount==1?"":'s') + ' in total';
						var filterCount = categoryItem[2];
						if (filter == '') {
							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}
				}

				if (filter == '') {
					categoryContent += "<tr><td colspan=5  style='text-align: center; color: red'>~No categorys to Display~</td>";
				} else {
					categoryContent += "<tr><td colspan=5 style='text-align: center; color: red'>~Filter returned 0 results~</td>";
				}

			}

			//No of pages
			pages = Math.floor(filterCount/10);
			last = Math.floor(startAt/10);
			lastPage = pages==last?'-1':pages;
			nextPage = lastPage==-1?'-1':startAt+10;
			previousPage = (last-1)==-1?'-1':startAt-10;

			categoryContent += getNavigation(startAt,previousPage,nextPage,lastPage,'c')

			categoryContent += "</table>";
			categoryCanvas.innerHTML = categoryContent;

		}
	}

	Url = "/modules/products/ajax_category_list.php?start="+startAt+"&length=10";
	if (filter != '') Url += "&filter="+filter;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}


/**
 * Delete a category from the DB
 */
function deletecategory(id) {
	if (confirm('WARNING!\nDeleting a category cannot be undone\n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				getCategories();
			}
		}

		Url = "/modules/products/ajax_category_delete.php?category_id=" + id;

		xmlhttp.open("GET",Url,true);
		xmlhttp.send();
	}
}

/**
 * Get a list of opplications from the server
 */
function getApplications(startAt,filter) {
	if (!startAt) startAt=0;
	var applicationCanvas = document.getElementById('applicationList');
	var applicationContent = "<table id='applicationTable' class='tbl_list'><tr><th width=500px >Product Application</th><th>Product Count</th><th>&nbsp;</th></tr>";

	//Check if there is anything to filter
	filter = document.getElementById('filterString').value;

	c=0;

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			applicationLine = al.split('|||');
			if (applicationLine.length > 2) {
				for (i=0; i<applicationLine.length; i++) {
					if (applicationLine[i] == '') continue;
					if (applicationLine[i].substr(0,5) == '<link') continue;

					applicationItem = applicationLine[i].split('~~~');

					if (applicationItem[0] == 'data') {

						applicationCount = applicationItem[1];
						document.getElementById('applicationCount').innerHTML = applicationCount + ' application' + (applicationCount==1?"":'s') + ' in total';
						var filterCount = applicationItem[2];
						if (filter == '') {
							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}


					applicationContent += "<tr id='row"+ c + "'>";
					applicationContent += "<td>"+ applicationItem[1] + "</td>";
					applicationContent += "<td style='text-align: center'>"+ applicationItem[2] + "</td>";
					applicationContent += "<td class='lastCol' align='right'>";
					applicationContent += "<button type='button' name='deleteApplication' id='d" + applicationItem[0] + "'><img src='/i/button/delete_16.png'>Delete</button>";
					applicationContent += "<button type='button' name='editApplication' id='e" + applicationItem[0] + "'><img src='/i/button/edit_16.png'>Edit</button>";
					applicationContent += "</td>";
					applicationContent += "</tr>";


					c++;
				}
			} else {
				for (i=0; i<applicationLine.length; i++) {
					if (applicationLine[i] == '') continue;
					if (applicationLine[i].substr(0,5) == '<link') continue;

					applicationItem = applicationLine[i].split('~~~');

					if (applicationItem[0] == 'data') {

						applicationCount = applicationItem[1];
						document.getElementById('applicationCount').innerHTML = applicationCount + ' application' + (applicationCount==1?"":'s') + ' in total';
						var filterCount = applicationItem[2];
						if (filter == '') {
							document.getElementById('filterCount').innerHTML = 'No Filter';
						} else {
							document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
						}
						continue;
					}
				}

				if (filter == '') {
					applicationContent += "<tr><td colspan=5  style='text-align: center; color: red'>~No applications to Display~</td>";
				} else {
					applicationContent += "<tr><td colspan=5 style='text-align: center; color: red'>~Filter returned 0 results~</td>";
				}

			}

			//No of pages
			pages = Math.floor(filterCount/10);
			last = Math.floor(startAt/10);
			lastPage = pages==last?'-1':pages;
			nextPage = lastPage==-1?'-1':startAt+10;
			previousPage = (last-1)==-1?'-1':startAt-10;

			applicationContent += getNavigation(startAt,previousPage,nextPage,lastPage,'a')

			applicationContent += "</table>";
			applicationCanvas.innerHTML = applicationContent;

		}
	}

	Url = "/modules/products/ajax_application_list.php?start="+startAt+"&length=10";
	if (filter != '') Url += "&filter="+filter;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Glose the application edit window
 */
function closeApplicationWindowMain() {
	parentID = getUrlValue('parent');
	parent.runWindowFunction(parentID, 'getApplications', new Array());
	window.parent.WD(getUrlValue('W')); //Close Window
}

/**
 * Delete an application
 */
function deleteApplication(applicationID) {
	if (confirm('WARNING!\nDeleting a product application cannot be undone\n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				getApplications();
			}
		}

		Url = "/modules/products/ajax_application_delete.php?application_id=" + applicationID;

		xmlhttp.open("GET",Url,true);
		xmlhttp.send();
	}
}


/**
 * Get list of subcategaries for a selected category
 */
function getCategory(parent, target) {
	var targetElement = document.getElementById(target);

	targetElement.length = 0;
	if (target == 'category2') document.getElementById('category3').length = 0;

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			line = xmlhttp.responseText.split('|||');
			for (i = 0; i < line.length; i++) {
				if (line[i] =='') continue;
				data = line[i].split('~~~');

				targetElement.options[targetElement.options.length] = new Option(data[1], data[0]);
			}

		}
	}

	Url = "/modules/products/ajax_category_list2.php?parent="+parent;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();

}

/**
 * Delete a selected color from the DB
 */
function deleteColor(colorID) {
	if (confirm("Are you sure you want to delete this color variation?\n\nThis cannot be undone")) {
		W = getUrlValue('W');
		document.location = "/modules/products/color_editor.php?W="+ W +"&colorID="+ colorID +"&delete";
	}
}