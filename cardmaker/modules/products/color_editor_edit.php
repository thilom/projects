<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

include 'toolbar_start.php';

if (count($_POST)  > 0) {

	//Vars
	$color_name = $_POST['colorName'];
	$file_name = empty($_FILES['colorFile']['tmp_name'])?'':$_FILES['colorFile']['name'];

	//Upload file
	if (!empty($_FILES['colorFile']['name'])) {
		move_uploaded_file($_FILES['colorFile']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/i/image_variations/{$_FILES['colorFile']['name']}");

		$statement = "UPDATE {$GLOBALS['db_prefix']}_products_image_variations
						SET imgVariation_file = :imgVariation_file
						WHERE imgVariation_id = :imgVariation_id
						LIMIT 1";
		$sql_file = $GLOBALS['dbCon']->prepare($statement);
		$sql_file->bindParam(':imgVariation_file', $file_name);
		$sql_file->bindParam(':imgVariation_id', $_GET['colorID']);
		$sql_file->execute();
	}

	//Update Name
	$statement = "UPDATE {$GLOBALS['db_prefix']}_products_image_variations
						SET imgVariation_name = :imgVariation_name
						WHERE imgVariation_id = :imgVariation_id
						LIMIT 1";
	$sql_save = $GLOBALS['dbCon']->prepare($statement);
	$sql_save->bindParam(':imgVariation_name', $color_name);
	$sql_save->bindParam(':imgVariation_id', $_GET['colorID']);
	$sql_save->execute();


	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>Color Updated</div>";
		echo "<div class='bMsg'><button type='button' onclick='document.location=\"/modules/products/color_editor.php?W={$_GET['W']}\"'><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/color_editor_edit.html');

//Get current data
$statement = "SELECT imgVariation_name
				FROM {$GLOBALS['db_prefix']}_products_image_variations
				WHERE imgVariation_id=:imgVariation_id
				LIMIT 1";
$sql_color = $GLOBALS['dbCon']->prepare($statement);
$sql_color->bindParam(':imgVariation_id', $_GET['colorID']);
$sql_color->execute();
$sql_color_data = $sql_color->fetch();
$sql_color->closeCursor();

//Repalec tags
$template = str_replace('<!-- W -->', $_GET['W'], $template);
$template = str_replace('<!-- color_name -->', $sql_color_data['imgVariation_name'], $template);

echo $template;

?>
