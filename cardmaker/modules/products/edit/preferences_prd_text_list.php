<?php

/**
 * Preferences for product text list
 *
 * @author Thilo Muller (2011)
 * @version $Id$
 */


//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$layout_list = '';
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/preferences_prd_text_list.html');
$category_options = '';
$link_id = '';
$product_category = '';
$manufacturer_options = '';
$product_manufacturer = '';
$manufacturer_aware = '';
$category_aware = '';
$application_options = '';
$product_application = '';
$application_aware = '';
$page_list = '';
$max_cols = '1';
$max_products = '';

//Get current Prefs
$pref_data = get_area_data($area_id, 'latest', array('preferences'));

//Expand Prefs
if (!empty($pref_data['data']['preferences'])) {
	$prefs = explode('|', $pref_data['data']['preferences']);

	foreach ($prefs as $pref) {
		if (empty($pref)) continue;
		list($var, $value) = explode('=', $pref);
		switch ($var) {
			case 'product_category':
				$product_category = $value;
				break;
			case 'product_manufacturer':
				$product_manufacturer = $value;
				break;
			case 'product_application':
				$product_application = $value;
				break;
			case 'link_id':
				$link_id = $value;
				break;
			case 'max_cols':
				$max_cols = $value;
				break;
			case 'max_products':
				$max_products = $value;
				break;
			case 'category_aware':
				$category_aware = $value=='1'?'checked':'';
				break;
			case 'application_aware':
				$application_aware = $value=='1'?'checked':'';
				break;
			case 'manufacturer_aware':
				$manufacturer_aware = $value=='1'?'checked':'';
				break;
		}
	}
}

//Save
if (count($_POST) > 0) {
	//Concat prefs
	$prefs = "product_category={$_POST['product_category']}|";
	$prefs .= "product_manufacturer={$_POST['product_manufacturer']}|";
	$prefs .= "product_application={$_POST['product_application']}|";
	$prefs .= "manufacturer_aware=" . (isset($_POST['manufacturerAware'])?"1":"0") ."|";
	$prefs .= "category_aware=" . (isset($_POST['categoryAware'])?"1":"0") ."|";
	$prefs .= "application_aware=" . (isset($_POST['applicationAware'])?"1":"0") ."|";
	$prefs .= "link_id={$_POST['link_id']}|";
	$prefs .= "max_cols={$_POST['max_cols']}|";
	$prefs .= "max_products={$_POST['max_products']}|";
	$prefs .= "template={$_POST['layoutID']}";

	if ($prefs != $pref_data['data']['preferences']) {
		$message = "&#187; Area Preferences Updated";

		//Save Data
		$fields['preferences'] = $prefs;
		update_area_data($area_id, 'draft', $fields);

		write_log("Preferences update for area (Draft)",'content_manager', $area_id);

	} else {
		$message = '&#187; No Changes, Nothing to update';
	}

	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button' onClick='document.location=\"preferences_editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"'><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}

//Get category List
$statement = "SELECT category_name, category_id
				FROM {$GLOBALS['db_prefix']}_products_categories
				ORDER BY category_name";
$sql_category = $GLOBALS['dbCon']->prepare($statement);
$sql_category->execute();
$sql_category_data = $sql_category->fetchAll();
$sql_category->closeCursor();

foreach ($sql_category_data as $category_data) {
	if ($product_category == $category_data['category_id']) {
		$category_options .= "<OPTION value='{$category_data['category_id']}' selected >{$category_data['category_name']}</OPTION>";
	} else {
		$category_options .= "<OPTION value='{$category_data['category_id']}'>{$category_data['category_name']}</OPTION>";
	}

}

//Get manufacturer List
$statement = "SELECT manufacturer_name, manufacturer_id
				FROM {$GLOBALS['db_prefix']}_products_manufacturers
				ORDER BY manufacturer_name";
$sql_manufacturer = $GLOBALS['dbCon']->prepare($statement);
$sql_manufacturer->execute();
$sql_manufacturer_data = $sql_manufacturer->fetchAll();
$sql_manufacturer->closeCursor();

foreach ($sql_manufacturer_data as $manufacturer_data) {
	if ($product_manufacturer == $manufacturer_data['manufacturer_id']) {
		$manufacturer_options .= "<OPTION value='{$manufacturer_data['manufacturer_id']}' selected >{$manufacturer_data['manufacturer_name']}</OPTION>";
	} else {
		$manufacturer_options .= "<OPTION value='{$manufacturer_data['manufacturer_id']}'>{$manufacturer_data['manufacturer_name']}</OPTION>";
	}
}

//Get application list
$statement = "SELECT application_name, application_id
				FROM {$GLOBALS['db_prefix']}_products_applications
				ORDER BY application_name";
$sql_application = $GLOBALS['dbCon']->prepare($statement);
$sql_application->execute();
$sql_application_data = $sql_application->fetchAll();
$sql_application->closeCursor();
foreach ($sql_application_data as $application_data) {
	if ($product_application == $application_data['application_id']) {
		$application_options .= "<OPTION value='{$application_data['application_id']}' selected >{$application_data['application_name']}</OPTION>";
	} else {
		$application_options .= "<OPTION value='{$application_data['application_id']}'>{$application_data['application_name']}</OPTION>";
	}
}

//Get and assemble page list
$statement = "SELECT page_name, page_title
				FROM {$GLOBALS['db_prefix']}_pages
				ORDER BY page_name";
$sql_pages = $GLOBALS['dbCon']->prepare($statement);
$sql_pages->execute();
$sql_pages_data = $sql_pages->fetchAll();
$sql_pages->closeCursor();
foreach ($sql_pages_data as $page_data) {
	$page_list .= "<tr>";
	$page_list .= "<td onclick=\"document.getElementById('text2').value=('{$page_data['page_name']}');document.getElementById('option_placeholder').style.display='none'\" onmouseout=\"this.style.backgroundColor='silver';this.style.color='#000000'\" onmouseover=\"this.style.backgroundColor='#316AC5';this.style.color='white'\" class='comboOption'>";
	$page_list .= "{$page_data['page_name']} - {$page_data['page_title']}";
	$page_list .= "</td></tr>";
}

//Replace Tags
$template = str_replace('<!-- category_options -->', $category_options, $template);
$template = str_replace('<!-- manufacturer_options -->', $manufacturer_options, $template);
$template = str_replace('<!-- application_options -->', $application_options, $template);
$template = str_replace('<!-- manufacturer_aware -->', $manufacturer_aware, $template);
$template = str_replace('<!-- category_aware -->', $category_aware, $template);
$template = str_replace('<!-- application_aware -->', $application_aware, $template);
$template = str_replace('<!-- link_id -->', $link_id, $template);
$template = str_replace('<!-- area_id -->', $_GET['Eid'], $template);
$template = str_replace('<!-- max_cols -->', $max_cols, $template);
$template = str_replace('<!-- max_products -->', $max_products, $template);
$template = str_replace('<!-- page_list -->', $page_list, $template);

echo $template;

?>
