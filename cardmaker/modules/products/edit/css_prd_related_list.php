<?php
/**
 * CSS editor for related products list
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Vars
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_{$_GET['Eid']}.css";
$template = '';
$css_data = '';
$has_template = false;

if (count($_POST) > 0) {
	$css_data = $_POST['css_data'];
	file_put_contents($file, $css_data);
}

//Get Preferences
$pref_data = get_area_data($area_id, 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);

//Has a template been selected
if (isset($preferences['template']) && !empty($preferences['template'])) {
	$has_template = true;
}

if ($has_template === true) {
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/css_prd_related_list.html');
	
	
	
	if (is_file($file)) {
		$css_data = file_get_contents($file);
	} else {
		file_put_contents($file, '');
	}

	if (empty($css_data) || isset($_GET['reload_default']) ) {
		//Get template file
		include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/products/layouts/product_related_list.php");
		foreach ($layout as $key=>$layout_values) {
			if (isset($layout_values['id']) && $layout_values['id'] == $preferences['template']) {
				$file = $layout_values['file_css'];
				break;
			}
		}
		$css_data = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/products/layouts/$file");
		
		$css_data = str_replace('_suffix', "_$area_id", $css_data);
		
	}

	//Replace Tags
	$template = str_replace('<!-- css_data -->', $css_data, $template);
	$template = str_replace('<!-- area_id -->', $area_id, $template);
} else {
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/css_prd_ne_template.html');
}

echo $template;
?>
