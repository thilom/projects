<?php

/** 
 *  Area content page for products text list. Content is auto-generated.
 *  Will generate a warning instead.
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id$
 */

echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/html/modules_auto_generated.html');
?>

