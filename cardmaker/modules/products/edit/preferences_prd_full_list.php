<?php

/**
 * Preferences for product text list
 *
 * @author Thilo Muller (2011)
 * @version $Id$
 */


//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$layout_list = '';
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/preferences_prd_full_list.html');
$category_options = '';
$category_link = '';
$product_category = '';
$manufacturer_options = '';
$product_manufacturer = '';
$manufacturer_aware = '';
$category_aware = '';
$application_options = '';
$product_application = '';
$application_aware = '';
$page_list = '';
$page_list2 = '';
$products_link = '';
$display_type = '';
$display_type_list = '<option value="">None</option>';
$display_types = array('lightbox_1'=>'Lightbox - Opens the image in a javascript lightbox (image block only)');

//Get current Prefs
$pref_data = get_area_data($area_id, 'latest', array('preferences'));

//Expand Prefs
if (!empty($pref_data['data']['preferences'])) {
	$prefs = explode('|', $pref_data['data']['preferences']);

	foreach ($prefs as $pref) {
		if (empty($pref)) continue;
		list($var, $value) = explode('=', $pref);
		switch ($var) {
			case 'category_link':
				$category_link = $value;
				break;
			case 'products_link':
				$products_link = $value;
				break;
			case 'display_type':
				$display_type = $value;
				break;
		}
	}
}

//Save
if (count($_POST) > 0) {
	//Concat prefs
	$prefs = "category_link={$_POST['category_link']}|";
	$prefs .= "products_link={$_POST['products_link']}|";
	$prefs .= "template={$_POST['layoutID']}|";
	$prefs .= "display_type={$_POST['display_type']}";

	if ($prefs != $pref_data['data']['preferences']) {
		$message = "&#187; Area Preferences Updated";

		//Save Data
		$fields['preferences'] = $prefs;
		update_area_data($area_id, 'draft', $fields);

		write_log("Preferences update for area (Draft)",'content_manager', $area_id);

	} else {
		$message = '&#187; No Changes, Nothing to update';
	}

	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button'  onClick='document.location=\"preferences_editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"' ><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}

//Get and assemble page list
$statement = "SELECT page_name, page_title
				FROM {$GLOBALS['db_prefix']}_pages
				ORDER BY page_name";
$sql_pages = $GLOBALS['dbCon']->prepare($statement);
$sql_pages->execute();
$sql_pages_data = $sql_pages->fetchAll();
$sql_pages->closeCursor();
foreach ($sql_pages_data as $page_data) {
	$page_list .= "<tr>";
	$page_list .= "<td onclick=\"document.getElementById('text2').value=('{$page_data['page_name']}');document.getElementById('option_placeholder').style.display='none'\" onmouseout=\"this.style.backgroundColor='silver';this.style.color='#000000'\" onmouseover=\"this.style.backgroundColor='#316AC5';this.style.color='white'\" class='comboOption'>";
	$page_list .= "{$page_data['page_name']} - {$page_data['page_title']}";
	$page_list .= "</td></tr>";

	$page_list2 .= "<tr>";
	$page_list2 .= "<td onclick=\"document.getElementById('text1').value=('{$page_data['page_name']}');document.getElementById('option_placeholder2').style.display='none'\" onmouseout=\"this.style.backgroundColor='silver';this.style.color='#000000'\" onmouseover=\"this.style.backgroundColor='#316AC5';this.style.color='white'\" class='comboOption'>";
	$page_list2 .= "{$page_data['page_name']} - {$page_data['page_title']}";
	$page_list2 .= "</td></tr>";
}

//Assemble display types
foreach ($display_types as $key=>$type) {
	$display_type_list .= "<option value='$key' ";
	$display_type_list .= $key==$display_type?"selected":"";
	$display_type_list .= " >$type</option>";
}

//Replace Tags
$template = str_replace('<!-- category_link -->', $category_link, $template);
$template = str_replace('<!-- products_link -->', $products_link, $template);
$template = str_replace('<!-- area_id -->', $_GET['Eid'], $template);
$template = str_replace('<!-- page_list -->', $page_list, $template);
$template = str_replace('<!-- page_list2 -->', $page_list2, $template);
$template = str_replace('<!-- display_select -->', $display_type_list, $template);

echo $template;

?>
