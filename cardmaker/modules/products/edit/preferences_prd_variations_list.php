<?php

/**
 * Preferences for related products list
 *
 * @author Thilo Muller (2011)
 * @version $Id$
 */


//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$layout_list = '';
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/products/html/preferences_prd_variations_list.html');

//Get current Prefs
$pref_data = get_area_data($area_id, 'latest', array('preferences'));

//Expand Prefs
if (!empty($pref_data['data']['preferences'])) {
	$prefs = explode('|', $pref_data['data']['preferences']);

	foreach ($prefs as $pref) {
		if (empty($pref)) continue;
		list($var, $value) = explode('=', $pref);
		switch ($var) {
			case 'hide_manufacturers':
				$hide_manufacturers = $value;
				break;
		}
	}
}

//Save
if (count($_POST) > 0) {
	//Concat prefs
	$prefs = "template={$_POST['layoutID']}|";

	if ($prefs != $pref_data['data']['preferences']) {
		$message = "&#187; Area Preferences Updated";

		//Save Data
		$fields['preferences'] = $prefs;
		update_area_data($area_id, 'draft', $fields);

		write_log("Preferences update for area (Draft)",'content_manager', $area_id);

	} else {
		$message = '&#187; No Changes, Nothing to update';
	}

	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button' onClick='document.location=\"preferences_editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"'><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}


//Replace Tags
$template = str_replace('<!-- area_id -->', $_GET['Eid'], $template);

echo $template;

?>
