<?php

/**
 * Edit/add product categories
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/edit_manufacturer_data.html');
$manufacturer_name = '';
$message = '';
$manufacturer_id = isset($_GET['manufacturer_id'])?(int)$_GET['manufacturer_id']:0;
$version = isset($_GET['ver'])?(int)$_GET['ver']:1;

if (count($_POST) > 0) {

	//Vars
	$manufacturer_name = $_POST['manufacturerName'];
	$manufacturer_count = 0;

	//Check if category exists
	$statement = "SELECT COUNT(*) AS count
					FROM {$GLOBALS['db_prefix']}_products_manufacturers
					WHERE manufacturer_name=:manufacturer_name";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bindParam(':manufacturer_name', $manufacturer_name);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();
	$manufacturer_count = $sql_count_data['count'];

	if ($manufacturer_count > 0) {
		$message = '&#187; Manufacturer already exists, cannot add or update';
	} else {

		if (isset($manufacturer_id) && $manufacturer_id > 0) { //Update Manufacturer
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products_manufacturers
						SET manufacturer_name=:manufacturer_name
						WHERE manufacturer_id=:manufacturer_id
						LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam('manufacturer_name', $manufacturer_name);
			$sql_update->bindParam('manufacturer_id', $manufacturer_id);
			$sql_update->execute();
			$sql_update->closeCursor();

			$message = "Manufacturer Updated";

		} else { //Insert Manufacturer

			$manufacturer_id = next_id("{$GLOBALS['db_prefix']}_products_manufacturers", 'manufacturer_id');

			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_manufacturers
								(manufacturer_id, manufacturer_name)
							VALUES
								(:manufacturer_id, :manufacturer_name)";
			$sql_insert = $GLOBALS['dbCon']->prepare($statement);
			$sql_insert->bindParam(':manufacturer_id', $manufacturer_id);
			$sql_insert->bindParam(':manufacturer_name', $manufacturer_name);
			$sql_insert->execute();
			$sql_insert->closeCursor();
			$message = "Manufacturer Created";

			$manufacturer_name = addslashes($manufacturer_name);
		}
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	if ($version == 2) {
		echo "<div class='bMsg'><button type='button' id='manufacturerSavedMain'><img src='/i/button/next_16.png'>Continue</button></div>";
	} else {
		echo "<div class='bMsg'><button type='button' id='manufacturerSaved'><img src='/i/button/next_16.png'>Continue</button></div>";
	}
	echo "<script type='text/javascript'>init(); manufacturerID='$manufacturer_id';manufacturerName='$manufacturer_name';</script>";

	die();
}

if (isset($manufacturer_id) && $manufacturer_id >0) {
	$statement = "SELECT manufacturer_name
				FROM {$GLOBALS['db_prefix']}_products_manufacturers
				WHERE manufacturer_id=:manufacturer_id
				LIMIT 1";
	$sql_manufacturer = $GLOBALS['dbCon']->prepare($statement);
	$sql_manufacturer->bindParam(':manufacturer_id', $manufacturer_id);
	$sql_manufacturer->execute();
	$sql_manufacturer_data = $sql_manufacturer->fetch();
	$sql_manufacturer->closeCursor();
	$manufacturer_name = $sql_manufacturer_data['manufacturer_name'];
}

//Replace Tags
$template = str_replace('<!-- manufacturer_name -->', $manufacturer_name, $template);

echo $template;
?>
