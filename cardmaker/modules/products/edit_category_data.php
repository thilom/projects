<?php

/**
 * Edit/add product categories
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//if (!isset($_GET['new'])) include 'category_toolbar.php';

//Vars
$template = file_get_contents('html/edit_category_data.html');
$category_name = '';
$message = '';
$category_id = isset($_GET['category_id'])?(int)$_GET['category_id']:0;
$category_list = '';

if (count($_POST) > 0) {

	//Vars
	$category_name = $_POST['categoryName'];
	$category_parent = $_POST['categoryParent'];
	$category_count = 0;

	//Check if category exists
	$statement = "SELECT COUNT(*) AS count
					FROM {$GLOBALS['db_prefix']}_products_categories
					WHERE category_name=:category_name";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bindParam(':category_name', $category_name);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();
	$category_count = $sql_count_data['count'];

//	if ($category_count > 0) {
//		$message = "&#187; Category already exists, cannot be created or updated.";
//	} else {

		if (isset($category_id) && $category_id > 0)  {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_products_categories
						SET category_name=:category_name, category_parent=:category_parent
						WHERE category_id=:category_id
						LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':category_name', $category_name);
			$sql_update->bindParam(':category_id',$category_id);
			$sql_update->bindParam(':category_parent',$category_parent);
			$sql_update->execute();
			$sql_update->closeCursor();

			$message = "&#187; Category updated";

		} else {

			$category_id = next_id("{$GLOBALS['db_prefix']}_products_categories", 'category_id');

			//Save Category
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_products_categories
								(category_id, category_name, category_parent)
							VALUES
								(:category_id, :category_name, :category_parent)";
			$sql_insert = $GLOBALS['dbCon']->prepare($statement);
			$sql_insert->bindParam(':category_id', $category_id);
			$sql_insert->bindParam(':category_name', $category_name);
			$sql_insert->bindParam(':category_parent', $category_parent);
			$sql_insert->execute();
			$sql_insert->closeCursor();
			$message = "&#187; Category Created";

			$category_name = addslashes($category_name);
		}
//	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script src='/shared/common.js' type='text/javascript'></script>";
	echo "<script type='text/javascript' src='/modules/products/js/products.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	if (isset($_GET['version']) && $_GET['version'] == 2) {
		echo "<div class='bMsg'><button type='button' id='categorySavedMain'><img src='/i/button/next_16.png'>Continue</button></div>";
	} else {
		echo "<div class='bMsg'><button type='button' id='categorySaved'><img src='/i/button/next_16.png'>Continue</button></div>";
	}

	echo "<script type='text/javascript'>init(); categoryID='$category_id';categoryName='$category_name';</script>";

	die();
}

if (isset($category_id) && $category_id > 0) {
	$statement = "SELECT category_name
				FROM {$GLOBALS['db_prefix']}_products_categories
				WHERE category_id=:category_id
				LIMIT 1";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);
	$sql_category->bindParam(':category_id', $category_id);
	$sql_category->execute();
	$sql_category_data = $sql_category->fetch();
	$sql_category->closeCursor();
	$category_name = $sql_category_data['category_name'];
}

//Get data
$statement = "SELECT category_id, category_name
			FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_parent = '0' OR category_parent IS NULL
			ORDER BY category_name";
$sql_categories = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) $sql_categories->bindParam(':filter_string', $filter_string);
$sql_categories->execute();
$sql_categories_data = $sql_categories->fetchAll();
$sql_categories->closeCursor();

//Prepare statement - product_count
$statement = "SELECT count(*) AS count
			FROM {$GLOBALS['db_prefix']}_products_category_bridge
			WHERE category_id = :category_id";
$sql_product = $GLOBALS['dbCon']->prepare($statement);

//Assemble Data
foreach ($sql_categories_data as $category_data) {

	//Vars
	$product_count = 0;

	//Product Count
	$sql_product->bindParam(':category_id', $category_data['category_id']);
	$sql_product->execute();
	$sql_product_data = $sql_product->fetch();
	$product_count = $sql_product_data['count'];

	$category_list .= "<option value='{$category_data['category_id']}'>{$category_data['category_name']}</option>";

	//Get sub categories
	$statement = "SELECT category_id, category_name
			FROM {$GLOBALS['db_prefix']}_products_categories
			WHERE category_parent = :category_parent
			ORDER BY category_name";
	$sql_subcat = $GLOBALS['dbCon']->prepare($statement);
	$sql_subcat->bindParam(':category_parent', $category_data['category_id']);
	$sql_subcat->execute();
	$sql_subcat_data = $sql_subcat->fetchAll();
	$sql_subcat->closeCursor();

	if (!empty($sql_subcat_data)) {
		foreach ($sql_subcat_data as $subcat_data) {

			//Product Count
			$sql_product->bindParam(':category_id', $subcat_data['category_id']);
			$sql_product->execute();
			$sql_product_data = $sql_product->fetch();
			$product_count = $sql_product_data['count'];

			$category_list .= "<option value='{$subcat_data['category_id']}'>{$category_data['category_name']} / {$subcat_data['category_name']}</option>";

			//Get sub categories
			$statement = "SELECT category_id, category_name
					FROM {$GLOBALS['db_prefix']}_products_categories
					WHERE category_parent = :category_parent
					ORDER BY category_name";
			$sql_subcat2 = $GLOBALS['dbCon']->prepare($statement);
			$sql_subcat2->bindParam(':category_parent', $subcat_data['category_id']);
			$sql_subcat2->execute();
			$sql_subcat2_data = $sql_subcat2->fetchAll();
			$sql_subcat2->closeCursor();

			if (!empty($sql_subcat2_data)) {
				foreach ($sql_subcat2_data as $subcat2_data) {

					//Product Count
					$sql_product->bindParam(':category_id', $subcat2_data['category_id']);
					$sql_product->execute();
					$sql_product_data = $sql_product->fetch();
					$product_count = $sql_product_data['count'];

//					$category_list .= "{$subcat2_data['category_id']}~~~{$subcat2_data['category_name']}~~~$product_count~~~2|||";

				}
			}

		}
	}
}

//Replace Tags
$template = str_replace('<!-- category_name -->', $category_name, $template);
$template = str_replace('<!-- category_list -->', $category_list, $template);

echo $template;
?>
