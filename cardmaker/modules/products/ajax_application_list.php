<?php

/**
 * Return a list of applications for javascript
 *
 * @author Thilo Muller (2012)
 * @version $Id$
 */

ob_start();

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$application_list = '';
$filter_string = isset($_GET['filter'])?"%{$_GET['filter']}%":'';
$start = (int) $_GET['start'];
$limit = (int) $_GET['length'];
$filter_count = '0';

//Get total application count
$statement = "SELECT COUNT(*) AS application_count
			FROM {$GLOBALS['db_prefix']}_products_applications";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$application_count = $sql_count_data['application_count'];

//Get filter count
if (!empty($filter_string)) {
	$statement = "SELECT COUNT(*) AS filter_count
				FROM {$GLOBALS['db_prefix']}_products_applications";
	$statement .= " WHERE application_name LIKE :filter_string";
	$statement .= "	ORDER BY application_name";
	$sql_count = $GLOBALS['dbCon']->prepare($statement);
	$sql_count->bindParam(':filter_string', $filter_string);
	$sql_count->execute();
	$sql_count_data = $sql_count->fetch();
	$sql_count->closeCursor();
	$filter_count = $sql_count_data['filter_count'];
} else {
	$filter_count = $application_count;
}

//Get data
$statement = "SELECT application_id, application_name
			FROM {$GLOBALS['db_prefix']}_products_applications";
if (!empty($filter_string)) $statement .= " WHERE application_name LIKE :filter_string";
$statement .= "	ORDER BY application_name
				LIMIT $start, $limit";
$sql_applications = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) $sql_applications->bindParam(':filter_string', $filter_string);
$sql_applications->execute();
$sql_applications_data = $sql_applications->fetchAll();
$sql_applications->closeCursor();

//Prepare statement - product_count
$statement = "SELECT count(*) AS count
			FROM {$GLOBALS['db_prefix']}_products_application_bridge
			WHERE application_id = :application_id";
$sql_product = $GLOBALS['dbCon']->prepare($statement);

//Assemble Data
foreach ($sql_applications_data as $application_data) {

	//Vars
	$product_count = 0;

	//Product Count
	$sql_product->bindParam(':application_id', $application_data['application_id']);
	$sql_product->execute();
	$sql_product_data = $sql_product->fetch();
	$product_count = $sql_product_data['count'];

	$application_list .= "{$application_data['application_id']}~~~{$application_data['application_name']}~~~$product_count|||";
}

//Assemble data
$application_list = "data~~~$application_count~~~$filter_count|||" . $application_list;

echo $application_list;

$list = ob_get_clean();

echo trim($list);


?>
