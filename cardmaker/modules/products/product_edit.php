<?php

/**
 * Edit/Add products
 *
 * @author Thilo Muller(2011)
 * @version $Id$
 */

if (isset($_GET['f'])) {
	switch ($_GET['f']) {
		case 'variation':
			echo "<script>document.location='edit_product_variation.php?W={$_GET['W']}";
			echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
			echo "'</script>";
			break;
		case 'variation2':
			echo "<script>document.location='edit_product_variation2.php?W={$_GET['W']}";
			echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
			echo "'</script>";
			break;
		case 'Media':
			echo "<script>document.location='edit_product_media.php?W={$_GET['W']}";
			echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
			echo "'</script>";
			break;
		case 'full':
			echo "<script>document.location='edit_product_full.php?W={$_GET['W']}";
			echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
			echo "'</script>";
			break;
		case 'associations':
			echo "<script>document.location='edit_product_association.php?W={$_GET['W']}";
			echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
			echo "'</script>";
			break;
		case 'variation':
			echo "<script>document.location='edit_product_variation.php?W={$_GET['W']}";
			echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
			echo "'</script>";
			break;
		default:
			echo "<script>document.location='edit_product_data.php?W={$_GET['W']}";
			echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
			echo isset($_GET['parent'])?"&parent={$_GET['parent']}":'';
			echo "'</script>";
			break;

	}

} else {
	echo "<script>document.location='edit_product_data.php?W={$_GET['W']}";
	echo isset($_GET['product_id'])?"&product_id={$_GET['product_id']}":'';
	echo isset($_GET['parent'])?"&parent={$_GET['parent']}":'';
	echo "'</script>";
}

?>
