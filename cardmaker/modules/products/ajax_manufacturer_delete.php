<?php

/**
 * Delete a manufacturer from the database
 *
 * @author Thilo Muller(2012)
 * @version $Id$
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$manufacturer_id = isset($_GET['manufacturer_id'])?(int)$_GET['manufacturer_id']:0;

//Remove from DB
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_products_manufacturers
			WHERE manufacturer_id=:manufacturer_id
			LIMIT 1";
$sql_delete = $GLOBALS['dbCon']->prepare($statement);
$sql_delete->bindParam('manufacturer_id', $manufacturer_id);
$sql_delete->execute();
$sql_delete->closeCursor();

?>
