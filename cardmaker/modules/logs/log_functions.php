<?php
/**
 * Various functions to manage log entries
 *
 * @version $Id: log_functions.php 118 2011-12-11 06:53:50Z thilo $
 * @author Thilo Muller(2011)
 */

/**
 * Write an entry to the log table
 * @param string $message
 * @param string $module_id usully $security_id from module.ini.php
 * @param string $target What the entry relates to. eg. for the users module it will be the user ID
 * @param string $alt_target second entry for what the entry relates to.
 * @param int $user_id The user who is responsible for the entry
 */
function write_log($message, $module_id, $target = '', $alt_target='', $user_id='') {
	if (empty($user_id)) $user_id = $_SESSION['dbweb_user_id'];

	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_logs (log_timestamp, user_id, log_message, module_id, target, alt_target) VALUES (NOW(), :user_id, :log_message, :module_id, :target, :alt_target)";
	$sql_log_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_log_insert->bindParam(':user_id', $user_id);
	$sql_log_insert->bindParam(':log_message', $message);
	$sql_log_insert->bindParam(':module_id', $module_id);
	$sql_log_insert->bindParam(':target', $target);
	$sql_log_insert->bindParam(':alt_target', $alt_target);
	$sql_log_insert->execute();
	$sql_log_insert->closeCursor();

}

?>