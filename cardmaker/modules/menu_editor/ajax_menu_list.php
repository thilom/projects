<?php

/**
 * AJAX menu item list
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$menu_list = "<table class='tbl_list' style='border-collapse: collapse'><tr><th>Menu Structure</th></tr>";
$counter = 0;


//Get first levels
$statement = "SELECT menu_id, menu_name, menu_link
				FROM {$GLOBALS['db_prefix']}_menu
				WHERE parent_id=0
				ORDER BY menu_position";
$sql_first = $GLOBALS['dbCon']->prepare($statement);
$sql_first->execute();
$sql_first_data = $sql_first->fetchAll();
$sql_first->closeCursor();

foreach ($sql_first_data as $first_data) {

	//Get second level
	$statement = "SELECT menu_id, menu_name, menu_link, product_categories
				FROM {$GLOBALS['db_prefix']}_menu
				WHERE parent_id=:parent_id
				ORDER BY menu_position";
	$sql_second = $GLOBALS['dbCon']->prepare($statement);
	$sql_second->bindParam(':parent_id', $first_data['menu_id']);
	$sql_second->execute();
	$sql_second_data = $sql_second->fetchAll();
	$sql_second->closeCursor();

	//Check for category items
	$show_append = true;
	foreach ($sql_second_data as $second_data) {
		if ($second_data['product_categories'] == 'Y') {
			$show_append = false;
		}
	}

	$menu_list .= "<tr><td style='padding: 0;'>";
	$menu_list .= "<span onclick='editItem({$first_data['menu_id']})' class='menuItem'>{$first_data['menu_name']}</span>";
	if ($show_append === true) $menu_list .= "<span class='plusButton' onclick='editItem(0,{$first_data['menu_id']})'>[Append]</span>";
	$menu_list .= "<span class='removeButton' onclick='deleteItemForm({$first_data['menu_id']})'>[Remove]</span>";
	$menu_list .= "</td></tr>";


	//Assemble Secord Level
	foreach ($sql_second_data as $key=>$second_data) {

		//Get third level
		$statement = "SELECT menu_id, menu_name, menu_link, product_categories
				FROM {$GLOBALS['db_prefix']}_menu
				WHERE parent_id=:parent_id
				ORDER BY menu_position";
		$sql_third = $GLOBALS['dbCon']->prepare($statement);
		$sql_third->bindParam(':parent_id', $second_data['menu_id']);
		$sql_third->execute();
		$sql_third_data = $sql_third->fetchAll();
		$sql_third->closeCursor();

		//Check for category items
		$show_append = true;
		foreach ($sql_third_data as $third_data) {
			if ($third_data['product_categories'] == 'Y') {
				$show_append = false;
			}
		}

		$menu_list .= "<tr><td style='padding: 0;'>";
		if (count($sql_second_data)-1 == $key) {
			$menu_list .= "<img src='/i/connection_lvl1_end.png' style='margin: 0; padding: 0;vertical-align: middle'>";
		} else {
			$menu_list .= "<img src='/i/connection_lvl1.png' style='margin: 0; padding: 0;vertical-align: middle'>";
		}

		$menu_list .= "<span onclick='editItem({$second_data['menu_id']})' class='menuItem'>{$second_data['menu_name']}</span>";
		if ($second_data['product_categories'] != 'Y' || $show_append === false) {
			$menu_list .= "<span class='plusButton' onclick='editItem(0,{$second_data['menu_id']})'>[Append]</span>";
		}
		$menu_list .= "<span class='removeButton' onclick='deleteItemForm({$second_data['menu_id']})'>[Remove]</span>";
		$menu_list .= "</td></tr>";

		//Assemble third level
		foreach ($sql_third_data as $third_key=>$third_data) {

			$menu_list .= "<tr><td style='padding: 0;'>";
			if (count($sql_third_data)-1 == $third_key) {
				if (count($sql_second_data)-1 != $key) {
					$menu_list .= "<img src='/i/connection_straight.png' style='margin: 0; padding: 0;vertical-align: middle'>";
				} else {
					$menu_list .= "<img src='/i/connection_blank.png' style='margin: 0; padding: 0;vertical-align: middle'>";
				}
				$menu_list .= "<img src='/i/connection_lvl1_end.png' style='margin: 0; padding: 0;vertical-align: middle'>";
			} else {
				if (count($sql_second_data)-1 != $key) {
					$menu_list .= "<img src='/i/connection_straight.png' style='margin: 0; padding: 0;vertical-align: middle'>";
				} else {
					$menu_list .= "<img src='/i/connection_blank.png' style='margin: 0; padding: 0;vertical-align: middle'>";
				}
				$menu_list .= "<img src='/i/connection_lvl1.png' style='margin: 0; padding: 0;vertical-align: middle'>";
			}

			$menu_list .= "<span onclick='editItem({$third_data['menu_id']})' class='menuItem'>{$third_data['menu_name']}</span>";
			$menu_list .= "<span class='removeButton' onclick='deleteItemForm({$third_data['menu_id']})'>[Remove]</span>";
			$menu_list .= "</td></tr>";



		}




	}

	$menu_list .= "</li>";
}

$menu_list .= "<tr><td style='padding: 0px; text-align: center'><button style='width: 90%; height: 15px' onclick='editItem(0,0)'>Add Top-level Item</button></td></tr>";
$menu_list .= "</table>";


//Get menu list
//get_list(&$menu_list, 1, 2);

//var_dump($menu_list);
echo $menu_list;


function get_list($menu_list, $menu_group, $sub_count, $level=0, $parent_id=0) {
	global $counter;

	$statement = "SELECT menu_id, menu_name, parent_id
					FROM {$GLOBALS['db_prefix']}_menu
					WHERE menu_group=:menu_group AND parent_id=:parent_id
					ORDER BY menu_position";
	$sql_link = $GLOBALS['dbCon']->prepare($statement);
	$sql_link->bindParam(':menu_group', $menu_group);
	$sql_link->bindParam(':parent_id', $parent_id);
	$sql_link->execute();
	$sql_link_data = $sql_link->fetchAll();
	$sql_link->closeCursor();

	$item_count = 0;
	foreach ($sql_link_data as $link_data) {
		//Count sub items
		$statement = "SELECT COUNT(*) AS sub_count
						FROM {$GLOBALS['db_prefix']}_menu
						WHERE parent_id=:parent_id";
		$sql_sub = $GLOBALS['dbCon']->prepare($statement);
		$sql_sub->bindParam(':parent_id', $link_data['menu_id']);
		$sql_sub->execute();
		$sql_sub_data = $sql_sub->fetch();
		$sql_sub->closeCursor();

		$menu_list[$counter] = array();
		$menu_list[$counter]['menu_id'] = $link_data['menu_id'];
		$menu_list[$counter]['menu_name'] = $link_data['menu_name'];
		$menu_list[$counter]['parent_id'] = $link_data['parent_id'];
		$menu_list[$counter]['level'] = $level;
		$menu_list[$counter]['sub_count'] = $sql_sub_data['sub_count'];

		$item_count++;

		$menu_list[$counter]['item_count'] = $item_count;
		if ($item_count == $sub_count) {
			$menu_list[$counter]['end'] = 0;
		} else {
			$menu_list[$counter]['end'] = 1;
		}

		$counter++;


		get_list(&$menu_list, $menu_group, $sql_sub_data['sub_count'], $level+1, $link_data['menu_id']);

	}
}



?>
