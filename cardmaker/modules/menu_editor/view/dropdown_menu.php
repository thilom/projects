<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



//Vars
$menu_list = "<ul id='nav' class='dropdown dropdown-horizontal'>";

//Get first levels
$statement = "SELECT menu_id, menu_name, menu_link
				FROM {$GLOBALS['db_prefix']}_menu
				WHERE parent_id=0
				ORDER BY menu_position";
$sql_first = $GLOBALS['dbCon']->prepare($statement);
$sql_first->execute();
$sql_first_data = $sql_first->fetchAll();
$sql_first->closeCursor();

foreach ($sql_first_data as $first_data) {

	//Get second level
	$statement = "SELECT menu_id, menu_name, menu_link, product_categories
				FROM {$GLOBALS['db_prefix']}_menu
				WHERE parent_id=:parent_id
				ORDER BY menu_position";
	$sql_second = $GLOBALS['dbCon']->prepare($statement);
	$sql_second->bindParam(':parent_id', $first_data['menu_id']);
	$sql_second->execute();
	$sql_second_data = $sql_second->fetchAll();
	$sql_second->closeCursor();

	$menu_list .= "<li class='level1_$id' ";
	if (count($sql_second_data) > 0) {
		$menu_list .= " class='dir' ";
	}
	$menu_list .= "><a href='{$first_data['menu_link']}'>{$first_data['menu_name']}</a>";

	//Assemble Secord Level
	foreach ($sql_second_data as $key=>$second_data) {

		//Attach first level categories
		if ($second_data['product_categories'] == 'Y') {
			$statement = "SELECT category_name, category_id
							FROM {$GLOBALS['db_prefix']}_products_categories
							WHERE category_parent = '' OR category_parent IS NULL
							ORDER BY category_name";
			$sql_product = $GLOBALS['dbCon']->prepare($statement);
			$sql_product->execute();
			$sql_product_data = $sql_product->fetchAll();
			$sql_product->closeCursor();

			//Assemble third level
			foreach ($sql_product_data as $product_key=>$product_data) {
				if ($product_key == 0) {
					$menu_list .=  "<ul>";
				}

				$menu_list .=  "<li class='level2_$id' ><a href='{$product_data['category_id']}'>{$product_data['category_name']}</a>";
				$menu_list .=  "</li>";

				if ($product_key == count($sql_product_data)-1) {
					$menu_list .=  "</ul>";
				}

			}

			$menu_list .=  "</li>";

		} else {

			//Get third level
			$statement = "SELECT menu_id, menu_name, menu_link, product_categories
					FROM {$GLOBALS['db_prefix']}_menu
					WHERE parent_id=:parent_id
					ORDER BY menu_position";
			$sql_third = $GLOBALS['dbCon']->prepare($statement);
			$sql_third->bindParam(':parent_id', $second_data['menu_id']);
			$sql_third->execute();
			$sql_third_data = $sql_third->fetchAll();
			$sql_third->closeCursor();

			if ($key == 0) {
				$menu_list .=  "<ul>";
			}

			$menu_list .=  "<li class='level2_$id' ";
			if (count($sql_third_data) > 0) {
					$menu_list .= " class='dir' ";
				}
			$menu_list .= "><a href='{$second_data['menu_link']}'>{$second_data['menu_name']}</a>";

			//Assemble third level
			foreach ($sql_third_data as $third_key=>$third_data) {

				if ($third_data['product_categories'] == 'Y') {
					$statement = "SELECT category_name, category_id
									FROM {$GLOBALS['db_prefix']}_products_categories
									WHERE category_parent = '' OR category_parent IS NULL
									ORDER BY category_name";
					$sql_product = $GLOBALS['dbCon']->prepare($statement);
					$sql_product->execute();
					$sql_product_data = $sql_product->fetchAll();
					$sql_product->closeCursor();

					//Assemble third level
					foreach ($sql_product_data as $product_key=>$product_data) {
						if ($product_key == 0) {
							$menu_list .=  "<ul>";
						}

						$menu_list .=  "<li class='level3_$id'><a href='{$product_data['category_id']}'>{$product_data['category_name']}</a>";
						$menu_list .=  "</li>";

						if ($product_key == count($sql_product_data)-1) {
							$menu_list .=  "</ul>";
						}

					}

					$menu_list .=  "</li>";

				} else {
						if ($third_key == 0) {
							$menu_list .=  "<ul>";
						}

						$menu_list .=  "<li class='level3_$id'><a href='{$third_data['menu_link']}'>{$third_data['menu_name']}</a>";
						$menu_list .=  "</li>";

						if ($third_key == count($sql_third_data)-1) {
							$menu_list .=  "</ul>";
						}
				}

			}

			$menu_list .=  "</li>";

			if ($key == count($sql_second_data)-1) {
				$menu_list .=  "</ul>";
			}
		}

	}

	$menu_list .= "</li>";
}

//get_list(&$menu_list,1,0);
$menu_list .= "</ul>";

echo "<link href='/shared/drop-down-menu_v1.3/css/dropdown/dropdown.vertical.css' media='screen' rel='stylesheet' type='text/css' />";
//Get CSS file
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_$id.css";
if (is_file($file)) {
	echo "<link rel=stylesheet href='/styles/css_$id.css' media='screen' rel='stylesheet' type='text/css'>";
} else {
	echo "<link rel=stylesheet href='/modules/menu_editor/css/default_menu.css' media='screen' rel='stylesheet' type='text/css'>";
}
echo $menu_list;

function get_list($menu_list, $menu_group, $current_parent, $level=0, $parent_id=0) {
	global $counter;

	$statement = "SELECT menu_id, menu_name, parent_id
					FROM {$GLOBALS['db_prefix']}_menu
					WHERE menu_group=:menu_group AND parent_id=:parent_id
					ORDER BY menu_position";
	$sql_link = $GLOBALS['dbCon']->prepare($statement);
	$sql_link->bindParam(':menu_group', $menu_group);
	$sql_link->bindParam(':parent_id', $parent_id);
	$sql_link->execute();
	$sql_link_data = $sql_link->fetchAll();
	$sql_link->closeCursor();

	$item_count = 0;
	foreach ($sql_link_data as $link_data) {
		//Count sub items
		$statement = "SELECT COUNT(*) AS sub_count
						FROM {$GLOBALS['db_prefix']}_menu
						WHERE parent_id=:parent_id";
		$sql_sub = $GLOBALS['dbCon']->prepare($statement);
		$sql_sub->bindParam(':parent_id', $link_data['menu_id']);
		$sql_sub->execute();
		$sql_sub_data = $sql_sub->fetch();
		$sql_sub->closeCursor();

		switch ($level) {
			case 0:
				$menu_list .= "" . PHP_EOL;
				break;
			case 1:
				$menu_list .= "<ul class='dir'>" . PHP_EOL;
				break;
			case 2:
				$menu_list .= "<ul>" . PHP_EOL;
				break;
		}


		$menu_list .= "<li><a href=''>{$link_data['menu_name']}</a>";

		$item_count++;




//		$menu_list .= "</li>" . PHP_EOL;
		$counter++;


		get_list(&$menu_list, $menu_group, $current_parent, $level+1, $link_data['menu_id']);

		if ($level != 0) {
			$menu_list .= "</li></ul>" . PHP_EOL;
		} else {
			$menu_list .= "</li>" . PHP_EOL;
		}
	}
}
?>
