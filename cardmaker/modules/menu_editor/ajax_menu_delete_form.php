<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$template = file_get_contents('html/menu_delete_form.html');
$menu_id = (int) $_GET['itemID'];
$menu_name = '';

//Get Item Data
$statement = "SELECT menu_id, menu_name, menu_link, parent_id
				FROM {$GLOBALS['db_prefix']}_menu
				WHERE menu_id=:menu_id
				LIMIT 1";
$sql_menu = $GLOBALS['dbCon']->prepare($statement);
$sql_menu->bindParam(':menu_id', $menu_id);
$sql_menu->execute();
$sql_menu_data = $sql_menu->fetch();
$sql_menu->closeCursor();

$menu_name = $sql_menu_data['menu_name'];


//Replace tags
$template = str_replace('<!-- menu_name -->', $menu_name, $template);
$template = str_replace('<!-- menu_id -->', $menu_id, $template);

echo $template;

?>
