

/**
 * Get menu list
 */
function getMenu() {

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=drawMenuElements;

	Url = "/modules/menu_editor/ajax_menu_list.php?menu_group=1";

	xmlhttp.open("GET",Url,false);
	xmlhttp.send();
}

/**
 * Draw menu items
 */
function drawMenuElements() {
	if (xmlhttp.readyState==4 && xmlhttp.status==200) {
//		var menuHtml = "<table class='tbl_list' style='border-collapse: collapse'><tr><th>Menu Structure</th></tr>";
//		var menuArray = JSON.parse(xmlhttp.responseText);
//
//		for (var i in menuArray) {
//			menuHtml += "<tr style=''><td style='padding: 0;'";
//			menuHtml += "  ";
//			menuHtml +="><span onclick='editItem("+ menuArray[i].menu_id +")' class='menuItem'>";
//			if (menuArray[i].level == 2) menuHtml += "<img src='/i/connection_straight.png' style='margin: 0; padding: 0;vertical-align: middle'>";
//			if (menuArray[i].level == 1) {
//				if (menuArray[i].end == 0) {
//					menuHtml += "<img src='/i/connection_lvl1_end.png' style='margin: 0; padding: 0;vertical-align: middle'>";
//				} else {
//					menuHtml += "<img src='/i/connection_lvl1.png' style='margin: 0; padding: 0;vertical-align: middle'>";
//				}
//
//			}
//			if (menuArray[i].level == 2) {
//				if (menuArray[i].end == 0) {
//					menuHtml += "<img src='/i/connection_lvl1_end.png' style='margin: 0; padding: 0;vertical-align: middle'>";
//				} else {
//					menuHtml += "<img src='/i/connection_lvl1.png' style='margin: 0; padding: 0;vertical-align: middle'>";
//				}
//			}
//
//
//
//			menuHtml += menuArray[i].menu_name + "</span>";
//
//			if (menuArray[i].level != 2) {
//				menuHtml += "<span class='plusButton' onclick='editItem(0,"+ menuArray[i].menu_id +")'>[Append]</span>";
//			}
//			menuHtml += "<span class='removeButton' onclick='deleteItemForm("+ menuArray[i].menu_id +")'>[Remove]</span>";
//
//			menuHtml += "</td></tr>";
//		}
//
//		menuHtml += "<tr><td style='padding: 0px; text-align: center'><button style='width: 90%; height: 15px' onclick='editItem(0,0)'>+</button></td></tr>";
//		menuHtml += "</table>";

		document.getElementById('menuList').innerHTML = xmlhttp.responseText;
	}

}


/**
 * get data to edit menu item
 */
function editItem(itemID,parent) {
	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function () {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById('menuItemEdit').innerHTML = xmlhttp.responseText;
		}
	};

	Url = "/modules/menu_editor/ajax_menu_edit.php?itemID=" + itemID;
	if (parent) {
		Url += "&parent=" + parent;
	}

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}


/**
 * Submit form data for saving
 */
function submitItem(menuID, menuGroup) {
	var menuName = document.getElementById('menuName').value;
	var menuLink = document.getElementById('menuLink').value;
	var menuParent = document.getElementById('parent').value;
	var attachProduct = (document.getElementById('attachProduct') && document.getElementById('attachProduct').checked)?'Y':'N';

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function () {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var menuID2 = xmlhttp.responseText;
			getMenu();
			editItem(menuID2);
		}
	};

	Url = "/modules/menu_editor/ajax_menu_save.php?itemID=" + menuID;
	Url += "&menuName=" + menuName;
	Url += "&menuLink=" + menuLink;
	Url += "&menuParent=" + menuParent;
	Url += "&menuGroup=" + menuGroup;
	Url += "&attachProduct=" + attachProduct;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Delete a menu item
 */
function deleteItemForm(itemID) {
	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function () {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById('menuItemEdit').innerHTML = xmlhttp.responseText;
		}
	};

	Url = "/modules/menu_editor/ajax_menu_delete_form.php?itemID=" + itemID;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Delete an item from the DB
 */
function deleteItem(itemID) {

	if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function () {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			getMenu();
			document.getElementById('menuItemEdit').innerHTML = '';
		}
	};

	Url = "/modules/menu_editor/ajax_menu_delete.php?itemID=" + itemID;

	xmlhttp.open("GET",Url,true);
	xmlhttp.send();
}

/**
 * Get and replace the current CSS data with the defaults
 */
function get_css_default(f, areaID) {
	if (confirm('WARNING!\n\nThis process will replace the current data\n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				al = xmlhttp.responseText;
				document.getElementById('css_data').value = al
			}
		}
		xmlhttp.open("GET","/modules/menu_editor/ajax_default_css.php?file="+f+"&area_id=" + areaID,true);
		xmlhttp.send();
	}
}

