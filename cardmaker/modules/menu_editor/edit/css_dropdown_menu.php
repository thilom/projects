<?php
/**
 * CSS editor for drop-down menu
 *
 * @author Thilo Muller(2011)
 * @version $Id: css_places_bio.php 87 2011-09-14 06:45:49Z thilo $
 */

//Vars
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_{$_GET['Eid']}.css";
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/menu_editor/html/css_menu.html');
$css_data = '';

if (count($_POST) > 0) {
	$css_data = $_POST['css_data'];
	file_put_contents($file, $css_data);
}

if (is_file($file)) {
	$css_data = file_get_contents($file);
} else {
	file_put_contents($file, '');
}

if (empty($css_data) || isset($_GET['reload_default']) ) {
	$css_data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/menu_editor/css/default_menu.css');
}

//Replace Tags
$template = str_replace('<!-- css_data -->', $css_data, $template);
$template = str_replace('<!-- area_id -->', $_GET['Eid'], $template);

echo $template;
?>
