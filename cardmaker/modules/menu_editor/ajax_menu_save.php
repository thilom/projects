<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$menu_name = $_GET['menuName'];
$menu_link = $_GET['menuLink'];
$parent_id = $_GET['menuParent'];
$menu_id = (int) $_GET['itemID'];
$menu_group = isset($_GET['menuGroup'])?(int) $_GET['menuGroup']:'';

//Save Data
if ($menu_id == 0) {
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_menu
						(menu_name, menu_link, parent_id, menu_group)
					VALUES
						(:menu_name, :menu_link, :parent_id, :menu_group)";
	$sql_save = $GLOBALS['dbCon']->prepare($statement);
	$sql_save->bindParam(':menu_name', $menu_name);
	$sql_save->bindParam(':menu_link', $menu_link);
	$sql_save->bindParam(':parent_id', $parent_id);
	$sql_save->bindParam(':menu_group', $menu_group);
	$sql_save->execute();
	$menu_id = $GLOBALS['dbCon']->lastInsertId();
} else {
	$statement = "UPDATE {$GLOBALS['db_prefix']}_menu
					SET menu_name=:menu_name,
						menu_link=:menu_link,
						parent_id=:parent_id
					WHERE menu_id=:menu_id
					LIMIT 1";
	$sql_save = $GLOBALS['dbCon']->prepare($statement);
	$sql_save->bindParam(':menu_name', $menu_name);
	$sql_save->bindParam(':menu_link', $menu_link);
	$sql_save->bindParam(':menu_id', $menu_id);
	$sql_save->bindParam(':parent_id', $parent_id);
	$sql_save->execute();
}

if (isset($_GET['attachProduct']) && $_GET['attachProduct'] == 'Y') {
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_menu
						(menu_name, menu_link, parent_id, menu_group, product_categories)
					VALUES
						('&laquo;product categories&raquo;', '', :parent_id, :menu_group, 'Y')";
	$sql_attach = $GLOBALS['dbCon']->prepare($statement);
	$sql_attach->bindParam(':parent_id', $menu_id);
	$sql_attach->bindParam(':menu_group', $menu_group);
	$sql_attach->execute();
}
echo $menu_id;
?>