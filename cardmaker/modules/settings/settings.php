<?php
/**
 * Settings for Scrum. Gets a list of settings files from modules.ini.php and includes each one.
 *
 * @author Thilo Muller(2011)
 * @version $Id: settings.php 139 2012-03-02 19:45:57Z thilo $
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/settings/init.php';
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/modules/content_manager/functions.php';
require_once SITE_ROOT . '/shared/file_man.class.php';

//Vars
$module_data = get_module_variables();
$page = isset($_GET['d'])?$_GET['d']:'';
$menu_footer = "<td class='menu_drop'></td>";

//Menubar
foreach ($module_data as $data) {
	if (isset($data['settings'])) {
		 foreach ($data['settings'] as $settings_data) {
			$settings[$settings_data['name']] = $settings_data['file'];
			$settings_order[$settings_data['name']] = $settings_data['order'];
		 }
	}
}
asort($settings_order);
$menu_bar = "<table id=menu_bar><tr>";
foreach ($settings_order as $settings_name=>$dummy) {
	if (empty($page)) $page = $settings[$settings_name];
	$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='/modules/settings/settings.php?d={$settings[$settings_name]}'\">$settings_name</td>";
	$menu_footer .= "<td class='menu_drop'></td>";
}
$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'>$menu_footer</tr>";
$menu_bar .= "<table>";
echo "<div id=top_menu >$menu_bar</div>";

//Include Page
include $_SERVER['DOCUMENT_ROOT'] . "/modules/$page";

?>
