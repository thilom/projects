<?php
/** 
 * Text editor settings
 *
 * @author Thlo Muller(2011)
 * @version $Id: wysiwyg_settings.php 48 2011-05-06 03:47:20Z thilo $
 */

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/settings/html/wysiwyg_settings.html');
$message = '';

if (count($_POST)>0) {

	$settings = get_settings();

	//Prepare statement - Setting Update
	$statement = "UPDATE {$GLOBALS['db_prefix']}_settings SET value=:value WHERE tag=:tag";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Setting Check
	$statement = "SELECT COUNT(*) AS c FROM {$GLOBALS['db_prefix']}_settings WHERE tag=:tag";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Setting Insert
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings (tag, value) VALUES (:tag,:value)";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);

	//Save general settings
	$tag = "content_manager_no_css";
	if (!isset($_POST['hide_css_editor'])) $_POST['hide_css_editor'] = '';
	if ($settings[$tag] != $_POST['hide_css_editor']) {
		$sql_check->bindParam(':tag', $tag);
		$sql_check->execute();
		$sql_data = $sql_check->fetch();
		$sql_check->closeCursor();
		if ($sql_data['c'] == 0) {
			$sql_insert->bindParam(':value', $_POST['hide_css_editor']);
			$sql_insert->bindParam(':tag', $tag);
			$sql_insert->execute();
		} else {
			$sql_update->bindParam(':tag', $tag);
			$sql_update->bindParam(':value', $_POST['hide_css_editor']);
			$sql_update->execute();
		}

		$message .= "&#187; General settings updated<br>";
	}

	//Save WYSIWYG settings
	$wysiwyg = '';
	$group = '';
	$last_entry = '';
	$group_count = 0;
	$line_total = 0;
	foreach ($_POST['buttons'] as $data) {
		 if ($data == '|') {
			if ($line_total + $group_count > 40) {
				$wysiwyg .= '"BRK",';
				$line_total = 0;
			}
			$line_total += $group_count;
			$wysiwyg .= $group ;
			$group = '';
			$group_count = 0;
		 }
		 if ($last_entry != $data) {
			 $group .= "\"$data\",";
			 $group_count++;
		 }
		$last_entry = $data;
	}
//	$wysiwyg = substr($wysiwyg, 0, -5);
	$tag = "content_manager_wysiwyg";
	if ($settings[$tag] != $wysiwyg) {
		$sql_check->bindParam(':tag', $tag);
		$sql_check->execute();
		$sql_data = $sql_check->fetch();
		$sql_check->closeCursor();
		if ($sql_data['c'] == 0) {
			$sql_insert->bindParam(':value', $wysiwyg);
			$sql_insert->bindParam(':tag', $tag);
			$sql_insert->execute();
		} else {
			$sql_update->bindParam(':tag', $tag);
			$sql_update->bindParam(':value', $wysiwyg);
			$sql_update->execute();
		}
		
		$message .= "&#187; WYSIWYG settings updated<br>";
	}

	if (empty($message)) {
		$message = "&#187; No Changes, Nothing to update";
	} else {
		write_log("Content manager settings updated", 'content_manager');
	}
	echo "<br><br><div class='dMsg'>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/settings/settings.php?d=settings/settings/wysiwyg_settings.php\" ></div>";

	die();
}

$settings = get_settings();

//WYSIWYG settings
$wysiwyg = str_replace('"', '', $settings['content_manager_wysiwyg']);
$wysiwyg = str_replace('|,', '', $wysiwyg);
$wysiwyg = str_replace('BRK,', '', $wysiwyg);

//CSS Setting
$css_setting = $settings['content_manager_no_css']=='Y'?'checked':'';

//Replace tags
$template = str_replace('<!-- hide_css -->', $css_setting, $template);
$template = str_replace('!wysiwyg!', $wysiwyg, $template);

echo $template;

/**
 * Returns the current content manager settings
 */
function get_settings() {
	//Get current settings
	$statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
	$sql_settings = $GLOBALS['dbCon']->prepare($statement);
	$sql_settings->execute();
	$sql_data = $sql_settings->fetchAll();
	$sql_settings->closeCursor();

	foreach ($sql_data as $key=>$value) {
		switch($value['tag']) {
			case 'content_manager_no_css':
				$settings[$value['tag']] = $value['value'];
				break;
			case 'content_manager_wysiwyg':
				$settings[$value['tag']] = $value['value'];
				break;
		}
	}

	return $settings;
}
?>
