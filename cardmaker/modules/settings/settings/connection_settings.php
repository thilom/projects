<?php
/**
 * General scrum settings
 */

//Includes
include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php');

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/settings/html/connection_settings.html');
$ftp_updated = FALSE;
$ftp_server = '';
$ftp_username = '';
$ftp_password = '';
$ftp_root = '/';

//Get current FTP & Currency Details
$statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
$sql_ftp = $GLOBALS['dbCon']->prepare($statement);
$sql_ftp->execute();
$ftp_data = $sql_ftp->fetchAll();
$sql_ftp->closeCursor();
foreach ($ftp_data as $data) {
	switch ($data['tag']) {
		case 'ftp_server':
			$ftp_server = $data['value'];
			break;
		case 'ftp_username':
			$ftp_username = $data['value'];
			break;
		case 'ftp_password':
			$ftp_password = $data['value'];
			break;
		case 'ftp_root':
			$ftp_root = $data['value'];
			break;
	}
}

if (count($_POST) > 0) {
	$message = '';

	//Prepare statement - FTP Details Update
	$statement = "UPDATE {$GLOBALS['db_prefix']}_settings SET value=:value WHERE tag=:tag";
	$sql_ftp_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - FTP Check
	$statement = "SELECT COUNT(*) AS c FROM {$GLOBALS['db_prefix']}_settings WHERE tag=:tag";
	$sql_ftp_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - FTP Details Insert
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings (tag, value) VALUES (:tag,:value)";
	$sql_ftp_insert = $GLOBALS['dbCon']->prepare($statement);

	//Save FTP server data
	if ($_POST['ftp_server'] != $ftp_server) {
		$tag = 'ftp_server';

		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['ftp_server']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['ftp_server']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$ftp_updated = TRUE;
	}

	//Save FTP username data
	if ($_POST['ftp_username'] != $ftp_username) {
		$tag = 'ftp_username';

		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['ftp_username']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['ftp_username']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$ftp_updated = TRUE;
	}

	//Save FTP password data
	if ($_POST['ftp_password'] != $ftp_password) {
		$tag = 'ftp_password';

		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['ftp_password']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['ftp_password']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$ftp_updated = TRUE;
	}

	//Save FTP root data
	if ($_POST['ftp_root'] != $ftp_root) {
		$tag = 'ftp_root';

		$sql_ftp_check->bindParam(':tag',$tag);
		$sql_ftp_check->execute();
		$sql_check_data = $sql_ftp_check->fetch(PDO::FETCH_ASSOC);
		$count = $sql_check_data['c'];

		if ($count == 0) {
			$sql_ftp_insert->bindParam(':tag', $tag);
			$sql_ftp_insert->bindParam(':value', $_POST['ftp_root']);
			$sql_ftp_insert->execute();
		} else {
			$sql_ftp_update->bindParam(':value', $_POST['ftp_root']);
			$sql_ftp_update->bindParam(':tag', $tag);
			$sql_ftp_update->execute();
		}

		$ftp_updated = TRUE;
	}

	//FTP update log & message
	if ($ftp_updated === TRUE) {
		$message .= "&#187; Updated FTP connection data";
		write_log("Updated FTP connection data", $GLOBALS['security_id']);
	}

	//Save DB Settings
	if ($_POST['database_server'] != $GLOBALS['db_server'] || $_POST['database_name'] != $GLOBALS['db_name'] || $_POST['database_username'] != $GLOBALS['db_username'] || $_POST['database_password'] != $GLOBALS['db_password']) {

		$fm = new file_man();
		$fm->chmod_file('/settings/database.php', 0777);
		echo $fm->get_error();
		$fh = fopen($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php', 'w');
		fwrite($fh, "<?php" . PHP_EOL);
		fwrite($fh, "\$db_server = '{$_POST['database_server']}';" . PHP_EOL);
		fwrite($fh, "\$db_name = '{$_POST['database_name']}';" . PHP_EOL);
		fwrite($fh, "\$db_username = '{$_POST['database_username']}';" . PHP_EOL);
		fwrite($fh, "\$db_password = '{$_POST['database_password']}';" . PHP_EOL);
		fwrite($fh, "?>" . PHP_EOL);
		fclose($fh);
		$fm->chmod_file('/settings/database.php', 0644);
		$message .= "&#187; Updated Database connection data<br>";
		write_log("Updated Database connection data", $GLOBALS['security_id']);
	}

	if (empty($message)) $message = "&#187; No Changes, Nothing to update";
	echo "<br><br><div class='dMsg'>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/settings/settings.php\"' ></div>";

	die();
}


//Replace Tags
$template = str_replace('<!-- database_server -->', $GLOBALS['db_server'], $template);
$template = str_replace('<!-- database_name -->', $GLOBALS['db_name'], $template);
$template = str_replace('<!-- database_username -->', $GLOBALS['db_username'], $template);
$template = str_replace('<!-- database_password -->', $GLOBALS['db_password'], $template);
$template = str_replace('<!-- ftp_server -->', $ftp_server, $template);
$template = str_replace('<!-- ftp_username -->', $ftp_username, $template);
$template = str_replace('<!-- ftp_password -->', $ftp_password, $template);
$template = str_replace('<!-- ftp_root -->', $ftp_root, $template);


echo $template;
?>

