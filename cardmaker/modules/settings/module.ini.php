<?php
$module_name = "{$GLOBALS['prog_name']} Settings";
$module_id = "settings"; //needs to be the same as the module directory
$module_icon = 'off_settings_64.png';
$module_link = 'settings.php';
$window_width = '800';
$window_height = '600';
$window_position = '';


//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "{$GLOBALS['prog_name']} Settings";
$security_id = "mod_settings";

//Settings
$settings[0]['name'] = "general";
$settings[0]['file'] = "$module_id/settings/general_settings.php";
$settings[0]['order'] = "0";

$settings[1]['name'] = "Connection";
$settings[1]['file'] = "$module_id/settings/connection_settings.php";
$settings[1]['order'] = "1";

$settings[2]['name'] = "Content Manager";
$settings[2]['file'] = "$module_id/settings/wysiwyg_settings.php";
$settings[2]['order'] = "2";


?>