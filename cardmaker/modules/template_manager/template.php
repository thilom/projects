<?php
/** 
 * Settings for Scrum. Gets a list of settings files from modules.ini.php and includes each one.
 *
 * @author Thilo Muller(2011)
 * @version $Id: template.php 87 2011-09-14 06:45:49Z thilo $
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/settings/init.php';
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/modules/content_manager/functions.php';
require_once SITE_ROOT . '/shared/file_man.class.php';

echo "<br>";

//Vars
$module_data = get_module_variables();
$page = isset($_GET['d'])?$_GET['d']:'';

//Menubar
foreach ($module_data as $data) {
	if (isset($data['template'])) {
		 foreach ($data['template'] as $template_data) {
			$templates[$template_data['name']] = $template_data['file'];
			$templates_order[$template_data['name']] = $template_data['order'];
		 }
	}
}
asort($templates_order);

$menu_bar = "<table id=menu_bar><tr>";
foreach ($templates_order as $template_name=>$dummy) {
	if (empty($page)) $page = $templates[$template_name];
	$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='/modules/template_manager/template.php?d={$templates[$template_name]}'\">$template_name</td>";
}
$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td></tr>";
$menu_bar .= "<table>";
echo "<div id=top_menu >$menu_bar</div>";

//Include Page
include $_SERVER['DOCUMENT_ROOT'] . "/modules/$page";

?>
