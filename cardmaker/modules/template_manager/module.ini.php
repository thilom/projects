<?php
/**
 * Settings and configuration for the template manager module.
 *
 * @author Thilo Muller(2011)
 * @version $Id: module.ini.php 134 2012-01-09 04:01:28Z thilo $
 */

//Settings for CMS
$module_name = "Template Manager";
$module_id = "template_manager"; //needs to be the same as the module directory
$module_icon = 'off_template_64.png';
$module_link = 'template.php';
$window_width = '900';
$window_height = '';
$window_position = '';

//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');


//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Templates";
$security_id = "mod_templates";

?>