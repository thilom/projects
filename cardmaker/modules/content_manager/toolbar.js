function open_area(id,Eid,Aid) {
	window.parent.W('Area Settings','/modules/content_manager/area_type.php?Pid='+id+'&Eid='+Eid + '&Aid='+Aid ,620, 600);
	window.parent.WD(W);
}

/**
 * Opens the rollback window
 * 
 * @param int id page Id
 * @param int Eid Area ID
 * @param int Aid Area Name
 * @param int WR 
 */
function open_rollback(id,Eid,Aid,WR) {
	window.parent.W('Area Rollback','/modules/content_manager/rollback.php?Pid='+id+'&Eid='+Eid + '&Aid='+ Aid + '&WR=' + WR ,520);
	window.parent.WD(W);
}