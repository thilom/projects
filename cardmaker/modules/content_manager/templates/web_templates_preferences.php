<?php

/**
 *  Web template preferences
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: web_templates_preferences.php 87 2011-09-14 06:45:49Z thilo $
 */

include_once "{$_SERVER['DOCUMENT_ROOT']}/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/modules/logs/log_functions.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/shared/database_functions.php";
include_once "template_toolbar.php";

//Vars
$template_file = $_GET['file'];
$template_name = '';
$template_disabled = '';
$template_id = $_GET['template_id'];

if (count($_POST)>0) {
	$template_disabled = isset($_POST['template_disabled'])?'Y':'';
	
	$statement = "SELECT template_file, template_name, template_disabled FROM {$GLOBALS['db_prefix']}_templates WHERE template_id=:template_id LIMIT 1";
	$sql_template = $GLOBALS['dbCon']->prepare($statement);
	$sql_template->bindParam(':template_id',$template_id);
	$sql_template->execute();
	$sql_count = $sql_template->rowCount();
	$sql_template_data = $sql_template->fetch();
	$sql_template->closeCursor();
	
	if ($sql_count == 0) {
		$template_id = next_id("{$GLOBALS['db_prefix']}_templates", 'template_id');
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_templates (template_id, template_name, template_file, template_disabled) 
						VALUES (:template_id, :template_name, :template_file, :template_disabled)";
		$sql_template_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_template_insert->bindParam(':template_id', $template_id);
		$sql_template_insert->bindParam(':template_name', $_POST['template_name']);
		$sql_template_insert->bindParam(':template_file', $_GET['file']);
		$sql_template_insert->bindParam(':template_disabled', $template_disabled);
		$sql_template_insert->execute();
		$sql_template_insert->closeCursor();
		
		$message = "&#187; Template registered ({$_POST['template_name']}:{$_GET['file']})";
		write_log("Template registered ({$_POST['template_name']}:{$_GET['file']})",'template_manager', $template_id);
	} else {
		$message = '';
		
		if ($_POST['template_name'] != $sql_template_data['template_name']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_templates SET template_name=:template_name WHERE template_id=:template_id";
			$sql_template_name = $GLOBALS['dbCon']->prepare($statement);
			$sql_template_name->bindParam(':template_name', $_POST['template_name']);
			$sql_template_name->bindParam(':template_id', $template_id);
			$sql_template_name->execute();
			$sql_template_name->closeCursor();
			
			$message .= "&#187; Template updated ({$_POST['template_name']}:{$_GET['file']})";
			write_log("Template updated ({$_POST['template_name']}:{$_GET['file']})",'template_manager', $template_id);
		}
		
		$template_disabled = isset($_POST['template_disabled'])?'Y':'';
		if ($template_disabled != $sql_template_data['template_disabled']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_templates SET template_disabled=:template_disabled WHERE template_id=:template_id";
			$sql_template_name = $GLOBALS['dbCon']->prepare($statement);
			$sql_template_name->bindParam(':template_disabled', $template_disabled);
			$sql_template_name->bindParam(':template_id', $template_id);
			$sql_template_name->execute();
			$sql_template_name->closeCursor();
			
			if ($template_disabled == 'Y') {
				$message .= "<br>&#187; Template disabled ({$_POST['template_name']}:{$_GET['file']})";
				write_log("Template disabled ({$_POST['template_name']}:{$_GET['file']})",'template_manager', $template_id);
			} else {
				$message .= "<br>&#187; Template enabled ({$_POST['template_name']}:{$_GET['file']})";
				write_log("Template enabled ({$_POST['template_name']}:{$_GET['file']})",'template_manager', $template_id);
			}
		}
	}
	
	if (empty($message)) $message = "No Changes, Nothing to update";
	echo "<br> <br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"parent.WMN({$_GET['WR']});parent.WD({$_GET['W']})\" ></div>";

	die();
}

//Vars
$template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/content_manager/html/web_template_preferences.html");

//Get template data
if (!empty($template_id)) {
	$statement = "SELECT template_file, template_name, template_disabled FROM {$GLOBALS['db_prefix']}_templates WHERE template_id=:template_id LIMIT 1";
	$sql_template = $GLOBALS['dbCon']->prepare($statement);
	$sql_template->bindParam(':template_id',$template_id);
	$sql_template->execute();
	$sql_template_data = $sql_template->fetch();
	$sql_template->closeCursor();
	
	$template_name = $sql_template_data['template_name'];
	$template_disabled = $sql_template_data['template_disabled']=='Y'?'checked':'';
}


//Replace Tags
$template = str_replace('<!-- template_file -->', $template_file, $template);
$template = str_replace('<!-- template_name -->', $template_name, $template);
$template = str_replace('<!-- template_disabled -->', $template_disabled, $template);
$template = str_replace('!W!', $_GET['W'], $template);
$template = str_replace('!WR!', $_GET['WR'], $template);

echo $template;
?>
