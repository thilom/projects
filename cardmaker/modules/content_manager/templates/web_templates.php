<?php
/**
 *  Manage website templates
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: web_templates.php 87 2011-09-14 06:45:49Z thilo $
 */

include_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/terms.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/admin_style.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php';

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/html/template_list.html');
$template_list = '';
$total_templates = 0;
$unused_templates = 0;
$new_templates = 0;

//Prepare statement - Find registered templates
$statement = "SELECT template_id, template_name FROM {$GLOBALS['db_prefix']}_templates WHERE template_file=:template_file LIMIT 1";
$sql_templates = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Template usage
$statement = "SELECT COUNT(*) AS counter FROM {$GLOBALS['db_prefix']}_pages WHERE template_id=:template_id";
$sql_usage = $GLOBALS['dbCon']->prepare($statement);

//Get list of files
$files = scandir($_SERVER['DOCUMENT_ROOT'] . '/templates');
foreach ($files as $file_name) {
	$file_data = pathinfo("{$_SERVER['DOCUMENT_ROOT']}/templates/$file_name");
	if (isset($file_data['extension']) && ($file_data['extension'] == 'html' || $file_data['extension'] == 'htm')) {
		$total_templates++;
		$sql_templates->bindParam(':template_file', $file_name);
		$sql_templates->execute();
		$sql_template_data = $sql_templates->fetch();
		
		if (!empty($sql_template_data)) {
			$template_name = $sql_template_data['template_name'];
			$template_id = $sql_template_data['template_id'];
			$template_icon = '';
			
			//Usage
			$sql_usage->bindParam('template_id', $template_id);
			$sql_usage->execute();
			$template_usage = $sql_usage->fetch();
			$template_usage = $template_usage['counter'];
			if ($template_usage == 0) $unused_templates++;
		} else {
			$template_name = '';
			$template_id = '';
			$template_icon = "<img src='/i/silk/new.png' >";
			$new_templates++;
			$unused_templates++;
			$template_usage = 0;
		}
		
		$template_list .= "<tr><td>$template_icon</td>";
		$template_list .= "<td>$template_name</td>";
		$template_list .= "<td>{$file_data['basename']}</td>";
		$template_list .= "<td>$template_usage</td>";
		$template_list .= "<td align=right><input type=button value='Edit' class=edit_button onclick=\"P.W('Template:_$template_name','/modules/content_manager/templates/web_templates_manager.php?template_id=$template_id&file=$file_name&WR={$_GET['W']}',800,800,0,0,2);P.WMN(W)\" ></td></tr>";
		
	}
}

//Counters
$total_templates = $total_templates>1||$total_templates==0?"$total_templates Templates in total":"$total_templates template in total";
$new_templates = $new_templates>1||$new_templates==0?"$new_templates New templates":"$new_templates New template";
$unused_templates = $unused_templates>1||$unused_templates==0?"$unused_templates Unused templates":"$unused_templates Unused template";

//Replace Tags
$template = str_replace('<!-- css -->', $css, $template);
$template = str_replace('<!-- terms -->', $terms, $template);
$template = str_replace('<!-- terms -->', $terms, $template);
$template = str_replace('<!-- template_list -->', $template_list, $template);
$template = str_replace('<!-- total_templates -->', $total_templates, $template);
$template = str_replace('<!-- new_templates -->', $new_templates, $template);
$template = str_replace('<!-- unused_templates -->', $unused_templates, $template);

echo $template;
?>
