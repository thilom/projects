// Content manager start page
// version: $Id: start.js 132 2012-01-04 06:10:34Z thilo $
// Author: Jono Darvall(2009), Thilo Muller(2011)

var pg='';
var pa='';
var srt = '';



function d_GS(p) {
	pg=p
	pa=p
	dr_GS();
}

function s_GS(s) {
	np='';
	pd=pa.split('}')
	for (var i=0; i<pd.length; i++) {
		if (pd[i]=='') continue

		pData=pd[i].split('|#|')
		if (pData[1].search(s)==-1 && pData[2].search(s)==-1) continue;
		np += pd[i]+"}";
	}

	pg=np;
	dr_GS();
}

function st_GS(s) {

	cnt = 0;
	ndt = '';
	dt = new Array()

	pd=pa.split('}');
	for (var i=0; i<pd.length; i++) {
		if (pd[i]=='') continue
		pData = pd[i].split('|#|')
		dt[cnt] = new Array()
		dt[cnt]['a'] = pData[0]
		dt[cnt]['b'] = pData[1]
		dt[cnt]['c'] = pData[2]
		cnt++
	}


	for (i=0; i<dt.length; i++) {
		ndt += dt[i]['a']+"|"+dt[i]['b']+"|"+dt[i]['c']+"}"
	}

	pg = ndt;
	dr_GS();
}

function dr_GS() {
	a="<table  class='tbl_list' id='TRW'><tr>"
	a+="<th>&nbsp;</th>"
	a+="<th>Page Name</th>"
	a+="<th width: 50px>Page Title</th>"
	a+="<th>Last Update</th><th>Status</th>"
	a+="<th>&nbsp;</th></tr>"
	pd = pg.split('}')
	var cnt = 0
	var ownCnt = 0
	var appCnt = 0
	for (var i=0; i<pd.length; i++) {
		if (pd[i]=='') continue

		pData=pd[i].split('|#|')

		if (pData[6] == 1) {
			icon = '<img src="/i/button/faq_16.png" title="Pending Approval" >'
			appCnt++;
		} else {
			icon = pData[3]==1?'<img src="/i/button/edit_16.png" title="Editable" >':'<img src="/i/button/preview_16.png" title="View Only" >';
		}

		a+="<tr>";
		a+="<td>"+icon+"</td>"
		a+="<td>"+pData[1]+"</td>"
		a+="<td>"+pData[2]+"</td>"
		a+="<td>"+ pData[4] +"</td>"
		a+="<td>"+ pData[5] +"</td>"
		a+="<td align=right>"
		a+="<button type='button' ";


		a+="onclick=\"P.W('Page:_"+pData[2]+"','/modules/content_manager/manager.php?page="+pData[1]+"&WR="+W+"',P.wh-80,P.ww,0,0,2);P.WMN(W)\">"

		a+=pData[3]==1?"<img src='/i/button/edit_16.png'>":"<img src='/i/button/preview_16.png'>";
//		a+=" value="
		a+=pData[3]==1?"Edit ":"View ";
		a+="</button>";
		a+="<button type='button'  "
		a+= pData[3]==1?" onClick='remove_page("+ pData[0] +")' >":" disabled >"
		a+= "<img src='/i/button/delete_16.png'>Remove";
		a+="</button>"
		a+="</td>"
		a+="</tr>"
		if (pData[3] == 1) {
			ownCnt++;
		}
		cnt++
	}
	a+="<tr><th colspan=10></th></tr></table>";

	document.getElementById('content').innerHTML = a
	document.getElementById('totalPages').innerHTML = cnt>0?"Website consists of "+cnt+" pages":"Website consists of "+cnt+" page";
	document.getElementById('ownedPages').innerHTML = ownCnt>0?"You have access to edit "+ownCnt+" pages":"You have access to edit "+ownCnt+" page";
	document.getElementById('approvalPages').innerHTML = appCnt>1?appCnt+" updates are awaiting your approval":appCnt+" update is awaiting your approval";
	whack();
}

function ff(id,tx) {
	xx = document.getElementById(id)
	if (xx.value==tx) xx.value = ''
}

function fb(id,tx) {
	xx = document.getElementById(id)
	if (xx.value=='') xx.value=tx
}

/**
 * Remove a page
 */
function remove_page(pID) {
    if (confirm('WARNING!\nThis will permanently remove the page and CANNOT be undone.\n\nContinue?')) {
		document.location = "/modules/content_manager/start.php?W="+ W +"&remove=" + pID;
	}
}


