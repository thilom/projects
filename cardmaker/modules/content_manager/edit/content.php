<?php
/**
 * Manage the content for text type area
 *
 * @version $Id: content.php 139 2012-03-02 19:45:57Z thilo $
 * @author Thilo Muller(2011)
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$Eid = $_GET['Eid'];
$page_id = $_GET['Pid'];
$message = 'No Changes, Nothing to update';
$message2 = '';
$button1 = "";
$button1b = "";
$button2 = "<input type=button class='cancel_button' value='Close Without Saving' onclick='window.parent.WMN({$_GET['WR']},1);window.parent.WD(W)' >";
$button3 = "<input type=submit class='save_button' name='save_draft' value='Save As Draft' >";

if (count($_POST) > 0) {

	$area_data = get_area_data($Eid);

	if ((isset($_POST['save_draft']))) {
		 if ($area_data['data']['header'] != $_POST['header1']) {
			if ($area_data['version'] == 'current') {
				copy_area($Eid, "{$GLOBALS['db_prefix']}_areas", "{$GLOBALS['db_prefix']}_areas_editing", FALSE, TRUE, $_SESSION['dbweb_user_id']);
			}
			$statement = "UPDATE {$GLOBALS['db_prefix']}_areas_editing SET header=:header WHERE id=:id";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':id', $Eid);
			$sql_update->bindParam(':header', $_POST['header1']);
			$sql_update->execute();
			$sql_update->closeCursor();

			$message = "&#187; Content updated for text area";

			write_log("Content updated for text area (Draft)",'content_manager', $Eid);

		 }

	} else if ((isset($_POST['go_live']))) {
		 if ($sql_current_data[0]['header'] != $_POST['header1']) {
			copy_area($Eid, "{$GLOBALS['db_prefix']}_areas_editing", "{$GLOBALS['db_prefix']}_areas_history", FALSE, TRUE);
			copy_area($Eid, "{$GLOBALS['db_prefix']}_areas_editing", "{$GLOBALS['db_prefix']}_areas", TRUE, TRUE);

			$message = "&#187; Text area saved to live";
			write_log("Content saved to live",'content_manager', $Eid);
		 }

	} else if (isset($_POST['decline_update'])) {

	} else if (isset($_POST['approve_update'])) {


	}

	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"document.location='editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&WR={$_GET['WR']}&Eid={$_GET['Eid']}&W={$_GET['W']}'\" ></div>";
	die();
}

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/html/content.html');
$message = '';

//Prepare statement - editing
$statement = "SELECT header, last_edit_id, DATE_FORMAT(last_edit_date, '%d %M %Y') AS last_edit_date, title FROM {$GLOBALS['db_prefix']}_areas_editing WHERE id=:id";
$sql_area_editing = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - current
$statement = "SELECT header, title FROM {$GLOBALS['db_prefix']}_areas WHERE id=:id";
$sql_area_current = $GLOBALS['dbCon']->prepare($statement);

if (isset($_GET['Eid']) && $_GET['Eid'] != 0) {
	$area_data = get_area_data($Eid);

	$content = isset($area_data['data']['header'])?$area_data['data']['header']:'';
	$content = encodeHTML($content);
	$header = "<div style='height: 28px'></div><textarea id=header1 name=header1 >$content</textarea>";
	$header .= "<script>oEdit1.REPLACE('header1')</script>";
	$title = $area_data['data']['title'];
} else {
	$_GET['Eid'] = next_id("{$GLOBALS['db_prefix']}_areas", 'id');
	$title = "Area " . date('d/m/Y H:ia');
	//Insert new area
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_areas (id, area_id, title, type, module,header) VALUES (:id,:area_id,:title,'text','content_manager','')";
	$sql_new_area = $GLOBALS['dbCon']->prepare($statement);
	$sql_new_area->bindParam(':id', $_GET['Eid']);
	$sql_new_area->bindParam(':area_id', $area_id);
	$sql_new_area->bindParam(':title', $title);
	$sql_new_area->execute();
	$sql_new_area->closeCursor();
	$header = '';

	$header = "<textarea id=header1 name=header1 ></textarea>";
	$header .= "<script>oEdit1.REPLACE('header1')</script>";
}

//Check if current user is allowed to edit
$statement = "SELECT COUNT(*) AS editable FROM {$GLOBALS['db_prefix']}_pages_owners WHERE user_id=:user_id AND (page_id=:page_id OR page_id=0) LIMIT 1";
$sql_editable = $GLOBALS['dbCon']->prepare($statement);
$sql_editable->bindParam(':user_id', $_SESSION['dbweb_user_id']);
$sql_editable->bindParam(':page_id', $page_id);
$sql_editable->execute();
$editable = $sql_editable->fetch();
$sql_editable->closeCursor();
$editable = $editable['editable'];

//Overide limiting editing if developer or super user
if ($_SESSION['user_type'] == 'developer' || $_SESSION['user_type'] == 'superuser') $editable = 1;


//Get last editor
if ((isset($sql_data2['last_edit_id']))) {
	$statement = "SELECT CONCAT(firstname, ' ', lastname) as fname FROM {$GLOBALS['db_prefix']}_user WHERE user_id=:user_id";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bindParam(':user_id', $sql_data2['last_edit_id']);
	$sql_user->execute();
	$sql_data3 = $sql_user->fetch();
	$sql_user->closeCursor();

	$message = "<td><tt>Last Edited By: {$sql_data3['fname']}<br>Last Edited On: {$sql_data2['last_edit_date']}</tt></td>";
}

//Check if it's waiting for approval
$statement = "SELECT a.submit_by, DATE_FORMAT(a.history_date, '%d %M %Y') AS history_date, CONCAT(b.firstname, ' ', b.lastname) AS submit_name
				FROM {$GLOBALS['db_prefix']}_areas_pending AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_user AS b ON a.submit_by=b.user_id
				WHERE a.id=:id";
$sql_area_pending = $GLOBALS['dbCon']->prepare($statement);
$sql_area_pending->bindParam(':id', $Eid);
$sql_area_pending->execute();
$pending = $sql_area_pending->fetch();
$sql_area_pending->closeCursor();

if (isset($pending['submit_by']) && $pending['submit_by'] != 0) {

	//Get list of approvers
	$approvers_list = '';
	$concat = $GLOBALS['settings']['approval_system'] == 'single'?'or':'and';
	$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname) AS approver_name
					FROM {$GLOBALS['db_prefix']}_user_approve AS a
					LEFT JOIN {$GLOBALS['db_prefix']}_user AS b ON a.approved_by=b.user_id
					WHERE a.user_id=:user_id";
	$sql_approvers = $GLOBALS['dbCon']->prepare($statement);
	$sql_approvers->bindParam(':user_id', $pending['submit_by']);
	$sql_approvers->execute();
	$approvers_data = $sql_approvers->fetchAll();
	$sql_approvers->closeCursor();
	foreach ($approvers_data as $data) {
		$approvers_list .= "{$data['approver_name']} $concat ";
	}
	$approvers_list = $GLOBALS['settings']['approval_system']=='single'?substr($approvers_list, 0, -4):substr($approvers_list, 0, -5);

	$message = "<th style='text-align: left'><tt>Submitted For Approval By: {$pending['submit_name']}<br>Submitted On: {$pending['history_date']}</tt></th>";
	$message2 = "<th style='text-align: center; width: 200px'><tt style='color: red; font-weight: bold'>In approval process.Cannot be edited.<br>Approvers: $approvers_list</tt></th>";
	$header = "<iframe style='background-color: white; width: 1000px; height: 450px; border: 1px solid black; padding: 4px; margin-left: 2px; overflow: scroll' src='/modules/content_manager/edit/iframe_area.php?id=$Eid' > </iframe>";
}

//Check for approval method
if ((isset($GLOBALS['settings']['approval_system'])) && $GLOBALS['settings']['approval_system'] != 'none' ) {
	$statement = "SELECT COUNT(*) AS count FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id";
	$sql_approve = $GLOBALS['dbCon']->prepare($statement);
	$sql_approve->bindParam(':user_id', $_SESSION['dbweb_user_id']);
	$sql_approve->execute();
	$sql_approve_data = $sql_approve->fetch();
	$sql_approve->closeCursor();

	if ($sql_approve_data['count']>0) {
		if (isset($pending['submit_by'])) {
			$button1 = '';
			$button3 = '';

			//check for approval rights
			$statement = "SELECT COUNT(*) AS approval_count FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id AND approved_by=:approved_by";
			$sql_approver = $GLOBALS['dbCon']->prepare($statement);
			$sql_approver->bindParam(':user_id', $pending['submit_by']);
			$sql_approver->bindParam(':approved_by', $_SESSION['dbweb_user_id']);
			$sql_approver->execute();
			$approver = $sql_approver->fetch();
			$sql_approver->closeCursor();

			//Check if it's already been approved by the current user
			$statement = "SELECT DATE_FORMAT(approved_date, '%d %M %Y') AS approved_date FROM {$GLOBALS['db_prefix']}_areas_approved WHERE area_id=:area_id AND approver_id=:approver_id LIMIT 1";
			$sql_approved = $GLOBALS['dbCon']->prepare($statement);
			$sql_approved->bindParam(':area_id', $Eid);
			$sql_approved->bindParam(':approver_id', $_SESSION['dbweb_user_id']);
			$sql_approved->execute();
			$already_approved = $sql_approved->fetch();
			$sql_approved->closeCursor();

//			print_r($already_approved);

			if (isset($already_approved['approved_date'])) {
				$button1 = "";
				$button1b = "";
				$button2 = "<input type=button class='cancel_button' value='Close' onclick='window.parent.WMN({$_GET['WR']},1);window.parent.WD(W)' >";
				$message2 = "<th style='text-align: center; width: 200px'><tt style='color: red; font-weight: bold'>You have already approved this update on {$already_approved['approved_date']}.<br>Approvers: $approvers_list</tt></th>";
			} else {
				if (isset($approver['approval_count']) && $approver['approval_count'] > 0) {
					$button1 = "";
					$button1b = "";
					$button2 = "<input type=button class='cancel_button' value='Close Without Approving/Rejecting' onclick='window.parent.WMN({$_GET['WR']},1);window.parent.WD(W)' >";
				}
			}
		} else {
			$button1 = "";
		}
	} else {
		$button1 = "";
	}
} else {
	$button1 = "";
}

if ($editable == 0) {
	$button1 = "";
	$button1b = "";
	$button2 = "<input type=button class='cancel_button' value='Close' onclick='window.parent.WMN({$_GET['WR']},1);window.parent.WD(W)' >";
	$button3 = '';
	$message2 = "<th style='text-align: center; width: 200px'><tt style='color: red; font-weight: bold'>You do not have access to edit his page</tt></th>";
	$header = "<iframe style='background-color: white; width: 1000px; height: 450px; border: 1px solid black; padding: 4px; margin-left: 2px; overflow: scroll' src='/modules/content_manager/edit/iframe_area.php?id=$Eid' > </iframe>";
}

//Get WYSIWYG Buttons
$statement = "SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='content_manager_wysiwyg'";
$sql_wysiwyg = $GLOBALS['dbCon']->prepare($statement);
$sql_wysiwyg->execute();
$wysiwyg_data = $sql_wysiwyg->fetch();
$sql_wysiwyg->closeCursor();

//Replace tags
$template = str_replace('<!-- content -->', $header, $template);
$template = str_replace('!title!', $title, $template);
$template = str_replace('!WR!', $_GET['WR'], $template);
$template = str_replace('!Pid!', $_GET['Pid'], $template);
$template = str_replace('!Aid!', $_GET['Aid'], $template);
$template = str_replace('!W!', $_GET['W'], $template);
$template = str_replace('!Eid!', $_GET['Eid'], $template);
$template = str_replace('<!-- message -->', $message.$message2, $template);
$template = str_replace('<!-- button1 -->', $button1, $template);
$template = str_replace('<!-- button1b -->', $button1b, $template);
$template = str_replace('<!-- button2 -->', $button2, $template);
$template = str_replace('<!-- button3 -->', $button3, $template);
$template = str_replace('!editor_features!', $wysiwyg_data['value'], $template);

echo $template;

function encodeHTML($sHTML){
	$sHTML=str_replace("&","&amp;",$sHTML);
	$sHTML=str_replace("<","&lt;",$sHTML);
	$sHTML=str_replace(">","&gt;",$sHTML);
	return $sHTML;
}



?>
