<?php
/** 
 * Display the eare contents in an ifarame,
 *
 * @author Thilo Muller(2011)
 * @version $Id: iframe_area.php 48 2011-05-06 03:47:20Z thilo $
 */

include_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";

$area_id = $_GET['id'];

//Prepare statement - editing
$statement = "SELECT header FROM {$GLOBALS['db_prefix']}_areas_editing WHERE id=:id";
$sql_area_editing = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - pending
$statement = "SELECT header FROM {$GLOBALS['db_prefix']}_areas_pending WHERE id=:id";
$sql_area_pending = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - current
$statement = "SELECT header FROM {$GLOBALS['db_prefix']}_areas WHERE id=:id";
$sql_area_current = $GLOBALS['dbCon']->prepare($statement);

$sql_area_pending->bindParam(':id', $area_id);
$sql_area_pending->execute();
if ($sql_area_pending->rowCount() > 0) {
	$data = $sql_area_pending->fetch();
	$header = $data['header'];
	$sql_area_pending->closeCursor();
} else {
	$sql_area_pending->closeCursor();

	$sql_area_editing->bindParam(':id', $area_id);
	$sql_area_editing->execute();
	if ($sql_area_editing->rowCount() > 0) {
		$data = $sql_area_editing->fetch();
		$header = $data['header'];
		$sql_area_editing->closeCursor();
	} else {
		$sql_area_editing->closeCursor();
		$sql_area_current->bindParam(':id', $area_id);
		$sql_area_current->execute();
		if ($sql_area_current->rowCount() > 0) {
			$data = $sql_area_current->fetch();
			$header = $data['header'];
			$sql_area_current->closeCursor();
		} else {
			$header = '';
		}
	}
}

echo $header;
?>
