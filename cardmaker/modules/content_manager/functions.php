<?php
/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Copies data between area tables
 *
 * @param int $area_id
 * @param string $from_table The table name to copy from
 * @param string $to_table The table name to copy to
 * @param boolean $clear TRUE if you want to clear the destination table first
 * @param boolean $date TRUE if you want to add a datestamp to the destination table
 * @param int $user_id The ID of the user who submits an area for approval
 */
function copy_area($area_id, $from_table, $to_table, $clear=FALSE, $date=FALSE, $user_id=0) {

	if ($to_table != "{$GLOBALS['db_prefix']}_areas_history") {
		$statement = "DELETE FROM $to_table WHERE id=:id LIMIT 1";
		$sql_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_delete->bindParam(':id', $area_id);
		$sql_delete->execute();
		$sql_delete->closeCursor();
	}

	$statement = "SELECT area_id, title, page_id, type, module, shared, header, preferences
					FROM $from_table
					WHERE id=:id
					LIMIT 1";
	$sql_current = $GLOBALS['dbCon']->prepare($statement);
	$sql_current->bindParam(':id', $area_id);
	$sql_current->execute();
	$data = $sql_current->fetch();
	$sql_current->closeCursor();

	switch ($to_table) {
		case ("{$GLOBALS['db_prefix']}_areas"):
			$date_field = 'last_update';
			$user_field = '';
			break;
		case ("{$GLOBALS['db_prefix']}_areas_editing"):
			$date_field = 'last_edit_date';
			$user_field = 'last_edit_id';
			break;
		case ("{$GLOBALS['db_prefix']}_areas_pending"):
			$date_field = 'history_date';
			$user_field = 'submit_by';
			break;
		case ("{$GLOBALS['db_prefix']}_areas_deleted"):
			$date_field = 'delete_date';
			$user_field = 'delete_by';
			break;
		default:
			$date_field = 'history_date';
			$user_field = '';
			break;
	}

	$statement = "INSERT INTO $to_table (id, area_id, title, page_id, type, module, shared, header, preferences";
	if ($date === TRUE) $statement .= ", $date_field";
	if ($user_id != 0) $statement .= ", $user_field";
	$statement .= ") VALUES (:id,:area_id,:title,:page_id,:type,:module,:shared,:header, :preferences";
	if ($date === TRUE) $statement .= ", NOW()";
	if ($user_id != 0) $statement .= ", :user_id";
	$statement .= ")";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_insert->bindParam(':area_id', $data['area_id']);
	$sql_insert->bindParam(':title', $data['title']);
	$sql_insert->bindParam(':page_id', $data['page_id']);
	$sql_insert->bindParam(':type', $data['type']);
	$sql_insert->bindParam(':module', $data['module']);
	$sql_insert->bindParam(':shared', $data['shared']);
	$sql_insert->bindParam(':header', $data['header']);
	$sql_insert->bindParam(':preferences', $data['preferences']);
	$sql_insert->bindParam(':id', $area_id);
	if ($user_id != 0) $sql_insert->bindParam(':user_id', $user_id);
	$sql_insert->execute();
	$sql_insert->closeCursor();

	if ($clear === TRUE) {
		$statement = "DELETE FROM $from_table WHERE id=:id LIMIT 1";
		$sql_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_delete->bindParam(':id', $area_id);
		$sql_delete->execute();
		$sql_delete->closeCursor();
	}
}

function get_module_variables($module='') {



	//Vars
	$vars = array();
	$no_it = 1;

	//Iterate thru modules
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/modules/';
	if (is_dir($dir)) {
		$dh = opendir($dir);
		if ($dh) {
			while (($file = readdir($dh)) !== false) {
				if (is_dir($dir . $file)) {
					if ($file == '.' || $file == '..' || $file == '.svn') continue;
						$security_type = '';
						$security_name = '';
						$security_id = '';
						$module_name = '';
						$module_icon = '';
						$module_id = '';
						$content_type = array();
						$settings = '';
						$template = '';
						$window_width = '';
						$module_link = '';
						$window_height = '';
						$window_xposition = '';
						$window_yposition = '';
						$window_attribute = '';
						$disabled = false;
//						if (is_file($dir.$file.'/module.ini.php')) {

							if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
							if (!empty($module) && $module != $module_id) continue;
							if (!empty($module_id)) $vars[$module_id]['module_id'] = $module_id;
							if (!empty($module_link)) $vars[$module_id]['module_link'] = $module_link;
							if (!empty($module_name)) $vars[$module_id]['module_name'] = $module_name;
							if (!empty($module_icon)) $vars[$module_id]['module_icon'] = $module_icon;
							if (!empty($security_type)) $vars[$module_id]['security_type'] = $security_type;
							if (!empty($security_name)) $vars[$module_id]['security_name'] = $security_name;
							if (!empty($security_id)) $vars[$module_id]['security_id'] = $security_id;
							if (!empty($template)) $vars[$module_id]['template'] = $template;
							if (!empty($window_width)) $vars[$module_id]['window_width'] = $window_width;
							if (!empty($window_height)) $vars[$module_id]['window_height'] = $window_height;
							if (!empty($window_xposition)) $vars[$module_id]['window_xposition'] = $window_xposition;
							if (!empty($window_yposition)) $vars[$module_id]['window_yposition'] = $window_yposition;
							if (!empty($window_attribute)) $vars[$module_id]['window_attribute'] = $window_attribute;
							$vars[$module_id]['disabled'] = $disabled;

							if (isset($content_type) && !empty($content_type)) {
								foreach ($content_type as $data) {
									$vars[$module_id]['content_type'][] = $data;
								}
							}

							if (isset($settings) && !empty($settings)) {
								foreach ($settings as $data) {
									$vars[$module_id]['settings'][] = $data;
								}
							}
						}

			}
			closedir($dh);

		}
	}
//	var_dump($vars);

	return $vars;
}

/**
 * Get the data for a spcified area
 *
 * @param int $area_id The ID of the area
 * @param string $version The version to retrieve. latest, pending, draft, current
 * @param mixed $fields An array of fields to return. retrieves all if empty
 *
 * @return mixed
 */
function get_area_data($area_id, $version='latest', $fields='') {

	//Setup fields
	if (empty($fields)) {
		$field_list = '*';
	} else {
		$field_list = '';
		foreach ($fields as $field_name) {
			$field_list .= "$field_name,";
		}
		$field_list = substr($field_list, 0, -1);
	}

	switch ($version) {
		case 'latest':
			$statement = "SELECT $field_list FROM {$GLOBALS['db_prefix']}_areas_pending WHERE id=:area_id LIMIT 1"; //Get pending approval first
			$sql_pending = $GLOBALS['dbCon']->prepare($statement);
			$sql_pending->bindParam(':area_id', $area_id);
			$sql_pending->execute();
			if ($sql_pending->rowCount() > 0) {
				$sql_data['data'] = $sql_pending->fetch(PDO::FETCH_ASSOC);
				$sql_pending->closeCursor();
				$sql_data['version'] = 'pending';
			} else {
				$statement = "SELECT $field_list FROM {$GLOBALS['db_prefix']}_areas_editing WHERE id=:area_id LIMIT 1"; //Check the draft table next
				$sql_draft = $GLOBALS['dbCon']->prepare($statement);
				$sql_draft->bindParam(':area_id', $area_id);
				$sql_draft->execute();
				if ($sql_draft->rowCount() > 0) {
					$sql_data['data'] = $sql_draft->fetch(PDO::FETCH_ASSOC);
					$sql_draft->closeCursor();
					$sql_data['version'] = 'draft';
				} else {
					$statement = "SELECT $field_list FROM {$GLOBALS['db_prefix']}_areas WHERE id=:area_id LIMIT 1"; //Finally get the current version
					$sql_current = $GLOBALS['dbCon']->prepare($statement);
					$sql_current->bindParam(':area_id', $area_id);
					$sql_current->execute();
					$sql_data['data'] = $sql_current->fetch(PDO::FETCH_ASSOC);
					$sql_current->closeCursor();
					$sql_data['version'] = 'current';
				}
			}

			break;
		case 'pending':
			$statement = "SELECT $field_list FROM {$GLOBALS['db_prefix']}_areas_pending WHERE id=:area_id LIMIT 1";
			$sql_pending = $GLOBALS['dbCon']->prepare($statement);
			$sql_pending->bindParam(':area_id', $area_id);
			$sql_pending->execute();
			$sql_data['data'] = $sql_pending->fetch(PDO::FETCH_ASSOC);
			$sql_pending->closeCursor();
			$sql_data['version'] = 'pending';
			break;
		case 'draft':
			$statement = "SELECT $field_list FROM {$GLOBALS['db_prefix']}_areas_editing WHERE id=:area_id LIMIT 1";
			$sql_editing = $GLOBALS['dbCon']->prepare($statement);
			$sql_editing->bindParam(':area_id', $area_id);
			$sql_editing->execute();
			$sql_data['data'] = $sql_editing->fetch(PDO::FETCH_ASSOC);
			$sql_editing->closeCursor();
			$sql_data['version'] = 'draft';
			break;
		case 'current':
			$statement = "SELECT $field_list FROM {$GLOBALS['db_prefix']}_areas WHERE id=:area_id LIMIT 1";
			$sql_current = $GLOBALS['dbCon']->prepare($statement);
			$sql_current->bindParam(':area_id', $area_id);
			$sql_current->execute();
			$sql_data['data'] = $sql_current->fetch(PDO::FETCH_ASSOC);
			$sql_current->closeCursor();
			$sql_data['version'] = 'current';
			break;
	}

	return $sql_data;
}


/**
 * Updates a specified area with the specified data. If the draft velsion is to be updated,
 * it will first check to see if an entry exists and copy the corrent version to it if not.
 *
 * @param <type> $area_id The Area ID to update
 * @param <type> $version The table version. 'current' or 'draft'.
 * @param <type> $fields An array of field name => field value
 */
function update_area_data($area_id, $version, $fields) {
	switch ($version) {
		case 'current':
			$table = "{$GLOBALS['db_prefix']}_areas";
			break;
		case 'draft':
			$table = "{$GLOBALS['db_prefix']}_areas_editing";

			//Check if a draft version exists
			$draft_count = 0;
			$statement = "SELECT COUNT(*) AS counter FROM $table WHERE id=:area_id";
			$sql_draft_check = $GLOBALS['dbCon']->prepare($statement);
			$sql_draft_check->bindParam(':area_id', $area_id);
			$sql_draft_check->execute();
			$sql_draft_count = $sql_draft_check->fetch();
			$sql_draft_check->closeCursor();
			$draft_count = $sql_draft_count['counter'];

			//Copy Data
			if ($draft_count == 0) {
				copy_area($area_id, "{$GLOBALS['db_prefix']}_areas", $table);
			}
			break;
	}

	//Setup fields
	$field_list = '';
	foreach ($fields as $field_name=>$field_value) {
		$field_list .= "$field_name='$field_value',";
	}
	$field_list = substr($field_list, 0, -1);

	//Save
	$statement = "UPDATE $table SET $field_list WHERE id=:id";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_update->bindParam(':id', $area_id);
	$sql_update->execute();
	$sql_update->closeCursor();

}

/**
 * Takes a preferences string (as saved in the DB) and expands it to it's individual parts
 *
 * @param string $prefs The preferences string to expand
 * @return mixed
 */
function expand_preferences($prefs) {
	$preferences = array();
	$prefs = explode('|', $prefs);

	foreach ($prefs as $pref) {
		if (empty($pref)) continue;
		list($var, $value) = explode('=', $pref);
		$preferences[$var] = $value;
	}
	return $preferences;
}

/**
 * Get the mode for additional buttons in the toolbar.
 *   1:'go live'
 *   2:'submit for approval'
 *   3:'aprrove & decline'
 *
 * @param int $page_id The ID of the page that needs to be checked against
 * @param int $area_id The ID of the area to check
 * $param int $submitted_by The ID of the user who submitted the area for approval
 * @return int
 */
function get_toolbar_mode($page_id, $area_id, $submitted_by) {
	$mode=0;

	//Get approval system
	$statement = "SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='approval_system' LIMIT 1";
	$sql_settings = $GLOBALS['dbCon']->prepare($statement);
	$sql_settings->execute();
	$sql_setting_data = $sql_settings->fetch();
	$sql_settings->closeCursor();

	if ($sql_setting_data['value'] == 'none') {
		$mode = 1;
	} else {
		//Check if there are approvers for the user
		$statement = "SELECT COUNT(*) AS counter FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id";
		$sql_approvers = $GLOBALS['dbCon']->prepare($statement);
		$sql_approvers->bindParam(':user_id', $_SESSION['dbweb_user_id']);
		$sql_approvers->execute();
		$sql_approver_count = $sql_approvers->fetch();
		$sql_approvers->closeCursor();

		if ($sql_approver_count['counter'] == 0) {
			$mode = 1;
		} else {
			$area_data = get_area_data($area_id);
			if ($area_data['version'] == 'pending') {
				//Is the current user allowed to approve
				$statement = "SELECT COUNT(*) AS counter FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id AND approved_by=:approved_by";
				$sql_access = $GLOBALS['dbCon']->prepare($statement);
				$sql_access->bindParam(':user_id', $submitted_by);
				$sql_access->bindParam(':approved_by', $_SESSION['dbweb_user_id']);
				$sql_access->execute();
				$sql_access_data = $sql_access->fetch();
				$sql_access->closeCursor();

				if ($sql_access_data['counter']) $mode = 3;

			} else {
				$mode = 2;
			}
		}
	}

	return $mode;
}

/**
 * Get the disabled mode for go live/approve toolbar buttons
 *
 * @param int $area_id
 * @return bool
 */
function get_toolbar_bmode($area_id) {
	$mode = FALSE;
	$area_data = get_area_data($area_id);

	if ($area_data['version'] == 'draft') $mode = TRUE;

	return $mode;
}

/**
 * Checks if a module is installed
 */
function is_module($module_id) {
	$module_variables = get_module_variables($module_id);

	if (empty($module_variables)) {
		return false;
	} else {
		return true;
	}
}

?>