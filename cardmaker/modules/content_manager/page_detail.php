<?php

/**
 * Manage the page settings 
 * 
 * @author Thilo Muller(2011)
 * @version $Id: page_detail.php 111 2011-11-30 06:22:08Z thilo $
 */

//Include
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Vars
$template = file_get_contents('html/page_detail.html');
$page_id = $_GET['Pid'];
$page_title = '';
$page_description = '';
$page_keywords = '';
$page_template = '';
$template_list = '';
$message = '';

//Get page data
$statement = "SELECT page_title, page_description, page_keywords, template_id
			FROM {$GLOBALS['db_prefix']}_pages
			WHERE page_id=:page_id
			LIMIT 1";
$sql_page = $GLOBALS['dbCon']->prepare($statement);
$sql_page->bindParam(':page_id', $page_id);
$sql_page->execute();
$sql_page_data = $sql_page->fetch();
$sql_page->closeCursor();

$page_title = $sql_page_data['page_title'];
$page_description = $sql_page_data['page_description'];
$page_keywords = $sql_page_data['page_keywords'];

//Get list of templates
$statement = "SELECT template_id, template_name
			FROM {$GLOBALS['db_prefix']}_templates
			ORDER BY template_name";
$sql_template = $GLOBALS['dbCon']->prepare($statement);
$sql_template->execute();
$sql_template_data = $sql_template->fetchAll();
$sql_template->closeCursor();

if (count($_POST) > 0) {
	
	//Vars
	$post_page_title = $_POST['pageTitle'];
	$post_page_description = $_POST['description'];
	$post_page_keywords = $_POST['keywords'];
	$post_template = $_POST['template'];
	
	//Page Title
	if ($page_title != $post_page_title) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_pages
					SET page_title=:page_title
					WHERE page_id=:page_id";
		$sql_title = $GLOBALS['dbCon']->prepare($statement);
		$sql_title->bindParam(':page_title', $post_page_title);
		$sql_title->bindParam(':page_id', $page_id);
		$sql_title->execute();
		
		$message .= '&#187; Page title updated<br>';
	}
	
	//Page Template
	if ($sql_page_data['template_id'] != $post_template) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_pages
					SET template_id=:template_id
					WHERE page_id=:page_id";
		$sql_template = $GLOBALS['dbCon']->prepare($statement);
		$sql_template->bindParam(':template_id', $post_template);
		$sql_template->bindParam(':page_id', $page_id);
		$sql_template->execute();
		
		$message .= '&#187; Page template changed<br>';
	}
	
	//Page Keywords
	if ($page_keywords != $post_page_keywords) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_pages
					SET page_keywords=:page_keywords
					WHERE page_id=:page_id";
		$sql_keywords = $GLOBALS['dbCon']->prepare($statement);
		$sql_keywords->bindParam(':page_keywords', $post_page_keywords);
		$sql_keywords->bindParam(':page_id', $page_id);
		$sql_keywords->execute();
		
		$message .= '&#187; Page keywords updated<br>';
	}
	
	//Page Description
	if ($page_description != $post_page_description) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_pages
					SET page_description=:page_description
					WHERE page_id=:page_id";
		$sql_description = $GLOBALS['dbCon']->prepare($statement);
		$sql_description->bindParam(':page_description', $post_page_description);
		$sql_description->bindParam(':page_id', $page_id);
		$sql_description->execute();
		
		$message .= '&#187; Page description updated<br>';
	}
	
	$change_marker = empty($message)?'0':'1';
	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	

	echo "<script src='js/page_detail.js'></script>";
	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick='closeWindow($change_marker,{$_GET['W']},{$_GET['pW']})' ></div>";

	die();
}


//Assemble template list
foreach ($sql_template_data as $template_data) {
	if ($template_data['template_id'] == $sql_page_data['template_id']) {
		$template_list .= "<option value='{$template_data['template_id']}' selected >{$template_data['template_name']}</option>";
	} else {
		$template_list .= "<option value='{$template_data['template_id']}'>{$template_data['template_name']}</option>";
	}
}

//Replace Tags
$template = str_replace('<!-- page_title -->', $page_title, $template);
$template = str_replace('<!-- keywords -->', $page_keywords, $template);
$template = str_replace('<!-- description -->', $page_description, $template);
$template = str_replace('<!-- template_list -->', $template_list, $template);

echo $template;

?>