/* 
 *  Javascript rollback functions
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: rollback.js 87 2011-09-14 06:45:49Z thilo $
 */

/**
 * Redirect to rollback to an elected date
 * 
 * @param int Eid Area ID
 * @param int D Rollback Date
 * @param int WR
 * @param str Aid Area Name
 */
function rollback(Eid, D, WR, Aid, Pid, W) {
    p = '/modules/content_manager/rollback.php?rollback=' + Eid + '&date=' + D + '&WR=' + WR + '&Aid=' + Aid + '&Pid=' + Pid + '&W=' + W;
	document.location = p;
}

/**
 * Closes the rollback edit window and opens the area editor
 */
function close_edit(W, AName, Pid, Aid, WR, Eid) {
	window.parent.W('Edit Area:' + AName,'/modules/content_manager/editor.php?Pid='+ Pid +'&Aid='+ Aid +'&WR='+ WR +'&Eid='+Eid+'&W=' + W, parent.wh,parent.ww/1.4,0,0,2);
	window.parent.WD(W);
}
