<?php
//Capture page in buffer
ob_start();

//Initialize Variables
$EdiTing=1;
//$search_words = '';
$query_string = '';

//Includes 
require_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";

// get fluid items
include_once $_SERVER['DOCUMENT_ROOT']."/shared/directory_item.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/string.php";

// VERY IMPORTANT! template setup for editor
include_once $_SERVER['DOCUMENT_ROOT']."/modules/content_manager/manager_strip.php";
include_once $_SERVER['DOCUMENT_ROOT']."/modules/content_manager/manager_markers.php";

//Initialize variables
$template_areas=array();
$template_list="";
$page="";

if(isset($_POST['new_name'])) {
	$page=$_POST['new_name'];
} else if(isset($_REQUEST['page'])) {
	$page=$_REQUEST['page'];
} else {
	if(isset($_REQUEST['Pid']) && $_REQUEST['Pid'])	{
		$sql="SELECT page_name FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=".$_REQUEST['Pid'];
		$rsi=mysql_query($sql);
		if(mysql_num_rows($rsi)) {
			$gi=mysql_fetch_array($rsi);
			$page=$gi['page_name'];
		}
	}	else {
		$page="home";
	}
}


//Check if table is empty - First page will be the home page
$page_count=0;
$SQL_statement="SELECT count(*) FROM {$GLOBALS['db_prefix']}_pages";
$SQL_result=mysql_query($SQL_statement);
list($page_count)=mysql_fetch_array($SQL_result);

if($page_count==0) $page=isset($_POST['new_name'])?$_POST['new_name']:"home";	

$SQL_statement="SELECT p.page_id, p.template_id, t.template_file FROM {$GLOBALS['db_prefix']}_pages as p, {$GLOBALS['db_prefix']}_templates as t WHERE p.page_name='".$page."' && p.template_id=t.template_id";
$SQL_result = mysql_query($SQL_statement);
if(mysql_num_rows($SQL_result)==0)
{
	header("Location: /modules/content_manager/newPage.php?page=".$page."&W=".$_GET['W'].(isset($_GET['WR'])?"&WR=".$_GET['WR']:""));
	die();
}
else
{
	list($page_id,$template_id,$template_file)=mysql_fetch_array($SQL_result);
	//Check if $_GET exists or create
	if(!isset($_REQUEST['Pid'])||!isset($_REQUEST['Tid']))
	{
		header("location:".$_SERVER['REQUEST_URI'].(empty($_SERVER['QUERY_STRING'])?"?Pid=".$page_id."&Tid=".$template_id."&W=".$_REQUEST['W']:"&Pid=".$page_id."&Tid=".$template_id."&W=".$_REQUEST['W']));
		die();
	}

	//read template areas
	$template=file_get_contents($_SERVER['DOCUMENT_ROOT']."/templates/".$template_file);
	$template=string_strip($template); // converts tags to lowercase and strips out junk
	$template=str_replace("index.php","modules/content_manager/manager.php",$template); // replace index.php with manager.php
	$template=mark_string($template,"TMP"); // puts ids on all tags without
	include $_SERVER['DOCUMENT_ROOT']."/modules/content_manager/area.php";
}

//get Meta Tags
if(isset($page_id) && $page_id)
{
	$SQL_statement="SELECT page_title, page_description, page_keywords FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=".$page_id;
	if(($SQL_meta=mysql_query($SQL_statement))===FALSE)
		trigger_error(mysql_errro(), E_USER_ERROR);
	list($page_title,$page_description,$page_keywords)=mysql_fetch_array($SQL_meta);
	
	//Replace Meta tags
	if(!empty($page_title))
		$template=preg_replace('@<title>(.*)</title>@i',"<title>$page_title</title>",$template);
	if(!empty($page_keywords))
		$template=preg_replace('@<meta(.*)keywords(.*)>@i',"<meta name=keywords content=\"".$page_keywords."\">",$template);
	if(!empty($page_description))
		$template=preg_replace('@<meta(.*)description(.*)>@i',"<meta name=description content=\"".$page_description."\">",$template);

	// insert menu area
	if(strpos($template,"</body")!==TRUE)
		$template.="</body>";
	if(strpos($template," id=MENU_")!==TRUE)
		$template=str_replace("</body","<span id=MENU_ class=MENU_ lang=0></span></body",$template);
}

// get MAX area_id
//$area_ids="AREA_Aid='";
//$sql="SELECT area_id FROM {$GLOBALS['db_prefix']}_areas ORDER BY area_id DESC LIMIT 1";
//$ra=mysql_query($sql);
//if(mysql_num_rows($ra))
//{
//	$ga=mysql_fetch_array($ra);
//		$area_ids.=$ga['area_id'];
//}
//else
//	$area_ids.="0";
//$area_ids.="';";


// CSS extractor
include_once $_SERVER['DOCUMENT_ROOT']."/shared/css.php";

getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","_style.css");
if($itemname)
{
	echo "<link rel=stylesheet href=/i/fluid/".$itemname.">";
	$css=file_get_contents($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname);
}
else
{
	echo "<link rel=stylesheet href=/shared/style.css>";
	$css=file_get_contents($_SERVER['DOCUMENT_ROOT']."/shared/style.css");
}


// get page title
$sql="SELECT page_title FROM {$GLOBALS['db_prefix']}_pages WHERE page_name='".$page."'";
$rs=mysql_query($sql);
$g=mysql_fetch_array($rs);
$page="EDIT_name=\"".caps($g['page_title'])."\";";



echo "<script>";
echo $page;
if(isset($allpages))
	echo $allpages;
if(isset($area_ids))
	echo $area_ids;
if(isset($menu_styles))
	echo $menu_styles;
echo "</script>";

// Editor Javascript must be loaded in this order
echo "<script src=/shared/jQuery/jquery.js></script>";
echo "<script src=/modules/content_manager/manager_edit.js></script>";
//echo "<script src=/modules/content_manager/toolbar.js></script>";

$template=str_replace("index.php","modules/content_manager/manager.php",$template); // replace index.php with manager.php

//Serve Page
echo "<DL id=EDIT_AREA>";
echo $template;
echo "</DL>";

//Send page buffer to browser
ob_end_flush();
?>