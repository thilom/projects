var LINK_cap=new Array
var LINK_cap_A=new Array
var LINK_act=0

function LINK_tag(v)
{
	if(v.tagName.toUpperCase()!="A")
	{
		v=0
		alert("WARNING!\nNot an <A> link tag!\nTry again...")
	}
	return v
}

function LINK_start(v)
{
	LINK_close()
	m0de_sub=0
	var a=document.getElementById('LINK_box')
	a.style.top=y+9
	a.style.left=x+20
	var b=a.getElementsByTagName('INPUT')
	b[0].value=E_O.innerHTML
	b[1].value=E_O.rev
	b[2].value=E_O.target
	b[3].value=E_O.lang
	a.style.display=""
	EDIT_highLight(1)
	if(E_O.parentNode.id.indexOf("Mp_")==0&&E_O.parentNode.parentNode.id=="MENU_")
		m0de_sub=1
}

function LINK_capture()
{
	if(!LINK_cap_A[E_O])
	{
		LINK_cap_A[E_O]=1
		var n=MENU_cap.length
		LINK_cap[n]=new Array
		LINK_cap[n][0]=(E_O.id?E_O.id:"")
		LINK_cap[n][1]=(E_O.rev?E_O.rev:"")
		LINK_cap[n][2]=(E_O.className?E_O.className:"")
		LINK_cap[n][3]=(E_O.name?E_O.name:"")
		LINK_cap[n][4]=EDIT_getArea()
	}
}

function LINK_save()
{
	if(LINK_act)
	{
		var c=""
		var h=""
		if(LINK_cap.length)
		{
			var i=0
			while(LINK_cap[i])
			{
				c+=LINK_cap[i][0]+"|"
				c+=LINK_cap[i][1]+"|"
				c+=LINK_cap[i][2]+"|"
				c+=LINK_cap[i][3]+"|"
				c+=LINK_cap[i][4]+"}"
				document.getElementsByTagName(LINK_cap[i][0])
				h+=(E_O.innerHTML?E_O.innerHTML:"")+"|"
				h+=(E_O.rev?E_O.rev:"")+"|"
				h+=(E_O.target?E_O.target:"")+"|"
				h+=(E_O.lang?E_O.lang:"")+"}"
				i++
			}
		}

		EDIT_sv+=(c?"<textarea name=LINK_cap>"+c+"</textarea>":"")
		EDIT_sv+=(h?"<textarea name=LINK_val>"+h+"</textarea>":"")
		LINK_cap=new Array
		LINK_cap_A=new Array
		LINK_act=0
	}
}

function LINK_close()
{
	MENU_TX('LINK_box')
	document.getElementById("EDIT_zhl").style.display="none"
}

function LINK_end()
{
	LINK_close()
}

function LINK_value(t,d)
{
	LINK_capture()
	EDIT_mw('LINK_box')
	if(d==1)
		E_O.innerHTML=t.value
	else if(d==2)
	{
		E_O.rev=t.value
		E_O.href=t.value.replace("index.php","modules/content_manager/manager.php")
	}
	else if(d==3)
		E_O.target=t.value
	else if(d==4)
	{
		if(!E_O.id.indexOf("Mq_"))
			alert("WARNING!\nTooltips cannot be applied to menu dropdowns")
		else if(t.value)
		{
			E_O.lang=t.value.replace(/\n/g,"<br>").replace(/"/g,"&quot;").replace(/'/g,"&#39;")
			E_O.onmouseover=function(e){T_S(e,this.lang)}
		}
		else
		{
			E_O.lang=""
			E_O.onmouseover=function(e){}
		}
	}
	LINK_act++
	EDIT_TimeSave()
	EDIT_highLight(1)
}

function LINK_insert_address(v,t)
{
	if(t)
	{
		var o=document.getElementById("Mq_1Ladtg")
		o.value=(v?v:"")
		LINK_value(o,3)
	}
	else
	{
		var o=document.getElementById("Mq_1Laddr")
		o.value=(v?"/index.php?page="+v:"")
		LINK_value(o,2)
	}
}