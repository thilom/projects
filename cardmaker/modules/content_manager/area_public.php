<?php
/**
 * Fetch and display areas for the public
 *
 * @author Thilo Muller(2011)
 * @version $Id: area_public.php 126 2011-12-17 09:40:42Z thilo $
 */

include_once 'functions.php';

$scripts = array();
$stripMarked = "";

preg_match_all("@<!--AREA_([\w]+)-->@i", $template, $matches);
foreach ($matches[1] as $preg_match) {
	$template_areas[] = $preg_match;
}

//Got shared areas
preg_match_all("@<!--SHARED_([\w]+)-->@i", $template, $matches);
foreach ($matches[1] as $preg_match) {
	$shared_areas[] = $preg_match;
}

if (empty($page_id)) {
	$statement = "SELECT page_id FROM {$GLOBALS['db_prefix']}_pages WHERE page_home=1 LIMIT 1";
	$sql_home = $GLOBALS['dbCon']->prepare($statement);
	$sql_home->execute();
	$sql_home_data = $sql_home->fetch();
	$sql_home->closeCursor();
	$page_id = $sql_home_data['page_id'];
}



//get data for each area
$counter = 0;
foreach ($template_areas as $area_id) {
	$edit_count = 0;
	$header = '';
	$module = '';
	$type = '';
	$id = 0;
	$area_content = '';

	//Is there a current area
	$statement = "SELECT id,module,type FROM {$GLOBALS['db_prefix']}_areas WHERE area_id=:area_id AND (page_id=:page_id OR shared='1') LIMIT 1";
	$sql_pages_current = $GLOBALS['dbCon']->prepare($statement);
	$sql_pages_current->bindParam(':area_id', $area_id);
	$sql_pages_current->bindParam(':page_id', $page_id);
	$sql_pages_current->execute();
	$edit_count = $sql_pages_current->rowCount();
	$sql_pages_data = $sql_pages_current->fetch();
	$sql_pages_current->closeCursor();
	$id = $sql_pages_data['id'];

	//Check for current version
	$area_data = get_area_data($id, 'current');

	// module
	$content = "";

	$module_settings = get_module_variables($area_data['data']['module']);
	if (isset($module_settings[$area_data['data']['module']]['content_type'])) {
		foreach ($module_settings[$area_data['data']['module']]['content_type'] as $aid=>$area_ini) {
			 if ($area_ini['id'] == $sql_pages_data['type']) {
				ob_start();
//				echo $_SERVER['DOCUMENT_ROOT'] . "/modules/{$module_settings[$area_data['data']['module']]['module_id']}/{$module_settings[$area_data['data']['module']]['content_type'][$aid]['public_display_file']}";
				include $_SERVER['DOCUMENT_ROOT'] . "/modules/{$module_settings[$area_data['data']['module']]['module_id']}/{$module_settings[$area_data['data']['module']]['content_type'][$aid]['public_display_file']}";

				$content = ob_get_contents();
				ob_end_clean();
			 }

		}
	}



	$area_content = $content;

	$template = str_replace("<!--AREA_" . $area_id . "-->", $area_content, $template);
	$counter++;
}

$counter = 0;
if (!empty($shared_areas)) {

	foreach ($shared_areas as $shared_id) {
		//Get current area data
		$statement = "SELECT id,module FROM {$GLOBALS['db_prefix']}_areas WHERE area_id=:area_id AND shared='1' LIMIT 1";
		$sql_shared_current = $GLOBALS['dbCon']->prepare($statement);
		$sql_shared_data = array();
		$sql_shared_current->bindParam(':area_id', $shared_id);
		$sql_shared_current->execute();
		$sql_shared_data = $sql_shared_current->fetch();
		$sql_shared_current->closeCursor();

		//Create DB entry if one doesn't already exist
//		if (empty($sql_shared_data)) {
//			$id = next_id("{$GLOBALS['db_prefix']}_areas", 'id');
//			$sql_shared_insert->bindParam(':id', $id);
//			$sql_shared_insert->bindParam(':area_id', $shared_id);
//			$sql_shared_insert->execute();
//		} else {
			$id = $sql_shared_data['id'];
//		}

		//javascript areas array
//		if ($sql_setting_data['shared_areas'] =='Y') {
//			$js_areas .= "shr[$counter]='$shared_id'" . PHP_EOL;
//		}


		//Check for current editing version
		$area_data = get_area_data($id,'current');

		// module Data
		$content = '';
//		$area_content = "<div id=EditShared_" . $shared_id . " lang=" . $id . " name=_AREA>";
		$module_settings = get_module_variables($area_data['data']['module']);
		if (isset($module_settings[$area_data['data']['module']]['content_type'])) {
			foreach ($module_settings[$area_data['data']['module']]['content_type'] as $aid=>$area_ini) {
				 if ($area_ini['id'] == $area_data['data']['type']) {
					ob_start();
					include $_SERVER['DOCUMENT_ROOT'] . "/modules/{$module_settings[$area_data['data']['module']]['module_id']}/{$module_settings[$area_data['data']['module']]['content_type'][$aid]['display_file']}";

					$content = ob_get_contents();
					ob_end_clean();
				 }
			}
		}
//		if (isset($content)) {
//			$content_test = trim(strip_tags($content));
//		} else {
//			$content_test = '';
//		}

//		$area_content.="<div id=_HD$id >$content</div>";
//		$area_content.="</div>";

		//Remove dirt
		$content = str_replace('//<![CDATA[', '', $content);
		$content = str_replace('//]]', '', $content);
		
		$template = str_replace("<!--SHARED_" . $shared_id . "-->", $content . '', $template);

		$counter++;
	}
}

?>