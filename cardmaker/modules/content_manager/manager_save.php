<?php
ob_start();
include_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";


// VERY IMPORTANT! template setup for editor
include_once $_SERVER['DOCUMENT_ROOT']."/modules/content_manager/manager_strip.php";
include_once $_SERVER['DOCUMENT_ROOT']."/modules/content_manager/manager_markers.php";
include_once $_SERVER['DOCUMENT_ROOT']."/modules/content_manager/innova_clean.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/document_write.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/directory_item.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/string.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/css.php";
$date=mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));

$contents="";
$mark="";
$area_type="";
$area_id="";
$item="";
$backup_template="";
$idtag_name="";


// recompiler
function ReComiple($a="")
{
	$o="";
	foreach($a as $k => $v)
	{
		$o.=$v;
		if(strpos($v,"<")!==FALSE)
			$o.=">";
	}
	$a=$o;
	$a=str_replace("  "," ",$a);
	$a=str_replace("  "," ",$a);
	$a=str_replace(" >",">",$a);
	$a=str_replace(">\n>",">",$a);
	return $a;
}


// stripper
function elementstrip($e,$v)
{
	if(stripos($e,$v)>-1)
	{
		$part1=substr($e,0,stripos($e,$v));
		$part2=substr($e,stripos($e,$v));
		$part2=substr($part2,1);
		$part2=substr($part2,stripos($part2," "));
		$e=$part1.$part2;
	}
	return $e;
}



// get Contents______________________________

function getContents($b,$e=0)
{
	global $contents;
	global $mark;
	global $area_type;
	global $area_id;
	global $item;
	global $backup_template;
	global $backup_template_name;
	global $idtag_name;

	$contents="";
	$mark="";
	$area_type=substr($b,0,3);
	$area_id=substr($b,3);
	$item="";

	if($area_type=="TMP")
	{
		$o="";
		$sql="SELECT template_file FROM {$GLOBALS['db_prefix']}_templates WHERE template_id=".$_POST['Tid'];
		$rt=mysql_query($sql);
		if(mysql_num_rows($rt))
		{
			$gt=mysql_fetch_array($rt);
			$item=$gt['template_file'];
			$contents=file_get_contents($_SERVER['DOCUMENT_ROOT']."/templates/".$item);
			if(!$backup_template)
				$backup_template=$contents;
		}
		$idtag_name="TMP";
	}
	else
	{
		$sql="SELECT ".($area_type=="_HD"?"header":"footer")." AS content FROM {$GLOBALS['db_prefix']}_areas WHERE id=".$area_id;
		$ra=mysql_query($sql);
		if(mysql_num_rows($ra))
		{
			$ga=mysql_fetch_array($ra);
			$item=$area_id;
			$contents=stripslashes($ga['content']);
		}
		if($area_type=="_HD")
			$idtag_name="HDR";
		elseif($area_type=="_FT")
			$idtag_name="FTR";
	}
	$contents=string_strip($contents); // converts tags to lowercase and strips out junk
	if($e)
	{
		$contents=str_replace(">"," >",$contents);
		$unmarked_contents=$contents;
		$contents=mark_string($contents,$idtag_name); // puts ids on all tags without
		$contents=explode(">",$unmarked_contents);
	}
}




// MENU_______________________________________________________________
if(isset($_POST['MENU_style']))
{
	// styles
	if(isset($_POST['MENU_style']))
	{
		getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","_style.css");
		if($itemname)
		{
			$css=file_get_contents($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname);
			unlink($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname);
		}
		else
			$css=file_get_contents($_SERVER['DOCUMENT_ROOT']."/shared/style.css");
		$x=explode("|",$_POST['MENU_style']);
		$css=css_insert($css,".MENU_ p a:link,.MENU_ p a:visited,.MENU_ p a:hover","color",($x[0]?$x[0]:"#000"));
		$css=css_insert($css,".MENU_ p a:link,.MENU_ p a:visited,.MENU_ p a:hover","background",($x[1]?$x[1]:"#FFF"));
		$css=css_insert($css,".MENU_ p a:hover","color",($x[2]?$x[2]:"#FFF"));
		$css=css_insert($css,".MENU_ p a:hover","background",($x[3]?$x[3]:"#CC0000"));
		$css=css_insert($css,".MENU_ p a:link,.MENU_ p a:visited,.MENU_ p a:hover","border-color",($x[4]?$x[4]:"#888"));
		$css=css_insert($css,".MENU_ p","border-color",($x[4]?$x[4]:"#888"));
		$css=str_replace("\n\n\n","\n\n",str_replace("\n\n\n\n","\n\n",$css));
		write_doc($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$date."_style.css",$css);
	}

	// get template
	if(isset($_POST['MENU_cap'])) // new or edited menus
	{
		// tag locator function

		function get_element($a)
		{
			global $contents;
			global $mark;
			global $area_type;
			global $area_id;
			global $item;
			global $idtag_name;

			$b=explode("|",$a);
			$b[1]="<".$b[1]." ";

			if($b[8])
			{
				getContents($b[8],1);

				$score=array();
				foreach($contents as $k => $v)
				{
					if(
					$v==""
					||
					strpos($v,"</")!==FALSE
					||
					strpos($v,"<!")!==FALSE
					||
					stripos($v,"<abbr")!==FALSE
					||
					stripos($v,"<applet")!==FALSE
					||
					stripos($v,"<address")!==FALSE
					||
					stripos($v,"<area")!==FALSE
					||
					stripos($v,"<body")!==FALSE
					||
					stripos($v,"<bgsound")!==FALSE
					||
					stripos($v,"<br")!==FALSE
					||
					stripos($v,"<colgroup")!==FALSE
					||
					stripos($v,"<embed")!==FALSE
					||
					stripos($v,"<form")!==FALSE
					||
					stripos($v,"<head")!==FALSE
					||
					stripos($v,"<html")!==FALSE
					||
					stripos($v,"<hr")!==FALSE
					||
					stripos($v,"<iframe")!==FALSE
					||
					stripos($v,"<link")!==FALSE
					||
					stripos($v,"<map")!==FALSE
					||
					stripos($v,"<meta")!==FALSE
					||
					stripos($v,"<object")!==FALSE
					||
					stripos($v,"<option")!==FALSE
					||
					stripos($v,"<optgroup")!==FALSE
					||
					stripos($v,"<param")!==FALSE
					||
					stripos($v,"<script")!==FALSE
					||
					stripos($v,"<style")!==FALSE
					||
					stripos($v,"<tbody")!==FALSE
					||
					stripos($v,"<thead")!==FALSE
					||
					stripos($v,"<tfoot")!==FALSE
					||
					stripos($v,"<title")!==FALSE
					||
					stripos($v,"<tr")!==FALSE
					||
					stripos($v,"<xml")!==FALSE
					||
					stripos($v,"<xmp")!==FALSE
					)
					{}
					else
					{
						$i=0;
						while($i<9)
						{
							if($b[$i] && $v && $mark[$k] && (strpos($mark[$k],$b[$i])!==FALSE || stripos($v,$b[$i])!==FALSE))
							{
								if($i==2) // id check
								{
									if(strpos($mark[$k],"Mq_")!==FALSE || strpos($mark[$k],$idtag_name)!==FALSE) // id
										$score[$k]=(isset($score[$k])?$score[$k]+5:5);
									else
										$score[$k]=(isset($score[$k])?$score[$k]+3:3);
								}
								else
									$score[$k]=(isset($score[$k])?$score[$k]+1:1);
							}
							$i++;
						}
					}
				}
				$found=-1;
				arsort($score);
				foreach($score as $k => $v)
				{
					if($found<0)
					{
						$found=$k;
						break;
					}
				}
				if($found>-1)
				{
					$tag=$contents[$found];
					$tag=elementstrip($tag," lang=");
					$tag=elementstrip($tag," id=");
					$tag=elementstrip($tag," onmouseover=");
					$tag=elementstrip($tag," onmouseout=");
					if($b[0])
						$tag=$tag." id=".$b[0]." onmouseover=_MENU(this,1) onmouseout=_MENU(this)";
					$contents[$found]=$tag;
					// finish
					$contents=ReComiple($contents);
					$contents=str_replace("  "," ",$contents);
					$contents=str_replace(" >",">",$contents);
					$contents=str_replace(">>",">",$contents);
					if($area_type=="TMP")
						write_doc($_SERVER['DOCUMENT_ROOT']."/templates/".$item,$contents);
					else
					{
						$contents=InnovaClean($contents);
						$sql="UPDATE {$GLOBALS['db_prefix']}_areas SET ".($area_type=="_HD"?"header":"footer")."='".$contents."' WHERE id=".$item;
						mysql_query($sql);
					}
				}
			}
		}

		// find elements
		$a=explode("}",$_POST['MENU_cap']);
		$i=0;
		while($a[$i])
		{
			get_element($a[$i]);
			$i++;
		}
	}


	if(isset($_POST['MENU_link']) && $_POST['MENU_link'])
	{
		$a1=explode("]!",$_POST['MENU_link']);
		foreach($a1 as $a0 => $a)
		{
			if($a)
			{
				$p="";
				$b=explode("![",$a);
				$c=explode(".",$b[0]);
				getContents($c[0],0);
				// remove menu container
				if($contents && strpos($contents,"<!-- MENUs")!==FALSE)
				{
					$part1=substr($contents,0,strpos($contents,"<!-- MENUs"));
					$part2=substr($contents,strpos($contents,"<!-- MENUs"));
					$part2=substr($part2,strpos($part2,"<!-- MENUs END")+18);
					$contents=$part1.$part2;
				}
				// construct menu container
				$d=explode("}",$b[1]);
				if(count($d)>1)
				{
					$p="\n";
					$p.="<!-- MENUs -->\n";
					$p.="<!-- DO NOT EDIT! -->\n";
					$p.="<span id=MENU".($area_type=="TMP"?"_":$area_type.$item)." class=MENU_ lang=".($c[1]?$c[1]:0).">\n";
					$i=0;
					while(isset($d[$i]))
					{
						if($d[$i])
						{
							$e=explode("{",$d[$i]);
							if($e[0]>-1)
							{
								$o="";
								$f=explode("^",$e[1]);
								$j=0;
								while($f[$j])
								{
									$g=explode("|",$f[$j]);
									$o.="<a href=".($g[3]?$g[3]:"javascript:void(0)");
									$o.=($g[4]?" target=".$g[4]:"");
									$o.=" onfocus=blur()";
									$o.=($g[0] && strpos($g[0],"cmS_")!==TRUE?" id=".$g[0]:"");
									$o.=($g[0] && strpos($g[0],"Mq_")!==FALSE?" onmouseover=_MENU(this,1) onmouseout=_MENU(this)":"");
									$o.=($g[2] && strpos($g[0],"Mq_")!==TRUE?" lang=\"".$g[2]."\" onmouseover=\"T_S(event,this.lang)\"":"");
									$o.=">";
									$o.=($g[1]?$g[1]:"Untitled");
									$o.="</a>";
									$o.="\n";
									$j++;
								}
								if($o)
									$p.="<p id=".$e[0]." onmouseover=_MENU(0,1,this) onmouseout=_MENU(0,0,this) style=display:none>\n".$o."</p>\n";
							}
						}
						$i++;
					}
					$p.="</span>\n";
					$p.="<!-- MENUs END -->\n";
					$p.="\n";
					$p=str_replace("> <","><",$p);
				}
	
				// insert menu container
				if($area_type=="TMP")
				{
					$part1=substr($contents,0,strpos($contents,"</body"));
					$part2=substr($contents,strpos($contents,"</body"));
					$contents=$part1.$p.$part2;
				}
				else
				{
					$contents=InnovaClean($contents);
					$contents.=$p;
				}
				// finish
				if($area_type=="TMP")
					write_doc($_SERVER['DOCUMENT_ROOT']."/templates/".$item,$contents);
				else
				{
					$sql="UPDATE {$GLOBALS['db_prefix']}_areas SET ".($area_type=="_HD"?"header":"footer")."='".$contents."' WHERE id=".$item;
					mysql_query($sql);
				}
			}
		}
	}
}




// LINK_______________________________________________________________
if(isset($_POST['LINK_cap']) && $_POST['LINK_cap'])
{
	function get_element($b,$a)
	{
		global $contents;
		global $mark;
		global $area_type;
		global $area_id;
		global $item;
		global $idtag_name;

		$b=explode("|",$b);
		$b[1]="<".$b[1]." ";

		if($b[4])
		{
			getContents($b[4],1);

			$score=array();
			foreach($contents as $k => $v)
			{
				if(
				$v==""
				||
				strpos($v,"</")!==FALSE
				||
				strpos($v,"<!")!==FALSE
				||
				stripos($v,"<abbr")!==FALSE
				||
				stripos($v,"<applet")!==FALSE
				||
				stripos($v,"<address")!==FALSE
				||
				stripos($v,"<area")!==FALSE
				||
				stripos($v,"<body")!==FALSE
				||
				stripos($v,"<bgsound")!==FALSE
				||
				stripos($v,"<br")!==FALSE
				||
				stripos($v,"<colgroup")!==FALSE
				||
				stripos($v,"<embed")!==FALSE
				||
				stripos($v,"<form")!==FALSE
				||
				stripos($v,"<head")!==FALSE
				||
				stripos($v,"<html")!==FALSE
				||
				stripos($v,"<hr")!==FALSE
				||
				stripos($v,"<iframe")!==FALSE
				||
				stripos($v,"<link")!==FALSE
				||
				stripos($v,"<map")!==FALSE
				||
				stripos($v,"<meta")!==FALSE
				||
				stripos($v,"<object")!==FALSE
				||
				stripos($v,"<option")!==FALSE
				||
				stripos($v,"<optgroup")!==FALSE
				||
				stripos($v,"<param")!==FALSE
				||
				stripos($v,"<script")!==FALSE
				||
				stripos($v,"<style")!==FALSE
				||
				stripos($v,"<tbody")!==FALSE
				||
				stripos($v,"<thead")!==FALSE
				||
				stripos($v,"<tfoot")!==FALSE
				||
				stripos($v,"<title")!==FALSE
				||
				stripos($v,"<tr")!==FALSE
				||
				stripos($v,"<xml")!==FALSE
				||
				stripos($v,"<xmp")!==FALSE
				)
				{}
				else
				{
					$i=0;
					while($i<5)
					{
						if($b[$i] && $mark[$k] && strpos($v,"<a ")!==FALSE && (strpos($mark[$k],$b[$i])!==FALSE || stripos($v,$b[$i])!==FALSE))
						{
							if($i==0) // id check
							{
								if(strpos($mark[$k],$idtag_name)!==FALSE) // id
									$score[$k]=(isset($score[$k])?$score[$k]+5:5);
								else
									$score[$k]=(isset($score[$k])?$score[$k]+3:3);
							}
							else
								$score[$k]=(isset($score[$k])?$score[$k]+1:1);
						}
						$i++;
					}
				}
			}
			$found=-1;
			arsort($score);
			foreach($score as $k => $v)
			{
				if($found<0)
				{
					$found=$k;
					break;
				}
			}
			if($found>-1)
			{
				$tag=$contents[$found];
				$values=explode("|",$_POST['LINK_val'][$a]);
				$tag=elementstrip($tag," href=");
				$tag=elementstrip($tag," target=");
				$tag=elementstrip($tag," lang=");
				$tag=elementstrip($tag," onmouseover=");
				$tag=elementstrip($tag," onmouseout=");
				$tag=elementstrip($tag," onfocus=");
				if($values[1])
					$tag=$tag." href=".($values[1]?$values[1]:"javascript:void(0)");
				if($values[2])
					$tag=$tag." target=".$values[2];
				if($values[3])
					$tag=$tag." lang=\"".str_replace("\"","&quot;",str_replace("\n","<br>",str_replace("'","&#39;",$values[3])))."\" onmouseover=T_S(event,this.lang)";
				$tag=$tag." onfocus=blur()";
				$contents[$found]=$tag;
				// innerHTML
				$tag=$contents[$found+1];
				$tag=$values[0].substr($tag,strpos($tag,"<"));
				$contents[$found+1]=$tag;
				// finish
				$contents=ReComiple($contents);
				$contents=str_replace("  "," ",$contents);
				$contents=str_replace(" >",">",$contents);
				$contents=str_replace(">>",">",$contents);
				if($area_type=="TMP")
					write_doc($_SERVER['DOCUMENT_ROOT']."/templates/".$item,$contents);
				else
				{
					$contents=InnovaClean($contents);
					$sql="UPDATE {$GLOBALS['db_prefix']}_areas SET ".($area_type=="_HD"?"header":"footer")."='".$contents."' WHERE id=".$item;
					mysql_query($sql);
				}
			}
		}
	}

	// find elements
	$_POST['LINK_val']=explode("}",$_POST['LINK_val']);
	$a=explode("}",$_POST['LINK_cap']);
	$i=0;
	while($a[$i])
	{
		get_element($a[$i],$i);
		$i++;
	}

}







// LINK_______________________________________________________________
if(isset($_POST['TIP_cap']) && $_POST['TIP_cap'])
{
	function get_element($b,$a)
	{
		global $contents;
		global $mark;
		global $area_type;
		global $area_id;
		global $item;
		global $idtag_name;

		$b=explode("|",$b);
		$b[1]="<".$b[1]." ";

		if($b[8])
		{
			getContents($b[8],1);

			$score=array();
			foreach($contents as $k => $v)
			{
				if(
				$v==""
				||
				strpos($v,"</")!==FALSE
				||
				strpos($v,"<!")!==FALSE
				||
				stripos($v,"<abbr")!==FALSE
				||
				stripos($v,"<applet")!==FALSE
				||
				stripos($v,"<address")!==FALSE
				||
				stripos($v,"<area")!==FALSE
				||
				stripos($v,"<body")!==FALSE
				||
				stripos($v,"<bgsound")!==FALSE
				||
				stripos($v,"<br")!==FALSE
				||
				stripos($v,"<colgroup")!==FALSE
				||
				stripos($v,"<embed")!==FALSE
				||
				stripos($v,"<form")!==FALSE
				||
				stripos($v,"<head")!==FALSE
				||
				stripos($v,"<html")!==FALSE
				||
				stripos($v,"<hr")!==FALSE
				||
				stripos($v,"<iframe")!==FALSE
				||
				stripos($v,"<link")!==FALSE
				||
				stripos($v,"<map")!==FALSE
				||
				stripos($v,"<meta")!==FALSE
				||
				stripos($v,"<object")!==FALSE
				||
				stripos($v,"<option")!==FALSE
				||
				stripos($v,"<optgroup")!==FALSE
				||
				stripos($v,"<param")!==FALSE
				||
				stripos($v,"<script")!==FALSE
				||
				stripos($v,"<style")!==FALSE
				||
				stripos($v,"<tbody")!==FALSE
				||
				stripos($v,"<thead")!==FALSE
				||
				stripos($v,"<tfoot")!==FALSE
				||
				stripos($v,"<title")!==FALSE
				||
				stripos($v,"<tr")!==FALSE
				||
				stripos($v,"<xml")!==FALSE
				||
				stripos($v,"<xmp")!==FALSE
				)
				{}
				else
				{
					$i=0;
					while($i<9)
					{
						if($b[$i] && $mark[$k] && (strpos($mark[$k],$b[$i])!==FALSE || stripos($v,$b[$i])!==FALSE))
						{
							if($i==1) // id check
							{
								if(strpos($mark[$k],$idtag_name)!==FALSE) // id
									$score[$k]=(isset($score[$k])?$score[$k]+5:5);
								else
									$score[$k]=(isset($score[$k])?$score[$k]+3:3);
							}
							else
								$score[$k]=(isset($score[$k])?$score[$k]+1:1);
						}
						$i++;
					}
				}
			}
			$found=-1;
			arsort($score);
			foreach($score as $k => $v)
			{
				if($found<0)
				{
					$found=$k;
					break;
				}
			}
			if($found>-1)
			{
				$tag=$contents[$found];
				$values=explode("|",$_POST['TIP_val'][$a]);
				$tag=elementstrip($tag," lang=");
				$tag=elementstrip($tag," onmouseover=");
				$tag=elementstrip($tag," onmouseout=");
				if($values[0])
					$tag=$tag." lang=\"".str_replace("\"","&quot;",str_replace("\n","<br>",str_replace("'","&#39;",$values[0])))."\" onmouseover=T_S(event,this.lang)";
				$contents[$found]=$tag;
				// finish
				$contents=ReComiple($contents);
				$contents=str_replace("  "," ",$contents);
				$contents=str_replace(" >",">",$contents);
				$contents=str_replace(">>",">",$contents);
				if($area_type=="TMP")
					write_doc($_SERVER['DOCUMENT_ROOT']."/templates/".$item,$contents);
				else
				{
					$contents=InnovaClean($contents);
					$sql="UPDATE {$GLOBALS['db_prefix']}_areas SET ".($area_type=="_HD"?"header":"footer")."='".$contents."' WHERE id=".$item;
					mysql_query($sql);
				}
			}
		}
	}

	// find elements
	$_POST['TIP_val']=explode("}",$_POST['TIP_val']);
	$a=explode("}",$_POST['TIP_cap']);
	$i=0;
	while($a[$i])
	{
		get_element($a[$i],$i);
		$i++;
	}

}







// save backup of template
if($backup_template)
{
	$file=$_SERVER['DOCUMENT_ROOT']."/templates/backup/".str_replace(".html","",$item)."~".date('Y-m-d',$date).".html";
	if(!file_exists($file))
		write_doc($file,$backup_template);
}


ob_end_flush();
?>