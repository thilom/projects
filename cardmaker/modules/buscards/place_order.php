<?php

/*
 * Save user data to the DB and redirect to payment gateway
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$session_id = session_id();
$card_id = $_POST['card_id'];
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/buscards/html/paygate.html'); 
$currency = 'ZAR';
$order_value = str_replace('.','',$_POST['TransactionValue']);
$paygate_id = "10011013800";
$reference = str_pad($_POST['card_id'], 8, 0, STR_PAD_LEFT);
$return_url = "http://{$_SERVER['HTTP_HOST']}/modules/buscards/paygate_return.php";
$date = date('Y-m-d H:m:s');
$key = 'secret';
$email = $_POST['contactAddress'];

//Transaction checksum
$checksum = MD5("$paygate_id|$reference|$order_value|ZAR|$return_url|$date|$email|$key");

//Save to DB
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_buscard_user
					(card_id, name, company, telephone, email, street, town, province, postal_code, session_id)
				VALUES
					(:card_id, :name, :company, :telephone, :email, :street, :town, :province, :postal_code, :session_id)
				ON DUPLICATE KEY UPDATE
					card_id=:card_id, company=:company, name=:name, telephone=:telephone, email=:email, street=:street, town=:town, 
					province=:province, postal_code=:postal_code";
$sql_save = $GLOBALS['dbCon']->prepare($statement);
$sql_save->bindParam(':card_id', $card_id);
$sql_save->bindParam(':name', $_POST['contactName']);
$sql_save->bindParam(':company', $_POST['contactCompany']);
$sql_save->bindParam(':telephone', $_POST['contactTelephone']);
$sql_save->bindParam(':email', $email);
$sql_save->bindParam(':street', $_POST['street']);
$sql_save->bindParam(':town', $_POST['town']);
$sql_save->bindParam(':province', $_POST['province']);
$sql_save->bindParam(':postal_code', $_POST['postal']);
$sql_save->bindParam(':session_id', $session_id);
$sql_save->execute();

//Replace tags
$template = str_replace('<!-- paygate_id -->', $paygate_id, $template);
$template = str_replace('<!-- reference -->', $reference, $template);
$template = str_replace('<!-- amount -->', $order_value, $template);
$template = str_replace('<!-- return_url -->', $return_url, $template);
$template = str_replace('<!-- date -->', $date, $template);
$template = str_replace('<!-- email -->', $email, $template);
$template = str_replace('<!-- checksum -->', $checksum, $template);


echo $template;

?>
