<?php

/*
 * Business Card: successful transactions
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/shared/database_functions.php';

// Toolbar
include "toolbar.php";

//Vars
$template = file_get_contents('html/buscard_failed.html');


echo $template;

?>