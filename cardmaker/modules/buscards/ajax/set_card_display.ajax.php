<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2014/06/26
 * Time: 10:21 AM
 */


//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/shared/database_functions.php';

//Vars
$state = $_POST['state'] == 'true'?'Y':'';

//Set state
$statement = "UPDATE {$GLOBALS['db_prefix']}_buscard_data
                SET display_card_admin = :state
                WHERE card_id = :card_id
                LIMIT 1";
$sql_update = $GLOBALS['dbCon']->prepare($statement);
$sql_update->bindParam(':card_id', $_POST['id']);
$sql_update->bindParam(':state', $state);
$sql_update->execute();