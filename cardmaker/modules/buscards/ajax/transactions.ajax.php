<?php

/*
 * Fetch a list of transactions
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/shared/database_functions.php';

//Vars
$status = $_GET['status'];
$transactions = '{ '. PHP_EOL .' "aaData": [' ;

//Get transaction list
if ($status == 'success') {
	$statement = "SELECT a.card_id, c.name, c.company, b.transaction_id, b.amount, DATE_FORMAT(b.transaction_date, '%d-%m-%Y') AS transaction_date
					FROM {$GLOBALS['db_prefix']}_buscard_data AS a
					LEFT JOIN {$GLOBALS['db_prefix']}_buscard_transaction AS b ON a.card_id=b.card_id
					LEFT JOIN {$GLOBALS['db_prefix']}_buscard_user AS c ON a.card_id=c.card_id
					WHERE b.transaction_status=1";
	$sql_transactions = $GLOBALS['dbCon']->prepare($statement);
	$sql_transactions->execute();
	$sql_transactions_data = $sql_transactions->fetchAll();
	$sql_transactions->closeCursor();
} else {
	$statement = "SELECT a.card_id, c.name, c.company, b.transaction_id, b.amount, DATE_FORMAT(b.transaction_date, '%d-%m-%Y') AS transaction_date
					FROM {$GLOBALS['db_prefix']}_buscard_data AS a
					LEFT JOIN {$GLOBALS['db_prefix']}_buscard_transaction AS b ON a.card_id=b.card_id
					LEFT JOIN {$GLOBALS['db_prefix']}_buscard_user AS c ON a.card_id=c.card_id
					WHERE b.transaction_status!=1";
	$sql_transactions = $GLOBALS['dbCon']->prepare($statement);
	$sql_transactions->execute();
	$sql_transactions_data = $sql_transactions->fetchAll();
	$sql_transactions->closeCursor();
}


//Assemble JSON transactions
foreach ($sql_transactions_data as $transaction_data) {
	$transaction_amount = substr($transaction_data['amount'],0, -2) . '.' . substr($transaction_data['amount'], -2);
	$transactions .=  PHP_EOL .'{' . PHP_EOL;
	$transactions .= '"date": "' . $transaction_data['transaction_date'] . '",' . PHP_EOL;
	$transactions .= '"name": "' . $transaction_data['name'] . '",' . PHP_EOL;
	$transactions .= '"company": "' . $transaction_data['company'] . '",' . PHP_EOL;
	$transactions .= '"transactionID": "' . ("CARD" . str_pad($transaction_data['card_id'], 5, 0, STR_PAD_LEFT)). '",' . PHP_EOL;
	$transactions .= '"amount": "' . $transaction_amount . '",' . PHP_EOL;
	$transactions .= '"button": "<button onclick=\'displayTransaction(\"'.$transaction_data['card_id'].'\")\'>View</button>"' . PHP_EOL;
	$transactions .= "},";
}

$transactions = substr($transactions,0, -1);
$transactions .= ']}';

echo $transactions;
?>
