<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Business Cards";
$module_id = "buscards"; //needs to be the same as the module directory
$module_icon = 'off_card_design_64.png';
$module_link = 'buscard.php';
$window_width = '800';
$window_height = '';
$window_xposition = '';
$window_yposition = '';

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Business Cards";
$security_id = "mod_buscards";

//Content Types
$content_type[0]['name'] = 'Business Card: Order Form';
$content_type[0]['id'] = 'buscard_order';
$content_type[0]['content_file'] = 'edit/buscard_order.php';
$content_type[0]['display_file'] = 'view/buscard_order.php';
$content_type[0]['public_display_file'] = 'view/buscard_order.php';
$content_type[0]['preferences_file'] = 'edit/buscard_order.php';
$content_type[0]['css_file'] = 'edit/buscard_order.php';
$content_type[0]['explanation'] = 'The order form for business cards';

$content_type[1]['name'] = 'Business Card: View Card';
$content_type[1]['id'] = 'buscard_view';
$content_type[1]['content_file'] = 'edit/buscard_view.php';
$content_type[1]['display_file'] = 'view/buscard_view.php';
$content_type[1]['public_display_file'] = 'view/buscard_view.php';
$content_type[1]['preferences_file'] = 'edit/buscard_view.php';
$content_type[1]['css_file'] = 'edit/buscard_view.php';
$content_type[1]['explanation'] = 'Show the back and front of card (In-line PDF)';

//Settings
//$settings[0]['name'] = "Products";
//$settings[0]['file'] = "$module_id/settings/product_settings.php";
//$settings[0]['order'] = "20";
?>