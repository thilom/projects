

/**
 * Initialize JS
 */
function init() {
	
}

/**
 * Get transactions
 */
function getTransactions(status) {
	$(document).ready(function() {	
		var oTable = $('#transactionList').dataTable( {
			"bProcessing": true,
			"sAjaxSource": '/modules/buscards/ajax/transactions.ajax.php?status=' + status,
			"aoColumns": [
				{ "mData": "date",  "sType": "date"},
				{ "mData": "name" },
				{ "mData": "company" },
				{ "mData": "transactionID" },
				{ "mData": "amount" },
				{ "mData": "button", "bSortable": false }
			],
			"bJQueryUI": true
		} );
	} );
}

/**
 * Display a selected transaction
 */
function displayTransaction(cardID) {
	url = '/modules/buscards/viewBuscard.php?cardID=' + cardID;
	title = "View Business Card Transaction";
	window.parent.W(title,url,500,900);
}
