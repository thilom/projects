<?php

/*
 * Return from payment gateway
 * 
 * @todo Send email on successfull transaction
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//vars
$paygate_id = $_POST['PAYGATE_ID'];
$reference = $_POST['REFERENCE'];
$transaction_status = $_POST['TRANSACTION_STATUS'];
$result_code = $_POST['RESULT_CODE'];
$auth_code = $_POST['AUTH_CODE'];
$amount = $_POST['AMOUNT'];
$result_desc = $_POST['RESULT_DESC'];
$transaction_id = $_POST['TRANSACTION_ID'];
$risk_indicator = $_POST['RISK_INDICATOR'];
$key = 'secret';
$card_id = (int) $_POST['REFERENCE'];


//Authunticate transaction
$checksum = md5("$paygate_id|$reference|$transaction_status|$result_code|$auth_code|$amount|$result_desc|$transaction_id|$risk_indicator|$key");
if ($checksum !== $_POST['CHECKSUM']) {
	$transaction_status = 9;
}

//Save transaction
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_buscard_transaction
					(card_id, transaction_status, result_code, amount, result_desc, transaction_id, risk_indicator, transaction_date)
				VALUES
					(:card_id, :transaction_status, :result_code, :amount, :result_desc, :transaction_id, :risk_indicator, NOW())";
$sql_result = $GLOBALS['dbCon']->prepare($statement);
$sql_result->bindParam(':card_id', $card_id);
$sql_result->bindParam(':transaction_status', $transaction_status);
$sql_result->bindParam(':result_code', $result_code);
$sql_result->bindParam(':amount', $amount);
$sql_result->bindParam(':result_desc', $result_desc);
$sql_result->bindParam(':transaction_id', $transaction_id);
$sql_result->bindParam(':risk_indicator', $risk_indicator);
$sql_result->execute();

//Redirect
if ($transaction_status == 1) {
	
	/* Email - START */
	
	//Get user data
	$statement = "SELECT name, company, telephone, email, street, town, province, postal_code
					FROM {$GLOBALS['db_prefix']}_buscard_user
					WHERE card_id=:card_id
					LIMIT 1";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);
	$sql_user->bindParam(':card_id', $card_id);
	$sql_user->execute();
	$sql_user_data = $sql_user->fetch();
	$sql_user->closeCursor();
	
	//Get card details
	$statement = "SELECT pdf_file_front, pdf_file_back
					FROM {$GLOBALS['db_prefix']}_buscard_data
					WHERE card_id=:card_id
					LIMIT 1";
	$sql_card = $GLOBALS['dbCon']->prepare($statement);
	$sql_card->bindParam(':card_id', $card_id);
	$sql_card->execute();
	$sql_card_data = $sql_card->fetch();
	$sql_card->closeCursor();
	
	//Get transaction data
	$statement = "SELECT transaction_status, result_code, transaction_date, transaction_id, amount, result_desc
					FROM {$GLOBALS['db_prefix']}_buscard_transaction
					WHERE card_id=:card_id";
	$sql_transaction = $GLOBALS['dbCon']->prepare($statement);
	$sql_transaction->bindParam(':card_id', $card_id);
	$sql_transaction->execute();
	$sql_transaction_data = $sql_transaction->fetchAll();
	$sql_transaction->closeCursor();

	//Assemble transactions
	foreach ($sql_transaction_data as $transaction_data) {
		switch ($transaction_data['transaction_status']) {
			case '1':
				$status = '<span style="color: green; font-weight: bold">Approved</span>';
				break;
			case '0':
				$status = '<span style="color: orangered; font-weight: bold">Not Done</span>';
				break;
			case '2':
				$status = '<span style="color: red; font-weight: bold">Declined</span>';
				break;
			case '9':
				$status = '<span style="color: red; font-weight: bold">Possible Transaction Tampering</span>';
				break;
		}
		$transaction_amount = substr($transaction_data['amount'],0, -2) . '.' . substr($transaction_data['amount'], -2);
		$transactions .= "<tr style='border-bottom: 1px dotted white'>";
		$transactions .= "<td style='width: 150px; font-weight: bold; font-size: 12px; background-color: gray'>Date</td><td style='background-color: silver; font-size: 12px;'>{$transaction_data['transaction_date']}</td>";
		$transactions .= "<td style='width: 150px; font-weight: bold; font-size: 12px; background-color: gray'>Status</td><td style='background-color: silver; font-size: 12px;'>$status</td>";
		$transactions .= "</tr>";
		$transactions .= "<tr style='border-bottom: 1px dotted white'>";
		$transactions .= "<td style='width: 150px; font-weight: bold; font-size: 12px; background-color: gray'>Transaction ID</td><td style='background-color: silver; font-size: 12px;'>{$transaction_data['transaction_id']}</td>";
		$transactions .= "<td style='width: 150px; font-weight: bold; font-size: 12px; background-color: gray'>Result Code</td><td style='background-color: silver; font-size: 12px;'>{$transaction_data['result_code']}</td>";
		$transactions .= "</tr>";
		$transactions .= "<tr style='border-bottom: 1px dotted white'>";
		$transactions .= "<td style='width: 150px; font-weight: bold; font-size: 12px; background-color: gray'>Amount</td><td style='background-color: silver; font-size: 12px;'>{$transaction_amount}</td>";
		$transactions .= "<td style='width: 150px; font-weight: bold; font-size: 12px; background-color: gray'>Result Description</td><td style='background-color: silver; font-size: 12px;'>{$transaction_data['result_desc']}</td>";
		$transactions .= "</tr>";
		$transactions .= "<tr><td colspan=5><hr></td></tr>";
	}
	
	//Assemble Email
	$email_contents = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/buscards/html/email_content.html');
	$email_contents = str_replace('<!-- name -->', $sql_user_data['name'], $email_contents);
	$email_contents = str_replace('<!-- company -->', $sql_user_data['company'], $email_contents);
	$email_contents = str_replace('<!-- telephone -->', $sql_user_data['telephone'], $email_contents);
	$email_contents = str_replace('<!-- email -->', $sql_user_data['email'], $email_contents);
	$email_contents = str_replace('<!-- street -->', $sql_user_data['street'], $email_contents);
	$email_contents = str_replace('<!-- town -->', $sql_user_data['town'], $email_contents);
	$email_contents = str_replace('<!-- province -->', $sql_user_data['province'], $email_contents);
	$email_contents = str_replace('<!-- postal -->', $sql_user_data['postal_code'], $email_contents);
	$email_contents = str_replace('<!-- card_front -->', $sql_card_data['pdf_file_front'], $email_contents);
	$email_contents = str_replace('<!-- card_back -->', $sql_card_data['pdf_file_back'], $email_contents);
	$email_contents = str_replace('<!-- transactions -->', $transactions, $email_contents);
	
	//Send Email
	require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
	$mail = new PHPMailer();
	try {
		$mail->AddAddress('thilo@palladianbytes.co.za');
		$mail->AddAddress('gary@maxxg.co.za');
		$mail->SetFrom('noreply@inprocess.co.za', 'cardMaker Server');
		$mail->AddReplyTo('noreply@inprocess.co.za', 'cardMaker Server');
		$mail->Subject = "Business Card Purchase from server ({$sql_user_data['name']} {$sql_user_data['company']})";
		$mail->MsgHTML($email_contents);
		$mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $sql_card_data['pdf_file_back'], 'Card-Back.pdf');
		$mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $sql_card_data['pdf_file_front'], 'Card-Front.pdf');
		$mail->Send();
		echo 'OK';
	} catch (phpmailerException $e) {
		echo $e->errorMessage(); //Pretty error messages from PHPMailer
		echo 'Fail';
	} catch (Exception $e) {
		echo 'Fail';
		echo $e->getMessage(); //Boring error messages from anything else!
	}
	
	/* Email - END */
	
	echo "<script>document.location = '/index.php?page=transaction_success';</script>";
	
} else {
	if ($transaction_status == '2') {
		echo "<script>alert('Transaction Failed');document.write('Returning to order page...'); document.location = '/index.php?page=place_order';</script>";
	} else {
		echo "<script>alert('Transaction Cancelled');document.write('Returning to order page...'); document.location = '/index.php?page=place_order';</script>";
	}
	
}

?>
