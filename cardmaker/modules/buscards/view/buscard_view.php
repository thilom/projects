<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$session_id = session_id();

//Heading
echo "<span class='orderHeading3'>Artwork Proof</span>";

//Show front
echo "<br>&nbsp;<br>Front of Card<br><div class='pdfObject'><object data='/temp_cards/{$session_id}_front_public.pdf' type='application/pdf' width='385' height='202'>  alt : <a href='/temp_cards/{$session_id}_front_public.pdf'>Front of Card</a></object></div>";

//Show Back
echo "<br>Back of Card<br><div class='pdfObject'><object data='/temp_cards/{$session_id}_back_public.pdf' type='application/pdf' width='385' height='202'>  alt : <a href='/temp_cards/{$session_id}_back_public.pdf>Back of Card</a></object></div>";

?>
