<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/tcpdf/tcpdf.php';

//Vars
$session_id = session_id();
$pdf_file_front = "/temp_cards/{$session_id}_front.pdf";
$pdf_file_front_public = "/temp_cards/{$session_id}_front_public.pdf";
$pdf_file_back = "/temp_cards/{$session_id}_back.pdf";
$pdf_file_back_public = "/temp_cards/{$session_id}_back_public.pdf";
$page_size = array(50, 90);
$content_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/buscards/html/buscard_order.html');
$card_front = array();
$card_back = array();
$contact_name = '';
$contact_company = '';
$contact_tel = '';
$contact_address = '';
$street = '';
$town = '';
$province = '';
$postal = '';
$clear_id = false;

//Assemble Front Card Data for saving
if (count($_POST) > 0) {
	for ($x=0; $x<10; $x++) {
		$card_front[$x]['text'] = $_POST["Fline$x"];
		$card_front[$x]['style'] = $_POST["FlineStyle$x"];
	}
	$card_front['logo'] = $_POST['cardLogoSrc'];
	$card_front['cardTemplate'] = $_POST['cardTemplate'];
	$card_front_json = json_encode($card_front);


	//Assemble back card data for saving
	switch ($_POST['backCardSelect']) {
		case 'text':
			$card_back['type'] = 'text';
			for ($x=0; $x<10; $x++) {
				$card_back[$x]['text'] = $_POST["Bline$x"];
				$card_back[$x]['style'] = $_POST["BlineStyle$x"];
			}
			break;
		case 'logo':
			$card_back['type'] = 'logo';
			break;
		case 'empty':
			$card_back['type'] = 'empty';
			break;
	}
	$card_back_json = json_encode($card_back);

	//Is the current card already saved
	$statement = "SELECT a.card_id, b.transaction_status
					FROM {$GLOBALS['db_prefix']}_buscard_data AS a
					LEFT JOIN {$GLOBALS['db_prefix']}_buscard_transaction AS b ON a.card_id=b.card_id
					WHERE a.session_id = :session_id";
	$sql_saved = $GLOBALS['dbCon']->prepare($statement);
	$sql_saved->bindParam(':session_id', $session_id);
	$sql_saved->execute();
	$sql_saved_data = $sql_saved->fetch();
	$sql_saved->closeCursor();
	if ($sql_saved_data['transaction_status'] == '1') {
		//Clear session ID's from data
		$statement = "UPDATE {$GLOBALS['db_prefix']}_buscard_data
						SET session_id = NULL
						WHERE session_id = :session_id
						LIMIT 1";
		$sql_clear = $GLOBALS['dbCon']->prepare($statement);
		$sql_clear->bindParam(':session_id', $session_id);
		$sql_clear->execute();

		$clear_id = true;

	}


	//Save to DB
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_buscard_data
						(card_front, card_back, pdf_file_front, pdf_file_back, card_quantity, session_id)
					VALUES
						(:card_front, :card_back, :pdf_file_front, :pdf_file_back, :card_quantity, :session_id)
					ON DUPLICATE KEY UPDATE
							card_id=LAST_INSERT_ID(card_id),
							card_front = :card_front,
							card_back = :card_back,
							pdf_file_front = :pdf_file_front,
							pdf_file_back = :pdf_file_back,
							card_quantity = :card_quantity";
	$sql_card = $GLOBALS['dbCon']->prepare($statement);
	$sql_card->bindParam(':card_front', $card_front_json);
	$sql_card->bindParam(':card_back', $card_back_json);
	$sql_card->bindParam(':pdf_file_front', $pdf_file_front);
	$sql_card->bindParam(':pdf_file_back', $pdf_file_back);
	$sql_card->bindParam(':card_quantity', $_POST['quantity']);
	$sql_card->bindParam(':session_id', $session_id);
	$sql_card->execute();
	$card_id = $GLOBALS['dbCon']->lastinsertid();

} else {
	//Get data from server
	$statement = "SELECT a.card_id, b.card_front, b.card_back, b.card_quantity
					FROM {$GLOBALS['db_prefix']}_buscard_user AS a
						LEFT JOIN {$GLOBALS['db_prefix']}_buscard_data AS b ON a.card_id=b.card_id
					WHERE a.session_id = :session_id
					LIMIT 1";
	$sql_card = $GLOBALS['dbCon']->prepare($statement);
	$sql_card->bindParam(':session_id', $session_id);
	$sql_card->execute();
	$sql_card_data = $sql_card->fetch();
	$sql_card->closeCursor();
	$card_data_front = json_decode($sql_card_data['card_front'], true);
	$card_data_back = json_decode($sql_card_data['card_back'], true);
	$_POST['quantity'] = $sql_card_data['card_quantity'];
	$card_id = $sql_card_data['card_id'];

	//expand card front
	for ($i=0; $i<10; $i++) {
		$_POST["Fline$i"] = $card_data_front[$i]['text'];
		$_POST["FlineStyle$i"] = $card_data_front[$i]['style'];
	}
	$_POST['cardLogoSrc'] = $card_data_front['logo'];
	$_POST['cardTemplate'] = $card_data_front['cardTemplate'];

	//Expand card back
	switch ($card_data_back['type']) {
		case 'logo':

			break;
		case 'text':
			for ($i=0; $i<10; $i++) {
				$_POST["Bline$i"] = $card_data_back[$i]['text'];
				$_POST["BlineStyle$i"] = $card_data_back[$i]['style'];
			}
			break;
		case 'empty':

			break;
	}
	$_POST['backCardSelect'] = $card_data_back['type'];


}

//Create Reference
$reference_number = "CARD" . str_pad($card_id, 5, 0, STR_PAD_LEFT);

/* CARD FRONT */
//Create Temp PDF
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, 'mm', PDF_PAGE_FORMAT, true, 'UTF-8', false);


//PDF Settings
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('cardMaker.co.za');
$pdf->SetTitle('Business Card');
$pdf->SetSubject('Business Card');
$pdf->setAutoPageBreak(false);
$pdf->SetTopMargin(7);
$pdf->addFont('pdfahelvetica', '', "{$_SERVER['DOCUMENT_ROOT']}/shared/tcpdf/fonts/pdfahelvetica.php");
$pdf->SetFont('pdfahelvetica', '', 10, '', true);
//die();

// add a page
$pdf->AddPage('L', $page_size);

//Draw Temp Border
//$pdf->Rect(0,0,$pdf->getPageWidth(), $pdf->getPageHeight(), '',array('all' => array('width' => 1)), '');

//Add card front text
if ($_POST['cardTemplate'] == 1) {
	$x_start = 2;
	for ($x=0; $x<10; $x++) {
		$font_style = '';
		$x_start += 3.75; //3.55
		$font_size = 10.0;
		$line_correction = 0;

		//Get styles
		$style=$_POST["FlineStyle$x"];

		//Bold
		if ($style[0] == '1')$font_style .= 'B';

		//Italic
		if ($style[1] == '1') $font_style .= 'I';

		//Fontsize
		if ($style[2] == '1') {
			$font_size = 7.25;
	//		$line_correction += -1;
		}
		if ($style[3] == '1') $font_size = 8.70;
		if ($style[4] == '1') {
			$font_size = 10.00;
	//		$line_correction += 0.5;
		}

		$pdf->SetFont('pdfahelvetica', $font_style, $font_size, '', true);
		$pdf->SetFontSpacing(0);
		$pdf->Text(35.75, $x_start, $_POST["Fline$x"]);
	//	$x_start += $line_correction;
	}

	//Add logo
	if (isset($_POST['cardLogoSrc']) && !empty($_POST['cardLogoSrc'])) {
		$pdf->Image($_SERVER['DOCUMENT_ROOT'] . $_POST['cardLogoSrc'],5,7,28.00,36.00,'','','',false,300,'',false,false,0,'CT');
	}
} else {
	$x_start = 20;
	for ($x=0; $x<4; $x++) {
		$pdf->Cell(70, 4.10, ' ',0,1,'C', false, '', 0, true, 'T', 'C');
	}
	for ($x=0; $x<6; $x++) {
		$font_style = '';
//		$x_start += 3.75; //3.55
		$font_size = 10;
//			$line_correction = 0;

		//Get styles
		$style=$_POST["FlineStyle$x"];

		//Bold
		if ($style[0] == '1')$font_style .= 'B';

		//Italic
		if ($style[1] == '1') $font_style .= 'I';

		//Fontsize
		if ($style[2] == '1') {
			$font_size = 7.25;
//				$line_correction += -1;
		}
		if ($style[3] == '1') $font_size = 8.70;
		if ($style[4] == '1') {
			$font_size = 10.00;
//				$line_correction += 1;
		}

		$pdf->SetFont('pdfahelvetica', $font_style, $font_size, '', true);
		$pdf->SetFontSpacing(0);
		$pdf->Cell(70, 3.74, $_POST["Fline$x"],0,1,'C', false, '', 0, true, 'T', 'C');
//			$x_start += $line_correction;
	}

	//Add logo
	if (isset($_POST['cardLogoSrc']) && !empty($_POST['cardLogoSrc'])) {
//		$pdf->Image($_SERVER['DOCUMENT_ROOT'] . $_POST['cardLogoSrc'],5,0,28.00,36.00,'','','',false,300,'',false,false,0,'CT');
		$pdf->image($_SERVER['DOCUMENT_ROOT'] . $_POST['cardLogoSrc'],'C',3.80,0,16.50,'',false,'C', false, 300, 'C', false, false, 0, 'CT', false, false);
	}
}
$pdf_public = clone $pdf;
$pdf_public->Image("{$_SERVER['DOCUMENT_ROOT']}/i/proof.png",0,0,90,50,'','','',true,300,'',false,false,0,'CM');

//Add border to proof
$pdf_public->Rect(0,0,$pdf_public->getPageWidth(), $pdf_public->getPageHeight(), '',array('all' => array('width' => 0.5)), '');

//Save File
$pdf->Output($_SERVER['DOCUMENT_ROOT'] . $pdf_file_front, 'F');
$pdf_public->Output($_SERVER['DOCUMENT_ROOT'] . $pdf_file_front_public, 'F');

/* Card Back */
$pdf_back = new TCPDF(PDF_PAGE_ORIENTATION, 'mm', PDF_PAGE_FORMAT, true, 'UTF-8', false);

//PDF Settings
$pdf_back->setPrintHeader(false);
$pdf_back->setPrintFooter(false);// set document information
$pdf_back->SetCreator(PDF_CREATOR);
$pdf_back->SetAuthor('cardMaker.co.za');
$pdf_back->SetTitle('Business Card');
$pdf_back->SetSubject('Business Card');
$pdf_back->setAutoPageBreak(false);
$pdf_back->SetTopMargin(7);
$pdf_back->addFont('pdfahelvetica', '', "{$_SERVER['DOCUMENT_ROOT']}/shared/tcpdf/fonts/dejavusans.php");
$pdf_back->SetFont('pdfahelvetica', '', 10, '', true);

// add a page
$pdf_back->AddPage('L', $page_size);

//Add content
switch ($_POST['backCardSelect']) {
	case 'logo':
//		$pdf_back->Image($_SERVER['DOCUMENT_ROOT'] . '/templates/images/cardmaker_logo.png',3,6.75,84,20,'','','',false,300,'',false,false,0,'CM');
		$pdf_back->SetFont('pdfahelvetica', '', 8, '', true);
		$pdf_back->Ln(17);
		$pdf_back->Cell(0, 35, 'www.CardMaker.co.za',0,0,'C');
		break;
	case 'text':
//		$pdf_back->Ln(-5);
		$x_start = 0;
		for ($x=0; $x<10; $x++) {
			$font_style = '';
			$x_start += 3.75; //3.55
			$font_size = 10;
//			$line_correction = 0;

			//Get styles
			$style=$_POST["BlineStyle$x"];

			//Bold
			if ($style[0] == '1')$font_style .= 'B';

			//Italic
			if ($style[1] == '1') $font_style .= 'I';

			//Fontsize
			if ($style[2] == '1') {
				$font_size = 7.25;
//				$line_correction += -1;
			}
			if ($style[3] == '1') $font_size = 8.70;
			if ($style[4] == '1') {
				$font_size = 10.00;
//				$line_correction += 1;
			}

			$pdf_back->SetFont('pdfahelvetica', $font_style, $font_size, '', true);
			$pdf_back->SetFontSpacing(0);
			$pdf_back->Cell(70, 3.75, $_POST["Bline$x"],0,1,'C', false, '', 0, true, 'T', 'C');
//			$x_start += $line_correction;
		}
		break;
	case 'empty':

		break;
}

//
$pdf_back_public = clone $pdf_back;
$pdf_back_public->Image("{$_SERVER['DOCUMENT_ROOT']}/i/proof.png",0,0,90,50,'','','',true,300,'',false,false,0,'CM');

//Add border to proof
$pdf_back_public->Rect(0,0,$pdf_back_public->getPageWidth(), $pdf_back_public->getPageHeight(), '',array('all' => array('width' => 0.5)), '');

//Save File
$pdf_back->Output($_SERVER['DOCUMENT_ROOT'] . $pdf_file_back, 'F');
$pdf_back_public->Output($_SERVER['DOCUMENT_ROOT'] . $pdf_file_back_public, 'F');

//Check sessions
$statement = "SELECT name, company, telephone, email, street, town, province, postal_code, display_card
				FROM {$GLOBALS['db_prefix']}_buscard_user
				WHERE session_id = :session_id
				LIMIT 1";
$sql_session = $GLOBALS['dbCon']->prepare($statement);
$sql_session->bindParam(':session_id', $session_id);
$sql_session->execute();
$sql_session_data = $sql_session->fetch();
$sql_session->closeCursor();
if ($sql_session !== false) {
	$contact_name = $sql_session_data['name'];
	$contact_company = $sql_session_data['company'];
	$contact_tel = $sql_session_data['telephone'];
	$contact_address = $sql_session_data['email'];
	$street = $sql_session_data['street'];
	$town = $sql_session_data['town'];
	$province = $sql_session_data['province'];
	$postal = $sql_session_data['postal_code'];
	$display_card = $sql_session_data['display_card']=='Y'?'checked':'';
}

//Clear Session from user
if ($clear_id === true) {
	$statement = "UPDATE {$GLOBALS['db_prefix']}_buscard_user
					SET session_id = NULL
					WHERE session_id = :session_id
					LIMIT 1";
	$sql_clear = $GLOBALS['dbCon']->prepare($statement);
	$sql_clear->bindParam(':session_id', $session_id);
	$sql_clear->execute();
}

//Calculate price
$product_code = "{$_POST['backCardSelect']}{$_POST['quantity']}";
$statement = "SELECT SUM(product_price) AS price
				FROM {$GLOBALS['db_prefix']}_products
				WHERE product_code=:product_code || product_code=:product_code2";
$sql_cost = $GLOBALS['dbCon']->prepare($statement);
$sql_cost->bindParam(':product_code', $product_code);
$sql_cost->bindParam(':product_code2', $_POST['quantity']);
$sql_cost->execute();
$sql_cost_data = $sql_cost->fetch();
$sql_cost->closeCursor();

//Replace tags
//if ($sql_cost_data['price'] == 199) $sql_cost_data['price'] = '0.00';
$content_template = str_replace('<!-- card_quantity -->', $_POST['quantity'], $content_template);
$content_template = str_replace('<!-- order_value -->', $sql_cost_data['price'], $content_template);
$content_template = str_replace('<!-- card_id -->', $card_id, $content_template);
$content_template = str_replace('<!-- contact_name -->', $contact_name, $content_template);
$content_template = str_replace('<!-- contact_company -->', $contact_company, $content_template);
$content_template = str_replace('<!-- contact_tel -->', $contact_tel, $content_template);
$content_template = str_replace('<!-- contact_address -->', $contact_address, $content_template);
$content_template = str_replace('<!-- street -->', $street, $content_template);
$content_template = str_replace('<!-- town -->', $town, $content_template);
$content_template = str_replace('<!-- province -->', $province, $content_template);
$content_template = str_replace('<!-- postal -->', $postal, $content_template);
$content_template = str_replace('<!-- reference_number -->', $reference_number, $content_template);
$content_template = str_replace('<!-- display_card -->', $display_card, $content_template);


echo $content_template;

?>
