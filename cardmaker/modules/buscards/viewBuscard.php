<?php

/*
 * View details of a business card
 * 
 * 
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/shared/database_functions.php';

//vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/buscards/html/view_buscard.html');
$card_id = $_GET['cardID'];
$transactions = '';

//Get User data
$statement = "SELECT name, company, telephone, email, street, town, province, postal_code, display_card
				FROM {$GLOBALS['db_prefix']}_buscard_user
				WHERE card_id=:card_id
				LIMIT 1";
$sql_user = $GLOBALS['dbCon']->prepare($statement);
$sql_user->bindParam(':card_id', $card_id);
$sql_user->execute();
$sql_user_data = $sql_user->fetch();
$sql_user->closeCursor();

//Get card details
$statement = "SELECT pdf_file_front, pdf_file_back, display_card_admin
				FROM {$GLOBALS['db_prefix']}_buscard_data
				WHERE card_id=:card_id
				LIMIT 1";
$sql_card = $GLOBALS['dbCon']->prepare($statement);
$sql_card->bindParam(':card_id', $card_id);
$sql_card->execute();
$sql_card_data = $sql_card->fetch();
$sql_card->closeCursor();

//Get transaction data
$statement = "SELECT transaction_status, result_code, transaction_date, transaction_id, amount, result_desc
				FROM {$GLOBALS['db_prefix']}_buscard_transaction
				WHERE card_id=:card_id";
$sql_transaction = $GLOBALS['dbCon']->prepare($statement);
$sql_transaction->bindParam(':card_id', $card_id);
$sql_transaction->execute();
$sql_transaction_data = $sql_transaction->fetchAll();
$sql_transaction->closeCursor();

//Assemble transactions
foreach ($sql_transaction_data as $transaction_data) {
	switch ($transaction_data['transaction_status']) {
		case '1':
			$status = '<span class="statusApproved">Approved</span>';
			break;
		case '0':
			$status = '<span class="statusNotDone">Not Done</span>';
			break;
		case '2':
			$status = '<span class="statusDeclined">Declined</span>';
			break;
		case '9':
			$status = '<span class="statusDeclined">Possible Transaction Tampering</span>';
			break;
		default: 
			$status = '';
	}
	$transaction_amount = substr($transaction_data['amount'],0, -2) . '.' . substr($transaction_data['amount'], -2);
	$transactions .= "<tr>";
	$transactions .= "<td class='tagCol'>Date</td><td>{$transaction_data['transaction_date']}</td>";
	$transactions .= "<td>&nbsp;</td>";
	$transactions .= "<td class='tagCol'>Status</td><td>$status</td>";
	$transactions .= "</tr>";
	$transactions .= "<tr>";
	$transactions .= "<td class='tagCol'>Transaction ID</td><td>{$transaction_data['transaction_id']}</td>";
	$transactions .= "<td>&nbsp;</td>";
	$transactions .= "<td class='tagCol'>Result Code</td><td>{$transaction_data['result_code']}</td>";
	$transactions .= "</tr>";
	$transactions .= "<tr>";
	$transactions .= "<td class='tagCol'>Amount</td><td>{$transaction_amount}</td>";
	$transactions .= "<td>&nbsp;</td>";
	$transactions .= "<td class='tagCol'>Result Description</td><td>{$transaction_data['result_desc']}</td>";
	$transactions .= "</tr>";
	$transactions .= "<tr>";
	$transactions .= "<td colspan='5'></td>";
	$transactions .= "</tr>";
	$transactions .= "<tr><td colspan=5><hr></td></tr>";
}

//Display status and display
$display_card = $sql_card_data['display_card_admin']=='Y'?'checked':'';
$display_enabled = $sql_user_data['display_card'] == 'Y'?'':'disabled';
$display_text = $sql_user_data['display_card'] == 'Y'?'Showcase Card on Website':'Showcase card not approved by client';

//Replace Tags
$template = str_replace('<!-- name -->', $sql_user_data['name'], $template);
$template = str_replace('<!-- company -->', $sql_user_data['company'], $template);
$template = str_replace('<!-- telephone -->', $sql_user_data['telephone'], $template);
$template = str_replace('<!-- email -->', $sql_user_data['email'], $template);
$template = str_replace('<!-- street -->', $sql_user_data['street'], $template);
$template = str_replace('<!-- town -->', $sql_user_data['town'], $template);
$template = str_replace('<!-- province -->', $sql_user_data['province'], $template);
$template = str_replace('<!-- postal -->', $sql_user_data['postal_code'], $template);
$template = str_replace('<!-- card_front -->', $sql_card_data['pdf_file_front'], $template);
$template = str_replace('<!-- card_back -->', $sql_card_data['pdf_file_back'], $template);
$template = str_replace('<!-- transactions -->', $transactions, $template);
$template = str_replace('<!-- card_id -->', $card_id, $template);
$template = str_replace('<!-- display_card -->', $display_card, $template);
$template = str_replace('<!-- display_enabled -->', $display_enabled, $template);
$template = str_replace('<!-- display_text -->', $display_text, $template);

echo $template;

?>
