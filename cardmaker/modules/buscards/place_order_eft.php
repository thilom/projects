<?php

/*
 * Save user data to the DB, Send email to client and an email to cardmaker. 
 * 
 * Return to hamo page after confimation
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$session_id = session_id();
$card_id = $_POST['card_id'];
$order_value = str_replace('.','',$_POST['TransactionValue']);
$reference = str_pad($_POST['card_id'], 5, 0, STR_PAD_LEFT);
$date = date('Y-m-d H:m:s');
$email = $_POST['contactAddress'];
$transactions = '';
$reference_number = "CARD" . str_pad($card_id, 5, 0, STR_PAD_LEFT);
$display_card = isset($_POST['display_card'])?'Y':'N';

//Save to DB
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_buscard_user
					(card_id, name, company, telephone, email, street, town, province, postal_code, session_id, display_card)
				VALUES
					(:card_id, :name, :company, :telephone, :email, :street, :town, :province, :postal_code, :session_id, :display_card)
				ON DUPLICATE KEY UPDATE
					card_id=:card_id, company=:company, name=:name, telephone=:telephone, email=:email, street=:street, town=:town, 
					province=:province, postal_code=:postal_code, display_card = :display_card";
$sql_save = $GLOBALS['dbCon']->prepare($statement);
$sql_save->bindParam(':card_id', $card_id);
$sql_save->bindParam(':name', $_POST['contactName']);
$sql_save->bindParam(':company', $_POST['contactCompany']);
$sql_save->bindParam(':telephone', $_POST['contactTelephone']);
$sql_save->bindParam(':email', $email);
$sql_save->bindParam(':street', $_POST['street']);
$sql_save->bindParam(':town', $_POST['town']);
$sql_save->bindParam(':province', $_POST['province']);
$sql_save->bindParam(':postal_code', $_POST['postal']);
$sql_save->bindParam(':session_id', $session_id);
$sql_save->bindParam(':display_card', $display_card);
$sql_save->execute();

//Save transaction
$transaction_status = 1;
$statement = "INSERT INTO {$GLOBALS['db_prefix']}_buscard_transaction
					(card_id, transaction_status, amount, transaction_date)
				VALUES
					(:card_id, :transaction_status, :amount, NOW())";
$sql_result = $GLOBALS['dbCon']->prepare($statement);
$sql_result->bindParam(':card_id', $card_id);
$sql_result->bindParam(':transaction_status', $transaction_status);
$sql_result->bindParam(':amount', $order_value);
$sql_result->execute();

//Get user data
$statement = "SELECT name, company, telephone, email, street, town, province, postal_code, display_card
				FROM {$GLOBALS['db_prefix']}_buscard_user
				WHERE card_id=:card_id
				LIMIT 1";
$sql_user = $GLOBALS['dbCon']->prepare($statement);
$sql_user->bindParam(':card_id', $card_id);
$sql_user->execute();
$sql_user_data = $sql_user->fetch();
$sql_user->closeCursor();

/* Assemble and send email to CardMaker */

//Get card details
$statement = "SELECT pdf_file_front, pdf_file_back
				FROM {$GLOBALS['db_prefix']}_buscard_data
				WHERE card_id=:card_id
				LIMIT 1";
$sql_card = $GLOBALS['dbCon']->prepare($statement);
$sql_card->bindParam(':card_id', $card_id);
$sql_card->execute();
$sql_card_data = $sql_card->fetch();
$sql_card->closeCursor();

$display_card = $sql_user_data['display_card']=='Y'?'checked':'';


//Assemble Email
$email_contents = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/buscards/html/email_content.html');
$email_contents = str_replace('<!-- name -->', $sql_user_data['name'], $email_contents);
$email_contents = str_replace('<!-- company -->', $sql_user_data['company'], $email_contents);
$email_contents = str_replace('<!-- telephone -->', $sql_user_data['telephone'], $email_contents);
$email_contents = str_replace('<!-- email -->', $sql_user_data['email'], $email_contents);
$email_contents = str_replace('<!-- street -->', $sql_user_data['street'], $email_contents);
$email_contents = str_replace('<!-- town -->', $sql_user_data['town'], $email_contents);
$email_contents = str_replace('<!-- province -->', $sql_user_data['province'], $email_contents);
$email_contents = str_replace('<!-- postal -->', $sql_user_data['postal_code'], $email_contents);
$email_contents = str_replace('<!-- card_front -->', $sql_card_data['pdf_file_front'], $email_contents);
$email_contents = str_replace('<!-- card_back -->', $sql_card_data['pdf_file_back'], $email_contents);
$email_contents = str_replace('<!-- value -->', $_POST['TransactionValue'], $email_contents);
$email_contents = str_replace('<!-- volume -->', $_POST['TransactionVolume'], $email_contents);
$email_contents = str_replace('<!-- reference_number -->', $reference_number, $email_contents);
$email_contents = str_replace('<!-- display_card -->', $display_card, $email_contents);

//Send Email to cardMaker
require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/PHPMailer/class.phpmailer.php';
$mail = new PHPMailer();
$mail->IsSMTP();
try {
	$mail->Host = "mail.cardmaker.co.za";
	$mail->Port = 25;
	$mail->SMTPAuth = true;
	$mail->Username = "order@cardmaker.co.za";
	$mail->Password = "xrrme27+].9_";
//	$mail->AddAddress('thilo@palladianbytes.co.za');
//	$mail->AddAddress('gary@maxxg.co.za');
	$mail->AddAddress('order@cardmaker.co.za');
//	$mail->AddAddress('cardmaker@palladianbytes.co.za');
	$mail->SetFrom('order@cardmaker.co.za', 'cardMaker Server');
	$mail->AddReplyTo('order@cardmaker.co.za', 'cardMaker Server');
	$mail->Subject = "EFT Business Card Purchase from server ({$sql_user_data['name']} {$sql_user_data['company']})";
	$mail->MsgHTML($email_contents);
	$mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $sql_card_data['pdf_file_back'], 'Card-Back.pdf');
	$mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $sql_card_data['pdf_file_front'], 'Card-Front.pdf');
	$mail->Send();
//	echo 'OK';
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
//	echo 'Fail';
} catch (Exception $e) {
//	echo 'Fail';
	echo $e->getMessage(); //Boring error messages from anything else!
}

//Assemble and send order confirmation to clients
$client_template = file_get_contents('html/client_email.html');
$cardmaker_logo = "{$_SERVER['DOCUMENT_ROOT']}/templates/images/cardmaker_logo.png";
$order_details = $_POST['transactionDesc'];
$client_template = str_replace('<!-- client_email -->', $sql_user_data['email'], $client_template);
$client_template = str_replace('<!-- reference_number -->', $reference_number, $client_template);
$client_template = str_replace('<!-- order_details -->', $order_details, $client_template);
$client_template = str_replace('<!-- delivery_address -->', $order_details, $client_template);
$client_template = str_replace('<!-- street -->', $sql_user_data['street'], $client_template);
$client_template = str_replace('<!-- town -->', $sql_user_data['town'], $client_template);
$client_template = str_replace('<!-- province -->', $sql_user_data['province'], $client_template);
$client_template = str_replace('<!-- postal -->', $sql_user_data['postal_code'], $client_template);

$mail2 = new PHPMailer();
$mail2->IsSMTP();
try {
	$mail2->Host = "mail.cardmaker.co.za";
	$mail2->Port = 25;
	$mail2->SMTPAuth = true;
	$mail2->Username = "order@cardmaker.co.za";
	$mail2->Password = "xrrme27+].9_";
	$mail2->ClearAddresses();
	$mail2->AddAddress($sql_user_data['email'], $sql_user_data['name']);
	$mail2->SetFrom('order@cardmaker.co.za', 'cardMaker Server');
	$mail2->AddReplyTo('order@cardmaker.co.za', 'cardMaker Server');
	$mail2->Subject = "Your business card order from CardMaker";
	$mail2->MsgHTML($client_template);
	$mail2->AddEmbeddedImage($cardmaker_logo, 'cardmakerLogo', 'CardMaker Logo');
	$mail2->AddAttachment($_SERVER['DOCUMENT_ROOT'] . (str_replace('_back', '_back_public', $sql_card_data['pdf_file_back'])), 'Card-Back.pdf');
	$mail2->AddAttachment($_SERVER['DOCUMENT_ROOT'] . (str_replace('_front', '_front_public', $sql_card_data['pdf_file_front'])), 'Card-Front.pdf');
	$mail2->Send();
} catch (phpmailerException $e) {
	echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
	echo $e->getMessage(); //Boring error messages from anything else!
}



echo "<script>alert('Order submitted - Thank you. \\nPlease check your email for confirmation and payment details.');document.write('Returning to home page...'); document.location = '/index.php';</script>";

?>
