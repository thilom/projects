<?php

/**
 * Get the default business card
 */

//Includes
$ajax = 1;
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Vars
$session_id = session_id();
$data = array();

//Get data
$statement = "SELECT card_front, card_back, card_quantity
				FROM {$GLOBALS['db_prefix']}_buscard_data
				WHERE session_id=:session_id";
$sql_buscard = $GLOBALS['dbCon']->prepare($statement);
$sql_buscard->bindParam(':session_id', $session_id);
$sql_buscard->execute();
$sql_buscard_data = $sql_buscard->fetch();
$sql_buscard->closeCursor();
if ($sql_buscard_data === false) {
	$data['front'] = '{"0":{"text":"Cdr. James Bond","style":"10001"},"1":{"text":"Intelligence Officer","style":"00010"},"2":{"text":"Royal Naval Reserve","style":"00010"},"3":{"text":"","style":"00010"},"4":{"text":"Her Majesty\'s Secret Service","style":"00010"},"5":{"text":"Military Intelligence, Section 6","style":"00010"},"6":{"text":"","style":"00010"},"7":{"text":"85 Vauxhall Cross","style":"00010"},"8":{"text":"London","style":"00010"},"9":{"text":"England","style":"00010"},"logo":""}';
	$data['back'] = '{"type":"logo","0":{"text":"Secret Intelligence Service MI6","style":"10001"},"1":{"text":"","style":"00010"},"2":{"text":"SIS collects secret intelligence and mounts","style":"00010"},"3":{"text":"covert operations overseas in support of","style":"00010"},"4":{"text":"British Government objectives.","style":"00010"},"5":{"text":"","style":"00010"},"6":{"text":"","style":"00010"},"7":{"text":"","style":"00010"},"8":{"text":"","style":"00010"},"9":{"text":"","style":"00010"}}';
	$data['quantity'] = 500;
} else {
	$data['front'] = $sql_buscard_data['card_front'];
	$data['back'] = $sql_buscard_data['card_back'];
	$data['quantity'] = $sql_buscard_data['card_quantity'];
}

echo json_encode($data);

?>
