<?php

//Includes
$ajax = 1;
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Vars
$product_list = array();

//Get back card products
$statement = "SELECT product_name, product_code, product_price
				FROM {$GLOBALS['db_prefix']}_products_category_bridge AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_products AS b ON a.product_id=b.product_id
				WHERE a.category_id = 2";
$sql_back = $GLOBALS['dbCon']->prepare($statement);
$sql_back->execute();
$sql_back_data = $sql_back->fetchAll();
$sql_back->closeCursor();

//Assemble Products
$counter = 0;
foreach ($sql_back_data as $product_data) {
	$product_list['back'][$counter]['name'] = $product_data['product_name'];
	$product_list['back'][$counter]['code'] = $product_data['product_code'];
	$product_list['back'][$counter]['price'] = $product_data['product_price'];
	
	$counter++;
}

//Get product packages
$statement = "SELECT product_name, product_code, product_price
				FROM {$GLOBALS['db_prefix']}_products_category_bridge AS a
				LEFT JOIN {$GLOBALS['db_prefix']}_products AS b ON b.product_id=a.product_id
				WHERE a.category_id = 1
				ORDER BY b.product_code+0 ASC";
$sql_package = $GLOBALS['dbCon']->prepare($statement);
$sql_package->execute();
$sql_package_data = $sql_package->fetchAll();
$sql_package->closeCursor();

//Assemble Packages
$counter = 0;
foreach ($sql_package_data as $product_data) {
	$product_list['packages'][$counter]['name'] = $product_data['product_name'];
	$product_list['packages'][$counter]['code'] = $product_data['product_code'];
	$product_list['packages'][$counter]['price'] = $product_data['product_price'];
	
	$counter++;
}

echo json_encode($product_list);

?>
