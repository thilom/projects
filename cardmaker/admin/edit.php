<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Get page variables
$splitPos = strrpos($_GET['p'], '_');
$area_name = substr($_GET['p'], 0, $splitPos);
$page_id = substr($_GET['p'], $splitPos+1);

//Check if an entry already exists & add it if it doesn't exist
$SQL_statement = "SELECT * FROM dbweb_page_contents WHERE page_id='$page_id' && content_area_name='$area_name'";
if (($SQL_count = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
if (mysql_num_rows($SQL_count) == 0) {
	$SQL_statement = "INSERT INTO dbweb_page_contents (page_id, content_area_name, module) VALUES ('$page_id', '$area_name', 'text')";
	if ((mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	echo mysql_error();
}

//Get Edit File
$SQL_statement = "SELECT module FROM dbweb_page_contents WHERE page_id='$page_id' && content_area_name='$area_name'";
if (($SQL_result = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
if (mysql_num_rows($SQL_result) == 0) {
	$module_link = 'text';
} else {
	list($module_link) = mysql_fetch_array($SQL_result);
}

include_once(SITE_ROOT . "/modules/$module_link/edit.php");
?>