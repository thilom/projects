from gi.repository import Gtk

class Event_handler():

    def __init__(self, builder):
        self.builder = builder

    def onDestroy(self, *args):
        Gtk.main_quit()

    def openManageFermentables(self, button):
        print("Opening Malt Screen")
        stack = self.builder.get_object('mainStack')
        page = self.builder.get_object('stackpage_manageFermentables')
        stack.set_visible_child(page)





