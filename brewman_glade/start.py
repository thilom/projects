import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk
from gi.repository import Gdk
from event_handler import Event_handler
from db.brewman_db import BrewmanDB
from fermentables import Fermentables


class BrewWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self)
        builder = Gtk.Builder()
        builder.add_from_file("brewman-ui.glade")

        win = builder.get_object("window1")
        win.show_all()
        builder.connect_signals(Event_handler(builder))
        win.connect("destroy", Gtk.main_quit)

        self.dbConnection = BrewmanDB()
        self.fermentables = Fermentables(self.dbConnection, builder)
        self.fermentables.populate_list()

        with open("styles.css", "r") as f:
            css = f.read().encode()
        self.css_provider = Gtk.CssProvider()
        self.css_provider.load_from_data(css)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), self.css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)


window = BrewWindow()
Gtk.main()
