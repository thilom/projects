
class Fermentable(object):
    type = 'Fermentable'
    ID = None
    fName = None
    fOrigin = None
    fColor = None
    fPotential = None
    fExtractpotential = None
    fDescription = None


class Yeast(object):
    ID = None
    yName = None


class Hops(object):
    type = 'Hops'
    ID = None
    hName = None
    hOrigin = None
