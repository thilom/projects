import sqlite3
from sqlite3 import Error
from brewman_db_create import DBCreate


class BrewmanDB(object):

    def __init__(self):
        self.DBConnection = None
        self.connect()
        self.db_setup()

    def connect(self):
        try:
            self.DBConnection = sqlite3.connect("brewman.db")
        except Error as e:
            print("Failed connect to DB: %s" % e)

    def db_setup(self):
        try:
            cursor = self.DBConnection.cursor()
            cursor.execute("SELECT * FROM Fermentables")
        except Error as e:
            print("[Fermentables] Table does not exist. Attempting to create: %s" % e)
            db_create = DBCreate(self.DBConnection)
            db_create.create_fermentables_table()


