from sqlite3 import Error
import brewman_db_sql as sql


class DBCreate(object):

    def __init__(self, db_connection):
        self.db_connection = db_connection

    def create_fermentables_table(self):
        try:
            cursor = self.db_connection.cursor()
            cursor.execute(sql.TABLE_FERMENTABLES)
        except Error as e:
            print("Failed to create [Fermentables]: " % e)
            return False
        return True
