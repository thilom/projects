import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

FERM_GRID_NAME_POS = 0
FERM_GRID_ORIGIN_POS = 1
FERM_GRID_BUTTON_POS = 2

class Fermentables(object):

    def __init__(self, db_connection, builder):
        self.db_connection = db_connection
        self.builder = builder
        self.row_count = 0

    def _get_grid(self):
        return self.builder.get_object('fermentable_grid')

    def _show_grid(self):
        grid = self._get_grid()
        grid.show_all()

    def _attach_fermentable_header(self):
        grid = self._get_grid()
        label1 = Gtk.Label(xalign=0)
        label1.set_markup("<b>Fermentable Name</b>")
        label1.set_margin_end(10)
        label1.set_margin_start(5)
        grid.attach(label1, FERM_GRID_NAME_POS, self.row_count, 1, 1)
        label1 = Gtk.Label(xalign=0)
        label1.set_markup("<b>Origin</b>")
        grid.attach(label1, FERM_GRID_ORIGIN_POS, self.row_count, 1, 1)
        self.row_count += 1

    def _attach_fermentable_row(self, fermentable_name, fermentable_origin):
        grid = self._get_grid()
        eb = Gtk.EventBox()
        label1 = Gtk.Label(fermentable_name, xalign=0)
        label1.set_margin_end(10)
        label1.set_margin_start(5)
        eb.add(label1)

        label2 = Gtk.Label(fermentable_origin, xalign=0)
        button2 = Gtk.Button('Button 2')
        eb.connect('enter-notify-event', self.mouseover)
        print(dir(grid.props))

        grid.attach(eb, FERM_GRID_NAME_POS, self.row_count, 1, 1)
        grid.attach(label2, FERM_GRID_ORIGIN_POS, self.row_count, 1, 1)
        grid.attach(button2, FERM_GRID_BUTTON_POS, self.row_count, 1, 1)
        self.row_count += 1

    def mouseover(self, a, b):
        print(a.get_child())
        active_lbl = a.get_child()
        lbl = self.builder.get_object('lblName')
        lbl.set_text('Testing')

    def populate_list(self):
        self._attach_fermentable_header()
        self._attach_fermentable_row('Long Fermentable Name', 'Origin 1')
        self._attach_fermentable_row('Another Long Fermentable Name', 'Origin 2')
        self._attach_fermentable_row('Short Name', 'Origin 3')
        self._show_grid()
