<?php 

e4();

function e1() {
	//Vars
	$counter = 0;
	
	//Includes
	include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';
	
	//Prepare statement
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_name = $dbCon->prepare($statement);
	
	
	//Redirect /static/establishments
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/static/establishments/';
	$dir_contents = scandir($dir);
	foreach ($dir_contents as $file) {
		if($file == '.' || $file == '..') continue;
		
		//Get ID
		$establishment_code = substr($file, 13);
		$establishment_code = substr($establishment_code, 0, strpos($establishment_code, '.'));
		
		//Get estab name
		$sql_name->bind_param('s', $establishment_code);
		$sql_name->execute();
		$sql_name->bind_result($establishment_name);
		$sql_name->store_result();
		$sql_name->fetch();
		
		//Assemble name
		$establishment_name = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\' ), '', $establishment_name );
		$file_name = strtolower(str_replace(' ', '_', $establishment_code.'_'.$establishment_name));
		
		//Assemble page Data
		$pageData = "<?php \n";
		$pageData .= "header('HTTP/1.1 301 Moved Permanently');";
		$pageData .= "header('Location: http://booktravel.travel/accommodation/$file_name.html');";
		$pageData .= "exit();";
		
		file_put_contents($dir . $file, $pageData);
		
		echo "<a href='/static/establishments/$file'>$file</a> - redirected to <a href='/accommodation/$file_name.html'>$file_name.html</a><br>";
		$counter++;
	}
}

function e2() {
	//Vars
	$counter = 0;
	echo "<table width=100% border=1>";
	$themes = array('bed & breakfast'=>'bed_and_breakfasts', 'guest house'=>'guest_houses', 'lodge'=>'lodges', 'self-catering'=>'self_catering', 'hotel'=>'hotels' );
	
	//Includes
	include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';
	
	//Prepare statement
	$statement = "SELECT province_id, country_id FROM nse_location_town WHERE town_name=?";
	$sql_town = $dbCon->prepare($statement);
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=?";
	$sql_province = $dbCon->prepare($statement);
	$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=?";
	$sql_country = $dbCon->prepare($statement);
	$statement = "SELECT b.region_name FROM nse_location_region_towns AS a 
								JOIN nse_location_region_lang AS b ON a.region_id=b.region_id
								WHERE country_id=?";
	$sql_region = $dbCon->prepare($statement);
	
	//Redirect /static/establishments
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/staticnew/accommodation/';
	$dir_contents = scandir($dir);
	foreach ($dir_contents as $file) {
		if ($file == '.' || $file == '..') continue;
		
		$nFile = str_replace('_', ' ', $file);
		$nFile = str_replace('.html', '', $nFile);
		
		$file_theme = 'places_to_stay';
		$place_name = $nFile;
		foreach($themes as $t=>$th) {
			if (strpos($nFile, $t) !== FALSE) {
				$file_theme = $th;
				$place_name = str_replace($t, '', $nFile);
			}
		}
		
		//Find place
		$cID = '';
		$pID = '';
		$country_name = '';
		$province_name = '';
		$region_name = '';
		$sql_town->bind_param('s', $place_name);
		$sql_town->execute();
		$sql_town->bind_result($pID, $cID);
		$sql_town->store_result();
		$sql_town->fetch();
		
		if (!empty($pID)) {
			$sql_province->bind_param('s', $pID);
			$sql_province->execute();
			$sql_province->bind_result($province_name);
			$sql_province->store_result();
			$sql_province->fetch();
		}
		
		if (!empty($cID)) {
			$sql_country->bind_param('s', $cID);
			$sql_country->execute();
			$sql_country->bind_result($country_name);
			$sql_country->store_result();
			$sql_country->fetch();
			
			$sql_region->bind_param('s', $cID);
			$sql_region->execute();
			$sql_region->bind_result($region_name);
			$sql_region->store_result();
			$sql_region->fetch();
		}
		
		$place_name = trim($place_name);
		$link = "/accommodation/$file_theme/$region_name/$country_name/$province_name/$place_name/index.html";
		$link = str_replace(' ', '_', $link);
		$link = strtolower($link);
		
		//Check if destination exists
		$ok = '';
		if (is_file($_SERVER['DOCUMENT_ROOT'] . $link)) {
			//Assemble page Data
			$pageData = "<?php \n";
			$pageData .= "header('HTTP/1.1 301 Moved Permanently');";
			$pageData .= "header('Location: http://booktravel.travel$link');";
			$pageData .= "exit();";
			
			file_put_contents($dir . $file, $pageData);
			$ok = 'OK';
		}
		
		echo "<tr><td><a href='/staticnew/accommodation/$file'>$file</a></td><td>$ok</td><td>$file_theme</td><td><a href='$link'>$link</a></td></tr>";
		$counter++;
	}
	
	echo "</table>";
}

function e3() {
	//Vars
	$counter = 0;
	echo "<table width=100% border=1>";
	$themes = array('bed & breakfast'=>'bed_and_breakfasts', 'guest house'=>'guest_houses', 'lodge'=>'lodges', 'self-catering'=>'self_catering', 'hotel'=>'hotels' );
	
	//Includes
	include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';
	
	//Prepare Statements
	$statement = 'SELECT province_id, country_id FROM nse_location_town WHERE town_name=?';
	$sql_town = $dbCon->prepare($statement);
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=?";
	$sql_province = $dbCon->prepare($statement);
	$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=?";
	$sql_country = $dbCon->prepare($statement);
	$statement = "SELECT b.region_name FROM nse_location_region_towns AS a 
								JOIN nse_location_region_lang AS b ON a.region_id=b.region_id
								WHERE country_id=?";
	$sql_region = $dbCon->prepare($statement);
	
	//Redirect /static/establishments
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/staticnew/towns/';
	$dir_contents = scandir($dir);
	foreach ($dir_contents as $file) {
		if ($file == '.' || $file == '..') continue;
		
		$town = substr($file, 0, -5);
		$place_name = $town;
		$town = str_replace('_', ' ', $town);
		
		$cID = '';
		$pID = '';
		$sql_town->bind_param('s', $town);
		$sql_town->execute();
		$sql_town->bind_result($pID, $cID);
		$sql_town->store_result();
		$sql_town->fetch();
		
		if (!empty($pID)) {
			$sql_province->bind_param('s', $pID);
			$sql_province->execute();
			$sql_province->bind_result($province_name);
			$sql_province->store_result();
			$sql_province->fetch();
		}
		
		if (!empty($cID)) {
			$sql_country->bind_param('s', $cID);
			$sql_country->execute();
			$sql_country->bind_result($country_name);
			$sql_country->store_result();
			$sql_country->fetch();
			
			$sql_region->bind_param('s', $cID);
			$sql_region->execute();
			$sql_region->bind_result($region_name);
			$sql_region->store_result();
			$sql_region->fetch();
		}
		
		$place_name = trim($place_name);
		$link = "/accommodation/places_to_stay/$region_name/$country_name/$province_name/$place_name/index.html";
		$link = str_replace(' ', '_', $link);
		$link = strtolower($link);
		
	//Check if destination exists
		$ok = '';
		if (is_file($_SERVER['DOCUMENT_ROOT'] . $link)) {
			//Assemble page Data
			$pageData = "<?php \n";
			$pageData .= "header('HTTP/1.1 301 Moved Permanently');";
			$pageData .= "header('Location: http://booktravel.travel$link');";
			$pageData .= "exit();";
			
			file_put_contents($dir . $file, $pageData);
			$ok = 'OK';
		}
		
		$counter++;
		echo "$counter - $dir{$file}($ok)<br>";
	}
}

function e4() {
//Vars
	$counter = 0;
	echo "<table width=100% border=1>";
	$themes = array('bed & breakfast'=>'bed_and_breakfasts', 'guest house'=>'guest_houses', 'lodge'=>'lodges', 'self-catering'=>'self_catering', 'hotel'=>'hotels' );
	
	//Includes
	include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';
	
	//Prepare Statements
	$statement = 'SELECT province_id, country_id FROM nse_location_province WHERE province_name=?';
	$sql_province1 = $dbCon->prepare($statement);
	$statement = "SELECT country_id FROM nse_location_country_lang WHERE country_name=?";
	$sql_country1 = $dbCon->prepare($statement);
	$statement = "SELECT province_name FROM nse_location_province WHERE province_id=?";
	$sql_province = $dbCon->prepare($statement);
	$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=?";
	$sql_country = $dbCon->prepare($statement);
	$statement = "SELECT b.region_name FROM nse_location_region_towns AS a 
								JOIN nse_location_region_lang AS b ON a.region_id=b.region_id
								WHERE country_id=?";
	$sql_region = $dbCon->prepare($statement);
	
	//Redirect /static/establishments
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/staticnew/';
	$dir_contents = scandir($dir);
	foreach ($dir_contents as $file) {
		if ($file == '.' || $file == '..') continue;
		
		$town = substr($file, 0, -5);
		$place_name = $town;
		$town = str_replace('_', ' ', $town);
		
		$cID = '';
		$pID = '';
		$type = '';
		
		$sql_province1->bind_param('s', $town);
		$sql_province1->execute();
		$sql_province1->bind_result($pID, $cID);
		$sql_province1->store_result();
		if (0 == $sql_province1->num_rows()) {
			$sql_country1->bind_param('s', $town);
			$sql_country1->execute();
			$sql_country1->bind_result($cID);
			$sql_country1->store_result();
			if (0 == $sql_country1->num_rows()) {
				echo "$counter - $dir{$file}(FAILED)<br>";
				continue;
			} else {
				$sql_country1->fetch();
				$type = 'C';
			}
		} else {
			$sql_province1->fetch();	
			$type = 'P';
		}		
		
		$sql_province->bind_param('s', $pID);
		$sql_province->execute();
		$sql_province->bind_result($province_name);
		$sql_province->store_result();
		$sql_province->fetch();
		
		$sql_country->bind_param('s', $cID);
		$sql_country->execute();
		$sql_country->bind_result($country_name);
		$sql_country->store_result();
		$sql_country->fetch();
		
		$sql_region->bind_param('s', $cID);
		$sql_region->execute();
		$sql_region->bind_result($region_name);
		$sql_region->store_result();
		$sql_region->fetch();
		
		if ('C' == $type) {
			$link = "/accommodation/places_to_stay/$region_name/$country_name/index.html";
		} else {
			$link = "/accommodation/places_to_stay/$region_name/$country_name/$province_name/index.html";
		}
		$link = str_replace(' ', '_', $link);
		$link = strtolower($link);
		
		//Check if destination exists
		$ok = '';
		if (is_file($_SERVER['DOCUMENT_ROOT'] . $link)) {
			//Assemble page Data
			$pageData = "<?php \n";
			$pageData .= "header('HTTP/1.1 301 Moved Permanently');";
			$pageData .= "header('Location: http://booktravel.travel$link');";
			$pageData .= "exit();";
			
			file_put_contents($dir . $file, $pageData);
			$ok = 'OK';
		}
		
		$counter++;
		echo "$counter - $dir{$file}($link)<br>";
	}
}

?>