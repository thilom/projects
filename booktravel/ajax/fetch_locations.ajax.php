<?php
/**
 * Return a list of available locations for a specefied keyword
 *
 * 2016-04-26: Thilo Muller - Created
 */

//Vars
$url = "http://v2.api.booktravel.travel/v2/search_location.php";
$url = "http://api.booktravel.local/v2/search_location.php";
$posts = '';

//Assemble post data
foreach ($_POST as $key=>$value) {
    $posts .= "$key=$value&";
}
$posts .= "source=booktravel";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "$url");
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$posts);

$result = curl_exec($ch);

curl_close($ch);

echo $result;


