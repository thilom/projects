<?xml version="1.0" encoding="ISO-8859-2" ?>
<%@ page 
language="java"
import="java.io.*, java.util.*, com.eti.object.*, com.eti.util.*, za.co.aatravel.beans.Accommodation, org.apache.log4j.Logger"
%><%	 
	String town = request.getParameter("town");
	List estabs = DataRetriever.fetchEstablishments(town);	
	
        String xml = "<establishments>";        
	for (int i = 0; i < estabs.size(); i++) 
	{
		Accommodation estab = (Accommodation)estabs.get(i);                
                xml += "<establishment id='" + estab.getCode() + "'>" + java.net.URLEncoder.encode(estab.getName(), "UTF-8") + "</establishment>";
	}
	xml += "</establishments>";
        response.addHeader("Content-type", "text/xml");
	out.print(xml);
%>
