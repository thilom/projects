<?php 
include($_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php');

if (isset($_POST['delEstab'])) {
	delete_estab();
} else {
	form();
}

function form() {
	echo "<form action='' method=post>Code: <input type=text name=code><input type=submit name=delEstab value=Delete></form>";
}

function delete_estab() {
	$code = $_POST['code'];
	
	$statement = "DELETE FROM nse_establishment WHERE establishment_code=?";
	$sql_delete1 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete1->bind_param('s', $code);
	$sql_delete1->execute();
	$sql_delete1->close();
	
	$statement = "DELETE FROM nse_establishment_contact WHERE establishment_code=?";
	$sql_delete2 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete2->bind_param('s', $code);
	$sql_delete2->execute();
	$sql_delete2->close();
	
	$statement = "DELETE FROM nse_establishment_data WHERE establishment_code=?";
	$sql_delete3 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete3->bind_param('s', $code);
	$sql_delete3->execute();
	$sql_delete3->close();
	
	$statement = "DELETE FROM nse_establishment_deprecated WHERE establishment_code=?";
	$sql_delete4 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete4->bind_param('s', $code);
	$sql_delete4->execute();
	$sql_delete4->close();
	
	$statement = "DELETE FROM nse_establishment_descriptions WHERE establishment_code=?";
	$sql_delete5 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete5->bind_param('s', $code);
	$sql_delete5->execute();
	$sql_delete5->close();
	
	$statement = "DELETE FROM nse_establishment_icon WHERE establishment_code=?";
	$sql_delete6 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete6->bind_param('s', $code);
	$sql_delete6->execute();
	$sql_delete6->close();
	
	$statement = "DELETE FROM nse_establishment_images WHERE establishment_code=?";
	$sql_delete7 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete7->bind_param('s', $code);
	$sql_delete7->execute();
	$sql_delete7->close();
	
	$statement = "DELETE FROM nse_establishment_location WHERE establishment_code=?";
	$sql_delete8 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete8->bind_param('s', $code);
	$sql_delete8->execute();
	$sql_delete8->close();
	
	$statement = "DELETE FROM nse_establishment_reservation WHERE establishment_code=?";
	$sql_delete9 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete9->bind_param('s', $code);
	$sql_delete9->execute();
	$sql_delete9->close();
	
	$statement = "DELETE FROM nse_establishment_restype WHERE establishment_code=?";
	$sql_delete10 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete10->bind_param('s', $code);
	$sql_delete10->execute();
	$sql_delete10->close();
	
	$statement = "DELETE FROM nse_search_drum WHERE establishment_code=?";
	$sql_delete11 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete11->bind_param('s', $code);
	$sql_delete11->execute();
	$sql_delete11->close();
	
	$statement = "DELETE FROM nse_search_brush WHERE establishment_code=?";
	$sql_delete12 = $GLOBALS['dbCon']->prepare($statement);
	$sql_delete12->bind_param('s', $code);
	$sql_delete12->execute();
	$sql_delete12->close();
	
	echo "Establishment Deleted";
}

?>