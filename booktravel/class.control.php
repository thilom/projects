<?php

error_reporting(E_ALL);
ini_set('display_errors', 0);

class control {
	private $api;
	private $output;
	private $pageParam;
	private $config;
	private $debugSite = false;
	
	private $template;
	private $templateLinkBlock;
	private $searchString;
	
	private $listPage;
	
	private $pageTitle;
	private $pageDescription;



	function __construct() {
		global $api;
		global $output;
		global $pageParam;
		global $template;
		global $templateLinkBlock;
		global $searchString;
		global $config;
		$areaXml = '';
		$config = new config();
		$api = new api();

		if (isset($_POST["ajaxpost"])) {

			if(isset($_POST["getAdvSearch"])) {
				$pageNum = 1;
				$api->setMethod("fetch");
				$api->setType("aaweb_areas");
				$pageUrl = "";
				$urlSplit = explode("/",$_POST["getAdvSearch"]);
				for($i=0;$i<12;$i++) {
					$page = explode("_",$urlSplit[$i]);
					if ($page[0] == "page") {
						$pageNum = $page[1];
					} else {
						if ($urlSplit[$i] != "") {
							$pageUrl .= $urlSplit[$i]."/";
						}
					}
				}
				$api->addParam("url",trim($pageUrl,"/"));
				$areaXml = $api->getApiXML();
				$null = $this->ajaxGetRegionData($areaXml,$pageNum);
				$api->setMethod("fetch");
				$api->setType("advsearchfields");
				$advSearchXml = $api->getApiXML();
				$null = $this->ajaxAdvSearch($advSearchXml);
			}
			
			if(isset($_POST["getRegionData"])) {
				$pageNum = 1;
				$api->setMethod("fetch");
				$api->setType("aaweb_areas");
				$pageUrl = "";
				$urlSplit = explode("/",$_POST["getRegionData"]);
				for($i=0;$i<12;$i++) {
					$page = explode("_",$urlSplit[$i]);
					if ($page[0] == "page") {
						$pageNum = $page[1];
					} else {
						if ($urlSplit[$i] != "") {
							$pageUrl .= $urlSplit[$i]."/";
						}
					}
				}
				$api->addParam("url",trim($pageUrl,"/"));
				$areaXml = $api->getApiXML();
				echo $this->ajaxGetRegionData($areaXml,$pageNum);
			}
			if(isset($_POST["searchLocation"])) {
				$api->setMethod("fetch");
				$api->setType("websearch");
				$api->addParam("type","location");
				$api->addParam("search_string",$_POST["searchLocation"]);
				$searchXml = $api->getApiXML();
				echo $this->ajaxLocationSearch($searchXml);
			}
		} else {
			if (isset($_POST["searchString"])) {
				$_SESSION["searchString"] = $_POST["searchString"];
				header("location: /search/".urlencode($_POST["searchString"]));
			}

			$pageQuery = isset($_GET["page"])?$_GET["page"]:'';
			$pageParam = explode("/",$pageQuery);
			
			include('redirects.php');

			for($i=0;$i<13;$i++) {
				if (!isset($pageParam[$i])) {
					$pageParam[$i] = "";
				}
			}
			
			if ($pageParam[0] == "search") {
				$_SESSION["searchString"] = $pageParam[1];
			}
			
			if ($pageParam[0] == "asearch") {
				$_SESSION["asearch"] = $pageParam[1];
			}
			
			if (!isset($_SESSION["searchString"])) {
				$_SESSION["searchString"] = "";
			}
			$searchString = $_SESSION["searchString"];
			
			$template = file_get_contents("templates/site_template.html");
			$templateGeneral = file_get_contents("templates/general_content.tpl");
			$templatelinkBlockStart = strpos($templateGeneral,"<!-- block_start -->");
			$templatelinkBlockEnd = strpos($templateGeneral,"<!-- block_end -->");
			$templateLinkBlock = substr($templateGeneral,$templatelinkBlockStart,($templatelinkBlockEnd-$templatelinkBlockStart));

			$this->getSearch();
			$this->getBreadCrumb($areaXml);
			$this->getBody();
			$this->getMeta();
			
			echo $template;
		}

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}
	
	function ajaxAdvSearch($advSearchXml) {
		$categories = "";
		$categories .= "<div><input onclick='javascript:advSearchQA=\"\"' type='radio' name='advCategory' value='' />All Quality Assurance</div>";
		foreach ($advSearchXml->aaCategories->category AS $category) {
			$categories .= "<div><input onclick='javascript:advSearchQA=\"".$category['code']."\"' type='radio' name='advCategory' value='".$category['code']."' />$category</div>";
		}
		echo "{%startDiv:AdvancedSearchQA%}$categories{%endDiv%}";
		
		$icons = "";
		
		foreach ($advSearchXml->iconCategory AS $category) {
			$icons .= "<fieldset class='advSearchFieldsetIcon'><legend>".$category['name']."</legend>";
			foreach ($category AS $icon) {
				$icons .= "<div onclick='javascript:advSearchIconClick(this,\"".$icon['id']."\");' class='advSearchIconOff'><div class='advSearchIconImg'><img align='absmiddle' src='".$icon['url']."' /></div><div class='advSearchIconText'>".$icon."</div></div>";
			}
			$icons .= "</fieldset>";
		}
		echo "{%startDiv:AdvancedSearchIcons%}$icons{%endDiv%}";

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}
	
	function ajaxLocationSearch($searchXml) {
		$list = "<button type='button' class='close' aria-label='Close' onclick='document.getElementById(\"LocationSearchBox\").className=\"hidden\"'><span aria-hidden='true'>&times;</span></button>";
		$list .= "<h3>Results for <em>".$_POST["searchLocation"]."</em></h3>";
		$log_params = "";
		foreach ($searchXml->search->location AS $location) {
			$list .= "<div><u><a href='/".$location['safe_url']."'>".urldecode($location)."</a></u></div>";
			$log_params .= $location['safe_url'].",";
		}
		$log_params = rtrim($log_params,",");
		$log_url = $_POST["searchLocation"];
		$log_type = "ls";
		$log_ip = $_SERVER["REMOTE_ADDR"];
		$this->webLog($log_params,$log_url,$log_type,$log_ip);
		
		$list = str_replace("__matchstart__", "<em><strong>", $list);
		$list = str_replace("__matchend__", "</strong></em>", $list);

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return "{%startDiv:LocationSearchBox%}$list{%endDiv:javascript:openResult();%}";


	}
	
	function ajaxGetRegionData($areaXml,$page) {
		global $api;
		global $pageTitle;
	
		$preUrl = "accommodation/".$areaXml->theme["safe_url"];
		$preUrl .= "/".$areaXml->region["safe_url"];
		$preUrl = trim($preUrl,"/");
		
		$loader = "aaLoading()";
		
		$dropDowns = "";
		$dropCount = 0;
		
		if (isset($areaXml->countries->country)) {
			$list = "";
			$firstLink = "<strong>Select a country</strong>";
			foreach ($areaXml->countries->country AS $counrty) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$counrty["safe_url"]."\");advSearchLoc=\"/$preUrl/".$counrty["safe_url"]."\";return false;";
					//$onclick=str_replace("/",",",$onclick);
				}
				$listLink = "".urldecode($counrty)."";
				if ($counrty["id"]."" == $areaXml->country["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$counrty["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li><a onclick='$onclick' href='/$preUrl/".$counrty["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$counrty["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->country["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->provinces->province)) {
			$list = "";
			$firstLink = "<strong>Select a province</strong>";
			foreach ($areaXml->provinces->province AS $province) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$province["safe_url"]."\");advSearchLoc=\"/$preUrl/".$province["safe_url"]."\";return false;";
				}
				$listLink = "".urldecode($province)."";
				if ($province["id"]."" == $areaXml->province["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$province["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$province["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$province["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->province["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->province_regions->province_region)) {
			$list = "";
			$firstLink = "<strong>Select a region</strong>";
			foreach ($areaXml->province_regions->province_region AS $province_region) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$province_region["safe_url"]."\");advSearchLoc=\"/$preUrl/".$province_region["safe_url"]."\";return false;";
				}
				$listLink = "".urldecode($province_region)."";
				if ($province_region["id"]."" == $areaXml->province_region["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$province_region["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$province_region["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$province_region["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->province_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->sub_regions->sub_region)) {
			$list = "";
			$firstLink = "<strong>Select a sub-region</strong>";
			foreach ($areaXml->sub_regions->sub_region AS $sub_region) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$sub_region["safe_url"]."\");advSearchLoc=\"/$preUrl/".$sub_region["safe_url"]."\";return false;";
				}
				$listLink = "".urldecode($sub_region)."";
				if ($sub_region["id"]."" == $areaXml->sub_region["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$sub_region["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$sub_region["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$sub_region["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->sub_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->towns->town)) {
			$list = "";
			$firstLink = "<strong>Select a town</strong>";
			foreach ($areaXml->towns->town AS $town) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$town["safe_url"]."\");advSearchLoc=\"/$preUrl/".$town["safe_url"]."\";return false;";
				}
				$listLink = "".urldecode($town)."";
				if ($town["id"]."" == $areaXml->town["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$town["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$town["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$town["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->town["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->town_regions->town_region)) {
			$list = "";
			$firstLink = "<strong>Select a town region</strong>";
			foreach ($areaXml->town_regions->town_region AS $town_region) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$town_region["safe_url"]."\");advSearchLoc=\"/$preUrl/".$town_region["safe_url"]."\";return false;";
				}
				$listLink = "".urldecode($town_region)."";
				if ($town_region["id"]."" == $areaXml->town_region["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$town_region["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li><a onclick='$onclick' href='/$preUrl/".$town_region["safe_url"]."'>".$listLink."</a></li>";
					// onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$town_region["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		$preUrl .= "/".$areaXml->town_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->suburbs->suburb)) {
			$list = "";
			$firstLink = "<strong>Select a suburb</strong>";
			foreach ($areaXml->suburbs->suburb AS $suburb) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$suburb["safe_url"]."\");advSearchLoc=\"/$preUrl/".$suburb["safe_url"]."\";return false;";
				}
				$listLink = "".urldecode($suburb)."";
				if ($suburb["id"]."" == $areaXml->suburb["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$suburb["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$suburb["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$suburb["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$rdclass = $this->getProvinceClass($areaXml);
		
		$preUrl .= "/".$areaXml->suburb["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->themes->theme)) {
			if (isset($areaXml->suburb)) {$areaHeading = $areaXml->suburb;} else
			if (isset($areaXml->town_region)) {$areaHeading = $areaXml->town_region;} else
			if (isset($areaXml->town)) {$areaHeading = $areaXml->town;} else
			if (isset($areaXml->sub_region)) {$areaHeading = $areaXml->sub_region;} else
			if (isset($areaXml->province_region)) {$areaHeading = $areaXml->province_region;} else
			if (isset($areaXml->province)) {$areaHeading = $areaXml->province;} else
			if (isset($areaXml->country)) {$areaHeading = $areaXml->country;} else
			if (isset($areaXml->region)) {$areaHeading = $areaXml->region;}
			$themeList = "<div class='areaBlock'>";
			$themeList .= "<h1 class='$rdclass'>Accommodation in ".urldecode($areaHeading)."</h1><ul>";
			foreach ($areaXml->themes->theme AS $theme) {
				$themUrl = str_replace("/".$areaXml->theme["safe_url"]."/", "/".$theme["safe_url"]."/", $preUrl);
				$themeList .= "<li><a  href='/$themUrl'>".urldecode($theme)."</a> (".$theme["count"].")</li>";
				//onclick='javascript:loadPage(\"getRegionData=/$themUrl&changetheme=1\");$loader;return false;'
			}
			$themeList .= "</ul></div>";
		}
		
		$api->setMethod("fetch");
		$api->setType("aaweb_establist");
		if (isset($areaXml->theme)) {$api->addParam("theme_id",$areaXml->theme['id']);}
		if (isset($areaXml->region)) {$api->addParam("region_id",$areaXml->region['id']);}
		if (isset($areaXml->country)) {$api->addParam("country_id",$areaXml->country['id']);}
		if (isset($areaXml->province)) {$api->addParam("province_id",$areaXml->province['id']);}
		if (isset($areaXml->province_region)) {$api->addParam("pregion_id",$areaXml->province_region['id']);}
		if (isset($areaXml->sub_region)) {$api->addParam("sregion_id",$areaXml->sub_region['id']);}
		if (isset($areaXml->town)) {$api->addParam("town_id",$areaXml->town['id']);}
		if (isset($areaXml->town_region)) {$api->addParam("tregion_id",$areaXml->town_region['id']);}
		if (isset($areaXml->suburb)) {$api->addParam("suburb_id",$areaXml->suburb['id']);}
		//$pageSplit = explode("_",$page);
		$api->addParam("page",$page);
		$areaEstabListXml = $api->getApiXML();
		if (isset($_POST["ajaxpost"])) {
			$areaEstabList = $this->getEstList($areaEstabListXml,$areaXml,$preUrl."/page_".$page);
			$log_params = "";
			foreach ($areaEstabListXml->estlist->establishment AS $result) {
				$log_params .= $result["code"].",";
			}
			$log_params = rtrim($log_params,",");
			$log_url = "/".$preUrl."/page_".$page."/";
			$log_type = "l";
			$log_ip = $_SERVER["REMOTE_ADDR"];
			$this->webLog($log_params,$log_url,$log_type,$log_ip);
		}
		
		$rdclass = $this->getProvinceClass($areaXml);
		
		if (isset($_POST["ajaxpost"]) && !isset($_POST["getAdvSearch"])) {
			echo "{%startDiv:EstabListHolder%}$areaEstabList{%endDiv:javascript:document.location='#".$_POST['getRegionData']."';currentUrl='".$_POST['getRegionData']."';%}";
			$dropDowns = "{%startDiv:RegionDropDowns%}$dropDowns{%endDiv:javascript:document.getElementById('RegionDropDowns').className='$rdclass';document.title='$pageTitle';pageTracker._trackPageview('$preUrl');%}{%startDiv:AreaBlockHolder%}$themeList{%endDiv%}";
		} elseif (isset($_POST["getAdvSearch"])) {
			echo "{%startDiv:AdvancedSearchLocations%}$dropDowns{%endDiv%}'";
		}


        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";


        return $dropDowns;
	}

    function getEstList($apiResult,$areaXml,$url) {
        global $pageTitle;
        global $pageParam;
        $color = '';

        //echo $url;

        $loader = "aaLoading()";

        $pageUrl = "";
        $urlSplit = explode("/",$url);
        for($i=0;$i<13;$i++) {
            $page = !empty($urlSplit[$i])?explode("_",$urlSplit[$i]):array('');
            if ($page[0] == "page") {

            } else {
                if (isset($urlSplit[$i]) && $urlSplit[$i] != "") {
                    $pageUrl .= $urlSplit[$i]."/";
                    //echo $urlSplit[$i];
                } else {
                    $pageUrl = '';
                }
            }
        }
        //echo $pageUrl;

        if (isset($apiResult->estlist->establishment) || true) {

            $pageHeader = "";
            $headerCount = 0;
            if (isset($areaXml->suburb)) {
                $pageHeader .= ", ".urldecode($areaXml->suburb);
                $headerCount++;
            }
            if (isset($areaXml->town_region)) {
                $pageHeader .= ", ".urldecode($areaXml->town_region);
                $headerCount++;
            }
            if (isset($areaXml->town)) {
                $pageHeader .= ", ".urldecode($areaXml->town);
                $headerCount++;
            }
            if (isset($areaXml->sub_region)) {
                $pageHeader .= ", ".urldecode($areaXml->sub_region);
                $headerCount++;
            }
            if (isset($areaXml->province_region)) {
                $pageHeader .= ", ".urldecode($areaXml->province_region);
                $headerCount++;
            }
            if (isset($areaXml->province)) {
                if ($headerCount < 5) {
                    $pageHeader .= ", ".urldecode($areaXml->province);
                    $headerCount++;
                }
            }
            if (isset($areaXml->country)) {
                if ($headerCount < 5) {
                    $pageHeader .= ", ".urldecode($areaXml->country);
                    $headerCount++;
                }
            }
            if (isset($areaXml->region)) {
                if ($headerCount < 5) {
                    $pageHeader .= ", ".urldecode($areaXml->region);
                    $headerCount++;
                }
            }
            $pageHeader = trim($pageHeader,", ");
            $breadcrumbs = $this->getBreadCrumb($areaXml);
            $rdclass = $this->getProvinceClass($areaXml);

            $estabUrl = $pageUrl;


            if($url == "") {
                $pageTitle = "Search results for ".$pageParam[1]." - Booktravel.travel";
                $pageUrl = $pageParam[0]."/".$pageParam[1]."/";
                $estabUrl = "accommodation/";
            } elseif (isset($areaXml->establishment)) {
                $pageTitle = urldecode($areaXml->establishment)." in $pageHeader - Booktravel.travel";
            } else {
                if ($areaXml->theme['safe_url'] == "bonsela") {
                    $pageTitle = "Discount Accommodation in $pageHeader - Bonsela Programme";
                } else {
                    $pageTitle = urldecode($areaXml->theme)." in $pageHeader - Booktravel.travel";
                }
            }

            if ($areaXml == "advsearch") {
                $pageTitle = "Advanced Search Results";
                $pageUrl = $pageParam[0]."/".$pageParam[1]."/".$pageParam[2]."/".$pageParam[3]."/";
                $estabUrl = "accommodation/";
                $log_params = "";
                foreach ($apiResult->estlist->establishment AS $result) {
                    $log_params .= $result["code"].",";
                }
                $log_params = rtrim($log_params,",");
                $log_url = $_SERVER["REQUEST_URI"];
                $log_type = "as";
                $log_ip = $_SERVER["REMOTE_ADDR"];
                $this->webLog($log_params,$log_url,$log_type,$log_ip);
            }

            $addBanner = "<div align='center' style='margin-top:20px;'>
			    <script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>
                <!-- booktravel.travel/essentialtravelinfo.com right sky ad -->
                <ins class=\"adsbygoogle\"
                     style=\"display:inline-block;width:160px;height:600px\"
                     data-ad-client=\"ca-pub-4514882728543448\"
                     data-ad-slot=\"6493604526\"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script></div>";

            //Open Row
            $estList = "<div class='row'>";

            //Open First Col
            if($url == "") {
                $estList .= "<div class='col-md-9'>";
            } else {
                $estList .= "<div class='col-md-12'>";
            }
            $estList .= "<div style='text-align: left'>$breadcrumbs</div>";
            $estList .= "<h1 class='estListHeader '>$pageTitle</h1>";
            $rightContent = "<a href='/index.php?p=map_regions'><img src='/images/mini-map-right.png' /></a><br />$addBanner";
            if ($_SERVER["HTTP_HOST"] == "na.essentialtravelinfo.com") {
                $rightContent = file_get_contents("templates/template_right_na.tpl");
            }

            $paging = '';
            if ($apiResult->estlist->page > 1) {
                $paging .= "<a onclick='javascript:loadPage(\"getRegionData=$pageUrl"."page_".(($apiResult->estlist->page)-1)."\");$loader;return false;' href='/$pageUrl"."page_".(($apiResult->estlist->page)-1)."'>&lt;&lt;Previous</a> | ";
            }
            $paging .= $apiResult->estlist->page['totalresults']." Results found, Page ".$apiResult->estlist->page." of ".$apiResult->estlist->page['totalpages'];

            if ((($apiResult->estlist->page['totalpages'])+1) > (($apiResult->estlist->page)+1)) {
                $paging .= " | <a onclick='javascript:loadPage(\"getRegionData=$pageUrl"."page_".(($apiResult->estlist->page)+1)."\");$loader;return false;'  href='/$pageUrl"."page_".(($apiResult->estlist->page)+1)."'>Next &gt;&gt;</a>";
            }

            $estList .= "<div class='estListPaging'>$paging</div>";

            $estCounter = 0;
            foreach ($apiResult->estlist->establishment AS $result) {
                if ($color == "EBEBEB") {
                    $color = "ffffff";
                } else {
                    $color = "EBEBEB";
                }

                $locationLinks = "";
                $theme_url = "places_to_stay";
                $region_url = "southern_africa";
                if(isset($areaXml->theme['safe_url'])) {
                    $theme_url = $areaXml->theme['safe_url'];
                }
                if(isset($areaXml->region['safe_url'])) {
                    $region_url = $areaXml->region['safe_url'];
                }
                $locationLinksUrl = "/accommodation/".$theme_url."/".$region_url;
                if (isset($result->country)) {
                    $locationLinksUrl .= "/".$result->country["safe_url"];
                    $locationLinks = "<a href='$locationLinksUrl'>".urldecode($result->country)."</a>, ".$locationLinks;
                }
                if (isset($result->province)) {
                    $locationLinksUrl .= "/".$result->province["safe_url"];
                    $locationLinks = "<a href='$locationLinksUrl'>".urldecode($result->province)."</a>, ".$locationLinks;
                }
                if (isset($result->province_region)) {
                    $locationLinksUrl .= "/".$result->province_region["safe_url"];
                    $locationLinks = "<a href='$locationLinksUrl'>".urldecode($result->province_region)."</a>, ".$locationLinks;
                }
                if (isset($result->sub_region)) {
                    $locationLinksUrl .= "/".$result->sub_region["safe_url"];
                    $locationLinks = "<a href='$locationLinksUrl'>".urldecode($result->sub_region)."</a>, ".$locationLinks;
                }
                if (isset($result->town)) {
                    $locationLinksUrl .= "/".$result->town["safe_url"];
                    $locationLinks = "<a href='$locationLinksUrl'>".urldecode($result->town)."</a>, ".$locationLinks;
                }
                if (isset($result->town_region)) {
                    $locationLinksUrl .= "/".$result->town_region["safe_url"];
                    $locationLinks = "<a href='$locationLinksUrl'>".urldecode($result->town_region)."</a>, ".$locationLinks;
                }
                if (isset($result->suburb)) {
                    $locationLinksUrl .= "/".$result->suburb["safe_url"];
                    $locationLinks = "<a href='$locationLinksUrl'>".urldecode($result->suburb)."</a>, ".$locationLinks;
                }

                $specials = "";
                if (isset($result->specials->special)) {
                    foreach ($result->specials->special AS $special) {
                        if (isset($special->start_date)) {
                            $specials .= "<strong>From ".urldecode($special->start_date)." to ".urldecode($special->end_date)."</strong><br />".urldecode(utf8_encode($special->description))."<br /><br />";
                        }
                    }
                }

                $description = urldecode($result->description);

                if ($specials != "" && ($areaXml->theme['safe_url'] == "specials" || $areaXml->theme['safe_url'] == "bonsela")) {
                    $description = $specials;
                }

                $locationLinks = trim($locationLinks,", ");

                $restype = urldecode($result->restype);

                if (urldecode($result->restype_description) != "") {
                    $restype = "<span class='hidden' style='z-index: 999999; text-align: left' id='restype_".$result["code"]."'>".urldecode($result->restype_description)."</span>
					<u style='cursor:pointer; text-decoration: none' onmouseout='javascript:document.getElementById(\"restype_".$result["code"]."\").className=\"hidden\"' onmouseover='javascript:document.getElementById(\"restype_".$result["code"]."\").className=\"restypeDesc\"'>$restype</u>";
                }
                $estSpecialsOverlay = "";
                if($result->special_type == "s") {$estSpecialsOverlay = "<img class='specList' src='/images/overlay_special.png' />";}
                if($result->special_type == "b") {$estSpecialsOverlay = "<img class='specList' src='/images/overlay_bonsela.png' />";}

                if (isset($areaXml->theme)) {
                    $theme = ' - ' .urldecode($areaXml->theme);
                } else {
                    $theme = '';
                }

                $estList .= "<div class='listEst'>
                                <div class='listEstHeader'>
                                    <div class='row'>
                                        <div class='col-xs-8' style='text-align: left'><h3><span class='est_name'><a href='$locationLinksUrl/" . $result["code"] . "_" . $result->link_name . "/' style='color:white;'>" . urldecode($result->name) . "</a></span></h3></div>
                                        <div class='col-xs-4' style='text-align: right'>$restype<br>" . urldecode($result->room_count) . "</div>
                                    </div>
                                </div>
                                <div class='listEstContent'>
                                    <div class='row'>
                                        <div class='col-xs-3' style='text-align: left'>
                                            <a href='$locationLinksUrl/" . $result["code"] . "_" . $result->link_name . "/' style='color:#0770B0;'><img src='http://booktravel.travel/image.php?e=" . $result["code"] . "&bg=$color' class='img-responsive' /></a>
                                        </div>
                                        <div class='col-xs-9' style='text-align: left'>
                                            $locationLinks" . $theme . "<br>
                                            " . urldecode($description) . "
                                            <a href='$locationLinksUrl/" . $result["code"] . "_" . $result->link_name . "/' class='listEstLink'>Read More...</a>
                                        </div>
                                    </div>
                                </div>
                            </div>";

                if ($estCounter == 0) {
                    $estList .= "<div class='well spaceTop20'>
                                    <div class='row'>
                                        <div class='col-xs-12 col-sm-6 alignCenter'>
                                            <script async src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>
                                                <!-- Booktravel - Inter-Search Result - Mobile -->
                                                <ins class='adsbygoogle'
                                                     style='display:inline-block;width:320px;height:50px'
                                                     data-ad-client='ca-pub-4514882728543448'
                                                     data-ad-slot='6055430521'></ins>
                                                <script>
                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>

                                        <div class='col-sm-6 hidden-xs alignCenter'>
                                            <script async src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>
                                                <!-- Booktravel - Inter-Search Result - Mobile -->
                                                <ins class='adsbygoogle'
                                                     style='display:inline-block;width:320px;height:50px'
                                                     data-ad-client='ca-pub-4514882728543448'
                                                     data-ad-slot='6055430521'></ins>
                                                <script>
                                                (adsbygoogle = window.adsbygoogle || []).push({});
                                                </script>
                                        </div>
                                    </div>
                                </div>";
                }

                $estCounter++;

            }
            $estList .= "<div class='estListPaging'>$paging</div>";

            //Close Col
            $estList .= "</div>";

//            if($url == "") {
//                $estList .= "<div id='SearchRight' class='col-md-3'>$rightContent</div>";
//            }

//            $estList .= "<br /><br />$breadcrumbs"; //Moved to top of column (Thilo)

            //Close row
            $estList .= "</div>";

            //Log Function Stats
            // $search_string = __FILE__ . "|" . __FUNCTION__;
            // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

            return $estList;//.$blockAreas;
        }




    }
	
	function webLog($params,$url,$type,$ip) {
		global $config;
		$postFields = "";
		$postFields .= "params=".urlencode($params)."&url=".urlencode($url)."&type=".urlencode($type)."&ip=".urlencode($ip)."&agent=".urlencode($_SERVER["HTTP_USER_AGENT"]);
		

		if (
			strpos($_SERVER["HTTP_USER_AGENT"],"msnbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Baiduspider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Googlebot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"bingbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"psbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Sosospider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Exabot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Slurp") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Ezooms") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"msnbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"spbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"YandexBot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"discobot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"ia_archiver") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Sogou web spider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"VB Project") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"findlinks") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"AhrefsBot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"MJ12bot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"informerabot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"WordPress") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"libwww-perl/5") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"LYCOSA") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Speedy Spider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"MSIE 999.1") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Aghaven/Nutch-1.2") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"SemrushBot/0.9") > -1
			|| $_SERVER["HTTP_USER_AGENT"] == ''
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/4.0 (compatible; ICS)'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/4.0 (compatible'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/4.0'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Java/1.6.0_26'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/5.0'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Java/1.6.0_29'
		) {
			//DO NOT LOG
		} else {
			$curlAPI = curl_init($config->apiUrl."weblog.php");
	 		curl_setopt($curlAPI,CURLOPT_POST,1);
	 		curl_setopt($curlAPI,CURLOPT_POSTFIELDS,$postFields);
	 		curl_setopt($curlAPI,CURLOPT_HEADER,0);
	 		curl_setopt($curlAPI,CURLOPT_RETURNTRANSFER,1);
	 		$curlFetched = curl_exec($curlAPI);
	 		curl_close($curlAPI);
		}


        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

    }
	
	function getSearch() {
		global $searchString;
		global $template;
		global $api;
		global $pageParam;
		
		$listPage = 1;
		
		for($i=1;$i<8;$i++) {
			$page = explode("_",$pageParam[$i]);
				
			if ($page[0] == "page") {
				$listPage = $page[1];
			}
		}

		if ($pageParam[0] == "search") {
			$api->setMethod("fetch");
			$api->setType("search");
			$api->addParam("keyword",$searchString);
			$api->addParam("page",$listPage);
			$searchXml = $api->getApiXML();
			$template = $this->setTemplate($template,"body",$this->getEstList($searchXml,"search",""));
			
			$log_params = "";
			foreach ($searchXml->estlist->establishment AS $result) {
				$log_params .= $result["code"].",";
			}
			$log_params = rtrim($log_params,",");
			$log_url = $_SERVER["REQUEST_URI"];
			$log_type = "s";
			$log_ip = $_SERVER["REMOTE_ADDR"];
			$this->webLog($log_params,$log_url,$log_type,$log_ip);
			
		}
		if ($pageParam[0] == "advsearch") {
			//$api->setMethod("fetch");
			//$api->setType("search");
			//$api->addParam("keyword",$searchString);
			//$api->addParam("page",$listPage);
			//$searchXml = $api->getApiXML();
			//$template = $this->setTemplate($template,"body",$this->getEstList($searchXml,"search",""));
			
			$pageUrl = trim($pageParam[1],",");
			$pageUrl = str_replace(",","/",$pageUrl);
			
			$page=1;
			
			$api->setMethod("fetch");
			$api->setType("aaweb_areas");
			$api->addParam("url",trim($pageUrl,""));
			$areaXml = $api->getApiXML();
			
			$api->setMethod("fetch");
			$api->setType("aaweb_establist");
			if (isset($areaXml->theme)) {$api->addParam("theme_id",$areaXml->theme['id']);}
			if (isset($areaXml->region)) {$api->addParam("region_id",$areaXml->region['id']);}
			if (isset($areaXml->country)) {$api->addParam("country_id",$areaXml->country['id']);}
			if (isset($areaXml->province)) {$api->addParam("province_id",$areaXml->province['id']);}
			if (isset($areaXml->province_region)) {$api->addParam("pregion_id",$areaXml->province_region['id']);}
			if (isset($areaXml->sub_region)) {$api->addParam("sregion_id",$areaXml->sub_region['id']);}
			if (isset($areaXml->town)) {$api->addParam("town_id",$areaXml->town['id']);}
			if (isset($areaXml->town_region)) {$api->addParam("tregion_id",$areaXml->town_region['id']);}
			if (isset($areaXml->suburb)) {$api->addParam("suburb_id",$areaXml->suburb['id']);}
			$api->addParam("aaqa_code",trim(str_replace("_","/",$pageParam[2])," "));
			$api->addParam("icon_ids",trim($pageParam[3],","));
			$page = explode("_",$pageParam[4]);
			
			//echo str_replace("_","/",$pageParam[2]);
			
			$api->addParam("page",$page[1]);
			$areaEstabListXml = $api->getApiXML();
			$areaEstabList = $this->getEstList($areaEstabListXml,"advsearch","");
			$template = $this->setTemplate($template,"body",$areaEstabList);
			
		}

		//Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}
	
	function getMeta() {
		global $template;
		global $searchString;
		global $themeXml;
		global $suburbXml;
		global $townXml;
		global $provinceXml;
		global $countryXml;
		global $pageTitle;
		$page_keywords = '';

		if (isset($this->pageDescription) && !empty($this->pageDescription)) {
			$page_description = $this->pageDescription;
		} else {
			$template = $this->setTemplate($template, "search_string", $searchString);

			//if ($pageTitle != "") {
			//	$template = $this->setTemplate($template,"page_title",$pageTitle);
			//} else {
			//	$template = $this->setTemplate($template,"page_title","booktravel.travel");
			//}

			$content = file_get_contents("content/home.html");

			$reg = "@<meta[\s]+[^>]*?name[\s]?=[\s\"\']+(.*?)[\s\"\']+content[\s]?=[\s\"\']+(.*?)[\"\']+.*?>@i";
			preg_match_all($reg, $content, $matches);

			foreach ($matches[1] as $k => $v) {
				if (strtolower($v) == 'description') {
					$contLoc = strpos($matches[0][$k], "content=");
					$description = substr($matches[0][$k], $contLoc + 8);
					$char = substr($description, 0, 1);
					$charPos = strpos($description, $char, 1);
					$description = substr($description, 1, $charPos - 1);
					$page_description = $description;
				}
				if (strtolower($v) == 'keywords') {
					$contLoc = strpos($matches[0][$k], "content=");
					$keywords = substr($matches[0][$k], $contLoc + 8);
					$char = substr($keywords, 0, 1);
					$charPos = strpos($keywords, $char, 1);
					$keywords = substr($keywords, 1, $charPos - 1);
					$page_keywords = $keywords;
				}
			}
		}
		if ($pageTitle != "") {
			$template = $this->setTemplate($template,"page_title",$pageTitle);
		} else {
			$page_title = "Accommodation in Cape Town, Garden Route, Mozambique & Victoria Falls – Book Travel";
		
			$reg = "@<title>+(.*?)</title>@i";
			preg_match_all($reg, $content, $matches);
			if (isset($matches[1][0]) && !empty($matches[1][0])) $page_title = $matches[1][0];
		
			$template = $this->setTemplate($template,"page_title",$page_title);
		}
		$template = $this->setTemplate($template,"page_description",$page_description);
		$template = $this->setTemplate($template,"page_keywords",$page_keywords);


        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

    }
	
	function getBreadCrumb($areaXml) {

        if (!empty($areaXml) && isset($areaXml->theme["safe_url"])) {
            $preUrl = "accommodation/" . $areaXml->theme["safe_url"];
            $preUrl .= "/" . $areaXml->region["safe_url"];
            $preUrl = trim($preUrl, "/");

            $loader = "aaLoading()";

            $breadCrumbs = "";

            if (isset($areaXml->region)) {
                $breadCrumbs .= $this->createBreadcrumb("/", "booktravel.travel");
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->theme));
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->region));
            }

            $preUrl .= "/" . $areaXml->country["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->country)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->country));
            }

            $preUrl .= "/" . $areaXml->province["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->province)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->province));
            }

            $preUrl .= "/" . $areaXml->province_region["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->province_region)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->province_region));
            }

            $preUrl .= "/" . $areaXml->sub_region["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->sub_region)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->sub_region));
            }

            $preUrl .= "/" . $areaXml->town["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->town)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->town));
            }
            $preUrl .= "/" . $areaXml->town_region["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->town_region)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->town_region));
            }
            $preUrl .= "/" . $areaXml->suburb["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->suburb)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->suburb));
            }
            $preUrl .= "/" . $areaXml->establishment['code'] . "_" . $areaXml->establishment["safe_url"];
            $preUrl = trim($preUrl, "/");
            if (isset($areaXml->establishment)) {
                $breadCrumbs .= $this->createBreadcrumb("/$preUrl", urldecode($areaXml->establishment));
            }

            //Log Function Stats
            // $search_string = __FILE__ . "|" . __FUNCTION__;
            // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

            return trim($breadCrumbs, " &#187; ");
        } else {


            //Log Function Stats
            // $search_string = __FILE__ . "|" . __FUNCTION__;
            // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

            return '';
        }


    }
	
	function createBreadcrumb($url,$title) {

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return "<div style='display:inline;' itemscope itemtype='http://data-vocabulary.org/Breadcrumb'><a href='$url' itemprop='url'><span itemprop='title'>$title</span></a></div> &#187; ";
	}

    /**
     * @param $establishment
     * @param $areaXml
     * @return mixed|string
     *
     * @deprecated since 2016-03-01
     */
	function getEstablishment($establishment,$areaXml) {
		global $pageParam;
		global $api;
		global $template;
		global $showEstab;
		global $pageTitle;

		$establishment_template = "";
		
		$rightContent = "";
			
			$establishment_code = $establishment['code'];
            $qa_block = '';

			$api->setMethod("fetch");
			$api->setType("establishment");
			$api->addParam("code",$establishment['code']);
			$establishmentXml = $api->getApiXML();
			
			if (isset($establishmentXml->establishment)) {
				$showEstab = true;
				if ($establishmentXml->establishment == "false") {
					$showEstab = false;
					header("HTTP/1.0 404 Not Found");
					$establishment_template = "<div id='EstHolder'><h1>404 - Establishment not found</h1><br /><br /><br />Sorry but the establishment you are looking for does not exist in our database<div style='margin:50px;' align='center'><strong><a href='http://booktravel.travel/'>Back to Home Page</a></strong></div></div>";
				} else {
					$establishment_template = file_get_contents("templates/establishment.html");
					
					//Set Template Est Location
						
						$suburb = urldecode($establishmentXml->establishment->suburb);
						if (!empty($suburb)) $location = "$suburb, ";
						$town = urldecode($establishmentXml->establishment->town);
						$location .= "$town, ";
						$province = urldecode($establishmentXml->establishment->province);
						$country = urldecode($establishmentXml->establishment->country);
						if (!empty($province)) {
							$location .= "$province, $country";
						} else {
							$location .= " $country";
						}
						
						//echo $establishmentXml->establishment->locations->country;
						
						$locationLinks = "";
						$locationLinksUrl = "/accommodation/".$areaXml->theme['safe_url']."/".urldecode($establishmentXml->establishment->locations->country_region["safe_url"]);
						if (isset($establishmentXml->establishment->locations->country)) {
							$locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->country["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->country)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->province)) {
							$locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->province["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->province)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->province_region)) {
							$locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->province_region["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->province_region)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->sub_region)) {
							$locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->sub_region["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->sub_region)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->town)) {
							$locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->town["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->town)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->town_region)) {
							$locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->town_region["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->town_region)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->suburb)) {
							$locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->suburb["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->suburb)."</a>, ".$locationLinks;
						}
						$locationLinks = trim($locationLinks,", ");
						
						$newLink = $locationLinksUrl."/".$establishmentXml->establishment["code"]."_".urldecode($establishmentXml->establishment->link_name);
						//$newLink = "/accommodation/".$establishmentXml->establishment["code"]."_".urldecode($establishmentXml->establishment->link_name);
						$actualLink = "/".$_GET['page'];
						//echo $page_param[1];
						
						//echo "$newLink<br />$actualLink";
						$thankyou = strpos($_GET['page'],"/thank/");
						if ($newLink != $actualLink) {
							header('HTTP/1.1 301 Moved Permanently');
							if ($thankyou > 1) {
								$newLink .= "#EstablishmentTabQuote_tabthank";
							}
							header("Location: $newLink");
							exit;
						}
						
						$establishment_template = $this->setTemplate($establishment_template,"location",$locationLinks);
						
						//Establishment Log
					
						$log_params = $establishment['code'];
						$log_url = $_SERVER["REQUEST_URI"];
						$log_type = "e";
						$log_ip = $_SERVER["REMOTE_ADDR"];
						$this->webLog($log_params,$log_url,$log_type,$log_ip);
					
					
					$pageHeader = "";
					$headerCount = 0;
					if (isset($areaXml->suburb)) {
						$pageHeader .= ", ".urldecode($areaXml->suburb);
						$headerCount++;
					}
					if (isset($areaXml->town_region)) {
						$pageHeader .= ", ".urldecode($areaXml->town_region);
						$headerCount++;
					}
					if (isset($areaXml->town)) {
						$pageHeader .= ", ".urldecode($areaXml->town);
						$headerCount++;
					}
					if (isset($areaXml->sub_region)) {
							$pageHeader .= ", ".urldecode($areaXml->sub_region);
							$headerCount++;
					}
					if (isset($areaXml->province_region)) {
							$pageHeader .= ", ".urldecode($areaXml->province_region);
							$headerCount++;
					}
					if (isset($areaXml->province)) {
						if ($headerCount < 5) {
							$pageHeader .= ", ".urldecode($areaXml->province);
							$headerCount++;
						}
					}
					if (isset($areaXml->country)) {
						if ($headerCount < 5) {
							$pageHeader .= ", ".urldecode($areaXml->country);
							$headerCount++;
						}
					}
					if (isset($areaXml->region)) {
						if ($headerCount < 5) {
							$pageHeader .= ", ".urldecode($areaXml->region);
							$headerCount++;
						}
					}
					$pageHeader = trim($pageHeader,", ");
					
					
					//Set Template Est Name
						$establishment_name = urldecode($establishmentXml->establishment->establishment_name);
						$rdclass = $this->getProvinceClass($areaXml);
						$establishment_template = $this->setTemplate($establishment_template,"location_title","<h1 class='$rdclass'>".$pageHeader."</h1>");
						$establishment_template = $this->setTemplate($establishment_template,"establishment_name",$establishment_name);
						$pageTitle = $establishment_name." in ";
						if (isset($areaXml->suburb)) {
							$pageTitle .= $areaXml->suburb.", ";
						}
						$pageTitle .= $areaXml->town." - booktravel.travel";
					
					//Set Template QA Status
						$qa_rating = urldecode($establishmentXml->establishment->qa_status);
						if (isset($qa_rating) && !empty($qa_rating)) {
							$qa_block .= "<a style='border:0;background:0;float:none;' target='_blank' href='http://booktravel.travel/index.php?p=quality_assured'><img align='left' src='http://booktravel.travel/lookup_images/$qa_rating' width=97 height=148 /></a>";
						} else {
							$qa_block .= '';
						}
						//$qa_block .= '';
					
					//Set Template Est Restype
						$restype = urldecode($establishmentXml->establishment->restype_name);
						$qa_name = urldecode($establishmentXml->establishment->qa_name);
					
					//Set Template Est Descriptions
						$short_description = urldecode($establishmentXml->establishment->short_description);
						if (!empty($establishmentXml->establishment->long_description)) {
							$description = urldecode($establishmentXml->establishment->long_description);
						} else {
							$description = urldecode($establishmentXml->establishment->short_description);
						}
						$description = trim($description,"<br />");
						$description = "<p><strong>$qa_name $restype</strong><br />$description</p>";
						if (!empty($establishmentXml->establishment->child_policy)) {
							$child_policy = urldecode($establishmentXml->establishment->child_policy);
							$description .= "<p><strong>Child Policy</strong><br />$child_policy</p>";
						}
						if (!empty($establishmentXml->establishment->cancellation_policy)) {
							$cancellation_policy = urldecode($establishmentXml->establishment->cancellation_policy);
							$description .= "<p><strong>Cancellation Policy</strong><br />$cancellation_policy</p>";
						}
						$establishment_template = $this->setTemplate($establishment_template,"description",$description);
						
					//Set Landmarks
						$landmarks = "";
						$isLandmark = false;
						foreach ($establishmentXml->establishment->landmarks->landmark AS $landmark) {
							$isLandmark = true;
							$landmarkFile = $this->contentFileFromName(urldecode($landmark->name));
							if (file_exists('content/'.$landmarkFile.".html")) {
								$landmarks .= "<li><a target='_blank' href='/index.php?p=".$landmarkFile."'>".urldecode($landmark->name)."</a>";
							} else {
								$landmarks .= "<li>".urldecode($landmark->name)."";
							}
							if ($landmark->distance != "0" && $landmark->distance != "") {
								$landmarks .= " (".urldecode($landmark->distance.$landmark->measurement).")";
							}
							$landmarks .= "</li>";
						}
						
					//Get Specials
						$specials = "";
						$bonsela = "";
						foreach ($establishmentXml->establishment->specials->special AS $special) {
							if (isset($special->start_date) && $special->bonsela == "0") { 
								$specials .= "<strong>From ".urldecode($special->start_date)." to ".urldecode($special->end_date)."</strong><br />".urldecode($special->description)."<br /><br />";
							}
							if (isset($special->start_date) && $special->bonsela == "1") { 
								$bonsela .= "<strong>From ".urldecode($special->start_date)." to ".urldecode($special->end_date)."</strong><br />".urldecode($special->description)."<br /><br />";
							}
						}
						if ($bonsela != "") {
							$establishment_template = $this->setTemplate($establishment_template,"bonsela","<p></p><div class='estSpecialBonsela'><div class='estHeading'>Bonsela Discount</div><a href='/bonsela'>More info about the Bonsela Discount Programme</a><br /><br />$bonsela</div>");
						}
						if ($specials != "") {
							$establishment_template = $this->setTemplate($establishment_template,"specials","<p></p><div class='estSpecial'><div class='estHeading'>Specials</div>$specials</div>");
						}
						
					//Set 360
						$link360 = "";
						$content360 = "";
						if ($establishmentXml->establishment->threesixtyid != "" && $establishmentXml->establishment->threesixtyserver > 0) {
							$link360 = "<br /><a href='javascript:openGuestReview(\"\");' rel='lightbox'><img src='/images/360.jpg' /></a>";
							if ($establishmentXml->establishment->threesixtyserver == 2) {
								$link = urldecode($establishmentXml->establishment->threesixtyid);
								$content360 = "<iframe scrolling='no' frameborder='0' style='height:350px;width:600px;' src='http://360-sa.co.za/vtours/".$link."'></iframe>";
							}
							if ($establishmentXml->establishment->threesixtyserver == 1) {
								$link = urldecode($establishmentXml->establishment->threesixtyid);
								$content360 = "<iframe scrolling='no' frameborder='0' style='height:350px;width:600px;' src='http://www.govisit.co.za/home/embed-360/?lookup=".$link."&snap=center&snapwidth=600&snapheight=400'></iframe>";
							}
						}
						$establishment_template = $this->setTemplate($establishment_template,"360_virtual_tour",$content360);
						//echo $establishmentXml->establishment->threesixtyid." ".$establishmentXml->establishment->threesixtyserver;
					//http://360-sa.co.za/vtours/wild-olive-guest-house/
					//http://www.govisit.co.za/home/embed-360/?lookup=320&snap=center&snapwidth=600&snapheight=400
						
					//Set Template Est Images
						$imageInfoGallery = "";
						$imageGallery = "<table><tr>";
						$priOver = "";
						if ($specials != "") {
							$priOver = "<img id='EstablishmentSpecialOverlay' src='/images/overlay_special.png' />";
						}
						if ($bonsela != "") {
							$priOver = "<img id='EstablishmentSpecialOverlay' src='/images/overlay_bonsela.png' />";
						}
						$i=0;
						foreach ($establishmentXml->establishment->images->image AS $imageXml) {
							if ($i<6) {
                                if (!isset($image_title)) $image_title = '';
                                if (!isset($image_description)) $image_description = '';
								$imageInfoGallery .= "<div class='establishmentImage'>";
								if($i==0){$imageInfoGallery .= $priOver;}
								$imageInfoGallery .= "<img style='cursor:pointer;' onclick='javascript:estabTabClicked(document.getElementById(\"EstablishmentTabGallery\"));' width='300' src='/res_images/".urldecode($imageXml->name)."' />";
								if($i==0){$imageInfoGallery .= "<div id='EstablishmentQAOverlay'>$qa_name Accommodation</div>";}
								$imageInfoGallery .= "</div><div class='establishmentImageDesc'>$image_title<br /><p>$image_description</p></div><br style='clear:both' />";
							}
							if ($i==0) {
								$imageInfoGallery .= $link360;
							}
							$i++;
						}
						$iii=0;
						$i=0;
						foreach ($establishmentXml->establishment->images->image AS $imageXml) {
							if ($iii%3 == 0) {
								$imageGallery .= "</tr><tr>";
							}
							$iii++;
							$imageGallery .= "<td valign='top'><div class='establishmentImage'>";
							if($i==0){$imageGallery .= $priOver;}
							$imageGallery .= "<img width='300' src='/res_images/". (urldecode($imageXml->name)) ."' />";
							if($i==0){$imageGallery .= "<div id='EstablishmentQAOverlay'>$qa_name Accommodation</div>";}
							$imageGallery .= "</div><div class='establishmentImageDesc'>$image_title<br /><p>$image_description</p></div><br style='clear:both' /></td>";
							$i++;
						}
						$imageGallery .= "</table>";
						$establishment_template=str_replace("<!-- info_gallery -->",$imageInfoGallery,$establishment_template);
						$establishment_template=str_replace("<!-- image_gallery -->",$imageGallery,$establishment_template);
						
					//Set Template Est Contact Data
						$establishment_tel = "";
						$establishment_fax = "";
						$establishment_cell = "";
						
						$contact_details = "";
						$contact_address = "";
						$contact_gps = "";
						
						$contact_tel = urldecode($establishmentXml->establishment->contact_tel);
						$base_tel = urldecode($establishmentXml->establishment->general_number);
						$reservation_tel = urldecode($establishmentXml->establishment->reservation_tel);
						$contact_fax = urldecode($establishmentXml->establishment->contact_fax);
						$reservation_fax = urldecode($establishmentXml->establishment->reservation_fax);
						$reservation_cell = urldecode($establishmentXml->establishment->reservation_cell);
						$reservation_email = urldecode($establishmentXml->establishment->reservation_email);
						
						$establishment_tel = !empty($contact_tel)?$contact_tel:$establishment_tel;
						$establishment_tel = !empty($base_tel)?$base_tel:$establishment_tel;
						$establishment_tel = !empty($reservation_tel)?$reservation_tel:$establishment_tel;
						$establishment_fax = !empty($contact_fax)?$contact_fax:$establishment_fax;
						
						if (substr($establishment_tel, 0, 4) == '&fax') {
							$establishment_fax = substr($establishment_tel, 5);
							$establishment_tel = substr($establishment_tel, 5);
						}
						$establishment_fax = !empty($reservation_fax)?$reservation_fax:$establishment_fax;
						$establishment_cell = !empty($contact_cell)?$contact_cell:$establishment_cell;
						$establishment_cell = !empty($reservation_cell)?$reservation_cell:$establishment_cell;
						if (substr($establishment_tel, -1) == ',' || substr($establishment_tel, -1) == ';') $establishment_tel = substr($establishment_tel, 0, -1);
						if (substr($establishment_fax, -1) == ',' || substr($establishment_fax, -1) == ';') $establishment_fax = substr($establishment_fax, 0, -1);
						if (substr($establishment_cell, -1) == ',' || substr($establishment_cell, -1) == ';') $establishment_cell = substr($establishment_cell, 0, -1);
						
						if (!isset($establishmentXml->establishment->contacts->contact)) {
							if (!empty($establishment_tel)) $contact_details .= "<li><strong>Tel No</strong>: $establishment_tel<br />";
							if (!empty($establishment_cell)) $contact_details .= "<li><strong>Cell No</strong>: $establishment_cell<br />";
							if (!empty($establishment_fax)) $contact_details .= "<li><strong>Fax No</strong>: $establishment_fax<br />";
						} else {
							foreach ($establishmentXml->establishment->contacts->contact AS $contactXml) {
								$contact_type = urldecode($contactXml->contact_type);
								$contact_value = urldecode($contactXml->contact_value);
								$contact_description = urldecode($contactXml->contact_description);
								switch ($contact_type) {
									case 'tel1':
										$tel1_block = "<strong>Tel</strong>: ";
										if (!empty($contact_description)) $tel1_block .= "$contact_description - ";
										$tel1_block .= "$contact_value<br />";
										break;
									case 'tel2':
										$tel1_block = "<strong>Tel 2</strong>: ";
										if (!empty($contact_description)) $tel2_block .= "$contact_description - ";
										$tel2_block .= "$contact_value<br />";
										break;
									case 'cell1':
										$cell1_block = "<strong>Cell</strong>: ";
										if (!empty($contact_description)) $cell1_block .= "$contact_description - ";
										$cell1_block .= "$contact_value<br />";
										break;
									case 'cell2':
										$cell2_block = "<strong>Cell 2</strong>: ";
										if (!empty($contact_description)) $cell2_block .= "$contact_description - ";
										$cell2_block .= "$contact_value<br />";
										break;
									case 'fax1':
										$fax1_block = "<strong>Fax</strong>: ";
										if (!empty($contact_description)) $fax1_block .= "$contact_description - ";
										$fax1_block .= "$contact_value<br />";
										break;
									case 'fax2':
										$fax2_block = "<strong>Fax 2</strong>: ";
										if (!empty($contact_description)) $fax2_block .= "$contact_description - ";
										$fax2_block .= "$contact_value<br />";
										break;
									case 'email':
										$reservation_email = $contact_value;
										break;
								}
							}
							
							if (!empty($tel1_block)) $contact_details .= "$tel1_block";
							if (!empty($tel2_block)) $contact_details .= "$tel2_block";
							if (!empty($cell1_block)) $contact_details .= "$cell1_block";
							if (!empty($cell2_block)) $contact_details .= "$cell2_block";
							if (!empty($fax1_block)) $contact_details .= "$fax1_block";
							if (!empty($fax2_block)) $contact_details .= "$fax2_block";
						}
						
						if (!empty($establishmentXml->establishment->website_url)) {
							$website_url = urldecode($establishmentXml->establishment->website_url);
							if (substr($website_url, 0, 7) != 'http://') $website_url = "http://$website_url";
							$web_block = "<strong>Web</strong>: <a href='http://booktravel.travel/e/website.php?id=$establishment_code' target='_blank' rel='nofollow' >View Website</a><br />";
						}
						if (!empty($web_block)) $contact_details .= "$web_block";
						$contact_details .= "<strong>Web Code</strong>: ".$establishment['code'];
						
						$base_street1 = urldecode($establishmentXml->establishment->street_address_line1);
						$base_street2 = urldecode($establishmentXml->establishment->street_address_line2);
						$base_street3 = urldecode($establishmentXml->establishment->street_address_line3);
						if (!empty($base_street1) || !empty($base_street2) || !empty($base_street3)) {
							$contact_address .= "<strong>Physical Address: </strong><br />";
							if (!empty($base_street1)) $contact_address .= "$base_street1<br />";
							if (!empty($base_street2)) $contact_address .= "$base_street2<br />";
							if (!empty($base_street3)) $contact_address .= "$base_street3";
						}
						
						$reservation_postal1 = urldecode($establishmentXml->establishment->reservation_postal1);
						$reservation_postal2 = urldecode($establishmentXml->establishment->reservation_postal2);
						$reservation_postal3 = urldecode($establishmentXml->establishment->reservation_postal3);
						$reservation_postal_code = urldecode($establishmentXml->establishment->reservation_postal_code);
						
						$base_postal1 = urldecode($establishmentXml->establishment->postal_address_line1);
						$base_postal2 = urldecode($establishmentXml->establishment->postal_address_line2);
						$base_postal3 = urldecode($establishmentXml->establishment->postal_address_line3);
						$base_postalCode = urldecode($establishmentXml->establishment->postal_address_code);
						
						if (empty($reservation_postal1)) {
							if (!empty($base_postal1) || !empty($base_postal2) || !empty($base_postal3)) {
								$contact_address .= "<br /><strong>Postal Address: </strong><br />";
								if (!empty($base_postal1)) {
									if (substr($base_postal1, -1) == ',' || substr($base_postal1, -1) == ';') $base_postal1 = substr($base_postal1, 0, -1);
									$contact_address .= "$base_postal1<br>";
								}
								if (!empty($base_postal2)) {
									if (substr($base_postal2, -1) == ',' || substr($base_postal2, -1) == ';') $base_postal2 = substr($base_postal2, 0, -1);
									$contact_address .= "$base_postal2<br>";
								}
								if (!empty($base_postal3)) {
									if (substr($base_postal3, -1) == ',' || substr($base_postal3, -1) == ';') $base_postal3 = substr($base_postal3, 0, -1);
									$contact_address .= "$base_postal3<br>";
								}
								if (!empty($base_postalCode)) $contact_address .= "$base_postalCode<br>";
							}
						} else {
							if (!empty($reservation_postal1) || !empty($reservation_postal2) || !empty($reservation_postal3)) {
								$contact_address .= "<strong>Postal Address: </strong><br />";
								if (!empty($reservation_postal1)) {
									if (substr($reservation_postal1, -1) == ',' || substr($reservation_postal1, -1) == ';') $reservation_postal1 = substr($reservation_postal1, 0, -1);
									$contact_address .= "$reservation_postal1<br>";
								}
								if (!empty($reservation_postal2)) {
									if (substr($reservation_postal2, -1) == ',' || substr($reservation_postal2, -1) == ';') $reservation_postal2 = substr($reservation_postal2, 0, -1);
									$contact_address .= "$reservation_postal2<br>";
								}
								if (!empty($reservation_postal3)) {
									if (substr($reservation_postal3, -1) == ',' || substr($reservation_postal3, -1) == ';') $reservation_postal3 = substr($reservation_postal3, 0, -1);
									$contact_address .= "$reservation_postal3<br>";
								}
								if (!empty($reservation_postal_code)) $contact_address .= "$reservation_postal_code<br>";
								$contact_address .= "</td></tr>";
							}
						}
						
						$gps_latitude = urldecode($establishmentXml->establishment->gps_latitude);
						$gps_longitude = urldecode($establishmentXml->establishment->gps_longitude);
						if (empty($gps_latitude)) {
							$gps = '';
							$gps_coord = '';
						} else {
							$contact_address .= "<strong>GPS: </strong><br />";
							$gps_latitude = stripslashes($gps_latitude);
							$gps_longitude = stripslashes($gps_longitude);
							$gps_latitude = str_replace('<34', '"', $gps_latitude);
							$gps_longitude = str_replace('<35', '"', $gps_longitude);
							$contact_address .= "Latitude: ".round($gps_latitude, 6)."<br />Longitude: ".round($gps_longitude, 6);
							//echo $gps_latitude;
							$gps_coord = "GPS Co-ordinates for ".$establishment_name." : ".round($gps_latitude, 6).", ".round($gps_longitude, 6);
						}
						
						$room_count = urldecode($establishmentXml->establishment->room_count);
						
					//Set Template Est Booking Data
						$contact_enquire = "";
						$prebook_id = urldecode($establishmentXml->establishment->prebook_id);
						$nightsbridge_id = urldecode($establishmentXml->establishment->nightsbridge_bbid);
						$quote_tab = "javascript:estabTabClicked(this)";
						$hide_quote_block = "display:block;";
						if ($nightsbridge_id > 0) {
							$nightsbridge_button = "<br /><div align='center'><a style='cursor:pointer' target='_blank' href='https://www.nightsbridge.co.za/bridge/Search?bbid=$nightsbridge_id&nbid=16'><input type='button' class='EstablishmentButton' value='Check Availability' /></a></div><br />";
							//$quote_tab = "javascript:document.location='https://www.nightsbridge.co.za/bridge/Search?bbid=$nightsbridge_id&nbid=16';";
							$hide_quote_block = "display:none;";
						}
						if (!empty($prebook_id)) $booking_link = "";
						if (!empty($nightsbridge_id)) {
							$contact_enquire = "<br /><br /><a class='emButton' target='_blank' href='https://www.nightsbridge.co.za/bridge/Search?bbid=$nightsbridge_id&nbid=16'>Check Availability</a>";
							$contact_enquire .= "";
							$contact_enquire .= "";
						}
						
						$establishment_template = str_replace('<!-- code -->', $establishment_code, $establishment_template);
						$establishment_template = str_replace('<!-- nightsbridge_button -->', $nightsbridge_button, $establishment_template);
						$establishment_template = str_replace('<!-- request_quote_tab -->', $quote_tab, $establishment_template);
						$establishment_template = str_replace('<!-- hide_quote_block -->', $hide_quote_block, $establishment_template);
						
					//Set Template Est Icons
						$icon_counter = 0;
						$facilities = "";
						foreach ($establishmentXml->establishment->icons->icon AS $iconXml) {
							$icon_name = urldecode($iconXml->name);
							$icon_url = urldecode($iconXml->url);
							$icon_description = urldecode($iconXml->description);
							//if ($icon_name == 'PFOLIO') continue;
							$facilities .= "<img width='33' height='31' src='$icon_url' title='$icon_description'>";
							$icon_counter++;
							if ($icon_counter == 12){$facilities .= "<br>";$icon_counter = 0;}
						}
						$establishment_template = str_replace('<!-- facilities -->', $facilities, $establishment_template);
						
					//Set Template Est directions
						$direction_block = "";
						$direction = "";
						$direction = urldecode($establishmentXml->establishment->directions->directions);
						$map_type = urldecode($establishmentXml->establishment->directions->map_type);
						$image_name = urldecode($establishmentXml->establishment->directions->image_name);
						
						//echo $map_type;
						
						if (!empty($direction) || $map_type == 'google' || $gps_coord || $map_type == "upload") {
							//$map_type = 'google';
							$direction = nl2br(stripslashes($direction));
							$map_file = strtolower($establishment_code) . '.jpg';
							if ($gps_coord != "" && empty($map_type)) {
								$map_type = "google";
							} 
							if (!empty($map_type) && $map_type == 'upload') {$direction_block .= "<table align=right><tr valign=top><td><a href='/res_maps/$image_name' target='_blank' ><img src='/res_maps/TN_$map_file' border=0 /></a></td></tr></table>";}
							//echo $map_type.$direction_block;
							if (!empty($map_type) && $map_type == 'google')  {
								
								
								if (empty($gps_latitude)) {
									//Get Geocode
									$delay = 0;
									$geocode_pending = true;
									
									$address = '';
									if (!empty($base_street1)) $address .= "$base_street1,";
									if (!empty($base_street2)) $address .= "$base_street2,";
									if (!empty($base_street3)) $address .= "$base_street3,";
									$address = substr($address, 0, -1);
									
									$map_url = "http://maps.google.com/maps/geo?output=xml&key=" . $GLOBALS['api_key'] . "&q=" . urlencode($address);
								
									while ($geocode_pending) {
										$xml = simplexml_load_file($map_url);
										
										$status = $xml->Response->Status->code;
										if (strcmp($status, "200") == 0) {
											$geocode_pending = false;
										    $coordinates = $xml->Response->Placemark->Point->coordinates;
										    $coordinatesSplit = split(",", $coordinates);
										    // Format: Longitude, Latitude, Altitude
										    $lat = $coordinatesSplit[1];
										    $lng = $coordinatesSplit[0];
											
											if (str_word_count($short_description) > 50) {
												$words = str_word_count($short_description, 1);
												$words = array_slice($words, 0, 50);
												$map_description = implode(' ', $words);
												$map_description .= '...';
											} else {
												$map_description = $short_description;
											}
										    
											$map_description = str_replace(array("\r\n","\r","\n"), '', $map_description);
											$map_description = trim($map_description);
											$map_description = str_replace("'", '&apos;', $map_description);
										    $direction_block .= "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key={$GLOBALS['api_key']}&sensor=false' type='text/javascript'></script>";
										    $direction_block .= "<table width=100%><tr valign=top><td>";
										    $direction_block .= "<script type='text/javascript'>
																    function initialize() {
																      if (GBrowserIsCompatible()) {
																        var map = new GMap2(document.getElementById('map_canvas'));
																        map.setCenter(new GLatLng($lat, $lng), 13);
																        map.setUIToDefault();
																        map.openInfoWindowHtml(map.getCenter(),
					                  										'<table width=300px ><tr><td><span style=\"font-family: arial; font-size:10pt; font-weight: bold\">".addslashes($establishment_name)."</span><br><table align=left><tr><td>";
								    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/res_images/$map_image")) $direction_block .= "<img src=/res_images/$map_image style=\"border: 1px solid black\"/>";
								    $direction_block .= "</td></tr></table><span style=\"font-family: arial; font-size: 8pt\" >$map_description</span></td></tr></table>');
									
																      }
																    }
																
																    </script>";
										    $direction_block .= "<div id='map_canvas'></div></td></tr></table>";
											$direction_block .= "<script>initialize()</script>";
										} else if (strcmp($status, "620") == 0) {
										    $delay += 100000;
										} else {
											$geocode_pending = false;
										}
										usleep($delay);
									}
								} else {
									
									if (str_word_count($short_description) > 50) {
										$words = str_word_count($short_description, 1);
										$words = array_slice($words, 0, 50);
										$map_description = implode(' ', $words);
										$map_description .= '...';
									} else {
										$map_description = $short_description;
									}
									
									$map_description = str_replace(array("\r\n","\r","\n"), '', $map_description);
									$map_description = trim($map_description);
									$map_description = str_replace("'", '&apos;', $map_description);
									$direction_block .= "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key={$GLOBALS['api_key']}&sensor=false' type='text/javascript'></script>";
								    $direction_block .= "<table width=100%><tr valign=top><td>";
								    $direction_block .= "<script type='text/javascript'>
														    function initialize() {
														      if (GBrowserIsCompatible()) {
														        var map = new GMap2(document.getElementById('map_canvas'));
														        map.setCenter(new GLatLng($gps_latitude, $gps_longitude), 13);
														        map.setUIToDefault();
														        map.openInfoWindowHtml(map.getCenter(),
				                  									'<table width=300px ><tr><td><span style=\"font-family: arial; font-size:10pt; font-weight: bold\">".addslashes($establishment_name)."</span><br><table align=left><tr><td>";
								    if (!isset($map_image)) $map_image = '';
                                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/res_images/$map_image")) $direction_block .= "<img src=/res_images/$map_image style=\"border: 1px solid black\"/>";
								    $direction_block .= "</td></tr></table><span style=\"font-family: arial; font-size: 8pt\" >$map_description</span></td></tr></table>');
														      }
														    }
														
														    </script>";
								    $direction_block .= "$gps_coord<div id='map_canvas'></div></td></tr></table>";
									$direction_block .= "<script>initialize()</script>";
								}
							}
							if (trim($direction) != "") {
								$direction_block .= "<p>$direction</p>";
							}
						}
						
						$establishment_template = str_replace('<!-- directions -->', $direction_block, $establishment_template);
						
					//Set Template Est Price
						$price_low = "";
						$price_high = "";
						$price_quote = "";
						$price_quote_block = "";
						$price_category_suffix = $establishmentXml->establishment->price_category_suffix;
						if ($establishmentXml->establishment->price_in_low > 0) {
							$price_low = round($establishmentXml->establishment->price_in_low);
						}
						if ($establishmentXml->establishment->price_out_low > 0) {
							$price_low = round($establishmentXml->establishment->price_out_low);
						}
						if ($establishmentXml->establishment->price_out_high > 0) {
							$price_high = round($establishmentXml->establishment->price_out_high);
						}
						if ($establishmentXml->establishment->price_in_high > 0) {
							$price_high = round($establishmentXml->establishment->price_in_high);
						}
						if ($price_low > 0 && $price_high > 0) {
							$price_quote = "From: <strong class='nbelm'>R$price_low - R$price_high</strong> <span class='nbelm'>$price_category_suffix</span>";
							$price_quote_block .= $price_quote;
						}
						//if ($price_quote == "") {
							$price_quote = "".$establishmentXml->establishment->price."";
						//}
						
						$establishment_template = str_replace('<!-- price_quote -->',nl2br(urldecode($price_quote_block)),$establishment_template);
						$establishment_template = str_replace('<!-- price_form -->',nl2br(urldecode($price_quote_block)),$establishment_template);
						$establishment_template = str_replace('<!-- rates -->',nl2br(urldecode($price_quote)),$establishment_template);
						$establishment_template = str_replace('<!-- pricing -->',nl2br(urldecode($establishmentXml->establishment->price)),$establishment_template);
						
					//Set Template Awards
						$awards = "";
						foreach ($establishmentXml->establishment->awards->award as $awardXml) {
							$award_status = urldecode($awardXml->status);
							$award_name = urldecode($awardXml->category_description);
							
							if ($awardXml->year == "2012") {
								if (!empty($award_status)) {
									$awards .= "<table cellpadding=0 cellpacing=0 width=100%><tr><td align=center>";
									$awards .= "<img width='165' src='http://booktravel.travel/lookup_images/award_{$award_status}_2012.jpg' />";
									$awards .= "</td></tr></table><p>";
								}
							}
							if ($awardXml->year == "2011") {
								if (!empty($award_status)) {
									$awards .= "<table cellpadding=0 cellpacing=0 width=100%><tr><td align=center>";
									$awards .= "<img width='165' src='http://booktravel.travel/lookup_images/award_{$award_status}_2011.jpg' />";
									$awards .= "</td></tr></table><p>";
								}
							}

						}
						
						//$establishment_template = str_replace('<!-- awards -->', $awards, $establishment_template);
						
					//Set Template User Rating
						$user_rating = urldecode($establishmentXml->establishment->user_rating);
						$smilies = "";
						$comments = "";
						$comment = trim(urldecode($establishmentXml->establishment->visitor_comment));
						
						if (!empty($user_rating) || $comment != "") {
						    switch ($user_rating) {
						        case -1:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/shame.png'>";
						            break;
						        case 1:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/sad.png'>";
						            break;
						        case 2:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/hmm.png'><img align='absmiddle' src='http://booktravel.travel/images/hmm.png'>";
						            break;
						        case 3:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'>";
						            break;
						        case 4:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'>";
						            break;
						        case 5:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'>";
						            break;
						    }
						    
					        $visitor_name = trim(urldecode($establishmentXml->establishment->visitor_name));
					        
					        $town_name = trim(urldecode($establishmentXml->establishment->visitor_location));
					        
					        $comments .= "'$comment'<span style='font-size: 8pt'> ";
					        if (!empty($visitor_name) && $town_name != ',') $comments .= " - ";
					        if (!empty($visitor_name)) $comments .= "$visitor_name";
					        if (($town_name) != ',') {
					            if (substr($town_name, 0, 1) == ',') $town_name = substr($town_name, 1);
					            $comments .= " in $town_name";
					        }
					        $comments .= "</span>";
						    $estab_link = str_replace(' ', '_', $establishment_name);
						    $read_more = '<div style="padding-bottom:3px;" align="center"><input onclick=\'estabTabClicked(document.getElementById("EstablishmentTabReviews"));\' class="EstablishmentButton" type=\'button\' value="Read More Reviews" /></div>';
						} else {
						    $smilies = "<span style='font-family: arial; font-size: 10pt'>This establishment has not yet been reviewed.</span>";
						    $read_more = '';
						    $comments = '';
						}
						
						$star_text = array(1=>'<img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />');
						
						$star_rating = '';
						$star_grading = '';
						$star_grading = urldecode($establishmentXml->establishment->star_grading);
						if (!empty($star_grading)) $star_rating = $star_text[$star_grading] . ' (TGCSA)';
						
						$establishment_template = str_replace('<!-- rating -->', $smilies, $establishment_template);
						$establishment_template = str_replace('<!-- comment -->', substr($comments,0,120), $establishment_template);
						$establishment_template = str_replace('<!-- read_more -->', $read_more , $establishment_template);
						$establishment_template = str_replace('<!-- star_rating -->', $star_rating , $establishment_template);
						
						//$rightContent .= "<br />".$contact_enquire."<br style='clear:both;' /><br /><a class='emButton' style='padding-bottom:7px;' target='_blank' href='http://booktravel.travel/index.php?p=enquiry&c=$establishment_code'>Email Enquiry</a><br style='clear:both;' />";
						$rightContent .= "";
						if ($comments != "") {
							$rightContent .= "<br /><div align='center'>$smilies</div><div style='padding-left:2px;'>".substr($comment,0,128)."...<br /><em>$visitor_name</em><br />$read_more</div>";
							
						}
						$rightContent .= '<div align=\'center\'><input onclick=\'document.location="/guest_review/'.$establishment_code.'"\' class="EstablishmentButton" type=\'button\' value="Add a Guest Review" /></div>';
						$rightContent .= '<div class="linkBlock linkBlockEst" style="text-align=center;"><ul>';
						//$rightContent .= ;
						$rightContent .= "<div align='center'>".$star_rating."</div>";
						$rightContent .= '</ul></div>';
						$rightContent .= '<h1>Contact Details</h1><div class="linkBlock linkBlockEst" align="left"><ul>';
						$rightContent .= $contact_details;
						$rightContent .= '</ul></div><h1>Address</h1><div class="linkBlock linkBlockEst" align="left"><ul>';
						$rightContent .= $contact_address."</ul></div>";
						$rightContent .= $awards;
						
						if ($isLandmark) {
							$landmarks = '<h1>Nearby Points of Interest</h1><div class="linkBlock linkBlockEst" align="left"><ul>'.$landmarks.'</ul></div>';
						} else {
							$landmarks = '';
						}
						
						
						$rightContent .= $landmarks;
						
						$establishment_template = str_replace('<!-- right_content -->', $rightContent, $establishment_template);
						$template = str_replace('<!-- right_links -->', $rightContent , $template);


					//Guest Reviews
						$establishment_template = str_replace('<!-- smilies -->', $smilies, $establishment_template);
						$reviews = "";
						foreach ($establishmentXml->establishment->reviews->review as $reviewXML) {
							if (urldecode($reviewXML->visitor_comment) != "") {
								$reviews .= "<div align='center' style='padding:10px;text-align:justify;' class='EstablishmentBlueBlock'>";
								$reviews .= '"'.urldecode($reviewXML->visitor_comment).'"';
								$reviews .= "</div><div align='right'><div style='float:left'>".urldecode($reviewXML->date)."</div><strong>Review from ".urldecode($reviewXML->firstname)." ".urldecode($reviewXML->surname)." in ".urldecode($reviewXML->country)."</strong></div><br /><br />";
							}
						}
						$establishment_template = str_replace('<!-- reviews -->', $reviews, $establishment_template);
						$establishment_template = str_replace('<!-- establishment_code -->', $establishment_code, $establishment_template);
                        
                        $publickey = "6LfZCeMSAAAAADAe_nC8kx-QgLTOVmm9lB4o6_cD";
                    	$privatekey = "6LfZCeMSAAAAACcBTQVH6eS9F0IDfcl1J4EOWXhY";
                    	require_once('recaptcha/recaptchalib.php');
                        if (!isset($error)) $error = '';
                    	$captcha_html = recaptcha_get_html($publickey, $error);
                    	$establishment_template = str_replace("<!-- captcha -->",$captcha_html,$establishment_template);


				}
			}


        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return $establishment_template;
	}

    /**
     * Get and draw establishment - version 2
     *
     * @param $establishment
     * @param $areaXml
     * @internal param $xmlArea
     * @return string
     */
    function getEstablishment_v2($establishment,$areaXml) {

        $server = "www.booktravel.travel";
        require_once($_SERVER['DOCUMENT_ROOT'] . '/v2/settings/vars.php');

        //Vars
        global $api;
        global $pageTitle;
        $establishmentTemplate = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/templates/establishment.html");
        $nightsbridge_button = '';
		$web_block = '';

        //Fetch establishment details
        $api->setMethod("fetch");
        $api->setType("establishment");
        $api->addParam("code",$establishment['code']);
        $establishmentXml = $api->getApiXML();
//		echo "<pre>"; var_dump($establishmentXml); echo '</pre>';

        //TODO: Establishment not found

        //Establishment Code
        $establishmentTemplate = str_replace('<!-- establishment_code -->', $establishment['code'], $establishmentTemplate );

        //Establishment title (<H1>)
        $establishment_name = urldecode($establishmentXml->establishment->establishment_name);
        $establishmentTemplate = str_replace('<!-- establishment_title -->', $establishment_name, $establishmentTemplate );
        $establishmentTemplate = str_replace('<!-- establishment_name -->', $establishment_name, $establishmentTemplate );

        //Page Title
//		echo "<pre>"; var_dump($areaXml); echo '</pre>';
        $pageTitle = $establishment_name." in ";
		if (isset($establishmentXml->establishment->suburb) && !empty($establishmentXml->establishment->suburb)) $pageTitle .=  urldecode($establishmentXml->establishment->suburb) . ', ';
		$pageTitle .= urldecode($areaXml->town);

        //Establishment Location
        $locationLinks = "";
        $locationLinksUrl = "/accommodation/".$areaXml->theme['safe_url']."/".urldecode($establishmentXml->establishment->locations->country_region["safe_url"]);
        if (isset($establishmentXml->establishment->locations->country)) {
            $locationLinksUrl .= "/".$establishmentXml->establishment->locations->country["safe_url"];
            $locationLinks = " &raquo; <a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->country)."</a> ".$locationLinks;
        }
        if (isset($establishmentXml->establishment->locations->province)) {
            $locationLinksUrl .= "/".$establishmentXml->establishment->locations->province["safe_url"];
            $locationLinks = " &raquo; <a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->province)."</a> ".$locationLinks;
        }
        if (isset($establishmentXml->establishment->locations->province_region)) {
            $locationLinksUrl .= "/".$establishmentXml->establishment->locations->province_region["safe_url"];
            $locationLinks = " &raquo; <a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->province_region)."</a>".$locationLinks;
        }
        if (isset($establishmentXml->establishment->locations->sub_region)) {
            $locationLinksUrl .= "/".$establishmentXml->establishment->locations->sub_region["safe_url"];
            $locationLinks = " &raquo; <a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->sub_region)."</a> ".$locationLinks;
        }
        if (isset($establishmentXml->establishment->locations->town)) {
            $locationLinksUrl .= "/".$establishmentXml->establishment->locations->town["safe_url"];
            $locationLinks = " &raquo; <a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->town)."</a>".$locationLinks;
        }
        if (isset($establishmentXml->establishment->locations->town_region)) {
            $locationLinksUrl .= "/".$establishmentXml->establishment->locations->town_region["safe_url"];
            $locationLinks = " &raquo; <a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->town_region)."</a>".$locationLinks;
        }
        if (isset($establishmentXml->establishment->locations->suburb)) {
            $locationLinksUrl .= "/".urldecode($establishmentXml->establishment->locations->suburb["safe_url"]);
            $locationLinks = " <a href='$locationLinksUrl'>".urldecode($establishmentXml->establishment->locations->suburb)."</a>".$locationLinks;
        }
        $locationLinks = trim($locationLinks,", ");
        $locationLinks = trim($locationLinks,"&raquo;");
        $establishmentTemplate = str_replace('<!-- establishment_location -->', $locationLinks, $establishmentTemplate );


        //Establishment Restype
        $restype = urldecode($establishmentXml->establishment->restype_name);

        //Establishment Description
        if (!empty($establishmentXml->establishment->long_description)) {
            $description = urldecode($establishmentXml->establishment->long_description);
        } else {
            $description = urldecode($establishmentXml->establishment->short_description);
        }
        $description = trim($description,"<br />");
        $description = "<p><strong>$restype</strong><br />$description</p>";
        if (!empty($establishmentXml->establishment->child_policy)) {
            $child_policy = urldecode($establishmentXml->establishment->child_policy);
            $description .= "<p><strong>Child Policy</strong><br />$child_policy</p>";
        }
        if (!empty($establishmentXml->establishment->cancellation_policy)) {
            $cancellation_policy = urldecode($establishmentXml->establishment->cancellation_policy);
            $description .= "<p><strong>Cancellation Policy</strong><br />$cancellation_policy</p>";
        }
        $establishmentTemplate = str_replace('<!-- establishment_description -->', $description, $establishmentTemplate );


        //Establishment Facilities
        $facilities = "";
        foreach ($establishmentXml->establishment->icons->icon as $iconXml) {
            $icon_url = urldecode($iconXml->url);
            $icon_description = urldecode($iconXml->description);
            $facilities .= "<img width='33' height='31' src='$icon_url' title='$icon_description'>";
        }
        $establishmentTemplate = str_replace('<!-- establishment_facilities -->', $facilities, $establishmentTemplate);

        //Info block images
        $infoGallery = '';
        foreach ($establishmentXml->establishment->images->image as $imageXml) {
            if ($imageXml->primary == '1') {
                $image_title = $imageXml->title;
                $image_description = $imageXml->description;

                $infoGallery = "<img src='http://booktravel.travel/res_images/" . (urldecode($imageXml->name)) . "' title='$image_title' class='img-responsive infoGallery' onclick='launchGallery(this.src, this.title, \"$image_description\")'>";
            }

            if (empty($infoGallery)) {
                $image_title = $imageXml->title;
                $image_description = $imageXml->description;

                $infoGallery = "<img src='http://$server/res_images/" . (urldecode($imageXml->name)) . "' title='$image_title' class='img-responsive infoGallery' onclick='launchGallery(this.src, this.title, \"$image_description\")'>";
            }
        }
        $establishmentTemplate = str_replace('<!-- info_gallery -->', $infoGallery, $establishmentTemplate);

        //Star Grading
        $star_rating = '';
        for ($i = 0; $i < urldecode($establishmentXml->establishment->star_grading); $i++) {
            $star_rating .= '<img src="/images/gold_star.jpg" />';
        }
        if (!empty($star_rating)) $star_rating .= ' (TGCSA)';
        $establishmentTemplate = str_replace('<!-- star_rating -->', $star_rating, $establishmentTemplate);

        //Contact Details
        $establishment_tel = "";
        $establishment_fax = "";
        $establishment_cell = "";
        $contact_details = "";
        $contact_address = "";
        $contact_tel = urldecode($establishmentXml->establishment->contact_tel);
        $base_tel = urldecode($establishmentXml->establishment->general_number);
        $reservation_tel = urldecode($establishmentXml->establishment->reservation_tel);
        $contact_fax = urldecode($establishmentXml->establishment->contact_fax);
        $reservation_fax = urldecode($establishmentXml->establishment->reservation_fax);
        $reservation_cell = urldecode($establishmentXml->establishment->reservation_cell);
        $establishment_tel = !empty($contact_tel)?$contact_tel:$establishment_tel;
        $establishment_tel = !empty($base_tel)?$base_tel:$establishment_tel;
        $establishment_tel = !empty($reservation_tel)?$reservation_tel:$establishment_tel;
        $establishment_fax = !empty($contact_fax)?$contact_fax:$establishment_fax;
        if (substr($establishment_tel, 0, 4) == '&fax') {
            $establishment_fax = substr($establishment_tel, 5);
            $establishment_tel = substr($establishment_tel, 5);
        }
        $establishment_fax = !empty($reservation_fax)?$reservation_fax:$establishment_fax;
        $establishment_cell = !empty($contact_cell)?$contact_cell:$establishment_cell;
        $establishment_cell = !empty($reservation_cell)?$reservation_cell:$establishment_cell;
        if (substr($establishment_tel, -1) == ',' || substr($establishment_tel, -1) == ';') $establishment_tel = substr($establishment_tel, 0, -1);
        if (substr($establishment_fax, -1) == ',' || substr($establishment_fax, -1) == ';') $establishment_fax = substr($establishment_fax, 0, -1);
        if (substr($establishment_cell, -1) == ',' || substr($establishment_cell, -1) == ';') $establishment_cell = substr($establishment_cell, 0, -1);
        if (!isset($establishmentXml->establishment->contacts->contact)) {
            if (!empty($establishment_tel)) $contact_details .= "<li><strong>Tel No</strong>: $establishment_tel<br />";
            if (!empty($establishment_cell)) $contact_details .= "<li><strong>Cell No</strong>: $establishment_cell<br />";
            if (!empty($establishment_fax)) $contact_details .= "<li><strong>Fax No</strong>: $establishment_fax<br />";
        } else {
            foreach ($establishmentXml->establishment->contacts->contact AS $contactXml) {
                $contact_type = urldecode($contactXml->contact_type);
                $contact_value = urldecode($contactXml->contact_value);
                $contact_description = urldecode($contactXml->contact_description);
                switch ($contact_type) {
                    case 'tel1':
                        $tel1_block = "<div class='row'><div class='col-xs-3'><strong>Tel</strong>";
                        if (!empty($contact_description)) $tel1_block .= " - $contact_description";
                        $tel1_block .= ": </div>";
                        $tel1_block .= "<div class='col-xs-9'>$contact_value</div></div>";
                        break;
                    case 'tel2':
                        $tel2_block = "<div class='row'><div class='col-xs-3'><strong>Tel</strong>";
                        if (!empty($contact_description)) $tel2_block .= " - $contact_description";
                        $tel2_block .= ": </div>";
                        $tel2_block .= "<div class='col-xs-9'>$contact_value</div></div>";
                        break;
                    case 'cell1':
                        $cell1_block = "<div class='row'><div class='col-xs-3'><strong>Cell</strong>";
                        if (!empty($contact_description)) $cell1_block .= " - $contact_description";
                        $cell1_block .= ": </div>";
                        $cell1_block .= "<div class='col-xs-9'>$contact_value</div></div>";
                        break;
                    case 'cell2':
                        $cell2_block = "<div class='row'><div class='col-xs-3'><strong>Cell 2</strong>";
                        if (!empty($contact_description)) $cell2_block .= " - $contact_description";
                        $cell2_block .= ": </div>";
                        $cell2_block .= "<div class='col-xs-9'>$contact_value</div></div>";
                        break;
                    case 'fax1':
                        $fax1_block = "<div class='row'><div class='col-xs-3'><strong>Fax</strong>";
                        if (!empty($contact_description)) $fax1_block .= " - $contact_description";
                        $fax1_block .= ": </div>";
                        $fax1_block .= "<div class='col-xs-9'>$contact_value</div></div>";
                        break;
                    case 'fax2':
                        $fax2_block = "<div class='row'><div class='col-xs-3'><strong>Fax</strong>";
                        if (!empty($contact_description)) $fax2_block .= " - $contact_description";
                        $fax2_block .= ": </div>";
                        $fax2_block .= "<div class='col-xs-9'>$contact_value</div></div>";
                        break;
                    case 'email':
                        $reservation_email = $contact_value;
                        break;
                }
            }
            if (!empty($tel1_block)) $contact_details .= "$tel1_block";
            if (!empty($tel2_block)) $contact_details .= "$tel2_block";
            if (!empty($cell1_block)) $contact_details .= "$cell1_block";
            if (!empty($cell2_block)) $contact_details .= "$cell2_block";
            if (!empty($fax1_block)) $contact_details .= "$fax1_block";
            if (!empty($fax2_block)) $contact_details .= "$fax2_block";
            $contact_details = "$contact_details";
        }
        if (!empty($establishmentXml->establishment->website_url)) {
            $website_url = urldecode($establishmentXml->establishment->website_url);
            if (substr($website_url, 0, 7) != 'http://') $website_url = "http://$website_url";
            $web_block = "<div class='row'><div class='col-xs-3'><strong>Web</strong>:</div><div class='col-xs-9'><a href='http://booktravel.travel/e/website.php?id={$establishment['code']}' target='_blank' rel='nofollow' >View Website</a></div></div>";
        }
        $establishmentTemplate = str_replace('<!-- contact_details -->', $contact_details . $web_block, $establishmentTemplate);

        //Address Details
        $contact_address = "";
        $base_street1 = urldecode($establishmentXml->establishment->street_address_line1);
        $base_street2 = urldecode($establishmentXml->establishment->street_address_line2);
        $base_street3 = urldecode($establishmentXml->establishment->street_address_line3);
        if (!empty($base_street1) || !empty($base_street2) || !empty($base_street3)) {
            $contact_address .= "<strong>Physical Address: </strong><br />";
            if (!empty($base_street1)) $contact_address .= "$base_street1<br />";
            if (!empty($base_street2)) $contact_address .= "$base_street2<br />";
            if (!empty($base_street3)) $contact_address .= "$base_street3";
        }
        $reservation_postal1 = urldecode($establishmentXml->establishment->reservation_postal1);
        $reservation_postal2 = urldecode($establishmentXml->establishment->reservation_postal2);
        $reservation_postal3 = urldecode($establishmentXml->establishment->reservation_postal3);
        $reservation_postal_code = urldecode($establishmentXml->establishment->reservation_postal_code);
        $base_postal1 = urldecode($establishmentXml->establishment->postal_address_line1);
        $base_postal2 = urldecode($establishmentXml->establishment->postal_address_line2);
        $base_postal3 = urldecode($establishmentXml->establishment->postal_address_line3);
        $base_postalCode = urldecode($establishmentXml->establishment->postal_address_code);
        if (empty($reservation_postal1)) {
            if (!empty($base_postal1) || !empty($base_postal2) || !empty($base_postal3)) {
                $contact_address .= "<br /><strong>Postal Address: </strong><br />";
                if (!empty($base_postal1)) {
                    if (substr($base_postal1, -1) == ',' || substr($base_postal1, -1) == ';') $base_postal1 = substr($base_postal1, 0, -1);
                    $contact_address .= "$base_postal1<br>";
                }
                if (!empty($base_postal2)) {
                    if (substr($base_postal2, -1) == ',' || substr($base_postal2, -1) == ';') $base_postal2 = substr($base_postal2, 0, -1);
                    $contact_address .= "$base_postal2<br>";
                }
                if (!empty($base_postal3)) {
                    if (substr($base_postal3, -1) == ',' || substr($base_postal3, -1) == ';') $base_postal3 = substr($base_postal3, 0, -1);
                    $contact_address .= "$base_postal3<br>";
                }
                if (!empty($base_postalCode)) $contact_address .= "$base_postalCode<br>";
            }
        } else {
            if (!empty($reservation_postal1) || !empty($reservation_postal2) || !empty($reservation_postal3)) {
                $contact_address .= "<strong>Postal Address: </strong><br />";
                if (!empty($reservation_postal1)) {
                    if (substr($reservation_postal1, -1) == ',' || substr($reservation_postal1, -1) == ';') $reservation_postal1 = substr($reservation_postal1, 0, -1);
                    $contact_address .= "$reservation_postal1<br>";
                }
                if (!empty($reservation_postal2)) {
                    if (substr($reservation_postal2, -1) == ',' || substr($reservation_postal2, -1) == ';') $reservation_postal2 = substr($reservation_postal2, 0, -1);
                    $contact_address .= "$reservation_postal2<br>";
                }
                if (!empty($reservation_postal3)) {
                    if (substr($reservation_postal3, -1) == ',' || substr($reservation_postal3, -1) == ';') $reservation_postal3 = substr($reservation_postal3, 0, -1);
                    $contact_address .= "$reservation_postal3<br>";
                }
                if (!empty($reservation_postal_code)) $contact_address .= "$reservation_postal_code<br>";
                $contact_address .= "</td></tr>";
            }
        }
        $gps_latitude = urldecode($establishmentXml->establishment->gps_latitude);
        $gps_longitude = urldecode($establishmentXml->establishment->gps_longitude);
        if (empty($gps_latitude)) {
            $gps = '';
            $gps_coord = '';
        } else {
            $contact_address .= "<strong>GPS: </strong><br />";
            $gps_latitude = stripslashes($gps_latitude);
            $gps_longitude = stripslashes($gps_longitude);
            $gps_latitude = str_replace('<34', '"', $gps_latitude);
            $gps_longitude = str_replace('<35', '"', $gps_longitude);
            $contact_address .= "Latitude: ".round($gps_latitude, 6)."<br />Longitude: ".round($gps_longitude, 6);
            //echo $gps_latitude;
            $gps_coord = "GPS Co-ordinates for ".$establishment_name." : ".round($gps_latitude, 6).", ".round($gps_longitude, 6);
        }
        $establishmentTemplate = str_replace('<!-- address_details -->', $contact_address, $establishmentTemplate);

        //Points of interest
        $landmarks = "<div class='row'>";
        $isLandmark = false;
        foreach ($establishmentXml->establishment->landmarks->landmark AS $landmark) {
            $isLandmark = true;
            $landmarkFile = $this->contentFileFromName(urldecode($landmark->name));
            if (file_exists('content/'.$landmarkFile.".html")) {
                $landmarks .= "<div class='col-xs-12 col-sm-12'><a target='_blank' href='/index.php?p=".$landmarkFile."'>".urldecode($landmark->name)."</a>";
            } else {
                $landmarks .= "<div class='col-xs-12 col-sm-12'>".urldecode($landmark->name)."";
            }
            if ($landmark->distance != "0" && $landmark->distance != "") {
                $landmarks .= " (".urldecode($landmark->distance.$landmark->measurement).")";
            }
            $landmarks .= "</div>";
        }
        $landmarks .= "</div>";
        $establishmentTemplate = str_replace('<!-- points_of_interest -->', $landmarks, $establishmentTemplate);

        //Establishment Rates Tab
        $price_quote = "".$establishmentXml->establishment->price."";

        $rates = nl2br(urldecode($price_quote));
        $establishmentTemplate = str_replace('<!-- price_form -->',urldecode($price_quote) ,$establishmentTemplate);

        //Establishment Map
        $map_image = '';
        $short_description = urldecode($establishmentXml->establishment->short_description);
        $direction_block = "";
        $direction = urldecode($establishmentXml->establishment->directions->directions);
        $map_type = urldecode($establishmentXml->establishment->directions->map_type);
        $image_name = urldecode($establishmentXml->establishment->directions->image_name);
        if (!empty($direction) || $map_type == 'google' || $gps_coord || $map_type == "upload") {
            //$map_type = 'google';
            $direction = nl2br(stripslashes($direction));
            $map_file = strtolower($establishment['code']) . '.jpg';
            if ($gps_coord != "" && empty($map_type)) {
                $map_type = "google";
            }
            if (!empty($map_type) && $map_type == 'upload') {$direction_block .= "<table align=right><tr valign=top><td><a href='/res_maps/$image_name' target='_blank' ><img src='/res_maps/TN_$map_file' border=0 /></a></td></tr></table>";}
            if (!empty($map_type) && $map_type == 'google')  {
                if (empty($gps_latitude)) {
                    //Get Geocode
                    $delay = 0;
                    $geocode_pending = true;
                    $address = '';
                    if (!empty($base_street1)) $address .= "$base_street1,";
                    if (!empty($base_street2)) $address .= "$base_street2,";
                    if (!empty($base_street3)) $address .= "$base_street3,";
                    $address = substr($address, 0, -1);
                    $map_url = "http://maps.google.com/maps/geo?output=xml&key=" . $GLOBALS['api_key'] . "&q=" . urlencode($address);
                    while ($geocode_pending) {
                        $xml = simplexml_load_file($map_url);
                        $status = $xml->Response->Status->code;
                        if (strcmp($status, "200") == 0) {
                            $geocode_pending = false;
                            $coordinates = $xml->Response->Placemark->Point->coordinates;
                            $coordinatesSplit = split(",", $coordinates);
                            // Format: Longitude, Latitude, Altitude
                            $lat = $coordinatesSplit[1];
                            $lng = $coordinatesSplit[0];
                            if (str_word_count($short_description) > 50) {
                                $words = str_word_count($short_description, 1);
                                $words = array_slice($words, 0, 50);
                                $map_description = implode(' ', $words);
                                $map_description .= '...';
                            } else {
                                $map_description = $short_description;
                            }
                            $map_description = str_replace(array("\r\n","\r","\n"), '', $map_description);
                            $map_description = trim($map_description);
                            $map_description = str_replace("'", '&apos;', $map_description);
                            $direction_block .= "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key={$GLOBALS['api_key']}&sensor=false' type='text/javascript'></script>";
                            $direction_block .= "<table width=100%><tr valign=top><td>";
                            $direction_block .= "<script type='text/javascript'>
																    function initialize() {
																      if (GBrowserIsCompatible()) {
																        var map = new GMap2(document.getElementById('map_canvas'));
																        map.setCenter(new GLatLng($lat, $lng), 13);
																        map.setUIToDefault();
																        map.openInfoWindowHtml(map.getCenter(),
					                  										'<table width=300px ><tr><td><span style=\"font-family: arial; font-size:10pt; font-weight: bold\">".addslashes($establishment_name)."</span><br><table align=left><tr><td>";
                            if (is_file($_SERVER['DOCUMENT_ROOT'] . "/res_images/$map_image")) $direction_block .= "<img src=/res_images/$map_image style=\"border: 1px solid black\"/>";
                            $direction_block .= "</td></tr></table><span style=\"font-family: arial; font-size: 8pt\" >$map_description</span></td></tr></table>');
																      }
																    }
																    </script>";
                            $direction_block .= "<div id='map_canvas'></div></td></tr></table>";
                            $direction_block .= "<script>initialize()</script>";
                        } else if (strcmp($status, "620") == 0) {
                            $delay += 100000;
                        } else {
                            $geocode_pending = false;
                        }
                        usleep($delay);
                    }
                } else {
                    if (str_word_count($short_description) > 50) {
                        $words = str_word_count($short_description, 1);
                        $words = array_slice($words, 0, 50);
                        $map_description = implode(' ', $words);
                        $map_description .= '...';
                    } else {
                        $map_description = $short_description;
                    }
                    $map_description = str_replace(array("\r\n","\r","\n"), '', $map_description);
                    $map_description = trim($map_description);
                    $map_description = str_replace("'", '&apos;', $map_description);
                    $direction_block .= "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key={$GLOBALS['api_key']}&sensor=false' type='text/javascript'></script>";
                    $direction_block .= "<table width=100%><tr valign=top><td>";
                    $direction_block .= "<script type='text/javascript'>
														    function initialize() {
														      if (GBrowserIsCompatible()) {
														        var map = new GMap2(document.getElementById('map_canvas'));
														        map.setCenter(new GLatLng($gps_latitude, $gps_longitude), 13);
														        map.setUIToDefault();
														        map.openInfoWindowHtml(map.getCenter(),
				                  									'<table width=300px ><tr><td><span style=\"font-family: arial; font-size:10pt; font-weight: bold\">".addslashes($establishment_name)."</span><br><table align=left><tr><td>";
                    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/res_images/$map_image")) $direction_block .= "<img src=/res_images/$map_image style=\"border: 1px solid black\"/>";
                    $direction_block .= "</td></tr></table><span style=\"font-family: arial; font-size: 8pt\" >$map_description</span></td></tr></table>');
														      }
														    }

														    </script>";
                    $direction_block .= "$gps_coord<div id='map_canvas'></div></td></tr></table>";
                    $direction_block .= "<script>initialize()</script>";
                }
            }
            if (trim($direction) != "") {
                $direction_block .= "<p>$direction</p>";
            }
        }
        $establishmentTemplate = str_replace('<!-- establishment_map -->', $direction_block, $establishmentTemplate);

        //Nightsbridge
        $nightsbridge_id = urldecode($establishmentXml->establishment->nightsbridge_bbid);
        $quote_tab = "javascript:estabTabClicked(this)";
        $hide_quote_block = "display:block;";
        $nightsbridge_display = 'none';
        if ((int) $nightsbridge_id > 0) {
            $nightsbridge_button = "";
            $hide_quote_block = "display:none;";
            $establishmentTemplate = str_replace('<!-- bbid -->', $nightsbridge_id, $establishmentTemplate);
            $nightsbridge_display = 'block';
        }

        //Get nigtsbridge data
        if (!empty($establishmentXml->establishment->nightsbridge_bbid)) {
            if (isset($_COOKIE['searchSettings']) && !empty($_COOKIE['searchSettings'])) {
                $cookie_data = explode('|', $_COOKIE['searchSettings']);
                $api_data2 = array();
                $api_data2['messagename'] = "AvailRQ";
                $api_data2['credentials']['nbid'] = $nbid;
                $api_data2['credentials']['password'] = $nightsbridge_password;
                $api_data2['bblist'] = array();
                $api_data2['bblist']['bbid'] = array();
                $api_data2['bblist']['bbid'][] = (int) $establishmentXml->establishment->nightsbridge_bbid;
                $api_data2['startdate'] = date('Y-m-d', strtotime($cookie_data[0]));
                $api_data2['enddate'] = date('Y-m-d', strtotime($cookie_data[1]));
                $api2['data'] = json_encode($api_data2);
                $data = "data={$api2['data']}";

                $s = curl_init();
                curl_setopt($s,CURLOPT_URL,$nightsbridge_api_url);
                curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($s,CURLOPT_FORBID_REUSE,true);
                curl_setopt($s,CURLOPT_FRESH_CONNECT,true);
                curl_setopt($s,CURLOPT_POST,1);
                curl_setopt($s,CURLOPT_POSTFIELDS,$data);
                $d = curl_exec($s);
                curl_close($s);


                $nightsbridge_result = json_decode($d, true);

                if ($nightsbridge_result['success'] == true) {
                    if (isset($_COOKIE['searchSettings'])) {
                        $booking_data = explode('|', $_COOKIE['searchSettings']);
                        if (!isset($nightsbridge_result['data']['bb'][0]['noavailability'])) {
                            $nightsbridge_button = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/nightbridge_booknow.html");
                            $start_date = date("d M, Y", strtotime($booking_data[0]));
                            $end_date = date("d M, Y", strtotime($booking_data[1]));
							$nbStart = date('Y-m-d', strtotime($booking_data[0]));
							$nbEnd = date('Y-m-d', strtotime($booking_data[1]));
                            $nightsbridge_button = str_replace('<!-- start -->', $start_date, $nightsbridge_button);
                            $nightsbridge_button = str_replace('<!-- end -->', $end_date, $nightsbridge_button);
                            $nightsbridge_button = str_replace('<!-- start_date -->', $nbStart, $nightsbridge_button);
                            $nightsbridge_button = str_replace('<!-- end_date -->', $nbEnd, $nightsbridge_button);
                            $nightsbridge_button = str_replace('<!-- bbid -->', $establishmentXml->establishment->nightsbridge_bbid[0], $nightsbridge_button);
                            $nightsbridge_button = str_replace('<!-- nbid -->', 16, $nightsbridge_button);
                            $nightsbridge_display = 'none';

                            //Rates
                            $rates_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/establishment_rates.html");
                            $rates ='';
                            foreach ($nightsbridge_result['data']['bb'][0]['roomtypes'] as $roomData) {
                                $prices = '';
                                foreach ($roomData['mealplans'] as $priceData) {
                                    $prices .= "<div class='col-xs-4'>{$priceData['mealplandesc']}</div>";
                                    $prices .= "<div class='col-xs-4'>R{$priceData['rates']['pax1']} (1 Guest)</div>";
                                    $prices .= "<div class='col-xs-4'>R{$priceData['rates']['pax2']} (2 Guests)</div>";
                                }
                                $rate_block = str_replace('<!-- roomtypename -->', $roomData['roomtypename'], $rates_template);
                                $rate_block = str_replace('<!-- description -->', $roomData['description'], $rate_block);
                                $rate_block = str_replace('<!-- prices -->', $prices, $rate_block);
                                $rate_block = str_replace('<!-- childpolicy -->', $roomData['childpolicy']['general'], $rate_block);
                                $rate_block = str_replace('<!-- max_guests -->', "{$roomData['maxoccupancy']} (Max. {$roomData['maxadults']} Adults)", $rate_block);
                                if (empty($roomData['rtimage'])) {
                                    $rate_block = str_replace('<!-- roomImage -->', "", $rate_block);
                                } else {
                                    $rate_block = str_replace('<!-- roomImage -->', "<img src='{$nightsbridge_result['data']['rtimageurl']}{$roomData['rtimage']}' class='img-responsive'>", $rate_block);
                                }

                                $rates .= $rate_block;
                            }

							//Images
                            require_once "{$_SERVER['DOCUMENT_ROOT']}/v2/classes/establishment.class.php";
                            $establishmentClass2 = new Establishment();
                            foreach ($nightsbridge_result['data']['bb'][0]['roomtypes'] as $roomData) {
                                if (empty($roomData['rtimage'])) continue;
                                $filename = explode('/', $roomData['rtimage']);
                                $filename = $filename[1];

                                //Insert into DB
                                $ins = $establishmentClass2->insert_image($establishment['code'], $filename, $filename, NULL, $roomData['roomtypename'] ,'');

                                if ($ins === true) {
                                    $img = file_get_contents("{$nightsbridge_result['data']['rtimageurl']}{$roomData['rtimage']}");
                                    file_put_contents("{$_SERVER['DOCUMENT_ROOT']}/res_images/{$filename}", $img);
                                }
                            }
                        }
                    }
                    $display_request_quote = 'none';
					$tc_display = 'none';
                } else {
                    $display_request_quote = 'block';
					$tc_display = 'block';
                }
            }
        }
        $establishmentTemplate = str_replace('<!-- nightsbridge_button2 -->', $nightsbridge_button, $establishmentTemplate);
        $establishmentTemplate = str_replace('<!-- request_quote_tab -->', $quote_tab, $establishmentTemplate);
        $establishmentTemplate = str_replace('<!-- hide_quote_block -->', $hide_quote_block, $establishmentTemplate);
        $establishmentTemplate = str_replace('<!-- nightsbridge_display -->', $nightsbridge_display, $establishmentTemplate);
        $establishmentTemplate = str_replace('<!-- display_request -->', $display_request_quote, $establishmentTemplate);
        $establishmentTemplate = str_replace('<!-- rates -->',$rates,$establishmentTemplate);
        $establishmentTemplate = str_replace('<!-- tc_display -->',$tc_display,$establishmentTemplate);
        //CAPTCHA
        $publickey = "6LfZCeMSAAAAADAe_nC8kx-QgLTOVmm9lB4o6_cD";
        $privatekey = "6LfZCeMSAAAAACcBTQVH6eS9F0IDfcl1J4EOWXhY";
        require_once('recaptcha/recaptchalib.php');
        if (!isset($error)) $error = '';
        $captcha_html = recaptcha_get_html($publickey, $error);
        $establishmentTemplate = str_replace("<!-- captcha -->",$captcha_html,$establishmentTemplate);

        //Gallery Tab
        $imageGallery = "<div class='imageGallery'>";
        foreach ($establishmentXml->establishment->images->image AS $imageXml) {
            $image_title = $imageXml->title;
            $image_description = $imageXml->description;
            $imageGallery .= "<div class='col-md-4 galleryImage' style='height: 300px'><img class='img-responsive' title='{$pageTitle}' style='cursor:pointer;' onclick='launchGallery(this.src, this.title, \"$image_description\")' src='http://$server/res_images/".urldecode($imageXml->name)."' /><div class='establishmentImageDesc'>".urldecode($imageXml->title)."<br /><p>".urldecode($imageXml->description)."</p></div></div>";
        }
        $imageGallery .= "</div>";
        $establishmentTemplate=str_replace("<!-- gallery -->",$imageGallery,$establishmentTemplate);

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

//        if (strpos($establishmentTemplate, '<!-- bbid -->') > 0) {
//            $establishmentTemplate = preg_replace('/(<iframe id="bbidFrame".+<\/iframe>)/s', '', $establishmentTemplate);
//        }

		//Page Description
		$this->pageDescription = urldecode($establishmentXml->establishment->short_description);

        return $establishmentTemplate;
    }

	function getBody() {
		global $templateLinkBlock;
		global $template;
		global $config;
		global $pageParam;
		global $api;

		$pageQuery = isset($_GET["page"])?$_GET["page"]:'';

		if ($pageParam[0] == "accommodation") {
			$api->setMethod("fetch");
			$api->setType("aaweb_areas");
			$api->addParam("url",$pageQuery);

            if (isset($_COOKIE['searchSettings']) && !empty($_COOKIE['searchSettings'])) {
                $cookieData = explode('|', $_COOKIE['searchSettings']);
                $api->addParam("check_start",$cookieData[0]);
                $api->addParam("check_end",$cookieData[1]);
                $api->addParam("adults",$cookieData[2]);
                $api->addParam("children",$cookieData[3]);
            }

			$areaXml = $api->getApiXML();

			if (isset($areaXml->establishment['code'])) {
				$establishment_body = $this->getEstablishment_v2($areaXml->establishment,$areaXml);
			}
			$travelInfoPage = "";
			$preUrl = "accommodation/".$areaXml->theme["safe_url"];
			
			$preUrl .= "/".$areaXml->region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->country["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->province["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->province_region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->sub_region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->town["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->town_region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl = trim($preUrl,"/");
			
			$rdclass = $this->getProvinceClass($areaXml);
			
			if (isset($areaXml->themes->theme)) {
				if (isset($areaXml->suburb)) {$areaHeading = $areaXml->suburb;$travelInfoPage = $areaXml->suburb['safe_url'];} else
				if (isset($areaXml->town_region)) {$areaHeading = $areaXml->town_region;$travelInfoPage = $areaXml->town_region['safe_url'];} else
				if (isset($areaXml->town)) {$areaHeading = $areaXml->town;$travelInfoPage = $areaXml->town['safe_url'];} else
				if (isset($areaXml->sub_region)) {$areaHeading = $areaXml->sub_region;$travelInfoPage = $areaXml->sub_region['safe_url'];} else
				if (isset($areaXml->province_region)) {$areaHeading = $areaXml->province_region;$travelInfoPage = $areaXml->province_region['safe_url'];} else
				if (isset($areaXml->province)) {$areaHeading = $areaXml->province;$travelInfoPage = $areaXml->province['safe_url'];} else
				if (isset($areaXml->country)) {$areaHeading = $areaXml->country;$travelInfoPage = $areaXml->country['safe_url'];} else
				if (isset($areaXml->region)) {$areaHeading = $areaXml->region;$travelInfoPage = $areaXml->region['safe_url'];}
				
				$travelInfoList = "";
				
				$travelInfoPage = $this->contentFileFromName($travelInfoPage);
				if (file_exists("content/".$travelInfoPage.".html")) {
					$travelInfoList .= "<div class='panel panel-default'>";
					$travelInfoList .= "<div class='panel-heading'>Travel Information</div>";
					$travelInfoList .= "<div class='panel-body'><a target='_blank' href='/index.php?p=".$travelInfoPage."'>".urldecode($areaHeading)." info</a></div>";
					$travelInfoList .= "</div>";
				}
				
				$themeList2 = "<div class='areaBlock'>";
				$themeList2 .= "<h1 class='$rdclass'>Discount Accommodation</h1><ul>";
				$themeList = "<div class='panel panel-default'>";
				$themeList .= "<div class='panel-heading'>Accommodation<br><small>".urldecode($areaHeading)."</small></div><div class='panel-body'>";
				$showThemeList2 = false;
				foreach ($areaXml->themes->theme AS $theme) {
					$accomThemeFix = false;
					$themUrl = $preUrl;
					if (strpos($preUrl, "accommodation/accommodation") > -1) {
						$accomThemeFix = true;
						$themUrl = str_ireplace("accommodation/accommodation/", "accommodation/", $themUrl);
					}
					$themUrl = str_replace($areaXml->theme["safe_url"], $theme["safe_url"], $themUrl);
					if ($accomThemeFix) {
						$themUrl = "accommodation/".$themUrl;
					}
					if ($theme["safe_url"] == "specials" || $theme["safe_url"] == "bonsela") {
						$themeList2 .= "<a href='/$themUrl'>".urldecode($theme)."</a> (".$theme["count"].")<br>";
						$showThemeList2 = true;
					} else {
						$themeList .= "<a href='/$themUrl'>".urldecode($theme)."</a> (".$theme["count"].")<br>";
					}
				}
				$themeList .= "</div></div>";
				$themeList2 .= "</ul></div>";
				if (!$showThemeList2) {
					$themeList2 = "";
				}
			}
			if (!isset($areaXml->establishment)) {
				$api->setMethod("fetch");
				$api->setType("aaweb_establist");
				if (isset($areaXml->theme)) {$api->addParam("theme_id",$areaXml->theme['id']);}
				if (isset($areaXml->region)) {$api->addParam("region_id",$areaXml->region['id']);}
				if (isset($areaXml->country)) {$api->addParam("country_id",$areaXml->country['id']);}
				if (isset($areaXml->province)) {$api->addParam("province_id",$areaXml->province['id']);}
				if (isset($areaXml->province_region)) {$api->addParam("pregion_id",$areaXml->province_region['id']);}
				if (isset($areaXml->sub_region)) {$api->addParam("sregion_id",$areaXml->sub_region['id']);}
				if (isset($areaXml->town)) {$api->addParam("town_id",$areaXml->town['id']);}
				if (isset($areaXml->town_region)) {$api->addParam("tregion_id",$areaXml->town_region['id']);}
				if (isset($areaXml->suburb)) {$api->addParam("suburb_id",$areaXml->suburb['id']);}
				
				$listPage = 1;
		
				for($i=1;$i<12;$i++) {
					$page = explode("_",$pageParam[$i]);
						
					if ($page[0] == "page") {
						$listPage = $page[1];
					}
				}
				
				$api->addParam("page",$listPage);
				$areaEstabListXml = $api->getApiXML();
				$areaEstabList = $this->getEstList($areaEstabListXml,$areaXml,$preUrl);
				$log_params = "";
				foreach ($areaEstabListXml->estlist->establishment AS $result) {
					$log_params .= $result["code"].",";
				}
				$log_params = rtrim($log_params,",");
				$log_url = $_SERVER["REQUEST_URI"];
				$log_type = "l";
				$log_ip = $_SERVER["REMOTE_ADDR"];
				$this->webLog($log_params,$log_url,$log_type,$log_ip);
			}
			
			$addBanner = "&nbsp;<div align='center' style='margin-top:20px;'>
			    <script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>
                <!-- booktravel.travel/essentialtravelinfo.com right sky ad -->
                <ins class=\"adsbygoogle\"
                     style=\"display:inline-block;width:160px;height:600px\"
                     data-ad-client=\"ca-pub-4514882728543448\"
                     data-ad-slot=\"6493604526\"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script></div>";
			//$addBanner = "";

            if (!isset($listPage)) $listPage = '';
            if (!isset($areaEstabList)) $areaEstabList = '';
            if (!isset($suburbList)) $suburbList = '';
            if (!isset($townRegList)) $townRegList = '';
            if (!isset($townList)) $townList = '';
            if (!isset($subRegList)) $subRegList = '';
            if (!isset($provRegList)) $provRegList = '';
            if (!isset($provinceList)) $provinceList = '';
            if (!isset($countryList)) $countryList = '';
            if (!isset($regionList)) $regionList = '';
			$regionDropDowns = $this->ajaxGetRegionData($areaXml,$listPage);
			
			$areaBody = "";
			
//			$areaBody .= "<div id='RegionDropDowns' class='$rdclass'>$regionDropDowns</div>
//			<div id='AreaBodyHolder'>
//				<div id='EstabListHolder' class='areaEstHolder'>$areaEstabList </div>
//				<div id='AreaBlockHolder' class='areaBlockHolder'>$travelInfoList$themeList2$themeList$suburbList$townRegList$townList$subRegList$provRegList$provinceList$countryList$regionList <br /><br /> <a href='/index.php?p=map_regions'><img src='/images/mini-map-right.png' /></a>
//				</div>
//			</div>$addBanner";
			$areaBody .= "
<div class='row'>
	<div class='col-xs-12 col-sm-9'>
		$areaEstabList
	</div>
	<div class='hidden-xs col-sm-3'>
		$travelInfoList$themeList2$themeList$suburbList$townRegList$townList$subRegList$provRegList$provinceList$countryList$regionList <br /><br /> <a href='/index.php?p=map_regions'><img src='/images/mini-map-right.png' /></a>
	</div>
</div>
			";
			
			$keys = $areaXml->country."+".$areaXml->province."+".$areaXml->town;
			$keys = trim($keys,"+");
			
			$body = "";
		}

		if (empty($pageParam[1])) {
			$body = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/content/home.html");
			if ($pageParam[0] == "asearch") {
				$body .= "<script>openAdvancedSearch();</script>";
			}
		}
		if ($pageParam[0] == "asearch") {
			$template = $this->setTemplate($template,"body",'<h1 class="estListHeader sa">Advanced Search</h1><div id="AdvancedSearchContent">
				<fieldset class="advSearchFieldset">
					<legend>Select Location</legend>
					<div id="AdvancedSearchLocations"></div>
				</fieldset>
				<fieldset class="advSearchFieldset">
					<legend>AA Qaulity Assurance</legend>
					<div id="AdvancedSearchQA"></div>
				</fieldset>
				<br style="clear:both;" /><br />
				<input type="button" onclick="javascript:submitAdvSearch();" value="Search" />
				<div id="AdvancedSearchIcons"></div>
				<br style="clear:both;" /><br />
				<input type="button" onclick="javascript:submitAdvSearch();" value="Search" />
			</div>
			<script>
				openAdvancedSearch();
			</script>');
		} elseif ($pageParam[0] == "guest_review") {
			$template = $this->setTemplate($template,"body",'<iframe style="border:0;" width="800" height="700" src="http://booktravel.travel/survey/survey.php?code='.$pageParam[1].'&type=aaweb" />');
		} elseif (isset($establishment_body) && $establishment_body != "") {
//			$template = $this->setTemplate($template,"body","<div id='RegionDropDowns' class='$rdclass'>$regionDropDowns</div>".$establishment_body);
			$template = $this->setTemplate($template,"body","".$establishment_body);
		} else {
            $b = '';
            if (isset($areaBody)) $b .= $areaBody;
            if (isset($body)) $b .= $body;
                $template = $this->setTemplate($template, "body", $b);
		}



        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}
	
	function getProvinceClass($areaXml) {
		$rdclass = "sa";

        if (isset($areaXml->province)) {
            if ($areaXml->province == "Gauteng") $rdclass = "gp";
            if ($areaXml->province == "Western+Cape") $rdclass = "wc";
            if ($areaXml->province == "Eastern+Cape") $rdclass = "ec";
            if ($areaXml->province == "Northern+Cape") $rdclass = "nc";
            if ($areaXml->province == "KwaZulu-Natal") $rdclass = "kzn";
            if ($areaXml->province == "Free+State") $rdclass = "fs";
            if ($areaXml->province == "Mpumalanga") $rdclass = "mp";
            if ($areaXml->province == "Limpopo") $rdclass = "lm";
            if ($areaXml->province == "North+West") $rdclass = "nw";
        }

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

        return $rdclass;
	}
	
	function xmlStr($str) {

        $str = urldecode($str);

        //Log Function Stats
        $search_string = __FILE__ . "|" . __FUNCTION__ . "('$str')";
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return $str;
	}
	
	function setTemplate($template,$section,$content) {

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return str_replace("<!-- $section -->", $content, $template);
	}
	
	function getTemplateBlock($filename,$block) {
		$template = file_get_contents("templates/$filename");
		$templateBlockStart = strpos($template,"<!-- start_$block -->");
		$templateBlockEnd = strpos($template,"<!-- end_$block -->");
		$templateBlock = substr($template,$templateBlockStart,($templateBlockEnd-$templateBlockStart));

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return $templateBlock;
	}
	private function contentFileFromName($name) {
		$filename = urldecode(urldecode($name));
		$filename = trim($filename);
		$filename = str_replace("-","_",$filename);
		$filename = str_replace(" ","_",$filename);
		$filename = str_replace("(","",$filename);
		$filename = str_replace(")","",$filename);
		$filename = str_replace("&","_",$filename);
		$filename = str_replace("'","",$filename);
		$filename = str_replace("__001__","_",$filename);
		$filename = strtolower($filename);

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return $filename;
	}
	private function makeUrlSafe($str) {
		$str = str_replace("&", "__001__", $str);
		$safe_url = strtolower($str);
		$safe_url = str_replace(" ","_",$safe_url);
		$safe_url = urlencode($safe_url);

        //Log Function Stats
        // $search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return $safe_url;
	}
	
}?>
