<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/27
 * Time: 4:04 PM
 */

//Includes
require_once "../v2/settings/init.php";

//Vars
$from_type = $_GET['from_type'];
$from_id = $_GET['from_id'];
$to_type = $_GET['to_type'];
$to_id = $_GET['to_id'];

//Get establishments to move
switch ($from_type) {
    case 'suburb':

        $statement = "SELECT establishment_code
                        FROM nse_establishment_nlocations
                        WHERE location_id = :location_id
                          AND location_type = 'suburb'";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':location_id', $from_id);
        $sql_select->execute();
        $establishments = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        break;

    case 'town':

        $statement = "SELECT establishment_code
                        FROM nse_establishment_nlocations
                        WHERE location_id = :location_id
                          AND location_type = 'town'";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':location_id', $from_id);
        $sql_select->execute();
        $establishments = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        break;
}

//Get ID's
switch ($to_type) {
    case 'suburb':

        $loc['suburb'] = $to_id;

        $statement = "SELECT b.town_id, c.province_id, d.country_id
                        FROM nse_nlocations_suburbs AS a
                        LEFT JOIN nse_nlocations_towns AS b ON a.town_id = b.town_id
                        LEFT JOIN nse_nlocations_provinces AS c ON b.province_id = c.province_id
                        LEFT JOIN nse_nlocations_countries As d ON b.country_id = d.country_id
                        WHERE a.suburb_id = :suburb_id
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':suburb_id', $to_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        $loc['town'] = $sql_result['town_id'];
        $loc['province'] = $sql_result['province_id'];
        $loc['country'] = $sql_result['country_id'];

        break;

    case 'town':

        $loc['suburb'] = '';

        $statement = "SELECT b.town_id, c.province_id, d.country_id
                        FROM nse_nlocations_towns AS b
                        LEFT JOIN nse_nlocations_provinces AS c ON b.province_id = c.province_id
                        LEFT JOIN nse_nlocations_countries As d ON b.country_id = d.country_id
                        WHERE b.town_id = :town_id
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':town_id', $to_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        $loc['town'] = $sql_result['town_id'];
        $loc['province'] = $sql_result['province_id'];
        $loc['country'] = $sql_result['country_id'];

        break;
}

foreach ($establishments as $e) {
    //Clear current data
    $statement = "DELETE FROM nse_establishment_nlocations
                    WHERE establishment_code = :establishment_code";
    $sql_delete = $GLOBALS['dbCon']->prepare($statement);
    $sql_delete->bindParam(':establishment_code', $e['establishment_code']);
    $sql_delete->execute();
    $sql_delete->closeCursor();

    foreach ($loc as $k=>$l) {
        if (!empty($l)) {
            $statement = "INSERT INTO nse_establishment_nlocations
                              (establishment_code, location_id, location_type)
                              VALUES
                              (:e, :id, :t)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bindParam(':e', $e['establishment_code']);
            $sql_insert->bindParam(':id', $l);
            $sql_insert->bindParam(':t', $k);
            $sql_insert->execute();
            $sql_insert->closeCursor();
        }
    }

    //Updane old table
    $statement = "UPDATE nse_establishment_location
                    SET suburb_id = :suburb,
                        town_id = :town,
                        province_id = :province,
                        country_id = :country
                    WHERE establishment_code = :e";
    $sql_update = $GLOBALS['dbCon']->prepare($statement);
    $sql_update->bindParam(':e', $e['establishment_code']);
    $sql_update->bindParam(':suburb', $loc['suburb']);
    $sql_update->bindParam(':town', $loc['town']);
    $sql_update->bindParam(':province', $loc['province']);
    $sql_update->bindParam(':country', $loc['country']);
    $sql_update->execute();
    $sql_update->closeCursor();
}

//echo "<pre>"; var_dump($establishments); echo '</pre>';
//echo "<pre>"; var_dump($loc); echo '</pre>';