<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/27
 * Time: 2:48 PM
 */


//Includes
require_once "../v2/settings/init.php";

//Vars
$location_id = $_GET['id'];
$location_type = $_GET['type'];

//Set delete to Final, deactivate location and mark record as checked
switch ($location_type) {
    case 'suburb':

        $statement = "UPDATE nse_nlocations_suburbs
                        SET checked = 'Y',
                            `delete` = 'F',
                             active = 'N'
                         WHERE suburb_id = :suburb_id
                         LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':suburb_id', $location_id);
        $sql_update->execute();
//        $sql_result = $sql_update->fetch(PDO::FETCH_ASSOC);
        $sql_update->closeCursor();

        break;
    case 'town':

        $statement = "UPDATE nse_nlocations_towns
                        SET checked = 'Y',
                            `delete` = 'F',
                             active = 'N'
                         WHERE town_id = :town_id
                         LIMIT 1";
        $sql_update = $GLOBALS['dbCon']->prepare($statement);
        $sql_update->bindParam(':town_id', $location_id);
        $sql_update->execute();
//        $sql_result = $sql_update->fetch(PDO::FETCH_ASSOC);
        $sql_update->closeCursor();

        break;

}
