<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/22
 * Time: 2:20 PM
 */


include $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';

//Vars
$ok = '#D2EFB4';
$nok = 'red';


//Get establishments
$statement = "SELECT establishment_code, establishment_name
                FROM nse_establishment";
$sql_select = $GLOBALS['dbPDO']->prepare($statement);
$sql_select->execute();
$sql_establ = $sql_select->fetchAll(PDO::FETCH_ASSOC);
$sql_select->closeCursor();


//Draw result
echo "<style>
        TH {
            background-color: #1b6d85;
        }

        .ln {
            border-top: 1px solid gray;
        }

        .lm1 {
            border-bottom: 1px dotted silver;
        }
</style>";
echo "<table style='width: 100%'>";
echo "<tr><th>Establishment</th><th>Suburb</th><th>Town</th><th>Province</th><th>Country</th></tr>";

foreach ($sql_establ AS $data) {

    $location = array(  'old' => array('suburb'=>'', 'town'=>'', 'province'=>'', 'country'=>''),
                        'new' => array('suburb'=>'', 'town'=>'', 'province'=>'', 'country'=>''));

    //Get old location
    $statement = "SELECT town_id, province_id, country_id, suburb_id
                    FROM nse_establishment_location
                    WHERE establishment_code = :establishment_code
                    LIMIT 1";
    $sql_select = $GLOBALS['dbPDO']->prepare($statement);
    $sql_select->bindParam(':establishment_code', $data['establishment_code']);
    $sql_select->execute();
    $sql_old = $sql_select->fetch(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();
    if (count($sql_old) != 0 && $sql_old !== false) {
        $location['old']['suburb'] = $sql_old['suburb_id']=='0'?'':$sql_old['suburb_id'];
        $location['old']['town'] = $sql_old['town_id']=='0'?'':$sql_old['town_id'];
        $location['old']['province'] = $sql_old['province_id']=='0'?'':$sql_old['province_id'];
        $location['old']['country'] = $sql_old['country_id'];
    }

//Get new Location
    $statement = "SELECT location_id, location_type
                    FROM nse_establishment_nlocations
                    WHERE establishment_code = :establishment_code";
    $sql_select = $GLOBALS['dbPDO']->prepare($statement);
    $sql_select->bindParam(':establishment_code', $data['establishment_code']);
    $sql_select->execute();
    $sql_new = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    if ($sql_new !== false && count($sql_new) > 0) {
        foreach ($sql_new as $nData) {
            switch ($nData['location_type']) {
                case 'suburb':
                    $location['new']['suburb'] = $nData['location_id'];
                    break;
                case 'town':
                    $location['new']['town'] = $nData['location_id'];
                    break;
                case 'province':
                    $location['new']['province'] = $nData['location_id'];
                    break;
                case 'country':
                    $location['new']['country'] = $nData['location_id'];
                    break;
            }
        }
    }

    $fc_suburb = $location['old']['suburb']==$location['new']['suburb']?$ok:$nok;
    $fc_town = $location['old']['town']==$location['new']['town']?$ok:$nok;
    $fc_province = $location['old']['province']==$location['new']['province']?$ok:$nok;
    $fc_country = $location['old']['country']==$location['new']['country']?$ok:$nok;

    if (empty($location['new']['suburb']) &&
        empty($location['new']['town']) &&
        empty($location['new']['province']) &&
        empty($location['new']['country'])) {
        $fc_est = $nok;

        //Fix
        foreach ($location['old'] as $lKey=>$lData) {
            if (empty($lData)) continue;
            $statement = "INSERT INTO nse_establishment_nlocations
                          (establishment_code, location_id, location_type)
                          VALUES
                          (:establishment_code, :location_id, :location_type)";
            $sql_insert = $GLOBALS['dbPDO']->prepare($statement);
            $sql_insert->bindParam(':establishment_code', $data['establishment_code']);
            $sql_insert->bindParam(':location_id', $lData);
            $sql_insert->bindParam(':location_type', $lKey);
            $sql_insert->execute();
            $sql_insert->closeCursor();
        }

    }else {
        $fc_est = 'white';
        continue;
    }

    echo "<tr>
               <td class='ln' rowspan='2' style='background-color: $fc_est'>{$data['establishment_code']}<br><small>{$data['establishment_name']}</small></td>
               <td class='ln lm1' style='background-color: $fc_suburb'>{$location['old']['suburb']}</td>
               <td class='ln lm1' style='background-color: $fc_town'>{$location['old']['town']}</td>
               <td class='ln lm1' style='background-color: $fc_province'>{$location['old']['province']}</td>
               <td class='ln lm1' style='background-color: $fc_country'>{$location['old']['country']}</td>
</tr>";
    echo "<tr>
               <td style='background-color: $fc_suburb'>{$location['new']['suburb']}</td>
               <td style='background-color: $fc_town'>{$location['new']['town']}</td>
               <td style='background-color: $fc_province'>{$location['new']['province']}</td>
               <td style='background-color: $fc_country'>{$location['new']['country']}</td>
</tr>";
}

echo "</table>";



