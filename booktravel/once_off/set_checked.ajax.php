<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/25
 * Time: 2:52 PM
 */

//Includes
require_once "../v2/settings/init.php";

//Vars
$type = $_GET['type'];
$id = $_GET['id'];
$method = $_GET['method'];


//save
switch ($type){
    case 'country':
        if ($method == 'checked') {
            //set checked to field
            $statement = "UPDATE nse_nlocations_countries
                            SET checked = 'Y',
                              `delete` = 'N'
                            WHERE country_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'delete') {
            $statement = "UPDATE nse_nlocations_countries
                            SET checked = '-',
                              `delete` = 'Y'
                            WHERE country_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'skip') {
            $statement = "UPDATE nse_nlocations_countries
                            SET checked = '-',
                              `delete` = 'N'
                            WHERE country_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        }
        break;
    case 'region':
        if ($method == 'checked') {
            //set checked to field
            $statement = "UPDATE nse_nlocations_cregions
                            SET checked = 'Y',
                              `delete` = 'N'
                            WHERE region_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'delete') {
            $statement = "UPDATE nse_nlocations_cregions
                            SET checked = '-',
                              `delete` = 'Y'
                            WHERE region_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'skip') {
            $statement = "UPDATE nse_nlocations_cregions
                            SET checked = '-',
                              `delete` = 'N'
                            WHERE region_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        }
        break;
    case 'cregion':
        if ($method == 'checked') {
            //set checked to field
            $statement = "UPDATE nse_nlocations_regions
                            SET checked = 'Y',
                              `delete` = 'N'
                            WHERE region_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'delete') {
            $statement = "UPDATE nse_nlocations_regions
                            SET checked = '-',
                              `delete` = 'Y'
                            WHERE region_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'skip') {
            $statement = "UPDATE nse_nlocations_regions
                            SET checked = '-',
                              `delete` = 'N'
                            WHERE region_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        }
        break;
    case 'province':
        if ($method == 'checked') {
            //set checked to field
            $statement = "UPDATE nse_nlocations_provinces
                            SET checked = 'Y',
                              `delete` = 'N'
                            WHERE province_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'delete') {
            $statement = "UPDATE nse_nlocations_provinces
                            SET checked = '-',
                              `delete` = 'Y'
                            WHERE province_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'skip') {
            $statement = "UPDATE nse_nlocations_provinces
                            SET checked = '-',
                              `delete` = 'N'
                            WHERE province_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        }
        break;
    case 'town':
        if ($method == 'checked') {
            //set checked to field
            $statement = "UPDATE nse_nlocations_towns
                            SET checked = 'Y',
                              `delete` = 'N'
                            WHERE town_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'delete') {
            $statement = "UPDATE nse_nlocations_towns
                            SET checked = '-',
                              `delete` = 'Y'
                            WHERE town_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'skip') {
            $statement = "UPDATE nse_nlocations_towns
                            SET checked = '-',
                              `delete` = 'N'
                            WHERE town_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        }
        break;
    case 'suburb':
        if ($method == 'checked') {
            //set checked to field
            $statement = "UPDATE nse_nlocations_suburbs
                            SET checked = 'Y',
                              `delete` = 'N'
                            WHERE suburb_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'delete') {
            $statement = "UPDATE nse_nlocations_suburbs
                            SET checked = '-',
                              `delete` = 'Y'
                            WHERE suburb_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        } else if ($method == 'skip') {
            $statement = "UPDATE nse_nlocations_suburbs
                            SET checked = '-',
                              `delete` = 'N'
                            WHERE suburb_id = $id
                            LIMIT 1";
            $sql_update = $GLOBALS['dbCon']->prepare($statement);
            //        $sql_update->bindParam(':area_id', $area_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        }
        break;
}
