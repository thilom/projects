<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/25
 * Time: 10:31 AM
 */

//Includes
require_once "../v2/settings/init.php";

//Vars
$template = file_get_contents('location_import.html');
$counter = 1;
$lines = '';
$location_found = false;
$country_class = 'blueBox';
$province_class = 'blueBox';
$region_class = 'blueBox';
$town_class = 'blueBox';
$suburb_class = 'blueBox';
$subregion_class = 'blueBox';
$sregion_class = 'blueBox';
$row = 0;

//Open and read counter
$cnt = file_get_contents('counter.txt');

//Open and read CSV file for reading
$csv = fopen('import.csv', 'r+');
    while (($data = fgetcsv($csv, 1000, ",")) !== FALSE) {

        $locations = array();
        $locations['country'] = $data[0];
        $locations['province'] = $data[1];
        $locations['region'] = $data[2];
        $locations['subregion'] = $data[3];
        $locations['town'] = $data[4];
        $locations['sregion'] = $data[5];
        $locations['suburb'] = $data[6];

        if ($counter == $cnt + 1) break;
        $counter++;
    }


    if (!empty($locations['suburb'])) {

        $suburb_class = 'greenBox';

        $data = check_suburb($locations['suburb']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('suburb', $d['suburb_id']);

                $lines .= "<div class='row'>
                        <div class='col-md-1'>Suburb</div>
                        <div class='col-md-1'>{$d['suburb_id']}</div>
                        <div class='col-md-6'>{$d['suburb_name']} ({$d['town_name']}, {$d['country_name']})</div>
                        <div class='col-md-1'>{$estab_count}</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"suburb\", {$d['suburb_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_region($locations['suburb']);
        if (!empty($data)) {
            foreach ($data as $d) {

                if ($d['parent_type'] == 'region') {
                    $type = 'subRegion';
                } else {
                    $type = 'region';
                }

                $lines .= "<div class='row'>
                        <div class='col-md-1'>{$type}</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_province($locations['suburb']);
        if (!empty($data)) {
            foreach ($data as $d) {

                $lines .= "<div class='row'>
                    <div class='col-md-1'>Province</div>
                    <div class='col-md-1'>{$d['province_id']}</div>
                    <div class='col-md-6'>{$d['province_name']}</div>
                    <div class='col-md-1'>N/A</div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"province\", {$d['province_id']}, \"checked\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"delete\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"skip\")'></div>
                        </div>";
                $row++;
            }
        }

        $data = check_sregion($locations['suburb']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $lines .= "<div class='row'>
                        <div class='col-md-1'>sRegion</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_town($locations['suburb']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('town', $d['town_id']);
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Town</div>
                        <div class='col-md-1'>{$d['town_id']}</div>
                        <div class='col-md-6'>{$d['town_name']} ({$d['province_name']})</div>
                        <div class='col-md-1'>$estab_count</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"town\", {$d['town_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }



    } else if (!empty($locations['sregion'])) {
        $sregion_class = 'greenBox';

        $data = check_region($locations['sregion']);
        if (!empty($data)) {
            foreach ($data as $d) {

                if ($d['parent_type'] == 'region') {
                    $type = 'subRegion';
                } else {
                    $type = 'region';
                }

                $lines .= "<div class='row'>
                        <div class='col-md-1'>{$type}</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_province($locations['sregion']);
        if (!empty($data)) {
            foreach ($data as $d) {

                $lines .= "<div class='row'>
                    <div class='col-md-1'>Province</div>
                    <div class='col-md-1'>{$d['province_id']}</div>
                    <div class='col-md-6'>{$d['province_name']}</div>
                    <div class='col-md-1'>N/A</div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"province\", {$d['province_id']}, \"checked\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"delete\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"skip\")'></div>
                        </div>";
                $row++;
            }
        }

        $data = check_sregion($locations['sregion']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $lines .= "<div class='row'>
                        <div class='col-md-1'>sRegion</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_town($locations['sregion']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('town', $d['town_id']);
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Town</div>
                        <div class='col-md-1'>{$d['town_id']}</div>
                        <div class='col-md-6'>{$d['town_name']} ({$d['province_name']})</div>
                        <div class='col-md-1'>$estab_count</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"town\", {$d['town_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_suburb($locations['sregion']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('suburb', $d['suburb_id']);

                $lines .= "<div class='row'>
                        <div class='col-md-1'>Suburb</div>
                        <div class='col-md-1'>{$d['suburb_id']}</div>
                        <div class='col-md-6'>{$d['suburb_name']} ({$d['town_name']}, {$d['country_name']})</div>
                        <div class='col-md-1'>{$estab_count}</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"suburb\", {$d['suburb_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

    } else if (!empty($locations['town'])) {
        $town_class = 'greenBox';

        $data = check_town($locations['town']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('town', $d['town_id']);
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Town</div>
                        <div class='col-md-1'>{$d['town_id']}</div>
                        <div class='col-md-6'>{$d['town_name']} ({$d['province_name']})</div>
                        <div class='col-md-1'>$estab_count</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"town\", {$d['town_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_region($locations['town']);
        if (!empty($data)) {
            foreach ($data as $d) {

                if ($d['parent_type'] == 'region') {
                    $type = 'subRegion';
                } else {
                    $type = 'region';
                }

                $lines .= "<div class='row'>
                        <div class='col-md-1'>{$type}</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_province($locations['town']);
        if (!empty($data)) {
            foreach ($data as $d) {

                $lines .= "<div class='row'>
                    <div class='col-md-1'>Province</div>
                    <div class='col-md-1'>{$d['province_id']}</div>
                    <div class='col-md-6'>{$d['province_name']}</div>
                    <div class='col-md-1'>N/A</div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"province\", {$d['province_id']}, \"checked\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"delete\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"skip\")'></div>
                        </div>";
                $row++;
            }
        }

        $data = check_sregion($locations['town']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $lines .= "<div class='row'>
                        <div class='col-md-1'>sRegion</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_suburb($locations['town']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('suburb', $d['suburb_id']);

                $lines .= "<div class='row'>
                        <div class='col-md-1'>Suburb</div>
                        <div class='col-md-1'>{$d['suburb_id']}</div>
                        <div class='col-md-6'>{$d['suburb_name']} ({$d['town_name']}, {$d['country_name']})</div>
                        <div class='col-md-1'>{$estab_count}</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"suburb\", {$d['suburb_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }


    } else if (!empty($locations['subregion'])) {
        $subregion_class = 'greenBox';

        $data = check_region($locations['subregion']);
        if (!empty($data)) {
            foreach ($data as $d) {

                if ($d['parent_type'] == 'region') {
                    $type = 'subRegion';
                } else {
                    $type = 'region';
                }

                $lines .= "<div class='row'>
                        <div class='col-md-1'>{$type}</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_province($locations['subregion']);
        if (!empty($data)) {
            foreach ($data as $d) {

                $lines .= "<div class='row'>
                    <div class='col-md-1'>Province</div>
                    <div class='col-md-1'>{$d['province_id']}</div>
                    <div class='col-md-6'>{$d['province_name']}</div>
                    <div class='col-md-1'>N/A</div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"province\", {$d['province_id']}, \"checked\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"delete\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"skip\")'></div>
                        </div>";
                $row++;
            }
        }

        $data = check_sregion($locations['subregion']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $lines .= "<div class='row'>
                        <div class='col-md-1'>sRegion</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_town($locations['subregion']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('town', $d['town_id']);
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Town</div>
                        <div class='col-md-1'>{$d['town_id']}</div>
                        <div class='col-md-6'>{$d['town_name']} ({$d['province_name']})</div>
                        <div class='col-md-1'>$estab_count</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"town\", {$d['town_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_suburb($locations['subregion']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('suburb', $d['suburb_id']);

                $lines .= "<div class='row'>
                        <div class='col-md-1'>Suburb</div>
                        <div class='col-md-1'>{$d['suburb_id']}</div>
                        <div class='col-md-6'>{$d['suburb_name']} ({$d['town_name']}, {$d['country_name']})</div>
                        <div class='col-md-1'>{$estab_count}</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"suburb\", {$d['suburb_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

    } else if (!empty($locations['region'])) {
        $region_class ='greenBox';

        $data = check_region($locations['region']);
        if (!empty($data)) {
            foreach ($data as $d) {

                if ($d['parent_type'] == 'region') {
                    $type = 'subRegion';
                } else {
                    $type = 'region';
                }

                $lines .= "<div class='row'>
                        <div class='col-md-1'>{$type}</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_province($locations['region']);
        if (!empty($data)) {
            foreach ($data as $d) {

                $lines .= "<div class='row'>
                    <div class='col-md-1'>Province</div>
                    <div class='col-md-1'>{$d['province_id']}</div>
                    <div class='col-md-6'>{$d['province_name']}</div>
                    <div class='col-md-1'>N/A</div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"province\", {$d['province_id']}, \"checked\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"delete\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"skip\")'></div>
                        </div>";
                $row++;
            }
        }

        $data = check_sregion($locations['region']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $lines .= "<div class='row'>
                        <div class='col-md-1'>sRegion</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"cregion\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"cregion\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"cregion\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_town($locations['region']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('town', $d['town_id']);
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Town</div>
                        <div class='col-md-1'>{$d['town_id']}</div>
                        <div class='col-md-6'>{$d['town_name']} ({$d['province_name']})</div>
                        <div class='col-md-1'>$estab_count</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"town\", {$d['town_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_suburb($locations['region']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('suburb', $d['suburb_id']);

                $lines .= "<div class='row'>
                        <div class='col-md-1'>Suburb</div>
                        <div class='col-md-1'>{$d['suburb_id']}</div>
                        <div class='col-md-6'>{$d['suburb_name']} ({$d['town_name']}, {$d['country_name']})</div>
                        <div class='col-md-1'>{$estab_count}</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"suburb\", {$d['suburb_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"suburb\", {$d['suburb_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

    } else if (!empty($locations['province'])) {

        $location_found = true;
        $province_class = 'greenBox';

        $data = check_province($locations['province']);
        if (!empty($data)) {
            foreach ($data as $d) {

                $lines .= "<div class='row'>
                    <div class='col-md-1'>Province</div>
                    <div class='col-md-1'>{$d['province_id']}</div>
                    <div class='col-md-6'>{$d['province_name']}</div>
                    <div class='col-md-1'>N/A</div>
                    <div class='col-md-1 center'><input type='radio' value='{$d['province_id']}' name='update$row' onchange='setChecked(\"province\", {$d['province_id']}, \"checked\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='{$d['province_id']}' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"delete\")'></div>
                    <div class='col-md-1 center'><input type='radio' value='{$d['province_id']}' name='update$row' onclick='setChecked(\"province\", {$d['province_id']}, \"skip\")'></div>
                        </div>";
                $row++;
            }
        }

        $data = check_country($locations['province']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Country</div>
                        <div class='col-md-1'>{$d['country_id']}</div>
                        <div class='col-md-6'>{$d['country_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='country-{$d['country_id']}' name='update$row' onchange='setChecked(\"country\", {$d['country_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='country-{$d['country_id']}' name='update$row' onclick='setChecked(\"country\", {$d['country_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='country-{$d['country_id']}' name='update$row' onclick='setChecked(\"country\", {$d['country_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_region($locations['province']);
        if (!empty($data)) {
            foreach ($data as $d) {

                if ($d['parent_type'] == 'region') {
                    $type = 'subRegion';
                } else {
                    $type = 'region';
                }

                $lines .= "<div class='row'>
                        <div class='col-md-1'>{$type}</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='{$d['country_id']}' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='{$d['country_id']}' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='{$d['country_id']}' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_sregion($locations['province']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Region</div>
                        <div class='col-md-1'>{$d['region_id']}</div>
                        <div class='col-md-6'>{$d['region_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"region\", {$d['region_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"region\", {$d['region_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

        $data = check_town($locations['province']);
        if (!empty($data)) {
            foreach ($data as $d) {
                $estab_count = count_establishments('town', $d['town_id']);
                $lines .= "<div class='row'>
                        <div class='col-md-1'>Town</div>
                        <div class='col-md-1'>{$d['town_id']}</div>
                        <div class='col-md-6'>{$d['town_name']} ({$d['province_name']})</div>
                        <div class='col-md-1'>$estab_count</div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onchange='setChecked(\"town\", {$d['town_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='1' name='update$row' onclick='setChecked(\"town\", {$d['town_id']}, \"skip\")'></div>
                            </div>";
                $row++;
            }
        }

    } else if (!empty($locations['country'])) {
        if (check_country_done($locations['country']) === false) {
            $location_found = true;

            $country_class = 'greenBox';

            $data = check_country($locations['country']);
            if (!empty($data)) {
                foreach ($data as $d) {

                    $lines .= "<div class='row'>
                        <div class='col-md-1'>Country</div>
                        <div class='col-md-1'>{$d['country_id']}</div>
                        <div class='col-md-6'>{$d['country_name']}</div>
                        <div class='col-md-1'>N/A</div>
                        <div class='col-md-1 center'><input type='radio' value='country-{$d['country_id']}' name='update$row' onchange='setChecked(\"country\", {$d['country_id']}, \"checked\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='country-{$d['country_id']}' name='update$row' onclick='setChecked(\"country\", {$d['country_id']}, \"delete\")'></div>
                        <div class='col-md-1 center'><input type='radio' value='country-{$d['country_id']}' name='update$row' onclick='setChecked(\"country\", {$d['country_id']}, \"skip\")'></div>
                            </div>";
                    $row++;
                }
            }

        }


    }


//Update Counter
file_put_contents('counter.txt', $counter);

$current_location = "<div class='col-md-1 $country_class'><b>{$locations['country']}</b><br><small>Country</small></div>";
$current_location .= "<div class='col-md-2 $province_class'><b>{$locations['province']}</b><br><small>Province</small></div>";
$current_location .= "<div class='col-md-2 $region_class'><b>{$locations['region']}</b><br><small>Region</small></div>";
$current_location .= "<div class='col-md-2 $subregion_class'><b>{$locations['subregion']}</b><br><small>subRegion</small></div>";
$current_location .= "<div class='col-md-1 $town_class'><b>{$locations['town']}</b><br><small>Town</small></div>";
$current_location .= "<div class='col-md-2 $sregion_class'><b>{$locations['sregion']}</b><br><small>sRegion</small></div>";
$current_location .= "<div class='col-md-2 $suburb_class'><b>{$locations['suburb']}</b><br><small>Suburb</small></div>";

//Replace tags
$template = str_replace('<!-- current_location -->', $current_location, $template);
$template = str_replace('<!-- lines -->', $lines, $template);
$template = str_replace('<!-- counter -->', $counter, $template);

echo $template;

function check_suburb($suburb_name) {

    $statement = "SELECT a.suburb_id, a.suburb_name, b.town_name, c.country_name
                    FROM nse_nlocations_suburbs AS a
                    LEFT JOIN nse_nlocations_towns AS b ON a.town_id = b.town_id
                    LEFT JOIN nse_nlocations_countries AS c ON b.country_id = c.country_id
                    WHERE a.suburb_name = :suburb_name
                      AND a.checked = '-'
                      AND a.delete = 'N' ";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':suburb_name', $suburb_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;

}

function check_sregion($region_name) {
    $statement = "SELECT region_id, region_name
                    FROM nse_nlocations_regions
                    WHERE region_name = :region_name
                      AND checked='-'
                      AND `delete` = 'N'";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':region_name', $region_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;

}

function check_town($town_name) {
    $statement = "SELECT a.town_id, a.town_name, b.province_name
                    FROM nse_nlocations_towns AS a
                    LEFT JOIN nse_nlocations_provinces AS b ON a.province_id = b.province_id
                    WHERE a.town_name = :town_name
                      AND a.checked='-'
                      AND a.delete = 'N'";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':town_name', $town_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;
}

function check_region($region_name) {
    $statement = "SELECT region_id, region_name, parent_type
                    FROM nse_nlocations_cregions
                    WHERE region_name = :region_name
                      AND checked='-'
                      AND `delete` = 'N'";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':region_name', $region_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;
}

function check_country($country_name) {
    $statement = "SELECT country_id, country_name
                    FROM nse_nlocations_countries
                    WHERE country_name = :country_name
                      AND checked='-'
                      AND `delete` = 'N'";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':country_name', $country_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;
}

function check_province($province_name) {
    $statement = "SELECT province_id, province_name
                    FROM nse_nlocations_provinces
                    WHERE province_name = :province_name
                      AND checked='-'
                      AND `delete` = 'N'";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':province_name', $province_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;
}


function count_establishments($location_type, $location_id) {

    $statement = "SELECT COUNT(*) AS c
                    FROM nse_establishment_nlocations
                    WHERE location_type = '$location_type'
                      AND location_id = $location_id";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
//    $sql_select->bindParam(':area_id', $area_id);
    $sql_select->execute();
    $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result['c'];


}

function check_country_done($country_name) {
    $statement = "SELECT COUNT(*) as c
                    FROM nse_nlocations_countries
                    WHERE country_name = :country_name
                      AND checked = 'Y' ";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':country_name', $country_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result['c']>0?true:false;
}


