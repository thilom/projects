<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/08
 * Time: 1:19 PM
 */

include $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';

//Vars
$dir = "{$_SERVER['DOCUMENT_ROOT']}/content/";
$files = array();
$domain = 'bt';

//Read all files
$d = opendir($dir);
while (false !== ($entry = readdir($d))) {

    if (is_dir("$dir$entry")) continue;
    if (substr($entry, -4) != 'html') continue;

   $files[] = $entry;
}


//Read files and save to
foreach ($files as $data) {

    $file_content = file_get_contents($dir.$data);

    //Get title
    $res = preg_match("/<title>(.*)<\/title>/siU", $file_content, $title_matches);
    if (!$res) {
        $title = '';
    } else {
        $title = preg_replace('/\s+/', ' ', $title_matches[1]);
        $title = trim($title);
    }

    //Strip head
    $file_content = substr($file_content, strpos($file_content, '</head>')+7);

    //Page id
    $page_id = substr($data, 0, -5);

    //Write to db
    $statement = "INSERT INTO nse_pages
                      (page_code, page_name, page_content, page_domain)
                    VALUES
                      (:page_code, :page_name, :page_content, :page_domain)";
    $sql_insert = $GLOBALS['dbPDO']->prepare($statement);
    $sql_insert->bindParam(':page_code', $page_id);
    $sql_insert->bindParam(':page_name', $title);
    $sql_insert->bindParam(':page_content', $file_content);
    $sql_insert->bindParam(':page_domain', $domain);
    $sql_insert->execute();
    $sql_insert->closeCursor();
}