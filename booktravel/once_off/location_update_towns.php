<?php
/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/25
 * Time: 9:47 AM
 */

//Includes
require_once "../v2/settings/init.php";

//Vars
$template = file_get_contents('location_update_towns.html');
$counter = 0;

//Get next deletion
$statement = "SELECT a.town_id, a.town_name, b.province_name
                FROM nse_nlocations_towns AS a
                LEFT JOIN nse_nlocations_provinces AS b ON a.province_id = b.province_id
                WHERE a.delete = 'Y'";
$sql_select = $GLOBALS['dbCon']->prepare($statement);
$sql_select->bindParam(':area_id', $area_id);
$sql_select->execute();
$sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
$sql_select->closeCursor();

//Establishment Count
$est_count = count_establishments('town', $sql_result['town_id']);

//SuburbName
$suburb= "{$sql_result['province_name']}, {$sql_result['town_name']}";

//Find other Suburbs
$suburb_list = check_suburb($sql_result['town_name']);
$other_suburbs = '';
if (empty($suburb_list)) {
    $other_suburbs .= "<tr><td>Nothing Found</td></tr>";
} else {
    foreach ($suburb_list AS $data) {

        //Get establishment Count
        $c = count_establishments('suburb', $data['suburb_id']);

        //Keep/Delete/Unksnown icon
        $icon = '';
        if ($data['delete'] == 'Y') {
            $icon = '<i class="fa fa-times" aria-hidden="true" style="color: red"></i>';
            $input = '';
        } else if ($data['delete'] == 'F') {
            $icon = '<i class="fa fa-times-circle" aria-hidden="true" style="color: red"></i>';
            $input = '';
        } else if ($data['checked'] == 'Y') {
            $icon = '<i class="fa fa-check" aria-hidden="true" style="color: green"></i>';
            if ($est_count == 0) {
                $input = '';
            } else {
                $input = "<button class='btn btn-sm btn-info' onclick='setTransfer(\"town\", {$sql_result['town_id']}, \"suburb\", {$data['suburb_id']})'>Transfer</button>";
            }
        } else {
            $icon = '<i class="fa fa-question" aria-hidden="true"></i>';
            if ($est_count == 0) {
                $input = '';
            } else {
                $input = "<button class='btn btn-sm btn-info' onclick='setTransfer(\"town\", {$sql_result['town_id']}, \"suburb\", {$data['suburb_id']})'>Transfer</button>";
            }
        }



        $other_suburbs .= "<tr><td>$icon</td><td>{$data['suburb_name']} ({$data['town_name']}, {$data['country_name']})</td><td>$c</td>
                               <td>$input</td>
                                </tr>";
    }
}

//Find other towns
$town_list = check_town($sql_result['town_name']);
$other_towns = '';
foreach ($town_list AS $data) {

    //Get establishment Count
    $c = count_establishments('town', $data['town_id']);

    //Keep/Delete/Unksnown icon
    $icon = '';
    if ($data['town_id'] == $sql_result['town_id']) {
        $icon = '<i class="fa fa-arrow-right" aria-hidden="true" style="color: blue"></i>';
        $input = '';
    } else if ($data['delete'] == 'Y') {
        $icon = '<i class="fa fa-times" aria-hidden="true" style="color: red"></i>';
        $input = '';
    } else if ($data['checked'] == 'Y') {
        $icon = '<i class="fa fa-check" aria-hidden="true" style="color: green"></i>';
        if ($est_count == 0) {
            $input = '';
        } else {
            $input = "<button class='btn btn-sm btn-info' onclick='setTransfer(\"town\", {$sql_result['town_id']}, \"town\", {$data['town_id']})'>Transfer</button>";
        }
    } else {
        $icon = '<i class="fa fa-question" aria-hidden="true"></i>';
        if ($est_count == 0) {
            $input = '';
        } else {
            $input = "<button class='btn btn-sm btn-info' onclick='setTransfer(\"town\", {$sql_result['town_id']}, \"town\", {$data['town_id']})'>Transfer</button>";
        }
    }

    $other_towns .= "<tr><td>$icon</td><td>{$data['town_name']} ({$data['province_name']}, {$data['country_name']})</td><td>$c</td>
                        <td style='text-align: right'>$input</td>
                        </tr>";
}

$template = str_replace('<!-- suburb -->', $suburb, $template);
$template = str_replace('<!-- town_id -->', $sql_result['town_id'], $template);
$template = str_replace('<!-- establishment_count -->', $est_count, $template);
$template = str_replace('<!-- location_id -->', $sql_result['town_id'], $template);
$template = str_replace('<!-- other_suburbs -->', $other_suburbs, $template);
$template = str_replace('<!-- other_towns -->', $other_towns, $template);

echo $template;



function count_establishments($location_type, $location_id) {
    $statement = "SELECT COUNT(*) AS c
                    FROM nse_establishment_nlocations
                    WHERE location_type = '$location_type'
                      AND location_id = $location_id";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
//    $sql_select->bindParam(':area_id', $area_id);
    $sql_select->execute();
    $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result['c'];
}

function check_suburb($suburb_name) {
    $suburb_name = "%$suburb_name%";
    $statement = "SELECT a.suburb_id, a.suburb_name, b.town_name, c.country_name, a.checked, a.delete
                    FROM nse_nlocations_suburbs AS a
                    LEFT JOIN nse_nlocations_towns AS b ON a.town_id = b.town_id
                    LEFT JOIN nse_nlocations_countries AS c ON b.country_id = c.country_id
                    WHERE a.suburb_name LIKE :suburb_name";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':suburb_name', $suburb_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;
}

function check_town($town_name) {
    $town_name = "%$town_name%";
    $statement = "SELECT a.town_id, a.town_name, b.province_name, a.checked, a.delete, c.country_name
                    FROM nse_nlocations_towns AS a
                    LEFT JOIN nse_nlocations_provinces AS b ON a.province_id = b.province_id
                    LEFT JOIN nse_nlocations_countries AS c ON a.country_id = c.country_id
                    WHERE a.town_name LIKE :town_name";
    $sql_select = $GLOBALS['dbCon']->prepare($statement);
    $sql_select->bindParam(':town_name', $town_name);
    $sql_select->execute();
    $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
    $sql_select->closeCursor();

    return $sql_result;
}