<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';

//Vars
$invoices = array();


//Get invoice list
$statement = "SELECT a.j_inv_id, a.j_inv_to_company, a.j_inv_date
				FROM nse_invoice AS a
				LEFT JOIN nse_invoice_item AS b ON b.j_invit_invoice=a.j_inv_id
				";
$sql_inv = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);
$sql_inv->execute();
$sql_inv->store_result();
$sql_inv->bind_result($invoice_id, $establishment_code, $invoice_date);
while ($sql_inv->fetch()) {
	$invoice_date2 = date('Y-m', $invoice_date);
	if ($invoice_date2 != '2011-04') continue;
	if ($establishment_code == '0') continue;
	$invoices[$invoice_id] = $establishment_code;
//	echo "$invoice_id :: $establishment_code :: $invoice_date :: $invoice_date2<br>";
}
$sql_inv->close();


//Prepare statement - QA Out
$statement = "SELECT DATE_FORMAT(cancelled_date, '%Y%m') FROM nse_establishment_qa_cancelled WHERE establishment_code=? LIMIT 1";
$sql_qa_out = $GLOBALS['dbCon']->prepare($statement);
echo mysqli_error($GLOBALS['dbCon']);

foreach ($invoices as $invoice_id=>$establishment_code) {
	$sql_qa_out->bind_param('s', $establishment_code);
	$sql_qa_out->execute();
	$sql_qa_out->bind_result($qa_out);
	$sql_qa_out->fetch();
	$sql_qa_out->free_result();
	if ($qa_out > '201104') unset($invoices[$invoice_id]);
}

//CHeck for assessment
$statement = "SELECT count(*) FROM nse_establishment_assessment WHERE invoice_id=?";
$sql_assessment = $GLOBALS['dbCon']->prepare($statement);

foreach ($invoices as $invoice_id=>$establishment_code) {
	$assessment_count = 0;
	$sql_assessment->bind_param('s', $invoice_id);
	$sql_assessment->execute();
	$sql_assessment->bind_result($assessment_count);
	$sql_assessment->fetch();
	$sql_assessment->free_result();
	if ($assessment_count != 0) unset($invoices[$invoice_id]);
}

//Prepare statement - Get estab data
$statement = "SELECT CONCAT(b.firstname, ' ', b.lastname)
				FROM nse_establishment AS a
				LEFT JOIN nse_user AS b ON a.assessor_id=b.user_id
				WHERE a.establishment_code=?
				LIMIT 1";
$sql_user = $GLOBALS['dbCon']->prepare($statement);


//Prepare statement - insert assessment
$statement = "INSERT INTO nse_establishment_assessment (establishment_code, assessment_status, assessor_name, active, invoice_id, assessment_type_2) 
				VALUES (?, 'draft', ?, 'Y', ?, 'fa')";
$sql_insert = $GLOBALS['dbCon']->prepare($statement);

foreach ($invoices as $invoice_id=>$establishment_code) {
	$sql_user->bind_param('s', $establishment_code);
	$sql_user->execute();
	$sql_user->bind_result($assessor_name);
	$sql_user->fetch();
	$sql_user->free_result();
	
	$sql_insert->bind_param('sss', $establishment_code, $assessor_name, $invoice_id);
	$sql_insert->execute();
}

print_r($invoices);
?>
