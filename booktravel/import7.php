<?php
/** 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";

//Prepare statement
$statement = "UPDATE nse_establishment SET accommodation_directory='Y' WHERE establishment_code=?";
$sql_update = $GLOBALS['dbCon']->prepare($statement);

$statement = "SELECT count(*) FROM nse_establishment WHERE establishment_code=?";
$sql_check = $GLOBALS['dbCon']->prepare($statement);

$file_handle = fopen("qa_estabs.csv", "r");
if ($file_handle !== FALSE) {
    while (($data = fgetcsv($file_handle, 1000, ",")) !== FALSE) {
		$establishment_code = $data[0];
		$establishment_name = $data[1];
		if ($establishment_code == 'webcode') continue;
//		echo $establishment_code . "<br>";

		$sql_check->bind_param('s', $establishment_code);
		$sql_check->execute();
		$sql_check->bind_result($count);
		$sql_check->fetch();
		if ($count == '0') echo "$establishment_code - $establishment_name<br>";

		$sql_update->bind_param('s', $establishment_code);
		$sql_update->execute();
	}
}
?>
