function gotoLocation(strUrl, selObj) {
	location = strUrl + "?countryKey=" + selObj.options[selObj.selectedIndex].value;
}

function populate(x){
	for (m=cacheobj.options.length-1;m>0;m--)
	cacheobj.options[m]=null
	selectedarray=eval(x)
	for (i=0;i<selectedarray.length;i++)
	cacheobj.options[i]=new Option(selectedarray[i].value,selectedarray[i].text)
	cacheobj.options[0].selected=true
	document.info.region.value=x
	document.info.province.value=document.info.region.value
	//document.getElementById("lookatme").focus();
}

function populateFromBox(x){
	for (m=cacheobj.options.length-1;m>0;m--)
	cacheobj.options[m]=null
	selectedarray=eval(x)
	for (i=0;i<selectedarray.length;i++)
	cacheobj.options[i]=new Option(selectedarray[i].value,selectedarray[i].text)
	cacheobj.options[0].selected=true
	document.info.region.value=x
	document.info.regionsl.value=document.info.region.value
}

function changeText(txt){}

function movepic(img_name,img_src) {
	document[img_name].src=img_src;
}

/*
 * AA Amazing race form checkers
 */

function check_contestant_form() {
	if (!document.reg.first_name.value) {
		alert('Please provide a first name');
		return false;
	}
	if (!document.reg.last_name.value) {
		alert('Please provide a last name');
		return false;
	}
	if (!emailCheck(document.reg.email.value)) {
		alert('Please provide a valid email address');
		return false;
	}
	if (!emailCheck(document.reg.email1.value)) {
		alert('Please provide a valid email address for your 1st supporter');
		return false;
	}
	if (!emailCheck(document.reg.email2.value)) {
		alert('Please provide a valid email address for your 2nd supporter');
		return false;
	}
	if (!emailCheck(document.reg.email3.value)) {
		alert('Please provide a valid email address for your 3rd supporter');
		return false;
	}
	if (!emailCheck(document.reg.email4.value)) {
		alert('Please provide a valid email address for your 4th supporter');
		return false;
	}
	if (document.reg.email1.value == document.reg.email2.value || 
		document.reg.email1.value == document.reg.email3.value ||
		document.reg.email1.value == document.reg.email4.value ||
		document.reg.email2.value == document.reg.email3.value ||
		document.reg.email2.value == document.reg.email4.value || 
		document.reg.email3.value == document.reg.email4.value) {
		alert('Please make sure you specify four different email addresses!');
		return false;
	}
	return true;
}

function check_friends_form() {
	if (!emailCheck(document.friends.friend1.value)) {
		alert('Please provide a valid email address for your 1st friend');
		return false;
	}
	if (!emailCheck(document.friends.friend2.value)) {
		alert('Please provide a valid email address for your 2nd friend');
		return false;
	}
	return true;
}

function check_pitstop_form() {
	if (document.pitstop.buddy1.value) {
		if (!emailCheck(document.pitstop.buddy1.value)) {
			alert('Please provide a valid email address for your 1st buddy');
			return false;
		}
	}
	if (document.pitstop.buddy2.value) {
		if (!emailCheck(document.pitstop.buddy2.value)) {
			alert('Please provide a valid email address for your 2nd buddy');
			return false;
		}
	}
	if (document.pitstop.email1.value == document.pitstop.buddy1.value || 
		document.pitstop.email1.value == document.pitstop.buddy2.value ||
		document.pitstop.email2.value == document.pitstop.buddy1.value ||
		document.pitstop.email2.value == document.pitstop.buddy2.value ||
		document.pitstop.email3.value == document.pitstop.buddy1.value || 
		document.pitstop.email3.value == document.pitstop.buddy2.value ||
		document.pitstop.email4.value == document.pitstop.buddy1.value || 
		document.pitstop.email4.value == document.pitstop.buddy2.value) {
		alert('Please make sure you specify email addresses that are different from your supporters email addresses!');
		return false;
	}
	if (document.pitstop.self.value == document.pitstop.buddy1.value || 
		document.pitstop.self.value == document.pitstop.buddy2.value) {
		alert('You cannot add your own email address as a buddy!');
		return false;
	}
	return true;
}


/*
 * Default site form checkers
 */
 
function check_availability_form() {
	if (!document.email.subject.value) {
		alert('Please provide a subject');
		return false;
	}
	if (!document.email.message.value) {
		alert('Please provide a message body');
		return false;
	}
	return true;
}

function check_feedback_form() {
	if (!document.feedback.lname.value) {
		alert('Please provide a surname');
		return false;
	}
	if (!document.feedback.email.value) {
		alert('Please provide a valid email address');
		return false;
	}			
	if (!document.feedback.comments.value) {
		alert('Please provide some comments on your stay');
		return false;
	}
	if (!document.feedback.tel.value) {
		alert('Please provide a telephone number');
		return false;
	}
	if (!emailCheck(document.feedback.email.value)) {
		alert('Please supply a valid email address');
		return false;
	}
	return true;
}

function check_company_edit_form() {
	if (!document.edit.company.value) {
		alert('Please provide a company name');
		return false;
	}
	if (!document.edit.assisted.value) {
		alert('Please provide an "assisted" message');
		return false;
	}
	if (!document.edit.full.value) {
		alert('Please provide a "full" message');
		return false;
	}
	return true;
}

function check_subscription_form() {
	if (!emailCheck(document.subscribe.email.value)) {
		alert('Please supply a valid email address');
		return false;
	}
}

function check_reminder_form() {
	if (!emailCheck(document.reminder.email.value)) {
		alert('Please supply a valid email address');
		return false;
	}
}

function check_login_form() {
	if (!document.login.email.value || !document.login.email2.value) {
		alert('Please provide an email address');
		return false;
	}	
	if (!document.login.surname.value) {
		alert('Please enter your surname');
		return false;
	}
	return true;
}

function check_company_add_form() {
	if (!document.add.company.value) {
		alert('Please provide a company name');
		return false;
	}
	if (!document.add.assisted.value) {
		alert('Please provide an "assisted" message');
		return false;
	}
	if (!document.add.full.value) {
		alert('Please provide a "full" message');
		return false;
	}
	return true;
}

function check_enquiry_form() {
	if (!document.enquiry.title.value) {
		alert('Please select a title');
		return false;
	}	
	if (!document.enquiry.fname.value) {
		alert('Please provide your first name');
		return false;
	}	
	if (!document.enquiry.lname.value) {
		alert('Please provide your last name');
		return false;
	}	
	if (!document.enquiry.email.value) {
		alert('Please provide your email address');
		return false;
	}
	if (document.enquiry.email.value != document.enquiry.email2.value) {
		alert('Please make sure your email address and confirmation address match');
		return false;
	}
	if (!emailCheck(document.enquiry.email.value)) {
		alert('Please supply a valid email address');
		return false;
	}
	if (!document.enquiry.arriveDay.value) {
		alert('Please provide an arrival day');
		return false;
	}	
	if (!document.enquiry.departDay.value) {
		alert('Please provide a departure day');
		return false;
	}
	if (!document.enquiry.adults.value) {
		alert('Please specify the number of adults');
		return false;
	}
	if (!document.enquiry.children.value) {
		alert('Please specify the number of children');
		return false;
	}
	if (!document.enquiry.rooms.value) {
		alert('Please specify the number of rooms');
		return false;
	}
	return true;
}

function check_carhire_form() {
	if (!document.carhire.arriveDay.value || !document.carhire.arriveMonth.value || !document.carhire.arriveYear.value) {
		alert('Please provide all arrival date information');
		return false;
	}	
	if (!document.carhire.departDay.value || !document.carhire.departMonth.value || !document.carhire.departYear.value) {
		alert('Please provide all departure date information');
		return false;
	}	
	if (!document.carhire.details.value) {
		alert('Please provide the details of the transportation you will need');
		return false;
	}	
	if (!document.carhire.visit.value) {
		alert('Please provide the area you will be visiting');
		return false;
	}	
	return true;
}

function check_booking_form() {
	if (!document.booking.name.value) {
		alert('Please provide a first name');
		return false;
	}	
	if (!document.booking.surname.value) {
		alert('Please provide a last name');
		return false;
	}	
	if (!document.booking.email.value) {
		alert('Please provide an email address');
		return false;
	}	
	return true;
}

function check_contact_form() {
	if (!document.contact.name.value) {
		alert('Please provide a name');
		return false;
	}	
	if (!document.contact.email.value) {
		alert('Please provide an email address');
		return false;
	}	
	if (!document.contact.comments.value) {
		alert('Please provide your comments');
		return false;
	}	
	return true;
}

function check_call_centre_form() {
	if (!document.info.town.value == "ALL" && !document.info.townManual.value == "") {
		alert('Please either select a town from the list, OR provide a town name');
		return false;
	}	
	if (document.info.town.value == "ALL" && document.info.townManual.value == "") {
		alert('Please supply a town name');
		return false;
	}
	return true;
}

function swapClass(obj,strClassName) {
	obj.className = strClassName;
}


function clearList(obj) 
{
	var list = document.getElementById(obj);
	while (list.length > 0) list.remove(0);
}

var populateRegions = function(t) 
{ 	
	var xml = t.responseXML;
	var regions = xml.getElementsByTagName("region");
	document.getElementById("region")[0]=new Option( 'Select a region', 'null', false );
	for(var i=0; i < regions.length; i++) 
	{
	   var region = regions[i].firstChild.data;
	   var region_id = regions[i].attributes[0].value;
	   document.getElementById("region")[i+1] = new Option( region, region_id, false );
	}
}

var populateTowns = function(t) 
{ 	
	clearList("town");
	var xml = t.responseXML;
	var towns = xml.getElementsByTagName("town");
	document.getElementById("town")[0]=new Option( 'All Towns', 'ALL', false );
	for(var i=0; i < towns.length; i++) 
	{
	   var town = unescape(towns[i].firstChild.data);
	   var town_id = towns[i].attributes[0].value;
	   document.getElementById("town")[i+1] = new Option( town.replace( new RegExp("[+]", "g"), " "), town_id, false );
	}
}

/*var populateTowns = function(t)  
{ 	
	document.getElementById('towndiv').innerHTML = t.responseText;
}*/


//open URL in frame/iframe
function openInFrame(targetFrame,strUrl){
	parent[targetFrame].location = strUrl;
}

function emailCheck (emailStr) {
	/* The following pattern is used to check if the entered e-mail address
	   fits the user@domain format.  It also is used to separate the username
	   from the domain. */
	var emailPat=/^(.+)@(.+)$/
	/* The following string represents the pattern for matching all special
	   characters.  We don't want to allow special characters in the address. 
	   These characters include ( ) < > @ , ; : \ " . [ ]    */
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
	/* The following string represents the range of characters allowed in a 
	   username or domainname.  It really states which chars aren't allowed. */
	var validChars="\[^\\s" + specialChars + "\]"
	/* The following pattern applies if the "user" is a quoted string (in
	   which case, there are no rules about which characters are allowed
	   and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
	   is a legal e-mail address. */
	var quotedUser="(\"[^\"]*\")"
	/* The following pattern applies for domains that are IP addresses,
	   rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
	   e-mail address. NOTE: The square brackets are required. */
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
	/* The following string represents an atom (basically a series of
	   non-special characters.) */
	var atom=validChars + '+'
	/* The following string represents one word in the typical username.
	   For example, in john.doe@somewhere.com, john and doe are words.
	   Basically, a word is either an atom or quoted string. */
	var word="(" + atom + "|" + quotedUser + ")"
	// The following pattern describes the structure of the user
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
	/* The following pattern describes the structure of a normal symbolic
	   domain, as opposed to ipDomainPat, shown above. */
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
	
	
	/* Finally, let's start trying to figure out if the supplied address is
	   valid. */
	
	/* Begin with the coarse pattern to simply break up user@domain into
	   different pieces that are easy to analyze. */
	var matchArray=emailStr.match(emailPat)
	if (matchArray==null) {
	  /* Too many/few @'s or something; basically, this address doesn't
		 even fit the general mould of a valid e-mail address. */
		//alert("Email address seems incorrect (check @ and .'s)")
		return false
	}
	var user=matchArray[1]
	var domain=matchArray[2]
	
	// See if "user" is valid 
	if (user.match(userPat)==null) {
		// user is not valid
		//alert("The email username doesn't seem to be valid.")
		return false
	}
	
	/* if the e-mail address is at an IP address (as opposed to a symbolic
	   host name) make sure the IP address is valid. */
	var IPArray=domain.match(ipDomainPat)
	if (IPArray!=null) {
		// this is an IP address
		  for (var i=1;i<=4;i++) {
			if (IPArray[i]>255) {
				//alert("Email destination IP address is invalid!")
			return false
			}
		}
		return true
	}
	
	// Domain is symbolic name
	var domainArray=domain.match(domainPat)
	if (domainArray==null) {
		//alert("The email domain name doesn't seem to be valid.")
		return false
	}
	
	/* domain name seems valid, but now make sure that it ends in a
	   three-letter word (like com, edu, gov) or a two-letter word,
	   representing country (uk, nl), and that there's a hostname preceding 
	   the domain or country. */
	
	/* Now we need to break up the domain to get a count of how many atoms
	   it consists of. */
	var atomPat=new RegExp(atom,"g")
	var domArr=domain.match(atomPat)
	var len=domArr.length
	if (domArr[domArr.length-1].length<2 || 
		domArr[domArr.length-1].length>3) {
	   // the address must end in a two letter or three letter word.
	   //alert("The address must end in a three-letter domain, or two letter country.")
	   return false
	}
	
	// Make sure there's a host name preceding the domain.
	if (len<2) {
	   var errStr="The email address is missing a hostname!"
	   //alert(errStr)
	   return false
	}
	
	// If we've gotten this far, everything's valid!
	return true;
}
//  End -->

function showDiv(newDiv) {
	document.getElementById(newDiv).className = 'detailsShow';
}

function hideDiv(newDiv) {
	document.getElementById(newDiv).className = 'detailsHide';
}