var execCount = 0;
var currentDiv = "";
var topHeading = "";
var queAllow = true;
var queBool = new Array();
var queUrl = new Array();
var quePost = new Array();
var queLoader = new Array();
var ajaxHistory = new Array();
var postDone = new Array();
var ajaxHistoryCount = 0;
var ajaxHistoryMaxCount = 0;
var recordHistory = true;
var totalBytes = 0;
var queBusy = false;
queBool[0] = false;
queUrl[0] = "";
quePos = 0;
queDone = 0;
currentUrl="";

var advSearchLoc = "";
var advSearchQA = "";
var advSearchIcons = "";

function aaLoading() {
	loaderDiv="<div class=areaLoader><img src=\"/images/ajax_loader.gif\" align=\"absmiddle\" /> Loading...</div>";
	document.getElementById("RegionDropDowns").innerHTML=loaderDiv;
	document.getElementById("AreaBlockHolder").innerHTML=loaderDiv;
	document.getElementById("EstabListHolder").innerHTML=loaderDiv;
}

function uc(val) {
  return encodeURIComponent(val);
}

function loadPage(post) {
  queBool[quePos] = true;
  queUrl[quePos] = "/index.php";
  quePost[quePos] = post;
  queLoader[quePos] = "AjaxLoader";
  quePos++;
}

function getXML() {
  var historyUrl = document.location+"";
  //var historyArr = historyUrl.split("#");
  //if (currentUrl+"" != "") {
//	  if (historyArr[1]!=currentUrl) {
//	  	loadPage("getRegionData="+historyArr[1]);
//	  }
 // }
  url = false;
  if (queBool[queDone] && queBusy == false) {
      url = queUrl[queDone];
      postvalue = quePost[queDone];
      queDone++;
  }
  if (url) {
      xmlHttp = new GetXmlHttpObject()
      if (xmlHttp==null) {
        alert ("Browser does not support HTTP Request")
        return
      }
      if (url.charAt(0)=="-")
        recordHistory = false;
      if (recordHistory) {
        ajaxHistoryCount++;
        if (ajaxHistoryCount > ajaxHistoryMaxCount) {
          ajaxHistoryMaxCount++;
        }
        ajaxHistory[""+ajaxHistoryCount+""]=url;
      }
      recordHistory = true;
      setTimeout("xmlHttp.onreadystatechange=stateChanged",0);
      setTimeout('xmlHttp.open("POST",url,true)',0);
      setTimeout('xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded")',0);
      setTimeout('xmlHttp.setRequestHeader("Content-length", postvalue.length)',0);
      setTimeout('xmlHttp.setRequestHeader("Connection", "close")',0);
      setTimeout('xmlHttp.send("ajaxpost=1&"+postvalue)',0);
  } else {
      //document.getElementById("AjaxLoader").className='hidden';
  }
  setTimeout('getXML()',100);
}

function prevPage() {
  recordHistory = false;
  if (ajaxHistoryCount > 1) {
    ajaxHistoryCount--;
    loadPage(ajaxHistory[""+ajaxHistoryCount+""]);
  }
  recordHistory = true;
}

function refreshPage() {
  recordHistory = false;
  loadPage(ajaxHistory[""+ajaxHistoryCount+""]);
  recordHistory = true;
}

function nextPage() {
  recordHistory = false;
  if (ajaxHistoryCount < ajaxHistoryMaxCount) {
    ajaxHistoryCount++;
    loadPage(ajaxHistory[""+ajaxHistoryCount+""]);
  }
  recordHistory = true;
}

function stateChanged() {
  if (xmlHttp.readyState==1) {
    //document.getElementById("AjaxLoader").className='';
    queBusy = true;
  }
  if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete") {
    execCount++;
    //var debugText = "<strong>Execution: "+execCount+"</strong><br />"+xmlHttp.responseText.replace(/</g,"&lt;");
   // var debugText = "<strong>Execution: "+execCount+"</strong><br />"+xmlHttp.responseText;
    var contentText = xmlHttp.responseText;
    //document.getElementById('AjaxDebug').innerHTML=debugText;
    //var countBytesText = xmlHttp.responseText.replace(/\n/g,"  ");
    //countBytesText = countBytesText.replace(/\t/g,"  ");
    //totalBytes = totalBytes+countBytesText.length;
    //document.getElementById('ByteStats').innerHTML="Last Fetch: "+countBytesText.length+" bytes<br />Total Fetched: "+(Math.round((totalBytes/1024)*100)/100)+"KB";
    var divContent = new Array();
    divContent = contentText.split("{%startDiv:");
    for (var i=1;i<divContent.length;i++) {
      var divIDEndPos = divContent[i].indexOf('%}');
      var contentStartPos = divIDEndPos+2;
      var appendContent = false;
      if ((divContent[i].indexOf('+')) == (divIDEndPos-1)) {
        divIDEndPos--;
        contentStartPos++;
        appendContent = true;
      }
      var divID = divContent[i].substring(0,divIDEndPos);
      //alert(divID);
      var contentEndPos = divContent[i].indexOf('{%endDiv');
      var divInnerHTML = divContent[i].substring(contentStartPos,contentEndPos);
      if (appendContent) {
        document.getElementById(divID).innerHTML=document.getElementById(divID).innerHTML+divInnerHTML;
      } else {
        //alert(divID)
        document.getElementById(divID).innerHTML=divInnerHTML;
      }
      if (divContent[i].charAt(contentEndPos+8)==":") {
        var endEndTag = divContent[i].lastIndexOf('%}');
        if (divContent[i].substring(contentEndPos+9,contentEndPos+19) == "javascript") {
          var endEndTag = divContent[i].lastIndexOf('%}');
          var exeJS = divContent[i].substring(contentEndPos+20,endEndTag);
          setTimeout(exeJS,0);
        } else {
          var endEndTag = divContent[i].lastIndexOf('%}');
          var newLoadPage = divContent[i].substring(contentEndPos+9,endEndTag);
          setTimeout('loadPage(\''+newLoadPage+'\');',0);
        }
      }
    }
    //queAllow = true
    //i = 0;
    //while (queBool[i] == false) {
    //  i++;
    //}
    //if (queBool[i]==true) {
    //  loadPage(queUrl[i])
    //  queBool[i] = false
    //   queUrl[i] = ""
   // }
    
    //document.getElementById("AjaxLoader").className='hidden';
    
    queBusy = false;
  }
}
function GetXmlHttpObject(handler) { 
  var objXMLHttp = null
  if (window.XMLHttpRequest) {
    objXMLHttp = new XMLHttpRequest()
  }
  else if (window.ActiveXObject) {
    objXMLHttp = new ActiveXObject("Microsoft.XMLHTTP")
  }
  return objXMLHttp
}
function encodeIdValue(Id) {
  return escape(document.getElementById(Id).value);
}

function setCheckboxValue(obj) {
  if (obj.checked) {
    loadPage('setvar='+obj.name+'&value='+obj.value);
  } else {
    loadPage('setvar='+obj.name+'&value=');
  }
}

function getRadioValue(obj) {
  for (var i=0; i < obj.length; i++) {
    if (obj[i].checked) {
      return obj[i].value;
    }
  }
}

function getCheckboxValue(obj) {
  returnVal = "";
  for (var i=0; i < obj.length; i++) {
    if (obj[i].checked) {
      returnVal = returnVal + obj[i].value + "|";
    }
  }
}


function hideSelects(action) {
    if (action!='visible'){action='hidden';}
    if (navigator.appName.indexOf("Explorer") || navigator.appName.indexOf("MSIE")) {
            for (var R = 0; R < document.getElementsByTagName("select").length; R++) {
                if (document.getElementsByTagName("select")[R].options) {
                    document.getElementsByTagName("select")[R].style.visibility = action;
                }
            }
        
    }
}

function showLarge(img) {
	document.getElementById('largeImage').src = img;
}

function showHideSearch() {
	if (document.getElementById('search-container').className != 'hidden') {
		document.getElementById('search-container').className = 'hidden';
		document.getElementById('searchShowText').innerHTML = '[show]';
		document.getElementById('moreOptions').className = 'hidden';
	} else {
		document.getElementById('search-container').className = '';
		document.getElementById('searchShowText').innerHTML = '[hide]';
		document.getElementById('moreOptions').className = '';
		
	}
}

function showHideSearchMore() {
	if (document.getElementById('search-more-container').className != 'hidden') {
		document.getElementById('search-more-container').className = 'hidden';
		document.getElementById('searchShowTextMore').innerHTML = '[show]';
	} else {
		document.getElementById('search-more-container').className = '';
		document.getElementById('searchShowTextMore').innerHTML = '[hide]';
	}
}

function searchIconClick(iconid) {
	if (document.getElementById('iconover_'+iconid).className != 'iconover') {
		document.getElementById('iconover_'+iconid).className = 'iconover';
	} else {
		document.getElementById('iconover_'+iconid).className = 'iconover hidden';
	}
}

function hideForm() {
	document.getElementById('FormHolder').className = 'hidden';
	document.getElementById('FormBg').className = 'hidden';
}

function showForm() {
	document.getElementById('FormHolder').className = '';
	document.getElementById('FormBg').className = '';
	document.getElementById('FormBg').style.height = document.documentElement.scrollHeight+"px";
	document.getElementById('Logo').scrollIntoView();
}

function openGuestReview(code) {
	var guestReviewDiv = document.getElementById('GuestReview');
	guestReviewDiv.style.width=getDocWidth()+"px";
	guestReviewDiv.style.height=getDocHeight()+"px";
	guestReviewDiv.className='';
	//document.getElementById('GuestReviewContent').innerHTML = '<iframe style="border:0;" width="700" height="500" src="http://dev001.aatravel.co.za/survey/survey.php?code='+code+'" />';
}

function closeGuestReview() {
	var guestReviewDiv = document.getElementById('GuestReview');
	guestReviewDiv.style.width="1px";
	guestReviewDiv.style.height="1px";
	guestReviewDiv.className='hidden';
}

function openAdvancedSearch() {
	//var advSearchDiv = document.getElementById('AdvancedSearch');
	//advSearchDiv.style.width=getDocWidth()+"px";
	//advSearchDiv.style.height=getDocHeight()+"px";
	//advSearchDiv.className='';
	loadPage('getAdvSearch=accommodation/places_to_stay/southern_africa/south_africa');
	advSearchLoc = 'accommodation/places_to_stay/southern_africa/south_africa';
	advSearchQA = "";
}

function submitAdvSearch() {
	for(i=0;i<14;i++) {
		advSearchLoc = advSearchLoc.replace("/",",");
	}
	if (advSearchQA=="") {
		advSearchQA = " ";
	}
	if (advSearchIcons=="") {
		advSearchIcons = " ";
	}
	document.location='/advsearch/'+advSearchLoc+'/'+advSearchQA+'/'+advSearchIcons+'/page_1/';
	//alert('/advsearch/'+advSearchLoc+'/1/'+advSearchQA+'/'+advSearchIcons+'/');
}

function advSearchIconClick(obj,value) {
	if (obj.className == "advSearchIconOff") {
		advSearchIcons = advSearchIcons.replace(value+",","");
		advSearchIcons = advSearchIcons+value+",";
		obj.className = "advSearchIconOn";
	} else {
		advSearchIcons = advSearchIcons.replace(value+",","");
		obj.className = "advSearchIconOff";
	}
}

function closeAdvancedSearch() {
	var advSearchDiv = document.getElementById('AdvancedSearch');
	advSearchDiv.style.width="1px";
	advSearchDiv.style.height="1px";
	advSearchDiv.className='hidden';
}

function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

function getDocWidth() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollWidth, D.documentElement.scrollWidth),
        Math.max(D.body.offsetWidth, D.documentElement.offsetWidth),
        Math.max(D.body.clientWidth, D.documentElement.clientWidth)
    );
}

function setTabFromURL() {
	docLoc = document.location+"";
	selectedTab = docLoc.split("#");
	//alert(selectedTab.length);
	if (selectedTab.length == 2) {
		sTab = selectedTab[1].split("_");
		estabTabClicked(document.getElementById(sTab[0]));
		if (sTab[1] == "tabthank") {
			document.getElementById('EstablishmentQuoteThank').innerHTML = "<h1>Thank you</h1><br />Thank you for your enquiry, the establishment will be in contact with you shortly. Would you like to <a href='http://www.essentialtravelinfo.com/index.php?p=flights' target='_blank'>book a flight</a> or <a href='http://www.essentialtravelinfo.com/index.php?p=car_hire' target='_blank'>hire a car</a>?<br /><br /><input type='button' value='Book a flight' onclick='javascript:window.open(\"http://www.essentialtravelinfo.com/index.php?p=flights\")' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' value='Hire a car' onclick='javascript:window.open(\"http://www.essentialtravelinfo.com/index.php?p=car_hire\")' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' value='Close' onclick='javascript:estabTabClicked(document.getElementById(\"EstablishmentTabInfo\"))' /><br /><br />";
			document.getElementById('EstablishmentQuoteThank').style.display = 'block';
			document.getElementById('EstablishmentQuoteForm').style.display = 'none';
			estabTabClicked(document.getElementById("EstablishmentTabQuote"));
		}
	}
}

function estabTabClicked(obj) {
	document.getElementById('EstablishmentTabInfo').className = 'establishmentTab';
	document.getElementById('EstablishmentTabRates').className = 'establishmentTab';
	document.getElementById('EstablishmentTabQuote').className = 'establishmentTab';
	document.getElementById('EstablishmentTabReviews').className = 'establishmentTab';
	document.getElementById('EstablishmentTabMap').className = 'establishmentTab';
	document.getElementById('EstablishmentTabGallery').className = 'establishmentTab';
	obj.className = 'establishmentTab establishmentTabSelected';
	
	document.getElementById('EstablishmentInfoGallery').className = 'hidden';
	document.getElementById('EstablishmentDescription').className = 'hidden';
	document.getElementById('EstablishmentQuoteContent').className = 'hidden';
	document.getElementById('EstablishmentReviewContent').className = 'hidden';
	document.getElementById('EstablishmentMap').className = 'hidden';
	document.getElementById('EstablishmentImageGallery').className = 'hidden';
	document.getElementById('EstablishmentRates').className = 'hidden';
	document.getElementById('EstablishmentRightContent').className = '';
	document.location='#'+obj.id+"_tab";
	
	
	if (obj.id == 'EstablishmentTabInfo') {
		document.getElementById('EstablishmentInfoGallery').className = '';
		document.getElementById('EstablishmentDescription').className = '';
		document.getElementById('EstablishmentMap').className = '';
	}
	if (obj.id == 'EstablishmentTabQuote') {
		document.getElementById('EstablishmentQuoteContent').className = '';
	}
	if (obj.id == 'EstablishmentTabRates') {
		document.getElementById('EstablishmentRates').className = '';
		document.getElementById('EstablishmentInfoGallery').className = '';
	}
	if (obj.id == 'EstablishmentTabReviews') {
		document.getElementById('EstablishmentReviewContent').className = '';
	}
	if (obj.id == 'EstablishmentTabMap') {
		document.getElementById('EstablishmentMap').className = '';
	}
	if (obj.id == 'EstablishmentTabGallery') {
		document.getElementById('EstablishmentImageGallery').className = '';
		document.getElementById('EstablishmentRightContent').className = 'hidden';
	}
	
}

function requestQuote() {
	document.getElementById('CheckIn').value = document.getElementById('RightCheckIn').value;
	document.getElementById('CheckOut').value = document.getElementById('RightCheckOut').value;
	if (parseInt(document.getElementById('RightTotalGuests').value) != 0) {
		document.getElementById('Adults').value = document.getElementById('RightTotalGuests').value;
	} else {
		document.getElementById('TotalGuests').value = 1;
	}
	estabTabClicked(document.getElementById('EstablishmentTabQuote'));
}

function submitQuote() {
	if(document.getElementById('CheckIn').value == '') {
		alert('Please select a check in date');
		return false;
	}
	if(document.getElementById('CheckOut').value == '') {
		alert('Please select a check out date');
		return false;
	}
	if(document.getElementById('CheckOut').value == '') {
		alert('Please select a check out date');
		return false;
	}
	if((document.getElementById('CheckOut').value).replace(/-/gi,"") < (document.getElementById('CheckIn').value).replace(/-/gi,"")) {
		alert('The check-out cannot be earlier than the check-in date.\nPlease select a new check-out date.');
		return false;
	}
	if(parseInt(document.getElementById('TotalGuests').value) < 1) {
		alert('Please specify at least 1 guest');
		return false;
	}
	if(document.getElementById('FirstName').value == '') {
		alert('Please enter your first name');
		return false;
	}
	if(document.getElementById('LastName').value == '') {
		alert('Please enter your last name');
		return false;
	}
	if(document.getElementById('LastName').value == '') {
		alert('Please enter your last name');
		return false;
	}
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if(reg.test(document.getElementById('Email').value) == false) {
		alert('Please enter a valid email address');
		return false;
	}
	if (document.getElementById('Email').value != document.getElementById('ConfirmEmail').value) {
		alert('The confirmed email address does not match');
		return false;
	}
	if(document.getElementById('TelNo').value == '') {
		alert('Please enter your tel number');
		return false;
	}
	if(document.getElementById('CellNo').value == '') {
		alert('Please enter your cell number');
		return false;
	}
}

function guestCount() {
	if (parseInt(document.getElementById('Adults').value)) {
		//do nothing
	} else {
		document.getElementById('Adults').value = 0;
	}
	if (parseInt(document.getElementById('Children').value)) {
		//do nothing
	} else {
		document.getElementById('Children').value = 0;
	}
	document.getElementById('TotalGuests').value = (parseInt(document.getElementById('Adults').value)+parseInt(document.getElementById('Children').value));
}


getXML();