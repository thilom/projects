<?php
/**
 * Index page
 *
 * Set default variables and takes care of page mapping according to $_GET['p']. Defaults to '/contents/home.html'.
 * The script appends '.html' to $_GET['p'] and looks for the page in '/contents/'. If the page is not found it includes
 * a page according to a switch case and finally failng that it loads the default '/contents/home.html'.
 *
 * The script makes use of a template with pre-defined tags which are replaced with relevant content. The tags follow the
 * HTML comment format '<!-- tag_name -->'. This helps to hide tags in case they are not replaced by the script. Included files must
 * fill $content with the contents/body of the page.
 * 
 * -- Updated to use new directory '/e/' - Thilo(2009)
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category general
 *
 */
session_start();
//Initialize Variables
$content = '';
$breadcrumbs = '&nbsp;';
$page = '';
$page_title = 'AA Travel Guides - Trusted for Quality Assured Accommodation in South Africa';
$page_description = "Trusted for Quality Assured Accommodation in South Africa, AA Travel Guides is the largest online travel database in South Africa for accommodation in hotels, B &amp; B's, guest houses, self catering chalets and apartments, lodges, game and nature reserves throughout South Africa; including Cape Town, the Garden Route, Knysna, George, Durban, Sun City, Sabi Sands,  and the Kruger National Park. We also cover Namibia, Zimbabwe, Botswana, Zambia, Mozambique Lesotho and Swaziland. Plan your ideal safari, visit the Cape Winelands, tour the Garden Route. Find out where to stay wherever you go in southern Africa";
$page_keywords = "Quality Assured Accommodation, south africa accommodation, accommodation, south africa, accommodation south africa, South African accommodation, sa accommodation, sa tourism";
$booking_link = '/index.php?p=real_time_bookings';

//Includes & Setup
include_once 'aa_init/init.php';

//Get Template
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/site_template.html');

//Get page
if (isset($_GET['p'])) {
	$page = $_GET['p'];
	if (is_file($_SERVER['DOCUMENT_ROOT'] . "/content/$page.html")) {
		include 'page.php';
	} else {
		switch ($page) {
			case 'search':
			case 'search2':
				include $_SERVER['DOCUMENT_ROOT'] . '/e/search_new.php';
				if (isset($_POST['searchString'])) $template = str_replace('<!-- search_string -->', $_POST['searchString'], $template);
				if (isset($_POST['search_loc'])) $template = str_replace('search_loc_status', $_POST['search_loc'], $template);
				if (isset($_POST['search_estab'])) $template = str_replace('search_estab_status', $_POST['search_estab'], $template);
				break;
			case 'asearch';
				header('Location: http://booktravel.travel/asearch');
				include $_SERVER['DOCUMENT_ROOT'] . '/e/asearch.php';
				break;

			case 'map':
				include $_SERVER['DOCUMENT_ROOT'] . '/e/google.php';
				break;
			case 'feedback':
				header("location:/guest_review/".$_GET["id"]);
				include 'feedback2.php';
				break;
			case 'successenquiry':
				include $_SERVER['DOCUMENT_ROOT'] . '/e/enquire.php';
				break;
			case 'enquiry':
				include $_SERVER['DOCUMENT_ROOT'] . '/e/enquire.php';
				break;
			case 'subscription':
				include 'subscribe_thanks.php';
				break;
			case 'order_process':
				include 'order_process.php';
				break;
			case 'reviews':
				include $_SERVER['DOCUMENT_ROOT'] . '/e/reviews.php';
				break;
			case 'book_now':
				include 'mailcontent/workshops2009.php';
				break;
			case 'error':
				include $_SERVER['DOCUMENT_ROOT'] . '/e/errors.php';
				break;
			default:
				$content = file_get_contents('content/home.html');
		}
	}
} else {
	$content = file_get_contents('content/home.html');
}


//Replace Tags
$template = str_replace('<!-- body -->', $content, $template);
$template = str_replace('<!-- breadcrumbs -->', $breadcrumbs, $template);
$template = str_replace('<!-- page_title -->', $page_title, $template);
$template = str_replace('<!-- page_description -->', $page_description, $template);
$template = str_replace('<!-- page_keywords -->', $page_keywords, $template);
$template = str_replace('<!-- copyright_date -->', date('Y'), $template);
$template = str_replace('<!-- bookings_link -->', $booking_link, $template);
$template = str_replace('<!-- search_string -->', '', $template);

//Serve
echo $template;


?>