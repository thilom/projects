<?php 

class api {
	private $config;
	private $apiParams = "";
	private $apiMethod = "";
	private $apiType = "";

	function __construct() {
		global $config;
		$config = new config();

        //Log Function Stats
        //$search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}

	public function getApiXML() {

		global $config;
		global $apiMethod;
		global $apiType;
		global $apiParams;

		$postFields = "";
		$postFields .= "userid=".urlencode($config->apiUser)."&userpass=".urlencode($config->apiPass);
		$postFields .= "&".$apiMethod."=".$apiType;
		$postFields .= "&".$apiParams."&ip=".urlencode($_SERVER["REMOTE_ADDR"])."&weburl=".urlencode($_SERVER["REQUEST_URI"]);
		$postFields .= "&webtype=aa";
		
		$curlAPI = curl_init($config->apiUrl);
		
 		curl_setopt($curlAPI,CURLOPT_POST,1);
 		curl_setopt($curlAPI,CURLOPT_POSTFIELDS,$postFields);
 		curl_setopt($curlAPI,CURLOPT_HEADER,0);
 		curl_setopt($curlAPI,CURLOPT_RETURNTRANSFER,1);
 		$curlFetched = curl_exec($curlAPI);
//        echo "<pre>"; var_dump($curlFetched); echo '</pre>';
 		curl_close($curlAPI);
 		
 		$apiMethod = "";
 		$apiParams = "";
 		$apiType = "";
		$apiSimpleXML = new SimpleXMLElement($curlFetched);

		//Log Function Stats
        //$search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";

		return $apiSimpleXML;
	}
	
	
	public function setMethod($method) {
		global $apiMethod;
		$apiMethod = urlencode($method);

        //Log Function Stats
        //$search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}
	
	
	public function setType($type) {
		global $apiType;
		$apiType = urlencode($type);

        //Log Function Stats
        //$search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}
	
	public function addParam($name,$value) {
		global $apiParams;
		if ($apiParams != "") {
			$apiParams .= "&";
		}
		$apiParams .= urlencode($name)."=".urlencode($value);

        //Log Function Stats
        //$search_string = __FILE__ . "|" . __FUNCTION__;
        // include $_SERVER['DOCUMENT_ROOT'] . "/shared/function_log.php";
	}

}?>