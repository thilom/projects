<?php

session_start();


//Initialize Logging
//require_once './classes/logger.class.php';
//$start_time = microtime(true);
//$last_time = microtime(true);
//$logClass = new Logger();
//$enable_logging = true;
//$session = SHA1(session_id() . microtime(true));
//$log_order = 0;

if (!isset($_POST['locationSearch'])) {

	if (strpos($_SERVER["HTTP_HOST"], "booktravel.travel") > -1) {
		$count = file_get_contents("domain_counts/booktravel.travel");
		$count++;
		file_put_contents("domain_counts/booktravel.travel", $count);
		//header("location: http://booktravel.travel".$_SERVER["REQUEST_URI"]);
	} else {
		$count = file_get_contents("domain_counts/booktravel.travel");
		$count++;
		file_put_contents("domain_counts/booktravel.travel", $count);
	}

	if (strpos($_SERVER["HTTP_HOST"], "aatravel.co.zw") > -1) {
		if (strpos($_SERVER["REQUEST_URI"], "zimbabwe") > -1) {
			header("location: http://booktravel.travel" . $_SERVER["REQUEST_URI"]);
		} else {
			header("location: http://booktravel.travel/accommodation/places_to_stay/southern_africa/zimbabwe/");
		}
	}
}

/*
if (strpos($_SERVER["HTTP_HOST"],"dev") > -1) {

} else {
	if (strpos($_SERVER["REQUEST_URI"],"zimbabwe") > -1) {
		if (!(strpos($_SERVER["HTTP_HOST"],"aatravel.co.zw") > -1)) {
			header("location: http://www.aatravel.co.zw".$_SERVER["REQUEST_URI"]);
		}
	} else {
		if (!(strpos($_SERVER["HTTP_HOST"],"booktravel.travel") > -1)) {
			header("location: http://booktravel.travel".$_SERVER["REQUEST_URI"]);
		}
	}
}*/

if (isset($_GET["p"])) {
	include('index2.php');
} else if (preg_match('@/accommodation/*@', $_SERVER['REQUEST_URI']) && !is_establishment()) {
    //Redirect to version 2
	include 'v2/index.php';
} else {
	include('class.config.php');
	include('class.api.php');
	include('class.control.php');

	$control = new control;
}

//End script timer and write to db
//$end_time = microtime(true);
//$search_string['GET'] = $_GET;
//$search_string['POST'] = $_POST;
//$search_string = json_encode($search_string);
//$run_time = $end_time - $start_time;
//if ($enable_logging === true) $logClass->log_curl_time($search_string, $run_time);

function is_establishment() {

	$location_parts = explode('/', rtrim($_SERVER['REQUEST_URI'], '/'));
	$location_parts = array_reverse($location_parts);

	$parts = explode('_', $location_parts[0]);

	if (count($parts)> 0 && strlen($parts[0]) == 6) {
		return true;
	}  else {
		return false;
	}
}