<?php header("Content-Type:text/xml");

$db_host = 'dedi2042.nur4.host-h.net';
$db_user = 'aatravel_1';
$db_password = 'i7tigi7g';
$db_name = 'aatravel_db1';

function dbConnect($server,$username,$password) {
  mysql_connect($server,$username,$password);
}

function dbSelect($database) {
  $bool = mysql_select_db($database);
  if (!$bool) {
    //addError("Unable to connect to database: ".$database);
  }
}

function dbQuery($query) {
  $rsQuery = mysql_query($query);
  if (@@mysql_error()) {
    $queryArr["error"]["isError"] = true;
    $queryArr["error"]["errorMsg"] = @@mysql_error();
    echo @@mysql_error();
  } else {
    $queryArr["error"]["isError"] = false;
    $queryArr["error"]["errorMsg"] = "No Errors";
    $queryArr["queryInfo"]["numRows"] = mysql_numrows($rsQuery);
    $queryArr["queryInfo"]["insertID"] = false;
    if (mysql_insert_id()) {
        $queryArr["queryInfo"]["insertID"] = mysql_insert_id();    
    }
    for($i=0;$i<mysql_num_fields($rsQuery);$i++) {
      $field = mysql_fetch_field($rsQuery, $i);
      for($ii=0;$ii<mysql_num_rows($rsQuery);$ii++) {
        $queryArr[$field->name][$ii] = mysql_result($rsQuery,$ii,$field->name);
      }
    }
  }
  return $queryArr;
}

function sfs($string) {
	return mysql_real_escape_string($string);
}

function nfs($int) {
	if (is_numeric($int)) {
		return $int;
	} else {
		echo "NFS: Invalid integer passed";
		exit();
	}
}

dbConnect($db_host,$db_user,$db_password);
dbSelect($db_name);

if (isset($_GET["fetch"])) {
	$xmlCount = 0;
	$xml = "";
	
	if ($_GET["fetch"] == "estimages") {
		$query = "SELECT image_name, thumb_name, primary_image, image_title, image_description
			FROM nse_establishment_images
			WHERE establishment_code LIKE '".sfs($_GET["estcode"])."'
			ORDER BY primary_image DESC, image_name";
		$rsGetImages = dbQuery($query);
		for ($i=0;$i<$rsGetImages["queryInfo"]["numRows"];$i++) {
			if (is_file("/usr/www/users/aatrae/res_images/".$rsGetImages["image_name"][$i])) {
				$xmlCount++;
				$xml .= "<image>
					<name>".htmlspecialchars(utf8_encode($rsGetImages["image_name"][$i]))."</name>
					<thumb>".htmlspecialchars(utf8_encode($rsGetImages["thumb_name"][$i]))."</thumb>
					<primary>".htmlspecialchars(utf8_encode($rsGetImages["primary_image"][$i]))."</primary>
					<title>".htmlspecialchars(utf8_encode($rsGetImages["image_title"][$i]))."</title>
					<description>".htmlspecialchars(utf8_encode($rsGetImages["image_description"][$i]))."</description>
				</image>\n";
			}
		}
	}
	
	if ($_GET["fetch"] == "estbase") {
		$query = "SELECT establishment_name, street_address_line1, street_address_line2, street_address_line3, postal_address_line1,
			postal_address_line2, postal_address_line3, postal_address_code, general_number, nightsbridge_bbid, prebook_bid, website_url
			FROM nse_establishment
			WHERE establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetBase = dbQuery($query);
		for ($i=0;$i<$rsGetBase["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<base>
				<name>".htmlspecialchars(utf8_encode($rsGetBase["establishment_name"][$i]))."</name>
				<add1>".htmlspecialchars(utf8_encode($rsGetBase["street_address_line1"][$i]))."</add1>
				<add2>".htmlspecialchars(utf8_encode($rsGetBase["street_address_line2"][$i]))."</add2>
				<add3>".htmlspecialchars(utf8_encode($rsGetBase["street_address_line3"][$i]))."</add3>
				<postal1>".htmlspecialchars(utf8_encode($rsGetBase["postal_address_line1"][$i]))."</postal1>
				<postal2>".htmlspecialchars(utf8_encode($rsGetBase["postal_address_line2"][$i]))."</postal2>
				<postal3>".htmlspecialchars(utf8_encode($rsGetBase["postal_address_line3"][$i]))."</postal3>
				<postalcode>".htmlspecialchars(utf8_encode($rsGetBase["postal_address_code"][$i]))."</postalcode>
				<tel>".htmlspecialchars(utf8_encode($rsGetBase["general_number"][$i]))."</tel>
				<bbid>".htmlspecialchars(utf8_encode($rsGetBase["nightsbridge_bbid"][$i]))."</bbid>
				<prebid>".htmlspecialchars(utf8_encode($rsGetBase["prebook_bid"][$i]))."</prebid>
				<url>".utf8_encode($rsGetBase["website_url"][$i])."</url>
			</base>";
		}
	}
	
	if ($_GET["fetch"] == "estresv") {
		$query = "SELECT reservation_tel, reservation_fax, reservation_cell, reservation_email,
			reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code
			FROM nse_establishment_reservation
			WHERE establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetResv = dbQuery($query);
		for ($i=0;$i<$rsGetResv["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<resv>
				<tel>".htmlspecialchars(utf8_encode($rsGetResv["reservation_tel"][$i]))."</tel>
				<fax>".htmlspecialchars(utf8_encode($rsGetResv["reservation_fax"][$i]))."</fax>
				<cell>".htmlspecialchars(utf8_encode($rsGetResv["reservation_cell"][$i]))."</cell>
				<email>".htmlspecialchars(utf8_encode($rsGetResv["reservation_email"][$i]))."</email>
				<postal1>".htmlspecialchars(utf8_encode($rsGetResv["reservation_postal1"][$i]))."</postal1>
				<postal2>".htmlspecialchars(utf8_encode($rsGetResv["reservation_postal2"][$i]))."</postal2>
				<postal3>".htmlspecialchars(utf8_encode($rsGetResv["reservation_postal3"][$i]))."</postal3>
				<postalcode>".htmlspecialchars(utf8_encode($rsGetResv["reservation_postal_code"][$i]))."</postalcode>
			</resv>";
		}
	}
	
	if ($_GET["fetch"] == "estdesc") {
		$query = "SELECT establishment_description, description_type
			FROM nse_establishment_descriptions
			WHERE language_code LIKE 'EN' AND establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetDesc = dbQuery($query);
		for ($i=0;$i<$rsGetDesc["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<desc>
				<type>".htmlspecialchars(utf8_encode($rsGetDesc["description_type"][$i]))."</type>
				<description>".htmlspecialchars(utf8_encode($rsGetDesc["establishment_description"][$i]))."</description>
			</desc>";
		}
	}
	
	if ($_GET["fetch"] == "estprice") {
		$query = "SELECT price_description
				FROM nse_establishment_pricing
				WHERE establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetPrice = dbQuery($query);
		if ($rsGetPrice["queryInfo"]["numRows"] < 1) {
			$query = "SELECT price
			FROM nse_establishment_deprecated
			WHERE establishment_code LIKE '".sfs($_GET["estcode"])."'";
			$rsGetPrice = dbQuery($query);
		}
		for ($i=0;$i<$rsGetPrice["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<price>
				<description>".htmlspecialchars(utf8_encode($rsGetPrice["price_description"][$i]))."</description>
			</price>";
		}
	}
	
	if ($_GET["fetch"] == "esticons") {
		$query = "SELECT icon_url, icon_name, icon_description FROM nse_establishment_icon
			INNER JOIN nse_icon ON nse_establishment_icon.icon_id = nse_icon.icon_id
			INNER JOIN nse_icon_lang ON nse_establishment_icon.icon_id = nse_icon_lang.icon_id
			WHERE establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetIcons = dbQuery($query);
		for ($i=0;$i<$rsGetIcons["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<icon>
				<name>".htmlspecialchars(utf8_encode($rsGetIcons["icon_name"][$i]))."</name>
				<description>".htmlspecialchars(utf8_encode($rsGetIcons["icon_description"][$i]))."</description>
				<url>".htmlspecialchars(utf8_encode($rsGetIcons["icon_url"][$i]))."</url>
			</icon>";
		}
	}
	
	if ($_GET["fetch"] == "estqas") {
		$query = "SELECT aa_category_icon
			FROM nse_establishment
			INNER JOIN nse_aa_category ON nse_establishment.aa_category_code = nse_aa_category.aa_category_code
			WHERE establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetQas = dbQuery($query);
		for ($i=0;$i<$rsGetQas["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<qas>
				<icon>".htmlspecialchars(utf8_encode($rsGetQas["aa_category_icon"][$i]))."</icon>
			</qas>";
		}
	}
	
	if ($_GET["fetch"] == "estrating") {
		$query = "SELECT smilies
			FROM nse_feedback_cache
			WHERE category_code LIKE 'total' AND establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetRating = dbQuery($query);
		for ($i=0;$i<$rsGetRating["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<rating>
				<smilies>".htmlspecialchars(utf8_encode($rsGetRating["smilies"][$i]))."</smilies>
			</rating>";
		}
	}
	
	if ($_GET["fetch"] == "estlocation") {
		$query = "SELECT province_name, country_name, town_name, suburb_name, gps_latitude, gps_longitude
			FROM nse_establishment_location
			LEFT JOIN nse_location_province ON nse_establishment_location.province_id = nse_location_province.province_id
			LEFT JOIN nse_location_country_lang ON nse_establishment_location.country_id = nse_location_country_lang.country_id
			LEFT JOIN nse_location_town ON nse_establishment_location.town_id = nse_location_town.town_id
			LEFT JOIN nse_location_suburb ON nse_establishment_location.suburb_id = nse_location_suburb.suburb_id
			WHERE nse_location_country_lang.language_code='EN' AND nse_establishment_location.establishment_code LIKE '".sfs($_GET["estcode"])."'";
		$rsGetLocation = dbQuery($query);
		for ($i=0;$i<$rsGetLocation["queryInfo"]["numRows"];$i++) {
			$xmlCount++;
			$xml .= "<location>
				<province>".htmlspecialchars(utf8_encode($rsGetLocation["province_name"][$i]))."</province>
				<country>".htmlspecialchars(utf8_encode($rsGetLocation["country_name"][$i]))."</country>
				<town>".htmlspecialchars(utf8_encode($rsGetLocation["town_name"][$i]))."</town>
				<suburb>".htmlspecialchars(utf8_encode($rsGetLocation["suburb_name"][$i]))."</suburb>
				<gpslat>".htmlspecialchars(utf8_encode($rsGetLocation["gps_latitude"][$i]))."</gpslat>
				<gpslong>".htmlspecialchars(utf8_encode($rsGetLocation["gps_longitude"][$i]))."</gpslong>
			</location>";
		}
	}
	
	/*if ($_GET["fetch"] == "restypes") {
		$query = "SELECT nse_restype_category.category_id, category_code, category_name FROM nse_restype_category
			INNER JOIN nse_restype_category_lang ON nse_restype_category_lang.category_id = nse_restype_category.category_id
			WHERE toplevel_id = 1 ORDER BY category_name";
		$rsGetRestypes = dbQuery($query);
		for ($i=0;$i<$rsGetRestypes["queryInfo"]["numRows"];$i++) {
			echo "<restype id='".$rsGetRestypes["category_id"][$i]."'>
				<name>".htmlspecialchars($rsGetRestypes["category_name"][$i])."</name>
				<code>".$rsGetRestypes["category_code"][$i]."</code>
			</restype>\n";
		}
	}
	if ($_GET["fetch"] == "provinces") {
		$query = "SELECT province_id, province_name, province_code FROM nse_location_province WHERE country_id = 1 ORDER BY province_name";
		$rsGetProvinces = dbQuery($query);
		for ($i=0;$i<$rsGetProvinces["queryInfo"]["numRows"];$i++) {
			echo "<province id='".$rsGetProvinces["province_id"][$i]."'>
				<name>".htmlspecialchars($rsGetProvinces["province_name"][$i])."</name>
				<code>".$rsGetProvinces["province_code"][$i]."</code>
			</province>\n";
		}
	}
	if ($_GET["fetch"] == "towns") {
		if (!isset($_GET["provinceid"])) {
			echo "<error>Please specify the <em>provinceid<em></error>";
		} else {
			$query = "SELECT town_id, town_name FROM nse_location_town WHERE province_id = ".nfs($_GET["provinceid"])." ORDER BY town_name";
			$rsGetTowns = dbQuery($query);
			for ($i=0;$i<$rsGetTowns["queryInfo"]["numRows"];$i++) {
				echo "<town id='".$rsGetTowns["town_id"][$i]."'>
					<name>".htmlspecialchars($rsGetTowns["town_name"][$i])."</name>
				</town>\n";
			}
		}
	}
	if ($_GET["fetch"] == "suburbs") {
		if (!isset($_GET["townid"])) {
			echo "<error>Please specify the <em>townid<em></error>";
		} else {
			$query = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id = ".nfs($_GET["townid"])." ORDER BY suburb_name";
			//echo $query;
			$rsGetSuburbs = dbQuery($query);
			for ($i=0;$i<$rsGetSuburbs["queryInfo"]["numRows"];$i++) {
				echo "<suburb id='".$rsGetSuburbs["suburb_id"][$i]."'>
					<name>".htmlspecialchars($rsGetSuburbs["suburb_name"][$i])."</name>
				</town>\n";
			}
		}
	}
	
	/*if ($_GET["fetch"] == "results") {
		$query = "SELECT establishment_code.establishment_name FROM nse_establishment";
		$query .= " INNER JOIN nse_establishment_restype ON nse_establishment.establishment_code = nse_establishment_restype.establishment_code";
		$query .= " WHERE 1 = 1";
		if (isset($_GET["restypeid"])) {
			if ($_GET["restypeid"] != '') {
				$query .= " AND ";
			}
		}
	}*/
	
	echo "<travelapi xmlcount='".$xmlCount."'>".$xml."</travelapi>";
	
} else {
	echo "<error>Nothing to fetch</error>";
}?>  