<?php

	//Initialize Variables
	$stop_list = array();
	$order_weight = array();
	$place_weight = array();
	$saturation_weight = array();
	$wordPosition = array();
	$done = array();
	
	$stopList = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/shared/stop_words.txt');
	$stopList = str_replace(array("\r\n", "\n","\r"), '|', $stopList);
	$stopList = str_replace(' ', '', $stopList);
	$stop_list = explode('|', $stopList);
	
	function save_words($title, $word_string, $establishment_code) {
		//echo "<div style='background-color: silver'><b>$establishment_code - $title</b><br>$word_string</div>";
		echo "<div style=''><b>$establishment_code - $title</b></div>";
		$w_ret = explode_and_condense($word_string);
		$t_ret = explode_and_condense($title);
		$word_list = $w_ret['word_list'];
		$wordCount = $w_ret['wordCount'];
		$wordPosition = $w_ret['wordPosition'];
	
		//echo nl2br(print_r($w_ret));
		
		//Prepare statements
		$statement = "DELETE FROM nse_search_drum WHERE establishment_code=?";
		$sql_clear_db = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "INSERT INTO nse_search_drum (word, establishment_code, position, density, title) VALUES (?,?,?,?,?)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		
		
		//Remove establishment  from index
		$sql_clear_db->bind_param('s', $establishment_code);
		$sql_clear_db->execute();
		
		//Index Page (Title First)
		//echo "<div style='font-weight: bold'>Title</div>";
		foreach ($t_ret['wordPosition'] as $position=>$word) {
			$position = 0;
			if (isset($wordCount[$word])) {
				if (count($word_list) < 100) {
					$density = ($wordCount[$word] / 100) * 100 ;
				} else {
					$density = ($wordCount[$word] / count($word_list)) * 100 ;
				}
			} else {
				$density = 0.00;
			}
			$isTitle = '1';
			$sql_insert->bind_param('ssids', $word, $establishment_code, $position, $density, $isTitle);	
			$sql_insert->execute();
			//echo "<div style='margin-left: 10px'>$word - Position: $position, Density: $density</div>";
		}
		
		//echo "<p><div style='font-weight: bold'>Body</div>";
		//echo count($word_list);
		foreach ($wordPosition as $position=>$word) {
			if (isset($wordCount[$word])) {
				if (count($word_list) < 100) {
						$density = ($wordCount[$word] / 100) * 100 ;
					} else {
						$density = ($wordCount[$word] / count($word_list)) * 100 + 1 ;
					}
			} else {
				$density = 0.00;
			}
			$isTitle = '0';
			$sql_insert->bind_param('ssids', $word, $establishment_code, $position, $density, $isTitle);	
			$sql_insert->execute();
			//echo "<div style='margin-left: 10px'>$word({$wordCount[$word]}) - Position: $position, Density: $density</div>";
		}
		
	}
	
	function find($search_string) {
		$pages = array();
		$nPage = '';
		$wordlist = explode_and_condense($search_string);
		$searches = $wordlist['word_list'];
		$codes = '';
		
		$statement =  "SELECT establishment_code, density, title FROM nse_search_drum WHERE word LIKE ? ORDER BY density ASC, title";
		$sql_results = $GLOBALS['dbCon']->prepare($statement);
		
		$statement = "SELECT aa_estab FROM nse_establishment WHERE establishment_code = ?";
		$sql_aa_check = $GLOBALS['dbCon']->prepare($statement);
		
		$i=0;
		//$k = explode(' ', $keyword);
		$statement = "SELECT DISTINCT(establishment_code)
									FROM nse_search_drum 
									WHERE  word LIKE ?";
		$sql_search = $GLOBALS['dbCon']->prepare($statement);
		foreach($searches as $v) {
			$wordResult = array();
			$sWord = "%$v%";
			$sql_search->bind_param('s', $sWord);
			$sql_search->execute();
			$sql_search->bind_result($est);
			$sql_search->store_result();
			while($sql_search->fetch()) {
				$wordResult[] = $est;
			}			
			if (isset($searchResult)) {
				$searchResult = array_intersect($searchResult, $wordResult);
			} else {
				$searchResult = $wordResult;			
			}
		}
		$sql_search->close();

		foreach ($searchResult as $k=>$v) {
			$sql_aa_check->bind_param('s', $v);
			$sql_aa_check->execute();
			$sql_aa_check->bind_result($aa);
			$sql_aa_check->store_result();
			$sql_aa_check->fetch();
			if ($aa != 'Y') unset($searchResult[$k]);
		}
		
		arsort($searchResult);
		foreach ($searchResult as $k=>$p) {
			$nPage[$p]['title'] =  'none';
		}
		return $nPage;
	}
	
	function explode_and_condense($word_string) {		
		$words = strip_tags($word_string);
		
		//Remove punctuation
		$punc = array('.', ';', '"', "'", ',', ':', '/', '\\', '=', '+', '-', '_', '(', ')', '*', '&', '^', '%', '$', '#', '@', '!', '~', '`', '?', '<', '>');
		$words = str_replace($punc, '', $words);
		$words = strtolower($words);
		$word_list = explode(' ', $words);
		
		//Remove stop words
		foreach ($word_list as $k=>$v) {
			if (in_array(strtolower($v), $GLOBALS['stop_list'])) unset($word_list[$k]);
			if (strlen($v) < 3) unset($word_list[$k]);
		}
		
		//Get word positions
		$w['wordPosition'] = str_word_count(join(' ', $word_list), 1);
		
		//Count words
		$w['wordCount'] = array_count_values($word_list);
		
		//Remove duplicates
		$w['word_list'] = $word_list;
		
		return $w ;
	}


?>