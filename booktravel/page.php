<?php



//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . "/aa_init/init.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/page.class.php";

//Vars
$pageClass = new Page();


//Get page Content
$page_data= $pageClass->get_page_by_code($page, 'bt');

if (empty($page_data['page_response']) || $page_data['page_response'] == 200) {
	$content= $page_data['page_content'];
	$page_title = $page_data['page_name'];
} else if ($page_data['page_response'] == 301) {
	header("HTTP/1.0 301 Moved Permanently");
	die();
} else {
	header("HTTP/1.0 404 Not Found");
	die();
}




?>