<?php

//Vars
$base_dir = '/usr/www/users/aatrae';
ini_set("include_path",".:/usr/lib/php:/usr/local/lib/php:/usr/www/users/aatrae");

//Includes
include_once $base_dir . '/aa_init/init.php';

if (check_data_integrity()) add_integrity_code();

function check_data_integrity() {
	//Vars
	$lCode = '';
	
	//Get remote Code
	$ch = curl_init("http://www.za-places.co.za/aatravel/check_db.php");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$rCode = curl_exec($ch);
	curl_close($ch);
	
	//Get Local code
	$statement = "SELECT variable_value FROM nse_variables WHERE variable_name='db_check_code'";
	$sql_code = $GLOBALS['dbCon']->prepare($statement);
	$sql_code->execute();
	$sql_code->bind_result($lCode);
	$sql_code->store_result();
	$sql_code->fetch();
	
	
	//Compare
	if (trim($rCode) != trim($lCode)) {
		$message = "Date: " . date('Y-m-d H:m:s');
		mail('thilo@booktravel.travel', 'Possible DB Roll-back', $message);
		//mail('rory@booktravel.travel', 'Possible DB Roll-back', $message);
		return FALSE;
	}
	
	return TRUE;
	
}

function add_integrity_code() {
	$code = date('YmdHms');
	
	//Update Remote Code
	$ch = curl_init("http://www.za-places.co.za/aatravel/check_db.php?code=$code");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	
	//Save to DB
	$statement = "UPDATE nse_variables SET variable_value=? WHERE variable_name='db_check_code'";
	$sql_code = $GLOBALS['dbCon']->prepare($statement);
	$sql_code->bind_param('s', $code);
	$sql_code->execute();
}

?>