<?php
/**
 * Booktravel/Essential Travel version 2
 *
 * 2016-05-17: Thilo Muller - Created
 */

//Includes
require_once 'settings/init.php';
require_once 'classes/task.class.php';

//Vars
$taskClass = new Task();

//Set defaults
$site_data['page_title'] = $page_title;

//Template
$site_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/$default_template");

//Redirect
if (preg_match('@/accommodation/@', $_SERVER['REQUEST_URI'])) {
    $site_data['body'] = '';
    require_once "controllers/establishment_list.php";
} else {

}

//Replace Tags with Defaults
$site_template = $taskClass->merge_data($site_template, $site_data);

echo $site_template;



