<?php
/**
 * Booktravel / Essential travel establishment list
 *
 * Fetches a list of establishments for a specified location, assembles them into
 * HTML format and displays the result
 *
 * 2016-05-17: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/v2/classes/location_class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/v2/classes/establishment.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/v2/classes/task.class.php';

//Vars
$locationClass = new Location();
$establishmentClass = new Establishment();
$taskClass = new Task();
$establishment_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/establishment_block.html");
$list_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/establishment_lists.html");
$advert_block = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/google_ad_block_1.html");
$location_data = array();
$page_data = array();
$page_data['establishments'] = '';
$page_data['sidebar'] = '';
$page_data['nav_list'] = '';
$restypes = array();
$nightsbridge_codes = array();
$show_establishments = array();
$restype = array();

//Get location id
$location_info = $locationClass->get_id_from_name(trim($_SERVER['REQUEST_URI'], '/'));

//Get Theme - This is the second part of the URL
$parts = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
$theme = str_replace('_', ' ', $parts[1]);

//Get locations
switch ($location_info['location_type']) {
    case 'region':
        $region_primitives = $locationClass->get_region_primitives($location_info['location_id']);
        foreach ($region_primitives as $data) {
            $location_data[$data['location_type']][] = $data['location_id'];
        }
        break;
    case 'cregion':
//        $cregion_primitives = $locationClass->get_cregion_primitives($location_info['location_id']);
//        foreach ($cregion_primitives as $data) {
//            $location_data[$data['location_type']][] = $data['location_id'];
//        }
        break;
    default:
        $location_data[$location_info['location_type']][] = $location_info['location_id'];
}


//Get establishment list
$establishments = $establishmentClass->get_establishments_by_location($location_data, $theme);
$establishments = array_unique($establishments, SORT_REGULAR);

//Lowest location
$page_data['lowest_location'] = $locationClass->get_name_from_id($location_info['location_id'], $location_info['location_type']);

//Nightsbridge
foreach ($establishments as $data) {
    $establishment_data = $establishmentClass->get_establishment($data['establishment_code']);
    if (!empty($establishment_data['nightsbridge_bbid'])) {
//        if ($establishment_data['nightsbridge_bbid'] == '17631' || $establishment_data['nightsbridge_bbid'] == '17632' || $establishment_data['nightsbridge_bbid'] == '17633' )
        $nightsbridge_codes[] = $establishment_data['nightsbridge_bbid'];
    }
}

//NightsBridge Check
if (isset($_COOKIE['searchSettings']) && !empty($_COOKIE['searchSettings'])) {
    $cookie_data = explode('|', $_COOKIE['searchSettings']);
    $api_data = array();
    $api_data['messagename'] = "AvailRQ";
    $api_data['credentials']['nbid'] = $GLOBALS['nbid'];
    $api_data['credentials']['password'] = $GLOBALS['nightsbridge_password'];
    $api_data['bblist'] = array();
    $api_data['bblist']['bbid'] = array();
    foreach ($nightsbridge_codes as $data) {
        $api_data['bblist']['bbid'][] = (int) $data;
    }
    $api_data['startdate'] = date('Y-m-d', strtotime($cookie_data[0]));
    $api_data['enddate'] = date('Y-m-d', strtotime($cookie_data[1]));
    $api['data'] = json_encode($api_data);
    $data = "data={$api['data']}";
    $s = curl_init();
    curl_setopt($s,CURLOPT_URL,$GLOBALS['nightsbridge_api_url']);
    curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($s,CURLOPT_FORBID_REUSE,true);
    curl_setopt($s,CURLOPT_FRESH_CONNECT,true);
    curl_setopt($s,CURLOPT_POST,1);
    curl_setopt($s,CURLOPT_POSTFIELDS,$data);
    $d = curl_exec($s);
    curl_close($s);
    $nightsbridge_result = json_decode($d, true);
    if ($nightsbridge_result['success'] == true) {
        foreach ($nightsbridge_result['data']['bb'] as $nbData) {
            if (!isset($nbData['noavailability'])) {
                $show_establishments[] = $nbData['bbid'];

//                echo "<pre>"; var_dump($nbData); echo '</pre>';
//                foreach ($nbData['packages'] as $pData) {
                $nbrooms[$nbData['bbid']] = '';
                $rateDescrip[$nbData['bbid']] = '';
                $lowestRate[$nbData['bbid']] = 999999.00;
                    foreach ($nbData['roomtypes'] as $rData) {
                        $nbrooms[$nbData['bbid']] += $rData['roomsfree'];
                        foreach ($rData['mealplans'] as $mData) {
                            foreach ($mData['rates'] as $raKey=>$raData) {
                                if ($raKey == 'pax2') {
                                    if ($lowestRate[$nbData['bbid']] > $raData/2) {
                                        $lowestRate[$nbData['bbid']] = $raData/2;
                                        $rateDescrip[$nbData['bbid']] = $rData['ratescheme']==1?$rData['rateschemedesc']:'';
                                    }
                                } else {
                                    if ($lowestRate[$nbData['bbid']] > $raData) {
                                        $lowestRate[$nbData['bbid']] = $raData;
                                        $rateDescrip[$nbData['bbid']] = $rData['ratescheme']==1?$rData['rateschemedesc']:'';
                                    }
                                }

                            }
                        }
                    }
//                }

            }
        }
    }
}


//Draw establishments
$total_establishments = 0;
foreach ($establishments as $estabKey=>$data) {
    $establishment_data = $establishmentClass->get_establishment($data['establishment_code']);

    //Send imageless and descriptionless establishments to the end
    //TODO: Check for imageless establishments
    //TODO: Fix! establishments not added to estab search result
    if (empty($establishment_data['establishment_description'])) {
        unset($establishments[$estabKey]);
        $establishments[] = $data;
        continue;
    }


    if ($total_establishments < 10) {
        $block_data = array();
        $block_data['host_address'] = $_SERVER['HTTP_HOST'];
        $block_data['establishment_code'] = $data['establishment_code'];
        $block_data['establishment_name'] = $establishment_data['establishment_name'];
        $block_data['establishment_description'] = $establishment_data['establishment_description'];
        $block_data['restype_name'] = $establishment_data['restype_name'];
        $block_data['restype_description'] = $establishment_data['restype_description'];
        $block_data['accommodation_available'] = '';

        //Assemble location
        $link = "/accommodation/places_to_stay/" . (!empty($establishment_data['location']['cregion_name'])?$taskClass->makeURLSafe($establishment_data['location']['cregion_name']):'southern_africa');
        $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['country_name']);
        $block_data['location'] = "<a href='$link'>{$establishment_data['location']['country_name']}</a>";
        if (!empty($establishment_data['location']['province_name'])) {
            $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['province_name']);
            $block_data['location'] .= ' &middot; ' . "<a href='$link'>{$establishment_data['location']['province_name']}</a>";
        }
        if (!empty($establishment_data['location']['town_name'])) {
            $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['town_name']);
            $block_data['location'] .= ' &middot; ' . "<a href='$link'>{$establishment_data['location']['town_name']}</a>";
        }
        if (!empty($establishment_data['location']['suburb_name'])) {
            $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['suburb_name']);
            $block_data['location'] .= ' &middot; ' . "<a href='$link'>{$establishment_data['location']['suburb_name']}</a>";
        }

        //Establishment link
        $block_data['establishment_link'] = $link . '/' . $taskClass->makeURLSafe("{$data['establishment_code']}_{$establishment_data['establishment_name']}");

        if (!empty($show_establishments)) {
            if (!in_array($establishment_data['nightsbridge_bbid'], $show_establishments) && $establishment_data['premium_member'] !== 'Y') {
                continue;
            } else {
                if (in_array($establishment_data['nightsbridge_bbid'], $show_establishments)) {
                    $block_data['accommodation_available'] = "<div class='ribbon'><span onclick='bookNow(\"{$block_data['establishment_link']}\")' >Available</span></div>";
                }
            }
        }

        //Rooms counts
        if (isset($nbrooms[$establishment_data['nightsbridge_bbid']])) {
            $block_data['rooms'] = $nbrooms[$establishment_data['nightsbridge_bbid']] . ' Room' . ($nbrooms[$establishment_data['nightsbridge_bbid']]==1?'':'s') . ' Available';
            $block_data['rooms'] .= '<br>From R' . number_format($lowestRate[$establishment_data['nightsbridge_bbid']],2);
            $block_data['rooms'] .= '<br>' .$rateDescrip[$establishment_data['nightsbridge_bbid']];
        } else {
            if (empty($establishment_data['room_count'])) {
                $block_data['rooms'] = '';
            } else {
                $block_data['rooms'] = $establishment_data['room_count'];
                if (empty($establishment_data['room_type'])) {
                    $block_data['rooms'] .= " Rooms/Units";
                } else {
                    $block_data['rooms'] .= " {$establishment_data['room_type']}";
                }
            }
        }

        $establishment_block = $establishment_template;
        $establishment_block = $taskClass->merge_data($establishment_block, $block_data);
        $page_data['establishments'] .= $establishment_block;
    }

    if ($total_establishments == 0) {
        $page_data['establishments'] .= $advert_block;
    }

    $total_establishments++;

    if (!empty($show_establishments) && !in_array($establishment_data['nightsbridge_bbid'], $show_establishments)) {
        continue;
    }

    //Add to JS Nav List
    $page_data['nav_list'] .= "{$data['establishment_code']}-{$establishment_data['nightsbridge_bbid']}|";

    //Restype Count
    foreach ($establishment_data['themes'] as $data) {
        if (isset($restype[$data['theme_name']])) {
            $restype[$data['theme_name']]++;
        } else {
            $restype[$data['theme_name']] = 1;
        }
    }
}

//Navigation
//TODO: Move HTML to an external template
$page_data['nav_list'] = trim($page_data['nav_list'], '|');
$nav_inner_template = '<li id="nav_<!-- number -->" class="<!-- enabled --> <!-- active -->" data-page="<!-- number -->">
                        <span onclick="showNav(<!-- number -->)"><!-- number --></span>
                    </li>';
$page_count = ceil($total_establishments/10);
$page_data['navigation'] = '';
for ($i=1; $i<$page_count+1; $i++) {
    $nav_data = array('number' => $i, 'enabled' => '', 'active' => '');
    if ($i == 6) {
        break;
    }
    if ($i == 1) {
        $nav_data['active'] = 'active';
    }

    $page_data['navigation'] .= $taskClass->merge_data($nav_inner_template, $nav_data);
}


//Sidebar - Restypes and themes
foreach ($restype as $key=>$val) {
    $themeLink = '';
    foreach ($parts as $c=>$part) {
        if ($c == 1) {
            $themeLink .= "/" . $taskClass->makeURLSafe($key);
        } else {
            $themeLink .= "/$part";
        }
    }
    $page_data['sidebar'] .= "<a href='$themeLink'>$key</a><br>";
}

//Page Header
$description_list = '';
$theme = ucwords($theme);
$page_data['page_header'] = "{$theme} in {$page_data['lowest_location']}";
$page_data['page_subheader'] = "";
for ($i=2; $i<count($parts)-1; $i++) {
    $part = ucwords(urldecode(str_replace('_', ' ', $parts[$i])));
    $page_data['page_subheader'] .= "{$part} - ";
    $description_list = "{$part}, " . $description_list;
}
$page_data['page_subheader'] = substr($page_data['page_subheader'], 0, -2);
$description_list = substr($description_list, 0 ,-2);


$site_data['body'] = $taskClass->merge_data($list_template, $page_data);
$site_data['page_title'] = $page_data['page_header'] . ', ' .$page_data['page_subheader'];
$site_data['page_description'] = $page_data['page_header'] . ' (' .$description_list . ')';
$site_data['page_keywords'] = '';

