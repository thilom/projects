<?php

/**
 * Booktravel / Essential travel location functions
 *
 * 2016-05-17: Thilo Muller - Created
 */


class Location {

    /**
     * Place holder for the last error
     *
     * @var string
     */
    private $last_error = '';

    function get_id_from_name($location) {

        //Vars
        $location_data = false;

        //Break up the location
        $location_parts = explode('/', $location);
        $location_parts = array_reverse($location_parts);

        //Check first part
        $location_name = str_replace('_', ' ', urldecode($location_parts[0]));
        $statement = "SELECT location_id, location_type
                        FROM nse_nlocations_search
                        WHERE location_name = :location_name";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':location_name', $location_name);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        $sql_result = array_map("unserialize", array_unique(array_map("serialize", $sql_result)));
        if (count($sql_result) > 1) {
            //Get location data for the second part
            $parent_name = str_replace('_', ' ', urldecode($location_parts[1]));
            $statement = "SELECT location_id, location_type
                        FROM nse_nlocations_search
                        WHERE location_name = :location_name";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->bindParam(':location_name', $parent_name);
            $sql_select->execute();
            $sql_result2 = $sql_select->fetch(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();

            foreach ($sql_result as $data) {

                switch ($data['location_type']) {
                    case 'suburb':
                        //Get parent
                        $statement = "SELECT town_id
                                        FROM nse_nlocations_suburbs
                                        WHERE suburb_id = :suburb_id
                                        LIMIT 1";
                        $sql_select = $GLOBALS['dbCon']->prepare($statement);
                        $sql_select->bindParam(':suburb_id', $data['location_id']);
                        $sql_select->execute();
                        $sql_result3 = $sql_select->fetch(PDO::FETCH_ASSOC);
                        $sql_select->closeCursor();
                        if ($sql_result3['town_id'] == $sql_result2['location_id']) {
                            $location_data['location_id'] = $data['location_id'];
                            $location_data['location_type'] = 'town';
                        }
                        break;
                    case 'town':
                        //Get parent
                        $statement = "SELECT province_id
                                        FROM nse_nlocations_towns
                                        WHERE town_id = :town_id
                                        LIMIT 1";
                        $sql_select = $GLOBALS['dbCon']->prepare($statement);
                        $sql_select->bindParam(':town_id', $data['location_id']);
                        $sql_select->execute();
                        $sql_result3 = $sql_select->fetch(PDO::FETCH_ASSOC);
                        $sql_select->closeCursor();
                        if ($sql_result3['province_id'] == $sql_result2['location_id']) {
                            $location_data['location_id'] = $data['location_id'];
                            $location_data['location_type'] = 'town';
                        }
                        break;
                    case 'region':

                        break;
                }
            }
        } else if (count($sql_result) == 1) {
            $location_data = $sql_result[0];
        } else {
            $location_data = array('location_id' => 0, 'location_type' => '');
        }

        return $location_data;
    }

    function get_name_from_id($location_id, $location_type) {
        $statement = "SELECT location_name
                      FROM nse_nlocations_search
                      WHERE location_id = :location_id
                        AND location_type = :location_type
                      LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':location_id', $location_id);
        $sql_select->bindParam(':location_type', $location_type);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result['location_name'];
    }

    /**
     * Return an array of suburbs for a specified town or all suburbs
     * if town ID is omitted.
     *
     * @param bool $town_id
     * @return mixed FALSE on error
     */
    function get_suburbs($town_id = false) {
        try {
            $statement = "SELECT suburb_id, suburb_name
                            FROM nse_nlocations_suburbs
                            WHERE active = 'Y' ";
            if ($town_id !== false) $statement .= " AND town_id = :town_id ";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            if ($town_id !== false) $sql_select->bindParam(':town_id', $town_id);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }


    /**
     * Return a list of towns for either a province ID or a country ID. If
     * both are omitted it will return all towns in the database.
     *
     * @param bool|int $province_id
     * @param bool|int $country_id
     * @return bool|mixed
     */
    function get_towns($province_id = false, $country_id = false) {
        try {
            $statement = "SELECT town_id, town_name
                            FROM nse_nlocations_towns
                            WHERE active = 'Y' ";
            if ($province_id !== false) {
                $statement .= " AND province_id = :province_id ";
            } else if ($country_id !== false) {
                $statement .= " AND country_id = :country_id ";
            }
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            if ($province_id !== false) {
                $sql_select->bindParam(':province_id', $province_id);
            } else if ($country_id !== false) {
                $sql_select->bindParam(':country_id', $country_id);
            }
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Return a list of provinces for a specified country or all provinces if country ID is omitted,
     *
     * @param bool|int $country_id
     * @return bool|mixed
     */
    function get_provinces($country_id = false) {
        try {
            $statement = "SELECT province_id, province_name
                            FROM nse_nlocations_provinces";
            if ($country_id !== false) $statement .= " WHERE country_id = :country_id ";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            if ($country_id !== false) $sql_select->bindParam(':country_id', $country_id);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Return a list of all countries.
     *
     * @return bool|mixed
     */
    function get_countries() {
        try {
            $statement = "SELECT country_name, country_id
                            FROM nse_nlocations_countries";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Return a list of all regions.
     *
     * @return false|mixed
     */
    function get_regions() {
        try {
            $statement = "SELECT region_id, region_name
                            FROM nse_nlocations_regions
                             WHERE active = 'Y' ";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Return a list of country regions,
     *
     * @return false|mixed
     */
    function get_cregions() {
        try {
            $statement = "SELECT region_id, region_name
                            FROM nse_nlocations_cregions
                            WHERE active = 'Y' ";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Delete the contents of the search table for re-population.
     *
     * @return bool
     */
    function clear_search_table() {
        try {
            $statement = "DELETE FROM nse_nlocations_search";
            $sql_delete = $GLOBALS['dbCon']->prepare($statement);
            $sql_delete->execute();
            $sql_delete->closeCursor();
            return true;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Populate the location search table. $location_data is a multi-dimensional array
     * containing location_id (from relevant nlocations table), location_name and
     * location_type (suburb, town. province etc.)
     *
     * @param $location_data
     * @return bool
     */
    function insert_search_table($location_data) {
        try {
            $statement = "INSERT INTO nse_nlocations_search
                          (location_id, location_name, location_type)
                      VALUES
                          (:location_id, :location_name, :location_type)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            foreach ($location_data as $data) {
                $sql_insert->bindParam(':location_id', $data['location_id']);
                $sql_insert->bindParam(':location_name', $data['location_name']);
                $sql_insert->bindParam(':location_type', $data['location_type']);
                $sql_insert->execute();
            }
            $sql_insert->closeCursor();
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Get a list of suburbs, towns and provinces for a specified region.
     *
     * @param int $region_id
     * @return bool|mixed
     */
    function get_region_primitives($region_id) {
        try {
            $statement = "SELECT a.location_id, a.location_type
                            FROM nse_nlocations_regions_content AS a
                            LEFT JOIN nse_nlocations_regions AS b ON a.region_id = b.region_id
                            WHERE a.region_id = :region_id
                              AND b.active = 'Y' ";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->bindParam(':region_id', $region_id);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Get a list of suburbs, towns and provinces for a specified cregion.
     *
     * @param int $cregion_id
     * @return bool|mixed
     */
    function get_cregion_primitives($cregion_id) {
        try {
            $statement = "SELECT location_id, location_type
                            FROM nse_nlocations_cregion_locations
                            WHERE region_id = :region_id";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->bindParam(':region_id', $cregion_id);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            return $sql_result;
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }
    }
    

    /**
     * Return the last error generated
     *
     * @return string
     */
    function get_last_error() {
        return $this->last_error;
    }

    /**
     * Return a count of establishments grouped by theme for suburbs
     *
     * @param $suburb_id
     * @param int $theme_id
     * @return mixed
     */
    function get_suburb_establishment_count($suburb_id) {

        $statement = "SELECT COUNT(*) AS theme_count, c.theme_id, d.theme_name
                        FROM nse_establishment_nlocations AS a
                        LEFT JOIN nse_establishment AS b ON a.establishment_code = b.establishment_code
                        LEFT JOIN nse_establishment_themes AS c ON a.establishment_code = c.establishment_code AND c.type='aa'
                        LEFT JOIN nse_themes AS d ON c.theme_id = d.theme_id
                        WHERE location_id = :suburb_id
                          AND location_type = 'suburb'
                        GROUP BY c.theme_id";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':suburb_id', $suburb_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Return a count of establishments grouped by theme for towns
     *
     * @param $suburb_id
     * @param int $theme_id
     * @return mixed
     */
    function get_town_establishment_count($town_id) {

        $statement = "SELECT COUNT(*) AS theme_count, c.theme_id, d.theme_name
                        FROM nse_establishment_nlocations AS a
                        LEFT JOIN nse_establishment AS b ON a.establishment_code = b.establishment_code
                        LEFT JOIN nse_establishment_themes AS c ON a.establishment_code = c.establishment_code AND c.type='aa'
                        LEFT JOIN nse_themes AS d ON c.theme_id = d.theme_id
                        WHERE location_id = :town_id
                          AND location_type = 'town'
                        GROUP BY c.theme_id";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':town_id', $town_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Return province, countrp and region for a specified town,
     *
     * @param $town_id
     * @return mixed
     */
    function get_town_parents($town_id) {

        $statement = "SELECT b.province_name, c.country_name, c.country_id, e.region_name
                        FROM nse_nlocations_towns AS a
                        LEFT JOIN nse_nlocations_provinces AS b ON a.province_id = b.province_id
                        LEFT JOIN nse_nlocations_countries AS c ON c.country_id = a.country_id
                        LEFT JOIN nse_nlocations_regions_content AS d ON d.location_id=c.country_id
                        LEFT JOIN nse_nlocations_regions AS e ON d.region_id = e.region_id
                        WHERE a.town_id = :town_id
                        LIMIT 1";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':town_id', $town_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }



}