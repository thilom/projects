<?php

/**
 * Manage establinhment res Types
 *
 * 2016-05-18: Thilo Muller - Created
 */

class Restype {

    function get_restypes() {
        $statement = "SELECT restype_name,
                          (SELECT COUNT(*) FROM nse_establishment_restype WHERE) AS restype_count
                        FROM nse_restype_subcategory_lang";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':area_id', $area_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();
    }
}