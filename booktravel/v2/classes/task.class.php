<?php

/**
 * Booktravel / Essential Travel general tasks
 *
 * 2016-05-17: Thilo Muller - Created
 *
 */
class Task {

    /**
     * Merge page data into the template.
     *
     * By default, the function will replace '<!-- tag -->' but can be changed by adding the $placeholder_start and
     * $placeholder_end variables to the function call. Tags are replaced according to the array passed to the function.
     * Where the array key is the tag to replace eg. $merge_data['tag']=>'value' will replace <!-- tag --> with 'value'.
     *
     *
     * @param string $template The template to merge data into
     * @param mixed[] $merge_data An array of values to merge into the template
     * @param string $placeholder_start The start of the placeholder
     * @param string $placeholder_end The end of the placeholder
     * @return string The merged template
     */
    function merge_data($template, $merge_data, $placeholder_start = '<!-- ', $placeholder_end = ' -->') {
        foreach ($merge_data as $merge_key=>$merge_value) {
            $template = str_replace("{$placeholder_start}$merge_key{$placeholder_end}", $merge_value, $template);
        }
        return $template;
    }

    /**
     * Makes a string safe to use as part of a URL.
     *
     * @param string $urlString
     * @return string
     */
    function makeURLSafe($urlString) {
        $urlString = str_replace(' ','_', $urlString);
        $urlString = urlencode($urlString);
        return $urlString;
    }
}