<?php

/**
 * Booktravel / Essential Travel establishment related functions
 *
 * 2016-05-17: Thilo Muller - Created
 */



class Establishment {

    /**
     * Place holder for latest error
     * @var string
     */
    private $last_error = '';

    /**
     * Returns a list of establishment codes for a specified location.
     * $location_data is an array with the location_type as the key and
     * location ID as the value. eg. $location_data['suburb'] = array(12,242,45);
     *
     * @param int[] $location_data
     * @return bool|mixed[]
     */
    function get_establishments_by_location($location_data, $theme = '') {
        $result = array();
        foreach ($location_data as $location_type=>$location_ids) {
            $in_list = '';
            foreach ($location_ids as $id) {
                $in_list .= "$id,";
            }
            $in_list = substr($in_list, 0, -1);
            $statement = "SELECT DISTINCT a.establishment_code
                            FROM nse_establishment_nlocations AS a
                            LEFT JOIN nse_establishment AS b ON a.establishment_code = b.establishment_code
                            LEFT JOIN nse_establishment_themes AS c ON a.establishment_code = c.establishment_code
                            LEFT JOIN nse_themes AS d ON c.theme_id = d.theme_id
                            WHERE a.location_id IN ($in_list)
                              AND location_type = :location_type
                              AND theme_name = :theme_name
                              AND b.aa_estab = 'Y'
                              ORDER BY premium_member DESC, RAND()";

            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->bindParam(':location_type', $location_type);
            $sql_select->bindParam(':theme_name', $theme);
            $sql_select->execute();
            $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
            $result = array_merge($result, $sql_result);
            return $result;
        }
    }


    /**
     * Get establishment data
     *
     * @param string $establishment_code
     * @return bool|mixed[]
     */
    function get_establishment($establishment_code) {
        try {
            $statement = "SELECT a.establishment_name, b.establishment_description, c.room_count, c.room_type,
                              e.subcategory_name AS restype_name, e.restype_description, a.nightsbridge_bbid, a.premium_member
                        FROM nse_establishment AS a
                        LEFT JOIN nse_establishment_descriptions AS b ON a.establishment_code = b.establishment_code
                                                                        AND description_type='short_description'
                        LEFT JOIN nse_establishment_data AS c ON a.establishment_code = c.establishment_code
                        LEFT JOIN nse_establishment_restype AS d ON a.establishment_code = d.establishment_code
                        LEFT JOIN nse_restype_subcategory_lang AS e ON d.subcategory_id = e.subcategory_id AND e.language_code = 'EN'
                        WHERE a.establishment_code = :establishment_code
                        LIMIT 1";
            $sql_select = $GLOBALS['dbCon']->prepare($statement);
            $sql_select->bindParam(':establishment_code', $establishment_code);
            $sql_select->execute();
            $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
            $sql_select->closeCursor();
        } catch (PDOException $e) {
            $this->last_error = $e->getMessage();
            return false;
        }

        //Get establishment location
        $statement = "SELECT MAX(b.country_name) AS country_name, MAX(c.province_name) AS province_name, MAX(d.town_name) AS town_name,
		MAX(e.suburb_name) AS suburb_name, MAX(f.region_name) AS region_name, MAX(g.region_name) AS cregion_name
                        FROM nse_establishment_nlocations AS a
                        LEFT JOIN nse_nlocations_countries AS b ON a.location_id = b.country_id AND a.location_type='country'
                        LEFT JOIN nse_nlocations_provinces AS c ON a.location_id = c.province_id AND a.location_type='province'
                        LEFT JOIN nse_nlocations_towns AS d ON a.location_id = d.town_id AND a.location_type='town'
                        LEFT JOIN nse_nlocations_suburbs AS e ON a.location_id = e.suburb_id AND a.location_type='suburb'
                        LEFT JOIN nse_nlocations_cregions AS f ON a.location_id = f.region_id AND a.location_type='region'
                        LEFT JOIN nse_nlocations_regions AS g ON a.location_id = g.region_id AND a.location_type='cregion'
                        WHERE a.establishment_code = :establishment_code";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':establishment_code', $establishment_code);
        $sql_select->execute();
        $sql_result['location'] = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        //Get establishment Themes
        $statement = "SELECT theme_name
                        FROM nse_establishment_themes AS a
                        LEFT JOIN nse_themes AS b ON a.theme_id = b.theme_id
                        WHERE establishment_code = :establishment_code
                          AND type = 'aa'";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->bindParam(':establishment_code', $establishment_code);
        $sql_select->execute();
        $sql_result['themes'] = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }


    /**
     * Return the last error generated
     *
     * @return string
     */
    function get_last_error() {
        return $this->last_error;
    }


    function insert_image($establishment_code, $image_name, $thumb_name, $primary_image, $image_title, $image_description) {
        require "{$_SERVER['DOCUMENT_ROOT']}/v2/settings/init.php";
        $insert_date = time();


        //Check if image exists
        $statement = "SELECT image_id, date
                        FROM nse_establishment_images
                        WHERE image_name = :image_name
                        LIMIT 1";
        $sql_select = $dbCon->prepare($statement);
        $sql_select->bindParam(':image_name', $image_name);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();
        if (!empty($sql_result['image_id'])) {

            if (time()-$sql_result['date'] > 86400*30) {
                //Remove image
                $statement = "DELETE FROM nse_establishment_images
                                WHERE image_id = :image_id
                                LIMIT 1";
                $sql_remove = $dbCon->prepare($statement);
                $sql_remove->bindParam(':image_id', $sql_result['image_id']);
                $sql_remove->execute();
                $sql_remove->closeCursor();

                //Insert new image
                try {
                    $statement = "INSERT INTO nse_establishment_images
                                  (establishment_code, image_name, thumb_name, primary_image, image_title, image_description, date)
                                VALUES
                                  (:establishment_code, :image_name, :thumb_name, :primary_image, :image_title, :image_description, :date)";
                    $sql_insert = $dbCon->prepare($statement);
                    $sql_insert->bindParam(':establishment_code', $establishment_code);
                    $sql_insert->bindParam(':image_name', $image_name);
                    $sql_insert->bindParam(':thumb_name', $thumb_name);
                    $sql_insert->bindParam(':primary_image', $primary_image);
                    $sql_insert->bindParam(':image_title', $image_title);
                    $sql_insert->bindParam(':image_description', $image_description);
                    $sql_insert->bindParam(':date', $insert_date);
                    $sql_insert->execute();
                    $sql_insert->closeCursor();

                    return true;
                } catch (PDOException $e) {
                    $this->last_error = $e->getMessage();
                    return false;
                }
            } else {
                return false;
            }
        } else {
            try {
                $statement = "INSERT INTO nse_establishment_images
                                  (establishment_code, image_name, thumb_name, primary_image, image_title, image_description, date)
                                VALUES
                                  (:establishment_code, :image_name, :thumb_name, :primary_image, :image_title, :image_description, :date)";
                $sql_insert = $dbCon->prepare($statement);
                $sql_insert->bindParam(':establishment_code', $establishment_code);
                $sql_insert->bindParam(':image_name', $image_name);
                $sql_insert->bindParam(':thumb_name', $thumb_name);
                $sql_insert->bindParam(':primary_image', $primary_image);
                $sql_insert->bindParam(':image_title', $image_title);
                $sql_insert->bindParam(':image_description', $image_description);
                $sql_insert->bindParam(':date', $insert_date);
                $sql_insert->execute();
                $sql_insert->closeCursor();

                return true;
            } catch (PDOException $e) {
                $this->last_error = $e->getMessage();
                return false;
            }
            return true;
        }
    }


    function get_all_establishments() {
        $statement = "SELECT DISTINCT establishment_code
                        FROM nse_establishment
                        WHERE aa_estab = 'Y'
                          AND active = '1'";
        $sql_select = $GLOBALS['dbCon']->prepare($statement);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

}