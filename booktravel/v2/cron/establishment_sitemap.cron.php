<?php
/**
 * Create a sitemap for establishments
 *
 * 2016-07-20: Thilo Muller - Created
 */

//Check for document root (cron does not have one)
if (!isset($_SERVER['DOCUMENT_ROOT'])) $_SERVER['DOCUMENT_ROOT'] = '/home/booktravel/public_html';

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/v2/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/v2/classes/establishment.class.php";

//Vars
$estClass = new Establishment();
$index_file = 'est_index.xml';
$sitemap_file_prefix = 'est';
$lines_per_file = 2000;

//Get all establishments
$establishments = $estClass->get_all_establishments();

//Calculate number of sitemap files
$index_count = ceil(count($establishments)/$lines_per_file);

//Assemble indexpage
$index_content = '<?xml version="1.0" encoding="UTF-8"?>
                    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
for ($i=0; $i < $index_count; $i++) {
    $index_content .= "\n".'                        <sitemap>
                          <loc>http://'. $GLOBALS['site_url'] .'/sitemaps/'. $sitemap_file_prefix . $i .'.txt</loc>
                          <lastmod>'. date('c') .'</lastmod>
                       </sitemap>';
    echo '/sitemaps/'. $sitemap_file_prefix . $i .'.txt' . PHP_EOL;
}
$index_content .= "\n".'                    </sitemapindex>';

//Save index page
file_put_contents("{$_SERVER['DOCUMENT_ROOT']}/sitemaps/$index_file", $index_content);

//Assemble individual sitemap
$line_count = 0;
$file_count = 0;
foreach ($establishments as $data) {
    if ($line_count === 0) { //Open file for writing
        $handle = fopen("{$_SERVER['DOCUMENT_ROOT']}/sitemaps/{$sitemap_file_prefix}{$file_count}.txt", 'w');
    }

    //Get establishment data
    $establishment_data = $estClass->get_establishment($data['establishment_code']);

    //Assemble link
    $link = "http://www.{$GLOBALS['site_url']}/accommodation/places_to_stay/";
    if (empty($establishment_data['location']['cregion_name'])) {
        $establishment_data['location']['cregion_name'] = 'Southern Africa';
    }

   $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $establishment_data['location']['cregion_name'])))) . '/';
   $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $establishment_data['location']['country_name'])))) . '/';
   $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $establishment_data['location']['province_name'])))) . '/';
   $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $establishment_data['location']['town_name'])))) . '/';
   if (!empty($establishment_data['location']['suburb_name'])) {
        $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $establishment_data['location']['suburb_name'])))) . '/';
    }
    $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', "{$data['establishment_code']}_{$establishment_data['establishment_name']}"))));
    fwrite($handle, $link . PHP_EOL);
    $line_count++;

    if ($line_count == $lines_per_file) {
        fclose($handle);
        $line_count = 0;
        $file_count++;
    }
}