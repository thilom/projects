<?php
/**
 * Booktravel / Essential Travel location search table update.
 *
 * 2016-05-17: Thilo Muller - Created
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

//Includes
require_once '../settings/init.php';
require_once '../classes/location_class.php';

//Vars
$locationClass = new Location();

//Clear search table
if ($locationClass->clear_search_table() === false) {
    echo "An Error Occurred: " . $locationClass->get_last_error();
    die("<br>Stopping Execution");
}

//Get all suburbs, assemble and insert
$insert_list = array();
$suburbs = $locationClass->get_suburbs();
if ($suburbs !== false) {
    foreach ($suburbs as $data) {
        $line = array();
        $line['location_id'] = $data['suburb_id'];
        $line['location_name'] = $data['suburb_name'];
        $line['location_type'] = 'suburb';
        $insert_list[] = $line;
    }
    unset($suburbs);
    if ($locationClass->insert_search_table($insert_list) === false) {
        echo "An Error Occurred while inserting suburbs: " . $locationClass->get_last_error();
        die("<br>Stopping Execution");
    }
} else {
    echo "An Error Occurred while selecting suburbs: " . $locationClass->get_last_error();
    die("<br>Stopping Execution");
}

//Get towns ,assemble and insert
$insert_list = array();
$towns = $locationClass->get_towns();
if ($towns !== false) {
    foreach ($towns as $data) {
        $line = array();
        $line['location_id'] = $data['town_id'];
        $line['location_name'] = $data['town_name'];
        $line['location_type'] = 'town';
        $insert_list[] = $line;
    }
    unset($towns);
    if ($locationClass->insert_search_table($insert_list) === false) {
        echo "An Error Occurred while inserting towns: " . $locationClass->get_last_error();
        die("<br>Stopping Execution");
    }
} else {
    echo "An Error Occurred while selecting towns: " . $locationClass->get_last_error();
    die("<br>Stopping Execution");
}

//Get provinces, assemble and insert
$insert_list = array();
$provinces = $locationClass->get_provinces();
if ($provinces !== false) {
    foreach ($provinces as $data) {
        $line = array();
        $line['location_id'] = $data['province_id'];
        $line['location_name'] = $data['province_name'];
        $line['location_type'] = 'province';
        $insert_list[] = $line;
    }
    unset($provinces);
    if ($locationClass->insert_search_table($insert_list) === false) {
        echo "An Error Occurred while inserting provinces: " . $locationClass->get_last_error();
        die("<br>Stopping Execution");
    }
} else {
    echo "An Error Occurred while selecting provinces: " . $locationClass->get_last_error();
    die("<br>Stopping Execution");
}

//Get Countries, assemble and insert
$insert_list = array();
$countries = $locationClass->get_countries();
if ($countries !== false) {
    foreach ($countries as $data) {
        $line = array();
        $line['location_id'] = $data['country_id'];
        $line['location_name'] = $data['country_name'];
        $line['location_type'] = 'country';
        $insert_list[] = $line;
    }
    unset($countries);
    if ($locationClass->insert_search_table($insert_list) === false) {
        echo "An Error Occurred while inserting countries: " . $locationClass->get_last_error();
        die("<br>Stopping Execution");
    }
} else {
    echo "An Error Occurred while selecting countries: " . $locationClass->get_last_error();
    die("<br>Stopping Execution");
}

//Get regions, assemble and insert
$insert_list = array();
$regions = $locationClass->get_regions();
if ($regions !== false) {
    foreach ($regions as $data) {
        $line = array();
        $line['location_id'] = $data['region_id'];
        $line['location_name'] = $data['region_name'];
        $line['location_type'] = 'region';
        $insert_list[] = $line;
    }
    unset($regions);
    if ($locationClass->insert_search_table($insert_list) === false) {
        echo "An Error Occurred while inserting regions: " . $locationClass->get_last_error();
        die("<br>Stopping Execution");
    }
} else {
    echo "An Error Occurred while selecting regions: " . $locationClass->get_last_error();
    die("<br>Stopping Execution");
}

//Get country regions, assemble and insert
//$insert_list = array();
//$cregions = $locationClass->get_cregions();
//if ($cregions !== false) {
//    foreach ($cregions as $data) {
//        $line = array();
//        $line['location_id'] = $data['region_id'];
//        $line['location_name'] = $data['region_name'];
//        $line['location_type'] = 'cregion';
//        $insert_list[] = $line;
//    }
//    unset($cregions);
//    if ($locationClass->insert_search_table($insert_list) === false) {
//        echo "An Error Occurred while inserting country regions: " . $locationClass->get_last_error();
//        die("<br>Stopping Execution");
//    }
//} else {
//    echo "An Error Occurred while selecting country regions: " . $locationClass->get_last_error();
//    die("<br>Stopping Execution");
//}