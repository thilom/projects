<?php
/**
 * Generate the location sitemap
 *
 * 2016-07-21: Thilo Muller - Created
 */


//Check for document root (cron does not have one)
if (!isset($_SERVER['DOCUMENT_ROOT'])) $_SERVER['DOCUMENT_ROOT'] = '/home/booktravel/public_html';

//Includes
require_once "{$_SERVER['DOCUMENT_ROOT']}/v2/settings/init.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/v2/classes/location_class.php";

//Vars
$locationClass = new Location();
$index_file = 'loc_index.xml';
$sitemap_file_prefix = 'loc';
$lines_per_file = 2000;

//Get towns
$towns = $locationClass->get_towns();

//Get themes


//Iterate through each town
$file_count = 0;
$line_count = 0;
foreach ($towns as $tData) {

    $line_content = array();
    $themes = array();
    $establishment_count = 0;

    //Get parent structure
    $town_parents = $locationClass->get_town_parents($tData['town_id']);

    if ($town_parents['region_name'] == 'Australasia') continue;

    $town_region = urlencode(utf8_encode(strtolower(str_replace(' ', '_', $town_parents['region_name']))));
    $town_country = urlencode(utf8_encode(strtolower(str_replace(' ', '_', $town_parents['country_name']))));
    $town_province = urlencode(utf8_encode(strtolower(str_replace(' ', '_', $town_parents['province_name']))));

    //Get suburbs
    $suburbs = $locationClass->get_suburbs($tData['town_id']);

    //For each suburb: check if establishments are available and add to $line_content
    foreach ($suburbs AS $sData) {
        $suburb_count = $locationClass->get_suburb_establishment_count($sData['suburb_id']);
        foreach ($suburb_count AS $cData) {
            if ($cData['theme_count'] == 0 || empty($cData['theme_name'])) continue;
            $establishment_count += $cData['theme_count'];

            $link = "http://www.{$GLOBALS['site_url']}/accommodation/";
            $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $cData['theme_name'])))) . '/';
            $link .= "$town_region/";
            $link .= "$town_country/";
            if (!empty($town_province)) $link .= "$town_province/";
            $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $tData['town_name'])))) . '/';
            $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $sData['suburb_name'])))) . '/';

            $themes[] = urlencode(utf8_encode(strtolower(str_replace(' ', '_', $cData['theme_name'])))) . '/';

            $line_content[] = $link;
        }
    }

    //Add town
    if ($establishment_count > 0) {
        foreach ($themes AS $theme) {
            $link = "http://www.{$GLOBALS['site_url']}/accommodation/";
            $link .= $theme . '';
            $link .= "$town_region/";
            $link .= "$town_country/";
            if (!empty($town_province)) $link .= "$town_province/";
            $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $tData['town_name'])))) . '/';

            $line_content[] = $link;
        }
    } else {
        $theme_count = $locationClass->get_town_establishment_count($tData['town_id']);

        foreach ($theme_count AS $theme) {

            if ($theme['theme_count'] == 0 || empty($theme['theme_name'])) continue;

            $link = "http://www.{$GLOBALS['site_url']}/accommodation/";
            $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $theme['theme_name'])))) . '/';
            $link .= "$town_region/";
            $link .= "$town_country/";
            if (!empty($town_province)) $link .= "$town_province/";
            $link .= urlencode(utf8_encode(strtolower(str_replace(' ', '_', $tData['town_name'])))) . '/';

            $line_content[] = $link;
        }
    }

    //Write to sitemap files
    if (!empty($line_content)) {

        foreach ($line_content AS $line) {
            if ($line_count === 0) { //Open file for writing
                $handle = fopen("{$_SERVER['DOCUMENT_ROOT']}/sitemaps/{$sitemap_file_prefix}{$file_count}.txt", 'w');
            }
            fwrite($handle, $line . PHP_EOL);
            $line_count++;
            if ($line_count == $lines_per_file) {
                fclose($handle);
                $file_count++;
                $line_count = 0;
            }
        }
    }

}

//Assemble indexpage
$index_content = '<?xml version="1.0" encoding="UTF-8"?>
                    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
for ($i=0; $i <= $file_count; $i++) {
    $index_content .= "\n".'                        <sitemap>
                          <loc>http://'. $GLOBALS['site_url'] .'/sitemaps/'. $sitemap_file_prefix . $i .'.txt</loc>
                          <lastmod>'. date('c') .'</lastmod>
                       </sitemap>';
    echo '/sitemaps/'. $sitemap_file_prefix . $i .'.txt' . PHP_EOL;
}
$index_content .= "\n".'                    </sitemapindex>';

//Save index page
file_put_contents("{$_SERVER['DOCUMENT_ROOT']}/sitemaps/$index_file", $index_content);