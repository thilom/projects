
var settingMode = 0;

$(document).ready(function() {
    updateSettingsDisplay();
});


function launchGallery(imgSrc, imgTitle, imgDescription) {
    $('#modalImage').attr('src', '');
    $('#modalTitle').html('');
    $('#modalDescription').html('');
    $('#modalImage').attr('src', imgSrc);
    $('#modalTitle').html(imgTitle);
    $('#modalDescription').html(imgDescription);
    $('#imageGallery5').modal();
}

function showNightsbridge() {
    $('#estTabs a[href="#availability').tab('show')
}
function hideNightsbridge() {
    document.getElementById('nightsbridge').style.display = 'none';
}

function openReviews() {
    $('#estTabs a[href="#reviews').tab('show');
}

function addReview(id) {
    document.location = "/guest_review/" + id;
}

function openResult() {
    document.getElementById('LocationSearchBox').className=''
}



function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";path=/;" + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function checkCookie(cname) {
    var cookie=getCookie(cname);
    if (cookie!="") {
        return true;
    } else {
        return false;
    }
}

function searchLocation() {

    if ($('#searchString').val() == '') return 0;

    saveSettings();

        url = '/ajax/fetch_locations.ajax.php';
        sData = {'searchString': $('#searchString').val()}

        $.ajax({
            url: url,
            data: sData,
            method: 'post',
            complete: function(data) {
                console.log(data.responseText)

                returnResult = $.parseJSON(data.responseText);
                if (returnResult.count == 1) {
                    $('#LocationSearchBoxError').slideUp();
                    //document.location = '/' + returnResult.last_url;
                } else if (returnResult.count == 0) {
                    $('#LocationSearchBox').slideUp();
                    $('#LocationSearchBoxError').slideDown();
                    $('#pageBody').slideUp();
                } else {
                    $('#LocationSearchBoxError').slideUp();
                    $('#locationBody').html(returnResult.data);
                    $('#LocationSearchBox').slideDown();
                    $('#pageBody').slideUp();
                }

            }
        });
}


function saveSettings() {

    //Vars
    error_count = 0;
    errors = [];
    errorText = '';

    //Check Arrival Date
    if ($('#arrivingDate').val() == '') {
        errors[error_count] = 'Please enter a valid arrival date';
        error_count++;
    }

    //Check Leaving Date
    if ($('#leavingDate').val() == '') {
        errors[error_count] = 'Please enter a valid leaving date';
        error_count++;
    }

    if (checkCookie('searchSettings')) {
        setCookie('searchSettings', '', -20);
    }

    if(!checkCookie('new_visitor')) {
        cookieText = 'false';
        setCookie('new_visitor', cookieText, 0.5);
    }

    if ($('#searchString').val() != '') {
        cookieString = 'l|' + $('#searchString').val();
    } else if ($('#searchNameString').val() != '') {
        cookieString = 'e|' + $('#searchNameString').val();
    } else {
        cookieString = '|';
    }

    cookieText = $('#arrivingDate').val() + '|' + $('#leavingDate').val() + '|' + cookieString;
    setCookie('searchSettings', cookieText, 0.5);
    updateSettingsDisplay();



}

function updateSettingsDisplay() {

    var new_visitor = checkCookie('new_visitor');

    if (checkCookie('searchSettings') && getCookie('searchSettings') != '|') {
        d = getCookie('searchSettings').split('|');
        if (moment(d[0]).isSameOrBefore(moment())) {
            setCookie('searchSettings', '', -20);
        }
    }

    if (checkCookie('searchSettings')) {
        if (getCookie('searchSettings') == '|') {

        } else {
            data = getCookie('searchSettings').split('|');
            //peopleCount = parseInt(data[2]) + parseInt(data[3]);

            //Format Dates
            if (data[0] != '') {
                start = moment(data[0]).format('D MMMM YYYY');
                end = moment(data[1]).format('D MMMM YYYY');
            } else {
                start = '';
                end = '';
            }

            $('#arrivingDate').val(start);
            $('#leavingDate').val(end);
            if (data[2] == 'l') {
                $('#searchString').val(data[3])
            } else if (data[2] == 'e') {
                $('#searchNameString').val(data[3])
            }

        }

    } else {
    }
}


function showNav(pageNumber) {
    if (pageNumber == 999) {
        activeElement = $("#establishmentNavigation li.active").data('page');
        pageNumber = activeElement + 1;
        if (pageNumber > $("#establishmentNavigation li").length-2) return 0;
    }

    if (pageNumber == 0) {
        activeElement = $("#establishmentNavigation li.active").data('page');
        pageNumber = activeElement - 1;
        if (pageNumber == 0) return 0;
    }

    navs = $('#navList').val().split('|');
    navs = navs.slice((pageNumber*10)-10, pageNumber*10);
    navs = navs.join('|');

    $('#nav_' + pageNumber + ' span').html('<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>');

    $.ajax({
        url: '/v2/ajax/establishment_list.ajax.php?ids=' + navs,
        complete: function(data) {
            $('#establishmentContent').html(data.responseText);
            $('#nav_' + pageNumber + ' span').html(pageNumber);

            //Clear active states and set current page active
            for (i=1; i<6; i++) {
                $('#nav_' + i).removeClass('active');
            }
            $('#nav_' + pageNumber).addClass('active');
        }
    });
}

function bookNow(url) {
    document.location = url;
}

function startSearch() {
    if ($('#searchString').val() != '') {
       searchLocation();
    }

    if ($('#searchNameString').val() != '') {
        searchEstab();
    }
}

function searchEstab() {
    saveSettings();
    document.location='/search/'+encodeURI(document.getElementById('searchNameString').value)
}