<?php
/**
 * Send registration information
 *
 * 2016-06-22: Thilo Muller - Created
 */

//Includes
require_once '../lib/swiftmailer-5.x/lib/swift_required.php';

//Vars
$email_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/email/email_master.html");
$content_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/email/email_register.html");
$captcha_privatekey = "6LfKaCMTAAAAAFkoSeQFzS0k7mRpnY6AkSo4V242";
$captcha_response = $_POST['g-recaptcha-response'];
$captcha_vars = '';

//Assemble Captcha vars
$captcha_vars .= "secret={$captcha_privatekey}";
$captcha_vars .= "&response={$captcha_response}";
$captcha_vars .= "&remoteip={$_SERVER["REMOTE_ADDR"]}";

//Check reCAPTCHA
$captcha = curl_init();
curl_setopt($captcha,CURLOPT_URL,'https://www.google.com/recaptcha/api/siteverify');
curl_setopt($captcha,CURLOPT_RETURNTRANSFER,true);
curl_setopt($captcha,CURLOPT_POST,true);
curl_setopt($captcha,CURLOPT_POSTFIELDS,$captcha_vars);
$captcha_result = curl_exec($captcha);
curl_close($captcha);
$captcha_result = json_decode($captcha_result, true);
if ($captcha_result['success'] == false) {
    header("Location: http://{$_SERVER['HTTP_HOST']}/index.php?p=registration_failed");
    die();
}

//Assemble email
foreach ($_POST as $key=>$data) {
    $content_template = str_replace("<!-- {$key} -->", $data, $content_template);
}
$email_template = str_replace('<!-- email_content -->', $content_template, $email_template);


//Send email
$message = Swift_Message::newInstance()
    ->setSubject('Establishment Registration Request - Bookttravel.travel')
    ->setFrom(array('info@booktravel.travel' => 'Booktravel Website'))
    ->setTo(array('info@booktravel.travel'))
    ->setBody($email_template, 'text/html');
$transport = Swift_SmtpTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);
$result = $mailer->send($message);

header("Location: http://{$_SERVER['HTTP_HOST']}/index.php?p=registration_complete");




