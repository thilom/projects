<?php
/**
 * Returns a list of establishment details to the browser (AJAX) for
 * location listings
 *
 * 2016-05-19: Thilo Muller - Created
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/v2/settings/init.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/v2/classes/establishment.class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/v2/classes/task.class.php';

//Vars
$establishmentClass = new Establishment();
$taskClass = new Task();
$establishment_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/v2/templates/establishment_block.html");
$page_data = array();
$establishments = explode('|', $_GET['ids']);
$page_data['establishments'] = '';
$nightsbridge_codes = '';

foreach ($establishments as $data) {
    list($establishment_code, $bbid) = explode('-', $data);
    if (!empty($bbid)) {
        $nightsbridge_codes[] = $bbid;
    }
}

//NightsBridge Check
//echo "<pre>"; var_dump($_COOKIE); echo '</pre>';
if (isset($_COOKIE['searchSettings']) && !empty($_COOKIE['searchSettings'])) {
    $cookie_data = explode('|', $_COOKIE['searchSettings']);
    $api_data = array();
    $api_data['messagename'] = "AvailRQ";
    $api_data['credentials']['nbid'] = $GLOBALS['nbid'];
    $api_data['credentials']['password'] = $GLOBALS['nightsbridge_password'];
    $api_data['bblist'] = array();
    foreach ($nightsbridge_codes as $data) {
        $api_data['bblist']['bbid'][] = $data;
    }
    $api_data['startdate'] = date('Y-m-d', strtotime($cookie_data[0]));
    $api_data['enddate'] = date('Y-m-d', strtotime($cookie_data[1]));
    $api['data'] = json_encode($api_data);
    $data = json_encode($api);
    $s = curl_init();
    curl_setopt($s,CURLOPT_URL,$GLOBALS['nightsbridge_api_url']);
    curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($s,CURLOPT_FORBID_REUSE,true);
    curl_setopt($s,CURLOPT_FRESH_CONNECT,true);
    curl_setopt($s,CURLOPT_POST,true);
    curl_setopt($s,CURLOPT_POSTFIELDS,$data);
    //TODO: Enable for live site
    $d = curl_exec($s);
    curl_close($s);

    //TODO: Remove for live site
//    $d = '
//{"success":true,"error":{},"data":{"startdate":"2016-05-24","enddate":"2016-05-28","nights":4,"imageurl":"http://nightsbridge.biz/i/","rtimageurl":"https://www.nightsbridge.co.za/bridge/images/rt/","bb":[{"bbid":15176,"name":"Tuishuis Lodge","noavailability":{"status":"F","description":"Fully Booked"}},{"bbid":19347,"name":"Royal Ridge Guest House","noavailability":{"status":"F","description":"Fully Booked"}},{"bbid":20268,"name":"Ngena Guest House","currencycode":"ZAR","childpolicy":{"lowerlimit":0,"childage1":18,"childage2":0},"vatregistered":false,"roomtypes":[{"rtid":5,"roomtypename":"First Floor King Room","description":"First Floor room with king size bed (or twin on request). En-suite bathroom with shower. basin and WC (enclosed). Small bar fridge, coffee/tea tray, flat screen TV with DSTV hotel bouquet and internet access.","extrainfo":"","rtimage":"20268/20268-5.jpg","maxoccupancy":2,"maxadults":2,"roomsfree":1,"ratescheme":1,"rateschemedesc":"Per Person Sharing","childpolicy":{"general":"0 - 18 years pay 100% of the sharing rate","allowchild1":"Y","allowchild2":"Y"},"minlos":0,"mealplans":[{"rateid":5,"mealplandesc":"Bed & Breakfast","defaultmealplan":"true","rates":{"pax1":"575.00","pax2":"850.00"}}]}]}]}}';

    $nightsbridge_result = json_decode($d, true);
//    echo "<pre>"; var_dump($nightsbridge_result['data']['bb']); echo '</pre>';

    if ($nightsbridge_result['success'] == true) {
//        echo "<pre>"; var_dump($nightsbridge_result); echo '</pre>';
        foreach ($nightsbridge_result['data']['bb'] as $nbData) {
            if (!isset($nbData['noavailability'])) {
                $show_establishments[] = $nbData['bbid'];
            }
        }
    }
}

foreach ($establishments as $data) {
    list($establishment_code, $bbid) = explode('-',$data);
    $establishment_code = trim($establishment_code, '-');
    $establishment_data = $establishmentClass->get_establishment($establishment_code);

    $block_data = array();
    $block_data['host_address'] = $_SERVER['HTTP_HOST'];
    $block_data['establishment_code'] = $establishment_code;
    $block_data['establishment_name'] = $establishment_data['establishment_name'];
    $block_data['establishment_description'] = $establishment_data['establishment_description'];
    $block_data['restype_name'] = $establishment_data['restype_name'];
    $block_data['restype_description'] = $establishment_data['restype_description'];
    $block_data['accommodation_available'] = '';

    //Assemble location
    $link = "/accommodation/places_to_stay/" . $taskClass->makeURLSafe($establishment_data['location']['cregion_name']);
    $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['country_name']);
    $block_data['location'] = "<a href='$link'>{$establishment_data['location']['country_name']}</a>";
    if (!empty($establishment_data['location']['province_name'])) {
        $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['province_name']);
        $block_data['location'] .= ' &middot; ' . "<a href='$link'>{$establishment_data['location']['province_name']}</a>";
    }
    if (!empty($establishment_data['location']['town_name'])) {
        $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['town_name']);
        $block_data['location'] .= ' &middot; ' . "<a href='$link'>{$establishment_data['location']['town_name']}</a>";
    }
    if (!empty($establishment_data['location']['suburb_name'])) {
        $link .= "/" . $taskClass->makeURLSafe($establishment_data['location']['suburb_name']);
        $block_data['location'] .= ' &middot; ' . "<a href='$link'>{$establishment_data['location']['suburb_name']}</a>";
    }

    //Establishment link
    $block_data['establishment_link'] = $link . '/' . $taskClass->makeURLSafe("{$establishment_code}_{$establishment_data['establishment_name']}");

    if (!empty($show_establishments)) {
        if (!in_array($establishment_data['nightsbridge_bbid'], $show_establishments)) {
            continue;
        } else {
//            $block_data['accommodation_available'] = "<div class='ribbon'><span onclick='bookNow(\"{$establishment_data['nightsbridge_bbid']}\", \"{$nightsbridge_result['data']['startdate']}\", \"{$nightsbridge_result['data']['enddate']}\")' >Book Now</span></div>";
              $block_data['accommodation_available'] = "<div class='ribbon'><span onclick='bookNow(\"{$block_data['establishment_link']}\")' >Available</span></div>";
        }
    }

    //Rooms counts
    if (empty($establishment_data['room_count'])) {
        $block_data['rooms'] = '';
    } else {
        $block_data['rooms'] = $establishment_data['room_count'];
        if (empty($establishment_data['room_type'])) {
            $block_data['rooms'] .= " Rooms/Units";
        } else {
            $block_data['rooms'] .= " {$establishment_data['room_type']}";
        }
    }

    $establishment_block = $establishment_template;
    $establishment_block = $taskClass->merge_data($establishment_block, $block_data);
    $page_data['establishments'] .= $establishment_block;
}

echo $page_data['establishments'];