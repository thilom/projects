<?php
/**
 * Booktravel / Essential travel application variables
 *
 * 2016-05-17: Thilo Muller - Created
 */

//Database
$db_host = 'localhost';
$db_name = 'booktravel';
$db_user = 'root';
$db_password = 'root';

//$db_user = 'booktrav_db';
//$db_password = '~h4mPP}?k(wG';
//$db_name = 'booktrav_db';

//$db_name = 'palladia_booktravel';
//$db_user = 'palladia_master';
//$db_password = 'sandbox23X';


//Templates
$default_template = "/v2/templates/site_template.html";


//Default Values
$page_title = 'Booktravel';
$site_url = "booktravel.travel";


//Nightbridge values
$nightsbridge_api_url = 'http://www.nightsbridge.co.za/bridge/jsonapi/4.0';
$nbid = 16;
$nightsbridge_password = 'laieys';