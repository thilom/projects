<?php
/**
 * Booktravel / Essential travel initialization file
 *  - Init Global Variables
 *  - Global DB setup
 *
 * 2016-05-17: Thilo Muller - Created
 */



//Includes
require "{$_SERVER['DOCUMENT_ROOT']}/v2/settings/vars.php";

//Connect to DB
try {
    $dbCon = new PDO("mysql:host=".$db_host.";dbname=".$db_name,$db_user,$db_password);
    $dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    throw new PDOException("Could not connect to Database: " .$e->getMessage());
}


