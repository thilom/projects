<?php
/**
 * Exception classes
 *
 * @Version 2009030401
 * @Author Thilo Muller
 * 
 */

class SQLConnectException extends Exception {
	
}

class SQLQueryException extends Exception {
	
}

?>