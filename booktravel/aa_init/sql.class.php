<?php
/**
 * SQL database class.
 * 
 * Extends mysqli to add exceptions for use on booktravel.travel, essentialtravelinfo.co.za, booktravel.co.za and accomodationawards.com
 *
 * @Version 2009030401
 * @Author Thilo Muller
 * 
 */

class aa_sql extends mysqli {
	
	function __construct($host, $user, $password, $database) {
		parent::__construct($host, $user, $password, $database);
    	if (mysqli_connect_error()) {
    		throw new SQLConnectException(mysqli_connect_error(), mysqli_connect_errno());
    	}
	}
	
	function query($query) {
		$result = parent::query($query);
		if (mysqli_error($this)) {
			throw new SQLQueryException(mysqli_error($this), mysqli_errno($this));
		}
		return $result;
	}
	
}

?>