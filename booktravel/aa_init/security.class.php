<?php
/**
 * Security class. Handles most site security
 *
 * @Version 2009030401
 * @Author Thilo Muller
 * 
 */

class security {

	function __construct() {
		
	}
	
	public function check_alphanumeric($test_str, $max_length) {
		if (strlen($test_str) > $max_length) return FALSE;
		if (!ctype_alnum($test_str)) 	return FALSE;
		return TRUE;
	}
}

?>