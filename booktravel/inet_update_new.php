<?php 

include($_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php');
error_reporting(E_ALL);

update();

function form($message = '') {
	echo $message;
	?>
		<form action='' method=post>
			<textarea style='width: 100%; height: 400' name=estabCodes></textarea><br>
			<input type=submit name=updateNSE value='Update nse_?'>
		</form>
	<?php 
}

function update() {
	include $_SERVER['DOCUMENT_ROOT'] . "/export/pageUpdate.class.php";
	$pUpd = new pageUpdate();
	
	$icons = ARRAY('AASYCAS', 'PFOLIO', 'AMEX', 'FEDHASA', 'GHASA', 'SATSA', 'CRVILLE', 'RCI', 'LEGACY', 'RELAISCHAT', 'SSUN', 'SUNINT', 'THREECITY', 'CAPENC', 'KNCS', 'MPUPARKS', 'SANP', 'FORBYFOR',
									'BIRDWATCH', 'GAMEVIEW', 'WHALE', 'HIKING', 'MTBIKE', 'HORSERIDNG', 'GOLF', 'SWIMPOOL', 'SWIMRIVER', 'WATERSPORT', 'FISHING', 'CASINO', 'GAMESROOM', 'PLAYGROUND',
									'TRAMPOLINE', 'MINIGOLF', 'SQUASH', 'TENNIS', 'BOWLS', 'VOLLEYBALL', 'GYM', 'SAUNA', 'JACUZZI', 'BRAAI', 'PUBBAR', 'RESTAURANT', 'CONFERENCE', 'EMAILINTER',   
									'TVLOUNGE', 'TVRMNET', 'TVRSATELLI', 'LAUNDROMAT', 'CURIO', 'SHOPONSITE', 'PETROL', 'OPENPARK', 'COVERPARK', 'NONSMOKE', 'COT',  'MICROWV', 'TVINROOM', 
									'TVMNET', 'SATELLITE', 'HAIRDRIER', 'TOILETRIES', 'AIRCOND', 'HEATING', 'SAFE', 'TEACOFFEE', 'BARFRIDGE', 'IRONBOARD', 'BABYSIT', 'LAUNDSERV',
									'IRONSERV', 'ROOMSERV', 'LTDRMSERV', 'SPMEALS', 'CREDCARDS', 'EXTREME', 'DISABLED', 'PETSALLOWD', 'PETSARRANG', 'NOPETS', 'POWERPOINT', 'ABLUTION', 'GAYFRIEND',
									'KOSHER', 'HALAAL', 'SPA', 'PROTEA', 'FOREVER', 'WIFI');
	
	$icon_ids = array();
	$statement = "SELECT icon_id FROM nse_icon_lang WHERE icon_name=?";
	$sql_icon_id = $GLOBALS['dbCon']->prepare($statement);
	foreach ($icons as $iName) {
		$sql_icon_id->bind_param('s', $iName);
		$sql_icon_id->execute();
		$sql_icon_id->bind_result($icon_id);
		$sql_icon_id->store_result();
		$sql_icon_id->fetch();
		$icon_ids[$iName] = $icon_id;
	}
	$sql_icon_id->close();
		
	//Prepare Statements
	$statement = "SELECT 
									CODE, RESNAME, POBOX, POSTADDR1, POSTADDR2, POSTADDR3, POSTPCODE, STRADDR1, STRADDR2, STRADDR3, AACAT, WEBADDR, REP, SOURCE, ADVERTISER,
									TELNO1, CELLNO, EMAILADDR, FAXNO, CARAVANNO, CAMPNO, ROOMCNT, BEDCOUNT, PRIORITY, PRICING, PRICEINHI, PRICEDESCR, RESTEXTMEM,
									PICFILEN1, PICFILEN2, PICFILEN3, PICFILEN4, PICFILEN5, PICFILEN6, latitude, longitude, SE_TOWN, SE_SUBURB2, BOROUGH, REGION,
									AASYCAS, PFOLIO, AMEX, FEDHASA, GHASA, SATSA, CRVILLE, RCI, LEGACY, RELAISCHAT, SSUN, SUNINT, THREECITY, CAPENC, KNCS, MPUPARKS, SANP, FORBYFOR,
									BIRDWATCH, GAMEVIEW, WHALE, HIKING, MTBIKE, HORSERIDNG, GOLF, SWIMPOOL, SWIMRIVER, WATERSPORT, FISHING, CASINO, GAMESROOM, PLAYGROUND,
									TRAMPOLINE, MINIGOLF, SQUASH, TENNIS, BOWLS, VOLLEYBALL, GYM, SAUNA, JACUZZI, BRAAI, PUBBAR, RESTAURANT, CONFERENCE, EMAILINTER,   
									TVLOUNGE, TVRMNET, TVRSATELLI, LAUNDROMAT, CURIO, SHOPONSITE, PETROL, OPENPARK, COVERPARK, NONSMOKE, COT,  MICROWV, TVINROOM, 
									TVMNET, SATELLITE, HAIRDRIER, TOILETRIES, AIRCOND, HEATING, SAFE, TEACOFFEE, BARFRIDGE, IRONBOARD, BABYSIT, LAUNDSERV,
									IRONSERV, ROOMSERV, LTDRMSERV, SPMEALS, CREDCARDS, EXTREME, DISABLED, PETSALLOWD, PETSARRANG, NOPETS, POWERPOINT, ABLUTION, GAYFRIEND,
									RESTYPE, TGCGRAD, KOSHER, HALAAL, SPA, PROTEA, FOREVER, WIFI, AAGRAD
								FROM inetstru WHERE upd IS NULL LIMIT 1";
	$sql_inet = $GLOBALS['dbCon']->prepare($statement);
	
	$statement = "SELECT COUNT(*) FROM nse_establishment WHERE establishment_code=?";
	$sql_estab_check = $GLOBALS['dbCon']->prepare($statement);
	
	$sql_inet->execute();
	$sql_inet->bind_result($code, $establishment_name, $pobox, $postal1, $postal2, $postal3, $postal_code, $street1, $street2, $street3, $aa_category, $web_url, $rep, $source, $advertiser, $tel, $cell, $email, $fax, $caravan, $camp, $room, $bed, $priority, $price, $price_high, $price_descr, $descr, $pic1, $pic2, $pic3, $pic4, $pic5, $pic6, $lat, $lon, $town, $suburb, $borough, $region, $AASYCAS, $PFOLIO, $AMEX, $FEDHASA, $GHASA, $SATSA, $CRVILLE, $RCI, $LEGACY, $RELAISCHAT, $SSUN, $SUNINT, $THREECITY, $CAPENC, $KNCS, $MPUPARKS, $SANP, $FORBYFOR,
								$BIRDWATCH, $GAMEVIEW, $WHALE, $HIKING, $MTBIKE, $HORSERIDNG, $GOLF, $SWIMPOOL, $SWIMRIVER, $WATERSPORT, $FISHING, $CASINO, $GAMESROOM, $PLAYGROUND,
								$TRAMPOLINE, $MINIGOLF, $SQUASH, $TENNIS, $BOWLS, $VOLLEYBALL, $GYM, $SAUNA, $JACUZZI, $BRAAI, $PUBBAR, $RESTAURANT, $CONFERENCE, $EMAILINTER,   
								$TVLOUNGE, $TVRMNET, $TVRSATELLI, $LAUNDROMAT, $CURIO, $SHOPONSITE, $PETROL, $OPENPARK, $COVERPARK, $NONSMOKE, $COT,  $MICROWV, $TVINROOM, 
								$TVMNET, $SATELLITE, $HAIRDRIER, $TOILETRIES, $AIRCOND, $HEATING, $SAFE, $TEACOFFEE, $BARFRIDGE, $IRONBOARD, $BABYSIT, $LAUNDSERV,
								$IRONSERV, $ROOMSERV, $LTDRMSERV, $SPMEALS, $CREDCARDS, $EXTREME, $DISABLED, $PETSALLOWD, $PETSARRANG, $NOPETS, $POWERPOINT, $ABLUTION, $GAYFRIEND, $restype, $star,
								$KOSHER, $HALAAL, $SPA, $PROTEA, $FOREVER, $WIFI, $aagrad);
	$sql_inet->store_result();	
	
	while ($sql_inet->fetch()) {
		
		//Check if already up to date
		$statement = "SELECT last_updated FROM nse_establishment WHERE establishment_code=? LIMIT 1";
		$sql_updated = $GLOBALS['dbCon']->prepare($statement);
		$sql_updated->bind_param('s', $code);
		$sql_updated->execute();
		$sql_updated->store_result();
		$sql_updated->bind_result($last_updated);
		$sql_updated->fetch();
		$sql_updated->free_result();
		$sql_updated->close();
		if ($last_updated == '') {
			
		} else {
			$statement = "UPDATE inetstru SET upd='X' WHERE code=? LIMIT 1";
			$sql_upd = $GLOBALS['dbCon']->prepare($statement);
			$sql_upd->bind_param('s', $code);
			$sql_upd->execute();
			$sql_upd->close();
			die("$code - Newer Version Exists");
		}
		
		$code = trim($code);
		echo "<br>$code - ";
		$sql_estab_check->bind_param('s', $code);
		$sql_estab_check->execute();
		$sql_estab_check->bind_result($estab_count);
		$sql_estab_check->store_result();
		$sql_estab_check->fetch();
		
		
		//Get Town ID
		$statement = "SELECT town_id, country_id, province_id FROM nse_location_town WHERE lower(town_name) = ?";
		$searchTown = strtolower($town);
		$sql_town = $GLOBALS['dbCon']->prepare($statement);
		$sql_town->bind_param('s', $searchTown);
		$sql_town->execute();
		$sql_town->bind_result($town_id, $country_id, $province_id);
		$sql_town->store_result();
		$sql_town->fetch();
		$sql_town->close();
		
		//Get Suburb ID
		if (!empty($suburb)) {
			if (stripos($searchSuburb, '- Suburb')) $searchSuburb = substr($searchSuburb, 0, stripos($searchSuburb, '- Suburb'));
			$statement = "SELECT suburb_id FROM nse_location_suburb WHERE lower(suburb_name)=? && town_id=?";
			$searchSuburb = strtolower($suburb);
			$sql_town = $GLOBALS['dbCon']->prepare($statement);
			$sql_town->bind_param('ss', $searchSuburb, $town_id);
			$sql_town->execute();
			$sql_town->bind_result($suburb_id);
			$sql_town->store_result();
			$sql_town->fetch();
			$sql_town->close();
		} else {
			$suburb_id = '0';
		}
		
		//Get Borough ID
		if (!empty($borough)) {
			$statement = "SELECT borough_id FROM nse_location_borough WHERE lower(borough_name)=? && town_id=?";
			$searchBorough = strtolower($suburb);
			$sql_town = $GLOBALS['dbCon']->prepare($statement);
			$sql_town->bind_param('ss', $searchBorough, $town_id);
			$sql_town->execute();
			$sql_town->bind_result($borough_id);
			$sql_town->store_result();
			$sql_town->fetch();
			$sql_town->close();
		} else {
			$borough_id = '0';
		}
		
		//Fix Postal address
		if (!empty($pobox)) $postal1 = "$pobox $postal1";
		
		//Fix estab name
		$establishment_name = ucwords(strtolower($establishment_name));
		
		if (0 == $estab_count) {
			$statement = "INSERT INTO nse_establishment 
										(establishment_code, establishment_name, street_address_line1, street_address_line2, street_address_line3, postal_address_line1, postal_address_line2, postal_address_line3, postal_address_code, aa_category_code, website_url, rep_id, source, advertiser, qagrad) 
										VALUES 
										(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$sql_estab = $GLOBALS['dbCon']->prepare($statement);
			echo mysqli_error($GLOBALS['dbCon']);
			
			$sql_estab->bind_param('sssssssssssssss', $code, $establishment_name, $street1, $street2, $street3, $postal1, $postal2, $postal3, $postal_code, $aa_category, $web_url, $rep, $source, $advertiser, $aagrad);
			$sql_estab->execute();
			$sql_estab->close();
			
			$statement = "INSERT INTO nse_establishment_contact  (establishment_code, contact_tel, contact_cell, contact_email, contact_fax) VALUES (?,?,?,?,?)";
			$sql_contact = $GLOBALS['dbCon']->prepare($statement);
			echo mysqli_error($GLOBALS['dbCon']);
			$sql_contact->bind_param('sssss', $code, $tel, $cell, $email, $fax);
			$sql_contact->execute();
			$sql_contact->close();
			
			$statement = "INSERT INTO nse_establishment_data (establishment_code, caravan_sites, camp_sites, room_count, bed_count) VALUES (?,?,?,?,?)";
			$sql_data = $GLOBALS['dbCon']->prepare($statement);
			$sql_data->bind_param('sssss', $code, $caravan, $camp, $room, $bed);
			$sql_data->execute();
			$sql_data->close();
			
			$statement = "INSERT INTO nse_establishment_deprecated (establishment_code, priority, price, price_high, price_description) VALUES (?,?,?,?,?)";
			$sql_depricated = $GLOBALS['dbCon']->prepare($statement);
			$sql_depricated->bind_param('sssss', $code, $priority, $price, $price_high, $price_descr);
			$sql_depricated->execute();
			$sql_depricated->close();
			
			$statement = "INSERT INTO nse_establishment_descriptions (establishment_code, language_code, description_type, establishment_description) VALUES (?, 'EN', 'short description', ?)";
			$sql_depricated = $GLOBALS['dbCon']->prepare($statement);
			$sql_depricated->bind_param('ss', $code, $descr);
			$sql_depricated->execute();
			$sql_depricated->close();
			
			if (!empty($pic1)) {
				$statement = "INSERT INTO nse_establishment_images (establishment_code, image_name, thumb_name) VALUES ('$code', '$pic1','TN_$pic1')";
				if (!empty($pic2)) $statement .= ", ('$code','$pic2', 'TN_$pic2')";
				if (!empty($pic3)) $statement .= ", ('$code','$pic3', 'TN_$pic3')";
				if (!empty($pic4)) $statement .= ", ('$code','$pic4', 'TN_$pic4')";
				if (!empty($pic5)) $statement .= ", ('$code','$pic5', 'TN_$pic5')";
				if (!empty($pic6)) $statement .= ", ('$code','$pic6', 'TN_$pic6')";
				$sql_images = $GLOBALS['dbCon']->prepare($statement);
				$sql_images->execute();
				$sql_images->close();
			}
			
			$statement = "INSERT INTO nse_establishment_location (establishment_code, gps_latitude, gps_longitude, town_id, province_id, country_id, suburb_id, borough_id) VALUES (?,?,?,?,?,?,?,?)";
			$sql_location = $GLOBALS['dbCon']->prepare($statement);
			$sql_location->bind_param('ssssssss', $code, $lat, $lon, $town_id, $province_id, $country_id, $suburb_id, $borough_id);
			$sql_location->execute();
			$sql_location->close();
			
			$statement = "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES";
			foreach ($icons as $i) {
				if ($$i == 'Y') {
					if ($statement == "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES") {
						$statement .= " ('$code', '{$icon_ids[$i]}')";
					} else {
						$statement .= ",('$code', '{$icon_ids[$i]}')";
					}
				}
			}
			if ($statement != "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES") {
				$sql_icon = $GLOBALS['dbCon']->prepare($statement);
				$sql_icon->execute();
				$sql_icon->close();
			}
			
			$statement = "SELECT subcategory_code FROM nse_restype_subcategory WHERE subcategory_code=?";
			$sql_restype_id = $GLOBALS['dbCon']->prepare($statement);
			$sql_restype_id->bind_param('s', $restype);
			$sql_restype_id->execute();
			$sql_restype_id->bind_result($restype_id);
			$sql_restype_id->store_result();
			$sql_restype_id->fetch();
			$sql_restype_id->close();
			
			$statement = "INSERT INTO nse_establishment_restype (establishment_code, subcategory_id, star_grading) VALUES (?,?,?)";
			$sql_restype = $GLOBALS['dbCon']->prepare($statement);
			$sql_restype->bind_param('sss', $code, $restype_id, $star);
			$sql_restype->execute();
			$sql_restype->close();
			
		} else {
			$statement = "UPDATE nse_establishment 
										SET establishment_name=?, street_address_line1=?, street_address_line2=?, street_address_line3=?, postal_address_line1=?, postal_address_line2=?, postal_address_line3=?, postal_address_code=?,
										aa_category_code=?, website_url=?, rep_id=?, source=?, advertiser=?
										WHERE establishment_code=?";
			$sql_estab = $GLOBALS['dbCon']->prepare($statement);
			echo mysqli_error($GLOBALS['dbCon']);
			$sql_estab->bind_param('ssssssssssssss', $establishment_name, $street1, $street2, $street3, $postal1, $postal2, $postal3, $postal_code, $aa_category, $web_url, $rep, $source, $advertiser, $code);
			
			$sql_estab->execute();
			$sql_estab->close();
			
			$statement = "UPDATE nse_establishment_contact SET contact_tel=?, contact_cell=?, contact_email=?, contact_fax=? WHERE establishment_code=?";
			$sql_contact = $GLOBALS['dbCon']->prepare($statement);
			$sql_contact->bind_param('sssss', $tel, $cell, $email, $fax, $code);
			$sql_contact->execute();
			$sql_contact->close();
			
			$statement = "UPDATE nse_establishment_data SET caravan_sites=?, camp_sites=?, room_count=?, bed_count=? WHERE establishment_code=?";
			$sql_data = $GLOBALS['dbCon']->prepare($statement);
			$sql_data->bind_param('sssss', $caravan, $camp, $room, $bed, $code);
			$sql_data->execute();
			$sql_data->close();
			
			$statement = "UPDATE nse_establishment_deprecated SET priority=?, price=?, price_high=?, price_description=? WHERE establishment_code=?";
			$sql_depricated = $GLOBALS['dbCon']->prepare($statement);
			$sql_depricated->bind_param('sssss',  $priority, $price, $price_high, $price_descr, $code);
			$sql_depricated->execute();
			$sql_depricated->close();
			
			$statement = "UPDATE nse_establishment_descriptions SET establishment_description=? WHERE establishment_code=? && language_code='EN' && description_type='short description'";
			$sql_depricated = $GLOBALS['dbCon']->prepare($statement);
			echo mysqli_error($GLOBALS['dbCon']);
			$sql_depricated->bind_param('ss', $descr, $code);
			$sql_depricated->execute();
			$sql_depricated->close();
			
			$statement = "DELETE FROM nse_establishment_images WHERE establishment_code=?";
			$sql_images_delete = $GLOBALS['dbCon']->prepare($statement);
						echo mysqli_error($GLOBALS['dbCon']);
			$sql_images_delete->bind_param('s', $code);
			$sql_images_delete->execute();
			$sql_images_delete->close();
			
			if (!empty($pic1)) {
				$statement = "INSERT INTO nse_establishment_images (establishment_code, image_name, thumb_name) VALUES ('$code', '$pic1','TN_$pic1')";
				if (!empty($pic2)) $statement .= ", ('$code','$pic2', 'TN_$pic2')";
				if (!empty($pic3)) $statement .= ", ('$code','$pic3', 'TN_$pic3')";
				if (!empty($pic4)) $statement .= ", ('$code','$pic4', 'TN_$pic4')";
				if (!empty($pic5)) $statement .= ", ('$code','$pic5', 'TN_$pic5')";
				if (!empty($pic6)) $statement .= ", ('$code','$pic6', 'TN_$pic6')";
				$sql_images = $GLOBALS['dbCon']->prepare($statement);
				$sql_images->execute();
				$sql_images->close();
			}
			
			$statement = "UPDATE nse_establishment_location SET gps_latitude=?, gps_longitude=?, town_id=?, province_id=?, country_id=?, suburb_id=?, borough_id=? WHERE establishment_code=?";
			$sql_location = $GLOBALS['dbCon']->prepare($statement);
			$sql_location->bind_param('ssssssss', $lat, $lon, $town_id, $province_id, $country_id, $suburb_id, $borough_id, $code);
			$sql_location->execute();
			$sql_location->close();
			
			$statement = "DELETE FROM nse_establishment_icon WHERE establishment_code=?";
			$sql_icon_delete = $GLOBALS['dbCon']->prepare($statement);
			$sql_icon_delete->bind_param('s', $code);
			$sql_icon_delete->execute();
			$sql_icon_delete->close();
			
			$statement = "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES";
			foreach ($icons as $i) {
				if ($$i == 'Y') {
					if ($statement == "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES") {
						$statement .= " ('$code', '{$icon_ids[$i]}')";
					} else {
						$statement .= ",('$code', '{$icon_ids[$i]}')";
					}
				}
			}
			if ($statement != "INSERT INTO nse_establishment_icon (establishment_code, icon_id) VALUES") {
				$sql_icon = $GLOBALS['dbCon']->prepare($statement);
				echo mysqli_error($GLOBALS['dbCon']);
				$sql_icon->execute();
				$sql_icon->close();
			}
			
			$statement = "SELECT subcategory_id FROM nse_restype_subcategory WHERE subcategory_code=?";
			$sql_restype_id = $GLOBALS['dbCon']->prepare($statement);
			$sql_restype_id->bind_param('s', $restype);
			$sql_restype_id->execute();
			$sql_restype_id->bind_result($restype_id);
			$sql_restype_id->store_result();
			$sql_restype_id->fetch();
			$sql_restype_id->close();
			
			$statement = "UPDATE nse_establishment_restype SET subcategory_id=?, star_grading=? WHERE establishment_code=?";
			$sql_restype = $GLOBALS['dbCon']->prepare($statement);
			$sql_restype->bind_param('sss', $restype_id, $star, $code);
			$sql_restype->execute();
			$sql_restype->close();
			
		}
		
		$pUpd->update_establishment($code);
		
		$statement = "DELETE FROM inetstru WHERE CODE=?";
		$sql_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_delete->bind_param('s', $code);
		$sql_delete->execute();
		echo "$code";
		
	}	
	
	
	
}

?>