<?php 
	$subscription_email = $_POST['email'];

	$statement = "SELECT COUNT(*) FROM subscriptions WHERE email=?";
	$sql_email_check = $GLOBALS['dbCon']->prepare($statement);
	$sql_email_check->bind_param('s', $subscription_email);
	$sql_email_check->execute();
	$sql_email_check->bind_result($email_count);
	$sql_email_check->store_result();
	$sql_email_check->fetch();
	
	
	if ($email_count == 0) {
		$statement = "INSERT INTO subscriptions (email, date_added) VALUES (?, NOW())";
		$sql_email_insert = $dbCon->prepare($statement);
		$sql_email_insert->bind_param('s', $subscription_email);
		$sql_email_insert->execute();
	}
	
	$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/subscribe_thanks.tpl'); 
	
?>