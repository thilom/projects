<?php
/**
 * Log function stats
 *
 * 2016-03-24: Thilo Muller - Created
 */

if ($GLOBALS['enable_logging'] === true) {
    $GLOBALS['log_order']++;
    $current_time = microtime(true);
    if (!isset($search_string)) $search_string = "Unknown|Unknown";
    $run_time = $current_time - $GLOBALS['start_time'];
    $total_time = $current_time - $GLOBALS['last_time'];
    $GLOBALS['last_time'] = $current_time;
    $GLOBALS['logClass']->log_function_time($search_string, $run_time, $total_time);
}