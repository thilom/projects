<?php ini_set('display_errors', 1);
ini_set('error_reporting', E_ERROR);
function connect($dbUser,$dbPass,$dbName,$dbHost) {
   mysql_connect($dbHost,$dbUser,$dbPass);
   mysql_select_db($dbName);
}

//Query the database and put the results in an array
function dbQuery($query) {
   $rsQuery = mysql_query($query);
   if (@@mysql_error()) {
       $queryArr["error"]["isError"] = true;
       $queryArr["error"]["errorMsg"] = @@mysql_error();
       echo @@mysql_error();
     } else {
       $queryArr["error"]["isError"] = false;
       $queryArr["error"]["errorMsg"] = "No Errors";
       if (is_resource($rsQuery)) {
           $queryArr["queryInfo"]["numRows"] = mysql_numrows($rsQuery);
       } else {
           $queryArr["queryInfo"]["numRows"] = 0;
       }
       
       $queryArr["queryInfo"]["insertID"] = false;
       if (mysql_insert_id()) {
           $queryArr["queryInfo"]["insertID"] = mysql_insert_id();    
       }
       if (is_resource($rsQuery)) {
           for($i=0;$i<mysql_num_fields($rsQuery);$i++) {
                 $field = mysql_fetch_field($rsQuery, $i);
                 for($ii=0;$ii<mysql_num_rows($rsQuery);$ii++) {
                   $queryArr[$field->name][$ii] = mysql_result($rsQuery,$ii,$field->name);
                 }
           }
       }
     }
     unset($rsQuery);
     return $queryArr;
}

//Make strings safe to prevent SQL injection
function sfs($string) {
   return mysql_real_escape_string($string);
}

//Make integers save to prevent SQL injection
function nfs($string) {
   return mysql_real_escape_string(intval($string));
}

connect("c1aatravel","fV7hvjMG7wwNuQGU","c1aatravel","localhost");

$query = "SELECT NOW() AS start_time";
$rsGetStart = dbQuery($query);

$query = "TRUNCATE TABLE nse_establishment_nlocations_temp;";
$rsRunEstabLocationScript = dbQuery($query);
//FETCH SUBURBS	
$query = "INSERT INTO nse_establishment_nlocations(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,suburb_id AS location_id
		,'suburb' AS location_type
	FROM nse_establishment_location
	WHERE suburb_id IS NOT NULL AND suburb_id != 0;";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//FETCH TOWN REGION SUBURBS
$query = "INSERT INTO nse_establishment_nlocations_temp(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,nse_nlocations_cregions.region_id AS location_id
		,'region' AS location_type
	FROM nse_nlocations_cregions
	INNER JOIN nse_nlocations_cregion_locations ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
	INNER JOIN nse_establishment_location ON nse_nlocations_cregion_locations.location_id = nse_establishment_location.suburb_id AND nse_nlocations_cregion_locations.location_type LIKE 'suburb'
	WHERE nse_nlocations_cregions.parent_type LIKE 'town';";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//FETCH TOWNS
$query = "INSERT INTO nse_establishment_nlocations(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,town_id AS location_id
		,'town' AS location_type
	FROM nse_establishment_location
	WHERE town_id IS NOT NULL AND town_id != 0;";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//FETCH REGION SUBURBS INTO TEMP
$query = "INSERT INTO nse_establishment_nlocations_temp(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,nse_nlocations_cregions.region_id AS location_id
		,'region' AS location_type
	FROM nse_nlocations_cregions
	INNER JOIN nse_nlocations_cregion_locations ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
	INNER JOIN nse_establishment_location ON nse_nlocations_cregion_locations.location_id = nse_establishment_location.suburb_id AND nse_nlocations_cregion_locations.location_type LIKE 'suburb';";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//FETCH REGION TOWNS INTO TEMP
$query = "INSERT INTO nse_establishment_nlocations_temp(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,nse_nlocations_cregions.region_id AS location_id
		,'region' AS location_type
	FROM nse_nlocations_cregions
	INNER JOIN nse_nlocations_cregion_locations ON nse_nlocations_cregions.region_id = nse_nlocations_cregion_locations.region_id
	INNER JOIN nse_establishment_location ON nse_nlocations_cregion_locations.location_id = nse_establishment_location.town_id AND nse_nlocations_cregion_locations.location_type LIKE 'town';";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//MERGE REGION SUBURBS AND REGION TOWNS
$query = "INSERT INTO nse_establishment_nlocations(establishment_code,location_id,location_type)
	SELECT DISTINCT
	establishment_code
	,location_id
	,location_type 
	FROM nse_establishment_nlocations_temp
	WHERE location_type LIKE 'region'
	GROUP BY location_id,establishment_code 
	ORDER BY establishment_code;";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//FETCH COUNTRIES
$query = "INSERT INTO nse_establishment_nlocations(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,country_id AS location_id
		,'country' AS location_type
	FROM nse_establishment_location
	WHERE country_id IS NOT NULL AND country_id != 0;";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//FETCH PROVINCES
$query = "INSERT INTO nse_establishment_nlocations(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,province_id AS location_id
		,'province' AS location_type
	FROM nse_establishment_location
	WHERE province_id IS NOT NULL AND province_id != 0;";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
//FETCH COUNTRY REGIONS
$query = "INSERT INTO nse_establishment_nlocations(establishment_code,location_id,location_type)
	SELECT DISTINCT
		establishment_code
		,nse_nlocations_regions.region_id AS location_id
		,'cregion' AS location_type
	FROM nse_nlocations_regions
	INNER JOIN nse_nlocations_regions_content ON nse_nlocations_regions.region_id = nse_nlocations_regions_content.region_id
	INNER JOIN nse_nlocations_countries ON nse_nlocations_regions_content.location_id = nse_nlocations_countries.country_id
		AND nse_nlocations_regions_content.location_type LIKE 'country'
	INNER JOIN nse_establishment_location ON nse_nlocations_countries.country_id = nse_establishment_location.country_id
	WHERE region_type LIKE 'cregion';";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
$query = "DELETE FROM nse_establishment_nlocations WHERE date_updated < '".$rsGetStart["start_time"][0]."';";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);
$query = "TRUNCATE TABLE nse_establishment_nlocations_temp;";
echo $query;
$rsRunEstabLocationScript = dbQuery($query);

//exit;
$insered = 0;
$updated = 0;
$duplicates = 0;
$deleted = 0;

echo "Script start: ".$rsGetStart["start_time"][0]."\n";

$query = "SELECT
	theme_id
	,theme_name
	,restype_ids
	,icon_ids
	,star_grad
	,specials
FROM nse_themes
WHERE fk_theme_id != 0";
$rsGetThemes = dbQuery($query);

for($i=0;$i<$rsGetThemes["queryInfo"]["numRows"];$i++) {
	if ($rsGetThemes["theme_id"][$i] == 48 || $rsGetThemes["theme_id"][$i] == 49 || $rsGetThemes["theme_id"][$i] == 50) {
		echo "Updating: ".$rsGetThemes["theme_name"][$i]."\n";
		$where_extra = "";
		$query = "SELECT
			nse_establishment.establishment_code
		FROM nse_establishment
		LEFT JOIN nse_establishment_qa_cancelled ON nse_establishment.establishment_code = nse_establishment_qa_cancelled.establishment_code ";
		if ($rsGetThemes["specials"][$i] == "1") {
			$query .= "INNER JOIN nse_establishment_specials ON nse_establishment.establishment_code = nse_establishment_specials.establishment_code AND specials_end > NOW() 
				INNER JOIN nse_establishment_restype ON nse_establishment.establishment_code = nse_establishment_restype.establishment_code 
				INNER JOIN nse_restype_subcategory_parent ON nse_establishment_restype.subcategory_id = nse_restype_subcategory_parent.subcategory_id
				INNER JOIN nse_restype_category ON nse_restype_subcategory_parent.category_id = nse_restype_category.category_id
					AND toplevel_id = 1 ";
	    } elseif ($rsGetThemes["specials"][$i] == "2") {
			$query .= "INNER JOIN nse_establishment_specials ON nse_establishment.establishment_code = nse_establishment_specials.establishment_code AND specials_end > NOW() AND aa_members_only = 1
				INNER JOIN nse_establishment_restype ON nse_establishment.establishment_code = nse_establishment_restype.establishment_code 
				INNER JOIN nse_restype_subcategory_parent ON nse_establishment_restype.subcategory_id = nse_restype_subcategory_parent.subcategory_id
				INNER JOIN nse_restype_category ON nse_restype_subcategory_parent.category_id = nse_restype_category.category_id
					AND toplevel_id = 1 ";
		} elseif ($rsGetThemes["theme_url"][$i] == "realtimebookings") {
			$where_extra = " AND nightsbridge_bbid > 0";
		} else {
			$query .= "INNER JOIN nse_establishment_restype ON nse_establishment.establishment_code = nse_establishment_restype.establishment_code 
				INNER JOIN nse_restype_subcategory_parent ON nse_establishment_restype.subcategory_id = nse_restype_subcategory_parent.subcategory_id
				INNER JOIN nse_restype_category ON nse_restype_subcategory_parent.category_id = nse_restype_category.category_id
					AND toplevel_id = 1 ";
			if ($rsGetThemes["icon_ids"][$i]."" != "") {
				$query .= " INNER JOIN nse_establishment_icon ON nse_establishment.establishment_code = nse_establishment_icon.establishment_code
							AND nse_establishment_icon.icon_id IN (".$rsGetThemes["icon_ids"][$i].") ";
			}
			if ($rsGetThemes["restype_ids"][$i]."" != "") {
				$query .= " AND nse_establishment_restype.subcategory_id IN (".$rsGetThemes["restype_ids"][$i].") ";
			}
			if ($rsGetThemes["star_grad"][$i]."" != "") {
				$query .= " AND nse_establishment_restype.star_grading IN (".$rsGetThemes["star_grad"][$i].") ";
			}	
		}
		$query .= " WHERE aa_estab = 'Y' AND active = 1 AND (nse_establishment_qa_cancelled.cancelled_date IS NULL OR nse_establishment_qa_cancelled.cancelled_date = '0000-00-00' OR nse_establishment_qa_cancelled.cancelled_date = '') $where_extra
		GROUP BY nse_establishment.establishment_code";
		echo $query;
		//exit;
		$rsGetEstabs = dbQuery($query);
		for ($ii=0;$ii<$rsGetEstabs["queryInfo"]["numRows"];$ii++) {
			$establishment_code = trim($rsGetEstabs["establishment_code"][$ii]," ");
			//$query = "SELECT establishment_theme_id FROM nse_establishment_themes WHERE establishment_code LIKE '%".$establishment_code."%' AND theme_id = ".$rsGetThemes["theme_id"][$i];
			//$rsGetEstabTheme = dbQuery($query);
			//if ($rsGetEstabTheme["queryInfo"]["numRows"] == 1) {
			//	$query = "UPDATE nse_establishment_themes SET date_updated = NOW() WHERE establishment_code LIKE '%".$establishment_code."%'";
			//	$rsUpdateEstabTheme = dbQuery($query);
			//	$updated++;
			//} elseif ($rsGetEstabTheme["queryInfo"]["numRows"] > 1) {
			//	$query = "DELETE FROM nse_establishment_themes WHERE establishment_code LIKE '%".$establishment_code."%' AND theme_id = ".$rsGetThemes["theme_id"][$i];
			//	$rsDeleteEstabTheme = dbQuery($query);
			//	$query = "INSERT INTO nse_establishment_themes(establishment_code,theme_id,date_updated) VALUES('".$establishment_code."', ".$rsGetThemes["theme_id"][$i].",NOW())";
			//	$rsInsertEstabTheme = dbQuery($query);
			//	$duplicates++;
			//} else {
				$query = "INSERT INTO nse_establishment_themes(establishment_code,theme_id,type,date_updated) VALUES('".$establishment_code."', ".$rsGetThemes["theme_id"][$i].",'aa',NOW())";
				echo $query;
				$rsInsertEstabTheme = dbQuery($query);
				$insered++;
			//}
		}
	}
}

$query = "SELECT count(*) AS count FROM nse_establishment_themes WHERE date_updated < '".$rsGetStart["start_time"][0]."' AND type LIKE 'aa'";
$rsGetDeleteCount = dbQuery($query);

$deleted = $rsGetDeleteCount["count"][0];

$query = "DELETE FROM nse_establishment_themes WHERE date_updated < '".$rsGetStart["start_time"][0]."' AND type LIKE 'aa' AND (theme_id = 48 OR theme_id = 49 OR theme_id = 50)";
$rsDeleteCount = dbQuery($query);

$query = "SELECT NOW() AS end_time";
$rsGetEnd = dbQuery($query);

echo "Inserted: ".$insered."\n";
echo "Updated: ".$updated."\n";
echo "Duplicates: ".$duplicates."\n";
echo "Deleted: ".$deleted."\n";
echo "Script end: ".$rsGetEnd["end_time"][0]."\n";?>