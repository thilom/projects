<?php 
//Vars
$to_number = '';
$to_name = '';
$to_suburb = '';
$to_town = '';
	
//Get template
$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/google_route.tpl');

//Get estab address
if (isset($_GET['id'])) {
	$statement = "SELECT street_address_line1, street_address_line2, street_address_line3 FROM nse_establishment WHERE establishment_code=?";
	$sql_address = $GLOBALS['dbCon']->prepare($statement);
	$sql_address->bind_param('s', $_GET['id']);
	$sql_address->execute();
	$sql_address->store_result();
	$sql_address->bind_result($addr1, $addr2, $addr3);
	$sql_address->fetch();
	$sql_address->free_result();
	$sql_address->close();
	
	
}

//Replace Tags
$content = str_replace('<!-- api_key -->', $GLOBALS['api_key'], $content);
$content = str_replace('<!-- to_number -->', $to_number, $content);
$content = str_replace('<!-- to_name -->', $to_name, $content);
$content = str_replace('<!-- to_suburb -->', $to_suburb, $content);
$content = str_replace('<!-- to_town -->', $to_town, $content);

?>

