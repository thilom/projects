<?php
/**
 * New updates search function. No longer looks for keywords in establishment content.
 * The sebject of the search can now be selected by the user ie. Establishment search or location search.
 * The search defaults to a general search that looks for keywords representing location or activities and
 * then searches accordingly. ie. Searching for the pharse 'Hotels in Pretoria with spa' will find all hotel listings
 * situated in Pretoria that have spa listed as one of thier activities.
 *
 * -- Fixed search to be case insensitive - Thilo (2010)
 * -- Updated the script to allow for european character, hyphens and joined words - Thilo(2010)
 *
 * @author Thilo Muller(2010)
 * @package RVBus
 * @category search
 *
 */

//Vars
$nLine = '';
$search_string = isset($_POST['searchString'])?$_POST['searchString']:'';
$result_length = 10;

//Includes
include $_SERVER['DOCUMENT_ROOT'] . '/shared/class.stemmer.php';

//Get Template
$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/search_new.tpl');
$content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
$lTemplate = $matches[0][0];

if (isset($_GET['sr'])) {
	$nLine = retrieve_result();
} else if (isset($_POST['search_estab'])) {
	$nLine = search_establishments();
} else if (isset($_POST['search_loc'])) {
	$nLine = search_location();
} else {
	$nLine = search_general();
}

//Replace_tags
$content = preg_replace('@<!-- line_Start -->(.)*<!-- line_end -->@i', $nLine, $content);
$content = str_replace('$%^', "\n", $content);

/**
 * Search for an establishment name. The script first looks for an exact match to the search phrase before
 * breaking up the search phrase and looking for individual word matches in the search hash.
 */
function search_establishments() {
	//Vars
	$nLine = '';
	$establishment_code = '';
	$establishment_name = '';
	$counter = 0;
	$line_color = 'd9d9d9';
	$result_cache = '';
	$search_string = strtolower($GLOBALS['search_string']);
	
	$search_hash = str_replace(' ', '', $search_string);
	$search_hash = strtolower($search_hash);
	$search_hash = plainize_name($search_hash);
	$search_hash = sha1($search_hash);
	
	$statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE aa_estab='Y' && active='1' && (establishment_code=? || establishment_name=?)";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->bind_param('ss', $search_string, $search_string);
	$sql_estab->execute();
	$sql_estab->store_result();
	$sql_estab->bind_result($establishment_code, $establishment_name);
	while ($sql_estab->fetch()) {
		$establishment_code = trim($establishment_code);
		$establishment_list[$establishment_code] = $establishment_name;
	}
	$sql_estab->free_result();
	$sql_estab->close();
	
	$search_string = plainize_name($search_string);
	$words = explode(' ', $search_string);
	$statement = "SELECT establishment_code, establishment_name FROM nse_establishment WHERE aa_estab='Y' && active='1' && (";
	foreach ($words as $word) {
		$word = sha1(trim($word));
		$statement .= " search_hash like '%$word%' &&";
	}
	$statement = substr($statement, 0, -2);
	$statement .= ")";
	$sql_estab = $GLOBALS['dbCon']->prepare($statement);
	$sql_estab->execute();
	$sql_estab->store_result();
	$result_count = $sql_estab->num_rows;
	$sql_estab->bind_result($establishment_code, $establishment_name);
	while ($sql_estab->fetch()) {
		$establishment_code = trim($establishment_code);
		$establishment_list[$establishment_code] = $establishment_name;
	}
	$sql_estab->free_result();
	$sql_estab->close();
	
	if (empty($establishment_list)) {
		$nLine .= '<tr><td colspan=10 align=center><br>-- No Results Found --</br>&nbsp;</td></tr>';
		$result_count = 0;
		$total_pages = 1;
		$current_page = 1;
		$previous_link = '';
		$next_link = '';
	} else {
	
		foreach($establishment_list AS $establishment_code=>$establishment_name) {
			if ($counter < $GLOBALS['result_length'])  {
				$dir_name = directory_name($establishment_code . '_' . $establishment_name);
				$locations = get_locations($establishment_code);
				$description = get_description($establishment_code);
				$price = get_pricing($establishment_code);
				$line_color = $line_color=='d9d9d9'?'ffffff':'d9d9d9';
				$rating = get_rating($establishment_code);
				
				$line = str_replace('<!-- establishment_name -->', $establishment_name, $GLOBALS['lTemplate']);
				$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
				$line = str_replace('<!-- position -->', $counter, $line);
				$line = str_replace('<!-- search_string -->', $GLOBALS['search_string'], $line);
				$line = str_replace('<!-- directory_name -->', $dir_name, $line);
				$line = str_replace('<!-- price -->', $price, $line);
				$line = str_replace('<!-- locations -->', $locations, $line);
				$line = str_replace('<!-- description -->', $description, $line);
				$line = str_replace('<!-- line_color -->', $line_color, $line);
				$line = str_replace('<!-- rating -->', $rating, $line);
				$line = str_replace('<!-- search_type -->', 'E', $line);
			
				$nLine .= $line;
			}
			
			$result_cache .= "$establishment_code,";
			
			$counter++;
		}
		
		$result_id = sha1($nLine + microtime());
		
		//Save Search
		$options = '';
		if (isset($_POST['search_estab'])) $options = 'E';
		if (isset($_POST['search_loc'])) $options = 'L';
		$statement = "INSERT INTO nse_search_result_cache (search_hash, search_result, search_string, search_options) VALUES (?,?,?,?)";
		$sql_save = $GLOBALS['dbCon']->prepare($statement);
		$sql_save->bind_param('ssss', $result_id, $result_cache, $GLOBALS['search_string'], $options);
		$sql_save->execute();
		$sql_save->close();
		
		//Nav Tags
		$total_pages = ceil($result_count/$GLOBALS['result_length']);
		$current_page = isset($_GET['cp'])?$_GET['cp']:'1';
		$previous_cp = $current_page-1;
		$previous_link = $current_page==1?'':"<a href='/index.php?p=search2&cp=$previous_cp&sr=$result_id' >&#9668;Previous</a >";
		$next_cp = $current_page+1;
		$next_link = $current_page==$total_pages?'':"<a href='/index.php?p=search2&cp=$next_cp&sr=$result_id' >Next&#9658;</a >";
	}
	
	//Replace Tags
	$GLOBALS['content'] = str_replace('<!-- result_count -->', $result_count, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- total_pages -->', $total_pages, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- current_page -->', $current_page, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- previous_link -->', $previous_link, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- next_link -->', $next_link, $GLOBALS['content']);
	
	return $nLine;
}

function search_location() {
	//Vars
	$location_keywords = explode(' ', $GLOBALS['search_string']);
	$phrase_count = 0;
	$country_id = '';
	$province_id = '';
	$town_id = '';
	$suburb_id = '';
	$counter = 0;
	$nLine = '';
	$line_color = 'd9d9d9';
	$establishment_code = '';
	$establishment_name = '';
	$result_cache = '';
	$town_id_partial = '';
	$town_link = '';
	$used_words = array();
	
	$search_type = '';
	if (isset($_POST['search_loc'])) $search_type = 'L';
	if (isset($_POST['search_estab'])) $search_type = 'E';
	
	//Remove commas & extra spaces
	foreach ($location_keywords as $key=>$keyword) {
		$keyword = str_replace(',', '', $keyword);
		$keyword = trim($keyword);
		if (empty($keyword)) {
			unset ($location_keywords[$key]);
		} else {
			$location_keywords[$key] = $keyword;
		}
	}
	
	//Assemble search phrases
	foreach($location_keywords AS $word) {
		$location_phrase[$phrase_count]= $word;
		if ($phrase_count != 0) $location_phrase[$phrase_count-1] .= " $word";
		$phrase_count++;
	}
	$phrase_count = 0;
	foreach($location_keywords AS $word) {
		$location_phrase3[$phrase_count]= $word;
		if ($phrase_count != 0) $location_phrase3[$phrase_count-1] .= " $word";
		if ($phrase_count != 0 && $phrase_count != 1) $location_phrase3[$phrase_count-2] .= " $word";
		
		$phrase_count++;
	}
	foreach ($location_phrase3 as $key=>$search_word) {
		if (str_word_count($search_word,0) == 1) unset($location_phrase3[$key]);
		if (str_word_count($search_word,0) == 2) unset($location_phrase3[$key]);
	}
	foreach ($location_phrase as $key=>$search_word) {
		if (str_word_count($search_word,0) == 1) unset($location_phrase[$key]);
	}
	$location_phrase = array_merge($location_phrase,$location_phrase3);
	
	//Check for country ID
	foreach ($location_phrase AS $search_string) {
		$country_id = locate_country($search_string);
		if ($country_id != 0) {
			$a = explode(' ', $search_string);
				foreach ($a as $v) {
					$used_words[] = $v;
				}
			break;
		}
	}
	if ($country_id == 0) {
		foreach ($location_keywords AS $search_string) {
			$country_id = locate_country($search_string);
			if ($country_id != 0) $used_words[] = $search_string;
		}
	}
	
	//Check for province ID
	if ($country_id != 0) {
		foreach ($location_phrase AS $search_string) {
			$province_id = locate_province($search_string, $country_id);
			if ($province_id != 0) {
				$a = explode(' ', $search_string);
				foreach ($a as $v) {
					$used_words[] = $v;
				}
				break;
			}
		}
		
		if ($province_id == 0) {
			foreach ($location_keywords AS $search_string) {
				$province_id = locate_province($search_string, $country_id);
				if ($province_id != 0) {
					$used_words[] = $search_string;
					break;
				}
			}
		}
	} else {
		foreach ($location_phrase AS $search_string) {
			$province_id = locate_province($search_string);
			if ($province_id != 0) {
				$a = explode(' ', $search_string);
				foreach ($a as $v) {
					$used_words[] = $v;
				}
				break;
			}
		}
		
		if ($province_id == 0) {
			foreach ($location_keywords AS $search_string) {
				$province_id = locate_province($search_string);
				if ($province_id != 0) {
					$used_words[] = $search_string;
					break;
				}
			}
		}
	}

	
	//Check for town ID
	if ($province_id != 0) {
		foreach ($location_phrase AS $search_string) {
			$town_id = locate_town($search_string, $province_id);
			if ($town_id != 0) {
				$a = explode(' ', $search_string);
				foreach ($a as $v) {
					$used_words[] = $v;
				}
				break;
			}
			
		}
		if ($town_id == 0) {
			foreach ($location_keywords AS $search_string) {
				$town_id = locate_town($search_string, $province_id);
				
				if ($town_id != 0) {
					$used_words[] = $search_string;
					break;
				}
			}
		}
	} else {
	foreach ($location_phrase AS $search_string) {
			$town_id = locate_town($search_string);
			
			if ($town_id != 0) {
				$a = explode(' ', $search_string);
				foreach ($a as $v) {
					$used_words[] = $v;
				}
				break;
			}
		}
		if ($town_id == 0) {
			foreach ($location_keywords AS $search_string) {
				$town_id = locate_town($search_string);
				
				if ($town_id != 0) {
					$used_words[] = $search_string;
					break;
				}
			}
		}
	}
	
	
	//Check for suburb ID
	if ($town_id != 0) {
		foreach ($location_phrase AS $search_string) {
			$suburb_id = locate_suburb($search_string, $town_id);
			if ($suburb_id != 0) {
				$a = explode(' ', $search_string);
				foreach ($a as $v) {
					$used_words[] = $v;
				}
				break;
			}
		}
		if ($suburb_id == 0) {
			foreach ($location_keywords AS $search_string) {
				$suburb_id = locate_suburb($search_string, $town_id);
				if ($suburb_id != 0) {
					$used_words[] = $search_string;
					break;
				}
			}
		}
	} else {
		foreach ($location_phrase AS $search_string) {
			$suburb_id = locate_suburb($search_string);
			if ($suburb_id != 0) {
				$a = explode(' ', $search_string);
				foreach ($a as $v) {
					$used_words[] = $v;
				}
				break;
			}
		}
		if ($suburb_id == 0) {
			foreach ($location_keywords AS $search_string) {
				$suburb_id = locate_suburb($search_string);
				if ($suburb_id != 0) {
					$used_words[] = $search_string;
					break;
				}
			}
		}
	}
	
	//Get partial town matches
	if ($province_id != 0) {
		foreach ($location_phrase AS $search_string) {
			$town_id_partial = locate_partial_town($search_string, $province_id);
			if ($town_id_partial != 0) break;
		}
		if ($town_id_partial == 0) {
			foreach ($location_keywords AS $search_string) {
				$town_id_partial = locate_partial_town($search_string, $province_id);
				if ($town_id_partial != 0) break;
			}
		}
	} else {
	foreach ($location_phrase AS $search_string) {
			$town_id_partial = locate_partial_town($search_string);
			if ($town_id_partial != 0) break;
		}
		if ($town_id_partial == 0) {
			foreach ($location_keywords AS $search_string) {
				$town_id_partial = locate_partial_town($search_string);
				if ($town_id_partial != 0) break;
			}
		}
	}
	
	foreach ($town_id_partial as $partial_town_id) {
		if ($partial_town_id == $town_id) continue;
		$town_link[] = town_links($partial_town_id);
	}
	
	if (!empty($town_link)) {
		$link_count = 0;
		$nLine = "<tr><td colspan=10 ><table class=dum_bar><tr>";
		$nLine .= "<td class=dum_header >Perhaps you are looking for:</td>";
		foreach ($town_link as $link) {
			$nLine .= $link_count==0?"<td>$link</td></tr>":"<tr><td>&nbsp;</td><td>$link</td></tr>";
			$link_count++;
		}
		$nLine .= "</table></td></tr>";
	}
	
	sort($used_words);
	sort($location_keywords);
	if ($location_keywords == $used_words) {
	
		//Get establishments
		$statement = "SELECT DISTINCT(a.establishment_code), a.establishment_name
						FROM nse_establishment AS a
						JOIN nse_establishment_location AS d ON a.establishment_code = d.establishment_code
						WHERE a.aa_estab='Y' && a.active='1' &&";
		if ($country_id != 0) $statement .= "d.country_id=$country_id && ";
		if ($province_id != 0) $statement .= "d.province_id=$province_id && ";
		if ($town_id != 0) $statement .= " d.town_id=$town_id && ";
		if ($suburb_id != 0) $statement .= " d.suburb_id=$suburb_id && ";
		if (substr($statement, -3) == '&& ') $statement = substr($statement, 0, -3);
		
		
			
		$statement .= " ORDER BY RAND()";
		
		$sql_estab = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_estab->execute();
		$sql_estab->store_result();
		$sql_estab->bind_result($establishment_code, $establishment_name);
		$result_count = $sql_estab->num_rows;
		while ($sql_estab->fetch()) {
			$establishment_list[$establishment_code] = $establishment_name;
		}
		$sql_estab->free_result();
		$sql_estab->close();
	
	
		foreach($establishment_list AS $establishment_code=>$establishment_name) {
			if ($counter < $GLOBALS['result_length'])  {
				$dir_name = directory_name($establishment_code . '_' . $establishment_name);
				$locations = get_locations($establishment_code);
				$description = get_description($establishment_code);
				$price = get_pricing($establishment_code);
				$line_color = $line_color=='d9d9d9'?'ffffff':'d9d9d9';
				$rating = get_rating($establishment_code);
				
				$line = str_replace('<!-- establishment_name -->', $establishment_name, $GLOBALS['lTemplate']);
				$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
				$line = str_replace('<!-- position -->', $counter, $line);
				$line = str_replace('<!-- search_string -->', $GLOBALS['search_string'], $line);
				$line = str_replace('<!-- directory_name -->', $dir_name, $line);
				$line = str_replace('<!-- price -->', $price, $line);
				$line = str_replace('<!-- locations -->', $locations, $line);
				$line = str_replace('<!-- description -->', $description, $line);
				$line = str_replace('<!-- line_color -->', $line_color, $line);
				$line = str_replace('<!-- rating -->', $rating, $line);
				$line = str_replace('<!-- search_type -->', $search_type, $line);
				
				$nLine .= $line;
			}
			
			$result_cache .= "$establishment_code,";
			
			
			$counter++;
		}
		
		$result_id = sha1($nLine + microtime());
	
		//Save Search
		$options = '';
		if (isset($_POST['search_estab'])) $options = 'E';
		if (isset($_POST['search_loc'])) $options = 'L';
		$statement = "INSERT INTO nse_search_result_cache (search_hash, search_result, search_string, search_options) VALUES (?,?,?,?)";
		$sql_save = $GLOBALS['dbCon']->prepare($statement);
		$sql_save->bind_param('ssss', $result_id, $result_cache, $GLOBALS['search_string'], $options);
		$sql_save->execute();
		$sql_save->close();
	
	
		//Nav Tags
		$total_pages = ceil($result_count/$GLOBALS['result_length']);
		$current_page = isset($_GET['cp'])?$_GET['cp']:'1';
		$previous_cp = $current_page-1;
		$previous_link = $current_page==1?'':"<a href='/index.php?p=search2&cp=$previous_cp&sr=$result_id' >&#9668;Previous</a >";
		$next_cp = $current_page+1;
		$next_link = $current_page==$total_pages?'':"<a href='/index.php?p=search2&cp=$next_cp&sr=$result_id' >Next&#9658;</a >";
	
	} else {
		$nLine .= '<tr><td colspan=10 align=center><br>-- No Results Found --</br>&nbsp;</td></tr>';
		$result_count = 0;
		$total_pages = 1;
		$current_page = 1;
		$previous_link = '';
		$next_link = '';
	}
	
	//Replace Tags
	$GLOBALS['content'] = str_replace('<!-- result_count -->', $result_count, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- total_pages -->', $total_pages, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- current_page -->', $current_page, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- previous_link -->', $previous_link, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- next_link -->', $next_link, $GLOBALS['content']);
	//echo "Country: $country_id<br />Province: $province_id<br />Town: $town_id<br />Suburb: $suburb_id<br />";
	//print_r($with_keywords);
	return $nLine;
}

function search_general() {
	//Vars
	$search_string = $GLOBALS['search_string'];
	$word_list = str_word_count($search_string, 1);
	$in_found = false;
	$with_found = false;
	$country_id = 0;
	$province_id = 0;
	$town_id = 0;
	$suburb_id = 0;
	$icon_list = array();
	$icon_id = '';
	$restype_list = '';
	$restype_id = '';
	$counter = 0;
	$nLine = '';
	$line_color = 'd9d9d9';
	$establishment_code = '';
	$establishment_name = '';
	$phrase_hash = array();
	$location_counter = 0;
	$with_keywords = array();
	$with_phrase = array();
	$result_cache = '';
	
	$stem = new Stemmer();
	
	//Check for 'in' & 'with'
	foreach ($word_list as $word) {
		if (!$in_found && !$with_found && $word != 'with' && $word != 'in') $general_keywords[] = $word;
		if ($in_found && $word != 'with') $location_keywords[] = $word;
		if ($with_found && $word != 'in') $with_keywords[] = $word;
		if ($word == 'in') {
			$in_found = true;
			$with_found = false;
		}
		if ($word == 'with') {
			$with_found = true;
			$in_found = false;
		}
	}
	
	//Check if other words might also indicate a location
	if (empty($location_keywords)) {
		$general_counter = 0;
		foreach($general_keywords AS $word) {
			$general_phrase[$general_counter]= $word;
			if ($general_counter != 0) $general_phrase[$general_counter-1] .= " $word";
			$general_counter++;
		}
		//var_dump($general_phrase);
		
		foreach($general_phrase AS $search_word) {
			if (str_word_count($search_word, 0) == 1) continue;
			$country_id = locate_country($search_word);
			$province_id = locate_province($search_word);
			if ($town_id == 0) {$town_id = locate_town($search_word);}
			$suburb_id = locate_suburb($search_word);
			if ($country_id != 0 || $province_id != 0 || $town_id != 0 || $suburb_id != 0) {
				$words = explode(' ', $search_word);
				foreach ($words as $word) {
					$location_keywords[] = $word;
				}
			}
		}
		
		foreach($general_keywords AS $search_word) {
			$country_id = locate_country($search_word);
			$province_id = locate_province($search_word);
			if ($town_id == 0) {$town_id = locate_town($search_word);}
			$suburb_id = locate_suburb($search_word);
			if ($country_id != 0 || $province_id != 0 || $town_id != 0 || $suburb_id != 0) {
				if (empty($location_keywords)) {
					$location_keywords[] = $search_word;
				} else {
					if (!in_array($search_word, $location_keywords)) $location_keywords[] = $search_word;
				}
			}
		}
	}
	
	//Check if other words indicate an activity (icons)
	if (empty($with_keywords)) {
		$with_counter = 0;
		foreach($general_keywords AS $search_word) {
			$with_phrase[$with_counter] = $search_word;
			if ($with_counter != 0) $with_phrase[$with_counter-1] .= " $search_word";
			$with_counter++;
		}
		
		foreach ($with_phrase as $search_string) {
			$icon_list = locate_activities($search_string);
			if (count($icon_list) > 0) {
				$words = explode(' ', $search_string);
				foreach ($words as $word) {
					if (!in_array($word, $with_keywords)) $with_keywords[] = $word;
				}
			}
		}
	}
	
	if (!empty($location_keywords)) {
		foreach($location_keywords AS $search_word) {
			$location_phrase[$location_counter] = $search_word;
			if ($location_counter != 0) $location_phrase[$location_counter-1] .= " $search_word";
			$location_counter++;
		}
		
		foreach ($location_phrase AS $search_word) {
			if (str_word_count($search_word,0) == 1) continue;
			$phrase_hash[$search_word] = sha1(strtolower($search_word));
		}
		

		foreach ($location_keywords AS $search_word) {
			$word_hash[] = sha1(strtolower($search_word));
		}
		
		//Check for country ID
		foreach ($location_phrase AS $search_string) {
			$country_id = locate_country($search_string);
			if ($country_id != 0) break;
		}
		if ($country_id == 0) {
			foreach ($location_keywords AS $search_string) {
				$country_id = locate_country($search_string);
			}
		}
		
		//Check for province ID
		if ($country_id != 0) {
			foreach ($location_phrase AS $search_string) {
				$province_id = locate_province($search_string, $country_id);
				if ($province_id != 0) break;
			}
			
			if ($province_id == 0) {
				foreach ($location_keywords AS $search_string) {
					$province_id = locate_province($search_string, $country_id);
					if ($province_id != 0) break;
				}
			}
		} else {
			foreach ($location_phrase AS $search_string) {
				$province_id = locate_province($search_string);
				if ($province_id != 0) break;
			}
			
			if ($province_id == 0) {
				foreach ($location_keywords AS $search_string) {
					$province_id = locate_province($search_string);
					if ($province_id != 0) break;
				}
			}
		}

		
		//Check for town ID
		if ($province_id != 0) {
			foreach ($location_phrase AS $search_string) {
				$town_id = locate_town($search_string, $province_id);
				if ($town_id != 0) break;
			}
			if ($town_id == 0) {
				foreach ($location_keywords AS $search_string) {
					$town_id = locate_town($search_string, $province_id);
					if ($town_id != 0) break;
				}
			}
		} else {
		foreach ($location_phrase AS $search_string) {
				$town_id = locate_town($search_string);
				if ($town_id != 0) break;
			}
			if ($town_id == 0) {
				foreach ($location_keywords AS $search_string) {
					$town_id = locate_town($search_string);
					if ($town_id != 0) break;
				}
			}
		}
		
		
		//Check for suburb ID
		if ($town_id != 0) {
			foreach ($location_phrase AS $search_string) {
				$suburb_id = locate_suburb($search_string, $town_id);
				if ($suburb_id != 0) break;
			}
			if ($suburb_id == 0) {
				foreach ($location_keywords AS $search_string) {
					$suburb_id = locate_suburb($search_string, $town_id);
				if ($suburb_id != 0) break;
				}
			}
		} else {
		foreach ($location_phrase AS $search_string) {
				$suburb_id = locate_suburb($search_string);
				if ($suburb_id != 0) break;
			}
			if ($suburb_id == 0) {
				foreach ($location_keywords AS $search_string) {
					$suburb_id = locate_suburb($search_string);
				if ($suburb_id != 0) break;
				}
			}
		}
	}
	
	//Get icon list - phrase first
	if (!empty($with_keywords)) {
		$with_counter = 0;
		foreach($with_keywords AS $search_word) {
			$with_phrase[$with_counter] = $search_word;
			if ($with_counter != 0) $with_phrase[$with_counter-1] .= " $search_word";
			$with_counter++;
		}
		
		foreach ($with_phrase as $search_string) {
			if (str_word_count($search_string,0) == 1) continue;
			$icon_list += locate_activities($search_string);
		}
		
		//Get icon list - now individual words
		foreach ($with_keywords AS $search_word) {
			$icon_list += locate_activities($search_word);
		}
		
		if (!empty($icon_list)) $icon_list = array_unique($icon_list);
	}
	
	//Check for restype - Phrase first
	if (!empty($general_keywords)) {
		$general_phrase = implode(' ', $general_keywords);
		$general_phrase = "%$general_phrase%";
		$statement = "SELECT subcategory_id FROM nse_restype_subcategory_lang WHERE restype_keywords like ?";
		$sql_restype = $GLOBALS['dbCon']->prepare($statement);
		$sql_restype->bind_param('s', $general_phrase);
		$sql_restype->execute();
		$sql_restype->store_result();
		$sql_restype->bind_result($restype_id);
		while ($sql_restype->fetch()) {
			$restype_list[] = $restype_id;
		}
		
		if (empty($restype_list)) {
			foreach ($general_keywords AS $search_word) {
				$sql_restype = $GLOBALS['dbCon']->prepare($statement);
				$search_word = $stem->stem($search_word);
				$search_word = "%$search_word%";
				$sql_restype->bind_param('s', $search_word);
				$sql_restype->execute();
				$sql_restype->store_result();
				$sql_restype->bind_result($restype_id);
				while ($sql_restype->fetch()) {
					$restype_list[] = $restype_id;
				}
			}
		}
	}
	
	if (empty($country_id) && empty($province_id) && empty($town_id) && empty($suburb_id) && empty($restype_list) && empty($icon_list)) {
		$nLine .= '<tr><td colspan=10 align=center><br>-- No Results Found --</br>&nbsp;</td></tr>';
		$result_count = 0;
		$total_pages = 1;
		$current_page = 1;
		$previous_link = '';
		$next_link = '';
		
	} else {
	
		//Get establishments
		$statement = "SELECT DISTINCT(a.establishment_code), a.establishment_name
						FROM nse_establishment AS a
						LEFT JOIN nse_establishment_restype AS b ON a.establishment_code = b.establishment_code
						LEFT JOIN nse_establishment_icon AS c ON a.establishment_code = c.establishment_code
						JOIN nse_establishment_location AS d ON a.establishment_code = d.establishment_code
						WHERE a.aa_estab='Y' && a.active='1' && ";
		if ($country_id != 0) $statement .= "d.country_id=$country_id && ";
		elseif ($province_id != 0) $statement .= "d.province_id=$province_id && ";
		elseif ($town_id != 0) $statement .= "d.town_id=$town_id && ";
		elseif ($suburb_id != 0) $statement .= "d.suburb_id=$suburb_id && ";
		if (substr($statement, -3) == '&& ') $statement = substr($statement, 0, -3);
		
		if (!empty($restype_list)) {
			$statement .= "&& (";
			foreach ($restype_list As $k=>$restype_id) {
				$statement .= $k==0?'':' || ';
				$statement .= "b.subcategory_id=$restype_id";
			}
			$statement .= ") ";
		}
		
		if (!empty($icon_list)) {
			$statement .= "&& (";
			foreach ($icon_list As $k=>$icon_id) {
				$statement .= $k==0?'':' || ';
				$statement .= "c.icon_id=$icon_id";
			}
			$statement .= ") ";
		}
		
		$statement .= "ORDER BY RAND() ";
		
		$sql_estab = $GLOBALS['dbCon']->prepare($statement);
		$sql_estab->execute();
		$sql_estab->store_result();
		$result_count = $sql_estab->num_rows;
		$sql_estab->bind_result($establishment_code, $establishment_name);
		while ($sql_estab->fetch()) {
			$establishment_list[$establishment_code] = $establishment_name;
		}
		$sql_estab->free_result();
		$sql_estab->close();
		
	
		foreach($establishment_list AS $establishment_code=>$establishment_name) {
			if ($counter < $GLOBALS['result_length'])  {
				$dir_name = directory_name($establishment_code . '_' . $establishment_name);
				$locations = get_locations($establishment_code);
				$description = get_description($establishment_code);
				$price = get_pricing($establishment_code);
				$line_color = $line_color=='d9d9d9'?'ffffff':'d9d9d9';
				$rating = get_rating($establishment_code);
				
				$line = str_replace('<!-- establishment_name -->', $establishment_name, $GLOBALS['lTemplate']);
				$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
				$line = str_replace('<!-- position -->', $counter, $line);
				$line = str_replace('<!-- search_string -->', urlencode($GLOBALS['search_string']), $line);
				$line = str_replace('<!-- directory_name -->', $dir_name, $line);
				$line = str_replace('<!-- price -->', $price, $line);
				$line = str_replace('<!-- locations -->', $locations, $line);
				$line = str_replace('<!-- description -->', $description, $line);
				$line = str_replace('<!-- line_color -->', $line_color, $line);
				$line = str_replace('<!-- rating -->', $rating, $line);
				$line = str_replace('<!-- search_type -->', '', $line);
				
				$nLine .= $line;
			}
			$result_cache .= "$establishment_code,";
			
			$counter++;
			
		}
		
		$result_id = sha1($nLine + microtime());
		
		//Save Search
		$options = '';
		if (isset($_POST['search_estab'])) $options = 'E';
		if (isset($_POST['search_loc'])) $options = 'L';
		$statement = "INSERT INTO nse_search_result_cache (search_hash, search_result, search_string, search_options) VALUES (?,?,?,?)";
		$sql_save = $GLOBALS['dbCon']->prepare($statement);
		$sql_save->bind_param('ssss', $result_id, $result_cache, $GLOBALS['search_string'], $options);
		$sql_save->execute();
		$sql_save->close();
		
		//Nav Tags
		$total_pages = ceil($result_count/$GLOBALS['result_length']);
		$current_page = isset($_GET['cp'])?$_GET['cp']:'1';
		$previous_cp = $current_page-1;
		$previous_link = $current_page==1?'':"<a href='/index.php?p=search2&cp=$previous_cp&sr=$result_id' >&#9668;Previous</a >";
		$next_cp = $current_page+1;
		$next_link = $current_page==$total_pages?'':"<a href='/index.php?p=search2&cp=$next_cp&sr=$result_id' >Next&#9658;</a >";
	}
	
	//Replace Tags
	$GLOBALS['content'] = str_replace('<!-- result_count -->', $result_count, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- total_pages -->', $total_pages, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- current_page -->', $current_page, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- previous_link -->', $previous_link, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- next_link -->', $next_link, $GLOBALS['content']);
	//echo "Country: $country_id<br />Province: $province_id<br />Town: $town_id<br />Suburb: $suburb_id<br />";
	//print_r($with_keywords);
	return $nLine;
}

/**
 * Replaces special characters with the english alphabet (a-Z) equivalent.
 * @param $name
 */
function plainize_name($name) {
	$name = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $name);
	$name = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $name );
	$name = htmlentities($name);
	$name = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $name);
	$name = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $name);
	$name = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $name);
	$name = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $name);
	$name = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $name);
	$name = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $name);
	
	return $name;
}

/**
 * Returns the file name (excluding extention) from a given establishmont name.
 * @param $name
 */
function directory_name($name) {
	$dir_name = plainize_name($name);
	$dir_name = str_replace(' ', '_', $dir_name);
	$dir_name = strtolower($dir_name);
	
	return $dir_name;
}

function get_locations($establishment_code) {
	//Vars
	$suburb_id = '';
	$town_id = '';
	$province_id = '';
	$country_id = '';
	$location_string = '';
	$suburb_name = '';
	$town_name = '';
	$province_name = '';
	$country_name = '';
	
	//Get Location IDs
	$statement = "SELECT suburb_id, town_id, province_id, country_id FROM nse_establishment_location WHERE establishment_code=? LIMIT 1";
	$sql_ids = $GLOBALS['dbCon']->prepare($statement);
	$sql_ids->bind_param('s', $establishment_code);
	$sql_ids->execute();
	$sql_ids->store_result();
	$sql_ids->bind_result($suburb_id, $town_id, $province_id, $country_id);
	$sql_ids->fetch();
	$sql_ids->free_result();
	$sql_ids->close();
	
	//Get suburb name
	if ($suburb_id != 0) {
		$statement = "SELECT suburb_name FROM nse_location_suburb WHERE suburb_id=? LIMIT 1";
		$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb->bind_param('i', $suburb_id);
		$sql_suburb->execute();
		$sql_suburb->store_result();
		$sql_suburb->bind_result($suburb_name);
		$sql_suburb->fetch();
		$sql_suburb->free_result();
		$sql_suburb->close();
		
		$location_string .= "$suburb_name, ";
	}
	
	//Get town name
	if ($town_id != 0) {
		$statement = "SELECT town_name FROM nse_location_town WHERE town_id=?";
		$sql_town = $GLOBALS['dbCon']->prepare($statement);
		$sql_town->bind_param('i', $town_id);
		$sql_town->execute();
		$sql_town->store_result();
		$sql_town->bind_result($town_name);
		$sql_town->fetch();
		$sql_town->free_result();
		$sql_town->close();
		
		$location_string .= "$town_name, ";
	}
	
	//Get province name
	if ($province_id != 0) {
		$statement = "SELECT province_name FROM nse_location_province WHERE province_id=?";
		$sql_province = $GLOBALS['dbCon']->prepare($statement);
		$sql_province->bind_param('i', $province_id);
		$sql_province->execute();
		$sql_province->store_result();
		$sql_province->bind_result($province_name);
		$sql_province->fetch();
		$sql_province->free_result();
		$sql_province->close();
		
		$location_string .= "$province_name, ";
	}
	
	//Get country_name
	if ($country_id != 0) {
		$statement = "SELECT country_name FROM nse_location_country_lang WHERE country_id=? && language_code='EN'";
		$sql_country = $GLOBALS['dbCon']->prepare($statement);
		$sql_country->bind_param('i', $country_id);
		$sql_country->execute();
		$sql_country->store_result();
		$sql_country->bind_result($country_name);
		$sql_country->fetch();
		$sql_country->free_result();
		$sql_country->close();
		
		$location_string .= "$country_name";
	}
	
	return $location_string;
}

/**
 * Get the short description of a given establishment code and concatenat if too long
 *
 * @param $establishment_code
 */
function get_description($establishment_code) {
	//Vars
	$description = '';
	$new_description = '';
	
	$statement = "SELECT establishment_description FROM nse_establishment_descriptions WHERE establishment_code=? && (description_type='short_description' || description_type='short description') && language_code='EN' LIMIT 1";
	$sql_description = $GLOBALS['dbCon']->prepare($statement);
	$sql_description->bind_param('s', $establishment_code);
	$sql_description->execute();
	$sql_description->store_result();
	$sql_description->bind_result($description);
	$sql_description->fetch();
	$sql_description->free_result();
	$sql_description->close();
	
	$word_count = str_word_count($description, 0);
	
	
	if ($word_count > 50) {
		$word_list = str_word_count($description, 1);
		for ($i=0; $i<45; $i++) {
			$new_description .= $word_list[$i] . ' ';
		}
		$description = $new_description . '...';
	}
	
	return $description;
}


function get_pricing($establishment_code) {
	//Vars
	$category = '';
	$category_prefix = '';
	$category_suffix = '';
	$price_description = '';
	$price_html = '';
	$price = '';
	
	$statement = "SELECT category_prefix, category, category_suffix FROM nse_establishment_pricing WHERE establishment_code=? LIMIT 1";
	$sql_price = $GLOBALS['dbCon']->prepare($statement);
	$sql_price->bind_param('s', $establishment_code);
	$sql_price->execute();
	$sql_price->store_result();
	$sql_price->bind_result($category_prefix, $category, $category_suffix);
	$sql_price->fetch();
	$sql_price->free_result();
	$sql_price->close();
	
	if ($category_suffix == 'OR') $category_suffix = 'On Request';
	
	switch (substr($category,0,1)) {
		case 'G':
			$price_html = ":<span title=\"Under R100\">$category</span>";
			break;
		case 'F':
			$price_html = ":<span title=\"From R100 - R149\">$category</span>";
			break;
		case 'E':
			$price_html = ":<span title=\"From R150 - R199\">$category</span>";
			break;
		case 'D':
			$price_html = ":<span title=\"From R200 - R299\">$category</span>";
			break;
		case 'C':
			$price_html = ":<span title=\"From R300 - R399\">$category</span>";
			break;
		case 'B':
			$price_html = ":<span title=\"From R400 - R499\">$category</span>";
			break;
		case 'A':
			if ($category == 'A') {
					$price_html = ":<span title=\"From R500 - R999\">$category</span>";
			} else {
					$multiplier = substr($category, 1);
					$cost = $multiplier * 1000;
					$price_html = ":<span title=\"Over R$cost\">$category</span>";
			}
			break;
	}

	if (strpos($category_suffix, 'S/C') > 0 ) $category_suffix = str_replace('S/C', "<span title=\"Self-Catering\">S/C</span>", $category_suffix);
	if (strpos($category_suffix, 'B&B') > 0 ) $category_suffix = str_replace('B&B', "<span title=\"Bed & Breakfast\">B&B</span>", $category_suffix);
	if (strpos($category_suffix, 'DBB') > 0 ) $category_suffix = str_replace('DBB', "<span title=\"Dinner, Bed & Breakfast\">DBB</span>", $category_suffix);
	if (strpos($category_suffix, 'FB') > 0 ) $category_suffix = str_replace('FB', "<span title=\"Full Board\">FB</span>", $category_suffix);
	if (strpos($category_suffix, 'pp') == 0 ) $category_suffix = str_replace('pp', "<span title=\"per Person\">pp</span>", $category_suffix);
	
	if (!empty($category_prefix) && !empty($price_html)) $price = $category_prefix . $price_html;
	if (!empty($category_suffix)) $price .= "<br />$category_suffix";
	
	return $price;
}

function get_rating($establishment_code) {
	//Vars
	$category_name = '';
	$star_grading = 0;
	$rate_table = '<table align=left cellpadding=0 cellspacing=0><tr><td>';
	
	$statement = "SELECT b.aa_category_name
					FROM nse_establishment AS a
					JOIN nse_aa_category AS b ON a.aa_category_code = b.aa_category_code
					WHERE a.establishment_code=? LIMIT 1";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);

	$sql_category->bind_param('s', $establishment_code);
	$sql_category->execute();
	$sql_category->store_result();
	$sql_category->bind_result($category_name);
	$sql_category->fetch();
	$sql_category->free_result();
	$sql_category->close();
	
	if (!empty($category_name))	$rate_table .= '<img src="/lookup_images/aatravel_50.gif" >';
	
	$rate_table .= '</td></tr></table>';
	
	if (!empty($category_name)) $rate_table .= "<span class=est_body2>Quality Assured<br><b>$category_name</b></span><p>";
	
	$statement = "SELECT MAX(star_grading) FROM nse_establishment_restype WHERE establishment_code=?";
	$sql_star = $GLOBALS['dbCon']->prepare($statement);
	$sql_star->bind_param('s', $establishment_code);
	$sql_star->execute();
	$sql_star->store_result();
	$sql_star->bind_result($star_grading);
	$sql_star->fetch();
	$sql_star->free_result();
	$sql_star->close();
	
	$rate_table .= '<span style="color: black; font-family: arial; font-size: 8pt">';

	if (!empty($star_grading)) $rate_table .= "<p>$star_grading Star (TGCSA)</p>";

//	for ($i=0; $i<$star_grading; $i++) {
//		$rate_table .= '<img src="/images/star.png">';
//	}
	
	$rate_table .= '</span>';
	
	return $rate_table;
}

function locate_country($search_string) {
	$country_id = 0;
	$search_hash = sha1(strtolower($search_string));
	$search_hash = "%$search_hash%";
	
	$statement = "SELECT country_id FROM nse_location_country_lang WHERE search_hashes LIKE ? LIMIT 1";
	$sql_country = $GLOBALS['dbCon']->prepare($statement);
	$sql_country->bind_param('s', $search_hash);
	$sql_country->execute();
	$sql_country->store_result();
	$sql_country->bind_result($country_id);
	$sql_country->fetch();
	$sql_country->free_result();
	$sql_country->close();

	return $country_id;
}


function locate_province($search_string, $country_id=0) {
	$province_id = 0;
	$search_hash = sha1(strtolower($search_string));
	$search_hash = "%$search_hash%";
	
	if ($country_id != 0) {
		$statement = "SELECT province_id FROM nse_location_province WHERE search_hashes LIKE ? && country_id=? LIMIT 1";
		$sql_province = $GLOBALS['dbCon']->prepare($statement);
		$sql_province->bind_param('si', $search_hash, $country_id);
		$sql_province->execute();
		$sql_province->store_result();
		$sql_province->bind_result($province_id);
		$sql_province->fetch();
	} else {
		$statement = "SELECT province_id FROM nse_location_province WHERE search_hashes LIKE ? LIMIT 1";
		$sql_province = $GLOBALS['dbCon']->prepare($statement);
		$sql_province->bind_param('s', $search_hash);
		$sql_province->execute();
		$sql_province->store_result();
		$sql_province->bind_result($province_id);
		$sql_province->fetch();
	}
	$sql_province->free_result();
	$sql_province->close();
	
	return $province_id;
}

function locate_town($search_string, $province_id=0) {
	$town_id = 0;
	$search_hash = sha1(strtolower($search_string));
	//echo "\n\n$search_string\n\n";
	$search_hash = "%$search_hash%";
	
	//if ($province_id != 0) {
		//$statement = "SELECT town_id FROM nse_location_town WHERE search_hashes LIKE ? && province_id=? LIMIT 1";
		//$sql_town = $GLOBALS['dbCon']->prepare($statement);
		//$sql_town->bind_param('si', $search_hash, $province_id);
		//$sql_town->execute();
		//$sql_town->store_result();
		//$sql_town->bind_result($town_id);
		//$sql_town->fetch();
	//} else {
		$statement = "SELECT location_id FROM nse_search_locations WHERE location_name LIKE ? AND location_type like 'town' LIMIT 1";
		$sql_town = $GLOBALS['dbCon']->prepare($statement);
		$sql_town->bind_param('s', $search_string);
		$sql_town->execute();
		$sql_town->store_result();
		$sql_town->bind_result($town_id);
		$sql_town->fetch();
		
		//echo $search_string."-".$town_id."\n";
	//}
	$sql_town->free_result();
	$sql_town->close();
	
	
	return $town_id;
}

function locate_partial_town($search_string, $province_id=0) {
	$town_ids = array();
	$town_id = '';
	$search_string = "%$search_string%";
	
	if ($province_id != 0) {
		$statement = "SELECT town_id FROM nse_location_town WHERE (town_name LIKE ? || alternate_name LIKE ?) && province_id=? && country_id='1'";
		$sql_town = $GLOBALS['dbCon']->prepare($statement);
		$sql_town->bind_param('ssi', $search_string, $search_string, $province_id);
		$sql_town->execute();
		$sql_town->store_result();
		$sql_town->bind_result($town_id);
		while ($sql_town->fetch()) {
			$town_ids[] = $town_id;
		}
	} else {
		$statement = "SELECT town_id FROM nse_location_town WHERE (town_name LIKE ? || alternate_name LIKE ?) && country_id='1'";
		$sql_town = $GLOBALS['dbCon']->prepare($statement);
		$sql_town->bind_param('ss', $search_string, $search_string);
		$sql_town->execute();
		$sql_town->store_result();
		$sql_town->bind_result($town_id);
		while ($sql_town->fetch()) {
			$town_ids[] = $town_id;
		}
	}
	$sql_town->free_result();
	$sql_town->close();
	
	$town_ids = array_unique($town_ids);
	
	return $town_ids;
}

function town_links($town_id) {
	//Vars
	$town_name = '';
	$province_name = '';
	
	//Get Town Names
	$statement = "SELECT a.town_name, b.province_name
					FROM nse_location_town AS a
					JOIN nse_location_province AS b ON a.province_id=b.province_id
					WHERE a.town_id=?
					LIMIT 1";
	$sql_town_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_town_name->bind_param('i', $town_id);
	$sql_town_name->execute();
	$sql_town_name->store_result();
	$sql_town_name->bind_result($town_name, $province_name);
	$sql_town_name->fetch();
	$sql_town_name->free_result();
		
	$town_name = "<span onClick=\"search('$town_name, $province_name')\" class=search_link >$town_name, $province_name</span>";
	
	return $town_name;
	
}

function locate_suburb($search_string, $town_id=0) {
	$suburb_id = 0;
	$search_hash = sha1(strtolower($search_string));
	$search_hash = "%$search_string%";
	
	$statement = "SELECT location_id FROM nse_search_locations WHERE location_name LIKE ? AND location_type like 'suburb' LIMIT 1";
	$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
	$sql_suburb->bind_param('s', $search_string);
	$sql_suburb->execute();
	$sql_suburb->store_result();
	$sql_suburb->bind_result($suburb_id);
	$sql_suburb->fetch();
	
	/*if ($town_id != 0) {
		$statement = "SELECT suburb_id FROM nse_nlocations_suburbs WHERE suburb_name LIKE ? && town_id=? LIMIT 1";
		$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb->bind_param('si', $search_hash, $town_id);
		$sql_suburb->execute();
		$sql_suburb->store_result();
		$sql_suburb->bind_result($suburb_id);
		$sql_suburb->fetch();
	} else {
		$statement = "SELECT suburb_id FROM nse_nlocations_suburbs WHERE suburb_name LIKE ? LIMIT 1";
//		echo "$search_string - $search_hash<br>";
		$sql_suburb = $GLOBALS['dbCon']->prepare($statement);
		$sql_suburb->bind_param('s', $search_hash);
		$sql_suburb->execute();
		$sql_suburb->store_result();
		$sql_suburb->bind_result($suburb_id);
		$sql_suburb->fetch();
	}*/
	$sql_suburb->free_result();
	$sql_suburb->close();
	
	return $suburb_id;
}

function locate_activities($search_string) {
	$icon_list = array();
	$icon_id = '';
	
	$statement = "SELECT icon_id FROM nse_icon_lang WHERE search_hashes like ?";
	$sql_icons = $GLOBALS['dbCon']->prepare($statement);
	$search_string = sha1(strtolower($search_string));
	
	$search_hash = "%$search_string%";
	$sql_icons->bind_param('s', $search_hash);
	$sql_icons->execute();
	$sql_icons->store_result();
	$sql_icons->bind_result($icon_id);
	while ($sql_icons->fetch()) {
		$icon_list[] = $icon_id;
	}
	$sql_icons->free_result();
	$sql_icons->close();
	
	$icon_list = array_unique($icon_list);
	
	return $icon_list;
}

function retrieve_result() {
	//Vars
	$result_id = $_GET['sr'];
	$current_page = $_GET['cp'];
	$result_list = '';
	$search_results = array();
	$nLine = '';
	$line_color = 'd9d9d9';
	$search_string = '';
	$search_type = '';
	
	$statement = "SELECT search_result, search_string, search_options FROM nse_search_result_cache WHERE search_hash=? LIMIT 1";
	$sql_get = $GLOBALS['dbCon']->prepare($statement);
	$sql_get->bind_param('s', $result_id);
	$sql_get->execute();
	$sql_get->store_result();
	$sql_get->bind_result($result_list, $search_string, $search_type);
	$sql_get->fetch();
	$sql_get->free_result();
	$sql_get->close();
	
	$search_results = explode(',', $result_list);
	foreach ($search_results as $k=>$v) {
		if (empty($v)) unset($search_results[$k]);
	}
	$result_count = count($search_results);
	$start_results = ($current_page-1) * $GLOBALS['result_length'];
	$search_results = array_slice($search_results, $start_results, $GLOBALS['result_length']);

	$counter = ($current_page-1) * $GLOBALS['result_length'];
	$start_line = $counter;
	$line_counter = 0;
	
	foreach($search_results as $establishment_code) {
		$establishment_name = get_name($establishment_code);
		$dir_name = directory_name($establishment_code . '_' . $establishment_name);
		$locations = get_locations($establishment_code);
		$description = get_description($establishment_code);
		$price = get_pricing($establishment_code);
		$line_color = $line_color=='d9d9d9'?'ffffff':'d9d9d9';
		$rating = get_rating($establishment_code);
		
		$line = str_replace('<!-- establishment_name -->', $establishment_name, $GLOBALS['lTemplate']);
		$line = str_replace('<!-- establishment_code -->', $establishment_code, $line);
		$line = str_replace('<!-- position -->', $counter, $line);
		$line = str_replace('<!-- search_string -->', urlencode($search_string), $line);
		$line = str_replace('<!-- directory_name -->', $dir_name, $line);
		$line = str_replace('<!-- price -->', $price, $line);
		$line = str_replace('<!-- locations -->', $locations, $line);
		$line = str_replace('<!-- description -->', $description, $line);
		$line = str_replace('<!-- line_color -->', $line_color, $line);
		$line = str_replace('<!-- rating -->', $rating, $line);
		$line = str_replace('<!-- search_type -->', $search_type, $line);
		
		$nLine .= $line;
		
		$line_counter++;
		$counter++;
	}
	
	//Nav Tags
	$total_pages = ceil($result_count/$GLOBALS['result_length']);
	$current_page = isset($_GET['cp'])?$_GET['cp']:'1';
	$previous_cp = $current_page-1;
	$previous_link = $current_page==1?'':"<a href='/index.php?p=search2&cp=$previous_cp&sr=$result_id' >&#9668;Previous</a >";
	$next_cp = $current_page+1;
	$next_link = $current_page==$total_pages?'':"<a href='/index.php?p=search2&cp=$next_cp&sr=$result_id' >Next&#9658;</a >";
	
	//Replace Tags
	$GLOBALS['content'] = str_replace('<!-- result_count -->', $result_count, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- total_pages -->', $total_pages, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- current_page -->', $current_page, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- previous_link -->', $previous_link, $GLOBALS['content']);
	$GLOBALS['content'] = str_replace('<!-- next_link -->', $next_link, $GLOBALS['content']);
	
	return $nLine;
}


function get_name($establishment_code) {
	//Vars
	$establishment_name = '';
	
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=? LIMIT 1";
	$sql_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_name->bind_param('s', $establishment_code);
	$sql_name->execute();
	$sql_name->store_result();
	$sql_name->bind_result($establishment_name);
	$sql_name->fetch();
	$sql_name->free_result();
	$sql_name->close();
	
	return $establishment_name;
}































?>