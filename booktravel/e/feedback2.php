<?php
/**
 * User Feedback
 *
 * Handles user feedback for an establishment. Presents the form, saves the results to the DB and sends an email.
 *
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category feedback
 */

if (isset($_POST['saveData'])){
	save_feedback();
} else {
	feedback_form();
}

/**
 * Feedback Form
 *
 * Presents the feedback form to the site visitor. It uses the feedback form template (/templates/feedback.tpl), Replaces various tags and presents the result.
 *
 * @return bool
 */
function feedback_form() {
    //Vars
    $establishment_name = '';
   
	//Get template
	$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/feedback.tpl');
	
	//Get establishment detail
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_data = $GLOBALS['dbCon']->prepare($statement);
	echo mysqli_error($GLOBALS['dbCon']);
	$sql_data->bind_param('s', $_GET['id']);
	$sql_data->execute();
	$sql_data->bind_result($establishment_name);
	$sql_data->store_result();
	$sql_data->fetch();
	
	//Replace tags
	$template = str_replace('<!-- establishment_name -->', $establishment_name, $template);
	
	//Serve
	$GLOBALS['content'] = $template;
	
	return TRUE;
}

/**
 * Manage Feedback Results
 *
 * Saves the results of the feedback to the DB and sends an email to 'feedback@booktravel.travel'. The visitors data is also saved or updated.
 *
 * @return bool
 */
function save_feedback() {
    //session_start();
    
    //Vars
    $establishment_name = '';
    $establishment_town = '';
    $suburb = '';
    $establishment_email = '';
    $telephone = '';
    $value_trans = array(5=>'Excellent', 4=>'Very Good', 3=>'Good', 2=>'Average', 1=>'Poor', -1=>'Pathetic', 'na'=>'NA');
    $establishment_code = $_GET['id'];
	
	//Check Securicode
	include_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';
	$securimage = new Securimage();
	if ($securimage->check($_POST['captcha_code']) == false) {
		echo '<script>alert("Security Error!\nPlease enter the correct security code");history.back()</script>';
		die();
	}
	
	foreach($_POST as $k=>$v) {
		$$k = $v;
	}
	$updates = isset($updates)?1:0;
	
	//Update Data
	if (isset($_POST['new_visit']) && $_POST['new_visit'] == 1) {
		$statement = "INSERT INTO nse_visitor
    					(title_id, firstname,surname,email,country,town,phone,cell,fax,age_group,occupation,gender,receive_updates)
    					VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_insert->bind_param('sssssssssssss', $title, $fname,$lname,$email,$country,$town,$tel,$cell,$fax,$ageGroup,$occupation,$gender,$updates);
		$sql_insert->execute();
		$new_visit = mysqli_insert_id($GLOBALS['dbCon']);
	} else {
		$statement = "UPDATE nse_visitor SET
						title_id=?, firstname=?,surname=?,email=?,country=?,town=?,phone=?,cell=?,fax=?,
						age_group=?,occupation=?,gender=?,receive_updates=? WHERE visitor_id=?";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_insert->bind_param('ssssssssssssss', $title, $fname,$lname,$email,$country,$town,$tel,$cell,$fax,$ageGroup,$occupation,$gender,$updates,$new_visit);
		$sql_insert->execute();
		$new_visit = $_POST['new_visit'];
	}
    
	//Save feedback data to DB
	$statement = "INSERT INTO
					nse_establishment_feedback
						(establishment_code, feedback_date, visitor_id, feedback_status, visitor_comment, visit_date)
					VALUES
						(?,NOW(),?,'1',?,?)";
	$sql_feedback_data = $GLOBALS['dbCon']->prepare($statement);
	$sql_feedback_data->bind_param('ssss', $_GET['id'], $new_visit, $_POST['comments'], $_POST['date']);
	$sql_feedback_data->execute();
	$feedback_id = $sql_feedback_data->insert_id;
	
	//Save feedback results to DB
	$result = array('2_recommends_as'=>'2_recommends_as', '2_quality'=>'2_quality', '2_staff'=>'2_staff',
					'2_staff_dependability'=>'2_staff_dependability', '2_value'=>'2_value', '2_room_cleanliness'=>'2_room_cleanliness',
					'2_room_comfort'=>'2_room_comfort', '2_appliances'=>'2_appliances', '2_bathrooms'=>'2_bathrooms',
					'2_kitchen'=>'2_kitchen','2_food'=>'2_food','2_management'=>'2_management','2_security'=>'2_security',
					'2_checkin'=>'2_checkin','2_activities'=>'2_activities');
	$statement = "INSERT INTO nse_feedback_results (feedback_id, category_code, feedback_value) VALUES (?,?,?)";
	$sql_feedback_insert = $GLOBALS['dbCon']->prepare($statement);
	foreach ($result as $key=>$field) {
		if ($_POST[$key] == 'na') continue;
	    $sql_feedback_insert->bind_param('sss', $feedback_id, $field, $_POST[$key]);
	    $sql_feedback_insert->execute();
	}
	
	//Send Email
	$email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/feedback_email.tpl');
	
	//Get Establishment Name
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_name->bind_param('s', $_GET['id']);
	$sql_name->execute();
	$sql_name->bind_result($establishment_name);
	$sql_name->store_result();
	$sql_name->fetch();
	
	//Get Establishment Town
	$statement = "SELECT b.town_name, c.suburb_name
					FROM nse_establishment_location AS a
					JOIN nse_location_town AS b ON a.town_id=b.town_id
					LEFT JOIN nse_location_suburb AS c ON a.suburb_id=c.suburb_id
					WHERE a.establishment_code=?";
	$sql_location = $GLOBALS['dbCon']->prepare($statement);
	$sql_location->bind_param('s', $_GET['id']);
	$sql_location->execute();
	$sql_location->bind_result($establishment_town,$suburb);
	$sql_location->store_result();
	$sql_location->fetch();
	if (!empty($suburb)) $establishment_town = "$suburb, $establishment_town";
	
	//Get establishment telephone & email
	$statement = "SELECT a.contact_email, a.contact_tel, a.contact_cell, b.reservation_tel, b.reservation_email, b.reservation_cell
					FROM nse_establishment_contact AS a
					JOIN nse_establishment_reservation AS b ON a.establishment_code=b.establishment_code
					WHERE a.establishment_code=?";
	$sql_contact = $GLOBALS['dbCon']->prepare($statement);
	$sql_contact->bind_param('s', $_GET['id']);
	$sql_contact->execute();
	$sql_contact->bind_result($c_email, $c_tel, $c_cell, $r_tel, $r_email, $r_cell);
	$sql_contact->store_result();
	$sql_contact->fetch();
	$establishment_tel = $r_tel;
	if (empty($establishment_tel)) $establishment_tel = $c_tel;
	if (empty($establishment_tel)) $establishment_tel = $r_cell;
	if (empty($establishment_tel)) $establishment_tel = $c_cell;
	$establishment_email = $r_email;
	if (empty($establishment_email)) $establishment_email = $c_email;
	
	//Replace Tags
	$email_template = str_replace('<!-- establishment_name -->', $establishment_name, $email_template);
	$email_template = str_replace('<!-- town -->', $establishment_town, $email_template);
	$email_template = str_replace('<!-- email -->', $establishment_email, $email_template);
	$email_template = str_replace('<!-- telephone -->', $establishment_tel, $email_template);
	$email_template = str_replace('<!-- name -->', "$fname $lname", $email_template);
	$email_template = str_replace('<!-- traveler_email -->', $email, $email_template);
	$email_template = str_replace('<!-- visit_date -->', $date, $email_template);
	
	$email_template = str_replace('<!-- 2_recommends_as -->', $value_trans[$_POST['2_recommends_as']], $email_template);
	$email_template = str_replace('<!-- 2_quality -->', $value_trans[$_POST['2_quality']], $email_template);
	$email_template = str_replace('<!-- 2_staff -->', $value_trans[$_POST['2_staff']], $email_template);
	$email_template = str_replace('<!-- 2_staff_dependability -->', $value_trans[$_POST['2_staff_dependability']], $email_template);
	$email_template = str_replace('<!-- 2_value -->', $value_trans[$_POST['2_value']], $email_template);
	$email_template = str_replace('<!-- 2_room_cleanliness -->', $value_trans[$_POST['2_room_cleanliness']], $email_template);
	$email_template = str_replace('<!-- 2_room_comfort -->', $value_trans[$_POST['2_room_comfort']], $email_template);
	$email_template = str_replace('<!-- 2_appliances -->', $value_trans[$_POST['2_appliances']], $email_template);
	$email_template = str_replace('<!-- 2_bathrooms -->', $value_trans[$_POST['2_bathrooms']], $email_template);
	$email_template = str_replace('<!-- 2_kitchen -->', $value_trans[$_POST['2_kitchen']], $email_template);
	$email_template = str_replace('<!-- 2_food -->', $value_trans[$_POST['2_food']], $email_template);
	$email_template = str_replace('<!-- 2_management -->',$value_trans[$_POST['2_management']], $email_template);
	$email_template = str_replace('<!-- 2_security -->', $value_trans[$_POST['2_security']], $email_template);
	$email_template = str_replace('<!-- 2_checkin -->', $value_trans[$_POST['2_checkin']], $email_template);
	$email_template = str_replace('<!-- 2_activities -->', $value_trans[$_POST['2_activities']], $email_template);
	
	$email_template = str_replace('<!-- comments -->', $comments, $email_template);
	
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/PHPMailer/class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->AddReplyTo($email,"$fname $lname");
	$mail->SetFrom($email,"$fname $lname");
	//$mail->AddAddress('feedback@booktravel.travel');
	$mail->AddAddress('meloth14@gmail.com');
	$mail->Subject = "AA Travel - Review from $fname $lname";
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!";
	$mail->MsgHTML($email_template);
	$mail->Send();
	
	$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/feedback_thanks.tpl');
	$content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
	$GLOBALS['content'] = $content;
	
	//Prepare Statement - Check User
	$statement = "SELECT notification_value FROM nse_user_notices WHERE user_id=? && notification_code='feedback'";
	$sql_notice = $GLOBALS['dbCon']->prepare($statement);
	
	//Check if client has elected to recieve feedback updates
	$estab_user_id = '';
	$estab_user_email = '';
	$statement = "SELECT a.user_id, b.email
					FROM nse_user_establishments AS a
					JOIN nse_user AS b ON a.user_id=b.user_id
					WHERE a.establishment_code=?";
	$sql_users = $GLOBALS['dbCon']->prepare($statement);
	
	$sql_users->bind_param('s', $establishment_code);
	$sql_users->execute();
	$sql_users->store_result();
	$sql_users->bind_result($estab_user_id, $estab_user_email);
	while($sql_users->fetch()) {
		$notice_value = 0;
		$sql_notice->bind_param('i', $estab_user_id);
		$sql_notice->execute();
		$sql_notice->store_result();
		$sql_notice->bind_result($notice_value);
		$sql_notice->fetch();
		if ($notice_value == '1') {
		//Send email to Clients who have requested feedback notices
		$email_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/feedback_notice.tpl');
		
		//Repalce Tags
		$email_template = str_replace('<!-- establishment_name -->', $establishment_name, $email_template);
		$email_template = str_replace('<!-- town -->', $establishment_town, $email_template);
		$email_template = str_replace('<!-- email -->', $establishment_email, $email_template);
		$email_template = str_replace('<!-- telephone -->', $establishment_tel, $email_template);
		$email_template = str_replace('<!-- name -->', "$fname $lname", $email_template);
		$email_template = str_replace('<!-- traveler_email -->', $email, $email_template);
		$email_template = str_replace('<!-- visit_date -->', $date, $email_template);		
		$email_template = str_replace('<!-- 2_recommends_as -->', $value_trans[$_POST['2_recommends_as']], $email_template);
		$email_template = str_replace('<!-- 2_quality -->', $value_trans[$_POST['2_quality']], $email_template);
		$email_template = str_replace('<!-- 2_staff -->', $value_trans[$_POST['2_staff']], $email_template);
		$email_template = str_replace('<!-- 2_staff_dependability -->', $value_trans[$_POST['2_staff_dependability']], $email_template);
		$email_template = str_replace('<!-- 2_value -->', $value_trans[$_POST['2_value']], $email_template);
		$email_template = str_replace('<!-- 2_room_cleanliness -->', $value_trans[$_POST['2_room_cleanliness']], $email_template);
		$email_template = str_replace('<!-- 2_room_comfort -->', $value_trans[$_POST['2_room_comfort']], $email_template);
		$email_template = str_replace('<!-- 2_appliances -->', $value_trans[$_POST['2_appliances']], $email_template);
		$email_template = str_replace('<!-- 2_bathrooms -->', $value_trans[$_POST['2_bathrooms']], $email_template);
		$email_template = str_replace('<!-- 2_kitchen -->', $value_trans[$_POST['2_kitchen']], $email_template);
		$email_template = str_replace('<!-- 2_food -->', $value_trans[$_POST['2_food']], $email_template);
		$email_template = str_replace('<!-- 2_management -->',$value_trans[$_POST['2_management']], $email_template);
		$email_template = str_replace('<!-- 2_security -->', $value_trans[$_POST['2_security']], $email_template);
		$email_template = str_replace('<!-- 2_checkin -->', $value_trans[$_POST['2_checkin']], $email_template);
		$email_template = str_replace('<!-- 2_activities -->', $value_trans[$_POST['2_activities']], $email_template);		
		$email_template = str_replace('<!-- comments -->', $comments, $email_template);
		
		$mail = new PHPMailer(true);
		try {
			$mail->AddReplyTo('admin@booktravel.travel', 'AA Travel Guides');
		    //$mail->AddAddress($estab_user_email);
		    $mail->AddAddress('meloth14@gmail.com');
		    $mail->SetFrom('admin@booktravel.travel', 'AA Travel Guides');
		    $mail->Subject = "AA Travel Guides - Review from $fname $lname for $establishment_name";
		    $mail->MsgHTML($email_template);
		    //$mail->Send();
		    $mail->ClearAddresses();
		    
			//Log
	        $GLOBALS['log_tool']->write_entry("Requested changes approved - email sent", $_SESSION['dbweb_user_id']);
	    } catch (phpmailerException $e) {
	      //Log
	      $error = $e->errorMessage();
	      $GLOBALS['log_tool']->write_entry("Error! Requested changes approved email failed $error", $_SESSION['dbweb_user_id']);
	    } catch (Exception $e) {
	      //Log
	      $error = $e->errorMessage();
	      $GLOBALS['log_tool']->write_entry("Error! Requested changes approved email failed $error", $_SESSION['dbweb_user_id']);
	    }
			
		}
	}
	$sql_users->free_result();
	$sql_users->close();
	$sql_notice->close();
	
    return TRUE;
}

?>