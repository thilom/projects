<?php
/**
 * Redirect to an establishment website and logs the clickthru 
 */

//Vars
$establishment_code = $_GET['id'];
$website_url = '';
$ip_address = $_SERVER['REMOTE_ADDR'];

//Includes & Setup
include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';

//Get website url
$statement = "SELECT website_url FROM nse_establishment WHERE establishment_code=?";
$sql_url = $GLOBALS['dbCon']->prepare($statement);
$sql_url->bind_param('s', $establishment_code);
$sql_url->execute();
$sql_url->store_result();
$sql_url->bind_result($website_url);
$sql_url->fetch();
$sql_url->free_result();
$sql_url->close();

//Save to log
$statement = "INSERT INTO nse_establishment_clickthru_log (establishment_code, ip_address, clickthru_date) VALUES (?,?,NOW())";
$sql_log = $GLOBALS['dbCon']->prepare($statement);
$sql_log->bind_param('ss', $establishment_code, $ip_address);
$sql_log->execute();
$sql_log->close();


if (substr($website_url, 0, 4) != 'http') $website_url = "http://$website_url";

echo "<script>document.location='$website_url';</script>"

?>