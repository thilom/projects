<?php

//Vars
$session = isset($_GET['sd'])&&ctype_alnum($_GET['sd'])?$_GET['sd']:'';
$start = isset($_GET['st'])&&ctype_digit($_GET['st'])?$_GET['st']-1:'0';
$restype_list = isset($_POST['accomType'])?$_POST['accomType']:'';
$country_list = isset($_POST['country'])?$_POST['country']:'';
$province_list = isset($_POST['province'])?$_POST['province']:'';
$town_list = isset($_POST['town'])?$_POST['town']:'';
$suburb_list = isset($_POST['suburb'])?addslashes($_POST['suburb']):'';
$total_length = 0;
$list = '';
$accom_where = '';
$aaqa = array();
$sg = array();
$facilities = array();
$session_id = session_id();
$cache_timediff = '';
$establishment_name = '';
$town_name = '';
$province_name = '';
$country_name = '';
$suburb_name = '';
$borough_name = '';
$description = '';
$smilies = '';
$aa_category = '';
$star_grading = '';
$price_high = '';
$price_description = '';
$cache_timeout = 720000;
$time_diff = '';
$results = '';
$code = '';
$est = '';
$searchResult = array();

if (!empty($town_list) && substr($town_list, 0, 1) == 's') {
	list($suburb_list, $town_list) = explode('*', $town_list);
	$suburb_list = substr($suburb_list, 1);
}


//Get results
if (!isset($_POST['accomType'])) {
	$searchResult = get_from_cache();
} else {
	$searchResult = get_fresh_result();
}

$total_length = count($searchResult);
$searchResult = array_slice($searchResult, $start, 20);

//Prepare statements
$statement = "SELECT a.establishment_code AS eCode, a.establishment_name, c.town_name, d.province_name,
(select country_name FROM nse_location_country_lang where country_id=e.country_id && language_code='EN') AS country_name,
(select suburb_name FROM nse_location_suburb where suburb_id=b.suburb_id) AS suburb_name,
(select borough_name FROM nse_location_borough where borough_id=b.borough_id) AS borough_name,
(select establishment_description from nse_establishment_descriptions where establishment_code=eCode && language_code='EN' && description_type='short_description') AS short_description,
(SELECT smilies from nse_feedback_cache where establishment_code=eCode && category_code='all') AS smilies,
(SELECT aa_category_name FROM nse_aa_category WHERE aa_category_code=a.aa_category_code) AS aa_category,
(SELECT star_grading FROM nse_establishment_restype WHERE establishment_code=eCode) AS star_rating,
f.price_high, f.price_description
FROM nse_establishment  AS a
LEFT JOIN nse_establishment_location as b ON a.establishment_code=b.establishment_code
LEFT JOIN nse_location_town as c  ON c.town_id=b.town_id
LEFT JOIN nse_location_province AS d ON d.province_id=b.province_id
LEFT JOIN nse_location_country_lang AS e ON e.country_id=b.country_id
JOIN nse_establishment_deprecated AS f ON a.establishment_code=f.establishment_code
WHERE a.establishment_code=? LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);

$statement = "SELECT NOW()-cache_timestamp, establishment_name, town_name, province_name, country_name, suburb_name, borough_name, short_description, smilies, aa_category, star_grading, price_high, price_description
				FROM nse_cache_search_en WHERE establishment_code=?";
$sql_cache = $GLOBALS['dbCon']->prepare($statement);

$statement = "INSERT INTO nse_cache_search_en
				(establishment_code, establishment_name, town_name, province_name, country_name, suburb_name, borough_name, short_description, smilies, aa_category, star_grading, price_high, price_description)
				VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
$sql_cache_insert = $GLOBALS['dbCon']->prepare($statement);

$statement = "DELETE FROM nse_cache_search_en WHERE establishment_code=?";
$sql_cache_delete = $GLOBALS['dbCon']->prepare($statement);

//Assemble Data
if (!empty($searchResult)) {
	foreach ($searchResult as $k) {
		if (empty($k)) continue;
		$sql_cache->bind_param('s', $k);
		$sql_cache->execute();
		$sql_cache->store_result();
		$sql_cache->bind_result($cache_timediff, $establishment_name, $town_name, $province_name, $country_name, $suburb_name, $borough_name, $description, $smilies, $aa_category, $star_grading, $price_high, $price_description);
		$sql_cache->fetch();
		
		if (empty($cache_timediff) || $cache_timediff > $cache_timeout) {
			$sql_data->bind_param('s', $k);
			$sql_data->execute();
			$sql_data->store_result();
			$sql_data->bind_result($code, $establishment_name, $town_name, $province_name, $country_name, $suburb_name, $borough_name, $description, $smilies, $aa_category, $star_grading, $price_high, $price_description);
			$sql_data->fetch();
			
			$sql_cache_delete->bind_param('s', $k);
			$sql_cache_delete->execute();
			 
			$sql_cache_insert->bind_param('ssssssssssiss', $k, $establishment_name, $town_name, $province_name, $country_name, $suburb_name, $borough_name, $description, $smilies, $aa_category, $star_grading, $price_high, $price_description);
			$sql_cache_insert->execute();
		}
		
		//Assemble Location
		$location = '';
		if (!empty($town_name)) $location .= ", $town_name";
		if (!empty($province_name)) $location .= ", $province_name";
		if (!empty($country_name)) $location .= ", $country_name";
		$location = substr($location, 2);
		
		//Assemble Price Data
		if ($price_description == 'OR') $price_description = 'On Request';
		$pos = strpos($price_high, ':');
		if ($pos > 0) {
			$price_code = substr($price_high, $pos+1, 1);
			switch ($price_code) {
				case 'G':
					$price_html = ":<span title=\'Under R100\'>$price_code</span>";
					$price_high = str_replace(":$price_code", $price_html, $price_high);
					break;
				case 'F':
					$price_html = ":<span title=\'From R100 - R149\'>$price_code</span>";
					$price_high = str_replace(":$price_code", $price_html, $price_high);
					break;
				case 'E':
					$price_html = ":<span title=\'From R150 - R199\'>$price_code</span>";
					$price_high = str_replace(":$price_code", $price_html, $price_high);
					break;
				case 'D':
					$price_html = ":<span title=\'From R200 - R299\'>$price_code</span>";
					$price_high = str_replace(":$price_code", $price_html, $price_high);
					break;
				case 'C':
					$price_html = ":<span title=\'From R300 - R399\'>$price_code</span>";
					$price_high = str_replace(":$price_code", $price_html, $price_high);
					break;
				case 'B':
					$price_html = ":<span title=\'From R400 - R499\'>$price_code</span>";
					$price_high = str_replace(":$price_code", $price_html, $price_high);
					break;
				case 'A':
					$price_code = substr($price_high, $pos+1);
					if ($price_code == 'A') {
							$price_html = ":<span title=\'From R500 - R999\'>$price_code</span>";
							$price_high = str_replace(":$price_code", $price_html, $price_high);
					} else {
							$multiplier = substr($price_code, 1);
							$cost = $multiplier * 1000;
							$price_html = ":<span title=\'Over R$cost\'>$price_code</span>";
							$price_high = str_replace(":$price_code", $price_html, $price_high);
					}
					break;
			}
		}
		if (strpos($price_description, 'S/C') > 0 ) $price_description = str_replace('S/C', "<span title=\'Self-Catering\'>S/C</span>", $price_description);
		if (strpos($price_description, 'B&B') > 0 ) $price_description = str_replace('B&B', "<span title=\'Bed & Breakfast\'>B&B</span>", $price_description);
		if (strpos($price_description, 'DBB') > 0 ) $price_description = str_replace('DBB', "<span title=\'Dinner, Bed & Breakfast\'>DBB</span>", $price_description);
		if (strpos($price_description, 'FB') > 0 ) $price_description = str_replace('FB', "<span title=\'Full Board\'>FB</span>", $price_description);
		if (strpos($price_description, 'pp') == 0 ) $price_description = str_replace('pp', "<span title=\'per Person\'>pp</span>", $price_description);
		
		$link =  stripslashes($establishment_name);
		$link = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $link);
		$link = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $link );
		$link = htmlentities($link);
		$link = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $link);
		$link = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $link);
		$link = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $link);
		$link = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $link);
		$link = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $link);
		$link = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $link);
		$link = strtolower(str_replace(' ', '_', $link));
		$link = strtolower(trim($k)) . "_$link";
		
		$establishment_name = str_replace("'", "&acute;", $establishment_name);
		$location = str_replace("'", "&acute;", $location);
		$description = str_replace("'", "&acute;", $description);
		$description = str_replace(array("\r\n", "\r", "\n"), '', $description);
	 	$list .= "$k{}$establishment_name{}$location{}$description{}$aa_category{}$smilies{}$star_grading{}$link{}$price_high{}$price_description||";
	}
} else {
	$list = '';
}

$list = str_replace('"', " ", $list);

//Replace template tags
$content = str_replace('<!-- result_data -->', $list, $content);
$content = str_replace('<!-- total_length -->', $total_length, $content);
$content = str_replace('<!-- start -->', $start, $content);
$content = str_replace('<!-- search_hash -->', $session_id, $content);


function get_from_cache() {
	//Vars
	$searhResult = array();
	$results = '';
	$session_id = session_id();
	
	$statement = "SELECT search_result FROM nse_search_result_cache WHERE search_hash=? LIMIT 1";
	$sql_hash = $GLOBALS['dbCon']->prepare($statement);
	$sql_hash->bind_param('s', $session_id);
	$sql_hash->execute();
	$sql_hash->bind_result($results);
	$sql_hash->store_result();
	$sql_hash->fetch();
	$searchResult = explode(',', $results);
	$sql_hash->close();
	
	return $searchResult;
}

function get_fresh_result() {
	//Vars
	$restype_list = isset($_POST['accomType'])?$_POST['accomType']:'';
	$country_list = isset($_POST['country'])?$_POST['country']:'';
	$province_list = isset($_POST['province'])?$_POST['province']:'';
	$town_list = isset($_POST['town'])?$_POST['town']:'';
	$suburb_list = isset($_POST['suburb'])?addslashes($_POST['suburb']):'';
	$accom_where = '';
	$aaqa = array();
	$sg = array();
	$facilities = array();
	$session_id = session_id();
	$est = '';
	
	if (!empty($town_list) && substr($town_list, 0, 1) == 's') {
		list($suburb_list, $town_list) = explode('*', $town_list);
		$suburb_list = substr($suburb_list, 1);
	}
	
	
	$statement = "SELECT establishment_code FROM nse_search_brush ";
		
	$where = '';
	if ($restype_list != '0') $where .= empty($where)?" WHERE restype LIKE '%,$restype_list,%'":" && restype LIKE '%,$restype_list,%'";

	$aaqa = '';
	if (isset($_POST['aaqa_recommended']) || isset($_POST['aaqa_highrecomm']) || isset($_POST['aaqa_superior'])) {
			if (isset($_POST['aaqa_recommended'])) {
				$aaqa .= empty($aaqa)?"  ":" || ";
				$aaqa .= "aa_status LIKE '%,R,%' || aa_status LIKE '%,R/H,%'";
		}
		if (isset($_POST['aaqa_highrecomm'])) {
				$aaqa .= empty($aaqa)?"  ":" || ";
				$aaqa .= "aa_status LIKE '%,HR,%' || aa_status LIKE '%,R/H,%' || aa_status LIKE '%,H/S,%'";
		}
		if (isset($_POST['aaqa_superior'])) {
				$aaqa .= empty($aaqa)?"  ":" || ";
				$aaqa .= "aa_status LIKE '%,S,%' || aa_status LIKE '%,H/S,%'";
		}
		if (!empty($aaqa)) {
			$where .= empty($where)?" WHERE ($aaqa)":" && ($aaqa)";
		}
	} else {
		$aaqa .= empty($aaqa)?" ":" || ";
		$aaqa .= "aa_estab = 'Y'";
		$where .= empty($where)?" WHERE $aaqa":" && $aaqa";
	}
	
	$star_grading = '';
	for ($x=1; $x<6; $x++) {
		if (isset($_POST["sg_$x"])) $star_grading .= empty($star_grading)?" star_grading=$x":" || star_grading=$x";
	}
	if (!empty($star_grading)) $where .= " && ($star_grading)";
	
	foreach($_POST as $k=>$v) {
		if (substr($k, 0, 4) == 'fac_') {
			$icon = substr($k, 4);
			$where .= empty($where)?" WHERE icons LIKE '%$icon,%'":" && icons LIKE '%$icon,%'";
		}
	}
	$statement .= $where;
	$sql_search = $GLOBALS['dbCon']->prepare($statement);
	$sql_search->execute();
	$sql_search->bind_result($est);
	$sql_search->store_result();
	while($sql_search->fetch()) {
		$searchResult2[] = $est;
	}
	if (count($searchResult2) == 0) $searchResult2 = array();
	$sql_search->close();
	
	if ((empty($country_list) || $country_list == '0') && (empty($province_list) || $province_list == '0') && (empty($town_list) || $town_list == '0') && (empty($suburb_list) || $suburb_list == '0')) {
				
	} else {
		$searchResult3 = array();
		$statement = "SELECT establishment_code FROM nse_establishment_location ";
		$where = '';
		if (!empty($country_list) && $country_list != '0') $where .= empty($where)?" WHERE country_id=$country_list":" && country_id=$country_list";
		if (!empty($province_list) && $province_list != '0') $where .= empty($where)?" WHERE province_id=$province_list":" && province_id=$province_list";
		if (!empty($town_list) && $town_list != '0') $where .= empty($where)?" WHERE town_id=$town_list":" && town_id=$town_list";
		if (!empty($suburb_list) && $suburb_list != '0') $where .= empty($where)?" WHERE suburb_id=$suburb_list":" && suburb_id=$suburb_list";
		$statement .= $where;
		$sql_search = $GLOBALS['dbCon']->prepare($statement);
		$sql_search->execute();
		$sql_search->bind_result($est);
		$sql_search->store_result();
		while($sql_search->fetch()) {
			$searchResult3[] = $est;
		}
		$sql_search->close();
	}
	
	if (isset($searchResult2) && isset($searchResult3)) {
		$searchResult = array_intersect($searchResult2, $searchResult3);
	} elseif (!isset($searchResult2) && isset($searchResult3)) {
		$searchResult = $searchResult3;
	} elseif (isset($searchResult2) && !isset($searchResult3)) {
		$searchResult = $searchResult2;
	} else {
		$searchResult = array();
	}

	$searchResult = array_slice($searchResult, 0, 500);
	shuffle($searchResult);
	
	//Clear Cache result
	$statement = "DELETE FROM nse_search_result_cache WHERE search_hash=?";
	$sql_cache_result_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_cache_result_delete->bind_param('s', $session_id);
	$sql_cache_result_delete->execute();
	$sql_cache_result_delete->close();
	
	//Insert in cache
	$cache_result = implode(',', $searchResult);
	$statement = "INSERT INTO nse_search_result_cache (search_hash, search_result) VALUES (?,?)";
	$sql_cache_result_update = $GLOBALS['dbCon']->prepare($statement);
	$sql_cache_result_update->bind_param('ss', $session_id, $cache_result);
	$sql_cache_result_update->execute();
	$sql_cache_result_update->close();

	return $searchResult;
}

?>