<?php 
/**
 * Advanced search - Form generation
 * 
 * --Updated to remove warnings - Thilo(2009)
 * 
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category search
 */

//Initialize Variables
$accom_options = '';
$country_options = '';
$id = '';
$name = '';

//Get Template
if (isset($_POST['accomType']) || isset($_GET['sd'])) {
	$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/asearch_result.tpl');	
	include $_SERVER['DOCUMENT_ROOT'] . '/e/asearch_result.php';		
} else {
	$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/asearch_form.tpl')	;
	
	//Get country List
	$statement = "SELECT DISTINCT(c.country_id), c.country_name
								FROM nse_establishment AS a
									JOIN nse_establishment_location AS b 
										ON a.establishment_code=b.establishment_code
									JOIN nse_location_country_lang AS c 
										ON b.country_id=c.country_id
								WHERE c.language_code='EN' && a.aa_estab='Y'
								ORDER BY c.country_name";
	$sql_countries = $GLOBALS['dbCon']->prepare($statement);
	$sql_countries->execute();
	$sql_countries->bind_result($id, $name);
	$sql_countries->store_result();
	while($sql_countries->fetch()) {
		if ($id == 1) continue;
		$country_options .= "<option value='$id'>$name</option>";
	}
	$sql_countries->close();
	
	$content = str_replace('<!-- country_options -->', $country_options, $content);
	
}

	
?>