<?php

/**
 * Display Reviews
 * 
 * Retrieves and displays a list of reviews for an establishment. Establishment code should be available in $_GET['id'].
 * 
 * @author Thilo Muller (2009)
 * @package RVBus
 * @category feedback
 */

//Vars
$code = $_GET['id'];
$establishment_name = '';
$establishment_comment = '';
$smilies = '';
$nLine = '';
$comment = '';
$visitor_name = '';
$town_name = '';
$visit_date = '';
$name = '';
$matches = array();
$ratings = array(  -1=>"<img src='/images/shame.png'>",
                    1=>"<img src='/images/sad.png'>",
                    2=>"<img src='/images/hmm.png'><img src='/images/hmm.png'>",
                    3=>"<img src='/images/happy.png'><img src='/images/happy.png'><img src='/images/happy.png'>",
                    4=>"<img src='/images/happy.png'><img src='/images/happy.png'><img src='/images/happy.png'><img src='/images/happy.png'>",
                    5=>"<img src='/images/cool.png'><img src='/images/cool.png'><img src='/images/cool.png'><img src='/images/cool.png'><img src='/images/cool.png'>");

//Get Template
$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/reviews.tpl');
$content = str_replace(array("\r\n","\r","\n"), '$%^', $content);
preg_match_all('@<!-- line_start -->(.)*<!-- line_end -->@i', $content, $matches);
$lContent = $matches[0][0];                    
                    
//Get overall rating
$statement = "SELECT smilies FROM nse_feedback_cache WHERE establishment_code=? && category_code='total'";
$sql_total = $GLOBALS['dbCon']->prepare($statement);
$sql_total->bind_param('s', $code);
$sql_total->execute();
$sql_total->bind_result($smilies);
$sql_total->store_result();
$sql_total->fetch();
$sql_total->close();

//Get Name
$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
$sql_name = $GLOBALS['dbCon']->prepare($statement);
$sql_name->bind_param('s', $code);
$sql_name->execute();
$sql_name->bind_result($establishment_name);
$sql_name->store_result();
$sql_name->fetch();
$sql_name->close();

//Get review Data
$statement = "SELECT a.visitor_comment, b.firstname, b.surname, b.town, b.country, a.visit_date, a.establishment_comment
				FROM nse_establishment_feedback AS a 
				JOIN nse_visitor AS b ON a.visitor_id=b.visitor_id
				WHERE a.establishment_code=? && a.feedback_status='2' 
				ORDER BY a.feedback_id ASC";
$sql_comment = $GLOBALS['dbCon']->prepare($statement);
$sql_comment->bind_param('s', $code);
$sql_comment->execute();
$sql_comment->bind_result($comment, $visitor_firstname, $visitor_surname, $town_name, $country_name, $visit_date, $establishment_comment);
$sql_comment->store_result();
while ($sql_comment->fetch()) {
	if (empty($comment)) continue;
	$visitor_name = $visitor_firstname." ".$visitor_surname;
	$town_name = $town_name.", ".$country_name;
    $visitor_name = trim($visitor_name);
    $name = !empty($visitor_name)?"$visitor_name":"Anonymous";
    if (($town_name) != ', ') {
        if (substr($town_name, 0, 1) == ',') $town_name = substr($town_name, 1);
        $name .= " in $town_name";
    }
    
    if (!empty($establishment_comment)) $establishment_comment = "<b>Reply</b><br>$establishment_comment";
    $line = str_replace('<!-- name -->', $name, $lContent);
    $line = str_replace('<!-- date -->', $visit_date, $line);
    $line = str_replace('<!-- review -->', $comment, $line);
    $line = str_replace('<!-- comment -->', $establishment_comment, $line);
    $nLine .= $line;
}
$sql_comment->close();

//Replace Tags
$content = preg_replace('@<!-- line_start -->(.)*<!-- line_end -->@i', $nLine, $content);
$content = str_replace('<!-- average_rating -->', $ratings[$smilies], $content);
$content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
$content = str_replace('$%^', "\n", $content);

//Breadcrumb
$link = strtolower(str_replace(' ', '_', "$code $establishment_name"));
$link = str_replace(array("'",'(',')','"','=','+','[',']',',','/','\\'), '', $link);
$link = str_replace('&#212;', 'o', $link);
$breadcrumbs = "&lt;&lt;<a href='/accommodation/$link.html'>Return to $establishment_name page</a>";
?>