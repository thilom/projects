<?php
/**
 * Searches the DB and returns a list of establishments to a parent JS function.
 * 
 * @author Thilo Muller(2009)
 * @package RVBUS
 * @category search
 */

//Initialize Variables
$cache_timeout = 100;
$list = '';
$result_length = 10;
$start_position = isset($_GET['strt'])?$_GET['strt']:0;
$total_length = 0;
$search_string = isset($_GET['st'])?$_GET['st']:'';
$res = array();

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/e/search.class.php';

//Find exact Match (Establishment Name)
$str = strtolower($search_string);
$statement = "SELECT establishment_code FROM nse_establishment WHERE (lower(establishment_name)=? || establishment_code=?)  && aa_estab='Y' LIMIT 1";
$sql_full = $GLOBALS['dbCon']->prepare($statement);
$sql_full->bind_param('ss', $str, $str);
$sql_full->execute();
$sql_full->bind_result($estab_code);
$sql_full->store_result();
$sql_full->fetch();
$sql_full->close();

if (isset($estab_code) && !empty($estab_code)) {
	$res[$estab_code] = 1;
}

//Find Partial Match (Establishment Name);
$str = "%" . strtolower($search_string) . "%";
$statement = "SELECT establishment_code FROM nse_establishment WHERE lower(establishment_name) LIKE ? && aa_estab='Y'";
$sql_part = $dbCon->prepare($statement);
$sql_part->bind_param('s', $str);
$sql_part->execute();
$sql_part->bind_result($estab_code);
$sql_part->store_result();
while ($sql_part->fetch()) {
	$res[$estab_code] = 1;
}
$sql_part->close();

//Find matches in estamlishment details
$searchResult = find($search_string, $start_position, $result_length);

$statement = "SELECT a.establishment_code AS eCode, a.establishment_name, c.town_name, d.province_name,
(select country_name FROM nse_location_country_lang where country_id=e.country_id && language_code='EN') AS country_name,
(select suburb_name FROM nse_location_suburb where suburb_id=b.suburb_id) AS suburb_name,
(select borough_name FROM nse_location_borough where borough_id=b.borough_id) AS borough_name,
(select establishment_description from nse_establishment_descriptions where establishment_code=eCode && language_code='EN' && description_type='short_description' LIMIT 1) AS short_description,
(SELECT smilies from nse_feedback_cache where establishment_code=eCode && category_code='all') AS smilies,
(SELECT aa_category_name FROM nse_aa_category WHERE aa_category_code=a.aa_category_code) AS aa_category,
(SELECT star_grading FROM nse_establishment_restype WHERE establishment_code=eCode LIMIT 1) AS star_rating,
f.price_high, f.price_description
FROM nse_establishment  AS a
LEFT JOIN nse_establishment_location as b ON a.establishment_code=b.establishment_code
LEFT JOIN nse_location_town as c  ON c.town_id=b.town_id
LEFT JOIN nse_location_province AS d ON d.province_id=b.province_id
LEFT JOIN nse_location_country_lang AS e ON e.country_id=b.country_id
LEFT JOIN nse_establishment_deprecated AS f ON a.establishment_code=f.establishment_code
WHERE a.establishment_code=? LIMIT 1";
$sql_data = $GLOBALS['dbCon']->prepare($statement);


$statement = "SELECT NOW()-cache_timestamp, establishment_name, town_name, province_name, country_name, suburb_name, borough_name, short_description, smilies, aa_category,star_grading, price_high, price_description
							FROM nse_cache_search_en WHERE establishment_code=?";
$sql_cache = $GLOBALS['dbCon']->prepare($statement);


$statement = "INSERT INTO nse_cache_search_en
							(establishment_code, establishment_name, town_name, province_name, country_name, suburb_name, borough_name, short_description, smilies, aa_category,star_grading, price_high, price_description)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)";
$sql_cache_insert = $GLOBALS['dbCon']->prepare($statement);


$statement = "DELETE FROM nse_cache_search_en WHERE establishment_code=?";
$sql_cache_delete = $GLOBALS['dbCon']->prepare($statement);


foreach($res as $k=>$v) {
	if (isset($searchResult[$k])) unset($searchResult[$k]);
	$nResult[] = $k;
}

foreach ($searchResult as $k=>$v) {
	$nResult[] = $k;
}

$searchResult = array_flip($nResult);


foreach ($searchResult as $k=>$v) {
	//VARS
	$cache_timediff = '';
	$establishment_name = ''; 
	$town_name = '';
	$province_name = '';
	$country_name = '';
	$suburb_name = '';
	$borough_name = '';
	$description = '';
	$smilies = '';
	$aa_category = '';
	$star_grading = '';
	$price_high = '';
	$price_description = '';
	
	if (empty($k)) continue;
	$total_length++;
	$sql_cache->bind_param('s', $k);
	$sql_cache->execute();
	$sql_cache->store_result();
	$sql_cache->bind_result($cache_timediff, $establishment_name, $town_name, $province_name, $country_name, $suburb_name, $borough_name, $description, $smilies, $aa_category, $star_grading, $price_high, $price_description);
	$sql_cache->fetch();
	
	if (empty($cache_timediff) || $cache_timediff > $cache_timeout) {
		$sql_data->bind_param('s', $k);
		$sql_data->execute();
		$sql_data->store_result();
		$sql_data->bind_result($k, $establishment_name, $town_name, $province_name, $country_name, $suburb_name, $borough_name, $description, $smilies, $aa_category, $star_grading, $price_high, $price_description);
		$sql_data->fetch();
		
		$sql_cache_delete->bind_param('s', $k);
		$sql_cache_delete->execute();
		 
		$sql_cache_insert->bind_param('sssssssssssss', $k, $establishment_name, $town_name, $province_name, $country_name, $suburb_name, $borough_name, $description, $smilies, $aa_category, $star_grading, $price_high, $price_description);
		$sql_cache_insert->execute();
		
	}
	
	//Assemble Location
	$location = '';
	if (!empty($town_name)) $location .= ", $town_name";
	if (!empty($province_name)) $location .= ", $province_name";
	if (!empty($country_name)) $location .= ", $country_name";
	$location = substr($location, 2);
	
	//Assemble Price Data
	if ($price_description == 'OR') $price_description = 'On Request';
	$pos = strpos($price_high, ':');
	if ($pos > 0) {
		$price_code = substr($price_high, $pos+1, 1);
		switch ($price_code) {
			case 'G':
				$price_html = ":<span title=\"Under R100\">$price_code</span>";
				$price_high = str_replace(":$price_code", $price_html, $price_high);
				break;
			case 'F':
				$price_html = ":<span title=\"From R100 - R149\">$price_code</span>";
				$price_high = str_replace(":$price_code", $price_html, $price_high);
				break;
			case 'E':
				$price_html = ":<span title=\"From R150 - R199\">$price_code</span>";
				$price_high = str_replace(":$price_code", $price_html, $price_high);
				break;
			case 'D':
				$price_html = ":<span title=\"From R200 - R299\">$price_code</span>";
				$price_high = str_replace(":$price_code", $price_html, $price_high);
				break;
			case 'C':
				$price_html = ":<span title=\"From R300 - R399\">$price_code</span>";
				$price_high = str_replace(":$price_code", $price_html, $price_high);
				break;
			case 'B':
				$price_html = ":<span title=\"From R400 - R499\">$price_code</span>";
				$price_high = str_replace(":$price_code", $price_html, $price_high);
				break;
			case 'A':
				$price_code = substr($price_high, $pos+1);
				if ($price_code == 'A') {
						$price_html = ":<span title=\"From R500 - R999\">$price_code</span>";
						$price_high = str_replace(":$price_code", $price_html, $price_high);
				} else {
						$multiplier = substr($price_code, 1);
						$cost = $multiplier * 1000;
						$price_html = ":<span title=\"Over R$cost\">$price_code</span>";
						$price_high = str_replace(":$price_code", $price_html, $price_high);
				}
				break;
		}
	}
	if (strpos($price_description, 'S/C') > 0 ) $price_description = str_replace('S/C', "<span title=\"Self-Catering\">S/C</span>", $price_description);
	if (strpos($price_description, 'B&B') > 0 ) $price_description = str_replace('B&B', "<span title=\"Bed & Breakfast\">B&B</span>", $price_description);
	if (strpos($price_description, 'DBB') > 0 ) $price_description = str_replace('DBB', "<span title=\"Dinner, Bed & Breakfast\">DBB</span>", $price_description);
	if (strpos($price_description, 'FB') > 0 ) $price_description = str_replace('FB', "<span title=\"Full Board\">FB</span>", $price_description);
	if (strpos($price_description, 'pp') == 0 ) $price_description = str_replace('pp', "<span title=\"per Person\">pp</span>", $price_description);
	
	
	if ($total_length >= $start_position && $total_length < $start_position+$result_length) {
		$link =  stripslashes($establishment_name);
		$link = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;'), array('o','c','e','e','e','e','o','u','e','e'), $link);
		$link = str_replace ( array ("'", '(', ')', '"', '=', '+', '[', ']', ',', '/', '\\','&' ), '', $link );
		$link = htmlentities($link);
		$link = str_replace(array("&uuml;", "&Uuml;","&ugrave;","&uacute;","&ucirc;", "&Ugrave;","&Uacute;","&Ucirc;"), 'u', $link);
		$link = str_replace(array("&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;","&ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;"), 'o', $link);
		$link = str_replace(array("&igrave;","&iacute;","&icirc;","&iuml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;"), 'i', $link);
		$link = str_replace(array("&egrave;","&eacute;","&ecirc;","&euml;","&Egrave;","&Eacute;","&Ecirc;","&Euml;"), 'e', $link);
		$link = str_replace(array("&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;"), 'a', $link);
		$link = str_replace(array("&szlig;","&yuml;","&yacute;","&ntilde;","&aelig;","&Ntilde;","&AElig;"), array('ss','y','y','n','ae','n','ae'), $link);
		$link = strtolower(str_replace(' ', '_', $link));
//		$link = str_replace(array("'",'(',')','"','=','+','[',']',',','/','\\','&'), '', $link);
//		$link = str_replace(array('&#212;','&#199;','&#200;','&#201;','&#202;','&#203;','&#214;','&#220;','&#232;','&#233;','#212;','#199;','#200;','#201;','#202;','#203;','#214;','#220;','#232;','#233;'), array('o','c','e','e','e','e','o','u','e','e','o','c','e','e','e','e','o','u','e','e'), $link);
		$link = strtolower($k) . "_$link";
		$establishment_name = str_replace(array('"',"'","\r\n","\r","\n"), '', $establishment_name);
		$description = str_replace(array('"',"'","\r\n","\r","\n"), '', $description);
		$location = str_replace(array('"',"'"), '', $location);
	 	$list .= str_replace("'", "&acute;", "$k{}$establishment_name{}$location{}$description{}$aa_category{}$smilies{}$star_grading{}$link{}$price_high{}$price_description||");
	}
}

?>

<script>
parent.return_data('<?php echo $list ?>', <?php echo $start_position ?>, <?php echo $result_length ?>, <?php echo $total_length ?>, '<?php echo $search_string?>');
</script>