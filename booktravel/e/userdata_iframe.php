<?php 
//Includes & Setup
include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';

//Get Data
$statement = "SELECT firstname, surname,email,phone,cell,fax,country,town,title_id,contact_method,contact_secondary,age_group,occupation,gender,receive_updates,visitor_id FROM nse_visitor WHERE email=? && surname=?";
$sql_data = $dbCon->prepare($statement);
$sql_data->bind_param('ss', $_GET['e'], $_GET['s']);
$sql_data->execute();
$sql_data->bind_result($firstname, $surname,$email, $phone, $cell, $fax,$country,$town,$title_id,$contact_method,$contact_secondary,$age_group,$occupation,$gender,$receive_updates,$id);
$sql_data->store_result();
if ($sql_data->num_rows() == 0) {
	echo "<script>parent.return_data(false)</script>";
} else {
	$sql_data->fetch();
	$sql_data->close();
	echo "<script>parent.return_data(true,'$firstname','$surname','$email','$phone', '$cell', '$fax','$country','$town','$title_id','$contact_method','$contact_secondary','$age_group','$occupation','$gender','$receive_updates','$id')</script>";
}


?>