<?php
/**
 * Display Errors
 * 
 * Displays a predefined or general error. Error text is defined in '/content/errors/'.
 * The script will look for 'error_ID.html' as supplied by $_GET['id'] in this directoy. 
 * Failing that it will display a general error message (generalError.html).
 * 
 * @author Thilo Muller(2009)
 * @package RVBus
 * @category error
 */

/**
 * @var string Error ID
 */
$error_id = isset($_GET['id'])?$_GET['id']:'generalError';

if (is_file($_SERVER['DOCUMENT_ROOT'] . "/content/errors/$error_id.html")) {
    $content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/content/errors/$error_id.html");
} else {
    $content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/content/errors/generalError.html");
}

?>