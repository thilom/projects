<?php 
//Variable Init
$report = FALSE;
$sitemap_count = 2;
$sitemap_entries = 0;
$sitemap_entries_max = 1500;
$sitemap_content = '';

//Settings
set_time_limit(0);
error_reporting(E_ALL);

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';

//Initialize Variables
$maps = '';
$counter = 0;
$breadcrumbs = '';
$page_title = ''; 
$top_tiers = array();
$map_links=  array ('/visit/cape_map_1000.htm' => "Map of Cape Town and the Cape Peninsula",
										'/visit/western_Northern_cape_map.html' => "Map of the Western Cape and the Northern Cape",
										'/visit/namibia_map.html' => "Map of Namibia",
										'/visit/east_cape_kwa_zulu_map.html' => "Map of the Eastern Cape, Free State, Lesotho and Kwa-Zulu Natal",
										'/visit/limpopo_mpuma_swazi_map.html' => "Map of Gauteng, Mpumalanga, Swaziland, Kruger Park and Limpopo");

$top_tiers['restypes'] = array('guest_houses' => 'Guest Houses',
									'bed_and_breakfasts' => "Bed and Breakfasts",
									'hotels' => "Hotels",
									'game_reserves' => 'Game Reserves',
									'boutique_hotels' => 'Boutique Hotels',
									'backpacker_lodges' => 'Backpacker Lodges',
									'budget_hotels' => 'Budget Hotels',
									'self_catering' => 'Self Catering',
									'resorts' => 'Resorts',
									'holiday_cottages' => 'Holiday Cottages',
									'holiday_flats' => 'Holiday Flats',
									'houseboats' => 'Houseboats',
									'luxury_hotels' => 'Luxury Hotels',
									'township_accommodation' => 'Township Accommodation',
									'farm_holidays' => 'Farm Holidays',
									'lodges' => 'Lodges',
									'caravan_camping' => 'Caravan and Camping',
									'golf_resorts' => 'Golf Resorts',
									'game_lodges' => 'Game Lodges',
									'business_hotels' => 'Business Hotels');

$top_tiers['stars'] = array("five_star_hotels" => "5 Star Hotels",
													"four_star_hotels" => "4 Star Hotels",
													"three_star_hotels" => "3 Star Hotels",);

$top_tiers['activities'] = array('conference_venues' => 'Conference Venues',
															'health_spas' => 'Health Spas',
															'disabled_friendly' => 'Disabled Friendly Hotels',
															'bird_watching' => 'Bird Watching',
															'pet_friendly' => 'Pet Friendly Accommodation',
															'fishing' => 'Fishing',
															'whale_watching' => 'Whale Watching',
															'honeymoon' => 'Honeymoons',
															'4X4' => 'Four X Four Holidays',
															'hiking' => 'Hiking Trails',
															'child_friendly' => 'Child Friendly Accommodation',
															'gay_friendly' => 'Gay Friendly Guest Houses',
															'kosher' => 'Kosher',
															'halaal' => 'Halaal');

$top_tiers['other'] = array ('restaurants' => 'Restaurants',
													'museums' => 'Museums',
													'art_galleries' => 'Art Galleries',
													'wine_farms' => 'Wine Farms',
													'holiday_accommodation' => 'Holiday Accomodation',
													'places_to_stay' => 'Places to Stay');

$ids = array('hotels' => '16,28,35,22,23,25,26,36,37',
									'game_reserves' => '19,38,20',
									'boutique_hotels' => '42,26',
									'backpacker_lodges' => '42,26',
									'budget_hotels' => '36,37',
									'self_catering' => '36,37',
									'resorts' => '36,37',
									'holiday_cottages' => '32,46',
									'holiday_flats' => '34',
									'houseboats' => '21',
									'luxury_hotels' => '26,27,28',
									'township_accommodation' => '39',
									'farm_holidays' => '15',
									'lodges' => '47,38,26,19,48',
									'restaurants' => '124',
									'museums' => '116',
									'art_galleries' => '111',
									'wine_farms' => '167');
$ids['guest_houses'] = implode(',',get_subcategories('GH'));
$ids['bed_and_breakfasts'] = implode(',', get_subcategories('BB'));

$codes= array('guest_houses' => 'GH',
									'bed_and_breakfasts' => "BB",
									'hotels' => "HO",
									'game_reserves' => 'GR',
									'boutique_hotels' => 'BOU',
									'backpacker_lodges' => 'BACK',
									'budget_hotels' => 'SSHO ',
									'self_catering' => 'ASHO',
									'resorts' => 'RVA',
									'holiday_cottages' => 'HC',
									'holiday_flats' => 'SCSA',
									'houseboats' => 'HB',
									'luxury_hotels' => 'LH',
									'township_accommodation' => 'TRAD',
									'farm_holidays' => 'FARM ',
									'lodges' => 'LO',
									'caravan_camping' => 'CC',
									'golf_resorts' => 'GOLF',
									'game_lodges' => 'GLOD ',
									'business_hotels' => 'COMH',
									"five_star_hotels" => "STAR5",
									"four_star_hotels" => "STAR4",
									"three_star_hotels" => "STAR3",
									'conference_venues' => 'CONF',
									'health_spas' => 'HSPA ',
									'disabled_friendly' => 'DA',
									'bird_watching' => 'BIRD',
									'pet_friendly' => 'PET',
									'fishing' => 'FISH',
									'whale_watching' => 'WHALE ',
									'honeymoon' => 'HM',
									'4X4' => '4X4',
									'hiking' => 'HIKE ',
									'child_friendly' => 'CF',
									'gay_friendly' => 'GF',
									'kosher' => 'KOSHER',
									'halaal' => 'HALAAL',
									'restaurants' => 'REST',
									'museums' => 'MUSEUM',
									'art_galleries' => 'ARTGAL',
									'wine_farms' => 'WINEFARM');

//Assemble Map Links
foreach ($map_links as $k=>$v) {
	if ($counter == 3) {
		$maps .= " |<br>";
		$counter = 0;
	}
	$maps .= " | <a href='$k'>$v</a>";
	$counter++;
}

if ($_GET['p'] == 'establishments') {
	include 'export/establishment.php';	
} else {
	//Get Template
	$page_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/template1.tpl');
	$content_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/general_content.tpl');
	$content_template = str_replace(array("\r\n", "\r","\n"), '$%#', $content_template);
	preg_match('@<!-- block_start -->(.)*<!-- block_end -->@i', $content_template, $matches); 
	$block_template = $matches[0];
	$content_template = str_replace('<!-- maps -->', $maps, $content_template);
	$town_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/town.tpl'); 
	
	//Includes & Setup
	include_once 'aa_init/init.php';
	include_once 'export/restype_functions.php';
	
	//Update Tables
//	$statement = "UPDATE nse_establishment SET aa_estab=''";
//	$sql_aa_clean = $GLOBALS['dbCon']->prepare($statement);
//	$sql_aa_clean->execute();
//	$sql_aa_clean->close();
//	
//	$statement = "UPDATE nse_establishment SET aa_estab='Y' WHERE qagrad = 'Y' || advertiser='P' || advertiser='L' || advertiser='A' || advertiser='B' || advertiser='S'";
//	$sql_aa = $GLOBALS['dbCon']->prepare($statement);
//	$sql_aa->execute();
//	$sql_aa->close();
	
	//Prepare Statements
	$statement = "SELECT a.country_id, b.country_name 
										FROM nse_location_region_towns AS a
										JOIN nse_location_country_lang AS b ON a.country_id=b.country_id
										WHERE a.region_id=?";
	$sql_country = $dbCon->prepare($statement);
	
	$statement = "SELECT a.region_id, a.region_name
										FROM nse_location_region_lang AS a
										JOIN nse_location_region AS b ON a.region_id=b.region_id
										WHERE a.language_code='EN' && b.region_type='CB'";
	$sql_regions = $dbCon->prepare($statement);
	
	$statement = "SELECT region_name FROM nse_location_region_lang  WHERE region_id=? && language_code='EN'";
	$sql_region_name = $dbCon->prepare($statement);
	
	
	
	$statement_pp = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=? ";
	
	$statement_tt = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=?  ORDER BY town_name";
	
	$statement_suburbs = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=?  ORDER BY suburb_name";
	
	$statement_boroughs = "SELECT borough_id, borough_name FROM nse_location_borough WHERE town_id=?  ORDER BY borough_name";
	
	$statement_pc = "SELECT paragraph_title, paragraph_content FROM nse_location_province_content WHERE province_id=? && language_code='EN'";
	
	include $_GET['p'];
//	$check = 0;
	//include 'export/pages.php';   
	//include 'export/places_to_stay.php';   
	//if ($check == 0) include 'export/guest_houses.php';   
	//if ($check == 0) include 'export/bed_and_breakfasts.php'; 
	//if ($check == 0) include 'export/hotels.php'; 
	//if ($check == 0) include 'export/game_reserves.php';
	//if ($check == 0) include 'export/boutique_hotels.php';
	//if ($check == 0) include 'export/backpacker_lodges.php';
	//if ($check == 0) include 'export/budget_hotels.php';
	//if ($check == 0) include 'export/self_catering.php';
	//if ($check == 0) include 'export/resorts.php';
	//if ($check == 0) include 'export/holiday_cottages.php';
	//
	//if ($check == 0) include 'export/holiday_flats.php';
	//if ($check == 0) include 'export/houseboats.php';
	//if ($check == 0) include 'export/luxury_hotels.php';
	//if ($check == 0) include 'export/township_accommodation.php'; 
	//if ($check == 0) include 'export/farm_holidays.php';			
	//if ($check == 0) include 'export/lodges.php';
	//if ($check == 0) include 'export/caravan_camping.php';
	//if ($check == 0) include 'export/restaurants.php';
	//if ($check == 0) include 'export/museums.php';
	//if ($check == 0) include 'export/art_galleries.php';
	//
	//if ($check == 0) include 'export/wine_farms.php';  
	////if ($check == 0) include 'export/establishment.php';
	//if ($check == 0) include 'export/conference_venues.php';
	//if ($check == 0) include 'export/health_spas.php'; 
	//if ($check == 0) include 'export/disabled_friendly.php'; 
	//if ($check == 0) include 'export/golf_resorts.php';
	//if ($check == 0) include 'export/game_lodges.php';
	//if ($check == 0) include 'export/bird_watching.php';
	//if ($check == 0) include 'export/business_hotels.php';
	//if ($check == 0) include 'export/pet_friendly.php';    
	//
	//if ($check == 0) include 'export/fishing.php';
	//if ($check == 0) include 'export/whale_watching.php';
	//if ($check == 0) include 'export/honeymoon.php';
	//if ($check == 0) include 'export/4X4.php';
	//if ($check == 0) include 'export/hiking.php';
	//if ($check == 0) include 'export/child_friendly.php';
	//if ($check == 0) include 'export/gay_friendly.php';
	//if ($check == 0) include 'export/kosher.php'; 
	//if ($check == 0) include 'export/halaal.php';
	//if ($check == 0) include 'export/three_star_hotels.php'; 
	//
	//if ($check == 0) include 'export/four_star_hotels.php'; 
	//if ($check == 0) include 'export/five_star_hotels.php';
	//if ($check == 0) include 'export/holiday_accommodation.php';  
}

function create_dir($dir) {
		echo "<div style='font-weight: bold; color: navy'>$dir";
		if (!is_dir($_SERVER['DOCUMENT_ROOT'] . $dir)) {
			if (mkdir($_SERVER['DOCUMENT_ROOT'] . $dir, 0755)) {
				echo " <span style='font-style: arial; font-size: 8pt; color: navy'>- Directory Created</span>";
			} else {
				echo " <span style='font-style: arial; font-size: 8pt; color: red'>- Creation Failed</span>";
			}
		} else { 
			echo " <span style='font-style: arial; font-size: 8pt; color: navy'>- Directory Exists</span>";
		} 
		echo "</div>";
}

function create_file($file, $content) {
		echo "<div style='font-weight: normal; color: black'>$file";
		$note = is_file($_SERVER['DOCUMENT_ROOT'] . $file)?'File Updated':'File Created';
		if (file_put_contents($_SERVER['DOCUMENT_ROOT'] . "$file", $content)) {
			echo " <span style='font-style: arial; font-size: 8pt; color: navy'>- $note</span>";
		} else {
			echo " <span style='font-style: arial; font-size: 8pt; color: red'>- Update Failed</span>";
		}
		echo "</div>";
}


/**
 * Returns an array of subcategories 
 *
 * @param string $cat_id The parent category
 * @return mixed
 */
function get_subcategories($cat_id) {
	$statement = "SELECT b.subcategory_id 
								FROM nse_restype_category AS a
								JOIN nse_restype_subcategory_parent AS b ON a.category_id=b.category_id
								WHERE a.category_code=?";
	$sql_category = $GLOBALS['dbCon']->prepare($statement);
	$sql_category->bind_param('s', $cat_id);
	$sql_category->execute();
	$sql_category->bind_result($category_id);
	$sql_category->store_result();
	while ($sql_category->fetch()) {
		$category_list[] = $category_id;
	}
	$sql_category->close();
	return $category_list;
}

?>