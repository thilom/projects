<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key=<!-- api_key -->&sensor=false' type='text/javascript'></script>
<script type='text/javascript' src='/shared/cookie.js'></script>
<script type='text/javascript' src='/templates/google_map.js'></script>

<a href="http://booktravel.travel/index.php?p=map_regions" class="alinks" >Click here to search Accommodation by map</a>

<table width=100% >
	<tr>
		<td colspan=2>
			<table width=100% id=routes>
				<tr>
					<th class=divider>Route From</th>
					<th>Route To</th>
				</tr>
				<tr valign=top>
					<td class=divider>
						<table>
							<tr>
								<td >Street Number</td>
								<td><input type=text id=street_number style='width: 40' /></td>
							</tr>
							<tr>
								<td>Street Name</td>
								<td><input type=text id=street_name style='width: 200' /></td>
							</tr>
							<tr>
								<td>Suburb</td>
								<td><input type=text id=suburb style='width: 200' /></td>
							</tr>
							<tr>
								<td>Town/City</td>
								<td><input type=text id=town style='width: 200' /></td>
							</tr>							
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td>Street Number</td>
								<td><input type=text id=to_number style='width: 40' value='<!-- to_number -->' /></td>
							</tr>
							<tr>
								<td>Street Name</td>
								<td><input type=text id=to_name style='width: 200' value='<!-- to_name -->' /></td>
							</tr>
							<tr>
								<td>Suburb</td>
								<td><input type=text id=to_suburb style='width: 200' value='<!-- to_suburb -->' /></td>
							</tr>
							<tr>
								<td>Town/City</td>
								<td><input type=text id=to_town style='width: 200' value='<!-- to_town -->' /></td>
							</tr>
							<tr>
								<td></td>
								<td><input type=checkbox id=highway />Avoid Highways (if possible)</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div align=right><input type=button value='Calculate Route' onClick=get_directions() /></div>
			
		</td>
	</tr>
</table>
<table id=map_canvas1>
	<tr valign=top>
		<td><div id='map_canvas' style='width: 100%; height: 500px'></div></td>
		<td width=300px class=divider><div id=dir_header >Directions</div><div id='dir_canvas' style='width: 300px; font-size: 6pt'><br />Enter a from and to address above and click on 'Calculate Route'</div></td>
	</tr>
</table>
<script>initialize();get_from(); </script>