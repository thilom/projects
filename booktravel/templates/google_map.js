var dir;
var map;

    function initialize() {
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById('map_canvas'));
        map.setCenter(new GLatLng(-26.591346,23.491630), 5);
        map.setUIToDefault();
        setTimeout('map.checkResize();map.panTo(new GLatLng(-26.591346,23.491630));', 1000);
		dir = new GDirections(map,document.getElementById('dir_canvas'));		
      }
    }

    function get_directions() {
        from_address = '';
        if (document.getElementById('street_number').value != '') from_address += document.getElementById('street_number').value;
        if (document.getElementById('street_name').value != '') from_address += ' ' + document.getElementById('street_name').value;
        if (document.getElementById('suburb').value != '') from_address += ',' + document.getElementById('suburb').value;
        if (document.getElementById('town').value != '') from_address += ',' + document.getElementById('town').value + ', south africa';
        
        to_address = '';
        if (document.getElementById('to_number').value != '') to_address += document.getElementById('to_number').value;
        if (document.getElementById('to_name').value != '') to_address += ' ' + document.getElementById('to_name').value;
        if (document.getElementById('to_suburb').value != '') to_address += ',' + document.getElementById('to_suburb').value;
        if (document.getElementById('to_town').value != '') to_address += ',' + document.getElementById('to_town').value + ', south africa';
        
		//to_address = document.getElementById('to_address').value;
		document.getElementById('dir_canvas').innerHTML = '';
		
		if (document.getElementById('highway').checked == true) {
			dir.load('from: ' + from_address + ' to: ' + to_address,{avoidHighways:true});
		} else {
			dir.load('from: ' + from_address + ' to: ' + to_address);
		}
		
		map.checkResize();
		
		//set cookie
		cValue = document.getElementById('street_number').value + '|' + document.getElementById('street_name').value + '|' + document.getElementById('suburb').value + '|' +document.getElementById('town').value;
		set_cookie('map_from',cValue,1);
    }
    
    function get_from() {
    	cookie_val = get_cookie('map_from');
    	if (cookie_val) {
	    	from_val = cookie_val.split('|');
	    	
	    	document.getElementById('street_number').value = from_val[0];
	    	document.getElementById('street_name').value = from_val[1];
	    	document.getElementById('suburb').value = from_val[2];
	    	document.getElementById('town').value = from_val[3];
    	}
    }
    
    