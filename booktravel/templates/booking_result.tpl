<table>
	<tr>
		<td>
            Your enquiry has been forwarded to <b><!-- establishment_name --></b>.
             If you do not receive a reply we recommend you contact them directly by phone <!-- tel --> or fax <!-- fax -->.<p />
            <table >
                <tr valign=top>
                    <td>You may also be interested in:</td>
                    <td><ul>
                        <li><a href="http://www.essentialtravelinfo.com/book_flights.html"' target='_blank'>Book Flights</a></li>
                        <li><a href="http://online.avis.co.za/avisonline/za-gb/ibe.nsf/reservationhomemicrosite?openview&MST=1522F5403834066341257420002BFADA"' target='_blank'>Car Hire</a></li>
                        <li><a href="https://www.aigtravel.co.za/Default.aspx?username=CDAATravel"' target='_blank'>Get Travel Insurance</a></li>
                        <li><a href="http://www.essentialtravelinfo.com/route_calculator.html"' target='_blank'>Plan Your Route</a></li>
                    </ul></td>
                </tr>
            </table>
            
            
        </td>
	</tr>
	<tr>
		<td valign=center>
            &nbsp;
        </td>
	</tr>
	<tr>
		<td align=center>
        <input type=button value="Continue &gt;&gt;" onClick='document.location="/accommodation/<!-- establishment_link -->.html"'></td>
	</tr>
</table>