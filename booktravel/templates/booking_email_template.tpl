The potential guest whose name is below has made a reservation enquiry to you via the AA Travel Guides website,
Kindly respond directly to the potential guest(s).
<p />
Note that there is <b>NO COMMISSION</b> payable on this booking.
<p />
Kind regards,<br>
AA Travel Guides
<hr>

<table cellpadding=2 cellspacing=0 width=100%>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold;'>Name </td>
		<td  style='width: 80%'><!-- title --> <!-- fname --> <!-- lname --></td> 
	</tr>
	<tr style='background-color: #E9E9E9'>
		<td style='font-weight: bold;'>Email </td>
		<td><!-- email --></td>
	</tr>
	<tr>
		<td>&nbsp; </td>
		<td></td>
	</tr>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold'>Telephone Number </td>
		<td><!-- tel --></td>
	</tr>
	<tr style='background-color: #E9E9E9'>
		<td style='font-weight: bold; '>Fax Number </td>
		<td><!-- fax --></td>
	</tr>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold;'>Cell Number </td>
		<td><!-- cell --></td>
	</tr>
	<tr style='background-color: #E9E9E9'>
		<td style='font-weight: bold;'>Country </td>
		<td><!-- country --></td>
	</tr>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold; '>City/Town </td>
		<td><!-- town --></td>
	</tr>
	<tr style='background-color: #E9E9E9'>
		<td style='font-weight: bold; ' nowrap>Preferred Method of Contact </td>
		<td><!-- primaryContact --></td>
	</tr>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold;' nowrap>Alternative Method of Contact </td>
		<td><!-- secondaryContact --></td>
	</tr>
	<tr>
		<td>&nbsp; </td>
		<td></td>
	</tr>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold; '>Traveling Method </td>
		<td><!-- travelMethod --></td>
	</tr>
	<tr style='background-color: #E9E9E9'>
		<td style='font-weight: bold;'>No of Adults </td>
		<td><!-- adults --></td>
	</tr>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold;'>No of Children (under 12) </td>
		<td><!-- children --></td>
	</tr>
	<tr style='background-color: #E9E9E9'>
		<td style='font-weight: bold;'>No of Rooms </td>
		<td><!-- rooms --></td>
	</tr>
	<tr style='background-color: #F4F4F4'>
		<td style='font-weight: bold; '>Date of Arrival </td>
		<td><!-- arriveDate --></td>
	</tr>
	<tr style='background-color: #E9E9E9'>
		<td style='font-weight: bold;'>Date of Departure </td>
		<td><!-- departDate --></td>
	</tr>
	<tr>
		<td>&nbsp; </td>
		<td></td>
	</tr>
	<tr  style='background-color: #F4F4F4'>
		<td style='font-weight: bold; '>Additional Comments </td>
		<td><!-- requests --></td>
	</tr>
</table>