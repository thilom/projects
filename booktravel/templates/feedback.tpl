<script language="javascript" src="/scripts/CalendarPopup.js"></script>
<script>
var cal1 = new CalendarPopup("calDiv");
cal1.showNavigationDropdowns();
cal1.setCssPrefix("TEST");

var cal2 = new CalendarPopup("calDiv");
cal2.showNavigationDropdowns();
cal2.setCssPrefix("TEST");

function get_data() {
	e = document.getElementById('return_email').value;
	s = document.getElementById('return_surname').value;

	document.getElementById('return_data').src = "/userdata_iframe.php?e=" + e + "&s=" + s;
}

function return_data(a,firstname,surname,email,phone,cell,fax,country,town,title_id,primaryContact,secondaryContact,ageGroup,occupation,gender,receive_updates,id) {
	if (a) {
		document.getElementById('fname').value = firstname;
		document.getElementById('lname').value = surname;
		document.getElementById('email').value = email;
		document.getElementById('email2').value = email;
		document.getElementById('tel').value = phone;
		document.getElementById('cell').value = cell;
		document.getElementById('fax').value = fax;
		document.getElementById('country').value = country;
		document.getElementById('town').value = town;
		document.getElementById('title').value = title_id;
		document.getElementById('ageGroup').value = ageGroup;
		document.getElementById('occupation').value = occupation;
		document.getElementById('gender').value = gender;
		document.getElementById('new_visit').value = id;
		document.getElementById('updates').checked = receive_updates=='0'?false:true;
	} else {
		document.getElementById('fname').value = '';
		document.getElementById('lname').value = '';
		document.getElementById('email').value = '';
		document.getElementById('email2').value = '';
		document.getElementById('tel').value = '';
		document.getElementById('cell').value = '';
		document.getElementById('fax').value = '';
		document.getElementById('country').value = '';
		document.getElementById('town').value = '';
		document.getElementById('title').value = '';
		document.getElementById('ageGroup').value = '';
		document.getElementById('occupation').value = '';
		document.getElementById('gender').value = '';
		document.getElementById('new_visit').value = '1';
		document.getElementById('updates').checked = true;
		alert('Data not found');
	}
}
</script>
<h2>Review for <!-- establishment_name --></h2>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td class="text_grey_small">
    	Please enter your feedback for <!-- establishment_name --> below ONLY if you stayed <strong>overnight</strong> for at least 1 night <strong>
    	in the last 6 months</strong>.
    </td>
  </tr>
</table>
<br>

<form action="" method="post" name="reviewForm" id="reviewForm">
<table width="100%" align="center" cellpadding="2" cellspacing="0" id=feedbackOptions>
      <tr valign=top>
        <th width="*">&nbsp;</th>
        <th width="60" class="text_blue"><strong>Excellent</strong></th>
        <th width="60" class="text_blue"><strong>Very<br />Good </strong></th>
        <th width="60" class="text_blue"><strong>Good</strong></th>
        <th width="60" class="text_blue"><strong>Average</strong></th>
        <th width="60" class="text_blue"><strong>Poor</strong></th>
        <th width="60" class="text_blue"><strong>Pathetic</strong></th>
        <th width="60" class="text_blue"><strong>Not<br />Applicable</strong></th>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          How strongly would you recommend this establishment to other users? </td>
        <td align="center" valign="middle"><input name="2_recommends_as" type="radio" value="5" /></td>
        <td align="center"><input name="2_recommends_as" type="radio" value="4" /></td>
        <td align="center"><input name="2_recommends_as" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_recommends_as" type="radio" value="2" /></td>
        <td align="center"><input name="2_recommends_as" type="radio" value="1" /></td>
        <td align="center"><input name="2_recommends_as" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the quality of the accommodation and public areas: </td>
        <td align="center" ><input name="2_quality" type="radio" value="5" /></td>
        <td align="center" ><input name="2_quality" type="radio" value="4" /></td>
        <td align="center"><input name="2_quality" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_quality" type="radio" value="2" /></td>
        <td align="center"><input name="2_quality" type="radio" value="1" /></td>
        <td align="center"><input name="2_quality" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the knowledge and willingness of staff to provide service: </td>
        <td align="center"><input name="2_staff" type="radio" value="5" /></td>
        <td align="center"><input name="2_staff" type="radio" value="4" /></td>
        <td align="center"><input name="2_staff" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_staff" type="radio" value="2" /></td>
        <td align="center"><input name="2_staff" type="radio" value="1" /></td>
        <td align="center"><input name="2_staff" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the dependability of staff in providing service: </td>
        <td align="center"><input name="2_staff_dependability" type="radio" value="5" /></td>
        <td align="center"><input name="2_staff_dependability" type="radio" value="4" /></td>
        <td align="center"><input name="2_staff_dependability" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_staff_dependability" type="radio" value="2" /></td>
        <td align="center"><input name="2_staff_dependability" type="radio" value="1" /></td>
        <td align="center"><input name="2_staff_dependability" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate your sense of value for money: </td>
        <td align="center"><input name="2_value" type="radio" value="5" /></td>
        <td align="center"><input name="2_value" type="radio" value="4" /></td>
        <td align="center"><input name="2_value" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_value" type="radio" value="2" /></td>
        <td align="center"><input name="2_value" type="radio" value="1" /></td>
        <td align="center"><input name="2_value" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the cleanliness of every aspect of room/unit: </td>

        <td align="center"><input name="2_room_cleanliness" type="radio" value="5" /></td>
        <td align="center"><input name="2_room_cleanliness" type="radio" value="4" /></td>
        <td align="center"><input name="2_room_cleanliness" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_room_cleanliness" type="radio" value="2" /></td>
        <td align="center"><input name="2_room_cleanliness" type="radio" value="1" /></td>
        <td align="center"><input name="2_room_cleanliness" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the comfort of room/unit as a whole (space, layout etc): </td>

        <td align="center"><input name="2_room_comfort" type="radio" value="5" /></td>
        <td align="center"><input name="2_room_comfort" type="radio" value="4" /></td>
        <td align="center"><input name="2_room_comfort" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_room_comfort" type="radio" value="2" /></td>
        <td align="center"><input name="2_room_comfort" type="radio" value="1" /></td>
        <td align="center"><input name="2_room_comfort" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the working order and condition of appliances: </td>

        <td align="center"><input name="2_appliances" type="radio" value="5" /></td>
        <td align="center"><input name="2_appliances" type="radio" value="4" /></td>
        <td align="center"><input name="2_appliances" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_appliances" type="radio" value="2" /></td>
        <td align="center"><input name="2_appliances" type="radio" value="1" /></td>
        <td align="center"><input name="2_appliances" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the functionality of bathroom: </td>

        <td align="center"><input name="2_bathrooms" type="radio" value="5" /></td>
        <td align="center"><input name="2_bathrooms" type="radio" value="4" /></td>
        <td align="center"><input name="2_bathrooms" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_bathrooms" type="radio" value="2" /></td>
        <td align="center"><input name="2_bathrooms" type="radio" value="1" /></td>
        <td align="center"><input name="2_bathrooms" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the functionality of self-catering kitchen (if applicable): </td>

        <td align="center"><input name="2_kitchen" type="radio" value="5" /></td>
        <td align="center"><input name="2_kitchen" type="radio" value="4" /></td>
        <td align="center"><input name="2_kitchen" type="radio" value="3" /></td>
        <td align="center"><input name="2_kitchen" type="radio" value="2" /></td>
        <td align="center"><input name="2_kitchen" type="radio" value="1" /></td>
        <td align="center"><input name="2_kitchen" type="radio" value="-1" /></td>
        <td align="center"><input name="2_kitchen" type="radio" value="na" checked /></td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the quality of food prepared by the establishment <br />(if applicable): </td>

        <td align="center"><input name="2_food" type="radio" value="5" /></td>
        <td align="center"><input name="2_food" type="radio" value="4" /></td>
        <td align="center"><input name="2_food" type="radio" value="3" /></td>
        <td align="center"><input name="2_food" type="radio" value="2" /></td>
        <td align="center"><input name="2_food" type="radio" value="1" /></td>
        <td align="center"><input name="2_food" type="radio" value="-1" /></td>
        <td align="center"><input name="2_food" type="radio" value="na" checked /></td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the degree of friendliness, attitude and helpfulness of management and staff: </td>

        <td align="center"><input name="2_management" type="radio" value="5" /></td>
        <td align="center"><input name="2_management" type="radio" value="4" /></td>
        <td align="center"><input name="2_management" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_management" type="radio" value="2" /></td>
        <td align="center"><input name="2_management" type="radio" value="1" /></td>
        <td align="center"><input name="2_management" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate your feeling safety and security: </td>

        <td align="center"><input name="2_security" type="radio" value="5" /></td>
        <td align="center"><input name="2_security" type="radio" value="4" /></td>
        <td align="center"><input name="2_security" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_security" type="radio" value="2" /></td>
        <td align="center"><input name="2_security" type="radio" value="1" /></td>
        <td align="center"><input name="2_security" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the quality of welcome, service and information imparted at check-in: </td>

        <td align="center"><input name="2_checkin" type="radio" value="5" /></td>
        <td align="center"><input name="2_checkin" type="radio" value="4" /></td>
        <td align="center"><input name="2_checkin" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_checkin" type="radio" value="2" /></td>
        <td align="center"><input name="2_checkin" type="radio" value="1" /></td>
        <td align="center"><input name="2_checkin" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
       <tr onMouseOver='this.className="o"' onMouseOut='this.className=""'>
        <td align="left" class="text_grey_small">
          Rate the selection of activities available at the property and/or in the immediate area: </td>

        <td align="center"><input name="2_activities" type="radio" value="5" /></td>
        <td align="center"><input name="2_activities" type="radio" value="4" /></td>
        <td align="center"><input name="2_activities" type="radio" value="3" checked /></td>
        <td align="center"><input name="2_activities" type="radio" value="2" /></td>
        <td align="center"><input name="2_activities" type="radio" value="1" /></td>
        <td align="center"><input name="2_activities" type="radio" value="-1" /></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="right">Date of visit: </td>
        <td colspan="10"><input name="date" type="text" class="textfield" id="date" size="30" value="" ><img src='/images/calendar_icon.gif' border=0 onclick="cal2.select(document.getElementById('date'),'date2','dd MMM, yyyy'); return false;" id=date2 /></td>
      </tr>
      <tr>
        <td align="right" valign="top">Your comments: </td>
        <td colspan="10"><textarea name="comments" cols="50" rows="5" class="textfield" id="comments"></textarea><span class="error"> *</span></td>
      </tr>
</table>
<table width="100%"  border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>

        <th colspan="6" align="left" class="subheading">Your Details </th>
      </tr>
      <tr>
        <td align="right" class="text_grey_small">Title:</td>
        <td colspan="5" class="text_grey_small">
          <select name="title" id="title" class="textfield">
							<option value="1"></option>
							<option value="2">Mrs</option>

							<option value="3">Mr</option>
							<option value="4">Ms</option>
							<option value="5">miss</option>
							<option value="6">dr</option>
							<option value="7">Prof</option>
					  </select>

		</td>
		<td align=right rowspan=4>
			<table style='border-color: grey; border-width: 1 1 1 1; border-style: solid; background-color: silver; width: 200px' cellpadding=2 cellspacing=0>
				<tr>
					<td colspan=2 style='font-weight: bold; font-family: Arial; font-size: 14px'>Regular Visitors</td>
				</tr>
				<tr>
					<td style='font-family: Arial; font-size: 14px'>Email: </td>
					<td ><input type=text name='return_email' id='return_email' style='width: 120'></td>
				</tr>
				<tr>
					<td style='font-family: Arial; font-size: 14px'>Surname: </td>
					<td><input type=text name='return_surname' id='return_surname' style='width: 120'></td>
				</tr>
				<tr>
					<td colspan=2 align=center><input type=button name=fetchData value='Fetch my Details' style='font-family: Arial; font-size: 14px' onClick=get_data();></td>
				</tr>
			</table>
		</td>
      </tr>
      <tr>
        <td align="right" class="text_grey_small">First Name: </td>
        <td colspan="5" class="text_grey_small"><input name="fname" type="text" class="textfield" id="fname" /><span class="error"> *</span></td>
      </tr>
      <tr>

        <td align="right" class="text_grey_small">Surname:</td>
        <td colspan="5" class="text_grey_small"><input name="lname" type="text" class="textfield" id="lname" /><span class="error"> *</span></td>
      </tr>
      <tr>
        <td align="right" class="text_grey_small">Email Address: </td>
        <td colspan="5" class="text_grey_small"><input name="email" type="text" class="textfield" id="email" /><span class="error"> *</span></td>

      </tr>
	  <tr>
        <td align="right" class="text_grey_small">Confirm Email Address: </td>
        <td colspan="5" class="text_grey_small"><input name="email2" type="text" class="textfield" id="email2" /><span class="error"> *</span></td>
      </tr>
      <tr>
        <td align="right" class="text_grey_small">Country:</td>

        <td colspan="5" class="text_grey_small">
          <select name="country" id="country" class="textfield">
                            <option value="South Africa">South Africa</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="Germany">Germany</option>
                            <option value="United States Of America">United States Of America</option>
                            <option value="Argentina">Argentina</option>

                            <option value="Australia">Australia</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Brazil">Brazil</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Canada">Canada</option>
                            <option value="Canada">Canada</option>

                            <option value="Caribbean">Caribbean</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Czech Republic">Czech Republic</option>
                            <option value="Denmark">Denmark</option>

                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="Hong Kong">Hong Kong</option>
                            <option value="Hungary">Hungary</option>
                            <option value="India">India</option>
                            <option value="Ireland">Ireland</option>

                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Japan">Japan</option>
                            <option value="Korea">Korea</option>
                            <option value="Latin America">Latin America</option>
                            <option value="Luxemburg">Luxemburg</option>

                            <option value="Mexico">Mexico</option>
                            <option value="Middle East">Middle East</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="North Africa">North Africa</option>
                            <option value="Norway">Norway</option>

                            <option value="Peru">Peru</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Russia">Russia</option>
                            <option value="Slovakia">Slovakia</option>
                            <option value="Spain">Spain</option>

                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Taiwan">Taiwan</option>
                            <option value="Uruguay">Uruguay</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Other">Other</option>

                        </select> <span class="error"> *</span>
      </td>
      </tr>
	  <tr>
        <td align="right" class="text_grey_small">City/Town:</td>
        <td colspan="5" class="text_grey_small"><input name="town" id="town" class="textfield" type="text" /><span class="error"> *</span></td>

      </tr>
      <tr>
        <td align="right" class="text_grey_small">Telephone Number: </td>
        <td colspan="5" class="text_grey_small"><input name="tel" type="text" class="textfield" id="tel" /><span class="error"> *</span></td>
      </tr>
      <tr>
        <td align="right" class="text_grey_small">Fax Number: </td>

        <td colspan="5" class="text_grey_small"><input name="fax" type="text" class="textfield" id="fax" /></td>
      </tr>
	  <tr>
        <td align="right" class="text_grey_small">Cell Number: </td>
        <td colspan="5" class="text_grey_small"><input name="cell" type="text" class="textfield" id="cell" /></td>
      </tr>
	  <tr>
			<td valign="top" class="text_grey_small" align="right">Age group: </td>

			<td colspan="5">
				<select name="ageGroup" id="ageGroup" class="textfield">
					<option value="">Unspecified</option>
					<option value="Less than 21">Less than 21 years</option>
					<option value="21-30">21-30 years</option>
					<option value="31-40">31-40 years</option>
					<option value="41-50">41-50 years</option>

					<option value="51-60">51-60 years</option>
					<option value="61-70">61-70 years</option>
					<option value="Over 70">Over 70 years</option>
				</select>
			</td>
	  </tr>
		  <tr>

			<td valign="top" class="text_grey_small" align="right">Occupation: </td>
			<td colspan="5"><input name="occupation" type="text" class="textfield" id="occupation" /></td>
		  </tr>
		  <tr>
			<td valign="top" class="text_grey_small" align="right">Gender: </td>
			<td colspan="5">
				<select name="gender" id="gender" class="textfield">
					<option value="">Unspecified</option>

					<option value="M">Male</option>
					<option value="F">Female</option>
				</select>
			</td>
		  </tr>
	  <tr>
        <td align="right">&nbsp;</td>
        <td colspan="6" class="text_grey_small"><input name="updates" type="checkbox" id="updates" value="yes" checked="checked" />

        I travel regularly, and would like to  receive updates from the Essential Travel Info website via email </td>
      </tr>
	  <tr><td>&nbsp;</td><td colspan="5">&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;<input type=hidden name='new_visit' id='new_visit' value=1 ></td></tr>
	<tr valign=top>
	  <td  valign="top" class="text_grey_small" align=right>Security code: </td>
	  <td><img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" /> <input type=button value='Change Image' onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false"></td>
	</tr>
	<tr>
    	<td  class="text_grey_small">Verify Code: </td>
    	<td  class="text_grey_small"><input type="text" name="captcha_code" id="captcha_code" size="10" maxlength="6" /> (Copy security code from above image)</td>
    </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td colspan="5"><input type="submit" name="saveData" value="Submit Feedback" id="button" />
      </tr>
</table>
<div id='calDiv' STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"> </div>
</form>
<iframe id='return_data' style='display: none'> </iframe>