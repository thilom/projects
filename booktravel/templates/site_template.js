
var settingMode = 0;

$(document).ready(function() {
    updateSettingsDisplay();
});


function launchGallery(imgSrc, imgTitle, imgDescription) {
    $('#modalImage').attr('src', '');
    $('#modalTitle').html('');
    $('#modalDescription').html('');
    $('#modalImage').attr('src', imgSrc);
    $('#modalTitle').html(imgTitle);
    $('#modalDescription').html(imgDescription);
    $('#imageGallery5').modal();
}

function showNightsbridge() {
    $('#estTabs a[href="#availability').tab('show')
}
function hideNightsbridge() {
    document.getElementById('nightsbridge').style.display = 'none';
}

function openReviews() {
    $('#estTabs a[href="#reviews').tab('show');
}

function addReview(id) {
    document.location = "/guest_review/" + id;
}

function openResult() {
    document.getElementById('LocationSearchBox').className=''
}



function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function checkCookie(cname) {
    var cookie=getCookie(cname);
    if (cookie!="") {
        return true;
    } else {
        return false;
    }
}

function searchLocation() {

    if ($('#searchString').val() == '') return 0;

    var new_visitor = checkCookie('new_visitor');

    if (new_visitor === false) {
        settingMode = 1;
        editSearchSettings();
    } else {
        //url = '/search/' + encodeURI(document.getElementById('searchNameString2').value);
        url = '/ajax/fetch_locations.ajax.php';

        $.ajax({
            url: url,
            data: $('#searchForm').serialize(),
            method: 'post',
            complete: function(data) {

                returnResult = $.parseJSON(data.responseText)
                if (returnResult.count == 1) {
                    document.location = '/' + returnResult.last_url;
                } else {
                    $('#locationBody').html(returnResult.data);
                    $('#LocationSearchBox').slideDown();
                }

            }
        })
    }



}

function editSearchSettings() {
    $('#searchSettingsForm').modal();
}

function saveSettings() {

    //Vars
    error_count = 0;
    errors = [];
    errorText = '';

    //Check Arrival Date
    if ($('#arrivingDate').val() == '') {
        errors[error_count] = 'Please enter a valid arrival date';
        error_count++;
    }

    //Check Leaving Date
    if ($('#leavingDate').val() == '') {
        errors[error_count] = 'Please enter a valid leaving date';
        error_count++;
    }

    //Check Number of Children
    if (parseInt($('#childCount').val()) < 0) {
        errors[error_count] = 'Please enter a zero or more children';
        error_count++;
    }

    //Check Number of Adults
    if (parseInt($('#adultCount').val()) < 1) {
        errors[error_count] = 'Please enter a value greater than 0 for number of adults';
        error_count++;
    }

    //Show errors or save cookies
    if (error_count > 0) {
        for (i=0; i < errors.length; i++) {
            errorText += errors[i];
        }
        $('#settingErrors').html(errorText)
        $('#settingErrors').slideDown();
    } else {

        cookieText = 'false';
        setCookie('new_visitor', cookieText, 20);

        cookieText = $('#arrivingDate').val() + '|' + $('#leavingDate').val() + '|' + $('#adultCount').val() + '|' + $('#childCount').val()
        setCookie('searchSettings', cookieText, 20);
        updateSettingsDisplay();

        if (settingMode == 1) {
            searchLocation();
        }

        $('#searchSettingsForm').modal('hide');
    }



}

function updateSettingsDisplay() {

    var new_visitor = checkCookie('new_visitor');


    if (new_visitor === false) {
        settingsMessage = '';
    } else {
        if (checkCookie('searchSettings')) {
            data = getCookie('searchSettings').split('|');
            peopleCount = parseInt(data[2]) + parseInt(data[3]);
            settingsMessage = "Search Accommodation from " + data[0] + ' to ' + data[1] + " for " + peopleCount + (peopleCount == 1 ? ' person.' : ' people.') + '<a class="btn btn-sml" onclick="editSearchSettings()">[Edit]</a>';
        } else {
            settingsMessage = 'No settings <a class="btn btn-sml" onclick="editSearchSettings()">[Edit]</a>';
        }
    }

    $('#settingMessage').html(settingsMessage)
}