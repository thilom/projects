<table width="100%" border="2" cellpadding="2" cellspacing="0" bordercolor="#FFFFFF" bgcolor="#f2f2f2" id="table">
    <tbody>
      <tr>
        <td align="right" class="style1">Resort Name </td>
        <td width="469"><!-- name --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Province</td>
        <td><!-- province --></td>
      </tr>
      <tr>
        <td align="right" valign="top" class="style1">Postal Address </td>
        <td><!-- address --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Name of Owner </td>
        <td><!-- owner --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Name of Manager </td>
        <td><!-- manager --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Name of Front Office Manager </td>
        <td><!-- office_manager --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Contact Person </td>
        <td><!-- contact --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Telephone </td>
        <td><!-- tel --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Fax </td>
        <td><!-- fax --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Email Address </td>
        <td><!-- email --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Website Address </td>
        <td><!-- website --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Are you associated with any chain/marketing group?</td>
        <td><span class="style1"><!-- associated --></span></td>
      </tr>
      <tr>
        <td align="right" class="style1">If you are associated with a chain/marketing group - which one?</td>
        <td><!-- group --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Star Grading </td>
        <td><!-- star -->
        </td>
      </tr>
      <tr>
        <td align="right" class="style1">Number of units/rooms </td>
        <td><!-- rooms --></td>
      </tr>
      <tr>
        <td align="right" class="style1">What do you charge on average per room, per night for 2 people excluding meals?</td>
        <td><!-- price --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Type of accommodation offered</td>
        <td><!-- type -->
        </td>
      </tr>
      <tr>
        <td align="right" class="style1">Does every unit of accommodation have its own en suite bathroom? </td>
        <td><span class="style1"><!-- ensuite --></span></td>
      </tr>
      <tr>
        <td align="right" class="style1">Will you offer a guest paying full rate a discount of up to R50 if he/shepresents a discount coupon from an AA Travel Guide or Winners Guide?</td>
        <td><span class="style1"><!-- discount --></span></td>
      </tr>
      <tr>
        <td align="right" class="style1">First Category </td>
        <td><!-- first --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Second Category </td>
        <td><!-- second --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Third Category </td>
        <td><!-- third --></td>
      </tr>
      <tr>
        <td align="right" valign="top" class="style1">If there is no category for your establishment please give us your suggestions. </td>
        <td><!-- alternate --></td>
      </tr>
      <tr>
        <td align="right" class="style1">Is your target market </td>
        <td><!-- market -->
        </td>
      </tr>
      <tr>
        <td align="right" valign="top" class="style1">Is there any additional information you feel we need? </td>
        <td><!-- additional --></td>
      </tr>
      <tr>
        <td align="right"><input type="checkbox" class="style1" id="terms_box" onClick="checkChanged();"></td>
        <td class="style1">I have read and accept the terms and conditions. </td>
      </tr>
      <tr>
        <td align="right"><input type="checkbox" class="style1" id="bill_box" onClick="checkChanged();"></td>
        <td class="style1">I accept that I will be billed the QA fee in accordance with the prices above and the number of units in my establishment.</td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td><span class="grey_text">
          <input type="submit" name="Submit" value="Submit" id="submit_register" disabled="disabled">
        </span></td>
      </tr>
    </tbody>
</table>