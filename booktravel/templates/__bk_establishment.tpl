<link rel=stylesheet href=/templates/establishment.css>
<script src=/templates/establishment.js></script>
<table cellpadding=0 cellspacing=0 width=100%>
	<tr valign=top>
		<td>
			<!-- Col 1 -->
			<table cellpadding=0 cellspacing=0 width=624px style='background-color: white'>
				<tr>
					<td><img src='/images/details_tl.gif'></td>
					<td></td>
					<td align=right><img src='/images/details_tr.gif'></td>
				</tr>
				<tr valign=top>
					<td></td>
					<td>
						<h1 style='font-family: arial; font-weight: bold; color: #335da9; font-size: 16pt; margin-bottom: 4px'><!-- establishment_name --></h1>
						<h2 style='font-family: arial; font-weight: bold; color: #335da9; font-size: 10pt; margin-top: 0'><!-- location --><br><!-- restype --></br></h2>
						<img src='/res_images/<!-- primary_image -->' id=preImg>
						<!-- image_block_start --><table id=priBK cellspacing=10><tr><th style=background-image:url(/res_images/<!-- primary_image -->) id=priImg><img src=/loading.gif id=loader></th><td rowspan=2><div><!-- thumb_block --></div></td></tr><tr><th id=priTxt><!-- primary_text --></th></tr></table><center id=aftImg>Click on the thumbnails to view in large window</center><!-- image_block_end -->
						<br>
						<!-- description --><p>
                        <span class=section_heading >Guest Review </span> <br>
                        <div class=section_body><!-- rating --></div>
                        <div class=section_body><!-- comment --></div>
                        <div align=right>&nbsp;&nbsp;&nbsp;&nbsp;<!-- read_more --> <a href='/index.php?p=feedback&id=<!-- code -->' style='font-size: 10pt'>Review it!</a></div></span>
                        <p>
						<span class=section_heading >Pricing</span><br>
						<div class=section_body ><!-- pricing --></div>
						<p>
						<!-- specials_head_start --><span class=section_heading >Special Offers</span><!-- specials_head_end -->
						<div class=section_body><!-- special_offers --></div>
						<p>
						<!-- facilities_head_start --><span class=section_heading >Facilities & Activities</span><!-- facilities_head_end -->
						<div class=section_body><!-- facilities --></div>
						<p>
						<!-- directions -->
						<p id=disclaimer>Please note prices supplied are published in good faith but are subject to change.
						<img src='/e/log_establishment_view.php?code=<!-- code -->' width=1 height=1 />
						</p>
						
					</td>
					<td></td>
				</tr>
				<tr valign=bottom>
					<td><img src='/images/details_bl.gif'></td>
					<td></td>
					<td align=right><img src='/images/details_br.gif'></td>
				</tr>
			</table>
		</td>
		<td>&nbsp;
			
		</td>
		<td>
			<!-- Col 2 -->
			<table cellpadding=0 cellspacing=0 width=10>
				<tr valign=top>
					<td rowspan=2><img src='/images/spaceF.gif' width=1 height=268></td>
					<td style='background-image: url(/images/details_booking_top.gif); background-color: #E4E5E6; background-repeat: no-repeat' >
						<div style='font-weight: bold; font-size: 16pt; font-family: arial; text-align: center; padding-top: 10px; '>Venue Details</div><br>
						<!-- contact_block -->
						<div style='text-align: center; font-family: arial; font-size: 10pt; padding: 0 0 0 0'>
							<!-- booking_start --><a href='<!-- booking_link -->'><img src='/images/button_book_now.gif' border=0 width=186 height=32></a><br><!-- booking_end -->	<a href='/index.php?p=enquiry&c=<!-- code -->'><img src='/images/button_booking_enquiry.gif' border=0></a></div>
					</td>
				</tr>
				<tr height=16>
					<td><img src='/images/datails_booking_bottom.gif' width=201 height=16></td>
				</tr>
			</table><p>
			<table cellpadding=0 cellspacing=0 width=10>
				<tr valign=top>
					<td rowspan=2><img src='/images/spaceF.gif' width=1 height=268></td>
					<td style='background-image: url(/images/aa_endorsement_bk.gif); background-color: #ecc811; background-repeat: no-repeat' >
						<div style='font-weight: bold; font-size: 16pt; font-family: arial; text-align: center; padding-top: 10px'>Endorsements</div><br>
						<div style='font-family: arial; font-size: 10pt; padding: 10px'>
							<!-- qa_rating -->
						</div>
						<div style='text-align: center'><!-- star_rating --></div>
					</td>
				</tr>
				<tr height=16>
					<td><img src='/images/aa_endorsement_bt.gif'></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<img src='/e/log_establishment_views.php?code=<!-- code -->' width=1 height=1 />

