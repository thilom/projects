<script src='/shared/popup/popup.js'></script>
<script>
function getData(c, v) {
	if (c == 'T') {
		if (v == '0') {
			document.getElementById('town').options.length = 0;
			document.getElementById('town').options[0] = new Option('-= No Towns Available =-','-1');
			document.getElementById('town').disabled = true;
			document.getElementById('suburb').options.length = 0;
			document.getElementById('suburb').options[0] = new Option('-= No Suburbs Available =-','-1');
			document.getElementById('suburb').disabled = true;
		} else {
			document.getElementById('countryData').src = "js_locations.php?p=townList&pr=" + v;
		}
	}
	if (c == 'S') {
		if (v == '0') {
			document.getElementById('suburb').options.length = 0;
			document.getElementById('suburb').options[0] = new Option('-= No Suburbs Available =-','-1');
			document.getElementById('suburb').disabled = true;
		} else { 
			document.getElementById('countryData').src = "js_locations.php?p=suburbList&tn=" + v;
		}
	}
	if (c == 'C') {
		if (v == '0') {
			document.getElementById('province').options.length = 0;
			document.getElementById('province').options[0] = new Option('-= No Provinces Available =-','-1');
			document.getElementById('province').disabled = true;
			document.getElementById('town').options.length = 0;
			document.getElementById('town').options[0] = new Option('-= No Towns Available =-','-1');
			document.getElementById('town').disabled = true;
			document.getElementById('suburb').options.length = 0;
			document.getElementById('suburb').options[0] = new Option('-= No Suburbs Available =-','-1');
			document.getElementById('suburb').disabled = true;
		} else {
			document.getElementById('countryData').src = "js_locations.php?p=provinceList&pr=" + v;
		}
	}
}

function returnProvinces(data) {
	document.getElementById('province').disabled = false;
	document.getElementById('province').options.length = 0;
	document.getElementById('province').options[0] = new Option('Any Province','0');
	
	if (data.length == 0) {
		document.getElementById('province').options.length = 0;
		document.getElementById('province').options[0] = new Option('-= No Provinces Available =-','-1');
		document.getElementById('province').disabled = true;
	} else {
		d = data.split('|');
		for (x=0; x<d.length; x++) {
			ds = d[x].split('%');
			document.getElementById('province').options[x+1] = new Option(ds[1],ds[0]);
		}
	}
}

function returnTowns(data) {
	document.getElementById('town').disabled = false;
	document.getElementById('town').options.length = 0;
	document.getElementById('town').options[0] = new Option('Any Town','0');
	
	if (data.length == 0) {
		document.getElementById('town').options.length = 0;
		document.getElementById('town').options[0] = new Option('-= No Towns Available =-','-1');
		document.getElementById('town').disabled = true;
	} else {
		d = data.split('|');
		for (x=0; x<d.length; x++) {
			ds = d[x].split('%');
			document.getElementById('town').options[x+1] = new Option(ds[1],ds[0]);
		}
	}
}

function returnSuburbs(data) {
	document.getElementById('suburb').disabled = false;
	document.getElementById('suburb').options.length = 0;
	document.getElementById('suburb').options[0] = new Option('Any Suburb','0');

	if (data.length == 0) {
		document.getElementById('suburb').options.length = 0;
		document.getElementById('suburb').options[0] = new Option('-= No Suburbs Available =-','-1');
		document.getElementById('suburb').disabled = true;
	} else {
		d = data.split('|');
		for (x=0; x<d.length; x++) {
			ds = d[x].split('%');
			document.getElementById('suburb').options[x+1] = new Option(ds[1],ds[0]);
		}
	}
}

function change_aaqa(f) {
	a = document.getElementById('aaqa_any');
	b = document.getElementById('aaqa_recommended');
	c = document.getElementById('aaqa_highrecomm');
	d = document.getElementById('aaqa_superior');

	if (f == 'any' && a.checked == true) {
		b.checked = false;
		c.checked = false;
		d.checked = false;
	}

	if (f != 'any') {
		if (!b.checked && !c.checked && !d.checked) {
			a.checked = true;
		} else {
			a.checked = false;
		}
	}
}

function change_sg(g) {
	a = document.getElementById('sg_any');
	b = document.getElementById('sg_1');
	c = document.getElementById('sg_2');
	d = document.getElementById('sg_3');
	e = document.getElementById('sg_4');
	f = document.getElementById('sg_5');

	if (g == 'any' && a.checked) {
		b.checked = false;
		c.checked = false;
		d.checked = false;
		e.checked = false;
		f.checked = false;
	}

	if (g != 'any') {
		if (!b.checked && !c.checked && !d.checked && !e.checked && !f.checked) {
			a.checked = true;
		} else {
			a.checked = false;
		}
	}
}

function go() {
	document.getElementById('sForm').style.display='none';
	document.getElementById('search').style.display='block';
}

</script>
<div style='margin-right: auto; margin-left: auto; width: 100px; height: 400px; display: none' id=search><img src='/images/searching.gif'></div> 
<div id=sForm>
<form action='' method=post onSubmit='go()' style='font-family: arial'>
<table width=100%>
	<tr>
		<td colspan=3>
			<!-- keword/Location -->
			<table  width=100%>
				<!--  <TR>
					<td>Keyword </td>
					<td><input type=text name=keyword id=keyword size=40></td>
				</TR> -->
				<TR>
					<td nowrap>Accomodation Type </td>
					<td>
								<select  name='accomType' style='width: 200'>
									<option value='0'>Any Accomodation Type</option>
									<option value="BB">Bed and Breakfast</option>
                                    <option value="GH">Guest House</option>
									<option value="H">Hotel</option>
                                    <option value="L">Lodge Accommodation</option>
                                    <option value="SC">Self Catering</option>
                                    <option value="OT">Other</option>
                                    <option value="C">Country House</option>
                                    <option value="TA">Train Accommodation</option>
								</select>
					</td>
					<td></td>
				</TR>
				<!-- <TR>
					<td>Awarded </td>
					<td><select name=accomAwarded><option value=0>No</option><option value=1>Yes</option></select></td>
				</TR> -->
				<TR>
					<td colspan=3>&nbsp;</td>
				</TR>
				<TR>
					<td>Country </td>
					<td>
						<select name=country id=country onChange="getData('C', this.value)" style='width: 200'>
							<option value=1>South Africa</option>
							<!-- country_options -->
						</select>
					</td>
					<td rowspan=4>
						<div class=adS>
							<!--JavaScript Tag // Tag for network 567: 365 Digital / Primedia Online // Website: AA Travel Guide // Page: Advanced Search // Placement: Advanced Search-bottom-468 x 60 (2116376) // created at: Jun 17, 2009 11:13:22 AM-->
							<script language="javascript">
							<!--
							if (window.adgroupid == undefined) {
								window.adgroupid = Math.round(Math.random() * 1000);
							}
							document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn|3.0|567|2116376|0|113|ADTECH;loc=100;target=_blank;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
							//-->
							</script><noscript><a href="http://adserver.adtech.de/adlink|3.0|567|2116376|0|113|ADTECH;loc=300" target="_blank"><img src="http://adserver.adtech.de/adserv|3.0|567|2116376|0|113|ADTECH;loc=300" border="0" width="468" height="120"></a></noscript>
							<!-- End of JavaScript Tag -->
						</div>
					</td>
				</TR>
				<TR>
					<td>Province </td>
					<td>
						<select name=province id=province onChange="getData('T', this.value)" style='width: 200'>
							<option value=0 selected>Any Province</option>
							<option value=4>Eastern Cape</option>							
							<option value=1>Free State</option>
							<option value=9>Gauteng</option>
							<option value=6>Kwazulu-Natal</option>
							<option value=8>Limpopo</option>							
							<option value=2>Mpumalanga</option>
							<option value=5>North West</option>
							<option value=3>Northern Cape</option>
							<option value=7>Western Cape</option>
						</select>
					</td>
				</TR>
				<TR>
					<td>Town/City </td>
					<td><select name=town  id=town onChange="getData('S', this.value)" style='width: 200'><option value=0>Any Town/City</option></select></td>
				</TR>
				<TR>
					<td>Suburb </td>
					<td><select name=suburb id=suburb style='width: 200'><option value=0>Any Suburb</option></select></td>
				</TR>
				<TR>
					<td> </td>
					<td align=right width=100% colspan=2><input type=submit value='Search &#9658;'></td>
				</TR>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=10>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>
			<!--  Awards -->
			<table>
				<tr>
					<td>
						<b>AA Quality Assured Status</b>
						<table cellpadding=4 cellspacing=0>
							<tr valign=top>
								<td style='background-color: silver'><input type=checkbox name=aaqa_any id=aaqa_any checked onClick="change_aaqa('any')"></td>
								<td align=center style='background-color: silver; font-family: arial; font-weight: bold; font-size: 10pt'><img src='/lookup_images/aatravel_default.gif'  width=85 height=129><br>Any</td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td style='background-color: silver'><input type=checkbox name=aaqa_recommended id=aaqa_recommended onClick="change_aaqa()"></td>
								<td align=center style='background-color: silver; font-family: arial; font-weight: bold; font-size: 10pt'><img src='/lookup_images/aatravel_recommended.gif'  width=85 height=129><br>Recommended</td>
							</tr>
							<tr>
								<td colspan=10>&nbsp;</td>
							</tr>
							<tr valign=top>
								<td style='background-color: silver'><input type=checkbox name=aaqa_highrecomm id=aaqa_highrecomm onClick="change_aaqa()"></td>
								<td align=center style='background-color: silver; font-family: arial; font-weight: bold; font-size: 10pt'><img src='/lookup_images/aatravel_highrecomm.gif'  width=85 height=129><br>Highly<br>Recommended</td>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td style='background-color: silver'><input type=checkbox name=aaqa_superior id=aaqa_superior onClick="change_aaqa()"></td>
								<td align=center style='background-color: silver; font-family: arial; font-weight: bold; font-size: 10pt'><img src='/lookup_images/aatravel_superior.gif' width=85 height=129><br>Superior</td>
						</table>
					</td>
				</tr>
			</table>
			<p>
			<!-- Star Grading -->
			<b> Star Grading </b>
			<table>
				<tr> 
					<td><input type=checkbox name=sg_any id=sg_any checked onClick="change_sg('any')"> Any Grade</td>
				</tr>
				<tr>
					<td><input type=checkbox name=sg_1 id=sg_1 onClick="change_sg()"> <img src='/images/star.png'> (1 Star)</td>
				</tr>
				<tr>
					<td><input type=checkbox name=sg_2 id=sg_2 onClick="change_sg()">  <img src='/images/star.png'><img src='/images/star.png'> (2 Star)</td>
				</tr>
				<tr>
					<td><input type=checkbox name=sg_3 id=sg_3 onClick="change_sg()"> <img src='/images/star.png'><img src='/images/star.png'><img src='/images/star.png'> (3 Star)</td>
				</tr>
				<tr>
					<td><input type=checkbox name=sg_4 id=sg_4 onClick="change_sg()"> <img src='/images/star.png'><img src='/images/star.png'><img src='/images/star.png'><img src='/images/star.png'> (4 Star)</td>
				</tr>
				<tr>
					<td><input type=checkbox name=sg_5 id=sg_5 onClick="change_sg()"> <img src='/images/star.png'><img src='/images/star.png'><img src='/images/star.png'><img src='/images/star.png'><img src='/images/star.png'> (5 Star)</td>
				</tr>
			</table>
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>
			<!-- Facilities -->
			<b>On-site Activities &amp; Facilities</b>
			<table id=oa>
				<tr>
					<td><input type=checkbox name=fac_FORBYFOR></td>
					<td><img src='/icons1/FORBYFOR.gif' align=middle> 4x4</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_BIRDWATCH></td>
					<td><img src='/icons1/BIRDWATCH.gif' align=middle> Bird Watching</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_GAMEVIEW></td>
					<td><img src='/icons1/GAMEVIEW.gif' align=middle> Game Viewing</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_WHALE></td>
					<td><img src='/icons1/WHALE.gif' align=middle> Whale Watching</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_HIKING></td>
					<td><img src='/icons1/HIKING.gif' align=middle> Hiking</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_GOLF></td>
					<td><img src='/icons1/GOLF.gif' align=middle> Golf</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_WATERSPORT></td>
					<td><img src='/icons1/WATERSPORT.gif' align=middle> Water Sports</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_FISHING></td>
					<td><img src='/icons1/FISHING.gif' align=middle> Fishing</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_CASINO></td>
					<td><img src='/icons1/CASINO.gif' align=middle> Casino</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_spa></td>
					<td><img src='/icons1/SPA.gif' align=middle> Spa</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_CONFERENCE></td>
					<td><img src='/icons1/CONFERENCE.gif' align=middle> Conference Facilities</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_NONSMOKE></td>
					<td><img src='/icons1/NONSMOKE.gif' align=middle> Non-Smoking Rooms</td>
				</tr>
			</table>
			<p>
			<b>Services &amp; Extras</b>
			<table id=se>
				<tr>
					<td><input type=checkbox name=fac_BABYSIT ></td>
					<td><img src='/icons1/BABYSIT.gif' align=middle > Baby Sitting Service</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_HALAL ></td>
					<td><img src='/icons1/HALAL.gif' align=middle > Halaal</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_KOSHER></td>
					<td><img src='/icons1/KOSHER.gif' align=middle> Kosher</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_AMEX></td>
					<td><img src='/icons1/AMEX.gif' align=middle> Amex Accepted</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_CREDCARDS></td>
					<td><img src='/icons1/CREDCARDS.gif' align=middle> Credit Cards Accepted</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_DISABLED></td>
					<td><img src='/icons1/DISABLED.gif' align=middle> Mobility Impaired / Disabled</td>
				</tr>
				<tr>
					<td><input type=checkbox name=fac_PETSALLOWD></td>
					<td><img src='/icons1/PETSALLOWD.gif' align=middle> Pets Allowed</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=10>&nbsp;</td>
	</tr>
	
	<tr>
		<td align=right colspan=10><input type=submit value='Search &#9658;'></td>
	</tr>
</table>
</form>
</div>
<iframe id=countryData style='display: none'> </iframe>
