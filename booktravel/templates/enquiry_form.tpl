<script language="javascript" src="/scripts/CalendarPopup.js"></script>
<script>
var cal1 = new CalendarPopup("calDiv");
cal1.showNavigationDropdowns();
cal1.setCssPrefix("TEST");

var cal2 = new CalendarPopup("calDiv");
cal2.showNavigationDropdowns();
cal2.setCssPrefix("TEST");

function get_data() {
	e = document.getElementById('return_email').value;
	s = document.getElementById('return_surname').value;

	document.getElementById('return_data').src = "/e/userdata_iframe.php?e=" + e + "&s=" + s;
}

function return_data(a,firstname,surname,email,phone,cell,fax,country,town,title_id,primaryContact,secondaryContact,ageGroup,occupation,gender,receive_updates,id) {
	if (a) {
		document.getElementById('fname').value = firstname;
		document.getElementById('lname').value = surname;
		document.getElementById('email').value = email;
		document.getElementById('email2').value = email;
		document.getElementById('tel').value = phone;
		document.getElementById('cell').value = cell;
		document.getElementById('fax').value = fax;
		document.getElementById('country').value = country;
		document.getElementById('town').value = town;
		document.getElementById('title').value = title_id;
		document.getElementById('primaryContact').value = primaryContact;
		document.getElementById('ageGroup').value = ageGroup;
		document.getElementById('secondaryContact').value = secondaryContact;
		document.getElementById('occupation').value = occupation;
		document.getElementById('gender').value = gender;
		document.getElementById('new_visit').value = id;
		document.getElementById('updates').checked = receive_updates=='0'?false:true;
	} else {
		document.getElementById('fname').value = '';
		document.getElementById('lname').value = '';
		document.getElementById('email').value = '';
		document.getElementById('email2').value = '';
		document.getElementById('tel').value = '';
		document.getElementById('cell').value = '';
		document.getElementById('fax').value = '';
		document.getElementById('country').value = '';
		document.getElementById('town').value = '';
		document.getElementById('title').value = '';
		document.getElementById('primaryContact').value = '';
		document.getElementById('ageGroup').value = '';
		document.getElementById('secondaryContact').value = '';
		document.getElementById('occupation').value = '';
		document.getElementById('gender').value = '';
		document.getElementById('new_visit').value = '1';
		document.getElementById('updates').checked = true;
		alert('Data not found');
	}
}

function check_form(){
	err = false;
	req = new Array('fname','lname','email','tel','country','town','captcha_code');

	for (x=0; x<req.length;x++) {
		a = document.getElementById(req[x]);
		a.className = '';
		if (a.value == '') {
			a.className = 'form_error';	
			err = true;
		}
	}
	
	if (err) {
		alert("Form Error!\nPlease complete all highlighted fields and re-sebmit the form");
		return false;
	}

	apos=document.getElementById('email').value.indexOf("@")
	dotpos=document.getElementById('email').value.lastIndexOf(".")
	if ( apos<1 || dotpos-apos<2) {
		document.getElementById('email').className = 'form_error';
		document.getElementById('email').focus();
		alert("Form error\nInvalid email address. Please correct and re-submit the form")
		return false
	}

	document.getElementById('email2').className = '';
	if (document.getElementById('email').value != document.getElementById('email2').value) {
		document.getElementById('email').className = 'form_error';
		document.getElementById('email2').className = 'form_error';
		document.getElementById('email').focus();
		alert("Email Mismatch!\nPlease check that your email address is correct (in both fields)");
		return false;
	}
}

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td class="text_grey_small">Please fill in your details below and click on the &quot;<strong>Submit</strong>&quot;  button to send booking enquiries to <strong><!-- establishment_name --></strong>. All fields with red asterisks are required. <strong>Please note: </strong>all emails are sent directly to the establishment. If you do not receive a reply we recommend you contact them directly by phone or fax.</td>
  </tr>
</table>
<br>
<form action="/index.php?p=successenquiry&c=<!-- form_est_code -->" method="post" name="enquiryForm" id="enquiryForm" onSubmit="return check_form()">
<input type="hidden" name="estabCode" id="estabCode" value="GI7818" />
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table_light_grey_border" id="table">
    <tr valign=top>

      <td align="right" class="text_grey_small">Designation:</td>
      <td class="text_grey_small">
	    <select name="title" id="title" class="textfield">
					<option value="1"></option>
					<option value="2">Mrs</option>
					<option value="3">Mr</option>
					<option value="4">Ms</option>
					<option value="5">miss</option>
					<option value="6">dr</option>
					<option value="7">Prof</option>
				</select></td>
			<td rowspan=5 colspan=2 align=right>
				<table style='border-color: grey; border-width: 1 1 1 1; border-style: solid; background-color: silver; width: 200px' cellpadding=2 cellspacing=0>
					<tr>
						<td colspan=2 style='font-weight: bold; font-family: Arial; font-size: 14px'>Regular Visitors</td>
					</tr>
					<tr>
						<td style='font-family: Arial; font-size: 14px'>Email: </td>
						<td ><input type=text name='return_email' id='return_email' style='width: 120'></td>
					</tr>
					<tr>
						<td style='font-family: Arial; font-size: 14px'>Surname: </td>
						<td><input type=text name='return_surname' id='return_surname' style='width: 120'></td>
					</tr>
					<tr>
						<td colspan=2 align=center><input type=button name=fetchData value='Fetch my Details' style='font-family: Arial; font-size: 14px' onClick=get_data();></td>
					</tr>
				</table>
			</td>
    </tr>
    <tr>
      <td align="right" class="text_grey_small">First Name: </td>

      <td class="text_grey_small"><input name="fname" type="text" class="textfield" id="fname"><span class="error"> *</span></td>
    </tr>
    <tr>
      <td align="right" class="text_grey_small">Last Name: </td>
      <td class="text_grey_small"><input name="lname" type="text" class="textfield" id="lname"><span class="error"> *</span></td>
    </tr>

    <tr>
      <td align="right" class="text_grey_small">Email Address: </td>
      <td class="text_grey_small"><input name="email" type="text" class="textfield" id="email"><span class="error"> *</span></td>
    </tr>
    <tr>
      <td align="right" class="text_grey_small" nowrap>Confirm Email Address:</td>
      <td class="text_grey_small"><input name="email2" type="text" class="textfield" id="email2"><span class="error"> *</span></td>

    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
      <td align="right" class="text_grey_small">Telephone Number: </td>
      <td class="text_grey_small"><input name="tel" type="text" class="textfield" id="tel"><span class="error"> *</span></td>
       <td align="right" class="text_grey_small" nowrap>Preferred Method of Contact: </td>
      <td class="text_grey_small">
	    <select name="primaryContact" class="textfield" id="primaryContact">
	    <option value=""></option>
		  <option value="Email">Email</option>
          <option value="Telephone">Telephone</option>

          <option value="Fax">Fax</option>
          <option value="SMS">SMS</option>
        </select>
	  </td>
    </tr>
    <tr>
      <td align="right" class="text_grey_small">Fax Number: </td>

      <td class="text_grey_small"><input name="fax" type="text" class="textfield" id="fax"></td>
      <td align="right" class="text_grey_small" nowrap>Alternative Method of Contact: </td>

      <td class="text_grey_small">
	    <select name="secondaryContact" class="textfield" id="secondaryContact">
	    	<option value=""> </option>
		  <option value="Email">Email</option>
          <option value="Telephone">Telephone</option>
          <option value="Fax">Fax</option>
          <option value="SMS">SMS</option>
        </select>

	  </td>
    </tr>
    <tr>
      <td align="right" class="text_grey_small">Cell Number: </td>
      <td class="text_grey_small"><input name="cell" type="text" class="textfield" id="cell"></td>
    </tr>
    <tr>
      <td align="right" class="text_grey_small">Country:</td>

      <td class="text_grey_small">
          <input name="country" type="text" class="textfield" id="country"><span class="error"> *</span>
      </td>
    </tr>
	<tr>
      <td align="right" class="text_grey_small">City/Town:</td>
      <td class="text_grey_small"><input name="town" type="text" class="textfield" id="town"><span class="error"> *</span></td>

    </tr>
    <tr><td>&nbsp;</td></tr>
	<tr>
    	<td class="text_grey_small" align="right">Travelling By: </td>
		<td>
			<select name="travelMethod" id="travelMethod" class="textfield">
				<option value="Car" )>Car</option>
				<option value="Hire Car" >Hire Car</option>

				<option value="Air" >Air</option>
				<option value="Train" >Train</option>
				<option value="Ship" >Ship</option>
			</select>
		</td>
		 <td align="right" class="text_grey_small">Date of Arrival: </td>

      <td class="text_grey_small"><input name="arriveDate" type="text" class="textfield" id="arriveDate" size="10" value="" onclick="cal2.select(document.getElementById('arriveDate'),'arriveDate','yyyy-MM-dd'); return false;"></td>
    </tr>
    <tr>

      <td align="right" class="text_grey_small">No of Adults: </td>
      <td class="text_grey_small"><input name="adults" type="text" class="textfield" id="adults" value="" ></td>
      <td align="right" class="text_grey_small">Date of Departure: </td>
      <td class="text_grey_small" nowrap><input name="departDate" type="text" class="textfield" id="departDate" size="10" value=""  onclick="cal1.select(document.getElementById('departDate'),'departDate','yyyy-MM-dd'); return false;">
      </td>
    </tr>
    <tr>
      <td align="right" class="text_grey_small">No of Children : </td>
      <td class="text_grey_small"><input name="children" type="text" class="textfield" id="children" value=""> (under 12 years)</td>

    </tr>
    <tr>
      <td align="right" class="text_grey_small">No of Rooms: </td>
      <td class="text_grey_small"><input name="rooms" type="text" class="textfield" id="rooms" value=""></td>
    </tr>
	<tr><td>&nbsp;</td></tr>
    <tr>
      <td align="right" class="text_grey_small" valign="top"><span class="grey_text">Additional Comments:</span></td>
      <td colspan=4><textarea name="requests"  class="textfield" id="requests" style='width: 100%; height: 80'></textarea></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
	<tr>
  	<td valign="top" class="text_grey_small" align="right">Age group: </td>
	<td>

		<select name="ageGroup" id="ageGroup" class="textfield">
			<option value="">Unspecified</option>
			<option value="Less than 21">Less than 21 years</option>
			<option value="21-30">21-30 years</option>
			<option value="31-40">31-40 years</option>
			<option value="41-50">41-50 years</option>

			<option value="51-60">51-60 years</option>
			<option value="61-70">61-70 years</option>
			<option value="Over 70">Over 70 years</option>
		</select>
	</td>
  </tr>
  <tr>

  	<td valign="top" class="text_grey_small" align="right">Occupation: </td>
	<td><input name="occupation" type="text" class="textfield" id="occupation" /></td>
  </tr>
  <tr>
  	<td valign="top" class="text_grey_small" align="right">Gender: </td>
	<td>
		<select name="gender" id="gender" class="textfield">
			<option value="">Unspecified</option>

			<option value="M">Male</option>
			<option value="F">Female</option>
		</select>
	</td>
  </tr>
    <tr>
      <td align="right">&nbsp;</td>
      <td class="text_grey_small" colspan=10><input name="updates" type="checkbox" id="updates" value="yes" checked="checked" />
        I travel regularly, and would like to  receive updates from the AA Travel Info website via email </td>
    </tr>
	<tr><td>&nbsp;</td><td>&nbsp;<input type=hidden name='new_visit' id='new_visit' value=1 ></td></tr>
	<tr valign=top>
	  <td  valign="top" class="text_grey_small" align=right>Security code: </td>
	  <td><img id="captcha" src="/securimage/securimage_show.php" alt="CAPTCHA Image" /> <input type=button value='Change Image' onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false"></td>
	</tr>
	<tr>
    	<td  class="text_grey_small">Verify Code: </td>
    	<td  class="text_grey_small"><input type="text" name="captcha_code" id="captcha_code" size="10" maxlength="6" /> (Copy security code from above image)</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="SubmitEnquiry" value="Submit Enquiry" id="SubmitEnquiry"></td>
    </tr>
</table>
<div id='calDiv' STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"> </div>
<iframe id='return_data' style='display: none'> </iframe>
</form>