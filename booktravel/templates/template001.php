<?php
$file = "template001.php";
$last_modified_time = filemtime($file);
$etag = md5_file($file);

//echo gmdate("D, d M Y H:i:s", $last_modified_time);

header("Content-Type: text/css");
header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
header("Expires: Sat, 26 Jul 2030 05:00:00 GMT"); 
header("Etag: $etag");

if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time ||
    trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) {
    header("HTTP/1.1 304 Not Modified");
    exit; 
}
?>

BODY  {background-color: #e5e5e5;}
BODY, TD, TH, LI {font-family: arial; font-size: 10pt}
A {text-decoration: none;color: black}
H1 {font-family: arial; font-size: 16pt}
H2 {font-size: 12pt; margin-bottom: 4px; padding: 0}

img {
	border:0;
}

hr {
	clear:both;
}

a {
	color:#21489B;
}

#GuestReview {
	background-image: url('/images/tbg.png');
	position:absolute;
	top:0;
	left:0;
	z-index:200001;
	text-align: center;
}
#GuestReviewClose {
	float: right;
	border-left: 1px solid #000000;
	cursor: pointer;
	padding:3px;
}
#GuestReviewHolder {
	width:700px;
	background-color: #ffffff;
	border: 1px solid #000000;
	margin-top:40px;	
}
#GuestReviewHeader {
	background-color: #FFCC00;
	color:#000000;
	font-weight:bold;
	text-align: left;
	border-bottom: 1px solid #000000;
}

#AdvancedSearch {
	background-image: url('/images/tbg.png');
	position:absolute;
	top:0;
	left:0;
	z-index:200001;
	text-align: center;
}

#AdvancedSearchClose {
	float: right;
	border-left: 1px solid #000000;
	cursor: pointer;
	padding:3px;
}

#AdvancedSearchHolder {
	width:700px;
	background-color: #ffffff;
	border: 1px solid #000000;
	margin-top:40px;	
}
#AdvancedSearchHeader {
	background-color: #FFCC00;
	color:#000000;
	font-weight:bold;
	text-align: left;
	border-bottom: 1px solid #000000;
}
#RegionDropDowns {
	padding:4px;
	margin:0;
	height:20px;
	margin:0px;
	margin-top:28px;
	text-align: left;
	width: 952px;
	position: absolute;
	z-index:10;
}

.advSearchFieldset {
	float: left;
	width: 260px;
	padding: 0px;
	margin: 2px;
	text-align: left;
}
.advSearchFieldsetIcon {
	float: left;
	width: 680px;
	padding: 0px;
	margin: 2px;
	text-align: left;
}

.advSearchFieldsetIcon div {
}
.advSearchIconOff {
	float:left;
	width: 170px;
	height:35px;
	cursor:pointer;
}
.advSearchIconOn {
	float:left;
	width: 170px;
	height:35px;
	cursor:pointer;
	background-color: #0074BB;
	color:#ffffff;
}
.advSearchIconImg {
	float:left;
	margin:2px;
}
.advSearchIconText {
	float:right;
	width:133px;
}

#AdvancedSearchLocations {
}

#AdvancedSearchLocations .ulOut {
	border:1px solid #000000;
	margin:3px;
	padding:0;
	height:18px;
	overflow-y: hidden;
	cursor: pointer;
	background-color: #FFFFFF;
	background-image: url('/images/dropdown.gif');
	background-position: right top;
	background-repeat: no-repeat;
	padding-right: 15px;
	padding-left: 2px;
	position: absolute;
}

#AdvancedSearchLocations ul:hover {
	overflow-y: scroll;
	height: auto;
	max-height:250px;
	padding-right: 0px;
	z-index:10;
	position: absolute;
	
}

#AdvancedSearchLocations .ulHover {
	border:1px solid #000000;
	margin:0;
	padding:0;
	cursor: pointer;
	background-color: #FFFFFF;
	background-image: url('/images/dropdown.gif');
	background-position: right top;
	background-repeat: no-repeat;
	padding-right: 15px;
	padding-left: 2px;
	overflow-y: scroll;
	height: auto;
	max-height:250px;
	padding-right: 0px;
}

#AdvancedSearchLocations li {
	list-style: none;
	font-size:12px;
	padding:2px;
	margin:0;
	color:#000000;
}

#AdvancedSearchLocations a {
	color:#0066FF;
	font-family: arial;
	font-size: 12px;
	text-decoration: none;
}

#AdvancedSearchLocations a:hover {
	color: #21489B;
	
}

#AdvancedSearchLocations li:hover {
	background-color: #BFD9FF;
}

#AdvancedSearchLocations .dropAreaBlock {
	margin-right: 5px;
	z-index: 10;
	height:26px;
	text-align:left;
}

#SearchRight {
	position:absolute;
	margin-left:750px;
	margin-top:40px;
}

#thumbHolderDiv {
	max-height: 210px;
	overflow-y: scroll;
}

.restypeDesc {
	position:absolute;
	width:250px;
	color:#18479F;
	border:1px solid #18479F;
	background-color:#BFD9FF;
	padding:3px;
	margin-top:15px;
	margin-left:30px;
}

#LocationSearchBox {
	position: absolute;
	background-color: #EBEBEB;
	border:1px solid #CCCCCC;
	margin-top:-50px;
	margin-left:-5px;
	width:398px;
	height:124px;
	padding:3px;
	font-size: 14px;
	overflow-y: hidden;
}
#LocationSearchBox div {
	padding:3px;
}
#LocationSearchBox div:hover {
	background-color: #ffffff;
}

.hidden {
	display:none;
}
#LocationSearchBox h3 {
	padding:0;
	margin:0;
	margin-bottom:10px;
}
#EstHolder {
	float:left;
	width:737px;
	text-align: left;
	z-index:9;
	
}

#EstHolder h3 {
	font-size:13px;
	padding:0;
	margin:0;
	color:#21489B;
}

#EstRightHolder {
	float:left;
	width:219px;
	margin-left:3px;
	text-align: left;
	margin-top:50px;
}

#EstHolder h1 {
	padding:4px;
	margin:0;
	font-family: arial;
	font-size:16px;
	margin-right:-223px;
}

.estHeader h1 {
	padding:0;
}

.estListHeader {
	font-size:16px;
	padding:4px;
}

#EstRightHolder h1 {
	background-color: #FFCC00;
	padding:3px;
	margin:0;
	font-family: arial;
	font-size:14px;
}

.estHolderButton {
	background-color:#FFCC00;
	font-weight: bold;
	color:#000000;
	margin:6px;
	padding:8px;
	padding-bottom:7px;
	font-size:12px;
	width:184px;
	float:left;
	text-align: center;
	background-image: url('/images/button.png');
}

#EstRightHolder .emButton {
	background-color:#FFCC00;
	font-weight: bold;
	color:#000000;
	margin:6px;
	padding:8px;
	font-size:12px;
	width:184px;
	float:left;
	text-align: center;
	background-image: url('/images/button.png');
}

#EstRightHolder ul {
	margin:5px;
	padding:0;
}
#EstRightHolder li {
	margin: 0;
	padding:0;	
	list-style: none;
}

#EstThumbBlock {
	padding:3px;
	border:1px solid #97D1FF;
	float:left;
}

#EstImageHolder img {
	border:1px solid #CCCCCC;
	float:left;
	padding:2px;
	margin:3px;
	background-color:#ffffff;
}

#EstImageHolder {
	border:1px solid #CCCCCC;
	background-color: #EBEBEB;
	padding:3px;
	margin-left:53px;
}

#ImgTab {
	margin-left:0px;
	margin-top:40px;
	position:absolute;
	z-index:9;
}
#ThreeTab {
	margin-left:0px;
	margin-top:100px;
	position:absolute;
}

#ImageTabs {
}
.imageTab {
	
	background-color: #EBEBEB;
	float:left;
	margin-right:2px;
	margin-left:2px;
	font-size: 14px;
	padding:3px;
	margin-bottom:-2px;
	width:48px;
	height:48px;
	overflow: hidden;
	z-index:9;
}
.onTab {
	border:1px solid #CCCCCC;
	border-right:0;
}
.offTab {
	border:1px solid #CCCCCC;
	
}

.estHeading {
	padding-top:3px;
	padding-bottom:3px;
	color:#05057C;
	font-size:12px;
	font-weight:bold;
}

/* new area styles */

#EstabListHolder h1 {
	padding:4px;
	padding-bottom:5px;
	margin:0;
	font-family: arial;
	font-size:16px;
	margin-right:-225px;
	
}

#EstabListHolder .estListPaging {
	font-size:12px;
	text-align: left;
	margin-bottom:15px;
}
.estListPaging a {
	font-weight: bold;
}

#AreaBodyHolder {
	float: left;
}

#RegionDropDowns {
	padding:4px;
	margin:0;
	height:20px;
	margin:0px;
	margin-top:28px;
	text-align: left;
	width: 952px;
	position: absolute;
	z-index:10;
}

#RegionDropDowns .ulOut {
	border:1px solid #000000;
	margin:0;
	padding:0;
	height:18px;
	overflow-y: hidden;
	cursor: pointer;
	background-color: #FFFFFF;
	background-image: url('/images/dropdown.gif');
	background-position: right top;
	background-repeat: no-repeat;
	padding-right: 15px;
	padding-left: 2px;
}

#RegionDropDowns ul:hover {
	overflow-y: scroll;
	height: auto;
	max-height:250px;
	padding-right: 0px;
	z-index:10;
}

#RegionDropDowns .ulHover {
	border:1px solid #000000;
	margin:0;
	padding:0;
	cursor: pointer;
	background-color: #FFFFFF;
	background-image: url('/images/dropdown.gif');
	background-position: right top;
	background-repeat: no-repeat;
	padding-right: 15px;
	padding-left: 2px;
	overflow-y: scroll;
	height: auto;
	max-height:250px;
	padding-right: 0px;
}

#RegionDropDowns li {
	list-style: none;
	font-size:12px;
	padding:2px;
	margin:0;
	color:#000000;
}

#RegionDropDowns a {
	color:#0066FF;
	font-family: arial;
	font-size: 12px;
	text-decoration: none;
}

#RegionDropDowns a:hover {
	color: #21489B;
	
}

#RegionDropDowns li:hover {
	background-color: #BFD9FF;
}

#RegionDropDowns .dropAreaBlock {
	float:left;
	margin-right: 5px;
	z-index: 10;
}

.areaLoader {
	background-color: #FFCC00;
	border: 2px solid #000000;
	float: left;
	margin: 1px;
	padding:1px;
}

.areaBlockHolder {
	width:220px;
	text-align:left;
	float:left;
	clip: auto;
	margin-left: 5px;
	margin-top:60px;
}

.areaBlock {
}

.areaBlock ul {
	margin:0;
	padding:3px;
	border-top:0;
}
.areaBlock li {
	list-style: none;
	font-size:11px;
}
.areaBlock a {
	color:#0066FF;
	font-family: arial;
	font-size: 12px;
	text-decoration: none;
}

.areaBlock a:hover {
	color: #21489B;
}

.areaBlock h1 {
	padding:3px;
	margin:0;
	font-family: arial;
	font-size:12px;
}
.areaEstHolder {
	float:left;
	width:735px;
	text-align: left;
}
.listEstImg {
	height:118px;
	background-color: #FFCC00;
	width:150px;
	margin-bottom:3px;
	margin-top:3px;
	font-size:10px;
	font-weight:bold;
}
.listQaText {
	width:120px;
	text-align: center;
	position: absolute;
	margin-top: 88px;
	padding:2px;
}
/* new area styles */

/* added by John Gore - START */
.alinks {
	color:#0066FF;}
.alinks:hover {
	color:#0000FF;
	text-decoration:underline;}
.paragraphtext {
	font-family: Arial;
	font-size: 12px;}
.inparagraphheading {
	font-family: Arial;
	font-size: 15px; 
	font-weight: bold; }
	
.paragraphpictureright img { 
	display:block;
	border: 1px solid #CCCCCC;
	vertical-align:middle;
	margin-bottom: 3px; }
.paragraphpictureleft img { 
	display:block;
	border: 1px solid #CCCCCC;
	vertical-align:middle;
	margin-bottom: 3px; }
.paragraphpictureright { margin: 0.5em 0pt 0.5em 0.8em; float:right; 	background-color: #F9F9F9;
	border: 1px solid #CCCCCC;
	padding: 3px;
	font: 11px Arial, sans-serif; }
.paragraphpictureleft { margin: 0.5em 0.8em 0.5em 0; float:left;	background-color: #F9F9F9;
	border: 1px solid #CCCCCC;
	padding: 3px;
	font: 11px Arial, sans-serif; }
/* added by John Gore - END */


.searchHeader {
	color: white;
	font-family: arial;
	font-size: 12pt;
	font-weight: bold;
	padding-left: 8px;
}


/* Breadcrumbs */
#BreadCrumbs ul {
	padding:0;
	margin:0;
}
#BreadCrumbs li {
	display:inline;
	list-style: none;
	padding-right:5px;
}
/* Link Blocks */
.linksBlock {
	width:250px;
	float:left;
}
.linksBlock ul {
	padding:0;
	margin:0;
}
.linksBlock li {
	list-style: none;
}
/* Est List */
#estListHolder {
	width:530px;
	float:left;
	padding-top:10px;
}
.listHeaderSection1 {
	width:23px;
	height:33px;
	background-image: url("/images/towns_tl.gif");
	float:left;
}
.listHeaderSection2 {
	height:33px;
	width:487px;
	background-image: url("/images/towns_tc.gif");
	float:left;
	display:inline;
}
.listHeaderSection2search {
	height:33px;
	background-image: url("/images/towns_tc.gif");
	float:left;
	display:inline;
	width:782px;
}
.listHeaderSection3 {
	width:20px;
	height:33px;
	background-image: url("/images/towns_tr.gif");
	float:left;
}
.listHeaderSection4 {
	width:162px;
	height:33px;
	background-image: url("/images/search4.gif");
	float:left;
}
.listHeaderPadding {
	margin-top:7px;
}
.estListLinks {
	padding:0;
	margin:0;
	padding-left:8px;
}
.listPaging {
	background-color: #C0C0C0;
	margin-left:2px;
	margin-right:2px;
	text-align: center;
}
.listEst {
	width:737px;
}
.listEstDiv {
	float:left;
}
.listEstDiv h3 {
	margin:0;
	padding:0;
}
.qaListImg {
	position: absolute;
	margin-left:123px;
	margin-top:74px;
}
.est_name a {
	font-size: 14px;
}


.searchHeader {
	color: yellow;
	font-family: arial;
	font-size: 10pt;
	font-weight: bold;
	padding-left: 8px;
}

.searchLabel {
	float:left;
	clear:both;
	width:150px;
}
.searchField {
	float:left;
}

#search_loc {margin-left: 20px}

.dum_bar {background-color: silver; border-top: 1px dotted black; border-bottom: 1px dotted black; width: 831px; margin: 0 2px 0 2px}
.dum_header {font-weight: bold; margin-left: 4px; width: 200px}
.search_link {cursor: pointer; margin-left: 20px}

.est_name {
	color: #0770b0;
	font-family: arial;
	font-size: 8pt;
	font-weight: bold;
	padding-left: 8px;
	padding-top: 6px;
	text-align: left;
}

.est_body {
	color: black;
	font-family: arial;
	font-weight: normal;
	padding-left: 8px;
	text-align: left;
}

.est_body2 {
	color: black;
	font-family: arial;
	font-size: 8pt;
	font-weight: normal;
	text-align: left;
}

.nav_header, .nav_footer {background-color: silver;}
.nav_header TD {font-size: 10pt; padding: 1px 2px 1px 2px}
.nav_footer TD {font-size: 10pt; padding: 1px 2px 1px 2px; }

.searchLink {
	cursor: pointer;
}

.noLink {
	color: grey;
}

#errorT {font-size: 10pt; font-weight: bold; text-align: center}


#oa TR, #se TR  {
	vertical-align: top;
}

.adT {text-align: center;margin-bottom: 4}
.adS {text-align: right;}


/* TOWN CONTENT */
#content_block {overflow: hidden; border-color: silver; border-style: solid; border-width: 1px; background-color: #EBEBEB; padding: 4px; margin-top: 12px}
#content_block H2 {font-family: arial; font-size: 12pt; font-weight: bold; margin: 0px 0px 2px 0px}
#content_block .para_content {font-family: arial; font-size: 10pt; font-weight: normal; padding-bottom: 10px}
#more {border-color: silver; border-style: solid; border-width: 1px; background-color: silver; font-size: 10pt; font-weight: bold; text-align: right; padding: 4px}

.error {color: red}
.text_grey_small {font-family: Arial;font-size: 10pt;}
.form_error {background-color: yellow;}
#feedbackOptions {border-width: 1 1 1 1; border-style: solid}
#feedbackOptions TH {background-color: #CDD6DF; color: #26619c;font-size: 12px; border-color: grey; border-width: 0 0 1 0; border-style: dotted}
#feedbackOptions TD {background-color: #E4E9EE; border-color: grey; border-width: 0 0 1 0; border-style: dotted;}
.o {background-color: #F3CA00}

#estab_marque TD {padding: 0 10 0 0; font-family: arial; font-size: 11px}
#estab_marque IMG {border-width: 1; border-color: black; border-style: solid}

/* GOOGLE MAPS */
#dir_canvas TD {font-size: 8pt; font-family: arial;}
#routes {border-collapse: collapse; border: 1px dotted grey}
#routes TH {text-align: left; background-color: silver;}
#routes TD {font-size: 10pt; font-family: arial; background-color: #EBEBEB;}
#routes .divider {border-right: 1px dotted grey}
#map_canvas1 {border-collapse: collapse; border: 1px dotted grey; background-color: #EBEBEB; margin-top: 20px; width: 100%}
#map_canvas1 .divider {border-left: 1px dotted grey}
#dir_header {background-color: silver; font-weight: bold; font-family: arial; font-size: 12pt; width: 100%}
#map_canvas {border: 1px dotted grey}

/* CALENDAR */
.TESTcpYearNavigation,
.TESTcpMonthNavigation
		{
		background-color:#6677DD;
		text-align:center;
		vertical-align:middle;
		text-decoration:none;
		color:#FFFFFF;
		font-weight:bold;
		}
.TESTcpDayColumnHeader,
.TESTcpYearNavigation,
.TESTcpMonthNavigation,
.TESTcpCurrentMonthDate,
.TESTcpCurrentMonthDateDisabled,
.TESTcpOtherMonthDate,
.TESTcpOtherMonthDateDisabled,
.TESTcpCurrentDate,
.TESTcpCurrentDateDisabled,
.TESTcpTodayText,
.TESTcpTodayTextDisabled,
.TESTcpText
		{
		font-family:arial;
		font-size:8pt;
		}
TD.TESTcpDayColumnHeader
		{
		text-align:right;
		border:solid thin #6677DD;
		border-width:0 0 1 0;
		}
.TESTcpCurrentMonthDate,
.TESTcpOtherMonthDate,
.TESTcpCurrentDate
		{
		text-align:right;
		text-decoration:none;
		}
.TESTcpCurrentMonthDateDisabled,
.TESTcpOtherMonthDateDisabled,
.TESTcpCurrentDateDisabled
		{
		color:#D0D0D0;
		text-align:right;
		text-decoration:line-through;
		}
.TESTcpCurrentMonthDate
		{
		color:#6677DD;
		font-weight:bold;
		}
.TESTcpCurrentDate
		{
		color: #FFFFFF;
		font-weight:bold;
		}
.TESTcpOtherMonthDate
		{
		color:#808080;
		}
TD.TESTcpCurrentDate
		{
		color:#FFFFFF;
		background-color: #6677DD;
		border-width:1;
		border:solid thin #000000;
		}
TD.TESTcpCurrentDateDisabled
		{
		border-width:1;
		border:solid thin #FFAAAA;
		}
TD.TESTcpTodayText,
TD.TESTcpTodayTextDisabled
		{
		border:solid thin #6677DD;
		border-width:1 0 0 0;
		}
A.TESTcpTodayText,
SPAN.TESTcpTodayTextDisabled
		{
		height:20px;
		}
A.TESTcpTodayText
		{
		color:#6677DD;
		font-weight:bold;
		}
SPAN.TESTcpTodayTextDisabled
		{
		color:#D0D0D0;
		}
.TESTcpBorder
		{
		border:solid thin #6677DD;
		}