<?php 
//Initialize Variables


//map
if (isset($_POST['SubmitEnquiry'])) {
	send_form();
} else {
	display_form();
}

function display_form() {
	$GLOBALS['content'] = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/templates/enquiry_form.tpl");
	
	//Get Data
	$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
	$sql_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_name->bind_param('s', $_GET['c']);
	$sql_name->execute();
	$sql_name->bind_result($establishment_name);
	$sql_name->store_result();
	$sql_name->fetch();
	$sql_name->close();
	
	//Replace Tags
	$GLOBALS['content'] = str_replace('<!-- establishment_name -->', $establishment_name, $GLOBALS['content']);
}


function send_form() {
	session_start();
	
	//Check Securicode
	include_once $_SERVER['DOCUMENT_ROOT'] . '/securimage/securimage.php';
	$securimage = new Securimage();
	if ($securimage->check($_POST['captcha_code']) == false) {
		//echo '<script>alert("Security Error!\nPlease enter the correct security code");history.back()</script>';
		//die();
	}
	
	foreach($_POST as $k=>$v) {
		$$k = $v;
	}
	$updates = isset($updates)?1:0;
	
	//Update Data
	if (isset($_POST['new_visit']) && $_POST['new_visit'] == 1) {
		$statement = "INSERT INTO nse_visitor 
									(title_id, firstname,surname,email,country,town,phone,cell,fax,contact_method,contact_secondary,age_group,occupation,gender,receive_updates)
									VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_insert->bind_param('sssssssssssssss', $title, $fname,$lname,$email,$country,$town,$tel,$cell,$fax,$primaryContact,$secondaryContact,$ageGroup,$occupation,$gender,$updates);
		$sql_insert->execute();
		$new_visit = mysqli_insert_id($GLOBALS['dbCon']);
	} else {
		$statement = "UPDATE nse_visitor SET
									title_id=?, firstname=?,surname=?,email=?,country=?,town=?,phone=?,cell=?,fax=?,contact_method=?,
									contact_secondary=?,age_group=?,occupation=?,gender=?,receive_updates=? WHERE visitor_id=?";
		$sql_insert = $GLOBALS['dbCon']->prepare($statement);
		echo mysqli_error($GLOBALS['dbCon']);
		$sql_insert->bind_param('ssssssssssssssss', $title, $fname,$lname,$email,$country,$town,$tel,$cell,$fax,$primaryContact,$secondaryContact,$ageGroup,$occupation,$gender,$updates,$new_visit);
		$sql_insert->execute();
	}
	
	//Save enquiry
	$statement = "INSERT INTO nse_establishment_enquiry 
								(visitor_id,establishment_code,arrive_date,depart_date,adults,children,rooms,comment,travel_method,enquiry_date)
								VALUES (?,?,?,?,?,?,?,?,?,NOW())";
	$sql_enquiry = $GLOBALS['dbCon']->prepare($statement);
	$sql_enquiry->bind_param('sssssssss', $new_visit,$_GET['c'],$arriveDate,$departDate,$adults,$children,$rooms,$requests,$travelMethod);
	$sql_enquiry->execute();
	
	//Get Title
	$statement = "SELECT title_name FROM nse_title WHERE title_id=?";
	$sql_title = $GLOBALS['dbCon']->prepare($statement);
	$sql_title->bind_param('i', $title);
	$sql_title->execute();
	$sql_title->bind_result($title_name);
	$sql_title->store_result();
	$sql_title->fetch();
	$sql_title->close();
	
	//Get Establishment Email
	$statement = "SELECT reservation_email FROM nse_establishment_reservation WHERE establishment_code=?";
	$sql_email = $GLOBALS['dbCon']->prepare($statement);
	$sql_email->bind_param('s', $_GET['c']);
	$sql_email->execute();
	$sql_email->bind_result($res_email);
	$sql_email->store_result(); 
	$sql_email->fetch();
	$sql_email->close();
	
	//Get Establishment Data
	$statement = "SELECT a.establishment_name, b.reservation_tel, b.reservation_fax, c.town_id, c.suburb_id, d.contact_tel, d.contact_email, d.contact_fax, d.contact_cell 
								FROM nse_establishment AS a
								JOIN nse_establishment_reservation AS b ON a.establishment_code=b.establishment_code
								JOIN nse_establishment_location AS c ON a.establishment_code=c.establishment_code  
								JOIN nse_establishment_contact AS d ON a.establishment_code=d.establishment_code  
								WHERE a.establishment_code=?";
	$sql_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_name->bind_param('s', $_GET['c']);
	$sql_name->execute();
	$sql_name->bind_result($establishment_name,$restel, $resfax, $town_id, $suburb_id, $contact_tel, $contact_email, $contact_fax, $contact_cell);
	$sql_name->store_result();
	$sql_name->fetch();
	$sql_name->close();
	
	//Get  Town Name
	$statement = "SELECT town_name FROM nse_location_town WHERE town_id=?";
	$sql_name = $GLOBALS['dbCon']->prepare($statement);
	$sql_name->bind_param('i', $town_id);
	$sql_name->execute();
	$sql_name->bind_result($town_name2);
	$sql_name->store_result();
	$sql_name->fetch();
	$sql_name->close();
	
	//Get  Suburb Name
	$suburb_name = '';
	if ($suburb_id != 0) {
		$statement = "SELECT suburb_name FROM nse_location_suburb WHERE suburb_id=?";
		$sql_name = $GLOBALS['dbCon']->prepare($statement);
		$sql_name->bind_param('i', $suburb_id);
		$sql_name->execute();
		$sql_name->bind_result($suburb_name);
		$sql_name->store_result();
		$sql_name->fetch();
		$sql_name->close();
	}
	
	//Get email Template
	$emailTemplate = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/booking_email_template.tpl');
	
	//Replace Tags
	$emailTemplate = str_replace('<!-- title -->', $title_name, $emailTemplate);
	foreach($_POST as $k=>$v) {
		$emailTemplate = str_replace("<!-- $k -->", $v, $emailTemplate);
	}
	$emailTemplate = eregi_replace("[\]",'',$emailTemplate);
	
	//Send Emails
	if (empty($res_email)) $res_email = $contact_email;
	if (empty($res_email)) $res_email = "noemail@booktravel.travel";
	require_once($_SERVER['DOCUMENT_ROOT'] . '/PHPMailer/class.phpmailer.php');  
	$mail = new PHPMailer(); 
	$mail->AddReplyTo($email,"$fname $lname"); 
	$mail->SetFrom($email,"$fname $lname");
	$mail->AddAddress("$res_email");
	$mail->Subject = "Booking Enquiry via AA Travel Guides Website";
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!";
	$mail->MsgHTML($emailTemplate);
//	$mail->Send();
	
	$mail->ClearAddresses();
	$mail->AddAddress("resenquiries@booktravel.travel");
	if (!empty($suburb_name)) $suburb_name = "($suburb_name)";
	$mail->Subject = "COPY :: Booking Enquiry via AA Travel Guides Website $town_name2 $suburb_name, $establishment_name for {$_POST['lname']}";
//	$mail->Send();
	
	
	
	//Get page template
	$pageContent = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/booking_result.tpl');

	
	//$mail->ClearAddresses();
	//$mail->AddAddress("resenquiries@booktravel.travel");
	//$mail->Subject = "COPY :: Booking Enquiry via AA Travel Guides Website";
	//$mail->Send();
	
	//Assemble Establishment  tel/fax
	$restel = empty($restel)?$contact_tel:$restel;
	$restel = empty($restel)?$contact_cell:$restel;
	$resfax = empty($resfax)?$contact_fax:$resfax;
	$res_email = empty($res_email)?$contact_email:$res_email;
	
	
	//Send to Client
	if ($res_email = "noemail@booktravel.travel") $res_email = '';
	$cEmailTemplate = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/booking_email_client.tpl');
	
	$cEmailTemplate = str_replace('<!-- title -->', $title_name, $cEmailTemplate);
	foreach($_POST as $k=>$v) {
		$cEmailTemplate = str_replace("<!-- $k -->", $v, $cEmailTemplate);
	}
	$cEmailTemplate = eregi_replace("[\]",'',$cEmailTemplate);
	$cEmailTemplate = str_replace('<!-- establishment -->', $establishment_name, $cEmailTemplate);
	$cEmailTemplate = str_replace('<!-- est_tel -->', $restel, $cEmailTemplate);
	$cEmailTemplate = str_replace('<!-- est_fax -->', $resfax, $cEmailTemplate);
	$cEmailTemplate = str_replace('<!-- est_email -->', $res_email, $cEmailTemplate);
	$cEmailTemplate = str_replace('<!-- town2 -->', $town_name2, $cEmailTemplate);
	
	$mail->ClearAddresses();
	if (empty($res_email)) $res_email = $contact_email;
	if (!empty($res_email)) $mail->SetFrom($res_email,"$establishment_name");
	$mail->AddAddress($_POST['email']);
	$mail->Subject = "Your Booking Enquiry via AA Travel Guides Website";
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!";
	$mail->MsgHTML($cEmailTemplate);
    $mail->Send();	
	
	//Assemble Variables
	if (!empty($restel)) $restel = "(<b>$restel</b>)";
	if (!empty($resfax)) $resfax = "(<b>$resfax</b>)";
	$name = strtolower(str_replace(' ', '_', $establishment_name));
	$name = str_replace(array("'",'(',')','"','=','+','[',']',',','/','\\'), '', $name);
	
	//Replace Tags
	$pageContent = str_replace('<!-- establishment_name -->', $establishment_name, $pageContent); 
	$pageContent = str_replace('<!-- tel -->', $restel, $pageContent); 
	$pageContent = str_replace('<!-- fax -->', $resfax, $pageContent); 
	$pageContent = str_replace('<!-- establishment_link -->', $name, $pageContent); 
	
	$GLOBALS['content'] = $pageContent;
}
?>