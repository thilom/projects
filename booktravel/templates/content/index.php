<?php 
//Initialize Variables
$breadcrumbs = '&nbsp;';
$page = '';
$page_title = 'AA Travel Guides Quality Assured Accommodation in South Africa';
$page_description = "South Africa accommodation - Accommodation South Africa. AA Travel Guides is the largest online travel database in South Africa for accommodation in hotels, B &amp; B's, guest houses, self catering chalets and apartments, lodges, game and nature reserves throughout South Africa; including Cape Town, the Garden Route, Knysna, George, Durban, Sun City, Sabi Sands,  and the Kruger National Park. We also cover Namibia, Zimbabwe, Botswana, Zambia, Mozambique Lesotho and Swaziland. Plan your ideal safari, visit the Cape Winelands, tour the Garden Route. Find out where to stay wherever you go in southern Africa";
$page_keywords = "south africa accommodation, accommodation, south africa, accommodation south africa, South African accommodation, sa accommodation, sa tourism";

//Includes & Setup
include_once 'aa_init/init.php';

//Get Template
$template = file_get_contents('templates/template1.tpl');

//Get page
if (isset($_GET['p'])) {
	$page = $_GET['p'];
	if (is_file($_SERVER['DOCUMENT_ROOT'] . "/content/$page.html")) {
		include 'page.php';
	} elseif ($_GET['p'] == 'est') {
		$content = '';
		include 'establishment.php';
	} elseif($_GET['p'] == 'search') {
		$content = '';
		include 'search.php';
	} elseif($_GET['p'] == 'asearch') {
		$content = '';
		include 'asearch.php';
	} elseif ($_GET['p'] == 'province') {
		$content = '';
		include 'breadcrumbs.php';
		include 'province.php';
	} elseif ($_GET['p'] == 'town') {
		$content = '';
		include 'town.php';
		include 'breadcrumbs.php';
	} elseif ($_GET['p'] == 'suburb') {
		$content = '';
		include 'breadcrumbs.php';
	} elseif ($_GET['p'] == 'region') {
		$content = '';
		include 'breadcrumbs.php';
	} elseif ($_GET['p'] == 'regiontown') {
		$content = '';
		include 'breadcrumbs.php';
	} elseif ($_GET['p'] == 'feedback') {
		$content = '';
		include 'feedback.php';
	} elseif ($_GET['p'] == 'enquiry') {
		$content = '';
		include 'enquire.php';
	} elseif ($_GET['p'] == 'subscription') {
		$content = '';
		include 'subscribe_thanks.php';
	} elseif ($_GET['p'] == 'order_process') {
		$content = '';
		include 'order_process.php';
	} else {
		$content = file_get_contents('content/home.html');
	}
} else {
	$content = file_get_contents('content/home.html');
}




//Replace Tags
$template = str_replace('<!-- body -->', $content, $template);
$template = str_replace('<!-- breadcrumbs -->', $breadcrumbs, $template);
$template = str_replace('<!-- page_title -->', $page_title, $template);
$template = str_replace('<!-- page_description -->', $page_description, $template);
$template = str_replace('<!-- page_keywords -->', $page_keywords, $template);

//Serve
echo $template;


?>