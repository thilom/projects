<?php
	//Initialise Variables
	$code = '';
	$error = FALSE;
	$primary_image = '';
	error_reporting(E_ALL);
	session_start();
	
	include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';
	
//	if (isset($_GET['reset'])) unset($_SESSION['count']);
//	
//	if (isset($_SESSION['count'])) {
//		$_SESSION['count']++;
//	} else {
//		$_SESSION['count'] = 0;
//	}
	
	//echo "Counter - " . $_SESSION['count'] . " (". $_SESSION['count'] * 40 .")<br>"; 
	
	//prepare statements
	$statement = "INSERT INTO nse_export_establishments (establishment_code) VALUES (?)";
	$sql_ins = $dbCon->prepare($statement);
	$statement = "UPDATE nse_export_establishments SET complete='Y' WHERE establishment_code=?";
	$sql_update = $dbCon->prepare($statement);
	
	//Check Establishment Temp Table
	$statement = "INSERT INTO nse_export_establishments (establishment_code) VALUES (?)"; 
	$sql_ins = $dbCon->prepare($statement);
	$statement = "SELECT count(*) FROM nse_export_establishments" ;
	$sql_establishments = $dbCon->prepare($statement);
	$sql_establishments->execute();
	$sql_establishments->bind_result($count);
	$sql_establishments->store_result();
	$sql_establishments->fetch();
	$sql_establishments->close();
	if ($count == 0) {
		$statement = "SELECT establishment_code FROM nse_establishment WHERE aa_estab='Y'";
		$sql_est = $dbCon->prepare($statement);
		$sql_est->execute();
		$sql_est->bind_result($e);
		$sql_est->store_result();
		while ($sql_est->fetch()) {
			$sql_ins->bind_param('s', $e);
			$sql_ins->execute();
		}
		$sql_est->close();
	} 
	
	//Get Establishments
	$statement = "SELECT establishment_code FROM nse_export_establishments WHERE complete IS NULL LIMIT 10" ;
	$sql_establishments = $dbCon->prepare($statement);
	$sql_establishments->execute();
	$sql_establishments->bind_result($code);
	$sql_establishments->store_result();
	$nCount = $sql_establishments->num_rows();
	while ($sql_establishments->fetch()) {
		export_establishment($code);
		$sql_update->bind_param('s', $code);
		$sql_update->execute(); 
	}
	$sql_establishments->close(); 
	
	if ($nCount < 10) {
		echo "END!";
	} else {
		echo "<script>document.location = document.location</script>";
	}
	
	function export_establishment($code) {
				global $dbCon;
				$primary_image = '';
				$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/template1.tpl');
				$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/templates/establishment.tpl');
			
				//Get Reservation Data
				$statement = "SELECT reservation_tel, reservation_fax, reservation_cell, reservation_email, reservation_postal1, reservation_postal2, reservation_postal3, reservation_postal_code FROM nse_establishment_reservation WHERE establishment_code=?";
				$pstat = $dbCon->prepare($statement);
				$pstat->bind_param('s', $code);
				$pstat->execute();
				$pstat->store_result();
				$pstat->bind_result($tel, $fax, $cell, $email, $postal1, $postal2, $postal3, $postal_code);
				$pstat->fetch();
				$pstat->close();
				
				//Get  Base Data
				$statement = "SELECT establishment_name, street_address_line1, street_address_line2, street_address_line3, postal_address_line1, postal_address_line2, postal_address_line3, postal_address_code, general_number, nightsbridge_bbid, prebook_bid FROM nse_establishment WHERE establishment_code=?";
				$pstat = $dbCon->prepare($statement);
				$pstat->bind_param('s', $code);
				$pstat->execute();
				$pstat->store_result();
				$pstat->bind_result($establishment_name, $street1, $street2, $street3, $sPostal1, $sPostal2, $sPostal3, $sPostalCode, $tel2, $nightsbridge_id, $prebook_id);
				$pstat->fetch();
				$pstat->close();
				
				//Get  & Assemble description data
				$short_description = '';
				$long_description = '';
				$statement = "SELECT establishment_description, description_type FROM nse_establishment_descriptions WHERE language_code='EN' && establishment_code=?";
				$pstat = $dbCon->prepare($statement);
				$pstat->bind_param('s', $code);
				$pstat->execute();
				$pstat->store_result();
				$pstat->bind_result($description, $type);
				while ($pstat->fetch()) {
					if ($type == 'short description') $short_description = nl2br($description);
					if ($type == 'long description') $long_description = nl2br($description);
					if ($type == 'cancellation policy') $cancellation_policy = nl2br($description);
					if ($type == 'child policy') $child_policy = nl2br($description);
				}
				$pstat->close();
				if (!empty($long_description)) {
					$description = $long_description;
				} else {
					$description = $short_description;
				}
				if (!empty($child_policy)) $description .= "<p><span style='color: #335da9; font-family: arial; font-weight: bold; font-size: 12pt'>Child Policy</span><br><span style='font-family: arial; font-size: 10pt'>$child_policy</span>";
				if (!empty($cancellation_policy)) $description .= "<p><span style='color: #335da9; font-family: arial; font-weight: bold; font-size: 12pt'>Cancellation Policy</span><br><span style='font-family: arial; font-size: 10pt'>$cancellation_policy</span>";
				
				//Get Pricing Data
				$statement = "SELECT price FROM nse_establishment_deprecated WHERE establishment_code=?";
				$sql_price = $dbCon->prepare($statement);
				$sql_price->bind_param('s', $code);
				$sql_price->execute();
				$sql_price->store_result();
				$sql_price->bind_result($pricing);
				$sql_price->fetch();
				
				//Get Icons
				$facilities = '';
				$counter = 0;
				$statement = "SELECT b.icon_url, c.icon_name, c.icon_description
											FROM nse_establishment_icon AS a
											JOIN nse_icon AS b ON a.icon_id=b.icon_id
											JOIN nse_icon_lang AS c ON a.icon_id=c.icon_id 
											WHERE a.establishment_code=? && c.language_code='EN'";
				$pstat = $dbCon->prepare($statement);
				$pstat->bind_param('s', $code);
				$pstat->execute();
				$pstat->store_result();
				$pstat->bind_result($icon_url, $icon_name, $icon_description);
				while ($pstat->fetch()) {
					$facilities .= "<img src='$icon_url' title='$icon_description'> ";
					$counter++;
					if ($counter == 16) $facilities .= "<br>";
				}
				$pstat->close();
				
				//Get Quality Assured Data
				$statement = "SELECT b.aa_category_icon 
											FROM nse_establishment AS a
											JOIN nse_aa_category AS b ON a.aa_category_code=b.aa_category_code
											WHERE establishment_code=?";
				$pstat = $dbCon->prepare($statement);
				echo mysqli_error($dbCon);
				$pstat->bind_param('s', $code);
				$pstat->execute();
				$pstat->store_result();
				$pstat->bind_result($qa_rating);
				$pstat->fetch();
				$pstat->close();
				
				
				//Get location Data
				$statement = "SELECT b.province_name, c.country_name, d.town_name, e.suburb_name, a.gps_latitude, a.gps_longitude
											FROM nse_establishment_location AS a
											LEFT JOIN nse_location_province AS b ON a.province_id=b.province_id
											LEFT JOIN nse_location_country_lang AS c ON a.country_id=c.country_id
											LEFT JOIN nse_location_town AS d ON a.town_id=d.town_id
											LEFT JOIN nse_location_suburb AS e ON a.suburb_id=e.suburb_id
											WHERE a.establishment_code=? && c.language_code='EN'";
				$pstat = $dbCon->prepare($statement);
				echo mysqli_error($dbCon);
				$pstat->bind_param('s', $code);
				$pstat->execute();
				$pstat->store_result();
				$pstat->bind_result($province, $country, $town, $suburb, $gps_latitude, $gps_longitude);
				$pstat->fetch();
				$pstat->close();
		
				//Get Restype
				$statement = "SELECT b.subcategory_name, d.category_code 
											FROM nse_establishment_restype AS a 
											JOIN nse_restype_subcategory_lang as b ON a.subcategory_id=b.subcategory_id
											JOIN nse_restype_subcategory_parent AS c ON a.subcategory_id=c.subcategory_id
											JOIN nse_restype_category AS d ON c.category_id=d.category_id
											WHERE establishment_code=?";
				$sql_restype = $dbCon->prepare($statement);
				$sql_restype->bind_param('s', $code);
				$sql_restype->execute();
				$sql_restype->bind_result($restype_name, $restype_top);
				$sql_restype->store_result();
				$sql_restype->fetch();
				$sql_restype->close();
				
				//Get Page Data such as keywords 
				$statement = "SELECT description, keywords, breadcrumb, dir_link FROM nse_export_data WHERE identifier=?";
				$sql_data = $dbCon->prepare($statement);
				$sql_data->bind_param('s', $restype_top);
				$sql_data->execute();
				$sql_data->bind_result($meta_description, $keywords, $breadcrumb, $dir_link);
				$sql_data->store_result();
				$sql_data->fetch();
				$sql_data->close();
				
				if (empty($meta_description)) {
					$meta_description = "South Africa accommodation - Accommodation South Africa. AA Travel Guides is the largest online travel database in South Africa for accommodation in hotels, B &amp; B's, guest houses, self catering chalets and apartments, lodges, game and nature reserves throughout South Africa; including Cape Town, the Garden Route, Knysna, George, Durban, Sun City, Sabi Sands,  and the Kruger National Park. We also cover Namibia, Zimbabwe, Botswana, Zambia, Mozambique Lesotho and Swaziland. Plan your ideal safari, visit the Cape Winelands, tour the Garden Route. Find out where to stay wherever you go in southern Africa";
					$keywords = "south africa accommodation, accommodation, south africa, accommodation south africa, South African accommodation, sa accommodation, sa tourism";
				}
				
				//Assemble META tags
				$meta_description = str_replace('<!-- country -->', " $country ", $meta_description);
				$meta_description = str_replace('<!-- town -->', "$town ", $meta_description);
				$meta_description = str_replace('Essential Travel Info', 'AA Travel Guides', $meta_description);
				$keywords = str_replace('<!-- country -->', " $country", $keywords);
				$keywords = str_replace('<!-- town_name -->', "$town ", $keywords); 
				$keywords = str_replace('<!-- suburb -->', "  ", $keywords);
				
				//Assemble street address
				$street = '';
				if (!empty($street1)) $street .= "<img src='/images/street.png' valign=bottom> $street1<br>";
				if (!empty($street2)) $street .= "<img src='/images/blank.png' valign=bottom> $street2<br>";
				if (!empty($street3)) $street .= "<img src='/images/blank.png' valign=bottom> $street3<br>";
				
				//Assemble postal address
				if (empty($postal1)) {
					$postal = '';
					if (!empty($sPostal1)) {
						if (substr($sPostal1, -1) == ',' || substr($sPostal1, -1) == ';') $sPostal1 = substr($sPostal1, 0, -1);
						$postal .= "<img src='/images/email.png' valign=bottom> $sPostal1<br>";
					}
					if (!empty($sPostal2)) {
						if (substr($sPostal2, -1) == ',' || substr($sPostal2, -1) == ';') $sPostal2 = substr($sPostal2, 0, -1);
						$postal .= "<img src='/images/blank.png' valign=bottom> $sPostal2<br>";
					}
					if (!empty($sPostal3)) {
						if (substr($sPostal3, -1) == ',' || substr($sPostal3, -1) == ';') $sPostal3 = substr($sPostal3, 0, -1);
						$postal .= "<img src='/images/blank.png' valign=bottom> $sPostal3<br>";
					}
					if (!empty($sPostalCode)) $postal .= "<img src='/images/blank.png' valign=bottom> $sPostalCode<br>";
				} else {
					$postal = '';
					if (!empty($postal1)) {
						if (substr($postal1, -1) == ',' || substr($postal1, -1) == ';') $postal1 = substr($postal1, 0, -1);
						$postal .= "<img src='/images/email.png' valign=bottom> $postal1<br>";
					}
					if (!empty($postal2)) {
						if (substr($postal2, -1) == ',' || substr($postal2, -1) == ';') $postal2 = substr($postal2, 0, -1);
						$postal .= "<img src='/images/blank.png' valign=bottom> $postal2<br>";
					}
					if (!empty($postal3)) {
						if (substr($postal3, -1) == ',' || substr($postal3, -1) == ';') $postal3 = substr($postal3, 0, -1);
						$postal .= "<img src='/images/blank.png' valign=bottom> $postal3<br>";
					}
					if (!empty($postal_code)) $postal .= "<img src='/images/blank.png' valign=bottom> $postal_code<br>";
				}
				
				//Get images
				$images = array();
				$thumb_block='';
				$block_width = 3;
				$block_height = 10;
				$image_counter = 0;
				$icnt = 0;
				$statement = "SELECT image_name, thumb_name, primary_image FROM nse_establishment_images WHERE establishment_code=?";
				$pstat = $dbCon->prepare($statement);
				$pstat->bind_param('s', $code);
				$pstat->execute();
				$pstat->store_result();
				$pstat->bind_result($image, $thumb, $prim);
				
				while ($pstat->fetch()) {
					if (!empty($prim)) $primary_image = $image;
					$thumbs[] = $thumb;		
					$images[] = $image;
				}
				$pstat->close();
				
				//for($y=0; $y<$block_height; $y++) {
					//if (isset($images[$image_counter])) {
					foreach ($images as $k=>$v) {
						if (is_file($_SERVER['DOCUMENT_ROOT'] . "/res_images/" . $v)) {
							$thumb_block .= "<a href=javascript:void(imgSwap('{$images[$k]}')) onfocus=blur() style=background-image:url(/res_images/{$thumbs[$k]})></a>";
							$icnt++;
						}
					}
					//$image_counter++;
				//}
			
				if ($icnt == 0) {
					echo "[0 Image]";
					//$content = str_replace(array("\r\n","\r","\n"), '#$%', $content);
					//$content = preg_replace("@<!-- image_block_start -->(.)*<!-- image_block_end -->@i", '', $content);
					//$content = str_replace('#$%', "\n", $content);
				}
				
				if ($icnt == 1) {
					echo "[1 Image]";
					//$content = str_replace("\n", 'tns', $content);
					$image_block2 = "<img src='/res_images/$primary_image' />";
					//$content = preg_replace("@<!-- image_block_start -->(.)*<!-- image_block_end -->@im", $image_block, $content);
					//$content = str_replace('###', "\n", $content);
				}
				
				//Assemble heading country & province
				if (!empty($province)) {
					$province_header = ", $province";
					$country_header = '';
				} elseif (!empty($country)) {
					$province_header = "";
					$country_header = ", $country";
				} else {
					$province_header = "";
					$country_header = '';
				}
				
				//Assemble tel /faxnumbers
				if(empty($tel) && !empty($tel2)) {
					$tel = $tel2;
					$tel2 = '';
				}
				if (substr($tel, -1) == ',' || substr($tel, -1) == ';') $tel = substr($tel, 0, -1);
				if (substr($fax, -1) == ',' || substr($fax, -1) == ';') $fax = substr($fax, 0, -1);
				if (substr($cell, -1) == ',' || substr($cell, -1) == ';') $cell = substr($cell, 0, -1);
				
				//Assemble booking links
				$booking_link = '';
				if (!empty($prebook_id)) $booking_link = "";
				if (!empty($nightsbridge_id)) $booking_link = "https://www.nightsbridge.co.za/bridge/Search?bbid=$nightsbridge_id&nbid=18";
				
				//Assemble Page Title
				$page_title = "$establishment_name - $restype_name in $suburb $town, $province, $country ";
				
				//Assemble Breadcrumbs
				$breadcrumbs = '';
				
				//Assemble GPS Coordinates
				if (empty($gps_latitude)) {
					$gps = '';
				} else {
					$gps_latitude = stripslashes($gps_latitude);
					$gps_longitude = stripslashes($gps_longitude);
					$gps_latitude = str_replace('<34', '"', $gps_latitude);
					$gps_longitude = str_replace('<35', '"', $gps_longitude);
					$gps = "<div style='font-family: arial; font-size: 10pt; padding: 0 10 10 10'>							
							<img src='/images/blank.png' valign=bottom>Latitude: $gps_latitude<br>
							<img src='/images/blank.png' valign=bottom>Longitude: $gps_longitude
						</div>";
				}
				
				//Assemble Location 
				if (!empty($suburb)) {
					$location = "$suburb, ";
				}
				$location = "$town, ";
				if (!empty($province)) {
					$location .= "$province, $country";
				} else {
					$location .= " $country";
				}
				
				//Replace tags
				//$content = str_replace("\n", '###', $content);
				if (isset($image_block2)) $content = preg_replace("@<!-- image_block_start -->(.)*<!-- image_block_end -->@im", $image_block2, $content);
				$content = str_replace('<!-- tel1 -->', $tel, $content);
				$content = str_replace('<!-- tel2 -->', $tel2, $content);
				$content = str_replace('<!-- email -->', $email, $content);
				$content = str_replace('<!-- cell -->', $cell, $content);
				$content = str_replace('<!-- fax -->', $fax, $content);
				$content = str_replace('<!-- postal -->', $postal, $content);
				$content = str_replace('<!-- street -->', $street, $content);
				$content = str_replace('<!-- booking_link -->', $booking_link, $content);
				$content = str_replace('<!-- establishment_name -->', $establishment_name, $content);
				$content = str_replace('<!-- location -->', $location, $content);
				$content = str_replace('<!-- primary_image -->', $primary_image, $content);
				$content = str_replace('<!-- thumb_block -->', $thumb_block, $content);
				$content = str_replace('<!-- description -->', $description, $content);
				$content = str_replace('<!-- facilities -->', $facilities, $content);
				$content = str_replace('<!-- pricing -->', $pricing, $content);
				$content = str_replace('<!-- qa_rating -->', $qa_rating, $content);
				$content = str_replace('<!-- code -->', $code, $content);
				$content = str_replace('<!-- gps -->', $gps, $content); 
				
				if (empty($tel)) $content = preg_replace('@<!-- tel1_start -->(.)*<!-- tel1_end -->@i', '&nbsp;<br>', $content);
				if (empty($tel2)) $content = preg_replace('@<!-- tel2_start -->(.)*<!-- tel2_end -->@i', '&nbsp;<br>', $content);
				if (empty($fax)) $content = preg_replace('@<!-- fax_start -->(.)*<!-- fax_end -->@i', '&nbsp;<br>', $content);
				if (empty($email)) $content = preg_replace('@<!-- email_start -->(.)*<!-- email_end -->@i', '', $content);
				if (empty($cell)) $content = preg_replace('@<!-- cell_start -->(.)*<!-- cell_end -->@i', '&nbsp;<br>', $content);
				if (empty($booking_link)) $content = preg_replace('@<!-- booking_start -->(.)*<!-- booking_end -->@i', ' ', $content);
		
				$template = str_replace('<!-- body -->', $content, $template);
				$template = str_replace('<!-- page_title -->', $page_title, $template);
				$template = str_replace('<!-- breadcrumbs -->', $breadcrumbs, $template);
				$template = str_replace('<!-- page_description -->', $meta_description, $template);
				$template = str_replace('<!-- page_keywords -->', $keywords, $template);
				
				//Export HTML
				$name = strtolower(str_replace(' ', '_', $establishment_name));
				$name = str_replace(array("'",'(',')','"','=','+','[',']',',','/','\\'), '', $name);
				$file_name = "/accommodation/$name.html";
				create_file($file_name, $template);
	}
	
function create_file($file, $content) {
		echo "<div style='font-weight: normal; color: black'><a href='$file'>$file</a>";
		$note = is_file($_SERVER['DOCUMENT_ROOT'] . $file)?'File Updated':'File Created';
		if (file_put_contents($_SERVER['DOCUMENT_ROOT'] . "$file", $content)) {
			echo " <span style='font-style: arial; font-size: 8pt; color: navy'>- $note</span>";
		} else {
			echo " <span style='font-style: arial; font-size: 8pt; color: red'>- Update Failed</span>";
		}
		echo "</div>";
}
?>