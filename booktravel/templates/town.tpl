<table width=100% cellspacing=0 cellpadding=0 border=0>
	<tr valign=top>
		<td>
			<table width=100% cellspacing=0 cellpadding=0>
				<tr>
					<td><h1 style='color: #0770b0'><!-- town_name -->, <!-- province_name --></h1><span class=est_body><!-- town_text --></span></td>
				</tr>
				<tr>
					<td>
						<table cellspacing=0 cellpadding=0 style='margin-top: 10px; '  width=100%>
							<tr>
								<td width=23px><img src='/images/towns_tl.gif'></td>
								<td nowrap style='background-image: url(/images/towns_tc.gif); color: yellow; font-weight: bold; font-family: arial; width: 100%'>Places To Stay And What To Do In <!-- town_name --> </td>
								<td align=right width=20px><img src='/images/towns_tr.gif'  width=20px></td>
							</tr>
							<tr>
								<td style='background-color: white'></td>
								<td style='background-color: white' nowrap><!-- page_links --></td>
								<td style='background-color: white'></td>
							</tr>
							<tr>
								<td><img src='/images/towns_bl.gif'></td>
								<td style='background-image: url(/images/towns_bc.gif)'></td>
								<td align=right ><img src='/images/towns_br.gif' width=20 height=18></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table width=100% cellspacing=0 cellpadding=0>
				<tr>
					<td>					
						<!-- borough_start -->
						<table cellspacing=0 cellpadding=0 style='margin-top: 70px; margin-left: 10px' width=100%>
							<tr>
								<td><img src='/images/towns_tl.gif'></td>
								<td nowrap style='background-image: url(/images/towns_tc.gif); color: yellow; font-weight: bold; font-family: arial'><!-- province_name --> Borough</td>
								<td><img src='/images/towns_tr.gif'></td>
							</tr>
							<tr>
								<td style='background-color: white'></td>
								<td style='background-color: white' nowrap><!-- borough_list --></td>
								<td style='background-color: white'></td>
							</tr>
							<tr>
								<td><img src='/images/towns_bl.gif'></td>
								<td style='background-image: url(/images/towns_bc.gif)'></td>
								<td><img src='/images/towns_br.gif'></td>
							</tr>
						</table>
						<!-- borough_end -->
						<table cellspacing=0 cellpadding=0 style='margin-top: 70px; margin-left: 10px' width=100%>
							<tr>
								<td width=23><img src='/images/towns_tl.gif'></td>
								<td nowrap style='background-image: url(/images/towns_tc.gif); color: yellow; font-weight: bold; font-family: arial'><!-- province_name --> Towns</td>
								<td><img src='/images/towns_tr.gif'></td>
							</tr>
							<tr>
								<td style='background-color: white'></td>
								<td style='background-color: white' nowrap><!-- town_list --></td>
								<td style='background-color: white'></td>
							</tr>
							<tr>
								<td><img src='/images/towns_bl.gif'></td>
								<td style='background-image: url(/images/towns_bc.gif)'></td>
								<td><img src='/images/towns_br.gif'></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>