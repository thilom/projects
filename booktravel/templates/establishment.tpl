<script type="text/javascript" src="/DatePicker/datetimepicker_css.js"></script>

<div id="AreaBodyHolder">

	<div align="center" class='hidden' id="GuestReview" onclick="javascript:closeGuestReview();">
		<div align="center" id="GuestReviewBody">
			<div id="GuestReviewHolder" style="margin-top:300px;">
				<div id="GuestReviewHeader">
					<div style="margin:3px;float:left;">360 Virtual Tour</div>
					<div onclick="javascript:closeGuestReview();" id="GuestReviewClose">Close X</div>
					<br style="clear:both;">
				</div>
				<div id="GuestReviewContent">
					<!-- 360_virtual_tour --><br />
					Click and drag on the image to look around.
				</div>
			</div>
		</div>
	</div>

	<div id="EstablishmentLocation"><!-- location_title --></div>
	<div id="EstablishmentHeader">
		<div id="EstablishmentTabs">
			<div id="EstablishmentTabGallery" onclick="javascript:estabTabClicked(this)" class='establishmentTab'>Photo<br />Gallery</div>
			<div id="EstablishmentTabMap" onclick="javascript:estabTabClicked(this)" class='establishmentTab'>Map &amp;<br />Directions</div>
			<div id="EstablishmentTabReviews" onclick="javascript:estabTabClicked(this)" class='establishmentTab'>Guest<br />Reviews</div>
			<div id="EstablishmentTabQuote" onclick="<!-- request_quote_tab -->" class='establishmentTab'>Request a Quote</div>
			<div id="EstablishmentTabRates" onclick="javascript:estabTabClicked(this)" class='establishmentTab'>Rates</div>
			<div id="EstablishmentTabInfo" onclick="javascript:estabTabClicked(this)" class='establishmentTab establishmentTabSelected'>Info</div>
		</div>
		<div class="establishmentName"><h1><!-- establishment_name --></h1></div>
		<div class="establishmentLocation"><!-- location --></div>
	</div>
	<div id="EstablishmentContentInfo" class='establishmentContent'>
		<div id="EstablishmentInfoGallery"><!-- info_gallery --></div>
		<div id="EstablishmentDescription">
			<div style="min-height:150px;"><!-- description --></p></div>
			<!-- bonsela -->
			<!-- specials -->
			<p><strong>Facilities & Activities</strong><br />
				<!-- facilities -->
			</p>
			<p style='color:red;'>Please note prices supplied are published in good faith but are subject to change.</p>
		</div>
		<div id='EstablishmentImageGallery' class='hidden'><!-- image_gallery --></div>
		<div id='EstablishmentReviewContent' class='hidden'>
			<br /><div align="right">Average Guest Rating <!-- smilies --></div>
			<div>Guest Reviews for <strong><!-- establishment_name --></strong></div><br />
			<!-- reviews -->
			<div align="center">
				<input onclick='document.location="/guest_review/<!-- establishment_code -->"' class="EstablishmentButton" type='button' value="Add a Guest Review" />
				<input class="EstablishmentButton" type='button' value="Back to Info Page" onclick='javascript:estabTabClicked(document.getElementById("EstablishmentTabInfo"));' />
			</div>
		</div>
		<div id="EstablishmentRates" class="hidden"><h2>Rates</h2><br />
			<!-- rates -->
			<p style='color:red;'>Please note prices supplied are published in good faith but are subject to change.</p>
		</div>
		<div id='EstablishmentQuoteContent' class='hidden'>
			<div id="EstablishmentQuoteThank" align="center" style="display:none"></div>
			<form id="EstablishmentQuoteForm" method="post" action="http://v2.api.booktravel.travel/postAAWebEnquiry.php" onsubmit="javascript:return submitQuote();">
				<input type='hidden' name="establishmentCode" value="<!-- establishment_code -->" />
				<input type='hidden' name="website" value="booktravel.travel" />
				<table class="EstablishmentBlueBlock" style="padding:20px;" align="center">
					<tr><td align="center" colspan="10"><!-- price_form --></td></tr>
					<tr><td align="center" colspan="10">&nbsp;</td></tr>
					<tr><td>Check in:</td><td>
						<input name="checkIn" id='CheckIn' readonly="yes" onclick='javascript:NewCssCal("CheckIn")' type='text' size="10" />
					</td><td width="100">&nbsp;</td><td>Check out:</td><td>
						<input name="checkOut" id='CheckOut' readonly="yes" onclick='javascript:NewCssCal("CheckOut")' type='text' size="10" />
					</td></tr>
				</table><br />
				<div align='center'>Complete your details and click "Submit" below to send an email directly to <strong><!-- establishment_name --></strong></div>
				<br />
				
				<table width="700" class="EstablishmentBlueBlock" style="padding:20px;" align="center">
					<tr>
						<td>Adults:</td><td><input name="adults" id='Adults' type='text' size="2" value="1" onchange="guestCount()" /></td>
						<td width="30">&nbsp;</td>
						<td>Children:</td><td><input name="children" id='Children' type='text' size="2" value="0" onchange="guestCount()" /></td>
						<td width="30">&nbsp;</td>
						<td>Total number of guests:</td><td><input name="totalGuests" id='TotalGuests' value="1" disabled="yes" type='text' size="2" /></td>
						<td width="30">&nbsp;</td>
						<td>Number of Rooms:</td><td><input name="totalrooms" id='TotalGuests' value="1" type='text' size="2" /></td>
					</tr>
				</table><br />
				<span style="font-size:16px;font-weight:bold;padding-left:100px;">Your info:</span>
				<table width="700" class="EstablishmentBlueBlock" style="padding:20px;" align="center">
					<tr>
						<td width="40">&nbsp;</td><td>First Name: <input name="firstName" id='FirstName' type='text' size="15" /></td><td width="90">&nbsp;</td>
						<td align="right">Email: <input name="email" id='Email' type='text' size="15" /></td><td width="40">&nbsp;</td>
					</tr>
					<tr>
						<td width="40">&nbsp;</td><td>Last Name: <input name="lastName" id='LastName' type='text' size="15" /></td><td width="90">&nbsp;</td>
						<td align="right">Confirm Email: <input id='ConfirmEmail' type='text' size="15" /></td><td width="40">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10" align="center">
							Tel: <input name="telNo" id='TelNo' type='text' size="15" />
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Cell: <input name="cellNo" id='CellNo' type='text' size="15" />	
						</td>
					</tr>
				</table><br />
				<span style="font-size:16px;font-weight:bold;padding-left:100px;">Optional info:</span>
				<table width="700" class="EstablishmentGreyBlock" style="padding:5px;" align="center">
					<tr>
						<td>Town: <input name="townName" id='TownName' type='text' size="15" /></td><td width="20">&nbsp;</td>
						<td>Country: <input name="countryName" id='CountryName' type='text' size="15" /></td><td width="20">&nbsp;</td>
						<td>Travelling by: <input name="travelBy" id='TravelBy' type='text' size="15" /></td>
					</tr>
					<tr>
						<td>Age Group: <input name="ageGroup" id='AgeGroup' type='text' size="2" /></td><td width="20">&nbsp;</td>
						<td>Gender: <input name="gender" id='Gender' type='text' size="15" /></td><td width="20">&nbsp;</td>
						<td>Occupation: <input name="occupation" id='Occupation' type='text' size="15" /></td>
					</tr>
					<tr>
						<td colspan="10" valign="top">Additional Comments:<br /><textarea name="comments" id='AdditionalComments' rows="5" cols="80"></textarea><input style="margin-right:-200px;position:absolute;z-index:-1;font-size:10px;" name="Stitle" id='Stitle' type='text' size="15" /></td>
					</tr>
				</table><br />
				<div align="center">
					<input name="marketing" type="checkbox" name="Marketing" /> I travel regularly and would like to receive updates from the AA Travel Info website via email.<br />
					<br />
					<!-- captcha -->
					<br /><input name="enquirySubmit" class="EstablishmentButton" type='submit' value="Submit" /><br /><br />
					An email will be sent directly to the establishment and a copy will be sent to you as well.
				</div>
			</form>
		</div>
		<div id="EstablishmentRightContent">
			<!-- nightsbridge_button -->
			<div id="EstablishmentPriceBlock" style="<!-- hide_quote_block -->">
				<table align="center">
					<tr><td colspan="2" align="center"><!-- price_quote --></td></tr>
					<tr><td width="60">Check-in:</td><td>
						<input id='RightCheckIn' readonly="yes" onclick='javascript:NewCssCal("RightCheckIn")' type='text' size="10" />
					</td></tr>
					<tr><td>Check-out:</td><td>
						<input id='RightCheckOut' readonly="yes" onclick='javascript:NewCssCal("RightCheckOut")' type='text' size="10" />
					</td></tr>
					<tr><td>Number of Guests:</td><td><input id='RightTotalGuests' type='text' size="2" /></td></tr>
					<tr><td colspan="2" align="center"><input class="rqButton" onclick='javascript:requestQuote();guestCount();' type='button' value="Request a Quote" /></td></tr>
					<tr><td colspan="2" align="center" class="rqBottom">(emails establishment directly)</td></tr>
				</table>
			</div>
			<div id='EstablishmentRightContact'>
				<!-- right_content -->
				<div align='center' style='margin-top:20px;'>
			    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- booktravel.travel/essentialtravelinfo.com right sky ad -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:160px;height:600px"
                     data-ad-client="ca-pub-4514882728543448"
                     data-ad-slot="6493604526"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script></div>
			</div>
		</div>
		<div id="EstablishmentMap"><!-- directions --></div>
	</div>
	
	<div id='EstHolder'>
		
		<br style='clear:both;' />
	
		
	
		<p></p>
		
	</div>
	<div > <!--id="EstRightHolder"-->
		
		<br style="clear:both;" />
	</div>
</div>

<br style='clear:both;' />
<script>
	if (document.getElementById('EstablishmentMap').innerHTML+"" == "") {
		document.getElementById('EstablishmentTabMap').style.display = 'none';
	}
	setTabFromURL();
	//alert(document.getElementById('EstablishmentMap').innerHTML);
</script>