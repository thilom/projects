<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src='/templates/template1.js'></script>
	<script src='/scripts/ajax.js?date=20130208_1'></script>
	<link rel="stylesheet" href="/templates/template001.css">
	<link rel="stylesheet" href="/templates/template002.css">

	<meta name="google-site-verification" content="mzidUi1oCNYNJj2np_a6txKfeGS-2jyQ0NZZMf9R7LI" />
	<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="/templates/template001ie.css" />
	<![endif]-->
	<title><!-- page_title --></title>
	<meta name="description" content="<!-- page_description -->" />
	<meta name="KEYWORDS" content="<!-- page_keywords -->" />
	<script src="/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
	<link href="/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
	<meta name="google-site-verification" content="EaS7zYahKGzwVgSqXhWzfk1p-Y3SgRhgMt3AU7LRqLA" />
	<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-2022906-5");
		pageTracker._trackPageview();
		} catch(err) {}
	</script>
</head>

<body align="center" onload="">
	<div id='AdvancedSearch' align="center" class="hidden">
		<div id='AdvancedSearchBody' align="center"><div id='AdvancedSearchHolder'>
			<div id='AdvancedSearchHeader'><div style="margin:3px;float:left;">Advanced Search</div><div id="AdvancedSearchClose" onclick="javascript:closeAdvancedSearch();">Close X</div><br style="clear:both;" /></div>
            
			
		</div></div>
	</div>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<div align="center">
	<!-- hide logo -->
		<div align="center" id="aaqalogo2"><a href="http://www.booktravel.travel/"><img src="/images/book_travel.jpg" alt="Book Travel Logo" border="0" /></a></div>
	<!-- end hide logo -->
	<div id="fullbar">
		<!-- hide all menu -->
		<div id="middlebar">
			<ul id="MenuBar1" class="MenuBarHorizontal">
			  <li><a href="http://www.booktravel.travel">Home</a></li>
			  <li><a class="MenuBarItemSubmenu" >Accommodation</a>
			      <ul>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/western_cape/index.html">Western Cape</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/kwazulu-natal/index.html">KwaZulu-Natal</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/gauteng/index.html">Gauteng</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/eastern_cape/index.html">Eastern Cape</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/mpumalanga/index.html">Mpumalanga</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/limpopo/index.html">Limpopo</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/north_west/index.html">North West</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/free_state/index.html">Free State</a></li>
			        <li><a href="/accommodation/accommodation/southern_africa/south_africa/northern_cape/index.html">Northern Cape</a></li>
			      </ul>
			  </li>
			  <li><a class="MenuBarItemSubmenu" href="#">Travel Information</a>
			      <ul>
			        <li><a href="/index.php?p=western_cape">Western Cape</a>
			            <ul>
			              <li><a href="/index.php?p=cape_town">Cape Town</a></li>
			              <li><a href="/index.php?p=greater_cape_town" class="alinks">Greater Cape Town</a></li>
			              <li><a href="/index.php?p=cape_winelands" class="alinks">Cape Winelands</a></li>
			              <li><a href="/index.php?p=garden_route" class="alinks">Cape Garden Route</a></li>
			              <li><a href="/index.php?p=klein_karoo" class="alinks">Klein Karoo</a></li>
			              <li><a href="/index.php?p=west_coast" class="alinks">Cape West Coast</a></li>
			              <li><a href="/index.php?p=overberg" class="alinks">Cape Overberg</a></li>
			              <li><a href="/index.php?p=central_karoo" class="alinks">Cape Central Karoo</a></li>
			            </ul>
			        </li>
			        <li><a class="MenuBarItemSubmenu" href="/index.php?p=gauteng">Gauteng</a>
			            <ul>
			              <li><a href="/index.php?p=johannesburg" class="alinks">Johannesburg</a></li>
			              <li><a href="/index.php?p=greater_johannesburg" class="alinks">Greater Johannesburg</a></li>
						  <li><a href="/index.php?p=pretoria">Pretoria</a></li>
			              <li><a href="/index.php?p=tshwane" class="alinks">Tshwane Metro</a></li>
			              <li><a href="/index.php?p=east_rand_ekurhuleni" class="alinks">Ekurhuleni (East Rand)</a></li>
			              <li><a href="/index.php?p=the_vaal_sedibeng" class="alinks">Sedibeng (Vaal)</a></li>
			              <li><a href="/index.php?p=west_rand" class="alinks">West Rand</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=kwazulu_natal">KwaZulu-Natal</a>
			            <ul>
						  <li><a href="/index.php?p=durban">Durban</a></li>
			              <li><a href="/index.php?p=greater_durban" class="alinks">Greater Durban</a></li>
			              <li><a href="/index.php?p=drakensberg_kzn" class="alinks">Drakensberg</a></li>
			              <li><a href="/index.php?p=zululand" class="alinks">Zululand</a></li>
			              <li><a href="/index.php?p=battlefields" class="alinks">The Battlefields</a></li>
			              <li><a href="/index.php?p=midlands" class="alinks">The Midlands</a></li>
			              <li><a href="/index.php?p=south_coast" class="alinks">South Coast</a></li>
			              <li><a href="/index.php?p=north_coast" class="alinks">North Coast</a></li>
			              <li><a href="/index.php?p=east_griqualand" class="alinks">East Griqualand</a></li>
			              <li><a href="/index.php?p=elephant_coast" class="alinks">The Elephant Coast</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=eastern_cape">Eastern Cape</a>
			            <ul>
			                  <li><a href="/index.php?p=port_elizabeth">Port Elizabeth</a></li>
			                  <li><a href="/index.php?p=addo_elephant_park_and_surrounds" class="alinks">Addo Elephant Park</a></li>
			                  <li><a href="/index.php?p=tsitsikamma_garden_route" class="alinks">Garden Route</a></li>
			                  <li><a href="/index.php?p=wild_coast" class="alinks">Wild Coast</a></li>
			                  <li><a href="/index.php?p=sunshine_coast" class="alinks">Sunshine Coast</a></li>
			                  <li><a href="/index.php?p=karoo_heartland" class="alinks">Karoo Heartland</a></li>
			                  <li><a href="/index.php?p=amathole" class="alinks">Amathole</a></li>
			                  <li><a href="/index.php?p=friendly_n6" class="alinks">Friendly N6</a></li>
			                  <li><a href="/index.php?p=frontier_country" class="alinks">Frontier Country</a></li>
			                  <li><a href="/index.php?p=kouga" class="alinks">Kouga</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=mpumalanga">Mpumalanga</a>
			            <ul>
			                  <li><a href="/index.php?p=kruger_park_mpumalanga" class="alinks">Kruger National Park</a></li>
			                  <li><a href="/index.php?p=the_panorama" class="alinks">The Panorama</a></li>
			                  <li><a href="/index.php?p=cosmos_country" class="alinks">Cosmos Country</a></li>
			                  <li><a href="/index.php?p=cultural_heartland" class="alinks">Cultural Heartland</a></li>
			                  <li><a href="/index.php?p=grass_and_wetlands" class="alinks">Grass and Wetlands</a></li>
			                  <li><a href="/index.php?p=highlands_meander" class="alinks">Highlands Meander</a></li>
			                  <li><a href="/index.php?p=lowveld_legogote" class="alinks">Lowveld Legogote</a></li>
			                  <li><a href="/index.php?p=wild_frontier" class="alinks">Wild Frontier</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=limpopo">Limpopo</a>
			            <ul>
			                  <li><a href="/index.php?p=kruger_park_limpopo" class="alinks">Kruger National Park</a></li>
			                  <li><a href="/index.php?p=waterberg" class="alinks">Waterberg</a></li>
			                  <li><a href="/index.php?p=mopani" class="alinks">Mopani</a></li>
			                  <li><a href="/index.php?p=sekhukhune" class="alinks">Sekhukhune</a></li>
			                  <li><a href="/index.php?p=vhembe" class="alinks">Vhembe</a></li>
			                  <li><a href="/index.php?p=capricorn" class="alinks">Capricorn</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=north_west">North West</a>
			            <ul>
			                  <li><a href="/index.php?p=central_north_west" class="alinks">Central Region</a></li>
			                  <li><a href="/index.php?p=southern_north_west" class="alinks">Southern Region</a></li>
			                  <li><a href="/index.php?p=bojanala" class="alinks">Bojanala</a></li>
			                  <li><a href="/index.php?p=bophirima" class="alinks">Bophirima</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=free_state">Free State</a>
			            <ul>
			                  <li><a href="/index.php?p=bloemfontein">Bloemfontein</a></li>
			                  <li><a href="/index.php?p=southern_free_state_xhariep" class="alinks">Xhariep</a></li>
			                  <li><a href="/index.php?p=northern_free_state" class="alinks">Northern Free State</a></li>
			                  <li><a href="/index.php?p=free_state_goldfields" class="alinks">Goldfields</a></li>
			                  <li><a href="/index.php?p=motheo" class="alinks">Motheo</a></li>
			                  <li><a href="/index.php?p=eastern_free_state" class="alinks">Eastern Free State</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=northern_cape">Northern Cape</a>
			            <ul>
			                  <li><a href="/index.php?p=diamond_fields" class="alinks">Diamond Fields</a></li>
			                  <li><a href="/index.php?p=namaqualand" class="alinks">Namakwa</a></li>
			                  <li><a href="/index.php?p=kalahari" class="alinks">Kalahari</a></li>
			                  <li><a href="/index.php?p=green_kalahari" class="alinks">Green Kalahari</a></li>
			                  <li><a href="/index.php?p=karoo" class="alinks">Karoo</a></li>
			            </ul>
			        </li>
			        <li><a href="/index.php?p=southern_africa">Southern Africa</a>
			            <ul>
			                  <li><a href="/index.php?p=botswana" class="alinks">Botswana</a></li>
			                  <li><a href="/index.php?p=mozambique" class="alinks">Mozambique</a></li>
			                  <li><a href="/index.php?p=swaziland" class="alinks">Kingdom of Swaziland</a></li>
			                  <li><a href="/index.php?p=lesotho" class="alinks">Lesotho</a></li>
			                  <li><a href="/index.php?p=namibia" class="alinks">Namibia</a></li>
			                  <li><a href="/index.php?p=zambia" class="alinks">Zambia</a></li>
			            </ul>
			        </li>
			      </ul>
                  <li><a class="MenuBarItemSubmenu" href="#">Routes</a>
                  <ul>
			        <li><a href="/index.php?p=routes">South Africa</a></li>
			  	  </ul>
              <li><a class="MenuBarItemSubmenu" href="/specials/south_africa/">Specials</a>
			      <ul>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/western_cape">Western Cape</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/kwazulu-natal">KwaZulu-Natal</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/gauteng">Gauteng</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/eastern_cape">Eastern Cape</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/mpumalanga">Mpumalanga</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/limpopo">Limpopo</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/north_west">North West</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/free_state">Free State</a></li>
			        <li><a href="/accommodation/specials/southern_africa/south_africa/northern_cape">Northern Cape</a></li>
			        <li><a href="/accommodation/specials/southern_africa">Southern Africa</a></li>
			      </ul>
			  </li>
			  <li><a href="http://www.booktravel.travel/index.php?p=nb">Real Time Bookings</a></li>
			  <li><a class="MenuBarItemSubmenu" href="/index.php?p=about_booktravel">About</a>
			      <ul>
			        <li><a href="/index.php?p=about_booktravel">About Us</a></li>
			        <li><a href="/index.php?p=contact">Contact Us</a></li>
			        <li><a href="/index.php?p=industry">All Products</a></li>
			        <li><a href="/index.php?p=travel_books">Travel Books</a></li>
			        <li><a href="/index.php?p=accom_awards">Accommodation Awards</a></li>
			        <li><a href="/index.php?p=map">Get Directions (Map)</a></li>
			        <li><a href="/index.php?p=map_regions">Map Regions</a></li>
                    <li><a href="http://www.vanessasand.com">CEO Blog</a></li>
			        <li><a href="http://booktravel.travel/index.php?p=reg_form">Join Now!</a></li>
			      </ul>
                  <li><a class="MenuBarItemSubmenu" href="/index.php?p=map_regions">Map</a></li>
              <li><a class="MenuBarItemSubmenu" href="http://www.essentialtravelinfo.com/index.php?p=car_hire">Car Hire</a></li>
              <li><a class="MenuBarItemSubmenu" href="http://www.essentialtravelinfo.com/index.php?p=flights">Flights</a></li>
			</ul>
			<!-- end hide all menu -->

			<!-- aaqa logo was here -->
	  </div>
		<!-- hide all menu -->
	</div>
	<!-- end hide ALL menu -->

	<div id="allheaderholder">
		<div id="adminlogin">
			<a href="http://clientzone.booktravel.travel" class="alinks">admin login</a> &nbsp;
			<a href="/index.php?p=press" class="alinks">Press</a> &nbsp; 
			<a href="/index.php?p=contact" class="alinks">Contact Us</a>
		</div>
		<div id="toplefttext">Deal direct! No commission on bookings!</div>
		
		<!-- start seach box -->
		<div id="searchbox1">
			<div align="left" style="margin-left:5px;" ><div id="LocationSearchBox" class='hidden'></div>
				<form action="#" onsubmit="javascript:loadPage('searchLocation='+document.getElementById('searchString').value);return false;" id="searchForm" method="post" >
					<div id="searchheight"><span class="searchtext" >Search by Location : </span><span class="searchtext2" >(eg. Durban, Cape Town, etc.) </span></div>
					<input type=text name=searchString id=searchString style='width: 330px; height: 25px; color: #000000; border: 0; font-size: 18px; vertical-align: middle; font-family: Arial, Helvetica, sans-serif; padding: 3px;' value='' >
					<input style="vertical-align:middle;" type=submit name=search value='Find!' />
				</form>
			</div>
			
			<div align="left" style="margin-left:5px; top:-50px;">
				<form  onsubmit="document.location='/search/'+encodeURI(document.getElementById('searchNameString').value);return false;" id="searchForm" method="post" >
					<div id="searchheight"><span class="searchtext" >Search by Name : </span><span class="searchtext2" ><span id='WebCode' class="hidden">Webcode as published in the printed ASATA Accommodation Directory.</span>(eg. Hotel, Lodge or <u  onmouseover="javascript:document.getElementById('WebCode').className='restypeDesc'" onmouseout="javascript:document.getElementById('WebCode').className='hidden'" style="cursor:pointer;">Webcode</u>.)</span></div>
					<!--  -->
					<input type=text name=searchString id=searchNameString style='width: 330px; height: 25px; color: #000000; border: 0; font-size: 18px; vertical-align: middle; font-family: Arial, Helvetica, sans-serif; padding: 3px;' value='' >
					<input style="vertical-align:middle;" type=submit name=search value='Find!' />
					<input type=hidden name=search_estab id=search_estab value='checked' />
				
				<div align="right" style="">
					<a href="/index.php?p=google_site_search" title="Search AAtravel.co.za using Google Site Search">try Google Site Search</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<a href='/asearch/' title='Click here to search using more options' onclick=''>try Advanced Search</a>
					<a href='/index.php?p=search_help' ><img src='/images/help.png' title='Click here for more information on the Accommodation search function' border=0 /></a>
				</div>
				</form>
			</div>
		</div>
		<!-- end search box -->

		<div id="welcomebox"><span class="paragraphtext">Quality Accommodation. Establishments listed on this site care about offering their guests a warm welcome in well maintained spotlessly clean surroundings.<br />
            <a href="http://essentialtravelinfo.com/index.php?p=reg_form" class="alinks" target="_blank">Get Listed on this website...</a></span><br />
            <a href='http://www.facebook.com/pages/Essential-Travel-Info/502385106443837?ref=hl' target="_blank"><img src='http://v2.api.booktravel.travel/FaceBook_48x48.png' width='48' height='48' /></a>
			<a href='https://twitter.com/estravelinfo' target="_blank"><img src='http://v2.api.booktravel.travel/Twitter_48x48.png' width='48' height='48' /></a>
		</div>
	<!-- end all header holder -->
	</div>
</div>




<div id="allreset"><div align="center">
	<div id="allcontentholder">
		<div id="bannerad">
			<div align="center" style="display: none">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- booktravel.travel right 1 -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:120px;height:240px"
                     data-ad-client="ca-pub-4514882728543448"
                     data-ad-slot="6981079320"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script> 
                <br style="clear:both" />
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- booktravel.travel right 2 -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:120px;height:240px"
                     data-ad-client="ca-pub-4514882728543448"
                     data-ad-slot="8457812527"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
			</div>
		</div>
		<div id="allcontent" align="center">
			<div id="provincebox" style="display:none">
				<div class="wc"><a href="/accommodation/accommodation/southern_africa/south_africa/western_cape/" class="wc">Western Cape</a></div>
			    <div class="ec"><a href="/accommodation/accommodation/southern_africa/south_africa/eastern_cape/" class="ec">Eastern Cape</a></div>
			    <div class="nc"><a href="/accommodation/accommodation/southern_africa/south_africa/northern_cape/" class="nc">Northern Cape</a></div>
			    <div class="kzn"><a href="/accommodation/accommodation/southern_africa/south_africa/kwazulu-natal/" class="kzn">KwaZulu-Natal</a></div>
			    <div class="gp"><a href="/accommodation/accommodation/southern_africa/south_africa/gauteng/" class="gp">Gauteng</a></div>
			    <div class="fs"><a href="/accommodation/accommodation/southern_africa/south_africa/free_state/" class="fs">Free State</a></div>
			    <div class="mp"><a href="/accommodation/accommodation/southern_africa/south_africa/mpumalanga/" class="mp">Mpumalanga</a></div>
			    <div class="lm"><a href="/accommodation/accommodation/southern_africa/south_africa/limpopo/" class="lm">Limpopo</a></div>
			    <div class="nw"><a href="/accommodation/accommodation/southern_africa/south_africa/north_west/" class="nw">North West</a></div>
			    <div class="sa"><a href="/accommodation/accommodation/southern_africa/" class="sa">Southern Africa</a></div>
			</div>
			<br style='clear:both;' />
			<div align="left" id="HeaderDrops" >
			<!-- body -->
			</div>
			
		</div>
		
	
		<!-- start of footer -->
      
      <hr>
		<div id="footer">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  		<tr>
		    		<td>
						<div class="footerblock" >
							<div class="contentrightheading"> Popular Holiday Destinations:
			  					<ul class="contentfooterlist">
		    						<li><a href="/accommodation/accommodation/southern_africa/south_africa/western_cape/cape_town/index.html" class="alinks">Cape Town</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/kwazulu-natal/durban/index.html" class="alinks">Durban</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/eastern_cape/port_elizabeth/index.html" class="alinks">Port Elizabeth</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/gauteng/johannesburg/index.html" class="alinks">Johannesburg</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/mpumalanga/kruger_national_park/index.html" class="alinks">Kruger National Park</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/kwazulu-natal/drakensberg/index.html" class="alinks">Drakensberg</a></li>
								</ul>
							</div>
						</div>
						<div class="footerblock" >
							<div class="contentrightheading"> Accommodation by Province:
			  					<ul class="contentfooterlist">
		      						<li><a href="/accommodation/accommodation/southern_africa/south_africa/western_cape/index.html" class="alinks">Western Cape</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/kwazulu-natal/" class="alinks">KwaZulu Natal</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/eastern_cape/" class="alinks">Eastern Cape</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/gauteng/" class="alinks">Gauteng</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/mpumalanga/" class="alinks">Mpumalanga</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/northern_cape/" class="alinks">Northern Cape</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/north_west/" class="alinks">North West</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/limpopo/" class="alinks">Limpopo</a></li>
		            				<li><a href="/accommodation/accommodation/southern_africa/south_africa/free_state/" class="alinks">Free State</a></li>
								</ul>
							</div>
						</div>
		
						<div class="footerblock" >
							<div class="contentrightheading">Travel Info by Province:
								<ul class="contentfooterlist"><li><a href="/index.php?p=western_cape" class="alinks">Western Cape</a></li>
									<li><a href="/index.php?p=kwazulu_natal" class="alinks">KwaZulu Natal</a></li>
									<li><a href="/index.php?p=eastern_cape" class="alinks">Eastern Cape</a></li>
									<li><a href="/index.php?p=gauteng" class="alinks">Gauteng</a></li>
									<li><a href="/index.php?p=mpumalanga" class="alinks">Mpumalanga</a></li>
									<li><a href="/index.php?p=northern_cape" class="alinks">Northern Cape</a></li>
									<li><a href="/index.php?p=north_west" class="alinks">North West</a></li>
									<li><a href="/index.php?p=limpopo" class="alinks">Limpopo</a></li>
									<li><a href="/index.php?p=free_state" class="alinks">Free State</a></li>
								</ul>
							</div>
						</div>
		
						<div class="footerblock" >
							<div class="contentrightheading">Accommodation Specials:
								<ul class="contentfooterlist">
									<li><a href="/specials/cape_town_western_cape/" class="alinks">Cape Town</a></li>
									<li><a href="/specials/kwazulu-natal_south_africa/" class="alinks">Kwa-Zulu Natal</a></li>
									<li><a href="/specials/knysna_western_cape/" class="alinks">Knysna</a></li>
									<li><a href="/specials/gauteng_south_africa/" class="alinks">Gauteng</a></li>
									<li><a href="/specials/mossel_bay_western_cape/" class="alinks">Mossel Bay</a></li>
									<li><a href="/specials/franschhoek_western_cape/" class="alinks">Franschhoek</a></li>
									<li><a href="/specials/south_africa/" class="alinks">View all specials...</a></li>
								</ul>
							</div>
						</div>
		
						<div class="footerblock" >
							<div class="contentrightheading"> Contact Book Travel:
								<ul class="contentfooterlist">
									<li><a href="/index.php?p=about_booktravel" class="alinks">About booktravel.travel</a></li>
									<li><a href="mailto:info@booktravel.travel">info@booktravel.travel</a></li>
									<li><a href="/index.php?p=contact" class="alinks">Full Contact Details</a></li>
								</ul>
							</div>
						</div>
		
						<!-- end footer holder -->
					</td>
		  		</tr>
			</table>
		</div>
		<!-- end of footer -->
	</div>
</div></div>

<script type="text/javascript">
<!--
//-->
</script>

</body>
</html>
