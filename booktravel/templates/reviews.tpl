
<h2>Review for <!-- establishment_name --></h2>

<table>
    <tr valign=top>
        <td nowrap>Average Guest Rating</td>
        <td width=90%><!-- average_rating --></td>
    </tr>
</table>
<p />
<!-- line_start -->
<table width=100% cellpadding=2 cellspacing=0 style='border-color: grey; border-width: 1 1 1 1; border-style: dotted'>
    <tr style='background-color: silver'>
        <td style='font-size: 11pt'>Review From <!-- name --></td>
        <td align=right style='font-size: 11pt'><!-- date --></td>
    </tr>
    <tr>
        <td colspan=2 style='font-size: 10pt'><!-- review --></td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan=2 style='font-size: 10pt; font-style: italic'><!-- comment --></td>
    </tr>
</table>
<br />
<!-- line_end -->
