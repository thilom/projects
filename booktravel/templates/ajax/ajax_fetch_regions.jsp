<?xml version="1.0" encoding="ISO-8859-2" ?>
<%@ page 
language="java"
import="java.io.*, java.util.*, com.eti.object.*, com.eti.util.*, za.co.aatravel.beans.Province"
%><%	 
	List regions = DataRetriever.fetchProvinces();
	
       // String xml = "<?xml version=\"1.0\" ?>";
	String xml = "<regions>";
	for (int i = 0; i < regions.size(); i++) 
	{
		Province tmp = (Province)regions.get(i);
		xml += "<region id='" + tmp.getProvinceId() + "'>" + tmp.getName() + "</region>";
	}
	xml += "</regions>";
	response.addHeader("Content-type", "text/xml");
        response.addHeader("Cache-control", "no-cache");
	out.print(xml);
%>
