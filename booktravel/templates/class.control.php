<?php class control {
	private $api;
	private $output;
	private $pageParam;
	private $config;
	private $debugSite = false;
	
	private $template;
	private $templateLinkBlock;
	private $searchString;
	
	private $listPage;
	
	private $pageTitle;
	
	function __construct() {
		global $api;
		global $output;
		global $pageParam;
		global $template;
		global $templateLinkBlock;
		global $searchString;
		global $config;
		$config = new config();
		$api = new api();
		
		if (isset($_POST["ajaxpost"])) {
		
			if(isset($_POST["getAdvSearch"])) {
				$pageNum = 1;
				$api->setMethod("fetch");
				$api->setType("aaweb_areas");
				$pageUrl = "";
				$urlSplit = explode("/",$_POST["getAdvSearch"]);
				for($i=0;$i<12;$i++) {
					$page = explode("_",$urlSplit[$i]);
					if ($page[0] == "page") {
						$pageNum = $page[1];
					} else {
						if ($urlSplit[$i] != "") {
							$pageUrl .= $urlSplit[$i]."/";
						}
					}
				}
				$api->addParam("url",trim($pageUrl,"/"));
				$areaXml = $api->getApiXML();
				$null = $this->ajaxGetRegionData($areaXml,$pageNum);
				$api->setMethod("fetch");
				$api->setType("advsearchfields");
				$advSearchXml = $api->getApiXML();
				$null = $this->ajaxAdvSearch($advSearchXml);
			}
			
			if(isset($_POST["getRegionData"])) {
				$pageNum = 1;
				$api->setMethod("fetch");
				$api->setType("aaweb_areas");
				$pageUrl = "";
				$urlSplit = explode("/",$_POST["getRegionData"]);
				for($i=0;$i<12;$i++) {
					$page = explode("_",$urlSplit[$i]);
					if ($page[0] == "page") {
						$pageNum = $page[1];
					} else {
						if ($urlSplit[$i] != "") {
							$pageUrl .= $urlSplit[$i]."/";
						}
					}
				}
				$api->addParam("url",trim($pageUrl,"/"));
				$areaXml = $api->getApiXML();
				echo $this->ajaxGetRegionData($areaXml,$pageNum);
			}
			if(isset($_POST["searchLocation"])) {
				$api->setMethod("fetch");
				$api->setType("websearch");
				$api->addParam("type","location");
				$api->addParam("search_string",$_POST["searchLocation"]);
				$searchXml = $api->getApiXML();
				echo $this->ajaxLocationSearch($searchXml);
			}
		} else {
			if (isset($_POST["searchString"])) {
				$_SESSION["searchString"] = $_POST["searchString"];
				header("location: /search/".urlencode($_POST["searchString"]));
			}
		
			$pageQuery = $_GET["page"];
			$pageParam = explode("/",$pageQuery);
			
			include('redirects.php');
			
			for($i=0;$i<13;$i++) {
				if (!isset($pageParam[$i])) {
					$pageParam[$i] = "";
				}
			}
			
			if ($pageParam[0] == "search") {
				$_SESSION["searchString"] = $pageParam[1];
			}
			
			if ($pageParam[0] == "asearch") {
				$_SESSION["asearch"] = $pageParam[1];
			}
			
			if (!isset($_SESSION["searchString"])) {
				$_SESSION["searchString"] = "";
			}
			$searchString = $_SESSION["searchString"];
			
			$template = file_get_contents("templates/template1.tpl");
			$templateGeneral = file_get_contents("templates/general_content.tpl");
			$templatelinkBlockStart = strpos($templateGeneral,"<!-- block_start -->");
			$templatelinkBlockEnd = strpos($templateGeneral,"<!-- block_end -->");
			$templateLinkBlock = substr($templateGeneral,$templatelinkBlockStart,($templatelinkBlockEnd-$templatelinkBlockStart));
			
			$this->getSearch();
			$this->getBreadCrumb($areaXml);
			$this->getBody();
			$this->getMeta();
			
			echo $template;
		}
	}
	
	function ajaxAdvSearch($advSearchXml) {
		$categories = "";
		$categories .= "<div><input onclick='javascript:advSearchQA=\"\"' type='radio' name='advCategory' value='' />All Quality Assurance</div>";
		foreach ($advSearchXml->aaCategories->category AS $category) {
			$categories .= "<div><input onclick='javascript:advSearchQA=\"".$category['code']."\"' type='radio' name='advCategory' value='".$category['code']."' />$category</div>";
		}
		echo "{%startDiv:AdvancedSearchQA%}$categories{%endDiv%}";
		
		$icons = "";
		
		foreach ($advSearchXml->iconCategory AS $category) {
			$icons .= "<fieldset class='advSearchFieldsetIcon'><legend>".$category['name']."</legend>";
			foreach ($category AS $icon) {
				$icons .= "<div onclick='javascript:advSearchIconClick(this,\"".$icon['id']."\");' class='advSearchIconOff'><div class='advSearchIconImg'><img align='absmiddle' src='".$icon['url']."' /></div><div class='advSearchIconText'>".$icon."</div></div>";
			}
			$icons .= "</fieldset>";
		}
		echo "{%startDiv:AdvancedSearchIcons%}$icons{%endDiv%}";
	}
	
	function ajaxLocationSearch($searchXml) {
		$list = "<span style='cursor:pointer;position:absolute;margin-left:383px;' onclick=\"document.getElementById('LocationSearchBox').className='hidden'\" style='cursor:pointer;position:absolute;margin-left:370px;' >X</span>";
		$list .= "<h3>Results for <em>".$_POST["searchLocation"]."</em></h3>";
		$log_params = "";
		foreach ($searchXml->search->location AS $location) {
			$list .= "<div align='right'><u><a href='/".$location['safe_url']."'>".$this->xmlStr($location)."</a></u></div>";
			$log_params .= $location['safe_url'].",";
		}
		$log_params = rtrim($log_params,",");
		$log_url = $_POST["searchLocation"];
		$log_type = "ls";
		$log_ip = $_SERVER["REMOTE_ADDR"];
		$this->webLog($log_params,$log_url,$log_type,$log_ip);
		
		$list = str_replace("__matchstart__", "<em><strong>", $list);
		$list = str_replace("__matchend__", "</strong></em>", $list);
		return "{%startDiv:LocationSearchBox%}$list{%endDiv:javascript:document.getElementById('LocationSearchBox').className='';%}";
	}
	
	function ajaxGetRegionData($areaXml,$page) {
		global $api;
		global $pageTitle;
	
		$preUrl = "accommodation/".$areaXml->theme["safe_url"];
		$preUrl .= "/".$areaXml->region["safe_url"];
		$preUrl = trim($preUrl,"/");
		
		$loader = "aaLoading()";
		
		$dropDowns = "";
		$dropCount = 0;
		
		if (isset($areaXml->countries->country)) {
			$list = "";
			$firstLink = "<strong>Select a country</strong>";
			foreach ($areaXml->countries->country AS $counrty) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$counrty["safe_url"]."\");advSearchLoc=\"/$preUrl/".$counrty["safe_url"]."\";return false;";
					//$onclick=str_replace("/",",",$onclick);
				}
				$listLink = "".$this->xmlStr($counrty)."";
				if ($counrty["id"]."" == $areaXml->country["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$counrty["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li><a onclick='$onclick' href='/$preUrl/".$counrty["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$counrty["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->country["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->provinces->province)) {
			$list = "";
			$firstLink = "<strong>Select a province</strong>";
			foreach ($areaXml->provinces->province AS $province) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$province["safe_url"]."\");advSearchLoc=\"/$preUrl/".$province["safe_url"]."\";return false;";
				}
				$listLink = "".$this->xmlStr($province)."";
				if ($province["id"]."" == $areaXml->province["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$province["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$province["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$province["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->province["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->province_regions->province_region)) {
			$list = "";
			$firstLink = "<strong>Select a region</strong>";
			foreach ($areaXml->province_regions->province_region AS $province_region) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$province_region["safe_url"]."\");advSearchLoc=\"/$preUrl/".$province_region["safe_url"]."\";return false;";
				}
				$listLink = "".$this->xmlStr($province_region)."";
				if ($province_region["id"]."" == $areaXml->province_region["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$province_region["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$province_region["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$province_region["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->province_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->sub_regions->sub_region)) {
			$list = "";
			$firstLink = "<strong>Select a sub-region</strong>";
			foreach ($areaXml->sub_regions->sub_region AS $sub_region) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$sub_region["safe_url"]."\");advSearchLoc=\"/$preUrl/".$sub_region["safe_url"]."\";return false;";
				}
				$listLink = "".$this->xmlStr($sub_region)."";
				if ($sub_region["id"]."" == $areaXml->sub_region["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$sub_region["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$sub_region["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$sub_region["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->sub_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->towns->town)) {
			$list = "";
			$firstLink = "<strong>Select a town</strong>";
			foreach ($areaXml->towns->town AS $town) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$town["safe_url"]."\");advSearchLoc=\"/$preUrl/".$town["safe_url"]."\";return false;";
				}
				$listLink = "".$this->xmlStr($town)."";
				if ($town["id"]."" == $areaXml->town["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$town["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$town["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$town["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$preUrl .= "/".$areaXml->town["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->town_regions->town_region)) {
			$list = "";
			$firstLink = "<strong>Select a town region</strong>";
			foreach ($areaXml->town_regions->town_region AS $town_region) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$town_region["safe_url"]."\");advSearchLoc=\"/$preUrl/".$town_region["safe_url"]."\";return false;";
				}
				$listLink = "".$this->xmlStr($town_region)."";
				if ($town_region["id"]."" == $areaXml->town_region["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$town_region["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li><a onclick='$onclick' href='/$preUrl/".$town_region["safe_url"]."'>".$listLink."</a></li>";
					// onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$town_region["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		$preUrl .= "/".$areaXml->town_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->suburbs->suburb)) {
			$list = "";
			$firstLink = "<strong>Select a suburb</strong>";
			foreach ($areaXml->suburbs->suburb AS $suburb) {
				$onclick = "";
				if(isset($_POST["getAdvSearch"])) {
					$onclick="javascript:loadPage(\"getAdvSearch=/$preUrl/".$suburb["safe_url"]."\");advSearchLoc=\"/$preUrl/".$suburb["safe_url"]."\";return false;";
				}
				$listLink = "".$this->xmlStr($suburb)."";
				if ($suburb["id"]."" == $areaXml->suburb["id"]."") {
					$firstLink = "<strong><a onclick='$onclick' href='/$preUrl/".$suburb["safe_url"]."'>".$listLink."</a></strong>";
				} else {
					$list .= "<li ><a onclick='$onclick' href='/$preUrl/".$suburb["safe_url"]."'>".$listLink."</a></li>";
					//onclick='javascript:loadPage(\"getRegionData=/$preUrl/".$suburb["safe_url"]."\");$loader;return false;'
				}
			}
			$dropCount++;
			$onclick = "javascript:if (document.getElementById(\"DropBox$dropCount\").className == \"ulHover\") {document.getElementById(\"DropBox$dropCount\").className = \"ulOut\";} else {document.getElementById(\"DropBox$dropCount\").className = \"ulHover\";}";
			$dropDowns .= "<div onclick='$onclick' class='dropAreaBlock'><ul class='ulOut' id='DropBox$dropCount'><li>$firstLink</li>$list</ul></div>";
		}
		
		$rdclass = $this->getProvinceClass($areaXml);
		
		$preUrl .= "/".$areaXml->suburb["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->themes->theme)) {
			if (isset($areaXml->suburb)) {$areaHeading = $areaXml->suburb;} else
			if (isset($areaXml->town_region)) {$areaHeading = $areaXml->town_region;} else
			if (isset($areaXml->town)) {$areaHeading = $areaXml->town;} else
			if (isset($areaXml->sub_region)) {$areaHeading = $areaXml->sub_region;} else
			if (isset($areaXml->province_region)) {$areaHeading = $areaXml->province_region;} else
			if (isset($areaXml->province)) {$areaHeading = $areaXml->province;} else
			if (isset($areaXml->country)) {$areaHeading = $areaXml->country;} else
			if (isset($areaXml->region)) {$areaHeading = $areaXml->region;}
			$themeList = "<div class='areaBlock'>";
			$themeList .= "<h1 class='$rdclass'>Accommodation in ".$this->xmlStr($areaHeading)."</h1><ul>";
			foreach ($areaXml->themes->theme AS $theme) {
				$themUrl = str_replace("/".$areaXml->theme["safe_url"]."/", "/".$theme["safe_url"]."/", $preUrl);
				$themeList .= "<li><a  href='/$themUrl'>".$this->xmlStr($theme)."</a> (".$theme["count"].")</li>";
				//onclick='javascript:loadPage(\"getRegionData=/$themUrl&changetheme=1\");$loader;return false;'
			}
			$themeList .= "</ul></div>";
		}
		
		$api->setMethod("fetch");
		$api->setType("aaweb_establist");
		if (isset($areaXml->theme)) {$api->addParam("theme_id",$areaXml->theme['id']);}
		if (isset($areaXml->region)) {$api->addParam("region_id",$areaXml->region['id']);}
		if (isset($areaXml->country)) {$api->addParam("country_id",$areaXml->country['id']);}
		if (isset($areaXml->province)) {$api->addParam("province_id",$areaXml->province['id']);}
		if (isset($areaXml->province_region)) {$api->addParam("pregion_id",$areaXml->province_region['id']);}
		if (isset($areaXml->sub_region)) {$api->addParam("sregion_id",$areaXml->sub_region['id']);}
		if (isset($areaXml->town)) {$api->addParam("town_id",$areaXml->town['id']);}
		if (isset($areaXml->town_region)) {$api->addParam("tregion_id",$areaXml->town_region['id']);}
		if (isset($areaXml->suburb)) {$api->addParam("suburb_id",$areaXml->suburb['id']);}
		//$pageSplit = explode("_",$page);
		$api->addParam("page",$page);
		$areaEstabListXml = $api->getApiXML();
		if (isset($_POST["ajaxpost"])) {
			$areaEstabList = $this->getEstList($areaEstabListXml,$areaXml,$preUrl."/page_".$page);
			$log_params = "";
			foreach ($areaEstabListXml->estlist->establishment AS $result) {
				$log_params .= $result["code"].",";
			}
			$log_params = rtrim($log_params,",");
			$log_url = "/".$preUrl."/page_".$page."/";
			$log_type = "l";
			$log_ip = $_SERVER["REMOTE_ADDR"];
			$this->webLog($log_params,$log_url,$log_type,$log_ip);
		}
		
		$rdclass = $this->getProvinceClass($areaXml);
		
		if (isset($_POST["ajaxpost"]) && !isset($_POST["getAdvSearch"])) {
			echo "{%startDiv:EstabListHolder%}$areaEstabList{%endDiv:javascript:document.location='#".$_POST['getRegionData']."';currentUrl='".$_POST['getRegionData']."';%}";
			$dropDowns = "{%startDiv:RegionDropDowns%}$dropDowns{%endDiv:javascript:document.getElementById('RegionDropDowns').className='$rdclass';document.title='$pageTitle';pageTracker._trackPageview('$preUrl');%}{%startDiv:AreaBlockHolder%}$themeList{%endDiv%}";
		} elseif (isset($_POST["getAdvSearch"])) {
			echo "{%startDiv:AdvancedSearchLocations%}$dropDowns{%endDiv%}'";
		}
		return $dropDowns;
	}
		
	function getEstList($apiResult,$areaXml,$url) {
		global $pageTitle;
		global $pageParam;
		
		//echo $url;
		
		$loader = "aaLoading()";
	
		$pageUrl = "";
		$urlSplit = explode("/",$url);
		for($i=0;$i<13;$i++) {
			$page = explode("_",$urlSplit[$i]);
			if ($page[0] == "page") {
			
			} else {
				if ($urlSplit[$i] != "") {
					$pageUrl .= $urlSplit[$i]."/";
					//echo $urlSplit[$i];
				}
			}
		}
		
		if (isset($apiResult->estlist->establishment) || true) {
			
			$pageHeader = "";
			$headerCount = 0;
			if (isset($areaXml->suburb)) {
				$pageHeader .= ", ".$this->xmlStr($areaXml->suburb);
				$headerCount++;
			}
			if (isset($areaXml->town_region)) {
				$pageHeader .= ", ".$this->xmlStr($areaXml->town_region);
				$headerCount++;
			}
			if (isset($areaXml->town)) {
				$pageHeader .= ", ".$this->xmlStr($areaXml->town);
				$headerCount++;
			}
			if (isset($areaXml->sub_region)) {
					$pageHeader .= ", ".$this->xmlStr($areaXml->sub_region);
					$headerCount++;
			}
			if (isset($areaXml->province_region)) {
					$pageHeader .= ", ".$this->xmlStr($areaXml->province_region);
					$headerCount++;
			}
			if (isset($areaXml->province)) {
				if ($headerCount < 5) {
					$pageHeader .= ", ".$this->xmlStr($areaXml->province);
					$headerCount++;
				}
			}
			if (isset($areaXml->country)) {
				if ($headerCount < 5) {
					$pageHeader .= ", ".$this->xmlStr($areaXml->country);
					$headerCount++;
				}
			}
			if (isset($areaXml->region)) {
				if ($headerCount < 5) {
					$pageHeader .= ", ".$this->xmlStr($areaXml->region);
					$headerCount++;
				}
			}
			$pageHeader = trim($pageHeader,", ");
			$breadcrumbs = $this->getBreadCrumb($areaXml);
			$rdclass = $this->getProvinceClass($areaXml);
			
			$estabUrl = $pageUrl;
			
			
			if($url == "") {
				$pageTitle = "Search results for ".$pageParam[1]." - AA Travel Guides";
				$pageUrl = $pageParam[0]."/".$pageParam[1]."/";
				$estabUrl = "accommodation/";
			} elseif (isset($areaXml->establishment)) {
				$pageTitle = $this->xmlStr($areaXml->establishment)." in $pageHeader - AA Travel Guides";
			} else {
				if ($areaXml->theme['safe_url'] == "bonsela") {
					$pageTitle = "Discount Accommodation in $pageHeader - Bonsela Programme";
				} else {
					$pageTitle = $this->xmlStr($areaXml->theme)." in $pageHeader - AA Travel Guides";
				}
			}
			
			if ($areaXml == "advsearch") {
				$pageTitle = "Advanced Search Results";
				$pageUrl = $pageParam[0]."/".$pageParam[1]."/".$pageParam[2]."/".$pageParam[3]."/";
				$estabUrl = "accommodation/";
				$log_params = "";
				foreach ($apiResult->estlist->establishment AS $result) {
					$log_params .= $result["code"].",";
				}
				$log_params = rtrim($log_params,",");
				$log_url = $_SERVER["REQUEST_URI"];
				$log_type = "as";
				$log_ip = $_SERVER["REMOTE_ADDR"];
				$this->webLog($log_params,$log_url,$log_type,$log_ip);
			}
			
			$estList = "<h1 class='estListHeader $rdclass'>$pageTitle</h1>";
			if($url == "") {
				$estList .= "<div id='SearchRight'><a href='/index.php?p=map_regions'><img src='/images/mini-map-right.png' /></a></div>";
			}
			$estList .= "<br /><br />$breadcrumbs";
			
			
			
			if ($apiResult->estlist->page > 1) {
				$paging .= "<a onclick='javascript:loadPage(\"getRegionData=$pageUrl"."page_".(($apiResult->estlist->page)-1)."\");$loader;return false;' href='/$pageUrl"."page_".(($apiResult->estlist->page)-1)."'>&lt;&lt;Previous</a> | ";
			}
			$paging .= $apiResult->estlist->page['totalresults']." Results found, Page ".$apiResult->estlist->page." of ".$apiResult->estlist->page['totalpages'];
			
			if ((($apiResult->estlist->page['totalpages'])+1) > (($apiResult->estlist->page)+1)) {
				$paging .= " | <a onclick='javascript:loadPage(\"getRegionData=$pageUrl"."page_".(($apiResult->estlist->page)+1)."\");$loader;return false;'  href='/$pageUrl"."page_".(($apiResult->estlist->page)+1)."'>Next &gt;&gt;</a>";
			}
			
			$estList .= "<div class='estListPaging'>$paging</div>";
			
			foreach ($apiResult->estlist->establishment AS $result) {
				if ($color == "EBEBEB") {
					$color = "ffffff";
				} else {
					$color = "EBEBEB";
				}
				
				$locationLinks = "";
				$theme_url = "places_to_stay";
				$region_url = "southern_africa";
				if(isset($areaXml->theme['safe_url'])) {
					$theme_url = $areaXml->theme['safe_url'];
				}
				if(isset($areaXml->region['safe_url'])) {
					$region_url = $areaXml->region['safe_url'];
				}
				$locationLinksUrl = "/accommodation/".$theme_url."/".$region_url;
				if (isset($result->country)) {
					$locationLinksUrl .= "/".$result->country["safe_url"];
					$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($result->country)."</a>, ".$locationLinks;
				}
				if (isset($result->province)) {
					$locationLinksUrl .= "/".$result->province["safe_url"];
					$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($result->province)."</a>, ".$locationLinks;
				}
				if (isset($result->province_region)) {
					$locationLinksUrl .= "/".$result->province_region["safe_url"];
					$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($result->province_region)."</a>, ".$locationLinks;
				}
				if (isset($result->sub_region)) {
					$locationLinksUrl .= "/".$result->sub_region["safe_url"];
					$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($result->sub_region)."</a>, ".$locationLinks;
				}
				if (isset($result->town)) {
					$locationLinksUrl .= "/".$result->town["safe_url"];
					$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($result->town)."</a>, ".$locationLinks;
				}
				if (isset($result->town_region)) {
					$locationLinksUrl .= "/".$result->town_region["safe_url"];
					$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($result->town_region)."</a>, ".$locationLinks;
				}
				if (isset($result->suburb)) {
					$locationLinksUrl .= "/".$result->suburb["safe_url"];
					$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($result->suburb)."</a>, ".$locationLinks;
				}
				
				$specials = "";
				if (isset($result->specials->special)) {
					foreach ($result->specials->special AS $special) {
						if (isset($special->start_date)) {
							$specials .= "<strong>From ".$this->xmlStr($special->start_date)." to ".$this->xmlStr($special->end_date)."</strong><br />".$this->xmlStr(utf8_encode($special->description))."<br /><br />";
						}
					}
				}
				
				$description = $this->xmlStr($result->description);
				
				if ($specials != "" && ($areaXml->theme['safe_url'] == "specials" || $areaXml->theme['safe_url'] == "bonsela")) {
					$description = $specials;
				}
				
				$locationLinks = trim($locationLinks,", ");
				
				$restype = $this->xmlStr($result->restype);
				
				//if ($this->xmlStr($result->restype_description) != "") {
					$restype = "<span class='hidden' id='restype_".$result["code"]."'>".$this->xmlStr($result->restype_description)."</span>
					<u style='cursor:pointer;' onmouseout='javascript:document.getElementById(\"restype_".$result["code"]."\").className=\"hidden\"' onmouseover='javascript:document.getElementById(\"restype_".$result["code"]."\").className=\"restypeDesc\"'>$restype</u>";
				//}
				$estSpecialsOverlay = "";
				if($result->special_type == "s") {$estSpecialsOverlay = "<img class='specList' src='/images/overlay_special.png' />";}
				if($result->special_type == "b") {$estSpecialsOverlay = "<img class='specList' src='/images/overlay_bonsela.png' />";}
				$estList .= "<div class='listEst' style='background-color:#$color;'>
					<div class='listEstDiv listEstImg'><div class='listQaText'>".$this->xmlStr($result->qa_status)." Accommodation</div><img class='qaListImg' src='/images/qalist.gif' />$estSpecialsOverlay<a href='$locationLinksUrl/".$result["code"]."_".$result->link_name."/' style='color:#0770B0;'><img width='150' src='http://booktravel.travel/image.php?e=".$result["code"]."&bg=$color' /></a></div>
					<div class='listEstDiv' style='width:580px;'>
						<h3><span class='est_name'><a href='$locationLinksUrl/".$result["code"]."_".$result->link_name."/' style='color:#0770B0;'>".$this->xmlStr($result->name)."</a></span></h3>
						<p class='est_body' style='margin:0;'><span class='searchQaText'>".$this->xmlStr($result->qa_status)." ".$restype.", ".$this->xmlStr($result->room_count)."
						<br />$locationLinks</p>
						<p class='est_body'>".$this->xmlStr($description)." <a href='$locationLinksUrl/".$result["code"]."_".$result->link_name."/' style='color:#0770B0;white-space: nowrap;'>Read More...</a></p>
						
					</div>
					<!-- <div class='listEstDiv' style='width:90px;'><p class='est_body'><br />".$this->xmlStr($result->price)."</p></div> -->";
					
				/*if ($result->qa_status != "") {
					$estList .= "<div class='listEstDiv' style='width:50px;'><p class='est_body'><br /><img src='/lookup_images/aatravel_50.gif'></p></div>";
					$estList .= "<div class='listEstDiv' style='width:100px;'><p class='est_body'><br />Quality Assured<br /><strong>".$this->xmlStr($result->qa_status)."</strong></p></div>";
				}*/
				$estList .= "<br style='clear:both;' /></div>";
			}
			$estList .= "<div class='estListPaging'>$paging</div>";
			return $estList;//.$blockAreas;
		}
	}
	
	function webLog($params,$url,$type,$ip) {
		global $config;
		$postFields = "";
		$postFields .= "params=".urlencode($params)."&url=".urlencode($url)."&type=".urlencode($type)."&ip=".urlencode($ip)."&agent=".urlencode($_SERVER["HTTP_USER_AGENT"]);
		
		if (
			strpos($_SERVER["HTTP_USER_AGENT"],"msnbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Baiduspider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Googlebot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"bingbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"psbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Sosospider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Exabot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Slurp") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Ezooms") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"msnbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"spbot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"YandexBot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"discobot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"ia_archiver") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Sogou web spider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"VB Project") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"findlinks") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"AhrefsBot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"MJ12bot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"informerabot") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"WordPress") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"libwww-perl/5") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"LYCOSA") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Speedy Spider") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"MSIE 999.1") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"Aghaven/Nutch-1.2") > -1
			|| strpos($_SERVER["HTTP_USER_AGENT"],"SemrushBot/0.9") > -1
			|| $_SERVER["HTTP_USER_AGENT"] == ''
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/4.0 (compatible; ICS)'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/4.0 (compatible'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/4.0'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Java/1.6.0_26'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Mozilla/5.0'
			|| $_SERVER["HTTP_USER_AGENT"] == 'Java/1.6.0_29'
		) {
			//DO NOT LOG
		} else {
			$curlAPI = curl_init($config->apiUrl."weblog.php");
	 		curl_setopt($curlAPI,CURLOPT_POST,1);
	 		curl_setopt($curlAPI,CURLOPT_POSTFIELDS,$postFields);
	 		curl_setopt($curlAPI,CURLOPT_HEADER,0);
	 		curl_setopt($curlAPI,CURLOPT_RETURNTRANSFER,1);
	 		$curlFetched = curl_exec($curlAPI);
	 		curl_close($curlAPI);
		}
	}
	
	function getSearch() {
		global $searchString;
		global $template;
		global $api;
		global $pageParam;
		
		$listPage = 1;
		
		for($i=1;$i<8;$i++) {
			$page = explode("_",$pageParam[$i]);
				
			if ($page[0] == "page") {
				$listPage = $page[1];
			}
		}
		
		if ($pageParam[0] == "search") {
			$api->setMethod("fetch");
			$api->setType("search");
			$api->addParam("keyword",$searchString);
			$api->addParam("page",$listPage);
			$searchXml = $api->getApiXML();
			$template = $this->setTemplate($template,"body",$this->getEstList($searchXml,"search",""));
			
			$log_params = "";
			foreach ($searchXml->estlist->establishment AS $result) {
				$log_params .= $result["code"].",";
			}
			$log_params = rtrim($log_params,",");
			$log_url = $_SERVER["REQUEST_URI"];
			$log_type = "s";
			$log_ip = $_SERVER["REMOTE_ADDR"];
			$this->webLog($log_params,$log_url,$log_type,$log_ip);
			
		}
		if ($pageParam[0] == "advsearch") {
			//$api->setMethod("fetch");
			//$api->setType("search");
			//$api->addParam("keyword",$searchString);
			//$api->addParam("page",$listPage);
			//$searchXml = $api->getApiXML();
			//$template = $this->setTemplate($template,"body",$this->getEstList($searchXml,"search",""));
			
			$pageUrl = trim($pageParam[1],",");
			$pageUrl = str_replace(",","/",$pageUrl);
			
			$page=1;
			
			$api->setMethod("fetch");
			$api->setType("aaweb_areas");
			$api->addParam("url",trim($pageUrl,""));
			$areaXml = $api->getApiXML();
			
			$api->setMethod("fetch");
			$api->setType("aaweb_establist");
			if (isset($areaXml->theme)) {$api->addParam("theme_id",$areaXml->theme['id']);}
			if (isset($areaXml->region)) {$api->addParam("region_id",$areaXml->region['id']);}
			if (isset($areaXml->country)) {$api->addParam("country_id",$areaXml->country['id']);}
			if (isset($areaXml->province)) {$api->addParam("province_id",$areaXml->province['id']);}
			if (isset($areaXml->province_region)) {$api->addParam("pregion_id",$areaXml->province_region['id']);}
			if (isset($areaXml->sub_region)) {$api->addParam("sregion_id",$areaXml->sub_region['id']);}
			if (isset($areaXml->town)) {$api->addParam("town_id",$areaXml->town['id']);}
			if (isset($areaXml->town_region)) {$api->addParam("tregion_id",$areaXml->town_region['id']);}
			if (isset($areaXml->suburb)) {$api->addParam("suburb_id",$areaXml->suburb['id']);}
			$api->addParam("aaqa_code",trim(str_replace("_","/",$pageParam[2])," "));
			$api->addParam("icon_ids",trim($pageParam[3],","));
			$page = explode("_",$pageParam[4]);
			
			//echo str_replace("_","/",$pageParam[2]);
			
			$api->addParam("page",$page[1]);
			$areaEstabListXml = $api->getApiXML();
			$areaEstabList = $this->getEstList($areaEstabListXml,"advsearch","");
			$template = $this->setTemplate($template,"body",$areaEstabList);
			
		}
	}
	
	function getMeta() {
		global $template;
		global $searchString;
		global $themeXml;
		global $suburbXml;
		global $townXml;
		global $provinceXml;
		global $countryXml;
		global $pageTitle;
		
		$template = $this->setTemplate($template,"search_string",$searchString);
		
		if ($pageTitle != "") {
			$template = $this->setTemplate($template,"page_title",$pageTitle);
		} else {
			$template = $this->setTemplate($template,"page_title","AA Travel Guides");
		}
		
		//$template = $this->setTemplate($template,"page_title","AA Travel Guides");
		$template = $this->setTemplate($template,"page_description","South Africa accommodation - Accommodation South Africa. AA Travel Guides is the largest online travel database in South Africa for accommodation in hotels, B &amp; B's, guest houses, self catering chalets and apartments, lodges, game and nature reserves throughout South Africa; including Cape Town, the Garden Route, Knysna, George, Durban, Sun City, Sabi Sands,  and the Kruger National Park. We also cover Namibia, Zimbabwe, Botswana, Zambia, Mozambique Lesotho and Swaziland. Plan your ideal safari, visit the Cape Winelands, tour the Garden Route. Find out where to stay wherever you go in southern Africa");
		$template = $this->setTemplate($template,"page_keywords","south africa accommodation, accommodation, south africa, accommodation south africa, South African accommodation, sa accommodation, sa tourism");
		
		
	}
	
	function getBreadCrumb($areaXml) {
		$preUrl = "accommodation/".$areaXml->theme["safe_url"];
		$preUrl .= "/".$areaXml->region["safe_url"];
		$preUrl = trim($preUrl,"/");
		
		$loader = "aaLoading()";
		
		$breadCrumbs = "";
		
		if (isset($areaXml->region)) {
			$breadCrumbs .= $this->createBreadcrumb("/","AA Travel Guides");
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->theme));
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->region));
		}
		
		$preUrl .= "/".$areaXml->country["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->country)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->country));
		}
		
		$preUrl .= "/".$areaXml->province["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->province)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->province));
		}
		
		$preUrl .= "/".$areaXml->province_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->province_region)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->province_region));
		}
		
		$preUrl .= "/".$areaXml->sub_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->sub_region)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->sub_region));
		}
		
		$preUrl .= "/".$areaXml->town["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->town)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->town));
		}
		$preUrl .= "/".$areaXml->town_region["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->town_region)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->town_region));
		}
		$preUrl .= "/".$areaXml->suburb["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->suburb)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->suburb));
		}
		$preUrl .= "/".$areaXml->establishment['code']."_".$areaXml->establishment["safe_url"];
		$preUrl = trim($preUrl,"/");
		if (isset($areaXml->establishment)) {
			$breadCrumbs .= $this->createBreadcrumb("/$preUrl",$this->xmlStr($areaXml->establishment));
		}
		return trim($breadCrumbs," &#187; ");
	}
	
	function createBreadcrumb($url,$title) {
		return "<div style='display:inline;' itemscope itemtype='http://data-vocabulary.org/Breadcrumb'><a href='$url' itemprop='url'><span itemprop='title'>$title</span></a></div> &#187; ";
	}
	
	function getEstablishment($establishment,$areaXml) {
		global $pageParam;
		global $api;
		global $template;
		global $showEstab;
		global $pageTitle;
		
		$establishment_template = "";
		
		$rightContent = "";
			
			$establishment_code = $establishment['code'];
			
			$api->setMethod("fetch");
			$api->setType("establishment");
			$api->addParam("code",$establishment['code']);
			$establishmentXml = $api->getApiXML();
			
			if (isset($establishmentXml->establishment)) {
				$showEstab = true;
				if ($establishmentXml->establishment == "false") {
					$showEstab = false;
					header("HTTP/1.0 404 Not Found");
					$establishment_template = "<div id='EstHolder'><h1>404 - Establishment not found</h1><br /><br /><br />Sorry but the establishment you are looking for does not exist in our database<div style='margin:50px;' align='center'><strong><a href='http://booktravel.travel/'>Back to Home Page</a></strong></div></div>";
				} else {
					$establishment_template = file_get_contents("templates/establishment.tpl");
					
					//Set Template Est Location
						
						$suburb = $this->xmlStr($establishmentXml->establishment->suburb);
						if (!empty($suburb)) $location = "$suburb, ";
						$town = $this->xmlStr($establishmentXml->establishment->town);
						$location .= "$town, ";
						$province = $this->xmlStr($establishmentXml->establishment->province);
						$country = $this->xmlStr($establishmentXml->establishment->country);
						if (!empty($province)) {
							$location .= "$province, $country";
						} else {
							$location .= " $country";
						}
						
						//echo $establishmentXml->establishment->locations->country;
						
						$locationLinks = "";
						$locationLinksUrl = "/accommodation/".$areaXml->theme['safe_url']."/".$this->xmlStr($establishmentXml->establishment->locations->country_region["safe_url"]);
						if (isset($establishmentXml->establishment->locations->country)) {
							$locationLinksUrl .= "/".$this->xmlStr($establishmentXml->establishment->locations->country["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($establishmentXml->establishment->locations->country)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->province)) {
							$locationLinksUrl .= "/".$this->xmlStr($establishmentXml->establishment->locations->province["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($establishmentXml->establishment->locations->province)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->province_region)) {
							$locationLinksUrl .= "/".$this->xmlStr($establishmentXml->establishment->locations->province_region["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($establishmentXml->establishment->locations->province_region)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->sub_region)) {
							$locationLinksUrl .= "/".$this->xmlStr($establishmentXml->establishment->locations->sub_region["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($establishmentXml->establishment->locations->sub_region)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->town)) {
							$locationLinksUrl .= "/".$this->xmlStr($establishmentXml->establishment->locations->town["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($establishmentXml->establishment->locations->town)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->town_region)) {
							$locationLinksUrl .= "/".$this->xmlStr($establishmentXml->establishment->locations->town_region["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($establishmentXml->establishment->locations->town_region)."</a>, ".$locationLinks;
						}
						if (isset($establishmentXml->establishment->locations->suburb)) {
							$locationLinksUrl .= "/".$this->xmlStr($establishmentXml->establishment->locations->suburb["safe_url"]);
							$locationLinks = "<a href='$locationLinksUrl'>".$this->xmlStr($establishmentXml->establishment->locations->suburb)."</a>, ".$locationLinks;
						}
						$locationLinks = trim($locationLinks,", ");
						
						$newLink = $locationLinksUrl."/".$establishmentXml->establishment["code"]."_".$this->xmlStr($establishmentXml->establishment->link_name);
						//$newLink = "/accommodation/".$establishmentXml->establishment["code"]."_".$this->xmlStr($establishmentXml->establishment->link_name);
						$actualLink = "/".$_GET['page'];
						
						//echo "$newLink<br />$actualLink";
						
						if ($newLink != $actualLink) {
							header('HTTP/1.1 301 Moved Permanently');
							header("Location: $newLink");
							exit;
						}
						
						$establishment_template = $this->setTemplate($establishment_template,"location",$locationLinks);
						
						//Establishment Log
					
						$log_params = $establishment['code'];
						$log_url = $_SERVER["REQUEST_URI"];
						$log_type = "e";
						$log_ip = $_SERVER["REMOTE_ADDR"];
						$this->webLog($log_params,$log_url,$log_type,$log_ip);
					
					
					$pageHeader = "";
					$headerCount = 0;
					if (isset($areaXml->suburb)) {
						$pageHeader .= ", ".$this->xmlStr($areaXml->suburb);
						$headerCount++;
					}
					if (isset($areaXml->town_region)) {
						$pageHeader .= ", ".$this->xmlStr($areaXml->town_region);
						$headerCount++;
					}
					if (isset($areaXml->town)) {
						$pageHeader .= ", ".$this->xmlStr($areaXml->town);
						$headerCount++;
					}
					if (isset($areaXml->sub_region)) {
							$pageHeader .= ", ".$this->xmlStr($areaXml->sub_region);
							$headerCount++;
					}
					if (isset($areaXml->province_region)) {
							$pageHeader .= ", ".$this->xmlStr($areaXml->province_region);
							$headerCount++;
					}
					if (isset($areaXml->province)) {
						if ($headerCount < 5) {
							$pageHeader .= ", ".$this->xmlStr($areaXml->province);
							$headerCount++;
						}
					}
					if (isset($areaXml->country)) {
						if ($headerCount < 5) {
							$pageHeader .= ", ".$this->xmlStr($areaXml->country);
							$headerCount++;
						}
					}
					if (isset($areaXml->region)) {
						if ($headerCount < 5) {
							$pageHeader .= ", ".$this->xmlStr($areaXml->region);
							$headerCount++;
						}
					}
					$pageHeader = trim($pageHeader,", ");
					
					
					//Set Template Est Name
						$establishment_name = $this->xmlStr($establishmentXml->establishment->establishment_name);
						$rdclass = $this->getProvinceClass($areaXml);
						$establishment_template = $this->setTemplate($establishment_template,"location_title","<h1 class='$rdclass'>".$pageHeader."</h1>");
						$establishment_template = $this->setTemplate($establishment_template,"establishment_name",$establishment_name);
						$pageTitle = $establishment_name." - AA Travel Guides";
					
					//Set Template QA Status
						$qa_rating = $this->xmlStr($establishmentXml->establishment->qa_status);
						if (isset($qa_rating) && !empty($qa_rating)) {
							$qa_block .= "<a style='border:0;background:0;float:none;' target='_blank' href='http://booktravel.travel/index.php?p=quality_assured'><img align='left' src='http://booktravel.travel/lookup_images/$qa_rating' width=97 height=148 /></a>";
						} else {
							$qa_block .= '';
						}
					
					//Set Template Est Restype
						$restype = $this->xmlStr($establishmentXml->establishment->restype_name);
						$qa_name = $this->xmlStr($establishmentXml->establishment->qa_name);
					
					//Set Template Est Descriptions
						$short_description = $this->xmlStr($establishmentXml->establishment->short_description);
						if (!empty($establishmentXml->establishment->long_description)) {
							$description = $this->xmlStr($establishmentXml->establishment->long_description);
						} else {
							$description = $this->xmlStr($establishmentXml->establishment->short_description);
						}
						$description = trim($description,"<br />");
						$description = "<p>$qa_block <strong>$qa_name $restype</strong><br />$description</p>";
						if (!empty($establishmentXml->establishment->child_policy)) {
							$child_policy = $this->xmlStr($establishmentXml->establishment->child_policy);
							$description .= "<p><strong>Child Policy</strong><br />$child_policy</p>";
						}
						if (!empty($establishmentXml->establishment->cancellation_policy)) {
							$cancellation_policy = $this->xmlStr($establishmentXml->establishment->cancellation_policy);
							$description .= "<p><strong>Cancellation Policy</strong><br />$cancellation_policy</p>";
						}
						$establishment_template = $this->setTemplate($establishment_template,"description",$description);
						
					//Set Landmarks
						$landmarks = "";
						$isLandmark = false;
						foreach ($establishmentXml->establishment->landmarks->landmark AS $landmark) {
							$isLandmark = true;
							$landmarkFile = $this->contentFileFromName($this->xmlStr($landmark->name));
							if (file_exists('content/'.$landmarkFile.".html")) {
								$landmarks .= "<li><a target='_blank' href='/index.php?p=".$landmarkFile."'>".$this->xmlStr($landmark->name)."</a>";
							} else {
								$landmarks .= "<li>".$this->xmlStr($landmark->name)."";
							}
							if ($landmark->distance != "0" && $landmark->distance != "") {
								$landmarks .= " (".$this->xmlStr($landmark->distance.$landmark->measurement).")";
							}
							$landmarks .= "</li>";
						}
						
					//Get Specials
						$specials = "";
						$bonsela = "";
						foreach ($establishmentXml->establishment->specials->special AS $special) {
							if (isset($special->start_date) && $special->bonsela == "0") { 
								$specials .= "<strong>From ".$this->xmlStr($special->start_date)." to ".$this->xmlStr($special->end_date)."</strong><br />".$this->xmlStr($special->description)."<br /><br />";
							}
							if (isset($special->start_date) && $special->bonsela == "1") { 
								$bonsela .= "<strong>From ".$this->xmlStr($special->start_date)." to ".$this->xmlStr($special->end_date)."</strong><br />".$this->xmlStr($special->description)."<br /><br />";
							}
						}
						if ($bonsela != "") {
							$establishment_template = $this->setTemplate($establishment_template,"bonsela","<p></p><div class='estSpecialBonsela'><div class='estHeading'>Bonsela Discount</div><a href='/bonsela'>More info about the Bonsela Discount Programme</a><br /><br />$bonsela</div>");
						}
						if ($specials != "") {
							$establishment_template = $this->setTemplate($establishment_template,"specials","<p></p><div class='estSpecial'><div class='estHeading'>Specials</div>$specials</div>");
						}
						
					//Set 360
						$link360 = "";
						$content360 = "";
						if ($establishmentXml->establishment->threesixtyid != "" && $establishmentXml->establishment->threesixtyserver > 0) {
							$link360 = "<br /><a href='javascript:openGuestReview(\"\");' rel='lightbox'><img src='/images/360.jpg' /></a>";
							if ($establishmentXml->establishment->threesixtyserver == 2) {
								$link = urldecode($establishmentXml->establishment->threesixtyid);
								$content360 = "<iframe scrolling='no' frameborder='0' style='height:350px;width:600px;' src='http://360-sa.co.za/vtours/".$link."'></iframe>";
							}
							if ($establishmentXml->establishment->threesixtyserver == 1) {
								$link = urldecode($establishmentXml->establishment->threesixtyid);
								$content360 = "<iframe scrolling='no' frameborder='0' style='height:350px;width:600px;' src='http://www.govisit.co.za/home/embed-360/?lookup=".$link."&snap=center&snapwidth=600&snapheight=400'></iframe>";
							}
						}
						$establishment_template = $this->setTemplate($establishment_template,"360_virtual_tour",$content360);
						//echo $establishmentXml->establishment->threesixtyid." ".$establishmentXml->establishment->threesixtyserver;
					//http://360-sa.co.za/vtours/wild-olive-guest-house/
					//http://www.govisit.co.za/home/embed-360/?lookup=320&snap=center&snapwidth=600&snapheight=400
						
					//Set Template Est Images
						$imageInfoGallery = "";
						$imageGallery = "<table><tr>";
						$priOver = "";
						if ($specials != "") {
							$priOver = "<img id='EstablishmentSpecialOverlay' src='/images/overlay_special.png' />";
						}
						if ($bonsela != "") {
							$priOver = "<img id='EstablishmentSpecialOverlay' src='/images/overlay_bonsela.png' />";
						}
						$i=0;
						foreach ($establishmentXml->establishment->images->image AS $imageXml) {
							if ($i<6) {
								$imageInfoGallery .= "<div class='establishmentImage'>";
								if($i==0){$imageInfoGallery .= $priOver;}
								$imageInfoGallery .= "<img style='cursor:pointer;' onclick='javascript:estabTabClicked(document.getElementById(\"EstablishmentTabGallery\"));' width='300' src='/res_images/".$this->xmlStr($imageXml->name)."' />";
								if($i==0){$imageInfoGallery .= "<div id='EstablishmentQAOverlay'>$qa_name Accommodation</div>";}
								$imageInfoGallery .= "</div><div class='establishmentImageDesc'>$image_title<br /><p>$image_description</p></div><br style='clear:both' />";
							}
							if ($i==0) {
								$imageInfoGallery .= $link360;
							}
							$i++;
						}
						$iii=0;
						$i=0;
						foreach ($establishmentXml->establishment->images->image AS $imageXml) {
							if ($iii%3 == 0) {
								$imageGallery .= "</tr><tr>";
							}
							$iii++;
							$imageGallery .= "<td valign='top'><div class='establishmentImage'>";
							if($i==0){$imageGallery .= $priOver;}
							$imageGallery .= "<img width='300' src='/res_images/".$this->xmlStr($imageXml->name)."' />";
							if($i==0){$imageGallery .= "<div id='EstablishmentQAOverlay'>$qa_name Accommodation</div>";}
							$imageGallery .= "</div><div class='establishmentImageDesc'>$image_title<br /><p>$image_description</p></div><br style='clear:both' /></td>";
							$i++;
						}
						$imageGallery .= "</table>";
						$establishment_template=str_replace("<!-- info_gallery -->",$imageInfoGallery,$establishment_template);
						$establishment_template=str_replace("<!-- image_gallery -->",$imageGallery,$establishment_template);
						
					//Set Template Est Contact Data
						$establishment_tel = "";
						$establishment_fax = "";
						$establishment_cell = "";
						
						$contact_details = "";
						$contact_address = "";
						$contact_gps = "";
						
						$contact_tel = $this->xmlStr($establishmentXml->establishment->contact_tel);
						$base_tel = $this->xmlStr($establishmentXml->establishment->general_number);
						$reservation_tel = $this->xmlStr($establishmentXml->establishment->reservation_tel);
						$contact_fax = $this->xmlStr($establishmentXml->establishment->contact_fax);
						$reservation_fax = $this->xmlStr($establishmentXml->establishment->reservation_fax);
						$reservation_cell = $this->xmlStr($establishmentXml->establishment->reservation_cell);
						$reservation_email = $this->xmlStr($establishmentXml->establishment->reservation_email);
						
						$establishment_tel = !empty($contact_tel)?$contact_tel:$establishment_tel;
						$establishment_tel = !empty($base_tel)?$base_tel:$establishment_tel;
						$establishment_tel = !empty($reservation_tel)?$reservation_tel:$establishment_tel;
						$establishment_fax = !empty($contact_fax)?$contact_fax:$establishment_fax;
						
						if (substr($establishment_tel, 0, 4) == '&fax') {
							$establishment_fax = substr($establishment_tel, 5);
							$establishment_tel = substr($establishment_tel, 5);
						}
						$establishment_fax = !empty($reservation_fax)?$reservation_fax:$establishment_fax;
						$establishment_cell = !empty($contact_cell)?$contact_cell:$establishment_cell;
						$establishment_cell = !empty($reservation_cell)?$reservation_cell:$establishment_cell;
						if (substr($establishment_tel, -1) == ',' || substr($establishment_tel, -1) == ';') $establishment_tel = substr($establishment_tel, 0, -1);
						if (substr($establishment_fax, -1) == ',' || substr($establishment_fax, -1) == ';') $establishment_fax = substr($establishment_fax, 0, -1);
						if (substr($establishment_cell, -1) == ',' || substr($establishment_cell, -1) == ';') $establishment_cell = substr($establishment_cell, 0, -1);
						
						if (!isset($establishmentXml->establishment->contacts->contact)) {
							if (!empty($establishment_tel)) $contact_details .= "<li><strong>Tel No</strong>: $establishment_tel<br />";
							if (!empty($establishment_cell)) $contact_details .= "<li><strong>Cell No</strong>: $establishment_cell<br />";
							if (!empty($establishment_fax)) $contact_details .= "<li><strong>Fax No</strong>: $establishment_fax<br />";
						} else {
							foreach ($establishmentXml->establishment->contacts->contact AS $contactXml) {
								$contact_type = $this->xmlStr($contactXml->contact_type);
								$contact_value = $this->xmlStr($contactXml->contact_value);
								$contact_description = $this->xmlStr($contactXml->contact_description);
								switch ($contact_type) {
									case 'tel1':
										$tel1_block = "<strong>Tel</strong>: ";
										if (!empty($contact_description)) $tel1_block .= "$contact_description - ";
										$tel1_block .= "$contact_value<br />";
										break;
									case 'tel2':
										$tel1_block = "<strong>Tel 2</strong>: ";
										if (!empty($contact_description)) $tel2_block .= "$contact_description - ";
										$tel2_block .= "$contact_value<br />";
										break;
									case 'cell1':
										$cell1_block = "<strong>Cell</strong>: ";
										if (!empty($contact_description)) $cell1_block .= "$contact_description - ";
										$cell1_block .= "$contact_value<br />";
										break;
									case 'cell2':
										$cell2_block = "<strong>Cell 2</strong>: ";
										if (!empty($contact_description)) $cell2_block .= "$contact_description - ";
										$cell2_block .= "$contact_value<br />";
										break;
									case 'fax1':
										$fax1_block = "<strong>Fax</strong>: ";
										if (!empty($contact_description)) $fax1_block .= "$contact_description - ";
										$fax1_block .= "$contact_value<br />";
										break;
									case 'fax2':
										$fax2_block = "<strong>Fax 2</strong>: ";
										if (!empty($contact_description)) $fax2_block .= "$contact_description - ";
										$fax2_block .= "$contact_value<br />";
										break;
									case 'email':
										$reservation_email = $contact_value;
										break;
								}
							}
							
							if (!empty($tel1_block)) $contact_details .= "$tel1_block";
							if (!empty($tel2_block)) $contact_details .= "$tel2_block";
							if (!empty($cell1_block)) $contact_details .= "$cell1_block";
							if (!empty($cell2_block)) $contact_details .= "$cell2_block";
							if (!empty($fax1_block)) $contact_details .= "$fax1_block";
							if (!empty($fax2_block)) $contact_details .= "$fax2_block";
						}
						
						if (!empty($establishmentXml->establishment->website_url)) {
							$website_url = $this->xmlStr($establishmentXml->establishment->website_url);
							if (substr($website_url, 0, 7) != 'http://') $website_url = "http://$website_url";
							$web_block = "<strong>Web</strong>: <a href='http://booktravel.travel/e/website.php?id=$establishment_code' target='_blank' rel='nofollow' >View Website</a><br />";
						}
						if (!empty($web_block)) $contact_details .= "$web_block";
						$contact_details .= "<strong>Web Code</strong>: ".$establishment['code'];
						
						$base_street1 = $this->xmlStr($establishmentXml->establishment->street_address_line1);
						$base_street2 = $this->xmlStr($establishmentXml->establishment->street_address_line2);
						$base_street3 = $this->xmlStr($establishmentXml->establishment->street_address_line3);
						if (!empty($base_street1) || !empty($base_street2) || !empty($base_street3)) {
							$contact_address .= "<strong>Physical Address: </strong><br />";
							if (!empty($base_street1)) $contact_address .= "$base_street1<br />";
							if (!empty($base_street2)) $contact_address .= "$base_street2<br />";
							if (!empty($base_street3)) $contact_address .= "$base_street3";
						}
						
						$reservation_postal1 = $this->xmlStr($establishmentXml->establishment->reservation_postal1);
						$reservation_postal2 = $this->xmlStr($establishmentXml->establishment->reservation_postal2);
						$reservation_postal3 = $this->xmlStr($establishmentXml->establishment->reservation_postal3);
						$reservation_postal_code = $this->xmlStr($establishmentXml->establishment->reservation_postal_code);
						
						$base_postal1 = $this->xmlStr($establishmentXml->establishment->postal_address_line1);
						$base_postal2 = $this->xmlStr($establishmentXml->establishment->postal_address_line2);
						$base_postal3 = $this->xmlStr($establishmentXml->establishment->postal_address_line3);
						$base_postalCode = $this->xmlStr($establishmentXml->establishment->postal_address_code);
						
						if (empty($reservation_postal1)) {
							if (!empty($base_postal1) || !empty($base_postal2) || !empty($base_postal3)) {
								$contact_address .= "<br /><strong>Postal Address: </strong><br />";
								if (!empty($base_postal1)) {
									if (substr($base_postal1, -1) == ',' || substr($base_postal1, -1) == ';') $base_postal1 = substr($base_postal1, 0, -1);
									$contact_address .= "$base_postal1<br>";
								}
								if (!empty($base_postal2)) {
									if (substr($base_postal2, -1) == ',' || substr($base_postal2, -1) == ';') $base_postal2 = substr($base_postal2, 0, -1);
									$contact_address .= "$base_postal2<br>";
								}
								if (!empty($base_postal3)) {
									if (substr($base_postal3, -1) == ',' || substr($base_postal3, -1) == ';') $base_postal3 = substr($base_postal3, 0, -1);
									$contact_address .= "$base_postal3<br>";
								}
								if (!empty($base_postalCode)) $contact_address .= "$base_postalCode<br>";
							}
						} else {
							if (!empty($reservation_postal1) || !empty($reservation_postal2) || !empty($reservation_postal3)) {
								$contact_address .= "<strong>Postal Address: </strong><br />";
								if (!empty($reservation_postal1)) {
									if (substr($reservation_postal1, -1) == ',' || substr($reservation_postal1, -1) == ';') $reservation_postal1 = substr($reservation_postal1, 0, -1);
									$contact_address .= "$reservation_postal1<br>";
								}
								if (!empty($reservation_postal2)) {
									if (substr($reservation_postal2, -1) == ',' || substr($reservation_postal2, -1) == ';') $reservation_postal2 = substr($reservation_postal2, 0, -1);
									$contact_address .= "$reservation_postal2<br>";
								}
								if (!empty($reservation_postal3)) {
									if (substr($reservation_postal3, -1) == ',' || substr($reservation_postal3, -1) == ';') $reservation_postal3 = substr($reservation_postal3, 0, -1);
									$contact_address .= "$reservation_postal3<br>";
								}
								if (!empty($reservation_postal_code)) $contact_address .= "$reservation_postal_code<br>";
								$contact_address .= "</td></tr>";
							}
						}
						
						$gps_latitude = $this->xmlStr($establishmentXml->establishment->gps_latitude);
						$gps_longitude = $this->xmlStr($establishmentXml->establishment->gps_longitude);
						if (empty($gps_latitude)) {
							$gps = '';
							$gps_coord = '';
						} else {
							$contact_address .= "<strong>GPS: </strong><br />";
							$gps_latitude = stripslashes($gps_latitude);
							$gps_longitude = stripslashes($gps_longitude);
							$gps_latitude = str_replace('<34', '"', $gps_latitude);
							$gps_longitude = str_replace('<35', '"', $gps_longitude);
							$contact_address .= "Latitude: ".round($gps_latitude, 6)."<br />Longitude: ".round($gps_longitude, 6);
							//echo $gps_latitude;
							$gps_coord = "GPS Co-ordinates for ".$establishment_name." : ".round($gps_latitude, 6).", ".round($gps_longitude, 6);
						}
						
						$room_count = $this->xmlStr($establishmentXml->establishment->room_count);
						
					//Set Template Est Booking Data
						$contact_enquire = "";
						$prebook_id = $this->xmlStr($establishmentXml->establishment->prebook_id);
						$nightsbridge_id = $this->xmlStr($establishmentXml->establishment->nightsbridge_bbid);
						$nightsbridge_button = "<br /><div align='center'><a style='cursor:pointer' target='_blank' href='https://www.nightsbridge.co.za/bridge/Search?bbid=$nightsbridge_id&nbid=18'><input type='button' class='EstablishmentButton' value='Check Availability' /></a></div><br />";
						
						if (!empty($prebook_id)) $booking_link = "";
						if (!empty($nightsbridge_id)) {
							$contact_enquire = "<br /><br /><a class='emButton' target='_blank' href='https://www.nightsbridge.co.za/bridge/Search?bbid=$nightsbridge_id&nbid=18'>Check Availability</a>";
							$contact_enquire .= "";
							$contact_enquire .= "";
						}
						
						$establishment_template = str_replace('<!-- code -->', $establishment_code, $establishment_template);
						$establishment_template = str_replace('<!-- nightsbridge_button -->', $nightsbridge_button, $establishment_template);
						
					//Set Template Est Icons
						$icon_counter = 0;
						$facilities = "";
						foreach ($establishmentXml->establishment->icons->icon AS $iconXml) {
							$icon_name = $this->xmlStr($iconXml->name);
							$icon_url = $this->xmlStr($iconXml->url);
							$icon_description = $this->xmlStr($iconXml->description);
							//if ($icon_name == 'PFOLIO') continue;
							$facilities .= "<img width='33' height='31' src='$icon_url' title='$icon_description'>";
							$icon_counter++;
							if ($icon_counter == 12){$facilities .= "<br>";$icon_counter = 0;}
						}
						$establishment_template = str_replace('<!-- facilities -->', $facilities, $establishment_template);
						
					//Set Template Est directions
						$direction_block = "";
						$direction = "";
						$direction = $this->xmlStr($establishmentXml->establishment->directions->directions);
						$map_type = $this->xmlStr($establishmentXml->establishment->directions->map_type);
						$image_name = $this->xmlStr($establishmentXml->establishment->directions->image_name);
						
						//echo $map_type;
						
						if (!empty($direction) || $map_type == 'google' || $gps_coord || $map_type == "upload") {
							//$map_type = 'google';
							$direction = nl2br(stripslashes($direction));
							$map_file = strtolower($establishment_code) . '.jpg';
							if ($gps_coord != "" && empty($map_type)) {
								$map_type = "google";
							} 
							if (!empty($map_type) && $map_type == 'upload') {$direction_block .= "<table align=right><tr valign=top><td><a href='/res_maps/$image_name' target='_blank' ><img src='/res_maps/TN_$map_file' border=0 /></a></td></tr></table>";}
							//echo $map_type.$direction_block;
							if (!empty($map_type) && $map_type == 'google')  {
								
								
								if (empty($gps_latitude)) {
									//Get Geocode
									$delay = 0;
									$geocode_pending = true;
									
									$address = '';
									if (!empty($base_street1)) $address .= "$base_street1,";
									if (!empty($base_street2)) $address .= "$base_street2,";
									if (!empty($base_street3)) $address .= "$base_street3,";
									$address = substr($address, 0, -1);
									
									$map_url = "http://maps.google.com/maps/geo?output=xml&key=" . $GLOBALS['api_key'] . "&q=" . urlencode($address);
								
									while ($geocode_pending) {
										$xml = simplexml_load_file($map_url);
										
										$status = $xml->Response->Status->code;
										if (strcmp($status, "200") == 0) {
											$geocode_pending = false;
										    $coordinates = $xml->Response->Placemark->Point->coordinates;
										    $coordinatesSplit = split(",", $coordinates);
										    // Format: Longitude, Latitude, Altitude
										    $lat = $coordinatesSplit[1];
										    $lng = $coordinatesSplit[0];
											
											if (str_word_count($short_description) > 50) {
												$words = str_word_count($short_description, 1);
												$words = array_slice($words, 0, 50);
												$map_description = implode(' ', $words);
												$map_description .= '...';
											} else {
												$map_description = $short_description;
											}
										    
											$map_description = str_replace(array("\r\n","\r","\n"), '', $map_description);
											$map_description = trim($map_description);
											$map_description = str_replace("'", '&apos;', $map_description);
										    $direction_block .= "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key={$GLOBALS['api_key']}&sensor=false' type='text/javascript'></script>";
										    $direction_block .= "<table width=100%><tr valign=top><td>";
										    $direction_block .= "<script type='text/javascript'>
																    function initialize() {
																      if (GBrowserIsCompatible()) {
																        var map = new GMap2(document.getElementById('map_canvas'));
																        map.setCenter(new GLatLng($lat, $lng), 13);
																        map.setUIToDefault();
																        map.openInfoWindowHtml(map.getCenter(),
					                  										'<table width=300px ><tr><td><span style=\"font-family: arial; font-size:10pt; font-weight: bold\">".addslashes($establishment_name)."</span><br><table align=left><tr><td>";
								    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/res_images/$map_image")) $direction_block .= "<img src=/res_images/$map_image style=\"border: 1px solid black\"/>";
								    $direction_block .= "</td></tr></table><span style=\"font-family: arial; font-size: 8pt\" >$map_description</span></td></tr></table>');
									
																      }
																    }
																
																    </script>";
										    $direction_block .= "<div id='map_canvas'></div></td></tr></table>";
											$direction_block .= "<script>initialize()</script>";
										} else if (strcmp($status, "620") == 0) {
										    $delay += 100000;
										} else {
											$geocode_pending = false;
										}
										usleep($delay);
									}
								} else {
									
									if (str_word_count($short_description) > 50) {
										$words = str_word_count($short_description, 1);
										$words = array_slice($words, 0, 50);
										$map_description = implode(' ', $words);
										$map_description .= '...';
									} else {
										$map_description = $short_description;
									}
									
									$map_description = str_replace(array("\r\n","\r","\n"), '', $map_description);
									$map_description = trim($map_description);
									$map_description = str_replace("'", '&apos;', $map_description);
									$direction_block .= "<script src='http://maps.google.com/maps?file=api&amp;v=2&amp;key={$GLOBALS['api_key']}&sensor=false' type='text/javascript'></script>";
								    $direction_block .= "<table width=100%><tr valign=top><td>";
								    $direction_block .= "<script type='text/javascript'>
														    function initialize() {
														      if (GBrowserIsCompatible()) {
														        var map = new GMap2(document.getElementById('map_canvas'));
														        map.setCenter(new GLatLng($gps_latitude, $gps_longitude), 13);
														        map.setUIToDefault();
														        map.openInfoWindowHtml(map.getCenter(),
				                  									'<table width=300px ><tr><td><span style=\"font-family: arial; font-size:10pt; font-weight: bold\">".addslashes($establishment_name)."</span><br><table align=left><tr><td>";
								    if (is_file($_SERVER['DOCUMENT_ROOT'] . "/res_images/$map_image")) $direction_block .= "<img src=/res_images/$map_image style=\"border: 1px solid black\"/>";
								    $direction_block .= "</td></tr></table><span style=\"font-family: arial; font-size: 8pt\" >$map_description</span></td></tr></table>');
														      }
														    }
														
														    </script>";
								    $direction_block .= "$gps_coord<div id='map_canvas'></div></td></tr></table>";
									$direction_block .= "<script>initialize()</script>";
								}
							}
							if (trim($direction) != "") {
								$direction_block .= "<p>$direction</p>";
							}
						}
						
						$establishment_template = str_replace('<!-- directions -->', $direction_block, $establishment_template);
						
					//Set Template Est Price
						$price_low = "";
						$price_high = "";
						$price_quote = "";
						$price_quote_block = "";
						$price_category_suffix = $establishmentXml->establishment->price_category_suffix;
						if ($establishmentXml->establishment->price_in_low > 0) {
							$price_low = round($establishmentXml->establishment->price_in_low);
						}
						if ($establishmentXml->establishment->price_out_low > 0) {
							$price_low = round($establishmentXml->establishment->price_out_low);
						}
						if ($establishmentXml->establishment->price_out_high > 0) {
							$price_high = round($establishmentXml->establishment->price_out_high);
						}
						if ($establishmentXml->establishment->price_in_high > 0) {
							$price_high = round($establishmentXml->establishment->price_in_high);
						}
						if ($price_low > 0 && $price_high > 0) {
							$price_quote = "From: <strong class='nbelm'>R$price_low - R$price_high</strong> <span class='nbelm'>$price_category_suffix</span>";
							$price_quote_block .= $price_quote;
						}
						//if ($price_quote == "") {
							$price_quote = "".$establishmentXml->establishment->price."";
						//}
						
						$establishment_template = str_replace('<!-- price_quote -->',nl2br($this->xmlStr($price_quote_block)),$establishment_template);
						$establishment_template = str_replace('<!-- price_form -->',nl2br($this->xmlStr($price_quote_block)),$establishment_template);
						$establishment_template = str_replace('<!-- rates -->',nl2br($this->xmlStr($price_quote)),$establishment_template);
						$establishment_template = str_replace('<!-- pricing -->',nl2br($this->xmlStr($establishmentXml->establishment->price)),$establishment_template);
						
					//Set Template Awards
						$awards = "";
						foreach ($establishmentXml->establishment->awards->award as $awardXml) {
							$award_status = $this->xmlStr($awardXml->status);
							$award_name = $this->xmlStr($awardXml->category_description);
							
							if ($awardXml->year == "2009") {
								if (!empty($award_status)) {
									$award_icon = $this->makeUrlSafe($this->xmlStr($award_name));
									$awards .= "<table cellpadding=0 cellpacing=0 width=100%><tr><td align=center>";
									$awards .= "<img src='http://booktravel.travel/lookup_images/award_".$award_status."_2009.jpg' />";
									$awards .= "</td></tr></table><p>";
								}
							}
							if ($awardXml->year == "2010") {
								if (!empty($award_status)) {
									$awards .= "<table cellpadding=0 cellpacing=0 width=100%><tr><td align=center>";
									$awards .= "<img src='http://booktravel.travel/lookup_images/award_{$award_status}_2010.jpg' />";
									$awards .= "</td></tr></table><p>";
								}
							}
							if ($awardXml->year == "2011") {
								if (!empty($award_status)) {
									$awards .= "<table cellpadding=0 cellpacing=0 width=100%><tr><td align=center>";
									$awards .= "<img width='165' src='http://booktravel.travel/lookup_images/award_{$award_status}_2011.jpg' />";
									$awards .= "</td></tr></table><p>";
								}
							}
	
						}
						
						//$establishment_template = str_replace('<!-- awards -->', $awards, $establishment_template);
						
					//Set Template User Rating
						$user_rating = $this->xmlStr($establishmentXml->establishment->user_rating);
						$smilies = "";
						$comments = "";
						$comment = trim($this->xmlStr($establishmentXml->establishment->visitor_comment));
						
						if (!empty($user_rating) || $comment != "") {
						    switch ($user_rating) {
						        case -1:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/shame.png'>";
						            break;
						        case 1:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/sad.png'>";
						            break;
						        case 2:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/hmm.png'><img align='absmiddle' src='http://booktravel.travel/images/hmm.png'>";
						            break;
						        case 3:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'>";
						            break;
						        case 4:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'><img align='absmiddle' src='http://booktravel.travel/images/happy.png'>";
						            break;
						        case 5:
						            $smilies .= "<img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'><img align='absmiddle' src='http://booktravel.travel/images/cool.png'>";
						            break;
						    }
						    
					        $visitor_name = trim($this->xmlStr($establishmentXml->establishment->visitor_name));
					        
					        $town_name = trim($this->xmlStr($establishmentXml->establishment->visitor_location));
					        
					        $comments .= "'$comment'<span style='font-size: 8pt'> ";
					        if (!empty($visitor_name) && $town_name != ',') $comments .= " - ";
					        if (!empty($visitor_name)) $comments .= "$visitor_name";
					        if (($town_name) != ',') {
					            if (substr($town_name, 0, 1) == ',') $town_name = substr($town_name, 1);
					            $comments .= " in $town_name";
					        }
					        $comments .= "</span>";
						    $estab_link = str_replace(' ', '_', $establishment_name);
						    $read_more = '<div style="padding-bottom:3px;" align="center"><input onclick=\'estabTabClicked(document.getElementById("EstablishmentTabReviews"));\' class="EstablishmentButton" type=\'button\' value="Read More Reviews" /></div>';
						} else {
						    $smilies = "<span style='font-family: arial; font-size: 10pt'>This establishment has not yet been reviewed.</span>";
						    $read_more = '';
						    $comments = '';
						}
						
						$star_text = array(1=>'<img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />'
							,'<img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" /> <img src="/images/gold_star.jpg" />');
						
						$star_rating = '';
						$star_grading = '';
						$star_grading = $this->xmlStr($establishmentXml->establishment->star_grading);
						if (!empty($star_grading)) $star_rating = $star_text[$star_grading] . ' (TGCSA)';
						
						$establishment_template = str_replace('<!-- rating -->', $smilies, $establishment_template);
						$establishment_template = str_replace('<!-- comment -->', substr($comments,0,120), $establishment_template);
						$establishment_template = str_replace('<!-- read_more -->', $read_more , $establishment_template);
						$establishment_template = str_replace('<!-- star_rating -->', $star_rating , $establishment_template);
						
						//$rightContent .= "<br />".$contact_enquire."<br style='clear:both;' /><br /><a class='emButton' style='padding-bottom:7px;' target='_blank' href='http://booktravel.travel/index.php?p=enquiry&c=$establishment_code'>Email Enquiry</a><br style='clear:both;' />";
						$rightContent .= "";
						if ($comments != "") {
							$rightContent .= "<br /><div align='center'>$smilies</div><div style='padding-left:2px;'>".substr($comment,0,128)."...<br /><em>$visitor_name</em><br />$read_more</div>";
							
						}
						$rightContent .= '<div align=\'center\'><input onclick=\'document.location="/guest_review/'.$establishment_code.'"\' class="EstablishmentButton" type=\'button\' value="Add a Guest Review" /></div>';
						$rightContent .= '<div class="linkBlock linkBlockEst" style="text-align=center;"><ul>';
						//$rightContent .= ;
						$rightContent .= "<div align='center'>".$star_rating."</div>";
						$rightContent .= '</ul></div>';
						$rightContent .= '<h1>Contact Details</h1><div class="linkBlock linkBlockEst" align="left"><ul>';
						$rightContent .= $contact_details;
						$rightContent .= '</ul></div><h1>Address</h1><div class="linkBlock linkBlockEst" align="left"><ul>';
						$rightContent .= $contact_address."</ul></div>";
						$rightContent .= $awards;
						
						if ($isLandmark) {
							$landmarks = '<h1>Nearby Points of Interest</h1><div class="linkBlock linkBlockEst" align="left"><ul>'.$landmarks.'</ul></div>';
						} else {
							$landmarks = '';
						}
						
						
						$rightContent .= $landmarks;
						
						$establishment_template = str_replace('<!-- right_content -->', $rightContent, $establishment_template);
						$template = str_replace('<!-- right_links -->', $rightContent , $template);
					
					//Guest Reviews
						$establishment_template = str_replace('<!-- smilies -->', $smilies, $establishment_template);
						$reviews = "";
						foreach ($establishmentXml->establishment->reviews->review as $reviewXML) {
							if ($this->xmlStr($reviewXML->visitor_comment) != "") {
								$reviews .= "<div align='center' style='padding:10px;text-align:justify;' class='EstablishmentBlueBlock'>";
								$reviews .= '"'.$this->xmlStr($reviewXML->visitor_comment).'"';
								$reviews .= "</div><div align='right'><div style='float:left'>".$this->xmlStr($reviewXML->date)."</div><strong>Review from ".$this->xmlStr($reviewXML->firstname)." ".$this->xmlStr($reviewXML->surname)." in ".$this->xmlStr($reviewXML->country)."</strong></div><br /><br />";
							}
						}
						$establishment_template = str_replace('<!-- reviews -->', $reviews, $establishment_template);
						$establishment_template = str_replace('<!-- establishment_code -->', $establishment_code, $establishment_template);
						
						
					
				}
			}
		return $establishment_template;
	}
	
	function getBody() {
		global $templateLinkBlock;
		global $template;
		global $config;
		global $pageParam;
		global $api;
		
		$pageQuery = $_GET["page"];
		
		if ($pageParam[0] == "accommodation") {
			$api->setMethod("fetch");
			$api->setType("aaweb_areas");
			$api->addParam("url",$pageQuery);
			$areaXml = $api->getApiXML();
			
			if (isset($areaXml->establishment['code'])) {
				$establishment_body = $this->getEstablishment($areaXml->establishment,$areaXml);
			}
			$travelInfoPage = "";
			$preUrl = "accommodation/".$areaXml->theme["safe_url"];
			
			$preUrl .= "/".$areaXml->region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->country["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->province["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->province_region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->sub_region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->town["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl .= "/".$areaXml->town_region["safe_url"];
			$preUrl = trim($preUrl,"/");
			
			$preUrl = trim($preUrl,"/");
			
			$rdclass = $this->getProvinceClass($areaXml);
			
			if (isset($areaXml->themes->theme)) {
				if (isset($areaXml->suburb)) {$areaHeading = $areaXml->suburb;$travelInfoPage = $areaXml->suburb['safe_url'];} else
				if (isset($areaXml->town_region)) {$areaHeading = $areaXml->town_region;$travelInfoPage = $areaXml->town_region['safe_url'];} else
				if (isset($areaXml->town)) {$areaHeading = $areaXml->town;$travelInfoPage = $areaXml->town['safe_url'];} else
				if (isset($areaXml->sub_region)) {$areaHeading = $areaXml->sub_region;$travelInfoPage = $areaXml->sub_region['safe_url'];} else
				if (isset($areaXml->province_region)) {$areaHeading = $areaXml->province_region;$travelInfoPage = $areaXml->province_region['safe_url'];} else
				if (isset($areaXml->province)) {$areaHeading = $areaXml->province;$travelInfoPage = $areaXml->province['safe_url'];} else
				if (isset($areaXml->country)) {$areaHeading = $areaXml->country;$travelInfoPage = $areaXml->country['safe_url'];} else
				if (isset($areaXml->region)) {$areaHeading = $areaXml->region;$travelInfoPage = $areaXml->region['safe_url'];}
				
				$travelInfoList = "";
				
				$travelInfoPage = $this->contentFileFromName($travelInfoPage);
				if (file_exists("content/".$travelInfoPage.".html")) {
					$travelInfoList .= "<div class='areaBlock'>";
					$travelInfoList .= "<h1 class='$rdclass'>Travel Information</h1><ul>";
					$travelInfoList .= "<li><a target='_blank' href='/index.php?p=".$travelInfoPage."'>".$this->xmlStr($areaHeading)." info</a></li>";
					$travelInfoList .= "</div>";
				}
				
				$themeList2 = "<div class='areaBlock'>";
				$themeList2 .= "<h1 class='$rdclass'>Discount Accommodation</h1><ul>";
				$themeList = "<div class='areaBlock'>";
				$themeList .= "<h1 class='$rdclass'>Accommodation in ".$this->xmlStr($areaHeading)."</h1><ul>";
				$showThemeList2 = false;
				foreach ($areaXml->themes->theme AS $theme) {
					$accomThemeFix = false;
					$themUrl = $preUrl;
					if (strpos($preUrl, "accommodation/accommodation") > -1) {
						$accomThemeFix = true;
						$themUrl = str_ireplace("accommodation/accommodation/", "accommodation/", $themUrl);
					}
					$themUrl = str_replace($areaXml->theme["safe_url"], $theme["safe_url"], $themUrl);
					if ($accomThemeFix) {
						$themUrl = "accommodation/".$themUrl;
					}
					if ($theme["safe_url"] == "specials" || $theme["safe_url"] == "bonsela") {
						$themeList2 .= "<li><a href='/$themUrl'>".$this->xmlStr($theme)."</a> (".$theme["count"].")</li>";
						$showThemeList2 = true;
					} else {
						$themeList .= "<li><a href='/$themUrl'>".$this->xmlStr($theme)."</a> (".$theme["count"].")</li>";
					}
				}
				$themeList .= "</ul></div>";
				$themeList2 .= "</ul></div>";
				if (!$showThemeList2) {
					$themeList2 = "";
				}
			}
			if (!isset($areaXml->establishment)) {
				$api->setMethod("fetch");
				$api->setType("aaweb_establist");
				if (isset($areaXml->theme)) {$api->addParam("theme_id",$areaXml->theme['id']);}
				if (isset($areaXml->region)) {$api->addParam("region_id",$areaXml->region['id']);}
				if (isset($areaXml->country)) {$api->addParam("country_id",$areaXml->country['id']);}
				if (isset($areaXml->province)) {$api->addParam("province_id",$areaXml->province['id']);}
				if (isset($areaXml->province_region)) {$api->addParam("pregion_id",$areaXml->province_region['id']);}
				if (isset($areaXml->sub_region)) {$api->addParam("sregion_id",$areaXml->sub_region['id']);}
				if (isset($areaXml->town)) {$api->addParam("town_id",$areaXml->town['id']);}
				if (isset($areaXml->town_region)) {$api->addParam("tregion_id",$areaXml->town_region['id']);}
				if (isset($areaXml->suburb)) {$api->addParam("suburb_id",$areaXml->suburb['id']);}
				
				$listPage = 1;
		
				for($i=1;$i<12;$i++) {
					$page = explode("_",$pageParam[$i]);
						
					if ($page[0] == "page") {
						$listPage = $page[1];
					}
				}
				
				$api->addParam("page",$listPage);
				$areaEstabListXml = $api->getApiXML();
				$areaEstabList = $this->getEstList($areaEstabListXml,$areaXml,$preUrl);
				$log_params = "";
				foreach ($areaEstabListXml->estlist->establishment AS $result) {
					$log_params .= $result["code"].",";
				}
				$log_params = rtrim($log_params,",");
				$log_url = $_SERVER["REQUEST_URI"];
				$log_type = "l";
				$log_ip = $_SERVER["REMOTE_ADDR"];
				$this->webLog($log_params,$log_url,$log_type,$log_ip);
			}
			
			$addBanner = "<div align='center'><script language=\"javascript\">
						<!--
						if (window.adgroupid == undefined) {
							window.adgroupid = Math.round(Math.random() * 1000);
						}
						document.write('<scr'+'ipt language=\"javascript1.1\" src=\"http://adserver.adtech.de/addyn|3.0|567|1447296|0|1|ADTECH;loc=100;target=_blank;key=".$keys.";grp='+window.adgroupid+';misc='+new Date().getTime()+'\"></scri'+'pt>');
						//-->
						</script></div>";
			$addBanner = "";
			
			$regionDropDowns = $this->ajaxGetRegionData($areaXml,$listPage);
			
			$areaBody = "";
			
			$areaBody .= "<div id='RegionDropDowns' class='$rdclass'>$regionDropDowns</div>
				
			<div id='AreaBodyHolder'>
				<div id='EstabListHolder' class='areaEstHolder'>$areaEstabList</div>
				<div id='AreaBlockHolder' class='areaBlockHolder'>$travelInfoList$themeList2$themeList$suburbList$townRegList$townList$subRegList$provRegList$provinceList$countryList$regionList <br /><br /> <a href='/index.php?p=map_regions'><img src='/images/mini-map-right.png' /></a></div>
			</div>$addBanner";
			
			$keys = $areaXml->country."+".$areaXml->province."+".$areaXml->town;
			$keys = trim($keys,"+");
			
			$body = "";
		}			
		if ($pageParam[1] == "") {
			$body = file_get_contents("content/home.html");
			if ($pageParam[0] == "asearch") {
				$body .= "<script>openAdvancedSearch();</script>";
			}
		}
		if ($pageParam[0] == "asearch") {
			$template = $this->setTemplate($template,"body",'<h1 class="estListHeader sa">Advanced Search</h1><div id="AdvancedSearchContent">
				<fieldset class="advSearchFieldset">
					<legend>Select Location</legend>
					<div id="AdvancedSearchLocations"></div>
				</fieldset>
				<fieldset class="advSearchFieldset">
					<legend>AA Qaulity Assurance</legend>
					<div id="AdvancedSearchQA"></div>
				</fieldset>
				<br style="clear:both;" /><br />
				<input type="button" onclick="javascript:submitAdvSearch();" value="Search" />
				<div id="AdvancedSearchIcons"></div>
				<br style="clear:both;" /><br />
				<input type="button" onclick="javascript:submitAdvSearch();" value="Search" />
			</div>
			<script>
				openAdvancedSearch();
			</script>');
		} elseif ($pageParam[0] == "guest_review") {
			$template = $this->setTemplate($template,"body",'<iframe style="border:0;" width="800" height="700" src="http://booktravel.travel/survey/survey.php?code='.$pageParam[1].'&type=aaweb" />');
		} elseif ($establishment_body != "") {
			$template = $this->setTemplate($template,"body","<div id='RegionDropDowns' class='$rdclass'>$regionDropDowns</div>".$establishment_body);
		} else {
			$template = $this->setTemplate($template,"body",$areaBody.$body);
		}
	}
	
	function getProvinceClass($areaXml) {
		$rdclass = "sa";
		if ($areaXml->province == "Gauteng") {$rdclass = "gp";}
		if ($areaXml->province == "Western+Cape") {$rdclass = "wc";}
		if ($areaXml->province == "Eastern+Cape") {$rdclass = "ec";}
		if ($areaXml->province == "Northern+Cape") {$rdclass = "nc";}
		if ($areaXml->province == "KwaZulu-Natal") {$rdclass = "kzn";}
		if ($areaXml->province == "Free+State") {$rdclass = "fs";}
		if ($areaXml->province == "Mpumalanga") {$rdclass = "mp";}
		if ($areaXml->province == "Limpopo") {$rdclass = "lm";}
		if ($areaXml->province == "North+West") {$rdclass = "nw";}
		return $rdclass;
	}
	
	function xmlStr($str) {
		return urldecode($str);
	}
	
	function setTemplate($template,$section,$content) {
		return str_replace("<!-- $section -->", $content, $template);
	}
	
	function getTemplateBlock($filename,$block) {
		$template = file_get_contents("templates/$filename");
		$templateBlockStart = strpos($template,"<!-- start_$block -->");
		$templateBlockEnd = strpos($template,"<!-- end_$block -->");
		$templateBlock = substr($template,$templateBlockStart,($templateBlockEnd-$templateBlockStart));
		return $templateBlock;
	}
	private function contentFileFromName($name) {
		$filename = urldecode(urldecode($name));
		$filename = trim($filename);
		$filename = str_replace("-","_",$filename);
		$filename = str_replace(" ","_",$filename);
		$filename = str_replace("(","",$filename);
		$filename = str_replace(")","",$filename);
		$filename = str_replace("&","_",$filename);
		$filename = str_replace("'","",$filename);
		$filename = str_replace("__001__","_",$filename);
		$filename = strtolower($filename);
		return $filename;
	}
	private function makeUrlSafe($str) {
		$str = str_replace("&", "__001__", $str);
		$safe_url = strtolower($str);
		$safe_url = str_replace(" ","_",$safe_url);
		$safe_url = urlencode($safe_url);
		return $safe_url;
	}
	
}?>