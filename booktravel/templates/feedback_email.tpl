<style>
    BODY, TD, TH {font-family: arial; font-size: 10pt}
</style>
<table cellspacing=0 cellpadding=2 width=500 style='background-color: #F3CA00'>
    <tr>
        <td>
            Guest Feedback - <!-- establishment_name -->
        </td>
    </tr>
</table>
<p>&nbsp;</p>
<table cellspacing=0 cellpadding=0 width=500 >
    <tr>
        <td>
            Feedback has been submitted via the AA Travel website. Details of the feedback are given below.
        </td>
    </tr>
</table>
<p>&nbsp;</p>
<table cellspacing=0 cellpadding=0 width=500>
    <tr>
        <th style='background-color: grey; font-weight: bold; text-align:left' colspan=2>
            Establishment Details
        </th>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right width=150 style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Town 
        </td>
        <td width=350 style='padding-left: 4px'>
            <!-- town -->
        </td>
    </tr>
    <tr style='background-color: #DBDCDD'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Telephone 
        </td>
        <td style='padding-left: 4px'>
            <!-- telephone -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right  style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Email 
        </td>
        <td style='padding-left: 4px'>
            <a href='mailto:<!-- email -->'><!-- email --></a>
        </td>
    </tr>
    <tr>
        <td colspan=2>&nbsp;</td>
    </tr>
    <tr>
        <th style='background-color: grey; font-weight: bold; text-align:left' colspan=2>
            Traveler details
        </th>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right  style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Name 
        </td>
        <td style='padding-left: 4px'>
            <!-- name -->
        </td>
    </tr>
    <tr style='background-color: #DBDCDD'>
        <td align=right  style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Email 
        </td>
        <td style='padding-left: 4px'>
            <a href='mailto:<!-- traveler_email -->'><!-- traveler_email --></a>
        </td>
    </tr>
    <tr>
        <td colspan=2>&nbsp;</td>
    </tr>
     <tr style='background-color: #E9E9E9'>
        <th style='background-color: grey; font-weight: bold; text-align:left' colspan=2>
            Feedback
        </th>
    </tr>
     <tr style='background-color: #E9E9E9'>
        <td align=right  style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Date of Visit 
        </td>
        <td style='padding-left: 4px'>
            <!-- visit_date -->
        </td>
    </tr>
     <tr style='background-color: #DBDCDD'>
        <td align=right  style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Recommended 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_recommends_as -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Quality 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_quality -->
        </td>
    </tr>
    <tr style='background-color: #DBDCDD'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Staff FriendlIness  
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_staff -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Staff Dependability 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_staff_dependability -->
        </td>
    </tr>
    <tr style='background-color: #DBDCDD'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Value for Money 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_value -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Room Cleanliness 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_room_cleanliness -->
        </td>
    </tr>
    <tr style='background-color: #DBDCDD'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Room Comfort 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_room_comfort -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Condition of Appliances 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_appliances -->
        </td>
    </tr>
    <tr style='background-color: #DBDCDD'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Bathrooms 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_bathrooms -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Self Catering Kitchen 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_kitchen -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Quality of Food 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_food -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Helpfulness of Management and Staff 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_management -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Safety and Security 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_security -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Check-in 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_checkin -->
        </td>
    </tr>
    <tr style='background-color: #E9E9E9'>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Activities 
        </td>
        <td style='padding-left: 4px'>
            <!-- 2_activities -->
        </td>
    </tr>
    <tr style='background-color: #DBDCDD' valign=top>
        <td align=right style='border-width: 0 1 0 0; border-style: dotted; border-color: grey; padding-right: 4px'>
           Comments 
        </td>
        <td style='padding-left: 4px'>
            <!-- comments -->
        </td>
    </tr>
</table>