<h2>Review for <!-- establishment_name --></h2>
<table>
    <tr>
        <td style='font-size: 12'>
           Your review has been submitted for processing and approval. Thank you!<br>
           Please note that in some cases we will give the establishment concerned the right of reply.
        </td>
    </tr>
</table>
