<?php
/**
 * Displays the search image and saves the search impression to the DB (nse_establishment_searchlog)
 */

header ('Content-type: image/jpeg');


//Vars
$image = '';
$establishment_code = $_GET['e'];
$width = 0;
$height = 0;
$maxWidth = 150;
$maxHeight  = 88;
$empty_image = TRUE;
$src_image = $_SERVER['DOCUMENT_ROOT'] . "/images/nophoto.png";
$search_type = isset($_GET['ct'])?$_GET['ct']:'';
$search_string = isset($_GET['st'])?$_GET['st']:'';

//Includes
include_once 'aa_init/init.php';

//Get star Grading
$star_grading = '';
$statement = "SELECT star_grading FROM nse_establishment_restype WHERE establishment_code=? LIMIT 1";
$sql_star = $GLOBALS['dbCon']->prepare($statement);
$sql_star->bind_param('s', $establishment_code);
$sql_star->execute();
$sql_star->bind_result($star_grading);
$sql_star->fetch();
$sql_star->close();

//Log View
/*$statement = "INSERT INTO nse_establishment_searchlog (establishment_code, search_position, search_keyword, domain, search_type) VALUES (?, ?, ?,?,?)";
$sql_log = $GLOBALS['dbCon']->prepare($statement);
$sql_log->bind_param('sssss', $establishment_code, $_GET['sp'], $search_string, $_SERVER['HTTP_HOST'], $search_type);
$sql_log->execute();
$sql_log->close();*/

//Get Image
$statement = "SELECT image_name FROM nse_establishment_images WHERE establishment_code=? ORDER BY primary_image DESC";
$sql_images = $GLOBALS['dbCon']->prepare($statement);
$sql_images->bind_param('s', $establishment_code);
$sql_images->execute();
$sql_images->store_result();
$sql_images->bind_result($image);
while($sql_images->fetch()) {
	if (is_file($_SERVER['DOCUMENT_ROOT'] . "/res_images/$image")) {
		list($width, $height, $type, $attrib) = getimagesize($_SERVER['DOCUMENT_ROOT'] . "/res_images/$image");
		$src_image = $_SERVER['DOCUMENT_ROOT'] . "/res_images/$image";
		//if ($width > $height) {
			$empty_image = FALSE;
			break;
		//} else {
		//	$width = 0;
		//	$height = 0;
		//}
	} else {
		$empty_image = TRUE;
//		$src_image = $_SERVER['DOCUMENT_ROOT'] . "/images/nophoto.png";
//		list($width, $height, $type, $attrib) = getimagesize($src_image);
	}
}
$sql_images->close();

$im = imagecreatetruecolor($maxWidth,$maxHeight);
if ($_GET['bg'] == 'ffffff') {
	$bg_color = imagecolorallocate($im, 255, 255, 255 );
} else {
	$bg_color = imagecolorallocate($im, 217, 217, 217);
}
	
	
//Get & Copy Image
if ($empty_image) {
	//imagedestroy($im);
	$noImage = imagecreatefrompng($src_image);
	imagefilledrectangle($im, 0, 0, 150, 88, $bg_color);
	imagecopy($im, $noImage, 0, 0, 0, 0, 150, 88);
} else {
	//Calculate src image ratio
	$ratio = $maxHeight/$width;
	if ($height < $maxHeight/$ratio) {
		$ratio = $maxHeight/$height;
	}
	$srcWidth =(int)  $maxWidth/$ratio;
	$srcHeight = (int) $maxHeight/$ratio;
	
	if ($srcWidth > $width) $srcWidth = $width;
	$src_img = imagecreatefromjpeg($src_image);
	
	imagecopyresampled($im, $src_img, 0, 0, 0, 0, $maxWidth, $maxHeight, $srcWidth, $srcHeight);
}

//Add Star Grading
//$star = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/images/star.png');
//$yPos = $maxHeight - 17;
//$stxPos = 16;
//for ($x=1; $x<$star_grading+1; $x++) {
//    $xPos = $maxWidth - ($stxPos * $x);
//    imagecopy($im, $star, $xPos, $yPos, 0, 0, 16, 17);
//}
imagejpeg($im);
imagedestroy($im);
?>