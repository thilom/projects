<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/shared/securimage/securimage.php';

$content = file_get_contents("content/$page.html");
$message = '';
$js_values = '';
$c=0;
$hidden_fields = array('recipients','return','heading','subject','seccode#DNS');


if (count($_POST)>0) {

	 //CAPTCHA
	$img = new Securimage();
	$valid = $img->check($_POST['seccode#DNS']);

	if($valid == false) {

		$message = "<div style='color: red; font-weight: bold; text-align: center; background-color: silver; border: 1px dotted gray'>Sorry, the code you entered was invalid. Please Try Again.</div>";
		foreach ($_POST as $key=>$value) {
		 if ((in_array($key, $hidden_fields))) continue;
			$js_values .= "js_values[$c] = '$value'" . PHP_EOL;
			$js_keys .= "js_keys[$c] = '".str_replace('_', ' ', $key)."'" . PHP_EOL;

			$c++;
		}

	} else {
		include 'post_form.php';
	}
}

$content = str_replace('!uniqid!', md5(uniqid(time())), $content);
$content = str_replace('<!-- message -->', $message, $content);
$content = str_replace("'!js_values!'", $js_values, $content);
$content = str_replace("'!js_keys!'", $js_keys, $content);
?>