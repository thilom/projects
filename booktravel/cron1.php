<?php
/**
 * Cron job to update generated pages.
 */

//Vars
$base_dir = '/usr/www/users/aatrae';
ini_set("include_path",".:/usr/lib/php:/usr/local/lib/php:/usr/www/users/aatrae");
$export_code = '';
$level = '';
$current_id = '';
$export_category = '';
$export_dir = '';
$trail = '';
$old_thread_id = '';

//Includes
include_once $base_dir . '/aa_init/init.php';
include_once $base_dir . "/export/pageUpdate.class.php";

//Get previous thread ID

$statement = "SELECT variable_value FROM nse_variables WHERE variable_name='last_thread_id'";
$sql_old_thread = $GLOBALS['dbCon']->prepare($statement);
$sql_old_thread->execute();
$sql_old_thread->store_result();
$sql_old_thread->bind_result($old_thread_id);
$sql_old_thread->fetch();
$sql_old_thread->free_result();
$sql_old_thread->close();

//UPdate with new thread ID
$new_thread_id = $GLOBALS['dbCon']->thread_id;
$statement = "UPDATE nse_variables SET variable_value=? WHERE variable_name='last_thread_id'";
$sql_new_thread = $GLOBALS['dbCon']->prepare($statement);
$sql_new_thread->bind_param('s', $new_thread_id);
$sql_new_thread->execute();
$sql_new_thread->close();

//Kill thread from previous run
$GLOBALS['dbCon']->kill($old_thread_id);


//Instantiate
$gen = new pageUpdate();

//Prepare Statement - Set to complete
$statement = "UPDATE nse_export SET complete = 'Y' WHERE export_id=?";
$sql_update = $GLOBALS['dbCon']->prepare($statement);

//Get List of pages marked for export
$statement = "SELECT export_id, level, current_id, export_category, base_dir, trail FROM nse_export WHERE complete IS NULL";
$sql_list = $GLOBALS['dbCon']->prepare($statement);
$sql_list->execute();
$sql_list->store_result();
$sql_list->bind_result($export_code, $level, $current_id, $export_category, $export_dir, $trail);
while ($sql_list->fetch()) {
	switch($level) {
		case 'province':
			$gen->generate_province($current_id, $export_category, $export_dir, $trail);
			break;
		case 'town':
			$gen->generate_town($current_id, $export_category, $export_dir, $trail);
			break;
		case 'suburb':
			$gen->generate_suburb($current_id, $export_category, $export_dir, $trail);
			break;
	}
	$sql_update->bind_param('s', $export_code);
	$sql_update->execute();
}
$sql_list->free_result();

//Close Prepared Statements
$sql_list->close();
$sql_update->close();

?>