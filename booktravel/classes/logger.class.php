<?php

/**
 * General logging class
 *
 * 2016-03-23: Thilo Muller - Created
 */
class Logger {

    function __construct() {
        if (!isset($GLOBALS['dbCon']) || empty($GLOBALS['dbCon'])) {

//            require_once $_SERVER['DOCUMENT_ROOT'] . '/class.config.php';
//            $config2 = new config();

            $dbName = 'booktravel';
            $dbHost = 'localhost';
            $dbUser = 'root';
            $dbPass = 'root';

            try {
                $connect = "mysql:host=$dbHost;dbname=$dbName";
                $GLOBALS['dbCon'] = new PDO($connect, $dbUser, $dbPass);
                $GLOBALS['dbCon']->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $GLOBALS['dbCon']->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
            } catch (PDOException $e) {
                echo 'Failed to connect database';
            }
        }
    }


    function log_search_time($search_string, $elapsed_time) {

        $log_hash = SHA1($search_string);

        $statement = "INSERT INTO stats_script_log
                          (log_date, log_parameters, log_hash, log_elapsed_time)
                        VALUES
                          (NOW(), :log_parameters, :log_hash, :log_elapsed_time)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bindParam(':log_parameters', $search_string);
        $sql_insert->bindParam(':log_hash', $log_hash);
        $sql_insert->bindParam(':log_elapsed_time', $elapsed_time);
        $sql_insert->execute();
        $sql_insert->closeCursor();

    }

    function log_curl_time($search_string, $elapsed_time) {

        $log_hash = SHA1($search_string);

        $statement = "INSERT INTO stats_curl_log
                          (log_date, log_parameters, log_hash, log_elapsed_time)
                        VALUES
                          (NOW(), :log_parameters, :log_hash, :log_elapsed_time)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bindParam(':log_parameters', $search_string);
        $sql_insert->bindParam(':log_hash', $log_hash);
        $sql_insert->bindParam(':log_elapsed_time', $elapsed_time);
        $sql_insert->execute();
        $sql_insert->closeCursor();

    }

    function log_function_time($search_string, $elapsed_time, $function_time) {
//        $search_string = stripslashes($search_string);
        $log_hash = SHA1($search_string);

        $statement = "INSERT INTO stats_function_log
                          (log_date, log_parameters, log_hash, log_elapsed_time, session_id, log_order, log_function_time)
                        VALUES
                          (NOW(), :log_parameters, :log_hash, :log_elapsed_time, :session_id, :log_order, :log_function_time)";
        $sql_insert = $GLOBALS['dbCon']->prepare($statement);
        $sql_insert->bindParam(':log_parameters', $search_string);
        $sql_insert->bindParam(':log_hash', $log_hash);
        $sql_insert->bindParam(':log_elapsed_time', $elapsed_time);
        $sql_insert->bindParam(':session_id', $GLOBALS['session']);
        $sql_insert->bindParam(':log_order', $GLOBALS['log_order']);
        $sql_insert->bindParam(':log_function_time', $function_time);
        $sql_insert->execute();
        $sql_insert->closeCursor();

    }

}