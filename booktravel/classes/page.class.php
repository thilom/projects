<?php

/**
 * Created by PhpStorm.
 * User: thilom
 * Date: 2016/08/08
 * Time: 2:43 PM
 */
class Page {


    /**
     * return a list of all pages
     *
     * @param string $domain bt|eti
     * @return mixed
     */
    function get_pages($domain) {
        $statement = "SELECT page_id, page_code, page_name, page_response
                        FROM nse_pages
                        WHERE page_domain = :domain";
        $sql_select = $GLOBALS['db_pdo']->prepare($statement);
        $sql_select->bindParam(':domain', $domain);
        $sql_select->execute();
        $sql_result = $sql_select->fetchAll(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }


    /**
     * Get a specific page
     *
     * @param int $page_id
     * @return mixed
     */
    function get_page($page_id) {

        $statement = "SELECT page_id, page_name, page_code, page_response, page_content
                        FROM nse_pages
                        WHERE page_id =:page_id
                        LIMIT 1";
        $sql_select = $GLOBALS['db_pdo']->prepare($statement);
        $sql_select->bindParam(':page_id', $page_id);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    /**
     * Get a specific page
     *
     * @param int $page_id
     * @return mixed
     */
    function get_page_by_code($page_code, $page_domain) {

        $statement = "SELECT page_id, page_name, page_code, page_response, page_content
                        FROM nse_pages
                        WHERE page_code =:page_code
                          AND page_domain =:page_domain
                        LIMIT 1";
        $sql_select = $GLOBALS['dbPDO']->prepare($statement);
        $sql_select->bindParam(':page_code', $page_code);
        $sql_select->bindParam(':page_domain', $page_domain);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result;
    }

    function save_page($page_id, $page_data) {

        if ($page_id == 0) {
            $statement = "INSERT INTO nse_pages
                              (page_code, page_name, page_content, page_response, page_domain)
                            VALUES
                              (:page_code, :page_name, :page_content, :page_response, :page_domain)";
            $sql_insert = $GLOBALS['db_pdo']->prepare($statement);
            $sql_insert->bindParam(':page_name', $page_data['page_name']);
            $sql_insert->bindParam(':page_domain', $page_data['page_domain']);
            $sql_insert->bindParam(':page_content', $page_data['page_content']);
            $sql_insert->bindParam(':page_response', $page_data['page_response']);
            $sql_insert->bindParam(':page_code', $page_data['page_code']);
            $sql_insert->execute();
            $sql_insert->closeCursor();
        } else {
            $statement = "UPDATE nse_pages
                            SET page_name = :page_name,
                                page_content = :page_content,
                                page_response = :page_response
                            WHERE page_id = :page_id
                            LIMIT 1";
            $sql_update = $GLOBALS['db_pdo']->prepare($statement);
            $sql_update->bindParam(':page_name', $page_data['page_name']);
            $sql_update->bindParam(':page_content', $page_data['page_content']);
            $sql_update->bindParam(':page_response', $page_data['page_response']);
            $sql_update->bindParam(':page_id', $page_id);
            $sql_update->execute();
            $sql_update->closeCursor();
        }
    }


    /**
     * Check a page code for uniqueness
     *
     * @param $code
     * @param $domain
     * @return mixed
     */
    function check_code($code,$domain) {

        $statement = "SELECT COUNT(*) As c
                        FROM nse_pages
                        WHERE page_code = :page_code
                          AND page_domain = :page_domain";
        $sql_select = $GLOBALS['db_pdo']->prepare($statement);
        $sql_select->bindParam(':page_code', $code);
        $sql_select->bindParam(':page_domain', $domain);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result['c'];
    }


    function page_count($domain) {

        $statement = "SELECT COUNT(*) AS c
                        FROM nse_pages
                        WHERE page_domain = :domain";
        $sql_select = $GLOBALS['db_pdo']->prepare($statement);
        $sql_select->bindParam(':domain', $domain);
        $sql_select->execute();
        $sql_result = $sql_select->fetch(PDO::FETCH_ASSOC);
        $sql_select->closeCursor();

        return $sql_result['c'];
    }

}