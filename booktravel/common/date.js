// JavaScript Document

function isLeapYear(year)
{
   if(((year % 4)==0) && ((year % 100)!=0) || ((year % 400) == 0))
   {
       return true;
   }
   else
   {
       return false;
   }
}

function getMonth(monYear)
{
	var mon = monYear.substring(0, 2);
	return mon;
}

function getYear(monYear)
{
	var year = monYear.substring(2, 7);
	return year;
}

function buildMonYear(mon, year)
{
	var monYear = bufferNum(mon) + ' ' + year;
	return monYear;
}

function returnMonthShortName(month)
{
	switch (month)
	{
		case 1:
			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "May";
		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Oct";
		case 11:
			return "Nov";
		case 12:
			return "Dec";
		default:
			return "Jan";
	}
}

function validateDate (year, month, day)
{

  if (month == 2)
  {
    //check if leap year
    if (isLeapYear (year))
    {
       if (day > 29)
       {
          return "FEB29";
       }     
    }
    else
    {
       if (day > 28)
       {
          return "FEB28";
       }
    }
    return true;
  }
  
  if (month == 4 || month == 6 || month == 9 || month == 11)
  {
     if (day > 30)
     {
     	return "MONTH30" 
     }
     return true;
  }
  return true;
}

function travelDateInFuture (year, month, day)
{
   var postDate = "PASTDATE";	
   year++; month++; day++;
   year--; month--; day--;
   
   var thisDate = new Date ();
   
   thisDay = thisDate.getDate();
   thisMonth = thisDate.getMonth() + 1;
   thisYear = thisDate.getFullYear();
   
   if (thisYear > year)
   {
      return postDate;
   }
   else
   {
	  if (thisYear == year)
   	  {
      	 if (thisMonth > month)
     	 { 
         	return postDate;
      	 }
         else if (thisMonth == month)
      	 {  
        	if (thisDay > day)
         	{
            	return postDate;
         	} 
      	 }
      }	
   }
   return true;
}

function totalDays (year, month, day)
{
    year++; month++; day++;  //strange way to cast but it works...
    year--; month--; day--;
    
    var thisDate = new Date ();
    
    thisDay = thisDate.getDate();
    thisMonth = thisDate.getMonth() + 1;
    thisYear = thisDate.getFullYear();

    var totalNumOfDays = 0;
    var travelDays = 0;
    var dayOfYear = 0;
    
    //make sure the given date is valid
    var validDate = validateDate (year, month, day);
    if (validDate != true)
    {
    	return validDate;
    }
    
    //make sure this date is in the future
    var badDate = travelDateInFuture (year, month, day);
    if (badDate == false)
    {
    	return badDate;    
    }

    if (isLeapYear(year))
    {
       retMonthArray = new Array (0,31,60,91,121,152,182,213,244,274,305,335);
    }
    else
    {
       retMonthArray = new Array (0,31,59,90,120,151,181,212,243,273,304,334);
    }
   
    if (isLeapYear(thisYear))
    {
       currMonthArray = new Array (0,31,60,91,121,152,182,213,244,274,305,335);
       MAX_DAYS_IN_CURR_YEAR = 366;
    }
    else
    {
       currMonthArray = new Array (0,31,59,90,120,151,181,212,243,273,304,334);
       MAX_DAYS_IN_CURR_YEAR = 365;
    }

    dayOfYear = currMonthArray[thisMonth];  //this will give us total number of days up to the beginning of the current month
    dayOfYear = dayOfYear + thisDay;        //...and this, to the current day 

    if (thisYear < year)
    {
	    travelDays = retMonthArray[month];
   	    travelDays = travelDays + day;
   	    totalNumOfDays = ((MAX_DAYS_IN_CURR_YEAR - dayOfYear) + travelDays);
    }
    else
    {
        travelDays = currMonthArray[month];
    	travelDays = travelDays + day;
        totalNumOfDays = travelDays - dayOfYear;	
    }
    
    return totalNumOfDays;
}

function DateOrder (depYear, depMonth, depDay, retYear, retMonth, retDay)
{
	var orderError = "InvalidORDER";
    //strange way of casting, but it works best..
    retYear++; retYear--;
    retMonth++; retMonth--;
    retDay++; retDay--;
    
    depYear++; depMonth++; depDay++;
    depYear--; depMonth--; depDay--;
    
    var daysInAdvance;
	var curr_date = new Date();

    curr_day = curr_date.getDate();
    curr_month = curr_date.getMonth() + 1;
    curr_year = curr_date.getFullYear();

    // make sure we have valid departure dates...    
    var badDateOne = validateDate (depYear, depMonth, depDay);
    
    if (badDateOne != true)
    {
    	return badDateOne;
    }
    
    // validate return dates...
    var badDateTwo = validateDate (retYear, retMonth, retDay);
    
    if (badDateTwo != true)
    {
    	return badDateTwo;
    }

    //make sure the travel dates are in the future...
    var postDateOne = travelDateInFuture (depYear, depMonth, depDay);
    if (postDateOne != true)
    {
       return postDateOne;
    }

    var postDateTwo = travelDateInFuture (retYear, retMonth, retDay);  
    if (postDateTwo != true)
    {
       return postDateTwo;
    }

    //make sure return date is after departure date
    if (retYear < depYear)
    {
       return orderError;
    }
    else
    {
       	if (retYear == depYear)
       	{
       		if (retMonth < depMonth)
       		{
    	   		return orderError;
       		}
       		else 
		    {
        	    if (retMonth == depMonth)
           		{
	    	   		if (retDay < depDay)
    	       		{	
    		   			return orderError;
           	   		}
           	   		else if (retDay == depDay)
           	   		{
           	   			return "sameDATE";
           	   		}
           		}		
       		}
       	}	
    }
    return true;
}


function AddDays (num_days) {

	var d = new Date ();
	var TodayDay = d.getDate();
	var TodayMonth =d.getMonth() + 1;
	var TodayYear =d.getFullYear();
	var Month_Array = new Array;
	var ChangeDate = TodayDay + num_days;
	var NewDay = 0;
	var NewMonth = 0;
	var NewYear = 0;
	
	CheckLeap = isLeapYear (TodayYear);
		
	
	
	if (TodayMonth == 2) {
		if (CheckLeap == true) {
			if (ChangeDate > 29) {
				NewDay = ChangeDate - 29;
				NewMonth = 1;
			} else {
				NewDay = ChangeDate;
				NewMonth = 0;
			}
		} else {
				
			if (ChangeDate > 28) {
				NewDay = ChangeDate - 28;
				NewMonth = 1;
			} else {
				NewDay = ChangeDate;
				NewMonth = 0;
			}
				
		}
		
		
	} else if (( TodayMonth == 4)|| ( TodayMonth== 6) || ( TodayMonth== 9) || ( TodayMonth== 11)) {
		if (ChangeDate > 30) {
			NewDay = ChangeDate - 30;
			NewMonth = 1;
			NewYear = 0;
		} else {
			NewDay = ChangeDate;
			NewMonth = 0;
			NewYear = 0;
		}			
	} else if (( TodayMonth == 1) || ( TodayMonth == 3) || ( TodayMonth == 5) || ( TodayMonth == 7) || ( TodayMonth == 8)|| ( TodayMonth== 10)) {
		if (ChangeDate > 31) {
			NewDay = ChangeDate - 31;
			NewMonth = 1;
			NewYear = 0;
		} else {
			NewDay = ChangeDate;
			NewMonth = 0;
			NewYear = 0;
		}
	
	} else if (TodayMonth == 12) {
		if (ChangeDate > 31) {
			NewDay = ChangeDate - 31;
			NewMonth = 1;
			NewYear = 1;
		} else {
			NewDay = ChangeDate;
			NewMonth = 0;
			NewYear = 0;
		}
		
	}
	
	var Date_Array = new Array();
	
	Date_Array[0] = NewDay;
	
	if ((TodayMonth == 12) && (NewMonth == 1)) {
		Date_Array[1] = 1;	
	} else {
		Date_Array[1] = TodayMonth + NewMonth;	
	}
	Date_Array[2] = NewYear;		
	
	//alert ("Day: " + Date_Array[0] + "\nMonth: " + Date_Array[1] + "\nYear: " + Date_Array[2]);
	return Date_Array;
}