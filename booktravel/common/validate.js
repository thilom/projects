function ValidateInput() {
		
	if (document.fliteform.arriveat.value == "") {
			alert ("We cannot send you nowhere, so please select a destination city...");
			return false;	
	}
	
	if (document.fliteform.departfrom.value == document.fliteform.arriveat.value) {
			alert ("We cannot send you to the same place you are leaving from, so please make sure your From and To city's differ...");
			return false;	
	}

	var destYear;    var destMonth;   var destDay;     
    var departYear;  var departMonth; var departDay;   

	destYear = getYear(document.fliteform.rMonYearTemp.options[document.fliteform.rMonYearTemp.selectedIndex].value);
	destMonth = getMonth(document.fliteform.rMonYearTemp.options[document.fliteform.rMonYearTemp.selectedIndex].value);
	destDay = document.fliteform.rDayTemp.options[document.fliteform.rDayTemp.selectedIndex].value;

	departYear = getYear(document.fliteform.dmonyear.options[document.fliteform.dmonyear.selectedIndex].value);
	departMonth = getMonth(document.fliteform.dmonyear.options[document.fliteform.dmonyear.selectedIndex].value);
	departDay = document.fliteform.dday.options[document.fliteform.dday.selectedIndex].value;

	var valDate = DateOrder (departYear, departMonth, departDay, destYear, destMonth, destDay);

	if (valDate != true) {
			switch (valDate) {
  
				case "FEB29": 
    			alert ("February only has 29 days this year...");
  				return false;
  				break;
		
  			case "FEB28":
				alert ("February only has 28 days this year...");
  				return false;
  				break;

			case "MONTH30":
   	    	
				if (document.fliteform.trip[1].checked) {
					} else {
  					alert ("The month you have chosen only has 30 days...");
  					return false;
       			}		
					break;
				
			case "PASTDATE":
				if (document.fliteform.trip[1].checked) {
				} else {
   					alert ("Unfortunately we cannot send you into the past, so please make sure your dates are for the future...");
      				return false;
				}	
      			break;

    		case "InvalidORDER" :

		    	if (document.fliteform.trip[1].checked) {
					} else {
					alert ("We cannot return you before you leave so please make sure your return date is for after you leave..."); 
		      		return false;
					}	
				break;

   			case "sameDATE":
				break;  		
 		}

	}


	/**the 339 days issue**/
	var number_of_days = totalDays (destYear, destMonth, destDay);
    
	if (number_of_days > 339) {
	    	    
		if (document.fliteform.trip[1].checked) {
		} else {
			
				alert ("Sorry, we don't book flights more than 339 days into the future...");
				return false;
	
		}	

	}

	var Num_Adults = parseInt (document.fliteform.numadults.value);
	var Num_Children = parseInt (document.fliteform.numchildren.value);
	var Num_bugs = parseInt (document.fliteform.numinfants.value);
	var Tot_Passengers = Num_Adults + Num_Children + Num_bugs;

	if (parseInt (Tot_Passengers) > 9) {
		alert ("The total number of passengers cannot exceed 9");		
		return false;
	}	
	
    return;
}