var bannerImg = new Array();
  // Enter the names of the images below
  bannerImg[0]="images/sky.jpg";
  bannerImg[1]="images/sky.jpg";
  bannerImg[2]="images/centre03.jpg";
  bannerImg[3]="images/centre04.jpg";
  bannerImg[4]="images/centre05.jpg";
  bannerImg[5]="images/centre07.jpg";
  bannerImg[6]="images/centre08.jpg";
  bannerImg[7]="images/centre14.jpg";
  bannerImg[8]="images/centre11.jpg";
  bannerImg[9]="images/centre10.jpg";
  bannerImg[10]="images/centre15.jpg";

var newBanner = 0;
var totalBan = bannerImg.length;

function cycleBan() {
  newBanner++;
  if (newBanner == totalBan) {
    newBanner = 0;
  }
  document.banner.src=bannerImg[newBanner];
  // set the time below for length of image display
  // i.e., "4*1000" is 4 seconds
  setTimeout("cycleBan()", 4*1000);
}
window.onload=cycleBan;