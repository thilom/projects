// JavaScript Document

function disablereturn() {
	document.fliteform.rMonYearTemp.value = 'oneway';
	document.fliteform.rDayTemp.value = 'oneway';
	document.fliteform.rmonyear.value = 'oneway';
	document.fliteform.rday.value = 'oneway';
	document.fliteform.rDayTemp.disabled = true;
	document.fliteform.rMonYearTemp.disabled = true;
}


function enablereturn() 
{
	document.fliteform.rDayTemp.disabled = false;
	document.fliteform.rMonYearTemp.disabled = false;
	
	var xfrm = document.forms['fliteform'];
	var xToday = new Date();
	var xTodaymSec = xToday.getTime();
	var x2wk = new Date(xTodaymSec + 1209600000) // 14days from now 
	var x3wk = new Date(xTodaymSec + 1814400000) // 21days from now 
	var xdmon = String((x2wk.getMonth() * 1) + 1);
	var xdmonyear = xdmon + " " + String(x2wk.getFullYear());
	if (xdmon < 10) { xdmonyear = (String("0") + xdmonyear); }

	// --- guess return date ----------
	var xrmon = String((x3wk.getMonth() * 1) + 1);
	var xrmonyear = xrmon + " " + String(x3wk.getFullYear());
	if (xrmon < 10) { xrmonyear = (String("0") + xrmonyear); }
	for (var idx=0; idx < xfrm.elements['rMonYearTemp'].length; idx++) {
		var xval =  xfrm.elements['rMonYearTemp'].options[idx].value;
		if (xval == xrmonyear) {
			xfrm.elements['rMonYearTemp'].selectedIndex = idx;
			break;
		}
	}
	var xday = x3wk.getDate();
	for (idx=0; idx < xfrm.elements['rDayTemp'].length; idx++) {
		var xval =  xfrm.elements['rDayTemp'].options[idx].value;
		if (xval == xday) {
			xfrm.elements['rDayTemp'].selectedIndex = idx;
			break;
		}
	}
	document.fliteform.rmonyear.value = xrmonyear;
	document.fliteform.rday.value = xday;
}

function onSelectRetDay()
{
	document.fliteform.rday.value = document.fliteform.rDayTemp.value;
}

function onSelectRetMonYear()
{
	document.fliteform.rmonyear.value = document.fliteform.rMonYearTemp.value;
}

function fixDate(date) {
  var base = new Date(0);
  var skew = base.getTime();
  if (skew > 0)
    date.setTime(date.getTime() - skew);
}

function getDefaultCountry () {

	var CheckCountry = getCookie ('SelectCountry');
		CheckCountry = parseInt (CheckCountry);
	var DefaultCountry = 0;
	
	if (CheckCountry) {
		DefaultCountry = CheckCountry + 1;
	} else {
		DefaultCountry = 1;
	}
	//DefaultCountry=3;
	return parseInt (DefaultCountry);
}

function bufferNum(num)
{
	if(num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6 || num == 7 || num == 8 || num == 9)
		return '0' + num;
	else
		return num;
}
