<?php
/**
 * Establishment Redirect
 * 
 *  Redirects a link from nightsbridge to the correct html page of an establishment.
 *  
 *  @author Thilo Muller(2009)
 *  @package RVBus
 *  @category general
 */

/**
 * @var string Establishment Code
 */
$code = $_GET['code'];

/**
 * @var string Establishment Name
 */
$establishment_name = '';

//includes
include 'aa_init/init.php';

//Get establishment name
$statement = "SELECT establishment_name FROM nse_establishment WHERE establishment_code=?";
$sql_name = $dbCon->prepare($statement);
$sql_name->bind_param('s', $code);
$sql_name->execute();
$sql_name->bind_result($establishment_name);
$sql_name->store_result();
$sql_name->fetch();
$sql_name->close();

//Create Link
$link = strtolower(str_replace(' ', '_', "$code $establishment_name"));
$link = str_replace(array("'",'(',')','"','=','+','[',']',',','/','\\'), '', $link);
$link = str_replace('&#212;', 'o', $link);
$file_name = "/accommodation/$link.html";

//Redirect
if (!empty($establishment_name)) {
    header( "Location: http://booktravel.travel$file_name");
} else {
    header( "Location: http://booktravel.travel/index.php?p=error&id=noEstab");
}
?>