<?php
/**
 * Send a message to info@booktravel.travel
 *
 * 2015-11-24: Thilo Muller - Created
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

//Includes
require_once 'aa_init/init.php';
require_once 'aa_init/sql.class.php';
require_once 'shared/PHPMailer/class.phpmailer.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

//Vars
$mail = new PHPMailer();
$message_from_name = $_POST['yourName'];
$message_from_email = $_POST['yourEmail'];
$message_from_db = "{$_POST['yourName']} <{$_POST['yourEmail']}>";
$message_subject = empty($_POST['yourSubject'])?'No Subject Entered':$_POST['yourSubject'];
$message_content = $_POST['yourMessage'];

//Save message
$statement = "INSERT INTO nse_emails
                  (message_from, message_subject, message_content)
                VALUES
                  (:message_from, :message_subject, :message_content)";
$sql_insert = $GLOBALS['dbPDO']->prepare($statement);
$sql_insert->bindParam(':message_from', $message_from_db);
$sql_insert->bindParam(':message_subject', $message_subject);
$sql_insert->bindParam(':message_content', $message_content);
$sql_insert->execute();
$sql_insert->closeCursor();

//Send message
$mail->AddReplyTo($message_from_email,$message_from_name);
$mail->SetFrom($message_from_email,$message_from_name);
$mail->AddAddress('info@booktravel.travel',"Booktravel");
//$mail->AddAddress('thilo@palladianbytes.co.za',"Thilo Muller");
$mail->Subject = $message_subject;
$mail->MsgHTML($message_content);
$mail->Send();


