<?php 

//Includes 
include_once $_SERVER['DOCUMENT_ROOT'] . '/aa_init/init.php';

if (isset($_GET['p']) && $_GET['p'] == 'townList') {
	$tID = '';
	$tName = '';
	$town_list = array();
	
	//Prepare Statement - Pseudo-towns
	$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=? && pseudo_town='1'";
	$sql_pseudo = $GLOBALS['dbCon']->prepare($statement);
	
	$data = '';
	$province = $_GET['pr'];
	$statement = "SELECT town_id, town_name FROM nse_location_town WHERE province_id=? ORDER BY town_name";
	$sql_town = $GLOBALS['dbCon']->prepare($statement);
	$sql_town->bind_param('i', $province);
	$sql_town->execute();
	$sql_town->bind_result($tID, $tName);
	$sql_town->store_result();
	while ($sql_town->fetch()) {
		if (substr($tName, 0, 3) == 'Aaa') continue;
		if (strpos($tName, '***') != 0) continue;
		
		//Check for suburbs as towns
		$sql_pseudo->bind_param('s', $tID);
		$sql_pseudo->execute();
		$sql_pseudo->store_result();
		if ($sql_pseudo->num_rows > 0) {					
			$sql_pseudo->bind_result($suburb_id, $suburb_name);
			while ($sql_pseudo->fetch()) {
				$sName = strtolower(str_replace(' ', '_', $suburb_name));
				$town_list["s$suburb_id*$tID"] = $suburb_name;
			}
			$sql_pseudo->free_result();
		}
		
		//$tName = str_replace('&acutes', '', $tName);
		$town_list[$tID] = $tName;
		//$data .= "$tID%$tName|";
	}
	$sql_pseudo->close();

	asort($town_list);
	foreach($town_list as $id=>$name) {
		$data .= "$id%$name|";
	}
	
	$data = substr($data, 0, -1);
	$data = str_replace("'", '', $data);
	
	echo "<script>parent.returnTowns('$data')</script>";
}

if (isset($_GET['p']) && $_GET['p'] == 'suburbList') {
	$data = '';
	$town = $_GET['tn'];
	$statement = "SELECT suburb_id, suburb_name FROM nse_location_suburb WHERE town_id=? ORDER BY suburb_name";
	$sql_suburb = $dbCon->prepare($statement);
	$sql_suburb->bind_param('i', $town);
	$sql_suburb->execute();
	$sql_suburb->bind_result($sID, $sName);
	$sql_suburb->store_result();
	while ($sql_suburb->fetch()) {
		//$sName = str_replace('&acutes', '', $sName);
		$data .= "$sID%$sName|";
	}
	$data = substr($data, 0, -1);
	$data = str_replace("'", '', $data);
	
	
	echo "<script>parent.returnSuburbs('$data')</script>";
}

if (isset($_GET['p']) && $_GET['p'] == 'provinceList') {
	$data = '';
	$country = $_GET['pr'];
	$statement = "SELECT province_id, province_name FROM nse_location_province WHERE country_id=? ORDER BY province_name";
	$sql_country = $dbCon->prepare($statement);
	echo mysqli_error($dbCon);
	$sql_country->bind_param('i', $country);
	$sql_country->execute();
	$sql_country->bind_result($pID, $pName);
	$sql_country->store_result();
	while ($sql_country->fetch()) {
		$data .= "$pID%$pName|";
	}
	$data = substr($data, 0, -1);
	$data = str_replace("'", '', $data);
	
	echo "<script>parent.returnProvinces('$data')</script>";
	
	if ($data == '') {
		$data = '';
		$statement = "SELECT town_id, town_name FROM nse_location_town WHERE country_id=? ORDER BY town_name";
		$sql_town = $dbCon->prepare($statement);
		$sql_town->bind_param('i', $country);
		$sql_town->execute();
		$sql_town->bind_result($tID, $tName);
		$sql_town->store_result();
		while ($sql_town->fetch()) {
			if (substr($tName, 0, 3) == 'Aaa') continue;
			if (strpos($tName, '***') != 0) continue;
			$data .= "$tID%$tName|";
		}
		$data = substr($data, 0, -1);
		$data = str_replace("'", '', $data);
		
		echo "<script>parent.returnTowns('$data')</script>";
	} else {
		echo "<script>parent.returnTowns('')</script>";
	}
	echo "<script>parent.returnSuburbs('')</script>";
}
?>