<?php 

	//Includes
	include 'aa_init/init.php';	

	$statement = "DELETE FROM nse_export";
	$sql_est = $dbCon->prepare($statement);
	$sql_est->execute();
	$sql_est->close();
	
	$statement = "DELETE FROM nse_export_temp";
	$sql_est = $dbCon->prepare($statement);
	$sql_est->execute();
	$sql_est->close();
	
	$statement = "UPDATE nse_establishment SET aa_estab=''";
	$sql_aa_clean = $GLOBALS['dbCon']->prepare($statement);
	$sql_aa_clean->execute();
	$sql_aa_clean->close();
	
	$statement = "UPDATE nse_establishment SET aa_estab ='Y' WHERE award_status != '' || qagrad = 'Y' || advertiser='P' || advertiser='L' || advertiser='A' || advertiser='B' || advertiser='S'";
	$sql_aa = $GLOBALS['dbCon']->prepare($statement);
	$sql_aa->execute();
	$sql_aa->close();
	
	if (isset($_GET['estab'])) {
		$statement = "DELETE FROM nse_export_establishments";
		$sql_est = $dbCon->prepare($statement);
		$sql_est->execute();
		$sql_est->close();
		
		
		$statement = "DELETE FROM nse_export_establishments_cache";
		$sql_est = $dbCon->prepare($statement);
		$sql_est->execute();
		$sql_est->close();
		
		$statement = "DELETE FROM nse_search_temp";
		$sql_search = $dbCon->prepare($statement);
		$sql_search->execute();
		$sql_search->close();
		
		//Check Establishment Temp Table
		$statement = "INSERT INTO nse_export_establishments
									(establishment_code)
									VALUES
									(?)";
		$sql_estabs_insert = $dbCon->prepare($statement);
		
		$statement = "INSERT INTO nse_export_establishments_cache
									(establishment_code, establishment_name)
									VALUES
									(?,?)";
		$sql_cache_insert = $dbCon->prepare($statement);
		
		$statement = "INSERT INTO nse_search_temp (establishment_code) VALUES (?)";
		$sql_search_insert = $dbCon->prepare($statement);
		
		$statement = "SELECT establishment_code, establishment_name
									FROM nse_establishment WHERE aa_estab='Y'";
		$sql_estabs_get = $dbCon->prepare($statement);
		$sql_estabs_get->execute();
		$sql_estabs_get->store_result();
		$sql_estabs_get->bind_result($est_code, $est_name);
		while ($sql_estabs_get->fetch()) {
			$sql_cache_insert->bind_param('ss', $est_code, $est_name);
			$sql_cache_insert->execute();
			$sql_estabs_insert->bind_param('s', $est_code);
			$sql_estabs_insert->execute();
			$sql_search_insert->bind_param('s', $est_code);
			$sql_search_insert->execute(); 
		}
	}

echo "<script>parent.resetTables_done()</script>";
?>