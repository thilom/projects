#ifndef BREWMANDB_H
#define BREWMANDB_H
#include <qsqldatabase.h>


class BrewmanDB
{
public:
    BrewmanDB();
    QSqlDatabase db;
    bool connectDB();
    void closeDB();
};

#endif // BREWMANDB_H
