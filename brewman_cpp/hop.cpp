#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "hop.h"
#include <QString>
#include <brewmandb.h>
#include <QDebug>

BrewmanDB DB;

Hop::Hop()
{
    QString saveHop(hopData hop);
    std::vector<hopData> getHopList(QString filter);
    hopData getHopData(int hopID);
    QString updateHop(hopData hop);
    QString deleteHop(int rowid);
    QString GetHopFormName(int hopFormID);
    QString GetHopTypeName(int hopTypeID);
}

QString Hop::saveHop(hopData hop)
{
    if (DB.connectDB()) {
        QSqlQuery query(QSqlDatabase::database("BrewmanDBConnect"));
        query.prepare("INSERT INTO hops (hopName, CountryOfOrigin, hopAlpha, hopDescription, hopType, hopForm, hopBeta) "
                      "VALUES "
                      "(:hopName, :hopOrigin, :hopAlpha, :hopDescription, :hopType, :hopForm, :hopBeta)");
        query.bindValue(":hopName", hop.hopName);
        query.bindValue(":hopOrigin", hop.hopOrigin);
        query.bindValue(":hopAlpha", hop.hopAlpha);
        query.bindValue(":hopBeta", hop.hopBeta);
        query.bindValue(":hopDescription", hop.hopDescription);
        query.bindValue(":hopType", hop.hopType);
        query.bindValue(":hopForm", hop.hopForm);
        if (query.exec()) {
            DB.closeDB();
            return "Hop Saved";
        } else {
            DB.closeDB();
            return "Hop Save Failed: " + query.lastError().text();
        }
    } else {
        return "Connection Failed";
    }
}

std::vector<hopData> Hop::getHopList(QString filter)
{
    std::vector<hopData> retData;
    QString sqlString;
    if (filter.isEmpty()) {
        sqlString = "SELECT hopName, CountryOfOrigin, hopAlpha, rowid, hopForm, hopType "
                    "FROM hops "
                    "ORDER BY hopName ASC";
    } else {
        sqlString = "SELECT hopName, CountryOfOrigin, hopALpha, rowid, hopForm, hopType "
                    "FROM hops "
                    "WHERE hopName "
                    "LIKE '%"+ filter +"%' "
                    "ORDER BY hopName ASC";
    }
    if (DB.connectDB()) {
        QSqlQuery query(sqlString, QSqlDatabase::database("BrewmanDBConnect"));
        int hopNamePos = query.record().indexOf("hopName");
        int CountryOfOriginPos = query.record().indexOf("CountryOfOrigin");
        int hopAlphaPos = query.record().indexOf("hopAlpha");
        int hopFormPos = query.record().indexOf("hopForm");
        int hopTypePos = query.record().indexOf("hopType");
        int rowIDPos = query.record().indexOf("rowid");
        while (query.next()) {
            hopData hData;
            hData.hopName = query.value(hopNamePos).toString();
            hData.hopOrigin = query.value(CountryOfOriginPos).toString();
            hData.hopAlpha = query.value(hopAlphaPos).toDouble();
            hData.rowID = query.value(rowIDPos).toInt();
            hData.hopForm = query.value(hopFormPos).toInt();
            hData.hopType = query.value(hopTypePos).toInt();
            retData.push_back(hData);
        }
        DB.closeDB();
    }
    return retData;
}

hopData Hop::getHopData(int hopID)
{
    hopData hData;
    if (DB.connectDB()) {
        QSqlQuery query(QSqlDatabase::database("BrewmanDBConnect"));
        query.prepare("SELECT hopName, CountryOfOrigin, hopALpha, rowid, hopBeta, hopType, hopForm, hopDescription FROM hops WHERE rowid = :rowid");
        query.bindValue(":rowid", hopID);
        query.exec();
        int hopNamePos = query.record().indexOf("hopName");
        int hopCountryPos = query.record().indexOf("CountryOfOrigin");
        int hopAlphaPos = query.record().indexOf("hopAlpha");
        int hopBetaPos = query.record().indexOf("hopBeta");
        int hopFormPos = query.record().indexOf("hopForm");
        int hopTypePos = query.record().indexOf("hopType");
        int hopDescriptionPos = query.record().indexOf("hopDescription");
        while (query.next()) {
            hData.hopName = query.value(hopNamePos).toString();
            hData.hopOrigin = query.value(hopCountryPos).toString();
            hData.hopAlpha = query.value(hopAlphaPos).toDouble();
            hData.hopBeta = query.value(hopBetaPos).toDouble();
            hData.hopType = query.value(hopTypePos).toInt();
            hData.hopForm = query.value(hopFormPos).toInt();
            hData.hopDescription = query.value(hopDescriptionPos).toString();
            hData.rowID = hopID;
        }
    }
    qDebug() << "Fetched data for HopID: " << hopID;
    return hData;

}

QString Hop::updateHop(hopData hop) {
    if (DB.connectDB()) {
        QSqlQuery query(QSqlDatabase::database("BrewmanDBConnect"));
        query.prepare("UPDATE hops SET "
                      "hopName = :hopName, "
                      "hopDescription = :hopDescription, "
                      "CountryOfOrigin = :hopOrigin, "
                      "hopAlpha = :hopAlpha, "
                      "hopBeta = :hopBeta, "
                      "hopType = :hopType, "
                      "hopForm = :hopForm "
                      "WHERE rowid = :rowid ");
        query.bindValue(":hopName", hop.hopName);
        query.bindValue(":hopOrigin", hop.hopOrigin);
        query.bindValue(":hopAlpha", hop.hopAlpha);
        query.bindValue(":hopBeta", hop.hopBeta);
        query.bindValue(":hopDescription", hop.hopDescription);
        query.bindValue(":hopType", hop.hopType);
        query.bindValue(":hopForm", hop.hopForm);
        query.bindValue(":rowid", hop.rowID);
        if (query.exec()) {
            DB.closeDB();
            qDebug() << "RowID: " << hop.rowID;
            return "Hop Updated";
        } else {
            DB.closeDB();
            return "Hop Save Failed: " + query.lastQuery();
        }
    } else {
        return "Connection Failed";
    }
}

QString Hop::deleteHop(int rowid) {
    QString res;

    if (DB.connectDB()) {
        QSqlQuery query(QSqlDatabase::database("BrewmanDBConnect"));
        query.prepare("DELETE FROM hops WHERE rowid = :rowid");
        query.bindValue(":rowid", rowid);
        if (query.exec()) {
            res = "Hop Deleted";
        } else {
            res = "Hop Delete Failed";
        }
    }

    return res;
}

QString Hop::GetHopFormName(int hopFormID)
{
    switch (hopFormID) {
        case 1:
            return "Pellet";
            break;
        case 2:
            return "Leaf";
            break;
        case 3:
            return "Extract";
            break;
        default:
            return "-";
    }
}

QString Hop::GetHopTypeName(int hopTypeID)
{
    switch (hopTypeID) {
        case 1:
            return "Bittering";
            break;
        case 2:
            return "Aroma";
            break;
        case 3:
            return "Dual";
            break;
        default:
            return "-";
    }
}


