#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#ifndef HOP_H
#define HOP_H

struct hopData {
    QString hopName;
    QString hopDescription;
    QString hopOrigin;
    int hopType;
    int hopForm;
    double hopAlpha;
    double hopBeta;
    int hopStorageLoss;
    int rowID;
};

class Hop
{
public:
    Hop();
    QString saveHop(hopData hop);
    std::vector<hopData> getHopList(QString filter);
    hopData getHopData(int hopID);
    QString updateHop(hopData hop);
    QString deleteHop(int rowid);
    QString GetHopFormName(int hopFormID);
    QString GetHopTypeName(int hopTypeID);

    const static int HOP_TYPE_BITTERING = 1;
    const static int HOP_TYPE_AROMA = 2;
    const static int HOP_TYPE_DUAL = 3;

};

#endif // HOP_H
