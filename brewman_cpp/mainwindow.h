#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QDebug>
#include <QFileInfo>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionPreferences_triggered();

    void on_actionCreate_a_Recipe_triggered();

    void on_actionEdit_a_Recipe_triggered();

    void on_actionMange_Hops_triggered();

    void on_pushButton_clicked();

    void on_txtHopSearchValue_textChanged(const QString &arg1);

    void on_btnCancelAddHops_clicked();

    void on_btnSaveAddHops_clicked();

    void clearHopForm();

    void populateHopList(QString filter, int target);

    void on_tblHops_cellDoubleClicked(int row, int column);

    void populateHopForm(int hopID);

    void on_btnHopDelete_clicked();

    void on_btnAddIngredient_clicked();

    void on_tblAddHops_cellActivated(int row, int column);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
