#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdio.h>
#include <sqlite3.h>
#include <QtCore>
#include <QMessageBox>
#include <hop.h>
#include <string>
#include <QDebug>
#include <QString>

const int PAGE_MANAGE_HOPS = 3;
const int PAGE_ADD_HOPS = 8;
const int PAGE_ADD_INGREDIENT = 9;

const int HOP_TAGET_MANAGE = 0;
const int HOP_TAGET_ADD = 1;
const int HOP_TAGET_ALL = 2;

Hop hopObj;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0);
    ui->tblHops->setColumnHidden(3, true);
    ui->tblAddHops->setColumnHidden(4, true);
    ui->txtRowID->setHidden(true);

    QString filterString;
    this->populateHopList(filterString, HOP_TAGET_ALL);


//        QSqlQuery query ("SELECT RecipeName FROM Recipes");
//        int RecipeName = query.record().indexOf("RecipeName");
//        int rowCount = -1;
//        while (query.next()) {
//            rowCount = ui->tblRecipe->rowCount();
//            ui->tblRecipe->setRowCount(rowCount+1);
//            QString strRecipeName = query.value(RecipeName).toString();
//            QTableWidgetItem *newItem = new QTableWidgetItem(strRecipeName);
//            ui->tblRecipe->setItem(0, rowCount, newItem);
//        }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionPreferences_triggered()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_actionCreate_a_Recipe_triggered()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_actionEdit_a_Recipe_triggered()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::on_actionMange_Hops_triggered()
{
    ui->stackedWidget->setCurrentIndex(PAGE_MANAGE_HOPS);
}

void MainWindow::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(PAGE_ADD_HOPS);
    ui->btnSaveAddHops->setText("Save");
    ui->txtRowID->setText("0");
}

void MainWindow::on_txtHopSearchValue_textChanged(const QString &arg1)
{
    QString searchValue = ui->txtHopSearchValue->text();
    this->populateHopList(searchValue, HOP_TAGET_MANAGE);
}


void MainWindow::on_btnCancelAddHops_clicked()
{
    ui->stackedWidget->setCurrentIndex(PAGE_MANAGE_HOPS);
    this->clearHopForm();
}

void MainWindow::on_btnSaveAddHops_clicked()
{
    hopData hop;
    hop.hopName = ui->txtHopName->text();
    hop.hopDescription = ui->txtHopDescription->toPlainText();
    hop.hopOrigin = ui->txtHopOrigin->text();
    hop.hopAlpha = ui->spnHopAlpha->value();
    hop.hopBeta = ui->spnHopBeta->value();
    hop.hopType = ui->cmbHopType->currentIndex();
    hop.hopForm = ui->cmbHopForm->currentIndex();
    hop.rowID = ui->txtRowID->text().toInt();

    QString res;
    if (hop.rowID > 0) {
        res = hopObj.updateHop(hop);
    } else {
        res = hopObj.saveHop(hop);
    }
    ui->statusBar->showMessage(res);
    ui->stackedWidget->setCurrentIndex(PAGE_MANAGE_HOPS);
    this->clearHopForm();
    QString filterString;
    this->populateHopList(filterString, HOP_TAGET_MANAGE);
}

void MainWindow::clearHopForm()
{
    ui->txtHopName->setText("");
    ui->txtHopOrigin->setText("");
    ui->cmbHopType->setCurrentIndex(0);
    ui->cmbHopForm->setCurrentIndex(0);
    ui->txtHopDescription->setPlainText("");
    ui->spnHopAlpha->setValue(0.00);
    ui->spnHopBeta->setValue(0.00);
}

void MainWindow::populateHopList(QString filter, int target)
{
    ui->tblHops->setRowCount(0);
    std::vector<hopData> hData = hopObj.getHopList(filter);
    int rowCount;
    for (int i=0; i < hData.size(); i=i+1) {
        if (target == HOP_TAGET_ALL || target == HOP_TAGET_MANAGE) {
            rowCount = ui->tblHops->rowCount();
            ui->tblHops->setRowCount(rowCount+1);
            QTableWidgetItem *hopName = new QTableWidgetItem(hData[i].hopName);
            QTableWidgetItem *hopOrigin = new QTableWidgetItem(hData[i].hopOrigin);
            QString num;
            num.setNum(hData[i].hopAlpha);
            QTableWidgetItem *hopAlpha = new QTableWidgetItem(num + "%");
            QTableWidgetItem *rowID = new QTableWidgetItem(QString::number(hData[i].rowID));
            ui->tblHops->setItem(rowCount, 0, hopName);
            ui->tblHops->setItem(rowCount, 1, hopOrigin);
            ui->tblHops->setItem(rowCount, 2, hopAlpha);
            ui->tblHops->setItem(rowCount, 3, rowID);
        }

        if (target == HOP_TAGET_ADD || target == HOP_TAGET_ALL) {
            rowCount = ui->tblAddHops->rowCount();
            ui->tblAddHops->setRowCount(rowCount+1);
            QTableWidgetItem *hopName2 = new QTableWidgetItem(hData[i].hopName);
            QTableWidgetItem *hopOrigin2 = new QTableWidgetItem(hData[i].hopOrigin);
            QTableWidgetItem *hopForm2 = new QTableWidgetItem(hopObj.GetHopFormName(hData[i].hopForm));
            QTableWidgetItem *hopType2 = new QTableWidgetItem(hopObj.GetHopTypeName(hData[i].hopType));
            QTableWidgetItem *rowID2 = new QTableWidgetItem(QString::number(hData[i].rowID));
            ui->tblAddHops->setItem(rowCount, 0, hopName2);
            ui->tblAddHops->setItem(rowCount, 1, hopOrigin2);
            ui->tblAddHops->setItem(rowCount, 2, hopForm2);
            ui->tblAddHops->setItem(rowCount, 3, hopType2);
            ui->tblAddHops->setItem(rowCount, 4, rowID2);
        }

    }
}


void MainWindow::on_tblHops_cellDoubleClicked(int row, int column)
{
    int hopID = ui->tblHops->model()->index(row, 3).data().toInt();
    ui->stackedWidget->setCurrentIndex(PAGE_ADD_HOPS);
    this->populateHopForm(hopID);
}

void MainWindow::populateHopForm(int hopID)
{
    ui->statusBar->showMessage("hopID: " + QString::number(hopID));
    hopData hData = hopObj.getHopData(hopID);
    ui->txtHopName->setText(hData.hopName);
    ui->txtHopOrigin->setText(hData.hopOrigin);
    ui->spnHopAlpha->setValue(hData.hopAlpha);
    ui->spnHopBeta->setValue(hData.hopBeta);
    ui->txtHopDescription->setPlainText(hData.hopDescription);
    ui->cmbHopType->setCurrentIndex(hData.hopType);
    ui->cmbHopForm->setCurrentIndex(hData.hopForm);
    ui->txtRowID->setText(QString::number(hopID));
    ui->btnSaveAddHops->setText("Update");
    qDebug() << "Loaded Hop: " << hopID;
}


void MainWindow::on_btnHopDelete_clicked()
{
    int rowid = ui->txtRowID->text().toInt();
    QString res = hopObj.deleteHop(rowid);
    ui->statusBar->showMessage(res);
    ui->stackedWidget->setCurrentIndex(PAGE_MANAGE_HOPS);
    this->clearHopForm();
    QString filterString;
    this->populateHopList(filterString, HOP_TAGET_MANAGE);
}

void MainWindow::on_btnAddIngredient_clicked()
{
   ui->stackedWidget->setCurrentIndex(PAGE_ADD_INGREDIENT);
}

void MainWindow::on_tblAddHops_cellActivated(int row, int column)
{
    int hopID = ui->tblAddHops->model()->index(row, 4).data().toInt();
    qDebug() << "HOP ID: " << hopID;

    hopData hData = hopObj.getHopData(hopID);

    ui->txtAddHopDescription->setPlainText(hData.hopDescription);
    ui->statusBar->showMessage("Gotcha!");
}
