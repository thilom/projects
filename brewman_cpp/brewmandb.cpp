#include "brewmandb.h"
#include <sqlite3.h>
#include <QSqlDatabase>

BrewmanDB::BrewmanDB()
{
    bool connectDB();
    void closeDB();
    QSqlDatabase db;
}

bool BrewmanDB::connectDB()
{
    if(!QSqlDatabase::contains( "BrewmanDBConnect" )) {
        this->db = QSqlDatabase::addDatabase("QSQLITE", "BrewmanDBConnect");
        this->db.setDatabaseName("/home/thilo/dev/brewman2/brewman.db");

        if (this->db.open()) {
            return true;
        } else {
            return false;
        }
    }
    return true;
}

void BrewmanDB::closeDB()
{
    this->db.close();

}

