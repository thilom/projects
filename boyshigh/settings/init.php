<?php
define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT']); 
ini_set('log_errors',1);
ini_set('error_log',$_SERVER['DOCUMENT_ROOT'].'/public_html/error_log');
error_reporting(E_ALL & ~E_STRICT);
$dir = SITE_ROOT . '/modules/';
$content_types = array();
$sName = $_SERVER['SERVER_NAME'];
$version = '$Id: init.php 40 2011-04-19 05:30:40Z thilo $';
$public_version = "1.0.1";

// default database connection values
include('database.php');
include('variables.php');
include SITE_ROOT . '/modules/content_manager/functions.php';

//Start Session
$session_id = session_id();
if (empty($session_id)) {
    session_name('dbweb4x');
    ini_set('session.gc_maxlifetime', 30*60);
    session_start();
}

//Remove Slashes if magic_quotes is on
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}

//Error handling values
$reporting_level = E_ALL; //One of the standard PHP error level constants

//date and Time settings
$timezone = 'Africa/Johannesburg'; //Only useful on PHP version 5.1.0 or greater

//Constants
//define('PROCESS', $_SERVER['DOCUMENT_ROOT'] . '/shared/page_process.php');

//Connect to Database - traditional
if (($db_con = @mysql_connect($db_server, $db_username, $db_password)) === FALSE) die("<span style='font-weight: bold; color: red'>Failed to connect to Database</span>");
$db_con;
mysql_select_db($db_name);

//Connect to Database - PDO
try {
	$connect = "mysql:host=$db_server;dbname=$db_name";
	$dbCon = new PDO($connect, $db_username, $db_password);
	$dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch(PDOException $e) {
	echo 'Failed to connect: PDO';
}


//Get settings
$SQL_statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
$SQL_set = mysql_query($SQL_statement);
while($set = mysql_fetch_array($SQL_set)) {
	$settings[$set['tag']] = $set['value'];
}


//Itterate thru directories looking for module.ini.php
//$module_list = '';
//if (!isset($no_it)) {
//	$setting_tags = array('module_name', 'module_icon', 'module_link', 'window_width', 'window_height', 'window_xposition', 'window_yposition', 'window_attribute');
//	$modules = get_module_variables();
//	if (is_dir($dir)) {
//		if ($dh = opendir($dir)) {
//			while (($file = readdir($dh)) !== false) {
//				if (is_dir($dir . $file)) {
//					if ($file == '.' || $file == '..') continue;
//						if (!is_file($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
//						include_once($dir.$file.'/module.ini.php');
//						foreach ($setting_tags as $k=>$v) {
//							if (isset($$v)) {
//								$module_settings[$file][$v] = $$v;
//							}
//						}
//						if (!empty($type)) {
//							foreach ($type as $tab_name=>$module) {
//								$content_types[$tab_name] = $module;
//							}
//						}
//						$module_list[$security_id] = $security_name;
//				}
//			}
//			closedir($dh);
//		}
//	}
//}

//Set time zone
if (version_compare('5.1.0', PHP_VERSION) < 0) {
	date_default_timezone_set($timezone);
}

if (!isset($ajax) || $ajax != 1) echo "<link rel=stylesheet href=/shared/style.css>";
