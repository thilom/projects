<?php

if(!function_exists("size"))
{
	function size($f)
	{
		if($f>=1000)
		{
			$f=($f/1000);
			if($f>999)
			{
				$f=($f/1000);
				$f=round($f,1)."Mb";
			}
			else
				$f=round($f,0)."Kb";
		}
		else
			$f=round($f,0)."b";
		return $f;
	}
}
?>