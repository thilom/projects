<?php


 // ------------ lixlpixel recursive PHP functions -------------
 // deleteDirectory( directory to delete, empty )
 // expects path to directory and optional TRUE / FALSE to empty
 // of course PHP has to have the rights to delete the directory
 // you specify and all files and folders inside the directory
 // ------------------------------------------------------------
 // to use this function to totally remove a directory, write: deleteDirectory('path/to/directory/to/delete');
 // to use this function to empty a directory, write: deleteDirectory('path/to/full_directory',TRUE);

 function deleteDirectory($directory,$empty=FALSE)
 {
	$directory="/".trim($directory,"/");
	if(!file_exists($directory) || !is_dir($directory)) // if the path is not valid or is not a directory ...
		return FALSE; // ... we return false and exit the function
	elseif(!is_readable($directory)) // ... if the path is not readable
		return FALSE; // ... we return false and exit the function
	else // ... else if the path is readable
	{
		$handle = opendir($directory); // we open the directory
		while (FALSE !== ($item = readdir($handle))) // and scan through the items inside
		{
			if($item != '.' && $item != '..') // if the filepointer is not the current directory or the parent directory
			{
				$path = $directory.'/'.$item; // we build the new path to delete
				if(is_dir($path)) // if the new path is a directory
					deleteDirectory($path); // we call this function with the new path
				else // if the new path is a file
					unlink($path); // we remove the file
			}
		}
		closedir($handle); // close the directory
		if($empty == FALSE) // if the option to empty is not set to true
		{
			if(!rmdir($directory)) // try to delete the now empty directory
				return FALSE; // return false if not possible
		}
		return TRUE; // return success
	}
}
?>