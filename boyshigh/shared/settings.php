<?php
include_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/directory_item.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/extract_insert.php";


// prep contents for editing
$string=file_get_contents($_SERVER['DOCUMENT_ROOT']."/W/index.php");

if(isset($_GET['dflt']))
{
	getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","W.css");
	if($itemname)
		unlink($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname);

	getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","G.css");
	if($itemname)
		unlink($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname);
	echo "<script>window.parent.location=window.parent.location.href.replace(/#/g,'')</script>";
	die();
}


// get fluid css
include $_SERVER['DOCUMENT_ROOT']."/shared/css.php";

$admin_css="";
getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","W.css");
if($itemname)
{
	$admin_css=$_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname;
	$css1=file_get_contents($admin_css);
}
else
	$css1=file_get_contents($_SERVER['DOCUMENT_ROOT']."/W/W.css");

// get fluid css
$shared_css="";
getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","G.css");
if($itemname)
{
	$shared_css=$_SERVER['DOCUMENT_ROOT']."/i/fluid/".$itemname;
	$css2=file_get_contents($shared_css);
}
else
	$css2=file_get_contents($_SERVER['DOCUMENT_ROOT']."/shared/W.css");


if(isset($_POST['bgC']))
{
	include $_SERVER['DOCUMENT_ROOT']."/shared/document_write.php";

	string_insert($_POST['wT'],"title");
	string_write($_SERVER['DOCUMENT_ROOT']."/W/index.php");

	$css1=css_insert($css1,"body","background",(isset($_POST['bgC'])&&$_POST['bgC']?$_POST['bgC']:"#BBB"));
	$css1=css_insert($css1,".Wt .Wif","background",(isset($_POST['wfc'])&&$_POST['wfc']?$_POST['wfc']:"#CCC"));
	$css1=css_insert($css1,".Wt .Wif A","background",(isset($_POST['wfc'])&&$_POST['wfc']?$_POST['wfc']:"#CCC"));
	$css1=css_insert($css1,".Wt th .Wtt","background",(isset($_POST['wfc'])&&$_POST['wfc']?$_POST['wfc']:"#CCC"));
	$css1=css_insert($css1,"#Wn div","color",(isset($_POST['dfc'])&&$_POST['dfc']?$_POST['dfc']:"#000"));
	$css1=css_insert($css1,".Wt th .Wtt","color",(isset($_POST['wtc'])&&$_POST['wtc']?$_POST['wtc']:"#000"));
	$css1=css_insert($css1,".Wt th a:link,.Wt th a:visited,.Wt th a:hover","color",(isset($_POST['wtc'])&&$_POST['wtc']?$_POST['wtc']:"#000"));
	$css1=css_insert($css1,"#WnB td","background",(isset($_POST['pc'])&&$_POST['pc']?$_POST['pc']:"#CCC"));
	@unlink($admin_css);
	write_doc($_SERVER['DOCUMENT_ROOT']."/i/fluid/".mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"))."W.css",$css1);

	$css2=css_insert($css2,"body","background",(isset($_POST['wbg'])&&$_POST['wbg']?$_POST['wbg']:"#DDD"));
	$css2=css_insert($css2,"body","color",(isset($_POST['fc'])&&$_POST['fc']?$_POST['fc']:"#000"));
	$css2=css_insert($css2,".T td,.T th","color",(isset($_POST['fc'])&&$_POST['fc']?$_POST['fc']:"#000"));
	$css2=css_insert($css2,".T td,.T th","border-color",(isset($_POST['bc'])&&$_POST['bc']?$_POST['bc']:"#CCC"));
	$css2=css_insert($css2,".T th","color",(isset($_POST['hc'])&&$_POST['hc']?$_POST['hc']:"#888"));
	$css2=css_insert($css2,".TRW1","background",(isset($_POST['tr1'])&&$_POST['tr1']?$_POST['tr1']:"#D6D6D6"));
	$css2=css_insert($css2,".TRW2","background",(isset($_POST['tr2'])&&$_POST['tr2']?$_POST['tr2']:"#DDD"));
	$css2=css_insert($css2,".TRWH","background",(isset($_POST['tr3'])&&$_POST['tr3']?$_POST['tr3']:"#FFFF88"));
	@unlink($shared_css);
	write_doc($_SERVER['DOCUMENT_ROOT']."/i/fluid/".mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"))."G.css",$css2);

	include $_SERVER['DOCUMENT_ROOT']."/shared/img_processor.php";

	function delete($file)
	{
		@unlink($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$file.".png");
		@unlink($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$file.".jpg");
		@unlink($_SERVER['DOCUMENT_ROOT']."/i/fluid/".$file.".gif");
	}
	
	if((isset($_FILES['DB'])&&$_FILES['DB']['name'])||(isset($_POST['dDB'])&&$_POST['dDB']))
		delete("DB");
	if((isset($_FILES['DF'])&&$_FILES['DF']['name'])||(isset($_POST['dDF'])&&$_POST['dDF']))
		delete("DF");
	if((isset($_FILES['DL'])&&$_FILES['DL']['name'])||(isset($_POST['dDL'])&&$_POST['dDL']))
		delete("DL");
	if((isset($_FILES['DL2'])&&$_FILES['DL2']['name'])||(isset($_POST['dDL2'])&&$_POST['dDL2']))
		delete("L");

	function extract_ext($name,$file)
	{
		$ext=strtolower(substr($file['name'],strrpos($file['name'],".")));
		if($ext==".jpeg")
			$ext=".jpg";
		return $name.$ext;
	}

	$copyonly=1;
	$location=$_SERVER['DOCUMENT_ROOT']."/i/fluid/";

	if(isset($_FILES['DB'])&&$_FILES['DB']['size'])
	{
		$filename=extract_ext("DB",$_FILES['DB']);
		process_img($_FILES['DB']);
	}
	if(isset($_FILES['DF'])&&$_FILES['DF']['size'])
	{
		$filename=extract_ext("DF",$_FILES['DF']);
		process_img($_FILES['DF']);
	}
	if(isset($_FILES['DL'])&&$_FILES['DL']['size'])
	{
		$filename=extract_ext("DL",$_FILES['DL']);
		process_img($_FILES['DL']);
	}
	if(isset($_FILES['DL2'])&&$_FILES['DL2']['size'])
	{
		$filename=extract_ext("L",$_FILES['DL2']);
		process_img($_FILES['DL2']);
	}

	echo "<script>";
	if($process_errors)
		echo "alert(\"".str_replace("<hr>&#9658;","",str_replace("<br>","\\n",$process_errors))."\");";
	echo "window.parent.location=window.parent.location.href.replace(/#/g,'')";
	echo "</script>";
	die();
}




echo "<html>";
// get fluid css
include_once $_SERVER['DOCUMENT_ROOT']."/shared/admin_style.php";
echo $css;
echo "<script src=/shared/G.js></script>";
echo "<script src=/shared/color/C.js></script>";
echo "<style type=text/css>.T td{white-space:nowrap}</style>";
echo "<script src=/shared/trwacker.js></script>";
echo "<body onload=whack()>";

echo "<script>tabs(\"<a href=#>Interface</a><a href=/shared/system_settings.php>System</a>\",0)</script>";

echo "<br>";

echo "Set the look and feel of your Modularus interface here. You can change just about any aspect of Modularus. Just keep in mind that you should keep background images reasonably small in size to avoid long load times. Leave colour fields blank to revert to default colours.";

echo "<br><br>";
echo "<form method=post enctype=multipart/form-data>";
echo "<table class=T width=99% id=TRW>";

echo "<tr><th colspan=2>Page Details</th></tr>";

echo "<tr><td>&nbsp; &nbsp; Title</td><td width=90%><input type=text name=wT value=\"".string_extract("title")."\"></td></tr>";

echo "<tr><th colspan=2>Desktop</th></tr>";

echo "<tr><td>&nbsp; &nbsp; Colour</td><td><input type=text name=bgC value=".css_extract($css1,"body","background","#BBB")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the Colour of the desktop')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Background</td><td><input type=file name=DB onmouseover=\"T_S(event,'Set the image of the desktop. NOTE! This image is repeated')\">";
if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.gif"))
	echo " <input type=checkbox name=dDB value=1> Remove";
echo "</td></tr>";

echo "<tr><td>&nbsp; &nbsp; Foreground</td><td><input type=file name=DF onmouseover=\"T_S(event,'Set the image of the desktop foregound. NOTE! This image is not repeated and is centered. Use a single PNG file with a transparent background. Ensure the image is not too large (>100k)')\">";
if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.gif"))
	echo " <input type=checkbox name=dDF value=1> Remove";
echo "</td></tr>";

echo "<tr><td>&nbsp; &nbsp; Brand1</td><td><input type=file name=DL onmouseover=\"T_S(event,'The logo will appear centered above the module tools. This file must be a PNG file with transparent background and not exceed 200x200 pixels')\">";
if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.gif"))
	echo " <input type=checkbox name=dDL value=1> Remove";
echo "</td></tr>";

echo "<tr><td>&nbsp; &nbsp; Brand2</td><td><input type=file name=DL2 onmouseover=\"T_S(event,'The logo will appear at the bottom right. This file must be a PNG file with transparent background and not exceed 200x200 pixels')\">";
if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.gif"))
	echo " <input type=checkbox name=dDL2 value=1> Remove";
echo "</td></tr>";

echo "<tr><td>&nbsp; &nbsp; Link Colour</td><td width=90%><input type=text name=dfc value=".css_extract($css1,"#Wn div","color","#000")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the font-colour of the desktop icon text')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Panel Background</td><td width=90%><input type=text name=pc value=".css_extract($css1,"#WnB td","background","#CCC")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the background colour of the icon panel')\"></td></tr>";



echo "<tr><th colspan=2>Windows</th></tr>";

echo "<tr><td>&nbsp; &nbsp; Title Colour</td><td><input type=text name=wtc value=".css_extract($css1,".Wt th .Wtt","color","#000")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the Title font colour of window frames. NOTE! Titles are semi opaque')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Frame Colour</td><td><input type=text name=wfc value=".css_extract($css1,".Wt .Wif","background","#CCC")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour of the window frames')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Background Colour</td><td><input type=text name=wbg value=".css_extract($css2,"body","background","#BBB")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour of the window backgrounds')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Font Colour</td><td><input type=text name=fc value=".css_extract($css2,"body","color","#000")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour fonts')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Table borders</td><td><input type=text name=bc value=".css_extract($css2,".T td,.T th","border-color","#CCC")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour of borders of tables')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Header Font</td><td><input type=text name=hc value=".css_extract($css2,".T th","color","#888")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour of table headers font')\"></td></tr>";

echo "<tr><th colspan=2>Table Rows</th></tr>";

echo "<tr><td>&nbsp; &nbsp; Row 1</td><td><input type=text name=tr1 value=".css_extract($css2,".TRW1","background","#D6D6D6")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour of a table row')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Row 2</td><td><input type=text name=tr2 value=".css_extract($css2,".TRW2","background","#DDD")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour of an alternative row')\"></td></tr>";

echo "<tr><td>&nbsp; &nbsp; Row Mouseover</td><td><input type=text name=tr3 value=".css_extract($css2,".TRWH","background","#FFFF88")." onclick=toggle(1,this,event) onmouseover=\"T_S(event,'Set the colour of a row highlighted')\"></td></tr>";

echo "<tr><th colspan=2>";
echo "<input type=submit value=OK>";
echo " <input type=reset value=Reset>";
echo " <input type=button onclick=\"location=location.href.replace(/#/g,'')+'&dflt=1'\" value=Default>";
echo "</th></tr>";

echo "</table>";
echo "</form>";

echo "</body>";
echo "</html>";

?>