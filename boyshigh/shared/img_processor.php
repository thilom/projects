<?php
$process_errors="";

if(!function_exists("process_img"))
{
	function process_img($file)
	{
		global $jpgonly;
		global $filesize;
		global $filename;
		global $maxsize;
		global $location;
		global $dimension;
		global $compress;
		global $copyonly;
		global $process_errors;
		$errors="";

		$file_temp=(is_array($file)&&isset($file['tmp_name'])?$file['tmp_name']:$file);
		$image_info=getimagesize($file_temp);
		$file_name=(is_array($file)&&isset($file['name'])?$file['name']:substr($file_temp,strrpos($file_temp,"/")+1));
		$file_size=(is_array($file)&&isset($file['size'])?$file['size']:filesize($file_temp));
		$file_type=$image_info[2];

		if(is_dir($location)==false)
			$errors.="<hr>&#9658; Location for file does not exist (".$location.").<br>";
		$maxsize=($maxsize?$maxsize:5000000);
		if($file_size>$maxsize)
		{
			include_once $_SERVER['DOCUMENT_ROOT']."/shared/size_convertor.php";
			$errors.="<hr>&#9658; File ".$file_name." too big (".size($file_size)." > ".size($maxsize)." max).<br>";
		}

		if($file_type==1 || $file_type==3 || (isset($copyonly) && $copyonly))
			copy($file_temp,$location.$filename);
		elseif(isset($jpgonly) && $jpgonly && $file_type!=2)
			$errors.="<hr>&#9658; File ".$file_name." is not JPG or PNG format.<br>";
		elseif($file_type!=2 && $file_type!=3)
			$errors.="<hr>&#9658; File ".$file_name." is not JPG or PNG format.<br>";
		else
		{
			if(isset($processfilename) && $processfilename)
			{
				$filename=substr($file_temp,8);
				$ext=strtolower(substr($file_name,strrpos($file_name,".")));
				if($ext==".jpeg")$ext=".jpg";
				$filename.=$ext;
			}
			else
			{
				include_once $_SERVER['DOCUMENT_ROOT']."/shared/string.php";
				$filename=strip(str_replace(" ","_",$filename));
			}

			if($file_type==2)
			{
				$imgW=($dimension?$dimension:128);
				$imgH=($dimension?$dimension:128);
				$compress=($compress?$compress:60);
				$testW=$image_info[0];
				$testH=$image_info[1];
				if($testW>$imgW||$testH>$imgH)
				{
					$scale=min($imgW/$image_info[0],$imgH/$image_info[1]);
					$newwidth=(int)($image_info[0]*$scale);
					$newheight=(int)($image_info[1]*$scale);
					$deltaw=(int)(($imgW-$newwidth)/2);
					$deltah=(int)(($imgH-$newheight)/2);
				}
				else
				{
					$newwidth=(int)($image_info[0]);
					$newheight=(int)($image_info[1]);
				}
				$src_img=ImageCreateFromJPEG($file_temp);
				$dst_img=imagecreatetruecolor($newwidth-1,$newheight-1);
				for($c=0;$c<256;$c++)$white=ImageColorAllocate($dst_img,$c,$c,$c);
				imagecopyresampled($dst_img,$src_img,0,0,0,0,$newwidth,$newheight,ImageSX($src_img),ImageSY($src_img));
				imagejpeg($dst_img,$location.$filename,$compress);
				imagedestroy($src_img);
				imagedestroy($dst_img);
	
				@chmod($location.$filename,0777);
			}
		}
	
		$process_errors.=$errors;
	}
}
?>