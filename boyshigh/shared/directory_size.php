<?php

$sizeD=0;

if(!function_exists('sizeDirectory'))
{
	function sizeDirectory($directory,$loop=0)
	{
		global $sizeD;
	
		if(!$loop)
		{
			$sizeD=0;
			$directory="/".trim($directory,"/");
		}
		if(is_dir($directory) && is_readable($directory))
		{
			$handle=opendir($directory);
			while(FALSE!==($item=readdir($handle)))
			{
				if($item!='.' && $item!='..')
				{
					$path=$directory.'/'.$item;
					if(is_dir($path))
						sizeDirectory($path,1);
					else
						$sizeD=$sizeD+filesize($path);
				}
			}
			closedir($handle);
		}
		if(!$loop)
			return $sizeD;
	}
}
?>