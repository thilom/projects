<?php
// strip unwanted chars
function strip($v)
{
	$v=trim($v);
	$v=str_replace("'","`",$v);
	$v=str_replace('"',"`",$v);
	$v=str_replace("    "," ",$v);
	$v=str_replace("   "," ",$v);
	$v=str_replace("  "," ",$v);
	$v=str_replace("  "," ",$v);
	$v=str_replace("\t","",$v);
	$v=str_replace("\v","",$v);
	$v=str_replace("\r","",$v);
	$v=str_replace("\n","�",$v);
	$v=str_replace("|","I",$v);
	$v=str_replace("{","(",$v);
	$v=str_replace("}",")",$v);
	return $v;
}

// strip unwanted chars
function strip2($v)
{
	$v=trim($v);
	$v=str_replace("'","`",$v);
	$v=str_replace('"',"`",$v);
	$v=str_replace("    "," ",$v);
	$v=str_replace("   "," ",$v);
	$v=str_replace("  "," ",$v);
	$v=str_replace("  "," ",$v);
	$v=str_replace("\t","",$v);
	$v=str_replace("\v","",$v);
	$v=str_replace("\r","",$v);
	$v=str_replace("\n","<br>",$v);
	$v=str_replace("<br><br><br>","<br><br>",$v);
	$v=str_replace("<br><br><br>","<br><br>",$v);
	$v=str_replace("{","(",$v);
	$v=str_replace("}",")",$v);
	$v=str_replace("|","I",$v);
	return $v;
}

// truncate sting
function clip($v,$c=6000)
{
	$v=trim($v);
	$v=substr($v,0,$c);
	return $v;
}

// capitalize first char of all words
function caps($v,$c=0)
{
	$v=trim($v);
	if($c && strlen($v)<3)
		$o=strtoupper($v);
	else
	{
		$v=str_replace("(","( ",$v);
		$v=str_replace("-"," - ",$v);
		$v=str_replace("Mc","Mc ",$v);
		$v=str_replace("Mac","Mac ",$v);
		$v=str_replace(".",". ",$v);
		$v=str_replace("'","`",$v);
		$v=str_replace("&","& ",$v);
		$v=explode(" ",strtolower($v)." ");
		$o="";
		foreach($v as $r)
		{
			if($r=='on' || $r=='in' || $r=='at' || $r=='the' || $r=='and' || $r=='of' || $r=='as' || $r=='if' || $r=='for' || $r=='mc' ||  $r=='mac' || $r=='van' || $r=='la' || $r=='le' || $r=='da' || $r=='del' || $r=='der')
			{
				if($o)
					$o.=$r;
				else
					$o.=ucfirst($r);
			}
			elseif($c && !$o && strlen($r)<4)
				$o.=strtoupper($r);
			else
				$o.=ucfirst($r);
			$o.=" ";
		}
		$o=str_replace("( ","(",$o);
		$o=str_replace(" )",")",$o);
		$o=str_replace("Mc ","Mc",$o);
		$o=str_replace("Mac ","Mac",$o);
		$o=str_replace(". ",".",$o);
		$o=str_replace("  "," ",$o);
		$o=str_replace("` ","`",$o);
		$o=trim($o);
	}
	return trim($o);
}
?>