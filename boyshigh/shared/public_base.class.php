<?php
//Constants
define('BASE_CLASS', $_SERVER['DOCUMENT_ROOT'] . '/shared/base.class.php');

//Includes
require_once BASE_CLASS;

/**
 * Creates a public base class that can be accessed from outside a class.
 *
 */
class public_base extends base {
	
}

?>