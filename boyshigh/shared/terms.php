<?php
/** 
 * Terms and Conditions
 *
 * @version $Id: terms.php 48 2011-05-06 03:47:20Z thilo $
 *
 */

$terms = "
<div class=TC>
	
		NONDISCLOSURE.
		You may not disclose to any other party the content provided in this module. Accordingly,
		you will employ all efforts to protect the restricted or secure nature of the content provided
		from unauthorised distribution of any kind.

		<br><br>
		INTELLECTUAL PROPERTY.
		The content and services in
		this module may be protected by copyright, trademark and/or other intellectual property laws.
		{$GLOBALS['prog_name']} or its resellers do not grant any express or implied rights to use the content and services.
		Except as expressly authorised, you agree not to copy, republish, download, transmit, modify,
		rent, lease, loan, sell, assign, distribute, license, sublicense, reverse engineer, or create
		derivative works based on this module, its services or content.
	
		<br><br>
		LIMITATION OF WARRANTY.
		All Services and content accessible through this module, is provided 'as is' without
		warranty of any kind. You understand and acknowledge that
		<br>[i] {$GLOBALS['prog_name']} and/or its resellers
		does not control, endorse, or accept responsibility for any content offered through this site;
		<br>[ii] {$GLOBALS['prog_name']} and/or its resellers makes no representation or warranties whatsoever about any such
		third parties or their content; <br>[iii] any dealings you may have with such third parties are at your
		own risk; and <br>[iv] {$GLOBALS['prog_name']} and/or its resellers shall not be liable or responsible for any content offered
		by third parties. The downloading or use of any content through this site is done at your own discretion
		and risk and with your agreement that you will be solely responsible for any damage to your computer
		systems, loss of data, or other harm that results from such activities. {$GLOBALS['prog_name']} and/or its resellers assumes no
		liability for any computer virus or other destructive or offensive code, content or applications that may be
		used or downloaded from this site. No advice or information, whether oral or written, obtained by you from
		{$GLOBALS['prog_name']} and/or its resellers or from this site shall create any warranty not expressly stated in the terms of use.

</div>

";

?>
