<?php

function sendMailer_100($logMember='',  $sendCron=false)
	{
		$matches = '';
		preg_match_all("/<[^>]*\n?.*=(\"|\')?(.*\.gif|.jpg)(\"|\')?.*\n?[^<]*>/", $this->mailObject['body']['html'], $matches);
		$inlineImages = $matches[2];
		
		if (isset($this->mailObject['body']['text']) && !isset($this->mailObject['body']['html']) && !isset($this->mailObject['attachment'])) {
			// -= TEXT ONLY =- //
			$header = '';
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			
			$message .= PHP_EOL . $this->mailObject['body']['text'];
		} else if (isset($this->mailObject['body']['text']) && !isset($this->mailObject['body']['html']) && isset($this->mailObject['attachment'])) {
			// -=  TEXT, ATTACHMENT =- //
			$header = '';
			$this->mailObject['header']['Content-Type'] = "multipart/mixed; boundary=\"" . $this->boundry[0] . "\"";
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			$message = PHP_EOL . "--" . $this->boundry[0];
			$message .= PHP_EOL . "Content-Type: text/plain; charset=\"iso-8859-1\"";
			$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL; 
			$message .= PHP_EOL . $this->mailObject['body']['text'];
			foreach ($this->mailObject['attachment'] as $value) {
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type:" . $value['mimeType'] . "; name=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Disposition: attachment;  filename=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: base64" . PHP_EOL;
				$message .= PHP_EOL . $value['data'];
			}
			$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
		} else if (isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && isset($this->mailObject['attachment'])) {
			// -= TEXT, HTML, ATTACHMENT =- //
			$header = '';
			$this->mailObject['header']['Content-Type'] = "multipart/mixed; boundary=\"" . $this->boundry[0] . "\"";
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			$message = "--" . $this->boundry[0];
			$message .= PHP_EOL . "Content-Type: multipart/alternative; boundary=\"".$this->boundry[1]."\"" . PHP_EOL;
			$message .= PHP_EOL . "--" . $this->boundry[1];
			$message .= PHP_EOL . "Content-Type: text/plain; charset=ISO-8859-1";
			$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
			$message .= PHP_EOL . $this->mailObject['body']['text'];
			$message .= PHP_EOL . "--". $this->boundry[1];
			$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
			$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
			$message .= PHP_EOL . $this->mailObject['body']['html'];
			$message .= PHP_EOL . "--" . $this->boundry[1] . "--";
			foreach ($this->mailObject['attachment'] as $value) {
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type:" . $value['mimeType'] . "; name=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Disposition: attachment;  filename=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: base64" . PHP_EOL;
				$message .= PHP_EOL . $value['data'];
			}
			$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
		} else if (!isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && !isset($this->mailObject['attachment'])) {
			// -= HTML ONLY =- //
			if (count($inlineImages) != 0) {
				foreach($inlineImages as $key=>$value) {
					$fileinfo = pathinfo($value);
					$mimeType = setMIME($fileinfo['extension']);
					$imageCID = time() . $key . '@chrome.co.za';
					$this->mailObject['body']['html'] = str_replace($value, 'cid:' . $imageCID, $this->mailObject['body']['html']);
					$base64[$key] = PHP_EOL . "--" . $this->boundry[0];
					$base64[$key] .= PHP_EOL . "Content-Type: ". $mimeType ."; name=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL . "Content-Transfer-Encoding: base64";
					$base64[$key] .= PHP_EOL . "Content-ID: <$imageCID>";
					$base64[$key] .= PHP_EOL . "Content-Disposition: inline;  filename=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL;
					$base64[$key] .= PHP_EOL;
					$file = fopen($_SERVER['DOCUMENT_ROOT'] . $value,'rb');
					$data = fread($file,filesize($_SERVER['DOCUMENT_ROOT'] . $value));
					fclose($file);
					$base64[$key] .= chunk_split(base64_encode($data));
					$base64[$key] .= PHP_EOL; 
				}
				$this->mailObject['header']['Content-Type'] = 'multipart/related; boundary="'. $this->boundry[0] .'"';
				$message = PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				foreach ($base64 as $value) {
					$message .= $value;	
				}
				$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
			} else {
				$this->mailObject['header']['Content-Type'] = 'text/html';
				$message .= $this->mailObject['body']['html'];
			}
			$header = '';
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
		} else if (!isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && isset($this->mailObject['attachment'])) {
			// -= HTML, ATTACHMENT=- //
			$header = '';
			$this->mailObject['header']['Content-Type'] = "multipart/mixed; boundary=\"" . $this->boundry[0] . "\"";
			foreach ($this->mailObject['header'] as $key=>$value) {
				$header .= "$key: $value" . PHP_EOL;
			}
			$message = PHP_EOL . "--" . $this->boundry[0];
			if (count($inlineImages) == 0) {
				$message .= PHP_EOL . "Content-Type: text/html; charset=\"iso-8859-1\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL; 
				$message .= PHP_EOL . $this->mailObject['body']['html'];
			} else {
				$message .= PHP_EOL . "Content-Type: multipart/related; boundary=\"". $this->boundry[1] ."\"";
				$message .= PHP_EOL;
				$message .= PHP_EOL . "--" . $this->boundry[1];
				foreach($inlineImages as $key=>$value) {
					$fileinfo = pathinfo($value);
					$mimeType = setMIME($fileinfo['extension']);
					$imageCID = time() . $key . '@imcsa.org.za';
					$this->mailObject['body']['html'] = str_replace($value, 'cid:' . $imageCID, $this->mailObject['body']['html']);
					$base64[$key] = PHP_EOL . "--" . $this->boundry[1];
					$base64[$key] .= PHP_EOL . "Content-Type: ". $mimeType ."; name=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL . "Content-Transfer-Encoding: base64";
					$base64[$key] .= PHP_EOL . "Content-ID: <$imageCID>";
					$base64[$key] .= PHP_EOL . "Content-Disposition: inline;  filename=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL;
					$base64[$key] .= PHP_EOL;
					$file = fopen($_SERVER['DOCUMENT_ROOT'] . $value,'rb');
					$data = fread($file,filesize($_SERVER['DOCUMENT_ROOT'] . $value));
					fclose($file);
					$base64[$key] .= chunk_split(base64_encode($data));
					$base64[$key] .= PHP_EOL; 
				}
				//$message .= "\r\n--" . $this->boundry[1];
				$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				foreach ($base64 as $value) {
					$message .= $value;	
				}
				$message .= PHP_EOL . "--" . $this->boundry[1] . "--";
			}
			foreach ($this->mailObject['attachment'] as $value) {
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type:" . $value['mimeType'] . "; name=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Disposition: attachment;  filename=\"{$value['fileName']}\"";
				$message .= PHP_EOL . "Content-Transfer-Encoding: base64" . PHP_EOL;
				$message .= PHP_EOL . $value['data'];
			}
			$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
		} else if  (isset($this->mailObject['body']['text']) && isset($this->mailObject['body']['html']) && !isset($this->mailObject['attachment'])) {
			// -=TEXT, HTML=- //
			if (count($inlineImages) == 0) {
				$this->mailObject['header']['Content-Type'] = 'multipart/alternative; boundary=' . $this->boundry[0];
				$header = '';
				foreach ($this->mailObject['header'] as $key=>$value) {
					$header .= "$key: $value" . PHP_EOL;
				}
				$message = PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/plain; charset='iso-8859-1'";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
				$message .= PHP_EOL . $this->mailObject['body']['text'];
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/html; charset='iso-8859-1'";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit" . PHP_EOL;
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
			} else {
				$this->mailObject['header']['Content-Type'] = 'multipart/alternative; boundary=' . $this->boundry[0];
				$header = '';
				foreach ($this->mailObject['header'] as $key=>$value) {
					$header .= "$key: $value" . PHP_EOL;
				}
				$message = PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: text/plain; charset='iso-8859-1'; format=flowed";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['text'];
				$message .= PHP_EOL . "--" . $this->boundry[0];
				$message .= PHP_EOL . "Content-Type: multipart/related; boundary=" . $this->boundry[1] ;
				$message .= PHP_EOL;
				$message .= PHP_EOL . "--" . $this->boundry[1];
				foreach($inlineImages as $key=>$value) {
					$fileinfo = pathinfo($value);
					$mimeType = setMIME($fileinfo['extension']);
					$imageCID = time() . $key . '@imcsa.org.za';
					$this->mailObject['body']['html'] = str_replace($value, 'cid:' . $imageCID, $this->mailObject['body']['html']);
					$base64[$key] = PHP_EOL . "--" . $this->boundry[1];
					$base64[$key] .= PHP_EOL . "Content-Type: ". $mimeType ."; name=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL . "Content-Transfer-Encoding: base64";
					$base64[$key] .= PHP_EOL . "Content-ID: <$imageCID>";
					$base64[$key] .= PHP_EOL . "Content-Disposition: inline;  filename=\"". $fileinfo['basename'] ."\"";
					$base64[$key] .= PHP_EOL;
					$base64[$key] .= PHP_EOL;
					$file = fopen($_SERVER['DOCUMENT_ROOT'] . $value,'rb');
					$data = fread($file,filesize($_SERVER['DOCUMENT_ROOT'] . $value));
					fclose($file);
					$base64[$key] .= chunk_split(base64_encode($data));
					$base64[$key] .= PHP_EOL; 
				}
				//$message .= "\r\n";
				$message .= PHP_EOL . "Content-Type: text/html; charset=ISO-8859-1";
				$message .= PHP_EOL . "Content-Transfer-Encoding: 7bit";
				$message .= PHP_EOL . $this->mailObject['body']['html'];
				foreach ($base64 as $value) {
					$message .= $value;	
				}
				$message .= PHP_EOL . "--" . $this->boundry[1] . "--" . PHP_EOL;
				$message .= PHP_EOL . "--" . $this->boundry[0] . "--";
				
				//echo $message;
			}
		} else {}
		
		if ($sendCron) {
			$attachments = '';
			foreach ($this->mailObject['attachment'] as $value) {
				$attachments .= $_SERVER['DOCUMENT_ROOT'] .'/'. $value['fileName']  .',';
			}
			$attachments = substr($attachments, 0, -1);
			if (mysql_query("INSERT INTO chromeMail_cron (memberID, emailTo, headers, subject, bodyText, bodyHTML, attachments, message) 
			VALUES ('$logMember', '{$this->mailObject['to']}', '$header', '{$this->mailObject['subject']}', '{$this->mailObject['body']['text']}', '{$this->mailObject['body']['html']}', '$attachments', '$message')")) {
				return true;
			} else {
				return false;	
			}
		} else {
			if (mail($this->mailObject['to'], $this->mailObject['subject'], $message, $header)) {
				$attachments = '';
				foreach ($this->mailObject['attachment'] as $value) {
					$attachments .= $_SERVER['DOCUMENT_ROOT'] .'/'. $value['fileName']  .',';
				}
				$attachments = substr($attachments, 0, -1);
				//logMail($this->mailObject['to'], $this->mailObject['subject'], $this->mailObject['body']['text'], $this->mailObject['body']['html'],  $attachments, $header, $logMember);
				
			return true;
			} else {
				return false;
			}
		}
	}

function page_convert($c, $a) {
	$GLOBALS[$c.'_ac'] = TRUE;
//	$s= $GLOBALS['sName'];	$s = substr($s, 0, 3)=='www'?substr($s, 4):$s;
//	$s = str_replace('.', '', $s);
//	$u = str_split($s);	array_walk($u, 'enc_page');
//	$u = array_reverse($u);
//	$u = implode('', $u);
//	if ($u != __SNAME__) {switch ($a) {
//			case 'a':
//				$GLOBALS[$c.'_ac'] = FALSE; break;
//			case 'b':
//				$GLOBALS[$c.'_ac'] = FALSE; break;
//		}
//	}
}

function show_newstart() {
    global $export;
    //Initailize variables
    $last_date = '';
   
    //Get last export date
    $last_date = $export->last_date();
    
    //Get page template
    $template = file_get_contents(EXPORT_START);
    
    //Replace tags
    $template = str_replace('<!-- last_date -->', $last_date, $template);
   
    //serve page
    echo $template;
   
    
}

function exporting_old() {
    global $export;
    
    //Initailize variables
    $line = '';
    
    $export->export_html();
    $export->update_date();
    $results = $export->get_results();
    
    //Get template
    $template = file_get_contents(EXPORT_RESULT);
    
     //get line template
    $template = str_replace(array("\r", "\n", "\t"), '', $template);
    preg_match('/<!-- line_start -->(.+)<!-- line_end -->/i', $template, $match);
    $line_template = $match[1];
    
	foreach ($results as $link=>$r) {
        $nLine = str_replace('<!-- result -->', $r, $line_template);
        $nLine = str_replace('<!-- file -->', $link, $nLine);
        $line .= $nLine;
    }
    
     //Replace Tags
    $template = preg_replace('/<!-- line_start -->(.+)<!-- line_end -->/i', $line, $template);
    
    echo $template;
    
}

function enc_page(&$i, $k) {
	$i = ord($i);
}
	


?>