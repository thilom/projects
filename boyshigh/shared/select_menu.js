// ARGUMENTS
// d= display name
// t= tooltip
// n= input name
// v= selected value - if the first option !=0 then oprion 0=None is inserted
// a= array of option elements: 1|One}2|Two}1|One}
// c= function call: 'X(this.lang)' // NOTE the <option value=?> is stored in lang attribute
// o= initial state of drop content 1=open, "" or 0=closed

function fakeSelect(d,t,n,v,a,c,o)
{
	n=(n?n:d?d.replace(/ /g,""):"")
	var dn=(d?d:"&nbsp;")
	var dv=(v?v:"")
	var p=""
	if(a)
	{
		a=a.replace(/}/g,"�").split("�")
		var i=0,l=0
		while(a[i])
		{
			var b=a[i].split("|")
			if(b[0]==v)
			{
				dv=b[0]
				dn=b[1]
			}
			if(!i&&b[0]&&b[0]!=0)
			{
				p+="<em onclick=\"f_Ss(this,'"+n+"')"+(c?";"+c:"")+"\" lang=''>None</em>"
				l=l+17.3
			}
			p+="<em onclick=\"f_Ss(this,'"+n+"')"+(c?";"+c:"")+"\""+(b[0]==v?" class=f_Sem":"")+" lang=\""+b[0]+"\""+(b[2]?" onmouseover=\"T_S(event,'"+b[2].replace(/"/g,"").replace(/'/g,"").replace(/\n/g,"<br>")+"')\"":"")+">"+b[1]+"</em>"
			l=l+17.3
			i++
		}
	}
	else
		p+="<em onclick=\"f_Ss(this,'"+n+"')"+(c?";"+c:"")+"\" lang=''>None</em>"
	a=""
	a+="<span id=f_SM>"
	a+="<input type=hidden id=f_Si"+n+" name="+n+" value=\""+dv+"\">"
	a+="<abbr onclick=f_Su('f_Sv"+n+"')"+(d?" lang=\""+d+"\"":"")+(t?" onmouseover=\"T_S(event,'"+t+"'"+(t.length<48?",1":"")+")\"":"")+">"
	a+="<samp>&#9660;</samp>"
	a+="<b id=f_Sb"+n+">"+dn+"</b>"
	a+="</abbr>"
	a+="<var class=f_Sv id=f_Sv"+n+" style=height:"+(l>138?138:l)+(o?"":";display:none")+">"+p+"</var>"
	a+="</span>"
	return a
}

function f_Su(n)
{
	var v=document.getElementsByTagName("VAR")
	var i=0
	while(v[i])
	{
		if(v[i].className=="f_Sv")
		{
			if(v[i].id==n)
				v[i].style.display=(v[i].style.display==""?"none":"")
			else
				v[i].style.display="none"
		}
		i++
	}
}

function f_Ss(t,n)
{
	document.getElementById("f_Si"+n).value=t.lang
	document.getElementById("f_Sb"+n).innerHTML=t.innerHTML
	var p=t.parentNode.getElementsByTagName("EM")
	var i=0
	while(p[i])
	{
		p[i].className=""
		i++
	}
	t.className="f_Sem"
	 f_Su()
}

function f_Sr(n,v)
{
	var t=document.getElementById("f_Sb"+n).parentNode.parentNode
	t.getElementsByTagName("INPUT")[0]=(v?v:"")
	var p=t.getElementsByTagName("EM")
	var i=0
	while(p[i])
	{
		p[i].className=""
		if(p[i].lang==v)
		{
			p[i].className="f_Sem"
			document.getElementById("f_Sb"+n).innerHTML=p[i].innerHTML
		}
		i++
	}
}

document.write("<link rel=stylesheet href=/shared/select_menu.css>")