<?php
define('SETTINGS',$_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Includes
require_once SETTINGS;

//Constants
define('DESKTOP_GENERATOR', SITE_ROOT . '/admin/desktop.php');

require_once SITE_ROOT.'/modules/login/class.login.php';
$lg_obj = new login();

if (!empty($_POST['submit']) && $_POST['submit'] == 'login') {
	$lg_obj->do_login($_POST['login_username_txt'], $_POST['login_pass_txt']);
}

if (empty($_REQUEST['W1'])) {
	$W1 = "";
} else {
	$W1 = $_REQUEST['W1'];
}

if ($lg_obj->check_login()) {
	echo "<script>P=window.parent"; // collect the parent
//	echo ";P.WN(1,'Content Manager','/modules/content_manager/i/webexport-64.png','/modules/content_manager/start.php',0,560,100,0,2)";

	echo ";P.WN(0,'Logout','F/off_logout_64.png','',0,560)";
	require DESKTOP_GENERATOR;
	echo ";P.WD(1)"; // WD close window - the id of each window is passed to every window when created
	echo ";P.WD(0)";
	echo "</script>";

} else {
	$body = file_get_contents(SITE_ROOT . '/modules/login/html/login_form.html');
	$body = str_ireplace('<!-- $W1 -->', $W1, $body);
	echo $body;
}

?>