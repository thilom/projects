<?php

/**
 * A set of common PHP fuctions
 *
 * @author Thilo Muller (2012)
 * @version $Id$
 */

/**
 * Returns the current content manager settings
 */
function get_settings($tag) {
	$setting_value = '';

	$statement = "SELECT value
				FROM {$GLOBALS['db_prefix']}_settings
				WHERE tag=:tag
				LIMIT 1";
	$sql_settings = $GLOBALS['dbCon']->prepare($statement);
	$sql_settings->bindParam(':tag', $tag);
	$sql_settings->execute();
	$sql_data = $sql_settings->fetch();
	$sql_settings->closeCursor();
	if (isset($sql_data)) $setting_value = $sql_data['value'];

	return $setting_value;
}

?>
