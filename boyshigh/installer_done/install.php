<?php
/**
 * Installer file for modularus.
 *
 * @version $Id: install.php 28 2011-03-10 08:16:19Z thilo $
 */

//Inits
session_start();

$body = file_get_contents('html/install.html');
$p = '';
$db_prefix = 'modularus';

//unset($_SESSION['install']);
if (!isset($_SESSION['install'])) {
	$_SESSION['install']['system_check'] = 0;
	$_SESSION['install']['database_settings'] = 0;
	$_SESSION['install']['database_structure'] = 0;
	$_SESSION['install']['administration_user'] = 0;
	$_SESSION['install']['modularus_settings'] = 0;
}

if ($_SESSION['install']['system_check'] != 1) {
	$body = system_check ($body);
} else if ($_SESSION['install']['database_settings'] != 1) {
	$body = database_settings($body);
} else if ($_SESSION['install']['database_structure'] != 1) {
	$body = create_database_structure($body);
} else if ($_SESSION['install']['administration_user'] != 1) {
	$body = administration_user($body);
} else if ($_SESSION['install']['modularus_settings'] != 1) {
	$body = modularus_settings($body);
} else {
	$body = clean_up($body);
}

$body = menu($body);

echo $body;

function menu($body) {
	if ($_SESSION['install']['system_check'] == 1) {
		$body = str_replace('<!-- system_check -->', '<span class="ok">OK</span>', $body);
	} else if ($_SESSION['install']['system_check'] == -1) {
		$body = str_replace('<!-- system_check -->', '<span class="fail">FAIL</span>', $body);
	}

	if ($_SESSION['install']['database_settings'] == 1) {
		$body = str_replace('<!-- database_settings -->', '<span class="ok">OK</span>', $body);
	} else if ($_SESSION['install']['database_settings'] == -1) {
		$body = str_replace('<!-- database_settings -->', '<span class="fail">FAIL</span>', $body);
	}

	if ($_SESSION['install']['database_structure'] == 1) {
		$body = str_replace('<!-- create_database_structure -->', '<span class="ok">OK</span>', $body);
	} else if ($_SESSION['install']['database_structure'] == -1) {
		$body = str_replace('<!-- create_database_structure -->', '<span class="fail">FAIL</span>', $body);
	}

	if ($_SESSION['install']['administration_user'] == 1) {
		$body = str_replace('<!-- administration_user -->', '<span class="ok">OK</span>', $body);
	} else if ($_SESSION['install']['administration_user'] == -1) {
		$body = str_replace('<!-- administration_user -->', '<span class="fail">FAIL</span>', $body);
	}

	if ($_SESSION['install']['modularus_settings'] == 1) {
		$body = str_replace('<!-- modularus_settings -->', '<span class="ok">OK</span>', $body);
	} else if ($_SESSION['install']['modularus_settings'] == -1) {
		$body = str_replace('<!-- modularus_settings -->', '<span class="fail">FAIL</span>', $body);
	}

	return $body;
}

function system_check($body) {
	$p = file_get_contents('html/system_check.html');
	$f = false;

	//Database File
	if (!is_writable($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php')) {
		if (function_exists('chmod')) {
			@chmod($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php', 0777);
			if (!is_writable($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php')) {
				$p = str_replace('<!-- database_file_writable -->', '<span class="fail">FAIL</span>', $p);
				$f=true;
			} else {
				$p = str_replace('<!-- database_file_writable -->', '<span class="ok">OK</span>', $p);
			}
		} else {
			$p = str_replace('<!-- database_file_writable -->', '<span class="fail">FAIL</span>', $p);
			$f=true;
		}

	} else {
		$p = str_replace('<!-- database_file_writable -->', '<span class="ok">OK</span>', $p);
	}

	//PHP Version
	$version = explode('.', PHP_VERSION);
	if ($version[0] < 5) {
		$p = str_replace('<!-- php_version -->', '<span class="fail">FAIL</span>', $p);
		$f=true;
	} else {
		$p = str_replace('<!-- php_version -->', '<span class="ok">OK</span>', $p);
	}
	$p = str_replace('<!-- version -->', "{$version[0]}.{$version[1]}", $p);

	//MySQL Version
	ob_start();
	include 'sinfo.php';
	$file = ob_get_contents();
	ob_end_clean();
	$start = explode("<h2><a name=\"module_mysql\">mysql</a></h2>",$file,1000);
	if(count($start) < 2){
		$mysql_version = 0;
	}else{
		$again = explode("<tr><td class=\"e\">Client API version </td><td class=\"v\">",$start[1],1000);
		$last_time = explode(" </td></tr>",$again[1],1000);
		$mysql_version = $last_time[0];
	}
	$mver = explode('.', $mysql_version);
	if ($mver[0] < 5) {
		$p = str_replace('<!-- mysql_version -->', '<span class="fail">FAIL</span>', $p);
		$f=true;
	} else {
		$p = str_replace('<!-- mysql_version -->', '<span class="ok">OK</span>', $p);
	}
	$p = str_replace('<!-- mversion -->', $mysql_version, $p);

	//PDO available
	if (phpversion('PDO') != '') {
		$p = str_replace('<!-- pdo -->', '<span class="ok">OK</span>', $p);
	} else {
		$p = str_replace('<!-- pdo -->', '<span class="fail">FAIL</span>', $p);
		$f=true;
	}
	$p = str_replace('<!-- pdo_version -->', phpversion('PDO'), $p);

	if ($f) {
		$p = str_replace('<!-- next_disabled -->', 'disabled', $p);
		$p = str_replace('<!-- refresh_disabled -->', '', $p);
		$_SESSION['install']['system_check'] = -1;
	} else {
		$p = str_replace('<!-- next_disabled -->', '', $p);
		$p = str_replace('<!-- refresh_disabled -->', 'disabled', $p);
		$_SESSION['install']['system_check'] = 1;
	}

	$body = str_replace('<!-- body -->', $p, $body);
	return $body;
}

function database_settings($body) {
	$p = file_get_contents('html/database_settings.html');
	$message = '';

	if (isset($_POST['db_server'])) {

		//Write settings to file
		$fh = fopen($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php', 'w');
		fwrite($fh, "<?php" . PHP_EOL);
		fwrite($fh, "\$db_server = '{$_POST['db_server']}';" . PHP_EOL);
		fwrite($fh, "\$db_name = '{$_POST['db_name']}';" . PHP_EOL);
		fwrite($fh, "\$db_username = '{$_POST['db_username']}';" . PHP_EOL);
		fwrite($fh, "\$db_password = '{$_POST['db_password']}';" . PHP_EOL);
		fwrite($fh, "?>" . PHP_EOL);
		fclose($fh);

		include($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php');

		try {
			$connect = "mysql:host=$db_server;dbname=$db_name";
			$dbCon = new PDO($connect, $db_username, $db_password);
			$dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$_SESSION['install']['database_settings'] = 1;
			die("<script>document.location='/installer/install.php'</script>");
		} catch(PDOException $e) {
			$message = '<span class="msg">Failed to connect to DB. Please check your settings and try again</span>';
			$_SESSION['install']['database_settings'] = -1;
		}

		$p = str_replace('<!-- message -->', $message, $p);
	}



	include($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php');

	$p = str_replace('!db_server!', $db_server, $p);
	$p = str_replace('!db_name!', $db_name, $p);
	$p = str_replace('!db_username!', $db_username, $p);
	$p = str_replace('!db_password!', $db_password, $p);

	$body = str_replace('<!-- body -->', $p, $body);
	return $body;
}

function create_database_structure($body) {
	include('database_structure.php');

	include($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php');

	try {
		$connect = "mysql:host=$db_server;dbname=$db_name";
		$dbCon = new PDO($connect, $db_username, $db_password);
		$dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {

	}

	$p = file_get_contents('html/database_structure.html');
	$c = '';

	foreach($sql_statement as $k=>$statement) {
		$sql_exe = $dbCon->prepare($statement);
		$sql_exe->execute();

		$c .= "<tr><th>{$sql_name[$k]}</th><td><span class='ok'>OK</span></td></tr>";
	}

	$_SESSION['install']['database_structure'] = 1;

	$p = str_replace('<!-- table -->', $c, $p);
	$body = str_replace('<!-- body -->', $p, $body);
	return $body;
}

function administration_user($body) {
	$p = file_get_contents('html/administration_user.html');

	if (isset($_POST['administration_user'])) {
		include($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php');

		try {
			$connect = "mysql:host=$db_server;dbname=$db_name";
			$dbCon = new PDO($connect, $db_username, $db_password);
			$dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {}


		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_user (user_id,email,np,firstname,lastname,username,date_created) VALUES (1,:email,:np,:firstname,:lastname,:username,NOW())";
		$sql_admin = $dbCon->prepare($statement);
		$sql_admin->bindParam(':email', $_POST['email']);
		$sql_admin->bindParam(':np', MD5($_POST['password']));
		$sql_admin->bindParam(':firstname', $_POST['firstname']);
		$sql_admin->bindParam(':lastname', $_POST['lastname']);
		$sql_admin->bindParam(':username', $_POST['username']);
		$sql_admin->execute();
		$sql_admin->closeCursor();

		$statement = "INSERT INTO modularus_user_access (user_id, module_id,access) VALUES (1,'mod_users','y')";
		$sql_user = $dbCon->prepare($statement);
		$sql_user->execute();
		$sql_user->closeCursor();

		$_SESSION['install']['administration_user'] = 1;
		die("<script>document.location='/installer/install.php'</script>");
	}

	$body = str_replace('<!-- body -->', $p, $body);
	return $body;
}

function modularus_settings($body) {
	$p = file_get_contents('html/modularus_settings.html');

	if (isset($_POST['modularus_settings'])) {
		include($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php');

		try {
			$connect = "mysql:host=$db_server;dbname=$db_name";
			$dbCon = new PDO($connect, $db_username, $db_password);
			$dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {}

		if (isset($_POST['approval_system'])) {
			switch ($_POST['approval_system_type']) {
				case 'single':
					$approval = 'single';
					break;
				case 'all':
					$approval = 'all';
					break;
				default:
					$approval = 'none';
			}
		} else {
			$approval = 'none';
		}

		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings (tag, value) VALUES ('approval_system', :value)";
		$sql_settings = $dbCon->prepare($statement);
		$sql_settings->bindParam(':value', $approval);

		$sql_settings->execute();

		$_SESSION['install']['modularus_settings'] = 1;
		die("<script>document.location='/installer/install.php'</script>");

	}


	$body = str_replace('<!-- body -->', $p, $body);
	return $body;
}

function clean_up($body) {
	$p = file_get_contents('html/clean_up.html');

	if (is_writable($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php')) {
		if (function_exists('chmod')) {
			@chmod($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php', 0644);
			if (is_writable($_SERVER['DOCUMENT_ROOT'] . '/settings/database.php')) {
				$p = str_replace('<!-- database_file_writable -->', '<span class="fail">FAIL</span>', $p);
				$f=true;
			} else {
				$p = str_replace('<!-- database_file_writable -->', '<span class="ok">OK</span>', $p);
			}
		} else {
			$p = str_replace('<!-- database_file_writable -->', '<span class="fail">FAIL</span>', $p);
			$f=true;
		}

	} else {
		$p = str_replace('<!-- database_file_writable -->', '<span class="ok">OK</span>', $p);
	}


	rename($_SERVER['DOCUMENT_ROOT'] . '/installer', $_SERVER['DOCUMENT_ROOT'] . '/installer_done');
	if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/installer/')) {
		$p = str_replace('<!-- remove_installer -->', '<span class="fail">FAIL</span>', $p);
		$f=true;
	} else {
		$p = str_replace('<!-- remove_installer -->', '<span class="ok">OK</span>', $p);
	}

	$body = str_replace('<!-- body -->', $p, $body);
	return $body;
}
?>
