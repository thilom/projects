<?php
/**
 * This file contains the database structure for Modularus.
 *
 * @version $Id: database_structure.php 30 2011-03-10 08:56:58Z thilo $
 */

/**
 * The SQL create statement for the installer
 *
 * @version $Id: database_structure.php 30 2011-03-10 08:56:58Z thilo $
 */
$sql_statement = array();

/**
 * The name of the corresponding SQL statement. This name is used in the
 * installer to show the user what has been created.
 */
$sql_name = array();

$sql_name[0] = "Creating `modularus_areas`";
$sql_statement[0] = "CREATE TABLE `modularus_areas` (
					  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
					  `area_id` varchar(50) NOT NULL,
					  `title` varchar(64) DEFAULT NULL,
					  `page_id` int(16) DEFAULT '0',
					  `type` int(4) NOT NULL DEFAULT '0',
					  `module` varchar(64) DEFAULT NULL,
					  `shared` int(1) DEFAULT '0',
					  `header` text,
					  `footer` text NOT NULL,
					  `gizmo` text NOT NULL,
					  `gizmo_live` text NOT NULL,
					  `gizmo_temp` text NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[1] = "Creating `modularus_page_contents`";
$sql_statement[1] = "CREATE TABLE `modularus_page_contents` (
					  `content_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
					  `page_id` int(6) NOT NULL DEFAULT '0',
					  `content_area_name` varchar(50) NOT NULL DEFAULT '',
					  `module` text NOT NULL,
					  `shared_content` char(1) NOT NULL DEFAULT '',
					  PRIMARY KEY (`content_id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[2] = "Creating `modularus_pages`";
$sql_statement[2] = "CREATE TABLE `modularus_pages` (
					  `page_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
					  `template_id` int(4) NOT NULL DEFAULT '0',
					  `page_home` char(1) NOT NULL DEFAULT '',
					  `page_name` varchar(30) NOT NULL DEFAULT '',
					  `page_title` varchar(250) NOT NULL DEFAULT '',
					  `page_description` text NOT NULL,
					  `page_keywords` text NOT NULL,
					  `page_members_only` tinyint(4) NOT NULL DEFAULT '0',
					  PRIMARY KEY (`page_id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[3] = "Creating `modularus_settings`";
$sql_statement[3] = "CREATE TABLE `modularus_settings` (
					  `tag` varchar(100) NOT NULL DEFAULT '',
					  `value` text NOT NULL,
					  PRIMARY KEY (`tag`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[4] = "Creating `modularus_templates`";
$sql_statement[4] = "CREATE TABLE `modularus_templates` (
					  `template_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
					  `template_file` varchar(50) NOT NULL DEFAULT '',
					  `default_template` char(1) NOT NULL DEFAULT '',
					  `template_name` varchar(100) NOT NULL DEFAULT '',
					  PRIMARY KEY (`template_id`)
					) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1";

$sql_name[5] = "Creating `login_attempts`";
$sql_statement[5] = "CREATE TABLE `login_attempts` (
					  `la_id` int(11) NOT NULL AUTO_INCREMENT,
					  `la_date` bigint(20) NOT NULL DEFAULT '0',
					  `la_ip` varchar(50) NOT NULL DEFAULT '',
					  `la_username` varchar(100) NOT NULL DEFAULT '',
					  `la_attempt` int(11) NOT NULL DEFAULT '0',
					  PRIMARY KEY (`la_id`)
					) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8";

$sql_name[6] = "Creating `login_log`";
$sql_statement[6] = "CREATE TABLE `login_log` (
				  `ll_id` int(11) NOT NULL AUTO_INCREMENT,
				  `ll_time` bigint(20) NOT NULL DEFAULT '0',
				  `ll_desc` varchar(100) NOT NULL DEFAULT '',
				  PRIMARY KEY (`ll_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8";

$sql_name[7] = "Creating `modularus_pages_owners`";
$sql_statement[7] = "CREATE TABLE `modularus_pages_owners` (
					  `user_id` int(11) DEFAULT NULL,
					  `page_id` int(11) DEFAULT NULL
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[8] = "Creating `modularus_access_template`";
$sql_statement[8] = "CREATE TABLE `modularus_access_template` (
					  `template_id` int(11) NOT NULL AUTO_INCREMENT,
					  `template_name` varchar(250) DEFAULT NULL,
					  PRIMARY KEY (`template_id`)
					) ENGINE=MyISAM DEFAULT CHARSET=macce";

$sql_name[9] = "Creating `modularus_access_template_data`";
$sql_statement[9] = "CREATE TABLE `modularus_access_template_data` (
					  `template_id` int(10) unsigned NOT NULL,
					  `module_id` varchar(50) DEFAULT NULL,
					  `function_id` varchar(100) DEFAULT NULL,
					  `parent_id` varchar(100) DEFAULT NULL,
					  `access` char(1) DEFAULT NULL
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[10] = "Creating `modularus_logs`";
$sql_statement[10] = "CREATE TABLE `modularus_logs` (
					  `log_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					  `user_id` int(11) DEFAULT NULL,
					  `log_message` varchar(265) DEFAULT NULL,
					  `module_id` varchar(50) DEFAULT NULL,
					  `target` varchar(20) DEFAULT NULL,
					  `alt_target` varchar(20) DEFAULT NULL,
					  KEY `NewIndex1` (`user_id`)
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[11] = "Creating `modularus_user`";
$sql_statement[11] = "CREATE TABLE `modularus_user` (
						  `user_id` int(10) unsigned NOT NULL,
						  `email` varchar(100) DEFAULT NULL,
						  `np` varbinary(200) DEFAULT NULL,
						  `user_type_id` int(11) DEFAULT NULL,
						  `firstname` varchar(45) DEFAULT NULL,
						  `lastname` varchar(45) DEFAULT NULL,
						  `phone` varchar(45) DEFAULT NULL,
						  `cell` varchar(45) DEFAULT NULL,
						  `top_level_type` char(1) DEFAULT NULL,
						  `username` varchar(100) DEFAULT NULL,
						  `date_created` datetime DEFAULT NULL,
						  `inactive` char(1) DEFAULT NULL,
						  PRIMARY KEY (`user_id`)
						) ENGINE=MyISAM DEFAULT CHARSET=latin1";

$sql_name[12] = "Creating `modularus_user_access`";
$sql_statement[12] = "CREATE TABLE `modularus_user_access` (
					  `user_id` int(10) unsigned NOT NULL,
					  `module_id` varchar(50) DEFAULT NULL,
					  `function_id` varchar(100) DEFAULT NULL,
					  `parent_id` varchar(100) DEFAULT NULL,
					  `access` char(1) DEFAULT NULL
					) ENGINE=MyISAM DEFAULT CHARSET=latin1";

//$sql_name[13] = "";
//$sql_statement[13] = "";

$sql_name[14] = "Creating `modularus_user_approve`";
$sql_statement[14] = "CREATE TABLE `modularus_user_approve` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `approved_by` int(10) unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1";

?>
