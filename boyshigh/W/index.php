<?php
echo "<html>";
echo "<title>Administration Panel</title>";
echo "<link rel='shortcut icon' href='/W/favicon.ico' >";
echo "<HEAD>";

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] .  '/modules/content_manager/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'] .  '/settings/variables.php';

// get fluid css
include $_SERVER['DOCUMENT_ROOT']."/shared/directory_item.php";
getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","W.css");
if($itemname)
	echo "<link rel=stylesheet href=/i/fluid/".$itemname.">";
else
	echo "<link rel=stylesheet href=/W/W.css>";

echo "<script src=W.js></script>";
echo "<script src=/shared/T.js></script>";
//echo "<script src=/shared/C.js></script>";

echo "</HEAD>";
echo "<BODY";
//if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.gif"))
//{
//	echo " style=background:url(/i/fluid/DB.";
//	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.png"))
//		echo "png";
//	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DB.jpg"))
//		echo "jpg";
//	else
//		echo "gif";
//	echo ")";
//}
echo " onLoad='preload()'>";

//if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.gif"))
//{
//	echo "<table id=Wf><tr><td style='background:url(/i/fluid/DF.";
//	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.png"))
//		echo "png";
//	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DF.jpg"))
//		echo "jpg";
//	else
//		echo "gif";
//	echo ") no-repeat center center'></td></tr></table>";
//}

if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.gif"))
{
	echo "<table id=Wb><tr><td><img src=/i/fluid/L.";
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.png"))
		echo "png";
	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/L.jpg"))
		echo "jpg";
	else
		echo "gif";
	echo "></td></tr></table>";
}
elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/W/F/L.png"))
	echo "<table id=Wb><tr><td><img src=F/L.png><br><span style='color: gray; font-size: .8em'>{$_SERVER['HTTP_HOST']} | Version: {$GLOBALS['scrum_version']}</span></td></tr></table>";

echo "<table id=WnB class=i><tr><td></td><th></th></tr></table>";
echo "<table id=Wn onmouseover=Wn0(1) onmouseout=Wn0()><tr><th>";
if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.png")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.jpg")||file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.gif"))
{
	echo "<img src=/i/fluid/DL.";
	if(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.png"))
		echo "png";
	elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/i/fluid/DL.jpg"))
		echo "jpg";
	else
		echo "gif";
	echo " class=WL>";
}
echo "</th></tr></table>";

echo "<i id=Wm><a href=javascript:void(Wn0(1)) onfocus=blur() class=m>modules</a></i>";

echo "</body>";
echo "</html>";

//Check if already installed
$installed = true;
if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/installer/')) $installed = false;

$li = isset($_SESSION['dbweb_user_id'])?'0':'1';
echo $installed?"<script>WS(0,$li)</script>":"<script>WS(1)</script>";


//Preload icons
//Temporary since we can't use get_module_variables here. Need to find out why

echo "<script>";
echo "ims = new Array();";
echo "ims[0] = '/modules/products/i/hot_products_64.png';";
echo "ims[1] = '/W/F/off_logout_64.png';";
echo "ims[2] = '/modules/news/i/hot_news_64.png';";
echo "ims[3] = '/modules/places/i/hot_places_64.png';";
echo "ims[4] = '/modules/content_manager/i/hot_content_64.png';";
echo "ims[5] = '/modules/sports/i/hot_sports_64.png';";
echo "ims[6] = '/modules/support/i/hot_comment_64.png';";
echo "ims[7] = '/modules/vouchers/i/hot_check_64.png';";
echo "ims[8] = '/modules/people/i/hot_people_64.png';";
echo "ims[9] = '/modules/locations/i/hot_locations_64.png';";
echo "ims[10] = '/modules/cart/i/hot_cart_64.png';";
echo "ims[11] = '/modules/settings/i/hot_settings_64.png';";
echo "ims[12] = '/modules/tour/i/hot_tour_64.png';";
echo "ims[13] = '/modules/template_manager/i/hot_template_64.png';";
echo "ims[14] = '/modules/help/i/hot_help_64.png';";
echo "ims[15] = '/modules/quote/i/hot_quote_64.png';";
echo "function preload() {for (i=0; i<ims.length; i++) { var img = new Image(); img.src=ims[i];}}</script>";
?>