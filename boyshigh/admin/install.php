<?php
$SQL_statement = array();
$SQL_statement[] = "CREATE TABLE `dbweb_areas` (
										  `id` int(11) NOT NULL auto_increment,
										  `area_id` int(8) NOT NULL default '0',
										  `title` varchar(64) default NULL,
										  `page_id` int(16) default '0',
										  `type` int(4) NOT NULL default '0',
										  `module` varchar(64) default NULL,
										  `shared` int(1) default '0',
										  `header` text,
										  `footer` text NOT NULL,
										  `gizmo` text NOT NULL,
										  `gizmo_live` text NOT NULL,
										  `gizmo_temp` text NOT NULL,
										  PRIMARY KEY  (`id`)
										)  ";

$SQL_statement[] = "CREATE TABLE `dbweb_page_contents` (
										  `content_id` int(7) unsigned NOT NULL auto_increment,
										  `page_id` int(6) NOT NULL default '0',
										  `content_area_name` varchar(50) NOT NULL default '',
										  `module` text NOT NULL,
										  `shared_content` char(1) NOT NULL default '',
										  PRIMARY KEY  (`content_id`)
										) ";

$SQL_statement[] = "CREATE TABLE `dbweb_pages` (
										  `page_id` int(6) unsigned NOT NULL auto_increment,
										  `template_id` int(4) NOT NULL default '0',
										  `page_home` char(1) NOT NULL default '',
										  `page_name` varchar(30) NOT NULL default '',
										  `page_title` varchar(250) NOT NULL default '',
										  `page_description` text NOT NULL,
										  `page_keywords` text NOT NULL,
										  `page_members_only` tinyint(4) NOT NULL default '0',
										  PRIMARY KEY  (`page_id`)
										) ";

$SQL_statement[] = "CREATE TABLE `dbweb_settings` (
										  `tag` varchar(100) NOT NULL default '',
										  `value` text NOT NULL,
										  PRIMARY KEY  (`tag`)
										) ";

$SQL_statement[] = "CREATE TABLE `dbweb_templates` (
										  `template_id` int(4) unsigned NOT NULL auto_increment,
										  `template_file` varchar(50) NOT NULL default '',
										  `default_template` char(1) NOT NULL default '',
										  `template_name` varchar(100) NOT NULL default '',
										  PRIMARY KEY  (`template_id`)
										) ";

$SQL_statement[] = "INSERT INTO `dbweb_settings` (`tag`, `value`) VALUES ('rCode', ''),
											('google_verification_id', ''),
											('ftp_host', ''),
											('ftp_username', ''),
											('ftp_password', ''),
											('ftp_dir', ''),
											('google_analytics', ''),
											('meta_title', ''),
											('meta_description', ''),
											('meta_keywords', '')";

$SQL_statement[] = "CREATE TABLE `login_attempts` (
										  `la_id` int(11) NOT NULL auto_increment,
										  `la_date` bigint(20) NOT NULL default '0',
										  `la_ip` varchar(50) NOT NULL default '',
										  `la_username` varchar(100) NOT NULL default '',
										  `la_attempt` int(11) NOT NULL default '0',
										  PRIMARY KEY  (`la_id`)
										)";

$SQL_statement[] = "CREATE TABLE `login_log` (
										  `ll_id` int(11) NOT NULL auto_increment,
										  `ll_time` bigint(20) NOT NULL default '0',
										  `ll_desc` varchar(100) NOT NULL default '',
										  PRIMARY KEY  (`ll_id`)
										)";

$SQL_statement[] = "CREATE TABLE `login_user` (
										  `du_id` int(11) NOT NULL auto_increment,
										  `du_pass` varchar(50) NOT NULL default '',
										  `du_username` varchar(100) NOT NULL default '',
										  `du_email` varchar(150) NOT NULL default '',
										  `du_name` varchar(100) NOT NULL default '',
										  PRIMARY KEY  (`du_id`)
										)";

$SQL_statement[] = "INSERT INTO `login_user` (`du_id`, `du_pass`, `du_username`, `du_email`, `du_name`) 
										VALUES 
										(6, '21232f297a57a5a743894a0e4a801fc3', 'Administrator', '', 'Administrator')";

$SQL_statement[] = "CREATE TABLE `mailinglist_mails` (
										  `mail_id` int(10) unsigned NOT NULL auto_increment,
										  `mail_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
										  `mail_subject` varchar(100) NOT NULL default '',
										  `mail_body` text NOT NULL,
										  PRIMARY KEY  (`mail_id`)
										)";

$SQL_statement[] = "CREATE TABLE `sitemap_data` (
										  `link_name` varchar(100) NOT NULL default '',
										  `link` varchar(100) default NULL,
										  `group_id` int(5) default NULL,
										  PRIMARY KEY  (`link_name`)
										)";


foreach ($SQL_statement as $statement) {
	mysql_query($statement);
}

?>