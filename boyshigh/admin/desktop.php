<?php
//Define modules directory
//$dir = SITE_ROOT . '/modules/';
$access_list = array();

//Get access rights
$statement = "SELECT module_id FROM {$GLOBALS['db_prefix']}_user_access WHERE user_id=:user_id AND access='y'";
$sql_access = $GLOBALS['dbCon']->prepare($statement);
$sql_access->bindParam(':user_id', $_SESSION['dbweb_user_id']);
$sql_access->execute();
$sql_data = $sql_access->fetchAll();
$sql_access->closeCursor();

foreach ($sql_data as $data) {
	$access_list[] = $data['module_id'];
}

	$module_vars = get_module_variables();
//	var_dump($module_vars);
	foreach ($module_vars as $module_data) {
		 if (!empty($module_data['module_icon']) && isset($module_data['module_id'])) {
			$window_width = isset($module_data['window_width'])&&!empty($module_data['window_width'])?$module_data['window_width']:'0';
			$window_height = isset($module_data['window_height'])&&!empty($module_data['window_height'])?$module_data['window_height']:'480';
			$window_xposition = isset($module_data['window_xposition'])&&!empty($module_data['window_xposition'])?$module_data['window_xposition']:'0';
			$window_yposition =isset($module_data['window_yposition'])&& !empty($module_data['window_yposition'])?$module_data['window_yposition']:'0';
			$window_attribute = isset($module_data['window_attribute'])&&!empty($module_data['window_attribute'])?$module_data['window_attribute']:'0';

//Create Icon

			if (isset($module_data['security_type']) && $module_data['security_type'] == 's') {
				if ($module_data['disabled']===false) {
					if (in_array($module_data['security_id'], $access_list) || $_SESSION['user_type'] == 'developer')  {
						echo ";P.WN(1,'{$module_data['module_name']}','/modules/{$module_data['module_id']}/i/{$module_data['module_icon']}','/modules/{$module_data['module_id']}/{$module_data['module_link']}',$window_height,$window_width, $window_xposition, $window_yposition, $window_attribute)";
					}
				}
			} else {
				echo ";P.WN(1,'{$module_data['module_name']}','/modules/{$module_data['module_id']}/i/{$module_data['module_icon']}','/modules/{$module_data['module_id']}/{$module_data['module_link']}',$window_height,$window_width, $window_xposition, $window_yposition, $window_attribute)";
			}
		}
	}



?>