<?php
error_reporting(0);
session_start();
include_once("include/include.php");
	
if ( $SETTINGS["useCookie"] == false and isset($_SESSION["PHPEventCalendarLoGIn"])) $temp_sid=$_SESSION["PHPEventCalendarLoGIn"];
elseif (isset($_COOKIE["PHPEventCalendarLoGIn"])) $temp_sid=$_COOKIE["PHPEventCalendarLoGIn"];

$md_sum=md5($SETTINGS["admin_username"].$SETTINGS["admin_password"]);
$md_res=substr($temp_sid,0,strlen($md_sum));
if (strcmp($md_sum,$md_res)==0) {
	$ts=substr($temp_sid,strlen($md_sum));
	if ($ts>time()) $isLogged = true;
}

if ( $isLogged ){
	
	$sql = "SELECT * FROM ".$TABLES["Calendars"]." WHERE id='".SaveToDB($_REQUEST["cid"])."'";
	$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
	$Calendar = mysql_fetch_assoc($sql_result);
	$CalendarOptions = unserialize(ReadHTMLFromDB($Calendar["options"]));

	$sql = "SELECT * FROM ".$TABLES["Days"]." WHERE calendarId='".SaveToDB($_REQUEST["cid"])."' AND dt='".date("Y-m-d",$_REQUEST["dt"])."'";
	$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
	$DayData = mysql_fetch_assoc($sql_result);
?>
<br />
<a href="admin.php?ac=add_event&dt=<?php echo $_REQUEST["dt"]; ?>&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink">New Event</a>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer" style="margin-top:10px">
      <tr>
        <td colspan="3" align="left" valign="middle" bgcolor="#f6f6f6"><strong><?php echo getFormatedDate($_REQUEST["dt"]); ?></strong></td>
        <td colspan="4" align="left" valign="top" bgcolor="#f6f6f6">
		  <form action="admin.php" method="post" name="frm" style="margin:0px; padding:0px">
		  <input type="hidden" name="ac" value="update_day_color" />
		  <input type="hidden" name="cid" value="<?php echo $_REQUEST["cid"]; ?>" />
		  <input type="hidden" name="dt" value="<?php echo $_REQUEST["dt"]; ?>" />
		  <input type="hidden" name="did" value="<?php echo $DayData["id"]; ?>" />
		    Color: 
			<input name="dayColor" type="text" id="dayColor" size="8" maxlength="7" value="<?php echo ReadFromDB($DayData["dayColor"]); ?>" />&nbsp;
<a href="#" onClick="cp.select(frm.dayColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image99" width="18" height="19" border="0" align="absmiddle" id="Image99" /></a>&nbsp;&nbsp;
	<input name="Image9" type="image" id="Image9"  onMouseOver="MM_swapImage('Image9','','images/save-btn-over.png',1)" onMouseOut="MM_swapImgRestore()" src="images/save-btn.png" align="bottom">
		  </form>
		</td>
      </tr>
      <tr>
        <td width="4%" valign="top" class="PHPCalendarHeading">&nbsp;</td>
        <td width="12%" valign="top" class="PHPCalendarHeading">Start Time</td>
        <td width="12%" valign="top" class="PHPCalendarHeading">End Time </td>
        <td width="28%" valign="top" class="PHPCalendarHeading">Summary</td>
        <td width="34%" valign="top" class="PHPCalendarHeading">Description</td>
        <td width="10%" colspan="2" valign="top" class="PHPCalendarHeading">&nbsp;</td>
      </tr>
<?php 
	$sql = "SELECT * FROM ".$TABLES["Events"]." WHERE calendarId='".SaveToDB($_REQUEST["cid"])."' AND dt='".date("Y-m-d",$_REQUEST["dt"])."' ORDER BY dt,startTime,endTime";
	$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
	if (mysql_num_rows($sql_result)>0) {
		while ($EventData = mysql_fetch_assoc($sql_result)) {
	?>
      <tr>
        <td align="left" style="border-bottom:1px solid #DFE4E8" bgcolor="#<?php echo ReadFromDB($EventData["color"]); ?>">&nbsp;</td>
        <td align="left" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php if(ReadFromDB($EventData["show_time"])=="1" or ReadFromDB($EventData["show_time"])=="3") echo getFormatedTime(ReadFromDB($EventData["startTime"])); ?>&nbsp;</td>
        <td align="left" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php if(ReadFromDB($EventData["show_time"])=="1" or ReadFromDB($EventData["show_time"])=="2") echo getFormatedTime(ReadFromDB($EventData["endTime"])); ?>&nbsp;</td>
        <td align="left" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php echo ReadFromDB($EventData["title"]); ?>&nbsp;</td>
        <td align="left" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php 
		if (strlen(ReadFromDB($EventData["description"]))>300) 
			echo substr(ReadHTMLFromDB($EventData["description"]),0,300).'...';
		else 
			echo ReadHTMLFromDB($EventData["description"]);
		?></td>
        <td width="5%" align="center" valign="top" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8">
<a href="admin.php?ac=add_event&dt=<?php echo $_REQUEST["dt"]; ?>&cid=<?php echo $_REQUEST["cid"]; ?>&eid=<?php echo $EventData["id"]; ?>"/><img src="images/edit.png" alt="Edit Event" title="Edit Event" width="17" height="16" border="0" align="absmiddle" /></a></td>
        <td width="5%" align="center" valign="top" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8">
<a href='#' onclick='pass=confirm("Are you sure you want to delete this event?",""); if (pass) window.location="admin.php?ac=delEvent&cid=<?php echo $_REQUEST["cid"]; ?>&eid=<?php echo $EventData["id"]; ?>&dt=<?php echo $_REQUEST["dt"]; ?>";'><img src="images/delete.png" alt="Delete Event" title="Delete Event" width="17" height="15" border="0" align="absmiddle" /></a></td>
      </tr>
      <?php 
		};
	} else {
?>
      <tr>
        <td colspan="7" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php echo $PHP_EVENT_LANG["noEventMsg"]; ?></td>
	  </tr>	        
<?php	
	};	
?>
</table>
<?php
} else echo $PHP_EVENT_LANG['YouNeedToLoginFirst']; 
?>