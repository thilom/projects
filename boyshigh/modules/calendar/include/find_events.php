<?php
defined( '_PHPEventCalendar' ) or die( 'Restricted access' );

    if(isset($_REQUEST["p"])) $pageNum = $_REQUEST["p"];
    else $pageNum = 1;
    if(isset($_REQUEST["orderBy"])) $orderBy = $_REQUEST["orderBy"];
    else $orderBy = "dt";
    if(isset($_REQUEST["orderType"])) $orderType = $_REQUEST["orderType"];
    else $orderType = "DESC";
	if ($orderType == 'DESC') { $norderType = 'ASC'; } else { $norderType = 'DESC'; };
?>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>

<form action="admin.php" method="post" name="frmFindEvent" style="margin:0px; padding:0px">
<input type="hidden" name="ac" value="find_event" />
<input type="hidden" name="cid" value="<?php echo $_REQUEST["cid"]; ?>" />
	<table width="100%" border="0" cellspacing="2" cellpadding="2">
        <tr>
        	<td width="53%" align="left" valign="middle"><?php echo $PHP_EVENT_LANG['FindInfo']; ?>: 
   	      <input type="text" name="txt_search" style='width:40%' value="<?php echo $_REQUEST["txt_search"]; ?>" ></td>
        	<td width="47%" align="left" valign="middle"><input name="Image9" type="image" id="Image9"  onmouseover="MM_swapImage('Image9','','images/search-btn-over.png',1)" onmouseout="MM_swapImgRestore()" src="images/search-btn.png" /></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer" style="margin-top:15px">
		<tr>
		  <td width="17%" valign="top" class="PHPCalendarHeading"><a href="admin.php?ac=find_event&cid=<?php echo $_REQUEST["cid"]; ?>&orderType=<?php echo $norderType; ?>&txt_search=<?php echo urlencode($_REQUEST["txt_search"]); ?>&orderBy=dt" style="color:#FFF"><strong><?php echo $PHP_EVENT_LANG['FindDate']; ?></strong></a></td>
		  <td width="21%" valign="top" class="PHPCalendarHeading"><a href="admin.php?ac=find_event&cid=<?php echo $_REQUEST["cid"]; ?>&orderType=<?php echo $norderType; ?>&txt_search=<?php echo urlencode($_REQUEST["txt_search"]); ?>&orderBy=startTime" style="color:#FFF"><strong><?php echo $PHP_EVENT_LANG['FindTime']; ?></strong></a></td>
		  <td width="49%" valign="top" class="PHPCalendarHeading"><a href="admin.php?ac=find_event&cid=<?php echo $_REQUEST["cid"]; ?>&orderType=<?php echo $norderType; ?>&txt_search=<?php echo urlencode($_REQUEST["txt_search"]); ?>&orderBy=title" style="color:#FFF"><strong><?php echo $PHP_EVENT_LANG['FindTitle']; ?></strong></a></td>
		  <td width="7%" valign="top" class="PHPCalendarHeading">&nbsp;</td>
		  <td width="6%" valign="top" class="PHPCalendarHeading">&nbsp;</td>
		</tr>
	<?php 
		$txt_search = '';
		if($_REQUEST["txt_search"]!="") $txt_search = " AND (title LIKE '%".SaveToDB($_REQUEST["txt_search"])."%' OR description LIKE '%".SaveToDB($_REQUEST["txt_search"])."%')";
		
		$sql = "SELECT * FROM ".$TABLES["Events"]." WHERE calendarId='".SaveToDB($_REQUEST["cid"])."' ".$txt_search." ORDER BY " . SaveToDB($orderBy) . " " . SaveToDB($orderType);
		$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
		$count = mysql_num_rows($sql_result);

		$pages = ceil($count/$SETTINGS["pageSize"]);
		$sql = $sql . " LIMIT " . ($pageNum-1)*$SETTINGS["pageSize"] . "," . $SETTINGS["pageSize"];	
		$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
		if (mysql_num_rows($sql_result)>0) {
			while ($EventData = mysql_fetch_assoc($sql_result)) {
	?>
		<tr>
		  <td align="left" valign="top" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php echo getFormatedDate(strtotime($EventData["dt"])); ?></td>
		  <td align="left" valign="top" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php if(ReadFromDB($EventData["show_time"])=="1" or ReadFromDB($EventData["show_time"])=="3") {
	echo getFormatedTime(ReadFromDB($EventData["startTime"])); 
};
if(ReadFromDB($EventData["show_time"])=="1" or ReadFromDB($EventData["show_time"])=="2") {
	if(ReadFromDB($EventData["show_time"])=="1" or ReadFromDB($EventData["show_time"])=="3") echo ' - ';
	echo getFormatedTime(ReadFromDB($EventData["endTime"])); 
}; ?>&nbsp;</td>
		  <td align="left" valign="top" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php echo ReadHTMLFromDB($EventData["title"]); ?>&nbsp;</td>
		  <td align="center" valign="top" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><a href="admin.php?ac=add_event&dt=<?php echo strtotime($EventData["dt"]); ?>&cid=<?php echo $_REQUEST["cid"]; ?>&eid=<?php echo $EventData["id"]; ?>"><img src="images/edit.png" alt="Edit Event" title="Edit Event" width="17" height="16" border="0" align="absmiddle" /></a>		</td>
          <td align="center" valign="top" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><a href='#' onclick='pass=confirm("Are you sure you want to delete this event?",""); if (pass) window.location="admin.php?ac=delEvent&cid=<?php echo $_REQUEST["cid"]; ?>&eid=<?php echo $EventData["id"]; ?>&findEvent=1";' ><img src="images/delete.png" alt="Delete Event" title="Delete Event" width="17" height="15" border="0" align="absmiddle" /></a></td>
        </tr>
	<?php 
		};
	} else {
	?>
		<tr><td colspan="5" bgcolor="#f6f6f6" style="border-bottom:1px solid #DFE4E8"><?php echo $PHP_EVENT_LANG["noEventMsg"]; ?></td></tr>
	<?php	
	};	
	?>
  	</table>
<br>&nbsp;
      <?php 
          if ($pages>0) {
              echo "Pages: ";
              for($i=1;$i<=$pages;$i++){ 
                  echo "<a href='admin.php?ac=find_event&cid=".$_REQUEST["cid"]."&txt_search=".urlencode($_REQUEST["txt_search"])."&p=".$i."&orderBy=".$orderBy."&orderType=".$orderType."'>";
                  if($i == $pageNum ) echo "<strong>" .$i. "</strong>";
                  else echo $i; 
                  echo "</a> ";
              };
          };
      ?>  
</form>
