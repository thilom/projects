<?php
$Months["1"]  = "January";
$Months["2"]  = "February";
$Months["3"]  = "March";
$Months["4"]  = "April";
$Months["5"]  = "May";
$Months["6"]  = "June";
$Months["7"]  = "July";
$Months["8"]  = "August";
$Months["9"]  = "September";
$Months["10"] = "October";
$Months["11"] = "November";
$Months["12"] = "December";

$Days["0"] = "Sunday";
$Days["1"] = "Monday";
$Days["2"] = "Tuesday";
$Days["3"] = "Wednesday";
$Days["4"] = "Thursday";
$Days["5"] = "Friday";
$Days["6"] = "Saturday";

$PHP_EVENT_LANG["noEventMsg"] = "No events";
$PHP_EVENT_LANG['YouNeedToLoginFirst'] = 'You need to login first.';

// Admin Menu
$PHP_EVENT_LANG['Calendar'] = 'Calendar';
$PHP_EVENT_LANG['Events'] = 'Events';
$PHP_EVENT_LANG['New'] = 'New';
$PHP_EVENT_LANG['Find'] = 'Find';
$PHP_EVENT_LANG['Export'] = 'Export';
$PHP_EVENT_LANG['Options'] = 'Options';
$PHP_EVENT_LANG['General'] = 'General';
$PHP_EVENT_LANG['Fonts & Colors'] = 'Fonts & Colors';
$PHP_EVENT_LANG['Preview'] = 'Preview';
$PHP_EVENT_LANG['Install'] = 'Install';
$PHP_EVENT_LANG['VideoTutorials'] = 'Video tutorials';
$PHP_EVENT_LANG['Logout'] = 'Logout';

// admin Calendar page
$PHP_EVENT_LANG['AdminCalendar'] = 'Click on a date to display its events and options below. To preview the calendar as it will appear on your web page please click on the "Preview" menu link on the left. <br /><br />';

// admin Add/Edit event
$PHP_EVENT_LANG['EventDateTime'] = 'Event date and time';
$PHP_EVENT_LANG['EventDate'] = 'Date';
$PHP_EVENT_LANG['EventStartTime'] = 'Start time';
$PHP_EVENT_LANG['EventEndTime'] = 'End time';
$PHP_EVENT_LANG['EventRecurring'] = 'Recurring';
$PHP_EVENT_LANG['EventRecurringDay'] = 'Day';
$PHP_EVENT_LANG['EventRecurringWeek'] = 'Week';
$PHP_EVENT_LANG['EventRecurringMonth'] = 'Month';
$PHP_EVENT_LANG['EventRecurringYear'] = 'Year';
$PHP_EVENT_LANG['EventRecurringRepeat'] = 'Repeat every';
$PHP_EVENT_LANG['EventRecurringNext'] = 'for the next';
$PHP_EVENT_LANG['EventDetails'] = 'Event details';
$PHP_EVENT_LANG['EventStatus'] = 'Status';
$PHP_EVENT_LANG['EventStatusShow'] = 'Show';
$PHP_EVENT_LANG['EventStatusHide'] = 'Hide';
$PHP_EVENT_LANG['EventColor'] = 'Color';
$PHP_EVENT_LANG['EventTitle'] = 'Title';
$PHP_EVENT_LANG['EventDescription'] = 'Description';
$PHP_EVENT_LANG['EventApplyRecurring'] = 'Apply to all recurring events';

// admin Find Events
$PHP_EVENT_LANG['FindInfo'] = 'Search by Title or Description';
$PHP_EVENT_LANG['FindDate'] = 'DATE';
$PHP_EVENT_LANG['FindTime'] = 'TIME';
$PHP_EVENT_LANG['FindTitle'] = 'TITLE';


// admin Export
$PHP_EVENT_LANG['StartDate'] = 'Start date';
$PHP_EVENT_LANG['EndDate'] = 'End date';
$PHP_EVENT_LANG['Format'] = 'Format';
$PHP_EVENT_LANG['ExportInfo'] = 'There are multiple ways you can export your events. First, select the period for which you want the events to be exported. Then select the format - CSV file, XML file or iCalendar and click on the Export button.<br /><br />';
$PHP_EVENT_LANG['ExportTitle'] = 'EXPORT';

$PHP_EVENT_LANG['CSV'] = 'CSV stands for Comma Separated Values, sometimes also called Comma Delimited. A CSV file is a specially formatted plain text file which stores spreadsheet or basic database-style information in a very simple format, with one record on each line, and each field within that record separated by a comma and is put within single brakets.';

$PHP_EVENT_LANG['iCalendar'] = 'iCalendar or iCal, format is a way of formatting calendar information for easy exchange between platforms and applications. It is a standard file format for calendar files. iCalendar allows users to share event and meeting information via email and on web sites.';

$PHP_EVENT_LANG['XML'] = 'XML is a standard, simple, self-describing way of encoding both text and data so that content can be processed with relatively little human intervention and exchanged across diverse hardware, operating systems, and applications. XML is nothing special. It is just plain text. Software that can handle plain text can also handle XML. However, XML-aware applications can handle the XML tags specially.';


// admin Options
$PHP_EVENT_LANG['OptionsGeneral'] = 'General options';
$PHP_EVENT_LANG['OptionsGeneralInfo'] = 'Depending on font sizes and styles, the size of the calendar may also change';
$PHP_EVENT_LANG['OptionsWidth'] = 'Width';
$PHP_EVENT_LANG['OptionsHeight'] = 'Height';
$PHP_EVENT_LANG['OptionsWeekStartDay'] = 'Week start day';
$PHP_EVENT_LANG['OptionsMonthYearFormat'] = 'Month / Year format';
$PHP_EVENT_LANG['OptionsDateFormat'] = 'Date format';
$PHP_EVENT_LANG['OptionsTimeFormat'] = 'Time format';
$PHP_EVENT_LANG['OptionsTitlesPosition'] = 'Events titles position';
$PHP_EVENT_LANG['OptionsDescriptionPosition'] = 'Events description position';
$PHP_EVENT_LANG['OptionsBorders'] = 'Borders color and size';
$PHP_EVENT_LANG['OptionsOuterBorderSize'] = 'Outer border size';
$PHP_EVENT_LANG['OptionsOuterBorderColor'] = 'Outer border color';
$PHP_EVENT_LANG['OptionsInnerBorderSize'] = 'Inner border size';
$PHP_EVENT_LANG['OptionsInnerBorderColor'] = 'Inner border color';
$PHP_EVENT_LANG['OptionsArrows'] = 'Month navigation arrows';
$PHP_EVENT_LANG['OptionsPreviousMonth'] = 'Previous month arrow';
$PHP_EVENT_LANG['OptionsNextMonth'] = 'Next month arrow';
$PHP_EVENT_LANG['OptionsColors'] = 'Calendar colors';
$PHP_EVENT_LANG['OptionsMonthBackground'] = 'Month background';
$PHP_EVENT_LANG['OptionsWeekDaysBackground'] = 'Week days background';
$PHP_EVENT_LANG['OptionsDaysEvents'] = 'Day cells with events';
$PHP_EVENT_LANG['OptionsDaysNoEvents'] = 'Day cells without events';
$PHP_EVENT_LANG['OptionsDaysEmpty'] = 'Empty day cells';
$PHP_EVENT_LANG['OptionsFontsStylesSizes'] = 'Fonts colors, styles and sizes';
$PHP_EVENT_LANG['OptionsFontFamily'] = 'Family';
$PHP_EVENT_LANG['OptionsMonthFont'] = 'Month color &amp; size &amp; style';
$PHP_EVENT_LANG['OptionsWeekFont'] = 'Week days color &amp; size &amp; style';
$PHP_EVENT_LANG['OptionsDayEventsFont'] = 'Day cells with events  color &amp; size  &amp; style';
$PHP_EVENT_LANG['OptionsDaysNoEventsFont'] = 'Day cells without events color &amp; size  &amp; style';
$PHP_EVENT_LANG['OptionsDescriptionFont'] = 'Events description';
$PHP_EVENT_LANG['OptionsEventsTitle'] = 'Events title options';
$PHP_EVENT_LANG['OptionsTitlesBackground'] = 'Background color';
$PHP_EVENT_LANG['OptionsTitlesFont'] = 'Font color';
$PHP_EVENT_LANG['OptionsTitlesSize'] = 'Font size';
$PHP_EVENT_LANG['OptionsSummaryPositionTooltip'] = 'Tooltip';
$PHP_EVENT_LANG['OptionsSummaryPositionDay'] = 'In day cells';
$PHP_EVENT_LANG['OptionsDetailsPositionBelow'] = 'Below calendar';
$PHP_EVENT_LANG['OptionsDetailsPositionReplace'] = 'Replace calendar';
$PHP_EVENT_LANG['OptionsDetailsPositionRight'] = 'Right of calendar';


// admin install
$PHP_EVENT_LANG['InstallInfo'] = 'To put the calendar on your web page you will need to put the following code on it. Once you put the calendar on your web page you can make changes (change options and manage events) using the administration page and they will appear on your calendar. There is no need to replace this code when you make changes to your calendar.';
$PHP_EVENT_LANG['InstallHead'] = 'Put this in your web page head tag';
$PHP_EVENT_LANG['InstallCode'] = 'Put this where you want your calendar to appear on the web page';
$PHP_EVENT_LANG['InstallSameDomain'] = 'Please note that the calendar script should be installed on the same domain where the calendar web page is located.';


// admin help messages
$PHP_EVENT_LANG['Help_1'] = 'Select date for the event. Under Options - General page you can change the date format used.';
$PHP_EVENT_LANG['Help_2'] = 'Add start time for your event. If there is no start time just leave ---. Under Options - General page you can switch between 12 and 24 time format.';
$PHP_EVENT_LANG['Help_3'] = 'Add end time for your event. If there is no end time just leave ---. Under Options - General page you can switch between 12 and 24 time format.';
$PHP_EVENT_LANG['Help_4'] = 'If you want to set up a recurring event select how often (day,week,month,year) it will repeat and then for how long.';
$PHP_EVENT_LANG['Help_5'] = 'If you want to hide an event from your front end calendar just set status to Hide.';
$PHP_EVENT_LANG['Help_6'] = 'When you click on a date on the front end calendar there is a small color bar next to each event. You can set its color using this field. You can either select a color from the predefined ones or enter your own HTML color code.';
$PHP_EVENT_LANG['Help_7'] = 'Event title will be displayed on the front end calendar - as a floating message or in date cell. You can control that possition using the setting under Options - General page.';
$PHP_EVENT_LANG['Help_8'] = 'Start date for the period when events will be exported.';
$PHP_EVENT_LANG['Help_9'] = 'End date for the period when events will be exported.';
$PHP_EVENT_LANG['Help_10'] = 'Select file format for the export. Read below for details about each one of the available export formats.';
$PHP_EVENT_LANG['Help_11'] = 'Set calendar width. To preview the calendar seeing the actual size you specify click on Preview menu link.';
$PHP_EVENT_LANG['Help_12'] = 'Set calendar height. To preview the calendar seeing the actual size you specify click on Preview menu link.';
$PHP_EVENT_LANG['Help_13'] = 'Select day will be used for week starting day on your calendar.';
$PHP_EVENT_LANG['Help_14'] = 'Set the month and year format on the top of your calendar.';
$PHP_EVENT_LANG['Help_15'] = 'Set date format for your events.';
$PHP_EVENT_LANG['Help_16'] = 'Set either 24 hour time format or 12 hour format using am/pm.';
$PHP_EVENT_LANG['Help_17'] = 'You can have event titles in calendar date cells or as a floating message when you mouse over the dates. Click on Preview calendar menu link to see the changes.';
$PHP_EVENT_LANG['Help_18'] = 'When you click on a date on your calendar you can have the events loaded in 3 different places - below the calendar, on the right side of the calendar or just replace the calendar with the events for the selected date. Click on Preview calendar menu link to see the changes.';
?>