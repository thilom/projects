<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Sports";
$module_id = "sports"; //needs to be the same as the module directory
$module_icon = 'off_sports_64.png';
$module_link = 'sports.php';
$window_width = '800';
$window_height = '';
$window_position = '';

//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Sports";
$security_id = "mod_sports";

//Settings
$settings[0]['name'] = "Sports";
$settings[0]['file'] = "$module_id/settings/sports_settings.php";
$settings[0]['order'] = "30";

//Content Types
$content_type[0]['name'] = 'Rugby: Weekly result list';
$content_type[0]['id'] = 'rugby_weekly';
$content_type[0]['content_file'] = 's/rugby/edit/rugby_weekly.php';
$content_type[0]['display_file'] = 's/rugby/view/rugby_weekly.php';
$content_type[0]['public_display_file'] = 's/rugby/view/rugby_weekly_public.php';
$content_type[0]['preferences_file'] = 's/rugby/edit/rugby_weekly_prefs.php';
$content_type[0]['css_file'] = 's/rugby/edit/rugby_weekly_css.php';
$content_type[0]['explanation'] = 'List match results per week.';

$content_type[1]['name'] = 'Hockey: Weekly result list';
$content_type[1]['id'] = 'hockey_weekly';
$content_type[1]['content_file'] = 's/hockey/edit/hockey_weekly.php';
$content_type[1]['display_file'] = 's/hockey/view/hockey_weekly.php';
$content_type[1]['public_display_file'] = 's/hockey/view/hockey_weekly_public.php';
$content_type[1]['preferences_file'] = 's/hockey/edit/hockey_weekly_prefs.php';
$content_type[1]['css_file'] = 's/hockey/edit/hockey_weekly_css.php';
$content_type[1]['explanation'] = 'List match results per week.';

$content_type[2]['name'] = 'Sports Results';
$content_type[2]['id'] = 'sports_results';
$content_type[2]['content_file'] = '/edit/sports_results_content.php';
$content_type[2]['display_file'] = '/view/sports_results.php';
$content_type[2]['public_display_file'] = '/view/sports_results.php';
$content_type[2]['preferences_file'] = '/edit/sports_results_prefs.php';
$content_type[2]['css_file'] = '/edit/sports_results_content.php';
$content_type[2]['explanation'] = 'Sports results.';

$content_type[3]['name'] = 'Basketball: Weekly result list';
$content_type[3]['id'] = 'basketball_weekly';
$content_type[3]['content_file'] = 's/basketball/edit/basketball_weekly.php';
$content_type[3]['display_file'] = 's/basketball/view/basketball_weekly.php';
$content_type[3]['public_display_file'] = 's/basketball/view/basketball_weekly_public.php';
$content_type[3]['preferences_file'] = 's/basketball/edit/basketball_weekly_prefs.php';
$content_type[3]['css_file'] = 's/basketball/edit/basketball_weekly_css.php';
$content_type[3]['explanation'] = 'List match results per week.';

$content_type[4]['name'] = 'Water Polo: Weekly result list';
$content_type[4]['id'] = 'water_polo_weekly';
$content_type[4]['content_file'] = 's/water_polo/edit/water_polo_weekly.php';
$content_type[4]['display_file'] = 's/water_polo/view/water_polo_weekly.php';
$content_type[4]['public_display_file'] = 's/water_polo/view/water_polo_weekly_public.php';
$content_type[4]['preferences_file'] = 's/water_polo/edit/water_polo_weekly_prefs.php';
$content_type[4]['css_file'] = 's/water_polo/edit/water_polo_weekly_css.php';
$content_type[4]['explanation'] = 'List match results per week.';

$content_type[5]['name'] = 'Latest Sports Results';
$content_type[5]['id'] = 'latest_sports_results';
$content_type[5]['content_file'] = '/edit/sports_results_content.php';
$content_type[5]['display_file'] = '/view/latest_sports_results.php';
$content_type[5]['public_display_file'] = '/view/latest_sports_results.php';
//$content_type[5]['preferences_file'] = '/edit/latest_sports_results_prefs.php';
$content_type[5]['css_file'] = '/edit/latest_sports_results_content.php';
$content_type[5]['explanation'] = 'Latest sports results displaying all the available sports.';