<?php

/**
 * Initial sports window for sports module.
 * 
 * @author Thilo Muller(2011)
 * @version $Id: sports.php 87 2011-09-14 06:45:49Z thilo $
 */

//Vars
$window_function = isset($_GET['f'])?$_GET['f']:'';

//Get page
switch ($window_function) {
	case 'sports_report':
		include '';
		break;
	default:
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/sports_icons.php';
}

//Includes
include 'toolbar.php';


