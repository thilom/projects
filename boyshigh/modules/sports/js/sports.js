/**
 * Flip an icon to it's hot variant
 */
function iFlip(cntr) {
	iSrc = document.getElementById(cntr).src;
	
	currentImage = iSrc.substr(iSrc.lastIndexOf('/')+1);
	
	newImage = currentImage.replace('off_', 'hot_');
	
	newSrc = iSrc.replace(currentImage, newImage);
	
	document.getElementById(cntr).src = newSrc;
}

/**
 * Flip an icon to it's off variant
 */
function iFlip_off(cntr) {
	iSrc = document.getElementById(cntr).src;
	
	currentImage = iSrc.substr(iSrc.lastIndexOf('/')+1);
	
	newImage = currentImage.replace('hot_', 'off_');
	
	newSrc = iSrc.replace(currentImage, newImage);
	
	document.getElementById(cntr).src = newSrc;
}

/**
 * Opens a sport window
 */
function open_sport_window(loc,window_name) {
    W = getUrlValue('W');
	url = '/modules/sports/s/' + loc;
	window.parent.W(window_name,url,500,900);
	window.parent.WD(W);
}