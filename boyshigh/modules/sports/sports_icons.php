<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
include_once SITE_ROOT . "/shared/terms.php";
include_once 'functions.php';

//Vars
$template = file_get_contents('html/sports_icons.html');
$sport_block = '';
$sports_settings = 'cricket,water_polo,hockey,basketball,rugby';

//Get enabled icons
$statement = "SELECT value 
				FROM {$GLOBALS['db_prefix']}_settings
				WHERE tag='enabled_sports'
				LIMIT 1";
$sql_list = $GLOBALS['dbCon']->prepare($statement);
$sql_list->execute();
$sql_list_data = $sql_list->fetch();
$sql_list->closeCursor();
$sports_list = explode(',', $sports_settings);

sort($sports_list);

//Draw Icons
$max_col = 5;
$col_count = 1;
$sport_block = "<table style='width: 100%'> ";
foreach ($sports_list as $sport_id) {
	$sport_icon = get_info($_SERVER['DOCUMENT_ROOT'] . "/modules/sports/s/$sport_id", 'sport_icon');
	$sport_name = get_info($_SERVER['DOCUMENT_ROOT'] . "/modules/sports/s/$sport_id", 'sport_name');
	$sport_link = get_info($_SERVER['DOCUMENT_ROOT'] . "/modules/sports/s/$sport_id", 'sport_link');
	
	$sport_block .= $col_count==1?"<tr><td>":"<td>";
	$sport_block .= "<center onMouseOver='iFlip(\"$sport_id\")' onMouseOut='iFlip_off(\"$sport_id\")' ";
	$sport_block .= "onClick='open_sport_window(\"$sport_id/$sport_link\", \"Sport: $sport_name\")'>";
	$sport_block .= "<img src='/modules/sports/i/$sport_icon' id='$sport_id' ><br>$sport_name</center>" ;
	$sport_block .= "</td>";
    $col_count++;
	if ($col_count > $max_col) {
		$sport_block .= "</tr>";
		$col_count = 1;
	}

}

while ($col_count < $max_col) {
	$sport_block .= "<td>&nbsp;</td>";
	$col_count++;
	$sport_block .= $col_count==$max_col?"</tr>":'';
}

$sport_block .= "</table>";

//Replace Tags
$template = str_replace('<!-- terms -->', $terms, $template);
$template = str_replace('<!-- sport_block -->', $sport_block, $template);

echo $template;