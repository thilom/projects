<?php
/** 
 * Spart settings
 *
 * @author Thlo Muller(2011)
 * @version $Id: sports_settings.php 80 2011-08-26 06:33:15Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/functions.php';

//Vars
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/sports/html/sports_settings.html');
$message = '';
$settings = '';
$sports_list = array();
$enabled_sports = array();
$sport_block = '';

if (count($_POST)>0) {

	$settings = get_settings();

	//Prepare statement - Setting Update
	$statement = "UPDATE {$GLOBALS['db_prefix']}_settings SET value=:value WHERE tag=:tag";
	$sql_update = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Setting Check
	$statement = "SELECT COUNT(*) AS c FROM {$GLOBALS['db_prefix']}_settings WHERE tag=:tag";
	$sql_check = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Setting Insert
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_settings (tag, value) VALUES (:tag,:value)";
	$sql_insert = $GLOBALS['dbCon']->prepare($statement);

	//Assemble sports
	$sports_list = isset($_POST['sports'])?implode(',', $_POST['sports']):'';	
	//Save settings
	$tag = "enabled_sports";
	if ($settings[$tag] != $sports_list) {
		$sql_check->bindParam(':tag', $tag);
		$sql_check->execute();
		$sql_data = $sql_check->fetch();
		$sql_check->closeCursor();
		if ($sql_data['c'] == 0) {
			$sql_insert->bindParam(':value', $sports_list);
			$sql_insert->bindParam(':tag', $tag);
			$sql_insert->execute();
		} else {
			$sql_update->bindParam(':tag', $tag);
			$sql_update->bindParam(':value', $sports_list);
			$sql_update->execute();
		}

		$message .= "&#187; Sports settings updated<br>";
	}

	if (empty($message)) {
		$message = "&#187; No Changes, Nothing to update";
	} else {
		write_log("Sports settings updated", 'sports');
	}
	echo "<br><br><div class='dMsg'>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/settings/settings.php?d=sports/settings/sports_settings.php\"' ></div>";

	die();
}

$settings = get_settings();

if (isset($settings['enabled_sports'])) {
	$enabled_sports = explode(',', $settings['enabled_sports']);
}

//Get sports list
$dir = dir($_SERVER['DOCUMENT_ROOT'] . "/modules/sports/s/");
while (false !== ($entry = $dir->read())) {
	if (in_array($entry, array('.','..','.svn'))) continue;
	$sports_list[$entry]['name'] = get_info($dir->path . $entry, 'sport_name');
	$sports_list[$entry]['checked'] = in_array($entry, $enabled_sports)?'checked':'';
}
$dir->close();

//assemble sports list
$max_col = 5;
$col_count = 1;
$sport_block = "<table style='width: 100%'> ";
foreach ($sports_list as $sport=>$sport_data) {
	$sport_block .= $col_count==1?"<tr><td>":"<td>";
	$sport_block .= "<input type='checkbox' name='sports[]' id='$sport' {$sport_data['checked']} value='$sport' ><label for='$sport'>{$sport_data['name']}</label>" ;
	$sport_block .= "</td>";
	if ($col_count == $max_col) {
		$sport_block .= "</tr>";
		$col_count = 1;
	}
}

while ($col_count < $max_col) {
	$sport_block .= "<td>&nbsp;</td>";
	$col_count++;
	$sport_block .= $col_count==$max_col?"</tr>":'';
}

$sport_block .= "</table>";

//Replace Tags
$template = str_replace('<!-- sport_block -->', $sport_block, $template);

echo $template;

/**
 * Returns the current content manager settings
 */
function get_settings() {
	//Get current settings
	$statement = "SELECT tag, value FROM {$GLOBALS['db_prefix']}_settings";
	$sql_settings = $GLOBALS['dbCon']->prepare($statement);
	$sql_settings->execute();
	$sql_data = $sql_settings->fetchAll();
	$sql_settings->closeCursor();
	$settings = '';

	foreach ($sql_data as $key=>$value) {
		switch($value['tag']) {
			case 'enabled_sports':
				$settings[$value['tag']] = $value['value'];
				break;
		}
	}

	return $settings;
}


?>
