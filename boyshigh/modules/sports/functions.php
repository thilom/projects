<?php

/**
 * Various functions for use in the sports module
 * 
 * @author Thilo Muller(2011)
 * @version $Id: functions.php 87 2011-09-14 06:45:49Z thilo $
 */


/**
 * Gets a value for a variable in the sporst ini file (sport.ini.php)
 * 
 * @param type $path Path to the sport dir
 * @param type $info_name The variable to get the value of
 * @return type 'Unknown' if the variable does not exist
 */
function get_info($path, $info_name) {
	include($path.'/sport.ini.php');
	$info_value = isset($$info_name)?$$info_name:'Unknown';
	
	return $info_value;
}
