<?php
/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function first_day_of_week($date) {
	$start_date = '';

	for ($x=0; $x<7; $x++) {
		$week_day = date('w', strtotime("$date - $x days"));
		if ($week_day == 1) {
			$y = $x +7;
			$start_date = strtotime("$date - $x days");
			$start_day = date('d', $start_date);
			$start_month = date('m', $start_date);
			$start_year = date('Y', $start_date);
			$start_date = date('d M Y', mktime(0,0,0, $start_month, $start_day, $start_year));
			break;
		}
	}

	return $start_date;
}

