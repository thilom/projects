<?php

/**
 * Preferences for sprots results
 *
 * @author Sigal Zahavi (2013)
 * @version $Id$
 */


//Includes
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$layout_list = '';
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/sports/html/preferences_sports_results.html');
$sport_type = '';
$sport_type_list = '<option value="">None</option>';
$sport_types = array('hockey'=>'Hockey', 'rugby'=>'Rugby');

//Get current Prefs
$pref_data = get_area_data($area_id, 'latest', array('preferences'));

//Expand Prefs
if (!empty($pref_data['data']['preferences'])) {
	$prefs = explode('|', $pref_data['data']['preferences']);

	foreach ($prefs as $pref) {
		if (empty($pref)) continue;
		list($var, $value) = explode('=', $pref);
        $sport_type = $value;
	}
}

//Save
if (count($_POST) > 0) {
	$prefs = "sport_type={$_POST['sport_type']}";

	if ($prefs != $pref_data['data']['preferences']) {
		$message = "&#187; Area Preferences Updated";

		//Save Data
		$fields['preferences'] = $prefs;
		update_area_data($area_id, 'draft', $fields);

		write_log("Preferences update for area (Draft)",'content_manager', $area_id);

	} else {
		$message = '&#187; No Changes, Nothing to update';
	}

	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><button type='button'  onClick='document.location=\"preferences_editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"' ><img src='/i/button/next_16.png'>Continue</button></div>";

	die();
}

//Assemble sport types
foreach ($sport_types as $key=>$type) {
	$sport_type_list .= "<option value='$key' ";
	$sport_type_list .= $key==$sport_type?"selected":"";
	$sport_type_list .= " >$type</option>";
}

//Replace Tags
$template = str_replace('<!-- sport_types_list -->', $sport_type_list, $template);

echo $template;
/*
 * End of file modules/sports/edit/sports_results_prefs.php
 */