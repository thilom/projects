<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * 2014/03/17 Sigal Zahavi - Created
 */

//Vars
$window_function = isset($_GET['f'])?$_GET['f']:'';

//Get page
switch ($window_function) {
	case 'match_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/basketball/match_list.php';
		break;
	case 'team_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/basketball/team_list.php';
		break;
	case 'sports_report':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/basketball/report.php';
		break;
    case 'import_results':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/basketball/import.php';
		break;
	default:
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/basketball/match_list.php';
}
//Includes
include 'toolbar.php';

