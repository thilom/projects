<?php

/**
 * Edit or Create a team
 *
 * @author Thilo Muller(2011)
 * @version $Id: team_edit.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/import.html');



//Replace Tags
//$template = str_replace('<!-- member_list -->', $member_list, $template);

echo $template;
?>
