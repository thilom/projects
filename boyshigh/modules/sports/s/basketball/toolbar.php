<?php

/**
 *  Toolbar for tour editing
 *
 * 2014/03/17 Sigal Zahavi - Created
 *  @version $Id: toolbar.php 87 2011-09-14 06:45:49Z thilo $
 */

//Menu Bar
$menu_bar = "<table id=menu_bar><tr>";

$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='basketball.php?W={$_GET['W']}&f=match_list'\">Matches</td>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='basketball.php?W={$_GET['W']}&f=team_list'\">Teams</td>";

$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td></tr>";
$menu_bar .= "</table>";
echo "<div id=top_menu >$menu_bar</div>";