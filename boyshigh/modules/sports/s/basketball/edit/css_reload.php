<?php

/**
 * Get default CSS for an area & template
 *
 * 2014/03/17 Sigal Zahavi - Created @version $Id$
 */

//Include
$ajax = 1;
include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php');

//Vars
//$list_file = $_GET['file'];

//Get Preferences
$pref_data = get_area_data($_GET['area_id'], 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);

$css_data = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/sports/s/basketball/css/default_basketball_weekly_css.css");

$css_data = str_replace('_suffix', "_{$_GET['area_id']}", $css_data);

echo $css_data;