<?php

/**
 *  Area content page for tutor area description. Content is auto-generated.
 *  Will generate a warning instead.
 *
 *  2014/03/17 Sigal Zahavi - Created
 *  @version $Id$
 */

echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/html/modules_auto_generated.html');
?>