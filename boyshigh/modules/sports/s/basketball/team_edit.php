<?php

/**
 * Edit or Create a team
 *
 * 2014/03/17 Sigal Zahavi - Created
 * @version $Id: team_edit.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/team_edit.html');
$team_name = '';
$home_team = '';
$member_list = '';
$team_id = isset($_GET['team_id'])?$_GET['team_id']:0;
$message = '';
$team_members = array();
$members_to_delete = array();
$members_to_add = array();
$new_members = array();
$new_members_id = array();

if (isset($_GET['team_id'])) {
	//Get team name
	$statement = "SELECT team_name, home_team
					FROM {$GLOBALS['db_prefix']}_sports_team
					WHERE team_id=:team_id
					LIMIT 1";
	$sql_team = $GLOBALS['dbCon']->prepare($statement);
	$sql_team->bindParam(':team_id', $_GET['team_id']);
	$sql_team->execute();
	$sql_team_data = $sql_team->fetch();
	$sql_team->closeCursor();
	$team_name = $sql_team_data['team_name'];
	$home_team = $sql_team_data['home_team'];


	//Get team members
	$statement = "SELECT sportsman_id, sportsman_name, people_id
					FROM {$GLOBALS['db_prefix']}_sports_team_members
					WHERE team_id=:team_id";
	$sql_members = $GLOBALS['dbCon']->prepare($statement);
	$sql_members->bindParam(':team_id', $team_id);
	$sql_members->execute();
	$sql_members_data = $sql_members->fetchAll();
	$sql_members->closeCursor();

	$counter = 0;
	foreach ($sql_members_data as $members_data) {
		$sportsman_name = addslashes($members_data['sportsman_name']);
		$member_list .= "$counter~~~{$members_data['sportsman_id']}~~~$sportsman_name|||";
		$team_members[$members_data['sportsman_id']] = $members_data['sportsman_name'];
		$counter++;
	}
}

//Save Team
if (count($_POST) > 0) {

	//Vars
	$home_team_post = isset($_POST['homeTeam'])?'Y':'';

	//Assessmble new members into an array
//	$new_members_line = explode('|||',$_POST['teamMembers']);
//	foreach ($new_members_line as $member_line) {
//		if (empty($member_line)) continue;
//		$new_members_items = explode('~~~', $member_line);
//		$new_members[] = stripslashes($new_members_items[2]);
//		$new_members_id[] = $new_members_items[1];
//	}

	if ($team_id == 0) { //New Team

		//Get new team ID
		$team_id = next_id("{$GLOBALS['db_prefix']}_sports_team", 'team_id');

		//Save Team Name
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_team
							(team_id, team_name, sport_id, home_team)
						VALUES
							(:team_id, :team_name, 'basketball', :home_team)";
		$sql_insert_name = $GLOBALS['dbCon']->prepare($statement);
		$sql_insert_name->bindParam(':team_id', $team_id);
		$sql_insert_name->bindParam(':team_name', $_POST['teamName']);
		$sql_insert_name->bindParam(':home_team', $home_team_post);
		$sql_insert_name->execute();

//		//Save Team members
//		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_team_members
//							(sportsman_id, sportsman_name, team_id)
//						VALUES
//							(:sportsman_id, :sportsman_name, :team_id)";
//		$sql_insert_sportsman = $GLOBALS['dbCon']->prepare($statement);
//
//		foreach ($new_members as $member) {
//			$sportsman_name = $member;
//			$sportsman_id = next_id("{$GLOBALS['db_prefix']}_sports_team_members", 'sportsman_id');
//
//			$sql_insert_sportsman->bindParam(':sportsman_id', $sportsman_id);
//			$sql_insert_sportsman->bindParam(':sportsman_name', $sportsman_name);
//			$sql_insert_sportsman->bindParam(':team_id', $team_id);
//			$sql_insert_sportsman->execute();
//		}

		$message .= "&#187; New team saved<br>";
			write_log("New team saved",'sports', $team_id);

	} else { //Existing Team
		//Update Name
		if ($team_name != $_POST['teamName']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_team
							SET team_name=:team_name
							WHERE team_id=:team_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':team_name', $_POST['teamName']);
			$sql_name_update->bindParam(':team_id', $team_id);
			$sql_name_update->execute();

			$message .= "&#187; Team name updated<br>";
			write_log("Team name updated",'sports', $team_id);
		}

		//Update home team
		if ($home_team != $home_team_post) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_team
							SET home_team=:home_team
							WHERE team_id=:team_id
							LIMIT 1";
			$sql_name_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_name_update->bindParam(':home_team', $home_team_post);
			$sql_name_update->bindParam(':team_id', $team_id);
			$sql_name_update->execute();

			if ($home_team_post == 'Y') {
				$message .= "&#187; Team set as a home team<br>";
				write_log("Team set as a home team",'sports', $team_id);
			} else {
				$message .= "&#187; Team removed as a home team<br>";
				write_log("Team remaved as a home team",'sports', $team_id);
			}

		}

		//Members
//		foreach ($new_members as $member_name) {
//			if (!in_array($member_name, $team_members)) $members_to_add[] = $member_name;
//		}
//		foreach ($team_members as $member_name) {
//			if (!in_array($member_name, $new_members)) $members_to_delete[] = $member_name;
//		}

		//Add members
//		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_team_members
//							(team_id, sportsman_id, sportsman_name)
//						VALUES
//							(:team_id, :sportsman_id, :sportsman_name)";
//		$sql_insert_member = $GLOBALS['dbCon']->prepare($statement);
//		foreach ($members_to_add as $member_name) {
//			$sportsman_id = next_id("{$GLOBALS['db_prefix']}_sports_team_members", 'sportsman_id');
//			$sql_insert_member->bindParam(':team_id', $team_id);
//			$sql_insert_member->bindParam(':sportsman_id', $sportsman_id);
//			$sql_insert_member->bindParam(':sportsman_name', $member_name);
//			$sql_insert_member->execute();
//
//			$message .= "&#187; $member_name added to team<br>";
//			write_log("$member_name added to team",'sports', $team_id);
//		}

		//Delete members
//		$statement = "DELETE FROM {$GLOBALS['db_prefix']}_sports_team_members
//						WHERE team_id=:team_id AND sportsman_name=:sportsman_name
//						LIMIT 1";
//		$sql_delete_member = $GLOBALS['dbCon']->prepare($statement);
//		foreach ($members_to_delete as $member_name) {
//			$sql_delete_member->bindParam(':team_id', $team_id);
//			$sql_delete_member->bindParam(':sportsman_name', $member_name);
//			$sql_delete_member->execute();
//
//			$message .= "&#187; $member_name deleted from team<br>";
//			write_log("$member_name deleted from team",'sports', $team_id);
//		}
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/sports/s/basketball/team_edit.php?team_id=$team_id&W={$_GET['W']}\"' ></div>";


	die();
}

//Assemble checkboxes
$home_team = $home_team=='Y'?'checked':'';

//Replace Tags
$template = str_replace('<!-- team_name -->', $team_name, $template);
$template = str_replace('<!-- home_team -->', $home_team, $template);
$template = str_replace('<!-- member_list -->', $member_list, $template);

echo $template;
?>
