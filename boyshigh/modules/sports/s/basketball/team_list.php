<?php

/**
 * Team list for sports module (Basketball)
 *
 * 2014/03/17 Sigal Zahavi - Created
 * @version $Id: team_list.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
include_once SITE_ROOT . "/shared/terms.php";

//Includes
include 'toolbar.php';

//Vars
$template = file_get_contents('html/team_list.html');

//Replace Tags
$template = str_replace('<!-- terms -->', $terms, $template);

echo $template;