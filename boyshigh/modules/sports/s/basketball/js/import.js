
function cancelUpload() {
    W = getUrlValue('W');
    document.location = "/modules/sports/s/basketball/basketball.php?W=" + W;
}

function uploadDocument() {
    $('#uploadForm').ajaxSubmit(function(data) {
        
        $('#errorMessage').slideUp();
        $('#warningMessage').slideUp();
        $('#okMessage').slideUp();
        
        var response = $.parseJSON(data);
        
        if (response.status == 'error') {
            $('#errorContent').html(response.message);
            $('#errorMessage').slideDown();
        } else {
            
            if (response.status == 'warning') {
                $('#warningContent').html(response.message);
                $('#warningMessage').slideDown();
            } else {
                $('#okContent').html(response.message);
                $('#okMessage').slideDown();
            } 
            
            $('#step1Div').slideUp();
            drawTable();
            $('#step2Div').slideDown();
            
        }
    }); 
}

function drawTable() {
    $('#importTable').dataTable({
        "ajax": '/modules/sports/s/basketball/ajax/get_import_results.ajax.php',
        bFilter: false,
        bSort: false
    });
}

function saveResults() {
    $.ajax({
        url: '/modules/sports/s/basketball/ajax/save_import_results.ajax.php',
        complete: function() {
            W = getUrlValue('W');
            document.location = "/modules/sports/s/basketball/basketball.php?W=" + W;
        }
    });
}