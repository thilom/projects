<?php
 /**
  * Return a list of importable results
  * 
  * 2015-03-21
  * ; Thilo Muller      - Created
  */

//Includes
$ajax = 1;
require_once '../../../../../settings/init.php';

//Vars
$result = array('aaData' => array());

//Get list of results
$statement = "SELECT match_date, match_name, team_a, team_b, team_a_runs, team_a_wickets, team_b_runs, team_b_wickets
                FROM {$GLOBALS['db_prefix']}_sports_result_cricket_tmp";
$sql_select = $GLOBALS['dbCon']->prepare($statement);
$sql_select->execute();
$sql_select_data = $sql_select->fetchAll();
$sql_select->closeCursor();

//Assemble
foreach ($sql_select_data as $data) {
    $line = array();
    $line[] = $data['match_date'];
    $line[] = $data['match_name'];
    $line[] = $data['team_a'];
    $line[] = $data['team_b'];
    $line[] = "{$data['team_a_runs']}-{$data['team_a_wickets']}";
    $line[] = "{$data['team_b_runs']}-{$data['team_b_wickets']}";

    $result['aaData'][] = $line;
}

echo json_encode($result);


