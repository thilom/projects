<?php
/**
 * AJAX script for hockey auto-complete field
 *
 * Returns a list of matches in JSON format
 * 		2013/08/18: Thilo Muller - Script created
 *
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';

//Vars
$search_term  = "%{$_GET['term']}%";
$name_list = array();

//Get list of matches
$statement = "SELECT DISTINCT match_name
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE match_name LIKE :search_term
					AND sport_id='cricket'";
$sql_match = $GLOBALS['dbCon']->prepare($statement);
$sql_match->bindParam(':search_term', $search_term);
$sql_match->execute();
$sql_match_data = $sql_match->fetchAll();
$sql_match->closeCursor();
foreach ($sql_match_data as $key=>$data) {
	$name_list[$key]['name'] = $data['match_name'];
}


header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

echo json_encode($name_list);
