<?php

/**
 * Display weekly cricket results
 *
 * @author Thilo Muller(2012)
 * @version $Id$
 */

include_once $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/date.php';
echo 'thththth';
//vars
$result_list = '';
$current_day = date('d');
$current_month = date('m');
$current_year = date('Y');
$match_name = '';
$team_A_result = '';
$team_B_result = '';

//Get Preferences
$pref_data = get_area_data($id, 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);


//Get CSS file
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_$id.css";
if (is_file($file)) {
	echo "<link rel=stylesheet href='/styles/css_$id.css'>";
} else {
	$css_data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/modules/sports/s/cricket/css/default_cricket_weekly_css.css");
	$css_data = str_replace('_suffix', "_$id", $css_data);

	echo "<style >
			$css_data;
			</style>";
}

//Date from text fields
if (isset($_POST['start_date'])) {
	$_POST['resultDate'] = strtotime($_POST['start_date']);
	$_POST['resultDate'] .= '|' . strtotime($_POST['end_date']);
}

//Calculate start date
if (isset($_POST['resultDate'])) {
	list($start_timestamp, $end_timestamp) = explode('|', $_POST['resultDate']);
	$start_date = date('Y-m-d', $start_timestamp);
	$start_title = date('d M Y', $start_timestamp);
} else if (isset($_GET['start'])) {
	$start_timestamp = $_GET['start'];
	$start_date = date('Y-m-d', $_GET['start']);
	$start_title = date('d M Y', $_GET['start']);
} else {
	for ($x=0; $x<7; $x++) {
		$week_day = date('w', strtotime("today - $x days"));
		if ($week_day == 1) {
			$y = $x +7;
			$start_date = strtotime("today - $y days");
			$start_day = date('d', $start_date);
			$start_month = date('m', $start_date);
			$start_year = date('Y', $start_date);
			$start_date = date('Y-m-d', mktime(0,0,0, $start_month, $start_day, $start_year));
			$start_title = date('d M Y', mktime(0,0,0, $start_month, $start_day, $start_year));
			$start_timestamp = mktime(0,0,0, $start_month, $start_day, $start_year);
			break;
		}
	}
}

//Calculate end date
if (isset($_POST['resultDate'])) {
	list($start_timestamp, $end_timestamp) = explode('|', $_POST['resultDate']);
	$end_date = date('Y-m-d', $end_timestamp);
	$end_title = date('d M Y', $end_timestamp);
} else if (isset($_GET['end'])) {
	$end_timestamp = $_GET['end'];
	$end_date = date('Y-m-d', $_GET['end']);
	$end_title = date('d M Y', $_GET['end']);
} else {
	for ($x=0; $x<7; $x++) {
		$week_day = date('w', strtotime("today - $x days"));
		if ($week_day == 0) {
			$end_date = strtotime("today - $x days");
			$end_day = date('d', $end_date);
			$end_month = date('m', $end_date);
			$end_year = date('Y', $end_date);
			$end_date = date('Y-m-d', mktime(0,0,0, $end_month, $end_day, $end_year));
			$end_title = date('d M Y', mktime(0,0,0, $end_month, $end_day, $end_year));
			$end_timestamp = mktime(0,0,0, $end_month, $end_day, $end_year);
			break;
		}
	}
}

//Add popup calender
echo "<link rel='stylesheet' href='/shared/popup_calendar/calendar.css' >";
echo "<script type='text/javascript' src='/shared/popup_calendar/CalendarPopup.js'></script>";
echo "<script type='text/javascript'>var calStart = new CalendarPopup('calDiv');calStart.setCssPrefix('SCRUM');calStart.showNavigationDropdowns();</script>";
//echo "<script type='text/javascript'>var calEnd = new CalendarPopup('calDiv2');calEnd.setCssPrefix('SCRUM');calEnd.offsetY = 7;</script>";

//Assemble title
$result_list = "<div class='cricket_title_$id'>Cricket results for $start_title - $end_title</div>";
$result_list .= "<table class='cricket_results_$id'>";

//Prepare statement  - Get match teams
$statement = "SELECT team_name, team_side
			FROM {$GLOBALS['db_prefix']}_sports_match_team
			WHERE match_id=:match_id
			LIMIT 2";
$sql_team = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Get match results
$statement = "SELECT result_type, result_value
			FROM {$GLOBALS['db_prefix']}_sports_match_result_cricket
			WHERE match_id=:match_id";
$sql_result = $GLOBALS['dbCon']->prepare($statement);

//Get data
$statement = "SELECT a.match_name, DATE_FORMAT(a.match_start, '%W, %e %M %Y') AS match_start, a.match_id
			FROM {$GLOBALS['db_prefix']}_sports_match AS a
			WHERE a.match_start >= :start_date AND a.match_start < :end_date  AND sport_id='cricket'
			ORDER BY a.match_name, a.match_start";
$sql_match = $GLOBALS['dbCon']->prepare($statement);
$sql_match->bindParam(':start_date', $start_date);
$sql_match->bindParam(':end_date', $end_date);
$sql_match->execute();
$sql_match_data = $sql_match->fetchAll();
$sql_match->closeCursor();

$line_class = "result_line_A_$id";
foreach ($sql_match_data as $match_data) {


	if ($match_name != $match_data['match_name']) {
		$result_list .= "<tr><td colspan=10 class='cricket_match_$id'>{$match_data['match_name']}</td></tr>";
		$result_list .= "<tr><th>Date</th><th>PBHS Team</th><th>Opponent</th><th>Result</th><th>PBHS Score</th><th>Opponent Score</th></tr>";
		$match_name = $match_data['match_name'];
		$line_class = "result_line_A_$id";
	}

	//Get teams
	$sql_team->bindParam(':match_id', $match_data['match_id']);
	$sql_team->execute();
	$sql_team_data = $sql_team->fetchAll();
	foreach ($sql_team_data as $team_data) {
		if ($team_data['team_side'] == 'A') $team_A_name = $team_data['team_name'];
		if ($team_data['team_side'] == 'B') $team_B_name = $team_data['team_name'];
	}

	//Get results
	$sql_result->bindParam(':match_id', $match_data['match_id']);
	$sql_result->execute();
	$sql_result_data = $sql_result->fetchAll();
	foreach ($sql_result_data as $result_data) {
		if ($result_data['result_type'] == 'fullA') $team_A_result = $result_data['result_value'];
		if ($result_data['result_type'] == 'fullB') $team_B_result = $result_data['result_value'];
	}

	if ($team_A_result > $team_B_result) $result = 'Won';
	if ($team_A_result < $team_B_result) $result = 'Lost';
	if ($team_A_result == $team_B_result) $result = 'Draw';

	$result_list .= "<tr  class='$line_class'><td>{$match_data['match_start']}</td><td>$team_A_name</td><td>$team_B_name</td><td>$result</td><td>$team_A_result</td><td>$team_B_result</td></tr>";
	$line_class = $line_class=="result_line_A_$id"?"result_line_B_$id":"result_line_A_$id";
}

//Get earliest date
$statement = "SELECT DATE_FORMAT(MIN(match_start), '%d %M %Y') AS min_date
			FROM {$GLOBALS['db_prefix']}_sports_match
			WHERE sport_id='cricket' ";
$sql_min = $GLOBALS['dbCon']->prepare($statement);
$sql_min->execute();
$sql_min_data = $sql_min->fetch();
$sql_min->closeCursor();
$nav_start_date = first_day_of_week($sql_min_data['min_date']);

//Create array of start and end dates
$next_week = false;
$nav_array = array();
$counter = 0;
$nav_array[$counter]['start'] = strtotime($nav_start_date);
$nav_array[$counter]['end'] = strtotime("$nav_start_date + 7 days");
if ($nav_array[$counter]['end']  < time()) {
	$next_week = true;
	$week_end = date('d M Y', strtotime("$nav_start_date + 7 days"));
}
while ($next_week) {
	$counter++;
	$next_week = false;
	$nav_array[$counter]['start'] = strtotime($week_end);
	$nav_array[$counter]['end'] = strtotime("$week_end + 7 days");
	if ($nav_array[$counter]['end']  < time()) {
		$next_week = true;
		$week_end = date('d M Y', strtotime("$week_end + 7 days"));
	}
}

//Assemble navication dropdown
$nav_options = '';
$next_week = '';
$previous_week = '';

foreach ($nav_array as $key=>$nav_dates) {
	$nav_options .= "<option value='{$nav_dates['start']}|{$nav_dates['end']}'>";
	$nav_options .= date('d M Y', $nav_dates['start']);
	$nav_options .= " - ";
	$nav_options .= date('d M Y', $nav_dates['end']);
	$nav_options .= "</option>";

	//Check previous week
	$halfway = $start_timestamp + (($end_timestamp - $start_timestamp)/2);
	if ($halfway > $nav_dates['start'] && $halfway < $nav_dates['end'] ) {
		if (isset($nav_array[$key-1])) {
			$previous_week = "<a href='/index.php?page={$_GET['page']}&start={$nav_array[$key-1]['start']}&end={$nav_array[$key-1]['end']}'>Previous Week</a>";
		} else {
			$previous_week = '';
		}
		if (isset($nav_array[$key+1])) {
			$next_week = "<a href='/index.php?page={$_GET['page']}&start={$nav_array[$key+1]['start']}&end={$nav_array[$key+1]['end']}'>Next Week</a>";
		} else {
			$next_week = '';
		}
	}
}


//Navigation
//$result_list .= "<tr><td colspan=10>";
//$result_list .= "<table class='cricket_nav_$id'><tr>";
//$result_list .= "<td class='cricket_nav_prev_$id'>$previous_week</td>";
//$result_list .= "<td class='cricket_nav_center_$id'><form action='' method='post' id='resultForm'>View results for: <select name='resultDate' onChange='document.getElementById(\"resultForm\").submit()' ><option> </option>$nav_options</select></form></td>";
//$result_list .= "<td class='cricket_nav_next_$id'>$next_week</td>";
//$result_list .= "</tr></table>";
//$result_list .= "</td></tr>";
//$result_list .= "</table>";

//Navigation
$result_list .= "<tr><td colspan=10>";
$result_list .= "<table class='cricket_nav_$id'><tr>";
$result_list .= "<td class='cricket_nav_center_$id'>";
$result_list .= "<form action='' method='post' id='resultForm'>";
$result_list .= "Select Date Range: <input type='text' name='start_date' id='start_date' value='$start_title' ><A HREF='#' onClick=\"calStart.select(document.getElementById('start_date'),'anchor1x','d MMM y'); return false;\" NAME='anchor1x' ID='anchor1x'><img src='/i/button/calendar_24.png' style='vertical-align: middle'></A> ";
$result_list .= "to ";
$result_list .= "<input type='text' name='end_date' id='end_date' value='$end_title' ><A HREF='#' onClick=\"calStart.select(document.getElementById('end_date'),'anchor2x','d MMM y'); return false;\" NAME='anchor2x' ID='anchor2x'><img src='/i/button/calendar_24.png' style='vertical-align: middle'></A> ";
$result_list .= "<button>Go</button>";
$result_list .= "</form></td>";
$result_list .= "</tr></table>";
$result_list .= "</td></tr>";
$result_list .= "</table>";
$result_list .= "<DIV ID='calDiv' name='calDiv' STYLE='position:absolute;visibility:hidden;background-color:#FAF3F3;layer-background-color:white;'></DIV>";
//$result_list .= "<DIV ID='calDiv2' STYLE='position:absolute;visibility:hidden;background-color:white;layer-background-color:white;'></DIV>";

echo $result_list;




?>
