<?php

/**
 *  Display sport results for Cricket
 *
 * 2014/03/18 Sigal Zahavi - Created
  *  @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
include_once SITE_ROOT . "/shared/terms.php";

//Get Preferences
/*$pref_data = get_area_data($id, 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);
if(isset($preferences['sport_type']))
{
   $sport_type = $preferences['sport_type'];
}
else
{
    echo 'The sport type has not been completed for this page and is set to Cricket by default.';
    $sport_type = 'cricket';
}*/

//Vars
$sports_results_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/sports/html/sports_results.html');
//$results_table = $sport_type; //Which results table to query

$sport_results = '';
$team_a = '';
$team_b = '';
$team_a_half = '';
$team_b_half = '';
$team_a_final = '';
$team_b_final = '';
$current_match_id = 0;

//Get sports for the dropdown
/*$statement = "SELECT value
				FROM {$GLOBALS['db_prefix']}_settings
				WHERE tag='enabled_sports'
				LIMIT 1";
$sql_list = $GLOBALS['dbCon']->prepare($statement);
$sql_list->execute();
$sql_list_data = $sql_list->fetch();
$sql_list->closeCursor();
$sports_list = explode(',', $sql_list_data['value']);

sort($sports_list);
$sport_type = $sports_list[0]; //Default first displayed results to first sport on the dropdown

$sport_buttons = '<ul id="sportBtns">';
foreach($sports_list as $sport)
{
    $sport_name = ucwords($sport);

    //Removing underscore and making second word first letter uppercase
    foreach (array('_') as $delimiter) {
        if (strpos($sport_name, $delimiter)!==false) {
            $sport_name =implode($delimiter, array_map('ucfirst', explode($delimiter, $sport_name)));
        }
    }

    $sport_name = str_replace('_', ' ', $sport_name);
    $sport_buttons .= "<li><a href='/index.php?page={$sport_name}_results'>$sport_name</a></li>";
}
$sport_buttons = '</ul>';
/*$sport_list = '<select id="selectSport" name="sport_type">';
foreach($sports_list as $sport)
{
    $sport_name = ucwords($sport);

    //Removing underscore and making second word first letter uppercase
    foreach (array('_') as $delimiter) {
        if (strpos($sport_name, $delimiter)!==false) {
            $sport_name =implode($delimiter, array_map('ucfirst', explode($delimiter, $sport_name)));
        }
    }

    $sport_name = str_replace('_', ' ', $sport_name);
    if($sport == $sport_type)
        $sport_list .= "<option value='$sport' selected>$sport_name</option>";
    else
        $sport_list .= "<option value='$sport'>$sport_name</option>";
}
$sport_list .= '</select>';*/

//Get results
$statement = "SELECT a.match_id, a. result_type, a.result_value, b.match_start, b.match_name,
                IF(a.result_type='fullA', (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='A' AND sports_id=:sport_type LIMIT 1), (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='B' AND sports_id=:sport_type  LIMIT 1) ) AS team
                FROM modularus_sports_match_result_cricket AS a
                LEFT JOIN modularus_sports_match AS b ON a.match_id = b.match_id
                WHERE b.sport_id=:sport_type
                	AND (a.result_type = 'fullA' OR a.result_type='fullB')
                ORDER BY b.match_start DESC
                LIMIT 80";
$sql_result = $GLOBALS['dbCon']->prepare($statement);
$sql_result->bindParam(':sport_type', $sport_type);
$sql_result->execute();
$sql_result_data = $sql_result->fetchAll();
$sql_result->closeCursor();

foreach($sql_result_data as $result_data) {
    $match_id = $result_data['match_id'];
    $match_name = $result_data['match_name'];
    $match_date = $result_data['match_start'];
    $date = new DateTime($match_date);
    $match_date = $date->format('d/m/Y');

    if($current_match_id != $match_id && !isset($results[$match_id]))
    {
        $results[$match_id] = array(
            'match'=>$match_name,
            'date'=>$match_date,
            'id'=>$match_id
        );
        $current_match_id = $match_id;
    }

    switch($result_data['result_type'])
    {
        case 'halfA':
            $results[$match_id]['team_a'] = $result_data['team'];
            $results[$match_id]['half_a'] = $result_data['result_value'];
            break;
        case 'fullA':
			$results[$match_id]['team_a'] = $result_data['team'];
            $results[$match_id]['final_a'] = $result_data['result_value'];
            break;
        case 'halfB':
            $results[$match_id]['team_b'] = $result_data['team'];
            $results[$match_id]['half_b'] = $result_data['result_value'];
            break;
        case 'fullB':
			$results[$match_id]['team_b'] = $result_data['team'];
            $results[$match_id]['final_b'] = $result_data['result_value'];
            break;
    }
}

if (!empty($results)) {
	foreach($results as $result) {
		$match_name = $result['match'];
		$match_date = $result['date'];
		if (!isset($result['team_a'])) $result['team_a'] = '';
		if (!isset($result['team_b'])) $result['team_b'] = '';
		if (!isset($result['half_a'])) $result['half_a'] = '0';
		if (!isset($result['half_b'])) $result['half_b'] = '0';
		if (!isset($result['final_a'])) $result['final_a'] = '0';
		if (!isset($result['final_b'])) $result['final_b'] = '0';

		$sport_results .= "<tr>\n
								   <td rowspan=\"3\" style=\"border-bottom:3px solid #006400\">\n
										<b>{$match_name}</b><br>
										{$match_date}
									</td>\n
								</tr>\n
								<tr>\n
									<td style=\"border-top:none\">{$result['team_a']}</td>
									<td style=\"border-top:none\">{$result['final_a']}</td>
								</tr>\n
								<tr>\n
									<td style=\"border-bottom:3px solid #006400\">{$result['team_b']}</td>
									<td style=\"border-bottom:3px solid #006400\">{$result['final_b']}</td>
								</tr>\n
								";
	}
}
else
{
    $sport_results .= "<tr>\n
                        <td colspan='3'>No results where found</td>\n
                        </tr>";
}

$sports_results_template = str_replace('<!-- sport_results -->', $sport_results, $sports_results_template);

echo $sports_results_template;
/***
 * End of file cricket_sports_results.php
 */