<?php

/**
 * Edit or create a new match
 *
 * @author Thilo Muller(2012)
 * @version $Id: match_edit.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';


//Vars
$template = file_get_contents('html/match_edit.html');
$data_fields = array('fullA','fullB','halfA','halfB');
foreach ($data_fields as $data) $$data = '';
$fullA_runs = '';
$fullA_wickets = '';
$fullB_runs = '';
$fullB_wickets = '';
$halfA_runs = '';
$halfA_wickets = '';
$halfB_runs = '';
$halfB_wickets = '';
$sport_id = 'cricket';
$category_select = '';
$team_options = '';
$match_name = '';
$teamA = '';
$teamB = '';
$match_date = '';
$result_text = '';

//Get group list
$statement = "SELECT DISTINCT match_group
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE sport_id=:sport_id
				ORDER BY match_group";
$sql_group = $GLOBALS['dbCon']->prepare($statement);
$sql_group->bindParam(':sport_id', $sport_id);
$sql_group->execute();
$sql_group_data = $sql_group->fetchAll();
$sql_group->closeCursor();
foreach ($sql_group_data as $group_data) {
	$category_select .= "<tr>";
	$category_select .= "<td onclick=\"document.getElementById('text2').value=('{$group_data['match_group']}');document.getElementById('option_placeholder').style.display='none'\" onmouseout=\"this.style.backgroundColor='silver';this.style.color='#000000'\" onmouseover=\"this.style.backgroundColor='#316AC5';this.style.color='white'\" class='comboOption'>";
	$category_select .= $group_data['match_group'];
	$category_select .= "</td></tr>";
}

//Get team list
$statement = "SELECT team_id, team_name
				FROM {$GLOBALS['db_prefix']}_sports_team
				WHERE sport_id='cricket'
				ORDER BY team_name";
$sql_teams = $GLOBALS['dbCon']->prepare($statement);
$sql_teams->execute();
$sql_teams_data = $sql_teams->fetchAll();
$sql_teams->closeCursor();
foreach ($sql_teams_data as $team_data) {
	$team_options .= "<div onclick='insertTeam(\"{$team_data['team_id']}\",\"{$team_data['team_name']}\")'>{$team_data['team_name']}</div>";
}

if (isset($_GET['match_id'])) {
	$match_id = $_GET['match_id'];

	//Get data
	$statement = "SELECT match_name, DATE_FORMAT(match_start, '%e %M %Y') AS match_start, match_group
					FROM {$GLOBALS['db_prefix']}_sports_match
					WHERE match_id=:match_id
					LIMIT 1";
	$sql_match = $GLOBALS['dbCon']->prepare($statement);
	$sql_match->bindParam(':match_id', $match_id);
	$sql_match->execute();
	$sql_match_data = $sql_match->fetch();
	$sql_match->closeCursor();
	$match_name = $sql_match_data['match_name'];
	$match_date = $sql_match_data['match_start'];
	$match_group = $sql_match_data['match_group'];

	//Get Teams
	$statement = "SELECT team_name, team_side
					FROM {$GLOBALS['db_prefix']}_sports_match_team
					WHERE match_id=:match_id";
	$sql_team = $GLOBALS['dbCon']->prepare($statement);
	$sql_team->bindParam(':match_id', $match_id);
	$sql_team->execute();
	$sql_team_data = $sql_team->fetchAll();
	$sql_team->closeCursor();
	foreach ($sql_team_data as $team_data) {
		switch ($team_data['team_side']) {
			case 'A':
				$teamA = $team_data['team_name'];
				break;
			case 'B':
				$teamB = $team_data['team_name'];
				break;
		}
	}

	//Get results
	$statement = "SELECT result_type, result_runs_value, result_wickets_value
					FROM {$GLOBALS['db_prefix']}_sports_match_result_cricket
					WHERE match_id=:match_id";
	$sql_results = $GLOBALS['dbCon']->prepare($statement);
	$sql_results->bindParam(':match_id', $match_id);
	$sql_results->execute();
	$sql_results_data = $sql_results->fetchAll();
	$sql_results->closeCursor();
	foreach ($sql_results_data as $result_data) {
        switch($result_data['result_type'])
        {
            case 'fullA':
                $fullA_runs = $result_data['result_runs_value'];
                $fullA_wickets = $result_data['result_wickets_value'];
                break;
            case 'halfA':
                $halfA_runs = $result_data['result_runs_value'];
                $halfA_wickets = $result_data['result_wickets_value'];
                break;
            case 'fullB':
                $fullB_runs = $result_data['result_runs_value'];
                $fullB_wickets = $result_data['result_wickets_value'];
                break;
            case 'halfB':
                $halfB_runs = $result_data['result_runs_value'];
                $halfB_wickets = $result_data['result_wickets_value'];
                break;
        }
	}

    //Get match free text
    $statement = "SELECT result_text
                    FROM {$GLOBALS['db_prefix']}_sports_result_text_cricket
                    WHERE match_id = :match_id";
    $sql = $GLOBALS['dbCon']->prepare($statement);
    $sql->bindParam(':match_id', $match_id);
    $sql->execute();
    $result_text = $sql->fetchColumn();
    $sql->closeCursor();
}

//Save Match
if (count($_POST) > 0) {
	if (isset($_GET['match_id'])) {
		$match_id = $_GET['match_id'];
		$message = '';

		//Match Name
		if ($match_name != $_POST['match_name']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match
							SET match_name=:match_name
							WHERE match_id=:match_id
							LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':match_name', $_POST['match_name']);
			$sql_update->bindParam(':match_id', $match_id);
			$sql_update->execute();
			$message .= "&#187; Match name updated<br>";
			write_log("Match name updated",'sports', $match_id);
		}

		//Match Date
		$post_date = date('Y-m-d', strtotime($_POST['match_date']));
		if ($match_date != $_POST['match_date']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match
							SET match_start=:match_start
							WHERE match_id=:match_id
							LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':match_start', $post_date);
			$sql_update->bindParam(':match_id', $match_id);
			$sql_update->execute();
			$message .= "&#187; Match date updated<br>";
			write_log("Match date updated",'sports', $match_id);
		}

		//Match Team A
		if ($teamA != $_POST['teamA']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_team
							SET team_name=:team_name
							WHERE match_id=:match_id AND team_side='A'
							LIMIT 1";
			$sql_team = $GLOBALS['dbCon']->prepare($statement);
			$sql_team->bindParam(':team_name', $_POST['teamA']);
			$sql_team->bindParam(':match_id', $match_id);
			$sql_team->execute();

			$message .= "&#187; Match team (Side A) updated<br>";
			write_log("Match team (Side A) updated",'sports', $match_id);
		}

		//Match Team B
		if ($teamB != $_POST['teamB']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_team
							SET team_name=:team_name
							WHERE match_id=:match_id AND team_side='B'
							LIMIT 1";
			$sql_team = $GLOBALS['dbCon']->prepare($statement);
			$sql_team->bindParam(':team_name', $_POST['teamB']);
			$sql_team->bindParam(':match_id', $match_id);
			$sql_team->execute();
			$message .= "&#187; Match team (Side B) updated<br>";
			write_log("Match team (Side B) updated",'sports', $match_id);
		}

		//Match Results
		$match_results = "$fullA_runs{$fullA_wickets}{$fullB_runs}{$fullB_wickets}";
		$post_results = "{$_POST['fullA_runs']}{$_POST['fullA_wickets']}{$_POST['fullB_runs']}{$_POST['fullB_wickets']}";
		if ($match_results != $post_results) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_result_cricket
							SET result_runs_value=:result_runs_value, result_wickets_value=:result_wickets_value
							WHERE match_id=:match_id AND result_type=:result_type
							LIMIT 1";
			$sql_results = $GLOBALS['dbCon']->prepare($statement);
			foreach ($data_fields as $field_name) {
				$sql_results->bindParam(':match_id', $match_id);
				$sql_results->bindParam(':result_type', $field_name);
				$sql_results->bindParam(':result_runs_value', $_POST[$field_name . '_runs']);
				$sql_results->bindParam(':result_wickets_value', $_POST[$field_name . '_wickets']);
				$sql_results->execute();
			}

			$message .= "&#187; Match results updated<br>";
			write_log("Match results updated",'sports', $match_id);
		}

        //Result text
        if($_POST['result_text'] != '')
        {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_result_text_cricket
                            (match_id, result_text)
                            VALUES (:match_id, :result_text)
                            ON DUPLICATE KEY UPDATE result_text = :result_text";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':match_id', $match_id);
            $sql->bindParam(':result_text', $_POST['result_text']);
            $sql->execute();
            $sql->closeCursor();
        }
        //If result text is empty, check if it exists in the table and delete it
        else
        {
            $statement = "SELECT result_text
                            FROM {$GLOBALS['db_prefix']}_sports_result_text_cricket
                            WHERE match_id = :match_id";
            $sql_results = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':match_id', $match_id);
            $sql->execute();
            $result_text = $sql->fetchColumn();

            if(!empty($result_text))
            {
                $statement = "DELETE FROM {$GLOBALS['db_prefix']}_sports_result_text_cricket
                                WHERE match_id = :match_id";
                $sql_results = $GLOBALS['dbCon']->prepare($statement);
                $sql->bindParam(':match_id', $match_id);
                $sql->execute();
            }
        }
	} else {
		$match_id = next_id("{$GLOBALS['db_prefix']}_sports_match", 'match_id');

		//Save Match Data
		$match_name = $_POST['match_name'];
		$match_start = date('Y-m-d', strtotime($_POST['match_date']));

		//Create match
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match
							(match_id, sport_id, match_start, match_group, match_name)
						VALUES
							(:match_id, :sport_id, :match_start, '', :match_name)";
		$sql_create = $GLOBALS['dbCon']->prepare($statement);
		$sql_create->bindParam(':match_id', $match_id);
		$sql_create->bindParam(':sport_id', $sport_id);
		$sql_create->bindParam(':match_start', $match_start);
		$sql_create->bindParam(':match_name', $match_name);
		$sql_create->execute();
		$sql_create->closeCursor();

		//Save result
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_result_cricket
						(match_id, result_type, result_runs_value, result_wickets_value)
                        VALUES
						(:match_id, :result_type, :result_runs_value, :result_wickets_value)";
		$sql_results = $GLOBALS['dbCon']->prepare($statement);
		foreach ($data_fields as $field_name) {
			$sql_results->bindParam(':match_id', $match_id);
			$sql_results->bindParam(':result_type', $field_name);
			$sql_results->bindParam(':result_runs_value', $_POST[$field_name . '_runs']);
			$sql_results->bindParam(':result_wickets_value', $_POST[$field_name . '_wickets']);
			$sql_results->execute();
		}

		//Save Team A
		$team_side = 'A';
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_team
							(match_id, sports_id, team_name, team_side)
						VALUES
							(:match_id, :sports_id, :team_name, :team_side)";
		$sql_team = $GLOBALS['dbCon']->prepare($statement);
		$sql_team->bindParam(':match_id', $match_id);
		$sql_team->bindParam(':sports_id', $sport_id);
		$sql_team->bindParam(':team_name', $_POST['teamA']);
		$sql_team->bindParam(':team_side', $team_side);
		$sql_team->execute();

		//Save Team B
		$team_side = 'B';
		$sql_team->bindParam(':match_id', $match_id);
		$sql_team->bindParam(':sports_id', $sport_id);
		$sql_team->bindParam(':team_name', $_POST['teamB']);
		$sql_team->bindParam(':team_side', $team_side);
		$sql_team->execute();

		$message = "&#187; Cricket match saved<br>";
		write_log("cricket match added",'sports', $match_id);

        //Result text
        if($_POST['result_text'] != '')
        {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_result_text_cricket
                            (match_id, result_text)
                            VALUES (:match_id, :result_text)";
            $sql = $GLOBALS['dbCon']->prepare($statement);
            $sql->bindParam(':match_id', $match_id);
            $sql->bindParam(':result_text', $_POST['result_text']);
            $sql->execute();
            $sql->closeCursor();
        }
	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script type='text/javascript' src='/modules/quote/js/quote.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'>";
	echo "<input type=button value='Add Another Match' class=ok_button onClick='document.location=\"/modules/sports/s/cricket/match_edit.php\"' >";
	echo "<input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/sports/s/cricket/match_edit.php?match_id=$match_id\"' >";
	echo "</div>";

	die();
}

//Replace Tags
$template = str_replace('<!-- fullA_runs -->', $fullA_runs, $template);
$template = str_replace('<!-- fullA_wickets -->', $fullA_wickets, $template);
$template = str_replace('<!-- fullB_runs -->', $fullB_runs, $template);
$template = str_replace('<!-- fullB_wickets -->', $fullB_wickets, $template);
$template = str_replace('<!-- halfA -->', $halfA, $template);
$template = str_replace('<!-- halfB -->', $halfB, $template);
$template = str_replace('<!-- teamA -->', $teamA, $template);
$template = str_replace('<!-- teamB -->', $teamB, $template);
$template = str_replace('<!-- match_name -->', $match_name, $template);
$template = str_replace('<!-- match_date -->', $match_date, $template);
$template = str_replace('<!-- category_select -->', $category_select, $template);
$template = str_replace('<!-- TeamOptionList -->', $team_options, $template);
$template = str_replace('<!-- result_text -->', $result_text, $template);

echo $template;