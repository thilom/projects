<?php

/**
 * Returns a list of teams for tennis
 *
 * @author Thilo Muller (2011)
 * @version $Id$
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$match_list = '';
$startAt = isset($_GET['start'])?$_GET['start']:0;
$length = isset($_GET['length'])?$_GET['length']:10;
$filter_string = isset($_GET['filter'])?"%{$_GET['filter']}%":'';

//Get total match count
$statement = "SELECT COUNT(*) AS team_count
				FROM {$GLOBALS['db_prefix']}_sports_team
				WHERE sport_id='cricket'";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$match_list .= "|||data~~~{$sql_count_data['team_count']}";

//Get result match count
$statement = "SELECT COUNT(*) AS team_count
				FROM {$GLOBALS['db_prefix']}_sports_team
				WHERE sport_id='cricket'";
if (!empty($filter_string)) {
	$statement .= " AND team_name LIKE :filter_string";
}
$sql_count = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) {
	$sql_count->bindParam(':filter_string', $filter_string);
}
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$match_list .= "~~~{$sql_count_data['team_count']}";

//Get matches
$statement = "SELECT team_id,  team_name
				FROM {$GLOBALS['db_prefix']}_sports_team
				WHERE sport_id='cricket'";
if (!empty($filter_string)) {
	$statement .= " AND team_name LIKE :filter_string";
}
$statement .= "	LIMIT $startAt, $length";
$sql_matches = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) {
	$sql_matches->bindParam(':filter_string', $filter_string);
}
$sql_matches->execute();
$sql_matches_data = $sql_matches->fetchAll();
$sql_matches->closeCursor();



//Assemble Matches
foreach ($sql_matches_data as $match_data) {
	$match_list .= "|||{$match_data['team_id']}~~~{$match_data['team_name']}";
}

echo $match_list;

?>
