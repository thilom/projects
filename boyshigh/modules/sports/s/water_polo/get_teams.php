<?php

/**
 * Get teams for dropdown lists for teamA and teamB
 *
 * 2014/03/17 Sigal Zahavi - Created
 * @version $Id: get_teams.php 87 2013-08-06 Sigal $

*/

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';


$sport_id = 'water_polo';
$term = trim(strip_tags($_GET['term']));//retrieve the search term that autocomplete sends
$team_side = trim(strip_tags($_GET['team_side']));

//Get Teams Dropdowns
$statement = "SELECT DISTINCT team_name, team_side
					FROM {$GLOBALS['db_prefix']}_sports_match_team
					WHERE team_name LIKE '%".$term."%'
					AND team_side = '" . $team_side . "'
					";
$sql_team = $GLOBALS['dbCon']->prepare($statement);
$sql_team->execute();
$sql_team_data = $sql_team->fetchAll();
$sql_team->closeCursor();
foreach ($sql_team_data as $team_data) {
    $team_options['teams'][] = array(
            'name' => $team_data['team_name'],
			);
}

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

echo json_encode($team_options);
/**
 * End of file: modules/sports/s/hockey/get_teams.php
*/