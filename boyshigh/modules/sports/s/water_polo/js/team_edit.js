var c=0;

/**
 * Initialize page
 */
function init() {
    document.onclick = handleEvent;
//	drawMembers();
}

/**
 * Event Handler
 */
function handleEvent(e) {
	var eSrc;
	if (window.event) {
		e = window.event;
		eSrc = e.srcElement;
	} else {
		eSrc = e.target;
	}
	if (e.type == 'click') {
		switch(eSrc.id) {
			case 'addMember':
				if (e.pageX || e.pageY) {
					posx = e.pageX;
					posy = e.pageY;
				} else if (e.clientX || e.clientY) 	{
					posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
					posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
				}
				openMember(posx, posy);
				break;
			case 'addMemberClose':
				closeMember();
				break;
			case 'addMemberButton':
				addMember();
				break;
			case 'cancelTeamEdit':
				cancelTeamEdit('/water_polo/team_list.php', 'Sports: Water Polo');
				break;
		}

		switch(eSrc.name) {
			case 'removeMember':
				id = eSrc.id.substr(1);
				removeMember(id);
				break;
		}
	}
}


/**
 * Get team members via AJAX
 */
function getMembers() {

	 if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				al = xmlhttp.responseText;
//				alert(al);
			}
		}

		matchUrl = "/modules/sports/s/tennis/ajax_team_members.php?team_id=0";

		xmlhttp.open("GET",matchUrl,true);
		xmlhttp.send();
}


/**
 * Opens the add Member form
 */
function openMember(posX, posY) {
    addMemberDiv = document.getElementById('addMemberDiv');
	addMemberDiv.style.top = posY;
	addMemberDiv.style.left = posX;
    addMemberDiv.className = 'addShow';
}

/**
 * closes the add member block
 */
function closeMember() {
    document.getElementById('addMemberDiv').className = 'addHidden';
}

/**
 * Add a new member to the list
 */
function addMember() {
	newMembers = '';
    memberName = document.getElementById('newMember').value;
	currentMembers = document.getElementById('teamMembers').value;

	//Check for empty member name
	if (memberName == '') {
		closeMember();
		return;
	}

	memberLine = currentMembers.split('|||');
	for (i = 0; i < memberLine.length; i++) {
		memberItems = memberLine[i].split('~~~');

		if (memberName == memberItems[1]) {
			alert('Member already exists for this team.');
			return;
		}
	}

	newMembers = currentMembers + i + "~~~0~~~" + memberName + '|||';

	document.getElementById('teamMembers').value = newMembers;

	closeMember();
	drawMembers();
}

/**
 * Removes a team member from the list
 */
function removeMember(id) {
	newMembers = '';
	currentMembers = document.getElementById('teamMembers').value;

	memberLine = currentMembers.split('|||');

	for (i = 0; i < memberLine.length; i++) {
		if (memberLine[i] == '') continue;
		memberItems = memberLine[i].split('~~~');
		if (id != memberItems[0]) {
			newMembers += i + "~~~0~~~" + memberItems[2] + '|||';
		}
	}

	document.getElementById('teamMembers').value = newMembers;

	closeMember();
	drawMembers();
}

/**
 * Draw members to the table
 */
function drawMembers() {
    var memberList = document.getElementById('teamMembers').value;
	var memberTable = document.getElementById('memberTable');


	//Clear table
	for (i = 0; i < c; i++) {
		if (document.getElementById('row'+i)) {
			row = document.getElementById('row'+i);
			memberTable.removeChild(row);
		}
	}

	c=0;
	var memberLine = memberList.split('|||');
	for (i in memberLine) {
		if (memberLine[i] == '') continue;
		memberData = memberLine[i].split('~~~');

		var TR = document.createElement('TR');
		var TD1 = document.createElement('TD');
		var TD2 = document.createElement('TD');

		//Add row ID
		TR.setAttribute('id', 'row'+i);

		//Add Name
		var TD1Text = document.createTextNode(memberData[2].replace("\\'", "'"));
		TD1.appendChild(TD1Text);
		TR.appendChild(TD1);

		//Create Buttons
		var deleteButton = document.createElement('INPUT');
		deleteButton.setAttribute('type', 'button');
		deleteButton.setAttribute('value', 'Remove');
		deleteButton.setAttribute('class', 'delete_button');
		deleteButton.setAttribute('name', 'removeMember');
		deleteButton.setAttribute('id', 'r'+memberData[0]);
		TD2.setAttribute('class', 'deleteCol');
		TD2.appendChild(deleteButton);
		TR.appendChild(TD2);

		//Attach to table
		memberTable.appendChild(TR);
		c++;
	}
}

/**
 * Close a team Editor Window
 */
function cancelTeamEdit(loc,window_name) {
	W = getUrlValue('W');
	url = '/modules/sports/s/' + loc;
	window.parent.W(window_name,url,500,900);
	window.parent.WD(W);
}


