<?php
/**
 * CSS editor for products text list
 *
 * 2014/03/17 Sigal Zahavi - Created
 * @version $Id$
 */

//Vars
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_{$_GET['Eid']}.css";
$template = '';
$css_data = '';
$has_template = false;

if (count($_POST) > 0) {
	$css_data = $_POST['css_data'];
	file_put_contents($file, $css_data);
}

//Get Preferences
$pref_data = get_area_data($area_id, 'latest', array('preferences'));
$preferences = expand_preferences($pref_data['data']['preferences']);

$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/water_polo/html/water_polo_weekly_css.html');

if (is_file($file)) {
	$css_data = file_get_contents($file);
} else {
	file_put_contents($file, '');
}

if (empty($css_data) || isset($_GET['reload_default']) ) {
	//Get template file
	$css_data = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/sports/s/water_polo/css/default_water_polo_weekly_css.css");
	$css_data = str_replace('_suffix', "_$area_id", $css_data);
}

//Replace Tags
$template = str_replace('<!-- css_data -->', $css_data, $template);
$template = str_replace('<!-- area_id -->', $area_id, $template);

echo $template;