<?php

/**
 * Deletes a match from the DB
 * 
 * @author Thilo Muller (2011)
 * @version $Id: ajax_match_delete.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$match_id = $_GET['matchID'];

//Delete Match
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE match_id=:match_id
				LIMIT 1";
$sql_delete = $GLOBALS['dbCon']->prepare($statement);
$sql_delete->bindParam(':match_id', $match_id);
$sql_delete->execute();
$sql_delete->closeCursor();


echo "OK";
?>
