<?php

/**
 * Edit or create a new match
 * 
 * @author Thilo Muller(2011)
 * @version $Id: match_edit.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';


//Vars
$template = file_get_contents('html/match_edit.html');
$data_fields = array('resultB','resultA','set1A','set2A','set3A','set4A','set5A','set1B','set2B','set3B',
						'set4B','set5B');
$data_fields2 = array('teamA','teamB','playerA1','playerA2','playerB1','playerB2','match_name','match_group',
						'match_date');
foreach ($data_fields as $data) $$data = '';
foreach ($data_fields2 as $data) $$data = '';
$sport_id = 'tennis';
$category_select = '';
$team_options = '';

//Get group list
$statement = "SELECT DISTINCT match_group 
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE sport_id=:sport_id
				ORDER BY match_group";
$sql_group = $GLOBALS['dbCon']->prepare($statement);
$sql_group->bindParam(':sport_id', $sport_id);
$sql_group->execute();
$sql_group_data = $sql_group->fetchAll();
$sql_group->closeCursor();
foreach ($sql_group_data as $group_data) {
	$category_select .= "<tr>";
	$category_select .= "<td onclick=\"document.getElementById('text2').value=('{$group_data['match_group']}');document.getElementById('option_placeholder').style.display='none'\" onmouseout=\"this.style.backgroundColor='silver';this.style.color='#000000'\" onmouseover=\"this.style.backgroundColor='#316AC5';this.style.color='white'\" class='comboOption'>";
	$category_select .= $group_data['match_group'];
	$category_select .= "</td></tr>";
}

//Get team list
$statement = "SELECT team_id, team_name
				FROM {$GLOBALS['db_prefix']}_sports_team
				WHERE sport_id='tennis'
				ORDER BY team_name";
$sql_teams = $GLOBALS['dbCon']->prepare($statement);
$sql_teams->execute();
$sql_teams_data = $sql_teams->fetchAll();
$sql_teams->closeCursor();
foreach ($sql_teams_data as $team_data) {
	$team_options .= "<option value={$team_data['team_id']}>{$team_data['team_name']}</option>";
}


if (isset($_GET['match_id'])) {
	$match_id = $_GET['match_id'];
	
	//Get data
	$statement = "SELECT match_name, DATE_FORMAT(match_start, '%e %M %Y') AS match_start, match_group
					FROM {$GLOBALS['db_prefix']}_sports_match
					WHERE match_id=:match_id
					LIMIT 1";
	$sql_match = $GLOBALS['dbCon']->prepare($statement);
	$sql_match->bindParam(':match_id', $match_id);
	$sql_match->execute();
	$sql_match_data = $sql_match->fetch();
	$sql_match->closeCursor();
	$m