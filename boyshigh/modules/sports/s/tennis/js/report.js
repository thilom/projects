/**
 * Initialize page
 */
function init() {
	calStart = new CalendarPopup("calDiv");
	calStart.setCssPrefix("SCRUM");
	calStart.offsetY = 7;
	
    document.onchange = handleEvent;
}

/**
 * Event Handler
 */
function handleEvent(e) {
	var eSrc;
	
	if (window.event) {
		e = window.event;
		eSrc = e.srcElement;
	} else {
		eSrc = e.target;
	}

if (e.type == 'click') {
		
	}
	
	if (e.type == 'change') {
		if (eSrc.id == 'dateSelection') {
			setDate(eSrc.value);
		}
	}
	
    if (e.type == 'blur') {
		if (eSrc.id == 'reportEnd') {
//			blurDate(eSrc.value);
		}
	}
}


/**
 * Set date fields for quick date select
 */
function setDate(srcValue) {
	
	switch (srcValue) {
		case '1':
			endDate = Date.today();
			startDate = Date.today().addDays(-7).days();
			endDate = endDate.toString('d MMMM yyyy');
			startDate = startDate.toString('d MMMM yyyy');
			break;
		case '2':
			endDate = Date.today();
			currentWeek = Date.today().getWeekOfYear();
			startDate = Date.today().moveToDayOfWeek(0, -1);
			endDate = endDate.toString('d MMMM yyyy');
			startDate = startDate.toString('d MMMM yyyy');
			break;
		case '3':
			endDate = Date.today().moveToDayOfWeek(0, -1);
			startDate = Date.today().moveToDayOfWeek(0, -2);
			endDate = endDate.toString('d MMMM yyyy');
			startDate = startDate.toString('d MMMM yyyy');
			break;
		case '4':
			endDate = Date.today().moveToDayOfWeek(0, -1);
			startDate = Date.today().moveToFirstDayOfMonth();
			endDate = endDate.toString('d MMMM yyyy');
			startDate = startDate.toString('d MMMM yyyy');
			break;
		case '5':
			endDate = Date.today().add({months: -1}).moveToLastDayOfMonth();
			startDate = Date.today().add({months: -1}).moveToFirstDayOfMonth();
			endDate = endDate.toString('d MMMM yyyy');
			startDate = startDate.toString('d MMMM yyyy');
			break;
		default:
			return;
	}

	document.getElementById('reportStart').value = startDate;
	document.getElementById('reportEnd').value = endDate;

}

/**
 * Check for change in date fields
 */
function blurDate(newDate) {
	alert(newDate)
    if (newDate != rEnd) {
		alert(change);
	}
}


