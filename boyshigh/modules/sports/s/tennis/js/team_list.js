var c;

/**
 * Initialize page
 */
function init() {
    document.onclick = handleEvent;
	getTeams();
}

/**
 * Event Handler
 */
function handleEvent(e) {
	var eSrc;
	if (window.event) {
		e = window.event;
		eSrc = e.srcElement;
	} else {
		eSrc = e.target;
	}
	

	if (e.type == 'click') {
		if (eSrc.id == 'addMatch') {
			openTeamWindow();
		}
		if (eSrc.id == 'editMatch') {
			openTeamWindow(eSrc.name);
		}
		if (eSrc.id == 'deleteMatch') {
			deleteTeam(eSrc.name);
		}
		if (eSrc.name == 'firstPage') {
			getTeams(0);
		}
		if (eSrc.name == 'previousPage') {
			next = eSrc.id.substr(1);
			next = (next*1);
			getTeams(next);
		}
		if (eSrc.name == 'nextPage') {
			next = eSrc.id.substr(1);
			next = (next*1);
			getTeams(next);
		}
		if (eSrc.name == 'lastPage') {
			next = eSrc.id.substr(1);
			next = next*10;
			getTeams(next);
		}
		
		if (eSrc.name == 'filterMatch') {
			getTeams();
		}
		
	}
}

/**
 * Opens the match editor
 */
function openTeamWindow(id) {
    W = getUrlValue('W');
	url = '/modules/sports/s/tennis/team_edit.php';
	if (id) url += "?team_id=" + id;
	window.parent.W('Tennis: Team Edit',url,500,900);
	window.parent.WD(W);
}

/**
 * Get a list of matches from the server and draw to open window.
 */
function getTeams(startAt,filter) {
	var teamCount;
	if (!startAt) startAt=0;
	var tbl = document.getElementById('matchList');
	var d=0, s=0;
	
	//Check if there is anything to filter
	filter = document.getElementById('filterString').value;
	
	//Kill all rows
	for (i = 0; i < c; i++) {
		row = document.getElementById('row'+i);
		tbl.removeChild(row);
		
		emptyTR=document.createElement('TR');
		emptyTR.setAttribute('id', 'row'+d);
		emptyTD=document.createElement('TR');
		emptyTD.setAttribute('colspan', '5');
		emptyText=document.createTextNode('\u00a0');
		
		emptyTD.appendChild(emptyText);
		emptyTR.appendChild(emptyTD);
		tbl.appendChild(emptyTR);
		
		d++;
	}
	
	c=0;
	
    if (window.XMLHttpRequest)  {
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			al = xmlhttp.responseText;
			matchLine = al.split('|||');
			for (i in matchLine) {
				if (matchLine[i].substr(0,5) == '<link') continue;

				matchItem = matchLine[i].split('~~~');

				if (matchItem[0] == 'data') {
					teamCount = matchItem[1];
					document.getElementById('matchCount').innerHTML = teamCount + ' Team' + (teamCount==1?"":'s') + ' in total';
					filterCount = matchItem[2];
					if (filter == '') {
						document.getElementById('filterCount').innerHTML = 'No Filter';
					} else {
						document.getElementById('filterCount').innerHTML = filterCount + ' Filtered Result' + (filterCount==1?"":'s');
					}
					continue;
				}

				//Clear Row
				if (s < d) {
					row = document.getElementById('row'+s);
					tbl.removeChild(row);
					s++;
				}

				//Table Elements
				tr = document.createElement('TR');
				tr.setAttribute('id', 'row'+ c)
				tdDate = document.createElement('TD');
				tdName = document.createElement('TD');
				tdGroup = document.createElement('TD');
				tdButtons = document.createElement('TD');
				tdButtons.setAttribute('class','lastCol');

				//Text Nodes
				txtName = document.createTextNode(matchItem[1]);

				//Buttons
				editButton = document.createElement('INPUT');
				editButton.setAttribute('type', 'button');
				editButton.setAttribute('value', 'Edit');
				editButton.setAttribute('id', 'editMatch');
				editButton.setAttribute('name', matchItem[0]);
				editButton.setAttribute('class','edit_button');
				deleteButton = document.createElement('INPUT');
				deleteButton.setAttribute('type', 'button');
				deleteButton.setAttribute('value', 'Delete');
				deleteButton.setAttribute('id', 'deleteMatch');
				deleteButton.setAttribute('name', matchItem[0]);
				deleteButton.setAttribute('class', 'delete_button');

				//Put it all together
				tdName.appendChild(txtName);
				tdButtons.appendChild(deleteButton);
				tdButtons.appendChild(editButton);

				tr.appendChild(tdName);
				tr.appendChild(tdButtons);

				tbl.appendChild(tr);

				c++;
			}

			while (s<d) {
				row = document.getElementById('row'+s);
				tbl.removeChild(row);
				s++;   
			}

			//No of pages
			pages = Math.floor(filterCount/10);
			last = Math.floor(startAt/10);
			lastPage = pages==last?'-1':pages;
			nextPage = lastPage==-1?'-1':startAt+10;
			previousPage = (last-1)==-1?'-1':startAt-10;

			drawNavigation('matchList',startAt,previousPage,nextPage,lastPage);
		}
	}

	matchUrl = "/modules/sports/s/tennis/ajax_team_list.php?start="+startAt+"&length=10";
	if (filter != '') matchUrl += "&filter="+filter;

	xmlhttp.open("GET",matchUrl,true);
	xmlhttp.send();
}

/**
 * Creates a navigation bar 
 */
function drawNavigation(tableID, firstPage, previousPage, nextPage, lastPage) {
    parentNode = document.getElementById(tableID);

	//Add TR
	tr = document.createElement('TR');
	tr.setAttribute('id', 'row'+c);
	td = document.createElement('TD');
	td.setAttribute('colspan','4');
	
	//Inner Table
	innerTable = document.createElement('TABLE');
	innerTable.setAttribute('class', 'navigation');
	innerTR = document.createElement('TR');
	innerTDfirst = document.createElement('TD');
	innerTDprevious = document.createElement('TD');
	innerTDnext = document.createElement('TD');
	innerTDlast = document.createElement('TD');
	
	//Addition nodes and setting for firstTD
	innerTDfirst.setAttribute('class', 'navFirstTD');
	if (firstPage == '0') {
		tdFirstSpan = document.createElement('SPAN');
		tdFirstSpan.setAttribute('class', 'navLinkLeftOff');
	} else {
		tdFirstSpan = document.createElement('A');
		tdFirstSpan.setAttribute('name', 'firstPage');
		tdFirstSpan.setAttribute('class', 'navLinkLeft');	
	}
	tdFirstText = document.createTextNode('First');
	tdFirstSpan.appendChild(tdFirstText);
	innerTDfirst.appendChild(tdFirstSpan);
	
	//Addition nodes and setting for previousTD
	innerTDprevious.setAttribute('class', 'navPreviousTD');
	if (previousPage < 0) {
		tdpreviousSpan = document.createElement('SPAN');
		tdpreviousSpan.setAttribute('class', 'navLinkLeftOff');	
	} else {
		tdpreviousSpan = document.createElement('A');
		tdpreviousSpan.setAttribute('name', 'previousPage');
		tdpreviousSpan.setAttribute('class', 'navLinkLeft');
		tdpreviousSpan.setAttribute('id', 'n' +previousPage);
	}
	
	tdpreviousText = document.createTextNode('Previous');
	tdpreviousSpan.appendChild(tdpreviousText);
	innerTDprevious.appendChild(tdpreviousSpan);
	
	//Addition nodes and setting for nextTD
	innerTDnext.setAttribute('class', 'navNextTD');
	if (nextPage < 0) {
		tdnextSpan = document.createElement('SPAN');
		tdnextSpan.setAttribute('class', 'navLinkRightOff');
	} else {
		tdnextSpan = document.createElement('A');
		tdnextSpan.setAttribute('name', 'nextPage');
		tdnextSpan.setAttribute('class', 'navLinkRight');
		tdnextSpan.setAttribute('id', 'n' +nextPage);
	}
		
	tdnextText = document.createTextNode('Next');
	tdnextSpan.appendChild(tdnextText);
	innerTDnext.appendChild(tdnextSpan);
	
	//Addition nodes and setting for lastTD
	innerTDlast.setAttribute('class', 'navLastTD');
	if (lastPage < 0) {
		tdlastSpan = document.createElement('SPAN');
		tdlastSpan.setAttribute('class', 'navLinkRightOff');	
	} else {
		tdlastSpan = document.createElement('A');
		tdlastSpan.setAttribute('name', 'lastPage');
		tdlastSpan.setAttribute('class', 'navLinkRight');	
		tdlastSpan.setAttribute('id', 'n' + lastPage);	
	}
	
	tdlastText = document.createTextNode('Last');
	tdlastSpan.appendChild(tdlastText);
	innerTDlast.appendChild(tdlastSpan);
	
	//Assemble inner Table
	innerTR.appendChild(innerTDfirst);
	innerTR.appendChild(innerTDprevious);
	innerTR.appendChild(innerTDnext);
	innerTR.appendChild(innerTDlast);
	innerTable.appendChild(innerTR);
	
	//Attach element
	td.appendChild(innerTable);
	tr.appendChild(td);
	parentNode.appendChild(tr);
	
	c++;
}

/**
 * Delete a match
 */
function deleteTeam(teamID) {
    if (confirm('WARNING!\nThis will permanently delete the team and cannot be undone.\n\nContinue?')) {
		 if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				al = xmlhttp.responseText;
				alert('Team Deleted');
				getTeams();
			}
		}
		url = "/modules/sports/s/tennis/ajax_team_delete.php?teamID=" + teamID;
		
		xmlhttp.open("GET",url,true);
		xmlhttp.send();
	}
	
}