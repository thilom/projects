
var teamSide;

/**
 * Initialize page
 */
function init() {
    document.onkeyup = handleEvent;
    document.onclick = handleEvent;
    document.onchange = handleEvent;
}

/**
 * Event Handler
 */
function handleEvent(e) {
	var eSrc;
	if (window.event) {
		e = window.event;
		eSrc = e.srcElement;
	} else {
		eSrc = e.target;
	}
	if (e.type == 'keyup') {
		switch(eSrc.id) {
			case 'teamB':
			case 'teamA':
			case 'playerB1':
			case 'playerB2':
			case 'playerA1':
			case 'playerA2':
			
				drawPlayers();
				break;
			case 'set1A':
			case 'set2A':
			case 'set3A':
			case 'set4A':
			case 'set5A':
			case 'set1B':
			case 'set2B':
			case 'set3B':
			case 'set4B':
			case 'set5B':
				calcResults();
				break;
		}
	}

	if (e.type == 'click') {
		switch(eSrc.id) {
			case 'teamASelect':
				openTeamSelect('A');
				break;
			case 'teamBSelect':
				openTeamSelect('B');
				break;
			case 'addTeamClose':
				closeTeamSelect();
				break;
			case 'addTeamButton':
				insertTeam();
				break;
		}
	}
    
	if (e.type == 'change') {
		switch (eSrc.id) {
			case 'teamID':
				populateTeamList(eSrc.value);
				break;
		}

	}   

}

/**
 * Draws the player or teamname to results table
 */
function drawPlayers() {
	var contentA, contentB;
	
	var teamA = document.getElementById('teamA').value;
	var teamB = document.getElementById('teamB').value;
	var playerA1 = document.getElementById('playerA1').value;
	var playerA2 = document.getElementById('playerA2').value;
	var playerB1 = document.getElementById('playerB1').value;
	var playerB2 = document.getElementById('playerB2').value;
	
	//Assemble A
	if (playerA1 == '' && playerA2 == '') {
		contentA = teamA;
	} else {
		contentA = playerA1;
		if (playerA2 != '') contentA += "<br>" + playerA2;
	}
	
	//Assemble B
	if (playerB1 == '' && playerB2 == '') {
		contentB = teamB;
	} else {
		contentB = playerB1;
		if (playerB2 != '') contentB += "<br>" + playerB2;
	}
    
	document.getElementById('playersA').innerHTML = contentA;
	document.getElementById('playersB').innerHTML = contentB;
	
}
	

/**
 * Calculate the results
 */
function calcResults() {
	var err;
	var teamAresult = 0;
	var teamBresult = 0;
	var teamA = new Array('set1A','set2A','set3A','set4A','set5A');
	var teamB = new Array('set1B','set2B','set3B','set4B','set5B');
   
    drawError('');
   
	for (i in teamA) {
		setA = document.getElementById(teamA[i]).value;
		setB = document.getElementById(teamB[i]).value;
		
		
		if (setA > setB) {
			teamAresult++;
		} else if(setB > setA) {
			teamBresult++;
		} else {
			if (setA!='' && setB!='') {
				s =  Number(i);
				err = 'Set ' + (s+1) + ' is a draw. Are you sure this is correct?';
				drawError(err);
			}
		}
	}
	
	//Insert results
	document.getElementById('resultA').value = teamAresult;
	document.getElementById('resultB').value = teamBresult;
}

/**
 * Displays an error for result calculations
 */
function drawError(err) {
	err = err==''?'':"<b>WARNING!</b><br>" + err;
    document.getElementById('resultError').innerHTML = err;
}

/**
 * Opens the Add Team window
 */
function openTeamSelect(team) {
	teamBox = document.getElementById('teamList');
	if (team == 'A') {
		teamBox.style.left = 26;
	} else {
		teamBox.style.left = 464;
	}
	teamBox.style.top = 130;
	
	teamBox.className = 'showList';
	teamSide = team;
}

/**
 * Close team select window
 */
function closeTeamSelect() {
   document.getElementById('teamList').className = 'hideList';
}


/**
 * Populates the team list for selection
 */
function populateTeamList(teamID, teamSide) {
	anchor = document.getElementById('selectionBox');
	
	//Clear selection list
	anchor.innerHTML = '';
	
    if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			
				//Text
				introDiv = document.createElement('DIV');
				introDiv.setAttribute('class', 'intro');
				intro = document.createTextNode('Select the sportsman to include in this match(optional)');
				hr = document.createElement('HR');
				introDiv.appendChild(intro);
				introDiv.appendChild(hr);
				anchor.appendChild(introDiv);
				
				
				al = xmlhttp.responseText;
				if (window.DOMParser) {
					  parser=new DOMParser();
					  xmlDoc=parser.parseFromString(al,"text/xml");
				} else  {
					  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
					  xmlDoc.async="false";
					  xmlDoc.loadXML(al);
				} 
				el = xmlDoc.documentElement.childNodes;
				for (i = 0; i < el.length; i++) {
					memberName = el[i].childNodes[0].nodeValue;
					
					Chk = document.createElement('INPUT');
					Chk.setAttribute('type', 'checkbox');
					Chk.setAttribute('value', memberName);
					
					txt = document.createTextNode(memberName);
					br = document.createElement('BR');
					
					anchor.appendChild(Chk);
					anchor.appendChild(txt);
					anchor.appendChild(br);
				}
    
			
				
			}
		}
		
		matchUrl = "/modules/sports/s/tennis/ajax_team_members.php?team_id=" + teamID;
		
		xmlhttp.open("GET",matchUrl,true);
		xmlhttp.send();
}

/**
 * Inserts the selected teams and players
 */
function insertTeam() {
	cnt = 1;
	
	teamSelect = document.getElementById('teamID');
	teamName = teamSelect.options[teamSelect.selectedIndex].text;
	document.getElementById('team'+teamSide).value = teamName;
	
	chkBoxes = document.getElementById('selectionBox').getElementsByTagName('INPUT');
	for (i = 0; i < chkBoxes.length; i++) {
		if (chkBoxes[i].checked) {
			document.getElementById('player' + teamSide + cnt).value = chkBoxes[i].value;	
			cnt++;
			if (cnt>2) break;
		}
	}
	
	drawPlayers();
	closeTeamSelect();
}