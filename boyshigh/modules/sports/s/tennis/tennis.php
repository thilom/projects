<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Includes
include 'toolbar.php';

//Vars
$window_function = isset($_GET['f'])?$_GET['f']:'';

//Get page
switch ($window_function) {
	case 'match_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/tennis/match_list.php';
		break;
	case 'team_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/tennis/team_list.php';
		break;
	case 'sports_report':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/tennis/report.php';
		break;
	default:
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/tennis/tennis_overview.php';
}

?>
