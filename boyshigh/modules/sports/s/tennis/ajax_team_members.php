<?php

/**
 * Get a list of team members for a spcified team
 * 
 * @author Thilo Muller (2011)
 * @version $Id$
 */

//Includes
$ajax = true;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$team_id = $_GET['team_id'];


//Get members
$statement = "SELECT sportsman_name
				FROM {$GLOBALS['db_prefix']}_sports_team_members
				WHERE team_id=:team_id";
$sql_members = $GLOBALS['dbCon']->prepare($statement);
$sql_members->bindParam(':team_id', $team_id);
$sql_members->execute();
$sql_members_data = $sql_members->fetchAll();
$sql_members->closeCursor();

echo "<members>";
foreach ($sql_members_data as $members_data) {
	
	echo "<name>{$members_data['sportsman_name']}</name>";
	
}
echo "</members>";

?>
