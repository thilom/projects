<?php

/**
 * Create a match report
 * 
 * @author Thilo Muller(2011)
 * @version $Id$  
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$template = file_get_contents('html/report.html');
$group_list = '<option value="all">All Groups</option>';
$team_list = '<option value="all">All Teams</option>';
$sportsmen_list = '<option value="all">All Sportsmen</option>';

if (count($_POST) > 0) {
	
	$template = file_get_contents('html/report_result.html');
	
	include 'include_result.php';
	
	//Replace tags
	$template = str_replace('<!-- report_data -->', $report_data, $template);
	
	echo $template;
	
	die();
}

//Get group list
$statement = "SELECT DISTINCT match_group
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE sport_id='tennis'
				ORDER BY match_group";
$sql_group = $GLOBALS['dbCon']->prepare($statement);
$sql_group->execute();
$sql_group_data = $sql_group->fetchAll();
$sql_group->closeCursor();

//Assemble group list
foreach ($sql_group_data as $group_data) {
	if (empty($group_data['match_group'])) continue;
	$group_list .= "<option value='{$group_data['match_group']}'>{$group_data['match_group']}</option>";
}


//Get team list
$statement = "SELECT DISTINCT team_name
				FROM {$GLOBALS['db_prefix']}_sports_match_team
				WHERE sports_id='tennis'
				ORDER BY team_name";
$sql_team = $GLOBALS['dbCon']->prepare($statement);
$sql_team->execute();
$sql_team_data = $sql_team->fetchAll();
$sql_team->closeCursor();

//Assemble Team list
foreach ($sql_team_data as $team_data) {
	if (empty($team_data['team_name'])) continue;
	$team_list .= "<option value='{$team_data['team_name']}'>{$team_data['team_name']}</option>";
}

//Get sportsman list
$statement = "SELECT DISTINCT player_name
				FROM {$GLOBALS['db_prefix']}_sports_match_sportsmen
				WHERE sports_id='tennis'
				ORDER BY player_name";
$sql_sportsman = $GLOBALS['dbCon']->prepare($statement);
$sql_sportsman->execute();
$sql_sportsman_data = $sql_sportsman->fetchAll();
$sql_sportsman->closeCursor();

//Assemble Sportsman list
foreach ($sql_sportsman_data as $sportsman_data) {
	if (empty($sportsman_data['player_name']))continue;
	$sportsmen_list .= "<option value='{$sportsman_data['player_name']}'>{$sportsman_data['player_name']}</option>";
}

//Replace Tags
$template = str_replace('<!-- group_list -->', $group_list, $template);
$template = str_replace('<!-- team_list -->', $team_list, $template);
$template = str_replace('<!-- sportsman_list -->', $sportsmen_list, $template);

echo $template;

?>
