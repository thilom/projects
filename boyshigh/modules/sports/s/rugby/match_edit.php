<?php

/**
 * Edit or create a new match
 *
 * @author Thilo Muller(2012)
 * @version $Id: match_edit.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';


//Vars
$template = file_get_contents('html/match_edit.html');
$data_fields = array('fullA','fullB','halfA','halfB');
foreach ($data_fields as $data) $$data = '';
$sport_id = 'rugby';
$category_select = '';
$team_options = '';
$match_name = '';
$teamA = '';
$teamB = '';
$match_date = '';

//Get group list
$statement = "SELECT DISTINCT match_group
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE sport_id=:sport_id
				ORDER BY match_group";
$sql_group = $GLOBALS['dbCon']->prepare($statement);
$sql_group->bindParam(':sport_id', $sport_id);
$sql_group->execute();
$sql_group_data = $sql_group->fetchAll();
$sql_group->closeCursor();
foreach ($sql_group_data as $group_data) {
	$category_select .= "<tr>";
	$category_select .= "<td onclick=\"document.getElementById('text2').value=('{$group_data['match_group']}');document.getElementById('option_placeholder').style.display='none'\" onmouseout=\"this.style.backgroundColor='silver';this.style.color='#000000'\" onmouseover=\"this.style.backgroundColor='#316AC5';this.style.color='white'\" class='comboOption'>";
	$category_select .= $group_data['match_group'];
	$category_select .= "</td></tr>";
}

//Get team list
$statement = "SELECT team_id, team_name
				FROM {$GLOBALS['db_prefix']}_sports_team
				WHERE sport_id='rugby'
				ORDER BY team_name";
$sql_teams = $GLOBALS['dbCon']->prepare($statement);
$sql_teams->execute();
$sql_teams_data = $sql_teams->fetchAll();
$sql_teams->closeCursor();
foreach ($sql_teams_data as $team_data) {
	$team_options .= "<div onclick='insertTeam(\"{$team_data['team_id']}\",\"{$team_data['team_name']}\")'>{$team_data['team_name']}</div>";
}


if (isset($_GET['match_id'])) {
	$match_id = $_GET['match_id'];

	//Get data
	$statement = "SELECT match_name, DATE_FORMAT(match_start, '%e %M %Y') AS match_start, match_group
					FROM {$GLOBALS['db_prefix']}_sports_match
					WHERE match_id=:match_id
					LIMIT 1";
	$sql_match = $GLOBALS['dbCon']->prepare($statement);
	$sql_match->bindParam(':match_id', $match_id);
	$sql_match->execute();
	$sql_match_data = $sql_match->fetch();
	$sql_match->closeCursor();
	$match_name = $sql_match_data['match_name'];
	$match_date = $sql_match_data['match_start'];
	$match_group = $sql_match_data['match_group'];

	//Get Teams
	$statement = "SELECT team_name, team_side
					FROM {$GLOBALS['db_prefix']}_sports_match_team
					WHERE match_id=:match_id";
	$sql_team = $GLOBALS['dbCon']->prepare($statement);
	$sql_team->bindParam(':match_id', $match_id);
	$sql_team->execute();
	$sql_team_data = $sql_team->fetchAll();
	$sql_team->closeCursor();
	foreach ($sql_team_data as $team_data) {
		switch ($team_data['team_side']) {
			case 'A':
				$teamA = $team_data['team_name'];
				break;
			case 'B':
				$teamB = $team_data['team_name'];
				break;
		}
	}

	//Team Players
//	$statement = "SELECT player_name, player_side
//					FROM {$GLOBALS['db_prefix']}_sports_match_sportsmen
//					WHERE match_id=:match_id";
//	$sql_players = $GLOBALS['dbCon']->prepare($statement);
//	$sql_players->bindParam(':match_id', $match_id);
//	$sql_players->execute();
//	$sql_players_data = $sql_players->fetchAll();
//	$sql_players->closeCursor();
//	foreach ($sql_players_data as $players_data) {
//		switch ($players_data['player_side']) {
//			case 'A1':
//				$playerA1 = $players_data['player_name'];
//				break;
//			case 'A2':
//				$playerA2 = $players_data['player_name'];
//				break;
//			case 'B1':
//				$playerB1 = $players_data['player_name'];
//				break;
//			case 'B2':
//				$playerB2 = $players_data['player_name'];
//				break;
//		}
//	}

	//Get results
	$statement = "SELECT result_type, result_value
					FROM {$GLOBALS['db_prefix']}_sports_match_result_rugby
					WHERE match_id=:match_id";
	$sql_results = $GLOBALS['dbCon']->prepare($statement);
	$sql_results->bindParam(':match_id', $match_id);
	$sql_results->execute();
	$sql_results_data = $sql_results->fetchAll();
	$sql_results->closeCursor();
	foreach ($sql_results_data as $results_data) {
		$$results_data['result_type'] = $results_data['result_value'];
	}

}

//Save Match
if (count($_POST) > 0) {
	if (isset($_GET['match_id'])) {
		$match_id = $_GET['match_id'];
		$message = '';

		//Match Name
		if ($match_name != $_POST['match_name']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match
							SET match_name=:match_name
							WHERE match_id=:match_id
							LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':match_name', $_POST['match_name']);
			$sql_update->bindParam(':match_id', $match_id);
			$sql_update->execute();
			$message .= "&#187; Match name updated<br>";
			write_log("Match name updated",'sports', $match_id);
		}

		//Match Date
		$post_date = date('Y-m-d', strtotime($_POST['match_date']));
		if ($match_date != $_POST['match_date']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match
							SET match_start=:match_start
							WHERE match_id=:match_id
							LIMIT 1";
			$sql_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_update->bindParam(':match_start', $post_date);
			$sql_update->bindParam(':match_id', $match_id);
			$sql_update->execute();
			$message .= "&#187; Match date updated<br>";
			write_log("Match date updated",'sports', $match_id);
		}

		//Match Group
//		if ($match_group != $_POST['match_group']) {
//			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match
//							SET match_group=:match_group
//							WHERE match_id=:match_id
//							LIMIT 1";
//			$sql_update = $GLOBALS['dbCon']->prepare($statement);
//			$sql_update->bindParam(':match_group', $_POST['match_group']);
//			$sql_update->bindParam(':match_id', $match_id);
//			$sql_update->execute();
//			$message .= "&#187; Match group updated<br>";
//			write_log("Match group updated",'sports', $match_id);
//		}

		//Match Team A
		if ($teamA != $_POST['teamA']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_team
							SET team_name=:team_name
							WHERE match_id=:match_id AND team_side='A'
							LIMIT 1";
			$sql_team = $GLOBALS['dbCon']->prepare($statement);
			$sql_team->bindParam(':team_name', $_POST['teamA']);
			$sql_team->bindParam(':match_id', $match_id);
			$sql_team->execute();

			$message .= "&#187; Match team (Side A) updated<br>";
			write_log("Match team (Side A) updated",'sports', $match_id);
		}

		//Match Team B
		if ($teamB != $_POST['teamB']) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_team
							SET team_name=:team_name
							WHERE match_id=:match_id AND team_side='B'
							LIMIT 1";
			$sql_team = $GLOBALS['dbCon']->prepare($statement);
			$sql_team->bindParam(':team_name', $_POST['teamB']);
			$sql_team->bindParam(':match_id', $match_id);
			$sql_team->execute();
			$message .= "&#187; Match team (Side B) updated<br>";
			write_log("Match team (Side B) updated",'sports', $match_id);
		}

		//Player A1
//		if ($playerA1 != $_POST['playerA1']) {
//			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_sportsmen
//							SET player_name=:player_name
//							WHERE match_id=:match_id AND player_side='A1'
//							LIMIT 1";
//			$sql_player = $GLOBALS['dbCon']->prepare($statement);
//			$sql_player->bindParam(':match_id', $match_id);
//			$sql_player->bindParam(':player_name', $_POST['playerA1']);
//			$sql_player->execute();
//			$message .= "&#187; Match player (Side A) updated<br>";
//			write_log("Match player (Side A) updated",'sports', $match_id);
//
//		}

		//Player A2
//		if ($playerA2 != $_POST['playerA2']) {
//			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_sportsmen
//							SET player_name=:player_name
//							WHERE match_id=:match_id AND player_side='A2'
//							LIMIT 1";
//			$sql_player = $GLOBALS['dbCon']->prepare($statement);
//			$sql_player->bindParam(':match_id', $match_id);
//			$sql_player->bindParam(':player_name', $_POST['playerA2']);
//			$sql_player->execute();
//			$message .= "&#187; Match player (Side A) updated<br>";
//			write_log("Match player (Side A) updated",'sports', $match_id);
//		}

		//Player B1
//		if ($playerB1 != $_POST['playerB1']) {
//			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_sportsmen
//							SET player_name=:player_name
//							WHERE match_id=:match_id AND player_side='B1'
//							LIMIT 1";
//			$sql_player = $GLOBALS['dbCon']->prepare($statement);
//			$sql_player->bindParam(':match_id', $match_id);
//			$sql_player->bindParam(':player_name', $_POST['playerB1']);
//			$sql_player->execute();
//			$message .= "&#187; Match player (Side B) updated<br>";
//			write_log("Match player (Side B) updated",'sports', $match_id);
//		}

		//Player B2
//		if ($playerB2 != $_POST['playerB2']) {
//			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_sportsmen
//							SET player_name=:player_name
//							WHERE match_id=:match_id AND player_side='B2'
//							LIMIT 1";
//			$sql_player = $GLOBALS['dbCon']->prepare($statement);
//			$sql_player->bindParam(':match_id', $match_id);
//			$sql_player->bindParam(':player_name', $_POST['playerB2']);
//			$sql_player->execute();
//			$message .= "&#187; Match player (Side B) updated<br>";
//			write_log("Match player (Side B) updated",'sports', $match_id);
//		}

		//Match Results
		$match_results = "$halfA{$halfB}$fullA{$fullB}";
		$post_results = "{$_POST['halfA']}{$_POST['halfB']}{$_POST['fullA']}{$_POST['fullB']}";
		if ($match_results != $post_results) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_sports_match_result_rugby
							SET result_value=:result_value
							WHERE match_id=:match_id AND result_type=:result_type
							LIMIT 1";
			$sql_results = $GLOBALS['dbCon']->prepare($statement);
			foreach ($data_fields as $field_name) {
				$sql_results->bindParam(':match_id', $match_id);
				$sql_results->bindParam(':result_type', $field_name);
				$sql_results->bindParam(':result_value', $_POST[$field_name]);
				$sql_results->execute();
			}

			$message .= "&#187; Match results updated<br>";
			write_log("Match results updated",'sports', $match_id);
		}



	} else {
		$match_id = next_id("{$GLOBALS['db_prefix']}_sports_match", 'match_id');

		//Save Match Data
		$match_name = $_POST['match_name'];
		$match_start = date('Y-m-d', strtotime($_POST['match_date']));
//		$match_group = $_POST['match_group'];

		//Create match
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match
							(match_id, sport_id, match_start, match_group, match_name)
						VALUES
							(:match_id, :sport_id, :match_start, '', :match_name)";
		$sql_create = $GLOBALS['dbCon']->prepare($statement);
		$sql_create->bindParam(':match_id', $match_id);
		$sql_create->bindParam(':sport_id', $sport_id);
		$sql_create->bindParam(':match_start', $match_start);
		$sql_create->bindParam(':match_name', $match_name);
		$sql_create->execute();
		$sql_create->closeCursor();

		//Save result
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_result_rugby
						(match_id, result_type, result_value)
							VALUES
						(:match_id, :result_type, :result_value)";
		$sql_results = $GLOBALS['dbCon']->prepare($statement);
		foreach ($data_fields as $field_name) {
			$sql_results->bindParam(':match_id', $match_id);
			$sql_results->bindParam(':result_type', $field_name);
			$sql_results->bindParam(':result_value', $_POST[$field_name]);
			$sql_results->execute();
		}

		//Save Team A
		$team_side = 'A';
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_team
							(match_id, sports_id, team_name, team_side)
						VALUES
							(:match_id, :sports_id, :team_name, :team_side)";
		$sql_team = $GLOBALS['dbCon']->prepare($statement);
		$sql_team->bindParam(':match_id', $match_id);
		$sql_team->bindParam(':sports_id', $sport_id);
		$sql_team->bindParam(':team_name', $_POST['teamA']);
		$sql_team->bindParam(':team_side', $team_side);
		$sql_team->execute();

		//Save Team B
		$team_side = 'B';
		$sql_team->bindParam(':match_id', $match_id);
		$sql_team->bindParam(':sports_id', $sport_id);
		$sql_team->bindParam(':team_name', $_POST['teamB']);
		$sql_team->bindParam(':team_side', $team_side);
		$sql_team->execute();

		//Save players Side A1
//		$team_side = 'A1';
//		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_sportsmen
//							(match_id, sports_id, player_name, player_side)
//						VALUES
//							(:match_id, :sports_id, :player_name, :player_side)";
//		$sql_players = $GLOBALS['dbCon']->prepare($statement);
//		$sql_players->bindParam(':match_id', $match_id);
//		$sql_players->bindParam(':sports_id', $sport_id);
//		$sql_players->bindParam(':player_name', $_POST['playerA1']);
//		$sql_players->bindParam(':player_side', $team_side);
//		$sql_players->execute();

		//Save players Side A2
		$team_side = 'A2';
//		$sql_players->bindParam(':match_id', $match_id);
//		$sql_players->bindParam(':sports_id', $sport_id);
//		$sql_players->bindParam(':player_name', $_POST['playerA2']);
//		$sql_players->bindParam(':player_side', $team_side);
//		$sql_players->execute();

		//Save players Side B1
//		$team_side = 'B1';
//		$sql_players->bindParam(':match_id', $match_id);
//		$sql_players->bindParam(':sports_id', $sport_id);
//		$sql_players->bindParam(':player_name', $_POST['playerB1']);
//		$sql_players->bindParam(':player_side', $team_side);
//		$sql_players->execute();

		//Save players Side B2
//		$team_side = 'B2';
//		$sql_players->bindParam(':match_id', $match_id);
//		$sql_players->bindParam(':sports_id', $sport_id);
//		$sql_players->bindParam(':player_name', $_POST['playerB2']);
//		$sql_players->bindParam(':player_side', $team_side);
//		$sql_players->execute();

		$message = "&#187; Rugby match saved<br>";
		write_log("rugby match added",'sports', $match_id);

	}

	if (empty($message)) $message = '&#187; No Changes, Nothing to update';
	echo "<script type='text/javascript' src='/modules/quote/js/quote.js'></script>";
	echo "<br><div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'>";
	echo "<input type=button value='Add Another Match' class=ok_button onClick='document.location=\"/modules/sports/s/rugby/match_edit.php\"' >";
	echo "<input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/sports/s/rugby/match_edit.php?match_id=$match_id\"' >";
	echo "</div>";


	die();
}

//Replace Tags
$template = str_replace('<!-- fullA -->', $fullA, $template);
$template = str_replace('<!-- fullB -->', $fullB, $template);
$template = str_replace('<!-- halfA -->', $halfA, $template);
$template = str_replace('<!-- halfB -->', $halfB, $template);
//$template = str_replace('<!-- set3A -->', $set3A, $template);
//$template = str_replace('<!-- set4A -->', $set4A, $template);
//$template = str_replace('<!-- set5A -->', $set5A, $template);
//$template = str_replace('<!-- set1B -->', $set1B, $template);
//$template = str_replace('<!-- set2B -->', $set2B, $template);
//$template = str_replace('<!-- set3B -->', $set3B, $template);
//$template = str_replace('<!-- set4B -->', $set4B, $template);
//$template = str_replace('<!-- set5B -->', $set5B, $template);
$template = str_replace('<!-- teamA -->', $teamA, $template);
$template = str_replace('<!-- teamB -->', $teamB, $template);
//$template = str_replace('<!-- playerA1 -->', $playerA1, $template);
//$template = str_replace('<!-- playerA2 -->', $playerA2, $template);
//$template = str_replace('<!-- playerB1 -->', $playerB1, $template);
//$template = str_replace('<!-- playerB2 -->', $playerB2, $template);
$template = str_replace('<!-- match_name -->', $match_name, $template);
$template = str_replace('<!-- match_date -->', $match_date, $template);
//$template = str_replace('<!-- match_group -->', $match_group, $template);
//$template = str_replace('<!-- match_group -->', $match_group, $template);
$template = str_replace('<!-- category_select -->', $category_select, $template);
$template = str_replace('<!-- TeamOptionList -->', $team_options, $template);

echo $template;
?>
