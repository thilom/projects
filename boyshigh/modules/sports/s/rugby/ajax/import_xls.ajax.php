<?php

/**
 * Import an uploaded results file
 * 
 * 2015-03-21: Thilo Muller     - Created
 */

//Includes
$ajax = 1;
require_once '../../../../../settings/init.php';
require_once '../../../../../shared/phpExcel-1.8.0/Classes/PHPExcel.php';


//Vars
$excelClass = new PHPExcel_Reader_Excel2007();
$result['status'] = '';
$insert_count = 0;
$warning_count = 0;
$import_count = 0;

//Check 
if (empty($_FILES['upload']['name'])) {
    $result['status'] = 'error';
    $result['message'] = "No file attached. Please choose a file and try again.";
    echo json_encode($result);
    die();
}

//Check that it's an excel file
$ext = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);
if ($ext != 'xlsx') {
    $result['status'] = 'error';
    $result['message'] = "The uploaded document is not an Excel2007 file";
    echo json_encode($result);
    die();
}

//Clear Temp Table
$statement = "DELETE FROM {$GLOBALS['db_prefix']}_sports_result_rugby_tmp";
$sql_clear = $GLOBALS['dbCon']->prepare($statement);
$sql_clear->execute();

//Read document
$excelClass->setReadDataOnly(true);
$excelObject = $excelClass->load($_FILES['upload']['tmp_name']);

//Check that it's a rugby results file
$file_name = $excelObject->getActiveSheet()->getCellByColumnAndRow(2, 1)->getValue();
if ($file_name != 'Rugby Results') {
    $result['status'] = 'error';
    $result['message'] = "The uploaded file does not appear to be an official rugby results file.";
    echo json_encode($result);
    die();
}

//Loop through each line and save to temp table
$total_rows = $excelObject->getActiveSheet()->getHighestRow();
for ($x = 3; $x<$total_rows+1; $x++) {
    $match_date = $excelObject->getActiveSheet()->getCellByColumnAndRow(0, $x)->getValue();
    $match_date = date('Y-m-d',PHPExcel_Shared_Date::ExcelToPHP(str_replace('/', '-', $match_date)));
//    $match_date = date('Y-m-d', strtotime(str_replace('/', '-', $match_date)));

    $match_name = $excelObject->getActiveSheet()->getCellByColumnAndRow(1, $x)->getValue();
    $team_a = $excelObject->getActiveSheet()->getCellByColumnAndRow(2, $x)->getValue();
    $team_b = $excelObject->getActiveSheet()->getCellByColumnAndRow(3, $x)->getValue();
    $team_a_result = $excelObject->getActiveSheet()->getCellByColumnAndRow(4, $x)->getValue();
    $team_b_result = $excelObject->getActiveSheet()->getCellByColumnAndRow(5, $x)->getValue();
    
    //Check if already imported [DB View: rugby_results]
    $statement = "SELECT COUNT(*) AS count_result
                    FROM rugby_results
                    WHERE match_start = :match_start
                        AND match_name = :match_name
                        AND team_a = :team_a
                        AND team_b = :team_b
                        AND team_a_result = :team_a_result
                        AND team_b_result = :team_b_result";
    $sql_check = $GLOBALS['dbCon']->prepare($statement);
    $sql_check->bindParam(':match_start', $match_date);
    $sql_check->bindParam(':match_name', $match_name);
    $sql_check->bindParam(':team_a', $team_a);
    $sql_check->bindParam(':team_b', $team_b);
    $sql_check->bindParam(':team_a_result', $team_a_result);
    $sql_check->bindParam(':team_b_result', $team_b_result);
    $sql_check->execute();
    $sql_check_data = $sql_check->fetch();
    $sql_check->closeCursor();
    
    
//    echo "$match_date : '$match_name' : $team_a : $team_b : $team_a_result : $team_b_result \n";
//    var_dump($sql_check_data['count_result']);
    
    //Add to temp table
    if ($sql_check_data['count_result'] == 0) {
        
        if (empty($match_date) || empty($match_name) || empty($team_a) || empty($team_b)) {
            $warning_count++;
        } else {
            $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_result_rugby_tmp
                            (match_date, match_name, team_a, team_b, team_a_result, team_b_result)
                        VALUES
                            (:match_date, :match_name, :team_a, :team_b, :team_a_result, :team_b_result)";
            $sql_insert = $GLOBALS['dbCon']->prepare($statement);
            $sql_insert->bindParam(':match_date', $match_date);
            $sql_insert->bindParam(':match_name', $match_name);
            $sql_insert->bindParam(':team_a', $team_a);
            $sql_insert->bindParam(':team_b', $team_b);
            $sql_insert->bindParam(':team_a_result', $team_a_result);
            $sql_insert->bindParam(':team_b_result', $team_b_result);
            $sql_insert->execute();

            $insert_count++;
        }
        
        
    }
    
    if ($insert_count == 0) {
        $result['status'] = 'error';
        $result['message'] = "No new results to import.";
    } else {
        $result['status'] = 'ok';
        
        if ($warning_count != 0) {
            $result['status'] = 'warning';
            if ($warning_count == 1) {
                $result['message'] = "1 entry is incomplete (insufficient data) and will not be imported.";
                if ($insert_count == 1) {
                    $result['message'] .= "<br>1 results will be imported";
                } else {
                    $result['message'] .= "<br>$insert_count results will be imported";
                }
            } else {
                $result['message'] = "$warning_count entries are incomplete (no data) and will not be imported.";
                if ($insert_count == 1) {
                    $result['message'] .= "<br>1 results will be imported";
                } else {
                    $result['message'] .= "<br>$insert_count results will be imported";
                }
            }
        } else {
            if ($insert_count == 1) {
                $result['message'] = "1 result will be imported";
            } else {
                $result['message'] = "$insert_count results will be imported";
            }
        }
    }
    
    
}





echo json_encode($result);


