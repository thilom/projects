<?php

/**
 * Move import list to live table
 * 
 * 2015-03-22: Thilo Muller     - Created
 */

//Includes
$ajax = 1;
require_once '../../../../../settings/init.php';

//Vars

//Get temp data
$statement = "SELECT match_date, match_name, team_a, team_b, team_a_result, team_b_result
                FROM {$GLOBALS['db_prefix']}_sports_result_rugby_tmp";
$sql_temp = $GLOBALS['dbCon']->prepare($statement);
$sql_temp->execute();
$sql_temp_data = $sql_temp->fetchAll(PDO::FETCH_ASSOC);
$sql_temp->closeCursor();

//Insert into live DB
foreach ($sql_temp_data as $data) {
    
    //Get match ID
    $statement = "SELECT MAX(match_id)+1 AS next_id
                    FROM {$GLOBALS['db_prefix']}_sports_match";
    $sql_id = $GLOBALS['dbCon']->prepare($statement);
    $sql_id->bindParam(':val_id', $val_id);
    $sql_id->execute();
    $sql_id_data = $sql_id->fetch();
    $sql_id->closeCursor();
    $match_id = $sql_id_data['next_id'];
    
    
    //Add match
    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match
                        (match_id, match_start, match_name, sport_id)
                    VALUES
                        (:match_id, :match_start, :match_name, 'rugby')";
    $sql_insert = $GLOBALS['dbCon']->prepare($statement);
    $sql_insert->bindParam(':match_id', $match_id);
    $sql_insert->bindParam(':match_start', $data['match_date']);
    $sql_insert->bindParam(':match_name', $data['match_name']);
    $sql_insert->execute();
    
    
    //Insert Teams (A)
    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_team
                        (match_id, sports_id, team_name, team_side)
                    VALUES
                        (:match_id, 'rugby', :team_name, 'A')";
    $sql_team_a = $GLOBALS['dbCon']->prepare($statement);
    $sql_team_a->bindParam(':match_id', $match_id);
    $sql_team_a->bindParam(':team_name', $data['team_a']);
    $sql_team_a->execute();
    
    //Insert Teams (B)
    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_team
                        (match_id, sports_id, team_name, team_side)
                    VALUES
                        (:match_id, 'rugby', :team_name, 'B')";
    $sql_team_b = $GLOBALS['dbCon']->prepare($statement);
    $sql_team_b->bindParam(':match_id', $match_id);
    $sql_team_b->bindParam(':team_name', $data['team_b']);
    $sql_team_b->execute();
    
    //Insert Results (A - Full)
    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_result_rugby
                        (match_id, result_type, result_value)
                    VALUES
                        (:match_id, 'fullA', :result_value)";
    $sql_result = $GLOBALS['dbCon']->prepare($statement);
    $sql_result->bindParam(':match_id', $match_id);
    $sql_result->bindParam(':result_value', $data['team_a_result']);
    $sql_result->execute();
    
    //Insert Results (A - Half)
    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_result_rugby
                        (match_id, result_type, result_value)
                    VALUES
                        (:match_id, 'halfA', 0)";
    $sql_result = $GLOBALS['dbCon']->prepare($statement);
    $sql_result->bindParam(':match_id', $match_id);
    $sql_result->execute();
    
    //Insert Results (B - Full)
    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_result_rugby
                        (match_id, result_type, result_value)
                    VALUES
                        (:match_id, 'fullB', :result_value)";
    $sql_result = $GLOBALS['dbCon']->prepare($statement);
    $sql_result->bindParam(':match_id', $match_id);
    $sql_result->bindParam(':result_value', $data['team_b_result']);
    $sql_result->execute();
    
    //Insert Results (B - Half)
    $statement = "INSERT INTO {$GLOBALS['db_prefix']}_sports_match_result_rugby
                        (match_id, result_type, result_value)
                    VALUES
                        (:match_id, 'halfB', 0)";
    $sql_result = $GLOBALS['dbCon']->prepare($statement);
    $sql_result->bindParam(':match_id', $match_id);
    $sql_result->execute();
    
    
    
}
