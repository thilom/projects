<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 * 06 Aug 2013 Sigal Zahavi Moved inclue toolbar to the end of the file to avoid headers errors
 */

//Vars
$window_function = isset($_GET['f'])?$_GET['f']:'';

//Get page
switch ($window_function) {
	case 'match_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/rugby/match_list.php';
		break;
	case 'team_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/rugby/team_list.php';
		break;
	case 'sports_report':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/rugby/report.php';
		break;
	case 'import_results':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/rugby/import.php';
		break;
	default:
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/rugby/match_list.php';
}
//Includes
include 'toolbar.php';
?>
