<?php

/**
 * Returns a list of matches for tennis
 *
 * @author Thilo Muller (2011)
 * @author Sigal Zahavi 30/08/2013 - Added team names and results
 * @version $Id: ajax_match_list.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;

//Vars
$match_list = '';
$startAt = isset($_GET['start'])?$_GET['start']:0;
$length = isset($_GET['length'])?$_GET['length']:10;
$filter_string = isset($_GET['filter'])?"%{$_GET['filter']}%":'';

//Get total match count
$statement = "SELECT COUNT(*) AS match_count
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE sport_id='hockey'";
$sql_count = $GLOBALS['dbCon']->prepare($statement);
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$match_list .= "|||data~~~{$sql_count_data['match_count']}";

//Get result match count
$statement = "SELECT COUNT(*) AS match_count
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE sport_id='hockey'";
if (!empty($filter_string)) {
	$statement .= " AND (match_name LIKE :filter_string OR match_group LIKE :filter_string)";
}
$sql_count = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) {
	$sql_count->bindParam(':filter_string', $filter_string);
}
$sql_count->execute();
$sql_count_data = $sql_count->fetch();
$sql_count->closeCursor();
$match_list .= "~~~{$sql_count_data['match_count']}";

//Get matches
$statement = "SELECT match_id, match_start, match_name, match_group
				FROM {$GLOBALS['db_prefix']}_sports_match
				WHERE sport_id='hockey'";
if (!empty($filter_string)) {
	$statement .= " AND (match_name LIKE :filter_string OR match_group LIKE :filter_string)";
}
$statement .= "ORDER BY match_start DESC
				LIMIT $startAt, $length";
$sql_matches = $GLOBALS['dbCon']->prepare($statement);
if (!empty($filter_string)) {
	$sql_matches->bindParam(':filter_string', $filter_string);
}
$sql_matches->execute();
$sql_matches_data = $sql_matches->fetchAll();
$sql_matches->closeCursor();

//Match players
$statement = "SELECT player_name, player_side
				FROM {$GLOBALS['db_prefix']}_sport_matches_sportsmen
				WHERE match_id=:match_id";

//Getting Team names and results
$statement = "SELECT team_name, team_side, result_type, result_value
                    FROM {$GLOBALS['db_prefix']}_sports_match_team a
                    JOIN {$GLOBALS['db_prefix']}_sports_match_result_rugby b
                    ON a.match_id = b.match_id
                    WHERE a.match_id = :match_id";
$sql_team = $GLOBALS['dbCon']->prepare($statement);

//Assemble Matches
foreach ($sql_matches_data as $match_data) {
	//Get team names and results
	$team_A = '';
	$team_B = '';
    $fullA = 0;
    $fullB = 0;
	$sql_team->bindParam(':match_id', $match_data['match_id']);
	$sql_team->execute();
	$sql_team_data = $sql_team->fetchAll();
	foreach ($sql_team_data as $team_data) {
		switch($team_data['team_side']) {
			case 'A':
				$team_A = $team_data['team_name'];
				break;
			case 'B':
				$team_B = $team_data['team_name'];
				break;
		}

        switch($team_data['result_type'])
        {
            case 'fullA':
                $fullA = $team_data['result_value'];
                break;
            case 'fullB':
                $fullB = $team_data['result_value'];
                break;
        }
	}

	$match_list .= "|||{$match_data['match_id']}~~~{$match_data['match_start']}~~~{$match_data['match_name']}~~~{$match_data['match_group']}~~~$team_A ($fullA)~~~$team_B ($fullB)";
}

echo $match_list;
/*
 * End of file ajax_match_list.php
 */