<?php

/**
 * Match list for sports module (Tennis)
 *
 * @author Thilo Muller (2011)
 * @version $Id: match_list.php 87 2011-09-14 06:45:49Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
include_once SITE_ROOT . "/shared/terms.php";
include 'toolbar.php';



//Vars
$template = file_get_contents('html/match_list.html');

//Replace Tags
$template = str_replace('<!-- terms -->', $terms, $template);

echo $template;

?>
