<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



//Vars
$window_function = isset($_GET['f'])?$_GET['f']:'';

//Get page
switch ($window_function) {
	case 'match_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/hockey/match_list.php';
		break;
	case 'team_list':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/hockey/team_list.php';
		break;
	case 'sports_report':
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/hockey/report.php';
		break;

    case 'import_results':
        include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/hockey/import.php';
        break;
	default:
		include $_SERVER['DOCUMENT_ROOT'] . '/modules/sports/s/hockey/match_list.php';
}


//Includes
include 'toolbar.php';
?>
