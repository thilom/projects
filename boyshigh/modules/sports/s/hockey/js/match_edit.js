var teamSide;

/**
 * Initialize page
 */
function init() {
    document.onkeyup = handleEvent;
    document.onclick = handleEvent;
    document.onchange = handleEvent;
}

/**
 * Event Handler
 */
function handleEvent(e) {
	var eSrc;
	if (window.event) {
		e = window.event;
		eSrc = e.srcElement;
	} else {
		eSrc = e.target;
	}
	if (e.type == 'keyup') {
		switch(eSrc.id) {
			case 'teamB':
			case 'teamA':
			case 'playerB1':
			case 'playerB2':
			case 'playerA1':
			case 'playerA2':

				drawPlayers();
				break;
			case 'set1A':
			case 'set2A':
			case 'set3A':
			case 'set4A':
			case 'set5A':
			case 'set1B':
			case 'set2B':
			case 'set3B':
			case 'set4B':
			case 'set5B':
				calcResults();
				break;
		}
	}

	if (e.type == 'click') {
		switch(eSrc.id) {
			case 'teamASelect':
				openTeamSelect('A');
				break;
			case 'teamBSelect':
				openTeamSelect('B');
				break;
			case 'addTeamClose':
				closeTeamSelect();
				break;
			case 'addTeamButton':
				insertTeam();
				break;
		}
	}

	if (e.type == 'change') {
		switch (eSrc.id) {
			case 'teamID':
				populateTeamList(eSrc.value);
				break;
		}

	}

}

/**
 * Draws the player or teamname to results table
 */
function drawPlayers() {
//	var contentA, contentB;

	var teamA = document.getElementById('teamA').value;
	var teamB = document.getElementById('teamB').value;

//	matchName = teamA + ' vs ' + teamB


	document.getElementById('playersA').innerHTML = teamA;
	document.getElementById('playersB').innerHTML = teamB;
//	document.getElementById('matchName').value = matchName;

}


/**
 * Displays an error for result calculations
 */
function drawError(err) {
	err = err==''?'':"<b>WARNING!</b><br>" + err;
    document.getElementById('resultError').innerHTML = err;
}

/**
 * Opens the Add Team window
 */
function openTeamSelect(team) {

	var teamBox = document.getElementById('teamList');
	var elementPos = findPos(document.getElementById('team'+team+'Select'));

	teamBox.style.top = (elementPos[1] + 23 )+'px';

	if (team == 'B') {
		teamBox.style.left = (elementPos[0] - 310 ) + 'px';
	} else {
		teamBox.style.left = elementPos[0] + 'px';
	}


	teamBox.className = 'showList';
	teamSide = team;
}

/**
 * Close team select window
 */
function closeTeamSelect() {
   document.getElementById('teamList').className = 'hideList';
}


/**
 * Populates the team list for selection
 */
function populateTeamList(teamID, teamSide) {
	anchor = document.getElementById('selectionBox');

	//Clear selection list
	anchor.innerHTML = '';

    if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {

			if (xmlhttp.readyState==4 && xmlhttp.status==200) {

				//Text
				introDiv = document.createElement('DIV');
				introDiv.setAttribute('class', 'intro');
				intro = document.createTextNode('Select the sportsman to include in this match(optional)');
				hr = document.createElement('HR');
				introDiv.appendChild(intro);
				introDiv.appendChild(hr);
				anchor.appendChild(introDiv);


				al = xmlhttp.responseText;
				if (window.DOMParser) {
					  parser=new DOMParser();
					  xmlDoc=parser.parseFromString(al,"text/xml");
				} else  {
					  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
					  xmlDoc.async="false";
					  xmlDoc.loadXML(al);
				}
				el = xmlDoc.documentElement.childNodes;
				for (i = 0; i < el.length; i++) {
					memberName = el[i].childNodes[0].nodeValue;

					Chk = document.createElement('INPUT');
					Chk.setAttribute('type', 'checkbox');
					Chk.setAttribute('value', memberName);

					txt = document.createTextNode(memberName);
					br = document.createElement('BR');

					anchor.appendChild(Chk);
					anchor.appendChild(txt);
					anchor.appendChild(br);
				}



			}
		}

		matchUrl = "/modules/sports/s/tennis/ajax_team_members.php?team_id=" + teamID;

		xmlhttp.open("GET",matchUrl,true);
		xmlhttp.send();
}

/**
 * Inserts the selected teams and players
 */
function insertTeam(teamID, teamName) {

	document.getElementById('team' + teamSide).value = teamName;
	document.getElementById('team' + teamSide + 'ID').value = teamID;

	drawPlayers();
	closeTeamSelect();
}

/**
 * Close edit window
 */
function closeMatchEdit() {
		document.location = '/modules/sports/s/hockey/hockey.php?W=6&f=match_list';
}