/**
 * Get and replace the current CSS data with the defaults
 */
function get_css_default(areaID, file) {
	if (confirm('WARNING!\n\nThis process will replace the current data\n\nContinue?')) {
		if (window.XMLHttpRequest)  {
			xmlhttp=new XMLHttpRequest();
		} else {
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById('css_data').value = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","/modules/sports/s/hockey/edit/css_reload.php?area_id=" + areaID,false);
		xmlhttp.send();
	}
}



