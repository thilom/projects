<?php

/**
 *  Display sport results for Hockey/Rugby
 *
 *  @author Sigal Zahavi(2013)
 *  @version $Id$
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
include_once SITE_ROOT . "/shared/terms.php";

//Vars
$sports_results_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/sports/html/latest_sports_results.html');

//Get sports for the buttons
$statement = "SELECT value
				FROM {$GLOBALS['db_prefix']}_settings
				WHERE tag='enabled_sports'
				LIMIT 1";
$sql_list = $GLOBALS['dbCon']->prepare($statement);
$sql_list->execute();
$sql_list_data = $sql_list->fetch();
$sql_list->closeCursor();
$sports_list = explode(',', $sql_list_data['value']);

sort($sports_list);
$sport_type = $sports_list[0]; //Default first displayed results to first sport on the dropdown

if (!empty($sport_type)) {
    $sport_buttons = '<ul id="sportBtns">';
    foreach($sports_list as $sport)
    {
        $sport_name = ucwords($sport);

        //Removing underscore and making second word first letter uppercase
        foreach (array('_') as $delimiter) {
            if (strpos($sport_name, $delimiter)!==false) {
                $sport_name =implode($delimiter, array_map('ucfirst', explode($delimiter, $sport_name)));
            }
        }

        $sport_name = str_replace('_', ' ', $sport_name);
        $sport_buttons .= "<li><a href='/index.php?page={$sport}_results' class='btn'>$sport_name</a></li>";
    }
    $sport_buttons .= '</ul>';
} else {
    $sport_buttons = '';
}

$sports_results_template = str_replace('<!-- sport_buttons -->', $sport_buttons, $sports_results_template);

echo $sports_results_template;
/***
 * End of file sports_results.php
 */