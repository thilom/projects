<?php

/**
 * Get teams for dropdown lists for teamA and teamB
 *
 * @author Sigal Zahavi(2013)
 * @version $Id: get_teams.ajax.php 87 2013-08-06 Sigal $

*/

//Includes
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/shared/database_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';

//Vars
$sport_type = $_GET['sport_type'];
$term = trim(strip_tags($_GET['term']));//retrieve the search term that autocomplete sends

//If coming from sport results search there is no team side, if coming from match_edit there is a team side
if(isset($_GET['team_side']))
    $team_side = trim(strip_tags($_GET['team_side']));
else
    $team_side = '';

//Get Teams Dropdowns
if($team_side != '')
{
    $statement = "SELECT DISTINCT team_name, team_side
                        FROM {$GLOBALS['db_prefix']}_sports_match_team
                        WHERE team_name LIKE '%".$term."%'
                        AND team_side=:team_side
                        And sports_id=:sport_type
                        ";
    $sql_team = $GLOBALS['dbCon']->prepare($statement);
    $sql_team->bindParam(':sport_type', $sport_type);
    $sql_team->bindParam(':team_side', $team_side);
}
else
{
    $statement = "SELECT DISTINCT team_name, team_side
                        FROM {$GLOBALS['db_prefix']}_sports_match_team
                        WHERE team_name LIKE '%".$term."%'
                        And sports_id=:sport_type
                        ";
    $sql_team = $GLOBALS['dbCon']->prepare($statement);
    $sql_team->bindParam(':sport_type', $sport_type);
}

$sql_team->execute();
$sql_team_data = $sql_team->fetchAll();
$sql_team->closeCursor();

foreach ($sql_team_data as $team_data) {
    $team_options['teams'][] = array(
        'name' => $team_data['team_name'],
    );
}

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

echo json_encode($team_options);
/**
 * End of file: get_teams.ajax.php
*/