<?php
/*
 * Searching sports results by dates range or team name
 * Date: 12 Aug 2013 Programmer: Sigal Zahavi
 */
$ajax = 1;
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php';
//Vars
$sport_type = $_POST['sport_type'];
$results_table = $sport_type;//Sets which results table to look at
$search_by_date = FALSE;
$search_by_team = FALSE;
$from_date = '';
$to_date = '';
$team = '';
$sport_results = '';
$error = '';
$current_match_id = 0;

if($_POST['team'] == '' && $_POST['from_date'] == '' && $_POST['to_date'] == '')
    $error = "Please fill in the Team Name or the Dates you would like to search by.";

//Check if need to search by dates
if($_POST['from_date'] != '' || $_POST['to_date'] != '')
{
    $search_by_date = TRUE;
    $from_date = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['from_date'])));
    $to_date = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['to_date'])));

    //Form Validation
    if($from_date == '')
        $error = 'Please fill in the From Date field.';

    if($to_date == '')
        $error = 'Please fill in the To Date field.';

    if($from_date > $to_date)
        $error = 'From Date field cannot be bigger than the To Date field.';
}

//Check if need to search by team
if($_POST['team'] != '')
{
    $search_by_team = TRUE;
    $team = $_POST['team'];
}

//Errors were found
if($error)
{
    $results = array('error_found'=>1, 'error'=>$error);
}
else
{
    //Search by team and dates
    if($search_by_date && $search_by_team)
    {
        if($from_date == $to_date)
        {
            //Get all the matches that the searched for team is in
            $statement = "SELECT DISTINCT a.match_id, match_name, match_start, team_name, team_side
                            FROM modularus_sports_match_team a
                            LEFT JOIN modularus_sports_match b ON a.match_id = b.match_id
                            WHERE team_name=:team AND sports_id=:sport_type AND match_start=:from_date
                         ";

            $sql_team = $GLOBALS['dbCon']->prepare($statement);
            $sql_team->bindParam(':sport_type', $sport_type);
            $sql_team->bindParam(':from_date', $from_date);
            $sql_team->bindParam(':team', $team);
        }
        else
        {
            //Get all the matches that the searched for team is in
            $statement = "SELECT DISTINCT a.match_id, match_name, match_start, team_name, team_side
                            FROM modularus_sports_match_team a
                            LEFT JOIN modularus_sports_match b ON a.match_id = b.match_id
                            WHERE team_name=:team AND sports_id=:sport_type AND match_start BETWEEN :from_date AND :to_date
                         ";

            $sql_team = $GLOBALS['dbCon']->prepare($statement);
            $sql_team->bindParam(':sport_type', $sport_type);
            $sql_team->bindParam(':from_date', $from_date);
            $sql_team->bindParam(':to_date', $to_date);
            $sql_team->bindParam(':team', $team);
        }

        $sql_team->execute();
        $sql_team_data = $sql_team->fetchAll();
        $sql_team->closeCursor();

        if(count($sql_team_data))
        {
            foreach($sql_team_data as $team_data)
            {
                $match_id = $team_data['match_id'];
                if($team_data['match_name'])
                    $match_name = $team_data['match_name'];
                else
                    $match_name = '';
                $match_date = $team_data['match_start'];
                $date = new DateTime($match_date);
                $match_date = $date->format('d/m/Y');
                $team_side = $team_data['team_side'];

                if($current_match_id != $match_id)
                {
                    $results[$match_id] = array(
                        'match'=>$match_name,
                        'date'=>$match_date,
                        'id'=>$match_id
                    );
                    $current_match_id = $match_id;
                }

                //Getting the teams names
                //The team searched for is side A we need to get team B name
                if($team_side == 'A')
                {
                    $results[$match_id]['team_a'] = $team;
                    $team_side_b = 'B';

                    //Get team B name
                    $statement = "SELECT team_name
                                  FROM modularus_sports_match_team
                                  WHERE match_id=:match_id AND team_side=:team_side
                                 ";
                    $sql_team_b = $GLOBALS['dbCon']->prepare($statement);
                    $sql_team_b->bindParam(':match_id', $match_id);
                    $sql_team_b->bindParam(':team_side', $team_side_b);
                    $sql_team_b->execute();
                    $sql_team_b_data = $sql_team_b->fetch();
                    $sql_team_b->closeCursor();

                    $results[$match_id]['team_b'] = $sql_team_b_data['team_name'];
                }
                //The team searched for is side B we need to get team A name
                else
                {
                    $results[$match_id]['team_b'] = $team;
                    $team_side_a = 'A';

                    //Get team A name
                    $statement = "SELECT team_name
                                  FROM modularus_sports_match_teams
                                  WHERE match_id=:match_id AND team_side=:team_side
                                 ";
                    $sql_team_a = $GLOBALS['dbCon']->prepare($statement);
                    $sql_team_a->bindParam(':match_id', $match_id);
                    $sql_team_a->bindParam(':team_side', $team_side_a);
                    $sql_team_a->execute();
                    $sql_team_a_data = $sql_team_a->fetch();
                    $sql_team_a->closeCursor();

                    $results[$match_id]['team_a'] = $sql_team_a_data['team_name'];
                }
            }

            //Getting the results
            foreach($results as $result)
            {
                $match_id = $result['id'];

                //Get the results
                $statement = "SELECT result_type, result_value
                                  FROM modularus_sports_match_result_$results_table
                                  WHERE match_id=:match_id
                                 ";
                $sql_result = $GLOBALS['dbCon']->prepare($statement);
                $sql_result->bindParam(':match_id', $match_id);
                $sql_result->execute();
                $sql_result_data = $sql_result->fetchAll();
                $sql_result->closeCursor();

                foreach ($sql_result_data as $result_data)
                {
                    switch($result_data['result_type'])
                    {
                        case 'halfA':
                            $results[$match_id]['half_a'] = $result_data['result_value'];
                            break;
                        case 'fullA':
                            $results[$match_id]['final_a'] = $result_data['result_value'];
                            break;
                        case 'halfB':
                            $results[$match_id]['half_b'] = $result_data['result_value'];
                            break;
                        case 'fullB':
                            $results[$match_id]['final_b'] = $result_data['result_value'];
                            break;
                    }
                }
            }
        }
        else
        {
            $results = array('no_results'=>1);
        }
    }
    //Search by dates only
    elseif($search_by_date)
    {
        if($from_date != $to_date)
        {
            $statement = "SELECT a.match_id, a. result_type, a.result_value, b.match_start, b.match_name, b.match_id,
                            IF(a.result_type='fullA' OR a.result_type='halfA',
                            (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='A' LIMIT 1),
                            (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='B' LIMIT 1) ) AS team
                            FROM modularus_sports_match_result_$results_table AS a
                            LEFT JOIN modularus_sports_match AS b ON a.match_id=b.match_id
                            WHERE b.sport_id=:sport_type AND b.match_start BETWEEN :from_date AND :to_date
                          ";

            $sql_result = $GLOBALS['dbCon']->prepare($statement);
            $sql_result->bindParam(':sport_type', $sport_type);
            $sql_result->bindParam(':from_date', $from_date);
            $sql_result->bindParam(':to_date', $to_date);
        }
        else
        {
            $statement = "SELECT a.match_id, a. result_type, a.result_value, b.match_start, b.match_name, b.match_id,
                            IF(a.result_type='fullA' OR a.result_type='halfA',
                            (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='A' LIMIT 1),
                            (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='B' LIMIT 1) ) AS team
                            FROM modularus_sports_match_result_$results_table AS a
                            LEFT JOIN modularus_sports_match AS b ON a.match_id=b.match_id
                            WHERE b.sport_id=:sport_type AND b.match_start=:from_date
                          ";

            $sql_result = $GLOBALS['dbCon']->prepare($statement);
            $sql_result->bindParam(':sport_type', $sport_type);
            $sql_result->bindParam(':from_date', $from_date);
        }

        $sql_result->execute();
        $sql_result_data = $sql_result->fetchAll();
        $sql_result->closeCursor();

        //Found matching records
        if(count($sql_result_data))
        {
            foreach($sql_result_data as $result_data)
            {
                $match_id = $result_data['match_id'];
                if($result_data['match_name'])
                    $match_name = $result_data['match_name'];
                else
                    $match_name = '';
                $match_date = $result_data['match_start'];
                $date = new DateTime($match_date);
                $match_date = $date->format('d/m/Y');

                if($current_match_id != $match_id)
                {
                    $results[$match_id] = array(
                        'match'=>$match_name,
                        'date'=>$match_date,
                        'id'=>$match_id
                    );
                    $current_match_id = $match_id;
                }

                switch($result_data['result_type'])
                {
                    case 'halfA':
                        $results[$match_id]['team_a'] = $result_data['team'];
                        $results[$match_id]['half_a'] = $result_data['result_value'];
                        break;
                    case 'fullA':
                        $results[$match_id]['final_a'] = $result_data['result_value'];
                        break;
                    case 'halfB':
                        $results[$match_id]['team_b'] = $result_data['team'];
                        $results[$match_id]['half_b'] = $result_data['result_value'];
                        break;
                    case 'fullB':
                        $results[$match_id]['final_b'] = $result_data['result_value'];
                        break;
                }
            }
        }
        else
        {
            $results = array('no_results'=>1);
        }
    }
    //Search by team only
    else
    {
        //Get all the matches that the searched for team is in
        $statement = "SELECT DISTINCT a.match_id, match_name, match_start, team_name, team_side
                        FROM modularus_sports_match_team a
                        LEFT JOIN modularus_sports_match b ON a.match_id = b.match_id
                        WHERE team_name=:team AND sports_id=:sport_type
                     ";

        $sql_team = $GLOBALS['dbCon']->prepare($statement);
        $sql_team->bindParam(':sport_type', $sport_type);
        $sql_team->bindParam(':team', $team);
        $sql_team->execute();
        $sql_team_data = $sql_team->fetchAll();
        $sql_team->closeCursor();

        //Found matching records
        if(count($sql_team_data))
        {
            foreach($sql_team_data as $team_data)
            {
                $match_id = $team_data['match_id'];
                if($team_data['match_name'])
                    $match_name = $team_data['match_name'];
                else
                    $match_name = '';
                $match_date = $team_data['match_start'];
                $date = new DateTime($match_date);
                $match_date = $date->format('d/m/Y');
                $team_side = $team_data['team_side'];

                if($current_match_id != $match_id)
                {
                    $results[$match_id] = array(
                        'match'=>$match_name,
                        'date'=>$match_date,
                        'id'=>$match_id
                    );
                    $current_match_id = $match_id;
                }

                //Getting the teams names
                //The team searched for is side A we need to get team B name
                if($team_side == 'A')
                {
                    $results[$match_id]['team_a'] = $team;
                    $team_side_b = 'B';

                    //Get team B name
                    $statement = "SELECT team_name
                                  FROM modularus_sports_match_team
                                  WHERE match_id=:match_id AND team_side=:team_side
                                 ";
                    $sql_team_b = $GLOBALS['dbCon']->prepare($statement);
                    $sql_team_b->bindParam(':match_id', $match_id);
                    $sql_team_b->bindParam(':team_side', $team_side_b);
                    $sql_team_b->execute();
                    $sql_team_b_data = $sql_team_b->fetch();
                    $sql_team_b->closeCursor();

                    $results[$match_id]['team_b'] = $sql_team_b_data['team_name'];
                }
                //The team searched for is side B we need to get team A name
                else
                {
                    $results[$match_id]['team_b'] = $team;
                    $team_side_a = 'A';

                    //Get team A name
                    $statement = "SELECT team_name
                                  FROM modularus_sports_match_team
                                  WHERE match_id=:match_id AND team_side=:team_side
                                 ";
                    $sql_team_a = $GLOBALS['dbCon']->prepare($statement);
                    $sql_team_a->bindParam(':match_id', $match_id);
                    $sql_team_a->bindParam(':team_side', $team_side_a);
                    $sql_team_a->execute();
                    $sql_team_a_data = $sql_team_a->fetch();
                    $sql_team_a->closeCursor();

                    $results[$match_id]['team_a'] = $sql_team_a_data['team_name'];
                }
            }

            //Getting the results
            foreach($results as $result)
            {
                $match_id = $result['id'];

                //Get the results
                $statement = "SELECT result_type, result_value
                                  FROM modularus_sports_match_result_$results_table
                                  WHERE match_id=:match_id
                                 ";
                $sql_result = $GLOBALS['dbCon']->prepare($statement);
                $sql_result->bindParam(':match_id', $match_id);
                $sql_result->execute();
                $sql_result_data = $sql_result->fetchAll();
                $sql_result->closeCursor();

                foreach ($sql_result_data as $result_data)
                {
                    switch($result_data['result_type'])
                    {
                        case 'halfA':
                            $results[$match_id]['half_a'] = $result_data['result_value'];
                            break;
                        case 'fullA':
                            $results[$match_id]['final_a'] = $result_data['result_value'];
                            break;
                        case 'halfB':
                            $results[$match_id]['half_b'] = $result_data['result_value'];
                            break;
                        case 'fullB':
                            $results[$match_id]['final_b'] = $result_data['result_value'];
                            break;
                    }
                }
            }
        }
        else
        {
            $results = array('no_results'=>1);
        }
    }
}

echo json_encode($results);
/***
 * End of file sports_results.ajax.php
 */