<?php

/**
 *  Display sport results for sports
 *
 *  @author Sigal Zahavi(2013)
 *  @version $Id$
 */



//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
include_once SITE_ROOT . "/shared/terms.php";
//Get Preferences
$pref_data = get_area_data($id, 'latest', array('preferences'));

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

$preferences = expand_preferences($pref_data['data']['preferences']);
if(isset($preferences['sport_type']))
{
   $sport_type = $preferences['sport_type'];
}
else
{
    echo 'The sport type has not been completed for this page and is set to Rugby by default.';
    $sport_type = 'rugby';
}

//Vars
$sports_results_template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/sports/html/sports_results.html');

$sport_results = '';
$team_a = '';
$team_b = '';
$team_a_half = '';
$team_b_half = '';
$team_a_final = '';
$team_b_final = '';
$current_match_id = 0;

//Get results
if($sport_type != 'cricket')
{
    $statement = "SELECT a.match_id, a. result_type, a.result_value, b.match_start, b.match_name,
                IF(a.result_type='fullA', (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='A' AND sports_id=:sport_type LIMIT 1), (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='B' AND sports_id=:sport_type  LIMIT 1) ) AS team
                FROM modularus_sports_match_result_{$sport_type} AS a
                LEFT JOIN modularus_sports_match AS b ON a.match_id = b.match_id
                WHERE b.sport_id=:sport_type
                	AND (a.result_type = 'fullA' OR a.result_type='fullB')
                ORDER BY b.match_start DESC
                LIMIT 80";

}
else
{
    $statement = "SELECT a.match_id, a. result_type, a.result_runs_value, a.result_wickets_value, b.match_start, b.match_name,
                IF(a.result_type='fullA', (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='A' AND sports_id=:sport_type LIMIT 1), (SELECT team_name FROM modularus_sports_match_team WHERE match_id=a.match_id AND team_side='B' AND sports_id=:sport_type  LIMIT 1) ) AS team
                FROM modularus_sports_match_result_{$sport_type} AS a
                LEFT JOIN modularus_sports_match AS b ON a.match_id = b.match_id
                WHERE b.sport_id=:sport_type
                	AND (a.result_type = 'fullA' OR a.result_type='fullB')
                ORDER BY b.match_start DESC
                LIMIT 80";
}
$sql_result = $GLOBALS['dbCon']->prepare($statement);
$sql_result->bindParam(':sport_type', $sport_type);
$sql_result->execute();
$sql_result_data = $sql_result->fetchAll();
$sql_result->closeCursor();

foreach($sql_result_data as $result_data) {
    $match_id = $result_data['match_id'];
    $match_name = $result_data['match_name'];
    $match_date = $result_data['match_start'];
    $date = new DateTime($match_date);
    $match_date = $date->format('d/m/Y');

    if($current_match_id != $match_id && !isset($results[$match_id]))
    {
        $results[$match_id] = array(
            'match'=>$match_name,
            'date'=>$match_date,
            'id'=>$match_id
        );
        $current_match_id = $match_id;
    }

    switch($result_data['result_type'])
    {
        case 'halfA':
            $results[$match_id]['team_a'] = $result_data['team'];
            if($sport_type != 'cricket')
            {
                $results[$match_id]['half_a'] = $result_data['result_value'];
            }
            else
            {
                //Check if the match has the free text value
                $statement = "SELECT result_text
                                FROM FROM modularus_sports_result_text_cricket
                                WHERE match_id = :match_id";
                $sql = $GLOBALS['dbCon']->prepare($statement);
                $sql->bindParam(':match_id', $match_id);
                $sql->execute();
                $result_text = $sql_result->fetchColumn();
                $sql->closeCursor();
                $results[$match_id]['half_text'] = $result_text;
                $results[$match_id]['team_a_runs_halfA'] = $result_data['result_runs_value'];
                $results[$match_id]['half_a'] = $results[$match_id]['team_a_runs_halfA'] . '-' . $result_data['result_wickets_value'];
            }
            break;
        case 'fullA':
			$results[$match_id]['team_a'] = $result_data['team'];
            if($sport_type != 'cricket')
            {
                $results[$match_id]['final_a'] = $result_data['result_value'];
            }
            else
            {
                $results[$match_id]['team_a_runs_fullA'] = $result_data['result_runs_value'];
                $results[$match_id]['final_a'] = $results[$match_id]['team_a_runs_fullA'] . '-' . $result_data['result_wickets_value'];
                //Check if the match has the free text value
                $statement = "SELECT result_text
                                FROM modularus_sports_result_text_cricket
                                WHERE match_id = :match_id";
                $sql = $GLOBALS['dbCon']->prepare($statement);
                $sql->bindParam(':match_id', $match_id);
                $sql->execute();
                $result_text = $sql->fetchColumn();
                $sql->closeCursor();
                $results[$match_id]['final_text'] = $result_text;
            }
            break;
        case 'halfB':
            $results[$match_id]['team_b'] = $result_data['team'];
            if($sport_type != 'cricket')
            {
                $results[$match_id]['half_b'] = $result_data['result_value'];
            }
            else
            {
                $results[$match_id]['team_b_runs_halfB'] = $result_data['result_runs_value'];
                $results[$match_id]['half_b'] = $results[$match_id]['team_b_runs_halfB'] . '-' . $result_data['result_wickets_value'];
            }
            break;
        case 'fullB':
			$results[$match_id]['team_b'] = $result_data['team'];
            if($sport_type != 'cricket')
            {
                $results[$match_id]['final_b'] = $result_data['result_value'];
            }
            else
            {
                $results[$match_id]['team_b_runs_fullB'] = $result_data['result_runs_value'];
                $results[$match_id]['final_b'] = $results[$match_id]['team_b_runs_fullB'] . '-' . $result_data['result_wickets_value'];
            }
            break;
    }
}

if (!empty($results)) {
	foreach($results as $result) {
		$match_name = $result['match'];
		$match_date = $result['date'];
		if (!isset($result['team_a'])) $result['team_a'] = '';
		if (!isset($result['team_b'])) $result['team_b'] = '';
        if($sport_type != 'cricket')
        {
            if (!isset($result['half_a'])) $result['half_a'] = '0';
            if (!isset($result['half_b'])) $result['half_b'] = '0';
            if (!isset($result['final_a'])) $result['final_a'] = '0';
            if (!isset($result['final_b'])) $result['final_b'] = '0';
        }
        else
        {
            if (!isset($result['half_a'])) $result['half_a'] = '0-0';
            if (!isset($result['half_b'])) $result['half_b'] = '0-0';
            if (!isset($result['final_a'])) $result['final_a'] = '0-0';
            if (!isset($result['final_b'])) $result['final_b'] = '0-0';
        }

		$sport_results .= "<tr>\n
								   <td rowspan=\"3\" style=\"border-bottom:3px solid #006400\">\n
										<b>{$match_name}</b><br>
										{$match_date}
									</td>\n
								</tr>\n
								<tr>\n
									<td style=\"border-top:none\">{$result['team_a']}</td>
									<td style=\"border-top:none\">{$result['final_a']}</td>
								</tr>\n
								<tr>\n
									<td style=\"border-bottom:3px solid #006400\">{$result['team_b']}</td>
									<td style=\"border-bottom:3px solid #006400\">{$result['final_b']}</td>
								</tr>\n
								";
        if($sport_type == 'cricket')
        {
            if($result['final_a'] != 0 || $result['final_b'] != 0)
            {
                $winning_team = $result['team_a_runs_fullA'] > $result['team_b_runs_fullB'] ? $result['team_a'] : $result['team_b'];
                $diff = $result['team_a_runs_fullA'] > $result['team_b_runs_fullB'] ? $result['team_a_runs_fullA'] - $result['team_b_runs_fullB'] : $result['team_b_runs_fullB'] - $result['team_a_runs_fullA'];
                if($diff != 0)
                    $message = "$winning_team won by $diff runs";
                else
                    $message = "The match ended in a tie";
                $sport_results .= "<tr>\n
                                    <td></td>
                                    <td colspan='2'>$message</td>\n
                                    </tr>";
                if(!empty($result['final_text']))
                    $sport_results .= "<tr>\n
                                        <td colspan='3'>{$result['final_text']}</td>\n
                                        </tr>\n";
            }
        }
	}
}
else
{
    $sport_results .= "<tr>\n
                        <td colspan='3'>No results where found</td>\n
                        </tr>";
}

$sports_results_template = str_replace('<!-- sport_type -->', $sport_type, $sports_results_template);
$sports_results_template = str_replace('<!-- sport_results -->', $sport_results, $sports_results_template);

echo $sports_results_template;
/***
 * End of file sports_results.php
 */