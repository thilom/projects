<?php
//Constants
if (!defined('SETTINGS_FORM')) define('SETTINGS_FORM', 'html/seo_settings.html');
$form_vars = array('google_verification_id', 'google_analytics', 'meta_title', 'meta_description', 'meta_keywords');

//Includes
include_once 'module.ini.php';

//Map
if (isset($_POST['saveForm'])) {
	save_form();
} else {
	display_form();
}

function display_form() {
	//Get Template
	$template = file_get_contents(SETTINGS_FORM);
	
	//Get Data & Replace Tags
	foreach ($GLOBALS['form_vars'] as $vars) {
		$SQL_statement = "SELECT value FROM dbweb_settings WHERE tag='$vars'";
		if (($SQL_val =  mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
		list($val) = mysql_fetch_array($SQL_val);
		$template = str_replace("<!-- $vars -->", $val, $template);
	}	
	
	//Serve
	echo $template;
	
}


function save_form() {
	//FTP Class
	include_once SITE_ROOT . '/shared/file_man.class.php';
	$fle = new file_man(); 
	
	//Get current Google ID
	$SQL_statement = "SELECT value FROM dbweb_settings WHERE tag='google_verification_id'";
	if (($SQL_old_id =  mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	list($old_id) = mysql_fetch_array($SQL_old_id);
	
	//Check if google file already exists
	if ($old_id != $_POST['google_verification_id']) {
		if (is_file(SITE_ROOT . "/$old_id")) unlink(SITE_ROOT . "/$old_id");
		$fle->create_file('/', $_POST['google_verification_id'] . ".html" );
	}
	
	foreach ($GLOBALS['form_vars'] as $var) {
		//Check if entry exists
		$SQL_statement = "SELECT * FROM dbweb_settings WHERE tag='$var'";
		if (($SQL_result = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
		$count = mysql_num_rows($SQL_result);
		
		$val = mysql_real_escape_string($_POST[$var]);
		if ($count == 0) {
			$SQL_statement = "INSERT INTO dbweb_settings(tag, value) VALUES ('$var', '$val')";
		} else {
			$SQL_statement = "UPDATE dbweb_settings SET value='$val' WHERE tag='$var'" ;
		}
		if ((mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	}

	display_form();
}
?>