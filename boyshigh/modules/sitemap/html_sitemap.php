<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] .'/settings/init.php';
include_once SITE_ROOT.'/modules/sitemap/module.ini.php';

if (isset($_POST['updateHTML'])) {
	update_map();
} else {
	display_form();
}

function display_form() {	
	//Initialize Variables
	$dir = SITE_ROOT . "/modules";
	$links = array();
	$table_exists = FALSE;
	$nLine = '';
	$lTemplate = '';
	
	 //Get template
	$template = file_get_contents(HTML_SITEMAP);
	
	//Get line template
	$template = str_replace(array("\n", "\r", "\t"), '', $template);
	preg_match_all('@<!-- line_start -->(.*)<!-- line_end -->@i', $template, $match);
	$lTemplate = $match[1][0];
	
	//Get a list of pages for each module. Add links to the links[] array.
	if ($dh = opendir($dir)) {
        while (($nDir = readdir($dh)) !== false) {
            if (is_file($dir."/$nDir/sitemap.php") && $nDir != '.' && $nDir != '..') {
            	include_once($dir."/$nDir/sitemap.php");
            }
        }
        closedir($dh);
    }

    //Check DB exists and create
    $SQL_statement = "show tables";
    if (($SQL_tables = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
    while(list($table) = mysql_fetch_array($SQL_tables)) {
    	if ($table == 'sitemap_data') $table_exists = TRUE;
    }
    if (!$table_exists) { //Create table
    	$SQL_statement = "CREATE TABLE sitemap_data (
    											link_name VARCHAR(100),
    											link	VARCHAR(100),
    											group_id 	INTEGER(5),
    											PRIMARY KEY (link_name)
    											)";
    	if ((mysql_query($SQL_statement)) == FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
    }
    
    //Save to DB
    foreach ($links as $id=>$link) {
    	//Check if entry already exists
    	$SQL_statement = "SELECT link FROM sitemap_data WHERE link_name='{$link['title']}'";
    	if (($SQL_res = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
    	$SQL_statement = '';
    	if (mysql_num_rows($SQL_res) == 0) {
    		$SQL_statement = "INSERT INTO sitemap_data (link_name, link) VALUES ('{$link['title']}', '{$link['link']}')";
    	} else {
    		list($l) = mysql_fetch_array($SQL_res);
    		if ($l != $link['link']) {
    			$SQL_statement = "UPDATE sitemap_data SET link='{$link['link']}' WHERE link_name='{$link['title']}'";
    		}
    	}
    	if (!empty($SQL_statement)) {
    		if ((mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
    	}    	
    }
    
    //Display List
    $SQL_statement = "SELECT a.link_name, a.group_id, b.page_title , a.link
    										FROM sitemap_data AS a
    										JOIN dbweb_pages AS b ON b.page_name=a.link_name
    										ORDER BY group_id";
    if (($SQL_list = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
    while(list($name, $group, $title, $l) = mysql_fetch_array($SQL_list)) {
    	if (empty($title)) {
    		$line = str_replace('<!-- name -->', $name, $lTemplate);
    	} else {
    		$line = str_replace('<!-- name -->', $title, $lTemplate);
    	}
    	
    	$line = str_replace('<!-- link -->', $l, $line);
    	$nLine .= $line;
    }
   
    
    //Replace Tags
    $template = preg_replace('@<!-- line_start -->(.*)<!-- line_end -->@i', $nLine, $template);

	
	//Server
	echo $template;
}


function update_map() {

	
	//Get template
	$template = file_get_contents(HTML_SITEMAP_RESULT);
	
	//Server
	echo $template;
}
?>