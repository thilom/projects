<?php

//Includes
include_once $_SERVER['DOCUMENT_ROOT'] .'/settings/init.php';
include_once SITE_ROOT.'/modules/sitemap/module.ini.php';

display_form();

function display_form() {
	//Initialize Variables
		$dir = SITE_ROOT . "/modules";
		$links = array();
		$table_exists = FALSE;
		$nLine = '';
		$lTemplate = '';
		$sitemap_length = 100;
		$page_count = 0;
		$line_counter = 0;
		$file_prefix = "google_sitemap_";
		$site_url = $_SERVER['SERVER_NAME'];
		
		//Classes
		include_once '../../shared/file_man.class.php';
	    $fm = new file_man();
		
		 //Get template
		$template = file_get_contents(GOOGLE_SITEMAP);
		$template = str_replace(array("\n","\r","\t"), '', $template);
		preg_match_all('@<!-- line_start -->(.*)<!-- line_end -->@i', $template, $match);
		$lTemplate = $match[1][0];
		
		//Get a list of pages for each module. Add links to the links[] array.
		if ($dh = opendir($dir)) {
	        while (($nDir = readdir($dh)) !== false) {
	            if (is_file($dir."/$nDir/sitemap.php") && $nDir != '.' && $nDir != '..') {
	            	include_once($dir."/$nDir/sitemap.php");
	            }
	        }
	        closedir($dh);
	    }
	
	    //Check DB exists and create
	    $SQL_statement = "show tables";
	    if (($SQL_tables = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	    while(list($table) = mysql_fetch_array($SQL_tables)) {
	    	if ($table == 'sitemap_data') $table_exists = TRUE;
	    }
	    if (!$table_exists) { //Create table
	    	$SQL_statement = "CREATE TABLE sitemap_data (
	    											link_name VARCHAR(100),
	    											link	VARCHAR(100),
	    											group_id 	INTEGER(5),
	    											PRIMARY KEY (link_name)
	    											)";
	    	if ((mysql_query($SQL_statement)) == FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	    }
	    
	    //Save to DB
	    foreach ($links as $id=>$link) {
	    	//Check if entry already exists
	    	$SQL_statement = "SELECT link FROM sitemap_data WHERE link_name='{$link['title']}'";
	    	if (($SQL_res = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	    	$SQL_statement = '';
	    	if (mysql_num_rows($SQL_res) == 0) {
	    		$SQL_statement = "INSERT INTO sitemap_data (link_name, link) VALUES ('{$link['title']}', '{$link['link']}')";
	    	} else {
	    		list($l) = mysql_fetch_array($SQL_res);
	    		if ($l != $link['link']) {
	    			$SQL_statement = "UPDATE sitemap_data SET link='{$link['link']}' WHERE link_name='{$link['title']}'";
	    		}
	    	}
	    	if (!empty($SQL_statement)) {
	    		if ((mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	    	}    	
	    }
	    
	    //Create Sitemap
	    $SQL_statement = "SELECT a.link_name, a.group_id, b.page_title , a.link
	    										FROM sitemap_data AS a
	    										JOIN dbweb_pages AS b ON b.page_name=a.link_name
	    										ORDER BY group_id";
	    if (($SQL_list = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	    while(list($name, $group, $title, $l) = mysql_fetch_array($SQL_list)) {
		    if ($line_counter == $sitemap_length) {
		    		fwrite($nFile, '</urlset>'  . PHP_EOL);
		    		$fm->dbweb_chmod("/{$file_prefix}{$page_count}.xml", 0755);
		    		$results[$page_count]['length'] = $sitemap_length;
		    		$results[$page_count]['file'] = "{$file_prefix}{$page_count}.xml";
		    		
		    		$line_counter = 0;
		    		$page_count++;
		    	}
		    	if ($line_counter == 0) { 
		    		//Check if file exists
		    		if (!is_file(SITE_ROOT . "/{$file_prefix}{$page_count}.xml")) {
		    			$fm->create_file("/", "{$file_prefix}{$page_count}.xml");
		    			$fm->dbweb_chmod("/{$file_prefix}{$page_count}.xml", 0777);
		    		}
		    		$nFile = fopen(SITE_ROOT . "/{$file_prefix}{$page_count}.xml", 'w');
		    		fwrite($nFile, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL);
		    		fwrite($nFile, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL);
		    	}
		    	fwrite($nFile, '<url>' . PHP_EOL);
		    	if (substr($l, 0, 1) == '/') $l = substr($l, 1);
		    	fwrite($nFile, "<loc>http://$site_url/$l</loc>" . PHP_EOL);
		    	$fTime = date('Y-m-d', filemtime (SITE_ROOT . "/$l"));
		    	fwrite($nFile, "<lastmod>$fTime</lastmod>" . PHP_EOL);
		    	fwrite($nFile, "<changefreq>monthly</changefreq>" . PHP_EOL);
		    	fwrite($nFile, "<priority>0.5</priority>" . PHP_EOL);
		    	fwrite($nFile, '</url>' . PHP_EOL);
	    	$line_counter++;
	    }
	   fwrite($nFile, '</urlset>'  . PHP_EOL);
	   	$results[$page_count]['length'] = $line_counter;
		$results[$page_count]['file'] = "{$file_prefix}{$page_count}.xml";
	    
	   	foreach ($results as $f) {
	   			$line = str_replace('<!-- sitemap_link -->', "/{$f['file']}", $lTemplate);
	   			$line = str_replace('<!-- file_name -->', $f['file'], $line);
	   			$line = str_replace('<!-- file_link -->', "http://$site_url/{$f['file']}", $line);
	   			$nLine .= $line;
	   	}
	   
	    //Replace Tags
	    $template = preg_replace('@<!-- line_start -->(.*)<!-- line_end -->@i', $nLine, $template);
	
		
		//Server
		echo $template;
}

?>