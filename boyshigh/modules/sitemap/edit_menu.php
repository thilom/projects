<?php
//Initialize variables
$tab_string = "";
$div_string = '';
$tab = array();

//Includes
include('module.ini.php');

//Get template
$template = file_get_contents(CONTENTS);

//Get div template
$template = str_replace(array("\r", "\n", "\t"), '', $template);
preg_match('/<!-- content_start -->(.+)<!-- content_end -->/i', $template, $match);
$div_template = $match[1];

//Create Tabs
$class_counter = 1;
foreach ($tab['sitemap'] as $tab_name=>$tab_link) {
	if ($class_counter == 1) {
		$class = '';
		$class_counter = 0;
	} else {
		$class = 'i';
	}
	$tab_string .= "<a href='#' onclick='CC(this, \"$tab_name\")' name='tab' class=Tm>$tab_name</a>";
	ob_start();
	include($tab_link);
	$div_content = ob_get_contents();
	ob_end_clean();
	$div = str_replace('<!-- content -->', $div_content, $div_template);
	$div = str_replace('<!-- content_id -->', $tab_name, $div);
	$div = str_replace('<!-- class -->', $class, $div);
	$div_string .= $div;
}
$tab_string .= "<a href='#' onclick='CC(this, \"Page_Preferences\")' name='tab' class=Tm>Page Preferences</a>";
ob_start();
include(SITE_ROOT . '/modules/content_manager/page_preferences.php');
$div_content = ob_get_contents();
ob_end_clean();
$div = str_replace('<!-- content -->', $div_content, $div_template);
$div = str_replace('<!-- content_id -->', 'Page_Preferences', $div);
$div = str_replace('<!-- class -->', 'i', $div);
$div_string .= $div;

//Replace Tags
$template = str_replace('<!-- tabs -->', $tab_string, $template);
$template = preg_replace('/<!-- content_start -->(.+)<!-- content_end -->/i', $div_string, $template);

//Serve
echo $template;

?>

