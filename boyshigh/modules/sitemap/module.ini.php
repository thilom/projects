<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Site Map";
$module_icon = '';
$module_link = 'sitemap.php';
$window_width = '';
$window_height = '';
$window_position = '';

//Settings for Module


//Tabs tab $tab[name] = 'link'
$tab['sitemap']['Area Preferences'] = 'prefs.php';

//Types for content areas $type[name] = ''module'
$type['Site map'] = 'sitemap';

//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Constants
if (!defined('CONTENTS')) define('CONTENTS', 'html/content_page.html');
if (!defined('HTML_SITEMAP')) define('HTML_SITEMAP', 'html/html_sitemap.html');
if (!defined('HTML_SITEMAP_RESULT')) define('HTML_SITEMAP_RESULT', 'html/html_sitemap_result.html');
if (!defined('GOOGLE_SITEMAP')) define('GOOGLE_SITEMAP', 'html/google_sitemap.html');

?>