<?php
include_once 'module.ini.php';

if (isset($_POST['savePrefs'])) {
	save_form();
} else {
	show_form();
}

function show_form() {
	global $content_types;
	
	//initialize variables
	$type_options = '<OPTION> </OPTION>';
	list($area_name, $id) = explode('_', $_GET['p']);
	
	$prefs = file_get_contents(PREFS);
	
	//Get current preferences
	$SQL_statement = "SELECT module FROM dbweb_page_contents WHERE page_id='$id' && content_area_name='$area_name'";
	//echo $SQL_statement;
	if (($SQL_module = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	list($current_module) = mysql_fetch_array($SQL_module);
	
	//Get Types
	foreach ($content_types as $type_name=>$module_name) {
		//echo "$current_module | $module_name";
		if ($current_module == $module_name) {
			$type_options .= "<OPTION value='$module_name' selected>$type_name</OPTION>";
		} else {
			$type_options .= "<OPTION value='$module_name'>$type_name</OPTION>";
		}
	}
	
	//Replace Tags
	$prefs = str_replace('<!-- types -->', $type_options, $prefs);
	
	echo $prefs;
}

function save_form() {
	//Initialize variables
	list($area_name, $id) = explode('_', $_GET['p']);
	$module = mysql_real_escape_string($_POST['area_type']);
	
	//Check if entry already exists
	$SQL_statement = "SELECT count(*) FROM dbweb_page_contents WHERE page_id='$id' && content_area_name='$area_name'";
	if (($SQL_count = mysql_query($SQL_statement)) == FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	list($count) = mysql_fetch_array($SQL_count);
	
	//Write to database
	if ($count == 0) {
		$SQL_statement = "INSERT INTO dbweb_page_contents (page_id, content_area_name, module) VALUES ('$id', '$area_name', '$module')";
	} else {
		$SQL_statement = "UPDATE dbweb_page_contents SET module='$module' WHERE  page_id='$id' && content_area_name='$area_name'";
	}
	if ((mysql_query($SQL_statement)) == FALSE) trigger_error(mysql_error(), E_USER_NOTICE);
	
	echo "<script>";
	echo "var P=window.parent";
	echo ";P.WR({$_GET['WR']})";
	echo ";P.WD({$_GET['W']})";
	echo "</script>";

}
?>