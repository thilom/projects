<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/23
 * Time: 12:03 PM
 */


//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once ($_SERVER['DOCUMENT_ROOT'] . '/modules/clothing/products.class.php');

//Vars
$productCLass = new Products();
$cat_list2 = '';

//Get list of categories
$cats = $productCLass->get_categories('');

foreach ($cats as $data) {
    $cat_list2 .= "<tr><td class='sublinks'><a href='/index.php?page=clothing&cat={$data['category_id']}' class='submenu'>{$data['category_name']}</a></td></tr>";
}

echo $cat_list2;
