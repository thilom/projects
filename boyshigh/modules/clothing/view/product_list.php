<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/23
 * Time: 12:41 PM
 */


//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once ($_SERVER['DOCUMENT_ROOT'] . '/modules/clothing/products.class.php');

//Vars
$productClass = new Products();
$category = isset($_GET['cat'])?$_GET['cat']:'1';
$product_list2 = '';

//Get products
$products = $productClass->get_products_display($category);

//Assemble products
foreach ($products as $key=>$data) {
    $product_list2 .= "<span style='color: white; font-size: 1.5em; font-weight: bold '>$key</span>";

    $product_list2 .= "<table style='width: 100%'>";
    $counter = 0;
    foreach ($data as $pData) {
        if ($counter == 0) {
            $line1 = '<tr>';
            $line2 = '<tr>';
        }

        $line1 .= "<td  style='text-align: center'>";
        $line2 .= "<td  style='text-align: center' class='productImage'>";


        $line1 .= "<div style='position: relative; left: 0; top: 0;'>";
        $line1 .= "<img class='pointer' src='/i/clothing/{$pData['image']}' onclick='openProduct({$pData['id']},\"{$pData['name']}\")' style='position: relative; top: 0; left: 0;' >";
         if ($pData['stock'] == 'N') {
             $line1 .= "<img src='/i/nostock.png'  style='position: absolute; top: -5px; left: 13px;'>";
         }
        $line1 .= "</div>";

        $line2 .= "<div  style='width: 200px; text-align: left; margin: auto auto; color: white'>{$pData['name']}<br>R{$pData['price']}</div>";

        $line1 .= "</td>";
        $line2 .= "</td>";

        if ($counter == 2) {
            $counter = 0;
            $product_list2 .= $line1 . "</tr>";
            $product_list2 .= $line2 . "</tr>";
            $product_list2 .= "<tr><td colspan='3' style='height: 40px'>&nbsp;</tr>";
        } else {
            $counter++;
        }
    }

    while ($counter <= 3 && $counter > 0) {
        if (empty($line1)) {
            break;
        }
        $line1 .= "<td></td>";
        $line2 .= "<td></td>";
        if ($counter == 2) {
            $product_list2 .= $line1 . "</tr>";
            $product_list2 .= $line2 . "</tr>";
            $product_list2 .= "<tr><td colspan='3' style='height: 40px'>&nbsp;</tr>";
        }
        $counter++;
    }

    $product_list2 .= "</table>";

}
echo $product_list2;
