<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/20
 * Time: 3:32 PM
 */

require_once '../../settings/init.php';
require_once 'products.class.php';

//Vars
$productClass = new Products();
$product_id = $_GET['id'];
$template = file_get_contents('html/edit_product.html');
$category = '<option value="" >-=Select a Category=-</option>';
$subcategory = '<option value="" >-=Select a Subcategory=-</option>';

//Get product info
$product = $productClass->get_product($product_id);
$product_stock = $product['product_stock']=='Y'?'':'checked';
$product_image = empty($product['product_image'])?'':"<img src='/i/clothing/{$product['product_thumb']}' style='width: 200px'>";

//Categories
$category_list = $productClass->get_categories('');
foreach ($category_list as $data) {
	if ($data['category_id'] == $product['category']) {
		$category .= "<option value='{$data['category_id']}' selected >{$data['category_name']}</option>";
	} else {
		$category .= "<option value='{$data['category_id']}'>{$data['category_name']}</option>";
	}
}

if (!empty($product['category'])) {
	$subcategory_list = $productClass->get_categories($product['category']);
	foreach ($subcategory_list as $data) {
		if ($data['category_id'] == $product['subcategory']) {
			$subcategory .= "<option value='{$data['category_id']}' selected >{$data['category_name']}</option>";
		} else {
			$subcategory .= "<option value='{$data['category_id']}'>{$data['category_name']}</option>";
		}
	}
}


//Replace tags
$template = str_replace('<!-- product_name -->', $product['product_name'], $template);
$template = str_replace('<!-- product_price -->', $product['product_price'], $template);
$template = str_replace('<!-- product_description -->', $product['product_description'], $template);
$template = str_replace('<!-- category -->', $category, $template);
$template = str_replace('<!-- subcategory -->', $subcategory, $template);
$template = str_replace('<!-- stock -->', $product_stock, $template);
$template = str_replace('<!-- product_image -->', $product_image, $template);
$template = str_replace('<!-- product_id -->', $product_id, $template);

echo $template;