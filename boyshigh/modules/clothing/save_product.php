<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/20
 * Time: 5:18 PM
 */



require_once '../../settings/init.php';
require_once 'products.class.php';
require_once '../../shared/Zebra-2.2.3/Zebra_Image.php';

//Vars
$productClass = new Products();
$image = new Zebra_Image();
$template = file_get_contents('html/save_product.html');
$_POST['product_stock'] = isset($_POST['outOfStock'])?'N':'Y';
$product_id = $_POST['product_id'];

//Image
if (!empty($_FILES['product_image']['name'])) {
	move_uploaded_file($_FILES['product_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/i/clothing/originals/' . $_FILES['product_image']['name']);
	$image->source_path = $_SERVER['DOCUMENT_ROOT'] . '/i/clothing/originals/' . $_FILES['product_image']['name'];
	$ext = substr($image->source_path, strrpos($image->source_path, '.') + 1);
	$image->target_path = $_SERVER['DOCUMENT_ROOT'] . '/i/clothing/800_' .  $_FILES['product_image']['name'];
	$image->preserve_aspect_ratio = true;
	$image->enlarge_smaller_images = false;
	$image->resize(800, 800, ZEBRA_IMAGE_NOT_BOXED);
	$_POST['image'] = '800_' .  $_FILES['product_image']['name'];
	$image->target_path = $_SERVER['DOCUMENT_ROOT'] . '/i/clothing/200_' .  $_FILES['product_image']['name'];
	$image->resize(200, 200, ZEBRA_IMAGE_NOT_BOXED);
	$_POST['thumb'] = '200_' .  $_FILES['product_image']['name'];
}



//Save product
$product_id = $productClass->update_product($_POST, $product_id);

echo $template;

