<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/21
 * Time: 8:42 AM
 */


require_once '../../../settings/init.php';
require_once '../products.class.php';

//Vars
$productClass = new Products();
$list = array('aaData'=>array());

//Get data
$categories = $productClass->get_categories('');

foreach ($categories AS $data) {

	$category_usage = $productClass->get_category_usage($data['category_id'], '');
	$delete_warn = $category_usage>0?'true':'false';

	$line = array();
	$line[] = $data['category_name'];
	$line[] = '-';
	$line[] = $category_usage;
	$line[] = "<button onclick=\"removeCategory({$data['category_id']},$delete_warn)\">Remove</button>";
	$line[] = "<button onclick=\"edit_category('". $data['category_name'] . "',{$data['category_id']})\" >Edit</button>";
	$list['aaData'][] = $line;

	$subcategories = $productClass->get_categories($data['category_id']);
	foreach ($subcategories as $sData) {
		$category_usage = $productClass->get_category_usage($data['category_id'], $sData['category_id']);
		$delete_warn = $category_usage>0?'true':'false';

		$line = array();
		$line[] = $data['category_name'];
		$line[] = $sData['category_name'];
		$line[] = $category_usage;
		$line[] = "<button onclick=\"removeCategory({$sData['category_id']},$delete_warn)\">Remove</button>";
		$line[] = "<button onclick=\"edit_subcategory('". $sData['category_name'] . "',{$sData['category_id']})\" >Edit</button>";
		$list['aaData'][] = $line;
	}
}

echo json_encode($list);
