<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/21
 * Time: 6:38 AM
 */


require_once '../../../settings/init.php';
require_once '../products.class.php';

//Vars
$productClass = new Products();
$category_name = $_GET['cat'];
$subcategory_name = isset($_GET['subcat'])?$_GET['subcat']:'';

$data['newCatID'] = $productClass->add_category($category_name, $subcategory_name);
$data['newCatName'] = !empty($subcategory_name)?$subcategory_name:$category_name;

echo json_encode($data);



