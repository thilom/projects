<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/20
 * Time: 11:41 AM
 */

//Includes
$ajax = 1;
require_once '../../../settings/init.php';
require_once '../products.class.php';

//Vars
$productClass = new Products();
$items = array('aaData'=>array());

//Assemble items
$products = $productClass->list_products();
foreach ($products as $data) {
	$line = array();
	$line[] = $data['product_name'];
	$line[] = $data['product_price'];
	$line[] = $data['category'];
	$line[] = $data['subcategory'];
	$line[] = "<button onclick=\"removeProduct({$data['product_id']})\">Remove</button>";
	$line[] = "<button onclick=\"document.location='/modules/clothing/edit_product.php?id={$data['product_id']}'\">Edit</button>";

	$items['aaData'][] = $line;
}

echo json_encode($items);