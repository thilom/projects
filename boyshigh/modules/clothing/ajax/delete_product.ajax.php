<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/21
 * Time: 8:25 AM
 */


require_once '../../../settings/init.php';
require_once '../products.class.php';

//Vars
$productClass = new Products();
$product_id = $_GET['id'];

//Remove product
$productClass->remove_product($product_id);