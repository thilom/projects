<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/21
 * Time: 9:44 AM
 */


require_once '../../../settings/init.php';
require_once '../products.class.php';

//Vars
$productClass = new Products();
$category_id = $_GET['id'];
$category_name = $_GET['cat'];

//save category
$productClass->update_category_name($category_id, $category_name);