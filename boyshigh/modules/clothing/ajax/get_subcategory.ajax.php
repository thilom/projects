<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/20
 * Time: 6:21 PM
 */


require_once '../../../settings/init.php';
require_once '../products.class.php';

//Vars
$productClass = new Products();
$category_id = $_GET['id'];
$subcategory = '<option value="" >-=Select a Subcategory=-</option>';

$subcategory_list = $productClass->get_categories($category_id);
foreach ($subcategory_list as $data) {
		$subcategory .= "<option value='{$data['category_id']}'>{$data['category_name']}</option>";
}

echo $subcategory;
