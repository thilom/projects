<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/24
 * Time: 7:49 AM
 */


//Includes
include_once $_SERVER['DOCUMENT_ROOT'] . '/settings/init.php' ;
require_once ($_SERVER['DOCUMENT_ROOT'] . '/modules/clothing/products.class.php');

//Vars
$productClass = new Products();
$product_id = $_GET['id'];
$template = file_get_contents('../html/product_dialog.html');

//Get data
$product = $productClass->get_product($product_id);

if ($product['product_stock'] == 'N') {
    $nostock = "<img src='/i/nostock.png'  style='position: absolute; top: 0px; left: 0px;'>";
} else {
    $nostock = '';
}

//Replace tags
$template = str_replace('<!-- image -->', $product['product_image'], $template);
$template = str_replace('<!-- description -->', $product['product_description'], $template);
$template = str_replace('<!-- price -->', $product['product_price'], $template);
$template = str_replace('<!-- nostock -->', $nostock, $template);

echo $template;
