<?php
/**
 * Created by PhpStorm.
 * User: thilo
 * Date: 2013/11/20
 * Time: 3:25 PM
 */

class Products {

	function list_products() {
		$statement = "SELECT a.product_id, a.product_name, a.product_price, b.category_name AS subcategory,
								c.category_name AS category
						FROM {$GLOBALS['db_prefix']}_clothing AS a
						LEFT JOIN {$GLOBALS['db_prefix']}_clothing_categories AS b ON a.product_category = b.category_id
						LEFT JOIN {$GLOBALS['db_prefix']}_clothing_categories AS c ON b.category_parent = c.category_id";
		$sql_products = $GLOBALS['dbCon']->prepare($statement);
		$sql_products->execute();
		$sql_products_data = $sql_products->fetchAll();
		$sql_products->closeCursor();

		return $sql_products_data;
	}

	function get_product($product_id) {
		$statement = "SELECT a.product_name, a.product_price, a.product_description, b.category_id AS subcategory,
								c.category_id AS category, a.product_stock, a.product_image, a.product_thumb
						FROM {$GLOBALS['db_prefix']}_clothing AS a
						LEFT JOIN {$GLOBALS['db_prefix']}_clothing_categories AS b ON a.product_category = b.category_id
						LEFT JOIN {$GLOBALS['db_prefix']}_clothing_categories AS c ON b.category_parent = c.category_id
						WHERE product_id = :product_id";
		$sql_product = $GLOBALS['dbCon']->prepare($statement);
		$sql_product->bindParam(':product_id', $product_id);
		$sql_product->execute();
		$sql_product_data = $sql_product->fetch();
		$sql_product->closeCursor();

		return $sql_product_data;
	}

	function update_product($product_data, $product_id='') {

		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_clothing
							(product_id, product_name, product_description, product_price, product_category, product_stock)
						VALUES
							(:product_id, :product_name, :product_description, :product_price, :product_category, :product_stock)
						ON DUPLICATE KEY UPDATE
							product_name = :product_name,
							product_description = :product_description,
							product_price = :product_price,
							product_category = :product_category,
							product_stock = :product_stock";
		$sql_save = $GLOBALS['dbCon']->prepare($statement);
		$sql_save->bindParam(':product_name', $product_data['product_name']);
		$sql_save->bindParam(':product_description', $product_data['description']);
		$sql_save->bindParam(':product_price', $product_data['product_price']);
		$sql_save->bindParam(':product_category', $product_data['subcategory']);
		$sql_save->bindParam(':product_stock', $product_data['product_stock']);
		$sql_save->bindParam(':product_id', $product_id);
		$sql_save->execute();

		if (empty($product_id)) {
			$product_id =  $GLOBALS['dbCon']->lastinsertid();
		}

		//Save image
		if (isset($product_data['image'])) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_clothing
							SET product_image = :product_image,
								product_thumb = :product_thumb
							WHERE product_id = :product_id";
			$sql_img = $GLOBALS['dbCon']->prepare($statement);
			$sql_img->bindParam(':product_id', $product_id);
			$sql_img->bindParam(':product_image', $product_data['image']);
			$sql_img->bindParam(':product_thumb', $product_data['thumb']);
			$sql_img->execute();
		}
	}

	function update_category($category_data, $subcategory_data) {

		//Category
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_clothing_categories
							(category_id, category_name)
						VALUES
							(:category_id, :category_name)
						ON DUPLICATE KEY UPDATE
							category_name = :category_name";
		$sql_cat = $GLOBALS['dbCon']->prepare($statement);
		$sql_cat->bindParam(':category_id', $category_data['id']);
		$sql_cat->bindParam(':category_name', $category_data['name']);
		$sql_cat->execute();

		if (empty($category_data['id'])) $category_data['id'] = $GLOBALS['dbCon']->lastinsertid();

		//Sub category
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_clothing_categories
							(category_id, category_name, category_parent)
						VALUES
							(:category_id, :category_name, :category_parent)
						ON DUPLICATE KEY UPDATE
							category_name = :category_name
							category_parent = :category_parent";
		$sql_cat = $GLOBALS['dbCon']->prepare($statement);
		$sql_cat->bindParam(':category_id', $subcategory_data['id']);
		$sql_cat->bindParam(':category_name', $subcategory_data['name']);
		$sql_cat->bindParam(':category_parent', $category_data['id']);
		$sql_cat->execute();


	}

	function update_category_name($category_id, $category_name) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_clothing_categories
						SET category_name = :category_name
						WHERE category_id = :category_id
						LIMIT 1";
		$sql_update = $GLOBALS['dbCon']->prepare($statement);
		$sql_update->bindParam(':category_id', $category_id);
		$sql_update->bindParam(':category_name', $category_name);
		$sql_update->execute();
	}

	function get_categories($parent_id) {

		$statement = "SELECT category_id, category_name
						FROM {$GLOBALS['db_prefix']}_clothing_categories
						WHERE ";
		if (!empty($parent_id)) {
			$statement .= "category_parent = :category_parent";
		} else {
			$statement .= "category_parent IS NULL OR category_parent = ''";
		}
		$sql_cat = $GLOBALS['dbCon']->prepare($statement);
		if (!empty($parent_id)) $sql_cat->bindParam(':category_parent', $parent_id);
		$sql_cat->execute();
		$sql_cat_data = $sql_cat->fetchAll();
		$sql_cat->closeCursor();

		return $sql_cat_data;

	}

	function get_category_usage($category_id, $subcategory_id) {
		if (empty($subcategory_id)) {
			$statement = "SELECT COUNT(*) AS product_count
							FROM {$GLOBALS['db_prefix']}_clothing AS a
							LEFT JOIN {$GLOBALS['db_prefix']}_clothing_categories AS b
								ON a.product_category = b.category_id
							WHERE b.category_parent = :category_parent";
			$sql_count = $GLOBALS['dbCon']->prepare($statement);
			$sql_count->bindParam(':category_parent', $category_id);
			$sql_count->execute();
			$sql_count_data = $sql_count->fetch();
			$sql_count->closeCursor();

			return $sql_count_data['product_count'];
		} else {
			$statement = "SELECT COUNT(*) AS product_count
							FROM {$GLOBALS['db_prefix']}_clothing AS a
							LEFT JOIN {$GLOBALS['db_prefix']}_clothing_categories AS b
								ON a.product_category = b.category_id
							WHERE product_category = :product_category";
			$sql_count = $GLOBALS['dbCon']->prepare($statement);
			$sql_count->bindParam(':product_category', $subcategory_id);
			$sql_count->execute();
			$sql_count_data = $sql_count->fetch();
			$sql_count->closeCursor();

			return $sql_count_data['product_count'];
		}
	}

	function add_category($category, $subcategory) {
		if (empty($subcategory)) {
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_clothing_categories
								(category_name)
							VALUES
								(:category_name)";
			$sql_add = $GLOBALS['dbCon']->prepare($statement);
			$sql_add->bindParam(':category_name', $category);
			$sql_add->execute();
		} else {
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_clothing_categories
								(category_name, category_parent)
							VALUES
								(:category_name, :category_parent)";
			$sql_add = $GLOBALS['dbCon']->prepare($statement);
			$sql_add->bindParam(':category_name', $subcategory);
			$sql_add->bindParam(':category_parent', $category);
			$sql_add->execute();
		}

		return $GLOBALS['dbCon']->lastinsertid();
	}

	function remove_product($product_id) {
		$statement = "DELETE FROM {$GLOBALS['db_prefix']}_clothing
						WHERE product_id = :product_id
						LIMIT 1";
		$sql_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_delete->bindParam(':product_id', $product_id);
		$sql_delete->execute();
	}

	function remove_category($category_id) {
		$statement = "DELETE FROM {$GLOBALS['db_prefix']}_clothing_categories
						WHERE category_id = :category_id ";
		$sql_delete = $GLOBALS['dbCon']->prepare($statement);
		$sql_delete->bindParam(':category_id', $category_id);
		$sql_delete->execute();
	}

    function get_products_display($category_id) {

        $products = array();

        //Get subcategories
        $statement = "SELECT category_id, category_name
                        FROM {$GLOBALS['db_prefix']}_clothing_categories
                        WHERE category_parent = :category_parent";
        $sql_subcat = $GLOBALS['dbCon']->prepare($statement);
        $sql_subcat->bindParam(':category_parent', $category_id);
        $sql_subcat->execute();
        $sql_subcat_data = $sql_subcat->fetchAll();
        $sql_subcat->closeCursor();

        foreach ($sql_subcat_data AS $data) {
            $statement = "SELECT product_id, product_name, product_price, product_stock, product_thumb
                        FROM {$GLOBALS['db_prefix']}_clothing
                        WHERE product_category = :product_category";
            $sql_product = $GLOBALS['dbCon']->prepare($statement);
            $sql_product->bindParam(':product_category', $data['category_id']);
            $sql_product->execute();
            $sql_product_data = $sql_product->fetchAll();
            $sql_product->closeCursor();

            if (!empty($sql_product_data))  $products[$data['category_name']] = array();

            foreach ($sql_product_data as $pData) {
                $line = array();
                $line['name'] = $pData['product_name'];
                $line['id'] = $pData['product_id'];
                $line['price'] = $pData['product_price'];
                $line['stock'] = $pData['product_stock'];
                $line['image'] = $pData['product_thumb'];

                $products[$data['category_name']][] = $line;
            }


        }

        return $products;

    }



} 