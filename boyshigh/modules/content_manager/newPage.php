<?php
ob_start();
require_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/string.php";

if(isset($_POST['new_form']))
{
	$sql="SELECT page_id FROM {$GLOBALS['db_prefix']}_pages";
	$rs=mysql_query($sql);
	$pages=mysql_num_rows($rs);

	if(isset($_POST['page']))
	{
		$sql="SELECT page_id FROM {$GLOBALS['db_prefix']}_pages WHERE page_name='".$_POST['page']."'";
		$rs=mysql_query($sql);
		if(mysql_num_rows($rs))
			$_POST['page']=$_POST['page']."_";
	}

	$sql="INSERT INTO {$GLOBALS['db_prefix']}_pages ";
	$sql.="(";
	$sql.="page_title";
	$sql.=(isset($_POST['page'])?",page_name":"");
	$sql.=",template_id";
	$sql.=",page_members_only";
	$sql.=($pages?"":",page_home");
	$sql.=") ";
	$sql.="VALUES ";
	$sql.="(";
	$sql.="'".$_POST['titl']."'";
	$sql.=(isset($_POST['page'])?",'".str_replace("\"","",str_replace("'","",str_replace(" ","_",$_POST['page'])))."'":"");
	$sql.=",'".$_POST['temp']."'";
	$sql.=",".(isset($_POST['memb'])?1:0);
	$sql.=($pages?"":",'1'");
	$sql.=")";
	mysql_query($sql);

	$sql="SELECT page_id FROM {$GLOBALS['db_prefix']}_pages ORDER BY page_id DESC LIMIT 1";
	$r=mysql_query($sql);
	$g=mysql_fetch_array($r);
	$Pid=$g['page_id'];

	$sql="SELECT page_name FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=".$Pid;
	$r=mysql_query($sql);
	$g=mysql_fetch_array($r);

	//Add owner
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_pages_owners (user_id, page_id) VALUES (:user_id, :page_id)";
	$sql_owner = $GLOBALS['dbCon']->prepare($statement);
	$sql_owner->bindParam(':user_id', $_SESSION['dbweb_user_id']);
	$sql_owner->bindParam(':page_id', $Pid);
	$sql_owner->execute();
	$sql_owner->closeCursor();

	echo "<script>";
	echo "var P=window.parent";
	echo ";P.WR(0,'/modules/content_manager/start.php')";
	echo ";setTimeout(\"location='/modules/content_manager/manager.php?page=".$g['page_name']."&Pid=".$Pid."&Tid=".$_POST['temp']."&W=".$_POST['W']."'\",900)";
	echo "</script>";
	die();
}


echo "<html>";
echo "<script src=/shared/G.js></script>";
include_once $_SERVER['DOCUMENT_ROOT']."/shared/admin_style.php";
echo $css;
echo "<script src=/shared/val.js></script>";
echo "<script>P.WNC(W,\"".(isset($_GET['page'])?"Create: ".caps(str_replace("_"," ",$_GET['page'])):"New Page")."\")</script>";
echo "<body>";
echo "<table width=100% height=100%>";
echo "<tr>";
echo "<td>";
echo "<center>";

echo "<form method=post onsubmit='return validate(1)'>";
echo "<script>document.write(\"<input type=hidden name=W value=\"+W+\">\")</script>";
echo "<input type=hidden name=new_form value=1>";

echo "<table class=T width=340 onmouseover=validate()>";
echo "<TR>";
echo "<TH colspan=9>".(isset($_GET['page'])?"Create: ".caps(str_replace("_"," ",$_GET['page'])):"New Page")."</TH>";
echo "</TR>";

echo "<TR>";
echo "<TD>Title</TD>";
echo "<TD width=99%>";
echo "<input type=text name=titl value=\"".(isset($_GET['page'])?caps(str_replace("_"," ",$_GET['page'])):"")."\" class=W100 title='Requires value'>";
echo "</TD>";
echo "</TR>";

echo "<TR><TD>Link</TD><TD width=99%>";
if(isset($_GET['page']))
{
echo "<input type=text value=\"".$_GET['page']."\" class=W100 disabled onmouseover=\"T_S(event,'Link cannot be changed',1)\">";
echo "<input type=hidden name=page value=\"".$_GET['page']."\">";
}
else
	echo "<input type=text name=page value=\"\" class=W100 title='Requires value'>";
echo "</TD></TR>";

echo "<TR>";
echo "<TD>Template</TD>";
echo "<TD>";
echo "<SELECT name=temp class=W100 title='Requires value'>";
echo "<option value=0>";

$sql="SELECT template_id,template_name,template_file FROM {$GLOBALS['db_prefix']}_templates ORDER BY template_name";
$rs=mysql_query($sql);
while(list($id,$name,$file)=mysql_fetch_array($rs))
	echo "<option value=".$id.">".$name." ~ ".$file;

echo "</SELECT>";
echo "</TD>";
echo "</TR>";
echo "<TR>";
echo "<TD>Members</TD>";
echo "<TD>";
echo "<input type=checkbox name=memb value=1 onmouseover=\"T_S(event,'Only members will have access to this page')\">";
echo "</TD>";
echo "</TR>";
echo "<TR>";
echo "<TD colspan=2>";
echo "<input type=submit value=OK> ";
echo "<input type=button value=Cancel onClick=(history?history.go(-1):P.WD(W))>";
echo "</TD>";
echo "</TR>";
echo "</table>";
echo "</form>";
echo "</center>";
echo "</td>";
echo "</tr></table>";
echo "</body>";
echo "</html>";

ob_end_flush();

?>