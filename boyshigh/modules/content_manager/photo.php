<?php

preg_match_all("@<!--AREA_([\w]+)-->@i",$template,$matches);
$scripts=array();
$stripMarked="";

foreach($matches[1] as $preg_match)
{
	$template_areas[]=$preg_match;
}

if(empty($page_id))
{
	$page_id=1;
}

//get data for each area
foreach($template_areas as $area_id)
{
	$area_conten="";

	$sql="SELECT id,module,type,header,footer,gizmo FROM {$GLOBALS['db_prefix']}_areas WHERE area_id=".$area_id." AND (page_id=".$page_id." OR shared=1)";
	$rsa=mysql_query($sql);
	if(mysql_num_rows($rsa))
	{
		list($id,$module,$type,$header,$footer,$gizmo)=mysql_fetch_array($rsa);

		// header text
		if($header)
		{
			if(isset($EdiTing))
			{
				$stripMarked=stripslashes($header);
				$stripMarked=string_strip($stripMarked); // converts tags to lowercase and strips out junk
				$stripMarked=mark_string($stripMarked,"HDR"); // puts ids on all tags without
				$stripMarked=str_replace("index.php","modules/content_manager/manager.php",$stripMarked); // replace index.php with manager.php
				$area_conten.="<span id=_HD".$id." class=_AREA>".$stripMarked;
				if(strpos($stripMarked," id=MENU_HD")>0)
				{}
				else
					$area_conten.="<span id=MENU_HD".$id." class=MENU_ lang=0></span>";
				$area_conten.="</span>";
			}
			else
			{
				$area_conten.=stripslashes($header);
			}
		}

		// gizmo
		if($gizmo)
		{
			if(isset($EdiTing))
			{
				$area_conten.="<span id=_GZ".$gizmo." class=_AREA>";
			}
			$sql="SELECT type,live FROM dbweb_gizmos WHERE id=".$gizmo;
			$rgz=mysql_query($sql);
			if(mysql_num_rows($rgz))
			{
				$gz=mysql_fetch_array($rgz);
				$area_conten.=stripslashes($gz['live']);
				if($gz['type']=="slideshow" && !isset($scripts['/modules/gizmos/slideshow/engine.js']))
				{
					$area_conten.="<script src=/modules/gizmos/slideshow/engine.js></script>";
					$scripts['/modules/gizmos/slideshow/engine.js']=1;
				}
			}
			if(isset($EdiTing))
			{
				$area_conten.="</span>";
			}
		}

		// module
		if($module)
		{
			if(isset($EdiTing))
			{
				$area_conten.="<span id=_MD lang=".$id." class=_AREA>";
			}
			if($module=="text")
			{
				// ATTENTION!
				// for older versions
				// text is depricated
				// From July 2008
			}
			elseif($module=="gallery")
			{
				if($type==1)
				{
					$itemname=getExt($_SERVER['DOCUMENT_ROOT']."/i/fluid","_mini.css");
					$area_conten.=($itemname?"<link rel=stylesheet href=/i/fluid/".$itemname.">":"<link rel=stylesheet href=/modules/gallery/search_mini.css>");
					$area_conten.="<script src=/modules/gallery/search_mini.js></script>";
					$area_conten.="<script>gallery_search_mini()</script>";
				}
				else
				{
					include_once $_SERVER['DOCUMENT_ROOT']."/modules/gallery/gallery.php";
					$area_conten.="<script>GallerySearch()</script>";
					$area_conten.="<script>GalleryArea()</script>";
				}
			}
			else
			{
				ob_start();
				include $_SERVER['DOCUMENT_ROOT']."/modules/".$module."/content.php";
				$area_conten.=ob_get_contents();
				ob_end_clean();
			}
			if(isset($EdiTing))
			{
				$area_conten.="</span>";
			}
		}

		// footer
		if($footer)
		{
			if(isset($EdiTing))
			{
				$stripMarked=stripslashes($footer);
				$stripMarked=string_strip($stripMarked); // converts tags to lowercase and strips out junk
				$stripMarked=mark_string($stripMarked,"FTR"); // puts ids on all tags without
				$stripMarked=str_replace("index.php","modules/content_manager/manager.php",$stripMarked); // replace index.php with manager.php
				$area_conten.="<span id=_FT".$id." class=_AREA>".$stripMarked;
				if(strpos($stripMarked," id=MENU_FT")>0)
				{}
				else
					$area_conten.="<span id=MENU_FT".$id." class=MENU_ lang=0></span>";
				$area_conten.="</span>";
			}
			else
			{
				$area_conten.=stripslashes($footer);
			}
		}

		$check_content=strip_tags($area_conten);
		if(isset($EdiTing))
		{
			if(empty($check_content))
			{
				$area_conten="<span id=EditArea_".$area_id." lang=".$id." class=_AREA>";
				$area_conten.="<span id=_HD".$id." class=_AREA>&#9658;EMPTY AREA</span>";
				$area_conten.="</span>";
			}
			else
			{
				$area_conten="<span id=EditArea_".$area_id." lang=".$id." class=_AREA>".$area_conten;
				$area_conten.="</span>";
			}
		}
		elseif(empty($check_content))
		{
			$area_conten="";
		}

	}
	elseif(isset($EdiTing))
	{
		$area_conten="<span id=EditArea_".$area_id." class=_AREA>";
		$area_conten.="<span id=_HD".$area_id." class=_AREA>&#9658;EMPTY AREA</span>";
		$area_conten.="</span>";
	}

	$template=str_replace("<!--AREA".$area_id."-->",$area_conten,$template);
}

?>