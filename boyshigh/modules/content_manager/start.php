<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";
//include_once $_SERVER['DOCUMENT_ROOT'] . "/modules/content_manager/create_tables.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/terms.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/admin_style.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/string.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/functions.php';

//Vars
$template = file_get_contents('html/start.html');
$allpages = "";

if (isset($_GET['remove'])) {

	//Get area_id's
	$statement = "SELECT id FROM {$GLOBALS['db_prefix']}_areas WHERE page_id=:page_id";
	$sql_areas = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas->bindParam(':page_id', $_GET['remove']);
	$sql_areas->execute();
	$sql_area_data = $sql_areas->fetchAll();
	$sql_areas->closeCursor();

	//Remove areas history
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_areas_history WHERE page_id=:page_id";
	$sql_areas_history = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas_history->bindParam(':page_id', $_GET['remove']);
	$sql_areas_history->execute();
	$sql_areas_history->closeCursor();

	//Remove areas pending
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_areas_pending WHERE page_id=:page_id";
	$sql_areas_pending = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas_pending->bindParam(':page_id', $_GET['remove']);
	$sql_areas_pending->execute();
	$sql_areas_pending->closeCursor();

	//Remove areas drafts
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_areas_editing WHERE page_id=:page_id";
	$sql_areas_editing = $GLOBALS['dbCon']->prepare($statement);
	$sql_areas_editing->bindParam(':page_id', $_GET['remove']);
	$sql_areas_editing->execute();
	$sql_areas_editing->closeCursor();

	//Copy to deleted table
	foreach ($sql_area_data as $area_data) {
		copy_area($area_data['id'], "{$GLOBALS['db_prefix']}_areas", "{$GLOBALS['db_prefix']}_areas_deleted", TRUE, TRUE, $_SESSION['dbweb_user_id']);
	}

	//Copy page table
	$statement = "SELECT template_id, page_home, page_name, page_title, page_description, page_keywords, page_members_only FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=:page_id LIMIT 1";
	$sql_page = $GLOBALS['dbCon']->prepare($statement);
	$sql_page->bindParam(':page_id', $_GET['remove']);
	$sql_page->execute();
	$sql_page_data = $sql_page->fetch();
	$sql_page->closeCursor();

	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_pages_deleted
					(page_id, template_id, page_home, page_name, page_title, page_description,
						page_keywords, page_members_only, delete_date, delete_by)
					VALUES
					(:page_id, :template_id, :page_home, :page_name, :page_title, :page_description,
						:page_keywords, :page_members_only, NOW(), :delete_by)";
	$sql_page_insert = $GLOBALS['dbCon']->prepare($statement);
	$sql_page_insert->bindParam(':page_id', $_GET['remove'] );
	$sql_page_insert->bindParam(':template_id', $sql_page_data['template_id']);
	$sql_page_insert->bindParam(':page_home', $sql_page_data['page_home']);
	$sql_page_insert->bindParam(':page_name', $sql_page_data['page_name']);
	$sql_page_insert->bindParam(':page_title', $sql_page_data['page_title']);
	$sql_page_insert->bindParam(':page_description', $sql_page_data['page_description']);
	$sql_page_insert->bindParam(':page_keywords', $sql_page_data['page_keywords']);
	$sql_page_insert->bindParam(':page_members_only', $sql_page_data['page_members_only']);
	$sql_page_insert->bindParam(':delete_by', $_SESSION['dbweb_user_id']);
	$sql_page_insert->execute();
	$sql_page_insert->closeCursor();

	//Delete page
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=:page_id";
	$sql_page_delete = $GLOBALS['dbCon']->prepare($statement);
	$sql_page_delete->bindParam(':page_id', $_GET['remove']);
	$sql_page_delete->execute();
	$sql_page_delete->closeCursor();

	$message = "&#187; Page  '{$sql_page_data['page_name']}' removed";
	write_log("Page '{$sql_page_data['page_name']}' removed",'content_manager', $_GET['remove']);

	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"document.location='start.php?W={$_GET['W']}'\" ></div>";

	die();
}

//Get settings
$statement = "SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='approval_system' LIMIT 1";
$sql_settings = $GLOBALS['dbCon']->prepare($statement);
$sql_settings->execute();
$settings = $sql_settings->fetch();
$sql_settings->closeCursor();
$approval_system = $settings['value'];

//Get pages
$statement = "SELECT page_id,page_name,page_title
				FROM {$GLOBALS['db_prefix']}_pages
				ORDER BY page_title,page_name";
$sql_pages = $GLOBALS['dbCon']->prepare($statement);
$sql_pages->execute();
$sql_pages_data = $sql_pages->fetchAll();
$sql_pages->closeCursor();

//Prepare statement - Page Ownership
$statement = "SELECT COUNT(*) AS access_counter FROM {$GLOBALS['db_prefix']}_pages_owners WHERE (page_id=:page_id OR page_id=0) AND user_id=:user_id";
$sql_owners = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Last update date
$statement = "SELECT DATE_FORMAT(last_update,'%d %b %Y') AS last_update FROM {$GLOBALS['db_prefix']}_areas WHERE page_id=:page_id ORDER BY last_update LIMIT 1";
$sql_last_update = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Check for draft status
$statement = "SELECT count(*) As counter FROM {$GLOBALS['db_prefix']}_areas_editing WHERE page_id=:page_id";
$sql_status_draft = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Check for pending status
$statement = "SELECT count(*) AS counter FROM {$GLOBALS['db_prefix']}_areas_pending WHERE page_id=:page_id";
$sql_status_pending = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - is approver?
$statement = "SELECT COUNT(*) AS counter FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id AND approved_by=:approved_by LIMIT 1";
$sql_approver = $GLOBALS['dbCon']->prepare($statement);

//Prepare statement - Get list of pending areas with submitter
$statement = "SELECT submit_by FROM {$GLOBALS['db_prefix']}_areas_pending WHERE page_id=:page_id";
$sql_submitted_by = $GLOBALS['dbCon']->prepare($statement);

//Assemble pages
foreach ($sql_pages_data as $pages) {

	//Check Ownership
	$sql_owners->bindParam(':page_id', $pages['page_id']);
	$sql_owners->bindParam('user_id', $_SESSION['dbweb_user_id']);
	$sql_owners->execute();
	$sql_owners_data = $sql_owners->fetch();
	$page_owner = $sql_owners_data['access_counter']>0?1:0;

	//Get last update
	$sql_last_update->bindParam(':page_id', $pages['page_id']);
	$sql_last_update->execute();
	$sql_update_data = $sql_last_update->fetch();
	if (!isset($sql_update_data['last_update']) || empty($sql_update_data['last_update'])) $sql_update_data['last_update'] = '-';

	//Get current state
	$page_status = '';
	$sql_status_pending->bindParam(':page_id', $pages['page_id']);
	$sql_status_pending->execute();
	$sql_status_count = $sql_status_pending->fetch();
	if ($sql_status_count['counter'] > 0) {
		$page_status = 'Pending';
	} else {
		$sql_status_draft->bindParam(':page_id', $pages['page_id']);
		$sql_status_draft->execute();
		$sql_status_count = $sql_status_draft->fetch();
		if ($sql_status_count['counter'] > 0) {
			$page_status = 'Draft';
		} else {
			$page_status = 'Current';
		}
	}

	//Is approver?
	$approver = 0;
	if ($page_status == 'Pending') {
		$sql_submitted_by->bindParam(':page_id', $pages['page_id']);
		$sql_submitted_by->execute();
		$sql_submitted_data = $sql_submitted_by->fetchAll();
		foreach ($sql_submitted_data as $submit_data) {
			$sql_approver->bindParam(':user_id', $submit_data['submit_by']);
			$sql_approver->bindParam(':approved_by', $_SESSION['dbweb_user_id']);
			$sql_approver->execute();
			$sql_approver_count = $sql_approver->fetch();
			if ($sql_approver_count['counter'] > 0) {
				$approver = 1;
				break;
			}

		}
	}

	$page_title = ($pages['page_title'] ? str_replace(' ', '_', caps(str_replace("_", " ", $pages['page_title']))) : "Untitled");

	$allpages.="{$pages['page_id']}|#|{$pages['page_name']}|#|$page_title|#|$page_owner|#|{$sql_update_data['last_update']}|#|$page_status|#|$approver}";
}

// iterate thru templates and insert db entries for files found
include_once $_SERVER['DOCUMENT_ROOT'] . "/shared/directory_files.php";
getFiles($_SERVER['DOCUMENT_ROOT'] . "/templates");
if (count($filelist) > 0) {
	foreach ($filelist as $k => $v) {
		if (stripos($v, ".htm") !== false || stripos($v, ".php") !== false) {
			$sql = "SELECT * FROM {$GLOBALS['db_prefix']}_templates WHERE template_file='" . $v . "'";
			$rs = mysql_query($sql);
			if (!mysql_num_rows($rs)) {
				$name = str_ireplace(".html", "", $v);
				$name = str_ireplace(".htm", "", $name);
				$name = str_ireplace(".php", "", $name);
				$name = str_replace("_", " ", $name);
				$name = caps($name);
				$sql = "INSERT INTO {$GLOBALS['db_prefix']}_templates (template_name,template_file) VALUES ('" . $name . "','" . $v . "')";
				mysql_query($sql);
			}
		}
	}
}

//Icon legend
$icon_legend = '<img src="/i/button/edit_16.png" height="13">=Editable, <img src="/i/button/preview_16.png" height="13">=View Only';
if ($approval_system != 'none') $icon_legend .= ', <img src="/i/button/faq_16.png" height="13">=Pending Approval';

$sql = "SELECT template_id,template_name,template_file FROM {$GLOBALS['db_prefix']}_templates ORDER BY template_name";
$rs = mysql_query($sql);
if (mysql_num_rows($rs)) {
	$temps = "";
	while (list($id, $name, $file) = mysql_fetch_array($rs))
		$temps.=$id . "|" . $name . "|" . $file . "}";
}

$p_content = isset($temps) ? "P.CONTENT['TEMPLATES']=\"" . $temps . "\";" : '';

//Replace tags
$template = str_replace('<!-- css -->', $css, $template);
$template = str_replace('// temps //', $p_content, $template);
$template = str_replace('!W!', $_GET['W'], $template);
$template = str_replace('<!-- allpages -->', $allpages, $template);
$template = str_replace('<!-- terms -->', $terms, $template);
$template = str_replace('<!-- icon_legend -->', $icon_legend, $template);

echo $template;

?>