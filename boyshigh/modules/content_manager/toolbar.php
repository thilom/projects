<?php
/** 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

echo "<script src='toolbar.js'></script>";

//Get CSS setting
$statement = "SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='content_manager_no_css' LIMIT 1";
$sql_css = $GLOBALS['dbCon']->prepare($statement);
$sql_css->execute();
$sql_css_data = $sql_css->fetch();
$sql_css->closeCursor();

//Menu Bar
$menu_bar = "<table id=menu_bar><tr>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}'\">Area Content</td>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onclick=\"document.location='preferences_editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}'\">Area Preferences</td>";
if ($sql_css_data['value'] == 'Y' || $_SESSION['user_type']=='developer' || $_SESSION['user_type']=='superuser') {
		$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onclick=\"document.location='css_editor.php?Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}'\")>Area CSS</td>";
}

$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onclick=\"open_area('{$_GET['Pid']}','{$_GET['Eid']}','{$_GET['Aid']}&WR={$_GET['WR']}')\" >Area Settings</td>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onclick=\"open_rollback('{$_GET['Pid']}','{$_GET['Eid']}','{$_GET['Aid']}','{$_GET['WR']}')\")>Roll Back</td>";

switch ($toolbar_mode) {
	case 1:
		if (isset($toolbar_switch) && $toolbar_switch === FALSE) {
			$menu_bar .= "<td class='mo_off' title='There is nothing to put live. Save something first'>Go Live!</td>";
		} else {
			$menu_bar .= "<td class='mo_g' onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className='mo_g'\" onclick='document.location=\"approve.php?golive=$area_id&Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"')>Go Live!</td>";
		}
		break;
	case 2:
		if (isset($toolbar_switch) && $toolbar_switch === FALSE) {
			$menu_bar .= "<td class='mo_off' title='There is nothing to send for approval. Save something first'>Submit for Approval</td>";
		} else {
			$menu_bar .= "<td class='mo_b' onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className='mo_b'\" onclick='document.location=\"approve.php?submit=$area_id&Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"')>Submit for Approval</td>";
		}
		
		break;
	case 3:
		$menu_bar .= "<td class='mo_g' onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className='mo_g'\" onclick='document.location=\"approve.php?approve=$area_id&Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"')>Approve Update</td>";
		$menu_bar .= "<td class='mo_r' onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className='mo_r'\" onclick='document.location=\"approve.php?reject=$area_id&Pid={$_GET['Pid']}&Aid={$_GET['Aid']}&Eid={$_GET['Eid']}&WR={$_GET['WR']}&W={$_GET['W']}\"')>Reject Update</td>";
		break;
}

$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td></tr>";
$menu_bar .= "</table>";
echo "<div id=top_menu >$menu_bar</div>";

?>
