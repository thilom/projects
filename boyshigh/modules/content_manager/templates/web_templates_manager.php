<?php

/**
 *  Web template manager
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: web_templates_manager.php 87 2011-09-14 06:45:49Z thilo $
 */

include_once $_SERVER['DOCUMENT_ROOT'] . "/settings/init.php";
require_once $_SERVER['DOCUMENT_ROOT'] . '/modules/logs/log_functions.php';
include_once "template_toolbar.php";

//Vars
$template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/content_manager/html/web_template_content.html");
$template_id = $_GET['template_id'];
$message = 'No Changes, Nothing to update';

//Get template file name
$statement = "SELECT template_file, template_name FROM {$GLOBALS['db_prefix']}_templates WHERE template_id=:template_id LIMIT 1";
$sql_template = $GLOBALS['dbCon']->prepare($statement);
$sql_template->bindParam(':template_id', $template_id);
$sql_template->execute();
$sql_template_data = $sql_template->fetch();
$sql_template->closeCursor();

//Get web_template
$web_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/templates/{$sql_template_data['template_file']}");

if (count($_POST) > 0) {
	echo "<br>";
	if ($_POST['template_content'] != $web_template) {
		
		file_put_contents("{$_SERVER['DOCUMENT_ROOT']}/templates/{$sql_template_data['template_file']}", $_POST['template_content']);
		
		$message = "Template '{$sql_template_data['template_name']}' saved";
	}

	write_log("Template updated: {$sql_template_data['template_name']}({$sql_template_data['template_file']})",'template_manager', $template_id);
	echo "<div class='dMsg' width=200px>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick=\"parent.WMN({$_GET['WR']});parent.WD({$_GET['W']})\" ></div>";
	die();
}

//Get WYSIWYG Buttons
$statement = "SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='content_manager_wysiwyg'";
$sql_wysiwyg = $GLOBALS['dbCon']->prepare($statement);
$sql_wysiwyg->execute();
$wysiwyg_data = $sql_wysiwyg->fetch();
$sql_wysiwyg->closeCursor();

//Replace_tags
$template = str_replace('<!-- content -->', $web_template, $template);
$template = str_replace('!editor_features!', $wysiwyg_data['value'], $template);
$template = str_replace('!WR!', $_GET['WR'], $template);
$template = str_replace('!W!', $_GET['W'], $template);

echo $template;
?>
