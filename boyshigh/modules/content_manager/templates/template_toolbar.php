<?php

/**
 *  Toolbar for the web templates editar
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: template_toolbar.php 87 2011-09-14 06:45:49Z thilo $
 */

$menu_bar = "<table id=menu_bar><tr>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='/modules/content_manager/templates/web_templates_manager.php?template_id={$_GET['template_id']}&file={$_GET['file']}&WR={$_GET['WR']}&W={$_GET['W']}'\">Template Editor</td>";
$menu_bar .= "<td onMouseOver=\"this.className='mo'\" onMouseOut=\"this.className=''\" onClick=\"document.location='/modules/content_manager/templates/web_templates_preferences.php?template_id={$_GET['template_id']}&file={$_GET['file']}&WR={$_GET['WR']}&W={$_GET['W']}'\">Preferences</td>";
$menu_bar .= "<td class='end_b'></td>";
$menu_bar .= "</tr>";
$menu_bar .= "<tr class='menu_drop_tr'><td class='menu_drop'></td><td class='menu_drop'></td><td class='menu_drop'></td></tr>";
$menu_bar .= "</table>";
echo "<div id=top_menu >$menu_bar</div>";
?>
