<?php
/** 
 * CSS editor for text area
 *
 * @author Thilo Muller(2011)
 * @version $Id: css.php 48 2011-05-06 03:47:20Z thilo $
 */

//Vars
$file = $_SERVER['DOCUMENT_ROOT'] . "/styles/css_{$_GET['Eid']}.css";
$template = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/modules/content_manager/html/css_area.html');

if (count($_POST) > 0) {
	$css_data = $_POST['css_data'];
	file_put_contents($file, $css_data);
}

if (is_file($file)) {
	$css_data = file_get_contents($file);
} else {
	file_put_contents($file, '');
	$css_data = '';
}

//Replace Tags
$template = str_replace('<!-- css_data -->', $css_data, $template);

echo $template;
?>
