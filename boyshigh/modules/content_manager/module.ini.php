<?php
/**
 * Settings and configuration for the module.
 *
 * These values are used to populate the CMS.
 */

//Settings for CMS
$module_name = "Content Manager";
$module_id = "content_manager"; //needs to be the same as the module directory
$module_icon = 'off_content_64.png';
$module_link = 'start.php';
$window_width = '800';
$window_height = '';
$window_position = '';

//Tabs tab $tab[name] = 'link'
$tab['text']['Edit Content'] = 'edit.php';
$tab['text']['Settings'] = 'prefs.php';

//Include CMS settings
//include_once($_SERVER['DOCUMENT_ROOT'] . '/settings/init.php');

//Constants
if(!defined('EDITOR')) define('EDITOR', "html/edit_text.html");
if(!defined('PREFS')) define('PREFS', 'html/prefs_form.html');
if(!defined('CONTENTS')) define('CONTENTS', 'html/content_page.html');
if(!defined('PAGE_PREFS')) define('PAGE_PREFS', SITE_ROOT . '/modules/content_manager/html/page_preferences.html');

//Security
$security_type = 's'; //s->Secured, o->Open to all
$security_name = "Manage Website Content";
$security_id = "mod_content";

//Content Types
$content_type[0]['name'] = 'Text Area';
$content_type[0]['id'] = 'text';
$content_type[0]['content_file'] = 'edit/content.php';
$content_type[0]['display_file'] = 'view/content.php';
$content_type[0]['public_display_file'] = 'view/content.php';
$content_type[0]['preferences_file'] = 'edit/preferences.php';
$content_type[0]['css_file'] = 'edit/css.php';
$content_type[0]['explanation'] = 'Free form HTML area.';

//Templates
$template[0]['name'] = "Web Templates";
$template[0]['file'] = "$module_id/templates/web_templates.php";
$template[0]['order'] = "10";

?>