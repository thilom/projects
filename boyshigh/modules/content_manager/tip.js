var TIP_cap=new Array
var TIP_cap_A=new Array
var TIP_act=0

function TIP_tag(v)
{
	var t=v.tagName.toUpperCase()
	if(!v.id.indexOf("Mq_"))
	{
		v=0
		alert("WARNING!\nTooltips cannot be applied to menu dropdowns")
	}
	else if(v.id=="EDIT_AREA"||t=="BODY"||t=="HR"||t=="OBJECT"||t=="SCRIPT")
	{
		p=v.parentNode.tagName.toUpperCase()
		if(v.id!="EDIT_AREA"&&p!="BODY"&&p!="HR"&&p!="OBJECT"&&p!="SCRIPT")
		{
			if(confirm("WARNING!\nIt is not advisable to attach a ToolTip to <"+t+"> tags!\nWould you like to attach the ToolTip on this tags <"+p+"> parent instead?"))
				v=v.parentNode
			else
				v=0
		}
		else
		{
			v=0
			alert("WARNING!\nIncompatible <"+t+"> tag selected!\nTry again...")
		}
	}
	return v
}

function TIP_start(v)
{
	TIP_close()
	m0de_sub=0
	var a=document.getElementById('TIP_box')
	a.style.top=y+9
	a.style.left=x+20
	a.getElementsByTagName('TEXTAREA')[0].value=E_O.lang
	a.style.display=""
	EDIT_highLight(1)
	if(E_O.parentNode.id.indexOf("Mp_")==0&&E_O.parentNode.parentNode.id=="TIP_")
		m0de_sub=1
}

function TIP_capture(v)
{
	if(!TIP_cap_A[E_O])
	{
		TIP_cap_A[E_O]=1
		var n=TIP_cap.length
		TIP_cap[n]=new Array
		TIP_cap[n][0]=""
		TIP_cap[n][1]=E_O.tagName.toLowerCase()
		TIP_cap[n][2]=(E_O.id?E_O.id:"")
		TIP_cap[n][3]=(E_O.name?E_O.name:"")
		TIP_cap[n][4]=(E_O.src?E_O.src.substr(E_O.src.lastIndexOf("/")+1):"")
		TIP_cap[n][5]=(E_O.rev?E_O.rev:"")
		TIP_cap[n][6]=(E_O.lang?E_O.lang:"")
		TIP_cap[n][7]=(E_O.className?E_O.className:"")
		TIP_cap[n][8]=EDIT_getArea()
	}
}

function TIP_save()
{
	if(TIP_act)
	{
		var c=""
		var h=""
		if(TIP_cap.length)
		{
			var i=0
			while(TIP_cap[i])
			{
				c+=TIP_cap[i][0]+"|"
				c+=TIP_cap[i][1]+"|"
				c+=TIP_cap[i][2]+"|"
				c+=TIP_cap[i][3]+"|"
				c+=TIP_cap[i][4]+"|"
				c+=TIP_cap[i][5]+"|"
				c+=TIP_cap[i][6]+"|"
				c+=TIP_cap[i][7]+"|"
				c+=TIP_cap[i][8]+"}"
				document.getElementsByTagName(TIP_cap[i][0])
				h+=(E_O.lang?E_O.lang:"")+"|}"
				i++
			}
		}

		EDIT_sv+=(c?"<textarea name=TIP_cap>"+c+"</textarea>":"")
		EDIT_sv+=(h?"<textarea name=TIP_val>"+h+"</textarea>":"")
		TIP_cap=new Array
		TIP_cap_A=new Array
		TIP_act=0
	}
}

function TIP_close()
{
	MENU_TX('TIP_box')
	document.getElementById("EDIT_zhl").style.display="none"
}

function TIP_end()
{
	TIP_close()
}

function TIP_value(t)
{
	TIP_capture()
	EDIT_mw('TIP_box')
	if(t.value)
	{
		E_O.lang=t.value.replace(/\n/g,"<br>").replace(/"/g,"&quot;").replace(/'/g,"&#39;")
		E_O.onmouseover=function(e){T_S(e,this.lang)}
	}
	else
	{
		E_O.lang=""
		E_O.onmouseover=function(e){}
	}
	TIP_act++
	EDIT_TimeSave()
}