/* 
 *  Javacript for web template manager
 * 
 *  @author Thilo Muller(2011)
 *  @version $Id: web_template_content.js 87 2011-09-14 06:45:49Z thilo $
 */

var oEdit1=new InnovaEditor("oEdit1")
//oEdit1.cmdAssetManager="modalDialogShow('/shared/Editor/assetmanager/assetmanager.php',640,465)"
oEdit1.mode="HTML"
oEdit1.width="100%"
oEdit1.height="470px"
oEdit1.toolbarMode=0;
oEdit1.features=editor_features;
