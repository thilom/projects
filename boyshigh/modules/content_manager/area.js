var AREA_zAi=0

function AREA_toggle_mode() {
	for (i=0; i<ars.length; i++) {
		xx = document.getElementById('EditArea_'+ars[i])
		if (xx.className == 'area_highlight') {
			xx.className = ''
			xx.onclick = function(){}
		} else {
			xx.className = 'area_highlight'
			xx.onclick = function(){
				var n=""
				var t=""
				if(this.name)
				{
					n=this.name.substr(0,this.name.indexOf("_"))
					t=this.name.substr(this.name.indexOf("_")+1)

				}
				Aid = this.id.split('_')[1];
				P.W("Edit Area"+(n?": "+n:""),"/modules/content_manager/editor.php?Pid="+Pid+"&Aid="+Aid+"&WR="+W+(n?"&Nid="+n:"")+(this.lang?"&Eid="+this.lang:""),P.wh,P.ww/1.4,0,0,2)
				P.WMN(W)
			};
		}
	}
	for (i=0; i<shr.length; i++) {
		xx = document.getElementById('EditShared_'+shr[i])
		if (xx.className == 'shared_highlight') {
			xx.className = ''
			xx.onclick = function(){}
		} else {
			xx.className = 'shared_highlight'
			xx.onclick = function(){
				var n=""
				var t=""
				if(this.name)
				{
					n=this.name.substr(0,this.name.indexOf("_"))
					t=this.name.substr(this.name.indexOf("_")+1)

				}
				Aid = this.id.split('_')[1];
				P.W("Edit Area"+(n?": "+n:""),"/modules/content_manager/editor.php?Pid="+Pid+"&Aid="+Aid+"&WR="+W+(n?"&Nid="+n:"")+(this.lang?"&Eid="+this.lang:""),P.wh,P.ww/1.4,0,0,2)
				P.WMN(W)
			};
		}
	}
}

function AREA_show()
{
	G_wd()
	var d=document.getElementById("EDIT_z0ff")
	d.style.height=ph+20
	d.style.width=pw
	d.onmousemove=function(e){T_S(e,'<b>Exit</b> Area Mode',1)}
	d.onclick=function(){EDIT_end()}
	d.style.display=""
	var d=document.getElementById("EDIT_zAs")
	d.innerHTML=""
	var a=document.getElementsByTagName("SPAN")
	var i=0
	while(a[i])
	{
		if(!a[i].id.indexOf("EditArea_"))
		{
			item_XY(a[i])
			var q=document.createElement("DIV")
			q.innerHTML="<br>"
			q.style.top=i_Y+(FF?-2:0)
			q.style.left=i_X+(FF?-2:0)
			q.style.height=(i_H<5?5:i_H)+(FF?2:0)
			q.style.width=(i_W<5?5:i_W)+(FF?-2:0)
			q.onmouseover=function(e){T_S(e,"<b>Edit Area</b>",1)}
			q.className=a[i].id.substr(9)
			if(a[i].lang)
				q.lang=a[i].lang
			if(a[i].name)
				q.name=a[i].name
			q.onclick=function()
			{
				var n=""
				var t=""
				if(this.name)
				{
					n=this.name.substr(0,this.name.indexOf("_"))
					t=this.name.substr(this.name.indexOf("_")+1)
				}
				P.W("Edit Area"+(n?": "+n:""),"/modules/content_manager/editor.php?Pid="+Pid+"&Tid="+Tid+"&Aid="+this.className+"&WR="+W+(n?"&Nid="+n:"")+(this.lang?"&Eid="+this.lang:"")+(t?"&tArget="+t:""),P.wh,P.ww/1.4,0,0,2)
				P.WMN(W)
			}
			d.appendChild(q)
		}
		i++
	}
	d.style.display=""
}

function AREA_insert(e)
{
	if(m0de=="area"&&m0de_sub==2)
	{
		var E_O=FF?e.target:event.srcElement
		if(E_O==_zAi)
			_zAi.style.display="none"
		else
		{
			item_XY(E_O)
			_zAi.style.top=i_Y
			_zAi.style.left=i_X
			_zAi.style.height=i_H
			_zAi.style.width=i_W
			_zAi.style.display=""
		}
	}
}

function AREA_tag(v)
{
	var t=v.tagName.toUpperCase()
	if(t=="A"||t=="IMG"||t=="INPUT"||t=="SELECT"||t=="BUTTON"||t=="TEXTAREA"||t=="HR"||t=="OBJECT"||t=="SCRIPT")
	{
		p=v.parentNode.tagName.toUpperCase()
		if(p!="A"&&p!="IMG"&&p!="INPUT"&&p!="SELECT"&&p!="BUTTON"&&p!="TEXTAREA"&&p!="HR"&&p!="OBJECT"&&p!="SCRIPT")
		{
			if(confirm("WARNING!\nIt is not advisable to insert an Area into <"+t+"> tags!\nWould you like to insert the Area into this tags parent <"+p+"> instead?"))
				v=v.parentNode
			else
				v=0
		}
		else
		{
			v=0
			alert("WARNING!\nIncompatible <"+t+"> tag selected!\nTry again...")
		}
	}
	return v
}

function AREA_capture()
{
	var n=prompt("New Area\nPlease enter a descriptive name for this area","")
	if(n)
	{
		n=n.replace(/_/g," ").replace(/>/g,"")

		var t=E_O.tagName.toLowerCase()+"|"+(E_O.id?E_O.id:"")+"|"+(E_O.name?E_O.name:"")+"|"+(E_O.className?E_O.className:"")+"|"+(E_O.lang?E_O.lang:"")+"|"+(E_O.height?E_O.height:"")+"|"+(E_O.width?E_O.width:"")

		AREA_Aid++
		var s=document.createElement("SPAN")
		s.id="EditArea_"+AREA_Aid
		s.name=n+"_"+t
		s.className="_NEWAREA"
		s.innerHTML="<span id=_HD"+AREA_Aid+" class=_AREA><b>TEMPORARY AREA</b><tt>NOTE! This area must be edited and saved before real insertion into the template.<br><b>Click to edit area</b></tt></span><span id=_FT"+AREA_Aid+" class=_AREA></span></span>"
		E_O.appendChild(s)
		_zAi.style.display="none"

		P.W("Edit Area"+n,"/modules/content_manager/editor.php?Pid="+Pid+"&Tid="+Tid+"&Aid="+AREA_Aid+"&WR="+W+"&Nid="+n+"&tArget="+t,P.wh,P.ww/1.4,0,0,2)
		P.WMN(W)
	}
}

function AREA_close()
{
	document.getElementById("EDIT_z0ff").style.display="none"
	document.getElementById("EDIT_zAs").style.display="none"
	document.getElementById("EDIT_zAi").style.display="none"
}