<?php
/**
 * Manage Scrum BRM users
 * 
 * @author Thilo Muller(2011)
 * @version $Id: login_list.php 59 2011-05-23 14:09:00Z thilo $
 */

//Includes
include_once $_SERVER['DOCUMENT_ROOT'].'/settings/init.php';
include_once(SITE_ROOT . '/modules/login/module.ini.php');
require_once SITE_ROOT . '/modules/logs/log_functions.php';
require_once SITE_ROOT . '/shared/database_functions.php';
require_once SITE_ROOT.'/modules/login/class.login.php';

//Vars
$lg_obj = new login();
$user_types = array('normal','superuser','developer');
$user_id = isset($_GET['lid'])?$_GET['lid']:'';

//Get list of users 
$user_list = '';
$statement = "SELECT user_id, CONCAT(firstname, ' ', lastname) AS fullname FROM {$GLOBALS['db_prefix']}_user ORDER BY firstname, lastname";
$sql_users = $GLOBALS['dbCon']->prepare($statement);
$sql_users->execute();
$sql_data = $sql_users->fetchAll();
foreach ($sql_data as $k=>$data) {
	if ($data['user_id'] != $user_id) $user_list .= "<option value='{$data['user_id']}'>{$data['fullname']}";
}
$sql_users->closeCursor();

echo "<link rel=stylesheet href=/shared/style.css>";
echo "<script src='/shared/trwacker.js'></script>";

if (isset($_GET['a'])) {
	if (isset($_POST['Save'])) {
		$mBody = save_login($user_id);
	} else {
		$mBody = add_login($user_id);
	}
} else {
	$mBody = list_login();
}

if (empty($_REQUEST['W'])) {
	$W = "";
} else {
	$W = $_REQUEST['W'];
}

echo $mBody;

function list_login($rPP = 15) {
	$body = file_get_contents(SITE_ROOT . '/modules/login/html/login_list.html');
	$l_data = '';

	$statement ="SELECT firstname, lastname, user_id, username, email FROM {$GLOBALS['db_prefix']}_user ORDER BY firstname, lastname";
	$sql_list = $GLOBALS['dbCon']->prepare($statement);
	$sql_list->execute();
	if ($sql_list->rowCount() == 0) {
		$body = "<div style='text-align: center'>There are currently no users in the database</div>";
	} else {
		$res = $sql_list->fetchAll();
		foreach($res as $key=>$value) {
			$l_data.= "<tr>";
			$l_data.= "	<td>{$value['firstname']} {$value['lastname']}</td>";
			$l_data.= "	<td><a href='mailto:{$value['email']}'>{$value['email']}</a></td>";
			$l_data.= "	<td>{$value['username']}</td>";
			$l_data.= "	<td align=right><input type=button value='Edit' class='edit_button' title='edit' onclick='location.href=\"".$_SERVER['PHP_SELF']."?a&lid={$value['user_id']}\"'>";
			$l_data.= "</td>";
			$l_data.= "</tr>";
		}
	}

	$body = "<div class='top_buttons'><input type='button' value='Add User' class=add_button onCLick='document.location=\"/modules/login/login_list.php?a\"' ></div>" . $body;
	$body = str_ireplace('<!-- $data -->', $l_data, $body);
	$body .= "<SCRIPT>whack()</SCRIPT>";
	return $body;
}

function add_login($id = null) {
	global $W;
	$a_list = '';
	$body = file_get_contents(SITE_ROOT . '/modules/login/html/login_add.html');
	$body = str_ireplace('<!-- $W -->', $W, $body);
	$body = str_ireplace('<!-- SITE_URL -->', SITE_ROOT, $body);
	$all_page_access = '';

	//Get approval method
	$statement = "SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='approval_system' LIMIT 1";
	$sql_approval = $GLOBALS['dbCon']->prepare($statement);
	$sql_approval->execute();
	$sql_data = $sql_approval->fetchAll();
	$approval_type = $sql_data[0]['value'];
	$sql_approval->closeCursor();

	//Setup approval vars
	if ($approval_type == 'all' || $approval_type == 'single') {
		$user_message = '';
		$users_enabled = '';
	} else {
		$user_message = "<tt>This installation of {$GLOBALS['prog_name']} does not require up-line approval</tt>";
		$users_enabled = 'disabled';
	}

	//Pages list
	$pages_list = "";
	$statement = "SELECT page_id, page_name, page_title FROM {$GLOBALS['db_prefix']}_pages ORDER BY page_name";
	$sql_pages = $GLOBALS['dbCon']->prepare($statement);
	$sql_pages->execute();
	$sql_data = $sql_pages->fetchAll();
	foreach ($sql_data as $data) {
		$pages_list .= "<option value='{$data['page_id']}'>{$data['page_name']} - {$data['page_title']} </option>";
	}
	$sql_pages->closeCursor();
	
	//Get access rights
	$access_rights = "";
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/modules/';
	if (is_dir($dir)) {
		$dh = opendir($dir);
		if ($dh) {
			while (($file = readdir($dh)) !== false) {
				if (is_dir($dir . $file)) {
					if ($file == '.' || $file == '..') continue;
						$security_type = '';
						$security_name = '';
						$security_id = '';
						if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
						if (isset($security_type)) {
							if ($security_type == 's') {
								
								$access_rights .= "<tr><td>$security_name</td><td><input type='checkbox' name='access[]' value='$security_id' ></td></tr>";
							}
						}
				}
			}
			closedir($dh);
		}
	}

	//Assemble user types
	$user_type_radio = '';
	foreach ($GLOBALS['user_types'] as $user_type) {
		$user_type_radio .= "<input type='radio' value='$user_type' name='user_type' ";
		$user_type_radio .= $user_type=='normal'?"checked":'';
		$user_type_radio .= "> " . ucfirst($user_type) . "<br>";
	}
	
	if (empty($id)) {
		$body = str_ireplace('<!-- firstname -->', '', $body);
		$body = str_ireplace('<!-- lastname -->', '', $body);
		$body = str_ireplace('<!-- email -->', '', $body);
		$body = str_ireplace('<!-- username -->', '', $body);
		$body = str_ireplace('<!-- $cmd -->', 'login_mod', $body);
		$body = str_ireplace('!a_list!', $a_list, $body);
		$body = str_ireplace('<!-- user_list -->', $GLOBALS['user_list'], $body);
		$body = str_ireplace('<!-- access_rights -->', $access_rights, $body);
		$body = str_ireplace('<!-- disable_account -->', '', $body);
		$body = str_ireplace('<!-- owned_pages -->', '', $body);
		$body = str_ireplace('<!-- pages_list -->', $pages_list, $body);
		$body = str_ireplace('<!-- users_message -->', $user_message, $body);
		$body = str_ireplace('<!-- users_enabled -->', $users_enabled, $body);
		$body = str_ireplace('<!-- user_types -->', $user_type_radio, $body);
		$body = str_ireplace('<!-- all_page_access -->', '', $body);
		$body = str_ireplace('<!-- shared_areas -->', '', $body);
	} else {

		//Approval by list
		if ($approval_type == 'all' || $approval_type == 'single') {
			$statement = "SELECT approved_by FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id";
			$sql_app = $GLOBALS['dbCon']->prepare($statement);
			$sql_app->bindParam(':user_id', $id);
			$sql_app->execute();
			$sql_data = $sql_app->fetchAll();
			foreach ($sql_data as $k=>$data) {
				$a_list .= "{$data['approved_by']},";
			}
			$a_list = substr($a_list, 0, -1);
		}

		//Prepare statement - acess rights
		$statement = "SELECT access FROM {$GLOBALS['db_prefix']}_user_access WHERE user_id=:user_id AND module_id=:module_id";
		$sql_access = $GLOBALS['dbCon']->prepare($statement);

		//Get access rights
		$access_rights = "";
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/modules/';
		if (is_dir($dir)) {
			$dh = opendir($dir);
			if ($dh) {
				while (($file = readdir($dh)) !== false) {
					if (is_dir($dir . $file)) {
						if ($file == '.' || $file == '..') continue;
							$security_type = '';
							$security_name = '';
							$security_id = '';
							if (!@include($dir.$file.'/module.ini.php')) continue; //Make sure module.ini.php exists
							if (isset($security_type)) {
								if ($security_type == 's') {
									$sql_access->bindParam(':user_id', $id);
									$sql_access->bindParam(':module_id', $security_id);
									$sql_access->execute();
									if ($sql_access->rowCount() > 0) {
										$sql_data = $sql_access->fetchAll();
										$access = $sql_data[0]['access'] == 'y'?'checked':'';
									} else {
										$access = '';
									}
									$access_rights .= "<tr><td>$security_name</td><td><input type='checkbox' name='access[]' value='$security_id' $access ></td></tr>";
								}
							}
					}
				}
				closedir($dh);
			}
		}

		//Pages owned
		$owned_pages = '';
		$statement = "SELECT page_id FROM {$GLOBALS['db_prefix']}_pages_owners WHERE user_id=:user_id";
		$sql_pages = $GLOBALS['dbCon']->prepare($statement);
		$sql_pages->bindParam(':user_id', $id);
		$sql_pages->execute();
		$sql_data = $sql_pages->fetchAll();
		foreach ($sql_data as $data) {
			if ($data['page_id'] == 0) {
				$all_page_access = 'checked';
			} else {
				$owned_pages .= "{$data['page_id']},";
			}
		}
		$sql_pages->closeCursor();
		$owned_pages = substr($owned_pages,0,-1);

		
		//User Details
		$statement = "SELECT a.firstname, a.lastname, a.username, a.email, a.inactive, a.user_type, b.shared_areas 
						FROM {$GLOBALS['db_prefix']}_user AS a 
						LEFT JOIN {$GLOBALS['db_prefix']}_user_settings AS b ON a.user_id=b.user_id
						WHERE a.user_id=:user_id";
		$sql_user = $GLOBALS['dbCon']->prepare($statement);
		$sql_user->bindParam(':user_id', $id);
		$sql_user->execute();
		$data = $sql_user->fetchAll();
		$sql_user->closeCursor();
		$disable_account = $data[0]['inactive']?'checked':'';

		//Get current users user type
		$statement = "SELECT user_type FROM {$GLOBALS['db_prefix']}_user WHERE user_id=:user_id LIMIT 1";
		$sql_user = $GLOBALS['dbCon']->prepare($statement);
		$sql_user->bindParam(':user_id', $_SESSION['dbweb_user_id']);
		$sql_user->execute();
		$sql_user_data = $sql_user->fetch();
		$sql_user->closeCursor();
		
		//Assemble user types
		$user_type_radio = '';
		if (empty($data[0]['user_type'])) $data[0]['user_type'] = 'normal';
		foreach ($GLOBALS['user_types'] as $user_type) {
			$user_type_radio .= "<input type='radio' value='$user_type' name='user_type'";
			$user_type_radio .= $user_type==$data[0]['user_type']?' checked ':' ';
			if (($sql_user_data['user_type']!='developer'&&$data[0]['user_type']=='developer')) {
				$user_type_radio .= " disabled ><span style='color: gray' >";
			} else if ($sql_user_data['user_type']!='developer'&&$user_type=='developer') {
				$user_type_radio .= " disabled ><span style='color: gray' >";
			} else {
				$user_type_radio .= ' ><span>'; 
			}
			
			$user_type_radio .= ucfirst($user_type) . "</span><br>";
		}
		$developer_message = $sql_user_data['user_type']!='developer'&&$data[0]['user_type']=='developer'?"<tt>Only developers can change a developers user type</tt>":"";
		
		//Shared areas
		$shared_areas = $data[0]['shared_areas']=='Y'?'checked':'';
		
		$body = str_ireplace('<!-- firstname -->', $data[0]['firstname'], $body);
		$body = str_ireplace('<!-- lastname -->', $data[0]['lastname'], $body);
		$body = str_ireplace('<!-- email -->', $data[0]['email'], $body);
		$body = str_ireplace('<!-- username -->', $data[0]['username'], $body);
		$body = str_ireplace('<!-- $cmd -->', 'login_mod', $body);
		$body = str_ireplace('!a_list!', $a_list, $body);
		$body = str_ireplace('<!-- user_list -->', $GLOBALS['user_list'], $body);
		$body = str_ireplace('<!-- access_rights -->', $access_rights, $body);
		$body = str_ireplace('<!-- disable_account -->', $disable_account, $body);
		$body = str_ireplace('<!-- owned_pages -->', $owned_pages, $body);
		$body = str_ireplace('<!-- pages_list -->', $pages_list, $body);
		$body = str_ireplace('<!-- users_message -->', $user_message, $body);
		$body = str_ireplace('<!-- users_enabled -->', $users_enabled, $body);
		$body = str_ireplace('<!-- user_types -->', $user_type_radio, $body);
		$body = str_ireplace('<!-- developer_message -->', $developer_message, $body);
		$body = str_ireplace('<!-- all_page_access -->', $all_page_access, $body);
		$body = str_ireplace('<!-- shared_areas -->', $shared_areas, $body);
		$body = str_ireplace('!approval_system!', $approval_type, $body);
	}
	
	return $body;
}

function save_login($id) {
	$message = '';

	//Save user details
	if (empty($id)) {
		
		//Encrypt password
		$np = $GLOBALS['lg_obj']->hash_password($_POST['np']);
		
		//Get next ID
		$id = next_id('modularus_user', 'user_id');

		$inactive = isset($_POST['disabled_account'])?'1':'';
//		$np = MD5($_POST['np']);
		
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_user 
						(user_id, firstname, lastname, username, email, date_created, np, inactive, user_type) 
					   VALUES 
						(:user_id, :firstname, :lastname, :username, :email, NOW(), :password, :inactive, :user_type)";
		$sql_user_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_user_insert->bindParam(':user_id', $id);
		$sql_user_insert->bindParam(':firstname', $_POST['firstname']);
		$sql_user_insert->bindParam(':lastname', $_POST['lastname']);
		$sql_user_insert->bindParam(':username', $_POST['username']);
		$sql_user_insert->bindParam(':password', $np);
		$sql_user_insert->bindParam(':inactive', $inactive);
		$sql_user_insert->bindParam(':email', $_POST['email']);
		$sql_user_insert->bindParam(':user_type', $_POST['user_type']);
		$sql_user_insert->execute();

		$message .= "New user created: ({$_POST['firstname']} {$_POST['lastname']}";
		write_log("New user created: ({$_POST['firstname']} {$_POST['lastname']})", $GLOBALS['security_id'], $id);
	} 

	//Get current data
	$statement = "SELECT firstname, lastname, email, username, np, inactive, user_type 
					FROM {$GLOBALS['db_prefix']}_user WHERE user_id=:user_id LIMIT 1";
	$sql_user_select = $GLOBALS['dbCon']->prepare($statement);
	$sql_user_select->bindParam(':user_id', $id);
	$sql_user_select->execute();
	$sql_data = $sql_user_select->fetchAll();
	$sql_user_select->closeCursor();

	$field_list = array('firstname', 'lastname', 'email', 'username');

	foreach ($field_list as $field_name) {
		if ($_POST[$field_name] != $sql_data[0][$field_name]) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_user SET $field_name=:field_data WHERE user_id=:user_id";
			$sql_user_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_user_update->bindParam(':field_data', $_POST[$field_name]);
			$sql_user_update->bindParam(':user_id', $id);
			$sql_user_update->execute();
			$sql_user_update->closeCursor();

			$message .= "&#187; User update($field_name): {$sql_data[0][$field_name]} changed to {$_POST[$field_name]}<br>";
			write_log("User update($field_name): {$sql_data[0][$field_name]} changed to {$_POST[$field_name]}", $GLOBALS['security_id'], $id);
		}
	}

	//Password
	$nnp = $GLOBALS['lg_obj']->hash_password($_POST['np']);
	if ($_POST['np'] != 'Click to Change Password' && $sql_data[0]['np'] != $nnp) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_user SET np=:password WHERE user_id=:user_id";
		$sql_password = $GLOBALS['dbCon']->prepare($statement);
		$sql_password->bindParam(':password',$nnp);
		$sql_password->bindParam(':user_id', $id);
		$sql_password->execute();
		$sql_password->closeCursor();

		$message .= "&#187; Password updated for {$_POST['firstname']} {$_POST['lastname']}<br>";
		write_log("Password updated for {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id);
	}

	//Inactive account
	$inactive = isset($_POST['disabled_account'])?'1':'';
	if ($inactive != $sql_data[0]['inactive']) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_user SET inactive=:inactive WHERE user_id=:user_id";
		$sql_inactive = $GLOBALS['dbCon']->prepare($statement);
		$sql_inactive->bindParam(':inactive', $inactive);
		$sql_inactive->bindParam(':user_id', $id);
		$sql_inactive->execute();
		$sql_inactive->closeCursor();

		$active_message = $inactive==1?'de-activated':'activated';

		$message .= "&#187; Account $active_message for {$_POST['firstname']} {$_POST['lastname']}<br>";
		write_log("Account $active_message for {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id);
	}

	/* User Types */
	if ($_POST['user_type'] != $sql_data[0]['user_type']) {
		$statement = "UPDATE {$GLOBALS['db_prefix']}_user SET user_type=:user_type WHERE user_id=:user_id";
		$sql_user_type = $GLOBALS['dbCon']->prepare($statement);
		$sql_user_type->bindParam(':user_type', $_POST['user_type']);
		$sql_user_type->bindParam(':user_id', $id);
		$sql_user_type->execute();
		$sql_user_type->closeCursor();

		$message .= "&#187; User type changed to '{$_POST['user_type']}' for {$_POST['firstname']} {$_POST['lastname']}<br>";
		write_log("User type changed to '{$_POST['user_type']}' for {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id);
	}


	/* Account Access */
	//Get current access list
	$current_access = array();
	$statement = "SELECT module_id FROM {$GLOBALS['db_prefix']}_user_access WHERE user_id=:user_id AND access='y'";
	$sql_access = $GLOBALS['dbCon']->prepare($statement);
	$sql_access->bindParam(':user_id', $id);
	$sql_access->execute();
	$sql_dataA = $sql_access->fetchAll();
	$sql_access->closeCursor();
	foreach ($sql_dataA AS $data) {
		$current_access[] = $data['module_id'];
	}

	//Get new list
	if (!empty($_POST['access'])) {
		foreach ($_POST['access'] as $access_item) {
			$new_access[] = $access_item;
		}
	}

	//Prepare statement - insert access
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_user_access (user_id, module_id, access) VALUES (:user_id, :module_id, 'y')";
	$sql_access_insert = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Remove Access
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_user_access WHERE user_id=:user_id AND module_id=:module_id";
	$sql_access_delete = $GLOBALS['dbCon']->prepare($statement);

	//Add new access
	if (!empty($new_access)) {
		foreach ($new_access AS $access_module) {
			if (!in_array($access_module, $current_access)) {
				$sql_access_insert->bindParam(':user_id', $id);
				$sql_access_insert->bindParam(':module_id', $access_module);
				$sql_access_insert->execute();

				$message .= "&#187; Access granted to {$GLOBALS['module_list'][$access_module]} for {$_POST['firstname']} {$_POST['lastname']}<br>";
				write_log("Access granted to {$GLOBALS['module_list'][$access_module]} for {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id);
			}
		}
	}

	//Remove Access
	foreach ($current_access as $access_module) {
		if (!in_array($access_module, $new_access)) {
			$sql_access_delete->bindParam(':user_id', $id);
			$sql_access_delete->bindParam(':module_id', $access_module);
			$sql_access_delete->execute();

			$message .= "&#187; Access revoked to {$GLOBALS['module_list'][$access_module]} for {$_POST['firstname']} {$_POST['lastname']}<br>";
			write_log("Access revoked to {$GLOBALS['module_list'][$access_module]} for {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id);
		}
	}

	/* Up-line approval */
	//Get current approvals
	$current_approvals = array();
	$statement = "SELECT approved_by FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id";
	$sql_approve = $GLOBALS['dbCon']->prepare($statement);
	$sql_approve->bindParam(':user_id', $id);
	$sql_approve->execute();
	$sql_dataB = $sql_approve->fetchAll();
	$sql_approve->closeCursor();
	foreach ($sql_dataB as $data) {
		$current_approvals[] = $data['approved_by'];
	}

	//Get new approvals
	$new_approvals = explode(',', $_POST['access_list']);

	//Prepare statement - add approvals
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_user_approve (user_id, approved_by) VALUES (:user_id, :approved_by)";
	$sql_approve_insert = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - user details
	$statement = "SELECT firstname, lastname FROM {$GLOBALS['db_prefix']}_user WHERE user_id=:user_id LIMIT 1";
	$sql_user = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - remove approvals
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_user_approve WHERE user_id=:user_id AND approved_by=:approved_by";
	$sql_approve_delete = $GLOBALS['dbCon']->prepare($statement);

	//Add new approvals
	foreach ($new_approvals as $approval_id) {
		if (empty($approval_id)) continue;
		if (!in_array($approval_id, $current_approvals)) {
			$sql_approve_insert->bindParam(':user_id', $id);
			$sql_approve_insert->bindParam(':approved_by', $approval_id);
			$sql_approve_insert->execute();

			$sql_user->bindParam(':user_id', $approval_id);
			$sql_user->execute();
			$sql_dataC = $sql_user->fetchAll();

			$message .= "&#187; Added {$sql_dataC[0]['firstname']} {$sql_dataC[0]['lastname']} as approver for {$_POST['firstname']} {$_POST['lastname']}<br>";
			write_log("Added {$sql_dataC[0]['firstname']} {$sql_dataC[0]['lastname']} as approver for {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id);
		}
	}

	//Remove approval
	foreach ($current_approvals as $approval_id) {
		if (!in_array($approval_id, $new_approvals)) {
			$sql_approve_delete->bindParam(':user_id', $id);
			$sql_approve_delete->bindParam(':approved_by', $approval_id);
			$sql_approve_delete->execute();

			$sql_user->bindParam(':user_id', $approval_id);
			$sql_user->execute();
			$sql_dataC = $sql_user->fetchAll();

			$message .= "&#187; Removed {$sql_dataC[0]['firstname']} {$sql_dataC[0]['lastname']} as approver for {$_POST['firstname']} {$_POST['lastname']}<br>";
			write_log("Removed {$sql_dataC[0]['firstname']} {$sql_dataC[0]['lastname']} as approver for {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id);
		}
	}

	/* Page Ownership */
	//Current owned pages
	$current_pages = array();
	$all_page_access = '';
	$statement = "SELECT page_id FROM {$GLOBALS['db_prefix']}_pages_owners WHERE user_id=:user_id";
	$sql_pages = $GLOBALS['dbCon']->prepare($statement);
	$sql_pages->bindParam(':user_id', $id);
	$sql_pages->execute();
	$sql_dataD = $sql_pages->fetchAll();
	foreach ($sql_dataD as $data) {
		if ($data['page_id'] == 0) {
			$all_page_access = 'Y';
		} else {
			$current_pages[] = $data['page_id'];
		}
	}
	$sql_pages->closeCursor();

	//New pages
	$new_pages = explode(',', $_POST['owned_pages']);

	//Prepare statement - Add pages
	$statement = "INSERT INTO {$GLOBALS['db_prefix']}_pages_owners (page_id, user_id) VALUES (:page_id, :user_id)";
	$sql_pages_insert = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - Remove page ownership
	$statement = "DELETE FROM {$GLOBALS['db_prefix']}_pages_owners WHERE page_id=:page_id AND user_id=:user_id";
	$sql_pages_delete = $GLOBALS['dbCon']->prepare($statement);

	//Prepare statement - page_name
	$statement = "SELECT page_title FROM {$GLOBALS['db_prefix']}_pages WHERE page_id=:page_id";
	$sql_page_title = $GLOBALS['dbCon']->prepare($statement);

	//Add pages
	foreach ($new_pages as $pages_id) {
		if (empty($pages_id)) continue;
		if (!in_array($pages_id, $current_pages)) {
			$sql_pages_insert->bindParam(':page_id', $pages_id);
			$sql_pages_insert->bindParam(':user_id', $id);
			$sql_pages_insert->execute();

			$sql_page_title->bindParam(':page_id', $pages_id);
			$sql_page_title->execute();
			$sql_dataE = $sql_page_title->fetchAll();

			$message .= "&#187; Added {$_POST['firstname']} {$_POST['lastname']} as page owner to {$sql_dataE[0]['page_title']}<br>";
			write_log("Added {$_POST['firstname']} {$_POST['lastname']} as page owner to {$sql_dataE[0]['page_title']}", $GLOBALS['security_id'], $id, $pages_id);
		}
	}

	//All page access
	$new_page_access = isset($_POST['all_pages'])?'Y':'';
	if ($all_page_access != $new_page_access) {
		if (!empty($new_page_access)) {
			$statement = "INSERT INTO {$GLOBALS['db_prefix']}_pages_owners (user_id, page_id) VALUES (:user_id, 'Y')";
			$sql_all_pages = $GLOBALS['dbCon']->prepare($statement);
			$sql_all_pages->bindParam(':user_id', $id);
			$sql_all_pages->execute();
			$sql_all_pages->closeCursor();
			$message .= "&#187; Granted 'edit' access for all pages to {$_POST['firstname']} {$_POST['lastname']}<br>";
			write_log("Granted 'edit' access for all pages to {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id, $pages_id);
		} else {
			$statement = "DELETE FROM {$GLOBALS['db_prefix']}_pages_owners WHERE user_id=:user_id AND page_id=0 LIMIT 1";
			$sql_all_pages = $GLOBALS['dbCon']->prepare($statement);
			$sql_all_pages->bindParam(':user_id', $id);
			$sql_all_pages->execute();
			$sql_all_pages->closeCursor();
			$message .= "&#187; Revoked 'edit' access for all pages to {$_POST['firstname']} {$_POST['lastname']}<br>";
			write_log("Revoked 'edit' access for all pages to {$_POST['firstname']} {$_POST['lastname']}", $GLOBALS['security_id'], $id, $pages_id);

		}
	}

	//Remove page ownership
	foreach ($current_pages as $pages_id) {
		if (empty($pages_id)) continue;
		if (!in_array($pages_id, $new_pages)) {
			$sql_pages_delete->bindParam(':page_id', $pages_id);
			$sql_pages_delete->bindParam(':user_id', $id);
			$sql_pages_delete->execute();

			$sql_page_title->bindParam(':page_id', $pages_id);
			$sql_page_title->execute();
			$sql_dataE = $sql_page_title->fetchAll();

			$message .= "&#187; Removed {$_POST['firstname']} {$_POST['lastname']} as page owner to {$sql_dataE[0]['page_title']}<br>";
			write_log("Removed {$_POST['firstname']} {$_POST['lastname']} as page owner to {$sql_dataE[0]['page_title']}", $GLOBALS['security_id'], $id, $pages_id);
		}
	}

	/* Shared Areas */
	$shared_areas = isset($_POST['shared_areas'])?'Y':'';
	$current_shared = '';
	$statement = "SELECT shared_areas FROM {$GLOBALS['db_prefix']}_user_settings WHERE user_id=:user_id";
	$sql_settings = $GLOBALS['dbCon']->prepare($statement);
	$sql_settings->bindParam(':user_id', $id);
	$sql_settings->execute();
	$settings_count = $sql_settings->rowCount();
	if ($settings_count == 0) {
		$statement = "INSERT INTO {$GLOBALS['db_prefix']}_user_settings (user_id, shared_areas) VALUES (:user_id, :shared_areas)";
		$sql_settings_insert = $GLOBALS['dbCon']->prepare($statement);
		$sql_settings_insert->bindParam(':user_id', $id);
		$sql_settings_insert->bindParam(':shared_areas', $shared_areas);
		$sql_settings_insert->execute();
		$sql_settings_insert->closeCursor();
	} else {
		$sql_settings_data = $sql_settings->fetch();
		$current_shared = $sql_settings_data['shared_areas'];
		
		if ($current_shared != $shared_areas) {
			$statement = "UPDATE {$GLOBALS['db_prefix']}_user_settings SET shared_areas=:shared_areas WHERE user_id=:user_id";
			$sql_settings_update = $GLOBALS['dbCon']->prepare($statement);
			$sql_settings_update->bindParam(':user_id', $id);
			$sql_settings_update->bindParam(':shared_areas', $shared_areas);
			$sql_settings_update->execute();
			$sql_settings_update->closeCursor();
		}
	}
	$sql_settings->closeCursor();
	if (empty($shared_areas)) {
		$message .= "&#187; Revoked access to edit shared areas<br>";
		write_log("Revoked access to edit shared areas for {$_POST['firstname']} {$_POST['lastname']} ", $GLOBALS['security_id'], $id);
	} else {
		$message .= "&#187; Enabled access to edit shared areas<br>";
		write_log("Enabled access to edit shared areas for {$_POST['firstname']} {$_POST['lastname']} ", $GLOBALS['security_id'], $id);
	}
	
	
	

	if (empty($message)) $message = "&#187; No Changes, Nothing to update";
	echo "<div class='dMsg'>$message</div>";
	echo "<div class='bMsg'><input type=button value='Continue' class=ok_button onClick='document.location=\"/modules/login/login_list.php\" ></div>";
}


?>