/**
 * Users management funtions
 *
 * @version $Id: login_functions.js 58 2011-05-18 12:55:30Z thilo $
 */



function check_login_frm(f) {
	if (f.login_username_txt.value == "" || f.login_pass_txt.value == "") {
		alert('Please first complete the login form!');
		return false;
	}
	
	return true;
}

function checkMail(email_address) {
    var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if (filter.test(email_address)) {
    	return true;
    } else {
    	return false;
    }
}

function check_frm(f) {
	if (f.login_name_txt.value == "" || f.login_username_txt.value == "" || f.login_pass_txt.value == "") {
		alert('Please complete the entire form!');
		return false;
	}
	
	if (!checkMail(f.login_email_txt.value)) {
		alert('Please enter a valid email!');
		return false;
	}
	
	if (f.login_pass_txt.value != f.login_repass_txt.value) {
		alert('Passwords do not match');
		return false;
	}

	return true;
}

function draw_app() {
	app_list = document.getElementById('a_list').value
	wrt = '<table width=100%>';
	u = document.getElementById('users').options

	if (app_list) {
		alist = app_list.split(',')
		for (i=0;i<alist.length;i++) {
			for (x=0;x<u.length; x++) {
				if (u[x].value == alist[i]) wrt += "<tr><td>" + u[x].text + "</td><td width=10px><input type=button value='' class='cross_only_button' onClick='rem_app(" + u[x].value + ")' ></td></tr>"
			}
		}
		wrt += "</table>"
	} else {
		if (appSystem != 'none') wrt = "<tt style='color: blue'>Nobody selected for up-line approvals. Pages edited by this user will go live without approval.<tt>";
	}

	document.getElementById('a_lst').innerHTML = wrt
}

function add_app() {
	alist = new Array();
	app_list = document.getElementById('a_list').value
	u = document.getElementById('users').value
	dna = false

	alist = app_list.split(',')
	for (i=0;i<alist.length;i++) {
		if (alist[i] == u) dna = true
	}

	if (!dna) app_list += "," + u

	document.getElementById('a_list').value = app_list

	draw_app()
}

function rem_app(v) {
	app_list = document.getElementById('a_list').value
	new_list = '';

	alist = app_list.split(',')
	for (i=0;i<alist.length;i++) {
		if (alist[i] == '') continue;
		if (alist[i] != v) new_list += alist[i] + ","
	}

	document.getElementById('a_list').value = new_list

	draw_app()

}

function add_page() {
	alist = new Array();
	owned_pages = document.getElementById('owned_pages').value
	u = document.getElementById('pages').value
	dna = false

	alist = owned_pages.split(',')
	for (i=0;i<alist.length;i++) {
		if (alist[i] == u) dna = true
	}

	if (!dna) owned_pages += "," + u

	document.getElementById('owned_pages').value = owned_pages

	draw_pages()
}

function draw_pages() {
	app_list = document.getElementById('owned_pages').value
	wrt = '<table width=100%>';
	u = document.getElementById('pages').options
	alist = app_list.split(',')
	for (i=0;i<alist.length;i++) {
		for (x=0;x<u.length; x++) {
			if (u[x].value == alist[i]) wrt += "<tr><td>" + u[x].text + "</td><td width=10px><input type=button value='' class='cross_only_button' onClick='rem_page(" + u[x].value + ")' ></td></tr>"
			a = true;
		}
	}
	if (wrt == '<table width=100%>') wrt += "<tr><td colspan=2 style='text-align: center'>-=No Pages Owned=-</td></tr>";

	wrt += "</table>"

	document.getElementById('page_list').innerHTML = wrt
}

function rem_page(v) {
	app_list = document.getElementById('owned_pages').value
	new_list = '';

	alist = app_list.split(',')
	for (i=0;i<alist.length;i++) {
		if (alist[i] != v) new_list += alist[i] + ","
	}

	if (new_list == ',') new_list = ''

	document.getElementById('owned_pages').value = new_list

	draw_pages()
}

function ds_msg() {
	check = document.getElementById('disabled_account').checked

	if (check) {
		document.getElementById('msg').innerHTML = "-=This account is disabled=-";
	} else {
		document.getElementById('msg').innerHTML = "";
	}
}

function clr_field(id,txt,act,css) {
	var cVal = document.getElementById(id).value

	if (act) {
		if (cVal==txt) {
			document.getElementById(id).value = '';
			document.getElementById(id).className = css + ' onFocus'
		}

	} else {
		if (cVal=='') {
			document.getElementById(id).value = txt;
			document.getElementById(id).className = css + ' onBlur'
		}
	}
}