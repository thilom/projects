<?php
/**
 * Expressien of interest form
 *
 * 2015-11-04: Thilo Muller - Created
 */

//Includes
$ajax = 1;
require_once '../../../settings/init.php';
require_once '../../../shared/swiftmailer/lib/swift_required.php';

//Vars
$email_template = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/modules/miscelaneous/html/expression_of_interest.html");
$body['parents_firstname'] = $_POST['parentFirstname'];
$body['parents_lastname'] = $_POST['parentLastname'];
$body['contact_number'] = $_POST['contactNumber'];
$body['email_address'] = $_POST['emailAddress'];
$body['address'] = $_POST['address'];
$body['my_son'] = $_POST['mySon'];
$body['grade'] = $_POST['grade'];
$body['year'] = $_POST['year'];
$body['heard_about'] = $_POST['heardAbout'];
$body['achievements'] = $_POST['achievements'];
$body['agree'] = isset($_POST['TandC'])?'Yes, I understand':'Not Checked';
$body['server'] = $_SERVER['HTTP_HOST'];
$body['timestamp'] = date('d-M-Y H:i:s');


//Assemble multi-line sibling block
$body['siblings'] = '';
foreach ($_POST['sonsName'] as $key=>$data) {
    if (empty($_POST['sonsName'][$key])) continue;
    $body['siblings'] .= "{$_POST['sonsName'][$key]} - {$_POST['dateOfBirth'][$key]} - {$_POST['currentSchool'][$key]}<br>";
}

//Set up email body
$email_template = str_replace('<!-- parents_firstname -->', $body['parents_firstname'], $email_template);
$email_template = str_replace('<!-- parents_lastname -->', $body['parents_lastname'], $email_template);
$email_template = str_replace('<!-- contact_number -->', $body['contact_number'], $email_template);
$email_template = str_replace('<!-- email_address -->', $body['email_address'], $email_template);
$email_template = str_replace('<!-- address -->', $body['address'], $email_template);
//$email_template = str_replace('<!-- suburb -->', $body['suburb'], $email_template);
$email_template = str_replace('<!-- my_son -->', $body['my_son'], $email_template);
$email_template = str_replace('<!-- grade -->', $body['grade'], $email_template);
$email_template = str_replace('<!-- year -->', $body['year'], $email_template);
$email_template = str_replace('<!-- heard_about -->', $body['heard_about'], $email_template);
$email_template = str_replace('<!-- achievements -->', $body['achievements'], $email_template);
$email_template = str_replace('<!-- agree -->', $body['agree'], $email_template);
$email_template = str_replace('<!-- server -->', $body['server'], $email_template);
$email_template = str_replace('<!-- timestamp -->', $body['timestamp'], $email_template);
$email_template = str_replace('<!-- siblings -->', $body['siblings'], $email_template);

//echo $email_template;
//Send
$transport = Swift_SmtpTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);
$message = Swift_Message::newInstance()
    ->setSubject('Expression of Interest (via PBHS website)')
    ->setFrom(array($body['email_address'] => "{$body['parents_firstname']} {$body['parents_lastname']}"))
    ->setTo(array('info@boyshigh.com' => 'Pretoria Boys High School'))
    ->setBody('This message requires an HTML capable email client')
    ->addPart($email_template, 'text/html');

$mailer->send($message);