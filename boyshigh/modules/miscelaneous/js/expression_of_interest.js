
$(document).ready(function() {
    var d = new Date();
    //d.setFullYear(d.getFullYear() + 1);
    //alert(d.getFullYear());
    $('#year').append($('<option>', {
        value: d.getFullYear() + 1,
        text: d.getFullYear() + 1
    }));

    $('#year').append($('<option>', {
        value: d.getFullYear() + 2,
        text: d.getFullYear() + 2
    }));
});

function sendForm() {

    var requiredFields = ['parentFirstname', 'parentLastname', 'contactNumber', 'emailAddress', 'address', 'mySon', 'grade', 'year'];
    var error = false;

    //Check for incomplete fields
    for (var x = 0; x<requiredFields.length; x++) {
        if ($('#' + requiredFields[x]).val() == '') {
            error = true;
            $('#' + requiredFields[x]).parent().addClass('has-error');
        } else {
            $('#' + requiredFields[x]).parent().removeClass('has-error');
        }
    }

    //Check son information
    if ($('#sonsName').val() == '') {
        error = true;
        $('#detailsBlock').addClass('has-error');
        $('#sonsLabel').addClass('text-danger');
    } else {
        $('#detailsBlock').removeClass('has-error');
        $('#sonsLabel').removeClass('text-danger');
    }

    //Check T&C
    if ($('#TandC').is(':checked')) {
        $('#TandCLabel').removeClass('text-danger');
    } else {
        error = true;
        $('#TandCLabel').addClass('text-danger');
    }

    //Check Grade and Year
    if ($('#grade').val() == '' || $('#year').val() == '') {
        $('#availabilityLabel').addClass('text-danger');
        if ($('#grade').val() == '') {
            $('#gradeBlock').addClass('text-danger');
        }
        if ($('#year').val() == '') {
            $('#yearBlock').addClass('text-danger');
        }
    } else {
        $('#availabilityLabel').removeClass('text-danger');
        $('#gradeBlock').removeClass('text-danger');
        $('#yearBlock').removeClass('text-danger');
    }

    //Check valid email
    if (!checkemail($('#emailAddress').val())) {
        error = true;
        $('#emailAddress').parent().addClass('has-error');
    } else {
        $('#emailAddress').parent().removeClass('has-error');
    }

    if (error === true) {
        $('#errorMessage').slideDown();
    } else {
        $('#errorMessage').slideUp();
        $.ajax({
            url: '/modules/miscelaneous/ajax/expression_of_interest.ajax.php',
            method: 'post',
            data: $('#eoiForm').serialize(),
            complete: function (data) {
                $('#eoiForm').slideUp(function() {
                    $('#resultBlock').slideDown();
                });

            }
        });
    }

}


function addLine() {

    var row = '<div class="row"><div class="col-md-4"><div class="form-group"><input type="text" class="form-control" name="sonsName[]" value="" placeholder="Name" onkeyup="addLine()"></div></div>';
    row += '<div class="col-md-4"><div class="form-group"><input type="text" class="form-control" name="dateOfBirth[]" value="" placeholder="Date of Birth"></div></div>';
    row += '<div class="col-md-4"><div class="form-group"><input type="text" class="form-control" name="currentSchool[]" id="currentSchool" value="" placeholder="Current School"></div></div></div>';

    //Add line
    //alert($('#detailsBlock').children(':last').find('INPUT').attr('name'));
    if ($('#detailsBlock').children(':last').find('INPUT').val() == '') {

    } else {
        $('#detailsBlock').append(row);
    }
}

function checkemail(str){
    var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (filter.test(str))
        testresults=true
    else{
        testresults=false
    }
    return (testresults)
}