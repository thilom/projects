<?php
/**
 * Index.php
 *
 * @package SCRUM
 * @version $Id: index.php 62 2011-05-24 04:36:46Z thilo $
 *
 */

//Includes
require_once $_SERVER['DOCUMENT_ROOT']."/settings/init.php";

// get fluid items
include $_SERVER['DOCUMENT_ROOT']."/shared/directory_item.php";

//Initialize variables
$search_words = '';
$template_areas = array();
$template_list = '';
if (isset($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page = 'home';
}

$statement = "SELECT p.page_id, p.template_id, t.template_file, page_members_only
FROM {$GLOBALS['db_prefix']}_pages as p, {$GLOBALS['db_prefix']}_templates as t
WHERE p.page_name=:page_name && p.template_id=t.template_id";
$sql_pages = $GLOBALS['dbCon']->prepare($statement);
$sql_pages->bindParam(':page_name', $page);
$sql_pages->execute();
$sql_pages_data = $sql_pages->fetch();
$sql_pages->closeCursor();
$page_id = $sql_pages_data['page_id'];

//Check for member only page
if ($sql_pages_data['page_members_only'] == '1') {
	if (!isset($_SESSION['user']['type'])) {

		$statement = "SELECT value
					FROM {$GLOBALS['db_prefix']}_settings
					WHERE tag='login_page'
					LIMIT 1";
		$sql_login = $GLOBALS['dbCon']->prepare($statement);
		$sql_login->execute();
		$sql_login_data = $sql_login->fetch();
		$sql_login->closeCursor();

		$page = $sql_login_data['value'];
		$statement = "SELECT p.page_id, p.template_id, t.template_file, page_members_only
						FROM {$GLOBALS['db_prefix']}_pages as p, {$GLOBALS['db_prefix']}_templates as t
						WHERE p.page_name=:page_name && p.template_id=t.template_id";
		$sql_pages = $GLOBALS['dbCon']->prepare($statement);
		$sql_pages->bindParam(':page_name', $page);
		$sql_pages->execute();
		$sql_pages_data = $sql_pages->fetch();
		$sql_pages->closeCursor();
		$page_id = $sql_pages_data['page_id'];
	}
}

//read template areas
$template = file_get_contents(SITE_ROOT . "/templates/{$sql_pages_data['template_file']}");
include_once $_SERVER['DOCUMENT_ROOT']."/modules/content_manager/area_public.php";

//get Meta Tags
$SQL_statement = "SELECT page_title, page_description, page_keywords FROM {$GLOBALS['db_prefix']}_pages WHERE page_id='$page_id'";
if (($SQL_meta = mysql_query($SQL_statement)) === FALSE) trigger_error(mysql_errro(), E_USER_ERROR);
list($page_title, $page_description, $page_keywords) = mysql_fetch_array($SQL_meta);

//Replace Meta tags
if (!empty($page_title)) $template = preg_replace('@<title>(.*)</title>@i', "<title>$page_title</title>", $template);
if (!empty($page_keywords)) $template = preg_replace('@<meta(.*)keywords(.*)>@i', "<meta name='keywords' content='$page_keywords'>", $template);
if (!empty($page_description)) $template = preg_replace('@<meta(.*)description(.*)>@i', "<meta name='description' content='$page_description'>", $template);

// javascript
//echo "<script src=/shared/G.js></script>";

getExt(SITE_ROOT."/i/fluid","css");
if($itemname)echo "<link rel=stylesheet href=/i/fluid/".$itemname.">";

//Include Google analytics code
$sql="SELECT value FROM {$GLOBALS['db_prefix']}_settings WHERE tag='google_analytics'";
$sql_analytics=mysql_query($sql);
list($code)=mysql_fetch_array($sql_analytics);
if(!empty($code)) {
	$code=stripslashes(trim($code));
	if(strtolower(substr($code,0,7))!="<script")
		$code="<script>".$code."</script>";
	$body_loc=stripos($template,"</body");
	$part1=substr($template,0,$body_loc);
	$part2=substr($template,$body_loc);
	$template=$part1.$code.$part2;
}

//Serve Page
echo $template;

?>
