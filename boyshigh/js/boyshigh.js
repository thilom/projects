
function openProduct(id, title) {
    $.ajax({
        url: '/modules/clothing/ajax/get_product.ajax.php?id=' + id,
        success: function(data) {
            $('#productDialog').html(data);

            $('#productDialog').dialog({
                title: title,
                modal: true,
                resizable: false,
                width:'auto'
            });

//            $('.bigImg').load(function() {
//                var w = $('.bigImg').width();
//                $('#productDialog').dialog('option', 'width', w);
//            });

        }
    });



}