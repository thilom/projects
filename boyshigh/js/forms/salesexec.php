<?php

  $title=$_POST[title];
  $name=$_POST[name];
  $company=$_POST[company];
  $email=$_POST[email];
  $tel=$_POST[tel];
  $hear=$_POST[hear];
  $message=$_POST[message];
  
  function dorow($label,$value)
  {
    if ($value)
    return "<tr valign=\"top\"><td class=\"head\">$label</td><td class=\"sub\">$value</td></tr>\r\n";
  }
    
  //generate email to client
  $msg="<html>\r\n";
  $msg.="<head>\r\n";
  $msg.="<style type=\"text/css\">\r\n";
  $msg.="td,body {font-family: arial; font-size: 12px; padding: 10px; }\r\n";
  $msg.=".head {background-color: navy; color: white; font-weight: bold }\r\n";
  $msg.=".sub { background-color: lightblue; }\r\n";
  $msg.="</style>\r\n";
  $msg.="<body>";
  $msg.="<table>";
  $msg.=dorow("Brochures?: ","$brochures");
  $msg.=dorow("Casestudy?: ","$casestudy");
  $msg.=dorow("Factsheet?: ","$factsheet");
  $msg.=dorow("Presentation?: ","$presentation");
  $msg.=dorow("Extra Item?: ","$extraitem");
  $msg.=dorow("Title: ","$title");
  $msg.=dorow("Full Name: ","$name");
  $msg.=dorow("Company: ","$company");
  $msg.=dorow("Telephone: ","$tel");
  $msg.=dorow("E-Mail: ","<a href=\"mailto:$email\">$email</a>");  
  $msg.=dorow("How did you hear about Savvy Bizz: ","$hear");
  $msg.=dorow("Message: ","$message");
  $msg.="</table>\r\n";
  $msg.="</body>\r\n";
  $msg.="</html>";

  $headers="From: SavvyBizz Website <gwendy@savvybizz.com>\r\n";
  $headers.="MIME-Version: 1.0\r\n";
  $headers.="Content-type: text/html; charset=iso-8859-1\r\n";

  $clientemail="gwendy@savvybizz.com";
  mail($clientemail,"SavvyBizz Web Enquiry",$msg,$headers);

  echo "<script type=\"text/javascript\">alert('Thank you for your feedback. Your enquiry has been sent!');";
  echo "location='../../index.php';</script>";

?>
