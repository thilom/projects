<?php
error_reporting(0);
session_start();
include_once("include/include.php");

if ( $SETTINGS["useCookie"] == false and isset($_SESSION["PHPEventCalendarLoGIn"])) $temp_sid=$_SESSION["PHPEventCalendarLoGIn"];
elseif (isset($_COOKIE["PHPEventCalendarLoGIn"])) $temp_sid=$_COOKIE["PHPEventCalendarLoGIn"];

$md_sum=md5($SETTINGS["admin_username"].$SETTINGS["admin_password"]);
$md_res=substr($temp_sid,0,strlen($md_sum));
if (strcmp($md_sum,$md_res)==0) {
	$ts=substr($temp_sid,strlen($md_sum));
	if ($ts>time()) $isLogged = true;
}

if ( $isLogged ){

$calendarId = $_REQUEST["cid"];
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");

$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];

$prev_year = $cYear;
$next_year = $cYear; 

$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}

$sql = "SELECT * FROM ".$TABLES["Calendars"]." WHERE id='".SaveToDB($_REQUEST["cid"])."'";
$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
$Calendar = mysql_fetch_assoc($sql_result);
$CalendarOptions = unserialize(ReadHTMLFromDB($Calendar["options"]));

$calMonthName = $Months[$cMonth];
$formatMonth = getFormatedMonthYear($calMonthName,$cYear);

$FontFamily = $Fonts[$CalendarOptions["FontFamily"]];

$MonthFontSize = $FontSize[$CalendarOptions["MonthFontSize"]];
$MonthFontStyle = $Styles[$CalendarOptions["MonthFontStyle"]];

$WeekDaysFontSize = $FontSize[$CalendarOptions["WeekDaysFontSize"]];
$WeekDaysFontStyle = $Styles[$CalendarOptions["WeekDaysFontStyle"]];

$DayCellsEventsFontSize = $FontSize[$CalendarOptions["DayCellsEventsFontSize"]];
$DayCellsEventsFontStyle = $Styles[$CalendarOptions["DayCellsEventsFontStyle"]];

$DayCellsWithoutEventsFontSize = $FontSize[$CalendarOptions["DayCellsWithoutEventsFontSize"]];
$DayCellsWithoutEventsFontStyle = $Styles[$CalendarOptions["DayCellsWithoutEventsFontStyle"]];

$borderSize = $BorderSize[$CalendarOptions["borderSize"]];
$innerBorderSize = $BorderSize[$CalendarOptions["innerBorderSize"]];

$cellBorder = "border-collapse:collapse; border:solid ".$innerBorderSize."px #" . $CalendarOptions["innerBorderColor"] . "; ";

$EventTitleFontSize = $FontSize[$CalendarOptions["EventTitleFontSize"]];
?>
<table width="574px" border="0" cellspacing="0" cellpadding="2" style="border:solid <?php echo $borderSize; ?>px #<?php echo $CalendarOptions["borderColor"]; ?>">
  <tr>
    <td align="center" bgcolor="#<?php echo $CalendarOptions["MonthBGColor"]; ?>"><a href="javascript:ajaxpage('<?php echo $SETTINGS["installFolder"]."build_calendar.php?month=". $prev_month . "&year=" . $prev_year. "&cid=".$calendarId; ?>','EventCalendar<?php echo $calendarId; ?>','get');" style="font-family:<?php echo $FontFamily; ?>; color:#<?php echo $CalendarOptions["MonthFontColor"]; ?>; font-size:<?php echo $MonthFontSize; ?>px; <?php echo $MonthFontStyle; ?>"><?php echo ReadHTMLFromDB($CalendarOptions["leftArrow"]); ?></a></td>
    <td colspan="5" align="center" bgcolor="#<?php echo $CalendarOptions["MonthBGColor"]; ?>" style="font-family:<?php echo $FontFamily; ?>; color:#<?php echo $CalendarOptions["MonthFontColor"]; ?>; font-size:<?php echo $MonthFontSize; ?>px; <?php echo $MonthFontStyle; ?>; padding:10px"><?php echo $formatMonth; ?></td>
    <td align="center" bgcolor="#<?php echo $CalendarOptions["MonthBGColor"]; ?>"><a href="javascript:ajaxpage('<?php echo $SETTINGS["installFolder"]."build_calendar.php?month=". $next_month . "&year=" . $next_year. "&cid=".$calendarId; ?>','EventCalendar<?php echo $calendarId; ?>','get');" style="font-family:<?php echo $FontFamily; ?>; color:#<?php echo $CalendarOptions["MonthFontColor"]; ?>; font-size:<?php echo $MonthFontSize; ?>px; <?php echo $MonthFontStyle; ?>"><?php echo ReadHTMLFromDB($CalendarOptions["rightArrow"]); ?></a></td>
  </tr>
  <tr>
  	<?php
		$weekStartDay = $CalendarOptions["startDay"];
		for($j=0;$j<7;$j++){
			$ind = $weekStartDay+$j;
			if($ind>6) $ind = $ind - 7;
			echo "<td align='center' bgcolor='#" . $CalendarOptions["WeekDaysBGColor"] . "' style='font-family:".$FontFamily."; color:#".$CalendarOptions["WeekDaysFontColor"]."; font-size:12px; ".$WeekDaysFontStyle."; width:82px; padding-top:5px; padding-bottom:5px' >".$Days[$ind]."</td>";
		}	
	?>
  </tr>  
  
<?php
	$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
	$maxday = date("t",$timestamp);
	$thismonth = getdate ($timestamp);
	$startday = $thismonth['wday']; 

	$offset = $startday - $weekStartDay;
	if($offset<0) $offset = 7 + $offset;

	$Events = getEvents($calendarId, $cMonth, $cYear,'Hide');
	$DailyColors = getColors($calendarId, $cMonth, $cYear);

	for ($i=0; $i<($offset+$maxday); $i++) {
		if(($i % 7) == 0 ) echo "<tr>";
		if($i < $offset) {
			echo "<td bgcolor='#" . $CalendarOptions["DayCellsEmptyBGColor"] . "' style='".$cellBorder."'>&nbsp;</td>";
		} else {
			$day = $i - $offset + 1;
			$sqlMonth = ($cMonth<10) ? "0".$cMonth : $cMonth;
			$sqlDay = ($day<10) ? "0".$day : $day;
			$sqlDate = $cYear."-".$sqlMonth."-".$sqlDay;
			
			$MouseEvent = '';
			$EventsSummary = '';

			if(isset($DailyColors[$sqlDate])) $backgroundColor = 'background-color:#' . ReadFromDB($DailyColors[$sqlDate]["dayColor"]);

			if (isset($Events[$sqlDate])) {

				if($CalendarOptions["SummaryPosition"]=="tooltip") {
					$MouseEvent .= ' onMouseOver="ShowToolTip(\'d'.$day.'\')" onMouseOut="HideToolTip(\'d'.$day.'\')"';
				};	
					
				if(!isset($DailyColors[$sqlDate])) $backgroundColor = 'background-color:#' . $CalendarOptions["DayCellsWithEventsBGColor"];
				$dayFontColor = 'color:#' . $CalendarOptions["DayCellsEventsFontColor"];
				$dayFontSize = $DayCellsEventsFontSize;
				$dayFontStyle = $DayCellsEventsFontStyle;
					
			} else {
				if(!isset($DailyColors[$sqlDate])) $backgroundColor = "background-color:#" . $CalendarOptions["DayCellsWithoutEventsBGColor"];
				$dayFontColor = 'color:#' . $CalendarOptions["DayCellsWithoutEventsFontColor"];
				$dayFontSize = $DayCellsWithoutEventsFontSize;
				$dayFontStyle = $DayCellsWithoutEventsFontStyle;
			}
			
			$onClick = ' onClick="ajaxpage(\'date_events.php?dt='.strtotime($cYear.'-'.$cMonth.'-'.$day).'&cid='.$calendarId.'\',\'DateEvents\',\'get\');"'; 
			echo '<td align="left" valign="top" style="cursor:pointer; '.$cellBorder.$backgroundColor.'; height:50px" '.$MouseEvent.' '.$onClick.'>
					<span  style="font-family:'.$FontFamily.'; font-size:'.$dayFontSize.'px; '.$dayFontStyle.' '.$dayFontColor.'">'.$day.'</span>'.
				 '</td>';
		}	 
		if(($i % 7) == 6 ) echo "</tr>";
	}
	
	for ($i=($offset+$maxday); $i<42; $i++){
		if(($i % 7) == 0 ) echo "<tr>";
		echo "<td bgcolor='#" . $CalendarOptions["DayCellsEmptyBGColor"] . "' style='cursor:pointer; ".$cellBorder."; height:50px' >&nbsp;</td>";
		if(($i % 7) == 6 ) echo "</tr>";
	}
?>
</table>


<?php
} else echo $PHP_EVENT_LANG['YouNeedToLoginFirst']; 
?>