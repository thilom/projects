<?php defined( '_PHPEventCalendar' ) or die( 'Restricted access' ); ?>
<div style="background-image:url(images/menu-back.jpg); background-repeat:no-repeat; width:175; height:328; padding-top:15px; padding-left:15px; vertical-align:top">

<div style="height:27px; width:145px; background-image:url(images/<?php if ($_REQUEST["ac"]=='view') echo 'menu-btn-active.jpg'; else echo 'menu-btn.jpg'; ?>); background-repeat:no-repeat; padding-top:4px; padding-left:10px">
<a href="admin.php?ac=view&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink" <?php if ($_REQUEST["ac"]=='view') echo ' style="color:#FFFFFF" '; ?>><img src="images/menu-calendar.png" alt="View Calendar" title="View Calendar" name="Image1" width="23" height="21" border="0" align="absmiddle" id="Image1" /> <?php echo $PHP_EVENT_LANG['Calendar']; ?></a>
</div>

<div style="height:27px; width:145px; background-image:url(images/<?php if ($_REQUEST["ac"]=='add_event' or $_REQUEST["ac"]=='find_event' or $_REQUEST["ac"]=='export_events') echo 'menu-btn-active.jpg'; else echo 'menu-btn.jpg'; ?>); background-repeat:no-repeat; padding-top:4px; padding-left:10px; margin-top:7px">
<img src="images/menu-events.png" alt="Events" title="Events" name="Image1" width="23" height="21" border="0" align="absmiddle" id="Image1" /> <strong<?php if ($_REQUEST["ac"]=='add_event' or $_REQUEST["ac"]=='find_event' or $_REQUEST["ac"]=='export_events') echo ' style="color:#FFFFFF" '; ?>><?php echo $PHP_EVENT_LANG['Events']; ?></strong> </div>

<div style="height:22px; width:145px; padding-left:30px; margin-top:5px;">
<img src="images/menu-bullet.png" width="3" height="2" align="absmiddle" style="margin-right:10px" />
<a href="admin.php?ac=add_event&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink"<?php if ($_REQUEST["ac"]=='add_event') echo ' style="text-decoration:underline" '; ?>><?php echo $PHP_EVENT_LANG['New']; ?></a>
</div>

<div style="height:22px; width:145px; padding-left:30px;">
<img src="images/menu-bullet.png" width="3" height="2" align="absmiddle" style="margin-right:10px" />
<a href="admin.php?ac=find_event&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink"<?php if ($_REQUEST["ac"]=='find_event') echo ' style="text-decoration:underline" '; ?>><?php echo $PHP_EVENT_LANG['Find']; ?></a>
</div>

<div style="height:22px; width:145px; padding-left:30px;">
<img src="images/menu-bullet.png" width="3" height="2" align="absmiddle" style="margin-right:10px" />
<a href="admin.php?ac=export_events&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink"<?php if ($_REQUEST["ac"]=='export_events') echo ' style="text-decoration:underline" '; ?>><?php echo $PHP_EVENT_LANG['Export']; ?></a>
</div>


<div style="height:27px; width:145px; background-image:url(images/<?php if ($_REQUEST["ac"]=='options') echo 'menu-btn-active.jpg'; else echo 'menu-btn.jpg'; ?>); background-repeat:no-repeat; padding-top:4px; padding-left:10px; margin-top:7px">
<img src="images/menu-options.png" alt="Options" title="Options" name="Image1" width="23" height="21" border="0" align="absmiddle" id="Image1" /> <strong<?php if ($_REQUEST["ac"]=='options') echo ' style="color:#FFFFFF" '; ?>><?php echo $PHP_EVENT_LANG['Options']; ?></strong> </div>

<div style="height:22px; width:145px; padding-left:30px; margin-top:5px;">
<img src="images/menu-bullet.png" width="3" height="2" align="absmiddle" style="margin-right:10px" />
<a href="admin.php?ac=options&sac=general&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink"<?php if ($_REQUEST["sac"]=='general') echo ' style="text-decoration:underline" '; ?>><?php echo $PHP_EVENT_LANG['General']; ?></a>
</div>

<div style="height:22px; width:145px; padding-left:30px;">
<img src="images/menu-bullet.png" width="3" height="2" align="absmiddle" style="margin-right:10px" />
<a href="admin.php?ac=options&sac=colors&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink"<?php if ($_REQUEST["sac"]=='colors') echo ' style="text-decoration:underline" '; ?>><?php echo $PHP_EVENT_LANG['Fonts & Colors']; ?></a>
</div>


<div style="height:27px; width:145px; background-image:url(images/<?php if ($_REQUEST["ac"]=='html') echo 'menu-btn-active.jpg'; else echo 'menu-btn.jpg'; ?>); background-repeat:no-repeat; padding-top:4px; padding-left:10px; margin-top:7px">
<a href="admin.php?ac=html&cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink" <?php if ($_REQUEST["ac"]=='html') echo ' style="color:#FFFFFF" '; ?>><img src="images/menu-install.png" alt="View Calendar" title="View Calendar" name="Image1" width="23" height="21" border="0" align="absmiddle" id="Image1" /> <?php echo $PHP_EVENT_LANG['Install']; ?></a>
</div>


<div style="height:27px; width:145px; background-image:url(images/menu-btn.jpg); background-repeat:no-repeat; padding-top:4px; padding-left:10px; margin-top:7px">
<a href="preview.php?cid=<?php echo $_REQUEST["cid"]; ?>" class="menuLink" target="_blank"><img src="images/menu-preview.png" alt="View Calendar" title="View Calendar" name="Image1" width="23" height="21" border="0" align="absmiddle" id="Image1" /> <?php echo $PHP_EVENT_LANG['Preview']; ?></a>
</div>


</div>
<div style="width:175" align="center">
<img src="images/video.gif" width="21" height="13" align="baseline" /> <a href="http://www.phpcalendarscripts.com/video/event2.0/" target="_blank" style="color:#0063a8"><?php echo $PHP_EVENT_LANG['VideoTutorials']; ?></a></div>