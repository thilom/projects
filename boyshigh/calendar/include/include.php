<?php
$SETTINGS["useCookie"] = false;
$SETTINGS["pageSize"] = 20;
$TABLES["Calendars"] = 'php_event_calendars';
$TABLES["Days"] = 'php_event_days';
$TABLES["Events"] = 'php_event_events';
//////////////////
//////////////////
//////////////////
////////////////// DO NOT CHANGE BELOW
//////////////////
//////////////////
$SETTINGS["version"] = '2.0';
$SETTINGS["scriptid"] = '28';

include("options.php");
include("lang.php");

$defaultMonthFormat = 1;
$MonthFormats["1"] = "Month Year";		
$MonthFormats["2"] = "Month, Year";
$MonthFormats["3"] = "Year Month";
$MonthFormats["4"] = "Year, Month";
$defaultDateFormat = 1;
// DateFormat[1] (yyyy-mm-dd) is used by MySQL for date fields. DO NOT CHANGE THE FORMAT
$DateFormats["1"] = "yyyy-mm-dd";	
$DateFormats["2"] = "yyyy/mm/dd";
$DateFormats["3"] = "yyyy.mm.dd";
$DateFormats["4"] = "mm-dd-yyyy";
$DateFormats["5"] = "mm/dd/yyyy";
$DateFormats["6"] = "mm.dd.yyyy";
$DateFormats["7"] = "dd-mm-yyyy";
$DateFormats["8"] = "dd/mm/yyyy";
$DateFormats["9"] = "dd.mm.yyyy";
$DateSeparator["1"] = "-";	
$DateSeparator["2"] = "/";
$DateSeparator["3"] = ".";
$DateSeparator["4"] = "-";
$DateSeparator["5"] = "/";
$DateSeparator["6"] = ".";
$DateSeparator["7"] = "-";
$DateSeparator["8"] = "/";
$DateSeparator["9"] = ".";
$defaultTimeFormat = 1;
$TimeFormats["1"] = "12 hour";
$TimeFormats["2"] = "24 hour";
$defaultFont = 1;
$Fonts["1"]  = "Arial";					
$Fonts["2"]  = "Arial Black";
$Fonts["3"]  = "Book Antiqua";
$Fonts["4"]  = "Century";
$Fonts["5"]  = "Century Gothic";
$Fonts["6"]  = "Comic Sans MS";
$Fonts["7"]  = "Courier";
$Fonts["8"]  = "Courier New";
$Fonts["9"]  = "Impact";
$Fonts["10"] = "Lucida Console";
$Fonts["11"] = "Lucida Sans Unicode";
$Fonts["12"] = "Monotype Corsiva";
$Fonts["13"] = "Modern";
$Fonts["14"] = "Sans Serif";
$Fonts["15"] = "Serif";
$Fonts["16"] = "Small fonts";
$Fonts["17"] = "Symbol";
$Fonts["18"] = "Tahoma";
$Fonts["19"] = "Times New Roman";
$Fonts["20"] = "Verdana";
$defaultStyleFormat	= "Normal";
$Styles["Normal"] 	   = "font-weight: normal; font-style: normal; ";	
$Styles["Bold"] 	   = "font-weight: bold; ";
$Styles["Italic"] 	   = "font-style: italic; ";
$Styles["Underline"]   = "text-decoration: underline; ";
$Styles["Bold Italic"] = "font-weight: bold; font-style: italic; ";
$defaultFontSize = 2;
$FontSize["1"] = 10;
$FontSize["2"] = 12;
$FontSize["3"] = 14;
$FontSize["4"] = 16;
$FontSize["5"] = 18;
$FontSize["6"] = 20;
$FontSize["7"] = 24;
$FontSize["8"] = 28;
$FontSize["9"] = 30;
$defaultBorderSize = 1; // Cell border size
$BorderSize["0"] = 0;
$BorderSize["1"] = 1;
$BorderSize["2"] = 2;
$BorderSize["3"] = 3;
$BorderSize["4"] = 4;
$BorderSize["5"] = 5;
$BorderSize["6"] = 6;
$BorderSize["7"] = 7;
$BorderSize["8"] = 8;
$BorderSize["9"] = 9;

if ($install != '1') {
	$connection = mysql_connect($SETTINGS["hostname"], $SETTINGS["mysql_user"], $SETTINGS["mysql_pass"]) or die ('request "Unable to connect to MySQL server."');
	$db = mysql_select_db($SETTINGS["mysql_database"], $connection) or die ('request "Unable to select database."');
};

function SaveToDB($str) {
	return mysql_real_escape_string(utf8_encode($str));
};

function ReadFromDB($str) {
	return htmlentities(utf8_decode(stripslashes($str)),ENT_COMPAT,'UTF-8');
};

function ReadHTMLFromDB($str) {
	return utf8_decode(stripslashes($str));
};

function getFormatedMonthYear($month,$year){
	global $defaultMonthFormat, $MonthFormats, $CalendarOptions;
	$calMonthFormat = $CalendarOptions["monthYearFormat"];
	if(!isset($calMonthFormat)) $calMonthFormat = $defaultMonthFormat;
	$format = $MonthFormats[$calMonthFormat];

	$patterns[0] = '/Month/';
	$patterns[1] = '/Year/';
	
	$replace[0] = $month;
	$replace[1] = $year;
	
	$formatedMonth = preg_replace($patterns, $replace, $format);
	return $formatedMonth;
}

function getFormatedTime($original_time){
	global $CalendarOptions;
	return (($CalendarOptions["timeFormat"]==1) ? date("g:i a",strtotime(substr($original_time,0,5))) : substr($original_time,0,5));
}

function getFormatedDate($date_as_long){
	global $CalendarOptions, $defaultDateFormat, $DateFormats;

	$calDateFormat = $CalendarOptions["dateFormat"];
	if(!isset($calDateFormat)) $calMonthFormat = $defaultDateFormat;

	$format = $DateFormats[$calDateFormat];

	$patterns[0] = '/dd/';
	$patterns[1] = '/mm/';
	$patterns[2] = '/yyyy/';
	
	$replace[0] = date("d",$date_as_long);
	$replace[1] = date("m",$date_as_long);
	$replace[2] = date("Y",$date_as_long);
	
	$formatedDate = preg_replace($patterns, $replace, $format);
	return $formatedDate;	
}

function getEvents($cid, $month, $year, $show){
	global $TABLES, $connection;

	if ($show=='Show') $show = " AND show_event = 'Show'"; else $show='';
	
	$eventArr = array();
	$lastDay = date("t",mktime(0,0,0,$month,1,$year));
	$sql = "SELECT * FROM ".$TABLES["Events"]." 
			WHERE calendarId='".SaveToDB($cid)."' $show
			AND dt BETWEEN '".SaveToDB($year."-".$month."-01")."' AND '".SaveToDB($year."-".$month."-".$lastDay)."' 
			ORDER BY dt ASC, startTime ASC, endTime ASC";
	$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);	
	while($Event = mysql_fetch_assoc($sql_result)){
		$eventArr[$Event["dt"]][] = $Event;
	}
	return $eventArr;
}

function getColors($cid, $month, $year){
	global $TABLES, $connection;
	
	$colorsArr = array();
	$lastDay = date("t",mktime(0,0,0,$month,1,$year));
	$sql = "SELECT * FROM ".$TABLES["Days"]." WHERE calendarId='".SaveToDB($cid)."' AND dt BETWEEN '".SaveToDB($year."-".$month."-01")."' AND '".SaveToDB($year."-".$month."-".$lastDay)."' ORDER BY dt ASC";
	$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);	
	while($Color = mysql_fetch_assoc($sql_result)){
		$colorsArr[$Color["dt"]] = $Color;
	}
	return $colorsArr;
}

function getDateFromCalendarFormat($dt){
	global $DateSeparator, $DateFormats, $CalendarOptions;
	
	$dtArr = array();
	$calDateFormat = $CalendarOptions["dateFormat"];
	$dateParts = explode($DateSeparator[$calDateFormat],$dt);
	$templateParts = explode($DateSeparator[$calDateFormat],$DateFormats[$calDateFormat]);
	
	for($i=0;$i<count($templateParts);$i++) if(($templateParts[$i]=="yyyy") or ($templateParts[$i]=="yy")) $dtArr[0] = $dateParts[$i];
	for($i=0;$i<count($templateParts);$i++) if($templateParts[$i]=="mm") $dtArr[1] = $dateParts[$i];
	for($i=0;$i<count($templateParts);$i++) if($templateParts[$i]=="dd") $dtArr[2]= $dateParts[$i];
	return $dtArr;
}

function oppColor($c, $inverse=false){
	$temp[0] = $c[0].$c[1];
	$temp[1] = $c[2].$c[3];
	$temp[2] = $c[4].$c[5];
	$temp[0] = hexdec($temp[0]);
	$temp[1] = hexdec($temp[1]);
	$temp[2] = hexdec($temp[2]);
	if (array_sum($temp) > 255*1.5) {
		return '000000';
	} else {
		return 'FFFFFF';
	};
};
?>