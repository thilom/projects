<?php defined( '_PHPEventCalendar' ) or die( 'Restricted access' ); ?>
<p><?php echo $PHP_EVENT_LANG['InstallInfo']; ?></p>
<p><strong><?php echo $PHP_EVENT_LANG['InstallHead']; ?>:</strong></p>
<textarea name="html" style="width:96%" rows="3" >
<link href="<?php echo $SETTINGS["installFolder"]; ?>calendar.css" rel="stylesheet" type="text/css" />
</textarea>
<p><strong><?php echo $PHP_EVENT_LANG['InstallCode']; ?>:</strong></p>
<p>
<textarea name="html" style="width:96%" rows="3" >
<script language="javascript" src="<?php echo $SETTINGS["installFolder"]; ?>load.php"></script>
</textarea>
</p>
<p>
<em><?php echo $PHP_EVENT_LANG['InstallSameDomain']; ?></em>
</p>