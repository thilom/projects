<?php defined( '_PHPEventCalendar' ) or die( 'Restricted access' ); ?>

<?php
if ($_REQUEST["sac"]=='general') {
?>
<form action="admin.php" method="post" name="OptionsForm" id="OptionsForm" style="padding:0px; margin:0px">
  <input type="hidden" name="ac" value="save_options" />
  <input type="hidden" name="sac" value="general" />
  <input type="hidden" name="cid" value="<?php echo $_REQUEST["cid"]; ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer" style="margin-top:10px">
            <tr>
              <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['OptionsGeneral']; ?></td>
            </tr>
            <tr>
              <td colspan="2" bgcolor="#f6f6f6"><em><?php echo $PHP_EVENT_LANG['OptionsGeneralInfo']; ?></em></td>
            </tr>
            <tr>
              <td width="30%" bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsWidth']; ?>:</td>
			  <td width="68%" bgcolor="#f6f6f6"><input name="width" type="text" id="width" size="4" maxlength="4" value="<?php echo $CalendarOptions["width"]; ?>" />
			    pixels

<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpWidth')" onMouseOut="HideToolTip('HelpWidth')" />
<div class="PHPCalendarLogoutHelp" id="HelpWidth"><?php echo $PHP_EVENT_LANG['Help_11']; ?></div>
                
                
                </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsHeight']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="height" type="text" id="height" size="4" maxlength="4" value="<?php echo $CalendarOptions["height"]; ?>" />
			    pixels

<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpHeight')" onMouseOut="HideToolTip('HelpHeight')" />
<div class="PHPCalendarLogoutHelp" id="HelpHeight"><?php echo $PHP_EVENT_LANG['Help_12']; ?></div>
                
                </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsWeekStartDay']; ?>:</td>
		    <td bgcolor="#f6f6f6"><select name="startDay" id="startDay" style="width:100px">
                  <?php
					foreach($Days as $key=>$value){
						if($CalendarOptions["startDay"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                </select>             
                
<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpStartDay')" onMouseOut="HideToolTip('HelpStartDay')" />
<div class="PHPCalendarLogoutHelp" id="HelpStartDay"><?php echo $PHP_EVENT_LANG['Help_13']; ?></div>

                 </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsMonthYearFormat']; ?>:</td>
		    <td bgcolor="#f6f6f6"><select name="monthYearFormat" id="monthYearFormat" style="width:100px">
                  <?php
					foreach($MonthFormats as $key=>$value){
						if($CalendarOptions["monthYearFormat"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                </select>             
                
<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpMonthYear')" onMouseOut="HideToolTip('HelpMonthYear')" />
<div class="PHPCalendarLogoutHelp" id="HelpMonthYear"><?php echo $PHP_EVENT_LANG['Help_14']; ?></div>

                
                 </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDateFormat']; ?>:</td>
		    <td bgcolor="#f6f6f6"><select name="dateFormat" id="dateFormat" style="width:100px">
                  <?php
					foreach($DateFormats as $key=>$value){
						if($CalendarOptions["dateFormat"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                </select>              

<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpDateFormat')" onMouseOut="HideToolTip('HelpDateFormat')" />
<div class="PHPCalendarLogoutHelp" id="HelpDateFormat"><?php echo $PHP_EVENT_LANG['Help_15']; ?></div>
                
                
                </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsTimeFormat']; ?>:</td>
			  <td bgcolor="#f6f6f6"><select name="timeFormat" id="timeFormat" style="width:100px">
                <?php
					foreach($TimeFormats as $key=>$value){
						if($CalendarOptions["timeFormat"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                  </select>

<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpTimeFormat')" onMouseOut="HideToolTip('HelpTimeFormat')" />
<div class="PHPCalendarLogoutHelp" id="HelpTimeFormat"><?php echo $PHP_EVENT_LANG['Help_16']; ?></div>
                  
                  
                  </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsTitlesPosition']; ?>:</td>
              <td bgcolor="#f6f6f6"><select name="SummaryPosition" id="SummaryPosition">
              <option value="tooltip"<?php if($CalendarOptions["SummaryPosition"]=="tooltip") echo ' selected'; ?>><?php echo $PHP_EVENT_LANG['OptionsSummaryPositionTooltip']; ?></option>
              <option value="daycells"<?php if($CalendarOptions["SummaryPosition"]=="daycells") echo ' selected'; ?>><?php echo $PHP_EVENT_LANG['OptionsSummaryPositionDay']; ?></option>
              </select>
              
<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpTitles')" onMouseOut="HideToolTip('HelpTitles')" />
<div class="PHPCalendarLogoutHelp" id="HelpTitles"><?php echo $PHP_EVENT_LANG['Help_17']; ?></div>
              
              </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDescriptionPosition']; ?>:</td>
              <td bgcolor="#f6f6f6"><select name="DetailsPosition" id="DetailsPosition">
              <option value="below"<?php if($CalendarOptions["DetailsPosition"]=="below") echo ' selected'; ?>><?php echo $PHP_EVENT_LANG['OptionsDetailsPositionBelow']; ?></option>
              <option value="replace"<?php if($CalendarOptions["DetailsPosition"]=="replace") echo ' selected'; ?>><?php echo $PHP_EVENT_LANG['OptionsDetailsPositionReplace']; ?></option>
              <option value="right"<?php if($CalendarOptions["DetailsPosition"]=="right") echo ' selected'; ?>><?php echo $PHP_EVENT_LANG['OptionsDetailsPositionRight']; ?></option>
              </select>

<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpDescriptions')" onMouseOut="HideToolTip('HelpDescriptions')" />
<div class="PHPCalendarLogoutHelp" id="HelpDescriptions"><?php echo $PHP_EVENT_LANG['Help_18']; ?></div>
              
              
              </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6">&nbsp;</td>
              <td bgcolor="#f6f6f6">	<input type="image" name="Image9" id="Image9" src="images/save-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image9','','images/save-btn-over.png',1)">
</td>
            </tr>
            <tr>
              <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['OptionsBorders']; ?></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsOuterBorderSize']; ?>:</td>
			  <td bgcolor="#f6f6f6"><select name="borderSize" id="borderSize" style="width:40px">
                  <?php
					foreach($BorderSize as $key=>$value){
						if($CalendarOptions["borderSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
              </select>              </td>
    </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsOuterBorderColor']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="borderColor" type="text" id="borderColor" style="background-color:#<?php echo $CalendarOptions["borderColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["borderColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["borderColor"]; ?>" />
              <a href="#" onClick="cp.select(OptionsForm.borderColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image91" border="0" id="Image91" /></a></td>
    </tr>
            
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsInnerBorderSize']; ?>:</td>
			  <td bgcolor="#f6f6f6"><select name="innerBorderSize" id="innerBorderSize" style="width:40px">
                <?php
					foreach($BorderSize as $key=>$value){
						if($CalendarOptions["innerBorderSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
              </select>				</td>
    </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsInnerBorderColor']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="innerBorderColor" type="text" id="innerBorderColor" style="background-color:#<?php echo $CalendarOptions["innerBorderColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["innerBorderColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["innerBorderColor"]; ?>" />
              <a href="#" onClick="cp.select(OptionsForm.innerBorderColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a></td>
    </tr>
            <tr>
              <td bgcolor="#f6f6f6">&nbsp;</td>
              <td bgcolor="#f6f6f6">	<input type="image" name="Image8" id="Image8" src="images/save-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image8','','images/save-btn-over.png',1)">
</td>
            </tr>
            <tr>
              <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['OptionsArrows']; ?></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsPreviousMonth']; ?>:</td>
              <td bgcolor="#f6f6f6"><textarea name="leftArrow" cols="60" rows="2"><?php echo stripslashes(utf8_decode($CalendarOptions["leftArrow"])); ?></textarea></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsNextMonth']; ?>:</td>
              <td bgcolor="#f6f6f6"><textarea name="rightArrow" cols="60" rows="2"><?php echo stripslashes(utf8_decode($CalendarOptions["rightArrow"])); ?></textarea></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6">&nbsp;</td>
              <td bgcolor="#f6f6f6"><input type="image" name="Image7" id="Image7" src="images/save-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image7','','images/save-btn-over.png',1)"></td>
    </tr>
  </table>
</form>

<?php
} elseif ($_REQUEST["sac"]=='colors') {
?>

<form action="admin.php" method="post" name="OptionsForm" id="OptionsForm" style="padding:0px; margin:0px">
  <input type="hidden" name="ac" value="save_options" />
  <input type="hidden" name="sac" value="colors" />
  <input type="hidden" name="cid" value="<?php echo $_REQUEST["cid"]; ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer" style="margin-top:10px">
            <tr>
              <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['OptionsColors']; ?></td>
            </tr>
            <tr>
              <td width="43%" bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsMonthBackground']; ?>:</td>
			  <td width="57%" bgcolor="#f6f6f6"><input name="MonthBGColor" type="text" id="MonthBGColor" style="background-color:#<?php echo $CalendarOptions["MonthBGColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["MonthBGColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["MonthBGColor"]; ?>" /><a href="#" onClick="cp.select(OptionsForm.MonthBGColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a> </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsWeekDaysBackground']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="WeekDaysBGColor" type="text" id="WeekDaysBGColor" style="background-color:#<?php echo $CalendarOptions["WeekDaysBGColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["WeekDaysBGColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["WeekDaysBGColor"]; ?>" /><a href="#" onClick="cp.select(OptionsForm.WeekDaysBGColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDaysEvents']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="DayCellsWithEventsBGColor" type="text" id="DayCellsWithEventsBGColor" style="background-color:#<?php echo $CalendarOptions["DayCellsWithEventsBGColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["DayCellsWithEventsBGColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["DayCellsWithEventsBGColor"]; ?>" />
              <a href="#" onClick="cp.select(OptionsForm.DayCellsWithEventsBGColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDaysNoEvents']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="DayCellsWithoutEventsBGColor" type="text" id="DayCellsWithoutEventsBGColor" style="background-color:#<?php echo $CalendarOptions["DayCellsWithoutEventsBGColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["DayCellsWithoutEventsBGColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["DayCellsWithoutEventsBGColor"]; ?>" />
              <a href="#" onClick="cp.select(OptionsForm.DayCellsWithoutEventsBGColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDaysEmpty']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="DayCellsEmptyBGColor" type="text" id="DayCellsEmptyBGColor" style="background-color:#<?php echo $CalendarOptions["DayCellsEmptyBGColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["DayCellsEmptyBGColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["DayCellsEmptyBGColor"]; ?>" />
              <a href="#" onClick="cp.select(OptionsForm.DayCellsEmptyBGColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6">&nbsp;</td>
              <td bgcolor="#f6f6f6">	<input type="image" name="Image9" id="Image9" src="images/save-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image9','','images/save-btn-over.png',1)">
</td>
            </tr>
            <tr>
              <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['OptionsFontsStylesSizes']; ?></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsFontFamily']; ?>:</td>
			  <td bgcolor="#f6f6f6"><select name="FontFamily" id="FontFamily" style="width:160px">
                  <?php
					foreach($Fonts as $key=>$value){
						if($CalendarOptions["FontFamily"]==$key)
							echo "<option value='".$key."'  style='font-family:".$value."' selected>".$value."</option>";
						else
							echo "<option value='".$key."' style='font-family:".$value."'>".$value."</option>";		
					}
				?>
                </select>              </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsMonthFont']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="MonthFontColor" type="text" id="MonthFontColor" style="background-color:#<?php echo $CalendarOptions["MonthFontColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["MonthFontColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["MonthFontColor"]; ?>" />
                <a href="#" onClick="cp.select(OptionsForm.MonthFontColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a>&nbsp;
                <select name="MonthFontSize" id="MonthFontSize" style="width:40px">
                  <?php
					foreach($FontSize as $key=>$value){
						if($CalendarOptions["MonthFontSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                </select>
                &nbsp;
                <select name="MonthFontStyle" id="MonthFontStyle" style="width:80px">
                  <?php
					foreach($Styles as $key=>$value){
						if($CalendarOptions["MonthFontStyle"]==$key)
							echo "<option value='".$key."' selected>".$key."</option>";
						else
							echo "<option value='".$key."'>".$key."</option>";		
					}
				?>
                </select>              </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsWeekFont']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="WeekDaysFontColor" type="text" id="WeekDaysFontColor" style="background-color:#<?php echo $CalendarOptions["WeekDaysFontColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["WeekDaysFontColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["WeekDaysFontColor"]; ?>" />
                <a href="#" onClick="cp.select(OptionsForm.WeekDaysFontColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a>&nbsp;
                <select name="WeekDaysFontSize" id="WeekDaysFontSize" style="width:40px">
                  <?php
					foreach($FontSize as $key=>$value){
						if($CalendarOptions["WeekDaysFontSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                </select>
                &nbsp;
                <select name="WeekDaysFontStyle" id="WeekDaysFontStyle" style="width:80px">
                  <?php
					foreach($Styles as $key=>$value){
						if($CalendarOptions["WeekDaysFontStyle"]==$key)
							echo "<option value='".$key."' selected>".$key."</option>";
						else
							echo "<option value='".$key."'>".$key."</option>";		
					}
				?>
                </select>              </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDayEventsFont']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="DayCellsEventsFontColor" type="text" id="DayCellsEventsFontColor" style="background-color:#<?php echo $CalendarOptions["DayCellsEventsFontColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["DayCellsEventsFontColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["DayCellsEventsFontColor"]; ?>" />
			    <a href="#" onClick="cp.select(OptionsForm.DayCellsEventsFontColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a>&nbsp;
                <select name="DayCellsEventsFontSize" id="DayCellsEventsFontSize" style="width:40px">
                  <?php
					foreach($FontSize as $key=>$value){
						if($CalendarOptions["DayCellsEventsFontSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				  ?>
                </select>
				&nbsp;
				<select name="DayCellsEventsFontStyle" id="DayCellsEventsFontStyle" style="width:80px">
			  	<?php
					foreach($Styles as $key=>$value){
						if($CalendarOptions["DayCellsEventsFontStyle"]==$key)
							echo "<option value='".$key."' selected>".$key."</option>";
						else
							echo "<option value='".$key."'>".$key."</option>";		
					}
				?>
				</select>				</td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDaysNoEventsFont']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="DayCellsWithoutEventsFontColor" type="text" id="DayCellsWithoutEventsFontColor" style="background-color:#<?php echo $CalendarOptions["DayCellsWithoutEventsFontColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["DayCellsWithoutEventsFontColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["DayCellsWithoutEventsFontColor"]; ?>" />
                <a href="#" onClick="cp.select(OptionsForm.DayCellsWithoutEventsFontColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a>&nbsp;
                <select name="DayCellsWithoutEventsFontSize" id="DayCellsWithoutEventsFontSize" style="width:40px">
                  <?php
					foreach($FontSize as $key=>$value){
						if($CalendarOptions["DayCellsWithoutEventsFontSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                </select>
                &nbsp;
                <select name="DayCellsWithoutEventsFontStyle" id="DayCellsWithoutEventsFontStyle" style="width:80px">
                  <?php
					foreach($Styles as $key=>$value){
						if($CalendarOptions["DayCellsWithoutEventsFontStyle"]==$key)
							echo "<option value='".$key."' selected>".$key."</option>";
						else
							echo "<option value='".$key."'>".$key."</option>";		
					}
				?>
                </select>              </td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsDescriptionFont']; ?>:</td>
              <td bgcolor="#f6f6f6"><input name="EventsFontColor" type="text" id="EventsFontColor" style="background-color:#<?php echo $CalendarOptions["EventsFontColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["EventsFontColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["EventsFontColor"]; ?>" />
                <a href="#" onClick="cp.select(OptionsForm.EventsFontColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a>&nbsp;
                <select name="EventsFontSize" id="EventsFontSize" style="width:40px">
                  <?php
					foreach($FontSize as $key=>$value){
						if($CalendarOptions["EventsFontSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                </select>
                &nbsp;
                <select name="EventsFontStyle" id="EventsFontStyle" style="width:80px">
                  <?php
					foreach($Styles as $key=>$value){
						if($CalendarOptions["EventsFontStyle"]==$key)
							echo "<option value='".$key."' selected>".$key."</option>";
						else
							echo "<option value='".$key."'>".$key."</option>";		
					}
				?>
                </select></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6">&nbsp;</td>
              <td bgcolor="#f6f6f6">	<input type="image" name="Image8" id="Image8" src="images/save-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image8','','images/save-btn-over.png',1)">
</td>
            </tr>
            <tr>
              <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['OptionsEventsTitle']; ?></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsTitlesBackground']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="EventTitleBGColor" type="text" id="EventTitleBGColor" style="background-color:#<?php echo $CalendarOptions["EventTitleBGColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["EventTitleBGColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["EventTitleBGColor"]; ?>" />
                <a href="#" onClick="cp.select(OptionsForm.EventTitleBGColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsTitlesFont']; ?>:</td>
			  <td bgcolor="#f6f6f6"><input name="EventTitleFontColor" type="text" id="EventTitleFontColor" style="background-color:#<?php echo $CalendarOptions["EventTitleFontColor"]; ?>; color:#<?php echo oppColor($CalendarOptions["EventTitleFontColor"]); ?>" size="8" maxlength="7" value="<?php echo $CalendarOptions["EventTitleFontColor"]; ?>" />
			  	<a href="#" onClick="cp.select(OptionsForm.EventTitleFontColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image911" border="0" id="Image911" /></a></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['OptionsTitlesSize']; ?>:</td>
			  <td bgcolor="#f6f6f6"><select name="EventTitleFontSize" id="EventTitleFontSize" style="width:40px">
                <?php
					foreach($FontSize as $key=>$value){
						if($CalendarOptions["EventTitleFontSize"]==$key)
							echo "<option value='".$key."' selected>".$value."</option>";
						else
							echo "<option value='".$key."'>".$value."</option>";		
					}
				?>
                  </select></td>
            </tr>
            <tr>
              <td bgcolor="#f6f6f6">&nbsp;</td>
              <td bgcolor="#f6f6f6">	<input type="image" name="Image7" id="Image7" src="images/save-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image7','','images/save-btn-over.png',1)">
</td>
            </tr>
        </table>
</form>


<?php
};
?>