<?php
defined( '_PHPEventCalendar' ) or die( 'Restricted access' );

	if(!isset($_REQUEST["dt"])) $_REQUEST["dt"] = time();
	
	$timeFormat = $CalendarOptions["timeFormat"];
	$sHour = -1; $eHour = -1; $sMin = -1; $eMin = -1;
	if($_REQUEST["eid"]>0){
		$sql = "SELECT * FROM ".$TABLES["Events"]."	WHERE id='".SaveToDB($_REQUEST["eid"])."' AND calendarId = '".SaveToDB($_REQUEST["cid"])."'"; 
		$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
		$EventData = mysql_fetch_assoc($sql_result);
		
		$_REQUEST["dt"] = strtotime($EventData["dt"]);
		
		$startTime = explode(":", ReadFromDB($EventData["startTime"]));
		$sHour = $startTime[0];
		$sMin  = $startTime[1];
		$endTime = explode(":", ReadFromDB($EventData["endTime"]));
		$eHour = $endTime[0];
		$eMin  = $endTime[1];
		
		$sTime = "am";
		$eTime = "am";
		
		if($timeFormat==1){	
			if($sHour==12) $sTime = "pm";
			if($sHour==0) $sHour = 12;
			if($sHour>12) {
				$sHour = $sHour - 12;
				$sTime = "pm";
			}
			
			if($eHour==12) $eTime = "pm";
			if($eHour==0) $eHour = 12;
			if($eHour>12) {
				$eHour = $eHour - 12;
				$eTime = "pm";
			}
		}
		
		if(ReadFromDB($EventData["show_time"])=="2"){
			$sHour = -1; 
			$sMin = -1; 
		} elseif (ReadFromDB($EventData["show_time"])=="3") {
			$eHour = -1; 
			$eMin = -1;	
		} elseif (ReadFromDB($EventData["show_time"])=="4") {
			$sHour = -1; 
			$sMin = -1; 
			$eHour = -1; 
			$eMin = -1;	
		}
	}
	$dt = getFormatedDate($_REQUEST["dt"]);
?>
<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//-->
</script>

<form action="admin.php" method="post" style="margin:0px; padding:0px" name="frm">
  <input type="hidden" name="ac" value="save_data" />
  <input type="hidden" name="cid" value="<?php echo $_REQUEST["cid"]; ?>" />
  <input type="hidden" name="eid" value="<?php echo $_REQUEST["eid"]; ?>" />
  <input type="hidden" name="findEvent" value="<?php echo $_REQUEST["findEvent"]; ?>" />
	<table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer">
        <tr>
          <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['EventDateTime']; ?></td>
      </tr>
        <tr>
		  <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventDate']; ?>:</td>
          <td valign="middle" bgcolor="#f6f6f6">
	  	    <input type="text" name="dt"  id="dt" maxlength="10" size="12" value="<?php echo $dt; ?>" readonly="true" >
			  <img src="images/menu-calendar.png" alt="Pick a date" title="Pick a date" width="23" height="21" border="0" align="top" style="margin:0px; cursor:pointer" onClick="javascript:displayCalendar(document.frm.dt,'<?php echo $DateFormats[$CalendarOptions["dateFormat"]]; ?>',document.frm.dt)" />
<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpDate')" onMouseOut="HideToolTip('HelpDate')" />
<div class="PHPCalendarLogoutHelp" id="HelpDate"><?php echo $PHP_EVENT_LANG['Help_1']; ?></div>
            
              
              
		  </td>
        </tr>
        <tr>
          <td width="16%" bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventStartTime']; ?>:</td>
          <td width="84%" bgcolor="#f6f6f6">
       	  <select name="sHour" id="sHour">
            	<option  value="-1">----</option>
				<?php
                    if($timeFormat==1) {
                        $startAt = 1;
                        $endAt = 12;
                    }	
                    if($timeFormat==2) {
                        $startAt = 0;
                        $endAt = 23;
                    }
                    for ($i=$startAt; $i<=$endAt; $i++){
                        if ($sHour==$i) { $selected=' selected="selected"'; } else { $selected=''; };
                        echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
                    };
                ?>
            </select>
            <select name="sMin" id="sMin">
            	<option  value="-1">----</option>	
				<?php
                    for ($i=0; $i<60; $i = ($i+5)) {
                      if ($sMin==$i) { $selected=' selected="selected"'; } else { $selected=''; };
                      echo '<option value="'.$i.'"'.$selected.'>';
                      if($i < 10) echo "0" . $i;
                      else echo $i;
                      echo '</option>';
                    };
                ?>
            </select>   
			<?php
				if($timeFormat==1) {
			?>
				<select name="sTime">
					<option value="am" <?php if($sTime=="am") echo "selected"; ?>>AM</option>
					<option value="pm" <?php if($sTime=="pm") echo "selected"; ?>>PM</option>
				</select>
			<?php
				}
			?>
            
<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpStartTime')" onMouseOut="HideToolTip('HelpStartTime')" />
<div class="PHPCalendarLogoutHelp" id="HelpStartTime"><?php echo $PHP_EVENT_LANG['Help_2']; ?></div>
            
          </td>
        </tr>
        <tr>
          <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventEndTime']; ?>:</td>
          <td bgcolor="#f6f6f6">
       	  <select name="eHour" id="eHour">
            	<option value="-1">----</option>	
				<?php
                    if($timeFormat==1) {
                        $startAt = 1;
                        $endAt = 12;
                    }	
                    if($timeFormat==2) {
                        $startAt = 0;
                        $endAt = 23;
                    }
                    for ($i=$startAt; $i<=$endAt; $i++){
                        if ($eHour==$i) { $selected=' selected="selected"'; } else { $selected=''; };
                        echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
                    };
                ?>
            </select>
            <select name="eMin" id="eMin">
	            <option  value="-1">----</option>	
				<?php
                    for ($i=0; $i<60; $i = ($i+5)) {
                      if ($eMin==$i) { $selected=' selected="selected"'; } else { $selected=''; };
                      echo '<option value="'.$i.'"'.$selected.'>';
                      if($i < 10) echo "0" . $i;
                      else echo $i;
                      echo '</option>';
                    };
                ?>
            </select>   
			<?php
                if($timeFormat==1) {
            ?>
                <select name="eTime">
                    <option value="am" <?php if($eTime=="am") echo "selected"; ?>>AM</option>
                    <option value="pm" <?php if($eTime=="pm") echo "selected"; ?>>PM</option>
                </select>
            <?php
                }
            ?>	
            
<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpEndTime')" onMouseOut="HideToolTip('HelpEndTime')" />
<div class="PHPCalendarLogoutHelp" id="HelpEndTime"><?php echo $PHP_EVENT_LANG['Help_3']; ?></div>
            		
		  </td>
        </tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer" style="margin-top:10px">
        <tr>
          <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['EventRecurring']; ?></td>
      </tr>
	    <tr>
            <td width="16%" bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventRecurringRepeat']; ?>:</td>
            <td width="84%" bgcolor="#f6f6f6">
                <select name="period" id="period" onChange="ChangePeriodText()">
                  <option value="NA">--</option>
                  <option value="Day"><?php echo $PHP_EVENT_LANG['EventRecurringDay']; ?></option>
                  <option value="Week"><?php echo $PHP_EVENT_LANG['EventRecurringWeek']; ?></option>
                  <option value="Month"><?php echo $PHP_EVENT_LANG['EventRecurringMonth']; ?></option>
                  <option value="Year"><?php echo $PHP_EVENT_LANG['EventRecurringYear']; ?></option>
			    </select> &nbsp; <?php echo $PHP_EVENT_LANG['EventRecurringNext']; ?> &nbsp; 
			    <input type="text" name="repeat" size="2" maxlength="3" /> <span id="monthId"></span>

<img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpRecurring')" onMouseOut="HideToolTip('HelpRecurring')" />
<div class="PHPCalendarLogoutHelp" id="HelpRecurring"><?php echo $PHP_EVENT_LANG['Help_4']; ?></div>
                
                
			</td>
	    </tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer" style="margin-top:10px">
        <tr>
          <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['EventDetails']; ?></td>
      </tr>
        <tr>
          <td width="16%" bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventStatus']; ?>:</td>
          <td width="84%" bgcolor="#f6f6f6"><select name="show_event" id="show_event" onchange="ChangePeriodText()">
            <option value="Show"<?php if ($EventData["show_event"]=='Show') echo ' selected';  ?>><?php echo $PHP_EVENT_LANG['EventStatusShow']; ?></option>
            <option value="Hide"<?php if ($EventData["show_event"]=='Hide') echo ' selected';  ?>><?php echo $PHP_EVENT_LANG['EventStatusHide']; ?></option>
          </select>
          <img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpStatus')" onMouseOut="HideToolTip('HelpStatus')" />
          <div class="PHPCalendarLogoutHelp" id="HelpStatus"><?php echo $PHP_EVENT_LANG['Help_5']; ?></div>
          </td>
      </tr>
        <tr>
          <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventColor']; ?>:</td>
          <td bgcolor="#f6f6f6"><input name="eventColor" type="text" id="eventColor" size="8" maxlength="7" value="<?php echo ReadFromDB($EventData["color"]); ?>" />
          &nbsp;<a href="#" onClick="cp.select(frm.eventColor,'pick2');return false;" name="pick2" id="pick2"><img src="images/color.png" alt="Select color" title="Select color" name="Image99" width="18" height="19" border="0" align="absmiddle" id="Image99" /></a>

          <img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpColor')" onMouseOut="HideToolTip('HelpColor')" />
          <div class="PHPCalendarLogoutHelp" id="HelpColor"><?php echo $PHP_EVENT_LANG['Help_6']; ?></div>

          </td>
      </tr>
        <tr>
          <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventTitle']; ?>:</td>
          <td bgcolor="#f6f6f6"><input type="text" size="50" maxlength="50" name="title" id="title" value="<?php echo ReadFromDB($EventData["title"]); ?>">

          <img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpTitle')" onMouseOut="HideToolTip('HelpTitle')" />
          <div class="PHPCalendarLogoutHelp" id="HelpTitle"><?php echo $PHP_EVENT_LANG['Help_7']; ?></div>
          
          </td>
      </tr>
        <tr>
	      <td valign="top" bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EventDescription']; ?>:</td>
          <td bgcolor="#f6f6f6"><textarea name="description" id="description" cols="60" rows="10" style="width:100%"><?php echo ReadFromDB($EventData["description"]); ?></textarea></td>
    </tr>
    <?php
		$sql = "SELECT * FROM ".$TABLES["Events"]."	WHERE uid='".SaveToDB($EventData["uid"])."' AND calendarId = '".SaveToDB($_REQUEST["cid"])."'"; 
		$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
		if(mysql_num_rows($sql_result)>1){
	?>
    <tr>
      <td colspan="2" bgcolor="#f6f6f6"><input type="checkbox" name="apply_to_all" value="true" /> <?php echo $PHP_EVENT_LANG['EventApplyRecurring']; ?></td>
    </tr>
    <?php
		}
	?>	
  </table>
	<div style="padding-left:110px; padding-top:15px; padding-bottom:20px">
	<input type="image" name="Image9" id="Image9" src="images/save-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image9','','images/save-btn-over.png',1)">
	</div>
</form>
