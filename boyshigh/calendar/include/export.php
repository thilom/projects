<?php
defined( '_PHPEventCalendar' ) or die( 'Restricted access' );

$format = $CalendarOptions["dateFormat"];	
$startDate = getFormatedDate(mktime(0,0,0,date("n"),1,date("Y")));
$endDate = getFormatedDate(mktime(0,0,0,date("n"),date("t"),date("Y")));
echo $PHP_EVENT_LANG['ExportInfo'];
?>

<form method="post" action="export.php" name="frm" id="frm" target="_blank">
<input type="hidden" name="cid" value="<?php echo $_REQUEST["cid"]; ?>" />
  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="PHPCalendarInnerContainer">
        <tr>
          <td colspan="2" class="PHPCalendarHeading"><?php echo $PHP_EVENT_LANG['ExportTitle']; ?></td>
      </tr>
  <tr>
    <td width="232" bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['StartDate']; ?>:</td>
<td width="1369" bgcolor="#f6f6f6"><input type="text" name="startDate"  id="startDate" maxlength="10" size="12" value="<?php echo $startDate; ?>" readonly="true" >
			  <img src="images/menu-calendar.png" alt="Pick a date" title="Pick a date" border="0" align="absmiddle" style="margin:0px; cursor:pointer" onClick="displayCalendar(document.frm.startDate,'<?php echo $DateFormats[$format]; ?>',document.frm.startDate)" />
              

 <img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpExportStartDate')" onMouseOut="HideToolTip('HelpExportStartDate')" />
          <div class="PHPCalendarLogoutHelp" id="HelpExportStartDate"><?php echo $PHP_EVENT_LANG['Help_8']; ?></div>              
              </td>
  </tr>
  <tr>
    <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['EndDate']; ?>:</td>
<td bgcolor="#f6f6f6"><input type="text" name="endDate" id="endDate" maxlength="10" size="12" value="<?php echo $endDate; ?>" readonly="true" />  <img src="images/menu-calendar.png" alt="Pick a date" title="Pick a date" border="0" align="absmiddle" style="margin:0px; cursor:pointer" onClick="displayCalendar(document.frm.endDate,'<?php echo $DateFormats[$format]; ?>',document.frm.endDate)" />

 <img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpExportEndDate')" onMouseOut="HideToolTip('HelpExportEndDate')" />
          <div class="PHPCalendarLogoutHelp" id="HelpExportEndDate"><?php echo $PHP_EVENT_LANG['Help_9']; ?></div>              

</td>
  </tr>
  <tr>
    <td bgcolor="#f6f6f6"><?php echo $PHP_EVENT_LANG['Format']; ?>:</td>
    <td bgcolor="#f6f6f6">
      <select name="format" id="format">
        <option value="CSV">CSV</option>
        <option value="iCalendar">iCalendar</option>
        <option value="XML">XML</option>
      </select> <img src="images/help.png" width="9" height="13" style="cursor:pointer" onMouseOver="ShowToolTip('HelpExportFormat')" onMouseOut="HideToolTip('HelpExportFormat')" />
          <div class="PHPCalendarLogoutHelp" id="HelpExportFormat"><?php echo $PHP_EVENT_LANG['Help_10']; ?></div>
	</td>
  </tr>
</table>
	<div style="padding-left:120px; padding-top:15px; padding-bottom:20px">
	<input type="image" name="Image9" id="Image9" src="images/export-btn.png" onMouseOut="MM_swapImgRestore()"  onMouseOver="MM_swapImage('Image9','','images/export-btn-over.png',1)">
	</div>

</form>


<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="12%"><img src="images/export-csv.png" width="73" height="69" /></td>
    <td width="88%"><span style="margin-bottom:10px"><?php echo $PHP_EVENT_LANG['CSV']; ?></span></td>
  </tr>
  <tr>
    <td><img src="images/export-ical.png" width="73" height="68" /></td>
    <td><span style="margin-bottom:10px"><?php echo $PHP_EVENT_LANG['iCalendar']; ?></span></td>
  </tr>
  <tr>
    <td><img src="images/export-xml.png" width="74" height="69" /></td>
    <td><?php echo $PHP_EVENT_LANG['XML']; ?></td>
  </tr>
</table>
