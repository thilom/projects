<?php
error_reporting(0);
include_once("include/include.php");

$calendarId = htmlentities($_REQUEST["cid"]);
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");

$cMonth = htmlentities($_REQUEST["month"]);
$cYear = htmlentities($_REQUEST["year"]);

$prev_year = $cYear;
$next_year = $cYear; 

$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}

$sql = "SELECT * FROM ".$TABLES["Calendars"]." WHERE id='".SaveToDB($calendarId)."'";
$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
$Calendar = mysql_fetch_assoc($sql_result);
$CalendarOptions = unserialize(ReadHTMLFromDB($Calendar["options"]));

$calMonthName = $Months[$cMonth];
$formatMonth = getFormatedMonthYear($calMonthName,$cYear);

$FontFamily = $Fonts[$CalendarOptions["FontFamily"]];

$MonthFontSize = $FontSize[$CalendarOptions["MonthFontSize"]];
$MonthFontStyle = $Styles[$CalendarOptions["MonthFontStyle"]];

$WeekDaysFontSize = $FontSize[$CalendarOptions["WeekDaysFontSize"]];
$WeekDaysFontStyle = $Styles[$CalendarOptions["WeekDaysFontStyle"]];

$DayCellsEventsFontSize = $FontSize[$CalendarOptions["DayCellsEventsFontSize"]];
$DayCellsEventsFontStyle = $Styles[$CalendarOptions["DayCellsEventsFontStyle"]];

$DayCellsWithoutEventsFontSize = $FontSize[$CalendarOptions["DayCellsWithoutEventsFontSize"]];
$DayCellsWithoutEventsFontStyle = $Styles[$CalendarOptions["DayCellsWithoutEventsFontStyle"]];

$borderSize = $BorderSize[$CalendarOptions["borderSize"]];
$innerBorderSize = $BorderSize[$CalendarOptions["innerBorderSize"]];

$cellBorder = "border-collapse:collapse; border:solid ".$innerBorderSize."px #" . $CalendarOptions["innerBorderColor"] . "; ";

$EventTitleFontSize = $FontSize[$CalendarOptions["EventTitleFontSize"]];

$calendarWidth = $CalendarOptions["width"];
$colWidth = ceil($calendarWidth / 7);

$calendarHeight = $CalendarOptions["height"];
$rowHeight = ceil($calendarHeight / 7);
?>

<table width="<?php echo $calendarWidth; ?>px" height="<?php echo $calendarHeight; ?>px" border="0" cellspacing="0" cellpadding="2" style="border:solid <?php echo $borderSize; ?>px #<?php echo $CalendarOptions["borderColor"]; ?>">
  <tr>
    <td align="center" bgcolor="#<?php echo $CalendarOptions["MonthBGColor"]; ?>"><a href="javascript:ajaxpage('<?php echo $SETTINGS["installFolder"]."load_calendar.php?month=". $prev_month . "&year=" . $prev_year. "&cid=".$calendarId; ?>','EventCalendar<?php echo $calendarId; ?>','get');" style="font-family:<?php echo $FontFamily; ?>; color:#<?php echo $CalendarOptions["MonthFontColor"]; ?>; font-size:<?php echo $MonthFontSize; ?>px; <?php echo $MonthFontStyle; ?>"><?php echo ReadHTMLFromDB($CalendarOptions["leftArrow"]); ?></a></td>
    <td colspan="5" align="center" bgcolor="#<?php echo $CalendarOptions["MonthBGColor"]; ?>" style="font-family:<?php echo $FontFamily; ?>; color:#<?php echo $CalendarOptions["MonthFontColor"]; ?>; font-size:<?php echo $MonthFontSize; ?>px; <?php echo $MonthFontStyle; ?>; padding:10px"><?php echo $formatMonth; ?></td>
    <td align="center" bgcolor="#<?php echo $CalendarOptions["MonthBGColor"]; ?>"><a href="javascript:ajaxpage('<?php echo $SETTINGS["installFolder"]."load_calendar.php?month=". $next_month . "&year=" . $next_year. "&cid=".$calendarId; ?>','EventCalendar<?php echo $calendarId; ?>','get');" style="font-family:<?php echo $FontFamily; ?>; color:#<?php echo $CalendarOptions["MonthFontColor"]; ?>; font-size:<?php echo $MonthFontSize; ?>px; <?php echo $MonthFontStyle; ?>"><?php echo ReadHTMLFromDB($CalendarOptions["rightArrow"]); ?></a></td>
  </tr>
  <tr>
  	<?php
		$weekStartDay = $CalendarOptions["startDay"];
		for($j=0;$j<7;$j++){
			$ind = $weekStartDay+$j;
			if($ind>6) $ind = $ind - 7;
			echo "<td align='center' bgcolor='#" . $CalendarOptions["WeekDaysBGColor"] . "' style='font-family:".$FontFamily."; color:#".$CalendarOptions["WeekDaysFontColor"]."; font-size:".$WeekDaysFontSize."px; ".$WeekDaysFontStyle."; width:".$colWidth."px; padding-top:5px; padding-bottom:5px' >".$Days[$ind]."</td>";
		}	
	?>
  </tr>  
  
<?php
	$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
	$maxday = date("t",$timestamp);
	$thismonth = getdate ($timestamp);
	$startday = $thismonth['wday']; 

	$offset = $startday - $weekStartDay;
	if($offset<0) $offset = 7 + $offset;

	$Events = getEvents($calendarId, $cMonth, $cYear,'Show');
	$DailyColors = getColors($calendarId, $cMonth, $cYear);

	for ($i=0; $i<($offset+$maxday); $i++) {
		if(($i % 7) == 0 ) echo "<tr>";
		if($i < $offset) {
			echo "<td bgcolor='#" . $CalendarOptions["DayCellsEmptyBGColor"] . "' style='".$cellBorder."'>&nbsp;</td>";
		} else {
			$day = $i - $offset + 1;
			$sqlMonth = ($cMonth<10) ? "0".$cMonth : $cMonth;
			$sqlDay = ($day<10) ? "0".$day : $day;
			$sqlDate = $cYear."-".$sqlMonth."-".$sqlDay;
			
			$MouseEvent = '';
			$EventsSummary = '';

			if(isset($DailyColors[$sqlDate])) $backgroundColor = 'background-color:#' . ReadFromDB($DailyColors[$sqlDate]["dayColor"]);

			if (isset($Events[$sqlDate])) {

				if($CalendarOptions["SummaryPosition"]=="tooltip") {
					$EventsSummary = '<div class="toolTipNote" id="d'.$day.'" style="text-decoration:none; background-color:#'.$CalendarOptions["EventTitleBGColor"].'; font-family:'.$FontFamily.'; font-size:'.$EventTitleFontSize.'px; color:#'.$CalendarOptions["EventTitleFontColor"].' ">';
				} else {
					$EventsSummary = '<div id="d'.$day.'" style="text-align:left; text-decoration:none; background-color:#'.((isset($DailyColors[$sqlDate]))?ReadFromDB($DailyColors[$sqlDate]["dayColor"]):$CalendarOptions["EventTitleBGColor"]).'; font-family:'.$FontFamily.'; font-size:'.$EventTitleFontSize.'px; color:#'.$CalendarOptions["EventTitleFontColor"].' ">';
				};	


				foreach($Events[$sqlDate] as $event){
					$show_time = "";
					if(ReadFromDB($event["show_time"])=="1" or ReadFromDB($event["show_time"])=="3") $show_time = getFormatedTime(ReadFromDB($event["startTime"])).' - ';
					$EventsSummary .= $show_time.ReadHTMLFromDB($event["title"]).'<br />';
				}
				
				$EventsSummary .= '</div>';
				
				if($CalendarOptions["SummaryPosition"]=="tooltip") {
					$MouseEvent .= ' onMouseOver="ShowToolTip(\'d'.$day.'\')" onMouseOut="HideToolTip(\'d'.$day.'\')"';
				};	
					
				if(!isset($DailyColors[$sqlDate])) $backgroundColor = 'background-color:#' . $CalendarOptions["DayCellsWithEventsBGColor"];
				$dayFontColor = 'color:#' . $CalendarOptions["DayCellsEventsFontColor"];
				$dayFontSize = $DayCellsEventsFontSize;
				$dayFontStyle = $DayCellsEventsFontStyle;
					
			} else {
				if(!isset($DailyColors[$sqlDate])) $backgroundColor = "background-color:#" . $CalendarOptions["DayCellsWithoutEventsBGColor"];
				$dayFontColor = 'color:#' . $CalendarOptions["DayCellsWithoutEventsFontColor"];
				$dayFontSize = $DayCellsWithoutEventsFontSize;
				$dayFontStyle = $DayCellsWithoutEventsFontStyle;
			}
			

			if($CalendarOptions["DetailsPosition"]=="replace") {
				$onClick = ' onClick="ajaxpage(\''.$SETTINGS["installFolder"].'load_date_events.php?dt='.strtotime($cYear.'-'.$cMonth.'-'.$day).'&cid='.$calendarId.'\',\'EventCalendar'.$calendarId.'\',\'get\');"';
			} else {
				$onClick = ' onClick="ajaxpage(\''.$SETTINGS["installFolder"].'load_date_events.php?dt='.strtotime($cYear.'-'.$cMonth.'-'.$day).'&cid='.$calendarId.'\',\'DateEvents'.$calendarId.'\',\'get\');"';

			};

		
			
			echo '<td align="left" valign="top" style="cursor:pointer; '.$cellBorder.$backgroundColor.'; height:'.$rowHeight.'px" '.$MouseEvent.' '.$onClick.'>
					<span  style="font-family:'.$FontFamily.'; font-size:'.$dayFontSize.'px; '.$dayFontStyle.' '.$dayFontColor.'">'.$day.'</span>'.$EventsSummary.
				 '</td>';
		}	 
		if(($i % 7) == 6 ) echo "</tr>";
	}
	
	for ($i=($offset+$maxday); $i<42; $i++){
		if(($i % 7) == 0 ) echo "<tr>";
		echo "<td bgcolor='#" . $CalendarOptions["DayCellsEmptyBGColor"] . "' style='cursor:pointer; ".$cellBorder."; height:".$rowHeight."px' >&nbsp;</td>";
		if(($i % 7) == 6 ) echo "</tr>";
	}
?>
</table>