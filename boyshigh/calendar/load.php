<?php
error_reporting(0);
include_once("include/include.php");

if (!isset($_REQUEST["cid"])) $_REQUEST["cid"] = 1;
$calendarId = htmlentities($_REQUEST["cid"]);
$sql = "SELECT * FROM ".$TABLES["Calendars"]." WHERE id='".SaveToDB($calendarId)."'";
$sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
$Calendar = mysql_fetch_assoc($sql_result);
$CalendarOptions = unserialize(ReadHTMLFromDB($Calendar["options"]));

if($CalendarOptions["DetailsPosition"]=="right") {

	$loadCalendar = '
	<div id="EventCalendar'.$calendarId.'" style="float:left"></div>
	<div id="DateEvents'.$calendarId.'" style="margin:0px; padding:0px; float:left"></div>
	<div style="clear:both"></div>
	<script language="javascript">
	ajaxpage("'.$SETTINGS["installFolder"].'load_calendar.php?cid='.$calendarId.'","EventCalendar'.$calendarId.'","get");
	</script>
	';


} else {

	$loadCalendar = '
	<div id="EventCalendar'.$calendarId.'"></div>
	<div id="DateEvents'.$calendarId.'" style="margin:0px; padding:0px"></div>
	
	<script language="javascript">
	ajaxpage("'.$SETTINGS["installFolder"].'load_calendar.php?cid='.$calendarId.'","EventCalendar'.$calendarId.'","get");
	</script>
	';
	
};

$loadCalendar = preg_replace("/\n/","",$loadCalendar);
$loadCalendar = preg_replace("/\r/","",$loadCalendar);

?>

var flagCaptcha = false;
var flagFields = true;
var message = 'Please fill in all mandatory fields ! \n';

var bustcachevar=1; //bust potential caching of external pages after initial request? (1=yes, 0=no)
var bustcacheparameter="";

function createRequestObject(){
	try	{
		xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	}	catch(e)	{
		alert('Sorry, but your browser doesn\'t support XMLHttpRequest.');
	};
	return xmlhttp;
};

function ajaxpage(url, containerid, requesttype){
	var page_request = createRequestObject();

	if (bustcachevar) bustcacheparameter=(url.indexOf("?")!=-1)? "&"+new Date().getTime() : "?"+new Date().getTime();
	page_request.open('GET', url+bustcacheparameter, true);
	page_request.send(null);

	page_request.onreadystatechange=function(){
		loadpage(page_request, containerid);
	}

}

function loadpage(page_request, containerid){
	if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1))
	document.getElementById(containerid).innerHTML=page_request.responseText;
}

function ShowToolTip(object) {
    document.getElementById(object).style.visibility = 'visible';
}

function HideToolTip(object) {
    document.getElementById(object).style.visibility = 'hidden';
}

loadCalendar = '<?php echo $loadCalendar; ?>';
document.writeln(loadCalendar);